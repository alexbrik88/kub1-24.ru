<?php

use common\components\date\DateHelper;
use frontend\modules\documents\components\Message;
use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Modal;
use frontend\models\Documents;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use common\components\helpers\ArrayHelper;

/* @var \common\models\document\Act $model */
/* @var $message Message */
/* @var $actEssence \common\models\document\ActEssence */

$productionType = explode(', ', $model->invoice->production_type);
$invoice = $model->invoice;
$agreementArray = $invoice->contractor->getAgreements()->orderBy(['document_date' => SORT_DESC])->all();
$invoiceItemValue = "Счет&{$invoice->fullNumber}&{$invoice->document_date}&";
$invoiceItemName = 'Счет № ' . $invoice->fullNumber . ' от ' .
    DateHelper::format($invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
$agreementDropDownList = ['add-modal-agreement' => '[ + Добавить договор ]'] +
    [$invoiceItemValue => $invoiceItemName] + ArrayHelper::map($agreementArray, 'listItemValue', 'listItemName');
?>
<div class="portlet customer-info">
    <div class="portlet-title">
        <div class="col-md-10 caption">
            <?= $message->get(Message::TITLE_SHORT_SINGLE); ?>
            №
            <?= Html::activeTextInput($model, 'document_number', [
                'id' => 'account-number',
                'data-required' => 1,
                'class' => 'form-control input-editable-field ',
                'style' => 'max-width: 60px; display:inline-block;',
                'value' => $model->getDocumentNumber(),
            ]); ?>
            <?= Html::activeTextInput($model, 'document_additional_number', [
                'maxlength' => true,
                'class' => 'form-control  input-editable-field ',
                'placeholder' => 'доп. номер',
                'style' => 'max-width: 110px; display:inline-block;',
            ]); ?>

            от

            <div class="input-icon input-calendar input-editable-field">
                <i class="fa fa-calendar"></i>
                <?= Html::activeTextInput($model, 'document_date', [
                    'class' => 'form-control date-picker',
                    'size' => 16,
                    'data-date-viewmode' => 'years',
                    'value' => DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                ]); ?>
            </div>
        </div>

        <div class="actions">
            <?php if ($canUpdate) : ?>
                <button type="submit" class="btn-save btn darkblue btn-sm " style="color: #FFF;"
                        title="Сохранить">
                    <span class="ico-Save-smart-pls"></span></button>
                <a href="<?= Url::to(['view', 'type' => $model->type, 'id' => $model->id]) ?>"
                   class="btn-cancel btn darkblue btn-sm " style="color: #FFF;"
                   title="Отменить">
                    <span class="ico-Cancel-smart-pls"></span></a>

            <?php endif; ?>
        </div>
    </div>
    <div class="portlet-body no_mrg_bottom">
        <table class="table no_mrg_bottom">
            <tr>
                <td>
                    <span
                            class="customer-characteristic"><?= $message->get(Message::CONTRACTOR); ?>
                        :</span>
                    <span><?= Html::a($model->invoice->contractor_name_short, [
                            '/contractor/view',
                            'type' => $model->invoice->contractor->type,
                            'id' => $model->invoice->contractor->id,
                        ]) ?></span>
                </td>
            </tr>
            <tr>
                <td style="line-height: 32px;">
                    <div class="row" style="margin-bottom: 10px;margin-top: 10px;">
                        <div class="col-sm-6" style="max-width: 100px; padding-right: 0;">
                        <span class="customer-characteristic">
                            Основание:
                        </span>
                        </div>
                        <div class="col-sm-7" style="padding-left: 7px;">
                            <div class="input-editable-field" style="max-width: 450px;">
                                <?php Pjax::begin([
                                    'id' => 'agreement-pjax-container',
                                    'enablePushState' => false,
                                    'linkSelector' => false,
                                ]);
                                echo Select2::widget([
                                    'model' => $model,
                                    'attribute' => 'agreement',
                                    'data' => $agreementDropDownList,
                                    'hideSearch' => true,
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'placeholder' => '',
                                    ],
                                ]);
                                Pjax::end(); ?>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                <tr>
                    <td>
                        <div class="col-sm-12 field-act_comment p-l-0 p-r-0">
                            <label class="text-bold" style="font-size: 13px;">
                                Комментарий в акте для покупателя:
                            </label>
                            <div style="margin-bottom: 5px;">
                                <?= Html::activeTextarea($model, 'comment', [
                                    'maxlength' => true,
                                    'class' => 'form-control',
                                    'style' => 'width: 100%;font-size: 13px;',
                                    'value' => $actEssence->is_checked && $model->isNewRecord && $model->type == Documents::IO_TYPE_OUT ?
                                        $actEssence->text : $model->comment,
                                ]); ?>
                            </div>
                        </div>
                        <div class="col-sm-12 field-act-essence p-l-0 p-r-0">
                            <div class="row">
                                <div class="col-xs-1" style="width: 3%;">
                            <span style="position: relative;">
                                <?= Html::activeCheckbox($actEssence, 'is_checked', [
                                    'class' => 'form-control',
                                    'label' => false,
                                ]); ?>
                            </span>
                                </div>
                                <div class="col-xs-11">
                                    <label for="actessence-is_checked" class="control-label"
                                           style="padding-top: 0!important;font-size: 13px;">
                                        Сохранить текст для всех актов
                                    </label>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            <?php endif; ?>
            <tr>
                <td>
                    <div style="margin-bottom: 5px;">
                        <?= \frontend\modules\documents\widgets\DocumentFileScanWidget::widget([
                            'model' => $model,
                            'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
                            'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
                            'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
                            'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
                            'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
                            'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
                            'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
                        ]); ?>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
<?php
Modal::begin([
    'id' => 'agreement-modal-container',
    'header' => '<h3 id="agreement-modal-header">Добавить договор</h3>',
    'options' => ['style' => 'max-width: 600px;']
]);

Pjax::begin([
    'id' => 'agreement-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]);

Pjax::end();

Modal::end();

$this->registerJs('
$(document).on("change", "#act-agreement", function(e) {
    var value = $(this).val() || $(this).text();
    if (value == "add-modal-agreement") {
        e.preventDefault();

        $.pjax({url: "' . Url::to([
        '/documents/agreement/create',
        'contractor_id' => $model->invoice->contractor_id,
        'type' => $model->type,
        'returnTo' => 'act',
        'container' => 'agreement-select-container',
    ]) . '", container: "#agreement-form-container", push: false});

        $(document).on("pjax:success", function() {
            $("#agreement-modal-header").html($("[data-header]").data("header"));
            $(".date-picker").datepicker({format: "dd.mm.yyyy", language:"ru", autoclose: true}).on("change.dp", dateChanged);

            function dateChanged(ev) {
                if (ev.bubbles == undefined) {
                    var $input = $("[name=\'" + ev.currentTarget.name +"\']");
                    if (ev.currentTarget.value == "") {
                        if ($input.data("last-value") == null) {
                            $input.data("last-value", ev.currentTarget.defaultValue);
                        }
                        var $lastDate = $input.data("last-value");
                        $input.datepicker("setDate", $lastDate);
                    } else {
                        $input.data("last-value", ev.currentTarget.value);
                    }
                }
            };
        });
        $("#agreement-modal-container").modal("show");
        $("#act-agreement").val("").trigger("change");
    }
});
$("#agreement-pjax-container").on("pjax:complete", function() {
    if (window.AgreementValue) {
        $("#act-agreement").val(window.AgreementValue).trigger("change");
    }
});
'); ?>
