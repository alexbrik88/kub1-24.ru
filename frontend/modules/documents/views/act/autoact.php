<?php

use common\models\document\Autoact;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\document\Autoact */

if (empty($model)) {
    $model = Yii::$app->user->identity->company->autoact;
}
?>

<?php Pjax::begin([
    'id' => 'autoact-form-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]); ?>

<?php $form = ActiveForm::begin([
    'id' => 'autoact-form',
    'action' => ['autoact'],
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'options' => [
        'class' => 'autoact-form',
        'data-pjax' => '',
    ],
]); ?>

    <?= $form->field($model, 'rule', ['options' => ['class' => '']])->label(false)->radioList(Autoact::$ruleLabels, [
        'encode' => false,
        'item' => function ($index, $label, $name, $checked, $value) use ($form, $model) {
            $comment = isset(Autoact::$ruleComments[$value]) ? ' (' . Autoact::$ruleComments[$value] . ')' : '';
            $input = Html::radio($name, $checked, [
                'value' => $value,
                'class' => 'autoact-rule-radio',
                'label' => Html::tag('span', $label, [
                    'style' => 'font-weight: bold;',
                ]) . $comment,
                'labelOptions' => [
                    'class' => 'autoact-rule-label'
                ],
            ]);

            $input .= $this->render("autoactPartials/_rule{$value}", [
                'form' => $form,
                'model' => $model,
                'value' => $value,
                'checked' => $checked,
            ]);

            return Html::tag('div', $input, [
                'class' => 'form-group',
            ]);
        }
    ]); ?>

    <div class="form-actions">
        <div class="row action-buttons">
            <div class="spinner-button button-bottom-page-lg col-sm-1 col-xs-1">
                <button type="submit" class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs ladda-button"
                        data-style="expand-right" style="width: 130px!important;">
                    <span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>
                </button>
                <button type="submit" class="btn darkblue widthe-100 hidden-lg">
                    <i class="fa fa-floppy-o fa-2x"></i>
                </button>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                <span class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs" data-dismiss="modal">
                    Отменить
                </span>
                <span class="btn darkblue widthe-100 hidden-lg" title="Отменить" data-dismiss="modal">
                    <i class="fa fa-reply fa-2x"></i>
                </span>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
        </div>
    </div>

<?php ActiveForm::end(); ?>

<?php Pjax::end(); ?>