<?php

use common\components\date\DateHelper;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\components\TextHelper;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\status\ActStatus;
use common\models\employee\EmployeeRole;
use common\widgets\Modal;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\components\DocConverter;
use frontend\modules\documents\components\FilterHelper;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\models\ActSearch;
use frontend\modules\documents\widgets\DocumentFileWidget;
use frontend\modules\documents\widgets\StatisticWidget;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use frontend\widgets\RangeButtonWidget;
use frontend\widgets\TableConfigWidget;
use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Company;
use common\components\ImageHelper;
use common\models\document\OrderAct;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel ActSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ioType int */
/* @var $message Message */
/* @var $company Company */

$this->title = $message->get(Message::TITLE_PLURAL);

$company = Yii::$app->user->identity->company;
$period = StatisticPeriod::getSessionName();

$exists = Invoice::find()->joinWith('acts', false, 'INNER JOIN')
    ->byCompany(Yii::$app->user->identity->company->id)
    ->byDeleted()->byIOType($ioType)->exists();

$isFilter = (boolean)($searchModel->byNumber);

if ($exists) {
    if ($isFilter) {
        $emptyMessage = "По вашему запросу, в выбранном периоде «{$period}», ничего не найдено. Измените период, и попробуйте еще раз.";
    } else {
        $emptyMessage = "В выбранном периоде «{$period}», у вас нет актов. Измените период, чтобы увидеть имеющиеся акты.";
    }
} else {
    $emptyMessage = 'Вы еще не создали ни одного акта.';
}

$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, ['ioType' => $ioType]);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
    'ioType' => $ioType,
]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
    'ioType' => $ioType,
]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canSend = $ioType == \frontend\models\Documents::IO_TYPE_OUT &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = $ioType == \frontend\models\Documents::IO_TYPE_OUT;
$canUpdateStatus = Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);

$dropItems = [];
if ($canUpdateStatus){
    if ($ioType == \frontend\models\Documents::IO_TYPE_OUT) {
        $dropItems[] = [
            'label' => 'Передан',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-status', 'type' => $ioType, 'status' => ActStatus::STATUS_SEND])
            ]
        ];
        $dropItems[] = [
            'label' => 'Подписан',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-status', 'type' => $ioType, 'status' => ActStatus::STATUS_RECEIVED])
            ]
        ];
    } else {
        $dropItems[] = [
            'label' => 'Скан',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-original', 'type' => $ioType, 'val' => 0])
            ]
        ];
        $dropItems[] = [
            'label' => 'Оригинал',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-original', 'type' => $ioType, 'val' => 1])
            ]
        ];
    }
}
if ($ioType == $canIndex) {
    $dropItems[] = [
        'label' => 'Скачать в Excel',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'get-xls-link generate-xls-many_actions',
        ],
    ];
}
$showAutoActModal = Yii::$app->request->get('autoActModal');

$existsInvoicesToAdd = Invoice::find()
    ->where(['can_add_act' => true])
    ->byCompany(Yii::$app->user->identity->company->id)
    ->byDeleted()->byIOType($ioType)->exists();

$this->registerJs('
    $(document).on("change", ".autoact-rule-radio", function() {
        var rule = $(".autoact-rule-radio:checked").val();
        $("#autoact-form .collapse.in").collapse("hide");
        if (rule) {
            $("#autoact-form .rule" + rule + " input").prop({checked: false, disabled: false}).uniform("refresh");
            $("#autoact-form .rule" + rule + " .has-error").removeClass("has-error");
            $("#autoact-form .rule" + rule + " .help-block").html("");
            $("#autoact-form .rule" + rule).collapse("show");
        }
    });
    $(document).on("change", ".autoact-status-radio", function() {
        var $checked = $(".autoact-status-radio:checked");
        console.log($checked.val());
        $("#autoact-form .autoact-status-content.collapse.in").collapse("hide");
        if ($checked.val() == "1") {
            $checked.data("target")
            $($checked.data("target") + " input").prop({checked: false, disabled: false}).uniform("refresh");
            $($checked.data("target") + " .has-error").removeClass("has-error");
            $($checked.data("target") + " .help-block").html("");
            $($checked.data("target")).collapse("show");
        }
    });
    $(document).on("hidden.bs.collapse", ".autoact-rule-box", function () {
        $("#autoact-form .autoact-rule-box.collapse:not(.in) input").prop({checked: false, disabled: true}).uniform("refresh");
    });
    $(document).on("hidden.bs.collapse", ".autoact-status-content", function () {
        $("#autoact-form .autoact-status-content.collapse:not(.in) input").prop({checked: false, disabled: true}).uniform("refresh");
    });
    $(document).on("hidden.bs.modal", "#autoact-form-modal", function () {
        $.pjax.reload("#autoact-form-pjax");
    });
');
?>

<?php if (!Documents::getSearchQuery(Documents::DOCUMENT_INVOICE, $ioType)->byDeleted()->exists()): ?>

    <?php
    Modal::begin([
        'clientOptions' => ['show' => true],
        'closeButton' => false,
    ]);
    echo '<div class="text-center row"><p class="empty-invoice-modal">Перед тем как выставить Акт, нужно создать Счет.</p>';
    echo Html::a('СОЗДАТЬ СЧЁТ', ['invoice/create', 'type' => $ioType], ['class' => 'btn yellow']);
    echo '</div>';
    Modal::end();

    ?>
    <div class="alert-success alert fade in">
        <button id="contractor_alert_close" type="button" class="close"
                data-dismiss="alert" aria-hidden="true">×
        </button>
        Перед тем как выставить Акт,
        нужно <?= Html::a('создать счёт', ['invoice/create', 'type' => $ioType]) ?>
        .
    </div>
<?php endif; ?>

    <div class="portlet box">
        <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
            'ioType' => $ioType,
        ])): ?>
            <div class="btn-group pull-right title-buttons">
                <?php if ($company->createInvoiceAllowed($ioType)) : ?>
                    <?php if ($existsInvoicesToAdd): ?>
                        <?= Html::button('<i class="fa fa-plus"></i> ДОБАВИТЬ', [
                            'class' => 'btn yellow text-white add-document',
                        ]) ?>
                    <?php else: ?>
                        <?= Html::a('<i class="fa fa-plus"></i> ДОБАВИТЬ', [
                            '/documents/invoice/create',
                            'type' => $ioType,
                            'document' => Documents::SLUG_ACT,
                        ], [
                            'class' => 'btn yellow text-white',
                        ]) ?>
                    <?php endif; ?>
                <?php else : ?>
                    <?= Html::button('<i class="fa fa-plus"></i> ДОБАВИТЬ', [
                        'class' => 'btn yellow text-white action-is-limited',
                    ]) ?>
                <?php endif ?>
            </div>
        <?php endif; ?>
        <h3 class="page-title"><?= Html::encode($this->title) ?></h3>
        <?php if ($ioType == Documents::IO_TYPE_IN) : ?>
            <div class="row" style="margin: 25px 0 -25px 0;">
                <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_no_right']); ?>
            </div>
        <?php endif; ?>
    </div>
<?php if ($ioType == Documents::IO_TYPE_OUT) : ?>
    <div class="row statistic-block" style="margin-bottom: 15px;">
        <div class="col-sm-3">
            <div class="statistic-cell" style="background-color: #dfba49;">
                <i class="fa fa-comments"></i>

                <div class="stat-value">
                    <?php $sum = OrderAct::find()
                        ->andWhere(['act_id' => $searchModel->getQuery()
                            ->select('`act`.`id`')
                            ->andWhere(['status_out_id' => ActStatus::STATUS_PRINTED])
                            ->column()])
                        ->leftJoin('order', 'order.id = order_act.order_id')
                        ->sum('`order`.`amount_sales_with_vat` * `order_act`.`quantity`'); ?>
                    <?= TextHelper::invoiceMoneyFormat($sum, 2); ?>
                    <i class="fa fa-rub"></i>
                </div>
                <div class="stat-label">Распечатано</div>
                <div class="stat-footer" style="background-color: #d4af46;">
                    Количество актов:
                    <?= $searchModel->getQuery()->andWhere(['status_out_id' => ActStatus::STATUS_PRINTED])->count() ?>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="statistic-cell" style="background-color: #f3565d;">
                <i class="fa fa-comments"></i>

                <div class="stat-value">
                    <?php $sum = OrderAct::find()
                        ->andWhere(['act_id' => $searchModel->getQuery()
                            ->select('`act`.`id`')
                            ->andWhere(['status_out_id' => ActStatus::STATUS_SEND])
                            ->column()])
                        ->leftJoin('order', 'order.id = order_act.order_id')
                        ->sum('`order`.`amount_sales_with_vat` * `order_act`.`quantity`') ?>
                    <?= TextHelper::invoiceMoneyFormat($sum, 2); ?>
                    <i class="fa fa-rub"></i>
                </div>
                <div class="stat-label">Передано</div>
                <div class="stat-footer" style="background-color: #e25157;">
                    Количество актов:
                    <?= $searchModel->getQuery()->andWhere(['status_out_id' => ActStatus::STATUS_SEND])->count() ?>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="statistic-cell" style="background-color: #45b6af;">
                <i class="fa fa-comments"></i>

                <div class="stat-value">
                    <?php $sum = OrderAct::find()
                        ->andWhere(['act_id' => $searchModel->getQuery()
                            ->select('`act`.`id`')
                            ->andWhere(['status_out_id' => ActStatus::STATUS_RECEIVED])
                            ->column()])
                        ->leftJoin('order', 'order.id = order_act.order_id')
                        ->sum('`order`.`amount_sales_with_vat` * `order_act`.`quantity`') ?>
                    <?= TextHelper::invoiceMoneyFormat($sum, 2); ?>
                    <i class="fa fa-rub"></i>
                </div>
                <div class="stat-label">Подписано</div>
                <div class="stat-footer" style="background-color: #43a9a2;">
                    Количество актов:
                    <?= $searchModel->getQuery()->andWhere(['status_out_id' => ActStatus::STATUS_RECEIVED])->count() ?>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <?= RangeButtonWidget::widget(['cssClass' => 'btn default btn-calendar auto-width p-t-7 p-b-7 portlet mrg_bottom btn default doc-gray-button btn_row btn_select_days',]); ?>
            <?php if (Yii::$app->user->can(UserRole::ROLE_CHIEF)) : ?>
                <?php Modal::begin([
                    'id' => 'autoact-form-modal',
                    'header' => '<h1>Правила выставления АвтоАктов</h1>',
                    'toggleButton' => [
                        'tag' => 'div',
                        'label' => 'Настройка АвтоАктов',
                        'class' => 'btn default doc-gray-button btn_row pull-right auto-width p-t-7 p-b-7 portlet yellow',
                        'style' => 'clear: right; margin: 0;',
                    ],
                ]); ?>

                <?= $this->render('autoact') ?>

                <?php Modal::end(); ?>
            <?php endif ?>
        </div>
    </div>
<?php endif ?>

    <div class="row" id="widgets" style="margin-top:-20px; margin-bottom:5px;">
        <div class="col-sm-12">
            <div class="table-icons" style="margin-top: <?= ($ioType == Documents::IO_TYPE_OUT) ? '-10px' : '-15px' ?>; margin-bottom: 5px;">
                <?= Html::a('<i class="fa fa-file-excel-o"></i>', array_merge(['get-xls'], Yii::$app->request->queryParams), [
                    'class' => 'get-xls-link pull-right',
                    'title' => 'Скачать в Excel',
                ]); ?>
            </div>
        </div>
    </div>
<?php ActiveForm::begin([
    'method' => 'POST',
    'action' => ['generate-xls'],
    'id' => 'generate-xls-form',
]); ?>
<?php ActiveForm::end(); ?>

    <div class="portlet box darkblue">
        <div class="portlet-title">
            <div class="caption">
                Список актов
            </div>
            <div
                    class="search-tools tools tools_button <?= $ioType == Documents::IO_TYPE_OUT ? 'col-md-5 col-sm-5' : 'col-md-6 col-sm-6'; ?>">
                <div class="form-body">
                    <?php $form = \yii\widgets\ActiveForm::begin([
                        'method' => 'GET',
                        'fieldConfig' => [
                            'template' => "{input}\n{error}",
                            'options' => [
                                'class' => '',
                            ],
                        ],
                    ]); ?>
                    <div class="search_cont">
                        <div class="wimax_input ">
                            <?= $form->field($searchModel, 'byNumber')->textInput([
                                'placeholder' => 'Номер акта, название или ИНН контрагента',
                            ]); ?>
                        </div>
                        <div class="wimax_button">
                            <?= Html::submitButton('НАЙТИ', [
                                'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                            ]) ?>
                        </div>
                    </div>
                    <?php $form->end(); ?>
                </div>
            </div>
            <div
                    class="actions joint-operations <?= $ioType == Documents::IO_TYPE_OUT ? 'col-md-5 col-sm-5' : 'col-md-3 col-sm-3'; ?>"
                    style="display:none;">

                <?php if ($dropItems): ?>
                    <div class="dropdown">
                        <?= Html::a('Еще  <span class="caret"></span>', null, [
                            'class' => 'btn btn-default btn-sm dropdown-toggle',
                            'id' => 'dropdownMenu1',
                            'data-toggle' => 'dropdown',
                            'aria-expanded' => true,
                            'style' => 'height: 28px;',
                        ]); ?>
                        <?= Dropdown::widget([
                            'items' => $dropItems,
                            'options' => [
                                'style' => 'right: -30px;left: auto;top: 28px;',
                                'aria-labelledby' => 'dropdownMenu1',
                            ],
                        ]); ?>
                    </div>
                <?php endif; ?>

                <?php if ($canDelete) : ?>
                    <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                        'class' => 'btn btn-default btn-sm',
                        'data-toggle' => 'modal',
                    ]); ?>

                    <div id="many-delete" class="confirm-modal fade modal" role="dialog"
                         tabindex="-1" aria-hidden="true"
                         style="display: none; margin-top: -51.5px;">
                        <div class="modal-dialog ">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="form-body">
                                        <div class="row">Вы уверены, что хотите удалить
                                            выбранные акты?
                                        </div>
                                    </div>
                                    <div class="form-actions row">
                                        <div class="col-xs-6">
                                            <?= Html::a('ДА', null, [
                                                'class' => 'btn darkblue pull-right modal-many-delete',
                                                'data-url' => Url::to(['many-delete', 'type' => $ioType]),
                                            ]); ?>
                                        </div>
                                        <div class="col-xs-6">
                                            <button type="button" class="btn darkblue"
                                                    data-dismiss="modal">НЕТ
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif ?>

                <?php if ($canSend) : ?>
                    <?= Html::a('<i class="glyphicon glyphicon-envelope"></i> Отправить', null, [
                        'class' => 'btn btn-default btn-sm document-many-send',
                        'data-url' => Url::to(['many-send', 'type' => $ioType]),
                    ]); ?>
                    <div class="modal fade confirm-modal" id="many-send-error"
                         tabindex="-1"
                         role="modal"
                         aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close"
                                            data-dismiss="modal"
                                            aria-hidden="true"></button>
                                    <h3 style="text-align: center; margin: 0">Ошибка
                                        при отправке актов</h3>
                                </div>
                                <div class="modal-body">
                                    <div class="form-body">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif ?>

                <?php if ($canPrint) : ?>
                    <?= Html::a('<i class="fa fa-print"></i> Печать', [
                        'many-document-print',
                        'actionType' => 'pdf',
                        'type' => $ioType,
                        'multiple' => ''
                    ], [
                        'class' => 'btn btn-default btn-sm multiple-print',
                        'target' => '_blank',
                    ]); ?>
                <?php endif ?>
            </div>
        </div>
        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <div class="dataTables_wrapper dataTables_extended_wrapper">
                    <div>
                        <?= common\components\grid\GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'emptyText' => $emptyMessage,
                            'tableOptions' => [
                                'class' => 'table table-striped table-bordered table-hover dataTable customers_table overfl_text_hid fix-thead',
                                'id' => 'datatable_ajax',
                                'aria-describedby' => 'datatable_ajax_info',
                                'role' => 'grid',
                            ],

                            'headerRowOptions' => [
                                'class' => 'heading',
                            ],

                            'options' => [
                                'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                            ],

                            'pager' => [
                                'options' => [
                                    'class' => 'pagination pull-right',
                                ],
                            ],
                            'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),

                            'columns' => [
                                [
                                    'header' => Html::checkbox('', false, [
                                        'class' => 'joint-operation-main-checkbox',
                                    ]),
                                    'headerOptions' => [
                                        'class' => 'text-center',
                                        'width' => '5%',
                                    ],
                                    'contentOptions' => [
                                        'class' => 'text-center',
                                    ],
                                    'format' => 'raw',
                                    'value' => function (Act $model) {
                                        return Html::checkbox('Act[' . $model->id . '][checked]', false, [
                                            'class' => 'joint-operation-checkbox',
                                            'data-contractor' => $model->invoice->contractor_id,
                                        ]);

                                    },
                                ],
                                [
                                    'attribute' => 'document_date',
                                    'label' => 'Дата акта',
                                    'headerOptions' => [
                                        'class' => 'sorting',
                                        'width' => '10%',
                                    ],
                                    'contentOptions' => [
                                        'class' => 'link-view',
                                    ],
                                    'format' => 'raw',
                                    'value' => function (Act $data) {
                                        return DateHelper::format($data->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                                    },
                                ],

                                [
                                    'attribute' => 'document_number',
                                    'label' => '№ акта',
                                    'headerOptions' => [
                                        'class' => 'sorting',
                                    ],
                                    'contentOptions' => [
                                        'class' => 'document_number link-view',
                                    ],
                                    'format' => 'raw',
                                    'value' => function (Act $data) {
                                        return (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $data,])
                                            ? Html::a($data->fullNumber, ['/documents/act/view', 'type' => $data->type, 'id' => $data->id])
                                            : $data->fullNumber);
                                    },
                                ],
                                [
                                    'label' => 'Скан',
                                    'headerOptions' => [
                                        'class' => 'sorting',
                                        'width' => '70px',
                                    ],
                                    'contentOptions' => [
                                        'class' => 'link-view',
                                    ],
                                    'attribute' => 'has_file',
                                    'format' => 'raw',
                                    'value' => function ($model) {
                                        return DocumentFileWidget::widget(['model' => $model]);
                                    },
                                ],
                                [
                                    'attribute' => 'order_sum',
                                    'label' => 'Сумма',
                                    'headerOptions' => [
                                        'class' => 'sorting',
                                    ],
                                    'contentOptions' => [
                                        'class' => 'link-view',
                                    ],
                                    'format' => 'raw',
                                    'value' => function ($data) {
                                        $price = TextHelper::invoiceMoneyFormat($data->totalAmountWithNds, 2);
                                        return '<span class="price" data-price="' . str_replace(" ", "", $price) . '">' . $price . '</span>';
                                    },
                                ],

                                [
                                    'attribute' => 'contractor_id',
                                    'label' => 'Контрагент',
                                    'class' => DropDownSearchDataColumn::className(),
                                    'headerOptions' => [
                                        'width' => '30%',
                                        'class' => 'dropdown-filter',
                                    ],
                                    'contentOptions' => [
                                        'class' => 'contractor-cell',
                                    ],
                                    'filter' => FilterHelper::getContractorList($searchModel->type, Act::tableName(), true, false, false),
                                    'format' => 'raw',
                                    'value' => function (Act $data) {
                                        return '<span data-id="'.$data->invoice->contractor_id.'" title="' . htmlspecialchars($data->invoice->contractor_name_short) . '">' .
                                            $data->invoice->contractor_name_short .
                                            '</span>';
                                    },
                                ],
                                [
                                    'attribute' => 'status_out_id',
                                    'label' => 'Статус',
                                    'class' => DropDownDataColumn::className(),
                                    'headerOptions' => [
                                        'class' => 'dropdown-filter',
                                    ],
                                    'filter' => $searchModel->getStatusArray($searchModel->type),
                                    'format' => 'raw',
                                    'value' => function (Act $data) {
                                        return $data->statusOut->name;
                                    },
                                    'visible' => $ioType == Documents::IO_TYPE_OUT,
                                ],

                                [
                                    'attribute' => 'invoice_document_number',
                                    'label' => 'Счёт №',
                                    'headerOptions' => [
                                        'class' => 'sorting',
                                    ],
                                    'format' => 'raw',
                                    'value' => function (Act $data) {
                                        $invoice = $data->invoice;
                                        if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $invoice,])) {
                                            return Html::a($invoice->fullNumber, [
                                                    '/documents/invoice/view',
                                                    'type' => $data->type,
                                                    'id' => $data->invoice->id,
                                                ]) . DocumentFileWidget::widget(['model' => $invoice, 'cssClass' => 'pull-right']);
                                        } else {
                                            return $data->invoice->fullNumber;
                                        }
                                    },
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->registerJs('
    $(".invoice-file-link-preview").tooltipster({
        theme: ["tooltipster-kub"],
        contentCloning: true,
        trigger: "hover",
        side: "left",
    });
'); ?>
<?php if ($showAutoActModal): ?>
    <?php $this->registerJs('
    $(document).ready(function () {
        $("#autoact-form-modal").modal();
    });
'); ?>
<?php endif; ?>
<?php if ($company->show_popup_expose_other_documents && $ioType == Documents::IO_TYPE_OUT): ?>

    <?php $company->updateAttributes(['show_popup_expose_other_documents' => false]); ?>

    <?php Modal::begin([
        'header' => '<h2 class="header-name" style="text-transform: uppercase;">
            Подготовить акт, накладную<br> счет-фактуру
            </h2>',
        'footer' => $this->render('//layouts/modal/_partial_footer', [
            'type' => Company::AFTER_REGISTRATION_EXPOSE_OTHER_DOCUMENTS,
        ]),
        'id' => 'modal-loader-items'
    ]); ?>
    <div class="col-xs-12" style="padding: 0" id="modal-loader">
        <?= $this->render('//layouts/modal/_template_submodal', [
            'type' => 2,
            'description' => 'Для этого нужно выставить счет<br> и нажать нужную кнопку:',
            'video' => 'https://www.youtube.com/embed/jXesTUBlxl4',
            'link' => Url::to(['/documents/invoice/first-create', 'type' => Documents::IO_TYPE_OUT]),
            'image' => ImageHelper::getThumb('img/modal_registration/block-2.jpg', [680, 340], [
                'class' => 'hide-video',
                'style' => 'max-width: 100%',
            ]),
            'previousModal' => null,
            'nextModal' => 3,
        ]); ?>
    </div>
    <style>
        #modal-loader-items .modal-body {
            padding: 0;
        }
    </style>
    <?php Modal::end(); ?>
    <?php $this->registerJs('
        $(document).ready(function () {
            $(".modal#modal-loader-items").modal();
        });
    '); ?>
<?php endif; ?>
<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a('<i class="fa fa-print"></i> Печать', [
            'many-document-print',
            'actionType' => 'pdf',
            'type' => $ioType,
            'multiple' => ''
        ], [
            'class' => 'btn btn-sm darkblue text-white multiple-print',
            'target' => '_blank',
        ]) : null,
        $canSend ? Html::a('<i class="glyphicon glyphicon-envelope"></i> Отправить', null, [
            'class' => 'btn btn-sm darkblue text-white document-many-send',
            'data-url' => Url::to(['many-send', 'type' => $ioType]),
        ]) : null,
        $canDelete ? Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
            'class' => 'btn btn-sm darkblue text-white',
            'data-toggle' => 'modal',
        ]) : null,
        $dropItems ? Html::tag('div', Html::a('Еще  <b class="caret"></b>', '#', [
                'id' => 'dropdownMenu2',
                'class' => 'btn btn-sm darkblue text-white dropdown-toggle',
                'data-toggle' => 'dropdown',
            ]) . Dropdown::widget([
                'items' => $dropItems,
                'options' => [
                    'style' => 'left: auto; right: 0;'
                ],
            ]), ['class' => 'dropup']) : null,
    ],
]); ?>

<?= $this->render('@frontend/modules/documents/views/invoice/modal/_invoices_modal', [
    'company' => $company,
    'ioType'  => $ioType,
    'documentType' => Documents::SLUG_ACT,
    'documentTypeName' => 'Акт'
]) ?>

<?php if ($canSend): ?>
    <?= $this->render('@frontend/modules/documents/views/invoice/view/_many_send_message', [
        'models' => [],
        'useContractor' => false,
        'showSendPopup' => false,
        'typeDocument' => Documents::DOCUMENT_ACT
    ]); ?>
<?php endif; ?>
