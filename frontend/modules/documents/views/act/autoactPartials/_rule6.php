<?php

use common\models\document\Autoact;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model common\models\document\Autoact */
/* @var $value integer */
/* @var $checked boolean */

$cssClass = "autoact-rule-box rule{$value} collapse" . ($checked ? ' in' : '');
?>

<div class="<?= $cssClass ?>">
    <div class="autoact-rule-content">
        <?= $form->field($model, "[{$value}]status", [
            'options' => [
                'class' => '',
            ]
        ])->label(false)->radioList(Autoact::$statusArray, [
            'encode' => false,
            'item' => function ($index, $label, $name, $check, $val) use ($form, $model, $value, $checked) {
                $input = Html::radio($name, $check, [
                    'value' => $val,
                    'class' => 'autoact-status-radio',
                    'data-target' => "#status-content-{$value}",
                    'disabled' => !$checked,
                    'label' => Html::tag('span', $label, [
                        'clas' => 'label',
                    ]),
                    'labelOptions' => [
                        'class' => 'autoact-rule-label'
                    ],
                ]);

                if ($val == Autoact::STATUS_PAY) {
                    $input .= Html::beginTag('div', [
                        'id' => "status-content-{$value}",
                        'class' => 'autoact-status-content collapse' . ($check ? ' in' : ''),
                    ]);
                    $input .= $form->field($model, "[{$value}]no_pay_partial", ['options' => ['class' => '']])->checkbox([
                        'disabled' => !$checked,
                    ]);
                    $input .= $form->field($model, "[{$value}]no_pay_order", ['options' => ['class' => '']])->checkbox([
                        'disabled' => !$checked,
                    ]);
                    $input .= $form->field($model, "[{$value}]no_pay_emoney", ['options' => ['class' => '']])->checkbox([
                        'disabled' => !$checked,
                    ]);
                    $input .= Html::endTag('div');
                }

                return Html::tag('div', $input);
            }
        ]); ?>
        <?= $form->field($model, "[{$value}]send_auto", ['options' => ['class' => '']])->checkbox([
            'disabled' => !$checked,
        ])->hint('(Создался Акт и отправилось письмо клиенту с Актом)'); ?>
    </div>
</div>