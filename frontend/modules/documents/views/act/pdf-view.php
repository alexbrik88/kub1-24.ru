<?php

/* @var $this yii\web\View */
/* @var $model common\models\document\Act */

$this->title = $model->printTitle;
?>

<div class="page-content-in p-center pad-pdf-p">
    <?= $this->render('template', [
        'model' => $model,
        'addStamp' => isset($addStamp) ? $addStamp : false,
    ]); ?>
</div>

<?php /*if (isset($multiple) && (int)array_pop($multiple) !== $model->id): ?>
    <pagebreak/>
<?php endif;*/ ?>
