<?php

use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use frontend\widgets\ConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\file\widgets\FileUpload;

/* @var $this yii\web\View */
/* @var $model common\models\Agreement */

$this->context->layout = 'agreements-update';
$this->title = 'Просмотр договора';

$canUpdate = Yii::$app->user->can(permissions\Contractor::CREATE, [
    'model' => $model,
]);
$canUpdateStatus = Yii::$app->user->can(permissions\Contractor::CREATE, [
    'model' => $model,
]);

$showSendPopup = Yii::$app->session->remove('show_send_popup');
?>

    <div class="page-content-in" style="padding-bottom: 30px;">

        <div>
            <?php if ($backUrl !== null) {
                echo \yii\helpers\Html::a('Назад к списку', $backUrl, [
                    'class' => 'back-to-customers',
                ]);
            } ?>
        </div>

        <?php if ($model->essence) : ?>
        <div class="col-xs-12 pad0">
            <div class="col-xs-12 col-lg-7 pad0">
                <?= $this->render('partial/_viewText', [
                    'model' => $model,
                    'canUpdate' => $canUpdate,
                    'canUpdateStatus' => $canUpdateStatus
                ]); ?>
            </div>
            <div class="col-xs-12 col-lg-5 pad0 pull-right" style="padding-bottom: 5px !important; max-width: 480px;">
                <div class="col-xs-12" style="padding-right:0px !important;">
                    <?= $this->render('partial/_viewStatus', [
                        'model' => $model,
                        'canUpdate' => $canUpdate,
                        'canUpdateStatus' => $canUpdateStatus
                    ]); ?>
                </div>
            </div>
        </div>
        <div id="buttons-bar-fixed">
            <?= $this->render('partial/_viewActionsButtons', [
                'model' => $model,
                'canUpdate' => $canUpdate
            ]); ?>
        </div>
        <?php else: ?>
        <div class="agreement-form" data-header="Договор">
            <h3><b><?= Html::encode($model->document_name) ?></b></h3>
            <h4><b>№<?= Html::encode($model->document_number) ?>
                    от <?= \php_rutils\RUtils::dt()->ruStrFTime([
                        'format' => 'd F Y г.',
                        'monthInflected' => true,
                        'date' => $model->document_date,
                    ]); ?></b></h4>
                    <?php $file = ($model->files) ? $model->files[0] : null;
                    echo $file ? Html::a('<span class="icon icon-paper-clip m-r-10"></span>', [
                        '/contractor/agreement-file-get',
                        'id' => $model->id,
                        'file-id' => $file->id,
                    ], [
                        'class' => 'file-link',
                        'target' => '_blank',
                        'download' => '',
                        'data-pjax' => 0,
                    ]) : ''; ?>
        </div>
        <?php endif; ?>
    </div>

<?php if ($model->essence && $canUpdateStatus): ?>
    <?= $this->render('/invoice/view/_send_message', [
        'model' => $model,
        'showSendPopup' => $showSendPopup,
        'useContractor' => true
    ]); ?>
<?php endif; ?>