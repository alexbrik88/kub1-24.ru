<?php

use common\models\Agreement;
use common\models\AgreementEssence;
use common\components\date\DateHelper;
use common\widgets\Modal;
use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\AgreementType;
use common\components\image\EasyThumbnailImage;

$signatureLink = EasyThumbnailImage::thumbnailSrc($model->getImage('chiefSignatureImage'), 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);

$printLink = EasyThumbnailImage::thumbnailSrc($model->getImage('printImage'), 150, 150, EasyThumbnailImage::THUMBNAIL_INSET);

$logoLink = EasyThumbnailImage::thumbnailSrc($model->company->getImage('logoImage'), 150, 90, EasyThumbnailImage::THUMBNAIL_INSET);

$images = [
    'logo' => $logoLink,
    'print' => $printLink,
    'signature' => $signatureLink
];

/* @var $this yii\web\View */
/* @var $model common\models\Agreement */

?>

<style>
    .preview-agreement p {margin-bottom: 0}
    .pre-view-table {margin-bottom:150px}
</style>

<div class="page-content-in m-size-div container-first-account-table no_min_h pad0"
     style="border:1px solid #4276a4; margin-top:3px;">

    <div class="col-xs-12 pad5 pre-view-table">
        <div class="col-xs-12 pad3" style="height: <?= $model->document_type_id == AgreementType::TYPE_AGREEMENT ? '80px' : '40px' ?>;">
            <div class="col-xs-12 pad0 font-bold" style="height: inherit">
                <div class="actions" style="margin-top: -8px;">
                    <?= \frontend\modules\documents\widgets\DocumentLogWidget::widget([
                        'model' => $model,
                    ]); ?>

                    <?php if (!$model->is_completed && Yii::$app->user->can(frontend\rbac\permissions\Contractor::CREATE, [
                            'model' => $model,
                        ])
                    ): ?>
                        <a href="<?= Url::to(['update-contract', 'id' => $model->id]); ?>"
                           title="Редактировать" class="btn darkblue btn-sm"
                           style="padding-bottom: 4px !important;">
                            <i class="icon-pencil"></i>
                        </a>
                    <?php endif; ?>
                </div>

                <div style="padding-top: 12px">

                    <?php if ($model->hasLogo()) : ?>
                        <img style="max-height: 70px;max-width: 170px; position:absolute; top:25px; <?= $model->getLogoPosition() == 'RIGHT' ? 'right:0' : 'left:0'; ?>" src="<?= $logoLink ?>" alt="">
                    <?php endif; ?>

                    <?php if ($model->document_name) : ?>
                        <p style="font-size: 17px; width:100%; text-align: center">
                            <?php if ($model->document_type_id == \common\models\AgreementType::TYPE_AGREEMENT && empty($model->document_name)): ?>
                                Договор <br/>
                            <?php else : ?>
                                <br/>
                            <?php endif; ?>
                            <?= $model->document_name; ?> № <?= $model->fullNumber ?>
                        </p>
                    <?php endif; ?>
                </div>

            </div>

        </div>

        <div class="col-xs-12 pad3 preview-agreement" style="margin-top: 40px;">
            <div class="col-xs-12 pad0" style="font-size: 12px; text-align:justify">
                <?= $model->replacePatterns($model->essence->document_header, $images) ?>
                <br/>
                <?= $model->replacePatterns($model->essence->document_body, $images) ?>
                <br/>
                <div class="row">
                    <div class="col-xs-6" style="text-align: left">
                        <?= $model->replacePatterns(($model->type == Documents::IO_TYPE_IN) ? $model->essence->document_requisites_executer : $model->essence->document_requisites_customer, $images) ?>
                    </div>
                    <div class="col-xs-6" style="text-align: left">
                        <?= $model->replacePatterns(($model->type == Documents::IO_TYPE_IN) ? $model->essence->document_requisites_customer : $model->essence->document_requisites_executer, $images) ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

