<?php

use common\models\Agreement;
use frontend\rbac\permissions;
use yii\bootstrap\Html;
use frontend\widgets\ConfirmModalWidget;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $model \common\models\Agreement;
 */

?>
<div class="row action-buttons margin-no-icon">
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if ($canUpdate): ?>
            <button class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs send-message-panel-trigger">
                Отправить
            </button>
            <button class="btn darkblue widthe-100 hidden-lg send-message-panel-trigger" title="Отправить">
                <span class="ico-Send-smart-pls fs"></span>
            </button>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php
        echo Html::a('Печать', ['document-print', 'actionType' => 'print', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPrintTitle(),], [
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
            'target' => '_blank',
        ]);
        echo Html::a('<i class="fa fa-print fa-2x"></i>', ['document-print', 'actionType' => 'print', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPrintTitle(),], [
            'class' => 'btn darkblue widthe-100 hidden-lg',
            'target' => '_blank',
            'title' => 'Печать',
        ]);
        ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?= Html::a('Скачать', [
            'document-print',
            'actionType' => 'pdf',
            'id' => $model->id,
            'type' => $model->type,
            'filename' => $model->getPdfFileName(),
        ], [
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
            'encode' => false,
            'target' => '_blank',
        ]); ?>
        <?= Html::a('<i class="glyphicon glyphicon-download" style="font-size: 17px;"></i>', [
            'document-print',
            'actionType' => 'pdf',
            'id' => $model->id,
            'type' => $model->type,
            'filename' => $model->getPdfFileName(),
        ], [
            'class' => 'btn darkblue widthe-100 hidden-lg',
            'encode' => false,
            'target' => '_blank',
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (Yii::$app->user->can(permissions\Contractor::CREATE)): ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => 'Копировать',
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ],
                'confirmUrl' => Url::to(['copy', 'id' => $model->id, 'type' => $model->type,]),
                'message' => 'Вы уверены, что хотите скопировать этот договор?',
            ]); ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => '<i class="fa fa-files-o fa-2x"></i>',
                    'title' => 'Копировать',
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                ],
                'confirmUrl' => Url::to(['copy', 'id' => $model->id, 'type' => $model->type,]),
                'message' => 'Вы уверены, что хотите скопировать этот договор?',
            ]); ?>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php
            if (Yii::$app->user->can(permissions\Contractor::CREATE)): ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => (!$model->is_completed) ?
                            'Окончен' :
                            'В работе',
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    ],
                    'confirmUrl' => Url::to([
                            'update-status',
                            'id' => $model->id,
                            'type' => $model->type,
                            'status' => (!$model->is_completed) ?
                                'ended' :
                                'active'
                    ]),
                    'message' => (!$model->is_completed) ?
                        'Вы уверены, что хотите закрыть договор?' :
                        'Вы уверены, что хотите восстановить договор?'
                ]); ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => (!$model->is_completed) ?
                            '<i class="fa fa-archive fa-2x"></i>' :
                            '<i class="fa fa-archive fa-2x"></i>',
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                    ],
                    'confirmUrl' => Url::to([
                        'update-status',
                        'id' => $model->id,
                        'type' => $model->type,
                        'status' => (!$model->is_completed) ?
                            'ended' :
                            'active'
                    ]),
                    'message' => (!$model->is_completed) ?
                        'Вы уверены, что хотите закрыть договор?' :
                        'Вы уверены, что хотите восстановить договор?'
                ]); ?>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (Yii::$app->user->can(permissions\Contractor::CREATE)): ?>
            <?= Html::button('Удалить', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                'data-toggle' => 'modal',
                'href' => '#delete-confirm',
            ]); ?>
            <?= Html::button('<i class="fa fa-trash-o fa-2x"></i>', [
                'class' => 'btn darkblue widthe-100 hidden-lg',
                'data-toggle' => 'modal',
                'href' => '#delete-confirm',
                'title' => 'Удалить',
            ]); ?>
        <?php endif; ?>
    </div>
</div>

<?php if (Yii::$app->user->can(permissions\Contractor::CREATE)) {
    echo ConfirmModalWidget::widget([
        'options' => [
            'id' => 'delete-confirm',
        ],
        'toggleButton' => false,
        'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id, 'type' => $model->type]),
        'confirmParams' => [],
        'message' => "Вы уверены, что хотите удалить этот договор?",
    ]);
}; ?>
