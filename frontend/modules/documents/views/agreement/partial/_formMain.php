<?php

use common\models\Agreement;
use common\models\AgreementTemplate;
use kartik\select2\Select2;
use yii\helpers\Html;
use common\components\date\DateHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\document\Autoinvoice;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\status\InvoiceStatus;
use common\models\NdsOsno;
use common\models\product\Product;
use common\models\TaxationType;
use frontend\models\Documents;
use frontend\modules\documents\components\FilterHelper;
use frontend\modules\documents\components\Message;
use frontend\widgets\ConfirmModalWidget;
use frontend\widgets\ExpenditureDropdownWidget;
use yii\helpers\ArrayHelper;
use dosamigos\tinymce\TinyMce;
use common\models\AgreementType;

/* @var $form yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */
/* @var $model \common\models\Agreement */

$textAreaStyle = 'width: 100%; font-size:12px; text-align:justify; white-space: pre-line;';

$companyRs = [];
$rsData = [];
$allTemplates = [];
$contractorArray = [];

if (Yii::$app->user->identity !== null
    && method_exists(Yii::$app->user->identity, 'hasProperty')
    && Yii::$app->user->identity->hasProperty('company')) {

    $company = Yii::$app->user->identity->company;
    $company_id = $company->id;
} else {
    $company = $company_id = null;
}

$isNewRecord = $model->isNewRecord;

if ($company_id) {

    $companyRs = Company::findOne($company_id)->getCheckingAccountants()
        ->select(['bank_name', 'rs', 'id'])
        ->andWhere(['not', ['type' => CheckingAccountant::TYPE_CLOSED]])
        ->orderBy(['type' => SORT_ASC])
        ->indexBy('rs')
        ->asArray()->all();


    foreach ($companyRs as $rs) {
        $rsData[$rs['rs']] = $rs['bank_name'];
    }
    //$rsData["add-modal-rs"] = '[ + Добавить расчетный счет ]';

    $contractorType = Yii::$app->request->get('type', $model->type);
    $contractorDropDownConfig = [
        'class' => 'form-control contractor-select',
        //'disabled' => $fixedContractor,
    ];
    $contractorArray = Contractor::getAllContractorList($contractorType, false);
}

// get templates by type
$agreementType = Yii::$app->request->get('type', null);
$searchTemplatesCondition = ['company_id' => $company_id];
if ($agreementType)
    $searchTemplatesCondition['type'] = (int)$agreementType;

$templates = AgreementTemplate::find()->where($searchTemplatesCondition)->asArray()->all();


foreach ($templates as $t) {
    $allTemplates[$t['id']] = 'Шаблон №' . $t['document_number'] . ' от ' .
        DateHelper::format($t['document_date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
}
?>

<style>
    .agreement-template-form #account-number,
    .agreement-template-form #under-date {
        display: inline-block;
        width: 130px;
        margin-left: 5px;
        margin-right: 5px;
        height: 100%;
        vertical-align: middle;
    }
    .agreement-template-form .control-label {
        font-weight: bold;
    }
    .agreement-template-form .form-column-max-width {
        max-width: 590px;
    }
    .agreement-template-form .label-max-width {
        max-width: 175px;
        padding-right: 0;
    }
    .agreement-template-form .label-horizontal label {padding-bottom:5px;}

    <?php if ($model->document_type_id == AgreementType::TYPE_AGREEMENT) : ?>
        .agreement-template-form .show_agreement {display: inherit}
        .agreement-template-form .show_not_agreement {display: none}
    <?php else : ?>
        .agreement-template-form .show_agreement {display: none}
        .agreement-template-form .show_not_agreement {display: inherit}
    <?php endif; ?>

</style>

<?= $form->errorSummary($model); ?>
<?= $form->errorSummary($model->essence); ?>

<div class="row agreement-template-form" style="">
    <div class="col-sm-8">

        <div class="portlet">
            <div class="portlet-title">
                <div class="caption" style="width: 100%">
                    <table cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td valign="middle" style="width:1%; white-space:nowrap;">
                                <span class="">
                                    <?= Html::hiddenInput('', (int)$model->isNewRecord, [
                                        'id' => 'isNewRecord',
                                    ]) ?>

                                    <strong><?= $model->documentType->name ?> №</strong>

                                    <?= Html::activeTextInput($model, 'document_number', [
                                        'id' => 'account-number',
                                        'data-required' => 1,
                                        'class' => 'form-control',
                                        'value' => ($model->isNewRecord) ? '' : $model->document_number
                                    ]); ?>

                                    <br class="box-br">
                                </span>

                                <?= Html::activeTextInput($model, 'document_additional_number', [
                                    'maxlength' => true,
                                    'id' => 'account-number',
                                    'data-required' => 1,
                                    'class' => 'form-control',
                                    'placeholder' => 'доп. номер',
                                    'style' => 'display:inline',
                                ]); ?>

                                <span class="box-margin-top-t">от</span>
                                <div class="input-icon box-input-icon-top" style="display: inline-block; vertical-align: top; margin-left: 6px">
                                    <i class="fa fa-calendar"></i>
                                    <?= Html::activeTextInput($model, 'document_date_input', [
                                        'id' => 'under-date',
                                        'class' => 'form-control date-picker',
                                        'size' => 16,
                                        'data-date' => '12-02-2012',
                                        'data-date-viewmode' => 'years',
                                        'value' => ($model->isNewRecord) ?
                                            date(DateHelper::FORMAT_USER_DATE) :
                                            DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                                    ]); ?>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="portlet-body">
                <div class="form-column-max-width">

                    <div class="form-group row">
                        <div class="col-sm-4 label-max-width">
                            <label class="control-label">
                                Мой расч/счет в<span class="required" aria-required="true">*</span>:
                            </label>
                        </div>
                        <div class="col-sm-8">
                            <?= $form->field($model, 'company_rs', [
                                'template' => "{input}",
                                'options' => [
                                    'class' => '',
                                ],
                            ])->label(false)->dropDownList($rsData, [
                                'options' => [
                                    'class' => 'form-control',
                                ],
                                'disabled' => true
                            ]); ?>
                        </div>
                    </div>
                    <div class="form-group row">

                        <div class="col-sm-4" style="max-width: 175px; padding-right: 0;">
                            <label class="control-label">
                                <?=($model->type == Documents::IO_TYPE_IN) ? 'Поставщик' : 'Покупатель' ?>
                                <span class="required" aria-required="true">*</span>:
                            </label>
                        </div>
                        <div id="add-first-contractor" class="col-sm-8 details"
                             style="display: <?= $contractorArray ? 'none' : 'block'; ?>">
                            <?= Html::button('Заполнить реквизиты по ИНН', ['class' => 'btn yellow first-invoice-contractor']); ?>
                            <span class="tooltip2 ico-question valign-middle"
                                  data-tooltip-content="#tooltip_contractor"></span>
                            <span class="ico-video valign-middle" data-toggle="modal"
                                  data-target="#video_modal_contractor"></span>
                        </div>

                        <div id="select-existing-contractor" class="col-sm-8"
                             style="max-width: 500px; display: <?= $contractorArray ? 'block' : 'none'; ?>;">

                            <?php echo $form->field($model, 'contractor_id', ['template' => "{input}", 'options' => [
                                'class' => '',
                            ]])->label(false)->dropDownList($contractorArray, [
                                'options' => [
                                    'class' => 'form-control',
                                ],
                                'disabled' => true
                            ]);
                            ?>

                        </div>

                        <span class="hidden contractor-invoice-debt" style="position: absolute;width: 100%;top: -2px;">
                            Есть неоплаченные счета: <span class="count"></span><br>
                            на сумму <span class="amount"></span> Р
                    </span>

                    </div>
                    <div class="form-group row">
                        <div class="col-sm-4 label-max-width">
                            <label class="control-label">
                                Шаблон<span class="required" aria-required="true">*</span>:
                            </label>
                        </div>
                        <div class="col-sm-8">
                            <?= $form->field($model, 'agreement_template_id', [
                                'template' => "{input}",
                                'options' => [
                                    'class' => '',
                                ],
                            ])->label(false)->dropDownList($allTemplates, [
                                'options' => [
                                    'class' => 'form-control',
                                ],
                                'disabled' => true
                            ]);
                             ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-4 label-max-width">
                            <label class="control-label">
                                Дата окончания<span class="required" aria-required="true">*</span>:
                            </label>
                        </div>
                        <div class="col-sm-8">
                            <div class="input-icon">
                                <i class="fa fa-calendar"></i>
                                <?= Html::activeTextInput($model, 'payment_limit_date', [
                                    'class' => 'form-control date-picker',
                                    'style' => 'width:130px',
                                    'data-date-viewmode' => 'years',
                                    'data-delay' => $model->payment_delay ?: 10,
                                    'value' =>
                                        ($model->isNewRecord) ?
                                            date(DateHelper::FORMAT_USER_DATE, time()+10*3600*24) :
                                            DateHelper::format($model->payment_limit_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if (!$model->isNewRecord) : ?>
                    <div class="form-group row">
                        <div class="col-sm-12 label-horizontal">
                            <label class="control-label">
                                Шапка<span class="required" aria-required="true">*</span>:
                            </label>
                        </div>
                        <div class="col-sm-12">
                            <?= $form->field($model->essence, 'document_header', [
                                'template' => '{input}',
                                'options' => [
                                    'tag' => false,
                                ]
                            ])->widget(TinyMce::className(), [
                                'options' => ['rows' => 8],

                                'language' => 'ru',
                                'clientOptions' => [
                                    'plugins' => [
                                        "table paste code"

                                    ],
                                    'paste_webkit_styles' => "font-style text-align",
                                    'menubar' => false,
                                    'toolbar' => "code | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table | removeformat",
                                    'content_style' => "p {margin-top:0;margin-bottom:0}"
                                ]
                            ]) ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 label-horizontal">
                            <label class="control-label">
                                Текст<span class="required" aria-required="true">*</span>:
                            </label>
                        </div>
                        <div class="col-sm-12 tinymce-textarea">

                            <?= $form->field($model->essence, 'document_body', [
                                'template' => '{input}',
                                'options' => [
                                    'tag' => false,
                                ]
                            ])->widget(TinyMce::className(), [
                                'options' => ['rows' => 16],
                                'language' => 'ru',
                                'clientOptions' => [
                                    'plugins' => [
                                        "table paste code"

                                    ],
                                    'menubar' => false,
                                    'paste_webkit_styles' => "font-style text-align",
                                    'toolbar' => "code | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table | removeformat",
                                    'content_style' => "p {margin-top:0;margin-bottom:0}"
                                ]
                            ]) ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 label-horizontal">
                            <label class="control-label">
                                Ваши реквизиты<span class="required show_agreement" aria-required="true">*</span>:
                            </label>
                        </div>
                        <div class="col-sm-12">
                            <?= $form->field($model->essence, 'document_requisites_customer', [
                                'template' => '{input}',
                                'options' => [
                                    'tag' => false,
                                ]
                            ])->widget(TinyMce::className(), [
                                'options' => ['rows' => 8],
                                'language' => 'ru',
                                'clientOptions' => [
                                    'plugins' => [
                                        "table paste code"

                                    ],
                                    'menubar' => false,
                                    'paste_webkit_styles' => "font-style text-align",
                                    'toolbar' => "code | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table | removeformat",
                                    'content_style' => "p {margin-top:0;margin-bottom:0}"
                                ]
                            ]) ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 label-horizontal">
                            <label class="control-label">
                                Реквизиты контрагента<span class="required show_agreement" aria-required="true">*</span>:
                            </label>
                        </div>
                        <div class="col-sm-12">
                            <?= $form->field($model->essence, 'document_requisites_executer', [
                                'template' => '{input}',
                                'options' => [
                                    'tag' => false,
                                ]
                            ])->widget(TinyMce::className(), [
                                'options' => ['rows' => 8],
                                'language' => 'ru',
                                'clientOptions' => [
                                    'plugins' => [
                                        "table paste code"

                                    ],
                                    'menubar' => false,
                                    'paste_webkit_styles' => "font-style text-align",
                                    'toolbar' => "code | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table | removeformat",
                                    'content_style' => "p {margin-top:0;margin-bottom:0}"
                                ]
                            ]) ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
