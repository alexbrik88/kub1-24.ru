<?php

use common\components\date\DateHelper;
use common\models\Agreement;
use common\widgets\Modal;
use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\AgreementType;

/* @var $this yii\web\View */
/* @var $model common\models\Agreement */

$changeStatusDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->created_at,
]);

$status = $model->status;
$styleClass = $status->getStyleClass();
$iconClass  = $status->getIcon();
?>

        <div class="control-panel col-xs-12 pad0">
        <div class="col-xs-12 col-sm-3 pad3">
            <div class="btn full_w marg <?= $styleClass; ?>"
                 style="padding-left:0px; padding-right:0px;text-align: center;"
                 title="Дата изменения статуса">

                <?= date(DateHelper::FORMAT_USER_DATE, $model->status_updated_at ?: $model->created_at) ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-9 pad0">
            <div class="col-xs-12 pad3">
                <div class="btn full_w marg <?= $styleClass; ?>" title="Статус">
                    <i class="pull-left icon <?= $iconClass; ?>"></i>
                    <?= (!$model->is_completed) ? 'В работе' : 'Окончен' ?>
                </div>
            </div>
        </div>
    </div>

        <style>
            .main_inf_no-bord td {
                border: none !important;
            }
        </style>

        <!-- _main_info -->
        <?php if ($model->document_type_id == AgreementType::TYPE_AGREEMENT) : ?>
        <div class="control-panel col-xs-12 pad0">
            <div class="col-xs-12 col-sm-9 col-sm-push-3 pad3">
                <div style="margin-top: 4px">
                    <?= Html::a('<i class="pull-left icon fa fa-plus-circle"></i> СЧЁТ',
                        ($model->is_completed) ? '#' : Url::to(['/documents/invoice/create',
                        'type' => $model->type,
                        'contractorId' => $model->contractor_id,
                        'fromAgreement' => $model->id]), [
                        'class' => 'btn yellow full_w' ,
                        'style' => 'padding-left:4px;' . ($model->is_completed ? 'background:#a2a2a2;' : ''),
                        'disabled' => ($model->is_completed) ? true : false
                    ]); ?>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <!--col-md-5 block-left-->
        <div class="col-xs-12 col-lg-9 pull-right pad3" style="max-width: 720px;">
            <div class="portlet">
                <div class="customer-info bord-dark" style="margin:10px 0">

                    <div class="portlet-body no_mrg_bottom main_inf_no-bord">
                        <table class="table no_mrg_bottom">
                            <tbody>
                            <tr>
                                <td>
                                    <span class="customer-characteristic"><?= $model->documentType->name ?>:</span>
                                    № <?= $model->getFullNumber() ?> от <?= DateHelper::format(
                                        $model->document_date,
                                        DateHelper::FORMAT_USER_DATE,
                                        DateHelper::FORMAT_DATE); ?>
                                </td>
                            </tr>
                            <?php if ($model->document_name): ?>
                            <tr>
                                <td>
                                    <span class="customer-characteristic">Название документа:</span>
                                    <?= $model->document_name ?>
                                </td>
                            </tr>
                            <?php endif; ?>
                            <tr>
                                <td>
                                    <span class="customer-characteristic">Тип контрагента:</span>
                                    <?= ($model->type == Documents::IO_TYPE_OUT) ? 'Покупатель' : 'Поставщик' ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="customer-characteristic">Контрагент:</span>
                                    <?= $model->contractor ? Html::a($model->contractor->getNameWithType(), [
                                        '/contractor/view',
                                        'type' => $model->contractor->type,
                                        'id' => $model->contractor->id,
                                    ]) : $model->contractor->getNameWithType(); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="customer-characteristic">Дата окончания:</span>
                                    <?= DateHelper::format(
                                        $model->payment_limit_date,
                                        DateHelper::FORMAT_USER_DATE,
                                        DateHelper::FORMAT_DATE); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="customer-characteristic">Ответственный:</span>
                                    <?= \common\models\employee\Employee::findOne(Yii::$app->user->id)->getShortFio() ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?= \common\models\file\widgets\FileUpload::widget([
                                        'uploadUrl' => Url::to(['agreement-file-upload', 'id' => $model->id,]),
                                        'deleteUrl' => Url::to(['agreement-file-delete', 'id' => $model->id,]),
                                        'listUrl' => Url::to(['agreement-file-list', 'id' => $model->id,]),
                                    ]); ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="">
                    <div style="margin: 15px 0 0;">
                        <span style="font-weight: bold;">Комментарий</span>
                        <?= Html::tag('span', '', [
                            'id' => 'comment_internal_update',
                            'class' => 'glyphicon glyphicon-pencil',
                            'style' => 'cursor: pointer;',
                        ]); ?>
                    </div>
                    <div id="comment_internal_view" class="">
                        <?= Html::encode($model->comment_internal) ?>
                    </div>
                    <?php if ($canUpdate) : ?>
                        <?= Html::beginTag('div', [
                            'id' => 'comment_internal_form',
                            'class' => 'hidden',
                            'style' => 'position: relative;',
                            'data-url' => Url::to(['comment-internal', 'id' => $model->id]),
                        ]) ?>
                        <?= Html::tag('i', '', [
                            'id' => 'comment_internal_save',
                            'class' => 'fa fa-floppy-o',
                            'style' => 'position: absolute; top: -22px; right: 0px; cursor: pointer; font-size: 20px;',
                        ]); ?>
                        <?= Html::textarea('comment_internal', $model->comment_internal, [
                            'id' => 'comment_internal_input',
                            'rows' => 3,
                            'maxlength' => true,
                            'style' => 'width: 100%; padding-right: 35px; border: 1px solid #ddd;',
                        ]); ?>
                        <?= Html::endTag('div') ?>
                    <?php endif ?>
                </div>

            </div>
        </div>

<?php
if ($canUpdate) {
    $this->registerJs('
        $(document).on("click", "#comment_internal_update", function () {
            $("#comment_internal_view").toggleClass("hidden");
            $("#comment_internal_form").toggleClass("hidden");
        });
        $(document).on("click", "#comment_internal_save", function () {
            $.post($("#comment_internal_form").data("url"), $("#comment_internal_input").serialize(), function (data) {
                $("#comment_internal_view").text(data.value);
                $("#comment_internal_form").addClass("hidden");
                $("#comment_internal_view").removeClass("hidden");
            })
        });

    ');
}
?>