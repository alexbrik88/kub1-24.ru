<?php

use common\models\AgreementType;
use common\models\AgreementTemplate;
use common\models\AgreementTitleTemplate;
use frontend\widgets\ContractorDropdown;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use kartik\select2\Select2;

use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\Contractor;

/* @var $this yii\web\View */
/* @var $model common\models\Agreement */
/* @var $form yii\widgets\ActiveForm */
/* @var $fixedContractor boolean */
/* @var $fixedType boolean */
/* @var $newRecord boolean */
/* @var $disableAllFields boolean */

$rsData = [];
$allTemplates = [];
$sellerArray = $customerArray = [];
$sellerTemplatesArray = $customerTemplatesArray = [];
$company = $company_id = null;
$contractorTypesArray = [];
$titleTemplateData = AgreementTitleTemplate::find()->select('name')->indexBy('id')->column();

if (Yii::$app->user->identity !== null
    && method_exists(Yii::$app->user->identity, 'hasProperty')
    && Yii::$app->user->identity->hasProperty('company')) {

    $company = Yii::$app->user->identity->company;
    $company_id = $company->id;
}


if ($company_id) {

    //$currentRs = $model->company_rs ? [$model->company_rs => $model->company_bank_name] : [];
    $companyRs = Company::findOne($company_id)->getCheckingAccountants()
        ->select(['bank_name', 'rs', 'id'])
        ->andWhere(['not', ['type' => CheckingAccountant::TYPE_CLOSED]])
        ->orderBy(['type' => SORT_ASC])
        ->indexBy('rs')
        ->asArray()->all();

    $rsData = [];
    foreach ($companyRs as $rs) {
        $rsData[$rs['rs']] = $rs['bank_name'];
    }

    $contractorDropDownConfig = [
        'class' => 'form-control contractor-select',
    ];

    $sellerTemplatesArray = ['' => '---', 'add-agreement-template' => '[ + Добавить шаблон ]'] + AgreementTemplate::getAllTemplatesList($company_id, Contractor::TYPE_SELLER, $model->document_type_id);
    $customerTemplatesArray = ['' => '---', 'add-agreement-template' => '[ + Добавить шаблон ]'] + AgreementTemplate::getAllTemplatesList($company_id, Contractor::TYPE_CUSTOMER, $model->document_type_id);
    $hasTemplates = (bool)AgreementTemplate::findOne(['company_id' => $company_id]);
    $contractorTypesArray = [
        Contractor::TYPE_CUSTOMER => 'С покупателем',
        Contractor::TYPE_SELLER => 'С Поставщиком'
    ];
}

if ($model->type == Contractor::TYPE_SELLER) {
    $contractorType = Contractor::TYPE_SELLER;
    $staticData = ['add-modal-contractor' => '[ + Добавить поставщика ]'];
} else {
    $contractorType = Contractor::TYPE_CUSTOMER;
    $staticData = ['add-modal-contractor' => '[ + Добавить покупателя ]'];
}

if ($fixedType) {
    if ($model->type == Contractor::TYPE_SELLER)
        $contractorTypesArray = [Contractor::TYPE_SELLER => 'С Поставщиком'];
    else
        $contractorTypesArray = [Contractor::TYPE_CUSTOMER => 'С покупателем'];
}

if (!$newRecord) {
    $model->create_agreement_from = $model->agreement_template_id ? 1 : 0;
}

$header = $newRecord ? 'Добавить договор' : 'Редактировать договор';
$firstCreate = stristr(\Yii::$app->request->referrer, 'invoice/first-create') !== false;
$model->document_name = $model->document_name ? $model->document_name : ($firstCreate ? 'Договор' : null);

// Create/Update from Invoice
$hideTemplateFields = Yii::$app->request->get('hide_template_fields');

Pjax::begin([
    'id' => 'agreement-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]) ?>

<style type="text/css">
    .agreement-form .control-label {
        font-weight: bold;
    }

    .agreement-form .form-control {
        width: 100%;
    }

    #agreement-create_agreement_from > .radio {
        width: 170px
    }

    #agreement-type > .radio {
        width: 140px
    }

    .select2-container--krajee .select2-results__option[id$="add-agreement-template"] {
        color: #fff;
        background-color: #ffb848;
    }

    .select2-container--krajee .select2-results__option[id$="add-agreement-template"]:hover {
        background-color: #337ab7;
    }

</style>
<div class="agreement-form" data-header="<?= $header ?>">

    <?php $form = ActiveForm::begin([
        'id' => 'contractor-agreement-form',
        'enableClientValidation' => false,
        'action' => ['update',
            'id' => $model->id,
            'contractor_id' => ($fixedContractor ? $model->contractor_id : null),
            'type' => ($fixedType ? $model->type : null),
            'from_template' => ($fixedTemplate ? $model->agreement_template_id : null),
            'old_record' => !$newRecord,
            'returnTo' => $returnTo
        ],
        'layout' => 'horizontal',
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'label' => 'col-sm-4',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-8',
            ],
        ],
        'options' => [
            'data-pjax' => true,
            'data-max-files' => 5,
        ],
    ]); ?>

    <?php // echo $form->errorSummary($model); ?>

    <?php if ($newRecord) : ?>
        <div class="form-group" style="display: <?= $firstCreate ? 'none' : 'block' ?>;">
            <label class="control-label col-sm-4" for="agreement-create_agreement_from">Создать по шаблону</label>
            <div class="col-sm-8" style="padding-top:8px">
                <?= Html::activeCheckbox($model, 'create_agreement_from', [
                    'label' => false,
                    'value' => \common\models\Agreement::CREATE_AGREEMENT_FROM_TEMPLATE,
                    'onchange' => "changeAgreementFrom(this)",
                    'disabled' => $fixedTemplate ? true : false
                ]) ?>
                <?php if (!$hasTemplates) : ?>
                    <span>У вас нет ни одного шаблона. <?= Html::a('Создать?', '/documents/agreement-template/create')?></span>
                <?php endif; ?>
            </div>
        </div>
    <?php endif; ?>

<?php if ($hideTemplateFields): ?>
    <div class="hidden">
<?php endif; ?>

    <?= $form->field($model, 'type', [
        'options' => [
            'class' => 'form-group',
            'style' => 'display: ' . ($firstCreate ? 'none;' : 'block;'),
        ]])
        ->label('Тип')
        ->radioList($contractorTypesArray, [
            'onchange' => "changeDocumentType(this)",
            'value' => ($model->type) ?: Contractor::TYPE_CUSTOMER
        ]);
    ?>

    <div class="form-group" style="display: <?= $firstCreate ? 'none' : 'block' ?>;">
        <label class="control-label col-sm-4"
               id="contractor_label"><?= ($model->type == Contractor::TYPE_SELLER) ? 'Поставщик' : 'Покупатель' ?></label>
        <div class="col-sm-8">
            <?= $form->field($model, 'contractor_id', [
                'template' => "{input}{hint}{error}",
                'options' => [],
            ])->widget(ContractorDropdown::class, [
                'company' => $company,
                'contractorType' => $contractorType,
                'staticData' => $staticData,
                'options' => [
                    'readonly' => ($fixedContractor) ? true : false,
                    'placeholder' => '',
                    'data-createurl' => Url::to(['add-modal-contractor', 'type' => $model->type]),
                ],
            ])->label(false); ?>
        </div>
    </div>

    <div class="form-group <?= ($fixedTemplate ? '' : 'hidden') ?>">
        <label class="control-label col-sm-4">Мой расчетный счет</label>
        <div class="col-sm-8">
            <?php echo $form->field($model, 'company_rs', ['template' => "{input}{hint}{error}", 'options' => []])
                ->widget(Select2::classname(), ['data' => $rsData])
                ->label(false); ?>
        </div>
    </div>

    <?php
    Pjax::begin([
        'id' => 'templates-pjax',
        'enablePushState' => false,
        'linkSelector' => false,
        'options' => [
            'data-url' => Url::toRoute(['create', 'type' => $model->type])
        ]
    ]) ?>
    <div class="form-group <?= ($fixedTemplate || !empty($templateDocumentTypeId) ? '' : 'hidden') ?>">
        <label class="control-label col-sm-4">Шаблон</label>
        <div class="col-sm-8">
            <?php echo $form->field($model, 'agreement_template_id', ['template' => "{input}{hint}{error}", 'options' => []])
                ->widget(Select2::classname(), [
                    'data' => $model->type == Contractor::TYPE_SELLER ? $sellerTemplatesArray : $customerTemplatesArray
                ])
                ->label(false); ?>
        </div>
        <?= Html::dropDownList('seller_templates_id', 0, $sellerTemplatesArray, ['id' => 'seller_templates_id', 'style' => 'display:none']); ?>
        <?= Html::dropDownList('customer_templates_id', 0, $customerTemplatesArray, ['id' => 'customer_templates_id', 'style' => 'display:none']); ?>
    </div>
    <?php Pjax::end() ?>

<?php if ($hideTemplateFields): ?>
    </div>
<?php endif; ?>


    <div class="form-group">
        <label class="control-label col-sm-4">Тип документа</label>
        <div class="col-sm-8">
            <?php echo $form->field($model, 'document_type_id', ['template' => "{input}", 'options' => []])
                ->widget(Select2::classname(), [
                    'data' => AgreementType::find()->select(['name', 'id'])->orderBy(['sort' => SORT_ASC])->indexBy('id')->column()
                ])->label(false); ?>
        </div>
    </div>

    <?= $form->field($model, 'document_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'document_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'document_date_input', [
        'template' => Yii::$app->params['formDatePickerTemplate'],
    ])->textInput(['class' => 'form-control date-picker'])->label('Дата документа') ?>

    <?= $form->field($model, 'payment_limit_date_input', [
        'template' => Yii::$app->params['formDatePickerTemplate'],
    ])->textInput(['class' => 'form-control date-picker'])->label('Дата окончания') ?>

    <?= $form->field($model, 'title_template_id')->widget(Select2::classname(), [
        'data' => $titleTemplateData,
        'options' => [
            'placeholder' => '',
        ],
    ]); ?>

    <div id="agreement_upload_file" style="position:relative">
        <?= \common\models\file\widgets\FileUpload::widget([
            'uploadUrl' => Url::to(['agreement-file-upload', 'id' => $model->id,]),
            'deleteUrl' => Url::to(['agreement-file-delete', 'id' => $model->id,]),
            'listUrl' => Url::to(['agreement-file-list', 'id' => $model->id,]),
        ]); ?>
        <div style="position:absolute;bottom: 5px;left: 45px;">Загрузить скан/файл документа</div>
    </div>


    <div class="action-buttons row">
        <div class="spinner-button col-md-4 col-xs-1">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                'data-style' => 'expand-right',
                'style' => 'width:130px!important;',
            ]); ?>
            <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', ['class' => 'btn darkblue widthe-100 hidden-lg',
                'title' => 'Сохранить',]) ?>
        </div>
        <div class="col-xs-7"></div>
        <div class="col-md-8 col-xs-1" style="float: right;">
            <button type="button"
                    class="btn darkblue btn-cancel darkblue gray-darkblue widthe-100 hidden-md hidden-sm hidden-xs float-right"
                    data-dismiss="modal" style="width: 130px !important;">Отмена
            </button>
            <button type="button" class="btn darkblue gray-darkblue btn-cancel darkblue widthe-100 hidden-lg" title="Отмена"><i
                        class="fa fa-reply fa-2x"></i></button>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    <?php
    if (is_file($file = \Yii::getAlias('@common/assets/web/scripts/upload.js'))) {
        echo $this->renderFile($file);
    }
    ?>
</script>

<?php $this->registerJs(<<<JS

templatesReload = function(doc_type_id) {
    var pjax = $("#templates-pjax");
    $.pjax({
        url: pjax.attr("data-url"),
        data: {templateDocumentTypeId: doc_type_id},
        container: "#templates-pjax",
        push: false,
        timeout: 10000,
    });
}

agreementToggleFields = function(type_agreement_from) {
    var type_file = 0;
    var type_template = 1;
    var enable_fields;
    var disable_fields;

    if (type_agreement_from != type_template) {
        enable_fields = []
        disable_fields = [
            'company_rs',
            'agreement_template_id',
            'payment_limit_date_input'
        ];
    } else {
        disable_fields = []
        enable_fields = [
            'company_rs',
            'agreement_template_id',
            'payment_limit_date_input'
        ];
    }

    $.each(disable_fields, function(i, the_id) {
        $('#agreement-'+the_id).prop("disabled", true).parents('.form-group').addClass('hidden');
    });
    $.each(enable_fields, function(i, the_id) {
        $('#agreement-'+the_id).prop("disabled", false).parents('.form-group').removeClass('hidden');
    });
}

agreementResetFields = function(type_agreement_from) {
    var type_file = 0;
    var type_template = 1;
    var reset_fields;

    if (type_agreement_from != type_template) {
        reset_fields = [
            'document_number',
            'document_name',
            'company_rs',
            'agreement_template_id',
            'payment_limit_date_input'
        ];
    } else {
        reset_fields = [];
    }

    $.each(reset_fields, function(i, the_id) {
        $('#agreement-'+the_id).val('');
        $('#agreement-'+the_id).siblings('.help-block').html('');
        $('#agreement-'+the_id).parents('.form-group').removeClass('has-error');
    });

}

changeAgreementTemplate = function(agreement_template_id) {
    if (agreement_template_id > 0) {
        $.post("/documents/agreement/get-next-number/?agreement_template_id=" + agreement_template_id, {}, function(data) {
            if (typeof data.document_number !== "undefined" && $('#agreement-document_number').val() == '')
                $('#agreement-document_number').val(data.document_number);
            if (typeof data.document_name !== "undefined")
                $('#agreement-document_name').val(data.document_name);
        });
    }
}

changeAgreementFrom = function(el) {
    var type_file = 0;
    var type_template = 1;
    var type_agreement_from = $(el).prop('checked') ? type_template : type_file;
    var doc_type_id = $("#agreement-document_type_id").val();

    if (type_agreement_from && doc_type_id > 0) {
        templatesReload(doc_type_id);
    }
    agreementToggleFields(type_agreement_from);
    agreementResetFields(type_agreement_from);
    console.log(type_agreement_from);
}

changeDocumentType = function(el) {
    var type_seller = 1;
    var type_customer = 2;
    var type = $(el).find(':checked').val();
    if (type == type_seller) {
        $('#agreement-contractor_id').html($('#seller_id').html());
        $('#agreement-contractor_id').addClass('seller');
        $('#agreement-agreement_template_id').html($('#seller_templates_id').html());
        $('#contractor_label').html('Поставщик');
    } else {
        $('#agreement-contractor_id').html($('#customer_id').html());
        $('#agreement-contractor_id').addClass('customer');
        $('#agreement-agreement_template_id').html($('#customer_templates_id').html());
        $('#contractor_label').html('Покупатель');
    }
    console.log(type);
}

$(document).on("change", "#agreement-agreement_template_id", function (e) {
    var value = $(this).val() || $(this).text();
    if (value == "add-agreement-template") {
        e.preventDefault();
        window.location.href = "/documents/agreement-template";
    } else {
        changeAgreementTemplate(value);
    }
});

$("#agreement-document_type_id").on("change", function() {
    if ($("#agreement-create_agreement_from").prop("checked")) {
        if ($(this).val() > 0) {
            templatesReload($(this).val());
            $("#agreement-document_name").val('');
        }
    }
});

JS
); ?>

<?php $this->registerJs(' $(document).ready(function(){
    agreementToggleFields(' . ($model->create_agreement_from) . '); });
    // $("#agreement-document_type_id").trigger("change").val("' . $model->document_type_id . '");
'); ?>

<?php if ($fixedTemplate && $newRecord) {
    $this->registerJs('
    $(document).ready(function(){
        changeAgreementTemplate($("#agreement-agreement_template_id").val());
    });');
}
?>

<?php Pjax::end() ?>

