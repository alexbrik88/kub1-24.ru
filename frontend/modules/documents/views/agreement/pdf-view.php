<?php


use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\Contractor;
use yii\helpers\Html;
use common\models\document\OrderDocument;
use common\models\company\CompanyType;
use common\models\currency\Currency;

/* @var $this yii\web\View */
/* @var $model \common\models\Agreement */
/* @var $multiple [] */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->document_date,
]);

$this->title = $model->getPrintTitle();
$this->context->layoutWrapperCssClass = 'out-invoice out-document';
//$logoLink = EasyThumbnailImage::thumbnailSrc($model->company->getImage('logoImage'), 150, 90, EasyThumbnailImage::THUMBNAIL_INSET);

?>
    <style type="text/css">
        <?php if (is_file($file = \Yii::getAlias('@frontend/web/css/print/common.css'))) {
            echo $this->renderFile($file);
        }
        if (is_file($file = \Yii::getAlias('@frontend/web/css/print/documents-invoice.css'))) {
            echo $this->renderFile($file);
        } ?>
        .auto_tpl {
            font-style: italic;
            font-weight: normal;
        }
        .print-title {text-align: center}
        table, table td {vertical-align: top; text-align: justify}
        table td p, p {margin:0}
        /* Override styles */
        body {font-size:9pt}
        .pad-pdf-p {padding:0!important; padding-left:0.75cm;}
        .p-center {width:710px !important;}
        <?php if (isset($actionType) && $actionType == 'print') : ?>
            .agreement-image {
                visibility: hidden;
            }
        <?php endif ?>
    </style>

    <style media="print">
        @page {
            size: auto;
            margin: 1cm auto;
        }
    </style>

    <div class="page-content-in p-center pad-pdf-p" style="box-sizing: content-box;">
        <table class="no-b m-t" style="width: 100%; margin-top: 0;">

            <?php if ($model->document_name): ?>
            <tr>

                <td style="width: 150px; text-align: left">
                    <?php if ($model->hasLogo() && $model->getLogoPosition() == 'LEFT') : ?>

                    <?php endif; ?>
                </td>

                <td class="font-bold print-title"
                    style="padding-left: 0;padding-bottom: 12px; text-align: center">
                    <br/>
                    <?= $model->document_name; ?> № <?= $model->fullNumber ?>
                </td>

                <td style="width: 150px; text-align: right">
                    <?php if ($model->hasLogo() && $model->getLogoPosition() == 'RIGHT') : ?>

                    <?php endif; ?>
                </td>

            </tr>
            <?php endif; ?>
        </table>

        <?= $model->essence->document_header ?>

        <?= $model->essence->document_body ?>

        <table  class="no-b m-t-m" style="width: 100%;">
            <tr>
                <td style="width:50%; text-align: left;">
                    <?= $model->type == \frontend\models\Documents::IO_TYPE_IN ?
                        $model->essence->document_requisites_executer :
                        $model->essence->document_requisites_customer ?>
                </td>
                <td style="width:50%; text-align: left;">
                    <?= $model->type == \frontend\models\Documents::IO_TYPE_IN ?
                        $model->essence->document_requisites_customer :
                        $model->essence->document_requisites_executer ?>
                </td>
            </tr>
        </table>

    </div>

<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id): ?>
    <pagebreak/>
<?php endif; ?>