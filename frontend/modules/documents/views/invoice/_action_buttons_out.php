<?php

use common\models\document\status\InvoiceStatus;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use frontend\widgets\ConfirmModalWidget;
use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use frontend\models\Documents;

/** @var $this View */
/** @var \common\models\document\Invoice $model */
/* @var $useContractor boolean */
/* @var $showSendPopup integer */

$contractorId = $useContractor ? $model->contractor_id : null;

$canUpdate = Yii::$app->user->can(permissions\document\Document::UPDATE_STATUS, [
    'model' => $model,
]);
?>

<div class="row action-buttons">
    <div class="button-bottom-page-lg col-sm-1 col-xs-1" title="Отправить по e-mail">
        <?php if (!$model->is_deleted && $model->invoiceStatus->sendAllowed()): ?>
            <span id="tooltip-send-button" <?= isset($_COOKIE["tooltip_send_{$model->company_id}"]) && !$showSendPopup ?
                'class="tooltip2 tooltipstered-main" data-tooltip-content="#tooltip_send"' :
                null; ?> >
                <button class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs send-message-panel-trigger" id="send-btn">
                    Отправить
                </button>
                <button class="btn darkblue widthe-100 hidden-lg send-message-panel-trigger" title="Отправить">
                    <span class="ico-Send-smart-pls fs"></span>
                </button>
            </span>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php $printUrl = ['document-print', 'actionType' => 'print', 'id' => $model->id, 'type' => $model->type,
            'filename' => $model->getPrintTitle(),];
        echo Html::a('Печать', $printUrl, [
            'target' => '_blank',
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
        ]);
        echo Html::a('<i class="fa fa-print fa-2x"></i>', $printUrl, [
            'target' => '_blank',
            'title' => 'Печать',
            'class' => 'btn darkblue widthe-100 hidden-lg',
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <style>
            .dropdown-menu-mini {
                width: 100%;
                min-width: 98px;
                border-color: #4276a4 !important;
            }

            .dropdown-menu-mini a {
                padding: 7px 0px;
                text-align: center;
            }
        </style>
        <span <?= isset($_COOKIE["tooltip_pdf_{$model->company_id}"]) ?
            'class="dropup tooltip2 tooltipstered-main" data-tooltip-content="#tooltip_pdf_hide"' : 'class="dropup"'; ?>>
            <?= Html::a('Скачать', '#', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs dropdown-toggle dropdown-linkjs',
                'data-toggle' => 'dropdown',
            ]); ?>
            <?= Html::a('<i class="glyphicon glyphicon-download" style="font-size: 17px;"></i>', '#', [
                'class' => 'btn darkblue widthe-100 hidden-lg dropdown-toggle dropdown-linkjs',
                'data-toggle' => 'dropdown',
            ]); ?>
            <?= Dropdown::widget([
                'options' => [
                    'style' => '',
                    'class' => 'dropdown-menu-mini min-w-190 dropdown-centerjs',
                ],
                'items' => [
                    [
                        'label' => '<span style="display: inline-block;">PDF</span> файл',
                        'encode' => false,
                        'url' => ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPdfFileName()],
                        'linkOptions' => [
                            'target' => '_blank',
                        ]
                    ],
                    [
                        'label' => '<span style="display: inline-block;">Word</span> файл',
                        'encode' => false,
                        'url' => ['docx', 'id' => $model->id, 'type' => $model->type],
                        'linkOptions' => [
                            'target' => '_blank',
                            'class' => 'get-word-link',
                        ]
                    ],
                    [
                        'label' => 'Отправить на мой e-mail',
                        'url' => 'javascript:;',
                        'linkOptions' => [
                            'class' => 'send-message-panel-trigger send-to-me',
                        ]
                    ],
                ],
            ]); ?>
        </span>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (Yii::$app->user->can(permissions\document\Invoice::CREATE, ['ioType' => $model->type])): ?>
            <?php if (\Yii::$app->user->identity->company->createInvoiceAllowed($model->type)) : ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => 'Копировать',
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    ],
                    'confirmUrl' => Url::to(['create',
                        'type' => $model->type, 'clone' => $model->id, 'contractorId' => $contractorId,]),
                    'message' => 'Вы уверены, что хотите скопировать этот счёт?',
                ]);
                ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => '<i class="fa fa-files-o fa-2x"></i>',
                        'title' => 'Копировать',
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                    ],
                    'confirmUrl' => Url::to(['create',
                        'type' => $model->type, 'clone' => $model->id, 'contractorId' => $contractorId,]),
                    'message' => 'Вы уверены, что хотите скопировать этот счёт?',
                ]);
                ?>
            <?php else : ?>
                <span class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs action-is-limited">Копировать</span>
                <span class="btn darkblue widthe-100 hidden-lg action-is-limited" title="Копировать"><i
                            class="fa fa-files-o fa-2x"></i></span>
            <?php endif ?>
        <?php endif; ?>
    </div>

    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if ($model->isFullyPaid) : ?>
            <?php if (Yii::$app->user->can(UserRole::ROLE_CHIEF)): ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => 'Снять оплату',
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    ],
                    'confirmUrl' => Url::to(['unpaid',
                        'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,]),
                    'message' => 'Вы уверены, что нужно снять оплату со счета?',
                ]);
                ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => '<span class="ico-Paid-smart-pls fs"></span>',
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                    ],
                    'confirmUrl' => Url::to(['unpaid',
                        'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,]),
                    'message' => 'Вы уверены, что нужно снять оплату со счета?',
                ]);
                ?>
            <?php endif; ?>
        <?php else : ?>
            <?php if (!$model->is_deleted
                && $model->invoiceStatus->paymentAllowed($model->type)
                && Yii::$app->user->can(permissions\document\Invoice::ADD_CASH_FLOW)
            ): ?>
                <span id="tooltip-pay-button" <?= isset($_COOKIE["tooltip_paid_{$model->company_id}"]) && !$showSendPopup ?
                    'class="tooltip2 tooltipstered-main" data-tooltip-content = "#tooltip_paid"' :
                    null; ?>>
                    <button type="button" class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs"
                            data-toggle="modal" href="#paid">
                        Оплачен
                    </button>
                    <button type="button" class="btn darkblue widthe-100 hidden-lg" data-toggle="modal" title="Оплачен"
                            href="#paid">
                        <span class="ico-Paid-smart-pls fs"></span>
                    </button>
                </span>
            <?php endif; ?>
        <?php endif; ?>
    </div>

    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (!$model->is_deleted && $model->invoiceStatus->rejectAllowed() && $canUpdate): ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => 'Отменён',
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ],
                'confirmUrl' => Url::to(['update-status',
                    'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,]),
                'message' => 'Вы уверены, что хотите отклонить этот счёт?',
                'confirmParams' => [
                    'status' => InvoiceStatus::STATUS_REJECTED,
                ],
            ]);
            ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => '<span class="ico-Denied-smart-pls fs"></span>',
                    'title' => 'Отменён',
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                ],
                'confirmUrl' => Url::to(['update-status',
                    'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,]),
                'message' => 'Вы уверены, что хотите отклонить этот счёт?',
                'confirmParams' => [
                    'status' => InvoiceStatus::STATUS_REJECTED,
                ],
            ]);
            ?>
        <?php endif; ?>
    </div>

    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (!$model->is_deleted && Yii::$app->user->can(permissions\document\Document::DELETE)
            && in_array($model->invoice_status_id, InvoiceStatus::$deleteAllowed)): ?>
            <button type="button"
                    class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs"
                    data-toggle="modal" href="#delete-confirm">Удалить
            </button>
            <button type="button" class="btn darkblue widthe-100 hidden-lg"
                    data-toggle="modal" title="Удалить" href="#delete-confirm">
                <i class="fa fa-trash-o fa-2x"></i></button>
        <?php endif; ?>
    </div>
</div>
<div class="tooltip_templates container-tooltip_templates">
    <div id="tooltip_send" class="box-tooltip-templates">
        <div class="box-my-account">
            2
        </div>
        Нажмите на кнопку, и выберете, кому отправить счет
        <div class="btn-close-tooltip-templates fa fa-times"></div>
    </div>

    <div id="tooltip_pdf" class="box-tooltip-templates">
        <div class="box-my-account">
            3
        </div>
        Нажмите на кнопку, что бы скачать в формате PDF
        <div class="btn-close-tooltip-templates fa fa-times"></div>
    </div>


    <div id="tooltip_paid" class="box-tooltip-templates">
        <div class="box-my-account">
            3
        </div>
        Если счет оплачен, нажмите на кнопку и выберите тип оплаты
        <div class="btn-close-tooltip-templates fa fa-times"></div>
    </div>
</div>
<?php if ($model->type == Documents::IO_TYPE_OUT): ?>
<?= $this->registerJs('
    $(document).ready(function (e) {
        var $left = Math.ceil((+$(".dropdown-centerjs").width() - +$(".dropdown-linkjs:visible").width()) / 2);
        console.log($left);
        $(".dropdown-centerjs").css("left", "-" + $left + "px");
    });
'); ?>
<?php endif; ?>
