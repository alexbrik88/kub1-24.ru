<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 16.04.15
 * Time: 16:42
 */


use frontend\modules\documents\components\Message;
use frontend\widgets\ConfirmModalWidget;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */
/* @var $orders common\models\document\Order[] */
/* @var $message Message */
/* @var $useContractor string */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->document_date,
]);
$this->registerAssetBundle(\frontend\modules\documents\assets\TooltipAsset::className(), $this::POS_END);
$company = Yii::$app->user->identity->company->id;

$this->title = 'Шаблон ' . ($model->is_invoice_contract ? 'автосчёт-договора' : 'автосчёта') . '  №' . $model->fullNumber . ' от ' . $dateFormatted;
$this->context->layoutWrapperCssClass = 'out-invoice out-document';
?>

<?php Pjax::begin([
    'id' => 'view-auto-pjax',
    'enablePushState' => false,
    'timeout' => 5000,
    'linkSelector' => '.auto-status-link',
]) ?>

    <div class="page-content-in">
        <?= \yii\helpers\Html::a('Назад к списку', $useContractor ? [
            '/contractor/view',
            'type' => $model->type,
            'id' => $model->contractor_id,
            'tab' => 'autoinvoice',
        ] : [
            'index-auto',
        ], [
            'class' => 'back-to-customers',
        ]); ?>

        <?php if ($model->is_deleted): ?>
            <h1 class="text-warning">Автосчёт удалён</h1>
        <?php endif; ?>

        <div class="row">
            <?= $this->render('view-auto/_main_info', [
                'model' => $model,
                'dateFormatted' => $dateFormatted,
            ]); ?>
            <?= $this->render('view-auto/_status_block', [
                'model' => $model,
                'useContractor' => $useContractor,
            ]); ?>
        </div>

        <?= $this->render('view/_orders_table', [
            'model' => $model,
            'ioType' => $model->type,
        ]); ?>

        <?= $this->render('view-auto/_action_buttons', [
            'model' => $model,
            'useContractor' => $useContractor,
        ]); ?>
    </div>

<?php Pjax::end(); ?>

<?php
echo ConfirmModalWidget::widget([
    'options' => [
        'id' => 'copy-confirm',
    ],
    'toggleButton' => false,
    'confirmUrl' => Url::to(['copy-auto',
        'id' => $model->id,]),
    'message' => 'Вы уверены, что хотите скопировать этот шаблон?',
]);
echo ConfirmModalWidget::widget([
    'options' => [
        'id' => 'delete-confirm',
    ],
    'toggleButton' => false,
    'confirmUrl' => [
        'delete-auto',
        'id' => $model->id,
    ],
    'confirmParams' => [],
    'message' => "Вы уверены, что хотите удалить: {$this->title}?",
]);
?>

<?php Modal::begin([
    'id' => 'stop-confirm',
    'closeButton' => false,
    'toggleButton' => false,
    'options' => ['class' => 'confirm-modal'],
]); ?>
    <div class="form-body">
        <div class="row">Вы уверены, что хотите остановить шаблон?</div>
    </div>
    <div class="form-actions row">
        <div class="col-xs-6">
            <?= Html::a('ДА', ['view-auto', 'id' => $model->id, 'status' => 'stop'], [
                'class' => 'btn darkblue  pull-right auto-status-link',
            ]); ?>
        </div>
        <div class="col-xs-6">
            <button type="button" class="btn darkblue" data-dismiss="modal">НЕТ</button>
        </div>
    </div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'start-confirm',
    'closeButton' => false,
    'toggleButton' => false,
    'options' => ['class' => 'confirm-modal'],
]); ?>
    <div class="form-body">
        <div class="row">Вы уверены, что хотите запустить шаблон?</div>
    </div>
    <div class="form-actions row">
        <div class="col-xs-6">
            <?= Html::a('ДА', ['view-auto', 'id' => $model->id, 'status' => 'start'], [
                'class' => 'btn darkblue  pull-right auto-status-link',
            ]); ?>
        </div>
        <div class="col-xs-6">
            <button type="button" class="btn darkblue" data-dismiss="modal">НЕТ</button>
        </div>
    </div>
<?php Modal::end(); ?>

<?php $this->registerJs('
    $(document).on("click", ".auto-status-link", function() {
        $("#stop-confirm, #start-confirm").modal("hide");
    });
') ?>