<?php
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
?>
<div class="product-form">
    <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'validateOnSubmit' => true,
        'validateOnBlur' => false,
    ])); ?>

    <div class="form-body form-horizontal form-body_width">

        <?= $this->render('partial/_mainForm', [
            'model' => $model,
            'form' => $form,
        ])?>

        <?= $this->render('partial/_formButtons', [
            'model' => $model,
        ])?>

    </div>

    <div class="form-group">

    </div>

    <?php ActiveForm::end(); ?>
</div>
