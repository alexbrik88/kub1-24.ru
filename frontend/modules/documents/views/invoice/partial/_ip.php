<?php
use common\models\address;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use common\models\company\CompanyType;
use common\models\dictionary\address\AddressDictionary;
use common\components\widgets\AddressTypeahead;
use yii\helpers\Url;
use common\models\address\AddressHouseType;
use yii\helpers\ArrayHelper;
use common\models\company\CheckingAccountant;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $form yii\widgets\ActiveForm */
/* @var $documentType int */
/* @var $checkingAccountant CheckingAccountant */

if (!$model->address_legal_house_type_id) {
    $model->address_legal_house_type_id = \common\models\address\AddressHouseType::TYPE_HOUSE;
}
if (!$model->address_legal_apartment_type_id) {
    $model->address_legal_apartment_type_id = \common\models\address\AddressApartmentType::TYPE_APARTMENT;
}

$inputThinConfig = [
    'options' => [
        'class' => '',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{input}\n{error}\n{hint}",
];

$textInputConfig = [
    'labelOptions' => [
        'class' => 'control-label col-sm-4 col-md-4',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-8 inp_one_line width-inp',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n sel-w',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];

$inputConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'control-label col-sm-4 col-md-4 ogrn-input-width',
    ],
    'inputOptions' => [
        'class' => 'form-control width100',
    ],
    'wrapperOptions' => [
        'class' => 'col-sm-3 col-md-3 width100 no-margin-l',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];

$inputConfigRequired = [
    'options' => [
        'class' => 'form-group row',
    ],
    'labelOptions' => [
        'class' => 'control-label col-sm-4 col-md-4 width-inp',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-4 field-width width100 no-margin-l',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n sel-w',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];

$fioInitialsConfig = [
    'options' => [
        'class' => 'form-group required',
    ],
    'labelOptions' => [
        'class' => 'col-sm-8 col-md-8 control-label',
    ],
    'inputOptions' => [
        'class' => 'form-control field-width initial-field',
    ],
];

$remoteUrl = Url::to(['/dictionary/address']);?>


<?= $form->field($model, 'inn', array_merge($textInputConfig, [
    'options' => [
        'class' => 'legal form-group',
    ],
]))
    ->label('ИНН:')
    ->textInput([
            'placeholder' => 'Автозаполнение по ИНН',
            'maxlength' => true,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
            ],
        ]
    ); ?>
<div class="row required">
    <?= Html::label('ФИО', null, [
        'class' => 'control-label col-sm-4 col-md-4',
    ]); ?>
    <div class="col-sm-8 col-md-8 surname-input">
        <div class="row" style="margin-left: -5px; margin-right: -5px;">
            <div class="col-sm-4 col-md-4" style="padding-left: 5px; padding-right: 5px;">
                <?= $form->field($model, 'ip_lastname', $inputConfigRequired)
                    ->label(false)
                    ->textInput([
                        'placeholder' => 'Фамилия',
                        'class' => 'form-control'
                    ]);
                ?>
            </div>
            <div class="col-sm-4 col-md-4" style="padding-left: 5px; padding-right: 5px;">
                <?= $form->field($model, 'ip_firstname', $inputConfigRequired)
                    ->label(false)
                    ->textInput([
                        'placeholder' => 'Имя',
                        'class' => 'form-control'
                    ]); ?>
            </div>
            <div class="col-sm-4 col-md-4" style="padding-left: 5px; padding-right: 5px;">
                <?= $form->field($model, 'ip_patronymic', $inputConfigRequired)
                    ->label(false)
                    ->textInput([
                        'placeholder' => 'Отчество',
                        'class' => 'form-control'
                    ]); ?>
            </div>
        </div>
    </div>
</div>
<?= $form->field($model, 'address_legal', $inputConfig)->widget(\yii\widgets\MaskedInput::class, [
    'mask' => '9{6}, ~{1,245}',
    'definitions' => [
        '~' => [
            'validator' => '[ 0-9A-zА-я.,/-]',
            'cardinality' => 1,
        ],
    ],
    'options' => [
        'class' => 'form-control',
        'placeholder' => 'Индекс, Адрес',
    ],
]); ?>
<?php
echo $form->field($model, 'egrip', $inputConfig)
    ->textInput([
        'maxlength' => true,
    ]);
echo $form->field($checkingAccountant, 'rs', $inputConfig)
    ->textInput([
        'maxlength' => true,
    ]);
echo $form->field($checkingAccountant, 'bik', $inputConfig)
    ->widget(\common\components\widgets\BikTypeahead::classname(), [
        'remoteUrl' => Url::to(['/dictionary/bik']),
        'related' => [
            '#' . Html::getInputId($checkingAccountant, 'bank_name') => 'name',
            '#' . Html::getInputId($checkingAccountant, 'bank_city') => 'city',
            '#' . Html::getInputId($checkingAccountant, 'ks') => 'ks',
        ],
        'options' => [
            'placeholder' => 'Автозаполнение по БИК',
        ],
    ]);
echo $form->field($checkingAccountant, 'bank_name', $inputConfig)
    ->textInput([
        'maxlength' => true,
        'readonly' => true,
    ]);
echo $form->field($checkingAccountant, 'bank_city', $inputConfig)
    ->textInput([
        'maxlength' => true,
        'readonly' => true,
    ]);
echo $form->field($checkingAccountant, 'ks', $inputConfig)->textInput([
    'maxlength' => true,
    'readonly' => true,
]);
?>
</div>
<?php
$this->registerJs(<<<JS
$('#new-company-invoice-form #company-inn').suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: '78497656dfc90c2b00308d616feb9df60c503f51',
        type: 'PARTY',
        count: 10,

        onSelect: function(suggestion) {
            $('#new-company-invoice-form #company-inn').val(suggestion.data.inn);

            var nameFullArray = suggestion.data.name.full.split(' ');
            $('#new-company-invoice-form #company-ip_lastname').val(nameFullArray[0]);
            $('#new-company-invoice-form #company-ip_firstname').val(nameFullArray[1]);
            $('#new-company-invoice-form #company-ip_patronymic').val(nameFullArray[2]);

            $('#new-company-invoice-form #company-egrip').val(suggestion.data.ogrn);

            var address = '';
            if (suggestion.data.address.data && suggestion.data.address.data.postal_code) {
                if (suggestion.data.address.value.indexOf(suggestion.data.address.data.postal_code) == -1) {
                    address += suggestion.data.address.data.postal_code + ', ';
                }
            }
            address += suggestion.data.address.value;
            $('#company-address_legal').val(address);
        }
    });
JS
);