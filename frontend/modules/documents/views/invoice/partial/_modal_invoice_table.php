<?php

use common\components\grid\DropDownDataColumn;
use common\components\TextHelper;
use common\models\document\Invoice;
use frontend\modules\documents\components\FilterHelper;
use yii\helpers\Html;
use common\components\grid\GridView;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\documents\models\InvoiceSearch */
/* @var string $documentNumber */
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
</div>
<div class="invoice-list">
    <?= Html::beginForm(['/documents/invoice/get-invoices'], 'get', [
        'id' => 'search-invoice-form',
        'class' => 'add-to-payment-order',
        'data' => [
            'pjax' => true,
        ],
    ]); ?>
    <div class="portlet box btn-invoice m-b-0 shadow-t">
        <div class="search-form-default">
            <div class="col-xs-8 col-md-8 serveces-search m-l-n-sm m-t-10">
                <div class="input-group">
                    <div class="input-cont inp_pad">
                        <?php echo Html::activeTextInput($searchModel, 'document_number', [
                            'id' => 'invoice-number-search',
                            'placeholder' => '№ счёта...',
                            'class' => 'form-control',
                        ]); ?>
                    </div>
                    <span class="input-group-btn">
                        <?= Html::submitButton('Найти &nbsp;<i class="m-icon-swapright m-icon-white"></i>', [
                            'class' => 'btn green-haze',
                        ]); ?>
                    </span>
                </div>
            </div>
        </div>
        <div id="range-ajax-button">
            <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'ajax m-r-10 m-t-10',]); ?>
        </div>
        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <div id="in_invoice_table" class="dataTables_wrapper dataTables_extended_wrapper">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover dataTable customers_table',
                            'id' => 'datatable_ajax',
                            'aria-describedby' => 'datatable_ajax_info',
                            'role' => 'grid',
                        ],
                        'headerRowOptions' => [
                            'class' => 'heading',
                        ],
                        'options' => [
                            'id' => 'invoice-payment-order',
                            'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'pagination pull-right m-t',
                            ],
                        ],
                        'layout' => "{items}\n{pager}",
                        'columns' => [
                            [
                                'attribute' => 'invoice_id',
                                'label' => '',
                                'format' => 'raw',
                                'contentOptions' => [
                                    'style' => 'padding: 7px 5px 5px 5px;',
                                ],
                                'value' => function ($data) {
                                    /** @var Invoice $data */
                                    return Html::checkbox(Html::getInputName($data, 'id') . '[]', false, [
                                        'value' => $data->id,
                                        'class' => 'checkbox-invoice-id',
                                        'style' => 'margin-left: -9px;',
                                    ]);
                                },
                                'headerOptions' => [
                                    'width' => '4%',
                                ],
                            ],
                            [
                                'attribute' => 'document_date',
                                'label' => 'Дата счёта',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '24%',
                                ],
                                'format' => ['date', 'php:' . \common\components\date\DateHelper::FORMAT_USER_DATE],
                            ],
                            [
                                'attribute' => 'document_number',
                                'label' => '№ счёта',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '24%',
                                ],
                                'format' => 'raw',
                                'value' => function ($data) {
                                    /** @var Invoice $data */
                                    return Html::a($data->fullNumber, ['invoice/view', 'type' => $data->type, 'id' => $data->id]);
                                },
                            ],
                            [
                                'label' => 'Сумма',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '24%',
                                ],
                                'attribute' => 'total_amount_with_nds',
                                'value' => function (Invoice $model) {
                                    return TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2);
                                },
                            ],
                            [
                                'attribute' => 'contractor_id',
                                'label' => 'Контрагент',
                                'class' => \common\components\grid\DropDownSearchDataColumn::className(),
                                'headerOptions' => [
                                    'width' => '24%',
                                ],
                                'filter' => FilterHelper::getContractorList($searchModel->type, null, true, false, false),
                                'format' => 'raw',
                                'value' => 'contractor_name_short',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>
</div>
<div class="row action-buttons">
    <div class="col-sm-4">
        <button id="add-invoice-to-payment-order" class="btn btn-add darkblue" data-dismiss="modal" style="float: left;">Добавить</button>
    </div>
</div>