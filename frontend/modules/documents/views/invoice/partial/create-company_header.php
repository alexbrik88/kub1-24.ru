<?php
use common\models\Company;
use common\models\company\CompanyType;

/* @var $this yii\web\View */
/* @var $model Company */
?>
<button type="button" class="close" data-dismiss="modal"
        aria-hidden="true"></button>
<h1>Данные по
    ваше<?= $model->company_type_id == CompanyType::TYPE_IP ? 'му ИП' : 'й компании'; ?></h1>
