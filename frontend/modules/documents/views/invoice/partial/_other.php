<?php
use common\models\address;
use common\models\company\CompanyType;
use common\components\helpers\Html;
use common\components\widgets\AddressTypeahead;
use common\models\dictionary\address\AddressDictionary;
use common\components\helpers\ArrayHelper;
use yii\helpers\Url;
use common\components\widgets\BikTypeahead;
use common\models\company\CheckingAccountant;

/* @var $checkingAccountant CheckingAccountant */

if (!$model->address_legal_house_type_id) {
    $model->address_legal_house_type_id = \common\models\address\AddressHouseType::TYPE_HOUSE;
}
if (!$model->address_legal_apartment_type_id) {
    $model->address_legal_apartment_type_id = \common\models\address\AddressApartmentType::TYPE_APARTMENT;
}
$inputThinConfig = [
    'options' => [
        'class' => '',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{input}\n{error}\n{hint}",
];
$inputConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'control-label col-sm-4 col-md-4 ogrn-input-width',
    ],
    'inputOptions' => [
        'class' => 'form-control width100',
    ],
    'wrapperOptions' => [
        'class' => 'col-sm-3 col-md-3 width100 no-margin-l',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
$inputConfigRequired = [
    'options' => [
        'class' => 'form-group row',
    ],
    'labelOptions' => [
        'class' => 'control-label col-sm-4 col-md-4 width-inp',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-4 field-width width100 no-margin-l',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n sel-w',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
$textInputConfigs = [
    'options' => [
        'class' => 'form-group row',
    ],
    'labelOptions' => [
        'class' => 'control-label col-sm-4 col-md-4 width-inp',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n sel-w',
        'placeholder' => 'Автозаполнение по ИНН',
    ],
];
$textInputConfig = [
    'labelOptions' => [
        'class' => 'control-label col-sm-4 col-md-4',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-8 inp_one_line width-inp',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n sel-w',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
$inputListConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'control-label col-sm-8 col-md-8 label-width',
    ],
];
$fioInitialsConfig = [
    'options' => [
        'class' => 'form-group required',
    ],
    'labelOptions' => [
        'class' => 'col-sm-8 col-md-8 control-label',
    ],
    'inputOptions' => [
        'class' => 'form-control field-width initial-field',
    ],
];
$remoteUrl = Url::to(['/dictionary/address']);

echo $form->field($model, 'inn', array_merge($textInputConfig, [
    'options' => [
        'class' => 'legal form-group',
    ],
]))->label('ИНН:')->textInput([
    'placeholder' => 'Автозаполнение по ИНН',
    'maxlength' => true,
    'data' => [
        'toggle' => 'popover',
        'trigger' => 'focus',
        'placement' => 'bottom',
    ],
]);
echo $form->field($model, 'name_short', array_merge($textInputConfig, [
    'options' => [
        'class' => 'form-group row',
    ],
    'wrapperOptions' => [
        'class' => 'col-sm-8 col-md-8 inp_one_line width-inp',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n sel-w',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
]))
    ->label()
    ->textInput([
        'placeholder' => 'Без ООО/ИП',
        'maxlength' => true,
        'disabled' => $model->company_type_id == CompanyType::TYPE_IP,
        'data' => [
            'toggle' => 'popover',
            'trigger' => 'focus',
            'placement' => 'bottom',
            'content' => 'Впишите название компании без организационно-правовой формы (ИП, ООО, ЗАО, ПАО).',
        ],
    ]);

$companyTypes = CompanyType::getCompanyTypeVariants($model->company_type_id);

echo $form->field($model, 'company_type_id', array_merge($textInputConfig, [
    'options' => [
        'id' => 'contractor_company_type',
        'class' => 'form-group legal row field-contractor-company_type_id field-company-company_type_id required',
    ],
    'labelOptions' => [
        'class' => 'control-label col-sm-4 col-md-4 control-label_no-marg',
    ],
    'wrapperOptions' => [
        'class' => 'col-sm-4 col-md-4 inp_one_line width-inp',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n p-f sel-w',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
]))
    ->label('Форма')
    ->dropDownList(ArrayHelper::map($companyTypes, 'id', 'name_short'), [
        'disabled' => count($companyTypes) === 1,
    ]); ?>
    <div class="row required">
        <?= Html::label('ФИО Руководителя', null, [
            'class' => 'control-label col-sm-4 col-md-4',
        ]); ?>
        <div class="col-sm-8 col-md-8 surname-input">
            <div class="row" style="margin-left: -5px; margin-right: -5px;">
                <div class="col-sm-4 col-md-4" style="padding-left: 5px; padding-right: 5px;">
                    <?= $form->field($model, 'chief_lastname', $inputConfigRequired)
                        ->label(false)
                        ->textInput([
                            'placeholder' => 'Фамилия',
                            'class' => 'form-control'
                        ]);
                    ?>
                </div>
                <div class="col-sm-4 col-md-4" style="padding-left: 5px; padding-right: 5px;">
                    <?= $form->field($model, 'chief_firstname', $inputConfigRequired)
                        ->label(false)
                        ->textInput([
                            'placeholder' => 'Имя',
                            'class' => 'form-control'
                        ]); ?>
                </div>
                <div class="col-sm-4 col-md-4" style="padding-left: 5px; padding-right: 5px;">
                    <?= $form->field($model, 'chief_patronymic', $inputConfigRequired)
                        ->label(false)
                        ->textInput([
                            'placeholder' => 'Отчество',
                            'class' => 'form-control'
                        ]); ?>
                </div>
            </div>
        </div>
    </div>
    <?= $form->field($checkingAccountant, 'rs', $inputConfig)->textInput([
        'maxlength' => true,
    ]); ?>
    <?= $form->field($checkingAccountant, 'bik', $inputConfig)->widget(BikTypeahead::classname(), [
        'remoteUrl' => Url::to(['/dictionary/bik']),
        'related' => [
            '#' . Html::getInputId($checkingAccountant, 'bank_name') => 'name',
            '#' . Html::getInputId($checkingAccountant, 'bank_city') => 'city',
            '#' . Html::getInputId($checkingAccountant, 'ks') => 'ks',
        ],
        'options' => [
            'placeholder' => 'Автозаполнение по БИК',
        ],
    ]); ?>
    <?= $form->field($checkingAccountant, 'bank_name', $inputConfig)->textInput([
        'maxlength' => true,
        'readonly' => true,
    ]); ?>
    <?= $form->field($checkingAccountant, 'bank_city', $inputConfig)->textInput([
        'maxlength' => true,
        'readonly' => true,
    ]); ?>
    <?= $form->field($checkingAccountant, 'ks', $inputConfig)->textInput([
        'maxlength' => true,
        'readonly' => true,
    ]); ?>
    <?= $form->field($model, 'phone', array_merge($inputConfig, [
        'options'=>['class'=>'form-group required']
    ]))->widget(\yii\widgets\MaskedInput::className(), [

        'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
        'options' => [
            'class' => 'form-control field-width field-w inp_one_line_company',
            'placeholder' => '+7(XXX) XXX-XX-XX (Нужен для восстановления пароля)',
        ],
    ]); ?>
    <span class="addColumns long-message-input">
        Дополнительная информация по вашей компании
    </span>
    <div class="dopColumns selectedDopColumns">
        <?= $form->field($model, 'address_legal', $inputConfig)->widget(\yii\widgets\MaskedInput::class, [
            'mask' => '9{6}, ~{1,245}',
            'definitions' => [
                '~' => [
                    'validator' => '[ 0-9A-zА-я.,/-]',
                    'cardinality' => 1,
                ],
            ],
            'options' => [
                'class' => 'form-control',
                'placeholder' => 'Индекс, Адрес',
            ],
        ]); ?>
        <?= $form->field($model, 'ogrn', $inputConfig)->textInput([
            'maxlength' => true,
        ]); ?>
        <?= $form->field($model, 'kpp', $textInputConfig)->textInput([
            'maxlength' => true,
        ]); ?>
    </div>

<?php
$this->registerJs(<<<JS
    $('[id="company-inn"]').suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: '78497656dfc90c2b00308d616feb9df60c503f51',
        type: 'PARTY',
        count: 10,

        onSelect: function(suggestion) {
            $('#new-company-invoice-form #company-name_short').val(suggestion.data.name.short);
            $('#company-inn').val(suggestion.data.inn);
            $('#company-kpp').val(suggestion.data.kpp);
            $('#company-ogrn').val(suggestion.data.ogrn);

            var address = '';
            if (suggestion.data.address.data && suggestion.data.address.data.postal_code) {
                if (suggestion.data.address.value.indexOf(suggestion.data.address.data.postal_code) == -1) {
                    address += suggestion.data.address.data.postal_code + ', ';
                }
            }
            address += suggestion.data.address.value;
            $('#company-address_legal').val(address);

            var nameFull = suggestion.data.management.name;
                var lastName = '';
                var firstName = '';
                var patronymic = '';
                var endLastNamePlace = nameFull.indexOf(' ');
                for (var i = 0; i < endLastNamePlace; i++) {
                    lastName += nameFull[i];
                }
                var endNamePlace = nameFull.indexOf(' ', endLastNamePlace+1);
                for (var i = endLastNamePlace+1; i < endNamePlace; i++) {
                    firstName += nameFull[i];
                }
                for (var i = endNamePlace+1; i < nameFull.length; i++) {
                    patronymic += nameFull[i];
                }
                $('#company-chief_lastname').val(lastName);
                $('#company-chief_firstname').val(firstName);
                $('#company-chief_patronymic').val(patronymic);
        }
    });
JS
);