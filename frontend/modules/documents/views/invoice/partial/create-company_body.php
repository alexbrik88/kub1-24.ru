<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use common\models\company\CompanyType;
use common\models\company\CheckingAccountant;

$this->registerJs('
$("#company-has_chief_patronymic:not(.md-check)").uniform();
$(document).on("change", "#company-has_chief_patronymic", function () {
    if ($(this).prop("checked")) {
        $("#company-chief_patronymic, #company-ip_patronymic")
            .val("")
            .attr("readonly", true)
            .trigger("change");
        $(".field-company-chief_patronymic, .field-company-ip_patronymic")
            .removeClass("has-error")
            .find(".help-block").html("");
    } else {
        $("#company-chief_patronymic, #company-ip_patronymic")
            .attr("readonly", false);
    }
});
');

$form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'id' => 'new-company-invoice-form',
    'validationUrl' => ['validate-company-modal'],
    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'control-label col-md-4 width-inp',
        ],
    ],
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'validateOnSubmit' => true,
    'validateOnBlur' => true,
]));

/* @var $checkingAccountant CheckingAccountant */
$companyTypes = \common\models\company\CompanyType::getCompanyTypeVariants($model->company_type_id);
?>
<?= Html::activeHiddenInput($model, 'self_employed') ?>
<?= $form->field($model, 'company_type_id', [
    'options' => [
        'id' => 'contractor_company_type',
        'class' => 'one_line_date_dismissal hidden',
    ],
    'labelOptions' => [
        'class' => 'control-label col-md-1 m-m-f f-l-l m-m-f-r',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-w col-md-2 f-l sel-w inp_one_line_company',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n p-f sel-w',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
])->label('Форма')->dropDownList(\yii\helpers\ArrayHelper::map($companyTypes, 'id', 'name_short'), [
    'disabled' => count($companyTypes) === 1,
]); ?>
<?php if ($model->self_employed) : ?>
    <?= $this->render('_self', [
        'checkingAccountant' => $checkingAccountant,
        'model' => $model,
        'form' => $form,
    ]) ?>
<?php elseif ($model->company_type_id == CompanyType::TYPE_IP) : ?>
    <?= $this->render('_ip', [
        'checkingAccountant' => $checkingAccountant,
        'model' => $model,
        'form' => $form,
    ]) ?>
<?php else: ?>
    <?= $this->render('_other', [
        'checkingAccountant' => $checkingAccountant,
        'model' => $model,
        'form' => $form,
    ]) ?>
<?php endif; ?>
    <div class="form-body form-horizontal">
        <div class="form-actions">
            <div class="row action-buttons">
                <div class="button-bottom-page-lg col-sm-1 col-xs-2" style="width: 23.45%;">
                    <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs ladda-button',
                        'data-style' => 'expand-right',
                        'style' => 'width: 100%!important;',
                    ]); ?>
                    <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                        'title' => 'Сохранить',
                    ]); ?>
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width: 23.45%;">
                    <a href="#" style="width: 100%!important;"
                       class="btn darkblue gray-darkblue widthe-100 hidden-md hidden-sm hidden-xs">Отменить</a>
                    <a href="#" class="btn darkblue gray-darkblue widthe-100 hidden-lg"
                       title="Отменить"><i class="fa fa-reply fa-2x"></i></a>
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
            </div>
        </div>
    </div>

<?php ActiveForm::end(); ?>