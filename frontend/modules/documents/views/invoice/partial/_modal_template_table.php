<?php
use common\components\grid\DropDownDataColumn;
use common\components\TextHelper;
use common\models\document\Invoice;
use frontend\modules\documents\components\FilterHelper;
use yii\helpers\Html;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\documents\models\InvoiceSearch */
/* @var string $documentNumber */
?>

<?php \yii\widgets\Pjax::begin([
    'options' => [
        'id' => 'get-invoices-pjax',
    ],
    'enablePushState' => false,
]); ?>

    <div class="portlet box btn-invoice m-b-0 shadow-t">

        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <div id="in_invoice_table" class="dataTables_wrapper dataTables_extended_wrapper">
                    <?php echo common\components\grid\GridView::widget([
                        'dataProvider' => $templateProvider,
                        'filterModel' => $templateSearchModel,
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover dataTable documents_table overfl_text_hid',
                            'aria-describedby' => 'datatable_ajax_info',
                            'role' => 'grid',
                        ],

                        'headerRowOptions' => [
                            'class' => 'heading',
                        ],

                        'options' => [
                            'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'pagination pull-right m-t',
                            ],
                        ],
                        'layout' => "{items}\n{pager}",
                        'columns' => [
                            [
                                'attribute' => 'created_at',
                                'label' => 'Дата создания',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '24%',
                                ],
                                'format' => ['date'],
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

<?php \yii\widgets\Pjax::end(); ?>