<?php

use common\models\document\Invoice;
use frontend\modules\documents\forms\InvoiceOutViewForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\modules\documents\forms\InvoiceOutViewForm */
/* @var $invoice common\models\document\Invoice */

$companyExists = Yii::$app->user->isGuest ? false : Yii::$app->user->identity->getCompanies()->andWhere([
    'inn' => $invoice->contractor_inn,
    'blocked' => false,
])->andFilterWhere([
    'kpp' => $invoice->contractor_kpp,
])->exists();

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $invoice->document_date,
]);
?>

<?php Pjax::begin([
    'id' => 'out-view-pjax',
    'enablePushState' => false,
    'timeout' => 5000,
]); ?>

<?= \frontend\widgets\Alert::widget(); ?>

<?php $form = ActiveForm::begin([
    'id' => 'out-view-form',
    'action' => [
        'out-view-signup',
        'uid' => $invoice->uid,
        'email' => $email,
    ],
    'options' => [
        'data-pjax' => '',
    ],
]); ?>

<div class="row"">
    <div class="col-sm-12 form-header border-bottom-e4">
        Зарегистрируйтесь в сервисе КУБ,
        который помогает предпринимателям автоматизировать работу с документами и финансами.
        14 дней БЕСПЛАТНО.
    </div>
</div>

<?= $form->field($model, 'username', [
    'class' => 'common\components\floatLabelField\FloatLabelActiveField',
])->textInput(); ?>

<?= $form->field($model, 'password', [
    'class' => 'common\components\floatLabelField\FloatLabelActiveField',
    'validateOnChange' => false,
    'validateOnBlur' => false,
])->passwordInput(); ?>

<div class="text-bold">
    <?= $invoice->contractor_name_short ?>
</div>

<?= $form->field($model, 'taxationType')->checkboxList(InvoiceOutViewForm::$taxation, [
    'unselect' => null,
]); ?>

<div class="form-group">
    <?= Html::activeInput('submit', $model, 'registration', [
        'class' => 'btn darkblue text-white',
        'value' => 'Попробовать бесплатно',
    ]) ?>
</div>

<div class="form-group">
    Регистрируясь в системе, Вы даете согласие на
    <a href="<?= rtrim(Yii::$app->params['serviceSite'], '/') ?>/SecurityPolicy/Security_Policy.pdf"
       target="_blank">
        обработку персональных данных
    </a>
    и принимаете условия
    <a href="<?= rtrim(Yii::$app->params['serviceSite'], '/') ?>/TermsOfUseAll/TermsOfUseAll.pdf"
       target="_blank">
        лицензионного соглашения
    </a>.
</div>

<div>
    Уже зарегистрированы?
</div>
<?= Html::a('Войти в систему', [
    'out-view-login',
    'uid' => $invoice->uid,
    'email' => $email,
]) ?>

<?php ActiveForm::end(); ?>

<?php Pjax::end(); ?>

