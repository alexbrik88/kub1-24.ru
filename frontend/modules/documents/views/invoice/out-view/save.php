<?php

use common\models\document\Invoice;
use frontend\models\Documents;
use frontend\modules\documents\forms\InvoiceOutViewForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\modules\documents\forms\InvoiceOutViewForm */
/* @var $invoice common\models\document\Invoice */

$companyExists = Yii::$app->user->isGuest ? false : Yii::$app->user->identity->getCompanies()->andWhere([
    'inn' => $invoice->contractor_inn,
    'blocked' => false,
])->andFilterWhere([
    'kpp' => $invoice->contractor_kpp,
])->exists();

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $invoice->document_date,
]);
?>

<?php Pjax::begin([
    'id' => 'out-view-pjax',
    'enablePushState' => false,
    'timeout' => 5000,
]); ?>

<?= \frontend\widgets\Alert::widget(); ?>

<?php $form = ActiveForm::begin([
    'id' => 'out-view-form',
    'action' => [
        'out-view-save',
        'uid' => $invoice->uid,
        'email' => $email,
    ],
    'options' => [
        'data-pjax' => '',
    ],
]); ?>

<div class="row"">
    <div class="col-sm-12 form-header border-bottom-e4">
        <?php if ($invoice->is_invoice_contract && $invoice->type == Documents::IO_TYPE_OUT): ?>
            Счет-договор
        <?php else: ?>
            Счет
        <?php endif; ?>
        от <?= $invoice->company_name_short; ?>
        №
        <?= $invoice->fullNumber; ?>
        от
        <?= $dateFormatted; ?>
        <br>
        будет сохранен в компанию
        <br>
        <span style="font-weight: bold;">
            <?= $invoice->contractor_name_short ?>
        </span>
    </div>
</div>

<?php if ($companyExists) : ?>
    <div class="row">
        <div class="col-xs-6">
            <?= Html::activeInput('submit', $model, 'save', [
                'class' => 'btn darkblue text-white',
                'value' => 'Сохранить',
                'style' => 'width: 100%;',
            ]); ?>
        </div>
        <div class="col-xs-6">
            <?= Html::button('Отменить', [
                'class' => 'btn darkblue text-white close-panel',
                'style' => 'width: 100%;',
            ]); ?>
        </div>
    </div>
<?php else : ?>
    <div class="form-group">
        Добавление
        <span class="text-bold">
            <?= $invoice->contractor_name_short ?>
        </span>
    </div>

    <?= $form->field($model, 'taxationType')->checkboxList(InvoiceOutViewForm::$taxation, [
        'unselect' => null,
    ]); ?>

    <div class="row">
        <div class="col-xs-6">
            <?= Html::activeInput('submit', $model, 'save', [
                'class' => 'btn darkblue text-white',
                'value' => 'Добавить',
                'style' => 'width: 100%;',
            ]); ?>
        </div>
        <div class="col-xs-6">
            <?= Html::button('Отменить', [
                'class' => 'btn darkblue text-white close-panel',
                'style' => 'width: 100%;',
            ]); ?>
        </div>
    </div>
<?php endif ?>

<?php ActiveForm::end(); ?>

<?php Pjax::end(); ?>
