<?php

use common\models\Company;
use common\models\document\Invoice;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $ioType integer */
/* @var $fixedContractor boolean */
/* @var $isAuto boolean */

$invoiceBlock = 'invoice-block' . ($isAuto ? ' hidden' : '');
$autoinvoiceBlock = 'autoinvoice-block' . ($isAuto ? '' : ' hidden');
?>

<div class="form-actions">
    <div class="row action-buttons">
        <div class="spinner-button col-sm-1 col-xs-1">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs create-invoice mt-ladda-btn ladda-button'
                    . ((Yii::$app->user->identity->company->strict_mode == Company::ON_STRICT_MODE) ? ' company-in-strict-mode' : ''),
                'data-style' => 'expand-right',
            ]); ?>
            <?= Html::submitButton('<span class="ico-Save-smart-pls fs"></span>', [
                'class' => 'btn darkblue widthe-100 hidden-lg create-invoice'
                    . ((Yii::$app->user->identity->company->strict_mode == Company::ON_STRICT_MODE) ? ' company-in-strict-mode' : ''),
                'title' => 'Сохранить',
            ]); ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?php if (!$model->isAutoinvoice && $model->scenario == 'insert' && $ioType == 2 && \Yii::$app->controller->action->id != 'first-create') { ?>
                <button type="button" class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs autoinvoice <?= $invoiceBlock ?>">
                    АвтоСчет
                </button>
                <button type="button" class="btn darkblue widthe-100 hidden-lg autoinvoice <?= $invoiceBlock ?>" title="АвтоСчёт">
                    <i class="fa fa-repeat fa-2x"></i>
                </button>
            <?php } ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="spinner-button col-sm-1 col-xs-1">
            <div class="<?= $invoiceBlock ?>">
                <?php
                $cancelUrl = Url::previous('lastPage');
                if (Yii::$app->request->get('document') == 'proxy') {
                    $cancelUrl = ['/documents/proxy/index', 'type' => $ioType];
                }
                $permission = $model->isNewRecord ?
                    frontend\rbac\permissions\document\Document::INDEX :
                    frontend\rbac\permissions\document\Document::VIEW;

                if (Yii::$app->user->can($permission, ['model' => $model, 'ioType' => $ioType,])) {
                    echo Html::a('Отменить', $cancelUrl, [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                        'data-style' => 'expand-right',
                    ]);
                    echo Html::a('<span class="ico-Cancel-smart-pls fs"></span>', $cancelUrl, [
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                        'title' => 'Отменить',
                    ]);
                } ?>
            </div>
            <div class="<?= $autoinvoiceBlock ?>">
                <?php
                $cancelUrl = Url::previous('lastPage');
                $permission = $model->isNewRecord ?
                    frontend\rbac\permissions\document\Document::INDEX :
                    frontend\rbac\permissions\document\Document::VIEW;

                if (Yii::$app->user->can($permission, ['model' => $model, 'ioType' => $ioType,])) {
                    echo Html::a('Отменить', $cancelUrl, [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                        'data-style' => 'expand-right',
                    ]);
                    echo Html::a('<span class="ico-Cancel-smart-pls fs"></span>', $cancelUrl, [
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                        'title' => 'Отменить',
                    ]);
                } ?>
            </div>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
    </div>
</div>
