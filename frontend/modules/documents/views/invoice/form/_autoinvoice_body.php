<?php

use common\components\date\DateHelper;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\NdsOsno;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\modules\documents\components\FilterHelper;
use frontend\modules\documents\components\Message;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\Company;
use common\models\TaxationType;
use yii\helpers\Url;
use common\models\company\CheckingAccountant;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $fixedContractor boolean */
/* @var $message Message */
/* @var $ioType integer */

$contractorType = $ioType == \frontend\models\Documents::IO_TYPE_IN ? Contractor::TYPE_SELLER : Contractor::TYPE_CUSTOMER;

$contractorDropDownConfig = [
    'class' => 'form-control contractor-select',
    'disabled' => $fixedContractor,
];

$contractorArray = Contractor::getAllContractorList($contractorType);

if (empty($model->contractor_id)) {
    $contractorDropDownConfig['prompt'] = '';
}
?>

<div class="row">
    <div class="col-lg-6 col-md-8 col-sm-9 col-xs-12">
        <div class="form-group row">
            <div class="col-sm-4" style="max-width: 175px">
                <label class="control-label">
                    <?= $message->get(Message::CONTRACTOR); ?>
                    <span class="required" aria-required="true">*</span>:
                </label>
            </div>
            <div id="add-first-contractor" class="col-sm-8 details"
                 style="display: <?= count($contractorArray) > 1 ? 'none' : 'block'; ?>">
                <?= Html::button('Заполнить реквизиты по ИНН', ['class' => 'btn yellow contractor']); ?>
                <span class="tooltip2 ico-question valign-middle"
                      data-tooltip-content="#tooltip_contractor">
                </span>
                <span class="ico-video valign-middle" data-toggle="modal"
                      data-target="#video_modal_contractor">
                </span>
            </div>
            <div id="select-existing-contractor" class="col-sm-8"
                 style="display: <?= count($contractorArray) > 1 ? 'block' : 'none'; ?>;">
                <?php echo $form->field($model, 'contractor_id', ['template' => "{input}{hint}{error}", 'options' => [
                    'class' => '',
                    'style' => 'max-width: 500px;',
                ]])->widget(Select2::classname(), [
                    'data' => $contractorArray,
                    'options' => $contractorDropDownConfig,
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(false); ?>
            </div>
        </div>

        <?php if ($model->type == Documents::IO_TYPE_IN): ?>
            <div class="form-group required row">
                <div class="col-sm-4" style="max-width: 175px">
                    <label class="control-label">
                        Статья расходов
                    </label>
                </div>
                <div class="col-sm-8">
                    <?= $form->field($model, 'invoice_expenditure_item_id', ['template' => "{input}", 'options' => ['class' => '']])->dropDownList(ArrayHelper::map(InvoiceExpenditureItem::find()->orderBy(['sort' => SORT_ASC, 'name' => SORT_ASC])->all(), 'id', 'name'), [
                        'class' => 'form-control',
                        'prompt' => '',
                    ])->label(false); ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if ($company->strict_mode == Company::ON_STRICT_MODE) : ?>
            <div class="form-group required your-company row">
                <div class="col-sm-4" style="max-width: 175px">
                    <label class="control-label">
                        Ваша компания
                    </label>
                </div>
                <div class="col-sm-8 details"
                     style="display: <?php Company::hasRequiredParams() ? 'none' : 'blocke'; ?>">
                    <?= Html::button('Заполнить реквизиты по ИНН', ['class' => 'btn yellow company']); ?>
                    <span class="tooltip2 ico-question valign-middle"
                          data-tooltip-content="#tooltip_company">
                    </span>
                    <span class="ico-video valign-middle" data-toggle="modal"
                          data-target="#video_modal_company">
                    </span>
                </div>
                <?= Html::hiddenInput('Company[strict_mode]'); ?>
            </div>
        <?php endif; ?>
    </div>
</div>


<div class="invoice-production-type-block form-group form-group-type_account">
    <?php if ($company->strict_mode == Company::ON_STRICT_MODE) : ?>
        <?php if ($company->companyTaxationType->osno) : ?>
            <label class="col-md-2 control-label width-179">
                Тип счета
            </label>
            <?php
            if ($company->nds === null) {
                $company->nds = NdsOsno::WITH_NDS;
            }
            echo Html::activeRadioList($company, 'nds', ArrayHelper::map(NdsOsno::find()->all(), 'id', 'name'), [
                'class' => 'inp_one_line_company checkbox-width',
                'item' => function ($index, $label, $name, $checked, $value) {
                    return Html::tag('label', Html::radio($name, $checked, ['value' => $value]) . $label, [
                            'class' => 'radio-inline p-o radio-padding',
                        ]) . Html::a('Пример счета', [Url::to('/company/view-example-invoice'), 'nds' => $value == 1 ? true : false], [
                            'class' => 'radio-inline p-o radio-padding',
                            'target' => '_blank',
                        ]);
                },
            ]);
            ?>

        <?php endif; ?>
    <?php endif; ?>
    <?php
    //        echo $form->field($company, 'nds', $inputListConfig)
    //                ->label('В счетах цену за товар/услугу указывать:')
    //                ->radioList(ArrayHelper::map(\common\models\NdsOsno::find()->all(), 'id', 'name'), [
    //                    'class' => 'inp_one_line_company',
    //                    'item' => function ($index, $label, $name, $checked, $value) {
    //                        return Html::tag('label', Html::radio($name, $checked, ['value' => $value]) . $label, [
    //                                    'class' => 'radio-inline p-o radio-padding',
    //                                ]) . Html::a('Пример счета', ['view-example-invoice', 'nds' => $value == 1 ? true : false], [
    //                                    'class' => 'radio-inline p-o radio-padding',
    //                                    'target' => '_blank',
    //                        ]);
    //                    },
    //                ]);
    ?>

    <?=
    Html::activeHiddenInput($model, 'production_type', [
        'class' => 'invoice-production-type-hidden',
        'value' => '',
    ]);
    ?>
</div>