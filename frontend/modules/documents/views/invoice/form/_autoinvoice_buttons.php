<?php

use common\models\document\Invoice;
use yii\helpers\Html;
use common\models\Company;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $ioType integer */
/* @var $fixedContractor boolean */
?>

<div class="form-actions">
    <div class="row action-buttons">
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?php /*if (Yii::$app->user->identity->company->strict_mode == Company::ON_STRICT_MODE) {
                echo Html::a('Сохранить', 'javascript:;', [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs company-in-strict-mode',
                ]);
                echo Html::a('<span class="ico-Save-smart-pls fs"></span>', 'javascript:;', [
                    'class' => 'btn darkblue widthe-100 hidden-lg company-in-strict-mode',
                    'title' => 'Сохранить',
                ]);
                $display = 'none';
            } else {
                $display = 'block';
            };*/ ?>
            <?= Html::submitButton('Сохранить', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs create-invoice' . ((Yii::$app->user->identity->company->strict_mode == Company::ON_STRICT_MODE) ? 'company-in-strict-mode' : ''),
                //'style' => 'display: ' . $display . ';'
            ]); ?>
            <?= Html::submitButton('<span class="ico-Save-smart-pls fs"></span>', [
                'class' => 'btn darkblue widthe-100 hidden-lg create-invoice' . ((Yii::$app->user->identity->company->strict_mode == Company::ON_STRICT_MODE) ? 'company-in-strict-mode' : ''),
                'title' => 'Сохранить',
                //'style' => 'display: ' . $display . ';'
            ]); ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?php
            if (!$model->isNewRecord) {
                $cancelUrl = ['view',
                    'type' => $ioType,
                    'id' => $model->id,
                    'contractorId' => Yii::$app->request->getQueryParam('contractorId'),
                ];
            } else {
                $cancelUrl = $fixedContractor
                    ? ['/contractor/view', 'type' => $ioType, 'id' => $model->contractor_id,]
                    : ['index', 'type' => $ioType,];
            }

            $permission = $model->isNewRecord
                ? frontend\rbac\permissions\document\Document::INDEX
                : frontend\rbac\permissions\document\Document::VIEW;
            if (Yii::$app->user->can($permission, ['model' => $model, 'ioType' => $ioType,])) {
                echo Html::a('Отменить', $cancelUrl, [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ]);
                echo Html::a('<span class="ico-Cancel-smart-pls fs"></span>', $cancelUrl, [
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                    'title' => 'Отменить',
                ]);
            } ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
    </div>
</div>


<div class="modal fade" id="avtoschet" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-middle">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true"></button>
                <h1>Создать повторяющийся счет</h1>
            </div>
            <div class="modal-body">
                <form action="#" class="add-avtoschet">
                    <div class="form-body">
                        <div class="row">
                            <div class="form-group add-avtoschet-overlow">
                                <label
                                    class="control-label col-sm-3 col-md-3 add-avtoschet_label">Периодичность</label>

                                <div class="col-md-4 col-sm-4">
                                    <select class="form-control width100">
                                        <option>Каждый месяц</option>
                                        <option>Каждых 2 месяца</option>
                                        <option>Каждых 3 месяца</option>
                                    </select>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <select class="form-control width100">
                                        <option>15</option>
                                        <option>20</option>
                                        <option>25</option>
                                    </select>
                                </div>
                                <label
                                    class="control-label col-sm-3 col-md-3 add-avtoschet_label-number">Числа</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group add-avtoschet-overlow">
                                <label
                                    class="control-label col-sm-3 col-md-3 add-avtoschet_label">Начало</label>

                                <div class="col-md-3 col-sm-3">
                                    <div class="input-icon">
                                        <i class="fa fa-calendar"></i>
                                        <input
                                            class="form-control date-picker modal-document-date"
                                            data-date-viewmode="years">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group add-avtoschet-overlow">
                                <label
                                    class="control-label col-sm-3 col-md-3 add-avtoschet_label">Окончание</label>

                                <div class="col-md-3 col-sm-3">
                                    <div class="input-icon">
                                        <i class="fa fa-calendar"></i>
                                        <input
                                            class="form-control date-picker modal-document-date"
                                            data-date-viewmode="years">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="checkbox-inline">
                                    <div class="checker"
                                         id="uniform-inlineCheckbox1"><span
                                            class=""><input type="checkbox"
                                                            id=""
                                                            value=""></span>
                                    </div>
                                    В названии услуг автоматически добавлять
                                    месяц и год</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-body form-horizontal margin-top-30">
                        <div class="form-actions">
                            <div class="row action-buttons">
                                <div
                                    class="button-bottom-page-lg col-sm-1 col-xs-1">
                                   <?= Html::submitButton('Сохранитьfsdfg',['options'=>['class' => 'btn darkblue widthe-100 hidden-lg']]) ?>
                                </div>
                                <div
                                    class="button-bottom-page-lg col-sm-1 col-xs-1">
                                </div>
                                <div
                                    class="button-bottom-page-lg col-sm-1 col-xs-1">
                                </div>
                                <div
                                    class="button-bottom-page-lg col-sm-1 col-xs-1">
                                </div>
                                <div
                                    class="button-bottom-page-lg col-sm-1 col-xs-1">
                                </div>
                                <div
                                    class="button-bottom-page-lg col-sm-1 col-xs-1">
                                </div>
                                <div
                                    class="button-bottom-page-lg col-sm-1 col-xs-1">
                                    <a href="#"
                                       class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs">Отменить</a>
                                    <a href="#"
                                       class="btn darkblue widthe-100 hidden-lg"
                                       title="Отменить"><i
                                            class="fa fa-reply fa-2x"></i></a>
                                </div>
                                <div
                                    class="button-bottom-page-lg col-sm-1 col-xs-1">
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>