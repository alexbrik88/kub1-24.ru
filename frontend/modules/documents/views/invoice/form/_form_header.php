<?php

use common\components\date\DateHelper;
use common\models\document\Invoice;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\bootstrap\Dropdown;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $message Message */
/* @var $form ActiveForm */
/* @var $isAuto boolean */
/* @var $document string */

$invoiceBlock = 'invoice-block' . ($isAuto ? ' hidden' : '');
$autoinvoiceBlock = 'autoinvoice-block' . ($isAuto ? '' : ' hidden');
?>
<div class="portlet-title">
    <div class="caption" style="width: 100%">
        <table cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td valign="middle" style="width:1%; white-space:nowrap;">

                    <?php if (Yii::$app->request->get('mode') == 'previewScan'
                        && !$isAuto
                        && $model->type == Documents::IO_TYPE_IN
                        && Yii::$app->controller->action->id != 'first-create'
                        && in_array($document, [Documents::SLUG_ACT, Documents::SLUG_PACKING_LIST, Documents::SLUG_INVOICE_FACTURE, Documents::SLUG_UPD])
                        && ($model->isNewRecord)): ?>

                            <!-- ADD DOC WITH INVOICE NUMBER/DATE -->
                            <?= $this->render('_form_header_in_document_create', [
                                'create' => $create,
                                'ioType' => $ioType,
                                'invoiceBlock' => $invoiceBlock,
                                'newDoc' => $newDoc,
                                'model' => $model,
                                'document' => $document
                            ]) ?>

                    <?php else : ?>

                        <span class="<?= $invoiceBlock ?>">
                            <?= Html::hiddenInput('', (int)$model->isNewRecord, [
                                'id' => 'isNewRecord',
                            ]) ?>
                            <?= Html::activeHiddenInput($model, 'is_invoice_contract'); ?>
                            <?php if ($document == Documents::SLUG_ACT): ?>
                                <?= ($ioType == Documents::IO_TYPE_OUT) ? 'Исходящий акт' : 'Входящий акт' ?>
                            <?php elseif ($document == Documents::SLUG_PACKING_LIST): ?>
                                <?= ($ioType == Documents::IO_TYPE_OUT) ? 'Исходящая ТН' : 'Входящая ТН' ?>
                            <?php elseif ($document == Documents::SLUG_WAYBILL): ?>
                                <?= ($ioType == Documents::IO_TYPE_OUT) ? 'Исходящая ТТН' : 'Входящая ТТН' ?>
                            <?php elseif ($document == Documents::SLUG_INVOICE_FACTURE): ?>
                                <?= ($ioType == Documents::IO_TYPE_OUT) ? 'Исходящая СФ' : 'Входящая СФ' ?>
                            <?php elseif ($document == Documents::SLUG_UPD): ?>
                                <?= ($ioType == Documents::IO_TYPE_OUT) ? 'Исходящий УПД' : 'Входящий УПД' ?>
                            <?php else: ?>
                                <?php if (!$create && $model->isNewRecord && $model->type == Documents::IO_TYPE_OUT): ?>
                                    Исходящий
                                    <div style="display: inline-block;">
                                    <div class="dropdown" style="border-bottom: 1px dashed #ffb848;">
                                        <?= Html::a($model->is_invoice_contract ? 'счет-договор' : 'счет', '#', [
                                            'data-toggle' => 'dropdown',
                                            'class' => 'dropdown-toggle',
                                            'style' => 'text-decoration: none; color: #333;'
                                        ]); ?>
                                        <?= Dropdown::widget([
                                            'options' => [
                                                'style' => 'min-width: auto;',
                                            ],
                                            'id' => 'user-bank-dropdown',
                                            'items' => [
                                                [
                                                    'label' => 'счет',
                                                    'url' => 'javascript:;',
                                                    'linkOptions' => [
                                                        'class' => $model->is_invoice_contract ? null : 'active',
                                                        'id' => 'create-invoice',
                                                    ],
                                                ],
                                                [
                                                    'label' => 'счет-договор',
                                                    'url' => 'javascript:;',
                                                    'linkOptions' => [
                                                        'class' => !$model->is_invoice_contract ? null : 'active',
                                                        'id' => 'create-invoice-contract',
                                                    ],
                                                ],
                                            ],
                                        ]); ?>
                                    </div>
                                </div> №
                                <?php elseif (!$create && $model->type == Documents::IO_TYPE_OUT && $model->is_invoice_contract): ?>
                                    Исходящий счет-договор №
                                <?php else: ?>
                                    <?= $message->get(Message::TITLE_SINGLE); ?> №
                                <?php endif; ?>
                            <?php endif; ?>
                            <?= Html::activeTextInput($model, 'document_number', [
                                'id' => 'account-number',
                                'data-required' => 1,
                                'class' => 'form-control',
                            ]); ?>

                            <br class="box-br">
                        </span>

                        <?php if ($autoinvoice) : ?>
                            <span class="<?= $autoinvoiceBlock ?>">
                                <?php if ($model->isNewRecord && $model->type == Documents::IO_TYPE_OUT): ?>
                                    Шаблон
                                    <div style="display: inline-block;">
                                    <div class="dropdown" style="border-bottom: 1px dashed #333333;">
                                        <?= Html::a($model->is_invoice_contract ? 'автосчёт-договор' : 'автосчёта', '#', [
                                            'data-toggle' => 'dropdown',
                                            'class' => 'dropdown-toggle',
                                            'style' => 'text-decoration: none; color: #ffb848;'
                                        ]); ?>
                                        <?= Dropdown::widget([
                                            'id' => 'user-bank-dropdown',
                                            'items' => [
                                                [
                                                    'label' => 'автосчёта',
                                                    'url' => 'javascript:;',
                                                    'linkOptions' => [
                                                        'class' => $model->is_invoice_contract ? null : 'active',
                                                        'id' => 'create-invoice',
                                                    ],
                                                ],
                                                [
                                                    'label' => 'автосчёт-договора',
                                                    'url' => 'javascript:;',
                                                    'linkOptions' => [
                                                        'class' => !$model->is_invoice_contract ? null : 'active',
                                                        'id' => 'create-invoice-contract',
                                                    ],
                                                ],
                                            ],
                                        ]); ?>
                                    </div>
                                </div> №
                                <?php elseif ($model->type == Documents::IO_TYPE_OUT && $model->is_invoice_contract): ?>
                                    Шаблон автосчёт-договора №
                                <?php else: ?>
                                    Шаблон автосчёта №
                                <?php endif; ?>

                                <?= Html::activeTextInput($autoinvoice, 'document_number', [
                                    'id' => 'account-number',
                                    'data-required' => 1,
                                    'class' => 'form-control',
                                ]); ?>
                                <br class="box-br">
                            </span>
                        <?php endif ?>

                        <?php if ($model->type == Documents::IO_TYPE_OUT || $autoinvoice) : ?>
                            <?= Html::activeTextInput($model, 'document_additional_number', [
                                'maxlength' => true,
                                'id' => 'account-number',
                                'data-required' => 1,
                                'class' => 'form-control',
                                'placeholder' => 'доп. номер',
                            ]); ?>
                        <?php endif ?>
                        <span class="box-margin-top-t">от</span>
                        <div class="input-icon box-input-icon-top"
                             style="display: inline-block; vertical-align: top; margin-left: 6px">
                            <i class="fa fa-calendar"></i>
                            <?= Html::activeTextInput($model, 'document_date', [
                                'id' => 'under-date',
                                'class' => 'form-control date-picker invoice_document_date',
                                'size' => 16,
                                'data-date' => '12-02-2012',
                                'data-date-viewmode' => 'years',
                                'value' => DateHelper::format($model->document_date,
                                    DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                            ]); ?>
                        </div>

                    <?php endif; ?>
                </td>
                <td valign="middle" align="right">
                    <div class="" style="max-width: 628px; padding-left: 30px; text-align: left;">
                        <?php if (\Yii::$app->controller->action->id == 'first-create') : ?>
                            <span class="visible-lg-inline">
                                ГОТОВНОСТЬ
                                <?php if ($document == Documents::SLUG_ACT): ?>
                                    АКТА
                                <?php elseif ($document == Documents::SLUG_PACKING_LIST): ?>
                                    ТН
                                <?php elseif ($document == Documents::SLUG_INVOICE_FACTURE): ?>
                                    СФ
                                <?php elseif ($document == Documents::SLUG_UPD): ?>
                                    УПД
                                <?php else: ?>
                                    СЧЕТА
                                <?php endif; ?>
                                <span class="invoice-ready-value">0</span>
                                %</span>
                        <?php endif ?>
                        <?php if ($model->type == Documents::IO_TYPE_OUT && $model->isNewRecord && !$create && !$document) : ?>
                            <span class="box-margin-top-t tooltip3 pull-right" style="font-size: 14px;cursor: pointer;"
                                  data-tooltip-content="#invoice-contract-tooltip">Нужен счет-договор?</span>
                        <?php endif ?>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>