<?php

use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\widgets\Modal;
use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\documents\components\InvoiceHelper;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */
/* @var $orders common\models\document\Order[] */
/* @var $message Message */
/* @var $ioType integer */
/* @var $multiple [] */
/* @var $useContractor string */

TooltipAsset::register($this);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-right',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'side' => 'right',
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

$company = $model->company;

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'out-invoice out-document';

?>

<div class="pre-view-box" style="min-width: 520px; max-width:735px; margin-top:3px;">
    <div class="col-xs-12 pad5 pre-view-table">
        <div class="col-xs-12 pad3" style="height: 80px;">
            <div class="col-xs-6 pad0 font-bold" style="height: inherit">
                <?= $this->render('/invoice/view/_actions', [
                    'model' => $model,
                    'ioType' => $ioType,
                    'useContractor' => $useContractor,
                ]) ?>

                <div class="col-xs-12 pad0" style="padding-top: 22px !important;">
                    <p style="font-size: 17px"><?= $model->contractor_name_short; ?></p>
                </div>
            </div>
        </div>
        <div class="col-xs-12 pad3">
            <?= $this->render('/invoice/template', [
                'model' => $model,
                'addStamp' => true,
                'showHeader' => false,
            ]); ?>
        </div>
    </div>
</div>
<div class="hidden">
    <span id="unpaid-invoice">
        Что бы отредактировать счет, вам нужно снять оплату.<br>
        Для этого нажмите внизу на кнопку "Снять оплату"
    </span>
</div>
