<?php

use common\components\date\DateHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyType;
use common\models\document\Invoice;
use common\widgets\Modal;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $company Company */
/* @var $account CheckingAccountant */
/* @var $contractor Contractor */
/* @var $message Message */
/* @var $ioType integer */
/* @var $form ActiveForm */

$this->title = 'Создать ' . mb_strtolower($message->get(Message::TITLE_SINGLE));
$this->context->layoutWrapperCssClass = 'create-out-invoice';

$errorModelArray = array_merge([$company, $account, $contractor, $model], $model->orders);
$companyTypes = CompanyType::getCompanyTypeVariants($company->company_type_id);

$this->registerJsFile( '@web/scripts/jquery.autocomplete.js', ['depends' => 'yii\web\JqueryAsset'] );

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>

<style type="text/css">
.delete-column-left {
    display: none;
}
</style>
<div class="first-invoice">
    <?php $form = ActiveForm::begin([
        'id' => 'first-invoice-form',
        'fieldClass' => 'common\components\floatLabelField\FloatLabelActiveField',
        'fieldConfig' => [
            'template' => "{input}\n{label}\n{hint}",
        ],
        'options' => [
            'class' => 'form-md-underline',
            'enctype' => 'multipart/form-data',
            'data' => [
                'iotype' => $ioType,
            ]
        ],
        'enableClientValidation' => false,
    ]); ?>

        <?= $form->errorSummary($errorModelArray); ?>

        <div style="margin: 14px 0 25px;">
            <?= $this->render('first-create/form_main', [
                'form' => $form,
                'company' => $company,
                'account' => $account,
                'contractor' => $contractor,
                'model' => $model,
                'ioType' => $ioType,
            ]) ?>
        </div>

        <label class="control-label-group" style="margin: 0;">Товары (работы, услуги)</label>

        <?= $this->render('form/_product_table_invoice', [
            'form' => $form,
            'company' => $company,
            'account' => $account,
            'contractor' => $contractor,
            'model' => $model,
            'ioType' => $ioType,
        ]) ?>

        <?= $this->render('first-create/form_buttons', [
            'form' => $form,
            'company' => $company,
            'model' => $model,
            'ioType' => $ioType,
        ]) ?>

    <?php ActiveForm::end(); ?>
</div>

<?= $this->render('form/_form_product_new'); ?>

<?= $this->render('form/_form_product_existed', [
    'model' => $model,
]); ?>

<?= $this->render('first-create/tooltip_templates', [
    'model' => $model,
]); ?>

<div id="modal-remove-one-product" class="fade modal" role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none; margin-top: -45px;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body" style="margin-bottom: 15px;text-align: center;font-size: 16px;">
                    <div class="row">Вы уверены, что хотите удалить эту позицию из счета?</div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-6">
                        <button type="button" data-dismiss="modal" class="btn darkblue pull-right yes"
                                style="width: 80px;color: white;">ДА
                        </button>
                    </div>
                    <div class="col-xs-6">
                        <button type="button" data-dismiss="modal" class="btn darkblue"
                                style="width: 80px;color: white;">НЕТ
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->render('_modal_zero_submit', ['form' => 'first-invoice-form']); ?>

<?php $this-> registerJs(<<<JS
$(document).on("click", ".product-title-clear", function(){
    $(this).closest("td").find(".product-title").val("");
    $(this).closest("td").find(".product-title").focus();
});
var checkRemindTooltip = function() {
    if (parseInt($('#invoice-isautoinvoice').val()) == 1) {
        var remindDay = parseInt($('#autoinvoice-payment_delay option:selected').val()) - 1;
        $('#tooltip_remind #when_remind').text('на ' + remindDay + ' день');
    } else {
        var remindDate = moment($('#invoice-payment_limit_date').val(), 'DD.MM.YYYY').subtract(1, 'days');
        if (remindDate.isValid()) {
            $('#tooltip_remind #when_remind').text(remindDate.format('DD.MM.YYYY'));
        }
    }
    $('#tooltip_remind_ico').tooltipster('content', $('#tooltip_remind')[0].outerHTML);
};
checkRemindTooltip();
$(document).on('change','#invoice-payment_limit_date, #autoinvoice-payment_delay',function() {
    checkRemindTooltip();
});
JS
); ?>