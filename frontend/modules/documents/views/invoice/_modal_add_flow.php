<?php

use common\components\date\DateHelper;
use common\models\cash\CashFactory;
use frontend\models\Documents;
use frontend\modules\cash\models\CashBankSearch;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $model \common\models\document\Invoice */
/* @var $useContractor string */

Modal::begin([
    'options' => [
        'id' => 'paid',
        'class' => 'fade',
    ],
    'toggleButton' => false,
]);

echo $this->render('_add_flow_form', [
    'model' => $model,
    'useContractor' => $useContractor,
]);

Modal::end();
