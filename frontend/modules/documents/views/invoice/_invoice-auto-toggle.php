<?php
use frontend\models\Documents;
use yii\helpers\Html;
?>
<span style="position: absolute; right: 0; bottom: 0;border: 1px solid #e5e5e5;">
    <?php if (\Yii::$app->controller->action->id == 'index-auto') : ?>
        <?= Html::a('АвтоСчета', ['index-auto'], [
            'class' => 'btn yellow-text',
            'style' => 'width: 80px; padding: 4px;',
        ]) ?>
        <?= Html::a('Счета', ['index', 'type' => Documents::IO_TYPE_OUT], [
            'class' => 'btn yellow',
            'style' => 'width: 68px; padding: 4px;',
        ]) ?>
    <?php else : ?>
        <?= Html::a('Счета', ['index', 'type' => Documents::IO_TYPE_OUT], [
            'class' => 'btn yellow-text',
            'style' => 'width: 68px; padding: 4px;',
        ]) ?>
        <?= Html::a('АвтоСчета', ['index-auto'], [
            'class' => 'btn yellow',
            'style' => 'width: 80px; padding: 4px;',
        ]) ?>
    <?php endif ?>
</span>