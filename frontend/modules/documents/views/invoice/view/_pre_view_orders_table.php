<?php

use common\components\TextHelper;
use common\models\currency\Currency;
use common\models\product\Product;
use frontend\models\Documents;
use common\models\document\Invoice;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */
/* @var $ioType integer */

$precision = $model->price_precision;
$currCode = $model->currency_name == Currency::DEFAULT_NAME ? '' : " ({$model->currency_name})";
?>

<style>
    .bord-dark tr, .bord-dark td, .bord-dark th {
        border: 1px solid #000000;
    }
</style>

<div class="portlet">
    <table class="bord-dark" style="width: 99.9%; border:2px solid #000000">
        <thead>
        <tr style="text-align: center;">
            <th> №</th>
            <?php if ($model->show_article && Yii::$app->user->identity->config->invoice_form_article) : ?>
                <th> Артикул</th>
            <?php endif ?>
            <th> Товары (работы, услуги)</th>
            <th> Кол-во</th>
            <th> Ед.</th>
            <?php if ($model->has_weight): ?>
                <th> Вес, кг</th>
            <?php endif; ?>
            <?php if ($model->has_volume): ?>
                <th> Объем, м2</th>
            <?php endif; ?>
            <th> Цена</th>
            <?php if ($model->has_discount && !$model->is_hidden_discount) : ?>
                <th>
                    <?= ((YII_ENV_PROD && $model->company_id == 9888 ? 'Агентское вознаграждение' : 'Скидка') .
                        ($model->discount_type == Invoice::DISCOUNT_TYPE_RUBLE ? ' (руб.)' : ' %')); ?>
                </th>
                <th>
                    <?php if (YII_ENV_PROD && $model->company_id == 9888): ?>
                        Цена с агентским вознаграждением
                    <?php else: ?>
                        Цена со скидкой
                    <?php endif; ?>
                </th>
            <?php endif ?>
            <th> Сумма</th>
        </tr>
        </thead>
        <tbody class="bord-dark">
        <?php foreach ($model->orders as $order): ?>
            <?php
            $unitName = $order->unit ? $order->unit->name : Product::DEFAULT_VALUE;
            if ($order->quantity != intval($order->quantity)) {
                $order->quantity = rtrim(number_format($order->quantity, 10, '.', ''), 0);
            } ?>
            <tr>
                <td style="text-align: center; width: 5%"><?= $order->number; ?></td>
                <?php if ($model->show_article && Yii::$app->user->identity->config->invoice_form_article) : ?>
                    <td><?= $order->article; ?></td>
                <?php endif ?>
                <td style=" width: 0">
                    <?= $order->product_title . (
                    $isAuto && $model->auto->add_month_and_year &&
                    $order->product->production_type == Product::PRODUCTION_TYPE_SERVICE ?
                        '<i class="auto_tpl"> за месяц 20##г.</i>' : ''
                    ) ?>
                </td>
                <td style="text-align: right; width: 10%"><?= str_replace('.', ',', $order->quantity); ?></td>
                <td style="text-align: right; width: 7%"><?= $order->unit ? $order->unit->name : Product::DEFAULT_VALUE; ?></td>
                <?php if ($model->has_weight): ?>
                    <td style="text-align: right; width: 7%"><?= ($order->weight) ? TextHelper::invoiceMoneyFormat($order->weight * 100, $model->weightPrecision) : ''; ?></td>
                <?php endif; ?>
                <?php if ($model->has_volume): ?>
                    <td style="text-align: right; width: 7%"><?= ($order->volume) ? TextHelper::invoiceMoneyFormat($order->volume * 100, $model->volumePrecision) : ''; ?></td>
                <?php endif; ?>
                <?php if ($model->has_discount && !$model->is_hidden_discount) : ?>
                    <td style="text-align: right; width: 13%">
                        <?= TextHelper::invoiceMoneyFormat($order->view_price_base, $precision); ?>
                    </td>
                    <td style="text-align: right; width: 13%">
                        <?php if ($model->discount_type == Invoice::DISCOUNT_TYPE_RUBLE): ?>
                            <?= TextHelper::invoiceMoneyFormat($order->discount * 100, 2); ?>
                        <?php else: ?>
                            <?= strtr($order->discount, ['.' => ',']) ?>
                        <?php endif; ?>
                    </td>
                <?php endif ?>
                <td style="text-align: right; width: 13%">
                    <?= TextHelper::invoiceMoneyFormat($order->view_price_one, $precision); ?>
                </td>
                <td style="text-align: right; width: 15%">
                    <?= TextHelper::invoiceMoneyFormat($order->view_total_amount, $precision); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <table class="it-b" style="width: 99.9%;">
        <?php if ($model->has_discount && !$model->is_hidden_discount) : ?>
            <tr>
                <td width="430px" style="border: none"></td>
                <td class="txt-b2" style="text-align: right; border: none; width: 150px;">
                    <?= YII_ENV_PROD && $model->company_id == 9888 ? 'Агентское вознаграждение' : 'Сумма скидки'; ?><?= $currCode ?>
                    :
                </td>
                <td class="txt-b2" style="text-align: right; border: none; width: 124px;">
                    <b><?= TextHelper::invoiceMoneyFormat($model->view_total_discount, 2); ?></b>
                </td>
            </tr>
        <?php endif ?>
        <tr>
            <td width="430px" style="border: none"></td>
            <td class="txt-b2" style="text-align: right; border: none; width: 150px;">
                Итого<?= $currCode ?>:
            </td>
            <td class="txt-b2" style="text-align: right; border: none; width: 124px;">
                <b><?= TextHelper::invoiceMoneyFormat($model->view_total_amount, 2); ?></b>
            </td>
        </tr>
        <tr>
            <td width="430px" style="border: none"></td>
            <td class="txt-b2" style="text-align: right; border: none">
                <?php if ($model->hasNds) : ?>
                    <?php if ($model->nds_view_type_id == Invoice::NDS_VIEW_OUT) : ?>
                        НДС сверху<?= $currCode ?>:
                    <?php else : ?>
                        В том числе НДС<?= $currCode ?>:
                    <?php endif ?>
                <?php else : ?>
                    Без налога (НДС):
                <?php endif ?>
            </td>
            <td class="txt-b2" style="text-align: right; border: none;">
                <b><?= $model->hasNds ? TextHelper::invoiceMoneyFormat($model->view_total_nds, 2) : '-'; ?></b>
            </td>
        </tr>
        <tr>
            <td width="430px" style="border: none"></td>
            <td class="txt-b2" style="text-align: right; border: none">
                Всего к оплате<?= $currCode ?>:
            </td>
            <td class="txt-b2" style="text-align: right; border: none;">
                <b><?= TextHelper::invoiceMoneyFormat($model->view_total_with_nds, 2); ?></b>
            </td>
        </tr>
    </table>
</div>
