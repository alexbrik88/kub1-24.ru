<?php

use common\components\TextHelper;
use common\models\currency\Currency;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */

$currencyName = Html::tag('div', $model->currency_name, [
    'class' => 'currency_name',
    'data' => [
        'initial' => $model->currency_name,
    ],
    'style' => 'display: inline;'
]);
$currencyAmount = Html::tag('span', $model->currency_amount, [
    'class' => 'currency_rate_amount',
    'data' => [
        'initial' => $model->currency_amount,
    ],
]);
$currencyRate = Html::tag('span', $model->currency_rate, [
    'class' => 'currency_rate_value',
    'data' => [
        'initial' => $model->currency_rate,
    ],
]);
\common\components\floatLabelField\FloatLabelAsset::register(Yii::$app->getView());
$radioName = Html::getInputName($model, 'currency_rate_type');
$rateText = "{$currencyAmount} {$currencyName} = {$currencyRate} RUB";
?>
    <div>
        <span class="customer-characteristic">Обменный курс:</span>
        <?= Html::A($rateText, null, [
            'class' => 'collapse-toggle-link invoice-panel-trigger',
            'style' => 'position: relative;',
        ]); ?>
    </div>
    <div>
        <span class="customer-characteristic">Сумма счета в RUB:</span>
        <span class="total_amount_rub">
            <?= TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2); ?>
        </span>
    </div>
    <div class="invoice-panel">
        <div class="main-block">
            <span class="header">Установить <br> обменный курс</span>

            <div class="field-invoice-currency_rate_type">
                <?= Html::activeHiddenInput($model, 'currency_name', [
                    'data' => [
                        'initial' => $model->currency_name,
                    ],
                ]); ?>
                <?= Html::activeHiddenInput($model, 'currency_amount', [
                    'class' => 'currency_amount currency_rate_amount',
                    'data' => [
                        'initial' => $model->currency_amount,
                    ],
                ]); ?>
                <?= Html::activeHiddenInput($model, 'currency_rate', [
                    'class' => 'currency_rate currency_rate_value',
                    'data' => [
                        'initial' => $model->currency_rate,
                    ],
                ]); ?>
                <?= Html::hiddenInput($radioName, '') ?>
                <?= Html::beginTag('div', [
                    'id' => Html::getInputId($model, 'currency_rate_type'),
                    'class' => 'row form-md-underline',
                    'data' => [
                        'geturl' => Url::to(['/site/currency-rate']),
                        'seturl' => Url::to(['/documents/invoice/set-currency-rate', 'type' => $model->type, 'id' => $model->id]),
                    ],
                    'style' => 'padding-left: 25px;padding-right: 15px;padding-top: 40px;',
                ]); ?>
                <div class="md-radio-list" style="margin: 0;">
                    <div class="md-radio" style="width: 100%;display: inline-block;margin-bottom: 25px;
                    border-bottom: 1px solid #e4e4e4;"></div>
                    <div class="md-radio" style="width: 100%;display: inline-block; margin: 0 20px 0 0;
                    border-bottom: 1px solid #e4e4e4;padding-bottom: 25px;">
                        <?= Html::radio($radioName, $model->currency_rate_type == Currency::RATE_SERTAIN_DATE, [
                            'id' => 'radio_currency_rate_ondate',
                            'value' => Currency::RATE_SERTAIN_DATE,
                            'class' => 'currency-rate-type-radio md-radiobtn',
                            'data' => [
                                'initial' => $model->currency_rate_type == Currency::RATE_SERTAIN_DATE,
                            ],
                        ]) ?>
                        <label for="radio_currency_rate_ondate" style="margin: 0;font-size: 15px;">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span>
                            <?= Html::activeHiddenInput($model, 'currency_rate_amount', [
                                'class' => 'currency_rate_amount',
                                'data' => [
                                    'initial' => $model->currency_rate_amount,
                                ],
                            ]) ?>
                            <?= Html::activeHiddenInput($model, 'currency_rate_value', [
                                'class' => 'currency_rate_value',
                                'data' => [
                                    'initial' => $model->currency_rate_value,
                                ],
                            ]) ?>
                            <a href="http://www.cbr.ru" target="_blank">Курс ЦБ РФ</a> на
                            <?= Html::activeTextInput($model, 'currency_rateDate', [
                                'class' => 'invoice-currency_ratedate currency_rate_date',
                                'data' => [
                                    'default' => $model->document_date,
                                    'initial' => $model->currency_rateDate,
                                ],
                                'style' => 'font-size: 15px;',
                                'readonly' => true,
                            ]) ?>:
                            <?= Html::tag('div', $model->currency_rate_amount, [
                                'class' => 'currency_rate_amount',
                                'data' => [
                                    'initial' => $model->currency_rate_amount,
                                ],
                                'style' => 'display: inline;',
                            ]); ?>
                            <?= $currencyName ?> =
                            <?= Html::tag('div', $model->currency_rate_value, [
                                'class' => 'currency_rate_value',
                                'data' => [
                                    'initial' => $model->currency_rate_value,
                                ],
                                'style' => 'display: inline;',
                            ]) ?>
                            RUB
                        </label>
                    </div>
                    <div class="md-radio" style="width: 100%;display: inline-block; margin: 25px 20px 0 0;
                    border-bottom: 1px solid #e4e4e4;padding-bottom: 25px;">
                        <?= Html::radio($radioName, $model->currency_rate_type == Currency::RATE_PAYMENT_DATE, [
                            'id' => 'radio_currency_rate_onpayment',
                            'value' => Currency::RATE_PAYMENT_DATE,
                            'class' => 'currency-rate-type-radio md-radiobtn',
                            'data' => [
                                'initial' => $model->currency_rate_type == Currency::RATE_PAYMENT_DATE,
                            ],
                        ]) ?>
                        <label for="radio_currency_rate_onpayment" style="margin: 0;font-size: 15px;">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span>
                            <a href="http://www.cbr.ru" target="_blank">Курс ЦБ РФ</a> на день оплаты.
                            (До оплаты счет учитывается по курсу на <?= Html::tag('div', $model->currency_rateDate, [
                                'class' => 'currency_rate_date',
                                'data' => [
                                    'initial' => $model->currency_rateDate,
                                ],
                                'style' => 'display: inline;',
                            ]) ?>, при оплате пересчитывается).
                        </label>
                    </div>
                    <div class="md-radio"
                         style="width: 100%;display: inline-block; margin: 25px 20px 0 0;border-bottom: 1px solid #e4e4e4;padding-bottom: 25px;">
                        <?= Html::radio($radioName, $model->currency_rate_type == Currency::RATE_CUSTOM, [
                            'id' => 'radio_currency_rate_custom',
                            'value' => Currency::RATE_CUSTOM,
                            'class' => 'currency-rate-type-radio md-radiobtn',
                            'data' => [
                                'initial' => $model->currency_rate_type == Currency::RATE_CUSTOM,
                            ],
                        ]) ?>
                        <label for="radio_currency_rate_custom" style="margin: 0;font-size: 15px;">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span>
                            Установить другой обменный курс:<br>
                            <?= Html::activeTextInput($model, 'currencyAmountCustom', [
                                'value' => $model->currency_amount,
                                'class' => 'currency-custom-input currency_rate_amount',
                                'style' => 'width: 50px;text-align: center;font-size: 15px;',
                                'disabled' => $model->currency_rate_type != Currency::RATE_CUSTOM,
                                'data' => [
                                    'initial' => $model->currency_amount,
                                ],
                            ]) ?>
                            <?= $currencyName ?>
                            =
                            <?= Html::activeTextInput($model, 'currencyRateCustom', [
                                'value' => $model->currency_rate,
                                'class' => 'currency-custom-input currency_rate_value',
                                'disabled' => $model->currency_rate_type != Currency::RATE_CUSTOM,
                                'style' => 'text-align: center;font-size: 15px;',
                                'data' => [
                                    'initial' => $model->currency_rate,
                                ],
                            ]) ?>
                            RUB
                        </label>
                    </div>
                </div>
                <?= Html::endTag('div'); ?>
                <div class="form-actions" style="margin-top: 36%;">
                    <div class="row action-buttons">
                        <div class="col-sm-5 col-xs-5" style="width: 27.5%;">
                            <?= Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                                'id' => 'currency_rate_apply',
                                'class' => 'btn darkblue text-white mt-ladda-btn ladda-button widthe-100',
                                'data-style' => 'expand-right',
                            ]); ?>
                        </div>
                        <div class="col-sm-2 col-xs-2" style="width: 45%;"></div>
                        <div class="col-sm-5 col-xs-5" style="width: 27.5%;">
                            <?= Html::button('Отменить', [
                                'id' => 'currency_rate_cancel',
                                'class' => 'btn darkblue text-white pull-right widthe-100',
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <span class="side-panel-close" title="Закрыть">
        <span class="side-panel-close-inner"></span>
    </span>
    </div>
    <div id="visible-right-menu" style="display: none;">
        <div id="visible-right-menu-wrapper"></div>
    </div>
<?php $this->registerJs('
$(document).on("click", ".invoice-panel-trigger", function () {
    $(".invoice-panel").toggle("fast");
    $(this).toggleClass("active");
    $("#visible-right-menu").show();
    $("html").attr("style", "overflow: hidden;");

    return false;
});
$(document).on("click", "#visible-right-menu", function () {
    $(".invoice-panel").toggle("fast");
    $(".invoice-panel-trigger").toggleClass("active");
    $(this).hide();
    $("html").removeAttr("style");
});
$(document).on("click", ".invoice-panel .side-panel-close", function () {
    $(".invoice-panel").toggle("fast");
    $(".invoice-panel-trigger").toggleClass("active");
    $("#visible-right-menu").hide();
    $("html").removeAttr("style");
});
$("#invoice-currency_ratedate").datepicker({
    format: "dd.mm.yyyy",
    language:"ru",
    autoclose: true,
    endDate: new Date(),
});
');