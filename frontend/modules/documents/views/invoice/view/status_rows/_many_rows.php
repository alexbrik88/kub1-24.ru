<?php

/**
 * @var $model \common\models\document\Invoice
 * @var $document string
 * @var $title string
 */

use common\components\TextHelper;
use common\models\document\status\ActStatus;
use common\models\document\status\PackingListStatus;
use common\models\document\status\WaybillStatus;
use common\models\document\status\InvoiceFactureStatus;
use common\models\product\Product;
use frontend\models\Documents;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\components\date\DateHelper;
use common\models\document\status\ProxyStatus;

$iconClass = 'icon-doc';
if ($document == 'act') {
    $docQuery = $model->getAct();
    $iconClass = ActStatus::getStatusIcon($model->getAct()->min('status_out_id'));
} elseif ($document == 'packing-list') {
    $docQuery = $model->getPackingList();
    $iconClass = PackingListStatus::getStatusIcon($model->getPackingList()->min('status_out_id'));
} elseif ($document == 'proxy') {
    $docQuery = $model->getProxy();
    $iconClass = ProxyStatus::getStatusIcon($model->getProxy()->min('status_out_id'));
} elseif ($document == 'waybill') {
    $docQuery = $model->getWaybill();
    $iconClass = WaybillStatus::getStatusIcon($model->getWaybill()->min('status_out_id'));
} elseif ($document == 'invoice-facture') {
    $docQuery = $model->getInvoiceFactures();
    $iconClass = InvoiceFactureStatus::getStatusIcon($model->getInvoiceFactures()->min('status_out_id'));
} elseif ($document == 'upd') {
    $docQuery = $model->getUpds();
}

$docArray = !empty($docQuery) ? $docQuery->orderBy('document_number DESC')->all() : [];

$sum = $docArray ? array_sum(ArrayHelper::getColumn($docArray, 'totalAmountWithNds')) : 0;
?>
<div class="clearfix" style="display: block; position: relative;">
    <div class="col-xs-7 pad0" style="padding-right: 3px !important;">
        <a data-toggle="dropdown"
           class="btn yellow dropdown-toggle tooltipstered<?= $model->isRejected ? ' disabled' : ''; ?>"
            <?= $model->isRejected ? ' disabled="disabled"' : ''; ?>
           style="text-transform: uppercase;width: 100%; height: 34px;margin-left: 0;padding-right: 14px; padding-left: 14px;<?= $model->isRejected ? ' background-color: #a2a2a2;' : ''; ?>"
           title="<?= mb_convert_case(($addAvailable ? 'Добавить ' : '') . $title, MB_CASE_TITLE) ?>">
            <i class="pull-left icon <?= $iconClass; ?>"></i>
            <?= $title ?>
        </a>
        <ul class="dropdown-menu documents-dropdown">
            <?php if ($addAvailable) { ?>
                <li>
                    <?= Html::a(
                        '[+Добавить ' . $title . ']',
                        [$document . '/create', 'type' => $model->type, 'invoiceId' => $model->id],
                        ['options' => ['class' => 'dropdown-new']]
                    ); ?>
                </li>
            <?php }

            foreach ($docArray as $list_item) { ?>
                <li>
                    <a class="dropdown-item" style="position: relative;"
                       href="<?= Url::to([$document . '/view', 'type' => $model->type, 'id' => $list_item->id]) ?>">
                    <span class="dropdown-span-left">
                        № <?= $list_item->document_number ?>
                        от <?= DateHelper::format($list_item->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                    </span>
                        <?php if ($document == 'act') { ?>
                            <span class="dropdown-span-right"><i
                                        class="fa fa-rub"></i><?= TextHelper::invoiceMoneyFormat($list_item->totalAmountWithNds, 2) ?></span>
                        <?php } else { ?>
                            <span class="dropdown-span-right"><i
                                        class="fa fa-rub"></i><?= TextHelper::invoiceMoneyFormat($list_item->totalAmountWithNds, 2) ?></span>
                        <?php } ?>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>
    <div class="col-xs-5 pad0" style="padding-left: 3px !important;">
        <a class="btn yellow" style="width: 100%; <?= $model->isRejected ? ' background-color: #a2a2a2;' : ''; ?>"
            <?= $model->isRejected ? ' disabled="disabled"' : ''; ?>
           title="Сумма текущих <?= $title ?>">
            <i class="fa fa-rub"></i> <?= TextHelper::invoiceMoneyFormat($sum, 2); ?>
        </a>
    </div>
</div>
