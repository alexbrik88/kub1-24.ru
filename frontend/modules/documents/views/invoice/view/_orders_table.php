<?php
use common\components\TextHelper;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\product\Product;
use frontend\models\Documents;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */
/* @var $ioType integer */

$isIncoming = ($model->type == Documents::IO_TYPE_IN);
$hasNds = $model->hasNds;
$isNdsExclude = $model->nds_view_type_id == Invoice::NDS_VIEW_OUT;
$precision = $model->price_precision;
$currCode = $model->currency_name == Currency::DEFAULT_NAME ? '' : " ({$model->currency_name})";
?>

<?php if ($model->type == Documents::IO_TYPE_OUT && !$hasNds) : ?>
<style type="text/css">
#table-for-invoice-view tr > th:nth-child(5),
#table-for-invoice-view tr > td:nth-child(5) {
    display: none;
}
</style>
<?php endif ?>

<div class="portlet overl-auto">
    <table class="table table-striped table-bordered table-hover" id="table-for-invoice-view">
        <thead>
            <tr class="heading" role="row">
                <th width="5%" tabindex="0" aria-controls="datatable_ajax">
                    №
                </th>
                <th width="10%" tabindex="0" aria-controls="datatable_ajax">
                    Артикул
                </th>
                <th width="20%" tabindex="0" aria-controls="datatable_ajax">
                    Наименование
                </th>
                <th width="10%" class="" tabindex="0" aria-controls="datatable_ajax">
                    Количество
                </th>
                <th width="5%" class="" tabindex="0" aria-controls="datatable_ajax">
                    Ед.измерения
                </th>
                <th width="5%" class="" tabindex="0" aria-controls="datatable_ajax">
                    НДС
                </th>
                <th width="15%" class="" tabindex="0" aria-controls="datatable_ajax">
                    Цена
                </th>
                <?php if ($model->has_discount) : ?>
                    <th width="10%" class="" tabindex="0" aria-controls="datatable_ajax">
                        Скидка <?= $model->discount_type == Invoice::DISCOUNT_TYPE_RUBLE ? '(руб.)' : '%' ?>
                    </th>
                    <th width="15%" class="" tabindex="0" aria-controls="datatable_ajax">
                        Цена со скидкой
                    </th>
                <?php endif ?>
                <th width="15%" class="" tabindex="0" aria-controls="datatable_ajax">
                    Сумма
                </th>
            </tr>
        </thead>
        <tbody>

        <?php foreach ($model->orders as $order): ?>
            <?php
            $unitName = $order->unit ? $order->unit->name : Product::DEFAULT_VALUE;
            if ($model->type == Documents::IO_TYPE_IN) {
                if ($isNdsExclude) {
                    $priceOne = $model->toCurrency($order->purchase_price_no_vat);
                    $amount = $model->toCurrency($order->amount_purchase_no_vat);
                } else {
                    $priceOne = $model->toCurrency($order->purchase_price_with_vat);
                    $amount = $model->toCurrency($order->amount_purchase_with_vat);
                }
            } else {
                if ($isNdsExclude) {
                    $priceOne = $model->toCurrency($order->selling_price_no_vat);
                    $amount = $model->toCurrency($order->amount_sales_no_vat);
                } else {
                    $priceOne = $model->toCurrency($order->selling_price_with_vat);
                    $amount = $model->toCurrency($order->amount_sales_with_vat);
                }
            }
            ?>
            <tr role="row" class="odd">
                <td><?= $order->number; ?></td>
                <td><?= $order->article; ?></td>
                <td title="<?= $order->product_title; ?>">
                    <?= mb_strlen($order->product_title) > 120 ?
                        mb_substr($order->product_title, 0, 120) . '...':
                        $order->product_title; ?>
                </td>
                <td><?= $unitName == Product::DEFAULT_VALUE ? $unitName : str_replace('.', ',', $order->quantity) ?></td>
                <td><?= $unitName ?></td>
                <td>
                    <?php if ($hasNds) : ?>
                        <?= $isIncoming ? $order->purchaseTaxRate->name : $order->saleTaxRate->name; ?>
                    <?php else : ?>
                        Без НДС
                    <?php endif; ?>
                </td>

                <?php if ($model->has_discount) : ?>
                    <td>
                        <?= TextHelper::invoiceMoneyFormat($order->view_price_base, $precision); ?>
                    </td>
                    <td>
                        <?= $order->getDiscountViewValue() ?>
                    </td>
                <?php endif ?>
                <td>
                    <?= TextHelper::invoiceMoneyFormat($order->view_price_one, $precision); ?>
                </td>
                <td style="text-align: right;">
                    <?= TextHelper::invoiceMoneyFormat($order->view_total_amount, $precision); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="portlet pull-right m-t-n-10">
    <table class="table table-resume">
        <tbody>
            <?php if ($model->has_discount) : ?>
                <tr role="row" class="odd">
                    <td><b>Сумма скидки<?= $currCode ?>:</b></td>
                    <td><?= TextHelper::invoiceMoneyFormat($model->view_total_discount, 2); ?></td>
                </tr>
            <?php endif ?>
            <tr role="row" class="even">
                <td><b>Итого<?= $currCode ?>:</b></td>
                <td><?= TextHelper::invoiceMoneyFormat($model->view_total_amount, 2); ?></td>
            </tr>
            <tr role="row" class="odd">
                <td>
                    <b>
                        <?= $model->ndsViewType->title ?>
                    </b>
                </td>
                <td><?= $hasNds ? TextHelper::invoiceMoneyFormat($model->view_total_nds, 2) : '-'; ?></td>
            </tr>
            <tr role="row" class="even">
                <td><b>Всего к оплате<?= $currCode ?>:</b></td>
                <td><?= TextHelper::invoiceMoneyFormat($model->view_total_with_nds, 2); ?></td>
            </tr>
        </tbody>
    </table>
</div>