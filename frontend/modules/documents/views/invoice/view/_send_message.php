<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.08.2018
 * Time: 13:35
 */

use common\components\helpers\Html;
use frontend\modules\documents\forms\InvoiceSendForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use common\models\Contractor;
use common\models\document\Invoice;
use faryshta\widgets\JqueryTagsInput;
use common\models\company\CompanyType;
use common\models\document\EmailTemplate;
use common\components\helpers\ArrayHelper;
use yii\web\JsExpression;
use yii\web\View;
use common\models\document\Act;
use common\models\document\PackingList;
use common\models\document\Waybill;
use common\models\document\InvoiceFacture;
use common\models\document\Upd;
use common\models\document\OrderDocument;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use common\models\product\PriceList;
use common\models\employee\EmployeeRole;
use common\models\Agreement;
use common\models\document\AgentReport;
use common\models\driver\Driver;
use common\models\vehicle\Vehicle;
use common\models\logisticsRequest\LogisticsRequest;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice|\common\models\document\Act */
/* @var $user \common\models\employee\Employee */
/* @var $useContractor string */
/* @var $showSendPopup integer */
/* @var $requestType integer */

$user = Yii::$app->user->identity;
$employeeCompany = $user->currentEmployeeCompany;
$sendForm = new InvoiceSendForm($user->currentEmployeeCompany);
$emailTemplates = $user->company->getEmailTemplates($user->id);
$emailTemplatesArray = ArrayHelper::merge([0 => 'Стандартный'], ArrayHelper::map(EmailTemplate::find()->all(), 'id', 'name'));
$activeEmailTemplate = false; //$user->company->getActiveEmailTemplate($user->id);
$emailTemplatesCount = count($emailTemplates);
$emailSignature = $model->company->getEmailSignature($user->id);
$headerText = null;
$viewDocumentButtonText = null;
$emailPermanentText = null;
$requestType = isset($requestType) ? $requestType : null;

if ($model instanceof Invoice) {
    $headerText = 'счета';
    $viewDocumentButtonText = 'Открыть ' . ($model->is_invoice_contract ? 'счет-договор' : 'счет');
    $contractor = $model->contractor;
    $sendForm->textRequired = true;
    $sendForm->emailText = $model->emailText;
    $emailPermanentText = $model->getEmailPermanentText();
    if ($activeEmailTemplate) {
        $sendForm->emailText = $activeEmailTemplate->text;
    }
} elseif ($model instanceof Contractor) {
    $headerText = 'Счета';
    $contractor = $model;
} elseif ($model instanceof Agreement) {
    $headerText = 'договора';
    $viewDocumentButtonText = null;
    $contractor = $model->contractor;
    $sendForm->textRequired = true;
    $sendForm->emailText = $model->emailText;
} elseif ($model instanceof PriceList) {
    $sendForm->textRequired = true;
    $sendForm->emailText = $model->emailText;
    $headerText = 'прайс-листа';
    $viewDocumentButtonText = 'Открыть прайс-лист';
} elseif ($model instanceof AgentReport) {
    $sendForm->textRequired = true;
    $sendForm->emailText = $model->emailText;
    $contractor = $model->agent;
    $headerText = 'Отчета';
    $viewDocumentButtonText = null;
} elseif ($model instanceof OrderDocument) {
    $sendForm->textRequired = true;
    $sendForm->emailText = $model->emailText;
    $contractor = $model->contractor;
    $headerText = 'Заказа';
    $viewDocumentButtonText = 'Открыть заказ';
} elseif ($model instanceof Driver) {
    $sendForm->textRequired = true;
    $sendForm->emailText = $model->emailText;
    $contractor = $model->contractor;
    $headerText = 'водителя';
    $viewDocumentButtonText = null;
} elseif ($model instanceof Vehicle) {
    $sendForm->textRequired = true;
    $sendForm->emailText = $model->emailText;
    $contractor = $model->contractor;
    $headerText = 'транспортное средство';
    $viewDocumentButtonText = null;
} elseif ($model instanceof LogisticsRequest) {
    $sendForm->textRequired = true;
    $sendForm->emailText = $model->getEmailText($requestType);
    $headerText = 'Договора-заявки';
    $viewDocumentButtonText = 'Открыть заявку';
    if ($requestType == LogisticsRequest::TYPE_CUSTOMER) {
        $contractor = $model->customer;
        if ($model->customerAgreement) {
            $headerText = 'Заявки';
        }
        $headerText .= ' с Заказчиком';
    } else {
        $contractor = $model->carrier;
        if ($model->carrierAgreement) {
            $headerText = 'Заявки';
        }
        $headerText .= ' с Перевозчиком';
    }
} else {
    $contractor = $model->invoice->contractor;
    if ($model instanceof Act) {
        $sendForm->textRequired = true;
        $sendForm->emailText = $model->emailText;
        $headerText = 'акта';
        $viewDocumentButtonText = 'Открыть акт';
    } elseif ($model instanceof PackingList) {
        $sendForm->textRequired = true;
        $sendForm->emailText = $model->emailText;
        $headerText = 'товарной накладной';
        $viewDocumentButtonText = 'Открыть товарную накладную';
    } elseif ($model instanceof Waybill) {
        $sendForm->textRequired = true;
        $sendForm->emailText = $model->emailText;
        $headerText = 'товарно-транспортной накладной';
        $viewDocumentButtonText = 'Открыть товарно-транспортную накладную';
    } elseif ($model instanceof InvoiceFacture) {
        $sendForm->textRequired = true;
        $sendForm->emailText = $model->emailText;
        $headerText = 'счет-фактуры';
        $viewDocumentButtonText = 'Открыть счет фактуру';
    } elseif ($model instanceof Upd) {
        $sendForm->textRequired = true;
        $sendForm->emailText = $model->emailText;
        $headerText = 'УПД';
        $viewDocumentButtonText = 'Открыть УПД';
    }
}
if (!($model instanceof PriceList) && !($model instanceof Driver) && !($model instanceof Vehicle)) {
    $directorLabel = '<span class="email-label">' . $contractor->director_name .
        ($contractor->director_email ?
            (' (Руководитель)<br><span style="padding-left: 25px">' . $contractor->director_email . '</span>') :
            null) .
        '</span>';
    $chiefAccountantLabel = '<span class="email-label">' . $contractor->chief_accountant_name .
        ($contractor->chief_accountant_email ?
            (' (Главный бухгалтер)<br><span style="padding-left: 25px">' . $contractor->chief_accountant_email . '</span>') :
            null) .
        '</span>';
    $contactLabel = '<span class="email-label">' . $contractor->contact_name .
        ($contractor->contact_email ?
            (' (Контакт)<br><span style="padding-left: 25px">' . $contractor->contact_email . '</span>') :
            null) .
        '</span>';
}
if (isset($emailSignature) && $emailSignature->text) {
    $signature = $emailSignature->text;
} else {
    $employeeRole = $user->company->company_type_id != CompanyType::TYPE_IP ? $employeeCompany->position : null;
    $signature = <<<EMAIL_TEXT
С уважением,
{$employeeCompany->getFio(true)}
{$employeeRole}
EMAIL_TEXT;
}

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-message',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub', 'tooltipster-message-from'],
        'trigger' => 'hover',
        'side' => 'left',
        'zIndex' => 10000000,
    ],
]);
?>
    <style>
        .tooltipster-sidetip.tooltipster-noir.tooltipster-noir-customized .tooltipster-arrow-border {
            border-top-color: #fff;
        }
    </style>
    <div class="page-shading-panel hidden"></div>
    <div class="send-message-panel">
        <?php $form = ActiveForm::begin([
            'id' => 'send-document-form',
            'action' => $model instanceof Contractor ?
                Url::to(['/documents/invoice/many-send-by-contractor', 'type' => $model->type, 'contractor' => $model->id]) :
                ($model instanceof AgentReport ?
                    Url::to(['/documents/agent-report/send', 'type' => $model->type, 'id' => $model->id]) :
                    ($model instanceof PriceList ?
                        Url::to(['/price-list/send', 'productionType' => $model->production_type, 'id' => $model->id]) :
                        ($model instanceof Driver ?
                            Url::to(['/logistics/driver/send', 'id' => $model->id]) :
                            ($model instanceof Vehicle ?
                                Url::to(['/logistics/vehicle/send', 'id' => $model->id]) :
                                ($model instanceof LogisticsRequest ?
                                    Url::to(['/logistics/request/send', 'id' => $model->id, 'type' => $requestType]) :
                                    Url::to(['send', 'type' => $model->type, 'id' => $model->id, 'contractorId' => ($useContractor ? $contractor->id : null),])))))),
            'options' => [
                'name' => 'send-document-form',
            ],
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'validateOnBlur' => false,
            'validateOnType' => false,
            'validateOnChange' => false,
            'validateOnSubmit' => true,
        ]); ?>
        <div class="main-block" style="height: 100%;">
            <span class="header">
                <?= $header ?? "Отправка $headerText покупателю"; ?>
            </span>
            <div class="block-from-email tooltip-message" data-tooltip-content="#send-from-tooltip">
                <div class="email-label">От кого:</div>
                <div class="form-group form-md-line-input form-md-floating-label field-message-from"
                     style="width: 85%;">
                    <?= Html::textInput(null, $employeeCompany->getFio() . ' (' . $user->email . ')', [
                        'class' => 'form-control input-sm edited',
                        'id' => 'message-from',
                        'aria-required' => true,
                        'readonly' => true,
                    ]); ?>
                </div>
            </div>
            <div class="block-to-email">
                <div class="email-label">Кому:</div>
                <div class="dropdown-email">
                    <?= $form->field($sendForm, 'sendTo', [
                        'options' => [
                            'class' => 'form-group form-md-line-input form-md-floating-label field-message-to',
                            'style' => 'width: 96%;',
                        ],
                    ])->widget(JqueryTagsInput::className(), [
                        'clientOptions' => [
                            'width' => '100%',
                            'defaultText' => '',
                            'removeWithBackspace' => true,
                            'onAddTag' => new JsExpression("function (val) {
                            var tagInput = $('#invoicesendform-sendto');
                            var reg = /^([A-Za-zА-Яа-я0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,15})$/;
                            if (reg.test(val) == false) {
                                tagInput.removeTag(val);
                            } else {
                                $('.who-send-container .container-who-send-label').each(function () {
                                    var sendToCheckBox = $(this).find('.checker input');
                                    if (sendToCheckBox.data('value') == val) {
                                        if (!sendToCheckBox.is(':checked')) {
                                            sendToCheckBox.click();
                                        }
                                    }
                                });
                            }
                            if ($('#invoicesendform-sendto').val() == '') {
                                $('#invoicesendform-sendto_tagsinput #invoicesendform-sendto_tag')
                                .attr('placeholder', 'Укажите e-mail или выберите из списка')
                                .addClass('visible');
                            } else {
                                $('#invoicesendform-sendto_tagsinput #invoicesendform-sendto_tag')
                                .removeAttr('placeholder')
                                .removeClass('visible');
                            }
                        }"),
                            'onRemoveTag' => new JsExpression("function (val) {
                            $('.who-send-container .container-who-send-label').each(function () {
                                sendToCheckBox = $(this).find('.checker input');
                                if (sendToCheckBox.data('value') == val) {
                                    if (sendToCheckBox.is(':checked')) {
                                        sendToCheckBox.click();
                                    }
                                }
                            });
                            if ($('#invoicesendform-sendto').val() == '') {
                                $('#invoicesendform-sendto_tagsinput #invoicesendform-sendto_tag')
                                .attr('placeholder', 'Укажите e-mail или выберите из списка')
                                .addClass('visible');
                            } else {
                                $('#invoicesendform-sendto_tagsinput #invoicesendform-sendto_tag')
                                .removeAttr('placeholder')
                                .removeClass('visible');
                            }
                        }"),
                        ],
                    ])->label(false); ?>
                    <span class="arrow"></span>
                    <ul id="user-email-dropdown">
                        <?php if (!($model instanceof PriceList) && !($model instanceof Driver) &&
                            !($model instanceof Vehicle)): ?>
                            <li style="padding-top: 5px;height: 290px;overflow-y: auto;">
                                <?php if (!$contractor->chief_accountant_is_director && !$contractor->contact_is_director): ?>
                                    <div class="row who-send-container all">
                                        <div class="container-who-send-label">
                                            <?= $form->field($sendForm, 'sendToAll', [
                                                'options' => [
                                                    'class' => 'form-group',
                                                    'style' => 'margin-bottom: 0;',
                                                ],
                                                'template' => "{label}\n{input}",
                                            ])->checkbox([
                                                'label' => 'Выбрать всех',
                                                'labelOptions' => [
                                                    'style' => 'font-size: 13px; font-weight: bold;',
                                                ],
                                                'class' => 'check-all-recipients',
                                            ]); ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <div class="row who-send-container add-contractor-email">
                                    <div class="container-who-send-label"
                                         style="<?= $contractor->director_email ? 'width: 100%!important;' : null; ?>">
                                        <?= $form->field($sendForm, 'sendToChief', ['template' => "{label}\n{input}"])->checkbox([
                                            'label' => $directorLabel,
                                            'labelOptions' => [
                                                'style' => 'font-size: 13px;',
                                            ],
                                            'disabled' => !(bool)$contractor->director_email,
                                            'data-value' => $contractor->director_email,
                                            'data-url' => Url::to(['/email/set-employee-email', 'contractorID' => $contractor->id]),
                                        ]); ?>
                                    </div>
                                    <?php if (!$contractor->director_email): ?>
                                        <div class="container-who-send-input chief-email">
                                            <?= $form->field($sendForm, 'chiefEmail', [
                                                'options' => [
                                                    'class' => 'col-lg-12 col-md-12 col-xs-12 pad-right-none pad-left-none',
                                                ],
                                                'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                                            ])->label(false)->textInput([
                                                'placeHolder' => 'Укажите e-mail',
                                                'class' => 'form-control',
                                            ]); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <?php if (!$contractor->chief_accountant_is_director): ?>
                                    <div class="row who-send-container add-contractor-email">
                                        <div class="container-who-send-label"
                                             style="<?= $contractor->chief_accountant_email ? 'width: 100%!important;' : null; ?>">
                                            <?= $form->field($sendForm, 'sendToChiefAccountant', ['template' => "{label}\n{input}"])->checkbox([
                                                'label' => $chiefAccountantLabel,
                                                'labelOptions' => [
                                                    'style' => 'font-size: 13px;',
                                                ],
                                                'disabled' => !(bool)$contractor->chief_accountant_email,
                                                'data-value' => $contractor->chief_accountant_email,
                                                'data-url' => Url::to(['/email/set-employee-email', 'contractorID' => $contractor->id]),
                                            ]); ?>
                                        </div>
                                        <?php if (!$contractor->chief_accountant_email): ?>
                                            <div class="container-who-send-input chief-email"
                                                 style="<?= $contractor->chief_accountant_email ? 'margin-top: 10px;' : null; ?>">
                                                <?= $form->field($sendForm, 'chiefAccountantEmail', [
                                                    'options' => [
                                                        'class' => 'col-lg-12 col-md-12 col-xs-12 pad-right-none pad-left-none',
                                                    ],
                                                    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                                                ])->label(false)->textInput([
                                                    'placeHolder' => 'Укажите e-mail',
                                                    'class' => 'form-control width100',
                                                ]); ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (!$contractor->contact_is_director): ?>
                                    <div class="row who-send-container add-contractor-email">
                                        <div class="container-who-send-label"
                                             style="<?= $contractor->contact_email ? 'width: 100%!important;' : null; ?>">
                                            <?= $form->field($sendForm, 'sendToContact', ['template' => "{label}\n{input}"])->checkbox([
                                                'label' => $contactLabel,
                                                'labelOptions' => [
                                                    'style' => 'font-size: 13px;',
                                                ],
                                                'disabled' => !(bool)$contractor->contact_email,
                                                'data-value' => $contractor->contact_email,
                                                'data-url' => Url::to(['/email/set-employee-email', 'contractorID' => $contractor->id]),
                                            ]); ?>
                                        </div>
                                        <?php if (!$contractor->contact_email): ?>
                                            <div class="container-who-send-input chief-email"
                                                 style="<?= $contractor->contact_email ? 'margin-top: 10px;' : null; ?>">
                                                <?= $form->field($sendForm, 'contactAccountantEmail', [
                                                    'options' => [
                                                        'class' => 'col-lg-12 col-md-12 col-xs-12 pad-right-none pad-left-none',
                                                    ],
                                                    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                                                ])->label(false)->textInput([
                                                    'placeHolder' => 'Укажите e-mail',
                                                    'class' => 'form-control width100',
                                                ]); ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                                <?php foreach ($contractor->userEmails as $userEmail): ?>
                                    <div class="row who-send-container">
                                        <div class="container-who-send-label" style="width: 100%!important;">
                                            <div class="form-group" style="margin-bottom: 0;">
                                                <div class="checkbox" style="display: inline-block;width: 81%;">
                                                    <?= Html::checkbox('InvoiceSendForm[sendToUser][' . $userEmail->id . ']', false, [
                                                        'id' => 'invoicesendform-sendtouser',
                                                        'data-value' => $userEmail->email,
                                                        'label' => $userEmail->fio ? '<span class="email-label">' . $userEmail->fio .
                                                            '<br><span style="padding-left: 25px">' . $userEmail->email . '</span></span>' :
                                                            '<span class="email-label">' . $userEmail->email . '</span>',
                                                        'labelOptions' => [
                                                            'style' => 'font-size: 13px;',
                                                        ],
                                                    ]); ?>
                                                </div>
                                                <div class="update-user-email-block" style="display: none;">
                                                    <?= Html::textInput('fio', $userEmail->fio, [
                                                        'class' => 'form-control',
                                                        'id' => 'user-fio',
                                                        'placeholder' => 'Фамилия И.О.',
                                                    ]); ?>
                                                    <?= Html::textInput('email', $userEmail->email, [
                                                        'class' => 'form-control',
                                                        'id' => 'user-email',
                                                        'placeholder' => 'E-mail',
                                                    ]); ?>
                                                </div>
                                                <div class="email-actions">
                                                    <span class="glyphicon glyphicon-pencil update-user-email"></span>
                                                    <span class="glyphicon glyphicon-trash delete-user-email user-email-<?= $userEmail->id; ?>"
                                                          data-url="<?= Url::to(['/email/delete-user-email', 'id' => $userEmail->id]); ?>"
                                                          data-id="<?= $userEmail->id; ?>" data-toggle="modal"
                                                          data-target="#delete-confirm-user-email"></span>
                                                    <span class="fa fa-reply fa-2x undo-user-email"
                                                          style="display: none;"></span>
                                                    <span class="fa fa-floppy-o fa-2x save-user-email"
                                                          style="display: none;"
                                                          data-url="<?= Url::to(['/email/update-user-email', 'id' => $userEmail->id]); ?>"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                                <div class="row who-send-container">
                                    <div class="container-who-send-label" style="width: 100%!important;">
                                        <div class="form-group" style="margin-bottom: 0;">
                                            <div class="checkbox" style="display: inline-block;width: 81%;">
                                                <?= Html::checkbox('InvoiceSendForm[sendToMe]', false, [
                                                    'id' => 'invoicesendform-sendtome',
                                                    'data-value' => $user->email,
                                                    'label' => '<span class="email-label">' . $employeeCompany->getFio() .
                                                        '<br><span style="padding-left: 25px">' . $user->email . '</span></span>',
                                                    'labelOptions' => [
                                                        'style' => 'font-size: 13px;',
                                                    ],
                                                ]); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <span class="adding-email-to-contractor">
                                    Добавление контакта в список по <?= $contractor->getTitle(true); ?>
                                </span>
                                <span class="add-new-user-email-form">
                                    <?= Html::button('<span class="fa icon fa-plus-circle"></span>', [
                                        'class' => 'btn yellow new-user-email-submit',
                                    ]); ?>
                                    <?= Html::textInput('fio', null, [
                                        'class' => 'form-control',
                                        'id' => 'user-fio',
                                        'placeholder' => 'Фамилия И.О.',
                                    ]); ?>
                                    <?= Html::textInput('email', null, [
                                        'class' => 'form-control',
                                        'id' => 'user-email',
                                        'placeholder' => 'E-mail',
                                    ]); ?>
                                </span>
                            </li>
                            <li>
                                <div class="row action-buttons" style="margin: 10px 0;">
                                    <div class="col-sm-5 col-xs-5"
                                         style="width: 26%;padding-right: 0;padding-left: 5px;">
                                        <?= Html::button('Добавить', [
                                            'class' => 'btn darkblue text-white widthe-100 add-emails',
                                        ]); ?>
                                    </div>
                                    <div class="col-sm-5 col-xs-5 pull-right"
                                         style="width: 22.8%;padding-left: 0;padding-right: 5px;float: right;">
                                        <?= Html::button('Отменить', [
                                            'class' => 'btn darkblue text-white widthe-100 undo-adding-emails',
                                        ]); ?>
                                    </div>
                                </div>
                            </li>
                        <?php else: ?>
                            <?php $canCreate = in_array($user->currentEmployeeCompany->employee_role_id, [EmployeeRole::ROLE_CHIEF, EmployeeRole::ROLE_SUPERVISOR]);
                            $type = Contractor::TYPE_CUSTOMER;
                            $contractors = $model->company->getContractors()
                                ->andWhere(['type' => $type])
                                ->andWhere(['is_deleted' => Contractor::NOT_DELETED])
                                ->andWhere(['and',
                                    ['not', ['director_email' => null]],
                                    ['not', ['director_email' => '']],
                                ]);
                            if (!$canCreate) {
                                $contractors->andWhere(['responsible_employee_id' => $user->id]);
                            }
                            /* @var $contractors Contractor[] */
                            ?>
                            <li>
                                <span class="search-contractors">
                                    <?= Html::textInput('search', null, [
                                        'class' => 'form-control',
                                        'id' => 'search',
                                        'placeholder' => 'Поиск по названию, фио и e-mail...',
                                        'style' => 'width: 100%;',
                                    ]); ?>
                                </span>
                            </li>
                            <li style="padding-top: 5px;max-height: 250px;overflow-y: auto;">
                                <div class="row who-send-container all">
                                    <div class="container-who-send-label">
                                        <?= $form->field($sendForm, 'sendToAll', [
                                            'options' => [
                                                'class' => 'form-group',
                                                'style' => 'margin-bottom: 0;',
                                            ],
                                            'template' => "{label}\n{input}",
                                        ])->checkbox([
                                            'label' => 'Выбрать всех',
                                            'labelOptions' => [
                                                'style' => 'font-size: 13px; font-weight: bold;',
                                            ],
                                            'class' => 'check-all-recipients',
                                        ]); ?>
                                    </div>
                                </div>
                                <?php foreach ($contractors->each() as $contractor): ?>
                                    <div class="row who-send-container"
                                         data-search_name="<?= $contractor->getTitle(true); ?>"
                                         data-search_fio="<?= $contractor->director_name; ?>"
                                         data-search_email="<?= $contractor->director_email; ?>">
                                        <div class="container-who-send-label" style="width: 100%!important;">
                                            <div class="form-group" style="margin-bottom: 0;">
                                                <div class="checkbox" style="display: inline-block;width: 81%;">
                                                    <?= Html::checkbox('InvoiceSendForm[sendToContractor][' . $contractor->id . ']', false, [
                                                        'id' => 'invoicesendform-sendtouser',
                                                        'data-value' => $contractor->director_email,
                                                        'label' => '<span class="email-label">' .
                                                            $contractor->getTitle(true) . '<br>' .
                                                            ($contractor->director_name ?
                                                                ('<span style="padding-left: 25px">' . $contractor->director_name . '</span><br>') :
                                                                null) .
                                                            '<span style="padding-left: 25px">' . $contractor->director_email . '</span>
                                                                    </span>',
                                                        'labelOptions' => [
                                                            'style' => 'font-size: 13px;',
                                                        ],
                                                    ]); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                                <div class="row who-send-container">
                                    <div class="container-who-send-label" style="width: 100%!important;">
                                        <div class="form-group" style="margin-bottom: 0;">
                                            <div class="checkbox" style="display: inline-block;width: 81%;">
                                                <?= Html::checkbox('InvoiceSendForm[sendToMe]', false, [
                                                    'id' => 'invoicesendform-sendtome',
                                                    'data-value' => $user->email,
                                                    'label' => '<span class="email-label">' . $user->getFio() .
                                                        '<br><span style="padding-left: 25px">' . $user->email . '</span></span>',
                                                    'labelOptions' => [
                                                        'style' => 'font-size: 13px;',
                                                    ],
                                                ]); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row action-buttons" style="margin: 10px 0;">
                                    <div class="col-sm-5 col-xs-5"
                                         style="width: 26%;padding-right: 0;padding-left: 5px;">
                                        <?= Html::button('Добавить', [
                                            'class' => 'btn darkblue text-white widthe-100 add-emails',
                                        ]); ?>
                                    </div>
                                    <div class="col-sm-5 col-xs-5 pull-right"
                                         style="width: 22.8%;padding-left: 0;padding-right: 5px;float: right;">
                                        <?= Html::button('Отменить', [
                                            'class' => 'btn darkblue text-white widthe-100 undo-adding-emails',
                                        ]); ?>
                                    </div>
                                </div>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>

            <div class="block-subject-email">
                <div class="email-label">Тема:</div>
                <?= $form->field($sendForm, 'subject', [
                    'options' => [
                        'class' => 'form-group form-md-line-input form-md-floating-label field-message-subject',
                        'style' => 'width: 90%;',
                    ],
                ])->textInput([
                    'class' => 'form-control input-sm edited',
                    'id' => 'message-subject',
                    'aria-required' => true,
                    'value' => $model->getEmailSubject(),
                ])->label(false); ?>
            </div>
            <div class="row block-files-email" style="padding-bottom: 10px;margin-left: 0;margin-right: 0;">
                <span class="upload-file" data-url="<?= Url::to(['/email/upload-email-file']); ?>"
                      data-csrf-parameter="<?= Yii::$app->request->csrfParam; ?>"
                      data-csrf-token="<?= Yii::$app->request->csrfToken; ?>">
                    <span class="icon icon-paper-clip"></span>
                    <span class="upload-file-email border-b">Прикрепить файл</span>
                </span>
                <div id="file-ajax-loading" style="display: none;">
                    <img src="/img/loading.gif">
                </div>
                <?php if ($model instanceof Invoice): ?>
                    <span class="template-email border-b">Шаблон</span>
                <?php endif; ?>
                <span class="add-signature-email border-b"
                      style="<?= $model instanceof Invoice ? 'margin-right: 15px;' : null; ?>">Подпись</span>
                <div class="row field-invoicesendform-signature" style="display: none;">
                    <div class="arrow"></div>
                    <div class="signature-text-block">
                        <?= Html::textArea('signature', $signature, [
                            'class' => 'email-signature',
                            'rows' => 3,
                            'disabled' => true,
                        ]); ?>
                        <span class="glyphicon glyphicon-pencil update-email-signature"></span>
                        <span class="fa fa-floppy-o fa-2x save-email-signature"
                              data-url="<?= Url::to(['/email/email-signature']); ?>" style="display: none;"></span>
                        <span class="fa fa-reply fa-2x undo-update-email-signature" style="display: none;"></span>
                    </div>
                </div>
                <?php if ($model instanceof Invoice): ?>
                    <div class="row field-invoicesendform-template" style="display: none;">
                        <div class="arrow"></div>
                        <div class="template-text-block">
                            <?= Html::radioList('template', $activeEmailTemplate ? $activeEmailTemplate->id : 0, $emailTemplatesArray, [
                                'class' => 'template-variants',
                                'id' => 'template-variant-radio',
                                'item' => function ($index, $label, $name, $checked, $value) use ($model) {
                                    if ($value) {
                                        /* @var $emailTemplate EmailTemplate */
                                        $emailTemplate = EmailTemplate::findOne($value);
                                        $emailText = $emailTemplate->text;
                                    } else {
                                        $emailText = $model->emailText;
                                    }

                                    $return = '<label>';
                                    $return .= Html::radio($name, $checked, [
                                        'data-message' => $emailText,
                                    ]);
                                    if ($value) {
                                        $return .= '<span class="glyphicon glyphicon-pencil update-template" data-url="' .
                                            Url::to(['/email/update-template', 'id' => $value]) . '"></span>';
                                        $return .= '<span class="glyphicon glyphicon-trash delete-template template-' . $value . '" data-url="' .
                                            Url::to(['/email/delete-template', 'id' => $value]) . '" data-id="' . $value . '"
                                            data-toggle="modal" data-target="#delete-confirm-template"></span>';
                                    }
                                    $return .= '<span class="label-radio">' . $label . '</span>';
                                    $return .= '</label>';

                                    return $return;
                                },
                            ]); ?>
                            <?php if ($emailTemplatesCount < 3): ?>
                                <div class="grey-line"
                                     style="border-bottom: 1px solid #e4e4e4;margin-left: -5px;margin-right: -5px;padding-top: 10px;"></div>
                                <?= Html::textInput('templateName', null, [
                                    'placeholder' => 'Название шаблона',
                                    'class' => 'form-control',
                                    'id' => 'new-template-name',
                                ]); ?>
                                <div class="form-actions email-template-buttons">
                                    <div class="row action-buttons">
                                        <div class="col-sm-8 col-xs-8" style="padding-left: 20px;">
                                            <span class="glyphicon glyphicon-pencil"
                                                  style="color: rgb(91, 155, 209);cursor: pointer;"></span>
                                            <span class="fa fa-floppy-o fa-2x save-email-template"
                                                  style="color: rgb(91, 155, 209);cursor: pointer;display: none;"
                                                  data-url="<?= Url::to(['/email/email-template']); ?>"></span>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <div class="email_text_input">
                <?= $form->field($sendForm, 'emailText')->textArea([
                    'rows' => $model instanceof Invoice ? 1 : 4,
                    'style' => 'padding: 10px 0; width: 100%; border: 0;overflow-y: auto;',
                ])->hint($this->render('@frontend/modules/documents/views/invoice/_email_text_hint', [
                    'documentText' => $emailPermanentText,
                    'emailSignature' => $emailSignature,
                    'viewDocumentButtonText' => $viewDocumentButtonText,
                    'viewDocumentLink' => $model instanceof PriceList ?
                        Url::to(['/price-list/view', 'id' => $model->id, 'productionType' => $model->production_type]) :
                        ($model instanceof Driver || $model instanceof Vehicle) ?
                            null :
                            (($model instanceof Invoice) ? 'javascript:;' : null),
                ]))->label(false); ?>
            </div>
            <div class="row email-uploaded-files" style="<?= empty($permanentFiles) ? 'display: none;' : null; ?>">
                <?= Html::activeHiddenInput($sendForm, 'sendEmailFiles'); ?>
                <div class="one-file col-md-6 template">
                    <span class="file-name"></span>
                    <span class="file-size"></span>
                    <div class="file-actions">
                        <?= Html::a('<span class="glyphicon glyphicon-eye-open view-file"></span>', null, [
                            'target' => '_blank',
                            'class' => 'download-file',
                            'style' => 'display: none;',
                        ]); ?>
                        <span class="glyphicon glyphicon-trash delete-file"></span>
                    </div>
                </div>
            </div>
            <div class="form-actions" style="z-index: 999;">
                <div class="row action-buttons">
                    <div class="col-sm-5 col-xs-5" style="width: 11%;">
                        <div>
                            <?= Html::submitButton('<span class="ladda-label">Отправить</span><span class="ladda-spinner"></span>', [
                                'class' => 'btn darkblue text-white mt-ladda-btn ladda-button widthe-100',
                                'data-style' => 'expand-right',
                            ]); ?>
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-2" style="width: 15.3%;"></div>
                    <div class="col-sm-5 col-xs-5" style="width: 9.9%;">
                        <?= Html::button('Отменить', [
                            'class' => 'btn darkblue text-white pull-right widthe-100 side-panel-close-button',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php $form->end(); ?>
        <span class="side-panel-close" title="Закрыть">
            <span class="side-panel-close-inner"></span>
        </span>
    </div>

    <div id="delete-confirm-template" class="confirm-modal fade modal" role="dialog" tabindex="-1" aria-hidden="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">Вы уверены, что хотите удалить шаблон?</div>
                    </div>
                    <div class="form-actions row">
                        <div class="col-xs-6">
                            <?= Html::a('<span class="ladda-label">ДА</span><span class="ladda-spinner"></span>', null, [
                                'class' => 'btn darkblue pull-right mt-ladda-btn ladda-button delete-template-modal',
                                'data-style' => 'expand-right',
                            ]); ?>
                        </div>
                        <div class="col-xs-6">
                            <?= Html::button('НЕТ', [
                                'class' => 'btn darkblue',
                                'data-dismiss' => 'modal',
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="delete-confirm-user-email" class="confirm-modal fade modal" role="dialog" tabindex="-1"
         aria-hidden="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">Вы уверены, что хотите удалить почту?</div>
                    </div>
                    <div class="form-actions row">
                        <div class="col-xs-6">
                            <?= Html::a('<span class="ladda-label">ДА</span><span class="ladda-spinner"></span>', null, [
                                'class' => 'btn darkblue pull-right mt-ladda-btn ladda-button delete-user-email-modal',
                                'data-style' => 'expand-right',
                            ]); ?>
                        </div>
                        <div class="col-xs-6">
                            <?= Html::button('НЕТ', [
                                'class' => 'btn darkblue',
                                'data-dismiss' => 'modal',
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tooltip_templates container-tooltip_templates">
        <div id="send-from-tooltip" class="box-tooltip-templates" style="width: auto;text-align: left;">
            При ответе вашим клиентом
            <br>
            на письмо со счетом, ответ придет
            <br>
            на вашу почту – <?= $user->email; ?>
        </div>
    </div>
<?php if ($employeeCompany->company->getOutInvoiceSendEmailCount() == 0 && Yii::$app->controller->id != 'agreement'): ?>
    <?= $this->render('_send_message_tooltip'); ?>
<?php endif; ?>
<?php if (!($model instanceof PriceList)): ?>
    <?php if (!($model instanceof Driver) && !($model instanceof Vehicle) && !($model instanceof LogisticsRequest)): ?>
        <?php $this->registerJs('
            $(document).ready(function () {
                $.post("/email/get-permanent-files?id=' . $model->id . '", {tableName: "' . $model::tableName() . '"}, function (data) {
                    var $needUploadFiles = [];
    
                    for (var i = 0; i < data.files.length; i++) {
                        var $templateFile = $(".email-uploaded-files .one-file.template").clone();
    
                        $templateFile.removeClass("template");
                        $templateFile.attr("data-id", data.files[i].id);
                        $templateFile.prepend(data.files[i].previewImg);
                        $templateFile.find(".file-name").text(data.files[i].name).attr("title", data.files[i].name);
                        $templateFile.find(".file-size").text(data.files[i].size + " КБ");
                        $templateFile.find(".delete-file").attr("data-url", data.files[i].deleteUrl);
                        if (data.files[i].downloadUrl) {
                            $templateFile.find(".download-file").attr("href", data.files[i].downloadUrl).show();
                        }
                        $templateFile.show();
                        $(".email-uploaded-files").append($templateFile);
                        $("#file-ajax-loading").hide();
                        $(".email-uploaded-files").show();
                        if (data.files[i].documentPdf !== undefined) {
                            $(".view-document-link").attr("href", data.files[i].downloadUrl);
                        }
                    }
    
                    $(".email-uploaded-files .one-file:not(.template)").each(function () {
                        $needUploadFiles.push($(this).data("id"));
                    });
                    $("#invoicesendform-sendemailfiles").val($needUploadFiles.join(", "));
                });
            });
        '); ?>
    <?php endif; ?>
    <?php $this->registerJs('
    $("#user-email-dropdown .add-new-user-email-form .new-user-email-submit").click(function () {
        var $inputs = $(this).siblings("input");
        var $requiredInput = $(this).siblings("input#user-email");
        var $sendXhr = true;

        if ($requiredInput.val() == "") {
            $sendXhr = false;
        }

        if ($sendXhr) {
            var $this = $(this);
            $.post("/email/add-user-email?contractorID=' . $contractor->id . '", $inputs.serialize(), function (data) {
                if (data.result == true) {
                    $("#user-email-dropdown li:first .who-send-container:last").after(data.html);
                    $("#user-email-dropdown .who-send-container:last #invoicesendform-sendtouser").uniform();
                    $("#user-email-dropdown li:first .who-send-container:last .checker input").click();
                    $inputs.val("");
                    if (!data.canAdd) {
                        $("#user-email-dropdown .add-new-user-email-form").hide();
                    }
                }
            });
        }
    });
'); ?>
<?php else: ?>
    <?php $this->registerJs('
        $(document).on("keyup change", "#user-email-dropdown .search-contractors #search", function (e) {
            e.preventDefault();
            var $search = $(this).val().toLowerCase();

            if ($search !== "") {
                $("#user-email-dropdown .who-send-container").each(function (e) {
                    if ($(this).data("search_name").toLowerCase().search($search) == -1 &&
                    $(this).data("search_fio").toLowerCase().search($search) == -1 &&
                    $(this).data("search_email").toLowerCase().search($search) == -1) {
                        $(this).hide();
                    } else {
                        $(this).show();
                    }
                });
            } else {
                $("#user-email-dropdown .who-send-container").show();
            }
        });
    '); ?>
<?php endif; ?>
<?php
$this->registerJs('
$employeesEmail = $(".contractor-employees-email");
$signatureBlock = $(".field-invoicesendform-signature, .update-email-signature");
$templateBlock = $(".field-invoicesendform-template");

var sendMessagePanelOpen = function (sendToMe) {
    if (sendToMe) {
        $("#user-email-dropdown input:checkbox:checked:not(#invoicesendform-sendtome)").click();
        if (!$("#invoicesendform-sendtome").is(":checked")) {
            $("#invoicesendform-sendtome").click();
        }
        $("#user-email-dropdown .add-emails").click();
        $("form#send-document-form button:submit").closest("div").pulsate({
            color: "#bf1c56",
            reach: 20,
            repeat: 3
        });
    }
    $(".send-message-panel-trigger").addClass("active");
    $(".page-shading-panel").removeClass("hidden");
    $(".send-message-panel").show("fast");
    $("#visible-right-menu-message-panel").show();
    $("html").attr("style", "overflow: hidden;");
    $(".send-message-panel .main-block").scrollTop(0);
    if ($("#invoicesendform-sendto").val().length < 1) {
        $("#invoicesendform-sendto_tag").click()
    }
    if ($(".send-message-tooltip-panel").length > 0) {
        $(".send-message-tooltip-panel").show(500);
    }
}

var sendMessagePanelClose = function() {
    $(".send-message-panel-trigger").removeClass("active");
    $(".send-message-panel").hide("fast", function() {
        $(".page-shading-panel").addClass("hidden");
    });
    $("#visible-right-menu-message-panel").hide();
    $("html").removeAttr("style");
    if ($(".send-message-tooltip-panel").length > 0) {
        $(".send-message-tooltip-panel").hide(500);
    }
    $("form#send-document-form button:submit").removeClass("pulse-button");
}
$(document).on("click", ".send-message-panel-trigger", function (e) {
    e.preventDefault();

    var sendToMe = false;
    if ($(this).hasClass("send-to-me")) {
        sendToMe = true;
    }
    sendMessagePanelOpen(sendToMe);
});

$(document).on("click", ".send-message-panel .side-panel-close, .send-message-panel .side-panel-close-button", function () {
    sendMessagePanelClose();
});

$("#invoicesendform-sendto_tag").focus(function () {
    $employeesEmail.slideDown();
    $templateBlock.slideUp();
    $signatureBlock.slideUp();
    $(".field-invoicesendform-signature .form-actions").hide();
    $("textarea.email-signature").attr("disabled", true);
});

$("#invoicesendform-sendcopy_tag, #message-subject, #invoicesendform-emailtext").focus(function () {
    $employeesEmail.slideUp();
    $templateBlock.slideUp();
    $signatureBlock.slideUp();
    $(".field-invoicesendform-signature .form-actions").hide();
    $("textarea.email-signature").attr("disabled", true);
});

$(document).ready(function () {
    $(".add-signature-email, .template-email, .block-files-email, input[name=\"emailFile\"]").click(function () {
        $employeesEmail.slideUp();
    });
    $("input[name=\"emailFile\"]").click(function () {
        $templateBlock.slideUp();
        $signatureBlock.slideUp();
        $(".field-invoicesendform-signature .form-actions").hide();
        $("textarea.email-signature").attr("disabled", true);
    });
    $("#invoicesendform-sendto_tagsinput #invoicesendform-sendto_tag")
    .attr("placeholder", "Укажите e-mail или выберите из списка")
    .addClass("visible");

    var text = $("#invoicesendform-emailtext").val(),
    matches = text.match(/\n/g),
    breaks = matches ? matches.length : 0;

    $("#invoicesendform-emailtext").attr("rows", breaks + 1);
});

$(".add-signature-email").click(function () {
    if ($signatureBlock.is(":visible")) {
        $signatureBlock.slideUp();
        $(".field-invoicesendform-signature .form-actions").hide();
        $("textarea.email-signature").attr("disabled", true);
    } else {
        $templateBlock.slideUp();
        $signatureBlock.slideDown();
    }
});

$(".update-email-signature").click(function () {
   $(this).hide();
   $("textarea.email-signature").removeAttr("disabled");
   $(".save-email-signature, .undo-update-email-signature").css("display", "inline-block");
});

$(".undo-update-email-signature").click(function () {
    $(".update-email-signature").show();
    $(".save-email-signature, .undo-update-email-signature").hide();
    $("textarea.email-signature").attr("disabled", true);
});

$(".save-email-signature").click(function () {
    $.post($(this).data("url"), $("textarea.email-signature").serialize(), function (data) {
        if (data.result) {
            $(".update-email-signature").show();
            $(".save-email-signature, .undo-update-email-signature").hide();
            $("textarea.email-signature").attr("disabled", true);
            $(".email-signature-message-text").html(data.signature);
        }
    });
});

$(".save-email-template").click(function () {
    $saveButton = $(this);
    $.post($(this).data("url"), $("#new-template-name, #invoicesendform-emailtext").serialize(), function (data) {
        $templateVariants = $(".template-variants");
        if (data.result) {
            $templateVariants.append("<label>" +
                "<input type=\"radio\" name=\"template\" value=\"" + data.id +"\" data-message=\"" + data.text + "\"><span class=\"label-radio\">" + data.name +
                "</span><span class=\"glyphicon glyphicon-pencil update-template\" data-url=\"/email/update-template?id=" + data.id + "\"></span>" +
                "<span class=\"glyphicon glyphicon-trash delete-template template-" + data.id + "\" data-url=\"/email/delete-template?id=" + data.id + "\" data-toggle=\"modal\" data-target=\"#delete-confirm-template\" data-id=\"" + data.id + "\"></span>" +
            "</label>").find("input[value=\"" + data.id + "\"]").click();
            $.uniform.restore("[name=\'template\']");
            $("[name=\'template\']").uniform();
            if (data.canAdd == false) {
                $("#new-template-name, .template-text-block .form-actions, .template-text-block .grey-line").hide();
            }
            $saveButton.hide();
            $saveButton.siblings(".glyphicon-pencil").show();
        }
        $("#new-template-name").val("");
    });
});

$(".template-email").click(function () {
    if ($templateBlock.is(":visible")) {
        $templateBlock.slideUp();
    } else {
        $(".field-invoicesendform-signature .form-actions").hide();
        $("textarea.email-signature").attr("disabled", true);
        $signatureBlock.slideUp();
        $templateBlock.slideDown();
    }
});

$(document).on("change", "#template-variant-radio", function () {
    $("#invoicesendform-emailtext").val($(this).find("input:checked").data("message"));
});

$(document).on("click", ".delete-template", function () {
    $("#delete-confirm-template .delete-template-modal").attr("data-url", $(this).data("url")).attr("data-id", $(this).data("id"));
});

$(document).on("click", ".delete-user-email", function () {
    $("#delete-confirm-user-email .delete-user-email-modal").attr("data-url", $(this).data("url")).attr("data-id", $(this).data("id"));
});

$("#delete-confirm-template .delete-template-modal").click(function () {
    var $modal = $(this).closest("#delete-confirm-template");
    var $this = $(this);

    $.post($(this).attr("data-url"), null, function (data) {
        if (data.result) {
            var $item = $("span.template-" + $this.attr("data-id")).closest("label");

            $modal.modal("hide");
            $item.remove();
            $(".template-text-block label input:first").click();
            if (data.canAdd == true) {
                $("#new-template-name, .template-text-block .form-actions, .template-text-block .grey-line").show();
            }
            $(".template-text-block label input:first").click();
        }
    });
});

$("#delete-confirm-user-email .delete-user-email-modal").click(function () {
    var $modal = $(this).closest("#delete-confirm-user-email");
    var $this = $(this);

    $.post($(this).attr("data-url"), null, function (data) {
        if (data.result) {
            var $item = $("span.user-email-" + $this.attr("data-id")).closest(".who-send-container");
            var $checkBox = $item.find("#invoicesendform-sendtouser");

            if ($checkBox.is(":checked")) {
                $item.find("#invoicesendform-sendtouser").click();
            }
            $modal.modal("hide");
            $item.remove();
            if (data.canAdd == true) {
                $("#user-email-dropdown .add-new-user-email-form").show();
            }
        } else {
            alert(data.msg);
        }
    });
});

$(document).on("click", ".update-template", function () {
    $item = $(this).closest("label");
    $labelText = $item.find(".label-radio");

    $labelText.hide();
    $labelText.after("<input type=\"text\" class=\"form-control update-template-name\" value=\"" + $labelText.text().trim() + "\">");
    $(this).hide();
    $(this).siblings(".delete-template").hide();
    $(this).after("<span class=\"fa fa-floppy-o fa-2x save-template\" data-url=\"" + $(this).data("url") + "\"></span>");
    $(this).after("<span class=\"fa fa-reply fa-2x undo-template\"></span>");
});

$(document).on("click", ".undo-template", function () {
    $item = $(this).closest("label");

    $item.find(".update-template-name, .save-template, .undo-template").remove();
    $item.find(".delete-template, .update-template, .label-radio").show();
});

$(document).on("click", ".save-template", function () {
    $item = $(this).closest("label");
    $labelText = $item.find(".label-radio");

    $.post($(this).data("url"), {
        name: $item.find(".update-template-name").val(),
        text: $("#invoicesendform-emailtext").val(),
    }, function (data) {
        if (data.result) {
            $labelText.text(data.name);
            $item.find(".radio input").data("message", data.text);
        }
        $item.find(".update-template-name, .save-template, .undo-template").remove();
        $item.find(".delete-template, .update-template").show();
        $labelText.show();
    });
});

$("#invoicesendform-sendtoall").change(function () {
    if ($(this).is(":checked")) {
        $("#user-email-dropdown .who-send-container .checker input:not(:checked)").click();
    } else {
        if ($("#user-email-dropdown .who-send-container:not(.all) .checker input:not(:checked)").length == 0) {
            $("#user-email-dropdown .who-send-container .checker input:checked").click();
        }
    }
});

$(document).on("change", "#user-email-dropdown .who-send-container:not(.all) .checker input", function (event) {
    $mainCheckBox = $("#user-email-dropdown .who-send-container.all .checker input");
    $tagsInput = $("#invoicesendform-sendto");
    if ($("#user-email-dropdown .who-send-container:not(.all) .checker input:not(:checked)").length == 0) {
        if (!$mainCheckBox.is(":checked")) {
            $mainCheckBox.click();
        }
    } else {
        if ($mainCheckBox.is(":checked")) {
            $mainCheckBox.click();
        }
    }
});

$(".add-contractor-email .container-who-send-label input[type=\"checkbox\"]").change(function () {
    var $this = $(this);
    var $labelBlock = $(this).closest(".container-who-send-label");
    var $blockWhoSendInput = $(this).closest(".who-send-container").find(".container-who-send-input");
    var $input = $blockWhoSendInput.find("input");

    $.post($(this).data("url"), $input.serialize(), function (data) {
        if (data.result == true) {
            $blockWhoSendInput.remove();
            $labelBlock.find("span.email-label").html(data.labelText);
            $labelBlock.find(".checker input").attr("data-value", data.email);
            $labelBlock.css("width", "100%");
        }
    });
});

$(document).on("keyup change", "#new-template-name", function (e) {
    if ($(this).val() !== "") {
        $(this).siblings(".form-actions").find(".glyphicon-pencil").hide();
        $(this).siblings(".form-actions").find(".save-email-template").show();
    } else {
        $(this).siblings(".form-actions").find(".glyphicon-pencil").show();
        $(this).siblings(".form-actions").find(".save-email-template").hide();
    }
});

$(document).on("keyup change", ".container-who-send-input input", function (e) {
    var reg = /^([A-Za-zА-Яа-я0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,15})$/;
    if (reg.test($(this).val()) != false) {
        $(this).closest(".who-send-container").find("input[type=\"checkbox\"]").removeAttr("disabled").uniform();
    } else {
        $(this).closest(".who-send-container").find("input[type=\"checkbox\"]").attr("disabled", true).uniform();
    }
});

var _$uploadButton = $(".block-files-email span.upload-file");
var _uploadUrl = _$uploadButton.data("url");
var _csrfParameter = _$uploadButton.data("csrf-parameter");
var _csrfToken = _$uploadButton.data("csrf-token");
var uploadData = {};
uploadData[_csrfParameter] = _csrfToken;

var uploader = new ss.SimpleUpload({
    button: _$uploadButton[0], // HTML element used as upload button
    url: _uploadUrl, // URL of server-side upload handler
    data: uploadData,
    multipart: true,
    multiple: true,
    multipleSelect: true,
    encodeCustomHeaders: true,
    responseType: "json",
    name: "emailFile", // Parameter name of the uploaded file
    onSubmit: function() {
        $("#file-ajax-loading").css("display", "inline-block");
    },
    onComplete: function (filename, response) {
        if (response["result"] == true) {
            var $templateFile = $(".email-uploaded-files .one-file.template").clone();
            var $needUploadFiles = [];

            $templateFile.removeClass("template");
            $templateFile.attr("data-id", response["id"]);
            $templateFile.prepend(response["previewImg"]);
            $templateFile.find(".file-name").text(response["name"]).attr("title", response["name"]);
            $templateFile.find(".file-size").text(response["size"] + " КБ");
            $templateFile.find(".delete-file").attr("data-url", response["deleteUrl"]);
            if (response["downloadUrl"]) {
                $templateFile.find(".download-file").attr("href", response["downloadUrl"]).show();
            }
            $templateFile.show();
            $(".email-uploaded-files").append($templateFile);
            $("#file-ajax-loading").hide();
            $(".email-uploaded-files").show();

            $(".email-uploaded-files .one-file:not(.template)").each(function () {
                $needUploadFiles.push($(this).data("id"));
            });
            $("#invoicesendform-sendemailfiles").val($needUploadFiles.join(", "));
        } else {
            alert(response["msg"]);
            $("#file-ajax-loading").hide();
        }
    }
});

$(document).on("click", ".email-uploaded-files .one-file .delete-file", function () {
    var $oneFile = $(this).closest(".one-file");
    var $fileBlock = $(this).closest(".email-uploaded-files");
    $.post($(this).data("url"), null, function (data) {
        if (data.result) {
            var $needUploadFiles = [];

            $oneFile.remove();
            $(".email-uploaded-files .one-file:not(.template)").each(function () {
                $needUploadFiles.push($(this).data("id"));
            });
            $("#invoicesendform-sendemailfiles").val($needUploadFiles.join(", "));
            if ($fileBlock.find(".one-file:not(.template)").length == 0) {
                $fileBlock.hide();
            }
        } else {
            alert(data.msg);
        }
    });
});

$(".dropdown-email").click(function () {
    $(this).toggleClass("open-users");
    if ($(this).hasClass("open-users")) {
        $("#send-document-form .form-actions button").attr("disabled", true);
    } else {
        $("#send-document-form .form-actions button").removeAttr("disabled");
    }
}).find("ul#user-email-dropdown").click(function(e) {
    if (!$(e.target).hasClass("delete-user-email") && !$(e.target).hasClass("update-user-email") &&
    !$(e.target).hasClass("undo-user-email") && !$(e.target).hasClass("save-user-email") &&
    !$(e.target).hasClass("add-emails")) {
        e.stopPropagation();
    } else {
        $(this).closest(".dropdown-email").removeClass("open-users");
        if ($(e.target).hasClass("add-emails")) {
            $tagsInput = $("#invoicesendform-sendto");
            $tagsInputVal = $tagsInput.val().split(",");
            $mainCheckBox = $("#user-email-dropdown .who-send-container.all .checker input");

            $("#user-email-dropdown .who-send-container:not(.all) .checker input").each(function () {
                $email = $(this).attr("data-value");
                if ($(this).is(":checked")) {
                    if ($tagsInputVal.indexOf($email) == -1) {
                        $tagsInput.addTag($email);
                    }
                } else {
                    if ($tagsInputVal.indexOf($email) !== -1) {
                        $tagsInput.removeTag($email);
                    }
                }
            });
            if ($tagsInput.val() == "") {
                $(".field-invoicesendform-sendto").addClass("has-error");
                $(".field-invoicesendform-sendto .help-block").text("Нужно выбрать получателя.");
            } else {
                $(".field-invoicesendform-sendto").removeClass("has-error");
                $(".field-invoicesendform-sendto .help-block").text("");
                e.stopPropagation();
                $("#send-document-form .form-actions button").removeAttr("disabled");
                $(this).closest(".dropdown-email").removeClass("open-users");
            }
        }
    }
});

$(document).mouseup(function (e) {
    if ($(e.target).hasClass("page-shading-panel")) return;
    var div = $(".dropdown-email");
    if ($(e.target).hasClass("undo-adding-emails")) {
        div.removeClass("open-users");
    }
    if (!div.is(e.target) && div.has(e.target).length === 0 && $(e.target).attr("id") !== "visible-right-menu-wrapper") {
    	div.removeClass("open-users");
    }

    if (!div.hasClass("open-users")) {
        $("#send-document-form .form-actions button").removeAttr("disabled");
    }
});

$(document).on("click", ".container-who-send-label .email-actions .save-user-email", function () {
    var $emailActionsBlock = $(this).closest(".email-actions");
    var $inputs = $emailActionsBlock.siblings(".update-user-email-block").find("input");
    var $requiredInput = $emailActionsBlock.siblings(".update-user-email-block").find("input#user-email");
    var $label = $emailActionsBlock.siblings(".checkbox").find("label");
    var $sendXhr = true;

    if ($requiredInput.val() == "") {
        $sendXhr = false;
    }
    if ($sendXhr) {
        var $this = $(this);
        $.post($(this).data("url"), $inputs.serialize(), function (data) {
            if (data.result == true) {
                $label.find(".email-label").html(data.label);
                if ($label.find("input#invoicesendform-sendtouser").is(":checked")) {
                    $label.find("input#invoicesendform-sendtouser").click();
                }
                $label.find("input#invoicesendform-sendtouser").attr("data-value", data.email);
                $emailActionsBlock.find(".save-user-email, .undo-user-email").hide();
                $emailActionsBlock.find(".update-user-email, .delete-user-email").show();
                $emailActionsBlock.siblings(".checkbox").show();
                $emailActionsBlock.siblings(".update-user-email-block").hide();
            }
        });
    }
});

$(document).on("click", ".container-who-send-label .email-actions .update-user-email", function () {
    var $emailActionsBlock = $(this).closest(".email-actions");

    $(this).closest(".who-send-container").css("margin-bottom", "10px").css("margin-top", "10px");
    $(this).hide();
    $(this).siblings(".delete-user-email").hide();
    $(this).siblings(".undo-user-email, .save-user-email").show();
    $emailActionsBlock.siblings(".checkbox").hide();
    $emailActionsBlock.siblings(".update-user-email-block").show();
});

$(document).on("click", ".container-who-send-label .email-actions .undo-user-email", function () {
    var $emailActionsBlock = $(this).closest(".email-actions");

    $(this).closest(".who-send-container").removeAttr("style");
    $(this).hide();
    $(this).siblings(".save-user-email").hide();
    $(this).siblings(".update-user-email, .delete-user-email").show();
    $emailActionsBlock.siblings(".checkbox").show();
    $emailActionsBlock.siblings(".update-user-email-block").hide();
});
');

if (isset($showSendPopup) && $showSendPopup) {
    $this->registerJs('
        $(document).ready(function () {
            sendMessagePanelOpen();
            $.post("' . Url::to(['/documents/invoice/not-show']) . '", {"notShow": 1});

            $(document).on("click", ".send-message-panel .side-panel-close, .send-message-panel .side-panel-close-button", function () {
                if (!$("#tooltip-send-button").hasClass("tooltipstered")) {
                    $("#tooltip-send-button").addClass("tooltip2").addClass("tooltipstered-main").attr("data-tooltip-content", "#tooltip_send");
                    $("#tooltip-pay-button").addClass("tooltip2").addClass("tooltipstered-main").attr("data-tooltip-content", "#tooltip_paid");

                    $(".tooltip2").tooltipster({
                        "theme":["tooltipster-noir", "tooltipster-noir-customized"],
                        "trigger":"click",
                        "contentAsHTML":true
                    });
                    setTimeout(function() {
                        $(".tooltipstered-main").tooltipster("open");
                    }, 1000);
                }
            });

            $("#tooltip-send-button button, #tooltip-pay-button button").click(function () {
                if ($(this).closest("span").hasClass("tooltipstered")) {
                    $(this).closest("span").tooltipster("destroy");
                }
            });
        });
    ');
}
