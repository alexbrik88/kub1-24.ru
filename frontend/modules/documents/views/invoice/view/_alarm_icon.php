<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$color = ArrayHelper::getValue($_params_, 'color', 'currentColor');
$size = ArrayHelper::getValue($_params_, 'size', '18px');

?>
<div style="display: inline-block; width: <?=$size?>; height: <?=$size?>; vertical-align: middle;"><svg
    xmlns="http://www.w3.org/2000/svg"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    version="1.1" id="Capa_1" x="0px" y="0px" width="512" height="512" viewBox="0 0 515.1 515.1"
    style="enable-background:new 0 0 515.1 515.1; width: 100%; height: 100%; margin-top: -1px;"
    xml:space="preserve" class="">
    <g>
        <g>
            <g id="access-alarms">
                <path d="M512.55,99.45L395.25,0L362.1,38.25l117.3,99.45L512.55,99.45z M153,40.8L119.85,2.55L2.55,99.45L35.7,137.7L153,40.8z     M270.3,158.1h-38.25v153l119.85,73.951l20.4-30.602l-102-61.199V158.1z M257.55,56.1c-127.5,0-229.5,102-229.5,229.5    c0,127.5,102,229.5,229.5,229.5s229.5-102,229.5-229.5C487.05,158.1,385.05,56.1,257.55,56.1z M257.55,464.1    c-99.45,0-178.5-79.049-178.5-178.5c0-99.45,79.05-178.5,178.5-178.5s178.5,79.05,178.5,178.5    C436.05,385.051,357,464.1,257.55,464.1z"
                style="fill: <?=$color?>"></path>
            </g>
        </g>
    </g>
</svg></div>