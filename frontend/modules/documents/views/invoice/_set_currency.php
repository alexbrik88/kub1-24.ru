<?php

use common\models\currency\Currency;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */

$currencyName = Html::tag('span', $model->currency_name, [
    'class' => 'currency_name',
    'data' => [
        'initial' => $model->currency_name,
    ],
]);
$currencyAmount = Html::tag('span', $model->currency_amount, [
    'class' => 'currency_rate_amount',
    'data' => [
        'initial' => $model->currency_amount,
    ],
]);
$currencyRate = Html::tag('span', $model->currency_rate, [
    'class' => 'currency_rate_value',
    'data' => [
        'initial' => $model->currency_rate,
    ],
]);

$radioName = Html::getInputName($model, 'currency_rate_type');

$this->registerJs("
    $('#invoice-currency_ratedate').datepicker({
        format: 'dd.mm.yyyy',
        language:'ru',
        autoclose: true,
        endDate: new Date(),
    });
");
?>

<label class="text-bold">Установить обменный курс</label>
<?= Html::hiddenInput($radioName, '') ?>

<?= Html::beginTag('div', [
    'id' => Html::getInputId($model, 'currency_rate_type'),
    'class' => 'row',
    'data' => [
        'geturl' => Url::to(['/site/currency-rate']),
        'seturl' => Url::to(['/documents/invoice/set-currency-rate', 'type' => $model->type, 'id' => $model->id]),
    ],
]) ?>

    <div class="col-xs-12">
        <label class="currency-rate-type-label">
            <?= Html::radio($radioName, $model->currency_rate_type == Currency::RATE_SERTAIN_DATE, [
                'id' => 'radio_currency_rate_ondate',
                'value' => Currency::RATE_SERTAIN_DATE,
                'class' => 'currency-rate-type-radio',
                'data' => [
                    'initial' => $model->currency_rate_type == Currency::RATE_SERTAIN_DATE,
                ],
            ])?>
            <div class="label-part" style="max-width: 200px;">
                <?= Html::activeHiddenInput($model, 'currency_rate_amount', [
                    'class' => 'currency_rate_amount',
                    'data' => [
                        'initial' => $model->currency_rate_amount,
                    ],
                ]) ?>
                <?= Html::activeHiddenInput($model, 'currency_rate_value', [
                    'class' => 'currency_rate_value',
                    'data' => [
                        'initial' => $model->currency_rate_value,
                    ],
                ]) ?>
                <a href="http://www.cbr.ru" target="_blank">Курс ЦБ РФ</a> на
                <?= Html::activeTextInput($model, 'currency_rateDate', [
                    'class' => 'invoice-currency_ratedate currency_rate_date input-underline',
                    'data' => [
                        'default' => $model->document_date,
                        'initial' => $model->currency_rateDate,
                    ],
                    'readonly' => true,
                ]) ?>:
            </div>
            <div class="label-part" style="max-width: 180px;">
                <?= Html::tag('span', $model->currency_rate_amount, [
                    'class' => 'currency_rate_amount',
                    'data' => [
                        'initial' => $model->currency_rate_amount,
                    ],
                ]); ?>
                <?= $currencyName ?> =
                <?= Html::tag('span', $model->currency_rate_value, [
                    'class' => 'currency_rate_value',
                    'data' => [
                        'initial' => $model->currency_rate_value,
                    ],
                ]) ?>
                RUB
            </div>
        </label>
    </div>
    <div class="col-xs-12">
        <label class="currency-rate-type-label">
            <?= Html::radio($radioName, $model->currency_rate_type == Currency::RATE_PAYMENT_DATE, [
                'id' => 'radio_currency_rate_onpayment',
                'value' => Currency::RATE_PAYMENT_DATE,
                'class' => 'currency-rate-type-radio default-type',
                'data' => [
                    'initial' => $model->currency_rate_type == Currency::RATE_PAYMENT_DATE,
                ],
            ])?>
            <div class="label-part">
                <a href="http://www.cbr.ru" target="_blank">Курс ЦБ РФ</a> на день оплаты.
            </div>
            <div class="label-part" style="font-size: 12px;">
                (До оплаты счет учитывается по курсу на <?= Html::tag('span', $model->currency_rateDate, [
                    'class' => 'currency_rate_date',
                    'data' => [
                        'initial' => $model->currency_rateDate,
                    ],
                ]) ?>, при оплате пересчитывается).
            </div>
        </label>
    </div>
    <div class="col-xs-12">
        <label class="currency-rate-type-label">
            <?= Html::radio($radioName, $model->currency_rate_type == Currency::RATE_CUSTOM, [
                'id' => 'radio_currency_rate_custom',
                'value' => Currency::RATE_CUSTOM,
                'class' => 'currency-rate-type-radio',
                'data' => [
                    'initial' => $model->currency_rate_type == Currency::RATE_CUSTOM,
                ],
            ])?>
            <div class="label-part" style="max-width: 250px;">
                Установить другой обменный курс:
            </div>
            <div class="label-part" style="max-width: 225px;">
                <?= Html::activeTextInput($model, 'currencyAmountCustom', [
                    'value' => $model->currency_amount,
                    'class' => 'currency-custom-input currency_rate_amount',
                    'style' => 'width: 50px;',
                    'disabled' => $model->currency_rate_type != Currency::RATE_CUSTOM,
                    'data' => [
                        'initial' => $model->currency_amount,
                    ],
                ]) ?>
                <?= $currencyName ?>
                =
                <?= Html::activeTextInput($model, 'currencyRateCustom', [
                    'value' => $model->currency_rate,
                    'class' => 'currency-custom-input currency_rate_value',
                    'disabled' => $model->currency_rate_type != Currency::RATE_CUSTOM,
                    'data' => [
                        'initial' => $model->currency_rate,
                    ],
                ]) ?>
                RUB
            </div>
        </label>
    </div>

<?= Html::endTag('div') ?>
