<?php
use common\models\document\status\InvoiceStatus;
use frontend\models\Documents;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use frontend\widgets\ConfirmModalWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Dropdown;
use yii\web\View;

/** @var $this View */
/** @var $model \common\models\document\Invoice */
/** @var $useContractor boolean */

$contractorId = $useContractor ? $model->contractor_id : null;

?>

<div class="row action-buttons buttons-fixed" id="buttons-fixed">
    <div class="button-bottom-page-lg col-sm-1 col-xs-1 tooltip-hover" data-tooltip-content="#tooltip_recreate">
        <?= Html::a('Перевыставить', ['create', 'type' => Documents::IO_TYPE_OUT, 'from' => $model->id], [
            'class' => 'btn yellow text-white hidden-md hidden-sm hidden-xs',
            'title' => 'Выставить исходящий счет',
            'style' => 'padding-left: 15px; padding-right: 15px;'
        ]) ?>
        <?= Html::a('<i class="fa fa-sign-out"></i>', ['create', 'type' => Documents::IO_TYPE_OUT, 'from' => $model->id], [
            'class' => 'btn yellow text-white widthe-100 hidden-lg',
            'title' => 'Выставить исходящий счет',
        ]) ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="height: 34px;">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="height: 34px;">
        <?php $printUrl = ['document-print', 'actionType' => 'print', 'id' => $model->id, 'type' => $model->type,
            'filename' => $model->getPrintTitle(),];
        echo Html::a('Печать', $printUrl, [
            'target' => '_blank',
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
        ]);
        echo Html::a('<i class="fa fa-print fa-2x"></i>', $printUrl, [
            'target' => '_blank',
            'title' => 'Печать',
            'class' => 'btn darkblue widthe-100 hidden-lg',
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="height: 34px;">
        <style>
            .dropdown-menu-mini{
                width: 100%;
                min-width: 98px;
                border-color: #4276a4 !important;
            }
            .dropdown-menu-mini a{
                padding: 7px 0px;
                text-align: center;
            }
        </style>
        <span <?= isset($_COOKIE["tooltip_pdf_{$model->company_id}"]) ?
            'class="dropup tooltip2 tooltipstered-main" data-tooltip-content="#tooltip_pdf_hide"' : 'class="dropup"'; ?>>
            <?= Html::a('Скачать', '#', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs dropdown-toggle',
                'data-toggle' => 'dropdown'
            ]); ?>
            <?= Html::a('<i class="glyphicon glyphicon-download" style="font-size: 17px;"></i>', '#', [
                'class' => 'btn darkblue widthe-100 hidden-lg dropdown-toggle',
                'data-toggle' => 'dropdown'
            ]); ?>
            <?= Dropdown::widget([
                'options' => [
                    'style' => '',
                    'class' => 'dropdown-menu-mini'
                ],
                'items' => [
                    [
                        'label' => '<span style="display: inline-block;">PDF</span> файл',
                        'encode' => false,
                        'url' => ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPdfFileName()],
                        'linkOptions' => [
                            'target' => '_blank',
                        ]
                    ],
                    [
                        'label' => '<span style="display: inline-block;">Word</span> файл',
                        'encode' => false,
                        'url' => ['docx', 'id' => $model->id, 'type' => $model->type],
                        'linkOptions' => [
                            'target' => '_blank',
                            'class' => 'get-word-link',
                        ]
                    ],
                ],
            ]);?>
        </span>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="height: 34px;">
        <?php if (Yii::$app->user->can(permissions\document\Invoice::CREATE, ['ioType' => $model->type])): ?>
            <?php if (\Yii::$app->user->identity->company->createInvoiceAllowed($model->type)) : ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => 'Копировать',
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    ],
                    'confirmUrl' => Url::to(['create',
                        'type' => $model->type, 'clone' => $model->id, 'contractorId' => $contractorId,]),
                    'message' => 'Вы уверены, что хотите скопировать этот счёт?',
                ]);
                ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => '<i class="fa fa-files-o fa-2x"></i>',
                        'title' => 'Копировать',
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                    ],
                    'confirmUrl' => Url::to(['create',
                        'type' => $model->type, 'clone' => $model->id, 'contractorId' => $contractorId,]),
                    'message' => 'Вы уверены, что хотите скопировать этот счёт?',
                ]);
                ?>
            <?php else : ?>
                <span class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs action-is-limited">Копировать</span>
                <span class="btn darkblue widthe-100 hidden-lg action-is-limited" title="Копировать"><i class="fa fa-files-o fa-2x"></i></span>
            <?php endif ?>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if ($model->isFullyPaid) : ?>
            <?php if (Yii::$app->user->can(UserRole::ROLE_CHIEF)): ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => 'Снять оплату',
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    ],
                    'confirmUrl' => Url::to(['unpaid',
                        'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,]),
                    'message' => 'Вы уверены, что нужно снять оплату со счета?',
                ]);
                ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => '<span class="ico-Paid-smart-pls fs"></span>',
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                    ],
                    'confirmUrl' => Url::to(['unpaid',
                        'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,]),
                    'message' => 'Вы уверены, что нужно снять оплату со счета?',
                ]);
                ?>
            <?php endif; ?>
        <?php else : ?>
            <?php if (!$model->is_deleted
                && $model->invoiceStatus->paymentAllowed($model->type)
                && Yii::$app->user->can(permissions\document\Invoice::ADD_CASH_FLOW)
            ): ?>
                <span <?= isset($_COOKIE["tooltip_paid_{$model->company_id}"]) ? 'class="tooltip2 tooltipstered-main" data-tooltip-content = "#tooltip_paid"' : ''; ?>>
                    <button type="button" class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs"
                            data-toggle="modal" href="#paid">
                        Оплачен
                    </button>
                    <button type="button" class="btn darkblue widthe-100 hidden-lg" data-toggle="modal" title="Оплачен"
                            href="#paid">
                        <span class="ico-Paid-smart-pls fs"></span>
                    </button>
                </span>
            <?php endif; ?>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="height: 34px;">
        <?php if ($model->invoice_status_id !== InvoiceStatus::STATUS_PAYED): ?>
            <button type="button" class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs" data-toggle="modal"
                    href="#payment-order">
                Платежка
            </button>
            <button type="button" class="btn darkblue widthe-100 hidden-lg" data-toggle="modal" title="Платежка"
                    href="#payment-order">
                <span class="fa fa-bank m-r-sm"></span>
            </button>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (!$model->is_deleted && Yii::$app->user->can(permissions\document\Document::DELETE)
            && in_array($model->invoice_status_id, InvoiceStatus::$deleteAllowed)
        ): ?>
            <button type="button" class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs" data-toggle="modal"
                    href="#delete-confirm">Удалить
            </button>
            <button type="button" class="btn darkblue widthe-100 hidden-lg" title="Удалить" data-toggle="modal"
                    href="#delete-confirm"><i class="fa fa-trash-o fa-2x"></i></button>
        <?php endif; ?>
    </div>
</div>

<div class="tooltip_templates container-tooltip_templates">
    <div id="tooltip_recreate" class="box-tooltip-templates">
        На основании этого счета будет выставлен счет вашему покупателю.
        Вам останется выбрать Покупателя и поставить нужную цену
    </div>
</div>
