<?php

use common\components\date\DateHelper;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\document\Invoice */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Шаблоны АвтоСчетов';
?>

<?php /*Pjax::begin([
    'id' => 'invoice-auto-toggle-pjax',
    'timeout' => 5000,
    'linkSelector' => '.invoice-auto-toggle',
])*/ ?>

<div class="portlet box">
    <div class="btn-group pull-right title-buttons">
        <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
            'ioType' => Documents::IO_TYPE_OUT,
        ])
        ): ?>
            <?php if (\Yii::$app->user->identity->company->createInvoiceAllowed(Documents::IO_TYPE_OUT)) : ?>
                <a href="<?= Url::toRoute(['create', 'type' => Documents::IO_TYPE_OUT, 'auto' => true]); ?>"
                   class="btn yellow">
                    <i class="fa fa-plus"></i> ДОБАВИТЬ
                </a>
            <?php else : ?>
                <button class="btn yellow action-is-limited">
                    <i class="fa fa-plus"></i> ДОБАВИТЬ
                </button>
            <?php endif ?>
        <?php endif; ?>
    </div>
    <h3 class="page-title"><?= $this->title ?></h3>
</div>

<div class="row portlet" id="widgets">
    <div class="col-md-9 col-sm-9">
        <span class="index-auto-invoice-text">
            Если вы ежемесячно выставляете счета за одни и те же услуги или товары и в назначении меняется только месяц
            и год, то настройте шаблон АвтоСчета. Счета автоматически будут формироваться и отправляться вашим клиентам
            в день и час, которые вы зададите.
        </span>
    </div>
    <div class="col-md-3 col-sm-3">
        <div class="dashboard-stat" style="height: 122px; position: relative;">
            <?= $this->render('_invoice-auto-toggle') ?>
        </div>
    </div>
</div>

<div class="portlet box darkblue" id="templates-table">
    <div class="portlet-title">
        <div class="caption">
            Список шаблонов АвтоСчетов
        </div>
        <div class="tools search-tools tools_button col-md-5 col-sm-5">
            <div class="form-body">
                <?php $form = \yii\widgets\ActiveForm::begin([
                    'method' => 'GET',
                    'action' => ['index-auto'],
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'options' => [
                            'class' => '',
                        ],
                    ],
                ]); ?>
                <div class="search_cont">
                    <div class="wimax_input ">
                        <?= $form->field($searchModel, 'find_by')->textInput([
                            'placeholder' => 'Номер шаблона АвтоСчета или название контрагента',
                        ]); ?>
                    </div>
                    <div class="wimax_button">
                        <?= Html::submitButton('НАЙТИ', [
                            'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                        ]) ?>
                    </div>
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
        <div class="actions joint-operations col-md-4 col-sm-4" style="display:none;">
            <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete-auto', [
                'class' => 'btn btn-default btn-sm',
                'data-toggle' => 'modal',
            ]); ?>
            <?= Html::a('<i class="glyphicon glyphicon-ban-circle"></i> Остановить', '#many-stop', [
                'class' => 'btn btn-default btn-sm',
                'data-toggle' => 'modal',
            ]); ?>
            <?= \frontend\widgets\ConfirmModalWidget::widget([
                'options' => [
                    'id' => 'many-stop',
                ],
                'toggleButton' => false,
                'confirmUrl' => Url::toRoute(['autoinvoice/many-stop']),
                'confirmParams' => [],
                'message' => 'Вы уверены, что хотите остановить выбранные шаблоны?',
            ]); ?>
            <?= \frontend\widgets\ConfirmModalWidget::widget([
                'options' => [
                    'id' => 'many-delete-auto',
                ],
                'toggleButton' => false,
                'confirmUrl' => Url::toRoute(['autoinvoice/many-delete']),
                'confirmParams' => [],
                'message' => 'Вы уверены, что хотите удалить выбранные шаблоны?',
            ]); ?>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?= $this->render('_template_table', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]); ?>
        </div>
    </div>
</div>

<?php /*Pjax::end();*/ ?>
