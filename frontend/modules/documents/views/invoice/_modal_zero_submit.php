<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $form string */

$js = <<<JS
$(document).on('beforeSubmit', '#{$form}', function (e) {
    if ($('#modal-zero-submit.in').length) {
        $('#modal-zero-submit').modal('hide');
        var l = Ladda.create($('.ladda-button', this).get(0));
        l.start();

        return true;
    } else if (parseFloat($('.table-resume .amount', this).text()) > 0) {
        var hasZero = false;
        $('.product-row .price-with-nds').each(function(i, item) {
            if (parseFloat($(item).text()) == 0) {
                hasZero = true;
            }
        });
        if (hasZero) {
            $('#modal-zero-submit').modal('show');
            Ladda.stopAll();

            return false;
        }
    }

    return true;
});
JS;

$this->registerJs($js);
?>

<?php Modal::begin([
    'id' => 'modal-zero-submit',
    'closeButton' => false,
    'toggleButton' => false,
]); ?>

    <div style="margin-bottom: 30px; text-align: center;font-size: 16px;">
        В одном из наименований цена = "0".<br>
        Если это ошибка, то вернитесь и поправьте.<br>
        Если так и должно быть, нажмите "Сохранить".
    </div>
    <div class="form-actions row">
        <div class="col-xs-6">
            <?= Html::submitButton('Сохранить', [
                'class' => 'btn darkblue text-white pull-right',
                'form' => $form,
                'value' => 'submit',
            ]) ?>
        </div>
        <div class="col-xs-6">
            <?= Html::button('Вернуться', [
                'class' => 'btn darkblue text-white',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
    </div>

<?php Modal::end(); ?>