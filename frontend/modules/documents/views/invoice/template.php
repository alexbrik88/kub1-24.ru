<?php

use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\ImageHelper;
use common\components\TextHelper;
use common\models\company\CompanyType;
use common\models\currency\Currency;
use common\models\document\InvoiceContractorSignature;
use common\models\document\status\InvoiceStatus;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\modules\documents\components\InvoiceHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\document\Invoice;
use common\models\document\Autoinvoice;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */
/* @var $addStamp boolean */
/* @var $showHeader boolean */

common\assets\DocumentTemplateAsset::register($this);
$this->params['asset'] = common\assets\DocumentTemplateAsset::class;

$company = $model->company;
$contractor = $model->contractor;

$signatureHeight = $accountantSignatureHeight = $defaultSignatureHeight = 50;

$showHeader = ArrayHelper::getValue($_params_, 'showHeader', true);
$addStamp = ArrayHelper::getValue($_params_, 'addStamp', false);
$accountantSignatureLink = $signatureLink = $printLink = $logoLink = null;
if ($model->type == Documents::IO_TYPE_OUT) {
    $logoLink = EasyThumbnailImage::thumbnailSrc(
        $company->getImage('logoImage'),
        300,
        100,
        EasyThumbnailImage::THUMBNAIL_INSET_BOX
    );
    if ($addStamp) {
        if ($model->signed_by_name) {
            if ($model->employeeSignature) {
                $accountantSignatureHeight = $signatureHeight = ImageHelper::getSignatureHeight($model->employeeSignature->file ?? null, 50, 100);
                $accountantSignatureLink = $signatureLink = $model->employeeSignature ? EasyThumbnailImage::thumbnailSrc(
                    $model->employeeSignature->file,
                    165,
                    $accountantSignatureHeight,
                    EasyThumbnailImage::THUMBNAIL_INSET_BOX
                ) : null;
            }
        } else {
            $signatureHeight = ImageHelper::getSignatureHeight($model->getImage('chiefSignatureImage'), 50, 100);
            $signatureLink = EasyThumbnailImage::thumbnailSrc(
                $model->getImage('chiefSignatureImage'),
                165,
                $signatureHeight,
                EasyThumbnailImage::THUMBNAIL_INSET_BOX
            );

            if (!$company->chief_is_chief_accountant) {
                $accountantSignatureHeight = ImageHelper::getSignatureHeight($model->getImage('chiefAccountantSignatureImage'), 50, 100);
                $accountantSignatureLink = EasyThumbnailImage::thumbnailSrc(
                    $model->getImage('chiefAccountantSignatureImage'),
                    165,
                    $accountantSignatureHeight,
                    EasyThumbnailImage::THUMBNAIL_INSET_BOX
                );
            } else {
                $accountantSignatureHeight = $signatureHeight;
                $accountantSignatureLink = $signatureLink;
            }
        }
        $printLink = EasyThumbnailImage::thumbnailSrc(
            $model->getImage('printImage'),
            150,
            150,
            EasyThumbnailImage::THUMBNAIL_INSET_BOX
        );
    }
}
$invoiceContractorSignature = $company->invoiceContractorSignature;
if (!$invoiceContractorSignature) {
    $invoiceContractorSignature = new InvoiceContractorSignature();
    $invoiceContractorSignature->company_id = $model->company_id;
}

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->document_date,
]);
$isAuto = $model->invoice_status_id == InvoiceStatus::STATUS_AUTOINVOICE;
$ndsName = $model->ndsViewType->name;
$ndsValue = $model->hasNds ? TextHelper::invoiceMoneyFormat($model->view_total_nds, 2) : '-';
$precision = $model->price_precision;
$isArticle = $model->show_article;
$currCode = $model->currency_name == Currency::DEFAULT_NAME ? '' : " ({$model->currency_name})";

?>

<div class="document-template invoice-template">
    <?PHP IF ($showHeader) : ?>
        <table>
            <tbody>
                <tr>
                    <td class="p0ad doc-title" style="width: 60%;">
                        <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                            <?= strpos($model->company_name_short, 'ИП ') === 0 ?
                                $model->company_name_full :
                                $model->company_name_short; ?>
                        <?php else : ?>
                            <?= $model->contractor_name_short; ?>
                        <?php endif; ?>
                    </td>
                    <td class="p0ad text-r" style="width: 40%;">
                        <?php if ($logoLink) : ?>
                            <img src="<?= $logoLink ?>" alt="" style="max-height: 100px; max-width: 100%">
                        <?php endif; ?>
                    </td>
                </tr>
            </tbody>
        </table>
    <?PHP endif ?>

    <table class="" style="margin: 10px 0;">
        <tbody>
            <tr>
                <td colspan="2" rowspan="2" class="bor v-al-t bor0-b" style="width: 50%;">
                    <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                        <?= $model->company_bank_name; ?>
                    <?php else : ?>
                        <?= $contractor->bank_name; ?>
                    <?php endif; ?>
                </td>
                <td class="bor" style="width: 15%;">
                    БИК
                </td>
                <td class="bor bor0-b" style="width: 35%;">
                    <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                        <?= $model->company_bik; ?><br/>
                    <?php else : ?>
                        <?= $contractor->BIC; ?>
                    <?php endif; ?>
                </td>
            </tr>

            <tr>
                <td class="bor v-al-t bor0-b" style="width: 15%;">
                    Сч. №
                </td>
                <td  rowspan="2" class="bor v-al-t bor0-t" style="width: 35%;">
                    <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                        <?= $model->company_ks; ?>
                    <?php else : ?>
                        <?= $contractor->corresp_account; ?>
                    <?php endif; ?>
                </td>
            </tr>

            <tr>
                <td colspan="2" class="bor bor0-t" style="width: 50%;">
                    Банк получателя
                </td>
                <td class="bor v-al-t bor0-t" style="width: 15%;">
                    &nbsp;
                </td>
            </tr>

            <tr>
                <td class="bor" style="width: 25%;">
                    ИНН
                    <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                        <?= $model->company_inn; ?>
                    <?php else : ?>
                        <?= $contractor->ITN; ?>
                    <?php endif; ?>
                </td>
                <td class="bor" style="width: 25%;">
                    КПП
                    <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                        <?= $model->company_kpp; ?>
                    <?php else : ?>
                        <?= $contractor->PPC; ?>
                    <?php endif; ?>
                </td>
                <td class="bor v-al-t bor0-b" style="width: 15%;">
                    Сч. №
                </td>
                <td rowspan="4" class="bor v-al-t" style="width: 35%;">
                    <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                        <?= $model->company_rs; ?>
                    <?php else : ?>
                        <?= $contractor->current_account; ?>
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td rowspan="2" colspan="2" class="bor v-al-t bor0-b" style="width: 50%;">
                    <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                        <?= strpos($model->company_name_short, 'ИП ') === 0 ?
                            $model->company_name_full :
                            $model->company_name_short; ?>
                    <?php else : ?>
                        <?= $model->contractor_name_short; ?>
                    <?php endif; ?>
                </td>
                <td class="bor v-al-t bor0-t bor0-b" style="width: 15%;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="bor v-al-t bor0-t bor0-b" style="width: 15%;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" class="bor bor0-t" style="width: 50%;">
                    Получатель
                </td>
                <td class="bor v-al-t bor0-t" style="width: 15%;">
                    &nbsp;
                </td>
            </tr>
        </tbody>
    </table>

    <div class="doc-main-title text-c" style="margin: 12px 0;">
        <?php if ($model->is_invoice_contract && $model->type == Documents::IO_TYPE_OUT) : ?>
            Счет-договор
        <?php else : ?>
            Счет
        <?php endif; ?>
        №
        <?= $isAuto ? '<i class="auto_tpl">##</i>' : $model->fullNumber; ?>
        от
        <?= $isAuto ? '<i class="auto_tpl">## ###</i> 20<i class="auto_tpl">##</i> г.' : $dateFormatted; ?>
    </div>

    <div style="border-bottom: 2px solid #000000;"></div>

    <table style="margin: 5px 0;">
        <tr>
            <td class="v-al-t">
                Поставщик<br/>(Исполнитель):
            </td>
            <td class="font-b">
                <?= $model->getExecutorFullRequisites(); ?>
            </td>
        </tr>
        <tr>
            <td class="v-al-t pad-t-5">
                Покупатель<br/>(Заказчик):
            </td>
            <td class="pad-t-5 font-b">
                <?= $model->getCustomerFullRequisites(); ?>
            </td>
        </tr>
        <?php if ($model->basis_document_name && $model->basis_document_number && $model->basis_document_date) : ?>
            <tr>
                <td class="v-al-t pad-t-5">
                    Основание:
                </td>
                <td class="pad-t-5 font-b">
                    <?= $model->basis_document_name ?>
                    № <?= Html::encode($model->basis_document_number) ?>
                    от <?= DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                </td>
            </tr>
        <?php endif ?>
    </table>

    <table class="" style="margin: 8px 0; border: 2px solid #000000;">
        <thead>
            <tr>
                <th class="bor">№</th>
                <?php if ($isArticle) : ?>
                    <th class="bor">Артикул</th>
                <?php endif ?>
                <th class="bor">Товары (работы, услуги)</th>
                <th class="bor">Кол-во</th>
                <th class="bor">Ед.</th>
                <?php if ($model->has_weight) : ?>
                    <th class="bor"> Вес, кг</th>
                <?php endif; ?>
                <?php if ($model->has_volume) : ?>
                    <th class="bor"> Объем, м2</th>
                <?php endif; ?>
                <th class="bor">Цена</th>
                <?php if ($model->has_discount && !$model->is_hidden_discount) : ?>
                    <th class="bor">
                        <?= ((YII_ENV_PROD && $model->company_id == 9888 ? 'Агентское вознаграждение' : 'Скидка') .
                            ($model->discount_type == Invoice::DISCOUNT_TYPE_RUBLE ? ' (руб.)' : ' %')); ?>
                    </th>
                    <th class="bor">
                        <?php if (YII_ENV_PROD && $model->company_id == 9888) : ?>
                            Цена с агентским вознаграждением
                        <?php else : ?>
                            Цена со скидкой
                        <?php endif; ?>
                    </th>
                <?php endif ?>
                <th class="bor">Сумма</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($model->orders as $order) : ?>
                <?php
                $productTitle = $order->product_title;
                if ($isAuto && $model->auto->add_month_and_year &&
                    $order->product->production_type == Product::PRODUCTION_TYPE_SERVICE
                ) {
                    switch ($model->auto->period) {
                        case Autoinvoice::MONTHLY:
                            $productTitle .= '<i class="auto_tpl"> за месяц 20##г.</i>';
                            break;
                        case Autoinvoice::QUARTERLY:
                            $productTitle .= '<i class="auto_tpl"> за # квартал 20##г.</i>';
                            break;
                        case Autoinvoice::HALF_YEAR:
                            $productTitle .= '<i class="auto_tpl"> за # полугодие 20##г.</i>';
                            break;
                        case Autoinvoice::ANNUALLY:
                            $productTitle .= '<i class="auto_tpl"> за 20## год.</i>';
                            break;
                    }
                }
                $unitName = $order->unit ? $order->unit->name : Product::DEFAULT_VALUE;
                if ($order->quantity != intval($order->quantity)) {
                    $order->quantity = rtrim(number_format($order->quantity, 10, '.', ''), 0);
                }
                ?>
                <tr>
                    <td class="bor" style="text-align: center; width: 5%">
                        <?= $order->number; ?>
                    </td>
                    <?php if ($isArticle) : ?>
                        <td class="bor" style="width: 15%">
                            <?= $order->article; ?>
                        </td>
                    <?php endif ?>
                    <td class="bor" style="word-break: break-word;">
                        <?= $productTitle ?>
                    </td>
                    <td class="bor" style="text-align: right; width: 10%">
                        <?= $unitName == Product::DEFAULT_VALUE ? $unitName : str_replace('.', ',', $order->quantity); ?>
                    </td>
                    <td class="bor" style="text-align: right; width: 7%">
                        <?= $unitName ?>
                    </td>
                    <?php if ($model->has_weight) : ?>
                        <td class="bor" style="text-align: right; width: 7%">
                            <?= ($order->weight) ? TextHelper::invoiceMoneyFormat($order->weight * 100, $model->weightPrecision) : ''; ?>
                        </td>
                    <?php endif; ?>
                    <?php if ($model->has_volume) : ?>
                        <td class="bor" style="text-align: right; width: 7%">
                            <?= ($order->volume) ? TextHelper::invoiceMoneyFormat($order->volume * 100, $model->volumePrecision) : ''; ?>
                        </td>
                    <?php endif; ?>
                    <?php if ($model->has_discount && !$model->is_hidden_discount) : ?>
                        <td class="bor" style="text-align: right; width: 13%">
                            <?= TextHelper::invoiceMoneyFormat($order->view_price_base, $precision); ?>
                        </td>
                        <td class="bor" style="text-align: right; width: 13%">
                            <?= $order->getDiscountViewValue() ?>
                        </td>
                    <?php endif ?>
                    <td class="bor" style="text-align: right; width: 13%">
                        <?= TextHelper::invoiceMoneyFormat($order->view_price_one, $precision); ?>
                    </td>
                    <td class="bor" style="text-align: right; width: 13%">
                        <?= TextHelper::invoiceMoneyFormat($order->view_total_amount, $precision); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <table>
        <tbody>
            <?php if ($model->has_discount && !$model->is_hidden_discount) : ?>
                <tr>
                    <td class="text-r" style="width: 85%;">
                        <?= YII_ENV_PROD && $model->company_id == 9888 ? 'Агентское вознаграждение' : 'Сумма скидки'; ?><?= $currCode ?>:
                    </td>
                    <td class="text-r" style="width: 15%;">
                        <b><?= TextHelper::invoiceMoneyFormat($model->view_total_discount, 2); ?></b>
                    </td>
                </tr>
            <?php endif ?>
            <tr>
                <td class="text-r" style="width: 85%;">Итого<?= $currCode ?>:</td>
                <td class="text-r" style="width: 15%;">
                    <b><?= TextHelper::invoiceMoneyFormat($model->view_total_amount, 2); ?></b>
                </td>
            </tr>
            <tr>
                <td class="text-r" style="width: 85%;"><?= $ndsName ?>:</td>
                <td class="text-r" style="width: 15%;">
                    <b><?= $ndsValue ?></b>
                </td>
            </tr>
            <tr>
                <td class="text-r" style="width: 85%;">Всего к оплате<?= $currCode ?>:</td>
                <td class="text-r" style="width: 15%;">
                    <b><?= TextHelper::invoiceMoneyFormat($model->view_total_with_nds, 2); ?></b>
                </td>
            </tr>
        </tbody>
    </table>

    <div class="mar-t-10">
        Всего наименований <?= count($model->orders) ?>, на сумму
        <?= TextHelper::invoiceMoneyFormat($model->view_total_with_nds, 2); ?>
        <?= $model->currency_name == Currency::DEFAULT_NAME ? 'руб' : $model->currency_name ?>.
    </div>
    <div style="font-weight: bold;">
        <?= TextHelper::mb_ucfirst(Currency::textPrice($model->view_total_with_nds / 100, $model->currency_name)); ?>
    </div>
    <?php if ($model->has_weight) : ?>
        <div>
            Общий вес:
            <?= TextHelper::invoiceMoneyFormat(100 * InvoiceHelper::getTotalWeight($model), $model->weightPrecision) ?> кг
        </div>
    <?php endif; ?>
    <?php if ($model->has_volume) : ?>
        <div>
            Общий объем:
            <?= TextHelper::invoiceMoneyFormat(100 * InvoiceHelper::getTotalVolume($model), $model->volumePrecision) ?> м2
        </div>
    <?php endif; ?>
    <?php if ($model->is_invoice_contract && $model->type == Documents::IO_TYPE_OUT) : ?>
        <?php if ($model->contract_essence) : ?>
            <div style="padding-top: 10px; font-weight: bold;">
                Предмет договора:
            </div>
            <div class="txt-9" style="padding-top: 0;word-break: break-word;">
                <?= nl2br(Html::encode($model->contract_essence)) ?>
            </div>
        <?php endif; ?>
    <?php else : ?>
        <?php if ($model->comment) : ?>
            <div style="padding-top: 10px;">
                <?= nl2br(Html::encode($model->comment)) ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <div style="border-bottom: 2px solid #000000; margin: 5px 0;"></div>

    <?php if ($model->is_invoice_contract && $model->type == Documents::IO_TYPE_OUT && $invoiceContractorSignature->is_active) : ?>
        <div style=" -webkit-print-color-adjust: exact;">
            <div>
                <br/>
                <br/>
                <div style="font-size: 12pt;">Адреса и реквизиты сторон:</div>
                <br/>
                <table class="no-b" style="width: 100%; table-layout: fixed; -webkit-print-color-adjust: exact;">
                    <col width="50%"/>
                    <col width="50%"/>
                    <tbody>
                    <tr>
                        <td class="txt-t" style="font-size:11pt;">
                            <b>Исполнитель</b>
                        </td>
                        <td class="txt-t" style="font-size:11pt;">
                            <b>Заказчик</b>
                        </td>
                    </tr>
                    <tr>
                        <td class="txt-t" style="vertical-align: top;">
                            <?= strpos($model->company_name_short, 'ИП ') === 0 ?
                                $model->company_name_full :
                                $model->company_name_short; ?>
                        </td>
                        <td class="txt-t" style="vertical-align: top;">
                            <?= $model->contractor_name_short; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="txt-t">
                            <b>ИНН</b>
                            <?= $model->company_inn; ?>
                        </td>
                        <td class="txt-t">
                            <b>ИНН</b>
                            <?= $model->contractor_inn; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="txt-t">
                            <b>КПП</b>
                            <?= $model->company_kpp; ?>
                        </td>
                        <td class="txt-t">
                            <b>КПП</b>
                            <?= $model->contractor_kpp; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="txt-t">
                            <b><?= $company->company_type_id == CompanyType::TYPE_IP ? 'ОГРНИП' : 'ОГРН'; ?></b>
                            <?= $company->company_type_id == CompanyType::TYPE_IP ?
                                $model->company_egrip : $company->ogrn; ?>
                        </td>
                        <td class="txt-t">
                            <b><?= $contractor->company_type_id == CompanyType::TYPE_IP ? 'ОГРНИП' : 'ОГРН'; ?></b>
                            <?= $contractor->BIN; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="txt-t" style="vertical-align: top;">
                            <b>Юридический адрес</b>
                            <?= $model->company_address_legal_full; ?>
                        </td>
                        <td class="txt-t" style="vertical-align: top;">
                            <b>Юридический адрес</b>
                            <?= $model->contractor_address_legal_full; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="txt-t">
                            <b>Р/сч</b>
                            <?= $model->company_rs; ?><br>
                            <?= $model->company_bank_name ? ('в ' . $model->company_bank_name) : '<br>'; ?>
                        </td>
                        <td class="txt-t">
                            <b>Р/сч</b>
                            <?= $model->contractor_rs; ?><br>
                            <?= $model->contractor_bank_name ? ('в ' . $model->contractor_bank_name) : '<br>'; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="txt-t">
                            <b>Кор.счет</b>
                            <?= $model->company_ks; ?>
                        </td>
                        <td class="txt-t">
                            <b>Кор.счет</b>
                            <?= $model->contractor_ks; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="txt-t">
                            <b>БИК</b>
                            <?= $model->company_bik; ?>
                        </td>
                        <td class="txt-t">
                            <b>БИК</b>
                            <?= $model->contractor_bik; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="txt-t">
                            <br><br>
                            <?= $company->company_type_id == CompanyType::TYPE_IP ? 'Предприниматель' : 'Руководитель'; ?>
                        </td>
                        <td class="txt-t">
                            <br><br>
                            <?= $contractor->company_type_id == CompanyType::TYPE_IP ? 'Предприниматель' : 'Руководитель'; ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div style="height: 225px;
                    <?php if ($printLink) : ?>
                    background-image: url('<?= $printLink ?>');
                    background-repeat: no-repeat;
                    background-position: 130px 75px;
                    background-size: 22.2%;
                    background-image-resize: 0;
                    background-image-resolution: from-image;
                    <?php endif ?>
                    -webkit-print-color-adjust: exact;">
                <br/>
                <table class="podp-r va-bottom" style="background: no-repeat  255px 2px;  width: 675px; -webkit-print-color-adjust: exact;">
                    <tr>
                        <td class="pad-line"
                            style="border: none;border-bottom: 1px solid #000000;text-align: center; margin: 0 0 0 0; width: 20%;">
                            <img style="width: 128px; <?= 'margin-bottom:'.($defaultSignatureHeight - $signatureHeight).'px' ?>" src="<?= $signatureLink ?>" alt="">
                        </td>
                        <td style="border: none;width: 4%"></td>
                        <td class="pad-line"
                            style="border: none;border-bottom: 1px solid #000000;text-align: center; margin: 0 0 0 0; width: 20%; padding-top: 30px;">
                            <?= $model->signed_by_name ? $model->signed_by_name : $model->getCompanyChiefFio(true); ?>
                            <?php if ($model->signed_by_employee_id) : ?>
                                <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                                <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                            <?php endif ?>
                        </td>
                        <td style="border: none;width: 12%"></td>
                        <td class="pad-line"
                            style="border: none;border-bottom: 1px solid #000000;text-align: center; margin: 0 0 0 0; width: 20%; height: 40px!important;"></td>
                        <td style="border: none;width: 4%"></td>
                        <td class="pad-line"
                            style="border: none;border-bottom: 1px solid #000000;text-align: center; margin: 0 0 0 0; width: 20%; padding-top: 30px;">
                            <?= $contractor->getDirectorFio(); ?>
                        </td>
                    </tr>
                    <tr class="small-txt">
                        <td style="border: none;text-align: center; margin: 0 0 0 0; width: 20%; vertical-align: top;">
                            подпись
                        </td>
                        <td style="border: none;width: 4%"></td>
                        <td style="border: none;text-align: center; margin: 0 0 0 0; width: 20%; vertical-align: top;">
                            расшифровка подписи
                        </td>
                        <td style="border: none;width: 12%"></td>
                        <td style="border: none;text-align: center; margin: 0 0 0 0; width: 20%; vertical-align: top;">
                            подпись
                        </td>
                        <td style="border: none;width: 4%"></td>
                        <td style="border: none;text-align: center; margin: 0 0 0 0; width: 20%; vertical-align: top;">
                            расшифровка подписи
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    <?php else : ?>
        <?php if ((
            $model->type == Documents::IO_TYPE_OUT &&
            $company->company_type_id != CompanyType::TYPE_IP &&
            !$company->self_employed
        ) || (
            $model->type == Documents::IO_TYPE_IN &&
            $contractor->companyTypeId != CompanyType::TYPE_IP
        )) : ?>
            <div style="height: 200px;
                        <?php if ($printLink) : ?>
                        background-image: url('<?= $printLink ?>');
                        background-repeat: no-repeat;
                        background-position: 95% bottom;
                        background-size: 22.2%;
                        background-image-resize: 0;
                        background-image-resolution: from-image;
                        <?php endif ?>
                        -webkit-print-color-adjust: exact;">
                <table style="table-layout: fixed; margin-top: 20px; -webkit-print-color-adjust: exact;">
                    <tr>
                        <td class="font-b v-al-b" style="width: 16%;">
                            Руководитель:
                        </td>
                        <td style="width: 3%;"></td>
                        <td class="text-c v-al-b" style="border-bottom: 1px solid #000000; width: 25%;">
                            <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                                <?= ($model->signed_by_employee_id && $model->signEmployeeCompany) ?
                                    $model->signEmployeeCompany->position : $model->company_chief_post_name; ?>
                            <?php else : ?>
                                <?= $contractor->director_post_name; ?>
                            <?php endif; ?>
                        </td>
                        <td style="width: 3%;">
                        </td>
                        <td class="text-c p0ad v-al-b"
                            style="border-bottom: 1px solid #000000; position: relative; width: 25%;">
                            <?php if ($signatureLink) : ?>
                                <img src="<?= $signatureLink ?>" style="max-width: 100%; max-height: 100%; <?= 'margin-bottom:'.($defaultSignatureHeight - $signatureHeight).'px' ?>">
                            <?php endif; ?>
                        </td>
                        <td style="width: 3%;"></td>
                        <td class="text-c v-al-b"
                            style="border-bottom: 1px solid #000000; width: 25%;">
                            <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                                <?= $model->signed_by_name ? $model->signed_by_name : $model->getCompanyChiefFio(true); ?>
                                <?php if ($model->signed_by_employee_id) : ?>
                                    <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                                    <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                                <?php endif ?>
                            <?php else : ?>
                                <?= $contractor->director_name; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr class="small-txt va-top">
                        <td colspan="2" style="width: 19%;"></td>
                        <td class="p0ad v-al-t text-c" style="width: 25%;">
                            должность
                        </td>
                        <td style="width: 3%;"></td>
                        <td class="p0ad v-al-t text-c" style="width: 25%;">
                            подпись
                        </td>
                        <td style="width: 3%;"></td>
                        <td class="p0ad v-al-t text-c" style="width: 25%;">
                            расшифровка
                            подписи
                        </td>
                    </tr>
                </table>

                <table style="table-layout: fixed; margin-top: 20px; -webkit-print-color-adjust: exact;">
                    <tbody>
                        <tr>
                            <td class="font-b v-al-b" style="width: 30%;">
                                Главный (старший) бухгалтер:
                            </td>
                            <td style="width: 5%;"></td>
                            <td class="p0ad text-c v-al-b" style="border-bottom: 1px solid #000000; position: relative; width: 30%;">
                                <?php if ($accountantSignatureLink) : ?>
                                    <img src="<?= $accountantSignatureLink ?>" style="max-width: 100%; max-height: 100%; <?= 'margin-bottom:'.($defaultSignatureHeight - $accountantSignatureHeight).'px' ?>">
                                <?php endif; ?>
                            </td>
                            <td style="width: 5%;"></td>
                            <td class="text-c v-al-b"
                                style="border-bottom: 1px solid #000000; width: 30%;">
                                <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                                    <?= $model->signed_by_name ? $model->signed_by_name : $model->getCompanyChiefAccountantFio(true); ?>
                                    <?php if ($model->signed_by_employee_id) : ?>
                                        <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                                        <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                                    <?php endif ?>
                                <?php else : ?>
                                    <?php if ($contractor->chief_accountant_is_director) : ?>
                                        <?= $contractor->director_name; ?>
                                    <?php else : ?>
                                        <?= $contractor->chief_accountant_name; ?>
                                    <?php endif ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr class="small-txt">
                            <td style="width: 30%;"></td>
                            <td style="width: 5%;"></td>
                            <td class="text-c p0ad v-al-t" style="width: 30%;">
                                подпись
                            </td>
                            <td style="width: 5%;"></td>
                            <td class="text-c p0ad v-al-t" style="width: 30%;">
                                расшифровка подписи
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        <?php else : ?>
            <div style="height: 200px;
                        <?php if ($printLink) : ?>
                        background: url('<?= $printLink ?>');
                        background-repeat: no-repeat;
                        background-size: 22.2%;
                        background-position: 65% 0;
                        <?php endif ?>
                        -webkit-print-color-adjust: exact;">
                <table class="mar-t-10" style="-webkit-print-color-adjust: exact;">
                    <tr>
                        <td class="font-b v-al-b" style="width: 20%;">
                            <?php if ($company->self_employed) : ?>
                                Самозанятый
                            <?php else : ?>
                                Предприниматель
                            <?php endif ?>
                        </td>
                        <td style="width: 5%;"></td>
                        <td class="text-c p0ad" style="border-bottom: 1px solid #000000; width: 35%;">
                            <?php if ($model->type == Documents::IO_TYPE_OUT && $signatureLink) : ?>
                                <img src="<?= $signatureLink ?>" style="max-width: 100%; max-height: 100%; <?= 'margin-bottom:'.($defaultSignatureHeight - $signatureHeight).'px' ?>">
                            <?php endif; ?>
                        </td>
                        <td style="width: 5%;"></td>
                        <td class="text-c v-al-b" style="border-bottom: 1px solid #000000; width: 35%;">
                            <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                                <?= $model->signed_by_name ? $model->signed_by_name : $model->getCompanyChiefFio(true); ?>
                                <?php if ($model->signed_by_employee_id) : ?>
                                    <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                                    <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                                <?php endif ?>
                            <?php else : ?>
                                <?= $contractor->director_name; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr class="small-txt">
                        <td colspan="2"  style="width: 25%;"></td>
                        <td class="text-c"  style="width: 35%;">
                            подпись
                        </td>
                        <td  style="width: 5%;"></td>
                        <td class="text-c"  style="width: 35%;">
                            расшифровка подписи
                        </td>
                    </tr>
                </table>
            </div>
        <?php endif; ?>
    <?php endif; ?>
</div>
