<?php

use common\models\company\CompanyType;
use common\models\Contractor;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $contractor common\models\Contractor */
/* @var $form yii\widgets\ActiveForm */

$hasAdditionals = false;
$companyType = CompanyType::find()
    ->where(['in_contractor' => true])
    ->select(['id', 'name_short'])
    ->indexBy('name_short')->column();
$companyType = json_encode($companyType);
$isLegal = $contractor->face_type == Contractor::TYPE_LEGAL_PERSON;
?>

<div class="contractor-type-legal collapse<?= $isLegal ? ' in' : ''; ?>">
    <div class="row">
        <div class="col-sm-5">
            <?= $form->field($contractor, 'ITN')->textInput([
                'maxlength' => true,
            ])->label('<span class="is-empty">Автозаполнение по </span>ИНН') ?>
        </div>
        <div class="col-sm-5">
            <?= $form->field($contractor, 'PPC')->textInput([
                'maxlength' => true,
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-2">
            <?= $form->field($contractor, 'company_type_id')->hint(false)->dropDownList($contractor->getTypeArray(), [
                'id' => 'contractor-type_id',
                'style' => 'height: auto;',
                'placeholder' => '',
            ])->label('Форма') ?>
        </div>
        <div class="col-sm-10">
            <?= $form->field($contractor, 'name')->hint(false)->textInput([
                'maxlength' => true,
            ])->label('Наименование') ?>
        </div>
    </div>
</div>

<div class="contractor-type-fisical collapse<?= $isLegal ? '' : ' in'; ?>">
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($contractor, 'physical_fio')->textInput([
                'maxlength' => true,
            ]) ?>
        </div>
    </div>
</div>

<?= $form->field($contractor, 'director_email')->textInput([
    'maxlength' => true,
])->hint('Для отправки счетов') ?>

<div class="row">
    <div class="col-xs-12" style="margin: 15px 0 0;">
        <?= Html::a('Указать дополнительные данные', '#add_contractor_additional', [
            'class' => 'collapse-toggle-link text-bold',
            'data-toggle' => 'collapse',
            'aria-expanded' => 'true',
            'aria-controls' => 'add_contractor_additional',
            'style' => 'position: relative;',
        ]) ?>
        <?= Html::tag('span', '', [
            'class' => 'tooltip2 ico-question valign-middle',
            'style' => 'margin-left: 3px;',
            'data-tooltip-content' => '#tooltip_contractor_additional',
        ]); ?>
    </div>
    <div class="col-xs-12 collapse" id="add_contractor_additional">
        <div class="contractor-type-legal collapse<?= $isLegal ? ' in' : ''; ?>">
            <?= $form->field($contractor, 'legal_address')->textInput([
                'maxlength' => true,
            ]) ?>

            <div class="row">
                <div class="col-sm-5">
                    <?= $form->field($contractor, 'director_post_name')->textInput([
                        'maxlength' => true,
                    ])->label('Должность руководителя') ?>
                </div>
                <div class="col-sm-7">
                    <?= $form->field($contractor, 'director_name')->textInput([
                        'maxlength' => true,
                    ])->label('ФИО руководителя') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-7">
                    <?= $form->field($contractor, 'current_account')->textInput([
                        'maxlength' => true,
                    ])->label('Расчётный счёт')->hint('Нужен для Актов и Товарных накладных') ?>
                </div>
                <div class="col-sm-5">
                    <?= $form->field($contractor, 'BIC')->textInput([
                        'maxlength' => true,
                        'class' => 'form-control input-sm dictionary-bik' . ($contractor->BIC ? ' edited' : ''),
                        'data' => [
                            'url' => Url::to(['/dictionary/bik']),
                            'target-name' => '#' . Html::getInputId($contractor, 'bank_name'),
                            'target-city' => '#' . Html::getInputId($contractor, 'bank_city'),
                            'target-ks' => '#' . Html::getInputId($contractor, 'corresp_account'),
                            'target-collapse' => '#contractor-bank-block',
                        ],
                    ])->label('БИК банка')->hint('Нужен для Актов и Товарных накладных') ?>
                </div>
            </div>

            <div id="contractor-bank-block" class="collapse<?= $contractor->BIC ? ' in' : ''; ?>">
                <div class="row">
                    <div class="col-sm-7">
                        <?= $form->field($contractor, 'bank_name')->textInput([
                            'maxlength' => true,
                            'readonly' => true,
                        ]) ?>
                    </div>
                    <div class="col-sm-5">
                        <?= $form->field($contractor, 'bank_city')->textInput([
                            'maxlength' => true,
                            'readonly' => true,
                        ]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-7">
                        <?= $form->field($contractor, 'corresp_account')->textInput([
                            'maxlength' => true,
                            'readonly' => true,
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="contractor-type-fisical collapse<?= $isLegal ? '' : ' in'; ?>">
            <?= $form->field($contractor, 'physical_address')->textInput([
                'maxlength' => true,
            ])->label('Адрес регистрации') ?>

            <div class="row">
                <div class="col-sm-4">
                    <?= $form->field($contractor, 'physical_passport_series')->textInput()->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '9{2} 9{2}',
                        'options' => [
                            'class' => 'form-control input-sm' . ($contractor->physical_passport_series ? ' edited' : ''),
                        ],
                        'clientOptions' => [
                            'showMaskOnHover' => false,
                        ]
                    ]) ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($contractor, 'physical_passport_number')->textInput()->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '9{6}',
                        'options' => [
                            'class' => 'form-control input-sm' . ($contractor->physical_passport_number ? ' edited' : ''),
                        ],
                        'clientOptions' => [
                            'showMaskOnHover' => false,
                        ]
                    ]) ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($contractor, 'physical_passport_date_output')->textInput([
                        'maxlength' => true,
                        'class' => 'form-control input-sm date-picker' . ($contractor->physical_passport_date_output ? ' edited' : ''),
                        'autocomplete' => 'off',
                    ])->label('Дата выдачи') ?>
                    <?= Html::tag('i', '', [
                        'class' => 'fa fa-calendar',
                        'style' => 'position: absolute; top: 30px; right: 15px; color: #cecece; cursor: pointer;',
                    ]) ?>
                </div>
                <div class="col-sm-12">
                    <?= $form->field($contractor, 'physical_passport_issued_by')->textInput([
                        'maxlength' => true,
                    ]) ?>
                </div>
                <div class="col-sm-12">
                    <?= $form->field($contractor, 'physical_passport_department')->textInput()->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '9{3}-9{3}',
                        'options' => [
                            'class' => 'form-control input-sm' . ($contractor->physical_passport_department ? ' edited' : ''),
                        ],
                        'clientOptions' => [
                            'showMaskOnHover' => false,
                        ]
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>
</div>

<?php $this->registerJs(<<<JS
    var companyType = {$companyType};

    function contractorIsIp() {
        return $('#contractor-type_id').val() == "<?= CompanyType::TYPE_IP ?>";
    }
    function contractorIsNotIp() {
        return $('#contractor-type_id').val() && !contractorIsIp();
    }

    function contractorIsPhysical() {
        return false;
    }
    function contractorIsLegal() {
        return true;
    }
    function contractorIsForeignLegal() {
        return false;
    }
    function contractorIsAgent() {
        return false;
    }

    $('#contractor-name').on('input', function () {
        if ($('#contractor-type_id').val() == '1') {
            $('#contractor-director_name').val($('#contractor-name').val());
        }
    });
    $('#contractor-type_id').on('change', function () {
        if ($('#contractor-type_id').val() == '1') {
            $('#contractor-director_name').val($('#contractor-name').val());
        }
    });
    $('[id="contractor-itn"]').suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: '78497656dfc90c2b00308d616feb9df60c503f51',
        type: 'PARTY',
        count: 10,

        onSelect: function(suggestion) {
            var companyTypeId = '-1';
            if (!empty(suggestion.data.opf) && !empty(companyType[suggestion.data.opf.short])) {
                companyTypeId = companyType[suggestion.data.opf.short];
            }
            $('#contractor-name').val(suggestion.data.name.full);
            $('#contractor-itn').val(suggestion.data.inn);
            $('#contractor-ppc').val(suggestion.data.kpp);
            $('#contractor-bin').val(suggestion.data.ogrn);
            if (!empty(suggestion.data.management)) {
                $('#contractor-director_name').val(suggestion.data.management.name);
                $('#contractor-director_post_name').val(suggestion.data.management.post);
            }
            $('#contractor-type_id').val(companyTypeId);
            if ($('#contractor-type_id').val() == companyType['ИП']) {
                $('#contractor-director_name').val(suggestion.data.name.full);
            }
            var address = '';
            if (suggestion.data.address.data !== null && suggestion.data.address.data.postal_code) {
                if (suggestion.data.address.value.indexOf(suggestion.data.address.data.postal_code) == -1) {
                    address += suggestion.data.address.data.postal_code + ', ';
                }
            }
            address += suggestion.data.address.value;
            $('#contractor-legal_address').val(address);
            $('#contractor-actual_address').val(address);

            $('#first-invoice-form .invoice-form-contractor .form-control').each(function(){
                $(this).toggleClass('edited', $(this).val().length > 0);
            });
        }
    });
    $(document).on('change', '#contractor-face_type input', function() {
        console.log($('#contractor-face_type input:checked').val());
        if ($('#contractor-face_type input:checked').val() == '1') {
            $('.contractor-type-legal').collapse('hide');
            $('.contractor-type-fisical').collapse('show');
            $('.contractor-type-legal input[type=text]').val('').removeClass('edited');
            $('.contractor-type-legal .has-error').removeClass('has-error');
        } else {
            $('.contractor-type-fisical').collapse('hide');
            $('.contractor-type-legal').collapse('show');
            $('.contractor-type-fisical input[type=text]').val('').removeClass('edited');
            $('.contractor-type-fisical .has-error').removeClass('has-error');
        }
    });
JS
);
