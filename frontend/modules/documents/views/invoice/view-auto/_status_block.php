<?php
use common\models\document\Autoinvoice;
use common\models\document\InvoiceAuto;

$count = $model->auto->getInvoices()->andWhere(['not', ['email_messages' => null]])->count();
switch ($model->auto->status) {
    case Autoinvoice::ACTIVE:
        if ($count) {
            $statusIcon = 'icon-envelope';
        } else {
            $statusIcon = 'icon-doc';
        }
        break;
    
    case Autoinvoice::CANCELED:
        $statusIcon = 'icon-ban';
        break;
    
    case Autoinvoice::CANCELED:
        $statusIcon = 'icon-ban';
        break;
    
    default:
        $statusName = '';
        $statusIcon = '';
        break;
}
?>

<div class="control-panel col-lg-6 col-w-lg-5 col-md-6">
    <!--    <div class="status-panel pull-right"> -->
    <div class="status-panel pull-right">

        <div class="btn display-n box-changed-status darkblue" title="Дата изменения статуса">
            <?= date("d.m.Y", $model->invoice_status_updated_at); ?>
        </div>

        <div class="btn btn-status darkblue" title="Статус">
           <span class="icon pull-left <?= $statusIcon ?>"></span>
           <?= $model->auto->statusName ?>
        </div>

        <div class="btn width125 box-payment-amount darkblue" title="Количество отправленных счетов">
            <?= $count ?>
        </div>

    </div>
</div>