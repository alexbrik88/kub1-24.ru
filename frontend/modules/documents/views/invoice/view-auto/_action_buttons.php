<?php

use yii\helpers\Html;
use common\models\document\Autoinvoice;
use yii\bootstrap\Modal;

/* @var $model \common\models\document\InvoiceAuto */

$canActivate = date_create($model->auto->setNextDate()) < date_create($model->auto->date_to) &&
               $model->auto->status == Autoinvoice::CANCELED;
?>
    <div class="row action-buttons margin-no-icon">
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE)) {
                echo Html::button('Копировать', [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    'data-toggle' => 'modal',
                    'href' => '#copy-confirm'
                ]);
                echo Html::button('<i class="fa fa-files-o fa-2x"></i>', [
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                    'title' => 'Копировать',
                    'data-toggle' => 'modal',
                    'href' => '#copy-confirm'
                ]);
            } ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?php if ($model->auto->status == 1 && Yii::$app->user->can(frontend\rbac\permissions\document\Autoinvoice::STOP)): ?>
                <button type="button" class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs" data-toggle="modal"
                        href="#stop-confirm">
                    Остановить
                </button>
                <button type="button" class="btn darkblue widthe-100 hidden-lg" data-toggle="modal" title="Остановить"
                        href="#stop-confirm">
                    <i class="ico-Denied-smart-pls fs"></i>
                </button>
            <?php elseif ($model->auto->status != 1 && Yii::$app->user->can(frontend\rbac\permissions\document\Autoinvoice::START)): ?>
                <button type="button" class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs" data-toggle="modal"
                        href="<?= $canActivate ? '#start-confirm' : '#cannot-confirm'; ?>">
                    Запустить
                </button>
                <button type="button" class="btn darkblue widthe-100 hidden-lg" data-toggle="modal" title="Запустить"
                        href="<?= $canActivate ? '#start-confirm' : '#cannot-confirm'; ?>">
                    <i class="fa fa-trash-o fa-2x"></i>
                </button>
            <?php endif; ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?php if ($model->auto->status != 0 && Yii::$app->user->can(frontend\rbac\permissions\document\Autoinvoice::DELETE)): ?>
                <button type="button"
                        class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs"
                        data-toggle="modal" href="#delete-confirm">Удалить
                </button>
                <button type="button" class="btn darkblue widthe-100 hidden-lg"
                        data-toggle="modal" title="Удалить" href="#delete-confirm">
                    <i class="fa fa-trash-o fa-2x"></i></button>
            <?php endif; ?>
        </div>
    </div>
<?php if (!$canActivate): ?>
    <?php Modal::begin([
        'id' => 'cannot-confirm',
        'closeButton' => false,
        'toggleButton' => false,
        'options' => ['class' => 'confirm-modal'],
    ]); ?>
    <div class="form-body">
        <div class="row">Чтобы запустить АвтоСчет, измените дату окончания данного шаблона</div>
    </div>
    <div class="form-actions row">
        <div class="col-xs-12 text-center">
            <button type="button" class="btn darkblue" data-dismiss="modal">ОК</button>
        </div>
    </div>
    <?php Modal::end(); ?>
<?php endif; ?>