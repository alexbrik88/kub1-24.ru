<?php
use common\components\date\DateHelper;
use common\models\document\Autoinvoice;
use common\models\document\Invoice;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \common\models\document\Autoinvoice */
/* @var $message Message */
/* @var $ioType integer */
/* @var $dateFormatted string */
/* @var $useContractor string */

$auto = $model->auto;
?>
<!--col-md-5 block-left-->
<div class="col-lg-6 col-w-lg-7 col-md-6">
    <div class="portlet customer-info">
        <div class="portlet-title">
            <!--col-md-10 caption-->
            <div class="col-md-9 caption">
                Шаблон <?= $model->is_invoice_contract ? 'автосчёт-договора' : 'автосчёта'; ?> № <?= $auto->document_number; ?> от <?= $dateFormatted; ?>
            </div>

            <div class="actions">
                <?= \frontend\modules\documents\widgets\DocumentLogWidget::widget([
                    'model'=>$model,
                ]); ?>

                <?php if ($auto->status != 0 && Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE, [
                        'model' => $model,
                    ])
                ): ?>
                    <a href="<?= Url::to(['update-auto', 'id' => $model->id,]); ?>"
                       title="Редактировать" class="btn darkblue btn-sm">
                        <i class="icon-pencil"></i>
                    </a>
                <?php endif; ?>
            </div>
        </div>
        <div class="portlet-body no_mrg_bottom">
            <table class="table no_mrg_bottom">
                <tr>
                    <td>
                        <?= \yii\helpers\Html::a('Просмотр счета, который получит клиент', ['print-auto', 'id' => $model->id], [
                            'target' => '_blank',
                            'class' => isset($_COOKIE["tooltip_account-view_{$model->company_id}"]) ? 'tooltipstered-main  tooltip-right' : null,
                            'data-tooltip-content' => isset($_COOKIE["tooltip_account-view_{$model->company_id}"]) ? '#tooltip_account-view' : null,
                        ]); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="customer-characteristic">Покупатель: </span>
                        <span><?= Html::a($model->contractor->getShortName(), [
                                '/contractor/view',
                                'type' => $model->contractor->type,
                                'id' => $model->contractor->id,
                            ]) ?></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span
                                class="customer-characteristic">Отсрочка платежа:</span>
                        <span><?= $auto->payment_delay; ?> дней</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="customer-characteristic">Периодичность:</span>
                        <span><?= $auto->getPeriodText(); ?></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="customer-characteristic">Начало:</span>
                        <span><?= $auto->dateFrom ?></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="customer-characteristic">Окончание:</span>
                        <span><?= $auto->dateTo ?></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="customer-characteristic">Указывать в наименовании:</span>
                        <span><?= $auto->addToTitle; ?></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="customer-characteristic"
                              style="display:inline-block;">Тип счёта: </span>
                        <span><?= $model->getProductionType() ?></span>
                    </td>
                </tr>
                    <tr>
                        <td>
                            <?php if ($model->basis_document_name && $model->basis_document_number && $model->basis_document_date) : ?>
                                <span class="customer-characteristic">Договор:</span>
                                № <?= Html::encode($model->basis_document_number) ?>
                                от <?= DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                            <?php endif ?>
                        </td>
                    </tr>
            </table>
        </div>
    </div>
</div>