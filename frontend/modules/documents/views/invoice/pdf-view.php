<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 16.04.15
 * Time: 16:42
 */

use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\ImageHelper;
use common\components\TextHelper;
use common\models\currency\Currency;
use common\models\document\status\InvoiceStatus;
use frontend\modules\documents\components\Message;
use yii\helpers\Html;
use frontend\models\Documents;
use common\models\company\CompanyType;
use common\models\document\InvoiceContractorSignature;
use frontend\modules\documents\components\InvoiceHelper;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */
/* @var $orders common\models\document\Order[] */
/* @var $message Message */
/* @var $ioType integer */
/* @var $addStamp boolean */
/* @var $multiple [] */

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'out-invoice out-document';

$invoiceContractorSignature = $model->company->invoiceContractorSignature;
if (!$invoiceContractorSignature) {
    $invoiceContractorSignature = new InvoiceContractorSignature();
    $invoiceContractorSignature->company_id = $model->company_id;
}
?>

<div style="
    width: 675px;
    margin: 0 auto;
    padding: 20px 60px;
    background: #fff;
    box-sizing: content-box;
    overflow: hidden;
    zoom: 1;
    ">
    <?= $this->render('template', [
        'model' => $model,
        'addStamp' => $addStamp,
        'ioType' => $model->type,
        'orders' => $model->orders,
        'invoiceContractorSignature' => $invoiceContractorSignature,
    ]); ?>
</div>

<?php /* if (isset($multiple) && (int)array_pop($multiple) !== $model->id): ?>
<pagebreak/>
<?php endif; */ ?>
