<?php

use common\models\company\CompanyType;
use common\components\helpers\Html;

/* @var $emailSignature \common\models\document\EmailSignature */
/* @var $viewDocumentButtonText string */
/* @var $viewDocumentLink string */
/* @var $documentText string */

$employee = Yii::$app->user->identity;
$employeeCompany = $employee->currentEmployeeCompany;
?>
<div style="padding: 0 0 10px 0; border-bottom: 1px solid #e4e4e4;">
    <?php if (isset($documentText) && $documentText): ?>
    <div style="font-size: 14px;margin-bottom: 20px;">
        <?= $documentText; ?>
    </div>
    <?php endif; ?>
    <div style="font-size: 14px; margin-bottom: 25px;">
        <?php if ($viewDocumentLink): ?>
            <?= Html::a($viewDocumentButtonText, $viewDocumentLink, [
                'class' => 'view-document-link',
                'target' => '_blank',
                'style' => 'display: inline-block; padding: 8px 16px; color: #0077A7; background-color: #fff;
            border: 2px dashed #0077A7;text-decoration: none!important;',
            ]); ?>
        <?php else: ?>
            <?php if ($viewDocumentButtonText) : ?>
                <span style="display: inline-block; padding: 8px 16px; color: #fff; background-color: #0077A7;">
                    <?= $viewDocumentButtonText; ?>
                </span>
            <?php endif; ?>
        <?php endif; ?>
    </div>
    <div class="email-signature-message-text" style="font-size: 14px;">
        <?php if (isset($emailSignature) && $emailSignature->text): ?>
            <?= nl2br($emailSignature->text); ?>
        <?php else: ?>
            С уважением,<br>
            <?= $employeeCompany->getFio(true); ?>
            <?php if ($employee->company->company_type_id != CompanyType::TYPE_IP) : ?>
                <br>
                <?= $employeeCompany->position; ?>
            <?php endif ?>
        <?php endif; ?>
    </div>
</div>