<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\components\grid\GridView;
use common\components\grid\DropDownSearchDataColumn;
use common\models\AgreementTemplate;
use common\models\employee\Employee;
use frontend\widgets\BtnConfirmModalWidget;
use yii\grid\ActionColumn;
use frontend\models\Documents;
use frontend\rbac\permissions;

$this->title = 'Шаблоны договоров';

if ($type)
    $this->title .= ($type == Documents::IO_TYPE_OUT) ? ' с покупателями' : ' с поставщиками';

/** @var \frontend\modules\documents\models\AgreementTemplateSearch $searchModel */
?>

<div class="portlet box">
    <?php if (Yii::$app->user->can(permissions\AgreementTemplate::CREATE, ['ioType' => $type])): ?>
        <div class="btn-group pull-right">
            <?= Html::a('<i class="fa fa-plus"></i> ДОБАВИТЬ', Url::to(['create', 'type' => $type]), [
                'class' => 'btn yellow',
            ]); ?>
        </div>
    <?php endif; ?>
    <h3 class="page-title"><?= $this->title; ?></h3>
</div>

<?= $this->render('../layouts/_agreement-new-toggle', ['type' => $type]) ?>

<div class="portlet box darkblue">
    <div class="portlet-title row-fluid">
        <div class="caption col-md-6">
            Список шаблонов: <?= $dataProvider->totalCount ?>
        </div>
        <div class="tools search-tools tools_button col-md-5 col-sm-5">
            <div class="form-body">
                <?php $form = \yii\widgets\ActiveForm::begin([
                    'method' => 'GET',
                    'action' => ['index', 'type' => $type],
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'options' => [
                            'class' => '',
                        ],
                    ],
                ]); ?>
                <div class="search_cont">
                    <div class="wimax_input">
                        <?= $form->field($searchModel, 'find_by')->textInput([
                            'placeholder' => 'Номер шаблона или название шаблона',
                        ]); ?>
                    </div>
                    <div class="wimax_button">
                        <?= Html::submitButton('НАЙТИ', [
                            'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                        ]) ?>
                    </div>
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
    </div>

    <div class="portlet-body">
        <div class="table-container">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable documents_table status_nowrap agreements-templates-table fix-thead',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],

                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount, 'scroll' => true]),
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                'columns' => [
                    [
                        'attribute' => 'document_number',
                        'label' => '№ шаблона',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '8%',
                        ],
                        'format' => 'raw',
                        'value' => function ($data) {
                            if (!$data['document_number']) {
                                $data['document_number'] = '---';
                            }
                            $model = AgreementTemplate::findOne($data['id']);

                            return Yii::$app->user->can(frontend\rbac\permissions\AgreementTemplate::VIEW, ['model' => $model]) ?
                                Html::a($data['document_number'], [
                                    '/documents/agreement-template/view',
                                    'id' => $data['id'],
                                    'type' => $data['type'],
                                ]) :
                                $data['document_number'];
                        },
                    ],
                    [
                        'attribute' => 'document_date',
                        'label' => 'Дата',
                        'headerOptions' => ['style' => 'width:10%;'],
                        'format' => ['date', 'php:d.m.Y'],
                    ],
                    [
                        'headerOptions' => [
                            'width' => '18%',
                        ],
                        'contentOptions' => [
                            'style' => 'overflow: hidden;text-overflow: ellipsis;',
                        ],
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'document_type_id',
                        'filter' => $searchModel->getAgreementTypeFilterItems(),
                        'format' => 'raw',
                        'label' => 'Тип доку&shy;мента',
                        'encodeLabel' => false,
                        'value' => function ($data) {
                            return $data['agreementType'];
                        },
                    ],
                    [
                        'attribute' => 'document_name',
                        'label' => 'Название',
                        'headerOptions' => ['style' => 'width:35%']
                    ],
                    /*
                    [
                        'attribute' => 'type',
                        'label' => 'Тип',
                        'headerOptions' => [
                            'width' => '13%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-left text-ellipsis',
                        ],
                        'class' => DropDownSearchDataColumn::className(),
                        'value' => function ($data) {
                            return isset(AgreementTemplate::$TYPE[$data['type']]) ? AgreementTemplate::$TYPE[$data['type']] : null;
                        },
                        'format' => 'raw',
                        'filter' => [
                            '' => 'Все',
                            1 => 'С продавцом',
                            2 => 'С покупателем',
                        ]
                    ],
                    */
                    [
                        'attribute' => 'documents_created',
                        'label' => 'Кол-во созданных договоров',
                        'headerOptions' => ['style' => 'width:8%;']
                    ],
                    [
                        'attribute' => 'status',
                        'label' => 'Статус',
                        'headerOptions' => [
                            'width' => '7%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-left text-ellipsis',
                        ],
                        'class' => DropDownSearchDataColumn::className(),
                        'value' => function ($data) {
                            return isset(AgreementTemplate::$STATUS[$data['status']]) ? AgreementTemplate::$STATUS[$data['status']] : null;
                        },
                        'format' => 'raw',
                        'filter' => [
                            '' => 'Все',
                            1 => 'В работе',
                            2 => 'В архиве',
                        ]
                    ],
                    [
                        'attribute' => 'created_by',
                        'label' => 'Ответственный',
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-left text-ellipsis',
                        ],
                        'class' => DropDownSearchDataColumn::className(),
                        'value' => function ($data) {
                            $employee = Employee::findOne([
                                'id' => $data['created_by']
                            ]);

                            return (!empty($employee)) ? $employee->getShortFio() : '';
                        },
                        'format' => 'raw',
                        'filter' => $searchModel->getCreators()
                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>