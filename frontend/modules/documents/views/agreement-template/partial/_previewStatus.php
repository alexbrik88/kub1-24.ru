<?php

use common\components\date\DateHelper;
use common\models\AgreementTemplate;
use common\widgets\Modal;
use frontend\models\Documents;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\AgreementTemplate */

$changeStatusDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->created_at,
]);

?>

    <div class="control-panel col-xs-12 pad0">
        <div class="col-xs-12 col-sm-3 pad3">
            <div class="btn full_w marg darkblue"
                 style="padding-left:0px; padding-right:0px;text-align: center;"
                 title="Дата изменения статуса">

                <?= date(DateHelper::FORMAT_USER_DATE, $model->created_at) ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-9 pad0">
            <div class="col-xs-7 pad3">
                <div class="btn full_w marg darkblue"
                     title="Статус">

                    <?php if ($model->status == AgreementTemplate::STATUS_ACTIVE) echo 'В работе' ?>
                    <?php if ($model->status == AgreementTemplate::STATUS_ARCHIVED) echo 'В архиве' ?>
                    <?php if ($model->status == AgreementTemplate::STATUS_DELETED) echo 'Удален' ?>
                </div>
            </div>
            <div class="col-xs-5 pad3">
                <div class="btn full_w marg darkblue btn-status"
                     style="text-align: center; "
                     title="Создано документов">

                    <?= (int)$model->documents_created ?>
                </div>
            </div>
        </div>
    </div>

    <!-- _main_info -->
    <div class="control-panel col-xs-12 pad0">
        <div class="col-xs-12 col-sm-9 col-sm-push-3 pad3">
            <div style="margin-top: 4px">
                <?= Html::a('<i class="pull-left icon fa fa-plus-circle"></i> ' . mb_strtoupper($model->documentType->name),
                    '#', [
                        'class' => 'btn yellow full_w ' . ($model->status != AgreementTemplate::STATUS_ACTIVE ? '' : 'agreement-modal-link'),
                        'style' => 'padding-left:4px;' . ($model->status != AgreementTemplate::STATUS_ACTIVE ? 'background:#a2a2a2;' : ''),
                        'disabled' => ($model->status != AgreementTemplate::STATUS_ACTIVE ? true : false),
                        'data-url' => Url::to(['/documents/agreement/create', 'type' => $model->type, 'from_template' => $model->id])
                    ]); ?>
            </div>
        </div>
    </div>

    <style>
        .main_inf_no-bord td {
            border: none !important;
        }
    </style>

    <!-- _main_info -->
    <!--col-md-5 block-left-->
    <div class="col-xs-12 col-lg-9 pull-right pad3" style="max-width: 720px;">
        <div class="portlet">
            <div class="customer-info bord-dark" style="margin:10px 0">

                <div class="portlet-body no_mrg_bottom main_inf_no-bord">
                    <table class="table no_mrg_bottom">
                        <tbody>
                        <tr>
                            <td>
                                <span class="customer-characteristic">Шаблон документа:</span>
                                № <?= $model->document_number ?> от <?= DateHelper::format(
                                    $model->document_date,
                                    DateHelper::FORMAT_USER_DATE,
                                    DateHelper::FORMAT_DATE); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="customer-characteristic">Название документа:</span>
                                <?= $model->document_name ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="customer-characteristic">Тип контрагента:</span>
                                <?= ($model->type == $model::TYPE_WITH_BUYER) ? 'Покупатель' : 'Поставщик' ?>
                            </td>
                        </tr>
                        <?php if ($model->payment_delay > 0) : ?>
                        <tr>
                            <td>
                                <span class="customer-characteristic">Отсрочка платежа:</span>
                                <?= $model->payment_delay; ?> дней
                            </td>
                        </tr>
                        <?php endif; ?>
                        <tr>
                            <td>
                                <span class="customer-characteristic">Ответственный:</span>
                                <?= \common\models\employee\Employee::findOne(Yii::$app->user->id)->getShortFio() ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="">
                <div style="margin: 15px 0 0;">
                    <span style="font-weight: bold;">Комментарий</span>
                    <?= Html::tag('span', '', [
                        'id' => 'comment_internal_update',
                        'class' => 'glyphicon glyphicon-pencil',
                        'style' => 'cursor: pointer;',
                    ]); ?>
                </div>
                <div id="comment_internal_view" class="">
                    <?= Html::encode($model->comment_internal) ?>
                </div>
                <?php if ($canUpdate) : ?>
                    <?= Html::beginTag('div', [
                        'id' => 'comment_internal_form',
                        'class' => 'hidden',
                        'style' => 'position: relative;',
                        'data-url' => Url::to(['comment-internal', 'id' => $model->id]),
                    ]) ?>
                    <?= Html::tag('i', '', [
                        'id' => 'comment_internal_save',
                        'class' => 'fa fa-floppy-o',
                        'style' => 'position: absolute; top: -22px; right: 0px; cursor: pointer; font-size: 20px;',
                    ]); ?>
                    <?= Html::textarea('comment_internal', $model->comment_internal, [
                        'id' => 'comment_internal_input',
                        'rows' => 3,
                        'maxlength' => true,
                        'style' => 'width: 100%; padding-right: 35px; border: 1px solid #ddd;',
                    ]); ?>
                    <?= Html::endTag('div') ?>
                <?php endif ?>
            </div>

        </div>
    </div>

<?php
if ($canUpdate) {
    $this->registerJs('
        $(document).on("click", "#comment_internal_update", function () {
            $("#comment_internal_view").toggleClass("hidden");
            $("#comment_internal_form").toggleClass("hidden");
        });
        $(document).on("click", "#comment_internal_save", function () {
            $.post($("#comment_internal_form").data("url"), $("#comment_internal_input").serialize(), function (data) {
                $("#comment_internal_view").text(data.value);
                $("#comment_internal_form").addClass("hidden");
                $("#comment_internal_view").removeClass("hidden");
            })
        });

    ');
}
?>