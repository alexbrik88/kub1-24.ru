<?php

use common\models\AgreementTemplate;
use frontend\rbac\permissions;
use yii\bootstrap\Html;
use frontend\widgets\ConfirmModalWidget;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $model \common\models\AgreementTemplate;
 */

?>
<div class="row action-buttons margin-no-icon">
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (Yii::$app->user->can(permissions\AgreementTemplate::CREATE, ['ioType' => $model->type])): ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => 'Копировать',
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ],
                'confirmUrl' => Url::to(['copy', 'id' => $model->id, 'type' => $model->type,]),
                'message' => 'Вы уверены, что хотите скопировать этот шаблон?',
            ]); ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => '<i class="fa fa-files-o fa-2x"></i>',
                    'title' => 'Копировать',
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                ],
                'confirmUrl' => Url::to(['copy', 'id' => $model->id, 'type' => $model->type,]),
                'message' => 'Вы уверены, что хотите скопировать этот шаблон?',
            ]); ?>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (Yii::$app->user->can(permissions\AgreementTemplate::UPDATE, ['model' => $model])): ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => ($model->status == $model::STATUS_ACTIVE) ?
                            'В архив' :
                            'Из архива',
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    ],
                    'confirmUrl' => Url::to([
                            'update-status',
                            'id' => $model->id,
                            'type' => $model->type,
                            'status' => ($model->status == $model::STATUS_ACTIVE) ?
                                'archived' :
                                'active'
                    ]),
                    'message' => ($model->status == $model::STATUS_ACTIVE) ?
                        'Вы уверены, что хотите перенести шаблон в архив?' :
                        'Вы уверены, что хотите извлечь шаблон из архива?'
                ]); ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => ($model->status == $model::STATUS_ACTIVE) ?
                            '<i class="fa fa-archive fa-2x"></i>' :
                            '<i class="fa fa-archive fa-2x"></i>',
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                        'title' => ($model->status == $model::STATUS_ACTIVE) ?
                            'В архив' :
                            'Из архива',
                    ],
                    'confirmUrl' => Url::to([
                        'update-status',
                        'id' => $model->id,
                        'type' => $model->type,
                        'status' => ($model->status == $model::STATUS_ACTIVE) ?
                            'archived' :
                            'active'
                    ]),
                    'message' => ($model->status == $model::STATUS_ACTIVE) ?
                        'Вы уверены, что хотите перенести шаблон в архив?' :
                        'Вы уверены, что хотите извлечь шаблон из архива?'
                ]); ?>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (!$model->is_deleted && Yii::$app->user->can(permissions\AgreementTemplate::DELETE, ['model' => $model])): ?>
            <?= Html::button('Удалить', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                'data-toggle' => 'modal',
                'href' => '#delete-confirm',
            ]); ?>
            <?= Html::button('<i class="fa fa-trash-o fa-2x"></i>', [
                'class' => 'btn darkblue widthe-100 hidden-lg',
                'data-toggle' => 'modal',
                'href' => '#delete-confirm',
                'title' => 'Удалить',
            ]); ?>
            <?= ConfirmModalWidget::widget([
                'options' => [
                    'id' => 'delete-confirm',
                ],
                'toggleButton' => false,
                'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id, 'type' => $model->type,]),
                'confirmParams' => [],
                'message' => "Вы уверены, что хотите удалить этот шаблон?",
            ]); ?>
        <?php endif; ?>
    </div>
</div>

