<?php
use frontend\rbac\permissions;
use frontend\widgets\ConfirmModalWidget;
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $model common\models\AgreementTemplate */
?>

<div id="buttons-bar-fixed">
    <div class="row action-buttons margin-no-icon">
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?= Html::submitButton($model->isNewRecord ?
                '<span class="ladda-label">Создать</span><span class="ladda-spinner"></span>' :
                '<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => $model->isNewRecord ?
                    'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button' :
                    'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                'data-style' => 'expand-right',
            ]); ?>
            <?= Html::submitButton($model->isNewRecord ? '<i class="fa fa-floppy-o fa-2x"></i>' : '<i class="fa fa-floppy-o fa-2x"></i>', [
                'class' => $model->isNewRecord ? 'btn darkblue widthe-100 hidden-lg' : 'btn darkblue widthe-100 hidden-lg',
            ]); ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1"></div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1"></div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1"></div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1"></div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1"></div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?= Html::a('Отменить', $model->isNewRecord
                ? ['agreement-template/index', 'type' => $type]
                : ['agreement-template/view', 'id' => $model->id,],
                [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                ]); ?>
            <?= Html::a('<i class="fa fa-reply fa-2x"></i>', $model->isNewRecord
                ? ['agreement-template/index', 'type' => $type]
                : ['agreement-template/view', 'id' => $model->id,],
                [
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                    'title' => 'Отменить',
                ]); ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?php if (!$model->isNewRecord): ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => 'Удалить',
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    ],
                    'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id]),
                    'confirmParams' => [],
                    'message' => 'Вы уверены, что хотите удалить шаблон договора?',
                ]);
                echo \frontend\widgets\ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => '<i class="fa fa-trash-o fa-2x"></i>',
                        'title' => 'Удалить',
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                    ],
                    'confirmUrl' => Url::to(['delete', 'id' => $model->id,]),
                    'message' => 'Вы уверены, что хотите удалить шаблон договора?',
                ]);
                ?>
            <?php endif; ?>
        </div>
    </div>
</div>