<?php

use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use frontend\widgets\ConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Url;
use common\widgets\Modal;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\AgreementTemplate */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor string */
/* @var $invoiceContractorSignature \common\models\document\InvoiceContractorSignature */

$this->context->layout = 'agreements-update';
$this->title = 'Просмотр шаблона договора';

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->document_date,
]);

$canUpdate = Yii::$app->user->can(permissions\AgreementTemplate::UPDATE, [
    'model' => $model,
]);

?>

    <div class="page-content-in" style="padding-bottom: 30px;">

        <div>
            <?php if ($backUrl !== null) {
                echo \yii\helpers\Html::a('Назад к списку', $backUrl, [
                    'class' => 'back-to-customers',
                ]);
            } ?>
        </div>

        <?php if ($model->status === 0): ?>
            <h1 class="text-warning">Шаблон удалён</h1>
        <?php endif; ?>

        <div class="col-xs-12 pad0">

            <div class="col-xs-12 col-lg-7 pad0">
                <?= $this->render('partial/_previewText', [
                    'model' => $model,
                    'canUpdate' => $canUpdate
                ]); ?>
            </div>

            <div class="col-xs-12 col-lg-5 pad0 pull-right" style="padding-bottom: 5px !important; max-width: 480px;">

                <div class="col-xs-12" style="padding-right:0px !important;">

                    <?= $this->render('partial/_previewStatus', [
                        'model' => $model,
                        'canUpdate' => $canUpdate
                    ]); ?>

                </div>

            </div>

        </div>

        <div id="buttons-bar-fixed">
            <?= $this->render('partial/_previewActionsButtons', [
                'model' => $model,
                'canUpdate' => $canUpdate
            ]); ?>
        </div>

    </div>
    <div class="modal fade t-p-f modal_scroll_center mobile-modal" id="add-new" tabindex="-1" role="modal"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
                <div class="modal-body" id="block-modal-new-product-form">

                </div>
            </div>
        </div>
    </div>
<?php Modal::begin([
    'id' => 'agreement-modal-container',
    'header' => '<h3 id="agreement-modal-header">Добавить договор</h3>',
    'options' => ['style' => 'max-width: 600px;']
]);

Pjax::begin([
    'id' => 'agreement-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]);

// content

Pjax::end();

Modal::end();
?>

<?php
if (Yii::$app->session->get('showAgreementCreatePopup')) {
    $this->registerJs('
        $(document).ready(function (e) {
            $(".agreement-modal-link").click();
        });
    ');
}
$this->registerJs('
$(document).on("click", ".agreement-modal-link", function(e) {
    e.preventDefault();
    $.pjax({url: $(this).data("url"), container: "#agreement-form-container", push: false});
    $(document).on("pjax:success", function() {
        $("#agreement-modal-header").html($("[data-header]").data("header"));
        $(".date-picker").datepicker({format:"dd.mm.yyyy",language:"ru",autoclose:true}).on("change.dp", dateChanged);

            function dateChanged(ev) {
                if (ev.bubbles == undefined) {
                    var $input = $("[name=\'" + ev.currentTarget.name +"\']");
                    if (ev.currentTarget.value == "") {
                        if ($input.data("last-value") == null) {
                            $input.data("last-value", ev.currentTarget.defaultValue);
                        }
                        var $lastDate = $input.data("last-value");
                        $input.datepicker("setDate", $lastDate);
                    } else {
                        $input.data("last-value", ev.currentTarget.value);
                    }
                }
            };
    })
    $("#agreement-modal-container").modal("show");
});
$(document).on("show.bs.modal", "#agreement-modal-container", function(event) {
        $(".alert-success").remove();
});

');

?>