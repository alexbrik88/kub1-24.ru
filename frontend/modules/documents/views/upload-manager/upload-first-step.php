<?php
use common\components\ImageHelper;
use devgroup\dropzone\DropZoneAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use devgroup\dropzone\DropZone;

$this->title = 'Распознавание';
DropZoneAsset::register($this)->jsOptions = [ 'position' => \yii\web\View::POS_HEAD ];
?>

<div class="upload-header">
    <div class="row">

        <div class="col-md-4 pad-5">
            <div class="upload-block">
                <div class="ub-header">
                    Загрузка документов<br/>
                    по Email
                </div>
                <div class="ub-body">
                    <div class="icon">
                        <?= ImageHelper::getThumb('img/upload-files-email.png', [100, 100]) ?>
                    </div>
                </div>
                <div class="ub-footer">
                    <?php if ($scanUploadEmail->enabled): ?>
                        Ваши документы будут автоматически загружены на эту страницу, при отправке их по адресу:<br/>
                        <div style="margin-top:10px"><b><?= $scanUploadEmail->email ?></b></div>
                    <?php else: ?>
                        У вас есть способ избежать ручной загрузки документов. <br/>
                        <a href="javascript:" class="show-email-upload-modal">Подробнее...</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="col-md-4 pad-5">
            <div class="upload-block">
                <div class="ub-header">
                    Загрузка документов<br/>
                    через мобильное приложение
                </div>
                <div class="ub-body">
                    <div class="icon">
                        <?= ImageHelper::getThumb('img/android/android-qr.jpeg', [100, 100]); ?>
                        <?= Html::a(ImageHelper::getThumb('img/android/download-from-google-play.png', [100, 30], [
                            'style' => 'margin-top: 10px;',
                        ]), Yii::$app->params['googlePlayLink'], [
                            'target' => '_blank',
                        ]); ?>
                    </div>
                </div>
                <div class="ub-footer">
                    Не надо сканировать документы!
                    Скачайте мобильное приложение "КУБ" и делайте в нём фото документов. Они попадают на эту страницу.
                </div>
            </div>
        </div>

        <div class="col-md-4 pad-5">
            <div class="upload-block">
                <div class="ub-header">
                    Загрузка документов<br/>
                    с вашего компьютера
                </div>
                <div class="ub-body" style="margin-top:-16px;">

                    <div id="dz-upload-scan" class="dz-upload-scan dropzone"></div>

                    <script type="text/javascript">
                        Dropzone.autoDiscover = false;
                        var dropzone_scan_upload = new Dropzone("#dz-upload-scan", {
                            'paramName': 'ScanDocument[upload]',
                            'url': '/documents/upload-manager/create-scan',
                            'dictDefaultMessage':  '<div class="icon"><?=ImageHelper::getThumb('img/upload-files-pc.png', [100, 100])?></div>',
                            'dictInvalidFileType': 'Недопустимый формат файла',
                            'maxFilesize': 5,
                            'maxFiles': 1,
                            'uploadMultiple': false,
                            'acceptedFiles': "image/jpeg,image/png,application/pdf",
                            'params': {'dz': true, '<?= \Yii::$app->request->csrfParam ?>': '<?= \Yii::$app->request->getCsrfToken() ?>'},
                        });
                        dropzone_scan_upload.on('thumbnail', function (f, d) {
                        });
                        dropzone_scan_upload.on('success', function (f, d) {
                            location.href = location.href;
                        });
                        dropzone_scan_upload.on('error', function (f, d) {
                            alert(d);
                        });
                        dropzone_scan_upload.on('totaluploadprogress', function (progress) {
                        });
                    </script>
                    
                </div>
                <div class="ub-footer">
                    <div class="ub-download-permanent">Перетащите файлы в эту область или</div>
                    <button class="btn darkblue create-scan-btn" data-url="<?= Url::to(['create-scan']) ?>">
                        Выбрать файл для загрузки
                    </button>
                </div>
            </div>
        </div>

    </div>
</div>

<?php Modal::begin([
    'id' => 'scan-document-modal',
    'header' => Html::tag('h1', 'Фото документа'),
]); ?>

<?php Modal::end(); ?>

<?= $this->render('_partial/_email_upload_modal') ?>

<script>
    $(document).ready(function() {
        $(document).on('click', '.create-scan-btn', function() {
            $('.dz-upload-scan').click();
        });
        $(document).on("hidden.bs.modal", "#scan-document-modal", function () {
            $('#scan-document-modal .modal-body').html('')
        });
        $(document).on('click', '.show-email-upload-modal', function(e) {
            e.preventDefault();
            $('#email-upload-modal').modal();
            $.pjax({
                url: '/documents/upload-manager/get-email-upload-modal',
                container: '#email-upload-pjax',
                data: {},
                push: false,
                timeout: 5000
            });
        });
    });
</script>