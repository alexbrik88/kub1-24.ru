<?php

use common\models\Company;
use common\components\grid\GridView;
use common\components\ImageHelper;
use devgroup\dropzone\DropZoneAsset;
use frontend\models\Documents;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\documents\components\UploadManagerHelper;
use common\components\grid\DropDownSearchDataColumn;
use common\models\employee\Employee;
use yii\widgets\Pjax;
use frontend\rbac\permissions;
use common\models\file\FileDir;
use yii\bootstrap\Dropdown;
use kartik\select2\Select2;
use frontend\components\StatisticPeriod;
use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\file\FileUploadType;
use yii\helpers\ArrayHelper;
use frontend\rbac\UserRole;
use common\models\document\ScanDocument;
use \common\models\Contractor;

/* @var $company Company */
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\documents\models\UploadManagerFileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $directory FileDir or null */

$this->title = $title;

if (!isset($directory)) $directory = null;
if (!isset($isUploads)) $isUploads = false;
if (!isset($isTrash)) $isTrash = false;

$canIndex = $canCreate = $canDelete = $canUpdate = $canRestore = $canRecognize =
    Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
    Yii::$app->user->can(UserRole::ROLE_ACCOUNTANT) ||
    Yii::$app->user->can(UserRole::ROLE_SUPERVISOR);

$dropDirs = [];
$dropDirs[] = [
    'label' => 'В папку',
    'url' => 'javascript:;',
    'linkOptions' => [
        'class' => 'group-attach-files-to',
    ],
];

if (!$isUploads) {
    $dropDirs[] = [
        'label' => 'Распознавание',
        'url' => '#move-files-to',
        'linkOptions' => [
            'class' => 'move-files-to',
            'data-directory_id' => 0
        ],
    ];
}

if ($dirs = FileDir::getEmployeeAllowedDirs()) {
    /** @var FileDir $dir */
    foreach ($dirs as $dir) {

        if ($directory && $directory->id == $dir->id)
            continue;

        $dropDirs[] = [
            'label' => $dir->name,
            'url' => '#move-files-to',
            'linkOptions' => [
                'class' => 'move-files-to',
                'data-directory_id' => $dir->id
            ],
        ];
    }
}

$docTypesList = UploadManagerHelper::getDocumentTypesList($company);

$dropDocs = [];
foreach ($docTypesList as $label => $docTypes) {
    $dropDocs[] = [
        'label' => $label,
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'group-attach-files-to',
        ],
    ];
    foreach ($docTypes as $key => $value) {
        $dropDocs[] = [
            'label' => $value,
            'url' => '#attach-files-to',
            'linkOptions' => [
                'class' => 'attach-files-to',
                'data-doc_type' => $key
            ],
        ];
    }
}

if ($isTrash)
    $emptyText = 'Корзина пуста.';
elseif ($isUploads)
    $emptyText = 'Нет новых сканов.';
else
    $emptyText = 'Вы еще не добавили ни одного фото документа.';

$hasFilters = (bool)$searchModel->filter_contractor ||
    (bool)$searchModel->date_from ||
    (bool)$searchModel->filter_type ||
    (bool)$searchModel->filter_doc_type;

$dateFrom = $searchModel->date_from ? DateHelper::format($searchModel->date_from, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) : null;
$dateTo = $searchModel->date_to ? DateHelper::format($searchModel->date_to, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)  : null;

DropZoneAsset::register($this)->jsOptions = [ 'position' => \yii\web\View::POS_HEAD ];
?>

<div class="upload-manager-index">
    <div class="portlet box">
        <div class="row" style="margin-bottom:9px">
            <div class="col-sm-12">
                <div class="btn-group pull-right">
                    <div class="btn-group pull-right title-buttons">
                        <button class="btn yellow create-scan-btn" data-url="<?= Url::to(['create-scan', 'dir_id' => ($directory) ? $directory->id : null]) ?>">
                            <i class="fa fa-plus"></i> ЗАГРУЗИТЬ
                        </button>
                    </div>
                </div>
                <div class="tools search-tools tools_button hidden-sm hidden-xs pull-right">
                    <div class="form-body">
                    <?php $form = \yii\widgets\ActiveForm::begin([
                        'method' => 'GET',
                        'fieldConfig' => [
                            'template' => "{input}",
                            'options' => [
                                'class' => '',
                            ],
                        ],
                    ]); ?>
                    <div class="search_cont">
                        <div class="wimax_input ">
                            <?= $form->field($searchModel, 'find_by')->textInput([
                                'placeholder' => 'Поиск...',
                            ]); ?>
                        </div>
                        <div class="wimax_button">
                            <?= Html::submitButton('НАЙТИ', [
                                'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                            ]) ?>
                        </div>
                    </div>
                    <?php $form->end(); ?>
                </div>
                </div>
                <?php if (!$isTrash): ?>
                <div class="filter-block pull-right">
                    <div id="dropdown-with-calendar" class="dropdown pull-right">
                        <?= Html::a('Фильтр  <span class="caret"></span>', null, [
                            'class' => 'btn btn-sm dropdown-toggle-my darkblue-gray',
                            //'data-toggle' => 'dropdown',
                            'style' => $hasFilters ? 'color: #333!important; border-color: #f3565d!important;' : '',
                        ]); ?>

                        <ul class="dropdown-menu">
                            <li>
                                <div class="row">
                                    <span class="filter-label">Период</span>
                                    <?= Html::hiddenInput('date_from', $dateFrom, ['data-id' => 'date_from']) ?>
                                    <?= Html::hiddenInput('date_to', $dateTo, ['data-id' => 'date_to']) ?>

                                        <div class="btn default pull-right auto-width form-control" data-pjax="get-invoices-pjax" id="reportrange2" style="margin-right: 0; min-width: 217px;">
                                            <?php
                                            $this->registerJs("
                                            
                                                $('#reportrange2').daterangepicker({
                                                    format: 'DD.MM.YYYY',
                                                    startDate: '" . ($dateFrom ?: date('01.m.Y')) . "',
                                                    endDate: '" . ($dateTo ?: date('d.m.Y')) . "',
                                                    locale: {
                                                        daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                                                        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                                                        firstDay: 1
                                                    }
                                                });
                                            
                                                //date range statistic button
                                                $(document).on('apply.daterangepicker', '#reportrange2', function (ev, picker) {
                                                
                                                    $('#dropdown-with-calendar').find('input[name=\"date_from\"]').val(picker.startDate.format('YYYY-MM-DD'));
                                                    $('#dropdown-with-calendar').find('input[name=\"date_to\"]').val(picker.endDate.format('YYYY-MM-DD'));
                                                    
                                                    $('#reportrange2').find('span').html(picker.startDate.format('DD.MM.YYYY')+' - '+picker.endDate.format('DD.MM.YYYY'))

                                                    return false;                                                
                                                });
                                                
                                                $(document).on('click', '.dropdown-toggle-my', function() {
                                                    $('#dropdown-with-calendar').toggleClass('open');
                                                });
                                                                                                             
                                            "); ?>

                                            <span class="val"><?= ($searchModel->date_from) ? "{$dateFrom} - {$dateTo}" : 'Указать диапазон' ?></span>
                                            <b class="fa fa-angle-down"></b>
                                        </div>


                                </div>
                            </li>

                            <li>
                                <div class="row">
                                    <span class="filter-label">Вид документа</span>
                                    <span style="display: inline-block; width: 60%">
                                        <?= Select2::widget([
                                                'model' => $searchModel,
                                                'attribute' => 'filter_doc_type',
                                                'data' => [
                                                    '' => 'Все',
                                                    Documents::DOCUMENT_INVOICE => 'Счет',
                                                    Documents::DOCUMENT_ACT => 'Акт',
                                                    Documents::DOCUMENT_PACKING_LIST => 'ТН',
                                                    Documents::DOCUMENT_INVOICE_FACTURE => 'СФ',
                                                    Documents::DOCUMENT_UPD => 'УПД',
                                                    Documents::DOCUMENT_AGREEMENT => 'Договор',
                                                ],
                                                'options' => [
                                                    'class' => 'form-control',
                                                    'data-id' => 'filter_doc_type',
                                                    'style' => 'width: 100%'
                                                ],
                                                'pluginOptions' => [
                                                    'width' => '100%'
                                                ],
                                                'hideSearch' => true
                                        ]); ?>
                                    </span>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <span class="filter-label">Контрагент</span>
                                    <span style="display: inline-block; width: 60%">
                                        <?= Select2::widget([
                                            'model' => $searchModel,
                                            'attribute' => 'filter_contractor',
                                            'data' => [
                                                '' => 'Все',
                                                'Поставщики' => $searchModel->getFilterContractors(Contractor::TYPE_SELLER),
                                                'Покупатели' => $searchModel->getFilterContractors(Contractor::TYPE_CUSTOMER),
                                            ],
                                            'options' => [
                                                'class' => 'form-control',
                                                'data-id' => 'filter_contractor',
                                                'style' => 'width: 100%'
                                            ],
                                            'pluginOptions' => [
                                                'width' => '100%'
                                            ]
                                        ]); ?>
                                    </span>
                                </div>
                            </li>
                            <li style="border-bottom: 0;">
                                <div class="row">
                                    <span class="filter-label">Тип</span>
                                    <span style="display: inline-block; width: 60%">
                                        <?= Select2::widget([
                                            'model' => $searchModel,
                                            'attribute' => 'filter_type',
                                            'data' => [
                                                '' => 'Все',
                                                Documents::IO_TYPE_IN => 'От поставщиков',
                                                Documents::IO_TYPE_OUT => 'От покупателей',
                                            ],
                                            'options' => [
                                                'class' => 'form-control',
                                                'data-id' => 'filter_type',
                                                'style' => 'width: 100%'
                                            ],
                                            'pluginOptions' => [
                                                'width' => '100%'
                                            ]
                                        ]); ?>
                                    </span>
                                </div>
                            </li>
                            <li style="border-bottom: 0;">
                                <div class="form-actions">
                                    <div class="row action-buttons buttons-fixed">
                                        <div class="spinner-button col-sm-3 col-xs-3"
                                             style="width: 35%;padding-left: 0;">
                                            <?= Html::button('Найти', [
                                                'class' => 'apply-filters btn darkblue widthe-100',
                                            ]); ?>
                                        </div>
                                        <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width: 30%;"></div>
                                        <div class="spinner-button col-sm-1 col-xs-1"
                                             style="width: 35%;padding-right: 0;">
                                            <?= Html::button('Сбросить', [
                                                'class' => 'apply-default-filters btn darkblue widthe-100',
                                            ]); ?>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>

                    </div>
                </div>
                <?php endif; ?>
                <div class="joint-operations" style="display:none;">
                    <?php if ($canDelete) : ?>
                        <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                            'class' => 'btn darkblue-gray btn-sm hidden-md hidden-sm hidden-xs pull-right',
                            'data-toggle' => 'modal',
                        ]); ?>
                        <?= Html::a('<i class="glyphicon glyphicon-trash"></i>', '#many-delete', [
                            'class' => 'btn darkblue-gray btn-sm hidden-lg pull-right',
                            'data-toggle' => 'modal',
                        ]); ?>
                    <?php endif ?>
                    <?php if (!$isTrash): ?>
                        <?php if ($canUpdate && $dropDirs) : ?>
                            <div class="dropdown pull-right">
                                <?= Html::a('Переместить  <span class="caret"></span>', null, [
                                    'class' => 'btn btn-sm darkblue-gray dropdown-toggle',
                                    'id' => 'dropdownMenu1',
                                    'data-toggle' => 'dropdown',
                                    'aria-expanded' => true,
                                ]); ?>
                                <?= Dropdown::widget([
                                    'items' => $dropDirs,
                                    'options' => [
                                        'style' => 'width:204px; left: -30px; top: 36px;',
                                        'aria-labelledby' => 'dropdownMenu1',
                                    ],
                                ]); ?>
                            </div>
                        <?php endif ?>
                        <?php if ($canUpdate && $dropDocs) : ?>
                            <div class="dropdown pull-right">
                                <?= Html::a('Прикрепить  <span class="caret"></span>', null, [
                                    'class' => 'btn darkblue-gray btn-sm dropdown-toggle btn-many-attach',
                                    'id' => 'dropdownMenu2',
                                    'data-toggle' => 'dropdown',
                                    'aria-expanded' => true,
                                ]); ?>
                                <?= Dropdown::widget([
                                    'items' => $dropDocs,
                                    'options' => [
                                        'style' => 'right: -66px!important; left: auto; top: 36px;',
                                        'aria-labelledby' => 'dropdownMenu2',
                                    ],
                                ]); ?>
                            </div>
                        <?php endif ?>
                        <?php if ($canRecognize) : ?>
                            <?= Html::a('Распознать', '#many-recognize', [
                                'class' => 'btn btn-many-recognize darkblue-gray btn-sm hidden-md hidden-sm hidden-xs pull-right',
                                'data-toggle' => 'modal',
                                'style' => 'display:none'
                            ]); ?>
                            <?= Html::a('Распознать', '#many-recognize', [
                                'class' => 'btn btn-many-recognize darkblue-gray btn-sm hidden-lg pull-right',
                                'data-toggle' => 'modal',
                                'style' => 'display:none'
                            ]); ?>
                        <?php endif ?>
                    <?php else: ?>
                        <?php if ($canRestore) : ?>
                            <?= Html::a('<i class="glyphicon glyphicon-share"></i> Восстановить', '#many-restore', [
                                'class' => 'btn darkblue-gray btn-sm hidden-md hidden-sm hidden-xs pull-right',
                                'data-toggle' => 'modal',
                            ]); ?>
                            <?= Html::a('<i class="glyphicon glyphicon-share"></i>', '#many-restore', [
                                'class' => 'btn darkblue-gray btn-sm hidden-lg pull-right',
                                'data-toggle' => 'modal',
                            ]); ?>
                        <?php endif ?>
                    <?php endif; ?>


                    <?= $this->render('_partial/_joint_operations_modals', [
                         'canUpdate' => $canUpdate,
                         'canRestore' => $canRestore,
                         'canDelete' => $canDelete,
                         'canRecognize' => $canRecognize
                    ]) ?>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="scroll-table-wrapper">
                <?php Pjax::begin([
                    'id' => 'pjax-uploaded-files',
                    'enablePushState' => false,
                    'timeout' => 5000
                ]) ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-hover dataTable uploaded_files_table',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'emptyText' => $emptyText,
                    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                    'rowOptions'=>function($data){
                        return ['data-file_id' => $data['id']];
                    },
                    'columns' => [
                        [
                            'header' => Html::checkbox('', false, [
                                'class' => 'joint-operation-main-checkbox',
                            ]),
                            'headerOptions' => [
                                'class' => 'text-center',
                                'width' => '1%',
                            ],
                            'contentOptions' => [
                                'class' => 'text-center',
                            ],
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::checkbox('File[]', false, [
                                    'class' => 'joint-operation-checkbox',
                                    'value' => $data['id']
                                ]);
                            },
                        ],
                        [
                            'label' => '',
                            'headerOptions' => [
                                'width' => '1%',
                            ],
                            'format' => 'raw',
                            'value' => function($data) {
                                $url = UploadManagerHelper::getImgPreviewUrl($data);
                                $icon = UploadManagerHelper::getFileIcon($data);

                                if ($data['ext'] == 'pdf') {
                                    $img = Html::tag('embed', '', [
                                        'src' => Url::to($url),
                                        'width' => '250px',
                                        'height' => '400px',
                                        'name' => 'plugin',
                                        'type' => 'application/pdf',
                                    ]);
                                } else {
                                    $img = Html::img($url, [
                                        'style' => 'max-height:300px;max-width:300px;',
                                        'alt' => '',
                                    ]);
                                }
                                $content = Html::a($icon, $url, [
                                    'class' => 'scan_link text-center',
                                    'download' => 'download',
                                    'data' => [
                                        'pjax' => 0,
                                        'tooltip-content' => '#scan-tooltip-' . $data['id'],
                                    ]
                                ]);
                                $tooltip = Html::tag('span', $img, ['id' => 'scan-tooltip-' . $data['id']]);
                                $content .= Html::tag('div', $tooltip, ['class' => 'hidden']);

                                return $content;
                            }
                        ],
                        [
                            'label' => 'Файл',
                            'attribute' => 'filename_full',
                            'headerOptions' => [
                                'width' => '35%',
                            ],
                            'contentOptions' => [
                                'style' => 'min-width: 150px',
                            ],
                            'format' => 'raw',
                            'value' => function($data) use ($isTrash, $isUploads) {

                                if ($isTrash)
                                    $content = Html::tag('span', $data['filename_full'], ['title' => $data['filename_full']]);
                                elseif ($isUploads)
                                    $content = Html::a($data['filename_full'], ['preview-scans',
                                        'dir_id' => 'upload',
                                        'file_id' => $data['id']], ['data-pjax' => 0]);
                                else
                                    $content = Html::a($data['filename_full'], ['preview-scans',
                                            'dir_id' => $data['directory_id'],
                                            'file_id' => $data['id']], ['data-pjax' => 0]);

                                return Html::tag('div', $content, [
                                    'title' => $data['filename_full'],
                                    'style' => 'width:0; min-width:100%; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
                                ]);
                            }
                        ],
                        [
                            'label' => 'Дата загрузки',
                            'attribute' => 'created_at',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'format' => ['date', 'php:d.m.Y'],
                        ],
                        [
                            'label' => 'Размер',
                            'attribute' => 'filesize',
                            'headerOptions' => [
                                'width' => '5%',
                            ],
                            'contentOptions' => [
                                'class' => 'text-right'
                            ],
                            'format' => 'raw',
                            'value' => function($data) {

                                return UploadManagerHelper::formatSizeUnits($data['filesize']);
                            }
                        ],
                        [
                            'label' => 'Прикреплен',
                            'headerOptions' => [
                                'width' => '35%',
                                'class' => (($isUploads || $isTrash) ? 'hidden' : ''),
                            ],
                            'contentOptions' => [
                                'class' => (($isUploads || $isTrash) ? 'hidden' : ''),
                            ],
                            'format' => 'raw',
                            'value' => function($data) {
                                if ($owner = UploadManagerHelper::getAttachedDocument($data)) {

                                    /** @var \common\models\document\Invoice $invoice */
                                    $invoice = (isset($owner->invoice)) ? $owner->invoice : $owner;
                                    if ($owner instanceof \frontend\modules\documents\models\SpecificDocument) {
                                        return '';
                                    }
                                    $doc = ($owner->type == Documents::IO_TYPE_IN ? 'Вх. ' : 'Исх. ') .
                                        (isset($owner->shortPrefix) ? $owner->shortPrefix : $owner->printablePrefix) .
                                        Html::a(' №'.$owner->getFullNumber(), $owner->viewUrl, [
                                                'data-pjax' => 0,
                                                'target' => '_blank'
                                        ]).' от '.date_format(date_create($owner->document_date), 'd.m.Y');

                                    $contractor = Html::a(Html::encode($invoice->contractor->getShortName()), [
                                        '/contractor/view',
                                        'type' => $invoice->type,
                                        'id' => $invoice->contractor_id,
                                    ], [
                                        'data-pjax' => 0,
                                        'target' => '_blank'
                                    ]);

                                    $content = Html::tag('div', $doc, ['class' => 'attached-document document']);

                                    if (isset($owner->totalAmountWithNds)) {
                                        $content .= Html::tag('div',
                                            'сумма ' . TextHelper::invoiceMoneyFormat($owner->totalAmountWithNds, 2),
                                            ['class' => 'attached-document amount']);
                                    }

                                    $content .= Html::tag('div', $contractor, ['class' => 'attached-document contractor']);

                                    return $content;
                                }

                                return '';
                            }
                        ],
                        [
                            'label' => 'Детали',
                            'headerOptions' => [
                                'class' => ((!$isUploads) ? 'hidden' : ''),
                                'width' => '35%'
                            ],
                            'contentOptions' => [
                                'class' => ((!$isUploads) ? 'hidden' : ''),
                                'style' => 'font-size:10px'
                            ],
                            'format' => 'raw',
                            'value' => function($data) use ($company) {
                                $content = '';
                                $contractors = $products = '';

                                $scan = UploadManagerHelper::getScanDocument($data);
                                if ($scan instanceof ScanDocument) {
                                    if ($scan->recognize) {
                                        if ($scan->recognize->is_recognized && $scan->recognize->scanRecognizeDocuments) {
                                            $rDocument = $scan->recognize->scanRecognizeDocuments[0];

                                            //$content .= 'GUID: ' . $rDocument->guid . '<br/>';

                                            if ($rDocument->document_type == 'UNKNOWN') {
                                                $content .= $rDocument->documentTypeName;
                                                return $content;
                                            }

                                            foreach ($rDocument->scanRecognizeContractors as $rContractor) {

                                                if ($rContractor->type == Contractor::TYPE_SELLER && $rDocument->io_type != Documents::IO_TYPE_OUT
                                                ||  $rContractor->type == Contractor::TYPE_CUSTOMER && $rDocument->io_type != Documents::IO_TYPE_IN) {

                                                    $contractor = ($rContractor->contractor_id) ?
                                                        $rContractor->short_name :
                                                        Html::a($rContractor->short_name, 'javascript:;', [
                                                            'class' => 'add-modal-contractor',
                                                            'data-pjax' => 0,
                                                            'data-inn' => $rContractor->inn,
                                                            'data-type' => $rContractor->type,
                                                            'style' => 'color:red',
                                                            'title' => 'Добавить поставщика'
                                                        ]);

                                                    $contractors .= ($rContractor->type == 1 ? 'Поставщик: ' : 'Покупатель: ') . $contractor . '<br/>';
                                                }
                                            }

                                            $needSpecify = false;
                                            foreach ($rDocument->scanRecognizeProducts as $rProduct) {
                                                if (!$rProduct->product_id) {
                                                    $needSpecify = true;
                                                    break;
                                                }
                                            }

                                            $productsLink = Html::a($needSpecify ? 'Уточнить' : 'Посмотреть', 'javascript:;', [
                                                    'class' => 'create-doc-from-products-link link',
                                                    'style' => ($needSpecify ? 'color:red' : ''),
                                                    'data' => [
                                                        'pjax' => 0,
                                                        'type' => $rDocument->io_type ?: 1,
                                                        'doc_type' => $rDocument->getDocumentTypeId(),
                                                        'files' => [$data['id']]
                                                    ]
                                                ]);

                                            $content .= ($rDocument->io_type ? ($rDocument->io_type == 1 ? 'Вх. ' : 'Исх. ') : '') .
                                                        $rDocument->documentTypeName .
                                                        ' №' . $rDocument->document_number .
                                                        ' от ' . DateHelper::format($rDocument->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) .
                                                        '<br/>';
                                            $content .= $contractors;
                                            $content .= $productsLink;

                                        } else {
                                            $content = Html::tag('span', 'Распознается...', ['class' => 'scan-recognize-task', 'data-scan_id' => $scan->id]);
                                        }
                                    } else {
                                        $content = Html::a('Распознать', '#one-recognize', [
                                            'class' => 'btn-recognize btn btn-sm darkblue',
                                            'data-toggle' => 'modal',
                                            'data-id' => $data['owner_id']
                                        ]);
                                    }
                                }

                                return Html::tag('div', $content, [
                                    'style' => 'width:0; min-width:100%; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
                                ]);
                            }
                        ],

                        [
                            'label' => 'Папка',
                            'attribute' => 'directory_id',
                            'headerOptions' => [
                                'width' => '10%',
                                'class' => (($isUploads || $directory || $isTrash) ? 'hidden' : ''),
                            ],
                            'contentOptions' => [
                                'class' => (($isUploads || $directory || $isTrash) ? 'hidden' : ''),
                            ],
                            'format' => 'raw',
                            'value' => function($data) use ($isUploads, $directory, $isTrash) {

                                if ($isUploads || $directory || $isTrash)
                                    return '';

                                $directory = FileDir::findOne($data['directory_id']);
                                if ($directory) {
                                    return Html::a($directory->name, '/documents/upload-manager/directory/' . $directory->id, [
                                        'data-pjax' => 0
                                    ]);
                                }

                                // no owner
                                if ($data['owner_model'] == ScanDocument::className() && !UploadManagerHelper::getAttachedDocument($data)) {
                                    return Html::a('Распознавание', '/documents/upload-manager/upload', [
                                        'data-pjax' => 0
                                    ]);
                                }

                                return '';
                            }
                        ],
                        [
                            'attribute' => 'created_at_author_id',
                            'label' => 'Загрузил',
                            'headerOptions' => [
                                'width' => '5%',
                            ],
                            'contentOptions' => [
                                'class' => 'text-left text-ellipsis',
                                'style' => 'line-height:1.2; max-width: 200px',
                            ],
                            'class' => DropDownSearchDataColumn::className(),
                            'value' => function ($data) {

                                $scan = UploadManagerHelper::getScanDocument($data);
                                if ($scan instanceof ScanDocument) {
                                    if ($scan->is_from_employee) {
                                        $content = ($scan->employee) ? $scan->employee->getShortFio() : '';
                                    } elseif ($scan->is_from_contractor) {
                                        $content = ($scan->contractor) ? $scan->contractor->getShortName() : '';
                                    } elseif ($scan->from_email) {
                                        $content = $scan->from_email;
                                    } else {
                                        $content = ($scan->employee) ? $scan->employee->getShortFio() : '';
                                    }
                                    $uploadType = ArrayHelper::getValue(FileUploadType::findOne($data['upload_type_id']), 'name', 'С компьютера');

                                } else {
                                    $uploadType = 'К документу';

                                    $employee = Employee::findOne(['id' => $data['created_at_author_id']]);
                                    $content = $employee ? $employee->getShortFio() : '';
                                }

                                $content .= '<br/><span class="small">'.$uploadType.'</span>';

                                return Html::tag('span', $content, ['title' =>$content]);
                            },
                            'format' => 'raw',
                            'filter' => $searchModel->getCreators($dataProvider->query)
                        ],
                        [
                            'label' => '',
                            'headerOptions' => [
                                'width' => '5%',
                                'style' => 'min-width:122px!important;'
                            ],
                            'format' => 'raw',
                            'value' => function($data) use ($docTypesList, $isTrash, $isUploads) {

                                if ($isTrash || UploadManagerHelper::getAttachedDocument($data))
                                    return false;

                                $content = Select2::widget([
                                    'name' => 'btn-attach-image',
                                    'data' =>  $docTypesList,
                                    'options' => [
                                        'placeholder' => 'Прикрепить',
                                        'class' => 'form-control btn-attach-image',
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false,
                                        'minimumResultsForSearch' => -1,
                                        'dropdownCssClass' => 'dropdown-attach-image',
                                    ]
                                ]);

                                return $content;
                            }
                        ],
                    ],
                ]); ?>

                <?php Pjax::end() ?>
            </div>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    'id' => 'scan-document-modal',
    'header' => Html::tag('h1', 'Фото документа'),
]); ?>

<?php Modal::end(); ?>

<?= $this->render('_partial/_invoices_modal', [
    'company' => $company,
]) ?>

<?= $this->render('_partial/_kub_doc_js') ?>

<div style="display:none" id="dz-upload-scan" class="dz-upload-scan dropzone"></div>

<div class="modal fade t-p-f modal_scroll_center mobile-modal" id="add-new" tabindex="-1" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body" id="block-modal-new-product-form">

            </div>
        </div>
    </div>
</div>

<div id="hellopreloader" style="display: none;">
    <div id="hellopreloader_preload"></div>
</div>
<div id="visible-right-menu" style="display: none;">
    <div id="visible-right-menu-wrapper"></div>
</div>

<?= $this->render('_partial/_help_panel_uploads') ?>

<script type="text/javascript">
    Dropzone.autoDiscover = false;
    var dropzone_scan_upload = new Dropzone("#dz-upload-scan", {
        'paramName': 'ScanDocument[upload]',
        'url': '<?= Url::to(['create-scan', 'dir_id' => ($directory) ? $directory->id : null]) ?>',
        'dictDefaultMessage':  '<div class="icon"><?=ImageHelper::getThumb('img/upload-files-pc.png', [100, 100])?></div>',
        'dictInvalidFileType': 'Недопустимый формат файла',
        'maxFilesize': 5,
        'maxFiles': 1,
        'uploadMultiple': false,
        'acceptedFiles': "image/jpeg,image/png,application/pdf",
        'params': {'dz': true, '<?= \Yii::$app->request->csrfParam ?>': '<?= \Yii::$app->request->getCsrfToken() ?>'},
    });
    dropzone_scan_upload.on('thumbnail', function (f, d) {
    });
    dropzone_scan_upload.on('success', function (f, d) {
        location.href = location.href;
    });
    dropzone_scan_upload.on('error', function (f, d) {
        alert(d);
    });
    dropzone_scan_upload.on('totaluploadprogress', function (progress) {
    });
</script>

<script>

    $(document).on("change", ".btn-attach-image", function (e) {
        e.preventDefault();
        var addToDoc = $(this).val();
        var files = [$(this).parents('tr').data('file_id')];

        if (!addToDoc)
            return false;

        if (KubDoc.setParams(addToDoc, files)) {
            KubDoc.showModal();
            $(this).val('').trigger('change');
        }
    });

    $(document).on('click', '.group-attach-files-to', function(e) {
        return false;
    });

    $(document).on("click", ".attach-files-to", function (e) {
        e.preventDefault();
        var addToDoc = $(this).data('doc_type');
        var files = [];
        $('.uploaded_files_table').find('.joint-operation-checkbox').filter(':checked').each(function() {
            files.push($(this).val());
        });

        if (KubDoc.setParams(addToDoc, files)) {
            KubDoc.showModal();
        }

    });

    $('.modal-attach-image-yes').on('click', function () {
        $('#invoices-list').modal();
        $('#modal-attach-image').modal('hide');
        $.pjax({
            url: '/documents/upload-manager/get-invoices',
            container: '#get-vacant-invoices-pjax',
            data: {
                type: KubDoc.currentType,
                doc_type: KubDoc.currentDocType,
                files: KubDoc.currentFiles
            },
            push: false,
            timeout: 5000
        });
    });

    $(document).on('click', '.create-doc-from-products-link', function(e) {
        e.preventDefault();
        $.post('/documents/upload-manager/redirect-to-create-document', {
            type: $(this).data('type'),
            doc_type: $(this).data('doc_type'),
            files: $(this).data('files')
        }, function(data) {
            $('#modal-attach-image').modal('hide');
            if (data['redirect_url']) {
                location.href = data['redirect_url'];
            }
        });
    });

    $('.modal-attach-image-no').on('click', function () {
        $.post('/documents/upload-manager/redirect-to-create-document', {
                type: KubDoc.currentType,
                doc_type: KubDoc.currentDocType,
                files: KubDoc.currentFiles
            }, function(data) {
                $('#modal-attach-image').modal('hide');
                if (data['redirect_url']) {
                    location.href = data['redirect_url'];
                }
        });
    });

    $('.move-files-to').on('click', function(e) {
        e.preventDefault();
        $('#move-files-to').find('.modal-move-files-to').attr('data-url', '/documents/upload-manager/move-files-to?directory_id=' + $(this).data('directory_id'));
        $('#move-files-to').modal('show');
    });

    $(document).on('click', '.create-scan-btn', function() {
        $('.dz-upload-scan').click();
        //$("#scan-document-modal").modal('show');
        //$('#scan-document-modal .modal-body').load($(this).data('url'));
    });
    $(document).on("hidden.bs.modal", "#scan-document-modal", function () {
        $('#scan-document-modal .modal-body').html('')
    });

    $(document).ready(function() {
        $('.scan_link').tooltipster({
            theme: ['tooltipster-kub'],
            contentCloning: true,
            side: ['right', 'left', 'top', 'bottom'],
        });

        ScanRecognize.init();
    });

    // Recognize
    ScanRecognize = {
        timerId: null,
        timerPeriod: 60000,
        taskIdx: null,
        init: function()
        {
            if ($('.scan-recognize-task').length) {
                this.timerId = setInterval(this.checkRecognizeStatus, this.timerPeriod);
                this.checkRecognizeStatus();
            }
        },
        checkRecognizeStatus: function()
        {
            var els = $('.uploaded_files_table').find('.scan-recognize-task');
            var el;

            if (els.length === 0) {
                clearInterval(this.timerId);
                return;
            }

            if (this.taskIdx === null) {
                el = $(els).first();
            } else {
                el = $(els).eq(this.taskIdx + 1);
                if (!el.length)
                    el = $(els).first();
            }
            this.taskIdx = $(el).index();

            $.ajax({
                url: 'check-recognize-status',
                data: {scan_id: $(el).data('scan_id')},
                success: function(data) {
                    if (data.result) {
                        $.pjax.reload("#pjax-uploaded-files");
                        ScanRecognize.checkRecognizeStatus();
                    }
                }
            });
        }
    };
</script>

<div style="display:none">
    <!-- Contractor -->
    <select id="invoice-contractor_id"><option value="0"></option></select>
    <input type="hidden" id="invoice-contractor_inn"/>
    <input type="hidden" id="invoice-contractor_type"/>
    <!-- Product -->
    <input type="hidden" id="new_product_id"/>
    <input type="hidden" id="new_product_name"/>
    <input type="hidden" id="new_product_code"/>
    <br id="has_self_add_modal_product_method" data-url="add-modal-product"/>
</div>

<script>
    function beforeShowAddNewModal() {
        $('#add-new .modal-header').html("");
        $('#block-modal-new-product-form').html("");
    }
    
    $(document).on("click", ".add-modal-contractor", function() {
        var url  = "add-modal-contractor";
        var form = {
            documentType: $(this).data('type'),
            inn: $(this).data('inn')
        };

        $("#invoice-contractor_id").val(0);
        $("#invoice-contractor_inn").val($(this).data('inn'));
        $("#invoice-contractor_type").val($(this).data('type'));

        beforeShowAddNewModal();
        INVOICE.addNewContractor(url, form);

        return false;
    });

    $(document).on("click", ".add-modal-product", function() {
        var url = "add-modal-product";
        var form = {
            Product: {
                name: $(this).data('name'),
                production_type: 1,
                flag: 1,
                documentType: $(this).data('document-type'),
                price: $(this).data('price')
            }
        };

        $("#new_product_id").val(0);
        $("#new_product_name").val($(this).data('name'));
        $("#new_product_code").val($(this).data('code'));

        beforeShowAddNewModal();
        INVOICE.addNewProduct(url, form);

        return false;
    });

    // after adding new Contractor
    $(document).on("change", "select#invoice-contractor_id", function(e) {
        
        if ($(this).val() == 0)
            return;
        
        var url = "set-recognized-contractor-id";
        var form = {
            id: $("#invoice-contractor_id").val(),
            inn: $("#invoice-contractor_inn").val(),
            type: $("#invoice-contractor_type").val()
        };
        $.get(url, form, function (data) {
            $.pjax.reload("#pjax-uploaded-files");
        });
    });

    // after adding new Product
    $(document).on("change", "#new_product_id", function(e) {

        if ($(this).val() == 0)
            return;
        
        var url = "set-recognized-product-id";
        var form = {
            id: $("#new_product_id").val(),
            name: $("#new_product_name").val(),
            code: $("#new_product_code").val()
        };
        $.get(url, form, function (data) {
            $.pjax.reload("#pjax-uploaded-files");
        });
    });

    // show/hide recognize button
    $(document).on('change', '.joint-operation-checkbox', function () {
        var showRecognizeBtn = false;
        var hideAttachBtn = false;
        if ($('.joint-operation-checkbox:checked').length) {
            $('.joint-operation-checkbox:checked').each(function (i) {
                if (!showRecognizeBtn) showRecognizeBtn = $(this).closest('tr').find('.btn-recognize').length > 0;
                if (!hideAttachBtn) hideAttachBtn = $(this).closest('tr').find('.attached-document').length > 0;
            });
            if (showRecognizeBtn) {
                $('.btn-many-recognize').show();
            } else {
                $('.btn-many-recognize').hide();
            }
            if (hideAttachBtn) {
                $('.btn-many-attach').hide();
                $('.move-files-to').filter('[data-directory_id="0"]').hide();
            } else {
                $('.btn-many-attach').show();
                $('.move-files-to').filter('[data-directory_id="0"]').show();
            }
        }
    });

    // FILTERS
    $(".filter-block ul.dropdown-menu, .daterangepicker.dropdown-menu").click(function(e) {
        e.stopPropagation();
    });
    $(".filter-block .apply-filters").click(function () {
        var $filterData = [];

        $(".filter-block ul.dropdown-menu select, .filter-block ul.dropdown-menu input").each(function () {
            $filterData["UploadManagerFileSearch[" + $(this).data("id") + "]"] = $(this).val();
        });
        applyProductFilter($filterData);
    });
    $(".filter-block .apply-default-filters").click(function () {
        var $filterData = [];

        $(".filter-block ul.dropdown-menu select").each(function () {
            var $attrName = $(this).data("id");
            var $value = 0;
            if ($attrName == "filterStatus") {
                $value = 2;
            }
            $filterData["UploadManagerFileSearch[" + $attrName + "]"] = $value;
        });
        $(".filter-block ul.dropdown-menu input").each(function () {
            $filterData["UploadManagerFileSearch[" + $(this).data("id") + "]"] = "";
        });
        applyProductFilter($filterData);
    });

    function applyProductFilter(data) {
        var kvp = document.location.search.substr(1).split("&");
        for (var key in data) {
            var $key = key;
            var $value = data[key];
            if (kvp !== "") {
                var i = kvp.length;
                var x;
                while (i--) {
                    x = kvp[i].split("=");
                    if (x[0] == $key) {
                        x[1] = $value;
                        kvp[i] = x.join("=");
                        break;
                    }
                }
                if (i < 0) {
                    kvp[kvp.length] = [$key, $value].join("=");
                }
            } else {
                kvp[0] = $key + "=" + $value;
            }
        }
        document.location.search = kvp.join("&");
    }

</script>