<?php

use common\components\date\DateHelper;
use common\models\Contractor;
use common\models\document\ScanDocument;
use frontend\modules\documents\components\UploadManagerHelper;
use yii\helpers\Html;
use frontend\models\Documents;
use common\models\file\File;
use \common\models\Company;

/** @var Company $company */
/** @var array $files */

$filesIds = Yii::$app->request->get('files');

$jsParams = [
    'io_type' => $ioType,
    'document_number' => '',
    'document_date' => '',
    'contractor_id' => '',
    'products' => [],
    'products_ids' => [],
];

$noNDS = true;

if (!empty($filesIds) && !Yii::$app->request->post('Invoice'))
{
    foreach ((array)$filesIds as $fileId) {

        $file = File::findOne(['id' => $fileId, 'company_id' => $company->id]);

        if ($file instanceof File) {

            $scan = UploadManagerHelper::getScanDocument($file);

            if ($scan instanceof ScanDocument) {

                if ($scan->recognize && $scan->recognize->is_recognized && $scan->recognize->scanRecognizeDocuments) {
                    $rDocument = $scan->recognize->scanRecognizeDocuments[0];
                    $jsParams['document_number'] = (string)$rDocument->document_number;
                    $jsParams['document_date'] = DateHelper::format($rDocument->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    foreach ($rDocument->scanRecognizeContractors as $rContractor) {
                        if ($rContractor->type == Contractor::TYPE_SELLER && $ioType == Documents::IO_TYPE_IN) {
                            $jsParams['contractor_id'] = $rContractor->contractor_id;
                            break;
                        } elseif ($rContractor->type == Contractor::TYPE_CUSTOMER && $ioType == Documents::IO_TYPE_OUT) {
                            $jsParams['contractor_id'] = $rContractor->contractor_id;
                            break;
                        }
                    }
                    foreach ($scan->recognize->scanRecognizeDocuments as $rDocument) {
                        foreach ($rDocument->scanRecognizeProducts as $rProduct) {

                            $product = UploadManagerHelper::getProduct($rProduct->product_id);
                            $nds = $rProduct->getTaxRateData();

                            if ($product) {
                                $jsParams['products'][$rProduct->id] = [
                                    'id' => $product['id'],
                                    'title' => $product['title'],
                                    'article' => $product['article'],
                                    'weight' => $product['weight'],
                                    'volume' => $product['volume'],
                                    'productUnit' => $product['productUnit'],
                                    'priceForSellNds' => $nds,
                                    'priceForBuyNds' => $nds
                                ];
                            } else {

                                $jsParams['products'][$rProduct->id] = [
                                    'unknown_product' => true,
                                    'unknown_product_has_similar' => UploadManagerHelper::hasSimilarProductsTitle($company, $rProduct->name),
                                    'id' => 'RECOGNIZED_ID_' . $rProduct->id,
                                    'recognized_id' => $rProduct->id,
                                    'title' => $rProduct->name,
                                    'article' => $rProduct->code,
                                    'weight' => null,
                                    'volume' => null,
                                    'productUnit' => ['name' => $rProduct->unit_name, 'id' => UploadManagerHelper::getProductUnitId($rProduct->unit_name)],
                                    'priceForSellNds' => $nds,
                                    'priceForBuyNds' => $nds
                                ];

                                if ($rProduct->vat > 0)
                                    $noNDS = false;
                            }

                            $jsParams['products'][$rProduct->id]['quantity'] = $rProduct->quantity;
                            $jsParams['products'][$rProduct->id]['price_for_sell_with_nds'] = bcmul(100, $rProduct->amount);
                            $jsParams['products'][$rProduct->id]['price_for_buy_with_nds'] = bcmul(100, $rProduct->amount);
                        }
                    }
                }
            }
        }
    }
}

if ($jsParams['io_type']) {

    $jsProducts = json_encode(array_values($jsParams['products']));
    $jsParams = json_encode($jsParams);

    $this->registerJs("
    
        var jsParams = $jsParams;
        var jsProducts = $jsProducts;
        var noNDS = ".(int)$noNDS.";
        
        $(document).ready(function() { 
            console.log(jsProducts);
            if (jsParams.document_number)
                $('#account-number').val(jsParams.document_number);
            if (jsParams.document_date)
                $('#under-date').val(jsParams.document_date);
            if (jsParams.contractor_id)
                $('#invoice-contractor_id').val(jsParams.contractor_id).trigger('change');                                
          
            if (jsProducts) {
                INVOICE.addProductToTable(jsProducts);
                $('#from-new-add-row').hide();
            } 
            
            if (noNDS) {
                $('#invoice-nds_view_type_id').val(2).trigger('change');
            }               
        });
        
        function beforeShowAddNewModal() {
            $('#add-new .modal-header').html('');
            $('#block-modal-new-product-form').html('');
        }
    
        $(document).on('click', '.unknown_product', function() {
            var recognized_id = $(this).data('recognized_id');
            var addFromNew = $(this).hasClass('new');
            var addFromExists = $(this).hasClass('exists');
            
            // set global recognized_id
            $('#unknown_product_recognized_id').val(recognized_id);
            
            if (addFromNew) {     
                   
                var url = '/documents/upload-manager/add-modal-product/?recognizedId=' + recognized_id;
                var form = {
                    Product: {
                        flag: 1,
                        production_type: null,
                    },
                    document_type: jsParams.io_type,
                    recognized_id: recognized_id
                };
      
                beforeShowAddNewModal();
                INVOICE.addNewProduct(url, form);
            }
            
            if (addFromExists) {
            
                var doctype = $('#order-add-select').data('doctype');
                var title = $(this).parents('tr').find('input.product-title').val();
                var exists = $('input.selected-product-id').map(function (idx, elem) {
                    return $(elem).val();
                }).get();

                $.pjax({
                    url: '/documents/upload-manager/get-products?title=' + title,
                    container: '#pjax-product-grid',
                    data: {documentType: doctype, exists: exists},
                    push: false,
                    timeout: 10000,
                    scrollTo: false,
                });
                $('#add-from-exists').modal();            
            
            }           
    
            return false;
        });
        
        $(document).on('beforeSubmit', '#invoice-form', function (e) {
            if ($('.unknown_product').length) {
            
                window.toastr.error('Необходимо подтвердить новые товары и услуги', '', {
                    'closeButton': true,
                    'showDuration': 1000,
                    'hideDuration': 1000,
                    'timeOut': 2500,
                    'extendedTimeOut': 1000,
                    'escapeHtml': false,
                });            
            
                Ladda.stopAll();
                
                return false;
            }
            
            return true;
        });                

    ");
}
?>

<div style="display:none">
    <input type="hidden" id="unknown_product_recognized_id" name="recognizedId"/>
    <br id="has_self_add_modal_product_method"
        data-url="/documents/upload-manager/add-modal-product"
        data-param-input="#unknown_product_recognized_id"/>
</div>