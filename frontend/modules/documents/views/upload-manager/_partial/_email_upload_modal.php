<?php
use yii\widgets\Pjax;
?>
<div class="modal fade t-p-f" id="email-upload-modal" tabindex="1" role="modal" aria-hidden="true">
    <div class="modal-dialog" style="width:700px;">
        <div class="modal-content">
            <?php Pjax::begin([
                'id' => 'email-upload-pjax',
                'linkSelector' => false,
                'formSelector' => '#email-upload-form',
                'timeout' => 5000,
                'enablePushState' => false,
                'enableReplaceState' => false,
            ]); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h3 style="text-align: center; margin: 0">Загрузка документов по e-mail</h3>
            </div>
            <div class="modal-body"></div>
            <div class="row action-buttons pad-10">
                <div class="col-sm-12">
                    <button class="btn btn-add darkblue pull-right" data-dismiss="modal">Включить</button>
                </div>
            </div>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>

<?php
$this->registerJs("
        
    $(document).on('hidden.bs.modal', '#email-upload-modal', function () {
        $('#email-upload-modal .modal-body').html('')
    });

"); ?>

