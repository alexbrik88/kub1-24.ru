<?php

use yii\helpers\Html;

?>
<div class="odds-panel">
    <div class="main-block">
        <p class="bold" style="font-size:19px;">

        </p>
    </div>
    <span class="side-panel-close" title="Закрыть">
        <span class="side-panel-close-inner"></span>
    </span>
</div>

<?php
$this->registerJs('
$(document).ready(function(){
    $(".odds-panel-trigger").click(function(){
        $(".odds-panel").toggle("fast");
        $(this).toggleClass("active");
        $("#visible-right-menu").show();
        $("html").attr("style", "overflow: hidden;");
        return false;
    });
    $("#visible-right-menu").click(function () {
        $(".odds-panel").toggle("fast");
        $(".odds-panel-trigger").toggleClass("active");
        $(this).hide();
        $("html").removeAttr("style");
    });
    $(".odds-panel .side-panel-close").click(function () {
        $(".odds-panel").toggle("fast");
        $(".odds-panel-trigger").toggleClass("active");
        $("#visible-right-menu").hide();
        $("html").removeAttr("style");
    });
});
');