<?php
use yii\helpers\Html;
use yii\helpers\Url;

if (!isset($canUpdate)) $canUpdate = false;
if (!isset($canDelete)) $canDelete = false;
if (!isset($canRestore)) $canRestore = false;
if (!isset($canRecognize)) $canRecognize = false;
?>

<?php if ($canUpdate) : ?>
    <div id="move-files-to" class="confirm-modal fade modal" role="dialog"
         tabindex="-1" aria-hidden="true"
         style="display: none; margin-top: -51.5px;">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">Вы уверены, что хотите переместить
                            выбранные файлы?
                        </div>
                    </div>
                    <div class="form-actions row">
                        <div class="col-xs-6">
                            <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
                                'class' => 'btn darkblue pull-right modal-move-files-to ladda-button',
                                'data-url' => Url::to(['move-files-to']),
                                'data-style' => 'expand-right',
                            ]); ?>
                        </div>
                        <div class="col-xs-6">
                            <button type="button" class="btn darkblue"
                                    data-dismiss="modal">НЕТ
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-attach-image" class="confirm-modal fade modal" role="dialog"
         tabindex="-1" aria-hidden="true"
         style="display: none; margin-top: -51.5px;">
        <div class="modal-dialog ">
            <button type="button" class="close close-confirm-modal" data-dismiss="modal" aria-hidden="true"></button>
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row invoice_part">
                            <span class="kubdoc_name">Акт (ТН/УПД/СФ)</span> к счету, который уже есть в КУБе?
                        </div>
                        <div class="row agreement_part" style="display:none;">
                            Название договора уже заведено в КУБ?
                        </div>
                    </div>
                    <div class="form-actions row invoice_part">
                        <div class="col-xs-12" style="text-align: center">
                            <?= Html::a('<span class="ladda-label">ДА, счет уже есть</span><span class="ladda-spinner"></span>', null, [
                                'class' => 'btn darkblue modal-attach-image-yes ladda-button',
                                'data-url' => Url::to(['attach-to-present-document']),
                                'data-document_id' => null,
                                'data-style' => 'expand-right',
                                'style' => 'display: inline-block; margin:0 10px; width:194px!important;'
                            ]); ?>
                            <button type="button" class="btn darkblue modal-attach-image-no" style="display: inline-block; margin:0 10px; width:194px!important;"
                                    data-dismiss="modal">НЕТ, счет нужно создать
                            </button>
                        </div>
                    </div>
                    <div class="form-actions row agreement_part" style="display: none">
                        <div class="col-xs-12" style="text-align: center">
                            <?= Html::a('<span class="ladda-label">ДА, уже заведено</span><span class="ladda-spinner"></span>', null, [
                                'class' => 'btn darkblue modal-attach-image-yes ladda-button',
                                'data-url' => Url::to(['attach-to-present-document']),
                                'data-document_id' => null,
                                'data-style' => 'expand-right',
                                'style' => 'display: inline-block; margin:0 10px; width:161px!important;'
                            ]); ?>
                            <button type="button" class="btn darkblue modal-attach-image-no" style="display: inline-block; margin:0 10px; width:161px!important;"
                                    data-dismiss="modal">НЕТ, нужно завести
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>
<?php if ($canDelete) : ?>
    <div id="many-delete" class="confirm-modal fade modal" role="dialog"
         tabindex="-1" aria-hidden="true"
         style="display: none; margin-top: -51.5px;">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">Вы уверены, что хотите удалить выбранные файлы?
                        </div>
                    </div>
                    <div class="form-actions row">
                        <div class="col-xs-6">
                            <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
                                'class' => 'btn darkblue pull-right modal-many-delete ladda-button',
                                'data-url' => Url::to(['many-delete']),
                                'data-style' => 'expand-right',
                            ]); ?>
                        </div>
                        <div class="col-xs-6">
                            <button type="button" class="btn darkblue"
                                    data-dismiss="modal">НЕТ
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>
<?php if ($canRestore): ?>
<div id="many-restore" class="confirm-modal fade modal" role="dialog"
     tabindex="-1" aria-hidden="true"
     style="display: none; margin-top: -51.5px;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body">
                    <div class="row">Вы уверены, что хотите восстановить выбранные файлы?
                    </div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-6">
                        <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
                            'class' => 'btn darkblue pull-right modal-many-restore ladda-button',
                            'data-url' => Url::to(['many-restore']),
                            'data-style' => 'expand-right',
                        ]); ?>
                    </div>
                    <div class="col-xs-6">
                        <button type="button" class="btn darkblue"
                                data-dismiss="modal">НЕТ
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
<?php if ($canRecognize) : ?>
    <div id="many-recognize" class="confirm-modal fade modal" role="dialog"
         tabindex="-1" aria-hidden="true"
         style="display: none; margin-top: -51.5px;">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">Вы уверены, что хотите распознать выбранные файлы?</div>
                    </div>
                    <div class="form-actions row">
                        <div class="col-xs-6">
                            <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
                                'class' => 'btn darkblue pull-right modal-many-recognize ladda-button',
                                'data-url' => Url::to(['many-recognize']),
                                'data-style' => 'expand-right',
                            ]); ?>
                        </div>
                        <div class="col-xs-6">
                            <button type="button" class="btn darkblue"
                                    data-dismiss="modal">НЕТ
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="one-recognize" class="confirm-modal fade modal" role="dialog"
         tabindex="-1" aria-hidden="true"
         style="display: none; margin-top: -51.5px;">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">Вы уверены, что хотите распознать?</div>
                    </div>
                    <div class="form-actions row">
                        <div class="col-xs-6">
                            <?= Html::button('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', [
                                'class' => 'btn darkblue pull-right modal-one-recognize ladda-button',
                                'data-id' => null,
                                'data-url' => Url::to(['recognize-scan']),
                                'data-style' => 'expand-right',
                            ]); ?>
                        </div>
                        <div class="col-xs-6">
                            <button type="button" class="btn darkblue"
                                    data-dismiss="modal">НЕТ
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>

<script>
    $(document).on('click', '.btn-recognize', function() {
        $('.modal-one-recognize').attr('data-id', $(this).attr('data-id'));
    });
    $(document).on('click', '.modal-one-recognize', function() {
        location.href = $(this).data('url') + '?scan_id=' + $(this).data('id');
    });
</script>
