<?php
use yii\bootstrap\Modal;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\employee\Employee;

/** @var  $model \common\models\file\FileDir */

$header = ($action == 'create-dir') ? 'Создать папку' : 'Обновить папку';

$form = \yii\bootstrap\ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'action' => \yii\helpers\Url::to("/documents/upload-manager/{$action}/?id={$model->id}"),
    'method' => 'POST',

    'options' => [
        'id' => 'add-directory-form',
        'enctype' => 'multipart/form-data',
        'data-pjax' => true
    ],

    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'col-md-3 control-label',
        ],
    ],
    'enableClientValidation' => false,
    'enableAjaxValidation' => false,
    'validateOnSubmit' => false,
    'validateOnBlur' => false,
]));

$employeeArray = ArrayHelper::map(
        Employee::find()
            ->where(['company_id' => $model->company_id])
            ->indexBy('lastname')
            ->all(), 'id', function ($employee) {
        return $employee->getShortFio(true) ?: '(не задан)';
    });

if ($action == 'create-dir') {
    $selectedEmployers = $model->hasEmployers;
} else {
    $selectedEmployers = ArrayHelper::getColumn(
        $model->employers,
        'id'
    );
}

?>

<div class="row">
    <div class="col-md-12">
        <?php echo $form->field($model, 'name', ['wrapperOptions' => ['class' => 'col-md-9']])
            ->textInput(['class' => 'form-control']);
        ?>
    </div>
    <div class="col-md-12">
        <?php echo $form->field($model, 'hasEmployers', ['wrapperOptions' => ['class' => 'col-md-9', 'style' => 'padding-right:27px']])
            ->widget(Select2::classname(),[
                    'data' => $employeeArray,
                    'class' => 'form-control',
                    'options' => [
                        'id' => 'directory-employers',
                        'placeholder' => 'Выберите сотрудников...',
                        'multiple' => true,
                        'value' => $selectedEmployers,
                    ],
                    'pluginOptions' => [
                        'closeOnSelect' => false,
                    ],
                    'toggleAllSettings' => [
                        'selectLabel' => '<i></i>Все',
                        'unselectLabel' => '<i></i>Все',
                    ]
                ]); ?>
    </div>
</div>

<div class="action-buttons row">
    <div class="spinner-button col-md-4 col-xs-1">
        <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
            'data-style' => 'expand-right',
            'style' => 'width:130px!important;',
        ]); ?>
        <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', ['class' => 'btn darkblue widthe-100 hidden-lg',
            'title' => 'Сохранить',]) ?>
    </div>
    <div class="col-xs-7"></div>
    <div class="col-md-8 col-xs-1" style="float: right;">
        <button type="button"
                class="btn darkblue btn-cancel darkblue widthe-100 hidden-md hidden-sm hidden-xs float-right"
                data-dismiss="modal" style="width: 130px !important;">Отмена
        </button>
        <button type="button" class="btn darkblue btn-cancel darkblue widthe-100 hidden-lg" title="Отмена"><i
                class="fa fa-reply fa-2x"></i></button>
    </div>
</div>

<?php
$form->end();
?>