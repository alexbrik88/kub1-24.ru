<?php
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use common\components\date\DateHelper;
use yii\helpers\Url;
use frontend\modules\documents\models\SpecificDocument;

$this->title = 'Разные документы';
?>

<div class="portlet box">
    <div class="btn-group pull-right title-buttons">
        <a href="#add-document-popup" class="btn yellow add-new-document">
            <i class="fa fa-plus"></i> ДОБАВИТЬ
        </a>
    </div>
    <h3 class="page-title"><?= $this->title; ?></h3>
</div>

<div class="portlet box darkblue">
    <?= Html::beginForm(['index'], 'GET'); ?>
    <div class="search-form-default">
        <div class="col-md-9 pull-right serveces-search" style="max-width: 595px;">
            <div class="input-group">
                <div class="input-cont">
                    <?= Html::activeTextInput($searchModel, 'name', [
                        'placeholder' => 'Поиск...',
                        'class' => 'form-control',
                    ]) ?>
                </div>
                <span class="input-group-btn">
                    <?= Html::submitButton('Найти', [
                        'class' => 'btn green-haze',
                    ]); ?>
                </span>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>

    <div class="portlet-title">
        <div class="caption caption_for_input">
            Мои документы
        </div>
    </div>

    <div class="portlet-body accounts-list">
        <div class="table-container">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,

                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable customers_table',
                    'id' => 'datatable_ajax',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'emptyText' => 'Вы еще не добавили ни одного документа.',
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'columns' => [
                    [
                        'attribute' => 'created_at',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '15%',
                        ],
                        'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
                    ],
                    [
                        'attribute' => 'name',
                        'headerOptions' => [
                            'class' => 'sorting', // 'dropdown-filter'
                            'width' => '23%',
                        ],
                        'content' => function($data) {
                            return mb_strlen($data->name, 'utf8') > 25 ? '<span title="'.$data->name.'">'.mb_substr($data->name, 0, 25, 'utf8').'</span>...' : $data->name;
                        }
                    ],
                    [
                        'label' => 'Скан',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '70px',
                            'style' => 'padding-left: 10px!important;'
                        ],
                        'attribute' => 'has_file',
                        'format' => 'raw',
                        'value' => function(SpecificDocument $model) {
                            if ($model->savedFile !== null) {
                                return Html::a('<span class="pull-center icon icon-paper-clip"></span>',
                                    ['file-get', 'id' => $model->id, 'file-id' => $model->savedFile->id,],
                                    ['target' => '_blank', 'download' => '' ]);
                            } else {
                                return '-';
                            }
                        }
                    ],
                    [
                        'attribute' => 'description',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'content' => function($data) {
                            return mb_strlen($data->description, 'utf8') > 80 ? '<span title="'.$data->description.'">'.mb_substr($data->description, 0, 80, 'utf8').'</span>...' : $data->description;
                        }
                    ],
                    [
                        'attribute' => 'updated_at',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'content' => function(SpecificDocument $data) {
                            return ($data->updated_at && $data->updated_at != $data->created_at) ?
                                   date("d.m.Y H:i:s", (int) $data->updated_at) :
                                   "Документ не изменялся";
                        }
                    ],
                    [
                        'class' => ActionColumn::className(),
                        'template' => '{update}{delete}',
                        'headerOptions' => [
                            'width' => '3%',
                        ],
                        'visible' => Yii::$app->user->can(\frontend\rbac\permissions\document\Document::DELETE),
                        'buttons' => [
                            'update' => function ($url, SpecificDocument $model, $key) {
                                $options = [
                                    'title' => 'Изменить',
                                    'data' => [
                                        'toggle' => 'modal',
                                        'target' => '#update-movement',
                                    ],
                                ];

                                return Html::a("<span aria-hidden='true' class='icon-pencil'></span>", $url, $options);
                            },
                            'delete' => function ($url) {
                                return \frontend\widgets\ConfirmModalWidget::widget([
                                    'toggleButton' => [
                                        'label' => '<span aria-hidden="true" class="icon-close"></span>',
                                        'class' => '',
                                        'tag' => 'a',
                                    ],
                                    'confirmUrl' => $url,
                                    'confirmParams' => [],
                                    'message' => 'Вы уверены, что хотите удалить документ?',
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>

    <?php
    echo $this->render('_add_document_popup', [
        'modalId' => 'add-document-popup',
        'modalTitle' => 'Загрузка документа',
    ]);
    ?>
</div>

<div class="modal fade" id="update-movement" tabindex="-1" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h1  id="js-modal_update_title">Обновление документа</h1>
            </div>

            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

<?php
$js = <<<JS
$(function () {
    $('body').on('click', '.add-new-document', function (e) {
        e.preventDefault();
        var that = $(this);
        var modal = $(that.attr('href'));
        modal.modal('show');
    });
});
$('.js_filetype_doc').styler({
    'fileBrowse' : '<span class="glyphicon glyphicon-plus-sign"></span> Выбрать файл',
    'onFormStyled' : function() {
        $('#specificdocument-file-styler .jq-file__browse')
        .prependTo('#specificdocument-file-styler')
        .css({'float' : 'none', 'display' : 'inline-block'});
    }

});
JS;
$this->registerJs($js);
?>
