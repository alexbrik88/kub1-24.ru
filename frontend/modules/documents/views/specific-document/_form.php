<?php
/**
 * @var $this  yii\web\View
 * @var $model frontend\modules\documents\models\SpecificDocument
 * @var $form  yii\bootstrap\ActiveForm
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>

<div class="form-body">
    <?php
    $form = \yii\bootstrap\ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
        'options' => [
            'id' => 'update-document',
            'enctype' => 'multipart/form-data'
        ],

        'fieldConfig' => [
            'labelOptions' => [
                'class' => 'col-md-4 control-label',
            ],
        ],
        'enableClientValidation' => true,
        'enableAjaxValidation' => false,
        'validateOnSubmit' => true,
        'validateOnBlur' => false,
    ]));
    ?>

    <div class="row">
        <div class="col-md-12">
            <?php echo $form->field($model, 'name', ['wrapperOptions' => ['class' => 'col-md-8']])
                ->textInput(['class' => 'form-control']);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?php echo $form->field($model, 'description', ['wrapperOptions' => ['class' => 'col-md-8']])->textarea(['class' => 'form-control']); ?>
        </div>
    </div>

    <div class="row">
        <?= $form->field($model, 'file', [
            'template' => "{input}\n{hint}\n{error}",
            'options' => ['class' => 'col-sm-6'],
        ])->fileInput(['class' => 'js_filetype_doc_update']) ?>

    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-6">
                <?= Html::submitButton('Сохранить', [
                    'class' => '    btn darkblue pull-left text-white',
                ]) ?>
            </div>
            <div class="col-md-5">
                <?php if (Yii::$app->request->isAjax):?>
                    <?= Html::button('Отменить', [
                        'class' => '    btn darkblue pull-right text-white',
                        'onclick' => new \yii\web\JsExpression('$(this).closest(".modal").modal("hide")'),
                    ]) ?>
                <?php else:?>
                    <?= Html::a('Отменить', ['index'], [
                        'class' => '    btn darkblue pull-right text-white',
                    ]) ?>
                <?php endif;?>
            </div>
        </div>
    </div>
    <?php
    $form->end();
    ?>

</div>
<?php
$savedFileName = $model->savedFile? $model->savedFile->filename_full: '';

$js = <<<JS
    var savedFileName = "$savedFileName";
    $(function () {
        $('.js_filetype_doc_update').styler({
        'fileBrowse' : '<span class="glyphicon glyphicon-plus-sign"></span> Выбрать файл',
        'onFormStyled' : function() {
        $('#specificdocument-file-styler .jq-file__browse')
        .css({'float' : 'none', 'display' : 'inline-block'});
        }

        });
        if (savedFileName) {
            $(".jq-file__name").text(savedFileName).show();
        }
    });
JS;
$this->registerJs($js);
?>

