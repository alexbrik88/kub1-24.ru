<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\cash\CashBankFlows */
?>
<div class="cash-bank-flows-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
