<?php
/**
 * Created by PhpStorm.
 * User: hp-m6
 * Date: 20.01.2017
 * Time: 3:12
 */

use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\product\Product;

/* @var \common\models\document\Proxy $model */

$plus = 0;
?>
<?php if (isset($order)): ?>
    <?php $quantity = $order ? $order->getAvailableQuantity() : 1;
    if ($quantity != intval($quantity)) {
        $quantity = rtrim(number_format($quantity, 10, '.', ''), 0);
    } ?>
    <tr role="row" class="odd order">
        <td><?= $order->order ? $order->order->product_title : \common\components\helpers\Html::dropDownList('test', null, $result, ['class' => 'dropdownlist form-control', 'prompt' => '']); ?></td>
        <td><input
                    class="input-editable-field quantity form-control" type="number"
                    value="<?= $quantity ?>" min="1"
                    max="<?= $quantity ?>"
                    id="<?= $order->order_id ?>"
                    name="<?= $order ? 'OrderProxy[' . $order->order_id . '][quantity]' : '' ?>"
                    style="padding: 6px 6px;">
        </td>
        <td><?= $order->order->unit ? $order->order->unit->name : Product::DEFAULT_VALUE; ?></td>
        <?php if ($order->proxy->invoice->nds_view_type_id != Invoice::NDS_VIEW_WITHOUT) : ?>
            <td><?= $order->order->purchaseTaxRate->name; ?></td>
        <?php endif; ?>
        <input class="status" type="hidden" name="<?= 'OrderProxy[' . $order->order_id . '][status]' ?>"
               value="active">
        <?php if ($order->proxy->invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT) : ?>
            <td class="text-right"><?= TextHelper::invoiceMoneyFormat($order->priceNoNds, $precision); ?></td>
            <td class="text-right"><?= TextHelper::invoiceMoneyFormat($order->amountNoNds, $precision); ?></td>
        <?php else : ?>
            <td class="text-right"><?= TextHelper::invoiceMoneyFormat($order->priceWithNds, $precision); ?></td>
            <td class="text-right"><?= TextHelper::invoiceMoneyFormat($order->amountWithNds, $precision); ?></td>
        <?php endif; ?>
        <td class="text-center"><span
                    class="icon-close input-editable-field  delete-row"></span>
        </td>
    </tr>
<?php else : ?>
    <tr role="row" class="odd order">
        <td><?= \common\components\helpers\Html::dropDownList('test', null, $result, ['class' => 'dropdownlist form-control', 'prompt' => '']); ?></td>
        <td></td>
        <td></td>
        <?php if (isset($invoice) && $invoice->nds_view_type_id != Invoice::NDS_VIEW_WITHOUT) : ?>
            <td width="5%"></td>
        <?php endif; ?>
        <input class="status" type="hidden" name="" value="active">
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-center"><span class="editable-field"><?= $key + 1; ?></span><span
                    class="icon-close input-editable-field hide delete-row"></span>
        </td>
    </tr>
<?php endif; ?>