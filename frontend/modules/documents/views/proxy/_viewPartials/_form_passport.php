<?php
/* @var $model \common\models\EmployeeCompany */
use common\components\date\DateHelper;
use common\models\company\CompanyType;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $form yii\widgets\ActiveForm */

$textInputConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'control-label col-md-6 label-width',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-6 field-width inp_one_line',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];

?>

<div class="portlet box employeecompany-passport-info-wrapper">
    <div class="portlet-body min-width-container-passport" style="padding:0">
        <div class="row">
            <div class="col-xs-12 pad0">
                <div class="col-md-6">
                    <div class="form-group field-employeecompany-passport_isrf">
                        <?= $form->field($model, 'passport_isRf', array_merge($textInputConfig, ['options' => ['class' => '']]))
                            ->radioList(['1' => 'РФ', '0' => 'не РФ'], [
                                'item' => function ($index, $label, $name, $checked, $value) {
                                    return Html::tag('label',
                                        Html::radio($name, $checked, ['value' => $value]) . $label,
                                        [
                                            'class' => 'radio-inline p-o radio-padding',
                                        ]);
                                },
                            ]); ?>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group field-physical-passport-country <?= $model->passport_isRf == 1 ? 'hide' : '' ?>">
                        <?= $form->field($model, 'passport_country', array_merge($textInputConfig, [
                            'options' => [
                                'class' => '',

                            ],
                        ]))
                        ?>
                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="form-group field-employeecompany-ppc">
                    <?=
                    $form->field($model, 'passport_series', array_merge($textInputConfig, ['options' => ['class' => '']]))
                        ->widget(\yii\widgets\MaskedInput::className(), [
                            'mask' => ($model->passport_isRf == 1) ? '9{2} 9{2}' : '[9|a| ]{1,25}',
                            'options' => [
                                'class' => 'form-control',
                                'placeholder' => ($model->passport_isRf == 1) ? 'XX XX' : '',
                            ],
                        ]);
                    ?>
                </div>
                <div class="form-group field-employeecompany-bin">
                    <?=
                    $form->field($model, 'passport_issued_by', array_merge($textInputConfig, [
                        'options' => [
                            'class' => '',
                        ],
                    ]))->label('Кем выдан:')->textInput([
                        'maxlength' => false,
                    ]);
                    ?>
                </div>
                <div class="form-group field-employeecompany-legal_address">
                    <?=
                    $form->field($model, 'passport_department', array_merge($textInputConfig, [
                        'options' => [
                            'class' => '',
                        ],
                    ]))->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => ($model->passport_isRf == 1) ? '9{3}-9{3}' : '[9|a| ]{1,255}' ,
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => ($model->passport_isRf == 1) ? 'XXX-XXX' : '',
                        ],
                    ]);
                    ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group field-employeecompany-current_account">
                    <?=
                    $form->field($model, 'passport_number', array_merge($textInputConfig, [
                        'options' => [
                            'class' => '',
                        ],
                    ]))->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => ($model->passport_isRf == 1) ? '9{6}' : '[9|a| ]{1,25}',
                        //'regex'=> "[0-9]*",
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => ($model->passport_isRf == 1) ? 'XXXXXX' : '',
                        ],
                    ]);
                    ?>
                </div>

                <div class="form-group field-employeecompany-current_account <?= !empty($model->getErrors('passport_date_output')) ? 'has-error' : '' ?>">
                    <div class="field-employeecompany-passport_number">
                        <label for="under-date" class="control-label col-md-6 label-width">Дата выдачи:</label>

                        <div class="col-md-12 inp_one_line width-inp">
                            <div class="input-icon">
                                <i class="fa fa-calendar"></i>
                                <?=
                                Html::activeTextInput($model, 'passport_date_output', [
                                    'id' => 'under-date',
                                    'class' => 'form-control date-picker',
                                    'data-date-viewmode' => 'years',
                                    'style' => 'width: 155px;',
                                    'value' => DateHelper::format($model->passport_date_output, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                                ]);
                                ?>
                                <?= Html::error($model,'passport_date_output', ['class' => 'help-block help-block-error']); //error ?>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="form-group form-employeecompany-address">
                    <?= $form->field($model, 'passport_address', array_merge($textInputConfig, [
                        'options' => [
                            'class' => 'required',
                        ],
                    ]))->label('Адрес регистрации:')->textInput([
                        'maxlength' => true,
                    ]); ?>
                </div>

            </div>

        </div>
    </div>
</div>