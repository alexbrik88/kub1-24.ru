<?php
use common\components\date\DateHelper;
use common\models\document\Proxy;
use frontend\modules\documents\components\Message;
use yii\bootstrap\Html;
use yii\helpers\Url;
use common\models\product\Product;
use \yii\helpers\ArrayHelper;
use \common\models\employee\Employee;
use kartik\select2\Select2;
use yii\widgets\Pjax;
/* @var \yii\web\View $this */
/* @var Proxy $model */
/* @var $message Message */
/* @var string $dateFormatted */

$productionType = explode(', ', $model->invoice->production_type);

$employeeArray = ['add-modal-employee' => '[ + Добавить сотрудника ]', '' => '---'] + ArrayHelper::map(
        \common\models\EmployeeCompany::find()
            ->where(['company_id' => $model->invoice->company->id])
            ->indexBy('lastname')
            ->all(), 'employee_id', function ($model) {
        return $model->getShortFio(true) ?: '(не задан)';
    });

$cancelUrl = Url::to(['view',
    'type' => $ioType,
    'id' => $model->id]);

?>

<div class="portlet customer-info">
    <div class="portlet-title">
        <div class="col-md-9 caption">
            <?= $message->get(Message::TITLE_SHORT_SINGLE); ?>
            №
            <?= Html::activeTextInput($model, 'document_number', [
                'maxlength' => true,
                'class' => 'form-control  input-editable-field',
                'placeholder' => 'доп. номер',
                'style' => 'max-width: 110px; display:inline-block;',
            ]); ?>

            от
            <div class="input-icon input-calendar input-editable-field">
                <i class="fa fa-calendar"></i>
                <?= Html::activeTextInput($model, 'document_date', [
                    'class' => 'form-control date-picker',
                    'data-date-viewmode' => 'years',
                    'value' => DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                ]); ?>
            </div>
        </div>

        <div class="actions">
            <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE, ['model' => $model,])) : ?>
                <button type="submit" class="btn darkblue btn-sm" style="color: #FFF;"
                   title="Сохранить">
                    <span class="ico-Save-smart-pls"></span></button>
                <a href="<?= $cancelUrl ?>" class="btn darkblue btn-sm" style="color: #FFF;"
                   title="Отменить">
                    <span class="ico-Cancel-smart-pls"></span></a>
            <?php endif; ?>
        </div>
    </div>
    <div class="portlet-body no_mrg_bottom">
        <table class="table no_mrg_bottom">
            <tr>
                <td style="line-height: 32px;">
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">
                                    Действительна по:
                                </span>
                        </div>
                        <div class="col-sm-7" style="padding-left: 7px;">
                            <div class="input-editable-field" style="max-width: 450px;">
                                <div class="input-icon input-calendar input-editable-field">
                                    <i class="fa fa-calendar"></i>
                                    <?= Html::activeTextInput($model, 'limit_date', [
                                        'id' => 'under-date',
                                        'class' => 'form-control date-picker',
                                        'size' => 16,
                                        'data-date-viewmode' => 'years',
                                        'style' => 'width: 100%',
                                        'value' => DateHelper::format($model->limit_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="line-height: 32px;">
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">
                                    Выдана:
                                </span>
                        </div>
                        <div class="col-sm-7" style="padding-left: 7px;">
                            <div class="input-editable-field" style="max-width: 450px;">
                                <?php Pjax::begin([
                                    'id' => 'proxy-pjax-container',
                                    'enablePushState' => false,
                                    'linkSelector' => false,
                                ]); ?>
                                <?= Select2::widget([
                                    'model' => $model,
                                    'attribute' => 'proxy_person_id',
                                    'data' => $employeeArray,
                                    'options' => [
                                        'placeholder' => '',
                                    ],
                                ]); ?>
                                <?php Pjax::end() ?>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="customer-characteristic"><?= $message->get(Message::CONTRACTOR); ?>:</span>
                    <span><?= Html::a($model->invoice->contractor_name_short, [
                        '/contractor/view',
                        'type' => $model->invoice->contractor->type,
                        'id' => $model->invoice->contractor->id,
                    ]) ?></span>
                </td>
            </tr>

            <tr>
                <td>
                    <div style="margin-bottom: 5px;">
                        <?= \frontend\modules\documents\widgets\DocumentFileScanWidget::widget([
                            'model' => $model,
                            'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
                            'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
                            'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
                            'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
                            'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
                            'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
                            'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
                        ]); ?>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
