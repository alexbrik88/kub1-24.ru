<?php
use yii\widgets\Pjax;

if (!isset($fromInvoice))
    $fromInvoice = 0;

?>

<?= $this->render('_style') ?>
<?= $this->render('_add_employee_modal') ?>

<div class="modal fade t-p-f" id="accounts-list" tabindex="1" role="modal" aria-hidden="true">
    <div class="modal-dialog" style="width:700px;">
        <div class="modal-content">
            <?php Pjax::begin([
                'id' => 'get-vacant-invoices-pjax',
                'formSelector' => '#search-invoice-form',
                'timeout' => 5000,
                'enablePushState' => false,
                'enableReplaceState' => false,
            ]); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h3 style="text-align: center; margin: 0">Создание доверенности к счету</h3>
            </div>
            <div class="invoice-list" style="margin-top:15px"></div>
            <div class="row action-buttons pad-10">
                <div class="col-sm-12">
                    <button id="add-to-invoice" class="btn btn-add darkblue" data-dismiss="modal" disabled="">Добавить</button>
                    <button id="add-new-invoice" class="btn btn-add yellow pull-right" data-dismiss="modal">Создать новый счет</button>
                </div>
            </div>
            <?php Pjax::end(); ?>

        </div>
    </div>
</div>

<?php
$this->registerJs("
    //date range statistic button
    $(document).on('apply.daterangepicker', '#reportrange2', function (ev, picker) {
        var form = $('#search-invoice-form');
        $(form).find('input[name=\"date_from\"]').val(picker.startDate.format('YYYY-MM-DD'));
        $(form).find('input[name=\"date_to\"]').val(picker.endDate.format('YYYY-MM-DD'));
        $(form).find('input[name=\"label_name\"]').val(picker.chosenLabel);
        $(form).find('input[name=\"event_date_change\"]').val(1);
        $('#search-invoice-form').submit();
    });
        
    $('.add-proxy').on('click', function (e) {
        e.preventDefault();
        $('#accounts-list').modal();
        $.pjax({
            url: '/documents/proxy/get-invoices',
            container: '#get-vacant-invoices-pjax',
            data: {from_invoice: {$fromInvoice}},
            push: false,
            timeout: 5000
        });
    });
            
    $(document).on('click', '.checkbox-invoice-id', function() {
        var checkboxes = $('.checkbox-invoice-id');
        var addTo = $('#add-to-invoice');
        var addNew = $('#add-new-invoice');
        if ($(checkboxes).filter(':checked').length) {
            $(addTo).removeAttr('disabled');
        } else {
            $(addTo).attr('disabled', true);
        }
        if ($(checkboxes).filter(':checked').length > 1) {
            $(checkboxes).filter(':checked').prop('checked', false);
            $(this).prop('checked', true);
            $(checkboxes).uniform();
        }
    });   
        
    $(document).on('click', '#add-to-invoice', function () {
        var checkboxes = $('.checkbox-invoice-id');
        var invoiceId = $(checkboxes).filter(':checked').first().val();
        $('#proxy-invoice_id').val(invoiceId);
        $('#search-invoice-form').submit();
    });
    
    $(document).on('click', '#add-new-invoice', function() {
        $('#search-invoice-form').submit();
    });

    $(document).on('pjax:success', '#get-vacant-invoices-pjax',  function() {
        $('.date-picker').datepicker({format: 'dd.mm.yyyy', language:'ru', autoclose: true}).on('change.dp', dateChanged);
        function dateChanged(ev) {
            if (ev.bubbles == undefined) {
                var input = $('[name=\'\" + ev.currentTarget.name +\"\']');
                if (ev.currentTarget.value == '') {
                    if (input.data('last-value') == null) {
                        input.data('last-value', ev.currentTarget.defaultValue);
                    }
                    var lastDate = input.data('last-value');
                    input.datepicker('setDate', lastDate);
                } else {
                    input.data('last-value', ev.currentTarget.value);
                }
            }
        };
        var form = $('#search-invoice-form');
        $(form).find('input[name=\"event_date_change\"]').val(0);

    });

"); ?>


