<?php

use common\components\grid\DropDownDataColumn;
use common\components\TextHelper;
use common\models\document\Invoice;
use frontend\modules\documents\components\FilterHelper;
use yii\helpers\Html;
use common\components\grid\GridView;
use \frontend\components\StatisticPeriod;
use common\components\date\DateHelper;
use \frontend\models\Documents;
use \yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

$employeeArray = ['add-modal-employee' => '[ + Добавить сотрудника ]', '' => '---'] + ArrayHelper::map(
    Yii::$app->user->identity->company->getEmployeeCompanies()->andWhere([
        'is_working' => 1,
    ])->orderBy(['lastname' => SORT_ASC])->all(),
    'employee_id',
    function ($model) {
        return $model->getShortFio(true) ?: '(не задан)';
    }
);

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\documents\models\InvoiceSearch */
/* @var string $documentNumber */
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h3 style="text-align: center; margin: 0">Создание доверенности к счету</h3>
</div>
<div class="invoice-list" style="margin-top:15px">
    <?php $form = ActiveForm::begin([
            'method' => 'get',
            'action' => ['/documents/proxy/get-invoices'],
            'layout'  => 'horizontal',
            'options' => [
                'id' => 'search-invoice-form',
                'class' => 'add-to-payment-order',
                'data' => [
                    'pjax' => true,
                ],
            ],
            'fieldConfig' => [
                'horizontalCssClasses' => [
                    'label' => 'col-sm-4',
                    'offset' => 'col-sm-offset-4',
                    'wrapper' => 'col-sm-8',
                ],
            ],
    ]); ?>

    <?= Html::hiddenInput('event_date_change', 0) ?>
    <?= Html::hiddenInput('invoice_id', $invoiceId, ['id' => 'proxy-invoice_id']) ?>
    <?= Html::hiddenInput('from_invoice', $fromInvoice) ?>

    <?php if (!$fromInvoice): ?>

        <?= Html::hiddenInput('date_from', DateHelper::format($dateFrom, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE)) ?>
        <?= Html::hiddenInput('date_to', DateHelper::format($dateTo, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE)) ?>
        <?= Html::hiddenInput('label_name', $labelName) ?>

        <div class="portlet box btn-invoice m-b-0 shadow-t">
            <div class="search-form-default">
                <div class="col-xs-8 col-md-8 serveces-search m-l-n-sm m-t-10">
                    <div class="input-group">
                        <div class="input-cont inp_pad">
                            <?php echo Html::activeTextInput($searchModel, 'document_number', [
                                'id' => 'invoice-number-search',
                                'placeholder' => '№ счёта...',
                                'class' => 'form-control',
                            ]); ?>
                        </div>
                        <span class="input-group-btn">
                            <?= Html::submitButton('Найти &nbsp;<i class="m-icon-swapright m-icon-white"></i>', [
                                'class' => 'btn green-haze',
                            ]); ?>
                        </span>
                    </div>
                </div>
            </div>
            <div id="range-ajax-button">
                <div
                    class="btn default pull-right btn-calendar auto-width p-t-7 p-b-7 portlet mrg_bottom ajax m-r-10 m-t-10"
                    data-pjax="get-vacant-invoices-pjax" id="reportrange2">

                    <?php
                    $this->registerJs("
                    $('#reportrange2').daterangepicker({
                        format: 'DD.MM.YYYY',
                        ranges: {" . StatisticPeriod::getRangesJs() . "},
                        startDate: '" . $dateFrom . "',
                        endDate: '" . $dateTo . "',
                        locale: {
                            daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                            firstDay: 1
                        }
                    });

                    "); ?>

                    <?= ($labelName == 'Указать диапазон') ? "{$dateFrom}-{$dateTo}" : $labelName ?>
                    <b class="fa fa-angle-down"></b>
                </div>

            </div>
            <div class="portlet-body accounts-list">
                <div class="table-container" style="">
                    <div id="in_invoice_table" class="dataTables_wrapper dataTables_extended_wrapper">
                        <?= GridView::widget([
                            'id' => 'invoice-modal-grid-table',
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'tableOptions' => [
                                'class' => 'table table-striped table-bordered table-hover dataTable customers_table',
                                'id' => 'datatable_ajax',
                                'aria-describedby' => 'datatable_ajax_info',
                                'role' => 'grid',
                            ],
                            'headerRowOptions' => [
                                'class' => 'heading',
                            ],
                            'options' => [
                                'id' => 'invoice-payment-order',
                                'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                            ],
                            'pager' => [
                                'options' => [
                                    'class' => 'pagination pull-right m-t',
                                ],
                            ],
                            'layout' => "{items}\n{pager}",
                            'columns' => [
                                [
                                    'attribute' => 'invoice_id',
                                    'label' => '',
                                    'format' => 'raw',
                                    'contentOptions' => [
                                        'style' => 'padding: 7px 5px 5px 5px;',
                                    ],
                                    'value' => function ($data) use ($invoiceId) {
                                        /** @var Invoice $data */
                                        return Html::checkbox(Html::getInputName($data, 'id') . '[]', $invoiceId == $data->id, [
                                            'value' => $data->id,
                                            'class' => 'checkbox-invoice-id',
                                            'style' => 'margin-left: -9px;',
                                        ]);
                                    },
                                    'headerOptions' => [
                                        'width' => '4%',
                                    ],
                                ],
                                [
                                    'attribute' => 'document_date',
                                    'label' => 'Дата счёта',
                                    'headerOptions' => [
                                        'class' => 'sorting',
                                        'width' => '24%',
                                    ],
                                    'format' => ['date', 'php:' . \common\components\date\DateHelper::FORMAT_USER_DATE],
                                ],
                                [
                                    'attribute' => 'document_number',
                                    'label' => '№ счёта',
                                    'headerOptions' => [
                                        'class' => 'sorting',
                                        'width' => '24%',
                                    ],
                                    'format' => 'raw',
                                    'value' => function ($data) {
                                        /** @var Invoice $data */
                                        return Html::a($data->fullNumber, ['invoice/view', 'type' => $data->type, 'id' => $data->id], ['data-pjax' => 0, 'target' => '_blank']);
                                    },
                                ],
                                [
                                    'label' => 'Сумма',
                                    'headerOptions' => [
                                        'class' => 'sorting',
                                        'width' => '24%',
                                    ],
                                    'attribute' => 'total_amount_with_nds',
                                    'value' => function (Invoice $model) {
                                        return TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2);
                                    },
                                ],
                                [
                                    'attribute' => 'contractor_id',
                                    'label' => 'Контрагент',
                                    'class' => \common\components\grid\DropDownSearchDataColumn::className(),
                                    'headerOptions' => [
                                        'width' => '24%',
                                    ],
                                    'filter' => FilterHelper::getContractorList($searchModel->type, Invoice::tableName(), true, false, false),
                                    'format' => 'raw',
                                    'value' => 'contractor_name_short',
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>

    <?php endif; ?>

    <div style="padding:0 10px">
        <?= $form->field($model, 'limit_date_input', [
            'template' => Yii::$app->params['formDatePickerTemplate'],
        ])->textInput(['class' => 'form-control date-picker', 'style' => 'max-width:150px'])->label('Действительна по:') ?>
    </div>

    <div style="padding:0 10px">
        <?= $form->field($model, 'proxy_person_id')
        ->widget(Select2::classname(), [
            'data' => $employeeArray,
            'options' => [
                'placeholder' => '',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])->label('Доверенное лицо'); ?>
    </div>

    <?php $form->end(); ?>

</div>
<div class="row action-buttons pad-10">
    <div class="col-sm-12">
        <button id="add-to-invoice"
                class="btn btn-add darkblue"
                <?= (!$invoiceId) ? 'disabled=""' : '' ?>
                >Добавить</button>
        <?php if (!$fromInvoice): ?>
            <button id="add-new-invoice"
                    class="btn btn-add yellow pull-right"
                    style="width:175px!important;"
                    >Создать новый счет</button>
        <?php endif; ?>
    </div>
</div>