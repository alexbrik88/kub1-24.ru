<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use common\components\date\DateHelper;
use common\models\employee\Employee;

/* @var $this yii\web\View */
/* @var $model \common\models\EmployeeCompany */
/* @var $form yii\widgets\ActiveForm */

$header = 'Добавить сотрудника';

if ($model->passport_isRf === null) {
    $model->passport_isRf = 1;
    $model->passport_country = 'Россия';
    $model->position = 'Курьер';
}

$checkboxConfig = [
    'labelOptions' => [
        'class' => 'control-label col-md-4',
        'style' => 'padding-top: 0px;'
    ],
    'wrapperOptions' => [
        'class' => 'col-md-8 inp_one_line',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n sel-w',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];

Pjax::begin([
    'id' => 'proxy-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]) ?>

<style type="text/css">
.proxy-form .form-control {width: 100%;}
form .form-group p.help-block {padding:0}
</style>
<div class="proxy-form" data-header="<?= $header ?>">

    <?php $form = ActiveForm::begin([
        'id' => 'proxy-add-employee-form',
        'enableClientValidation' => false,
        'action' => ['create-employee'],
        'layout' => 'horizontal',
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'label' => 'col-sm-4',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-8',
            ],
        ],
        'options' => [
            'data-pjax' => true,
            'data-max-files' => 5,
            'data-employee' => $model->isNewRecord ? null : [
                'id' => $model->id,
                'name' => $model->getFio(true),
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'patronymic')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'has_no_patronymic', $checkboxConfig)
        ->label('Нет отчества:')->checkbox([], false); ?>

    <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $this->render('_form_passport', [
            'model' => $model,
            'form'  => $form ]) ?>


    <div class="action-buttons row">
        <div class="spinner-button col-md-4 col-xs-1">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                'data-style' => 'expand-right',
                'style' => 'width:130px!important;',
            ]); ?>
            <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', ['class' => 'btn darkblue widthe-100 hidden-lg',
                'title' => 'Сохранить',])   ?>
        </div>
        <div class="col-xs-7"></div>
        <div class="col-md-8 col-xs-6" style="float: right;">
            <button type="button" class="btn darkblue btn-btn-cancel darkblue widthe-100 hidden-md hidden-sm hidden-xs float-right" data-dismiss="modal" style="width: 130px !important;">Отмена</button>
            <button type="button" class="btn darkblue btn-btn-cancel darkblue widthe-100 hidden-lg" data-dismiss="modal" title="Отмена"><i class="fa fa-reply fa-2x"></i></button>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php Pjax::end() ?>
