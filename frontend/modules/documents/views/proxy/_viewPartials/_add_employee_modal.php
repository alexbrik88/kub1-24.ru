<?php
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

Modal::begin([
    'id' => 'proxy-modal-container',
    'header' => '<h3 id="proxy-modal-header">Добавить сотрудника</h3>',
    'options' => ['style' => 'max-width: 600px;']
]);

Pjax::begin([
    'id' => 'proxy-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
    'timeout' => '5000'
]);

Pjax::end();

Modal::end();

$this->registerJs('
$(document).on("change", "#proxy-proxy_person_id", function(e) { 
    var value = $(this).val() || $(this).text();
    if (value == "add-modal-employee") {
        e.preventDefault();

        $.pjax({url: "' . Url::to([
        '/documents/proxy/create-employee',
        'container' => 'proxy-select-container',
    ]) . '", container: "#proxy-form-container", push: false});

        $(document).on("pjax:success", "#proxy-form-container", function() {
            $("#proxy-modal-header").html($("[data-header]").data("header"));
            $(".date-picker").datepicker({format: "dd.mm.yyyy", language:"ru", autoclose: true}).on("change.dp", dateChanged);

            function dateChanged(ev) {
                if (ev.bubbles == undefined) {
                    var $input = $("[name=\'" + ev.currentTarget.name +"\']");
                    if (ev.currentTarget.value == "") {
                        if ($input.data("last-value") == null) {
                            $input.data("last-value", ev.currentTarget.defaultValue);
                        }
                        var $lastDate = $input.data("last-value");
                        $input.datepicker("setDate", $lastDate);
                    } else {
                        $input.data("last-value", ev.currentTarget.value);
                    }
                }
            };
            
            $(".radio-inline").click(function () {
                var radio = $(this).find("input:radio");
    
                if (radio.length > 0) {
                    radio = radio[0];
                    if (radio.name == "EmployeeCompany[passport_isRf]") {
                        if (radio.value == 0) {
                            $(".field-physical-passport-country").removeClass("hide");
                            Passport_isRf.hideInputsMask();
                        }
                        else {
                            $(".field-physical-passport-country").addClass("hide");
                            Passport_isRf.showInputsMask();
                        }
                    }
                }
            });
            
        });
        $("#proxy-modal-container").modal("show");
        $("#proxy-proxy_person_id").val("").trigger("change");
    }
});

');
?>