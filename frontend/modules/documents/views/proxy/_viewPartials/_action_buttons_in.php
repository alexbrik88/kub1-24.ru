<?php
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\rbac\permissions;

$canUpdate = Yii::$app->user->can(permissions\document\Document::UPDATE_STATUS, [
    'model' => $model,
]);

/** @var \common\models\document\Proxy $model */
?>


<div class="row action-buttons buttons-fixed view-action-buttons" id="buttons-fixed">
    <div class="button-bottom-page-lg col-sm-1 col-xs-1" title="Отправить по e-mail">
        <?php if ($canUpdate): ?>
            <button class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs send-message-panel-trigger">
                Отправить
            </button>
            <button class="btn darkblue widthe-100 hidden-lg send-message-panel-trigger" title="Отправить">
                <span class="ico-Send-smart-pls fs"></span></button>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php echo Html::a('Печать', ['document-print', 'actionType' => 'print', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPrintTitle(),], [
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
            'target' => '_blank',
        ]);
        echo Html::a('<i class="fa fa-print fa-2x"></i>', ['document-print', 'actionType' => 'print', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPrintTitle(),], [
            'class' => 'btn darkblue widthe-100 hidden-lg',
            'target' => '_blank',
            'title' => 'Печать',
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <style>
            .dropdown-menu-mini {
                width: 100%;
                min-width: 98px;
                border-color: #4276a4 !important;
            }

            .dropdown-menu-mini a {
                padding: 7px 0px;
                text-align: center;
            }
        </style>
        <span class="dropup">
            <?= Html::a('Скачать', ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPdfFileName()], [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs dropdown-toggle',
                'target' => '_blank'
            ]); ?>
            <?= Html::a('<i class="glyphicon glyphicon-download" style="font-size: 17px;"></i>', ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPdfFileName()], [
                'class' => 'btn darkblue widthe-100 hidden-lg dropdown-toggle',
                'target' => '_blank'
            ]); ?>
        </span>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::DELETE)): ?>
            <button type="button" class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs" data-toggle="modal" href="#delete-confirm">Удалить</button>
            <button type="button" class="btn darkblue widthe-100 hidden-lg" data-toggle="modal" href="#delete-confirm" title="Удалить"><i class="fa fa-trash-o fa-2x"></i></button>
        <?php endif; ?>
    </div>
</div>

<?php if ($canUpdate): ?>
    <?= $this->render('@frontend/modules/documents/views/invoice/view/_send_message', [
        'model' => $model,
        'useContractor' => null,
        'header' => 'Отправить',
    ]); ?>
<?php endif; ?>
