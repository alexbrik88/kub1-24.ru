<?php

use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\company;
use common\models\document\OrderProxy;
use common\models\product\Product;
use frontend\modules\documents\components\Message;
use php_rutils\RUtils;
use frontend\models\Documents;
use common\models\document\Proxy;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\document\Proxy */
/* @var $message Message */
/* @var $ioType integer */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);
$dateNumFormatted = DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

$limitDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->limit_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);
$limitDateNumFormatted = DateHelper::format($model->limit_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'ot-tn out-document out-act';
$company = $model->invoice->company;
$precision = $model->invoice->price_precision;

// REQUISITES todo: сделать условия для type=out
$consignee_requisites = $model->invoice->company_name_short;
$consignee_requisites .= $model->invoice->company_inn ? ', ИНН ' . $model->invoice->company_inn : '';
$consignee_requisites .= $model->invoice->company_kpp ? ', КПП ' . $model->invoice->company_kpp : '';
$consignee_requisites .= $model->invoice->company_address_legal_full ? ', ' . $model->invoice->company_address_legal_full : '';

$consignee_bank_requisites = "р/с {$model->invoice->company_rs}" .
    ", в банке {$model->invoice->company_bank_name}" .
    ", БИК {$model->invoice->company_bik}" .
    ($model->invoice->company_ks ? ", к/с {$model->invoice->company_ks}" : '');

$consignor_requisites = $model->invoice->contractor_name_short;

/** @var \common\models\EmployeeCompany $proxyEmployee */
$proxyEmployee = \common\models\EmployeeCompany::find()->with(['employee'])->andWhere([
    'employee_id' => $model->proxy_person_id,
    'company_id' => $company->id,
])->one();
if (!$proxyEmployee) {
    $proxyEmployee = new \common\models\EmployeeCompany([
        'scenario' => 'create',
        'send_email' => true,
        'company_id' => $company->id,
        'employee_role_id' => \common\models\employee\EmployeeRole::ROLE_EMPLOYEE,
        'time_zone_id' => \common\models\TimeZone::DEFAULT_TIME_ZONE,
        'is_product_admin' => true,
    ]);
}

if ($proxyEmployee->passport_date_output) {
    $passportDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
        'date' => $proxyEmployee->passport_date_output,
        'format' => 'd F Y г.',
        'monthInflected' => true,
    ]);
} else {
    $passportDateFormatted = '';
}

?>

<style>
    .proxy .table {margin:0;width:100%;font-family: Arial, sans-serif;}
    .proxy {padding-top: 1px}
    .proxy td {font-size: 8pt;padding: 1px 2px;}
    .proxy td.fs6 {font-size: 6pt;}
    .proxy td.fs7 {font-size: 7pt;}
    .proxy td.fs8 {font-size: 8pt;}
    .proxy td.fs8-b {font-size: 8pt; font-weight: bold;}
    .proxy td.m-l {padding:0;margin:0;}
    .proxy td.p-l-r {padding-left:10px;padding-right:10px;}
    .proxy .bt {border-top: 1px solid #000;}
    .proxy .bl {border-left: 1px solid #000;}
    .proxy .br {border-right: 1px solid #000;}
    .proxy .bb {border-bottom: 1px solid #000;}
    .proxy .bt2 {border-top: 2px solid #000;}
    .proxy .bl2 {border-left: 2px solid #000;}
    .proxy .br2 {border-right: 2px solid #000;}
    .proxy .bb2 {border-bottom: 2px solid #000;}
    .proxy .bt2 {border-top: 2px solid #000;}
    .proxy .tc {text-align: center;}
    .proxy .va-t {vertical-align: top;}
    .proxy .va-b {vertical-align: bottom;}
    .proxy .va-m {vertical-align: middle;}
    .proxy .tr {text-align: right;}
    .proxy .pr2 {padding-right:2px}
    .proxy .pr5 {padding-right:5px}
    .proxy .tip {font-size:6pt; margin:0; padding:1px 0; line-height:6pt; text-align: center;}
    .proxy .bold {font-weight:bold;}

    <?php if (Yii::$app->request->get('actionType') == 'print') : ?>
        @media print {
            @page {
                size: A4 portrait;
                margin: 0;
            }
        }
        .proxy {padding-top:1cm; padding-bottom:1cm}
    <?php endif ?>

</style>

<div class="proxy <?php if (!isset($preview_only)): ?>page-content-in p-center<?php endif; ?>">
    <table class="table">
        <tr>
            <td width="15%" class="tc p-l-r">Номер доверенности</td>
            <td width="10%" class="tc p-l-r">Дата выдачи</td>
            <td width="15%" class="tc p-l-r">Срок действия</td>
            <td class="tc p-l-r">Должность и фамилия лица, которому выдана доверенность</td>
            <td width="20%" class="tc p-l-r">Расписка в получени доверенности</td>
        </tr>
        <tr>
            <td class="tc fs6">1</td>
            <td class="tc fs6">2</td>
            <td class="tc fs6">3</td>
            <td class="tc fs6">4</td>
            <td class="tc fs6">5</td>
        </tr>
        <tr>
            <td class="tc"><?= Html::encode($model->document_number) ?></td>
            <td class="tc"><?= Html::encode($dateNumFormatted) ?></td>
            <td class="tc"><?= Html::encode($limitDateNumFormatted) ?></td>
            <td class="tc"><?= Html::encode($proxyEmployee->getFio()) ?></td>
            <td class="tc"></td>
        </tr>
    </table>
    <table class="table" style="margin-top:-1px">
        <tr>
            <td width="30%" class="tc va-m">Поставщик</td>
            <td width="30%" class="tc va-m">Номер и дата наряда<br/>(замещающего наряд документа)<br/>или извещения</td>
            <td class="tc va-m">Номер и дата документа, подтверждающего выполнение поручения</td>
        </tr>
        <tr>
            <td class="tc fs6">6</td>
            <td class="tc fs6">7</td>
            <td class="tc fs6">8</td>
        </tr>
        <tr>
            <td class="tc">
                <?= Html::encode($consignor_requisites) ?>
            </td>
            <td class="tc"></td>
            <td class="tc"></td>
        </tr>
    </table>
    <table class="table no-border">
        <tr>
            <td class="tc fs6 bb">Линия отреза</td>
        </tr>
    </table>

    <div class="text-right font-size-6 line-h-small" style="margin-top:15pt">
        <p>Типовая межотраслевая форма № М-2</p>
        <p>Утверждена постановлением Госкомстата России от 30.10.97 № 71а</p>
    </div>
    <table class="table no-border">
        <tr>
            <td colspan="4"></td>
            <td class="tc bt bl br">коды</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td class="fs8 text-right pr2">Форма по ОКУД</td>
            <td class="fs8-b tc bt2 bl2 br2">0315001</td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td width="11%" class="tr pr5">Организация</td>
            <td class="bb">
                <?= Html::encode($consignee_requisites) ?>
            </td>
            <td width="13%" class="tr pr2">по ОКПО</td>
            <td width="10%" class="tc bl2 br2 bt bb2"><?= Html::encode($model->company->okpo) ?></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td class="tip">наименование организации</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <table class="table no-border" style="margin-top:15pt;margin-bottom:5pt">
        <tr>
            <td width="28%"></td>
            <td width="18%" class="fs8-b">ДОВЕРЕННОСТЬ №</td>
            <td width="31%" class="bb fs8-b"><?= Html::encode($model->document_number) ?></td>
            <td></td>
        </tr>
    </table>
    <table class="table no-border">
        <tr>
            <td width="29%" class="tr bold ">Дата выдачи:</td>
            <td width="17%" class="bb bold"><?= $dateFormatted ?></td>
            <td></td>
        </tr>
        <tr>
            <td class="tr bold ">Доверенность действительна по:</td>
            <td class="bb bold"><?= $limitDateFormatted ?></td>
            <td></td>
        </tr>
    </table>
    <table class="table no-border" style="margin-top:5pt">
        <tr>
            <td class="bb"><?= Html::encode($consignee_requisites) ?></td>
        </tr>
        <tr>
            <td class="tip">наименование потребителя и его адрес</td>
        </tr>
    </table>
    <table class="table no-border">
        <tr>
            <td class="bb"><?= Html::encode($consignee_requisites) ?></td>
        </tr>
        <tr>
            <td class="tip">наименование плательщика и его адрес</td>
        </tr>
    </table>
    <table class="table no-border">
        <tr>
            <td width="25%" class="tr va-t">Счет №</td>
            <td class="bb">
                <?= Html::encode($consignee_bank_requisites) ?>
            </td>
        </tr>
        <tr>
            <td></td>
            <td class="tip">наименование банка</td>
        </tr>
    </table>
    <table class="table no-border">
        <tr>
            <td width="25%" class="tr">Доверенность выдана</td>
            <td width="25%" class="bb"><?= Html::encode($proxyEmployee->position) ?></td>
            <td width="1%"></td>
            <td class="bb"><?= Html::encode($proxyEmployee->getFio()) ?></td>
        </tr>
        <tr>
            <td></td>
            <td class="tip">должность</td>
            <td width="1%"></td>
            <td class="tip">фамилия, имя, отчество</td>
        </tr>
    </table>
    <table class="table no-border">
        <tr>
            <td width="25%" class="tr">Паспорт: серия</td>
            <td width="18%" class="bb"><?= Html::encode($proxyEmployee->passport_series) ?></td>
            <td width="2%">№</td>
            <td width="20%" class="bb"><?= Html::encode($proxyEmployee->passport_number) ?></td>
            <td></td>
        </tr>
        <tr>
            <td class="tr">Кем выдан</td>
            <td colspan="4" class="bb"><?= Html::encode($proxyEmployee->passport_issued_by) ?></td>
        </tr>
        <tr>
            <td class="tr">Дата выдачи</td>
            <td class="bb"><?= Html::encode($passportDateFormatted) ?></td>
            <td colspan="3"></td>
        </tr>
    </table>
    <table class="table no-border">
        <tr>
            <td width="25%" class="tr">На получение от</td>
            <td class="bb"><?= Html::encode($consignor_requisites) ?></td>
        </tr>
        <tr>
            <td></td>
            <td class="tip">наименование поставщика</td>
        </tr>
        <tr>
            <td width="25%" class="tr">материальных ценностей по</td>
            <td class="bb"></td>
        </tr>
        <tr>
            <td></td>
            <td class="tip">наименование, номер и дата документа</td>
        </tr>
        <tr>
            <td colspan="2" class="bb">&nbsp;</td>
        </tr>
    </table>

    <div class="text-center font-size-8" style="font-weight:bold">
        <p>Перечень товарно-материальных ценностей, подлежащих получению</p>
    </div>

    <table class="table">
        <tr>
            <td width="10%" class="tc">Номер по порядку</td>
            <td class="tc">Материальные ценности</td>
            <td width="10%" class="tc">Единица измерения</td>
            <td width="25%" class="tc">Количество (прописью)</td>
        </tr>
        <tr>
            <td class="tc fs6">1</td>
            <td class="tc fs6">2</td>
            <td class="tc fs6">3</td>
            <td class="tc fs6">4</td>
        </tr>
        <?php
        $orderQuery = OrderProxy::find()->where(['proxy_id'=>$model->id]);
        $orderArray = $orderQuery->all();
        foreach ($orderArray as $key => $order):
            if ($order->quantity != intval($order->quantity)) {
                $order->quantity = rtrim(number_format($order->quantity, 10, '.', ''), 0);
            }
            ?>
            <tr>
                <td class="tr"><?=($key+1)?></td>
                <td><?= Html::encode($order->order->product_title); ?></td>
                <td class="tc"><?= $order->order->unit ? $order->order->unit->name : ''; ?></td>
                <td class="tc"><?= RUtils::numeral()->getInWords($order->quantity, RUtils::MALE) ?></td>
            </tr>
        <?php endforeach; ?>
    </table>

    <table class="table no-border" style="margin-top:10pt">
        <tr>
            <td width="35%">Подпись лица, получившего доверенность</td>
            <td width="30%" class="bb"></td>
            <td style="padding-left:5pt">удостоверяем</td>
        </tr>
    </table>

    <table class="table no-border" style="margin-top:15pt">
        <tr>
            <td width="20%" class="bold">Руководитель</td>
            <td width="20%" class="bb"></td>
            <td width="1%"></td>
            <td width="20%" class="bb"><?= Html::encode($company->getChiefFio(true)) ?></td>
            <td></td>
        </tr>
        <tr>
            <td class="tc" style="padding:10px">М.П.</td>
            <td class="tip">подпись</td>
            <td></td>
            <td class="tip">расшифровка подписи</td>
            <td></td>
        </tr>
        <tr>
            <td class="tc"></td>
            <td colspan="4"></td>
        </tr>
        <tr>
            <td class="bold">Главный бухгалтер</td>
            <td class="bb"></td>
            <td></td>
            <td class="bb"><?= Html::encode($model->invoice->getCompanyChiefAccountantFio(true)) ?></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="tip">подпись</td>
            <td></td>
            <td class="tip">расшифровка подписи</td>
            <td></td>
        </tr>
    </table>

</div>

<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id): ?>
    <!--<pagebreak />-->
<?php endif; ?>


<?php /** @var BOOLEAN $addStamp */
$addStamp = (boolean) $model->add_stamp;
if ($model->signed_by_name) {
    $accountantSignatureLink = $signatureLink = (!$addStamp || !$model->employeeSignature) ? null:
        EasyThumbnailImage::thumbnailSrc($model->employeeSignature->file, 83, 25, EasyThumbnailImage::THUMBNAIL_INSET);
} else {
    $signatureLink = (!$addStamp) ? null:
        EasyThumbnailImage::thumbnailSrc($model->getImage('chiefSignatureImage'), 83, 25, EasyThumbnailImage::THUMBNAIL_INSET);

    if (!$company->chief_is_chief_accountant) {
        $accountantSignatureLink = (!$addStamp) ? null:
            EasyThumbnailImage::thumbnailSrc($model->getImage('chiefAccountantSignatureImage'), 83, 25, EasyThumbnailImage::THUMBNAIL_INSET);
    } else {
        $accountantSignatureLink = $signatureLink;
    }
}
$printLink = (!$addStamp) ? null:
    EasyThumbnailImage::thumbnailSrc($model->getImage('printImage'), 200, 200, EasyThumbnailImage::THUMBNAIL_INSET);
?>
<style>
    @media print {
        thead {display: table-header-group;}
    }
    <?php if (Yii::$app->request->get('actionType') == 'print') : ?>
    @page {
        size:A4 landscape;
    }
    <?php endif ?>
    <?php if ($addStamp) : ?>
    #print_layer {
        height: 230px;
        background-image: url('<?= $printLink ?>');
        background-position: 10% 30px;
        background-repeat: no-repeat;
        -webkit-print-color-adjust: exact;
    }
    #signature1_layer {
        background-image: url('<?= $signatureLink ?>');
        background-position: 30% <?= $model->signed_by_employee_id ? '41px' : '35px'; ?>;
        background-repeat: no-repeat;
        -webkit-print-color-adjust: exact;
    }
    #signature2_layer {
        background-image: url('<?= $signatureLink ?>');
        background-position: 30% <?= $model->signed_by_employee_id ? '85px' : '60px'; ?>;
        background-repeat: no-repeat;
        -webkit-print-color-adjust: exact;
    }
    <?php endif ?>

</style>