<?php

use common\components\date\DateHelper;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\models\Contractor;
use common\models\document\PaymentOrder;
use frontend\modules\documents\components\FilterHelper;
use frontend\modules\documents\widgets\PaymentOrderPaid;
use frontend\rbac\permissions;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\models\Documents;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\documents\models\PaymentOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Платёжные поручения';
$this->params['breadcrumbs'][] = $this->title;

$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canStatus = Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = Yii::$app->getUser()->can(permissions\document\Document::VIEW);
?>

<div class="portlet box">
    <div class="btn-group pull-right title-buttons">
        <?php if (frontend\rbac\permissions\document\PaymentOrder::CREATE): ?>
            <?= Html::a('<i class="fa fa-plus"></i> ДОБАВИТЬ', ['create'], ['class' => 'btn yellow']) ?>
        <?php endif; ?>
    </div>
    <h3 class="page-title"><?= Html::encode($this->title); ?></h3>

    <div class="row" style="margin: 25px 0 -25px 0;">
        <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_no_right',]); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?= Html::a('<i class="fa fa-file-excel-o"></i>', Url::current(['xls' => 1]), [
            'class' => 'get-xls-link pull-right',
            'title' => 'Скачать в Excel',
        ]); ?>
    </div>
</div>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Список платёжных поручений
        </div>
        <div class="tools search-tools tools_button col-sm-3">
            <div class="form-body">
                <?php $form = \yii\widgets\ActiveForm::begin([
                    'method' => 'GET',
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'options' => [
                            'class' => '',
                        ],
                    ],
                ]); ?>
                <div class="search_cont">
                    <div class="wimax_input ">
                        <?= $form->field($searchModel, 'byNumber')->textInput([
                            'placeholder' => 'Номер платежного поручения или название контрагента',
                        ]); ?>
                    </div>
                    <div class="wimax_button">
                        <?= Html::submitButton('НАЙТИ', [
                            'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                        ]) ?>
                    </div>
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
        <div class="actions joint-operations col-sm-5" style="display:none; min-width: 430px; padding-right: 0 !important;">
            <?php if ($canStatus) : ?>
                <?= PaymentOrderPaid::widget([
                    'id' => 'many-paid',
                    'toggleButton' => [
                        'tag' => 'a',
                        'label' => 'Оплачено',
                        'class' => 'btn btn-default btn-sm',
                        'data-pjax' => 0,
                    ],
                ]) ?>
            <?php endif ?>
            <?php if ($canPrint) : ?>
                <?= Html::a('Импорт в банк', Url::to(['import']), [
                    'class' => 'btn btn-default btn-sm multiple-import-link no-ajax-loading',
                    'data-url' => Url::to(['import']),
                    'data-pjax' => 0,
                ]); ?>
            <?php endif ?>
            <?php if ($canDelete) : ?>
                <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить </a>', '#many-delete', [
                    'class' => 'btn btn-default btn-sm',
                    'data-toggle' => 'modal',
                    'data-pjax' => 0,
                ]); ?>
                <div id="many-delete" class="confirm-modal fade modal" role="dialog"
                     tabindex="-1" aria-hidden="true"
                     style="display: none; margin-top: -51.5px;">
                    <div class="modal-dialog ">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="form-body">
                                    <div class="row">Вы уверены, что хотите удалить
                                        выбранные платежные поручения?
                                    </div>
                                </div>
                                <div class="form-actions row">
                                    <div class="col-xs-6">
                                        <?= Html::a('ДА', null, [
                                            'class' => 'btn darkblue pull-right modal-many-delete',
                                            'data-url' => Url::to(['many-delete',]),
                                        ]); ?>
                                    </div>
                                    <div class="col-xs-6">
                                        <button type="button" class="btn darkblue"
                                                data-dismiss="modal">НЕТ
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif ?>
            <?php if ($canPrint) : ?>
                <?= Html::a('<i class="fa fa-print"></i> Печать', Url::to([
                    'many-document-print', 'actionType' => 'pdf', 'type' => Documents::IO_TYPE_IN, 'multiple' => '']), [
                    'class' => 'btn btn-default btn-sm multiple-print',
                    'target' => '_blank',
                    'data-pjax' => 0,
                ]); ?>
            <?php endif ?>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <?php yii\widgets\Pjax::begin(['id' => 'payment-order-search-pjax']); ?>
            <div class="table-container" style="">
                <div class="dataTables_wrapper dataTables_extended_wrapper">

                    <?= common\components\grid\GridView::widget([
                        'id' => 'payment-order-search',
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'emptyText' => 'Вы еще не создали ни одного платежного поручения.',
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover dataTable documents_table',
                            'aria-describedby' => 'datatable_ajax_info',
                            'role' => 'grid',
                        ],

                        'headerRowOptions' => [
                            'class' => 'heading',
                        ],

                        'options' => [
                            'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'pagination pull-right',
                            ],
                        ],
                        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                        'columns' => [
                            [
                                'header' => Html::checkbox('', false, [
                                    'class' => 'joint-operation-main-checkbox',
                                ]),
                                'headerOptions' => [
                                    'class' => 'text-center',
                                    'width' => '5%',
                                ],
                                'contentOptions' => [
                                    'class' => 'text-center',
                                ],
                                'format' => 'raw',
                                'value' => function (PaymentOrder $model) {
                                    return Html::checkbox('PaymentOrder[' . $model->id . '][checked]', false, [
                                        'class' => 'joint-operation-checkbox',
                                    ]);

                                },
                            ],
                            [
                                'attribute' => 'document_date',
                                'label' => 'Дата ПП',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                ],
                                'format' => 'raw',
                                'value' => function (PaymentOrder $data) {
                                    return DateHelper::format($data->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                                },
                            ],
                            [
                                'attribute' => PaymentOrder::tableName() . '.`document_number` * 1',
                                'label' => '№ ПП',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                ],
                                'format' => 'raw',
                                'value' => function (PaymentOrder $data) {
                                    return Html::a($data->document_number, ['payment-order/view', 'id' => $data->id]);
                                },
                            ],
                            [
                                'label' => 'Сумма',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '9%',
                                ],
                                'format' => 'raw',
                                'attribute' => 'sum',
                                'value' => function (PaymentOrder $data) {
                                    $price = \common\components\TextHelper::invoiceMoneyFormat($data->sum, 2);
                                    return '<span class="price" data-price="' . str_replace(" ", "", $price) . '">' . $price . '</span>';
                                },
                            ],
                            [
                                'attribute' => 'contractor_name',
                                'label' => 'Контрагент',
                                'class' => DropDownSearchDataColumn::className(),
                                'headerOptions' => [
                                    'class' => 'dropdown-filter',
                                ],
                                'filter' => array_merge(['' => 'Все'], ArrayHelper::map($searchModel->contractorArray, 'contractor_name', 'contractor_name')),
                                'format' => 'raw',
                                'value' => function (PaymentOrder $model) {
                                    return '<span title="' . htmlspecialchars($model->contractor_name) . '">' . $model->contractor_name . '</span>';
                                },
                            ],
                            [
                                'attribute' => 'payment_order_status_id',
                                'label' => 'Статус',
                                'class' => DropDownSearchDataColumn::className(),
                                'headerOptions' => [
                                    'class' => 'dropdown-filter',
                                ],
                                'filter' => $searchModel->getStatusFilter(),
                                'format' => 'raw',
                                'value' => function (PaymentOrder $model) {
                                    return $model->paymentOrderStatus->name;
                                },
                            ],
                            [
                                'attribute' => 'id',
                                'label' => 'Счета в ПП',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                ],
                                'format' => 'raw',
                                'value' => function (PaymentOrder $data) {
                                    $result = null;
                                    if ($data->paymentOrderInvoices) {
                                        foreach ($data->getPaymentOrderInvoices()
                                                     ->joinWith('invoice')
                                                     ->orderBy([
                                                         'cast(document_number as unsigned)' => SORT_ASC,
                                                     ])
                                                     ->all() as $paymentOrderInvoice) {
                                            if ($paymentOrderInvoice->invoice !== null) {
                                                $invoiceNumber = '№ ' . $paymentOrderInvoice->invoice->fullNumber;
                                                $result .= '<div>' .
                                                    (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                                                        'model' => $paymentOrderInvoice->invoice,
                                                    ])
                                                        ? Html::a($invoiceNumber, ['/documents/invoice/view', 'type' => $paymentOrderInvoice->invoice->type, 'id' => $paymentOrderInvoice->invoice_id])
                                                        : $invoiceNumber) . '</div>';
                                            }
                                        }
                                    }

                                    return $result;
                                },
                            ],
                            [
                                'attribute' => 'document_author_id',
                                'label' => 'Ответственный',
                                'class' => DropDownSearchDataColumn::className(),
                                'headerOptions' => [
                                    'class' => 'dropdown-filter',
                                ],
                                'filter' => $searchModel->getAuthorFilter(),
                                'format' => 'raw',
                                'value' => function (PaymentOrder $model) {
                                    return $model->documentAuthor->getFio(true);
                                },
                            ],
                        ],
                    ]);
                    ?>
                </div>
            </div>
        <?php yii\widgets\Pjax::end(); ?>
    </div>
</div>

<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a('<i class="fa fa-print"></i> Печать', [
            'many-document-print',
            'actionType' => 'pdf',
            'type' => Documents::IO_TYPE_IN,
            'multiple' => '',
        ], [
            'class' => 'btn btn-sm darkblue text-white multiple-print',
            'target' => '_blank',
            'data-pjax' => 0,
        ]) : null,
        $canDelete ? Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить </a>', '#many-delete', [
            'class' => 'btn btn-sm darkblue text-white',
            'data-toggle' => 'modal',
            'data-pjax' => 0,
        ]) : null,
        $canPrint ? Html::a('Импорт в банк', Url::to(['import']), [
            'class' => 'btn btn-sm darkblue text-white multiple-import-link no-ajax-loading',
            'data-url' => Url::to(['import']),
            'data-pjax' => 0,
        ]) : null,
        $canStatus ? Html::a('Оплачено', '#many-paid', [
            'class' => 'btn btn-sm darkblue text-white',
            'data-toggle' => 'modal',
            'data-pjax' => 0,
        ]) : null,
    ],
]); ?>
