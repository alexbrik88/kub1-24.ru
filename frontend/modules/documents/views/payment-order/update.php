<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $company common\models\Company */

//$this->title = 'Обновить платёжное поручение: ' . ' ' . $model->document_number;
$this->params['breadcrumbs'][] = ['label' => 'Payment Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
$this->context->layoutWrapperCssClass = 'out-act out-document payment-order';
?>
<div class="payment-order-update m-t-n-lg">

    <div class="portlet box">
        <h3 class="page-title"><?= Html::encode($this->title); ?></h3>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'company' => $company,
    ]) ?>

</div>
