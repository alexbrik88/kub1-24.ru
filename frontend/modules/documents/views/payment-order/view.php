<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 25.05.2018
 * Time: 19:17
 */

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $company common\models\Company */

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'out-act out-document payment-order';
?>
<div class="page-content-in" style="padding-bottom: 30px;margin-top: -13px;">
    <div style="col-xs-12 pad3">
        <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\PaymentOrder::INDEX)): ?>
            <?= Html::a('Назад к списку', Url::toRoute(['index'])) ?>
        <?php endif; ?>
    </div>
    <div class="col-xs-12 pad0">
        <div class="portlet-body col-xs-12 col-lg-7"
             style="padding-left: 5px;padding-right: 5px;min-width: 520px;max-width: 735px;border: 1px solid rgb(66, 118, 164);margin-top: 8px;min-height: 815px;">
            <?= $this->render('view/main-info', [
                'model' => $model,
                'company' => $company,
            ]); ?>
        </div>
        <div class="col-xs-12 col-lg-5 pad0 pull-right"
             style="padding-bottom: 5px !important; max-width: 480px;margin-top: 5px;">
            <div class="col-xs-12" style="padding-right:0 !important;">
                <div class="control-panel col-xs-12 pad0 pull-right">
                    <?= $this->render('view/status-block', [
                        'model' => $model,
                    ]); ?>
                </div>
                <div class="col-xs-12 col-lg-9 pull-right pad3" style="max-width: 720px;margin-top: 5px;">
                    <?= $this->render('view/additional-info', [
                        'model' => $model,
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

    <div id="buttons-bar-fixed">
        <?= $this->render('view/action-buttons', [
            'model' => $model,
        ]); ?>
    </div>
</div>


