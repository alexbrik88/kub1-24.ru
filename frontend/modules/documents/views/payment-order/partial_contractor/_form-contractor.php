<?php

use frontend\widgets\ConfirmModalWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Contractor;
use common\models\company\CompanyType;
use common\components\date\DateHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $cancelUrl string */

/* @var $form ActiveForm */
$textInputConfig = [
    'labelOptions' => [
        'class' => 'control-label col-md-4',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-8 inp_one_line width-inp',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n sel-w',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
$checkboxConfig = [
    'labelOptions' => [
        'class' => 'control-label col-md-4',
        'style' => 'padding-top: 0px;'
    ],
    'wrapperOptions' => [
        'class' => 'col-md-8 inp_one_line',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n sel-w',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
$partialDirectorTextInputConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'control-label col-md-4',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-8 field-width inp_one_line',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
?>

<?php $form = ActiveForm::begin([
    'options' => [
        'class' => '',
    ],
    'id' => 'new-contractor-invoice-form',
    'enableClientValidation' => true,
    'validateOnSubmit' => true,
    'validateOnBlur' => false,
]); ?>
<?= Html::hiddenInput('documentType', $documentType); ?>
<?php echo $form->errorSummary($model);
$model->face_type = isset($model->face_type) ? $model->face_type : 0;
echo $form->field($model, 'face_type', [
    'options' => [
        'class' => 'form-group row',
    ],
])
    ->label($model->type == Contractor::TYPE_SELLER ? 'Тип поставщика: ' : 'Тип покупателя: ', ['class' => 'col-md-4 col-sm-4'])
    ->radioList(
        [
            Contractor::TYPE_LEGAL_PERSON => 'Юридическое лицо',
            Contractor::TYPE_PHYSICAL_PERSON => 'Физическое лицо',
        ],
        [
            'class' => 'radio-list type lastItemMrgRight0 col-sm-7 col-md-7',
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::tag('label',
                    Html::radio($name, $checked, ['value' => $value]) . $label,
                    [
                        'class' => 'radio-inline m-l-20',
                    ]);
            },
        ]); ?>
<div class="forContractor row <?= !$model->face_type ? '' : 'selectedDopColumns' ?>">
    <div class="form-body form-horizontal col-md-12">
        <?= $this->render('_partial_main_info', [
            'model' => $model,
            'form' => $form,
            'class' => 'required',
            'textInputConfigDirector' => $partialDirectorTextInputConfig,
            'face_type_opt' => $face_type_opt,
            'textInputConfig' => $textInputConfig,
        ]) ?>
    </div>
    <div class="clearfix"></div>
</div>

<div class="forContractor row <?= $model->face_type ? '' : 'selectedDopColumns' ?>">
    <div class="form-body form-horizontal col-md-12">
        <?= $this->render('_partial_main_info_physical', [
            'model' => $model,
            'form' => $form,
            'class' => 'required',
            'textInputConfigDirector' => $partialDirectorTextInputConfig,
            'face_type_opt' => $face_type_opt,
            'textInputConfig' => $textInputConfig,
            'checkboxConfig' => $checkboxConfig,
        ]) ?>
    </div>
    <div class="clearfix"></div>
</div>
<div class="row">
    <div class="form-body form-horizontal col-md-12">
        <?= $this->render('_partial_bank', [
            'model' => $model,
            'form' => $form,
            'class' => 'required',
            'textInputConfigDirector' => $partialDirectorTextInputConfig,
            'face_type_opt' => $face_type_opt,
            'textInputConfig' => $textInputConfig,
        ]) ?>
    </div>
</div>
<div class="clearfix"></div>
<div class="form-actions">
    <div class="row action-buttons">
        <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width: 23.45%;">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => $model->isNewRecord ? 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs ladda-button' : 'btn btn-primary widthe-100 hidden-md hidden-sm hidden-xs ladda-button',
                'data-style' => 'expand-right',
                'style' => 'width: 100%!important;',
            ]) ?>
            <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                'class' => $model->isNewRecord ? 'btn darkblue widthe-100 hidden-lg' : 'btn btn-primary widthe-100 hidden-lg'
            ]) ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?php
            if (!isset($cancelUrl)) {
                $cancelUrl = $model->isNewRecord
                    ? Url::to(['/contractor/index', 'type' => $model->type])
                    : Url::to(['/contractor/view', 'type' => $model->type, 'id' => $model->id,]);
            }
            ?>
            <a href="#"
               class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs">Отменить</a>
            <a href="#" class="btn darkblue widthe-100 hidden-lg"
               title="Отменить"><i class="fa fa-reply fa-2x"></i></a>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">

        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<script type="text/javascript">
    function contractorIsIp() {
        return $('#contractor-companytypeid').val() == "<?= CompanyType::TYPE_IP ?>";
    }
    function contractorIsNotIp() {
        return $('#contractor-companytypeid').val() && !contractorIsIp();
    }

    function contractorIsPhysical() {
        return $("#contractor-face_type input:checked").val() == "<?= Contractor::TYPE_PHYSICAL_PERSON ?>";
    }
    function contractorIsLegal() {
        return $("#contractor-face_type input:checked").val() == "<?= Contractor::TYPE_LEGAL_PERSON ?>";
    }
    function contractorIsForeignLegal() {
        return $("#contractor-face_type input:checked").val() == "<?= Contractor::TYPE_FOREIGN_LEGAL_PERSON ?>";
    }
    function contractorIsAgent() {
        return $("#contractor_is_agent_input:checked").length;
    }

    $('#contractor-companytypeid').change(function () {
        if ($(this).val() == "<?= CompanyType::TYPE_IP ?>") {
            $('.field-contractor-ppc').hide();
        } else {
            $('.field-contractor-ppc').show();
        }
        $('#new-contractor-invoice-form').yiiActiveForm('validateAttribute', 'contractor-itn');
    });
    $('#contractor-name').on('input', function () {
        if ($('#contractor-companytypeid').val() == '1') {
            $('#contractor-director_name').val($('#contractor-name').val());
        }
    });
    $('#contractor-companytypeid').on('change', function () {
        if ($('#contractor-companytypeid').val() == '1') {
            $('#contractor-director_name').val($('#contractor-name').val());
        }
    });
</script>
