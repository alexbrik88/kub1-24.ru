<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 25.05.2018
 * Time: 20:32
 */

use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use frontend\widgets\ConfirmModalWidget;
use frontend\models\Documents;
use frontend\rbac\UserRole;
use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */

$items = [
    [
        'label' => '<span style="display: inline-block;">Импорт в файл</span>',
        'encode' => false,
        'url' => ['import', 'id' => $model->id],
        'linkOptions' => [
            'target' => '_blank',
        ]
    ],
];
if ((Yii::$app->user->can(UserRole::ROLE_CHIEF) || Yii::$app->user->can(UserRole::ROLE_ACCOUNTANT)) &&
    ($paymentAccount = $model->company->getBankingPaymentAccountants()->one()) !== null &&
    ($alias = Banking::aliasByBik($paymentAccount->bik))
) {
    $items[] = [
        'label' => '<span style="display: inline-block;">Отправить в банк</span>',
        'encode' => false,
        'linkOptions' => [
            'class' => 'banking-module-open-link',
        ],
        'url' => [
            "/cash/banking/{$alias}/default/payment",
            'account_id' => $paymentAccount->id,
            'po_id' => $model->id,
            'p' => Banking::routeEncode(['/documents/payment-order/view', 'id' => $model->id]),
        ],
    ];
}
?>

<div class="row action-buttons margin-no-icon">
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <div class="dropup">
            <?= Html::a('Импорт в банк', '#', [
                'class' => 'btn darkblue widthe-100 dropdown-toggle',
                'data-toggle' => 'dropdown'
            ]); ?>
            <?= Dropdown::widget([
                'options' => [
                    'style' => '',
                    'class' => 'dropdown-menu-mini'
                ],
                'items' => $items,
            ]); ?>
            <?= BankingModalWidget::widget([
                'pageTitle' => $this->title,
                'pageUrl' => Url::to(['view', 'id' => $model->id]),
            ]) ?>
        </div>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?= Html::a('Печать', ['document-print', 'actionType' => 'print', 'id' => $model->id, 'type' => Documents::IO_TYPE_IN, 'filename' => $model->getPrintTitle(),], [
            'target' => '_blank',
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs reload-status print',
        ]); ?>
        <?= Html::a('<i class="fa fa-print fa-2x"></i>', ['document-print', 'actionType' => 'print', 'id' => $model->id, 'type' => Documents::IO_TYPE_IN, 'filename' => $model->getPrintTitle(),], [
            'target' => '_blank',
            'title' => 'Печать',
            'class' => 'btn darkblue widthe-100 hidden-lg reload-status print',
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?= Html::a('PDF', ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'type' => Documents::IO_TYPE_IN, 'filename' => $model->getPdfFileName(),], [
            'target' => '_blank',
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
        ]); ?>
        <?= Html::a('<i class="fa fa-file-pdf-o fa-2x"></i>', ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'type' => Documents::IO_TYPE_IN, 'filename' => $model->getPdfFileName(),], [
            'target' => '_blank',
            'title' => 'PDF',
            'class' => 'btn darkblue widthe-100 hidden-lg',
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\PaymentOrder::VIEW)
            && Yii::$app->user->can(frontend\rbac\permissions\document\PaymentOrder::CREATE)
        ): ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => 'Копировать',
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ],
                'confirmUrl' => Url::to(['copy', 'id' => $model->id,]),
                'message' => 'Вы уверены, что хотите скопировать это платёжное поручение?',
            ]);
            ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => '<i class="fa fa-files-o fa-2x"></i>',
                    'title' => 'Копировать',
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                ],
                'confirmUrl' => Url::to(['copy', 'id' => $model->id,]),
                'message' => 'Вы уверены, что хотите скопировать это платёжное поручение?',
            ]);
            ?>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\PaymentOrder::DELETE)): ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => 'Удалить',
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ],
                'confirmUrl' => Url::to(['delete', 'id' => $model->id,]),
                'confirmParams' => [],
                'message' => 'Вы уверены, что хотите удалить платежное поручение?',
            ]); ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => '<i class="fa fa-trash-o fa-2x"></i>',
                    'title' => 'Удалить',
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                ],
                'confirmUrl' => Url::to(['delete', 'id' => $model->id,]),
                'confirmParams' => [],
                'message' => 'Вы уверены, что хотите удалить платежное поручение?',
            ]); ?>
        <?php endif; ?>
    </div>
</div>
