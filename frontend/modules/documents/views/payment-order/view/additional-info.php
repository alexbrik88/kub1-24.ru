<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 25.05.2018
 * Time: 20:30
 */

use yii\helpers\Html;
use yii\helpers\Url;
use common\components\date\DateHelper;
use frontend\rbac\permissions\document\Document;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */

$is_budget_payment = (bool)$model->presence_status_budget_payment;
?>
<div class="portlet">
    <div class="customer-info" style="min-height:auto!important">
        <div class="portlet-body no_mrg_bottom main_inf_no-bord">
            <table class="table no_mrg_bottom">
                <tr>
                    <td>
                        <span class="customer-characteristic">
                            <?php if ($is_budget_payment) : ?>
                                Бюджетный платеж.<br/>
                                Получатель:
                            <?php else : ?>
                                Поставщик:
                            <?php endif; ?>
                        </span>
                        <span>
                            <?= $model->contractor ? Html::a($model->contractor_name, [
                                '/contractor/view',
                                'type' => $model->contractor->type,
                                'id' => $model->contractor->id,
                            ]) : $model->contractor_name; ?>
                        </span>
                    </td>
                </tr>
                <?php if (!$is_budget_payment) : ?>
                    <tr>
                        <td>
                            <span class="customer-characteristic">
                                Счета в платежном поручении:
                            </span>
                            <?php foreach ($model->getPaymentOrderInvoices()
                                               ->joinWith('invoice')
                                               ->orderBy([
                                                   'cast(document_number as unsigned)' => SORT_ASC,
                                               ])
                                               ->all() as $paymentOrderInvoice): ?>
                                <span style="display: block;">
                                    <?php if ($paymentOrderInvoice->invoice && Yii::$app->user->can(Document::VIEW, [
                                            'model' => $model,
                                        ])
                                    ): ?>
                                        <?= Html::a('№ ' . $paymentOrderInvoice->invoice->fullNumber .
                                            ' от ' . DateHelper::format($paymentOrderInvoice->invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                                            Url::to(['/documents/invoice/view', 'id' => $paymentOrderInvoice->invoice->id, 'type' => $paymentOrderInvoice->invoice->type])); ?>
                                    <?php else: ?>
                                        № <?= $paymentOrderInvoice->invoice->fullNumber; ?>
                                        от <?= DateHelper::format($paymentOrderInvoice->invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                    <?php endif; ?>
                                </span>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                <?php endif; ?>
            </table>
        </div>
    </div>
</div>
