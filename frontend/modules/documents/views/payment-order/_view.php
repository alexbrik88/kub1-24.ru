<?php

use common\components\date\DateHelper;
use common\models\company\CompanyType;
use php_rutils\RUtils;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $company common\models\Company */

?>
<div class="portlet-body">
    <div class="row no-margin-l">
        <div class="col-md-offset-7">
            <div class="data-block pull-left">
                <div class="data-block__data">
                    <?= RUtils::dt()->ruStrFTime([
                        'format' => \common\components\date\DateHelper::FORMAT_USER_DATE,
                        'date' => $model->document_date,
                    ]); ?>
                </div>
                <div class="data-block__title">
                    дата
                </div>
            </div>
            <div class="data-block pull-left">
                <div class="data-block__data">
                </div>
                <div class="data-block__title">
                    вид платежа
                </div>
            </div>
            <div class="data-block pull-left" style="margin: 0;">
                <div style="display: inline-block; padding: 3px 5px; border: 1px solid #000; min-width: 26px;">
                    <?= $model->taxpayersStatus ? $model->taxpayersStatus->code : '&nbsp;'; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="summery clearfix">
        <div class="height-30 summery__sum pull-left">
            Сумма прописью
        </div>
        <div class="height-30 summery__sum-in-words pull-left">
            <?= $model->sum_in_words; ?>
        </div>
    </div>
    <div class="clearfix payment-order__table">
        <div class="width-60 left-block pull-left">
            <div class="width-50 height-30 pull-left border-bootom-right">ИНН <?= $company->inn; ?></div>
            <div class="width-50 height-30 pull-left border-bootom">КПП <?= !empty($model->company_kpp) ? $model->company_kpp : ''; ?></div>
            <div class="width-100 pull-left">
                <div class="height-60">
                    <?= $company->getTitle($company->company_type_id == CompanyType::TYPE_IP ? false : true); ?>
                </div>
                <div class="height-30 bold-text border-bootom">Плательщик</div>
            </div>
            <div class="width-100 pull-left">
                <div class="height-30"><?= $company->bank_name; ?></div>
                <div class="height-30 bold-text border-bootom">Банк плательщика</div>
            </div>
            <div class="width-100 pull-left">
                <div class="height-30"><?= $model->contractor_bank_name; ?></div>
                <div class="height-30 bold-text border-bootom">Банк получателя</div>
            </div>
            <div class="width-50 height-30 pull-left border-bootom-right">ИНН <?= $model->contractor_inn; ?></div>
            <div class="width-50 height-30 pull-left border-bootom">КПП <?= $model->contractor_kpp; ?></div>
            <div class="width-100 pull-left">
                <div class="height-90"><?= $model->contractor_name; ?></div>
                <div class="height-30 bold-text">Получатель</div>
            </div>
        </div>
        <div class="width-40 right-block pull-left border-left">
            <div class="width-30 height-60 pull-left border-bootom-right">Сумма</div>
            <div
                class="width-70 height-60 pull-left border-bootom"><?= \common\components\TextHelper::invoiceMoneyFormat($model->sum,
                    2); ?></div>
            <div class="width-30 height-60 pull-left border-bootom-right">Сч №</div>
            <div class="width-70 height-60 pull-left"><?= $company->mainCheckingAccountant->rs; ?></div>
            <div class="width-30 height-30 pull-left border-bootom-right">БИК</div>
            <div class="width-70 height-30 pull-left"><?= $company->mainCheckingAccountant->bik; ?></div>
            <div class="width-30 height-30 pull-left border-bootom-right">Сч №</div>
            <div class="width-70 height-30 pull-left border-bootom"><?= $company->mainCheckingAccountant->ks; ?></div>
            <div class="width-30 height-30 pull-left border-bootom-right">БИК</div>
            <div class="width-70 height-30 pull-left border-bootom"><?= $model->contractor_bik; ?></div>
            <div class="width-30 height-30 pull-left border-bootom-right">Сч №</div>
            <div
                class="width-70 height-30 pull-left border-bootom"><?= $model->contractor_corresponding_account; ?></div>
            <div class="width-30 height-60 pull-left border-right">Сч №</div>
            <div class="width-70 height-60 pull-left"><?= $model->contractor_current_account; ?></div>
            <div class="width-100 pull-left">
                <div class="width-50 pull-left border-top">
                    <div class="width-60 height-30 pull-left border-bootom-right">Вид оп.</div>
                    <div class="width-40 height-30 pull-left">
                        <?= (!empty($model->operation_type_id)) ? $model->operationType->code : ''; ?>
                    </div>
                    <div class="width-60 height-30 pull-left border-bootom-right">Наз. пл.</div>
                    <div class="width-40 height-30 pull-left"></div>
                    <div class="width-60 height-30 pull-left border-right">Код</div>
                    <div class="width-40 height-30 pull-left"><?= $model->uin_code; ?></div>
                </div>
                <div class="width-50 pull-left border-left border-top">
                    <div class="width-60 height-30 pull-left border-bootom-right">Срок пл.</div>
                    <div class="width-40 height-30 pull-left"><?= DateHelper::format($model->payment_limit_date,
                            DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?></div>
                    <div class="width-60 height-30 pull-left border-bootom-right">Очер. пл.</div>
                    <div class="width-40 height-30 pull-left"><?= $model->ranking_of_payment; ?></div>
                    <div class="width-60 height-30 pull-left border-right">Рез. поле</div>
                    <div class="width-40 height-30 pull-left"></div>
                </div>
            </div>
        </div>
        <div class="width-100 pull-left border-top">
            <div class="height-30 pull-left border-right" style="width:30%"><?= $model->kbk; ?></div>
            <div class="height-30 pull-left border-right" style="width:20%"><?= $model->oktmo_code; ?></div>
            <div class="height-30 pull-left border-right" style="width:5%">
                <?= (!empty($model->payment_details_id)) ? $model->paymentDetails->code : ''; ?>
            </div>
            <div class="height-30 pull-left border-right" style="width:15%"><?= $model->tax_period_code; ?></div>
            <div class="height-30 pull-left border-right"
                 style="width:10.2%"><?= $model->document_number_budget_payment ?></div>
            <div class="height-30 pull-left border-right" style="width:11.9%;">
                <?php if ($model->document_date_budget_payment !== null) {
                    echo RUtils::dt()->ruStrFTime([
                        'format' => \common\components\date\DateHelper::FORMAT_USER_DATE,
                        'date' => $model->document_date_budget_payment,
                    ]);
                } elseif ($model->presence_status_budget_payment) {
                    echo 0;
                } ?>
            </div>
            <div class="height-30 pull-left" style="width:7.9%">
                <?= (!empty($model->payment_type_id)) ? $model->paymentType->code : ''; ?>
            </div>
        </div>
    </div>
    <div style="padding: 0 5px;">
        <?= $model->purpose_of_payment; ?>
    </div>
    <p class="height-30 bold-text">Назначение платежа</p>
</div>