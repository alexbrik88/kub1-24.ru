<?php
use yii\widgets\Pjax;
?>
<div class="modal fade t-p-f" id="accounts-list" tabindex="1" role="modal" aria-hidden="true">
    <div class="modal-dialog" style="width:700px;">
        <div class="modal-content">
            <?php Pjax::begin([
                'id' => 'get-invoices-pjax',
                'linkSelector' => false,
                'formSelector' => '#search-invoice-form',
                'timeout' => 5000,
                'enablePushState' => false,
                'enableReplaceState' => false,
            ]); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="invoice-list"></div>
            <div class="row action-buttons">
                <div class="col-sm-4">
                    <button id="add-invoice-to-payment-order" class="btn btn-add darkblue" data-dismiss="modal">Добавить</button>
                </div>
            </div>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>