<?php

use common\components\date\DateHelper;
use common\components\TaxRobotHelper;
use common\components\widgets\BikTypeahead;
use common\models\cash\CashBankFlows;
use common\models\Company;
use common\models\Contractor;
use common\models\company\CompanyType;
use common\models\company\CheckingAccountant;
use common\models\dictionary\bik\BikDictionary;
use common\models\document\OperationType;
use common\models\document\PaymentDetails;
use common\models\document\PaymentType;
use common\models\document\TaxpayersStatus;
use common\models\employee\EmployeeRole;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $company common\models\Company */
/* @var $form yii\bootstrap\ActiveForm */

$taxRobot = new TaxRobotHelper($company);
$companyIfns = Json::encode($model->company->ifns);

$companyStrictMode = $company->strict_mode == Company::ON_STRICT_MODE;

$rsItems = [];
$rsOptions = [];
$accountsArray = $model->company->getCheckingAccountants()
    ->andWhere(['not', ['type' => CheckingAccountant::TYPE_CLOSED]])
    ->orderBy(['type' => SORT_ASC])
    ->all();

foreach ($accountsArray as $account) {
    $rsItems[$account->rs] = $account->rs;
    $rsOptions[$account->rs] = [
        'data' => [
            'bank' => $account->bank_name,
            'city' => $account->bank_city,
            'bik' => $account->bik,
            'ks' => $account->ks,
        ]
    ];
}

$this->registerJs("
PaymentOrder.ifns = {$companyIfns};
");
?>
<?php $form = ActiveForm::begin([
    'id' => 'asd',
    'fieldConfig' => [
        'template' => "{label}\n{input}",
    ],
]); ?>
<div class="form-body">
    <?= Html::activeHiddenInput($model, 'invoiceIDs'); ?>

    <?= $form->errorSummary($model); ?>

    <div id="connection-to-account" class="no_mrg_bottom">

        <?= $form->field($model, 'relation_with_in_invoice')->label('Связано со счётом', [
            'class' => 'checkbox-inline'
        ])->checkbox([
            'id' => 'relation-with-invoice-checkbox',
            'data-toggle' => 'accounts-list',
            'class' => 'm-l-0',
            'data-contractor' => $model->relation_with_in_invoice && $model->contractor_id ?
                $model->contractor_id : null,
        ]); ?>

        <span class="bold-text" id="relation-invoice-span">входящий счёт № 115 от 28.02.2012</span>
    </div>

    <?= $form->field($model, 'presence_status_budget_payment')->label('Бюджетный платёж', [
        'class' => 'checkbox-inline',
    ])->checkbox([
        'id' => 'budget-payment-checkbox',
        'class' => 'm-l-1'
    ]) ?>

    <div class="portlet customer-info">
        <div class="portlet-title">

            <div class="col-md-10 caption">

                <?= $form->field($model, 'document_number')->label('платежное поручение №', [
                    'class' => 'control-label bold-text',
                    'style' => 'font-size: 21px',
                ])->textInput([
                    'maxlength' => 45,
                    'class' => 'form-control field-width pad-n',
                    'style' => 'width: 130px',
                    'value' => $model->document_number,
                ]) ?>

            </div>
            <div class="actions">
                <div class="height-30 enter-data pull-left">
                    <?= $form->field($model, 'taxpayers_status_id')->dropDownList(ArrayHelper::map(TaxpayersStatus::find()->andWhere(['not', ['id' => TaxpayersStatus::$deprecated]])->all(), 'id', 'name'), [
                        'class' => 'form-control js_select',
                        'prompt' => '',
                        'data-default-value' => $model->company->company_type_id == CompanyType::TYPE_IP ?
                                                TaxpayersStatus::DEFAULT_IP :
                                                TaxpayersStatus::DEFAULT_OOO,
                    ])->label(false); ?>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-offset-7">
                    <div class="data-block pull-left document_date">
                        <?= $form->field($model, 'document_date')->textInput([
                            'class' => 'date-picker',
                            'size' => 16,
                            'data' => [
                                'date-viewmode' => 'years',
                            ],
                            'value' => DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ])->label(false); ?>

                        <div class="data-block__title">
                            дата
                        </div>
                    </div>
                    <div class="data-block pull-left">
                        <div class="data-block__data">

                        </div>
                        <div class="data-block__title">
                            вид платежа
                        </div>
                    </div>
                </div>
            </div>
            <div class="summery clearfix">
                <div class="height-30 summery__sum pull-left">
                    Сумма прописью
                </div>
                <div class="height-30 summery__sum-in-words pull-left"></div>
            </div>
            <div class="summery clearfix">
            </div>
            <div class="clearfix payment-order__table">
                <div class="width-60 left-block pull-left">
                    <div class="width-50 height-30 pull-left border-bootom-right">
                        ИНН <span class="company_inn"><?= $company->inn ?></span>
                        <?php if ($companyStrictMode && !$company->inn) {
                            echo Html::button('Заполнить ваши реквизиты по ИНН', [
                                'class' => 'btn yellow first-invoice-company company_inn_button',
                                'style' => 'padding-top:4px;padding-bottom:4px;margin-top:-3px;']);
                        }
                        ?>
                    </div>
                    <div class="width-50 height-30 pull-left border-bootom">
                        КПП <span class="company_kpp"><?= $company->kpp; ?></span>
                    </div>
                    <div class="width-100 pull-left">
                        <div class="height-60 company_name">
                            <?= $company->getTitle($company->company_type_id == CompanyType::TYPE_IP ? false : true); ?>
                        </div>
                        <div class="height-30 bold-text border-bootom">Плательщик</div>
                    </div>
                    <div class="width-100 pull-left">
                        <div class="height-30">
                            <?= $form->field($model, 'company_bank_name')->textInput([
                                'readonly' => true,
                            ])->label(false); ?>
                            <?= $form->field($model, 'company_bank_city')->hiddenInput()->label(false); ?>
                        </div>
                        <div class="height-30 bold-text border-bootom">Банк плательщика</div>
                    </div>
                    <div class="width-100 pull-left">
                        <div class="height-30">
                            <?= $form->field($model, 'contractor_bank_name')->textInput([
                                'readonly' => true,
                                'class' => 'form-control',
                            ])->label(false); ?>
                            <?= $form->field($model, 'contractor_bank_city')->hiddenInput()->label(false); ?>
                        </div>
                        <div class="height-30 border-bootom">
                            <?= Html::activeLabel($model, 'contractor_bank_name', [
                                'label' => 'Банк получателя',
                                'class' => 'control-label bold-text',
                            ]); ?>
                        </div>
                    </div>
                    <div class="width-50 height-30 pull-left border-bootom-right" style="position:relative">
                        <?= $form->field($model, 'contractor_inn')->label('ИНН', [
                            'class' => 'control-label',
                        ])->textInput([
                            'maxlength' => true,
                            'class' => 'form-control field-width contractor_bank_config',
                            'style' => 'width: 85%; padding-bottom: 2px;',
                        ]) ?>
                        <?php if ($companyStrictMode && !$model->contractor_inn) {
                            echo Html::button('Заполнить реквизиты получателя', [
                                'class' => 'btn yellow payment-order-add-contractor contractor_inn_button',
                                'style' => 'position:absolute;top:0;padding-top:4px;padding-bottom:4px;']);
                        }
                        ?>
                    </div>
                    <div class="width-50 height-30 pull-left border-bootom">
                        <?= $form->field($model, 'contractor_kpp')->label('КПП', [
                            'class' => 'control-label',
                        ])->textInput([
                            'maxlength' => true,
                            'class' => 'form-control field-width contractor_bank_config',
                            'style' => 'width: 85%; padding-bottom: 2px;',
                        ]) ?>
                    </div>
                    <div class="width-100 pull-left">
                        <div class="height-90">
                            <?= $form->field($model, 'contractor_name', [
                                'options' => [
                                    'class' => 'form-group contractor-text-name' .
                                        ($model->relation_with_in_invoice ? ' hidden' : null),
                                ],
                            ])->textInput([
                                'class' => 'form-control',
                            ])->label(false); ?>

                            <?= $form->field($model, 'contractor_id', ['template' => "{input}", 'options' => [
                                'class' => 'contractor-list-name' .
                                    (!$model->relation_with_in_invoice ? ' hidden' : null),
                                'style' => 'width: 98%;',
                            ]])->widget(Select2::classname(), [
                                'data' => Contractor::getALLContractorList(Contractor::TYPE_SELLER, false),
                                'options' => [
                                    'class' => 'form-control contractor-select',
                                    'disabled' => !$model->relation_with_in_invoice,
                                ],
                            ]); ?>
                        </div>
                        <div class="height-30">
                            <?= Html::activeLabel($model, 'contractor_name', [
                                'label' => 'Получатель',
                                'class' => 'control-label bold-text',
                            ]); ?>
                        </div>
                    </div>
                </div>
                <div class="width-40 right-block pull-left border-left">
                    <div class="width-30 height-60 pull-left border-bootom-right">

                        <?= Html::activeLabel($model, 'sum', [
                            'label' => 'Сумма',
                            'class' => 'control-label',
                        ]); ?>

                    </div>
                    <div class="width-70 height-60 pull-left border-bootom">

                        <?= $form->field($model, 'sum')->textInput([
                            'class' => 'summery__sum-in-digits form-control js_input_to_money_format pad-n',
                            'value' => $model->sum / 100,
                            'data-default-value' => $taxRobot->getTaxPayableAmount() / 100,
                        ])->label(false); ?>

                    </div>
                    <div class="width-30 height-60 pull-left border-bootom-right">Сч №</div>
                    <div class="width-70 height-60 pull-left">
                        <?= $form->field($model, 'company_rs', [
                            'template' => "{input}",
                            'options' => [
                                'class' => '',
                            ],
                        ])->widget(Select2::classname(), [
                            'data' => $rsItems,
                            'hideSearch' => true,
                            'pluginOptions' => [
                                'templateResult' => new JsExpression('function (data, container) {
                                    if (data.id && data.element) {
                                        container.innerHTML = $(data.element).data("bank") + "<br>" + data.id;
                                    }
                                    return container;
                                }'),
                            ],
                            'options' => [
                                'class' => 'form-control',
                                'data-update-url' => Url::to(['update-checking-accountant']),
                                'data-create-url' => Url::to(['create-checking-accountant']),
                                'options' => $rsOptions,
                            ]
                        ])->label(false); ?>
                    </div>

                    <div class="width-30 height-30 pull-left border-bootom-right">БИК</div>

                    <div class="width-70 height-30 pull-left">
                        <?php if ($companyStrictMode && !$model->company_bik) {
                            echo Html::button('Заполнить ваш банк по БИК', [
                                'class' => 'btn yellow first-invoice-company company_bank_button',
                                'style' => 'padding-top:4px;padding-bottom:4px;margin-top:-3px;']);
                        } ?>
                        <?= $form->field($model, 'company_bik')->textInput([
                            'readonly' => true,
                            'style' => ($companyStrictMode && !$model->company_bik) ? 'display:none' : ''
                        ])->label(false); ?>
                    </div>
                    <div class="width-30 height-30 pull-left border-bootom-right">Сч №</div>
                    <div class="width-70 height-30 pull-left border-bootom">
                        <?= $form->field($model, 'company_ks')->textInput([
                            'readonly' => true,
                        ])->label(false); ?>
                    </div>

                    <div class="width-30 height-30 pull-left border-bootom-right">
                        <?= Html::activeLabel($model, 'contractor_bik', [
                            'label' => 'БИК',
                            'class' => 'control-label',
                        ]); ?>
                    </div>
                    <div class="width-70 height-30 pull-left border-bootom" style="position:relative">
                        <?= $form->field($model, 'contractor_bik')->label(false)->widget(BikTypeahead::classname(), [
                            'remoteUrl' => Url::to(['/dictionary/bik']),
                            'related' => [
                                '#paymentorder-contractor_bank_name' => 'name',
                                '#paymentorder-contractor_bank_city' => 'city',
                                '#paymentorder-contractor_corresponding_account' => 'ks',
                            ],
                            'options' => [
                                'id' => 'paymentorder-contractor_bik',
                                'class' => 'form-control',
                                'style' => ($companyStrictMode && !$model->contractor_bik) ? 'display:none':''
                            ],
                        ]); ?>
                        <?php if ($companyStrictMode && !$model->contractor_bik) {
                            echo Html::button('Заполнить банк получателя', [
                                'class' => 'btn yellow payment-order-add-contractor contractor_bank_button',
                                'style' => 'position:absolute;top:0;padding-top:4px;padding-bottom:4px;']);
                        }
                        ?>
                    </div>

                    <div class="width-30 height-30 pull-left border-bootom-right">
                        <?= Html::activeLabel($model, 'contractor_corresponding_account', [
                            'label' => 'Сч №',
                            'class' => 'control-label',
                        ]); ?>
                    </div>
                    <div class="width-70 height-30 pull-left border-bootom">
                        <?= $form->field($model, 'contractor_corresponding_account')->textInput([
                            'readonly' => true,
                            'class' => 'form-control',
                        ])->label(false); ?>
                    </div>

                    <div class="width-30 height-60 pull-left border-right">
                        <?= Html::activeLabel($model, 'contractor_current_account', [
                            'label' => 'Сч №',
                            'class' => 'control-label',
                        ]); ?>
                    </div>
                    <div class="width-70 height-60 pull-left">
                        <?= $form->field($model, 'contractor_current_account')->textInput([
                            'maxlength' => true,
                            'class' => 'form-control',
                        ])->label(false); ?>
                    </div>

                    <div class="width-100 pull-left">
                        <div class="width-50 pull-left border-top">
                            <div class="width-60 height-30 pull-left border-bootom-right">
                                <?= Html::activeLabel($model, 'operation_type_id', [
                                    'label' => 'Вид оп.',
                                    'class' => 'control-label',
                                ]); ?>
                            </div>
                            <div class="width-40 height-30 pull-left">
                                <?= $form->field($model, 'operation_type_id')
                                    ->dropDownList(ArrayHelper::map(OperationType::find()->all(), 'id', 'name'), [
                                        'class' => 'form-control',
                                    ])->label(false); ?>
                            </div>
                            <div class="width-60 height-30 pull-left border-bootom-right">
                                <label class="control-label">Наз. пл.</label>
                            </div>
                            <div class="width-40 height-30 pull-left">
                            </div>
                            <div class="width-60 height-30 pull-left border-right">Код</div>
                            <div class="width-40 height-30 pull-left">
                                <?= $form->field($model, 'uin_code')->textInput([
                                    'class' => 'form-control budget-payment pad-n',
                                    'maxlength' => true,
                                    'data-default-value' => '0',
                                ])->label(false); ?>
                            </div>
                        </div>
                        <div class="width-50 pull-left border-left border-top">
                            <div class="width-60 height-30 pull-left border-bootom-right">Срок пл.</div>
                            <div class="width-40 height-30 pull-left">
                                <?= $form->field($model, 'payment_limit_date')->textInput([
                                    'class' => 'date-picker',
                                    'style' => 'line-height: normal;',
                                    'size' => 16,
                                    'data' => [
                                        'date-viewmode' => 'years',
                                    ],
                                    'autocomplete' => 'off',
                                    'value' => DateHelper::format($model->payment_limit_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                                ])->label(false); ?>
                            </div>

                            <div class="width-60 height-30 pull-left border-bootom-right">
                                <?= Html::activeLabel($model, 'ranking_of_payment', [
                                    'label' => 'Очер. пл.',
                                    'class' => 'control-label',
                                ]); ?>
                            </div>
                            <div class="width-40 height-30 pull-left">
                                <?= $form->field($model, 'ranking_of_payment')->textInput([
                                    'class' => 'form-control pad-n',
                                ])->label(false); ?>
                            </div>
                            <div class="width-60 height-30 pull-left border-right">Рез. поле</div>
                            <div class="width-40 height-30 pull-left"></div>
                        </div>
                    </div>
                </div>

                <div class="width-100 pull-left border-top">
                    <div class="height-30 pull-left border-right" style="width:25%">
                        <?= $form->field($model, 'kbk')->label(false)->textInput([
                            'maxlength' => true,
                            'disabled' => (boolean) $model->relation_with_in_invoice,
                            'class' => 'form-control budget-payment',
                            'data-default-value' => '18210501011011000110',
                        ]); ?>
                    </div>
                    <div class="height-30 pull-left border-right" style="width:20%">
                        <?= $form->field($model, 'oktmo_code')->textInput([
                            'maxlength' => true,
                            'disabled' => (boolean) $model->relation_with_in_invoice,
                            'class' => 'form-control budget-payment',
                            'data-default-value' => $model->company->oktmo ?: ($model->company->okato ?: 0),
                        ])->label(false); ?>
                    </div>
                    <div class="height-30 pull-left border-right" style="width:10%">
                        <?= $form->field($model, 'payment_details_id')
                            ->dropDownList(ArrayHelper::map(PaymentDetails::find()->all(), 'id', 'code'), [
                                'prompt' => '',
                                'class' => 'form-control budget-payment',
                                'disabled' => (boolean) $model->relation_with_in_invoice,
                                'data-default-value' => PaymentDetails::DETAILS_1,
                            ])->label(false); ?>
                    </div>
                    <div class="height-30 pull-left border-right" style="width:15%">
                        <?= $form->field($model, 'tax_period_code')->textInput([
                            'class' => 'form-control budget-payment pad-n',
                            'disabled' => (boolean) $model->relation_with_in_invoice,
                            'data-default-value' => $taxRobot->getUsn6PeriodCode(),
                        ])->label(false); ?>
                    </div>
                    <div class="height-30 pull-left border-right" style="width:10.2%">
                        <?= $form->field($model, 'document_number_budget_payment')->textInput([
                            'class' => 'form-control budget-payment pad-n',
                            'disabled' => (boolean) $model->relation_with_in_invoice,
                            'data-default-value' => '0',
                        ])->label(false); ?>
                    </div>
                    <div class="height-30 pull-left border-right" style="width:11.9%;">
                        <?= $form->field($model, 'dateBudgetPayment')->textInput([
                            'class' => 'form-control date-picker budget-payment',
                            'style' => 'line-height: normal;',
                            'size' => 16,
                            'data' => [
                                'date-viewmode' => 'years',
                            ],
                            'disabled' => (boolean) $model->relation_with_in_invoice,
                            'data-default-value' => '0',
                        ])->label(false); ?>
                    </div>
                    <div class="height-30 pull-left" style="width:7.9%">
                        <?= $form->field($model, 'payment_type_id')
                            ->dropDownList(ArrayHelper::map(PaymentType::find()->all(), 'id', 'code'), [
                                'prompt' => '',
                                'class' => 'form-control budget-payment',
                                'disabled' => (boolean) $model->relation_with_in_invoice,
                                'data-default-value' => PaymentType::TYPE_0,
                            ])->label(false); ?>
                    </div>
                </div>
            </div>

            <?= $form->field($model, 'purpose_of_payment')->textarea([
                'class' => 'form-control',
                'data-default-value' => 'Налог, взимаемый с налогоплательщиков, выбравших в качестве объекта налогообложения доходы',
            ])->label(false); ?>

            <p class="height-30 bold-text">
                <?= Html::activeLabel($model, 'purpose_of_payment', [
                    'label' => 'Назначение платежа',
                    'class' => 'control-label bold-text',
                ]); ?>
            </p>
        </div>
    </div>
    <div class="form-actions">
        <div class="row action-buttons">
            <div class="spinner-button col-sm-1 col-xs-1">
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                ]); ?>
                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', ['class' => 'btn darkblue widthe-100 hidden-lg', 'title' => 'Сохранить',]) ?>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="spinner-button col-sm-1 col-xs-1">
                <?= Html::a('Отменить', ['index'], [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                ]); ?>
                <?= Html::a('<i class="fa fa-reply fa-2x"></i>', ['index'], ['class' => 'btn darkblue widthe-100 hidden-lg', 'title' => 'Отменить',]); ?>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
        </div>
    </div>
</div>

<?php if ($companyStrictMode) {
    echo Html::hiddenInput('payment_order_new_contractor_id', 0, ['id' => 'payment_order_new_contractor_id']);
} ?>

<?php ActiveForm::end(); ?>

<?= $this->render('partial/_in_invoice_table', [
    'model' => $model,
    'company' => $company,
]) ?>

<div id="many-charge" class="confirm-modal fade modal" role="dialog"
     tabindex="-1" aria-hidden="true"
     style="display: none; margin-top: -51.5px;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8 text-left many-charge-text" style="font-size: 15px;">
                            <span style="display: block;">Вы уверены, что хотите подготовить платежки в банк:</span>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-6">
                        <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
                            'class' => 'btn darkblue pull-right modal-many-charge ladda-button',
                            'data-url' => Url::to(['/documents/payment-order/many-create',]),
                            'data-style' => 'expand-right',
                        ]); ?>
                    </div>
                    <div class="col-xs-6">
                        <button type="button" class="btn darkblue"
                                data-dismiss="modal">НЕТ
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->registerJs('
    $("#relation-with-invoice-checkbox").click(function () {
        var $contractorNameText = $(".contractor-text-name");
        var $contractorNameList = $(".contractor-list-name");
        if ($(this).is(":checked")) {
            $contractorNameText.addClass("hidden");
            $contractorNameList.removeClass("hidden");
            $contractorNameList.find("select").removeAttr("disabled");
        } else {
            $contractorNameText.removeClass("hidden");
            $contractorNameList.addClass("hidden");
            $contractorNameList.find("select").attr("disabled", true);
        }
    });

    $(".contractor-list-name select").change(function () {
        $.post("get-contractor-data", {
            id: $(this).val()
        }, function ($data) {
            if ($data.result == true) {
                for (var $i in $data.contractor) {
                    if ($i !== "id") {
                        $("#paymentorder-" + $i).val($data.contractor[$i]);
                    }
                }
            }
        });
    });
    $(document).on("change", "#paymentorder-company_rs", function () {
        var form = this.form;
        var rs = $(":selected", this);
        if ($(this).val() && rs) {
            $("#paymentorder-company_bank_name", form).val(rs.data("bank"));
            $("#paymentorder-company_bank_city", form).val(rs.data("city"));
            $("#paymentorder-company_bik", form).val(rs.data("bik"));
            $("#paymentorder-company_ks", form).val(rs.data("ks"));
        }
    });
'); ?>
