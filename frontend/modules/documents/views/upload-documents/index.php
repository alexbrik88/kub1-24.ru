<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 30.05.2017
 * Time: 9:58
 */

use yii\bootstrap\Html;
use common\models\Company;
use common\components\ImageHelper;
use common\models\document\UploadedDocumentOrder;
use yii\helpers\Url;
use common\models\document\UploadedDocuments;

/* @var $this yii\web\View
 * @var $company Company
 * @var $uploadedDocumentOrder UploadedDocumentOrder
 * @var $currentUploadedDocument UploadedDocuments
 * @var $existsContractor boolean
 */

$uploadedDocumentsCount = $company->getUploadedDocuments()->count();
$this->title = 'Загруженные документы ' . $uploadedDocumentsCount;
?>
<div class="row" style="margin-left: 10px;margin-right: 10px;">
    <div id="w10-success-0" class="alert-success alert fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        Распознавание документов, работает в тестовом режиме.<br>
        Если у вас возникли трудности при распознавании, обращайтесь в службу поддержки: <?= Html::a('support@kub-24.ru', 'mailto:support@kub-24.ru'); ?>
    </div>
    <div class="uploaded-documents">
        <div class="portlet box">
            <div class="btn-group pull-right title-buttons">
                <?= Html::a('<i class="fa fa-download"></i> Загрузить', null, [
                    'class' => 'btn yellow',
                    'data-toggle' => 'modal',
                    'data-target' => '#modal-documents-upload',
                ]); ?>
            </div>
            <h3 class="page-title"><?= Html::encode($this->title); ?></h3>
        </div>
        <div
            class="col-md-12 portlet border-darc widget-coming widget-home clear-box ov-hidden"
            style="min-height: 250px;">
            <div class="col-md-1 slide-documents-left">
            </div>
            <div class="documents-slick col-md-12"
                 style="<?= $uploadedDocumentsCount > 7 ? 'width:94%;padding: 30px 0 10px 0;' : 'padding: 10px 35px 10px 35px;'; ?>">
                <?php foreach ($company->uploadedDocuments as $key => $uploadedDocument): ?>
                    <div
                        class="col-md-2 documents-block-img <?= $uploadedDocument->is_complete ? 'green-block' : 'red-block'; ?>">
                        <?= ImageHelper::getThumb($uploadedDocument->getResizeFilePath(), [130, 185], [
                            'class' => 'documents-img complete-document-img',
                        ]); ?>
                        <div class="col-md-12 document-name-block"></div>
                        <div
                            class="col-md-12 document-name"><?= $uploadedDocument->real_file_name ? $uploadedDocument->real_file_name : $uploadedDocument->file_name; ?></div>
                        <div
                            class="col-md-12 check-color <?= $uploadedDocument->id == $currentUploadedDocument->id ? 'checked-document' : ''; ?>"
                            data-url="<?= Url::to(['change-document', 'id' => $uploadedDocument->id]); ?>">
                        </div>
                        <div
                            class="col-md-12 uploaded-documents-line <?= $uploadedDocument->is_complete ? 'green-line' : 'red-line'; ?>">
                            <?= Html::checkbox('UploadedDocuments[' . $uploadedDocument->id . '][checked]', false, [
                                'style' => 'margin-bottom: 75px;',
                                'class' => 'is_complete-document' . ($uploadedDocument->is_complete ? ' complete' : ''),
                            ]); ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="col-md-1 slide-documents-right">
            </div>
        </div>

        <?= $this->render('_partial/_form', [
            'uploadedDocument' => $currentUploadedDocument,
            'company' => $company,
            'uploadedDocumentOrder' => $uploadedDocumentOrder,
            'existsContractor' => $existsContractor,
        ]); ?>
    </div>
</div>
<?= $this->render('_partial/modal_upload_documents', [
    'company' => $company,
]) ?>
