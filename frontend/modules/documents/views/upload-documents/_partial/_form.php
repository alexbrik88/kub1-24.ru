<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 01.06.2017
 * Time: 19:22
 */

use common\models\Company;
use common\models\document\UploadedDocuments;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\components\ImageHelper;
use common\components\date\DateHelper;
use common\models\document\UploadedDocumentOrder;
use frontend\widgets\ConfirmModalWidget;

/* @var $this yii\web\View
 * @var $uploadedDocument UploadedDocuments
 * @var $company Company
 * @var $uploadedDocumentOrder UploadedDocumentOrder
 * @var $existsContractor boolean
 */
?>
<div class="uploaded-documents-data-block">
    <?php if ($uploadedDocument): ?>
            <div class="row action-buttons" style="margin-bottom: 10px;">
                <div class="col-sm-2 col-xs-2 many-save-preview-document" style="display: none;">
                    <?= Html::a('Сохранить', null, [
                        'class' => 'btn darkblue text-white widthe-100 hidden-md hidden-sm hidden-xs',
                        'data-url' => Url::to(['many-save-documents']),
                    ]); ?>
                    <?= Html::a('<span class="glyphicon glyphicon-floppy-disk"></span>', null, [
                        'class' => 'btn darkblue text-white widthe-100 hidden-lg',
                        'data-url' => Url::to(['many-save-documents']),
                    ]); ?>
                </div>
                <div class="col-sm-2 col-xs-2 delete-preview-document" style="display: none;">
                    <?= Html::a('Удалить', null, [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                        'data-toggle' => 'modal',
                        'data-target' => '#many-delete-uploaded-documents-modal',
                    ]); ?>
                    <?= Html::a('<span class="glyphicon glyphicon-floppy-disk"></span>', null, [
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                        'data-toggle' => 'modal',
                        'data-target' => '#many-delete-uploaded-documents-modal',
                    ]); ?>
                </div>
            </div>

        <div class="col-md-12 p-l-0 p-r-0">
            <div class="col-md-8 p-l-0 form-document-block" style="width: 71%">
                <div
                    class="col-md-12 portlet border-darc widget-coming widget-home clear-box ov-hidden"
                    style="margin-bottom: 25px;">
                    <div style="padding: 20px 15px 20px 15px;">
                        <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
                            'action' => 'javascript:;',
                            'id' => 'uploaded-documents-form',
                            'options' => [
                                'enctype' => 'multipart/form-data',
                                'data' => [
                                    'accept-document-url' => Url::to(['accept-document', 'id' => $uploadedDocument->id]),
                                    'save-document-url' => Url::to(['save-document', 'id' => $uploadedDocument->id]),
                                    'save-upload-documents-order-url' => Url::to(['save-uploaded-documents-order', 'id' => $uploadedDocument->id]),
                                    'save-upload-documents-requisites-url' => Url::to(['save-upload-documents-requisites', 'id' => $uploadedDocument->id]),
                                ],
                            ],
                            'enableClientValidation' => false,
                        ])); ?>
                        <?= $this->render('requisites', [
                            'form' => $form,
                            'uploadedDocument' => $uploadedDocument,
                            'existsContractor' => $existsContractor,
                        ]); ?>

                        <?= $this->render('orders', [
                            'form' => $form,
                            'uploadedDocument' => $uploadedDocument,
                            'uploadedDocumentOrder' => $uploadedDocumentOrder,
                        ]); ?>

                        <div class="form-actions main-buttons-block">
                            <div class="row action-buttons">
                                <div class="col-sm-3 col-xs-3">
                                    <?= Html::a('Принять и сохранить', 'javascript:;', [
                                        'class' => 'btn darkblue text-white widthe-100 hidden-md hidden-sm hidden-xs save-document',
                                    ]); ?>
                                    <?= Html::a('<i class="fa fa-floppy-o fa-2x"></i>', 'javascript:;', [
                                        'class' => 'btn darkblue text-white widthe-100 hidden-lg save-document',
                                        'title' => 'Принять и сохранить',
                                    ]); ?>
                                </div>
                                <div class="col-sm-6 col-xs-6"></div>
                                <div class="col-sm-3 col-xs-3">
                                    <?= Html::a('Удалить', 'javascript:;', [
                                        'data-toggle' => 'modal',
                                        'data-target' => '#delete-uploaded-documents-modal',
                                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                                    ]); ?>
                                    <?= Html::a('<i class="fa fa-floppy-o fa-2x"></i>', 'javascript:;', [
                                        'class' => 'btn darkblue widthe-100 hidden-lg',
                                        'data-toggle' => 'modal',
                                        'data-target' => '#delete-uploaded-documents-modal',
                                        'title' => 'Удалить',
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>

            <div class="col-md-3 p-l-0 p-r-0 preview-img-with-loupe" style="width: 29%">
                <div
                    class="col-md-12 portlet border-darc widget-coming widget-home clear-box ov-hidden">
                    <?php if ($uploadedDocument->isPDF()): ?>
                        <?php foreach ($uploadedDocument->getPDFFilesPath() as $filePath): ?>
                            <?= ImageHelper::getThumb($filePath, [10000, 10000], [
                                'class' => 'preview-document-photo',
                                'style' => 'max-width: 100%;max-height: 100%;',
                                'data-large' => ImageHelper::getThumbSrc($filePath, [10000, 10000]),
                            ]); ?>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <?= ImageHelper::getThumb($uploadedDocument->getFilePath(), [10000, 10000], [
                            'class' => 'preview-document-photo',
                            'style' => 'max-width: 100%;max-height: 100%;',
                            'data-large' => ImageHelper::getThumbSrc($uploadedDocument->getFilePath(), [10000, 10000]),
                        ]); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div id="delete-uploaded-documents-modal"
             class="confirm-modal fade modal in"
             role="dialog" tabindex="-1" aria-hidden="false">
            <div class="modal-dialog ">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="form-body">
                            <div class="row">Вы уверены, что хотите удалить
                                выбранные
                                документы?
                            </div>
                        </div>
                        <div class="form-actions row">
                            <div class="col-xs-6">
                                <?= Html::a('ДА', Url::to(['delete', 'id' => $uploadedDocument->id]), [
                                    'class' => 'btn darkblue  pull-right',
                                ]); ?>
                            </div>
                            <div class="col-xs-6">
                                <?= Html::button('НЕТ', [
                                    'class' => 'btn darkblue close-modal-button',
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="many-delete-uploaded-documents-modal"
             class="confirm-modal fade modal in"
             role="dialog" tabindex="-1" aria-hidden="false">
            <div class="modal-dialog ">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="form-body">
                            <div class="row">Вы уверены, что хотите удалить
                                выбранные
                                документы?
                            </div>
                        </div>
                        <div class="form-actions row">
                            <div class="col-xs-6">
                                <?= Html::a('ДА', 'javascript:;', [
                                    'class' => 'btn darkblue many-delete-uploaded-document-modal-button pull-right',
                                    'data-url' => Url::to(['many-delete']),
                                ]); ?>
                            </div>
                            <div class="col-xs-6">
                                <?= Html::button('НЕТ', [
                                    'class' => 'btn darkblue close-modal-button',
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
