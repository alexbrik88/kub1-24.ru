<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 06.06.2017
 * Time: 11:04
 */

use common\components\helpers\Html;
use common\models\document\UploadedDocuments;
use common\models\document\UploadedDocumentOrder;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View
 * @var $uploadedDocument UploadedDocuments
 * @var $uploadedDocumentOrder UploadedDocumentOrder
 * @var $form ActiveForm
 */
?>
<div class="portlet orders-block" id="product-table-invoice">
    <div class="col-md-12" style="padding-right: 1px;margin-bottom: 10px;">
        <?= Html::a('<i class="icon-pencil"></i>', 'javascript:;', [
            'title' => 'Редактировать',
            'class' => 'btn darkblue btn-sm update-uploaded-documents-order',
            'style' => 'float: right;',
        ]); ?>
        <?= Html::a('<i class="ico-Cancel-smart-pls fs" style="font-size:17px;"></i>', 'javascript:;', [
            'title' => 'отменить',
            'class' => 'btn darkblue btn-sm in undo-update-uploaded-documents-order',
            'style' => 'float: right;color:white;display:none;',
        ]); ?>

        <?= Html::a('<i class="fa fa-floppy-o fa-2x" style="font-size:16px!important;"></i>', 'javascript:;', [
            'title' => 'Сохранить',
            'class' => 'btn darkblue btn-sm in save-upload-documents-order',
            'style' => 'float: right;margin-right: 5px;color:white;display:none;',
        ]); ?>
    </div>
    <div class="uploaded-documents-order-out">
        <?= $this->render('orders_out', [
            'uploadedDocument' => $uploadedDocument,
        ]); ?>
    </div>
    <div class="uploaded-documents-order-in">
        <?= $this->render('orders_in', [
            'uploadedDocument' => $uploadedDocument,
            'uploadedDocumentOrder' => $uploadedDocumentOrder,
            'form' => $form,
        ]); ?>
    </div>
</div>