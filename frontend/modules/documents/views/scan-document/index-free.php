<?php

use common\components\grid\GridView;
use common\models\document\ScanDocument;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\file\FileDir;
use frontend\modules\documents\components\UploadManagerHelper;
use common\components\grid\DropDownSearchDataColumn;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\documents\models\ScanDocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$dirs = FileDir::getEmployeeAllowedDirs();

?>
<div class="scan-document-index-free">
    <?php Pjax::begin([
        'id' => 'free-scan-document-pjax',
        'enablePushState' => false,
        'enableReplaceState' => false,
        'timeout' => 10000,
    ]); ?>

        <?= Html::beginForm('', 'GET', ['data' => ['pjax' => '']]); ?>

            <div class="search-form-default">
                <div class="input-group">
                    <div class="input-cont">
                        <?= Html::activeTextInput($searchModel, 'name', [
                            'placeholder' => 'Поиск...',
                            'class' => 'form-control',
                        ]) ?>
                    </div>
                    <span class="input-group-btn">
                        <?= Html::submitButton('Найти', [
                            'class' => 'btn green-haze',
                        ]); ?>
                    </span>
                </div>
            </div>

        <?= Html::endForm(); ?>

        <div class="table-container">
            <table class="table-free-scan-modal" style="table-layout: fixed">
                <tr>
                <td width="150px" class="border-right" rowspan="2">
                    <div id="free-scan-document-dirs">
                        <div class="directory-wrapper">
                            <?= Html::beginTag('a', [
                                'href' => 'javascript:;',
                                'class' => 'scan-directory directory ' . (($fromUploads) ? ' active' : ''),
                                'data-uploads' => 1
                            ]) .
                                Html::tag('i', '', [
                                    'class' => 'glyphicon glyphicon-download-alt',
                                    'width' => 14]) .
                                Html::tag('span', 'Загрузки') .
                            Html::endTag('a') ?>
                        </div>
                        <div class="directory-wrapper">
                            <?= Html::beginTag('a', [
                                'href' => 'javascript:;',
                                'class' => 'scan-directory directory ' . (($fromAllDocs) ? ' active' : ''),
                                'data-all' => 1
                            ]) .
                                Html::img('/img/documents/all-documents-black.png', [
                                    'class' => 'img-doc-folder',
                                    'width' => 14]) .
                                Html::tag('span', 'Все документы') .
                            Html::endTag('a') ?>
                        </div>
                        <?php if ($dirs): ?>
                            <div class="directory-wrapper">
                                <span class="directory">
                                    ПАПКИ
                                </span>
                            </div>
                            <?php foreach($dirs as $dir): ?>
                                <div id="dir-<?= $dir->id ?>" class="directory-wrapper">
                                    <?= Html::beginTag('a', [
                                        'href' => 'javascript:;',
                                        'class' => 'scan-directory directory ' . (($fromDirectory == $dir->id) ? ' active' : ''),
                                        'data-directory' => $dir->id
                                    ]) .
                                        Html::img('/img/documents/folder-black.png', [
                                            'class' => 'img-doc-folder',
                                            'width' => 14]) .
                                        Html::tag('span', $dir->name) .
                                    Html::endTag('a') ?>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </td>
                <td width="510px">
                    <?= GridView::widget([
                        'id' => 'free-scan-document-grid',
                        'dataProvider' => $dataProvider,
                        //'filterModel' => $searchModel,

                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover dataTable customers_table',
                            'style' => 'margin-top:0!important;border-top:none;border-left:none;table-layout: fixed;font-size:12px!important;'
                        ],

                        'headerRowOptions' => [
                            'class' => 'heading',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'pagination pull-right',
                            ],
                        ],
                        'emptyText' => 'Нет свободного скана документа.',
                        'layout' => "{items}\n{pager}",
                        'columns' => [
                            [
                                'class' => 'yii\grid\CheckboxColumn',
                                'header' => '',
                                'name' => 'scan-id',
                                'headerOptions' => [
                                    'width' => '20px;',
                                ],
                                'checkboxOptions' => [
                                    'class' => 'scan-id-checker',
                                ],
                            ],
                            [
                                'attribute' => 'created_at',
                                'headerOptions' => [
                                    'width' => '100px',
                                ],
                                'format' => ['date', 'php:d.m.Y'],
                            ],
                            [
                                'headerOptions' => [
                                    'width' => '30%',
                                ],
                                'contentOptions' => [
                                    'style' => 'overflow: hidden;text-overflow: ellipsis; white-space:nowrap;',
                                ],
                                'attribute' => 'file.filename_full',
                                'label' => 'Название',
                            ],
                            [
                                'label' => 'Скан',
                                'headerOptions' => [
                                    'width' => '30px',
                                    'style' => 'padding-left: 10px!important;'
                                ],
                                'format' => 'raw',
                                'value' => function ($model) {
                                    $content = '';
                                    if ($model->file) {
                                        $url = [
                                            'file',
                                            'id' => $model->id,
                                            'name' => $model->file->filename_full,
                                        ];
                                        $content .= Html::a('<span class="pull-center icon icon-paper-clip"></span>', $url, [
                                            'class' => 'scan_link',
                                            'data' => [
                                                'pjax' => 0,
                                                'tooltip-content' => '#scan-tooltip-' . $model->id,
                                            ]
                                        ]) . "\n";
                                        if ($model->file->ext == 'pdf') {
                                            $img = Html::tag('embed', '', [
                                                'src' => Url::to($url),
                                                'width' => '250px',
                                                'height' => '400px',
                                                'name' => 'plugin',
                                                'type' => 'application/pdf',
                                            ]);
                                        } else {
                                            $img = Html::img($url, [
                                                'style' => 'max-width: 300px; max-height: 300px;',
                                                'alt' => '',
                                            ]);
                                        }
                                        $tooltip = Html::tag('span', $img, ['id' => 'scan-tooltip-' . $model->id]);
                                        $content .= Html::tag('div', $tooltip, ['class' => 'hidden']);
                                    }

                                    return $content;
                                }
                            ],
                            [
                                'label' => 'Размер',
                                'attribute' => 'file.filesize',
                                'headerOptions' => [
                                    'width' => '50px',
                                ],
                                'format' => 'raw',
                                'value' => function($model) {

                                    return UploadManagerHelper::formatSizeUnits($model->file->filesize);
                                }
                            ],
                            [
                                'headerOptions' => [
                                    'width' => '30%',
                                ],
                                'contentOptions' => [
                                    'style' => 'overflow: hidden;text-overflow: ellipsis; white-space:nowrap;',
                                ],
                                'attribute' => 'employee_id',
                                'label' => 'Ответ&shy;ствен&shy;ный',
                                'encodeLabel' => false,
                                'format' => 'raw',
                                'value' => function (ScanDocument $model) {
                                    return $model->employee->getShortFio();
                                }
                            ]
                        ],
                    ]); ?>
                </td>
                </tr>
            <tr>
                <td style="vertical-align:bottom">
                    <?= Html::button('<span class="ladda-label">Прикрепить</span>', [
                        'class' => 'btn darkblue ladda-button text-white free-scan-bind-button',
                        'data-style' => 'expand-right',
                        'disabled' => true,
                        'style' => 'width: 140px; margin-left:15px;'
                    ]) ?>
                    <?= Html::button('Отменить', [
                        'class' => 'btn darkblue pull-right text-white',
                        'style' => 'width: 140px;',
                        'data-dismiss' => 'modal',
                    ]) ?>
                </td>
            </div>
        </div>



    <?php Pjax::end(); ?>
</div>
