<?php
use common\models\product\Product;
use frontend\models\Documents;
use common\models\document\InvoiceFacture;

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);
?>

<div class="font-xs text-r" style="line-height: 1;">
    Приложение №1
    <br>
    к постановлению Правительства Российской Федерации от 26 декабря 2011 г. № 1137
    <br>
    (в ред. Постановления Правительства РФ от 02.04.2021 № 534)
</div>

<div class="font-size-8 padding-min" style="margin: 5px 0;">
    <table class="no-border" style="width: 80%;">
        <tbody>
            <tr>
                <td style="width: 22%">

                </td>
                <td style="width: 78%">
                    <strong>СЧЕТ-ФАКТУРА № <?= $model->fullNumber; ?> от <?= $dateFormatted; ?></strong>
                </td>
                <td>
                    (1)
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <strong>ИСПРАВЛЕНИЕ № &mdash; от &mdash;</strong>
                </td>
                <td>
                    (1а)
                </td>
            </tr>
            <tr>
                <td>
                    Продавец:
                </td>
                <td style="border-bottom: 1px solid #000!important">
                    <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                        <?= $model->invoice->company_name_short; ?>
                    <?php else: ?>
                        <?= $model->invoice->contractor_name_short; ?>
                    <?php endif; ?>
                </td>
                <td>
                    (2)
                </td>
            </tr>
            <tr>
                <td>
                    Адрес:
                </td>
                <td style="border-bottom: 1px solid #000!important">
                    <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                        <?= $model->invoice->company_address_legal_full; ?>
                    <?php else: ?>
                        <?= $model->invoice->contractor_address_legal_full; ?>
                    <?php endif; ?>
                </td>
                <td>
                    (2а)
                </td>
            </tr>
            <tr>
                <td>
                    ИНН/КПП продавца:
                </td>
                <td style="border-bottom: 1px solid #000!important">
                    <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                        <?= $model->invoice->company_inn; ?> / <?= $model->invoice->company_kpp; ?>
                    <?php else: ?>
                        <?= $model->invoice->contractor_inn; ?> / <?= $model->invoice->contractor_kpp; ?>
                    <?php endif; ?>
                </td>
                <td>
                    (2б)
                </td>
            </tr>
            <tr>
                <td>
                    Грузоотправитель и его адрес:
                </td>
                <td style="border-bottom: 1px solid #000!important">
                    <?= $model->invoice->production_type ? (
                    $model->consignor ?
                        $model->consignor->getRequisitesFull(null, false) :
                        'он же'
                    ) : Product::DEFAULT_VALUE; ?>
                </td>
                <td>
                    (3)
                </td>
            </tr>
            <tr>
                <td>
                    Грузополучатель и его адрес:
                </td>
                <td style="border-bottom: 1px solid #000!important">
                    <?php if ($model->contractor_address == InvoiceFacture::CONTRACTOR_ADDRESS_LEGAL) {
                        $address = $model->consignee ? $model->consignee->legal_address : $model->invoice->contractor->legal_address;
                    } else {
                        $address = $model->consignee ? $model->consignee->actual_address : $model->invoice->contractor->actual_address;
                    }
                    echo $model->invoice->production_type ? ($model->consignee ?
                        $model->consignee->getRequisitesFull($address, false) :
                        $model->invoice->contractor->getRequisitesFull($address, false)
                    ) : Product::DEFAULT_VALUE; ?>
                </td>
                <td>
                    (4)
                </td>
            </tr>
            <tr>
                <td>
                    К платежно-расчетному документу:
                </td>
                <td style="border-bottom: 1px solid #000!important">
                    <?= $model->printablePaymentDocuments ?>
                </td>
                <td>
                    (5)
                </td>
            </tr>
            <tr>
                <td>
                    Документ об отгрузке:
                </td>
                <td style="border-bottom: 1px solid #000!important">
                    <?= $model->printableShippingDocuments ?>
                </td>
                <td>
                    (5а)
                </td>
            </tr>
            <tr>
                <td>
                    Покупатель:
                </td>
                <td style="border-bottom: 1px solid #000!important">
                    <?php if ($model->type == Documents::IO_TYPE_IN): ?>
                        <?= $model->invoice->company_name_short; ?>
                    <?php else: ?>
                        <?= $model->invoice->contractor_name_short; ?>
                    <?php endif; ?>
                </td>
                <td>
                    (6)
                </td>
            </tr>
            <tr>
                <td>
                    Адрес:
                </td>
                <td style="border-bottom: 1px solid #000!important">
                    <?php if ($model->type == Documents::IO_TYPE_IN): ?>
                        <?= $model->invoice->company_address_legal_full; ?>
                    <?php else: ?>
                        <?= $model->invoice->contractor_address_legal_full; ?>
                    <?php endif; ?>
                </td>
                <td>
                    (6а)
                </td>
            </tr>
            <tr>
                <td>
                    ИНН/КПП покупателя:
                </td>
                <td style="border-bottom: 1px solid #000!important">
                    <?php if ($model->type == Documents::IO_TYPE_IN): ?>
                        <?= $model->invoice->company_inn; ?> / <?= $model->invoice->company_kpp; ?>
                    <?php else: ?>
                        <?php if($model->invoice->contractor->face_type == \common\models\Contractor::TYPE_LEGAL_PERSON)
                            echo $model->invoice->contractor_inn . ' / ' . $model->invoice->contractor_kpp; ?>
                    <?php endif; ?>
                </td>
                <td>
                    (6б)
                </td>
            </tr>
            <tr>
                <td>
                    Валюта: наименование, код
                </td>
                <td style="border-bottom: 1px solid #000!important">
                    Российский рубль, 643
                </td>
                <td>
                    (7)
                </td>
            </tr>
            <tr>
                <td>
                    Идентификатор государственного контракта, договора (соглашения)(при наличии):
                </td>
                <td style="border-bottom: 1px solid #000!important">
                    <?= $model->state_contract ? '№ ' . $model->state_contract : Product::DEFAULT_VALUE ?>
                </td>
                <td>
                    (8)
                </td>
            </tr>
        </tbody>
    </table>

</div>
