<?php

use common\models\product\Product;
use frontend\models\Documents;
use common\models\document\InvoiceFacture;

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);
?>

<div class="font-size-8 padding-min" style="margin: 5px 0;">
    <table class="no-border" style="width: 100%;">
        <tbody>
            <tr>
                <td width="20%" class="pad-0"></td>
                <td width="12%" class="pad-0"></td>
                <td width="4%" class="pad-0"></td>
                <td width="12%" class="pad-0"></td>
                <td width="3%"  class="pad-0"></td><!---->
                <td width="16%" class="pad-0"></td>
                <td width="12%" class="pad-0"></td>
                <td width="18%" class="pad-0"></td>
                <td width="3%"  class="pad-0"></td>
            </tr>
            <tr>
                <td class="v-al-b font-size-10">Счет-фактура №</td>
                <td class="v-al-b text-c bor-b"><?= $model->fullNumber; ?></td>
                <td class="v-al-b text-c">от</td>
                <td class="v-al-b text-c bor-b"><?= $dateFormatted; ?></td>
                <td class="v-al-b text-c">(1)</td><!---->
                <td colspan="4" rowspan="2" class="font-xs text-r" style="line-height: 1;">
                    Приложение №1<br>к постановлению Правительства Российской Федерации от 26 декабря 2011 г. № 1137<br>(в ред. Постановления Правительства РФ от 02.04.2021 № 534)
                </td>
            </tr>
            <tr>
                <td class="v-al-b font-size-10">Исправление №</td>
                <td class="v-al-b text-c bor-b">&mdash;</td>
                <td class="v-al-b text-c">от</td>
                <td class="v-al-b text-c bor-b">&mdash;</td>
                <td class="v-al-b text-c">(1а)</td><!---->
            </tr>
            <tr>
                <td class="v-al-b font-b">Продавец:</td>
                <td colspan="3" class="v-al-b bor-b">
                    <?= ($model->type == Documents::IO_TYPE_OUT) ? $model->invoice->company_name_short : $model->invoice->contractor_name_short ?>
                </td>
                <td class="v-al-b text-c">(2)</td><!---->
                <td class="v-al-b font-b">Покупатель:</td>
                <td colspan="2" class="v-al-b bor-b">
                    <?= ($model->type == Documents::IO_TYPE_IN) ? $model->invoice->company_name_short : $model->invoice->contractor_name_short ?>
                </td>
                <td class="v-al-b text-c">(6)</td>
            </tr>
            <tr>
                <td class="v-al-b">Адрес:</td>
                <td colspan="3" class="v-al-b bor-b">
                    <?= ($model->type == Documents::IO_TYPE_OUT) ? $model->invoice->company_address_legal_full : $model->invoice->contractor_address_legal_full ?>
                </td>
                <td class="v-al-b text-c">(2а)</td><!---->
                <td class="v-al-b">Адрес:</td>
                <td colspan="2" class="v-al-b bor-b">
                    <?= ($model->type == Documents::IO_TYPE_IN) ? $model->invoice->company_address_legal_full : $model->invoice->contractor_address_legal_full ?>
                </td>
                <td class="v-al-b text-c">(6а)</td>
            </tr>
            <tr>
                <td class="v-al-b">ИНН/КПП продавца:</td>
                <td colspan="3" class="v-al-b bor-b">
                    <?= ($model->type == Documents::IO_TYPE_OUT) ? ($model->invoice->company_inn.' / '.$model->invoice->company_kpp) : ($model->invoice->contractor_inn.' / '.$model->invoice->contractor_kpp) ?>
                </td>
                <td class="v-al-b text-c">(2б)</td><!---->
                <td class="v-al-b">ИНН/КПП покупателя:</td>
                <td colspan="2" class="v-al-b bor-b">
                    <?= ($model->type == Documents::IO_TYPE_IN) ? ($model->invoice->company_inn.' / '.$model->invoice->company_kpp) : ($model->invoice->contractor_inn.' / '.$model->invoice->contractor_kpp) ?>
                </td>
                <td class="v-al-b text-c">(6б)</td>
            </tr>
            <tr>
                <td class="v-al-b">Грузоотправитель и его адрес:</td>
                <td colspan="3" class="v-al-b bor-b">
                    <?= ($model->invoice->production_type) ? ($model->consignor ? $model->consignor->getRequisitesFull(null, false) : 'он же') : Product::DEFAULT_VALUE; ?>
                </td>
                <td class="v-al-b text-c">(3)</td><!---->
                <td class="v-al-b">Валюта: наименование, код</td>
                <td colspan="2" class="v-al-b bor-b">
                    <?= 'Российский рубль, 643' ?>
                </td>
                <td class="v-al-b text-c">(7)</td>
            </tr>
            <tr>
                <td class="v-al-b">Грузополучатель и его адрес:</td>
                <td colspan="3" class="v-al-b bor-b">
                    <?php if ($model->contractor_address == InvoiceFacture::CONTRACTOR_ADDRESS_LEGAL) {
                        $address = $model->consignee ? $model->consignee->legal_address : $model->invoice->contractor->legal_address;
                    } else {
                        $address = $model->consignee ? $model->consignee->actual_address : $model->invoice->contractor->actual_address;
                    }
                    echo $model->invoice->production_type ? ($model->consignee ? $model->consignee->getRequisitesFull($address, false) : $model->invoice->contractor->getRequisitesFull($address, false)) : Product::DEFAULT_VALUE; ?>
                </td>
                <td class="v-al-b text-c">(4)</td><!---->
                <td rowspan="2" colspan="2" class="v-al-b">Идентификатор государственного контракта, договора (соглашения) (при наличии):</td>
                <td rowspan="2" class="v-al-b bor-b">
                    <?= $model->state_contract ? '№ ' . $model->state_contract : Product::DEFAULT_VALUE ?>
                </td>
                <td rowspan="2" class="v-al-b text-c">(8)</td>
            </tr>
            <tr>
                <td class="v-al-b">К платежно-расчетному документу №:</td>
                <td colspan="3" class="v-al-b bor-b">
                    <?= $model->printablePaymentDocuments ?>
                </td>
                <td class="v-al-b text-c">(5)</td><!---->
            </tr>
            <tr>
                <td class="v-al-b">Документ об отгрузке:</td>
                <td colspan="3" class="v-al-b bor-b">
                    <?= $model->printableShippingDocuments ?>
                </td>
                <td class="v-al-b text-c">(5а)</td><!---->
                <td colspan="4"></td>
            </tr>
        </tbody>
    </table>
</div>
