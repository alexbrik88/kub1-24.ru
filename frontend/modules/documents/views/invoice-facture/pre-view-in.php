<?php
use \frontend\modules\documents\widgets\CreatedByWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use \common\components\date\DateHelper;

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);
$precision = $model->invoice->price_precision;
?>


<div class="over-hidden m-size-div container-first-account-table no_min_h pad0" style="min-width: 520px; max-width:735px; border-top:1px solid #4276a4; border-left:1px solid #4276a4; margin-top:3px;">
    <div class="col-xs-12 pad5 pre-view-table">
        <div class="col-xs-12 pad3" style="height: 80px;">
            <div class="col-xs-12 pad0 font-bold" style="height: inherit">
                <div class="actions" style="margin-top: -8px;">
                    <?= \frontend\modules\documents\widgets\DocumentLogWidget::widget([
                        'model'=>$model,
                    ]); ?>
                    <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE, [
                        'model' => $model,
                    ])
                    ): ?>
                        <?= Html::a('<i class="icon-pencil"></i>', [
                            'update',
                            'type' => $ioType,
                            'id' => $model->id,
                            'contractorId' => ($useContractor ? $model->invoice->contractor_id : null),
                        ], [
                            'class' => 'btn darkblue btn-sm',
                            'style' => 'padding-bottom: 4px !important;',
                            'title' => 'Редактировать',
                        ]) ?>
                    <?php endif; ?>
                </div>
                <div class="portlet customer-info no-border">
                    <div class="portlet-title pad0" style="position: relative;">
                        <div class="caption">
                            <div style="padding-right: 60px;">
                                <?= $message->get(\frontend\modules\documents\components\Message::TITLE_SHORT_SINGLE); ?>
                                № <span class="editable-field"><?= $model->fullNumber; ?></span>
                                от
                                <span id="document_date_item" class="editable-field"  data-tooltip-content="#tooltip_document_date"><?= $dateFormatted; ?></span>
                            </div>
                            Исправление № &mdash; от &mdash;
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 pad3 m-t">
            <div style="margin-bottom: 3px">
                Продавец:
                <?php echo $model->invoice->contractor_name_short; ?>
            </div>
            <div style="margin-bottom: 3px">
                Адрес: <?php echo $model->invoice->contractor_address_legal_full; ?>
            </div>
            <div style="margin-bottom: 3px">
                ИНН/КПП продавца:
                <?php if($model->invoice->contractor->face_type == \common\models\Contractor::TYPE_LEGAL_PERSON)
                    echo $model->invoice->contractor_inn . '/' . $model->invoice->contractor_kpp; ?>
            </div>
            <div style="margin-bottom: 3px">
                Грузоотправитель и его адрес:
                <?= $model->invoice->production_type ? ($model->consignee ?
                    $model->consignee->getRequisitesFull(null, false) :
                    $model->invoice->contractor->getRequisitesFull(null, false)
                ) : \common\models\product\Product::DEFAULT_VALUE; ?>
            </div>
            <div style="margin-bottom: 3px">
                Грузополучатель и его адрес:
                <?= $model->invoice->production_type ? ($model->consignor ?
                    $model->consignor->getRequisitesFull(null, false) :
                    'он же'
                ) : \common\models\product\Product::DEFAULT_VALUE; ?>
            </div>
            <div style="margin-bottom: 3px">
                К платежно-расчетному документу
                <?= $paymentDocuments ? join(', ', $paymentDocuments) : '№ --- от ---'; ?>
            </div>
            <div style="margin-bottom: 3px">
                Покупатель:
                <?php echo $model->invoice->company_name_short; ?>
            </div>
            <div style="margin-bottom: 3px">
                Адрес:
                <?php echo $model->invoice->company_address_legal_full; ?>
            </div>
            <div style="margin-bottom: 3px">
                ИНН/КПП покупателя:
                <?php echo $model->invoice->company_inn; ?>
                /<?php echo $model->invoice->company_kpp; ?>
            </div>
            <div style="margin-bottom: 3px">
                Валюта: наименование, код Российский рубль, 643
            </div>
            <div style="margin-bottom: 3px">
                Идентификатор государственного контракта, договора (соглашения)(при наличии):
                <?= $model->state_contract ? '№ ' . $model->state_contract : \common\models\product\Product::DEFAULT_VALUE ?>
            </div>
        </div>
    </div>
</div>
