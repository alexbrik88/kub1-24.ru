<?php

use common\models\document\Autoinvoicefacture;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\document\Autoinvoicefacture */

if (empty($model)) {
    $company = Yii::$app->user->identity->company;
    $model = $company->autoinvoicefacture ? : new Autoinvoicefacture([
        'company_id' => $company->id,
        'rule' => Autoinvoicefacture::MANUAL,
        'send_auto' => false,
    ]);
}
?>

<?php Pjax::begin([
    'id' => 'autoinvoicefacture-form-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]); ?>

<?php $form = ActiveForm::begin([
    'id' => 'autoinvoicefacture-form',
    'action' => ['autoinvoicefacture'],
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'options' => [
        'class' => 'autoinvoicefacture-form',
        'data-pjax' => '',
    ],
]); ?>

    <?= $form->field($model, 'rule', ['options' => ['class' => '']])->label(false)->radioList(Autoinvoicefacture::$ruleLabels, [
        'encode' => false,
        'item' => function ($index, $label, $name, $checked, $value) use ($form, $model) {
            $ruleAdditional = 'additional-rule-' . $value;
            $comment = isset(Autoinvoicefacture::$ruleComments[$value]) ? Autoinvoicefacture::$ruleComments[$value] : '';
            $comment = Html::tag('div', $comment, [
                'style' => 'padding-left: 25px;',
            ]);
            $input = Html::radio($name, $checked, [
                'value' => $value,
                'class' => 'autoinvoicefacture-rule-radio',
                'data-additional' => '.' . $ruleAdditional,
                'label' => Html::tag('span', $label, [
                    'class' => 'text-bold',
                ]) . $comment,
            ]);
            if ($value == Autoinvoicefacture::BY_AUTO_DOC) {
                $input .= Html::tag('div', Html::activeCheckbox($model, 'send_auto'), [
                    'class' => 'additional-rule collapse ' . $ruleAdditional . ($checked ? ' in' : ''),
                    'style' => 'padding-left: 25px;',
                ]);
            }

            return Html::tag('div', $input, [
                'class' => 'form-group',
            ]);
        }
    ]); ?>

    <div class="form-actions">
        <div class="row action-buttons">
            <div class="col-xs-6">
                <button type="submit" class="btn darkblue hidden-md hidden-sm hidden-xs ladda-button"
                        data-style="expand-right" style="width: 130px!important;">
                    <span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>
                </button>
                <button type="submit" class="btn darkblue widthe-100 hidden-lg">
                    <i class="fa fa-floppy-o fa-2x"></i>
                </button>
            </div>
            <div class="col-xs-6 text-right">
                <span class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs" data-dismiss="modal" style="width: 130px!important;">
                    Отменить
                </span>
                <span class="btn darkblue widthe-100 hidden-lg" title="Отменить" data-dismiss="modal">
                    <i class="fa fa-reply fa-2x"></i>
                </span>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
        </div>
    </div>

<?php ActiveForm::end(); ?>

<?php Pjax::end(); ?>