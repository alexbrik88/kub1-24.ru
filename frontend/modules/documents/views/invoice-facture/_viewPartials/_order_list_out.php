<?php

use common\models\address\Country;
use common\components\TextHelper;

/* @var $model \common\models\document\InvoiceFacture */

$countryList = Country::find()->select('name_short')->indexBy('id')->column();
?>

<table class="table table-striped table-bordered table-hover customers_table smallWidthTd min-w1200">
    <thead>
    <tr class="heading" role="row">
        <th width="18%" style="min-width: 60px;">Наи&shy;ме&shy;но&shy;ва&shy;ние</th>
        <th width="5%" style="min-width: 60px;">Код<br>ви&shy;да<br>то&shy;ва&shy;ра</th>
        <th width="5%" style="min-width: 60px;">Ед. изм.<br>код</th>
        <th width="5%" style="min-width: 60px;">Ед. изм.<br>усл. обозн.</th>
        <th width="5%" style="min-width: 60px;">Кол-во</th>
        <th width="8%" style="min-width: 60px;">Цена за ед. из&shy;ме&shy;ре&shy;ния</th>
        <th width="8%" style="min-width: 60px;">Стои&shy;мость без на&shy;ло&shy;га</th>
        <th width="8%" style="min-width: 60px;">В т.ч. сум&shy;ма ак&shy;ци&shy;за</th>
        <th width="5%" style="min-width: 60px;">Нало&shy;го&shy;ва&shy;я став&shy;ка</th>
        <th width="8%" style="min-width: 60px;">Сумма на&shy;ло&shy;га</th>
        <th width="8%" style="min-width: 60px;">Стои&shy;мость с на&shy;ло&shy;гом</th>
        <th width="5%">Страна происх. то&shy;ва&shy;ра</th>
        <th width="5%">
            Регистрацион&shy;ный но&shy;мер та&shy;мо&shy;жен&shy;ной де&shy;кла&shy;ра&shy;ции
        </th>
        <th width="2%"></th>
    </tr>
    </thead>
    <tbody class="document-orders-rows">
    <?php foreach ($model->ownOrders as $key => $order): ?>
        <?= $this->render('_order_list_out_row', [
            'model' => $model,
            'order' => $order,
            'key' => $key,
            'precision' => $precision,
            'countryList' => $countryList,
        ]) ?>
    <?php endforeach; ?>
    </tbody>
</table>
