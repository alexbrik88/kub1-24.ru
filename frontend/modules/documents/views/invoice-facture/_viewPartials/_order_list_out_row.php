<?php
/** @var \common\models\document\InvoiceFacture $model */
use common\components\TextHelper;
use common\models\document\OrderInvoiceFacture;
use common\models\product\Product;
use yii\helpers\Html;

$invoiceOrder = $order->order;
$product = $invoiceOrder->product;
$isService = $product->production_type == Product::PRODUCTION_TYPE_SERVICE;
$hideUnits = ($isService && !$model->show_service_units);
$maxQuantity = OrderInvoiceFacture::availableQuantity($invoiceOrder, $model->id);
$unitName = $invoiceOrder->unit ? $invoiceOrder->unit->name : Product::DEFAULT_VALUE;
if ($order->quantity != intval($order->quantity)) {
    $order->quantity = rtrim(number_format($order->quantity, 10, '.', ''), 0);
}
$amountNoNds = $model->getPrintOrderAmount($order->order_id, true);
$amountWithNds = $model->getPrintOrderAmount($order->order_id);
?>
<tr role="row" class="odd">
    <td><?= $invoiceOrder->product_title; ?></td>
    <td><?= $product->item_type_code ? : Product::DEFAULT_VALUE; ?></td>
    <td>
        <?php $value = $product->productUnit ? $product->productUnit->code_okei : Product::DEFAULT_VALUE; ?>
        <?= Html::tag('span', $value, [
            'class' => $isService ? 'service_units_visible' . ($hideUnits ? ' hide' : '') : null,
        ]); ?>
        <?php if ($isService) {
            echo Html::tag('span', Product::DEFAULT_VALUE, [
                'class' => 'service_units_hidden' . ($hideUnits ? '' : ' hide'),
            ]);
        } ?>
    </td>
    <td>
        <?= Html::tag('span', $unitName, [
            'class' => $isService ? 'service_units_visible' . ($hideUnits ? ' hide' : '') : null,
        ]); ?>
        <?php if ($isService) {
            echo Html::tag('span', Product::DEFAULT_VALUE, [
                'class' => 'service_units_hidden' . ($hideUnits ? '' : ' hide'),
            ]);
        } ?>
    </td>
    <td>
        <?php
        $value = $order->quantity ? strtr($order->quantity, ['.' => ',']) : Product::DEFAULT_VALUE;
        if ($unitName == Product::DEFAULT_VALUE) {
            $value = $unitName;
        }
        ?>
        <span class="input-editable-field">
            <?php if ($order->quantity == 1 && $isService && $maxQuantity == 1) : ?>
                <?= Html::tag('span', $value, [
                    'class' => $isService ? 'service_units_visible' . ($hideUnits ? ' hide' : '') : null,
                ]); ?>
                <?php if ($isService) {
                    echo Html::tag('span', Product::DEFAULT_VALUE, [
                        'class' => 'service_units_hidden' . ($hideUnits ? '' : ' hide'),
                    ]);
                } ?>
                <?= Html::input('hidden', "orderParams[{$invoiceOrder->id}][quantity]", $order->quantity); ?>
            <?php else : ?>
                <?= Html::input('number', "orderParams[{$invoiceOrder->id}][quantity]", $order->quantity, [
                    'style' => 'width: 75px;',
                    'class' => 'form-control text-right',
                    'max' => $maxQuantity,
                    'min' => 0,
                ]); ?>
            <?php endif ?>
        </span>
    </td>
    <td class="text-right">
        <?php $value = $invoiceOrder->selling_price_no_vat ?
            TextHelper::invoiceMoneyFormat($invoiceOrder->selling_price_no_vat, $precision) :
            Product::DEFAULT_VALUE; ?>
        <?= Html::tag('span', $value, [
            'class' => $isService ? 'service_units_visible' . ($hideUnits ? ' hide' : '') : null,
        ]); ?>
        <?php if ($isService) {
            echo Html::tag('span', Product::DEFAULT_VALUE, [
                'class' => 'service_units_hidden' . ($hideUnits ? '' : ' hide'),
            ]);
        } ?>
    </td>
    <td><?= TextHelper::invoiceMoneyFormat($amountNoNds, $precision); ?></td>
    <td><?= $invoiceOrder->excise ? TextHelper::invoiceMoneyFormat($invoiceOrder->excise_price, 2) : 'без акциза'; ?></td>
    <td><?= $invoiceOrder->saleTaxRate->name; ?></td>
    <td><?= TextHelper::invoiceMoneyFormat($order->amountNds, $precision); ?></td>
    <td><?= TextHelper::invoiceMoneyFormat($amountWithNds, $precision); ?></td>
    <td>
        <?= Html::dropDownList(
            "orderParams[{$invoiceOrder->id}][country_id]",
            $order->country_id,
            $countryList,
            [
                'class' => 'form-control',
                'style' => 'width: 150px;',
            ]
        ); ?>
    </td>
    <td>
        <?= Html::textInput(
            "orderParams[{$invoiceOrder->id}][custom_declaration_number]",
            $order->custom_declaration_number,
            [
                'class' => 'form-control',
            ]
        ); ?>
    </td>
    <td>
        <span class="icon-close input-editable-field delete-row"></span>
    </td>
</tr>