<?php
use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;

/* @var InvoiceFacture $model */

if ($model->invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT) {
    $sum = $model->getPrintAmountNoNds();
} else {
    $sum = $model->getPrintAmountWithNds();
}
?>

<table class="table table-resume">
    <tbody>
    <tr role="row" class="even">
        <td class="bold-text">Итого:</td>
        <td><?= TextHelper::invoiceMoneyFormat($sum, 2); ?></td>
    </tr>
    <tr role="row" class="odd">
        <td class="bold-text"><?= $model->invoice->ndsViewValue ?>:</td>
        <td><?= TextHelper::invoiceMoneyFormat($model->totalNds, 2); ?></td>
    </tr>
    <tr role="row" class="even">
        <td class="bold-text">Всего к оплате:</td>
        <td><?= TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2); ?></td>
    </tr>
    </tbody>
</table>