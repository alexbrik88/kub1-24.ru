<?php
use common\components\date\DateHelper;
use frontend\modules\documents\components\Message;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \common\models\document\InvoiceFacture $model */
/* @var $message Message */
/** @var string $dateFormatted */
?>

<div class="portlet customer-info">
    <div class="portlet-title">
        <div class="col-md-9 caption">
            <?= $message->get(Message::TITLE_SHORT_SINGLE); ?>
            №
            <?= Html::activeTextInput($model, 'document_number', [
                'maxlength' => true,
                'class' => 'form-control  input-editable-field ',
                'placeholder' => 'номер',
                'style' => 'max-width: 110px; display:inline-block;',
            ]); ?>

            от
            <div class="input-icon input-calendar input-editable-field">
                <i class="fa fa-calendar"></i>
                <?= Html::activeTextInput($model, 'document_date', [
                    'id' => 'under-date',
                    'class' => 'form-control date-picker',
                    'size' => 16,
                    'data-date-viewmode' => 'years',
                    'value' => DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                ]); ?>
            </div>
        </div>

        <div class="actions">

            <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE, ['model' => $model,])) : ?>
                <button type="submit" class="btn-save btn darkblue btn-sm" style="color: #FFF;" title="Сохранить">
                    <span class="ico-Save-smart-pls"></span>
                </button>
                <?= Html::a('<span class="ico-Cancel-smart-pls"></span>', ['view', 'type' => $model->type, 'id' => $model->id], [
                    'class' => 'btn darkblue btn-sm invoice-facture-reload input-editable-field',
                    'style' => "color: #FFF;",
                    'title' => "Отменить"
                ]) ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="portlet-body no_mrg_bottom">
        <table class="table no_mrg_bottom">
            <tr>
                <td>
                    <span class="customer-characteristic"><?= $message->get(Message::CONTRACTOR); ?>:</span>
                    <span><?= Html::a($model->invoice->contractor_name_short, [
                        '/contractor/view',
                        'type' => $model->invoice->contractor->type,
                        'id' => $model->invoice->contractor->id,
                    ]) ?></span>
                </td>
            </tr>

            <tr>
                <td>
                    <div style="margin-bottom: 5px;">
                        <?= \frontend\modules\documents\widgets\DocumentFileScanWidget::widget([
                            'model' => $model,
                            'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
                            'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
                            'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
                            'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
                            'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
                            'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
                            'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
                        ]); ?>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>