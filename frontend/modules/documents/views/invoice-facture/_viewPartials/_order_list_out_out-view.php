<?php
/** @var \common\models\document\InvoiceFacture $model */
use common\components\TextHelper;
use common\models\product\Product;

?>

<table class="table table-bordered customers_table smallWidthTd th-bg-none width-1460 th-font-w-norm table-b">
    <thead>
    <tr class="heading" role="row">
        <th rowspan="2" class="text-center" width="20%">Наименование товара (описание выполненных работ, оказанных
            услуг), имущественного права
        </th>
        <th colspan="2" class="text-center b-b-ddd" width="5%">Единица измерения</th>
        <th rowspan="2" class="text-center" width="5%">Количество(объем)</th>
        <th rowspan="2" class="text-center" width="5%">Цена (тариф) за единицу измерения</th>
        <th rowspan="2" class="text-center" width="3%">Стоимость товаров (работ, услуг), имущественных прав без налога -
            всего
        </th>
        <th rowspan="2" class="text-center" width="8%">В том числе сумма акциза</th>
        <th rowspan="2" class="text-center" width="3%">Налоговая ставка</th>
        <th rowspan="2" class="text-center" width="3%">Сумма налога, предъявляемая покупателю</th>
        <th rowspan="2" class="text-center" width="3%">Стоимость товаров (работ, услуг), имущественных прав с налогом -
            всего
        </th>
        <th colspan="2" class="text-center b-b-ddd">Страна происхождения товара</th>
        <th rowspan="2" class="text-center b-l-ddd" width="5%">Номер таможенной декларации</th>
    <tr class="heading" role="row">
        <th class="text-center" width="5%">код</th>
        <th class="text-center" width="5%">условное обозначение</th>
        <th class="text-center" width="5%">цифровой код</th>
        <th class="text-center" width="5%">краткое наименование</th>
    </tr>
    </thead>
    <tbody>
    <tr class="td-p-0">
        <td class="text-center">1</td>
        <td class="text-center">2</td>
        <td class="text-center">2a</td>
        <td class="text-center">3</td>
        <td class="text-center">4</td>
        <td class="text-center">5</td>
        <td class="text-center">6</td>
        <td class="text-center">7</td>
        <td class="text-center">8</td>
        <td class="text-center">9</td>
        <td class="text-center">10</td>
        <td class="text-center">10a</td>
        <td class="text-center">11</td>
    </tr>

    <?php foreach ($model->invoice->orders as $order): ?>
        <tr role="row" class="odd td-p-t-b-0">
            <?php $unitName = $order->unit ? $order->unit->name : Product::DEFAULT_VALUE; ?>
            <td><?= $order->product_title; ?></td>
            <td><?= $order->product->productUnit ? $order->product->productUnit->code_okei : \common\models\product\Product::DEFAULT_VALUE; ?></td>
            <td><?= $unitName ?></td>
            <td class="text-right"><?= $unitName == Product::DEFAULT_VALUE & $unitName : TextHelper::moneyFormat($order->quantity); ?></td>
            <td class="text-right"><?= TextHelper::invoiceMoneyFormat($order->selling_price_no_vat, 2); ?></td>
            <td class="text-right"><?= TextHelper::invoiceMoneyFormat($order->amount_sales_no_vat, 2); ?></td>
            <td><?= $order->excise ? TextHelper::invoiceMoneyFormat($order->excise_price, 2) : 'без акциза'; ?></td>
            <td><?= $order->saleTaxRate->name; ?></td>
            <td class="text-right"><?= TextHelper::invoiceMoneyFormat($order->sale_tax, 2); ?></td>
            <td class="text-right"><?= TextHelper::invoiceMoneyFormat($order->amount_sales_with_vat, 2); ?></td>
            <td><?= $order->country->code; ?></td>
            <td><?= $order->country->name_short; ?></td>
            <td><?= $order->custom_declaration_number; ?></td>
        </tr>
    <?php endforeach; ?>

    <tr role="row" class="odd td-p-t-b-0">
        <td colspan="5" class="font-bold">Всего к оплате</td>
        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($model->invoice->total_amount_no_nds, 2); ?></td>
        <td colspan="2" class="text-center">X</td>
        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($model->invoice->total_amount_nds, 2); ?></td>
        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($model->invoice->total_amount_with_nds, 2); ?></td>
        <td colspan="3" class="b-r-b-0"></td>
    </tr>
    </tbody>
</table>