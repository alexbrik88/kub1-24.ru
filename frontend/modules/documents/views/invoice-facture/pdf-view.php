<?php

use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\document\InvoiceFacture */

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'out-sf out-document out-act';
?>

<style>

    div.document-template,
    div.document-template div,
    div.document-template span,
    div.document-template table,
    div.document-template tbody,
    div.document-template tfoot,
    div.document-template thead,
    div.document-template tr,
    div.document-template th,
    div.document-template td {
        font-size: 8pt;
    }

    div.document-template thead,
    div.document-template th {
        font-size: 6pt;
        font-family: Arial, sans-serif;
    }

    .font-size-6,
    .font-xs {
        font-size: 6pt;
        font-family: Arial, sans-serif!important;
    }

</style>

<div class="page-content-in p-center-album pad-pdf-p-album">
    <?= $this->render('template', [
        'model' => $model,
    ]); ?>
</div>

<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id) : ?>
    <pagebreak />
<?php endif; ?>
