<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\product\Product;
use frontend\modules\documents\components\Message;
use frontend\models\Documents;
use common\models\document\InvoiceFacture;
use common\models\company\CompanyType;
use common\components\image\EasyThumbnailImage;
use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\document\InvoiceFacture */

common\assets\DocumentTemplateAsset::register($this);

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$paymentDocuments = [];
foreach ($model->paymentDocuments as $doc) {
    $date = date('d.m.Y', strtotime($doc->payment_document_date));
    $paymentDocuments[] = "№ {$doc->payment_document_number} от {$date}";
}
$precision = $model->invoice->price_precision;

$company = $model->company;

$addStamp = (boolean) $model->add_stamp;

if ($model->signed_by_name) {
    $accountantSignatureLink = $signatureLink = !$model->employeeSignature
        ? null
        : EasyThumbnailImage::thumbnailSrc($model->employeeSignature->file, 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);
} else {
    $signatureLink = EasyThumbnailImage::thumbnailSrc($model->getImage('chiefSignatureImage'), 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);
}

$printLink = EasyThumbnailImage::thumbnailSrc($model->getImage('printImage'), 200, 200, EasyThumbnailImage::THUMBNAIL_INSET);
?>

<?php
// замена тройного тире на одно длинное start
ob_start();
?>

<div class="document-template invoice-facture-template">
    <div class="font-xs text-r" style="line-height: 1;">
        Приложение №1
        <br>
        к постановлению Правительства Российской Федерации от 26 декабря 2011 г. № 1137
        <br>
        (в редакции постановления Правительства Российской Федерации от 19 августа 2017 г. № 981)
    </div>

    <div class="doc-title" style="margin: 10px auto;">
        Счет-фактура № <?= $model->fullNumber; ?> от <?= $dateFormatted; ?>
        <br>
        Исправление № &mdash; от &mdash;
    </div>

    <div style="margin: 10px auto 5px;">
        <div style="margin-bottom: 3px">
            Продавец:
            <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                <?= $model->invoice->company_name_short; ?>
            <?php else : ?>
                <?= $model->invoice->contractor_name_short; ?>
            <?php endif; ?>
        </div>

        <div style="margin-bottom: 3px">
            Адрес:
            <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                <?= $model->invoice->company_address_legal_full; ?>
            <?php else : ?>
                <?= $model->invoice->contractor_address_legal_full; ?>
            <?php endif; ?>
        </div>

        <div style="margin-bottom: 3px">
            ИНН/КПП продавца:
            <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                <?= $model->invoice->company_inn; ?> / <?= $model->invoice->company_kpp; ?>
            <?php else : ?>
                <?= $model->invoice->contractor_inn; ?> / <?= $model->invoice->contractor_kpp; ?>
            <?php endif; ?>
        </div>

        <div style="margin-bottom: 3px">
            Грузоотправитель и его адрес:
            <?= $model->invoice->production_type ? (
                    $model->consignor ?
                    $model->consignor->getRequisitesFull(null, false) :
                    'он же'
                ) : Product::DEFAULT_VALUE; ?>
        </div>

        <div style="margin-bottom: 3px">
            Грузополучатель и его адрес:
            <?php
            if ($model->contractor_address == InvoiceFacture::CONTRACTOR_ADDRESS_LEGAL) {
                $address = $model->consignee ? $model->consignee->legal_address : $model->invoice->contractor->legal_address;
            } else {
                $address = $model->consignee ? $model->consignee->actual_address : $model->invoice->contractor->actual_address;
            }
            echo $model->invoice->production_type ? ($model->consignee ?
                $model->consignee->getRequisitesFull($address, false) :
                $model->invoice->contractor->getRequisitesFull($address, false)
            ) : \common\models\product\Product::DEFAULT_VALUE;
            ?>
        </div>

        <div style="margin-bottom: 3px">
            К платежно-расчетному документу
            <?= $paymentDocuments ? join(', ', $paymentDocuments) : '№ --- от ---'; ?>
        </div>

        <div style="margin-bottom: 3px">
            Покупатель:
            <?php if ($model->type == Documents::IO_TYPE_IN) : ?>
                <?= $model->invoice->company_name_short; ?>
            <?php else : ?>
                <?= $model->invoice->contractor_name_short; ?>
            <?php endif; ?>
        </div>

        <div style="margin-bottom: 3px">
            Адрес:
            <?php if ($model->type == Documents::IO_TYPE_IN) : ?>
                <?= $model->invoice->company_address_legal_full; ?>
            <?php else : ?>
                <?= $model->invoice->contractor_address_legal_full; ?>
            <?php endif; ?>
        </div>

        <div style="margin-bottom: 3px">
            ИНН/КПП покупателя:
            <?php if ($model->type == Documents::IO_TYPE_IN) : ?>
                <?= $model->invoice->company_inn; ?> / <?= $model->invoice->company_kpp; ?>
            <?php else : ?>
                <?php if ($model->invoice->contractor->face_type == \common\models\Contractor::TYPE_LEGAL_PERSON) : ?>
                    <?= $model->invoice->contractor_inn . ' / ' . $model->invoice->contractor_kpp; ?>
                <?php endif; ?>
            <?php endif; ?>
        </div>

        <div style="margin-bottom: 3px">
            Валюта: наименование, код Российский рубль, 643
        </div>

        <div style="margin-bottom: 3px">
            Идентификатор государственного контракта, договора (соглашения)(при наличии):
            <?= $model->state_contract ? '№ ' . $model->state_contract : Product::DEFAULT_VALUE ?>
        </div>
    </div>
    <table class="mar-b-20">
        <thead class="no-bold">
            <tr>
                <th rowspan="2" class="bor" width="13%">
                    Наименование товара (описание выполненных работ, оказанных
                    услуг), имущественного права
                </th>
                <th rowspan="2" class="bor" width="">
                    Код вида товара
                </th>
                <th colspan="2" class="bor" width="13%">
                    Единица измерения
                </th>
                <th rowspan="2" class="bor" width="5%">
                    Коли-чество (объем)
                </th>
                <th rowspan="2" class="bor" width="7%">Цена
                    (тариф) за единицу измерения
                </th>
                <th rowspan="2" class="bor" width="10%">
                    Стоимость товаров (работ, услуг), имущественных прав без
                    налога -
                    всего
                </th>
                <th rowspan="2" class="bor" width="7%">В том
                    числе сумма акциза
                </th>
                <th rowspan="2" class="bor" width="6%">
                    Налоговая ставка
                </th>
                <th rowspan="2" class="bor" width="9%">Сумма
                    налога, предъявляемая покупателю
                </th>
                <th rowspan="2" class="bor" width="9%">
                    Стоимость товаров (работ, услуг), имущественных прав с
                    налогом -
                    всего
                </th>
                <th colspan="2" class="bor" width="13%">
                    Страна происхождения товара
                </th>
                <th rowspan="2" class="bor" width="">
                    Регистрационный номер таможенной декларации
                </th>
            </tr>
            <tr>
                <th class="bor" width="3%">код</th>
                <th class="bor" width="">условное
                    обозначение (национальной)
                </th>
                <th class="bor" width="6%">цифровой код</th>
                <th class="bor" width="">краткое
                    наименование
                </th>
            </tr>
        </thead>
        <tbody class="">
            <tr class="">
                <td class="bor text-c">1</td>
                <td class="bor text-c">1a</td>
                <td class="bor text-c">2</td>
                <td class="bor text-c">2a</td>
                <td class="bor text-c">3</td>
                <td class="bor text-c">4</td>
                <td class="bor text-c">5</td>
                <td class="bor text-c">6</td>
                <td class="bor text-c">7</td>
                <td class="bor text-c">8</td>
                <td class="bor text-c">9</td>
                <td class="bor text-c">10</td>
                <td class="bor text-c">10a</td>
                <td class="bor text-c">11</td>
            </tr>

            <?php $model->isGroupOwnOrdersByProduct = true; ?>
            <?php foreach ($model->ownOrders as $ownOrder) : ?>
                <?php
                $invoiceOrder = $ownOrder->order;
                $product = $invoiceOrder->product;
                $amountNoNds = $model->getPrintOrderAmount($ownOrder->order_id, true);
                $amountWithNds = $model->getPrintOrderAmount($ownOrder->order_id);
                $hideUnits = ($product->production_type == Product::PRODUCTION_TYPE_SERVICE && !$model->show_service_units);
                if ($ownOrder->quantity != intval($ownOrder->quantity)) {
                    $ownOrder->quantity = rtrim(number_format($ownOrder->quantity, 10, '.', ''), 0);
                } ?>
                <tr>
                    <td class="bor"><?= $invoiceOrder->product_title; ?></td>
                    <td class="bor"><?= $product->item_type_code ? : Product::DEFAULT_VALUE; ?></td>
                    <td class="bor">
                        <?php
                        $value = $invoiceOrder->unit ? $invoiceOrder->unit->code_okei : Product::DEFAULT_VALUE;
                        echo $hideUnits ? Product::DEFAULT_VALUE : $value;
                        ?>
                    </td>
                    <td class="bor">
                        <?php
                        $value = $invoiceOrder->unit ? $invoiceOrder->unit->name : Product::DEFAULT_VALUE;
                        echo $hideUnits ? Product::DEFAULT_VALUE : $value;
                        ?>
                    </td>
                    <td class="bor text-r">
                        <?= ($hideUnits && $ownOrder->quantity == 1) ? Product::DEFAULT_VALUE : strtr($ownOrder->quantity, ['.' => ',']); ?>
                    </td>
                    <td class="bor text-r">
                        <?php
                        $value = $invoiceOrder->selling_price_no_vat ?
                                TextHelper::invoiceMoneyFormat($invoiceOrder->selling_price_no_vat, $precision) :
                                Product::DEFAULT_VALUE;
                        echo $hideUnits ? Product::DEFAULT_VALUE : $value;
                        ?>
                    </td>
                    <td class="bor text-r"><?= TextHelper::invoiceMoneyFormat($amountNoNds, $precision); ?></td>
                    <td class="bor text-c">
                        <?= $invoiceOrder->excise ? TextHelper::invoiceMoneyFormat($invoiceOrder->excise_price, 2) : 'без акциза'; ?>
                    </td>
                    <td class="bor text-c"><?= $invoiceOrder->saleTaxRate->name; ?></td>
                    <td class="bor text-r"><?= TextHelper::invoiceMoneyFormat($ownOrder->amountNds, $precision); ?></td>
                    <td class="bor text-r"><?= TextHelper::invoiceMoneyFormat($amountWithNds, $precision); ?></td>
                    <td class="bor"><?= $ownOrder->country->code == '--'? Product::DEFAULT_VALUE : $ownOrder->country->code ; ?></td>
                    <td class="bor"><?= $ownOrder->country->name_short == '--'? Product::DEFAULT_VALUE : $ownOrder->country->name_short ; ?></td>
                    <td class="bor"><?= $ownOrder->custom_declaration_number; ?></td>
                </tr>
            <?php endforeach; ?>

            <tr>
                <td colspan="6" class="bor font-b"><b>Всего к оплате</b></td>
                <td class="bor text-r"><?= TextHelper::invoiceMoneyFormat($model->getPrintAmountNoNds(), 2); ?></td>
                <td colspan="2" class="bor text-c">X</td>
                <td class="bor text-r"><?= TextHelper::invoiceMoneyFormat($model->totalNds, 2); ?></td>
                <td class="bor text-r"><?= TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2); ?></td>
                <td colspan="3" style="border: none"></td>
            </tr>
        </tbody>
    </table>
    <table class="mar-b-20" style="">
        <tr>
            <td class="font-size-8" width="19%">
                Руководитель организации<br>или иное уполномоченное лицо
            </td>
            <td width="10%" style="border-bottom: 1px solid black; padding:0; text-align: center">
                <?= Html::tag('img', null, [
                    'src' => $signatureLink,
                    'class' => 'signature_image',
                    'style' => 'display: ' . ($addStamp ? 'block;' : 'none;'),
                ]) ?>
            </td>
            <td width="1%"></td>
            <td class="v-al-b text-c" width="17%"
                style="border-bottom: 1px solid black">
                <?php if ($model->invoice->company->company_type_id != CompanyType::TYPE_IP) : ?>
                    <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                        <?= $model->signed_by_name ? $model->signed_by_name : $model->invoice->getCompanyChiefFio(true); ?>
                        <?php if ($model->signed_by_employee_id) : ?>
                            <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                            <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                        <?php endif ?>
                    <?php else : ?>
                        <?= $model->invoice->contractor->director_name; ?>
                    <?php endif; ?>
                <?php endif; ?>
            </td>
            <td class="font-size-8" width="18%">Главный бухгалтер<br>или иное
                уполномоченное лицо
            </td>
            <td width="10%" style="border-bottom: 1px solid black; padding:0; text-align: center">
                <?= Html::tag('img', null, [
                    'src' => $signatureLink,
                    'class' => 'signature_image',
                    'style' => 'display: ' . ($addStamp ? 'block;' : 'none;'),
                ]) ?>
            </td>
            <td width="1%"></td>
            <td class="v-al-b text-c" width="17%" style="border-bottom: 1px solid black">
                <?php if ($model->invoice->company->company_type_id != CompanyType::TYPE_IP) : ?>
                    <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                        <?= $model->signed_by_name ? $model->signed_by_name : $model->invoice->getCompanyChiefAccountantFio(true); ?>
                        <?php if ($model->signed_by_employee_id) : ?>
                            <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                            <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                        <?php endif ?>
                    <?php else : ?>
                        <?php if ($model->invoice->contractor->chief_accountant_is_director) : ?>
                            <?=$model->invoice->contractor->director_name?>
                        <?php else : ?>
                            <?=$model->invoice->contractor->chief_accountant_name?>
                        <?php endif ?>
                    <?php endif; ?>
                <?php endif; ?></td>
            <td width="6%"></td>
        </tr>
        <tr>
            <td></td>
            <td class="text-c font-xs v-al-t">(подпись)</td>
            <td></td>
            <td class="text-c font-xs v-al-t">(ф.и.о.)</td>
            <td></td>
            <td class="text-c font-xs v-al-t">(подпись)</td>
            <td></td>
            <td class="text-c font-xs v-al-t">(ф.и.о.)</td>
            <td width="6%"></td>
        </tr>
    </table>
    <table class="table no-border" style="">
        <tr>
            <td class="v-al-b" width="19%">
                Индивидуальный предприниматель<br>или иное уполномоченное лицо
            </td>
            <td width="10.4%" style="border-bottom: 1px solid black"></td>
            <td width="1%"></td>
            <td width="17.5%" class="v-al-b"
                style="border-bottom: 1px solid black">
                <?php if ($model->invoice->company->company_type_id == CompanyType::TYPE_IP) : ?>
                    <?= $model->invoice->getCompanyChiefFio(true); ?>
                <?php endif; ?></td>
            <td width="1%"></td>
            <td style="border-bottom: 1px solid black" class="v-al-b text-c">
                <?= $model->company->company_type_id == CompanyType::TYPE_IP ? $model->company->getCertificate(false) : null; ?>
            </td>
            <td width="6%"></td>
        </tr>
        <tr>
            <td></td>
            <td class="text-c font-xs v-al-t">(подпись)</td>
            <td width="1%"></td>
            <td class="text-c font-xs v-al-t">(ф.и.о.)</td>
            <td width="6%"></td>
            <td class="text-c font-xs v-al-t">(реквизиты
                свидетельства
                о государственной,<br/>
                регистрации индивидуального предпринимателя)
            </td>
            <td width="6%"></td>
        </tr>
    </table>
</div>

<?php
// замена тройного тире на одно длинное
$content = ob_get_contents();
ob_end_clean();
echo strtr($content, ['---' => '&mdash;']);
?>

<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id) : ?>
    <pagebreak />
<?php endif; ?>
