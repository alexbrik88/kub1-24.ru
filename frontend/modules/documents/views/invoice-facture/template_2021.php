<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\product\Product;
use frontend\modules\documents\components\Message;
use frontend\models\Documents;
use common\models\document\InvoiceFacture;
use common\models\company\CompanyType;
use common\components\image\EasyThumbnailImage;
use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\document\InvoiceFacture */

common\assets\DocumentTemplateAsset::register($this);

// bind PackingList orders position numbers with InvoiceFacture orders position numbers
$ownOrders = $model->getOwnOrdersByShippingDocuments();

$precision = $model->invoice->price_precision;

$company = $model->company;

$addStamp = (boolean) $model->add_stamp;

if ($model->signed_by_name) {
    $accountantSignatureLink = $signatureLink = !$model->employeeSignature
        ? null
        : EasyThumbnailImage::thumbnailSrc($model->employeeSignature->file, 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);
} else {
    $signatureLink = EasyThumbnailImage::thumbnailSrc($model->getImage('chiefSignatureImage'), 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);
}

$printLink = EasyThumbnailImage::thumbnailSrc($model->getImage('printImage'), 200, 200, EasyThumbnailImage::THUMBNAIL_INSET);

$tplVer = $company->invoice_facture_print_template == 2 ? 2 : 1;
?>

<?php
// замена тройного тире на одно длинное start
ob_start();
?>

<div class="document-template invoice-facture-template">

    <?= $this->render("templatePartials/_template_head_2021_v{$tplVer}", ['model' => $model]) ?>

    <table class="mar-b-20">
        <thead class="no-bold">
            <tr>
                <th rowspan="2" class="bor" width="2%">
                    №<br/>п/п
                </th>
                <th rowspan="2" class="bor" width="13%">
                    Наименование товара (описание выполненных работ, оказанных
                    услуг), имущественного права
                </th>
                <th rowspan="2" class="bor" width="2%">
                    Код вида товара
                </th>
                <th colspan="2" class="bor" width="13%">
                    Единица измерения
                </th>
                <th rowspan="2" class="bor" width="5%">
                    Коли-чество (объем)
                </th>
                <th rowspan="2" class="bor" width="7%">Цена
                    (тариф) за единицу измерения
                </th>
                <th rowspan="2" class="bor" width="10%">
                    Стоимость товаров (работ, услуг), имущественных прав без
                    налога -
                    всего
                </th>
                <th rowspan="2" class="bor" width="7%">В том
                    числе сумма акциза
                </th>
                <th rowspan="2" class="bor" width="6%">
                    Налоговая ставка
                </th>
                <th rowspan="2" class="bor" width="9%">Сумма
                    налога, предъявляемая покупателю
                </th>
                <th rowspan="2" class="bor" width="9%">
                    Стоимость товаров (работ, услуг), имущественных прав с
                    налогом -
                    всего
                </th>
                <th colspan="2" class="bor" width="13%">
                    Страна происхождения товара
                </th>
                <th rowspan="2" class="bor" width="4%">
                    Регистрационный номер декларации
                    на товары или регистрационный номер партии товара, подлежащего прослеживаемости
                </th>
                <th colspan="2" class="bor" width="">
                    Количественная единица измерения товара, используемая в целях осуществления прослеживаемости
                </th>
                <th rowspan="2" class="bor" width="6%">
                    Количество товара, подлежащего прослеживаемости, в количественной единице измерения товара, используемой в целях осуществления прослеживаемости
                </th>
            </tr>
            <tr>
                <th class="bor" width="3%">код</th>
                <th class="bor" width="4%">условное обозначение (национальной)</th>
                <th class="bor" width="6%">цифровой код</th>
                <th class="bor" width="4%">краткое наименование</th>
                <th class="bor" width="3%">код</th>
                <th class="bor" width="3%">условное обозначение</th>
            </tr>
        </thead>
        <tbody class="">
            <tr class="">
                <td class="bor text-c">1</td>
                <td class="bor text-c">1a</td>
                <td class="bor text-c">1б</td>
                <td class="bor text-c">2</td>
                <td class="bor text-c">2a</td>
                <td class="bor text-c">3</td>
                <td class="bor text-c">4</td>
                <td class="bor text-c">5</td>
                <td class="bor text-c">6</td>
                <td class="bor text-c">7</td>
                <td class="bor text-c">8</td>
                <td class="bor text-c">9</td>
                <td class="bor text-c">10</td>
                <td class="bor text-c">10a</td>
                <td class="bor text-c">11</td>
                <td class="bor text-c">12</td>
                <td class="bor text-c">12а</td>
                <td class="bor text-c">13</td>
            </tr>

            <?php $model->isGroupOwnOrdersByProduct = true; ?>
            <?php foreach ($ownOrders as $orderPos => $ownOrder) : ?>
                <?php
                $invoiceOrder = $ownOrder->order;
                $product = $invoiceOrder->product;
                $amountNoNds = $model->getPrintOrderAmount($ownOrder->order_id, true);
                $amountWithNds = $model->getPrintOrderAmount($ownOrder->order_id);
                $hideUnits = ($product->production_type == Product::PRODUCTION_TYPE_SERVICE && !$model->show_service_units);
                if ($ownOrder->quantity != intval($ownOrder->quantity)) {
                    $ownOrder->quantity = rtrim(number_format($ownOrder->quantity, 10, '.', ''), 0);
                } ?>
                <tr>
                    <td class="bor text-center"><?= $orderPos ?></td>
                    <td class="bor"><?= $invoiceOrder->product_title; ?></td>
                    <td class="bor"><?= $product->item_type_code ? : Product::DEFAULT_VALUE; ?></td>
                    <td class="bor">
                        <?php
                        $value = $invoiceOrder->unit ? $invoiceOrder->unit->code_okei : Product::DEFAULT_VALUE;
                        echo $hideUnits ? Product::DEFAULT_VALUE : $value;
                        ?>
                    </td>
                    <td class="bor">
                        <?php
                        $value = $invoiceOrder->unit ? $invoiceOrder->unit->name : Product::DEFAULT_VALUE;
                        echo $hideUnits ? Product::DEFAULT_VALUE : $value;
                        ?>
                    </td>
                    <td class="bor text-r">
                        <?= ($hideUnits && $ownOrder->quantity == 1) ? Product::DEFAULT_VALUE : strtr($ownOrder->quantity, ['.' => ',']); ?>
                    </td>
                    <td class="bor text-r">
                        <?php
                        $value = $invoiceOrder->selling_price_no_vat ?
                                TextHelper::invoiceMoneyFormat($invoiceOrder->selling_price_no_vat, $precision) :
                                Product::DEFAULT_VALUE;
                        echo $hideUnits ? Product::DEFAULT_VALUE : $value;
                        ?>
                    </td>
                    <td class="bor text-r"><?= TextHelper::invoiceMoneyFormat($amountNoNds, $precision); ?></td>
                    <td class="bor text-c">
                        <?= $invoiceOrder->excise ? TextHelper::invoiceMoneyFormat($invoiceOrder->excise_price, 2) : 'без акциза'; ?>
                    </td>
                    <td class="bor text-c"><?= $invoiceOrder->saleTaxRate->name; ?></td>
                    <td class="bor text-r"><?= TextHelper::invoiceMoneyFormat($ownOrder->amountNds, $precision); ?></td>
                    <td class="bor text-r"><?= TextHelper::invoiceMoneyFormat($amountWithNds, $precision); ?></td>
                    <td class="bor"><?= $ownOrder->country->code == '--'? Product::DEFAULT_VALUE : $ownOrder->country->code ; ?></td>
                    <td class="bor"><?= $ownOrder->country->name_short == '--'? Product::DEFAULT_VALUE : $ownOrder->country->name_short ; ?></td>
                    <td class="bor"><?= ($ownOrder->custom_declaration_number) ? $ownOrder->custom_declaration_number : Product::DEFAULT_VALUE; ?></td>
                    <td class="bor"><?= ($invoiceOrder->product->is_traceable && $invoiceOrder->unit) ? $invoiceOrder->unit->code_okei : Product::DEFAULT_VALUE ?></td>
                    <td class="bor"><?= ($invoiceOrder->product->is_traceable && $invoiceOrder->unit) ? $invoiceOrder->unit->name : Product::DEFAULT_VALUE ?></td>
                    <td class="bor text-r"><?= ($invoiceOrder->product->is_traceable) ? strtr($ownOrder->quantity, ['.' => ',']) : Product::DEFAULT_VALUE; ?></td>
                </tr>
            <?php endforeach; ?>

            <tr>
                <td colspan="7" class="bor font-b"><b>Всего к оплате</b></td>
                <td class="bor text-r"><?= TextHelper::invoiceMoneyFormat($model->getPrintAmountNoNds(), 2); ?></td>
                <td colspan="2" class="bor text-c">X</td>
                <td class="bor text-r"><?= TextHelper::invoiceMoneyFormat($model->totalNds, 2); ?></td>
                <td class="bor text-r"><?= TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2); ?></td>
                <td colspan="6" style="border: none"></td>
            </tr>
        </tbody>
    </table>
    <table class="mar-b-20" style="">
        <tr>
            <td class="font-size-8" width="19%">
                Руководитель организации<br>или иное уполномоченное лицо
            </td>
            <td width="10%" style="border-bottom: 1px solid black; padding:0; text-align: center">
                <?= Html::tag('img', null, [
                    'src' => $signatureLink,
                    'class' => 'signature_image',
                    'style' => 'display: ' . ($addStamp ? 'block;' : 'none;'),
                ]) ?>
            </td>
            <td width="1%"></td>
            <td class="v-al-b text-c" width="17%"
                style="border-bottom: 1px solid black">
                <?php if ($model->invoice->company->company_type_id != CompanyType::TYPE_IP) : ?>
                    <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                        <?= $model->signed_by_name ? $model->signed_by_name : $model->invoice->getCompanyChiefFio(true); ?>
                        <?php if ($model->signed_by_employee_id) : ?>
                            <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                            <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                        <?php endif ?>
                    <?php else : ?>
                        <?= $model->invoice->contractor->director_name; ?>
                    <?php endif; ?>
                <?php endif; ?>
            </td>
            <td class="font-size-8" width="18%">Главный бухгалтер<br>или иное
                уполномоченное лицо
            </td>
            <td width="10%" style="border-bottom: 1px solid black; padding:0; text-align: center">
                <?= Html::tag('img', null, [
                    'src' => $signatureLink,
                    'class' => 'signature_image',
                    'style' => 'display: ' . ($addStamp ? 'block;' : 'none;'),
                ]) ?>
            </td>
            <td width="1%"></td>
            <td class="v-al-b text-c" width="17%" style="border-bottom: 1px solid black">
                <?php if ($model->invoice->company->company_type_id != CompanyType::TYPE_IP) : ?>
                    <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                        <?= $model->signed_by_name ? $model->signed_by_name : $model->invoice->getCompanyChiefAccountantFio(true); ?>
                        <?php if ($model->signed_by_employee_id) : ?>
                            <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                            <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                        <?php endif ?>
                    <?php else : ?>
                        <?php if ($model->invoice->contractor->chief_accountant_is_director) : ?>
                            <?=$model->invoice->contractor->director_name?>
                        <?php else : ?>
                            <?=$model->invoice->contractor->chief_accountant_name?>
                        <?php endif ?>
                    <?php endif; ?>
                <?php endif; ?></td>
            <td width="6%"></td>
        </tr>
        <tr>
            <td></td>
            <td class="text-c font-xs v-al-t">(подпись)</td>
            <td></td>
            <td class="text-c font-xs v-al-t">(ф.и.о.)</td>
            <td></td>
            <td class="text-c font-xs v-al-t">(подпись)</td>
            <td></td>
            <td class="text-c font-xs v-al-t">(ф.и.о.)</td>
            <td width="6%"></td>
        </tr>
    </table>
    <table class="table no-border" style="">
        <tr>
            <td class="v-al-b" width="19%">
                Индивидуальный предприниматель<br>или иное уполномоченное лицо
            </td>
            <td width="10.4%" style="border-bottom: 1px solid black"></td>
            <td width="1%"></td>
            <td width="17.5%" class="v-al-b"
                style="border-bottom: 1px solid black">
                <?php if ($model->invoice->company->company_type_id == CompanyType::TYPE_IP) : ?>
                    <?= $model->invoice->getCompanyChiefFio(true); ?>
                <?php endif; ?></td>
            <td width="1%"></td>
            <td style="border-bottom: 1px solid black" class="v-al-b text-c">
                <?= $model->company->company_type_id == CompanyType::TYPE_IP ? $model->company->getCertificate(false) : null; ?>
            </td>
            <td width="6%"></td>
        </tr>
        <tr>
            <td></td>
            <td class="text-c font-xs v-al-t">(подпись)</td>
            <td width="1%"></td>
            <td class="text-c font-xs v-al-t">(ф.и.о.)</td>
            <td width="6%"></td>
            <td class="text-c font-xs v-al-t">(реквизиты
                свидетельства
                о государственной,<br/>
                регистрации индивидуального предпринимателя)
            </td>
            <td width="6%"></td>
        </tr>
    </table>
</div>

<?php
// замена тройного тире на одно длинное
$content = ob_get_contents();
ob_end_clean();
echo strtr($content, ['---' => '&mdash;']);
?>

<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id) : ?>
    <pagebreak />
<?php endif; ?>
