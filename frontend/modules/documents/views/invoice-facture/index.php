<?php

use common\components\date\DateHelper;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\widgets\Modal;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\components\DocConverter;
use frontend\modules\documents\components\FilterHelper;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Company;
use common\components\ImageHelper;
use \common\models\document\status\InvoiceFactureStatus;

/* @var $this yii\web\View */
/* @var $searchModel \frontend\modules\documents\models\InvoiceFactureSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ioType int */
/* @var $message Message */
/* @var $company Company */

$this->title = $message->get(Message::TITLE_PLURAL);

$period = StatisticPeriod::getSessionName();
$company = Yii::$app->user->identity->company;
$exists = Invoice::find()->joinWith('invoiceFacture', false, 'INNER JOIN')
    ->byCompany(Yii::$app->user->identity->company->id)
    ->byDeleted(false)->byIOType($ioType)->exists();

$isFilter = (boolean)($searchModel->byNumber);

if ($exists) {
    if ($isFilter) {
        $emptyMessage = "По вашему запросу, в выбранном периоде «{$period}», ничего не найдено. Измените период, и попробуйте еще раз.";
    } else {
        $emptyMessage = "В выбранном периоде «{$period}», у вас нет счетов-фактур. Измените период, чтобы увидеть имеющиеся счета-фактур.";
    }
} else {
    $emptyMessage = 'Вы еще не создали ни одного счёта фактуры.';
}

$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, ['ioType' => $ioType]);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
    'ioType' => $ioType,
]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
    'ioType' => $ioType,
]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canSend = $ioType == \frontend\models\Documents::IO_TYPE_OUT &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = $ioType == \frontend\models\Documents::IO_TYPE_OUT;
$canUpdateStatus = Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);

$dropItems = [];
if ($canUpdateStatus){
    if ($ioType == \frontend\models\Documents::IO_TYPE_OUT) {
        $dropItems[] = [
            'label' => 'Передан',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-status', 'type' => $ioType, 'status' => InvoiceFactureStatus::STATUS_DELIVERED])
            ]
        ];
    } else {
        $dropItems[] = [
            'label' => 'Скан',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-original', 'type' => $ioType, 'val' => 0])
            ]
        ];
        $dropItems[] = [
            'label' => 'Оригинал',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-original', 'type' => $ioType, 'val' => 1])
            ]
        ];
    }
}
if ($ioType == \frontend\models\Documents::IO_TYPE_OUT && $canIndex) {
    $dropItems[] = [
        'label' => 'Скачать в Excel',
        'url' => ['generate-xls'],
        'linkOptions' => [
            'class' => 'get-xls-link',
        ],
    ];
}

$existsInvoicesToAdd = Invoice::find()
    ->where(['can_add_invoice_facture' => true])
    ->byCompany(Yii::$app->user->identity->company->id)
    ->byDeleted()->byIOType($ioType)->exists();


$this->registerJs('
    $(document).on("change", ".autoinvoicefacture-rule-radio", function() {
        var additional = $(".autoinvoicefacture-rule-radio:checked").data("additional");
        $(".additional-rule.collapse.in").collapse("hide");
        $(additional).collapse("show");
        $("input[type=checkbox]", $(additional)).prop({disabled: false}).uniform("refresh");
    });
    $(document).on("hidden.bs.collapse", ".additional-rule.collapse", function () {
        $("input[type=checkbox]", this).prop({checked: false, disabled: true}).uniform("refresh");
    });
    $(document).on("hidden.bs.modal", "#autoinvoicefacture-form-modal", function () {
        $.pjax.reload("#autoinvoicefacture-form-pjax");
    });
');
?>
<?php if (!Documents::getSearchQuery(Documents::DOCUMENT_INVOICE, $ioType)->byDeleted()->exists()): ?>
    <?php
    Modal::begin([
        'clientOptions' => ['show' => true],
        'closeButton' => false,
    ]);
    echo '<div class="text-center"><p class="empty-invoice-modal">Перед тем как подготовить счета-фактуры, нужно создать счет.</p>';
    echo Html::a('СОЗДАТЬ СЧЁТ', ['invoice/create', 'type' => $ioType], ['class' => 'btn yellow']);
    echo '</div>';
    Modal::end();
    ?>
    <div class="alert-success alert fade in">
        <button id="contractor_alert_close" type="button" class="close"
                data-dismiss="alert" aria-hidden="true">×
        </button>
        Перед тем как подготовить счета-фактуры,
        нужно <?= Html::a('создать счёт', ['invoice/create', 'type' => $ioType]) ?>
        .
    </div>
<?php endif; ?>
<div class="portlet box">
    <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
            'ioType' => $ioType,
        ]) && !$company->companyTaxationType->usn): ?>
        <div class="btn-group pull-right title-buttons">
            <?php if ($company->createInvoiceAllowed($ioType)) : ?>
                <?php if ($existsInvoicesToAdd): ?>
                    <?= Html::button('<i class="fa fa-plus"></i> ДОБАВИТЬ', [
                        'class' => 'btn yellow text-white add-document',
                    ]) ?>
                <?php else: ?>
                    <?= Html::a('<i class="fa fa-plus"></i> ДОБАВИТЬ', [
                        '/documents/invoice/create',
                        'type' => $ioType,
                        'document' => Documents::SLUG_INVOICE_FACTURE,
                    ], [
                        'class' => 'btn yellow text-white',
                    ]) ?>
                <?php endif; ?>
            <?php else : ?>
                <?= Html::button('<i class="fa fa-plus"></i> ДОБАВИТЬ', [
                    'class' => 'btn yellow text-white action-is-limited',
                ]) ?>
            <?php endif ?>
        </div>
    <?php endif; ?>
    <h3 class="page-title"><?= Html::encode($this->title); ?></h3>
    <div class="row" style="margin: 25px 0 -25px 0;">

        <div class="clearfix">
            <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'doc-gray-button  btn_no_right']); ?>
        </div>
        <?php if ($ioType == Documents::IO_TYPE_OUT && Yii::$app->user->can(UserRole::ROLE_CHIEF)) : ?>
            <div class="clearfix" style="margin-bottom: 25px;">
                <?php Modal::begin([
                    'id' => 'autoinvoicefacture-form-modal',
                    'header' => '<h1>Правила выставления АвтоСчетФактур</h1>',
                    'toggleButton' => [
                        'tag' => 'div',
                        'label' => 'АвтоСчетФактура',
                        'class' => 'btn yellow pull-right',
                        'style' => 'width: calc(25% - 30px) !important; min-width: 156.5px;'
                    ],
                ]); ?>

                <?= $this->render('autoinvoicefacture') ?>

                <?php Modal::end(); ?>
            </div>
        <?php endif ?>
    </div>
</div>

<div class="row" id="widgets" style="margin-top:-20px; margin-bottom:5px;">
    <div class="col-sm-12">
        <div class="table-icons" style="margin-top: <?= ($ioType == Documents::IO_TYPE_OUT) ? '-10px' : '-15px' ?>; margin-bottom: 5px;">
            <?= Html::a('<i class="fa fa-file-excel-o"></i>', array_merge(['get-xls'], Yii::$app->request->queryParams), [
                'class' => 'get-xls-link pull-right',
                'title' => 'Скачать в Excel',
            ]); ?>
        </div>
    </div>
</div>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Список счетов-фактур
        </div>
        <div
                class="search-tools tools tools_button <?= $ioType == Documents::IO_TYPE_OUT ? 'col-md-4 col-sm-4' : 'col-md-6 col-sm-6'; ?>">
            <div class="form-body">
                <?php $form = \yii\widgets\ActiveForm::begin([
                    'method' => 'GET',
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'options' => [
                            'class' => '',
                        ],
                    ],
                ]); ?>
                <div class="search_cont">
                    <div class="wimax_input ">
                        <?= $form->field($searchModel, 'byNumber')->textInput([
                            'placeholder' => 'Номер счет-фактуры, название или ИНН контрагента',
                        ]); ?>
                    </div>
                    <div class="wimax_button">
                        <?= Html::submitButton('НАЙТИ', [
                            'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                        ]) ?>
                    </div>
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
        <div
                class="actions joint-operations <?= $ioType == Documents::IO_TYPE_OUT ? 'col-md-5 col-sm-5' : 'col-md-3 col-sm-3'; ?>"
                style="display:none;">

            <?php if ($dropItems): ?>
                <div class="dropdown">
                    <?= Html::a('Еще  <span class="caret"></span>', null, [
                        'class' => 'btn btn-default btn-sm dropdown-toggle',
                        'id' => 'dropdownMenu1',
                        'data-toggle' => 'dropdown',
                        'aria-expanded' => true,
                        'style' => 'height: 28px;',
                    ]); ?>
                    <?= Dropdown::widget([
                        'items' => $dropItems,
                        'options' => [
                            'style' => 'right: -30px;left: auto;top: 28px;',
                            'aria-labelledby' => 'dropdownMenu1',
                        ],
                    ]); ?>
                </div>
            <?php endif; ?>

            <?php if ($canDelete) : ?>
                <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                    'class' => 'btn btn-default btn-sm',
                    'data-toggle' => 'modal',
                ]); ?>

                <div id="many-delete" class="confirm-modal fade modal" role="dialog"
                     tabindex="-1" aria-hidden="true"
                     style="display: none; margin-top: -51.5px;">
                    <div class="modal-dialog ">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="form-body">
                                    <div class="row">Вы уверены, что хотите удалить
                                        выбранные счет фактуры?
                                    </div>
                                </div>
                                <div class="form-actions row">
                                    <div class="col-xs-6">
                                        <?= Html::a('ДА', null, [
                                            'class' => 'btn darkblue pull-right modal-many-delete',
                                            'data-url' => Url::to(['many-delete', 'type' => $ioType]),
                                        ]); ?>
                                    </div>
                                    <div class="col-xs-6">
                                        <button type="button" class="btn darkblue"
                                                data-dismiss="modal">НЕТ
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif ?>

            <?php if ($canSend) : ?>
                <?= Html::a('<i class="glyphicon glyphicon-envelope"></i> Отправить', null, [
                    'class' => 'btn btn-default btn-sm document-many-send',
                    'data-url' => Url::to(['many-send', 'type' => $ioType]),
                ]); ?>
                <div class="modal fade confirm-modal" id="many-send-error"
                     tabindex="-1"
                     role="modal"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close"
                                        data-dismiss="modal"
                                        aria-hidden="true"></button>
                                <h3 style="text-align: center; margin: 0">Ошибка
                                    при отправке счет фактур</h3>
                            </div>
                            <div class="modal-body">
                                <div class="form-body">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif ?>

            <?php if ($canPrint) : ?>
                <?= Html::a('<i class="fa fa-print"></i> Печать', Url::to([
                    'many-document-print',
                    'actionType' => 'pdf',
                    'type' => $ioType,
                    'multiple' => ''
                ]), [
                    'class' => 'btn btn-default btn-sm multiple-print',
                    'target' => '_blank',
                ]); ?>
            <?php endif ?>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <?= common\components\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'emptyText' => $emptyMessage,
            'tableOptions' => [
                'class' => 'table table-striped table-bordered table-hover dataTable customers_table overfl_text_hid outgoing_acts_table',
                'aria-describedby' => 'datatable_ajax_info',
                'role' => 'grid',
            ],
            'headerRowOptions' => [
                'class' => 'heading',
            ],
            'options' => [
                'class' => 'dataTables_wrapper dataTables_extended_wrapper',
            ],
            'rowOptions' => [
                'role' => 'row',
            ],
            'pager' => [
                'options' => [
                    'class' => 'pagination pull-right',
                ],
            ],
            'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
            'columns' => [
                [
                    'header' => Html::checkbox('', false, [
                        'class' => 'joint-operation-main-checkbox',
                    ]),
                    'headerOptions' => [
                        'class' => 'text-center',
                        'width' => '5%',
                    ],
                    'contentOptions' => [
                        'class' => 'text-center',
                    ],
                    'format' => 'raw',
                    'value' => function (InvoiceFacture $model) {
                        return Html::checkbox('InvoiceFacture[' . $model->id . '][checked]', false, [
                            'class' => 'joint-operation-checkbox',
                            'data-contractor' => $model->invoice->contractor_id,
                        ]);

                    },
                ],
                [
                    'attribute' => 'document_date',
                    'label' => 'Дата СФ',
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                    'contentOptions' => [
                        'class' => 'link-view',
                    ],
                    'value' => function ($data) {
                        return DateHelper::format($data->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    },
                ],
                [
                    'attribute' => 'document_number',
                    'label' => '№ СФ',
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                    'contentOptions' => [
                        'class' => 'document_number link-view',
                    ],
                    'format' => 'raw',
                    'value' => function (InvoiceFacture $data) {
                        return (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $data,])
                            ? Html::a($data->fullNumber, ['view', 'type' => $data->type, 'id' => $data->id])
                            : $data->fullNumber);
                    },
                ],
                [
                    'label' => 'Скан',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '70px',
                    ],
                    'contentOptions' => [
                        'class' => 'link-view',
                    ],
                    'attribute' => 'has_file',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Yii::$app->view->render('@documents/views/layouts/_doc-file-link', [
                            'model' => $model,
                        ]);
                    },
                ],
                [
                    'attribute' => 'totalAmountWithNds',
                    'label' => 'Сумма',
                    'format' => 'raw',
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                    'contentOptions' => [
                        'class' => 'link-view',
                    ],
                    'value' => function (InvoiceFacture $data) {
                        $price = \common\components\TextHelper::invoiceMoneyFormat($data->totalAmountWithNds, 2);
                        return '<span class="price" data-price="' . str_replace(" ", "", $price) . '">' . $price . '</span>';
                    },
                ],

                [
                    'attribute' => 'contractor_id',
                    'label' => 'Контрагент',
                    'class' => DropDownSearchDataColumn::className(),
                    'headerOptions' => [
                        'class' => 'dropdown-filter',
                        'width' => '30%',
                    ],
                    'contentOptions' => [
                        'class' => 'contractor-cell',
                    ],
                    'filter' => FilterHelper::getContractorList($searchModel->type, InvoiceFacture::tableName(), true, false, false),
                    'format' => 'raw',
                    'value' => function (InvoiceFacture $data) {
                        return '<span data-id="'.$data->invoice->contractor_id.'" title="' . htmlspecialchars($data->invoice->contractor_name_short) . '">' .
                            $data->invoice->contractor_name_short .
                            '</span>';
                    },
                ],

                [
                    'attribute' => 'status_out_id',
                    'label' => 'Статус',
                    'class' => DropDownDataColumn::className(),
                    'headerOptions' => [
                        'class' => 'dropdown-filter',
                    ],
                    'filter' => $searchModel->getStatusArray($searchModel->type),
                    'value' => function (InvoiceFacture $data) {
                        return $data->statusOut->name;
                    },
                    'visible' => $ioType == Documents::IO_TYPE_OUT,
                ],
                [
                    'attribute' => 'invoice_document_number',
                    'label' => 'Счёт №',
                    'format' => 'raw',
                    'headerOptions' => [
                        'class' => 'sorting',
                    ],
                    'value' => function (InvoiceFacture $data) {
                        if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $data,])
                        ) {
                            if ($data->invoice->file !== null) {
                                $invoiceFileLinkClass = null;
                                $tooltipId = null;
                                $contentPreview = null;
                                if (in_array($data->invoice->file->ext, ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf'])) {
                                    $invoiceFileLinkClass = 'invoice-file-link-preview';
                                    $tooltipId = 'invoice-file-link-preview-' . $data->invoice->file->id;
                                    $thumb = $data->invoice->file->getImageThumb(400, 600);
                                    if ($thumb) {
                                        $contentPreview .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                                        $contentPreview .= Html::beginTag('span', ['id' => $tooltipId]);
                                        $contentPreview .= Html::img($thumb, ['alt' => '']);
                                        $contentPreview .= Html::endTag('span');
                                        $contentPreview .= Html::endTag('div');
                                    }
                                }

                                return Html::a($data->invoice->fullNumber,
                                        ['invoice/view', 'type' => $data->type, 'id' => $data->invoice_id])
                                    . Html::a('<span class="pull-right icon icon-paper-clip"></span>',
                                        ['/documents/invoice/file-get', 'type' => $data->type, 'id' => $data->invoice->id, 'file-id' => $data->invoice->file->id,],
                                        [
                                            'class' => $invoiceFileLinkClass,
                                            'target' => '_blank',
                                            'data-tooltip-content' => '#' . $tooltipId,
                                            'style' => 'float: right',
                                        ]) . $contentPreview;
                            } else {
                                return Html::a($data->invoice->fullNumber,
                                    ['invoice/view', 'type' => $data->type, 'id' => $data->invoice_id]);
                            }
                        } else {
                            return $data->invoice->fullNumber;
                        }
                    },
                ],
            ],
        ]); ?>
    </div>
</div>
<?php $this->registerJs('
    $(".invoice-file-link-preview").tooltipster({
        theme: ["tooltipster-kub"],
        contentCloning: true,
        trigger: "hover",
        side: "left",
    });
'); ?>
<?php if ($company->show_popup_expose_other_documents && $ioType == Documents::IO_TYPE_OUT): ?>

    <?php $company->updateAttributes(['show_popup_expose_other_documents' => false]); ?>

    <?php Modal::begin([
        'header' => '<h2 class="header-name" style="text-transform: uppercase;">
            Подготовить акт, накладную<br> счет-фактуру
            </h2>',
        'footer' => $this->render('//layouts/modal/_partial_footer', [
            'type' => Company::AFTER_REGISTRATION_EXPOSE_OTHER_DOCUMENTS,
        ]),
        'id' => 'modal-loader-items'
    ]); ?>
    <div class="col-xs-12" style="padding: 0" id="modal-loader">
        <?= $this->render('//layouts/modal/_template_submodal', [
            'type' => 2,
            'description' => 'Для этого нужно выставить счет<br> и нажать нужную кнопку:',
            'video' => 'https://www.youtube.com/embed/jXesTUBlxl4',
            'link' => Url::to(['/documents/invoice/first-create', 'type' => Documents::IO_TYPE_OUT]),
            'image' => ImageHelper::getThumb('img/modal_registration/block-2.jpg', [680, 340], [
                'class' => 'hide-video',
                'style' => 'max-width: 100%',
            ]),
            'previousModal' => null,
            'nextModal' => 3,
        ]); ?>
    </div>
    <style>
        #modal-loader-items .modal-body {
            padding: 0;
        }
    </style>
    <?php Modal::end(); ?>
    <?php $this->registerJs('
        $(document).ready(function () {
            $(".modal#modal-loader-items").modal();
        });
    '); ?>
<?php endif; ?>
<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a('<i class="fa fa-print"></i> Печать', [
            'many-document-print',
            'actionType' => 'pdf',
            'type' => $ioType,
            'multiple' => ''
        ], [
            'class' => 'btn btn-sm darkblue text-white multiple-print',
            'target' => '_blank',
        ]) : null,
        $canSend ? Html::a('<i class="glyphicon glyphicon-envelope"></i> Отправить', null, [
            'class' => 'btn btn-sm darkblue text-white document-many-send',
            'data-url' => Url::to(['many-send', 'type' => $ioType]),
        ]) : null,
        $canDelete ? Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
            'class' => 'btn btn-sm darkblue text-white',
            'data-toggle' => 'modal',
        ]) : null,
        $dropItems ? Html::tag('div', Html::a('Еще  <b class="caret"></b>', '#', [
                'id' => 'dropdownMenu2',
                'class' => 'btn btn-sm darkblue text-white dropdown-toggle',
                'data-toggle' => 'dropdown',
            ]) . Dropdown::widget([
                'items' => $dropItems,
                'options' => [
                    'style' => 'left: auto; right: 0;'
                ],
            ]), ['class' => 'dropup']) : null,
    ],
]); ?>

<?= $this->render('@frontend/modules/documents/views/invoice/modal/_invoices_modal', [
    'company' => $company,
    'ioType'  => $ioType,
    'documentType' => Documents::SLUG_INVOICE_FACTURE,
    'documentTypeName' => 'Счет-фактуру'
]) ?>

<?php if ($canSend): ?>
    <?= $this->render('@frontend/modules/documents/views/invoice/view/_many_send_message', [
        'models' => [],
        'useContractor' => false,
        'showSendPopup' => false,
        'typeDocument' => Documents::DOCUMENT_INVOICE_FACTURE
    ]); ?>
<?php endif; ?>
