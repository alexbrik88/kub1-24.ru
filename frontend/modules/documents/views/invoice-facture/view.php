<?php
use \frontend\models\Documents;
use frontend\rbac\permissions\document\Document;
$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);
$paymentDocuments = [];
$this->title = $message->get(\frontend\modules\documents\components\Message::TITLE_SINGLE) . ' №' . $model->document_number;
foreach ($model->paymentDocuments as $doc) {
    $date = date('d.m.Y', strtotime($doc->payment_document_date));
    $paymentDocuments[] = "№ {$doc->payment_document_number} от {$date}";
}
$precision = $model->invoice->price_precision;
$this->registerJs(<<<JS
    buttonsScroll.init();
JS
);
 if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::DELETE)) {
    echo \frontend\widgets\ConfirmModalWidget::widget([
        'options' => [
            'id' => 'delete-confirm',
        ],
        'toggleButton' => false,
        'confirmUrl' => \yii\helpers\Url::toRoute(['delete', 'type' => $model->type, 'id' => $model->id]),
        'confirmParams' => [],
        'message' => "Вы уверены, что хотите удалить исходящую счет-фактуру № {$model->document_number}?",
    ]);
 }

$backUrl = null;
if ($useContractor && Yii::$app->user->can(frontend\rbac\permissions\Contractor::VIEW)) {
    $backUrl = ['/contractor/view', 'type' => $model->type, 'id' => $contractorId,];
} elseif (Yii::$app->user->can(frontend\rbac\permissions\document\Document::INDEX)) {
    $backUrl = ['index', 'type' => $model->type,];
}
?>
<div class="page-content-in out-document" style="padding-bottom: 30px;">
    <div class="col-xs-12 pad3">
        <?php if ($backUrl !== null) {
            echo \yii\helpers\Html::a('Назад к списку', $backUrl, [
                'class' => 'back-to-customers',
            ]);
        } ?>
    </div>
    <div class="col-xs-12 col-lg-7 pad0 ff-pdf-sans">
        <?php /* $this->render('pre-view-'. Documents::$ioTypeToUrl[$ioType], ['model' => $model,
            'ioType' => $model->type,
            'orders' => $model->orders,
            'useContractor' => $useContractor,
            'message' => $message,
            'paymentDocuments' => $paymentDocuments
        ]); */ ?>
        <style>
            .invoice-facture-template table th,
            .invoice-facture-template table td {
                font-size: 10px!important;
            }
        </style>
        <?= $this->render('/invoice-facture/template', [
            'model' => $model,
        ]); ?>
    </div>
    <?= $this->render('_viewPartials/_control_buttons_' . Documents::$ioTypeToUrl[$ioType], [
            'model' => $model,
            'useContractor' => $useContractor,
            'contractorId' => $contractorId,
            'message' => $message
    ]); ?>
    <?php /*
    <div class="col-xs-12 pad0 ff-pdf-sans" style="margin-top:-8px">
        <?= $this->render('pre-view-order-'. Documents::$ioTypeToUrl[$ioType], ['model' => $model,
            'ioType' => $model->type,
            'orders' => $model->orders,
            'useContractor' => $useContractor,
            'message' => $message,
            'paymentDocuments' => $paymentDocuments
        ]); ?>
    </div>*/ ?>
    <div id="buttons-bar-fixed">
        <?= $this->render('_viewPartials/_action_buttons_' . Documents::$ioTypeToUrl[$ioType], [
            'model' => $model,
            'useContractor' => $useContractor,
            'contractorId' => $contractorId,
        ]); ?>
    </div>
</div>
