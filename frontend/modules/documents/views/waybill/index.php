<?php

use common\components\date\DateHelper;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\models\document\Invoice;
use common\models\document\Waybill;
use common\widgets\Modal;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\components\DocConverter;
use frontend\modules\documents\components\FilterHelper;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Company;
use common\components\ImageHelper;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\documents\models\WaybillSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ioType int */
/* @var $message Message */

$this->title = $message->get(Message::TITLE_PLURAL);

$period = StatisticPeriod::getSessionName();
$company = Yii::$app->user->identity->company;
$exists = Invoice::find()->joinWith('waybills', false, 'INNER JOIN')
    ->byCompany(Yii::$app->user->identity->company->id)
    ->byDeleted(false)->byIOType($ioType)->exists();

$isFilter = (boolean) ($searchModel->byNumber);

if ($exists) {
    if ($isFilter) {
        $emptyMessage = "По вашему запросу, в выбранном периоде «{$period}», ничего не найдено. Измените период, и попробуйте еще раз.";
    } else {
        $emptyMessage = "В выбранном периоде «{$period}», у вас нет товарно-транспортных накладных. Измените период, чтобы увидеть имеющиеся накладные.";
    }
} else {
    $emptyMessage = 'Вы еще не создали ни одной товарно-транспортной накладной.';
}

$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, ['ioType' => $ioType]);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canSend = $ioType == \frontend\models\Documents::IO_TYPE_OUT &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = $ioType == \frontend\models\Documents::IO_TYPE_OUT;

$dropItems = [];
if ($ioType == Documents::IO_TYPE_OUT && $canIndex) {
    //$dropItems = [
    //    ['label' => 'Скачать в Excel', 'url' => ['generate-xls']],
    //];
}
?>
<?php if (!Documents::getSearchQuery(Documents::DOCUMENT_INVOICE, $ioType)->byDeleted()->exists()): ?>
    <?php
    Modal::begin([
        'clientOptions' => ['show' => true],
        'closeButton' => false,
    ]);
    echo '<div class="text-center"><p class="empty-invoice-modal">Перед тем как подготовить товарно-транспортную накладную, нужно создать счет.</p>';
    echo Html::a('СОЗДАТЬ СЧЁТ', ['invoice/create', 'type' => $ioType], ['class' => 'btn yellow']);
    echo '</div>';
    Modal::end();
    ?>
    <div class="alert-success alert fade in">
        <button id="contractor_alert_close" type="button" class="close"
                data-dismiss="alert" aria-hidden="true">×
        </button>
        Перед тем как подготовить товарно-транспортную накладную,
        нужно <?= Html::a('создать счёт', ['invoice/create', 'type' => $ioType]) ?>.
    </div>
<?php endif; ?>

<div class="portlet box">
    <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'doc-gray-button']); ?>
    <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
        'ioType' => $ioType,
    ])): ?>
        <div class="col-xs-3 pull-right">
            <div class="btn-group pull-right title-buttons">
                <?php if ($company->createInvoiceAllowed($ioType)) : ?>
                    <?= Html::a('<i class="fa fa-plus"></i> ДОБАВИТЬ', [
                        '/documents/invoice/create',
                        'type' => $ioType,
                        'create' => Documents::SLUG_WAYBILL,
                    ], [
                        'class' => 'btn yellow text-white',
                    ]) ?>
                <?php else : ?>
                    <?= Html::button('<i class="fa fa-plus"></i> ДОБАВИТЬ', [
                        'class' => 'btn yellow text-white action-is-limited',
                    ]) ?>
                <?php endif ?>
            </div>
        </div>
    <?php endif; ?>
    <h3 class="page-title"><?= Html::encode($this->title) ?></h3>
</div>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Список товарно-транспортных накладных
        </div>
        <div
            class="search-tools tools tools_button <?= $ioType == Documents::IO_TYPE_OUT ? 'col-md-4 col-sm-4' : 'col-md-6 col-sm-6'; ?>">
            <div class="form-body">
                <?php $form = \yii\widgets\ActiveForm::begin([
                    'method' => 'GET',
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'options' => [
                            'class' => '',
                        ],
                    ],
                ]); ?>
                <div class="search_cont">
                    <div class="wimax_input ">
                        <?= $form->field($searchModel, 'byNumber')->textInput([
                            'placeholder' => 'Номер накладной или название контрагента',
                        ]); ?>
                    </div>
                    <div class="wimax_button">
                        <?= Html::submitButton('НАЙТИ', [
                            'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                        ]) ?>
                    </div>
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
        <div
            class="actions joint-operations <?= $ioType == Documents::IO_TYPE_OUT ? 'col-md-4 col-sm-4' : 'col-md-3 col-sm-3'; ?>"
            style="display:none;">

            <?php if ($dropItems): ?>
                <div class="dropdown">
                    <?= Html::a('Еще  <span class="caret"></span>', null, [
                        'class' => 'btn btn-default btn-sm dropdown-toggle',
                        'id' => 'dropdownMenu1',
                        'data-toggle' => 'dropdown',
                        'aria-expanded' => true,
                        'style' => 'height: 28px;',
                    ]); ?>
                    <?= Dropdown::widget([
                        'items' => $dropItems,
                        'options' => [
                            'style' => 'right: -30px;left: auto;top: 28px;',
                            'aria-labelledby' => 'dropdownMenu1',
                        ],
                    ]); ?>
                </div>
            <?php endif; ?>

            <?php if ($canDelete) : ?>
                <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                    'class' => 'btn btn-default btn-sm',
                    'data-toggle' => 'modal',
                ]); ?>

                <div id="many-delete" class="confirm-modal fade modal" role="dialog"
                     tabindex="-1" aria-hidden="true"
                     style="display: none; margin-top: -51.5px;">
                    <div class="modal-dialog ">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="form-body">
                                    <div class="row">Вы уверены, что хотите удалить
                                        выбранные накладные?
                                    </div>
                                </div>
                                <div class="form-actions row">
                                    <div class="col-xs-6">
                                        <?= Html::a('ДА', null, [
                                            'class' => 'btn darkblue pull-right modal-many-delete',
                                            'data-url' => Url::to(['many-delete', 'type' => $ioType]),
                                        ]); ?>
                                    </div>
                                    <div class="col-xs-6">
                                        <button type="button" class="btn darkblue"
                                                data-dismiss="modal">НЕТ
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif ?>

            <?php if ($ioType == \frontend\models\Documents::IO_TYPE_OUT): ?>
                <?php if ($canSend) : ?>
                    <?= Html::a('<i class="glyphicon glyphicon-envelope"></i> Отправить', null, [
                        'class' => 'btn btn-default btn-sm document-many-send',
                        'data-url' => Url::to(['many-send', 'type' => $ioType]),
                    ]); ?>
                    <div class="modal fade confirm-modal" id="many-send-error"
                         tabindex="-1"
                         role="modal"
                         aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close"
                                            data-dismiss="modal"
                                            aria-hidden="true"></button>
                                    <h3 style="text-align: center; margin: 0">Ошибка
                                        при отправке товарных-накладных</h3>
                                </div>
                                <div class="modal-body">
                                    <div class="form-body">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif ?>

                <?php if ($canPrint) : ?>
                    <?= Html::a('<i class="fa fa-print"></i> Печать', Url::to([
                        'many-document-print',
                        'actionType' => 'pdf',
                        'type' => $ioType,
                        'multiple' => ''
                    ]), [
                        'class' => 'btn btn-default btn-sm multiple-print',
                        'target' => '_blank',
                    ]); ?>
                <?php endif ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <div class="dataTables_wrapper dataTables_extended_wrapper">
                <div>
                    <?= common\components\grid\GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'emptyText' => $emptyMessage,
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover dataTable customers_table overfl_text_hid outgoing_acts_table fix-thead',
                            'aria-describedby' => 'datatable_ajax_info',
                            'role' => 'grid',
                        ],

                        'headerRowOptions' => [
                            'class' => 'heading',
                        ],

                        'options' => [
                            'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                        ],

                        'pager' => [
                            'options' => [
                                'class' => 'pagination pull-right',
                            ],
                        ],
                        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),

                        'columns' => [
                            [
                                'header' => Html::checkbox('', false, [
                                    'class' => 'joint-operation-main-checkbox',
                                ]),
                                'headerOptions' => [
                                    'class' => 'text-center',
                                    'width' => '5%',
                                ],
                                'contentOptions' => [
                                    'class' => 'text-center',
                                ],
                                'format' => 'raw',
                                'value' => function (Waybill $model) {
                                    return Html::checkbox('Waybill[' . $model->id . '][checked]', false, [
                                        'class' => 'joint-operation-checkbox',
                                    ]);

                                },
                            ],
                            [
                                'attribute' => 'document_date',
                                'label' => 'Дата ТТН',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '10%',
                                ],
                                'format' => 'raw',
                                'value' => function (Waybill $data) {
                                    return DateHelper::format($data->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                                },
                            ],

                            [
                                'attribute' => 'document_number',
                                'label' => '№ ТТН',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '10%',
                                ],
                                'format' => 'raw',
                                'value' => function (Waybill $data) {
                                    return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                                        'model' => $data,
                                    ])
                                        ? Html::a($data->fullNumber, ['view', 'type' => $data->type, 'id' => $data->id])
                                        : $data->fullNumber;
                                },
                            ],
                            [
                                'label' => 'Скан',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '70px',
                                ],
                                'attribute' => 'has_file',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return Yii::$app->view->render('@documents/views/layouts/_doc-file-link', [
                                        'model' => $model,
                                    ]);
                                },
                            ],
                            [
                                'attribute' => 'invoice.total_amount_with_nds',
                                'label' => 'Сумма',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '10%',
                                ],
                                'format' => 'raw',
                                'value' => function (Waybill $data) {
                                    $price = \common\components\TextHelper::invoiceMoneyFormat($data->totalAmountWithNds, 2);
                                    return '<span class="price" data-price="'.str_replace(" ", "", $price).'">'.$price.'</span>';
                                },
                            ],

                            [
                                'attribute' => 'contractor_id',
                                'label' => 'Контрагент',
                                'class' => DropDownSearchDataColumn::className(),
                                'headerOptions' => [
                                    'width' => '30%',
                                    'class' => 'dropdown-filter',
                                ],
                                'filter' => FilterHelper::getContractorList($searchModel->type, Waybill::tableName(), true, false, false),
                                'format' => 'raw',
                                'value' => function (Waybill $data) {
                                    return '<span title="' . htmlspecialchars($data->invoice->contractor_name_short) . '">' . $data->invoice->contractor_name_short . '</span>';
                                },
                            ],

                            [
                                'attribute' => 'status_out_id',
                                'label' => 'Статус',
                                'class' => DropDownDataColumn::className(),
                                'headerOptions' => [
                                    'class' => 'dropdown-filter',
                                    'width' => '10%',
                                ],
                                'filter' => $searchModel->getStatusArray($searchModel->type),
                                'format' => 'raw',
                                'value' => function (Waybill $data) {
                                    return $data->status->name;
                                },
                                'visible' => $ioType == Documents::IO_TYPE_OUT,
                            ],

                            [
                                'attribute' => 'invoice_document_number',
                                'label' => 'Счёт №',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '10%',
                                ],
                                'format' => 'raw',
                                'value' => function (Waybill $data) {
                                    if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $data->invoice,])
                                    ) {
                                        if ($data->invoice->file !== null) {
                                            $invoiceFileLinkClass = null;
                                            $tooltipId = null;
                                            $contentPreview = null;
                                            if (in_array($data->invoice->file->ext, ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf'])) {
                                                $invoiceFileLinkClass = 'invoice-file-link-preview';
                                                $tooltipId = 'invoice-file-link-preview-' . $data->invoice->file->id;
                                                $thumb = $data->invoice->file->getImageThumb(400, 600);
                                                if ($thumb) {
                                                    $contentPreview .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                                                    $contentPreview .= Html::beginTag('span', ['id' => $tooltipId]);
                                                    $contentPreview .= Html::img($thumb, ['alt' => '']);
                                                    $contentPreview .= Html::endTag('span');
                                                    $contentPreview .= Html::endTag('div');
                                                }
                                            }

                                            return Html::a($data->invoice->fullNumber,
                                                ['/documents/invoice/view', 'type' => $data->type, 'id' => $data->invoice->id])
                                            . Html::a('<span class="pull-right icon icon-paper-clip"></span>',
                                                ['/documents/invoice/file-get', 'type' => $data->type, 'id' => $data->invoice->id, 'file-id' => $data->invoice->file->id,],
                                                [
                                                    'class' => $invoiceFileLinkClass,
                                                    'target' => '_blank',
                                                    'data-tooltip-content' => '#' . $tooltipId,
                                                    'style' => 'float: right',
                                                ]) . $contentPreview;
                                        } else {
                                            return Html::a($data->invoice->fullNumber,
                                                ['/documents/invoice/view', 'type' => $data->type, 'id' => $data->invoice->id]);
                                        }
                                    } else {
                                        return $data->invoice->fullNumber;
                                    }
                                },
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->registerJs('
    $(".invoice-file-link-preview").tooltipster({
        theme: ["tooltipster-kub"],
        contentCloning: true,
        trigger: "hover",
        side: "left",
    });
'); ?>

<?=\frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a('<i class="fa fa-print"></i> Печать', [
            'many-document-print',
            'actionType' => 'pdf',
            'type' => $ioType,
            'multiple' => ''
        ], [
            'class' => 'btn btn-sm darkblue text-white multiple-print',
            'target' => '_blank',
        ]) : null,
        $canSend ? Html::a('<i class="glyphicon glyphicon-envelope"></i> Отправить', null, [
            'class' => 'btn btn-sm darkblue text-white document-many-send',
            'data-url' => Url::to(['many-send', 'type' => $ioType]),
        ]) : null,
        $canDelete ? Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
            'class' => 'btn btn-sm darkblue text-white',
            'data-toggle' => 'modal',
        ]) : null,
        $dropItems ? Html::tag('div', Html::a('Еще  <b class="caret"></b>', '#', [
            'id' => 'dropdownMenu2',
            'class' => 'btn btn-sm darkblue text-white dropdown-toggle',
            'data-toggle' => 'dropdown',
        ]) . Dropdown::widget([
            'items' => $dropItems,
            'options' => [
                'style' => 'left: auto; right: 0;'
            ],
        ]), ['class' => 'dropup']) : null,
    ],
]);?>
