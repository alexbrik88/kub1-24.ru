<?php

use yii\helpers\Html;
use common\models\document\status;

/** @var \common\models\document\Waybill $model */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

$canUpdate = $model->status_out_id != status\WaybillStatus::STATUS_REJECTED
    &&
    Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE_STATUS, [
        'model' => $model,
    ]);

$this->registerJs('
    $(document).ready(function () {
        buttonsScroll.init();
    });
');


?>

    <div class="row action-buttons form-action-buttons hide">
        <div class="spinner-button col-sm-1 col-xs-1">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => 'form-action-button btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                'id' => 'save-btn',
                'form' => 'edit-waybill',
                'data-style' => 'expand-right',
            ]); ?>
            <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                'class' => 'form-action-button btn darkblue btn-save darkblue widthe-100 hidden-lg',
                'title' => 'Сохранить',
                'form' => 'edit-waybill',
            ]); ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <button type="button"
                    class="form-action-button btn darkblue btn-cancel darkblue widthe-100 hidden-md hidden-sm hidden-xs">
                Отменить
            </button>
            <button type="button" class="form-action-button btn darkblue btn-cancel darkblue widthe-100 hidden-lg"
                    title="Отменить"><i class="fa fa-reply fa-2x"></i></button>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
    </div>

    <div class="row action-buttons view-action-buttons">
        <div class="button-bottom-page-lg col-sm-1 col-xs-1" title="Отправить по e-mail">
            <?php if (true || $canUpdate): ?>
                <button class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs send-message-panel-trigger">
                    Отправить
                </button>
                <button class="btn darkblue widthe-100 hidden-lg send-message-panel-trigger" title="Отправить">
                    <span class="ico-Send-smart-pls fs"></span></button>
            <?php endif; ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?php if (true || $canUpdate) {
                echo Html::a('Печать', ['document-print', 'actionType' => 'print', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPrintTitle(),], [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    'target' => '_blank',
                ]);
                echo Html::a('<i class="fa fa-print fa-2x"></i>', ['document-print', 'actionType' => 'print', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPrintTitle(),], [
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                    'target' => '_blank',
                    'title' => 'Печать',
                ]);
            } ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?= Html::a('PDF', ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPdfFileName(),], [
                'target' => '_blank',
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
            ]); ?>
            <?= Html::a('<i class="fa fa-file-pdf-o fa-2x"></i>', ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPdfFileName(),], [
                'target' => '_blank',
                'class' => 'btn darkblue widthe-100 hidden-lg',
            ]); ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?php if ($canUpdate && Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE, ['model' => $model,])) {
                echo Html::a(status\WaybillStatus::findOne(status\WaybillStatus::STATUS_SEND)->name,
                    ['update-status', 'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,], [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                        'data' => [
                            'toggle' => 'modal',
                            'method' => 'post',
                            'params' => [
                                '_csrf' => Yii::$app->request->csrfToken,
                                'status' => status\WaybillStatus::STATUS_SEND,
                            ],
                        ],
                    ]);
                echo Html::a('<i class="fa fa-paper-plane-o fa-2x"></i>',
                    ['update-status', 'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,], [
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                        'title' => 'Передана',
                        'data' => [
                            'toggle' => 'modal',
                            'method' => 'post',
                            'params' => [
                                '_csrf' => Yii::$app->request->csrfToken,
                                'status' => status\WaybillStatus::STATUS_SEND,
                            ],
                        ],
                    ]);
            } ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?php if ($canUpdate && Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE, ['model' => $model,])) {
                echo Html::a(status\WaybillStatus::findOne(status\WaybillStatus::STATUS_RECEIVED)->name,
                    ['update-status', 'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,], [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                        'data' => [
                            'toggle' => 'modal',
                            'method' => 'post',
                            'params' => [
                                '_csrf' => Yii::$app->request->csrfToken,
                                'status' => status\WaybillStatus::STATUS_RECEIVED,
                            ],
                        ],
                    ]);
                echo Html::a('<i class="fa fa-pencil-square-o fa-2x"></i>',
                    ['update-status', 'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,], [
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                        'title' => 'Подписана',
                        'data' => [
                            'toggle' => 'modal',
                            'method' => 'post',
                            'params' => [
                                '_csrf' => Yii::$app->request->csrfToken,
                                'status' => status\WaybillStatus::STATUS_RECEIVED,
                            ],
                        ],
                    ]);
            } ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::DELETE)): ?>
                <button type="button" class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs" data-toggle="modal"
                        href="#delete-confirm">Удалить
                </button>
                <button type="button" class="btn darkblue widthe-100 hidden-lg" data-toggle="modal"
                        href="#delete-confirm" title="Удалить"><i class="fa fa-trash-o fa-2x"></i></button>
            <?php endif; ?>
        </div>
    </div>
<?php if ($canUpdate): ?>
    <?= $this->render('/invoice/view/_send_message', [
        'model' => $model,
        'useContractor' => $useContractor,
    ]); ?>
<?php endif; ?>