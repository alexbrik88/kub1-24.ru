<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10/20/15
 * Time: 2:52 PM
 * Email: t.kanstantsin@gmail.com
 */
use common\models\file\File;
use yii\bootstrap\Html;

/* @var \yii\web\View $this */
?>

<div class="file-list">
    <?php foreach (($model->file ? [$model->file] : []) as $file): /* @var File $file */ ?>
        <div class="file <?= $file->isNewRecord ? 'file-template' : ''; ?>"
             data-id="<?= $file->id; ?>"
        >
            <?= Html::a(Html::icon('paperclip') . $file->filename_full, ['file-get', 'type' => $model->type, 'id' => $model->id, 'file-id' => $file->id,], [
                'class' => 'file-link',
                'target' => '_blank',
            ]); ?>

            <?= Html::a(Html::icon('remove'), '#', [
                'class' => 'delete-file text-danger',
            ]); ?>
        </div>
    <?php endforeach; ?>
</div>
