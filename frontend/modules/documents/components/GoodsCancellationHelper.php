<?php

namespace frontend\modules\documents\components;

use common\components\helpers\ArrayHelper;
use common\models\document\GoodsCancellation;
use common\models\document\Invoice;
use common\models\document\NdsViewType;
use common\models\document\OrderGoodsCancellation;
use common\models\product\Product;
use common\models\TaxRate;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use Yii;

class GoodsCancellationHelper
{

    public static function save(GoodsCancellation &$model, $validation = true)
    {
        if ($validation && !self::validate($model)) {
            return false;
        }
        
        $saveClosure = function ($db) use ($model) {
            if (!$model->save(false)) {
                return false;
            }
            foreach ($model->orders as $order) {
                $order->goods_cancellation_id = $model->id;
                if (!$order->save(false)) {
                    return false;
                }
            }
            foreach ($model->removedOrders as $removedOrder) {
                if ($removedOrder->delete() === false) {
                    $model->addError('orders', 'Не удалось удалить позицию.');
                    return false;
                }
            }

            return true;
        };
        
        if ($isSaved = LogHelper::save($model, LogEntityType::TYPE_DOCUMENT,  ($model->isNewRecord) ? LogEvent::LOG_EVENT_CREATE : LogEvent::LOG_EVENT_UPDATE, $saveClosure)) {
            self::afterSave($model->id);
        }

        return (boolean)$isSaved;
    }
    
    public static function afterSave($modelId)
    {
        // todo: добавить списание со складов
    }

    /**
     * @param  GoodsCancellation $model
     * @param  array $data
     * @return boolean
     */
    public static function validate(&$model)
    {
        foreach ($model->orders as $order) {
            if (!$order->validate()) {
                $errors = $order->getFirstErrors();
                $model->addError('orders', reset($errors));
            }
        }

        $model->validate(null, false);

        return !$model->hasErrors();
    }

    public static function load(GoodsCancellation &$model, $data = [], $formName = null)
    {
        if ($model->load($data, $formName)) {

            self::ordersLoad($model, ArrayHelper::getValue($data, 'orderArray', []));

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public static function ordersLoad(GoodsCancellation &$model, $ordersData)
    {
        $orderArray = [];
        $orderDataArray = [];
        $model->removedOrders = $model->getOrders()->indexBy('id')->all();
        $noNds = true;

        if (is_array($ordersData)) {
            foreach ($ordersData as $data) {
                $productId = ArrayHelper::getValue($data, 'product_id');
                $productQuery = Product::find()->andWhere([
                    'id' => $productId,
                    'company_id' => $model->company_id,
                ]);

                $orderQuery = OrderGoodsCancellation::find()->andWhere([
                    'id' => ArrayHelper::getValue($data, 'id'),
                    'goods_cancellation_id' => $model->id,
                    'product_id' => $productId,
                ]);

                if ($order = $orderQuery->one()) {
                    $order->populateRelation('goodsCancellation', $model);
                } elseif ($product = $productQuery->one()) {
                    /** @var Product $product */
                    $product->price_for_buy_nds_id = $data['purchase_tax_rate_id'];
                    $order = self::createOrderByProduct($product, $model, null);
                } else {
                    $order = null;
                }

                if ($order) {

                    $orderArray[] = $order;
                    $orderDataArray[] = $data;
                    if (isset($model->removedOrders[$order->id])) {
                        unset($model->removedOrders[$order->id]);
                    }

                    if (isset($data['purchase_tax_rate_id']) && $data['purchase_tax_rate_id'] != TaxRate::RATE_WITHOUT)
                        $noNds = false;
                }
            }

            if ($orderArray && $noNds) {
                $model->nds_view_type_id = NdsViewType::NDS_VIEW_WITHOUT;
            } else {
                $selectedNds = ArrayHelper::getValue(Yii::$app->request->post('GoodsCancellation'), 'nds_view_type_id');
                $model->nds_view_type_id = ($selectedNds != NdsViewType::NDS_VIEW_WITHOUT) ? $selectedNds : $model->company->nds_view_type_id;
            }

            array_walk($orderArray, function ($order, $key) use ($orderDataArray) {
                $order->load($orderDataArray[$key], '');
                $order->calculateOrder();
                $order->number = $key + 1;
            });
        }

        if (!isset($model->nds_view_type_id)) {
            $model->nds_view_type_id = $model->company->nds_view_type_id ?? NdsViewType::NDS_VIEW_WITHOUT;
        }

        $model->populateRelation('orders', $orderArray);
    }

    public static function createOrderByProduct(Product $product, GoodsCancellation $owner, $count = 1, $price = null, $discount = 0, $markup = 0)
    {
        $order = new OrderGoodsCancellation();
        $order->populateRelation('goodsCancellation', $owner);
        $order->populateRelation('product', $product);
        $order->populateRelation('taxRate', $product->priceForBuyNds);

        $order->tax_rate_id = $product->price_for_buy_nds_id ? $product->price_for_buy_nds_id : TaxRate::RATE_WITHOUT;
        $order->product_id = $product->id;
        $order->product_title = $product->title;
        $order->product_code = $product->code;
        $order->article = $product->article;
        $order->unit_id = $product->product_unit_id;
        $order->country_id = $product->country_origin_id;
        $order->custom_declaration_number = $product->customs_declaration_number;

        if ($price === null) {
            if ($owner->nds_view_type_id == Invoice::NDS_VIEW_OUT) {
                $price = $product->price_for_buy_with_nds / (1 + floatval($product->priceForBuyNds->rate));
            } else {
                $price = $product->price_for_buy_with_nds;
            }
        }

        $order->price = bcdiv($price, 100, 2);
        $order->quantity = $count;
        $order->number = 1;
        $order->discount = $discount;
        $order->markup = $markup;

        $order->calculateOrder();

        return $order;
    }
}
