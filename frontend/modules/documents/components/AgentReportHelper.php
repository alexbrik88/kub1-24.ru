<?php

namespace frontend\modules\documents\components;

use common\components\date\DateHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use common\models\Contractor;
use common\models\contractor\ContractorAgentBuyer;
use common\models\document\AgentReport;
use common\models\document\AgentReportOrder;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\status\AgentReportStatus;
use common\models\document\status\InvoiceStatus;
use common\models\product\Product;
use common\models\product\ProductUnit;
use common\models\TaxRate;
use frontend\models\Documents;
use frontend\modules\export\models\one_c\OneCExport;
use Yii;
use yii\helpers\ArrayHelper;
use common\models\address\Country;

/**
 * AgentReportHelper
 */
class AgentReportHelper
{
    const PRODUCT_TITLE = 'Оплата по отчёту Агента';

    /**
     * @var string
     */
    public static $orderParamName = 'orderArray';

    /**
     * @param  AgentReport     $model
     * @param  array   $data
     * @param  string  $formName
     * @return boolean
     */
    public static function load(AgentReport &$model, $data, $formName = null)
    {
        if (is_array($data) && $model->load($data, $formName)) {
            $ordersData = ArrayHelper::getValue($data, self::$orderParamName);
            if ($ordersData !== null) {
                $model->ordersLoad($ordersData);
            }

            $model->calculateTotals();

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param  AgentReport $model
     * @return boolean
     */
    public static function validate(AgentReport &$model)
    {
        foreach ($model->agentReportOrders as $order) {
            if (!$order->validate()) {
                $model->addErrors($order->getErrors());
            }
        }

        $model->validate(null, false);

        return !$model->hasErrors();
    }

    /**
     * @param AgentReport $model
     * @param boolean $validate
     *
     * @return boolean
     */
    public static function save(AgentReport &$model, $validate = true)
    {
        if ($validate && !static::validate($model)) {
            return false;
        }

        return Yii::$app->db->transaction(function ($db) use ($model) {
            if ($model->save(false)) {
                foreach ($model->removedOrders as $order) {
                    /** @var AgentReportOrder $order */
                    if ($order->updateAttributes(['is_exclude' => true]) === false) {
                        $model->addError('agentReportOrders', "Нельзя удалить «{$order->contractor->getShortName()}»");
                        $db->transaction->rollBack();

                        return false;
                    }
                }
                foreach ($model->agentReportOrders as $order) {
                    if (!$order->save(false)) {
                        $model->addError('agentReportOrders', "Неудалось сохранить «{$order->contractor->getShortName()}»");
                        $db->transaction->rollBack();

                        return false;
                    }
                }

                return true;
            }
            $db->transaction->rollBack();

            return false;
        });
    }

    public static function getReportInvoice(Company $company, Contractor $contractor)
    {

        $invoice = new Invoice([
            'type' => Documents::IO_TYPE_IN,
            'company_id' => $company->id,
            'contractor_id' => $contractor->id,
            'production_type' => Product::PRODUCTION_TYPE_SERVICE,
            'document_number' => (string) Invoice::getNextDocumentNumber($company, Documents::IO_TYPE_IN, null, date(DateHelper::FORMAT_DATE)),
            'document_date' => date(DateHelper::FORMAT_DATE),
            'payment_limit_date' => date(DateHelper::FORMAT_DATE, strtotime('+' . (int)$contractor->seller_payment_delay . ' day')),
            'invoice_expenditure_item_id' => InvoiceExpenditureItem::ITEM_OTHER,
            'object_guid' => OneCExport::generateGUID(),
            'price_precision' => 4,
            'has_discount' => false,
            'nds_view_type_id' => ($contractor->taxation_system == Contractor::WITH_NDS) ? Invoice::NDS_VIEW_IN : Invoice::NDS_VIEW_WITHOUT
        ]);

        return $invoice;
    }


    /**
     * @param AgentReport $model
     * @param bool $saveProduct
     * @return Product|static
     */
    public static function getReportProduct(AgentReport $model, $saveProduct = false)
    {
        $document_date = DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

        $product = Product::findOne([
            'creator_id' => null,
            'company_id' => null,
            'production_type' => Product::PRODUCTION_TYPE_SERVICE,
            'title' => self::PRODUCT_TITLE,
        ]);
        if ($product === null) {
            $product = new Product([
                'creator_id' => null,
                'company_id' => null,
                'production_type' => Product::PRODUCTION_TYPE_SERVICE,
                'title' => self::PRODUCT_TITLE . " №{$model->fullNumber} от {$document_date}",
                'code' => Product::DEFAULT_VALUE,
                'product_unit_id' => ProductUnit::UNIT_COUNT,
                'box_type' => Product::DEFAULT_VALUE,
                'count_in_place' => Product::DEFAULT_VALUE,
                'place_count' => Product::DEFAULT_VALUE,
                'mass_gross' => Product::DEFAULT_VALUE,
                'has_excise' => 0,
                'price_for_buy_nds_id' => ($model->total_nds) ? TaxRate::RATE_20 : TaxRate::RATE_WITHOUT,
                'price_for_buy_with_nds' => $model->total_sum, // stored in kopecks
                'price_for_sell_nds_id' => ($model->total_nds) ? TaxRate::RATE_20 : TaxRate::RATE_WITHOUT,
                'price_for_sell_with_nds' => $model->total_nds, // stored in kopecks
                'country_origin_id' => Country::COUNTRY_WITHOUT,
                'customs_declaration_number' => Product::DEFAULT_VALUE,
                'object_guid' => OneCExport::generateGUID(),
            ]);

            if ($saveProduct) {
                $product->save(false);
            }
        }

        return $product;
    }

    /**
     * @param Contractor $agent
     */
    public static function generateReportsForAgent(Contractor $agent)
    {
        $invoicesDate = [];
        foreach ($agent->agentBuyersInfo as $buyerInfo) {
            $invoicesDate = array_merge(self::getAgentBuyerDateItems($buyerInfo));
        }

        foreach ($invoicesDate as $dateStart => $dateEnd) {
            @list($year, $month, $endDay) = explode('-', $dateEnd);
            if ($year && $month && $endDay) {

                /** @var AgentReport $agentReport */
                $agentReport = AgentReport::find()->where([
                    'company_id' => $agent->company_id,
                    'agent_id' => $agent->id,
                ])->andWhere(['between',
                    'document_date',
                    $year . '-' . $month . '-01',
                    $year . '-' . $month . '-' . $endDay
                ])->one();

                if (!$agentReport)
                {
                    $agentReport = new AgentReport();
                    $agentReport->type = Documents::IO_TYPE_OUT;
                    $agentReport->created_at = time();
                    $agentReport->document_author_id = Yii::$app->user->id;
                    $agentReport->status_out_id = AgentReportStatus::STATUS_CREATED;
                    $agentReport->status_out_author_id = Yii::$app->user->id;
                    $agentReport->status_out_updated_at = time();
                    $agentReport->document_date = $dateEnd;
                    $agentReport->pay_up_date = date(DateHelper::FORMAT_DATE, strtotime('+'.(int)$agent->customer_payment_delay.' days', strtotime($agentReport->document_date)));
                    $agentReport->document_number = self::getNextDocumentNumber($agent);
                    $agentReport->company_id = $agent->company_id;
                    $agentReport->agent_id = $agent->id;
                    $agentReport->agreement_id = $agent->agent_agreement_id;

                    $agentReport->save();
                }

                // Обновление статуса по счету
                if ($invoice = $agentReport->invoice) {
                    $agentReport->setStatusByInvoice();
                }

                foreach ($agent->agentBuyersInfo as $buyerInfo) {

                    if (strtotime($buyerInfo->start_date) > strtotime($dateEnd))
                        continue;

                    /** @var Contractor $buyer */
                    $buyer = $buyerInfo->buyer;

                    $order = AgentReportOrder::findOne([
                        'agent_report_id' => $agentReport->id,
                        'contractor_id' => $buyer->id
                    ]);

                    if ($order) {
                        $order->payments_sum = self::getBuyerMonthIncomeFlowsSum($buyer, $year, $month);
                    } else {
                        $order = new AgentReportOrder();
                        $order->agent_report_id = $agentReport->id;
                        $order->contractor_id = $buyer->id;
                        $order->payments_sum = self::getBuyerMonthIncomeFlowsSum($buyer, $year, $month);
                        $order->agent_percent = $agent->agent_payment_percent;
                    }
                    $agentReport->calculateOrder($order);
                    $order->save();
                }
                $agentReport->calculateTotals();
                $agentReport->save();
            }
        }
    }

    /**
     * @param $buyer
     * @param $year
     * @param $month
     * @return int
     */
    public static function getBuyerMonthIncomeFlowsSum($buyer, $year, $month) {

        return self::getBankSum($buyer, $year, $month)
             + self::getOrderSum($buyer, $year, $month)
             + self::getEmoneySum($buyer, $year, $month);
    }

    public static function getBankSum($buyer, $year, $month)
    {
        $date = new \DateTime();
        $monthFrom = $month;
        $dateFrom = $date->setDate($year, $monthFrom, 1);
        $dateTill = clone $dateFrom;
        $dateTill = $dateTill->modify('last day of ');

        $query = CashBankFlows::find()
            ->alias('cbf')
            ->andWhere([
                'cbf.company_id' => $buyer->company_id,
                'cbf.flow_type' => CashFlowsBase::FLOW_TYPE_INCOME,
                'cbf.contractor_id' => $buyer->id
            ])
            ->andWhere(['between', 'cbf.date', $dateFrom->format('Y-m-d'), $dateTill->format('Y-m-d')]);

        return (int)$query->sum('cbf.amount');
    }

    public static function getOrderSum($buyer, $year, $month)
    {
        $date = new \DateTime();
        $monthFrom = $month;
        $dateFrom = $date->setDate($year, $monthFrom, 1);
        $dateTill = clone $dateFrom;
        $dateTill = $dateTill->modify('last day of ');

        $query = CashOrderFlows::find()
            ->alias('cof')
            ->andWhere([
                'cof.company_id' => $buyer->company_id,
                'cof.flow_type' => CashFlowsBase::FLOW_TYPE_INCOME,
                'cof.is_accounting' => true,
                'cof.contractor_id' => $buyer->id
            ])
            ->andWhere(['between', 'cof.date', $dateFrom->format('Y-m-d'), $dateTill->format('Y-m-d')]);

        return (int)$query->sum('cof.amount');
    }

    public static function getEmoneySum($buyer, $year, $month)
    {
        $date = new \DateTime();
        $monthFrom = $month;
        $dateFrom = $date->setDate($year, $monthFrom, 1);
        $dateTill = clone $dateFrom;
        $dateTill = $dateTill->modify('last day of ');

        $query = CashEmoneyFlows::find()
            ->alias('cef')
            ->andWhere([
                'cef.company_id' => $buyer->company_id,
                'cef.flow_type' => CashFlowsBase::FLOW_TYPE_INCOME,
                'cef.is_accounting' => true,
                'cef.contractor_id' => $buyer->id
            ])
            ->andWhere(['between', 'cef.date', $dateFrom->format('Y-m-d'), $dateTill->format('Y-m-d')]);

        return (int)$query->sum('cef.amount');
    }


    /**
     * @param Contractor $agent
     * @return string
     */
    public static function getNextDocumentNumber(Contractor $agent)
    {
        $maxNumber = AgentReport::find()->andWhere([
            'company_id' => $agent->company->id,
            'agent_id' => $agent->id
        ])->max('document_number * 1');

        return (string)(1 + (int)$maxNumber);
    }

    /**
     * @param ContractorAgentBuyer $buyerInfo
     * @return array
     */
    public static function getAgentBuyerDateItems(ContractorAgentBuyer $buyerInfo)
    {
        $startDate = date('Y-m-01', strtotime($buyerInfo->start_date));
        $currentDate = date('Y-m-01');

        $items = [];
        do {
            $items[$startDate] = date('Y-m-t', strtotime($startDate));
            $startDate = date('Y-m-01', strtotime('+1 month', strtotime($startDate)));
        } while ($startDate <= $currentDate);
        ksort($items);

        return $items;
    }
}
