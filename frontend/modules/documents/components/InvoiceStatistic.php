<?php

namespace frontend\modules\documents\components;


use common\components\date\DateHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\document\status\InvoiceStatus;
use DateTime;
use frontend\models\ContractorSearch;
use frontend\models\Documents;
use frontend\modules\documents\models\InvoiceSearch;
use yii\db\ActiveQuery;
use Yii;
use frontend\components\StatisticPeriod;

class InvoiceStatistic
{
    const NOT_PAID = 'not_paid';
    const NOT_PAID_IN_TIME = 'not_paid_in_time';
    const PAID = 'paid';

    public static $inInvoiceColor = ['#7f0f7e', '#fecb2e', '#136bfb', '#ca0814', '#0f7f12', '#2dfffe', '#E3A5DF',
        '#46487C', '#A6A8CE', '#0D2626', '#1A0D26', '#FFA60A', '#0AFF27', '#E7FF0A', '#323423'];

    public static $donutChartColor = [
        '#4f81bd',
        '#c0504d',
        '#81a55c',
        '#8064a2',
        '#f79646',
        '#95b3d7',
        '#d99694',
        '#c3d69b',
        '#b2a2c7',
        '#b7dde8',
        '#fac08f',
        '#f2c314',
        '#a5a5a5',
        '#366092',
        '#953734',
        '#76923c',
        '#5f497a',
        '#92cddc',
        '#e36c09',
        '#c09100',
        '#7f7f7f',
        '#244061',
        '#632423',
        '#4f6128',
        '#3f3151',
        '#31859b',
        '#974806',
        '#7f6000',
    ];

    public static $invoiceStatuses = [
        self::NOT_PAID => [
            Documents::IO_TYPE_OUT => [
                InvoiceStatus::STATUS_CREATED, InvoiceStatus::STATUS_VIEWED,
                InvoiceStatus::STATUS_APPROVED, InvoiceStatus::STATUS_SEND,
                InvoiceStatus::STATUS_PAYED_PARTIAL, InvoiceStatus::STATUS_OVERDUE
            ],
            Documents::IO_TYPE_IN => [
                InvoiceStatus::STATUS_CREATED,
                InvoiceStatus::STATUS_PAYED_PARTIAL,
                InvoiceStatus::STATUS_OVERDUE
            ],
        ],
        self::NOT_PAID_IN_TIME => [
            Documents::IO_TYPE_OUT => [InvoiceStatus::STATUS_OVERDUE],
            Documents::IO_TYPE_IN => [InvoiceStatus::STATUS_OVERDUE],
        ],
        self::PAID => [
            Documents::IO_TYPE_OUT => [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL],
            Documents::IO_TYPE_IN => [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL],
        ],
    ];


    /**
     * @param $ioType
     * @param $company
     * @param $statuses
     * @param $dateRange
     * @param int $contractorId
     * @param int $contractorStatus
     * @param null $invoiceStatusId
     * @param null $searchModel
     * @param null $currentUser
     * @return array|false
     * @throws \yii\web\NotFoundHttpException
     */
    public static function getStatisticInfo($ioType, $company, $statuses, $dateRange, $contractorId = 0,
                                            $contractorStatus = 0, $invoiceStatusId = null, $searchModel = null,
                                            $currentUser = null, $projectId = null, $responsibleEmployeeId = null, $industryId = null, $salePointId = null)
    {
        if ($company == null) {
            $company = Yii::$app->user->identity->company->id;
        }

        $class = Documents::getModel(Documents::DOCUMENT_INVOICE);

        switch ($statuses) {
            case InvoiceStatistic::NOT_PAID:
                $selectSum = 'SUM(' . $class::tableName() . '.remaining_amount) AS sum';
                break;
            case InvoiceStatistic::PAID:
                $selectSum = 'SUM(' . $class::tableName() . '.payment_partial_amount) AS sum';
                break;
            default:
                $selectSum = 'SUM(' . $class::tableName() . '.total_amount_with_nds) AS sum';
        }

        $query = $class::find()
            ->asArray()
            ->select([
                'COUNT(' . $class::tableName() . '.id) AS count',
                $selectSum,
            ])
            ->byCompany($company)
            ->byIOType($ioType)
            ->byDeleted(false)
            ->byDocumentDateRange($dateRange['from'], $dateRange['to']);


        if (!empty($invoiceStatusId)) {
            $query->andWhere([$class::tableName() . '.invoice_status_id' => $invoiceStatusId]);
        }

        $joinContractor = false;
        if (!empty($contractorStatus)) {
            $joinContractor = true;
            $query->andWhere([
                Contractor::tableName() . '.status' => ContractorSearch::$activityStatusToStatus[(int)$contractorStatus],
            ]);
        }

        if ($currentUser == null) {
            $joinContractor = true;
            if (!Yii::$app->user->can(\frontend\rbac\permissions\document\Document::INDEX_STRANGERS)) {
                $query->andWhere([
                    'or',
                    [$class::tableName() . '.document_author_id' => Yii::$app->user->id],
                    [Contractor::tableName() . '.responsible_employee_id' => Yii::$app->user->id],
                ]);
            }
        }

        if ($joinContractor) {
            $query->joinWith(['contractor'], false);
        }

        if ($searchModel instanceof InvoiceSearch) {

            if ($searchModel->invoceFactureState != InvoiceSearch::INVOICE_FACTURE_ALL) {
                $query->leftJoin(
                    ['IF_table' => InvoiceFacture::find()->select(['id', 'invoice_id'])->groupBy('invoice_id')],
                    "{{IF_table}}.[[invoice_id]] = {{{$class::tableName()}}}.[[id]]"
                );
                if ($searchModel->invoceFactureState == InvoiceSearch::INVOICE_FACTURE_NOT_EXISTS) {
                    $query->andWhere(['IF_table.id' => null]);
                } elseif ($searchModel->invoceFactureState == InvoiceSearch::INVOICE_FACTURE_EXISTS) {
                    $query->andWhere(['not', ['IF_table.id' => null]]);
                }
            }

            InvoiceSearch::searchByDocFilter($searchModel, $query);
            InvoiceSearch::searchByRoleFilter($query);

            $query->andFilterWhere([
                $class::tableName() . '.document_author_id' => $searchModel->document_author_id,
            ]);
        }

        if (!empty($contractorId)) {
            if ($contractorId !== InvoiceSearch::CONTRACTOR_DUPLICATE) {
                $query->andFilterWhere([$class::tableName() . '.contractor_id' => $contractorId,]);
            } else {
                $cloneQuery = clone $query;
                $contractorIDs = $cloneQuery->select('contractor_id')->groupBy('contractor_id')->column();
                if ($contractorIDs) {
                    $contractorIDsString = implode(',', $contractorIDs);
                    $contractorIDs = Contractor::find()
                        ->andWhere(['id' => $contractorIDs])
                        ->andWhere("(`company_type_id`, `ITN`, `PPC`) IN (SELECT `company_type_id`, `ITN`, `PPC`
                    FROM `contractor`
                    WHERE `id` IN ({$contractorIDsString})
                    GROUP BY `company_type_id`, `ITN`, `PPC`
                    HAVING COUNT(`id`) > 1)")
                        ->column();
                    $query->andWhere([$class::tableName() . '.contractor_id' => $contractorIDs,]);
                }
            }
        }

        if (!empty($responsibleEmployeeId)) {
            $query->andWhere([$class::tableName() . '.responsible_employee_id' => $responsibleEmployeeId]);
        }

        if (strlen($projectId)) {
            $query->andWhere([$class::tableName() . '.project_id' => $projectId ?: null]);
        }
        if (strlen($industryId)) {
            $query->andWhere([$class::tableName() . '.industry_id' => $industryId ?: null]);
        }
        if (strlen($salePointId)) {
            $query->andWhere([$class::tableName() . '.sale_point_id' => $salePointId ?: null]);
        }

        self::_setStatuses($query, $statuses, $ioType);

        return $query->createCommand()->queryOne();
    }

    /**
     * @param $searchModel
     * @return array
     */
    public static function getForeignCurrencyStatisticInfo($searchQuery, $ioType)
    {
        $default = [
            'count' => 0,
            'sum' => 0,
        ];
        $fromTable = $searchQuery->getTableAlias();
        $query1 = (clone $searchQuery)->select([
            'currency_name',
            'count' => "COUNT({{{$fromTable}}}.[[id]])",
        ])->groupBy('currency_name')->asArray();
        $query2 = (clone $searchQuery)->select([
            'currency_name',
            'count' => "COUNT({{{$fromTable}}}.[[id]])",
        ])->groupBy('currency_name')->asArray();
        $query3 = (clone $searchQuery)->select([
            'currency_name',
            'count' => "COUNT({{{$fromTable}}}.[[id]])",
        ])->groupBy('currency_name')->asArray();

        $query1->addSelect([
            'sum' => "IFNULL(SUM({{{$fromTable}}}.[[remaining_amount]]), 0)",
        ])->andWhere([
            'invoice_status_id' => self::$invoiceStatuses[self::NOT_PAID][$ioType],
        ]);
        $query2->addSelect([
            'sum' => "IFNULL(SUM({{{$fromTable}}}.[[remaining_amount]]), 0)",
        ])->andWhere([
            'invoice_status_id' => self::$invoiceStatuses[self::NOT_PAID_IN_TIME][$ioType],
        ]);
        $query3->addSelect([
            'sum' => "IFNULL(SUM({{{$fromTable}}}.[[paid_amount]]), 0)",
        ])->andWhere([
            'or',
            ['invoice_status_id' => self::$invoiceStatuses[self::PAID][$ioType]],
            ['and',
                ['invoice_status_id' => InvoiceStatus::STATUS_OVERDUE],
                ['>', 'paid_amount', 0],
            ],
        ]);

        return [
            array_merge($default, $query1->createCommand()->queryOne() ?: []),
            array_merge($default, $query2->createCommand()->queryOne() ?: []),
            array_merge($default, $query3->createCommand()->queryOne() ?: []),
        ];
    }

    /**
     * @param $type
     * @param $company
     * @param $dateRange
     *
     * @param null $statusArray
     * @return array
     */
    public static function getCostsStatistics($type, $company, $dateRange, $statusArray = null)
    {
        $query = (new \yii\db\Query())
            ->select([
                'SUM(' . Invoice::tableName() . '.total_amount_with_nds) as sum',
                Invoice::tableName() . '.invoice_expenditure_item_id',
                InvoiceExpenditureItem::tableName() . '.name',
            ])
            ->from(Invoice::tableName())
            ->innerJoin(InvoiceExpenditureItem::tableName(),
                InvoiceExpenditureItem::tableName() . '.id = ' . Invoice::tableName() . '.invoice_expenditure_item_id'
            )
            ->where(['AND',
                [Invoice::tableName() . '.company_id' => $company],
                [Invoice::tableName() . '.type' => $type],
                ['between', Invoice::tableName() . '.document_date', $dateRange['from'], $dateRange['to']],
                ['not', [Invoice::tableName() . '.invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE]],
            ]);

        if ($statusArray !== null) {
            $query->andWhere([
                'in', 'invoice_status_id', (array)$statusArray,
            ]);
        }

        return $query->groupBy(Invoice::tableName() . '.invoice_expenditure_item_id')
            ->orderBy(Invoice::tableName() . '.invoice_expenditure_item_id')
            ->all();
    }

    /**
     * @param $type
     * @param $company
     *
     * @param null $datePeriod
     * @param null $statusArray
     * @return array|bool
     */
    public static function getTotalTypePrices($type, $company, $datePeriod = null, $statusArray = null)
    {
        $query = Invoice::find()
            ->byCompany($company)
            ->byIOType($type);

        if ($datePeriod !== null && isset($datePeriod['from'], $datePeriod['to'])) {
            $query->byDocumentDateRange($datePeriod['from'], $datePeriod['to']);
        }

        if ($statusArray !== null) {
            $query->andWhere([
                'in', 'invoice_status_id', (array)$statusArray,
            ]);
        }

        return $query->sum('total_amount_with_nds');
    }

    public static function getPaidSum()
    {
        $paidSum = 0;

        $datePeriod = StatisticPeriod::getSessionPeriod();

        $query = Invoice::find()->where(['AND',
            ['company_id' => Yii::$app->user->identity->company->id],
            ['type' => 1],
            ['between', 'document_date', $datePeriod['from'], $datePeriod['to']],
            ['invoice_status_id' => [1, 6, 8]],
        ])->all();

        for ($i = 0; $i < count($query); $i++) {
            $invoice = new \common\models\document\Invoice($query[$i]);
            $paidSum += $invoice->getPaidAmount();
        }

        return $paidSum;
    }

    /**
     * @param ActiveQuery $query
     * @param array|integer $statuses
     * @param $ioType
     */
    private static function _setStatuses(ActiveQuery $query, $statuses, $ioType)
    {
        if (is_array($statuses)) {
            $query->andWhere(['in', 'invoice_status_id', $statuses,]);
        } elseif (isset(self::$invoiceStatuses[$statuses])) {
            if ($statuses == InvoiceStatistic::PAID) {
                $query->andWhere(['or',
                    ['in', 'invoice_status_id', self::$invoiceStatuses[$statuses][$ioType]],
                    ['and',
                        ['invoice_status_id' => InvoiceStatus::STATUS_OVERDUE],
                        ['not', ['payment_partial_amount' => null]],
                        ['>', 'payment_partial_amount', 0],
                    ],
                ]);
            } else {
                $query->andWhere(['in', 'invoice_status_id', self::$invoiceStatuses[$statuses][$ioType]]);
            }
        } else {
            throw new \BadFunctionCallException('Статусы не найдены.');
        }
    }

    /**
     * @param DateTime $dateFrom
     * @param DateTime $dateTo
     * @param Company|integer $company
     */
    public static function getCountPaidByDayForPeriod(DateTime $dateFrom, DateTime $dateTo, $company)
    {
        $dateFrom = $dateFrom->setTime(0, 0)->getTimestamp();
        $dateTo = $dateTo->setTime(23, 59, 59)->getTimestamp();

        if ($company instanceof Company) {
            $company = $company->id;
        }

        return Invoice::find()
            ->select('COUNT(*) AS [[invoice_count]], DATE(FROM_UNIXTIME({{invoice}}.[[invoice_status_updated_at]])) AS [[paid_date]]')
            ->byCompany($company)
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byDeleted(false)
            ->byStatus([InvoiceStatus::STATUS_PAYED_PARTIAL, InvoiceStatus::STATUS_PAYED])
            ->andWhere(['between', 'invoice.invoice_status_updated_at', $dateFrom, $dateTo])
            ->groupBy('DATE(FROM_UNIXTIME({{invoice}}.[[invoice_status_updated_at]]))')
            ->indexBy('paid_date')
            ->column();
    }
}