<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.02.2018
 * Time: 7:54
 */

namespace frontend\modules\documents\components;


use common\models\document\OrderDocument;
use common\models\document\OrderDocumentProduct;
use common\models\product\Product;
use frontend\models\Documents;

/**
 * Class OrderDocumentProductHelper
 * @package frontend\modules\documents\components
 */
class OrderDocumentProductHelper
{
    /**
     * @param Product $product
     * @param OrderDocument $orderDocument
     * @param $count
     * @param null $priceOneWithVat
     * @param int $discount
     * @param int $markup
     * @return OrderDocumentProduct
     */
    public static function createOrderByProduct(Product $product, OrderDocument $orderDocument, $count, $priceOneWithVat = null, $discount = 0, $markup = 0, $taxRateId = null)
    {
        $order = new OrderDocumentProduct();
        $order->populateRelation('orderDocument', $orderDocument);
        $order->populateRelation('product', $product);
        $order->populateRelation('purchaseTaxRate', $product->priceForBuyNds);
        //$order->populateRelation('saleTaxRate', $product->priceForSellNds);

        $order->quantity = $count;

        $order->number = 1;
        $order->product_id = $product->id;
        $order->product_title = $product->title;
        $order->product_code = $product->code;
        $order->box_type = $product->box_type;
        $order->unit_id = $product->product_unit_id;
        $order->place_count = $product->place_count;
        $order->count_in_place = $product->count_in_place;
        $order->mass_gross = $product->mass_gross;
        $order->excise = $product->has_excise;
        $order->excise_price = $product->excise;
        $order->country_id = $product->country_origin_id;
        $order->custom_declaration_number = $product->customs_declaration_number;
        $order->discount = $discount;
        $order->markup = $markup;

        $order->calculateOrderPrice(
            $priceOneWithVat != null ? $priceOneWithVat : ($orderDocument->type == Documents::IO_TYPE_OUT
                ? $product->price_for_sell_with_nds
                : $product->price_for_buy_with_nds),
            ($orderDocument->type == Documents::IO_TYPE_OUT ? $product->price_for_sell_nds_id : $product->price_for_buy_nds_id)
        );

        $order->purchase_tax_rate_id = $taxRateId ?? ($product->price_for_buy_nds_id ? $product->price_for_buy_nds_id : 4);
        $order->sale_tax_rate_id = $taxRateId ?? ($product->price_for_sell_nds_id ? $product->price_for_sell_nds_id : 4);

        $order->calculateAmount();

        return $order;
    }
}