<?php

namespace frontend\modules\documents\components;


use frontend\models\Documents;
use yii\base\Exception;

class Message
{
    const TITLE_PLURAL = 1;
    const TITLE_SINGLE = 2;
    const TITLE_SHORT_SINGLE = 3;
    const CONTRACTOR = 4;
    const DOCUMENT_CREATE = 5;

    public static $message = [
        Documents::DOCUMENT_ACT => [
            Documents::IO_TYPE_OUT => [
                self::TITLE_PLURAL => 'Исходящие акты',
                self::TITLE_SINGLE => 'Исходящий акт',
                self::TITLE_SHORT_SINGLE => 'Исходящий акт',
                self::CONTRACTOR => 'Покупатель',
            ],
            Documents::IO_TYPE_IN => [
                self::TITLE_PLURAL => 'Входящие акты',
                self::TITLE_SINGLE => 'Входящий акт',
                self::TITLE_SHORT_SINGLE => 'Входящий акт',
                self::CONTRACTOR => 'Поставщик',
            ],
        ],
        Documents::DOCUMENT_FOREIGN_CURRENCY_INVOICE => [
            Documents::IO_TYPE_OUT => [
                self::TITLE_PLURAL => 'Исходящие инвойсы',
                self::TITLE_SINGLE => 'Исходящий инвойс',
                self::TITLE_SHORT_SINGLE => 'Исходящий инвойс',
                self::CONTRACTOR => 'Покупатель',
                self::DOCUMENT_CREATE => 'ДОБАВИТЬ',
            ],
            Documents::IO_TYPE_IN => [
                self::TITLE_PLURAL => 'Входящие инвойсы',
                self::TITLE_SINGLE => 'Входящий инвойс',
                self::TITLE_SHORT_SINGLE => 'Исходящий инвойс',
                self::CONTRACTOR => 'Поставщик',
                self::DOCUMENT_CREATE => 'ЗАГРУЗИТЬ',
            ],
        ],
        Documents::DOCUMENT_INVOICE => [
            Documents::IO_TYPE_OUT => [
                self::TITLE_PLURAL => 'Исходящие счета',
                self::TITLE_SINGLE => 'Исходящий счет',
                self::TITLE_SHORT_SINGLE => 'Исходящий счет',
                self::CONTRACTOR => 'Покупатель',
                self::DOCUMENT_CREATE => 'ДОБАВИТЬ',
            ],
            Documents::IO_TYPE_IN => [
                self::TITLE_PLURAL => 'Входящие счета',
                self::TITLE_SINGLE => 'Входящий счет',
                self::TITLE_SHORT_SINGLE => 'Исходящий счет',
                self::CONTRACTOR => 'Поставщик',
                self::DOCUMENT_CREATE => 'ЗАГРУЗИТЬ',
            ],
        ],
        Documents::DOCUMENT_INVOICE_FACTURE => [
            Documents::IO_TYPE_OUT => [
                self::TITLE_PLURAL => 'Исходящие счета-фактуры',
                self::TITLE_SINGLE => 'Исходящая счет-фактура',
                self::TITLE_SHORT_SINGLE => 'Исходящая СФ',
                self::CONTRACTOR => 'Покупатель',
            ],
            Documents::IO_TYPE_IN => [
                self::TITLE_PLURAL => 'Входящие счета-фактуры',
                self::TITLE_SINGLE => 'Входящая счет-фактура',
                self::TITLE_SHORT_SINGLE => 'Входящая СФ',
                self::CONTRACTOR => 'Поставщик',
            ],
        ],
        Documents::DOCUMENT_PACKING_LIST => [
            Documents::IO_TYPE_OUT => [
                self::TITLE_PLURAL => 'Исходящие товарные накладные',
                self::TITLE_SINGLE => 'Исходящая товарная накладная',
                self::TITLE_SHORT_SINGLE => 'Исходящая ТН',
                self::CONTRACTOR => 'Покупатель',
            ],
            Documents::IO_TYPE_IN => [
                self::TITLE_PLURAL => 'Входящие товарные накладные',
                self::TITLE_SINGLE => 'Входящая товарная накладная',
                self::TITLE_SHORT_SINGLE => 'Входящая ТН',
                self::CONTRACTOR => 'Поставщик',
            ],
        ],
        Documents::DOCUMENT_SALES_INVOICE => [
            Documents::IO_TYPE_OUT => [
                self::TITLE_PLURAL => 'Расходные накладные',
                self::TITLE_SINGLE => 'Расходная накладная',
                self::TITLE_SHORT_SINGLE => 'Расходная накладная',
                self::CONTRACTOR => 'Покупатель',
            ],
            Documents::IO_TYPE_IN => [
                self::TITLE_PLURAL => 'Расходные накладные',
                self::TITLE_SINGLE => 'Расходная накладная',
                self::TITLE_SHORT_SINGLE => 'Расходная накладная',
                self::CONTRACTOR => 'Покупатель',
            ],
        ],
        Documents::DOCUMENT_UPD => [
            Documents::IO_TYPE_OUT => [
                self::TITLE_PLURAL => 'Исходящие универсальные передаточные документы',
                self::TITLE_SINGLE => 'Исходящий универсальный передаточный документ',
                self::TITLE_SHORT_SINGLE => 'Исходящий УПД',
                self::CONTRACTOR => 'Покупатель',
            ],
            Documents::IO_TYPE_IN => [
                self::TITLE_PLURAL => 'Входящие универсальные передаточные документы',
                self::TITLE_SINGLE => 'Входящий универсальный передаточный документ',
                self::TITLE_SHORT_SINGLE => 'Входящий УПД',
                self::CONTRACTOR => 'Поставщик',
            ],
        ],
        Documents::DOCUMENT_AUTOINVOICE => [
            Documents::IO_TYPE_OUT => [
                self::TITLE_PLURAL => 'Шаблоны АвтоСчетов',
                self::TITLE_SINGLE => 'Шаблон АвтоСчета',
                self::TITLE_SHORT_SINGLE => 'Шаблон АвтоСчета',
                self::CONTRACTOR => 'Покупатель',
            ],
        ],
        Documents::DOCUMENT_WAYBILL => [
            Documents::IO_TYPE_OUT => [
                self::TITLE_PLURAL => 'Исходящие товарно-транспортные накладные',
                self::TITLE_SINGLE => 'Исходящая товарно-транспортная накладная',
                self::TITLE_SHORT_SINGLE => 'Исходящая ТТН',
                self::CONTRACTOR => 'Покупатель',
            ],
            Documents::IO_TYPE_IN => [
                self::TITLE_PLURAL => 'Входящие товарно-транспортные накладные',
                self::TITLE_SINGLE => 'Входящая товарно-транспортная накладная',
                self::TITLE_SHORT_SINGLE => 'Входящая ТТН',
                self::CONTRACTOR => 'Поставщик',
            ],
        ],
        Documents::DOCUMENT_PROXY => [
            Documents::IO_TYPE_OUT => [
                self::TITLE_PLURAL => 'Доверенности',
                self::TITLE_SINGLE => 'Доверенность',
                self::TITLE_SHORT_SINGLE => 'Доверенность',
                self::CONTRACTOR => 'Покупатель',
            ],
            Documents::IO_TYPE_IN => [
                self::TITLE_PLURAL => 'Доверенности',
                self::TITLE_SINGLE => 'Доверенность',
                self::TITLE_SHORT_SINGLE => 'Доверенность',
                self::CONTRACTOR => 'Поставщик',
            ],
        ],
        Documents::DOCUMENT_AGENT_REPORT => [
            Documents::IO_TYPE_OUT => [
                self::TITLE_PLURAL => 'Отчеты агента',
                self::TITLE_SINGLE => 'Отчет агента',
                self::TITLE_SHORT_SINGLE => 'Отчет агента',
                self::CONTRACTOR => 'Агент',
            ],
            Documents::IO_TYPE_IN => [
                self::TITLE_PLURAL => 'Отчеты агента',
                self::TITLE_SINGLE => 'Отчет агента',
                self::TITLE_SHORT_SINGLE => 'Отчет агента',
                self::CONTRACTOR => 'Принципал',
            ],
        ],
        Documents::DOCUMENT_GOODS_CANCELLATION => [
            self::TITLE_PLURAL => 'Списание товара',
            self::TITLE_SINGLE => 'Списание товара',
            self::TITLE_SHORT_SINGLE => 'Списание товара',
            self::CONTRACTOR => 'Покупатель',
        ],
    ];

    public static $messageAttachTo = [
        Documents::IO_TYPE_IN => [
            Documents::DOCUMENT_ACT => 'Акту',
            Documents::DOCUMENT_INVOICE => 'Счету',
            Documents::DOCUMENT_INVOICE_FACTURE => 'СФ',
            Documents::DOCUMENT_PACKING_LIST => 'ТН',
            Documents::DOCUMENT_UPD => 'УПД',
            Documents::DOCUMENT_AGREEMENT => 'Договору'
        ],
        Documents::IO_TYPE_OUT => [
            Documents::DOCUMENT_ACT => 'Исходящему Акту',
            Documents::DOCUMENT_INVOICE => 'Исходящему Счету',
            Documents::DOCUMENT_INVOICE_FACTURE => 'Исходящей СФ',
            Documents::DOCUMENT_PACKING_LIST => 'Исходящей ТН',
            Documents::DOCUMENT_UPD => 'Исходящему УПД',
            Documents::DOCUMENT_AGREEMENT => 'Исходящему Договору'
        ],
        Documents::DOCUMENT_WAYBILL => [
            Documents::IO_TYPE_OUT => [
                self::TITLE_PLURAL => 'Исходящие товарно-транспортные накладные',
                self::TITLE_SINGLE => 'Исходящая товарно-транспортная накладная',
                self::TITLE_SHORT_SINGLE => 'Исходящая ТТН',
                self::CONTRACTOR => 'Покупатель',
            ],
            Documents::IO_TYPE_IN => [
                self::TITLE_PLURAL => 'Входящие товарно-транспортные накладные',
                self::TITLE_SINGLE => 'Входящая товарно-транспортная накладная',
                self::TITLE_SHORT_SINGLE => 'Входящая ТТН',
                self::CONTRACTOR => 'Поставщик',
            ],
        ],
    ];

    public $documentType;
    public $ioType;

    public function __construct($documentType, $ioType)
    {
        $this->documentType = $documentType;
        $this->ioType = $ioType;
    }

    public function get($messageConst)
    {
        if ($this->ioType) {
            if (isset(self::$message[$this->documentType][$this->ioType][$messageConst])) {
                return self::$message[$this->documentType][$this->ioType][$messageConst];
            }
        } else {
            if (isset(self::$message[$this->documentType][$messageConst])) {
                return self::$message[$this->documentType][$messageConst];
            }
        }

        throw new Exception('Message not found');
    }
}
