<?php

namespace frontend\modules\documents\components;

use common\components\date\DateHelper;
use common\models\Agreement;
use common\models\Company;
use common\models\Contractor;
use common\models\document\AbstractDocument;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\document\ScanDocument;
use common\models\document\ScanDocumentFile;
use common\models\document\Upd;
use common\models\file\File;
use common\models\file\FileDir;
use common\models\product\Product;
use common\models\product\ProductUnit;
use frontend\models\Documents;
use frontend\modules\documents\models\SpecificDocument;
use Yii;
use yii\base\Exception;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * UploadManagerHelper
 */
class UploadManagerHelper
{
    public static $fileIcons = ['bmp', 'doc', 'docx', 'gif', 'jpg', 'jpeg', 'pdf', 'png', 'rtf', 'tiff', 'txt', 'xls', 'xlsx'];

    public static $_attachMessage;

    public static function getFileIcon($fileArr)
    {
        if ($fileArr['ext'] && in_array($fileArr['ext'], self::$fileIcons))
            return  "<img width='20' src='/img/file_type/{$fileArr['ext']}2.svg'/>";

        return "<span class='icon icon-paper-clip'></span>";
    }

    /**
     * @param $fileArr
     * @return array|string
     */
    public static function getImgPreviewUrl($fileArr)
    {
        if ($fileArr['owner_model'] && class_exists($fileArr['owner_model'])) {
            try {
                $model = $fileArr['owner_model']::findOne(['id' => $fileArr['owner_id']]);
            } catch (Exception $e) {
                return '';
            }

            if ($model instanceof ScanDocument) {

                return $url = [
                    '/documents/scan-document/file',
                    'id' => $model->id,
                    'name' => $model->file->filename_full,
                ];

            } else {

                $url = ['/documents/' . str_replace('_', '-', $fileArr['owner_table']) . '/file-get',
                        'id' => $fileArr['owner_id'],
                        'file-id' => $fileArr['id'],
                        'filename' => $fileArr['filename_full'],
                        'inline' => 1,
                        'pdf' => ($fileArr['ext'] == 'pdf') ? 1 : 0
                    ];

                if (isset($model->type))
                    $url['type'] = $model->type;
            }

            return $url;
        }

        return '';
    }

    /**
     * @param File|array $file
     * @return null
     */
    public static function getAttachedDocument($file)
    {
        $owner_model = (is_array($file)) ? $file['owner_model'] : $file->owner_model;
        $owner_id = (is_array($file)) ? $file['owner_id'] : $file->owner_id;

        $model = null;
        if ($owner_model && class_exists($owner_model)) {
            try {
                $model = $owner_model::findOne(['id' => $owner_id]);
            } catch (Exception $e) {
                $model = null;
            }

            if ($model instanceof ScanDocument) {
                if ($model->owner_model) {
                    try {
                        $model = $model->owner_model::findOne(['id' => $model->owner_id]);
                    } catch (Exception $e) {
                        $model = null;
                    }
                } else {
                    $model = null;
                }
            }
        }

        return $model;
    }

    /**
     * @param File|array $file
     * @return null
     */
    public static function getScanDocument($file)
    {
        if (!$file)
            return false;

        $owner_model = (is_array($file)) ? $file['owner_model'] : $file->owner_model;
        $owner_id = (is_array($file)) ? $file['owner_id'] : $file->owner_id;

        $model = null;
        if ($owner_model && $owner_model == ScanDocument::class) {
            return $owner_model::findOne(['id' => $owner_id]);
        }

        return false;
    }

    public static function getAttachFilesMessage() {
        return self::$_attachMessage;
    }

    /**
     * @param $document
     * @param array $filesIds
     * @return string
     */
    public static function setAttachFilesMessage($document, $filesIds = []) {
        $company = Yii::$app->user->identity->company;

        if (empty($filesIds)) {
            $filesIds = Yii::$app->request->get('files');
        }

        $allowedDirsIds = ArrayHelper::getColumn(FileDir::getEmployeeAllowedDirs(), 'id');

        $filenames = File::find()->where([
            'company_id' => $company->id,
            'id' => $filesIds,
        ])->andWhere(['or', ['directory_id' => $allowedDirsIds], ['created_at_author_id' => $document->document_author_id]])
          ->select('filename_full')->column();

        if (count($filenames) == 0)
            return 'Файл(ы) не прикреплены';

        $docDate = isset($document->document_date) ? DateHelper::format($document->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) : '';

        switch (true) {
            case $document instanceof Invoice:
                $attachedToDoc = 'Счету №' . $document->getFullNumber() . ' от ' . $docDate;
                $contractor = $document->contractor;
                break;
            case $document instanceof Act:
                $attachedToDoc = 'Акту №' . $document->getFullNumber() . ' от ' . $docDate;
                $contractor = $document->invoice->contractor;
                break;
            case $document instanceof PackingList:
                $attachedToDoc = 'ТН №' . $document->getFullNumber() . ' от ' . $docDate;
                $contractor = $document->invoice->contractor;
                break;
            case $document instanceof InvoiceFacture:
                $attachedToDoc = 'СФ №' . $document->getFullNumber() . ' от ' . $docDate;
                $contractor = $document->invoice->contractor;
                break;
            case $document instanceof Upd:
                $attachedToDoc = 'УПД №' . $document->getFullNumber() . ' от ' . $docDate;
                $contractor = $document->invoice->contractor;
                break;
            case $document instanceof Agreement:
                $attachedToDoc = 'Договору №' . $document->getFullNumber() . ' от ' . $docDate;
                $contractor = $document->contractor;
                break;
            default:
                $attachedToDoc = '';
                $contractor = null;
                break;
        }

        $contractorName = ($contractor) ? $contractor->getShortName() : '';
        self::$_attachMessage = 'Файл ' . implode(', ', $filenames) . '<br/>' .'прикреплен к ' . $attachedToDoc . '<br/>' . $contractorName;
    }

    /**
     * @param AbstractDocument $document
     * @param array $filesIds
     * @return integer
     */
    public static function attachFiles($document, $filesIds = [])
    {
        $company = Yii::$app->user->identity->company;

        if (empty($filesIds)) {
            $filesIds = Yii::$app->request->get('files');
        }

        $allowedDirsIds = ArrayHelper::getColumn(FileDir::getEmployeeAllowedDirs(), 'id');

        $files = File::find()->where([
            'company_id' => $company->id,
            'id' => $filesIds,
        ])
            ->andWhere(['or', ['directory_id' => $allowedDirsIds], ['created_at_author_id' => $document->document_author_id]])
            ->all();

        /** @var File $file */
        $count = 0;
        foreach ($files as $file) {

            /** @var AbstractDocument $oldDocument */
            $oldDocument = self::getAttachedDocument($file);

            if ($file->owner_model == ScanDocument::className()) {
                $scan = ScanDocument::findOne(['id' => $file->owner_id]);
                $scan->owner_model = $document::className();
                $scan->owner_table = $document::tableName();
                $scan->owner_id = $document->id;
                $scan->updateAttributes(['owner_model', 'owner_id', 'owner_table']);
                $count++;
            } else {
                $file->owner_model = $document::className();
                $file->owner_table = $document::tableName();
                $file->owner_id = $document->id;
                $file->updateAttributes(['owner_model', 'owner_id', 'owner_table']);
                $count++;
            }

            if ($oldDocument) {
                $oldDocument->updateHasFile();
            }
        }

        return $count;
    }

    /**
     * @param File $file
     * @return bool
     */
    public static function moveFileToTrash($file)
    {
        /** @var AbstractDocument $oldDocument */
        $oldDocument = self::getAttachedDocument($file);

        if ($file->owner_model == ScanDocument::className()) {
            $scan = ScanDocument::findOne(['id' => $file->owner_id]);
            $scan->owner_model = '';
            $scan->owner_id = '';
            $scan->owner_table = '';
            $scan->updateAttributes(['owner_model', 'owner_id', 'owner_table']);
        } else {
            $file->owner_model = '';
            $file->owner_id = '';
            $file->owner_table = '';
            $file->updateAttributes(['owner_model', 'owner_id', 'owner_table']);
        }

        $file->updateAttributes([
            'directory_id' => null,
            'is_deleted' => 1
        ]);

        if ($oldDocument)
            $oldDocument->updateHasFile();

        return true;
    }

    /**
     * @param $bytes
     * @return string
     */
    public static function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 0) . ' KB';
        }
        elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        }
        else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    /**
     * @param $company
     * @return array
     */
    public static function getDocumentTypesList($company)
    {
        $docTypesList = [
            'К документу от Поставщика' => [
                Documents::IO_TYPE_IN .'-'. Documents::DOCUMENT_INVOICE => 'Счет',
                Documents::IO_TYPE_IN .'-'. Documents::DOCUMENT_ACT => 'Акт',
                Documents::IO_TYPE_IN .'-'. Documents::DOCUMENT_PACKING_LIST => 'Тов. накладная',
                Documents::IO_TYPE_IN .'-'. Documents::DOCUMENT_UPD => 'УПД',
                Documents::IO_TYPE_IN .'-'. Documents::DOCUMENT_AGREEMENT => 'Договор',
            ],
            'К документу от Покупателя' => [
                Documents::IO_TYPE_OUT .'-'. Documents::DOCUMENT_INVOICE => 'Счет',
                Documents::IO_TYPE_OUT .'-'. Documents::DOCUMENT_ACT => 'Акт',
                Documents::IO_TYPE_OUT .'-'. Documents::DOCUMENT_PACKING_LIST => 'Тов. накладная',
                Documents::IO_TYPE_OUT .'-'. Documents::DOCUMENT_UPD => 'УПД',
                Documents::IO_TYPE_OUT .'-'. Documents::DOCUMENT_AGREEMENT => 'Договор',
            ]
        ];
        if (!$company->companyTaxationType->usn) {
            $docTypesList['К документу от Поставщика'][Documents::IO_TYPE_IN .'-'. Documents::DOCUMENT_INVOICE_FACTURE] = 'Счет фактура';
            $docTypesList['К документу от Покупателя'][Documents::IO_TYPE_OUT .'-'. Documents::DOCUMENT_INVOICE_FACTURE]  = 'Счет фактура';
        }

        return $docTypesList;
    }


    /**
     * @param $type
     * @param $ITN
     * @param $PPC
     * @param null $company
     * @return int|null
     */
    public static function findContractorId($type, $ITN, $PPC, $company = null)
    {
        if (!$company)
            $company = Yii::$app->user->identity->company;

        $query = Contractor::find()
            ->select(['id'])
            ->byCompany($company->id)
            ->byContractor($type)
            ->byIsDeleted(Contractor::NOT_DELETED)
            ->byStatus(Contractor::ACTIVE)
            ->andWhere(['ITN' => $ITN]);

        $contractors = (clone $query)->asArray()->column();

        // find by inn
        if (count($contractors) == 1) {
            return (int)$contractors[0];
        }

        // find by inn + kpp
        $contractors = (clone $query)->andWhere(['PPC' => $PPC])->asArray()->column();

        return ($contractors) ? (int)$contractors[0] : null;
    }

    /**
     * @param $code
     * @param null $company
     * @return |null
     */
    public static function findProductId($code, $company = null)
    {
        if (!$code)
            return null;

        if (!$company)
            $company = Yii::$app->user->identity->company;

        $products = Product::find()
            ->select('id')
            ->byCompany($company->id)
            ->byDeleted(false)
            ->andWhere(['or', ['code' => $code], ['article' => $code]])
            ->asArray()
            ->column();

        return ($products) ? $products[0] : null;
    }

    /**
     * @param $code
     * @param null $company
     * @return |null
     */
    public static function findProductIdByName($name, $company = null)
    {
        if (!$name)
            return null;

        if (!$company)
            $company = Yii::$app->user->identity->company;

        $products = Product::find()
            ->select('id')
            ->byCompany($company->id)
            ->byDeleted(false)
            ->andWhere(['title' => $name])
            ->asArray()
            ->column();

        return ($products) ? $products[0] : null;
    }

    /**
     * @param $haystack
     * @param $needle
     * @return bool
     */
    public static function inString($haystack, $needle) {
        return false !== mb_strpos(
            preg_replace('/[^a-zа-я0-9]/ui', '', mb_strtolower($haystack, 'utf-8')),
            preg_replace('/[^a-zA-Zа-яА-Я0-9]/ui', '', mb_strtolower($needle, 'utf-8')), 0, 'utf-8');
    }

    /**
     * @param $id
     * @param null $company
     * @return array|\yii\db\ActiveRecord|null
     */
    public static function getProduct($id, $company = null)
    {
        if (!$id)
            return null;

        if (!$company)
            $company = Yii::$app->user->identity->company;

        return Product::find()
            ->select([
                Product::tableName() . '.*',
                'SUM(product_store.quantity) * 1 as totalQuantity',
            ])
            ->joinWith([
                'priceForSellNds',
                'priceForBuyNds',
                'productUnit',
                'productStores',
            ])
            ->byCompany($company->id)
            ->byDeleted(false)
            ->andWhere([Product::tableName() . '.id' => $id])
            ->groupBy(Product::tableName() . '.id')
            ->asArray()
            ->one();
    }

    /**
     * @param $company
     * @param $title
     * @param int $max_length
     * @return string
     */
    public static function getSimilarProductsTitle(Company $company, $title, $max_length = 10)
    {
        $title = explode(' ', (string)$title);

        if (empty($title))
            return '';
        if (count($title) == 1)
            return $title[0];

        $title = array_splice($title, 0, $max_length);

        $expressions = $params = $orders = [];
        for ($pos = count($title); $pos > 1; $pos--) {

            $key = "w{$pos}";
            $expressions[$key] = new \yii\db\Expression("IF(LOCATE(:{$key}, title), 1, 0)");
            $params[$key] = implode(' ', array_slice($title, 0, $pos));
            $orders[$key] = SORT_DESC;
        }

        $ret = Product::find()
            ->select($expressions)
            ->addParams($params)
            ->where(['company_id' => $company->id])
            ->andWhere(['like', 'title' , $title[0].'%', false])
            ->orderBy($orders)
            ->asArray()
            ->limit(1)
            ->one();

        for ($pos = count($title); $pos > 0; $pos--)
        {
            $key = "w{$pos}";
            if (!empty($ret[$key])) {
                $title = array_splice($title, 0, $pos);
                break;
            }
        }

        return implode(' ', $title);
    }

    /**
     * @param Company $company
     * @param $title
     * @return bool
     */
    public static function hasSimilarProductsTitle(Company $company, $title)
    {
        $title = explode(' ', (string)$title);

        if (empty($title))
            return false;

        return Product::find()
            ->where(['company_id' => $company->id])
            ->andWhere(['like', 'title' , $title[0].'%', false])
            ->exists();
    }

    public static function getProductUnitId($unitName)
    {
        if (empty($unitName))
            return null;
        if ($unitName == 'услуга')
            $unitName = 'шт';

        return ProductUnit::find()
            ->where(['name' => $unitName])
            ->select('id')
            ->scalar();
    }
}
