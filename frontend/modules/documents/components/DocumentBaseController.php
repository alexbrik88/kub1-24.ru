<?php
namespace frontend\modules\documents\components;

use common\components\filters\AccessControl;
use common\components\pdf\PdfRenderer;
use common\components\pdf\Printable;
use common\components\phpWord\PhpWordAct;
use common\components\phpWord\PhpWordAgentReport;
use common\components\phpWord\PhpWordForeignCurrencyInvoice;
use common\components\phpWord\PhpWordInvoice;
use common\components\phpWord\PhpWordInvoiceFacture2020;
use common\components\phpWord\PhpWordInvoiceFacture2021;
use common\components\phpWord\PhpWordPackingList;
use common\components\phpWord\PhpWordSalesInvoice;
use common\components\phpWord\PhpWordUpd2020;
use common\components\phpWord\PhpWordUpd2021;
use common\models\document\AbstractDocument;
use common\models\document\Act;
use common\models\document\AgentReport;
use common\models\document\ForeignCurrencyInvoice;
use common\models\document\Invoice;
use common\models\document\InvoiceAuto;
use common\models\document\InvoiceFacture;
use common\models\document\OrderDocument;
use common\models\document\PackingList;
use common\models\document\SalesInvoice;
use common\models\document\Waybill;
use common\models\document\Upd;
use common\models\file;
use common\models\prompt\PageType;
use frontend\components\FrontendController;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\controllers\ActController;
use frontend\modules\documents\controllers\InvoiceFactureController;
use frontend\modules\documents\controllers\PackingListController;
use frontend\modules\documents\controllers\SalesInvoiceController;
use frontend\modules\documents\controllers\UpdController;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\rbac\permissions;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use backend\models\Prompt;
use backend\models\PromptHelper;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class DocumentController
 *
 * @package frontend\modules\documents\components
 */
abstract class DocumentBaseController extends FrontendController
{
    /**
     * @var
     */
    public $typeDocument;

    /**
     * Whether to allow file upload for document.
     * @var bool
     */
    protected $allowFileUpload = true;

    protected $isScanPreview = false;

    /**
     * @inheritdoc
     */
    public static function checkType($type)
    {
        if (!in_array($type, [Documents::IO_TYPE_IN, Documents::IO_TYPE_OUT])) {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
                                'ioType' =>  Yii::$app->request->getQueryParam('type'),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['document-print', 'pdf-view', 'print-view', 'docx', 'files', 'document-png'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::VIEW, [
                                'model' => $action->controller->loadModel(null, Yii::$app->request->getQueryParam('type'), true),
                            ]);
                        },
                    ],

                    [
                        'actions' => ['view', 'send', 'file-get', 'file-list', 'scan-list'],
                        'allow' => true,
                        'roles' => [
                            permissions\document\Document::VIEW,
                        ],
                        'roleParams' => function () {
                            return [
                                'model' => $this->loadModel(),
                            ];
                        },
                    ],
                    [
                        'actions' => ['file-upload', 'file-delete', 'scan-bind'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                'model' => $action->controller->loadModel(),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],

                    [
                        'actions' => ['update', 'original'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                'model' => $action->controller->loadModel(),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['update-status'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS, [
                                'model' => $action->controller->loadModel(),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
                                'model' => $action->controller->loadModel(),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'update-status' => ['post'],
                    'delete' => ['post', 'get'],

                    // files
                    'file-list' => ['get'],
                    'file-upload' => ['post'],
                    'file-delete    ' => ['post'],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();

        if ($this->allowFileUpload) {
            $documentModel = Documents::getBaseModel($this->typeDocument);

            $actions = array_merge($actions, [
                'file-get' => [
                    'class' => file\actions\GetFileAction::className(),
                    'model' => file\File::className(),
                    'idParam' => 'file-id',
                    'fileNameField' => 'filename_full',
                    'folderPath' => function($model) {
                        return $model->getUploadPath();
                    },
                ],

                'file-list' => [
                    'class' => file\actions\FileListAction::className(),
                    'model' => $documentModel::className(),
                    'fileLinkCallback' => function (file\File $file, AbstractDocument $ownerModel) {
                        return Url::to(['file-get', 'type' => $ownerModel instanceof OrderDocument ?
                            Documents::IO_TYPE_OUT :
                            $ownerModel->type, 'id' => $ownerModel->id, 'file-id' => $file->id,]);
                    },
                ],

                'file-delete' => [
                    'class' => file\actions\FileDeleteAction::className(),
                    'model' => $documentModel::className(),
                ],

                'file-upload' => [
                    'class' => file\actions\FileUploadAction::className(),
                    'model' => $documentModel::className(),
                    'noLimitCountFile' => true,
                    'folderPath' => 'documents' . DIRECTORY_SEPARATOR . $documentModel::$uploadDirectory,
                ],

                'scan-bind' => [
                    'class' => \frontend\modules\documents\components\actions\ScanBindAction::className(),
                    'model' => $documentModel::className(),
                ],

                'scan-list' => [
                    'class' => \frontend\modules\documents\components\actions\ScanListAction::className(),
                    'model' => $documentModel::className(),
                ],
            ]);
        }

        return $actions;
    }

    public function beforeAction($action)
    {
        $request = Yii::$app->getRequest();
        $referrer = $request->getReferrer();
        $fromUploadManager = (($action->id == 'update' || $action->id == 'create') && strpos($referrer, 'upload-manager'));
        if (!$request->getIsAjax() && $fromUploadManager) {
            Url::remember($referrer, 'lastUploadManagerPage');
        }
        $fromFilteredList = ($action->id == 'index');
        if (!$request->getIsAjax() && $fromFilteredList) {
            Url::remember($request->getUrl(), $action->controller->id . '_last_index_page');
        }

        $this->isScanPreview = $request->get('mode') == 'previewScan';

        return parent::beforeAction($action);
    }

    /**
     * Lists models.
     *
     * @param integer $type
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionIndex($type)
    {
        $searchModel = Documents::loadSearchProvider($this->typeDocument, $type);
        $dateRange = StatisticPeriod::getSessionPeriod();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $dateRange);

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        $message = new Message($this->typeDocument, $type);

        $prompt = null;
        if ($this->typeDocument === Documents::DOCUMENT_INVOICE) {
            $page_type = $type == Documents::IO_TYPE_IN ? PageType::TYPE_INVOICE_IN : PageType::TYPE_INVOICE_OUT;
            $prompt = (new PromptHelper())->getPrompts(Prompt::ACTIVE, Yii::$app->user->identity->company->company_type_id + 1, $page_type, true);
        }
        $event = null;
        switch (true) {
            case $this instanceof ActController:
                $event = $type == Documents::IO_TYPE_IN ? 62 : ($type == Documents::IO_TYPE_OUT ? 68 : null);
                break;
            case $this instanceof InvoiceFactureController:
                $event = $type == Documents::IO_TYPE_IN ? 63 : ($type == Documents::IO_TYPE_OUT ? 69 : null);
                break;
            case $this instanceof PackingListController:
                $event = $type == Documents::IO_TYPE_IN ? 64 : ($type == Documents::IO_TYPE_OUT ? 70 : null);
                break;
            case $this instanceof UpdController:
                $event = $type == Documents::IO_TYPE_IN ? 65 : ($type == Documents::IO_TYPE_OUT ? 71 : null);
                break;
//            case $this instanceof SalesInvoiceController:
//                $event = $type == Documents::IO_TYPE_IN ? 85 : ($type == Documents::IO_TYPE_OUT ? 86 : null);
//                break;
        }
        if ($event) {
            \common\models\company\CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, $event);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ioType' => $type,
            'message' => $message,
            'prompt' => $prompt,
        ]);
    }

    /**
     * Document files preview widget
     *
     * @return mixed
     */
    public function actionFiles($type, $id)
    {
        return $this->renderAjax('/default/files', [
            'model' => $this->loadModel($id, $type),
        ]);
    }

    /* PRINT METHODS */


    /**
     * @param $actionType
     * @param $id
     * @param $type
     * @param $filename
     * @return string|void
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionDocumentPrint($actionType, $id, $type, $filename)
    {
        $model = $this->loadModel($id, $type);

        if ($actionType === 'pdf' && $this->id == 'act') {
            $addStamp = Yii::$app->request->get('addStamp', $model->invoice->company->pdf_act_signed);
        } else {
            $addStamp = 0;
        }

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view', [
            'addStamp' => $addStamp,
        ]);
    }

    /**
     * @param $id
     * @param $type
     * @param int $page
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionDocumentPng($id, $type, $page = 1)
    {
        $model = $this->loadModel($id, $type);

        return $this->_documentPng($model, $page, []);
    }

    /**
     * @param $model
     * @param $asset
     * @param $actionType
     * @param $view
     * @param array $params
     * @return string|void
     * @throws Exception
     */
    protected function _documentPrint($model, $asset, $actionType, $view, $params = [])
    {
        if (!($model instanceof Printable)) {
            throw new Exception('Model must be instance of Printable.');
        }

        $event = null;
        switch (true) {
            case $model instanceof InvoiceAuto:
                break;
            case $model instanceof Invoice:
                $event = $actionType == 'print' ? 12 : ($actionType == 'pdf' ? 13 : null);
                break;
            case $model instanceof Act:
                $event = $actionType == 'print' ? 21 : ($actionType == 'pdf' ? 20 : null);
                break;
            case $model instanceof PackingList:
                $event = $actionType == 'print' ? 26 : ($actionType == 'pdf' ? 25 : null);
                break;
            case $model instanceof InvoiceFacture:
                $event = $actionType == 'print' ? 31 : ($actionType == 'pdf' ? 30 : null);
                break;
            case $model instanceof Upd:
                $event = $actionType == 'print' ? 36 : ($actionType == 'pdf' ? 35 : null);
                break;
            case $model instanceof Waybill:
                $event = null;
                break;
        }
        if ($event) {
            \common\models\company\CompanyFirstEvent::checkEvent($model->company, $event);
        }

        /** @var AbstractDocument $model */

        $renderer = new PdfRenderer([
            'view' => $view,
            'params' => array_merge([
                'model' => $model,
                'actionType' => $actionType,
                'message' => (isset(Message::$message[$this->typeDocument]) ? new Message($this->typeDocument,
                    $model->type) : null),
                'ioType' => ($model->hasAttribute('type') ? $model->type : null),
            ], $params),

            'destination' => PdfRenderer::DESTINATION_STRING,
            'filename' => $this->getPdfFileName($model),
            'displayMode' => ArrayHelper::getValue($params, 'displayMode', PdfRenderer::DISPLAY_MODE_FULLPAGE),
            'format' => ArrayHelper::getValue($params, 'documentFormat', 'A4-P'),
        ]);

        $this->view->params['asset'] = $asset;
        switch ($actionType) {
            case 'pdf':
                return $renderer->output();
            case 'print':
            default:
                if ($this->action->id != 'out-view') {
                    Yii::$app->view->registerJs('window.print();');
                }

                return $renderer->renderHtml();
        }
    }

    protected function _documentPng($model, $page, $params = [])
    {
        $renderer = new PdfRenderer([
            'view' => 'pdf-view',
            'params' => array_merge([
                'model' => $model,
                'actionType' => 'pdf',
                'message' => null,
                'ioType' => ($model->hasAttribute('type') ? $model->type : null),
            ], $params),
            'destination' => PdfRenderer::DESTINATION_STRING,
            'filename' => $this->getPdfFileName($model),
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => \yii\helpers\ArrayHelper::getValue($params, 'documentFormat', 'A4-P'),
        ]);

        $pdf_in = $renderer->output(false);

        $img = new \imagick();
        $img->setResolution(250, 250);
        $img->readImageBlob($pdf_in);

        $i=0;
        foreach($img as $_img) {

            $i++;

            if ($i == $page) {
                $white = new \imagick();
                $white->newImage($_img->getImageWidth(), $_img->getImageHeight(), "white");
                $white->compositeimage($_img, \imagick::COMPOSITE_OVER, 0, 0);
                $white->setImageFormat('png');

                /* Output the image */
                header("Content-Type: image/png");
                header('Content-disposition: inline;filename="' . urlencode(str_replace('.pdf', "-стр.{$i}.png", $this->getPdfFileName($model))) . '"');
                echo $white;
                die;
            }
        }

        header('HTTP/1.0 404 Not Found', true, 404);
        die;
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionPdfView($id)
    {
        /** @var Invoice $model */
        $model = $this->loadModel($id, Documents::IO_TYPE_OUT);

        $this->_pdf($model, 'pdf-view', [
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => $model->type,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPrintView($id)
    {
        $model = Documents::loadModel($id, $this->typeDocument, Documents::IO_TYPE_OUT);

        return $this->_print($model, 'out-view', [
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => $model->type,
        ]);
    }

    /**
     * Changin incoming documents status "is_original"
     * @param integer $id
     * @param integer $val
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionOriginal($id, $val)
    {
        if (!\Yii::$app->request->isPjax && !in_array($this->id, ['upd', 'act', 'invoice-facture', 'packing-list', 'proxy'])) {
            throw new \yii\web\NotFoundHttpException('Запрошенная страница не существует.');
        }

        $model = $this->loadModel($id, Documents::IO_TYPE_IN);

        $model->is_original = intval($val) ? true : false;
        $model->save(false, ['is_original', 'is_original_updated_at']);

        return $this->render('view', [
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => $model->type,
            'useContractor' => false,
            'contractorId' => null,
        ]);
    }

    /**
     * @param $id
     * @param $type
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionDocx($id, $type)
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Скачать документ в Word можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        $presentDocTypes = [
            'act',
            'invoice',
            'foreign-currency-invoice',
            'packing-list',
            'sales-invoice',
            'invoice-facture',
            'upd',
            'agent-report',
        ];

        if (in_array($this->id, $presentDocTypes)) {
            $model = $this->loadModel($id, $type);

            $docTypes = [
                'act' => PhpWordAct::class,
                'invoice' => PhpWordInvoice::class,
                'foreign-currency-invoice' => PhpWordForeignCurrencyInvoice::class,
                'packing-list' => PhpWordPackingList::class,
                'sales-invoice' => PhpWordSalesInvoice::class,
                'invoice-facture' => ($model->document_date < '2021-07-01') ? PhpWordInvoiceFacture2020::class : PhpWordInvoiceFacture2021::class,
                'upd' => ($model->document_date < '2021-07-01') ? PhpWordUpd2020::class : PhpWordUpd2021::class,
                'agent-report' => PhpWordAgentReport::class
            ];

            $word = new $docTypes[$this->id]($model);

            switch ($this->id) {
                case 'act':
                    \common\models\company\CompanyFirstEvent::checkEvent($model->company, 20);
                    break;
                case 'invoice':
                    \common\models\company\CompanyFirstEvent::checkEvent($model->company, 13);
                    break;
                case 'packing-list':
                    break;
                case 'agent-report':
                    break;
            }

            return $word->getFile();
        } else {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }
    }

    /**
     * @param null $id
     * @param null $ioType
     * @return ActiveRecord
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    protected function loadModel($id = null, $ioType = null)
    {
        if ($id === null) {
            $id = Yii::$app->request->getQueryParam('id');
        }
        if ($ioType === null) {
            $ioType = Yii::$app->request->getQueryParam('type');
        }

        $model = Documents::loadModel($id, $this->typeDocument, $ioType);
        if ($model === null) {
            throw new NotFoundHttpException('Model loading failed. Model not found.');
        }

        if (property_exists($model, 'findFreeNumber')) {
            $model->findFreeNumber = true;
        }

        return $model;
    }

    /**
     * @param integer $ioType
     *
     * @return Invoice|Act|PackingList|SalesInvoice|InvoiceFacture|ActiveRecord
     * @throws NotFoundHttpException
     */
    protected function getNewModel($ioType)
    {
        if (!in_array($ioType, [Documents::IO_TYPE_OUT, Documents::IO_TYPE_IN])) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }
        $class = Documents::getModel($this->typeDocument);
        $model = new $class;

        return $model;
    }

    /**
     * @param $type
     * @param $model
     * @param null $defaultUrl
     * @return \yii\web\Response
     */
    protected function redirectAfterDelete($type, $model, $defaultUrl = null)
    {
        $defaultUrl = $defaultUrl ?: $this->redirect([($model instanceof InvoiceAuto) ? 'index-auto' : 'index', 'type' => $type,]);

        if ($model instanceof Invoice || $model instanceof InvoiceAuto || $model instanceof ForeignCurrencyInvoice) {
            $contractor_id = $model->contractor_id;
        } else {
            $contractor_id = $model->invoice->contractor_id;
        }

        $useContractor = Yii::$app->request->getQueryParam('contractorId', null) == $contractor_id;
        if ($useContractor && Yii::$app->user->can(permissions\Contractor::VIEW)) {
            return $this->redirect(['/contractor/view', 'type' => $type, 'id' => $model->invoice->contractor_id,]);
        }

        return Yii::$app->user->can(permissions\document\Document::INDEX)
            ? $defaultUrl
            : $this->redirect(['/site/index']);
    }

    /**
     * @param $model
     * @param $view
     * @param $params
     * @param string $format by default - portrait, A4-L - for landscape.
     */
    protected function _pdf($model, $view, $params, $format = 'A4-P')
    {
        $renderer = new PdfRenderer([
            'view' => $view,
            'params' => $params,

            'destination' => PdfRenderer::DESTINATION_STRING,
            'filename' => $this->getPdfFileName($model),
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => $format,
        ]);

        $renderer->output(true);
    }

    /**
     * @param $model
     * @param $view
     * @param $params
     * @return string
     * @throws NotFoundHttpException
     */
    protected function _print($model, $view, $params)
    {
        if ($model === null) {
            throw new NotFoundHttpException();
        }

        $this->layout = 'print';

        return $this->render($view, $params);
    }

    /**
     * @param $model
     * @return string
     */
    protected function getPdfFileName($model)
    {
        return $model->printTitle . '.pdf';
    }

    public function actionGetManySendMessagePanel()
    {
        $typeDocument = Yii::$app->request->post('typeDocument');
        $sendWithDocs = Yii::$app->request->post('sendWithDocs');
        $useContractor = Yii::$app->request->post('useContractor');
        $ids = Yii::$app->request->post('ids');

        if (!$ids) {
            throw new NotFoundHttpException('Empty Ids');
        }

        $models = [];
        foreach ($ids as $id) {
            if ($model = Documents::loadModel($id, $typeDocument, Documents::IO_TYPE_OUT)) {
                if (Yii::$app->user->can(permissions\document\Document::VIEW, [
                    'ioType' => $model->type,
                    'model' => $model,
                ])) {
                    $models[] = $model;
                }
            }
        }

        return $this->renderAjax('@frontend/modules/documents/views/invoice/view/_many_send_message_form', [
            'models' => $models,
            'showSendPopup' => false,
            'typeDocument' => $typeDocument,
            'sendWithDocs' => $sendWithDocs,
            'useContractor' => $useContractor
        ]);
    }

    public function actionSendManyInOne($type, $id)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }
        /* @var Invoice $model */
        $model = $this->loadModel($id, $type);
        $sendForm = new InvoiceSendForm(Yii::$app->user->identity->currentEmployeeCompany, [
            'model' => $model,
            'textRequired' => true,
        ]);

        if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($sendForm);
        }

        if ($sendForm->load(Yii::$app->request->post()) && $sendForm->validate()) {

            $sendWithDocs = Yii::$app->request->post('send_with_documents');
            $ids = Yii::$app->request->post('ids', [$model->id]);

            $models = [];
            foreach ($ids as $id) {
                $models[] = $this->loadModel($id, Documents::IO_TYPE_OUT);
            }

            $saved = $sendForm->sendManyInOne($models, $sendWithDocs);
            if (is_array($saved)) {
                Yii::$app->session->setFlash('success', 'Отправлено ' . $saved[0] . ' из ' . $saved[1] . ' писем.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке писем.');
            }
        }

        $contractorId = Yii::$app->request->get('contractorId');

        return ($contractorId) ?
            $this->redirect(['/contractor/view', 'type' => $type, 'id' => $contractorId]) :
            $this->redirect(['index', 'type' => $type]);
    }

}