<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.02.2018
 * Time: 7:44
 */

namespace frontend\modules\documents\components;


use common\components\TextHelper;
use common\models\Company;
use common\models\document\OrderDocument;
use common\components\helpers\ArrayHelper;
use common\models\document\OrderDocumentInToOut;
use common\models\document\OrderDocumentProduct;
use common\models\product\Product;
use common\models\TaxRate;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use Yii;
use yii\db\Transaction;
use yii\helpers\VarDumper;

/**
 * Class OrderDocumentHelper
 * @package frontend\modules\documents\components
 */
class OrderDocumentHelper
{
    /**
     * @param OrderDocument $model
     * @param bool|true $validation
     * @param null $previousStatus
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\base\Exception
     */
    public static function save(OrderDocument &$model, $validation = true, $previousStatus = null)
    {
        $removedOrdersIdArray = array_keys($model->removedOrders);

        $saveClosure = function ($db) use ($model, $validation, $removedOrdersIdArray, $previousStatus) {
            /* @var $company Company */
            $company = $model->getCompany()->one();
            $model->production_type = $model->getProductionTypeByOrderArray();
            //$discount = 0;
            if ($company->store_has_discount && $company->store_discount_type === Company::DISCOUNT_TYPE_PERCENT &&
                ($model->total_amount / 100) > $company->store_discount) {
                $discount = intval($company->store_discount);
                $model->has_discount = true;
            }
            if (!$model->save($validation)) {
                return false;
            }
            $model->checkStatus($previousStatus);
            foreach ($model->orderDocumentProducts as $order) {
                if (isset($discount)) $order->discount = $discount; // store discount
                $order->order_document_id = $model->id;
                if (!$order->save($validation)) {
                    $model->addErrors($order->errors);

                    return false;
                }
            }
            Yii::$app->db->createCommand()
                ->delete(OrderDocumentProduct::tableName(), [
                    'id' => $removedOrdersIdArray,
                ])->execute();


            // save in-to-out relations
            if ($model->createFromOut) {
                foreach ($model->orderDocumentProducts as $order) {
                    foreach ($model->outProducts as $outProductId => $outPos) {
                        if ($order->product_id == $outProductId) {
                            foreach ($outPos['quantity'] as $k => $v) {

                                $_relAttributes = [
                                    'in_order_document' => $model->id,
                                    'in_order_document_product' => $order->id,
                                    'out_order_document' => $outPos['order_document_id'][$k],
                                    'out_order_document_product' => $outPos['order_product_id'][$k],
                                ];

                                $relIn2Out =
                                    OrderDocumentInToOut::findOne($_relAttributes)
                                    ?: new OrderDocumentInToOut($_relAttributes);

                                if (!$relIn2Out->save()) {
                                    Yii::error(VarDumper::dumpAsString($relIn2Out->getErrors()), 'validation');
                                }
                            }
                        }
                    }
                }
            }

            return true;
        };
        if ($model->isNewRecord) {
            $isSaved = LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, $saveClosure);
        } else {
            $isSaved = Yii::$app->db->transaction($saveClosure, Transaction::READ_COMMITTED);
        }

        return (boolean)$isSaved;
    }

    /**
     * @param OrderDocument $model
     */
    public static function loadOutProducts(OrderDocument &$model)
    {
        if ($model->isNewRecord) {
            $createFromOut = Yii::$app->request->get('out');
        } else {
            /** @var OrderDocumentInToOut[] $in2out */
            $in2out = OrderDocumentInToOut::find()->where(['in_order_document' => $model->id])->all();
            $createFromOut = [];
            foreach ($in2out as $i2o)
                $createFromOut[] = $i2o->out_order_document . '-' . $i2o->out_order_document_product;
        }

        $outProducts = [];
        foreach ((array)$createFromOut as $fromPosition) {
            @list($orderDocId, $orderProductId) = explode('-', $fromPosition);
            if ($orderDocId && $orderProductId) {
                if ($orderDoc = OrderDocument::findOne(['id' => $orderDocId, 'company_id' => $model->company->id])) {
                    if ($orderProduct = OrderDocumentProduct::findOne(['id' => $orderProductId, 'order_document_id' => $orderDoc->id])) {
                        $productId = $orderProduct->product_id;
                        if (!isset($outProducts[$productId])) {
                            $outProducts[$productId] = [
                                'order_document_id' => [],
                                'order_product_id' => [],
                                'quantity' => []
                            ];
                        }
                        $outProducts[$productId]['order_document_id'][] = $orderDocId;
                        $outProducts[$productId]['order_product_id'][] = $orderProductId;
                        $outProducts[$productId]['quantity'][] = (float)$orderProduct->quantity;
                    }
                }
            }
        }

        $model->createFromOut = (bool)$createFromOut;
        $model->outProducts = $outProducts;
    }

    /**
     * @param OrderDocument $model
     * @param array $post
     * @return bool
     */
    public static function load(OrderDocument &$model, $post = [])
    {
        if ($model->load($post)) {
            $ordersInput = ArrayHelper::getValue($post, 'orderArray');
            $ordersInput = is_array($ordersInput) ? array_values($ordersInput) : [];
            $orderArray = [];
            $existedOrders = ArrayHelper::getAssoc($model->orderDocumentProducts, 'id');

            foreach ($ordersInput as $key => $info) {
                if (!isset($info['model'], $info['id'], $info['title'], $info['count'], $info['priceOneWithVat'])) {
                    continue;
                }
                if (isset($info['contractor_id'])) {
                    $model->contractor_id = $info['contractor_id'];
                }
                if ($info['model'] == OrderDocumentProduct::tableName()) {
                    $order = OrderDocumentProduct::findById($model->id, $info['id']);
                    if ($order !== null) {
                        $order->populateRelation('orderDocument', $model);
                        $order->product_title = $info['title'];
                        $order->quantity = $info['count'];
                        $order->discount = $model->has_discount ? $info['discount'] : 0;
                        $order->markup = $model->has_markup ? $info['markup'] : 0;

                        if (isset($info['sale_tax_rate_id'])) {
                            $order->sale_tax_rate_id = $info['sale_tax_rate_id'];
                        }
                        if (isset($info['purchase_tax_rate_id'])) {
                            $order->purchase_tax_rate_id = $info['purchase_tax_rate_id'];
                        }

                        $order->calculateOrderPrice(
                            round(TextHelper::parseMoneyInput($info['priceOneWithVat']) * 100),
                            ($model->type == Documents::IO_TYPE_OUT) ? $order->sale_tax_rate_id : $order->purchase_tax_rate_id
                        );
                        $order->calculateAmount();
                        $orderArray[] = $order;
                        if (isset($existedOrders[$order->id])) {
                            unset($existedOrders[$order->id]);
                        }
                    }
                } elseif ($info['model'] == Product::tableName()) {
                    $product = Product::getById($model->company_id, $info['id']);
                    if ($product !== null) {
                        if ($model->nds_view_type_id == OrderDocument::NDS_VIEW_WITHOUT) {
                            $saleTaxRateId = TaxRate::RATE_WITHOUT;
                            $purchaseTaxRateId = TaxRate::RATE_WITHOUT;
                        } else {
                            $saleTaxRateId = $info['sale_tax_rate_id'] ?? null;
                            $purchaseTaxRateId = $info['purchase_tax_rate_id'] ?? null;
                        }
                        $order = OrderDocumentProductHelper::createOrderByProduct(
                            $product,
                            $model,
                            $info['count'],
                            (isset($info['priceOneWithVat']) ? round(TextHelper::parseMoneyInput($info['priceOneWithVat']) * 100) : 0),
                            ($model->has_discount && isset($info['discount']) ? $info['discount'] : 0),
                            ($model->has_markup && isset($info['markup']) ? $info['markup'] : 0),
                            ($model->type == Documents::IO_TYPE_OUT) ? $saleTaxRateId : $purchaseTaxRateId
                        );
                        $order->product_title = $info['title'];
                        $orderArray[] = $order;
                    }
                }
                array_walk($orderArray, function ($order, $key) {
                    $order->number = $key + 1;
                });
            }
            $model->populateRelation('orderDocumentProducts', $orderArray);
            $model->removedOrders = $existedOrders;
            $model->total_mass_gross = (string)array_sum(ArrayHelper::getColumn($model->orderDocumentProducts, 'mass_gross'));
            $model->total_place_count = (string)array_sum(ArrayHelper::getColumn($model->orderDocumentProducts, 'place_count'));
            $model->total_order_count = count($ordersInput);

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param OrderDocument $model
     * @param int $discount
     * @param int $markup
     * @return bool
     * @throws \Throwable
     */
    public static function saveFromPriceList(OrderDocument &$model, $discount = 0, $markup = 0)
    {
        $saveClosure = function ($db) use ($model, $discount, $markup) {
            $model->production_type = $model->getProductionTypeByOrderArray();
            if (!$model->save()) {
                return false;
            }

            foreach ($model->orderDocumentProducts as $order) {
                $order->discount = $discount;
                $order->markup = $markup;
                $order->order_document_id = $model->id;
                if (!$order->save()) {
                    $model->addErrors($order->errors);

                    return false;
                }
            }

            return true;
        };

        if ($model->isNewRecord) {
            $isSaved = LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, $saveClosure);
        } else {
            $isSaved = Yii::$app->db->transaction($saveClosure, Transaction::READ_COMMITTED);
        }

        return (boolean)$isSaved;
    }
}