<?php
namespace frontend\modules\documents\components;

use backend\models\Prompt;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\OrderDocument;
use common\models\document\PackingList;
use common\models\document\PaymentOrder;
use frontend\components\StatisticPeriod;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class FilterHelper
 * @package frontend\modules\documents\components
 */
class FilterHelper
{
    /**
     * @param $type
     * @param null $tableName
     * @param bool|false $addAll
     * @param bool|false $includeDeleted
     * @param bool|true $addNew
     * @return array
     */
    public static function getContractorList($type, $tableName = null, $addAll = false, $includeDeleted = false, $addNew = true)
    {
        $contractorArray = static::getContractorArray($type, $tableName, StatisticPeriod::getSessionPeriod(), $includeDeleted);

        return static::getFilterArray($contractorArray, 'id', 'shortName', $addAll, $addNew, $type);
    }

    /**
     * @param $type
     * @param null $tableName
     * @param null $dateRange
     * @param bool|false $includeDeleted
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getContractorArray($type, $tableName = null, $dateRange = null, $includeDeleted = false)
    {
        $query = Contractor::getSorted()
            ->distinct()
            ->byCompany(Yii::$app->user->identity->company->id)
            ->byContractor($type)
            ->byDeleted($includeDeleted)
            ->byStatus(Contractor::ACTIVE)
            ->andWhere(['!=', Contractor::tableName() . '.ITN', Yii::$app->user->identity->company->inn]);
        if ($tableName && $dateRange) {
            switch ($tableName) {
                case Invoice::tableName(): {
                    $query
                        ->leftJoin(['u' => $tableName], 'u.contractor_id = contractor.id')
                        ->andWhere(['between', 'u.document_date', $dateRange['from'], $dateRange['to']]);
                    break;
                }
                case OrderDocument::tableName(): {
                    $query
                        ->leftJoin(['u' => $tableName], 'u.contractor_id = contractor.id')
                        ->andWhere(['between', 'u.document_date', $dateRange['from'], $dateRange['to']]);
                    break;
                }
                case PackingList::tableName(): {
                    $query
                        ->leftJoin(['u' => Invoice::tableName()], 'u.contractor_id = contractor.id')
                        ->leftJoin(['p' => PackingList::tableName()], 'p.invoice_id = u.id')
                        ->andWhere(['u.is_deleted' => Invoice::NOT_IS_DELETED])
                        ->andWhere(['between', 'p.document_date', $dateRange['from'], $dateRange['to']]);
                    break;
                }
                case Act::tableName(): {
                    $query
                        ->leftJoin(['u' => Invoice::tableName()], 'u.contractor_id = contractor.id')
                        ->leftJoin(['p' => Act::tableName()], 'p.invoice_id = u.id')
                        ->andWhere(['u.is_deleted' => Invoice::NOT_IS_DELETED])
                        ->andWhere(['between', 'p.document_date', $dateRange['from'], $dateRange['to']]);
                    break;
                }
                case InvoiceFacture::tableName(): {
                    $query
                        ->leftJoin(['u' => Invoice::tableName()], 'u.contractor_id = contractor.id')
                        ->leftJoin(['p' => InvoiceFacture::tableName()], 'p.invoice_id = u.id')
                        ->andWhere(['u.is_deleted' => Invoice::NOT_IS_DELETED])
                        ->andWhere(['between', 'p.document_date', $dateRange['from'], $dateRange['to']]);
                    break;
                }
                case PaymentOrder::tableName(): {
                    $query
                        ->leftJoin(['u' => Invoice::tableName()], 'u.contractor_id = contractor.id')
                        ->leftJoin(['p' => PaymentOrder::tableName()], 'p.invoice_id = u.id')
                        ->andWhere(['u.is_deleted' => Invoice::NOT_IS_DELETED])
                        ->andWhere(['between', 'p.document_date', $dateRange['from'], $dateRange['to']]);
                    break;
                }
            }
        }

        return $query->all();
    }

    /**
     * @param bool $addAll
     * @param bool $includeDeleted
     * @return array
     */
    public static function getAllContractorList($type, $addAll = false, $includeDeleted = false)
    {
        $contractorArray = Contractor::find()
            ->distinct()
            ->select(['id', 'name'])
            ->byContractor($type)
            ->byCompany(Yii::$app->user->identity->company->id)
            ->byDeleted($includeDeleted)
            ->byStatus(Contractor::ACTIVE)
            ->orderBy('name')
            ->asArray()
            ->all();

        return static::getFilterArray($contractorArray, 'id', 'name', $addAll);
    }

    /**
     * @param array $models
     * @param $key
     * @param $value
     * @param bool|true $addAll
     * @param bool|true $addNew
     * @param int $type
     * @return array
     */
    public static function getFilterArray(array $models, $key, $value, $addAll = true, $addNew = true, $type = 1)
    {
        $filterArray = [];
        $addNewName = $type == Contractor::TYPE_CUSTOMER ? 'покупателя' : 'поставщика';

        if ($addAll !== false) {
            $filterArray[null] = 'Все';
        }

        if ($addNew !== false) {
            $filterArray["add-modal-contractor"] = '[ + Добавить ' . $addNewName . ' ]';
        }

        $filterArray += ArrayHelper::map($models, $key, $value);

        return $filterArray;
    }

    /**
     * @param bool|true $addAll
     * @return array
     */
    public static function getPromptStatusArray($addAll = true)
    {
        $promptStatusArray = [];

        if ($addAll !== false) {
            $promptStatusArray[null] = 'Все';
        }

        $promptStatusArray += Prompt::$PromptStatus;

        return $promptStatusArray;
    }
}
