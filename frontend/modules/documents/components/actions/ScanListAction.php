<?php

namespace frontend\modules\documents\components\actions;

use common\models\document\ScanDocument;
use frontend\modules\documents\widgets\ScanListWidget;
use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class BindScanAction
 *
 * @example acton definition:
 * public function actions()
 * {
 *      return [
 *          'scan-list' => [
 *              'class' => \frontend\modules\documents\components\actions\ScanListAction::className(),
 *              'model' => Model::className(),
 *          ],
 *      ];
 * }
 *
 */
class ScanListAction extends Action
{
    /**
     * Class name
     * @var string
     */
    public $model;

    /**
     * @var string
     */
    public $modelIdParam = 'id';

    /**
     * Initializes the action.
     * @throws InvalidConfigException if the font file does not exist.
     */
    public function init()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
    }

    /**
     * Runs the action.
     * This method displays the view requested by the user.
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function run()
    {
        return [
            'content' => ScanListWidget::widget([
                'model' => $this->findModel(Yii::$app->request->get($this->modelIdParam)),
            ]),
        ];
    }

    /**
     * @param $id
     * @return ActiveRecord
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    private function findModel($id)
    {
        if ($id === null) {
            throw new BadRequestHttpException('`id` not passed into parameters.');
        }

        /* @var ActiveRecord $class */
        $class = $this->model;
        $query = $class::find();
        $query->byCompany(Yii::$app->user->identity->company->id);

        $query->andWhere([
            $class::tableName() . '.id' => $id,
        ]);

        $model = $query->one();

        if ($model === null) {
            throw new NotFoundHttpException('Owner model not found.');
        }

        return $model;
    }
}
