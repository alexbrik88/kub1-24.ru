<?php

namespace frontend\modules\documents\components\actions;

use common\models\document\ScanDocument;
use frontend\models\Documents;
use frontend\modules\documents\widgets\ScanListWidget;
use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class BindScanAction
 *
 * @example acton definition:
 * public function actions()
 * {
 *      return [
 *          'scan-bind' => [
 *              'class' => \frontend\modules\documents\components\actions\ScanBindAction::className(),
 *              'model' => Model::className(),
 *          ],
 *      ];
 * }
 *
 */
class ScanBindAction extends Action
{
    /**
     * Class name
     * @var string
     */
    public $model;

    /**
     * @var string
     */
    public $modelIdParam = 'model-id';
    /**
     * @var string
     */
    public $scanIdParam = 'scan-id';

    /**
     * Initializes the action.
     * @throws InvalidConfigException if the font file does not exist.
     */
    public function init()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
    }

    /**
     * Runs the action.
     * This method displays the view requested by the user.
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function run()
    {
        $id = Yii::$app->request->get('id');
        $modelId = Yii::$app->request->post($this->modelIdParam);
        $scanId = Yii::$app->request->post($this->scanIdParam);
        $model = $this->findModel($id);
        $scanArray = $this->findScan($scanId);

        if ($modelId === '0') {
            foreach ($scanArray as $scan) {
                $scan->updateAttributes([
                    'owner_model' => null,
                    'owner_table' => null,
                    'owner_id' => null,
                ]);
            }
        } else {
            $fileCount = \common\models\file\File::find()->andWhere([
                'owner_model' => $model->className(),
                'owner_id' => $model->id,
            ])->count();
            $scanCount = \common\models\document\ScanDocument::find()->andWhere([
                'owner_model' => $model->className(),
                'owner_id' => $model->id,
            ])->count();

            $count = max(0, 3 - $fileCount - $scanCount);
            $documentModelClass = $this->model;
            if ($scanArray && $count) {
                foreach ($scanArray as $scan) {
                    if (0 < $count--) {
                        $scan->updateAttributes([
                            'owner_model' => $model->className(),
                            'owner_table' => $model->tableName(),
                            'owner_id' => $model->id,
                            'contractor_id' => isset($model->contractor_id) ? $model->contractor_id : (
                                isset($model->invoice) ? $model->invoice->contractor_id : null
                            ),
                            'document_type_id' => Documents::typeIdByClass($model->className()),
                        ]);
                    } else {
                        break;
                    }
                }
            }
        }
        $model->updateHasFile();

        return [
            'content' => ScanListWidget::widget(['model' => $model]),
        ];
    }

    /**
     * @param $id
     * @return ActiveRecord
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    private function findModel($id)
    {
        if ($id === null) {
            throw new BadRequestHttpException('`id` not passed into parameters.');
        }

        /* @var ActiveRecord $class */
        $class = $this->model;
        $query = $class::find();
        $query->byCompany(Yii::$app->user->identity->company->id);

        $query->andWhere([
            $class::tableName() . '.id' => $id,
        ]);

        $model = $query->one();

        if ($model === null) {
            throw new NotFoundHttpException('Owner model not found.');
        }

        return $model;
    }

    /**
     * @param $id
     * @return ActiveRecord
     * @throws NotFoundHttpException
     */
    private function findScan($id)
    {
        $query = ScanDocument::find()->where([
            'id' => $id,
            'company_id' => Yii::$app->user->identity->company->id,
        ]);

        $modelArray = $query->all();

        if (count($modelArray) === 0) {
            throw new NotFoundHttpException('ScanDocument model not found.');
        }

        return $modelArray;
    }

}
