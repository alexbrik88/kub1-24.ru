<?php

namespace frontend\modules\documents\components;

use common\components\date\DateHelper;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceAct;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceFacture;
use common\models\document\InvoiceInvoiceFacture;
use common\models\document\Order;
use common\models\document\OrderAct;
use common\models\document\OrderHelper;
use common\models\document\OrderInvoiceFacture;
use common\models\document\status\InvoiceStatus;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\modules\analytics\models\PlanCashContractor;
use frontend\modules\export\models\one_c\OneCExport;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * ActHelper
 */
class ActHelper
{
    /**
     * @var string
     */
    public static $orderParamName = 'orderArray';
    public static $surchargeProductParamName = 'surchargeProduct';

    /**
     * @param  Act     $model
     * @param  array   $data
     * @param  string  $formName
     * @return boolean
     */
    public static function load(Act &$model, $data, $formName = null)
    {
        if (is_array($data) && $model->load($data, $formName)) {
            $ordersData = ArrayHelper::getValue($data, self::$orderParamName);
            $newProductsData = ArrayHelper::getValue($data, self::$surchargeProductParamName);

            if ($ordersData !== null) {
                $model->ordersLoad($ordersData);
            }
            if ($newProductsData !== null) {
                $model->surchargeNewProducts = $newProductsData;
            }

            $model->order_sum = $model->totalAmountWithNds;
            $model->order_nds = $model->totalNds;

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param  Act     $model
     * @return boolean
     */
    public static function validate(Act &$model)
    {
        foreach ($model->orderActs as $orderAct) {
            if (!$orderAct->validate()) {
                $model->addErrors($orderAct->getErrors());
            }
        }

        $model->validate(null, false);

        return !$model->hasErrors();
    }

    /**
     * @param Act $model
     * @param bool $validate
     * @return bool|mixed
     * @throws \Throwable
     */
    public static function save(Act &$model, $validate = true)
    {
        if ($validate && !static::validate($model)) {
            return false;
        }

        $isSaved = Yii::$app->db->transaction(function ($db) use ($model) {

            if ($model->surchargeInvoiceOrders || $model->surchargeNewProducts) {

                $hasInvoiceFacture = array_reduce($model->invoices, function($carry, $item) { return $carry || $item->has_invoice_facture; }, false);
                $surchargeInvoice = self::createSurchargeInvoice($model, $hasInvoiceFacture);

                if (!$surchargeInvoice) {
                    $model->addError('orderActs', "Не удалось создать счет на доплату");
                    $db->transaction->rollBack();
                    return false;
                }

                if ($hasInvoiceFacture) {
                    $invoiceFacture = null;
                    foreach ($model->invoices as $i) {
                        if ($i->invoiceFacture) {
                            $invoiceFacture = $i->invoiceFacture;
                        }
                    }
                    if (!$invoiceFacture) {
                        $model->addError('orderActs', "Не удалось найти счет-фактуру");
                        $db->transaction->rollBack();
                        return false;
                    }

                    if (!self::addSurchargeOrdersToInvoiceFacture($surchargeInvoice, $invoiceFacture)) {
                        $model->addError('orderActs', "Не удалось добавить доплату в счет-фактуру");
                        $db->transaction->rollBack();
                        return false;
                    }

                    foreach ($model->invoices as $invoice) {
                        InvoiceHelper::checkForInvoiceFacture($invoice->id, $model->invoices);
                    }
                }
            }

            if ($model->save(false)) {
                foreach ($model->removedOrders as $orderAct) {
                    if ($orderAct->delete() === false) {
                        $model->addError('orderActs', "Нельзя удалить «{$orderAct->order->product_title}»");
                        $db->transaction->rollBack();

                        return false;
                    }
                }
                foreach ($model->orderActs as $orderAct) {
                    if (!$orderAct->save(false)) {
                        $model->addError('orderActs', "Не удалось сохранить «{$orderAct->order->product_title}»");
                        $db->transaction->rollBack();

                        return false;
                    }
                }

                foreach ($model->invoices as $invoice) {
                    InvoiceHelper::checkForAct($invoice->id, $model->invoices);
                }

                if (isset($invoice)) {
                    $invoice->updateDocumentsPaid();
                    if ($model->isNewRecord) {
                        PlanCashContractor::createFlowByInvoice($invoice, Documents::DOCUMENT_ACT);
                    } else {
                        PlanCashContractor::updateFlowByInvoice($invoice, Documents::DOCUMENT_ACT);
                    }
                }

                return true;
            }
            $db->transaction->rollBack();

            return false;
        });

        // trigger olap_documents_update_order_act
        if ($isSaved) {
            OrderAct::findOne(['act_id' => $model->id])->save(false);
        }

        return $isSaved;
    }

    /**
     * @param Act $model
     * @param bool $hasInvoiceFacture
     * @return bool|Invoice
     */
    public static function createSurchargeInvoice(Act $model, $hasInvoiceFacture = false)
    {
        $company = Yii::$app->user->identity->company;
        if (!$company->createInvoiceAllowed($model->invoice->type)) {
            return false;
        }
        $cloneInvoice = new Invoice($model->invoice->getAttributes([
            'type',
            'production_type',
            'document_date',
            'payment_limit_date',
            'has_markup',
            'has_discount',
            'nds_view_type_id',
            'price_precision',
            'currency_name',
            'currency_amount',
            'currency_rate',
            'currency_rate_type',
            'currency_rate_date',
            'currency_rate_amount',
            'currency_rate_value',
            'invoice_expenditure_item_id',
        ]));
        $cloneInvoice->is_surcharge = 1; // disable change quantity!
        $cloneInvoice->document_date = date(DateHelper::FORMAT_DATE);
        $cloneInvoice->document_number = ($model->type == Documents::IO_TYPE_OUT) ?
            (string)$model->invoice->getNextDocumentNumber($company->id, $model->type, null, $cloneInvoice->document_date) :
            (string)$model->invoice->document_number;
        $cloneInvoice->document_additional_number = $model->invoice->document_additional_number;
        $cloneInvoice->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime('+10 day'));
        $cloneInvoice->company_id = $model->invoice->company->id;
        $cloneInvoice->contractor_id = $model->invoice->contractor->id;
        $cloneInvoice->object_guid = OneCExport::generateGUID();
        $cloneInvoice->populateRelation('company', $model->invoice->company);
        $cloneInvoice->populateRelation('contractor', $model->invoice->contractor);

        $cloneOrderArray = [];
        $newOrderActArray = [];
        foreach ($model->surchargeInvoiceOrders as $orderId => $quantity) {
            /** @var Order $order */
            $order = Order::findOne($orderId);
            if ($order) {
                $cloneOrder = OrderHelper::createOrderByProduct(
                    $order->product,
                    $cloneInvoice,
                    $quantity,
                    $order->price * 100,
                    $order->discount
                );
                $cloneOrder->product_title = $order->product_title;

                $cloneOrderArray[] = $cloneOrder;
            }
        }
        foreach ((array)$model->surchargeNewProducts as $productID => $data) {
            /** @var Product $product */
            $product = Product::find()->where(['id' => $productID, 'company_id' => $model->invoice->company_id])->one();
            if ($product) {
                // invoice order
                $cloneOrder = OrderHelper::createOrderByProduct(
                    $product,
                    $cloneInvoice,
                    $data['quantity'],
                    (100 * (float)str_replace(',' ,'.', $data['price'])) ?: null
                );
                $cloneOrderArray[] = $cloneOrder;

                // act order
                $orderAct = new OrderAct([
                    'invoice_id' => null,                    
                    'order_id' => null,
                    'act_id' => $model->id,
                    'product_id' => $productID,
                    'quantity' => $cloneOrder->quantity,
                ]);

                $newOrderActArray[$productID] = $orderAct;
            }
        }

        $cloneInvoice->populateRelation('orders', $cloneOrderArray);
        // save invoice
        if (!$cloneInvoice->save()) {

            return false;
        }
        // save orders
        foreach ($cloneInvoice->orders as $cloneOrder) {
            $cloneOrder->invoice_id = $cloneInvoice->id;
            if (!$cloneOrder->save(false)) {

                return false;
            }
            if (isset($newOrderActArray[$cloneOrder->product_id])) {
                /** @var OrderAct $orderAct */
                $orderAct = $newOrderActArray[$cloneOrder->product_id];
                $orderAct->invoice_id = $cloneInvoice->id;
                $orderAct->order_id = $cloneOrder->id;
                if (!$orderAct->save(false)) {

                    return false;
                }
            }
        }
        // save relation
        $invoice2Act = new InvoiceAct();
        $invoice2Act->invoice_id = $cloneInvoice->id;
        $invoice2Act->act_id = $model->id;
        if (!$invoice2Act->save(false)) {
            return false;
        }

        // update flags
        $cloneInvoice->updateAttributes([
            //
            'can_add_act' => 0,
            'can_add_packing_list' => 0,
            'can_add_sales_invoice' => 0,
            'can_add_proxy' => 1,
            'can_add_invoice_facture' => (int)(!$hasInvoiceFacture),
            'can_add_upd' => 0,
            'has_act' => 1,
            'has_packing_list' => 0,
            'has_sales_invoice' => 0,
            'has_proxy' => 0,
            'has_invoice_facture' => (int)$hasInvoiceFacture,
            'has_upd' => 0,
            'has_file' => 0
        ]);

        return $cloneInvoice;
    }

    /**
     * @param Invoice $invoice
     * @param InvoiceFacture $invoiceFacture
     * @return bool
     */
    public static function addSurchargeOrdersToInvoiceFacture(Invoice $invoice, InvoiceFacture $invoiceFacture)
    {
        // save orders
        foreach ($invoice->orders as $order)
        {
            if ($order = OrderInvoiceFacture::createFromOrder($invoiceFacture->id, $order)) {
                $order->populateRelation('invoiceFacture', $invoiceFacture);
                if (!$order->save())
                    return false;
            }
        }

        // save relation
        $invoiceInvoiceFacture = new InvoiceInvoiceFacture();
        $invoiceInvoiceFacture->invoice_id = $invoice->id;
        $invoiceInvoiceFacture->invoice_facture_id = $invoiceFacture->id;
        if (!$invoiceInvoiceFacture->save(false)) {
            return false;
        }

        $invoice->updateAttributes([
            'can_add'
        ]);

        return true;
    }
}
