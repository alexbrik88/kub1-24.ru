<?php

namespace frontend\modules\documents\components;

use common\models\document\PackingList;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * PackingListHelper
 */
class PackingListHelper
{
    /**
     * @var string
     */
    public static $orderParamName = 'orderArray';

    /**
     * @param  PackingList     $model
     * @param  array   $data
     * @param  string  $formName
     * @return boolean
     */
    public static function load(PackingList &$model, $data, $formName = null)
    {
        if (is_array($data) && $model->load($data, $formName)) {
            $ordersData = ArrayHelper::getValue($data, self::$orderParamName);
            if ($ordersData !== null) {
                $model->ordersLoad($ordersData);
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param  PackingList     $model
     * @return boolean
     */
    public static function validate(PackingList &$model)
    {
        foreach ($model->orderPackingLists as $orderPackingList) {
            if (!$orderPackingList->validate()) {
                $model->addErrors($orderPackingList->getErrors());
            }
        }

        $model->validate(null, false);

        return !$model->hasErrors();
    }

    /**
     * @param PackingList     $model
     * @param boolean $validate
     *
     * @return boolean
     */
    public static function save(PackingList &$model, $validate = true)
    {
        if ($validate && !static::validate($model)) {
            return false;
        }

        $model->ordinal_document_number = $model->document_number;

        return Yii::$app->db->transaction(function ($db) use ($model) {
            if ($model->save(false)) {
                foreach ((array) $model->removedOrders as $orderPackingList) {
                    if ($orderPackingList->delete() === false) {
                        $model->addError('orderPackingLists', "Нельзя удалить «{$orderPackingList->order->product_title}»");
                        $db->transaction->rollBack();

                        return false;
                    }
                }
                foreach ((array) $model->orderPackingLists as $orderPackingList) {
                    if (!$orderPackingList->save(false)) {
                        $model->addError('orderPackingLists', "Неудалось сохранить «{$orderPackingList->order->product_title}»");
                        $db->transaction->rollBack();

                        return false;
                    }
                }
                $model->updateAttributes([
                    'orders_sum' => $model->totalAmountWithNds,
                    'order_nds' => $model->totalNds
                ]);

                return true;
            }
            $db->transaction->rollBack();

            return false;
        });
    }
}
