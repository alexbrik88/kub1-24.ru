<?php

namespace frontend\modules\documents\components;

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\models\document\Autoact;
use common\models\document\Autoinvoice;
use common\models\document\Invoice;
use common\models\document\InvoiceAct;
use common\models\document\InvoiceInvoiceFacture;
use common\models\document\InvoiceUpd;
use common\models\document\OrderAct;
use common\models\document\OrderInvoiceFacture;
use common\models\document\OrderPackingList;
use common\models\document\OrderUpd;
use common\models\product\Product;
use DateTime;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use Yii;
use yii\db\Query;
use yii\helpers\VarDumper;

class InvoiceHelper
{
    public static $save_product = false;

    /**
     * @param Invoice $model
     * @param Autoinvoice $autoinvoice
     * @param boolean $validation
     *
     * @return boolean
     */
    public static function save(Invoice &$model, $validation = true, $logUpdate = false)
    {
        if ($validation && !self::validate($model)) {
            return false;
        }
        $model->production_type = $model->getProductionTypeByOrderArray();
        if ($model->isAutoinvoice) {
            $model->document_number = $model->auto->document_number;
            $model->auto->contractor_id = $model->contractor_id;
            $dateFrom = $model->auto->dateFrom;
            if ($model->auto->dateFrom < date(DateHelper::FORMAT_USER_DATE) && $model->auto->dateTo > date(DateHelper::FORMAT_USER_DATE)) {
                $dateFrom = date(DateHelper::FORMAT_DATE);
            }
        }

        $saveClosure = function ($db) use ($model) {
            if (!$model->save(false)) {
                return false;
            }
            if ($model->isAutoinvoice) {
                $model->auto->id = $model->id;
                if (!$model->auto->save(false)) {
                    return false;
                }
            }
            foreach ($model->orders as $order) {
                $order->invoice_id = $model->id;
                if (self::$save_product) {
                    $product = $order->product;
                    if ($product->isNewRecord) {
                        if ($product->save()) {
                            $order->product_id = $product->id;
                        } else {
                            return false;
                        }
                    }
                }
                if (!$order->save(false)) {
                    return false;
                }
            }

            foreach ($model->removedOrders as $removedOrder) {
                if ($removedOrder->delete() === false) {
                    $model->addError('orders', 'Не удалось удалить позицию из счета.');
                    return false;
                }
            }

            self::afterSave($model->id);

            return true;
        };
        $isNew = $model->isNewRecord;
        if ($isSaved = LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, $isNew ? LogEvent::LOG_EVENT_CREATE : LogEvent::LOG_EVENT_UPDATE, $saveClosure)) {
            self::afterSave($model->id);
            if ($isNew) {
                if (!$model->isAutoinvoice) {
                    if ($model->type == Documents::IO_TYPE_OUT &&
                        $model->has_services &&
                        $model->company->autoact->rule == Autoact::BY_INVOICE
                    ) {
                        $model->createAct(DateTime::createFromFormat('Y-m-d', $model->document_date)->format('d.m.Y'));
                    }
                    // Create Autoplan Flow
                    \frontend\modules\analytics\models\PlanCashContractor::createFlowByInvoice($model);
                }
            } else {
                \frontend\modules\analytics\models\PlanCashContractor::updateFlowByInvoice($model);
            }
        }

        return (boolean)$isSaved;
    }

    /**
     * @param  Invoice $model
     * @param  Autoinvoice|null $autoinvoice
     * @param  array $data
     * @return boolean
     */
    public static function load(Invoice &$model, $data = [], $formName = null)
    {

        if ($model->load($data, $formName)) {

            self::loadPaymentLimitRule($model);

            if ($model->isAutoinvoice) {
                if ($model->auto === null) {
                    $autoinvoice = new Autoinvoice([
                        'company_id' => $model->company->id,
                    ]);
                    $autoinvoice->setNextNumber($model->company->id);
                    $model->populateRelation('auto', $autoinvoice);
                }

                $model->auto->load($data);
                $model->document_number = $model->auto->document_number;
            }

            $model->ordersLoad(ArrayHelper::getValue($data, 'orderArray', []));

            if (!$model->payment_limit_date && $model->payment_limit_rule != Invoice::PAYMENT_LIMIT_RULE_DOCUMENT) {
                $payment_delay = 10;
                $model->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime("+$payment_delay day"));
            }

            $model->total_mass_gross = (string)array_sum(ArrayHelper::getColumn($model->orders, 'mass_gross'));
            $model->total_place_count = (string)array_sum(ArrayHelper::getColumn($model->orders, 'place_count'));

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param  Invoice $model
     * @param  array $data
     * @return boolean
     */
    public static function validate(Invoice &$model)
    {
        if ($model->isNewRecord && !$model->company->createInvoiceAllowed($model->type)) {
            $limit = $model->company->getInvoiceLimit($model->type);
            $model->addError('company', "Для текущего тарифа вы не можете создавать более $limit счетов.");

            return false;
        }
        if ($removedOrdersIdArray = array_keys($model->removedOrders)) {
            $currentOrders = $model->orders;
            $query = (new Query)->select('order_id')->distinct()->where(['order_id' => $removedOrdersIdArray]);
            $forbiddenRemoveIds = [];
            if ($ids = $query->from(OrderPackingList::tableName())->column()) {
                $forbiddenRemoveIds = array_merge($forbiddenRemoveIds, $ids);
                $model->addError('orders', 'Нельзя удалить товары, которые находятся в товарных накладных');
            }
            if ($ids = $query->from(OrderAct::tableName())->column()) {
                $forbiddenRemoveIds = array_merge($forbiddenRemoveIds, $ids);
                $model->addError('orders', 'Нельзя удалить позиции, которые находятся в актах');
            }
            if ($ids = $query->from(OrderInvoiceFacture::tableName())->column()) {
                $forbiddenRemoveIds = array_merge($forbiddenRemoveIds, $ids);
                $model->addError('orders', 'Нельзя удалить позиции, которые находятся в СФ');
            }
            if ($ids = $query->from(OrderUpd::tableName())->column()) {
                $forbiddenRemoveIds = array_merge($forbiddenRemoveIds, $ids);
                $model->addError('orders', 'Нельзя удалить позиции, которые находятся в УПД');
            }

            if ($forbiddenRemoveIds) {
                $forbiddenRemoveOrders = $model->getOrders()->andWhere(['id' => array_unique($forbiddenRemoveIds)])->all();
                $orders = array_merge($currentOrders, $forbiddenRemoveOrders);
                $model->populateRelation('orders', $orders);
            }
        }

        if ($model->isAutoinvoice) {
            if ($model->auto === null) {
                $model->addError('auto', 'Ошибка создания АвтоСчета');
            } elseif (!$model->auto->validate()) {
                $model->addErrors($model->auto->getErrors());
            }
        }

        foreach ($model->orders as $order) {
            if (!$order->validate()) {
                $errors = $order->getFirstErrors();
                $model->addError('orders', reset($errors));
            }
        }

        $model->validate(null, false);

        return !$model->hasErrors();
    }

    /**
     * @inheritdoc
     */
    public static function afterSave(int $id)
    {
        $invoice = Invoice::findOne($id);

        // New auto_invoices & surcharge_invoices
        if (!$invoice || $invoice->is_surcharge) {
            return;
        }

        Yii::$app->db->createCommand("
            UPDATE {{invoice}}
            SET
                {{invoice}}.[[can_add_act]] = IF({{invoice}}.[[has_services]] = 1, (
                    SELECT IFNULL(SUM({{order}}.[[quantity]]), 0)
                    FROM {{order}}
                    INNER JOIN {{product}} ON {{order}}.[[product_id]] = {{product}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = :id
                    AND {{product}}.[[production_type]] = :service
                ) > (
                    SELECT IFNULL(SUM({{order_act}}.[[quantity]]), 0)
                    FROM {{order}}
                    INNER JOIN {{product}} ON {{order}}.[[product_id]] = {{product}}.[[id]]
                    INNER JOIN {{order_act}} ON {{order_act}}.[[order_id]] = {{order}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = :id
                    AND {{product}}.[[production_type]] = :service
                ), 0),
                {{invoice}}.[[can_add_packing_list]] = IF({{invoice}}.[[has_goods]], (
                    SELECT IFNULL(SUM({{order}}.[[quantity]]), 0)
                    FROM {{order}}
                    INNER JOIN {{product}} ON {{order}}.[[product_id]] = {{product}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = :id
                    AND {{product}}.[[production_type]] = :goods
                ) > (
                    SELECT IFNULL(SUM({{order_packing_list}}.[[quantity]]), 0)
                    FROM {{order}}
                    INNER JOIN {{product}} ON {{order}}.[[product_id]] = {{product}}.[[id]]
                    INNER JOIN {{order_packing_list}} ON {{order_packing_list}}.[[order_id]] = {{order}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = :id
                    AND {{product}}.[[production_type]] = :goods
                ), 0),
                {{invoice}}.[[can_add_proxy]] = IF({{invoice}}.[[has_goods]], (
                    SELECT IFNULL(SUM({{order}}.[[quantity]]), 0) FROM {{order}}
                    INNER JOIN {{product}} ON {{order}}.[[product_id]] = {{product}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = :id
                    AND {{product}}.[[production_type]] = :goods
                ) > (
                    SELECT IFNULL(SUM({{order_proxy}}.[[quantity]]), 0) FROM {{order_proxy}}
                    INNER JOIN {{proxy}} ON {{order_proxy}}.[[proxy_id]] = {{proxy}}.[[id]]
                    WHERE {{proxy}}.[[invoice_id]] = :id
                ), 0),
                {{invoice}}.[[can_add_waybill]] = IF({{invoice}}.[[has_goods]], (
                    SELECT IFNULL(SUM({{order}}.[[quantity]]), 0) FROM {{order}}
                    INNER JOIN {{product}} ON {{order}}.[[product_id]] = {{product}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = {{invoice}}.[[id]] AND {{product}}.[[production_type]] = :goods
                ) > (
                    SELECT IFNULL(SUM({{order_waybill}}.[[quantity]]), 0) FROM {{order_waybill}}
                    INNER JOIN {{waybill}} ON {{order_waybill}}.[[waybill_id]] = {{waybill}}.[[id]]
                    WHERE {{waybill}}.[[invoice_id]] = {{invoice}}.[[id]]
                ), 0),                
                {{invoice}}.[[can_add_invoice_facture]] = (
                    SELECT IFNULL(SUM({{order}}.[[quantity]]), 0) FROM {{order}}
                    WHERE {{order}}.[[invoice_id]] = :id
                ) > (
                    SELECT IFNULL(SUM({{order_invoice_facture}}.[[quantity]]), 0)
                    FROM {{order}}
                    INNER JOIN {{order_invoice_facture}} ON {{order_invoice_facture}}.[[order_id]] = {{order}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = :id
                ),
                {{invoice}}.[[can_add_upd]] = (
                    SELECT IFNULL(SUM({{order}}.[[quantity]]), 0)
                    FROM {{order}}
                    WHERE {{order}}.[[invoice_id]] = :id
                ) > (
                    SELECT IFNULL(SUM({{order_upd}}.[[quantity]]), 0)
                    FROM {{order}}
                    INNER JOIN {{order_upd}} ON {{order_upd}}.[[order_id]] = {{order}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = :id
                )
            WHERE {{invoice}}.[[id]] = :id;
        ", [
            ':id' => $id,
            ':service' => Product::PRODUCTION_TYPE_SERVICE,
            ':goods' => Product::PRODUCTION_TYPE_GOODS,
        ])->execute();
    }

    /**
     * @inheritdoc
     */
    public static function checkForAct($invoiceID, $allInvoices = [])
    {
        Yii::$app->db->createCommand('
            UPDATE {{invoice}}
            SET {{invoice}}.[[can_add_act]] = (
                    SELECT IFNULL(SUM({{order}}.[[quantity]]), 0) FROM {{order}}
                    INNER JOIN {{product}} ON {{order}}.[[product_id]] = {{product}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = :id
                    AND {{product}}.[[production_type]] = :type
                ) > (
                    SELECT IFNULL(SUM({{order_act}}.[[quantity]]), 0)
                    FROM {{order}}
                    INNER JOIN {{product}} ON {{order}}.[[product_id]] = {{product}}.[[id]]
                    INNER JOIN {{order_act}} ON {{order_act}}.[[order_id]] = {{order}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = :id
                    AND {{product}}.[[production_type]] = :type
                ),
                {{invoice}}.[[has_act]] = EXISTS(
                    SELECT {{order_act}}.[[act_id]]
                    FROM {{order}}
                    INNER JOIN {{product}} ON {{order}}.[[product_id]] = {{product}}.[[id]]
                    INNER JOIN {{order_act}} ON {{order_act}}.[[order_id]] = {{order}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = :id
                    AND {{product}}.[[production_type]] = :type
                )
            WHERE {{invoice}}.[[id]] IN (:id)
        ', [
            ':id' => $invoiceID,
            ':type' => Product::PRODUCTION_TYPE_SERVICE,
        ])->execute();

        self::checkForMaxDocumentDate($invoiceID);
    }

    /**
     * @inheritdoc
     */
    public static function checkForPackingList($id)
    {
        Yii::$app->db->createCommand('
            UPDATE {{invoice}}
            SET {{invoice}}.[[can_add_packing_list]] = (
                    SELECT IFNULL(SUM({{order}}.[[quantity]]), 0) FROM {{order}}
                    INNER JOIN {{product}} ON {{order}}.[[product_id]] = {{product}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = {{invoice}}.[[id]]
                    AND {{product}}.[[production_type]] = :type
                ) > (
                    SELECT IFNULL(SUM({{order_packing_list}}.[[quantity]]), 0)
                    FROM {{order}}
                    INNER JOIN {{product}} ON {{order}}.[[product_id]] = {{product}}.[[id]]
                    INNER JOIN {{order_packing_list}} ON {{order_packing_list}}.[[order_id]] = {{order}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = :id
                    AND {{product}}.[[production_type]] = :type
                ),
                {{invoice}}.[[has_packing_list]] = EXISTS(
                    SELECT {{order_packing_list}}.[[packing_list_id]]
                    FROM {{order}}
                    INNER JOIN {{product}} ON {{order}}.[[product_id]] = {{product}}.[[id]]
                    INNER JOIN {{order_packing_list}} ON {{order_packing_list}}.[[order_id]] = {{order}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = :id
                    AND {{product}}.[[production_type]] = :type
                )
            WHERE {{invoice}}.[[id]] = :id
        ', [
            ':id' => $id,
            ':type' => Product::PRODUCTION_TYPE_GOODS,
        ])->execute();

        self::checkForMaxDocumentDate($id);
    }

    /**
     * @inheritdoc
     */
    public static function checkForSalesInvoice($id)
    {
        Yii::$app->db->createCommand('
            UPDATE {{invoice}}
            SET {{invoice}}.[[can_add_sales_invoice]] = (
                    SELECT IFNULL(SUM({{order}}.[[quantity]]), 0) FROM {{order}}
                    INNER JOIN {{product}} ON {{order}}.[[product_id]] = {{product}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = :id
                    AND {{product}}.[[production_type]] = :type
                ) > (
                    SELECT IFNULL(SUM({{order_sales_invoice}}.[[quantity]]), 0) FROM {{order_sales_invoice}}
                    INNER JOIN {{sales_invoice}} ON {{order_sales_invoice}}.[[sales_invoice_id]] = {{sales_invoice}}.[[id]]
                    WHERE {{sales_invoice}}.[[invoice_id]] = :id
                ),
                {{invoice}}.[[has_sales_invoice]] = EXISTS(
                    SELECT {{sales_invoice}}.[[id]]
                    FROM {{sales_invoice}}
                    WHERE {{sales_invoice}}.[[invoice_id]] = :id
                )
            WHERE {{invoice}}.[[id]] = :id
        ', [
            ':id' => $id,
            ':type' => Product::PRODUCTION_TYPE_GOODS,
        ])->execute();
    }

    /**
     * @inheritdoc
     */
    public static function checkForWaybill($id)
    {
        Yii::$app->db->createCommand('
            UPDATE {{invoice}}
            SET {{invoice}}.[[can_add_waybill]] = (
                    SELECT IFNULL(SUM({{order}}.[[quantity]]), 0) FROM {{order}}
                    INNER JOIN {{product}} ON {{order}}.[[product_id]] = {{product}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = {{invoice}}.[[id]] AND {{product}}.[[production_type]] = :type
                ) > (
                    SELECT IFNULL(SUM({{order_waybill}}.[[quantity]]), 0) FROM {{order_waybill}}
                    INNER JOIN {{waybill}} ON {{order_waybill}}.[[waybill_id]] = {{waybill}}.[[id]]
                    WHERE {{waybill}}.[[invoice_id]] = {{invoice}}.[[id]]
                ),
                {{invoice}}.[[has_waybill]] = EXISTS(
                    SELECT {{waybill}}.[[id]] FROM {{waybill}} WHERE {{waybill}}.[[invoice_id]] = {{invoice}}.[[id]]
                )
            WHERE {{invoice}}.[[id]] = :id
        ', [
            ':id' => $id,
            ':type' => Product::PRODUCTION_TYPE_GOODS,
        ])->execute();
    }

    /**
     * @inheritdoc
     */
    public static function checkForProxy($id)
    {
        Yii::$app->db->createCommand('
            UPDATE {{invoice}}
            SET {{invoice}}.[[can_add_proxy]] = (
                    SELECT IFNULL(SUM({{order}}.[[quantity]]), 0) FROM {{order}}
                    INNER JOIN {{product}} ON {{order}}.[[product_id]] = {{product}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = :id
                    AND {{product}}.[[production_type]] = :type
                ) > (
                    SELECT IFNULL(SUM({{order_proxy}}.[[quantity]]), 0) FROM {{order_proxy}}
                    INNER JOIN {{proxy}} ON {{order_proxy}}.[[proxy_id]] = {{proxy}}.[[id]]
                    WHERE {{proxy}}.[[invoice_id]] = :id
                ),
                {{invoice}}.[[has_proxy]] = EXISTS(
                    SELECT {{proxy}}.[[id]] FROM {{proxy}} WHERE {{proxy}}.[[invoice_id]] = :id
                )
            WHERE {{invoice}}.[[id]] = :id
        ', [
            ':id' => $id,
            ':type' => Product::PRODUCTION_TYPE_GOODS,
        ])->execute();
    }

    /**
     * @inheritdoc
     */
    public static function checkForInvoiceFacture($id, $allInvoices = [])
    {
        Yii::$app->db->createCommand('
            UPDATE {{invoice}}
            SET {{invoice}}.[[can_add_invoice_facture]] = (
                    SELECT IFNULL(SUM({{order}}.[[quantity]]), 0) FROM {{order}}
                    WHERE {{order}}.[[invoice_id]] = :id
                ) > (
                    SELECT IFNULL(SUM({{order_invoice_facture}}.[[quantity]]), 0)
                    FROM {{order}}
                    INNER JOIN {{order_invoice_facture}} ON {{order_invoice_facture}}.[[order_id]] = {{order}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = :id
                ),
                {{invoice}}.[[has_invoice_facture]] = EXISTS(
                    SELECT {{order_invoice_facture}}.[[invoice_facture_id]]
                    FROM {{order}}
                    INNER JOIN {{order_invoice_facture}} ON {{order_invoice_facture}}.[[order_id]] = {{order}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = :id
                )
            WHERE {{invoice}}.[[id]] = :id
        ', [
            ':id' => $id,
        ])->execute();
    }

    /**
     * @inheritdoc
     */
    public static function checkForUpd($id, $allInvoices = [])
    {
        Yii::$app->db->createCommand('
            UPDATE {{invoice}}
            SET {{invoice}}.[[can_add_upd]] = (
                    SELECT IFNULL(SUM({{order}}.[[quantity]]), 0)
                    FROM {{order}}
                    WHERE {{order}}.[[invoice_id]] = {{invoice}}.[[id]]
                ) > (
                    SELECT IFNULL(SUM({{order_upd}}.[[quantity]]), 0)
                    FROM {{order}}
                    INNER JOIN {{order_upd}} ON {{order_upd}}.[[order_id]] = {{order}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = :id
                ),
                {{invoice}}.[[has_upd]] = EXISTS(
                    SELECT {{order_upd}}.[[upd_id]]
                    FROM {{order}}
                    INNER JOIN {{order_upd}} ON {{order_upd}}.[[order_id]] = {{order}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = :id
                )
            WHERE {{invoice}}.[[id]] = :id
        ', [
            ':id' => $id,
        ])->execute();

        self::checkForMaxDocumentDate($id);
    }

    public static function checkForMaxDocumentDate($id)
    {
        Yii::$app->db->createCommand('
            UPDATE {{invoice}}
            SET {{invoice}}.[[max_related_document_date]] = (
                SELECT MAX(p.`document_date`) AS `max_date` FROM `packing_list` p WHERE p.`invoice_id` = :id
                UNION ALL
                SELECT MAX(a.`document_date`) AS `max_date` FROM `act` a LEFT JOIN `invoice_act` r ON a.`id` = r.`act_id` WHERE r.`invoice_id` = :id
                UNION ALL
                SELECT MAX(u.`document_date`) AS `max_date` FROM `upd` u LEFT JOIN `invoice_upd` r ON u.`id` = r.`upd_id` WHERE r.`invoice_id` = :id
                ORDER BY `max_date` DESC
                LIMIT 1
            )
            WHERE {{invoice}}.[[id]] = :id
        ', [
            ':id' => $id,
        ])->execute();

        Yii::$app->db->createCommand('
            UPDATE {{invoice}}
            SET {{invoice}}.[[related_document_payment_limit_date]] = IF({{invoice}}.[[max_related_document_date]] IS NULL, NULL, DATE_ADD({{invoice}}.[[max_related_document_date]], INTERVAL {{invoice}}.[[related_document_payment_limit_days]] DAY))
            WHERE {{invoice}}.[[id]] = :id
        ', [
            ':id' => $id,
        ])->execute();
    }

    /**
     * @inheritdoc
     */
    public static function getTotalWeight(Invoice $invoice)
    {
        $total = 0;
        foreach ($invoice->orders as $order) {
            $total += bcmul($order->quantity, $order->weight,$invoice->weightPrecision);
        }

        return $total;
    }

    /**
     * @inheritdoc
     */
    public static function getTotalVolume(Invoice $invoice)
    {
        $total = 0;
        foreach ($invoice->orders as $order) {
            $total += bcmul($order->quantity, $order->volume,$invoice->volumePrecision);
        }

        return $total;
    }
    
    public static function getFloatPaymentLimitDate(Invoice $invoice)
    {
        switch ($invoice->payment_limit_rule) {
            case Invoice::PAYMENT_LIMIT_RULE_MIXED:
                return
                    $invoice->related_document_payment_limit_date
                        ?: $invoice->payment_limit_date;
            case Invoice::PAYMENT_LIMIT_RULE_DOCUMENT:
                return
                    $invoice->related_document_payment_limit_date;
            case Invoice::PAYMENT_LIMIT_RULE_INVOICE:
            default:
                return
                    $invoice->payment_limit_date;
        }
    }

    private static function loadPaymentLimitRule(Invoice &$model)
    {
        $postInvoice = Yii::$app->request->post('Invoice');
        $postPaymentLimitRules = Yii::$app->request->post('paymentLimitRule');
        $paymentLimitRule = ArrayHelper::getValue($postPaymentLimitRules, $postInvoice['payment_limit_rule'] ?? null, []);
        switch ($postInvoice['payment_limit_rule'] ?? null) {
            case Invoice::PAYMENT_LIMIT_RULE_MIXED:
                $model->payment_limit_days = ArrayHelper::getValue($paymentLimitRule, 'payment_limit_days');
                $model->payment_limit_percent = ArrayHelper::getValue($paymentLimitRule, 'payment_limit_percent');
                $model->related_document_payment_limit_days = ArrayHelper::getValue($paymentLimitRule, 'related_document_payment_limit_days');
                if ($d = date_create($model->document_date)) {
                    $limitDays = intval($model->payment_limit_days) ?: 10;
                    $model->payment_limit_date = $d->modify("+{$limitDays} day")->format(DateHelper::FORMAT_DATE);
                }
                break;
            case Invoice::PAYMENT_LIMIT_RULE_DOCUMENT:
                $model->payment_limit_date = null;
                $model->payment_limit_days = 0;
                $model->payment_limit_percent = 0;
                $model->related_document_payment_limit_days = ArrayHelper::getValue($paymentLimitRule, 'related_document_payment_limit_days');
                break;
            case Invoice::PAYMENT_LIMIT_RULE_INVOICE:
            default:
                $model->payment_limit_days = ArrayHelper::getValue($paymentLimitRule, 'payment_limit_days', 10);
                $model->payment_limit_percent = 100;
                $model->related_document_payment_limit_days = 0;
                break;
        }

        $model->related_document_payment_limit_percent = max(0, 100 - $model->payment_limit_percent);

        if (!$model->isNewRecord) {
            if ($maxDocumentDate = $model->max_related_document_date) {
                try {
                    $limitDays = intval($model->related_document_payment_limit_days) ?: 10;
                    $paymentLimitDocumentDate = date_create($maxDocumentDate)->modify("+ {$limitDays} day")->format('Y-m-d');
                } catch (\Exception $e) {
                    $paymentLimitDocumentDate = null;
                    Yii::error(VarDumper::dumpAsString($e), 'validation');
                }

                $model->related_document_payment_limit_date = $paymentLimitDocumentDate;
            } else {

                $model->related_document_payment_limit_date = null;
            }
        }
    }
}
