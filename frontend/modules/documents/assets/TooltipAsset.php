<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\modules\documents\assets;

use yii\web\AssetBundle;

/**
 * Improved performance but broken styles. Not used yet
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class TooltipAsset extends AssetBundle
{

    /**
     * @var string
     */
    public $sourcePath = '@bower/tooltipster/dist';

    /**
     * @var array
     */
    public $css = [
        'css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-noir.min.css',
    ];
    /**
     * @var array
     */
    public $depends = [
        'philippfrenzel\yii2tooltipster\yii2tooltipsterAsset',
    ];
}
