<?php

namespace frontend\modules\documents\assets;

use yii\web\AssetBundle;

/**
 * DocumentFileScanAsset
 */
class SideMenuDocumentsAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $basePath = '@webroot';

    /**
     * @var array
     */
    public $css = [
        'css/side-menu-documents.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'scripts/side-menu-documents.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'frontend\assets\AppAsset',
    ];
}
