<?php

namespace frontend\modules\documents\assets;

use yii\web\AssetBundle;

/**
 * DocumentFileScanAsset
 */
class DocumentFileScanAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $basePath = '@webroot';

    /**
     * @var array
     */
    public $css = [
        'css/document-file-scan.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'scripts/document-file-scan.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'frontend\assets\AppAsset',
    ];
}
