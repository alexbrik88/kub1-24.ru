<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\modules\documents\assets;

use yii\web\AssetBundle;

/**
 * Improved performance but broken styles. Not used yet
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class InvoicePrintAsset extends AssetBundle
{

    /**
     * @var string
     */
    public $basePath = '@webroot';

    /**
     * @var array
     */
    public $css = [
        'css/print/documents-invoice.css',
    ];
    /**
     * @var array
     */
    public $depends = [
        'frontend\modules\documents\assets\DocumentPrintAsset',
    ];
}
