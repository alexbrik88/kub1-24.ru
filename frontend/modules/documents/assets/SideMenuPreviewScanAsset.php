<?php

namespace frontend\modules\documents\assets;

use yii\web\AssetBundle;

/**
 * SideMenuPreviewScanAsset
 */
class SideMenuPreviewScanAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $basePath = '@webroot';

    /**
     * @var array
     */
    public $css = [
        'css/side-menu-preview-scan.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'scripts/side-menu-preview-scan.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'common\assets\CommonAsset',
    ];
}
