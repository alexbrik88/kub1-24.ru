<?php

namespace frontend\modules\analytics\controllers;

use frontend\rbac\UserRole;
use common\modules\analytics\AnalyticsManager;
use frontend\modules\analytics\models\AnalyticsSimpleSearch;
use Yii;
use frontend\components\FrontendController;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use frontend\components\PageSize;
use common\components\filters\AccessControl;
use common\components\date\DateHelper;
use common\components\debts\DebtsHelper;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use common\models\company\CompanyFirstEvent;
use common\models\company\CompanyPrepaymentWallets;
use common\models\document\Act;
use common\models\document\GoodsCancellation;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\OrderAct;
use common\models\document\PackingList;
use common\models\document\query\InvoiceQuery;
use common\models\document\Upd;
use common\models\employee\Config;
use common\models\employee\Employee;
use common\modules\cards\models\CardOperation;
use frontend\components\BusinessAnalyticsAccess;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use common\modules\acquiring\models\AcquiringOperation;
use frontend\modules\analytics\models\AnalyticsArticleDefaults;
use frontend\modules\analytics\models\AnalyticsArticleForm;
use frontend\modules\analytics\models\AnalyticsOptions;
use frontend\modules\analytics\models\BreakEvenSearchModel;
use frontend\modules\analytics\models\debtor\DebtorHelper2;
use frontend\modules\analytics\models\OddsSearch;
use frontend\modules\analytics\models\operationalEfficiency\OperationalEfficiencySearchModel;
use frontend\modules\analytics\models\StocksSearch;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\modules\analytics\models\BalanceSearch;
use frontend\modules\analytics\models\DebtReportSearch2;
use frontend\modules\analytics\models\ExpensesSearch;
use frontend\modules\analytics\models\FlowOfFundsReportSearch;
use frontend\modules\analytics\models\IncomeSearch;
use frontend\modules\analytics\models\PaymentCalendarSearch;
use frontend\modules\analytics\models\PlanCashContractor;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\modules\analytics\models\PlanCashRule;
use frontend\modules\analytics\models\ProfitAndLossCompanyItem;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;
use frontend\modules\analytics\models\ProfitAndLossSearchItemsModel;
use frontend\modules\reference\models\ArticleDropDownForm;
use frontend\modules\reference\models\ArticlesSearch;
use frontend\modules\reference\models\BalanceArticlesSearch;
use common\models\balance\BalanceArticle;
use common\models\product\Product;
use frontend\modules\analytics\models\balanceInitial\BalanceArticleInitialSearch;
use frontend\modules\analytics\models\balanceInitial\BalanceStoreInitialSearch;

/**
 * Class FinanceAjaxController
 */
class FinanceAjaxController extends FrontendController
{
    const VIEW = '@frontend/modules/analytics/views/finance/';
    static $monthsByQuarter = [1 => [1,3], 2 => [4,6], 3 => [7,9], 4 => [10,12]];

    public $enableCsrfValidation = false;

    /**
     * @var ProfitAndLossSearchItemsModel
     */
    private $itemsSearchModel;
    /**
     * @var ActiveDataProvider
     */
    private $itemsDataProvider;
    /**
     * @var string
     */
    private $itemsSearchBy;
    /**
     * @var boolean
     */
    private $isItemsSearchByChanged;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'update-profit-and-loss-articles',
                        ],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF, UserRole::ROLE_ACCOUNTANT],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'create-plan-item',
                            'update-plan-item',
                            'delete-flow-item',
                            'many-delete-flow-item',
                            'many-delete-doc-item',
                            'many-flow-item',
                            'many-flow-date',
                            'many-flow-recognition-date',
                            'many-doc-item',
                            'add-article-to-break-even',
                            'update-prepayment-articles',
                            'reset-profit-and-loss-articles-options'
                        ],
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::ADMIN,
                        ],
                        'matchCallback' => function($rule, $action) {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_FINANCE);
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'get-profit-and-loss-items',
                            'payment-calendar-part',
                            'odds-part',
                            'expenses-part',
                            'income-part',
                            'profit-and-loss-part',
                            'get-plan-fact-data',
                            'get-balance-data',
                            'get-balance-structure-data',
                            'get-balance-article-grid',
                            'get-balance-store-grid',
                            'get-balance-stacked-data',
                            'get-expense-charts-data',
                            'get-income-charts-data',
                            'get-profit-loss-charts-data',
                            'get-debtor-charts-data',
                            'get-stocks-products-by-group',
                            'get-stocks-chart-data',
                            'get-operational-efficiency-charts-data',
                            'get-industry-compare-data',
                            'get-project-compare-data'
                        ],
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function($rule, $action) {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_FINANCE);
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function actionGetStocksProductsByGroup()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $target = Yii::$app->request->post('target', "");
        $groupId = (int)str_replace('group_', '', $target);
        if (!($groupId))
            throw new Exception('Group not found');

        $searchModel = new StocksSearch();
        $searchModel->year = Yii::$app->request->post('year', date('Y'));
        $searchModel->title = Yii::$app->request->post('title', null);
        $searchModel->byQuantity = Yii::$app->request->post('quantity', false);

        return $this->render(self::VIEW . '_partial/stocks_table_products_ajax', [
            'groupId' => $groupId,
            'products' => $searchModel->searchProductsByGroup($groupId),
            'floorMap' => Yii::$app->request->post('floorMap', []),
            'isCurrentYear' => $searchModel->isCurrentYear
        ]);
    }

    /**
     * @return bool|string
     * @throws \Exception
     */
    public function actionGetProfitAndLossItems()
    {
        // raw data
        $searchBy = Yii::$app->request->get('searchBy');
        $enableToggle = false; //Yii::$app->request->get('enableToggle', false);
        $year = Yii::$app->request->get('year', date('Y'));
        $quarter = Yii::$app->request->get('quarter', null);
        $month =  Yii::$app->request->get('month', null);
        $article = Yii::$app->request->get('article', '-');
        $pageFilter = Yii::$app->request->get('pageFilter', []);
        $tdFilter = Yii::$app->request->get('tdFilter', []);

        if (!in_array($searchBy, ['docs', 'flows']))
            throw new \Exception('Page not found.');

        // calc data
        if ($quarter >= 1 && $quarter <= 4) {
            $monthStart = self::$monthsByQuarter[$quarter][0];
            $monthEnd   = self::$monthsByQuarter[$quarter][1];
        } elseif ($month >= 1 && $month <= 12) {
            $monthStart = $monthEnd = $month;
        } else {
            $monthStart = 1;
            $monthEnd = 12;
        }

        // needed data
        $periodStart = $year . '-' . str_pad($monthStart, 2, '0', STR_PAD_LEFT) . '-' . '01';
        $periodEnd = $year . '-' . str_pad($monthEnd, 2, '0', STR_PAD_LEFT) . '-' . cal_days_in_month(CAL_GREGORIAN, $monthEnd, $year);

        $this->itemsSearchModel = new ProfitAndLossSearchItemsModel();
        $this->itemsSearchBy = $searchBy;

        //////////////////////////////////////////////////////////////////////
        $filter = [
            'industry_id' => null,
            'sale_point_id' => null,
            'project_id' => null
        ];
        foreach ($filter as $k => $f) {
            $filter[$k] = ($pageFilter[$k] ?? null) ?: ($tdFilter[$k] ?? null);
        }
        //////////////////////////////////////////////////////////////////////

        if ($searchBy == 'flows') {
            $this->itemsDataProvider = $this->_getProfitAndLossItemsByFlow($article, $periodStart, $periodEnd, $filter);
            if ($this->itemsDataProvider->totalCount == 0 && $enableToggle) {
                $this->itemsSearchBy = 'docs';
                $this->isItemsSearchByChanged = true;
                $this->itemsSearchModel = new ProfitAndLossSearchItemsModel();
                $this->itemsDataProvider = $this->_getProfitAndLossItemsByDoc($article, $periodStart, $periodEnd, $filter);
            }
        } elseif ($searchBy == 'docs') {
            $this->itemsDataProvider = $this->_getProfitAndLossItemsByDoc($article, $periodStart, $periodEnd, $filter);
            if ($this->itemsDataProvider->totalCount == 0 && $enableToggle) {
                $this->itemsSearchBy = 'flows';
                $this->isItemsSearchByChanged = true;
                $this->itemsSearchModel = new ProfitAndLossSearchItemsModel();
                $this->itemsDataProvider = $this->_getProfitAndLossItemsByFlow($article, $periodStart, $periodEnd, $filter);
            }
        }

        $this->itemsDataProvider->pagination->pageSize = PageSize::get('per-page', 50);

        return $this->renderAjax(self::VIEW . '_partial/profit_and_loss_items_' . $this->itemsSearchBy, [
            'searchModel' => $this->itemsSearchModel,
            'itemsDataProvider' => $this->itemsDataProvider,
            'searchBy' => $this->itemsSearchBy,
            'isSearchByChanged' => $this->isItemsSearchByChanged ? $this->itemsSearchBy : false
        ]);
    }

    private function _getProfitAndLossItemsByDoc($article, $periodStart, $periodEnd, $pageFilter = [])
    {
        @list($flowType, $itemId, $palTypeId) = explode('-', $article);

        if ($flowType && $itemId) {

            $searchModel = &$this->itemsSearchModel;
            $searchModel->ioType = ($flowType == 'income') ? Documents::IO_TYPE_OUT : Documents::IO_TYPE_IN;
            $searchModel->accountingOperationsOnly = Yii::$app->request->post('accountingOperationsOnly');
            $searchModel->pal_type_id = $palTypeId;
            $searchModel->pageFilters = $pageFilter;

            if ($flowType == 'income') {
                if (in_array($itemId, ['industry_id', 'sale_point_id', 'contractor_id', 'project_id', 'product_id', 'product_group_id'])) {
                    $searchModel->isRevenue = true;
                    $searchModel->{$itemId} = explode('_', $palTypeId); // третий параметр = строка id1_id2_id3
                } elseif ($itemId == 'revenue_doc') {
                    $searchModel->isRevenue = true;
                } else {
                    $searchModel->income_item_id = $itemId;
                }
            } elseif ($flowType == 'expense') {
                if ($itemId == InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT) {
                    $searchModel->onlyNotForSaleProducts = true;
                    $searchModel->expenditure_item_id = $itemId;
                } elseif (in_array($itemId, [InvoiceExpenditureItem::ITEM_PRIME_COST_PRODUCT, InvoiceExpenditureItem::ITEM_PRIME_COST_SERVICE])) {
                    $searchModel->expenditure_item_id = null;
                    $searchModel->isRevenue = true;
                    $searchModel->isPrimeCosts = true;
                    $searchModel->primeCostsArticle = $itemId;
                    $searchModel->ioType = Documents::IO_TYPE_OUT;
                } else {
                    $searchModel->expenditure_item_id = $itemId;
                }
            }

            return $searchModel->searchItemsByDocs($periodStart, $periodEnd);
        }

        throw new \yii\db\Exception('Page not found!');
    }

    private function _getProfitAndLossItemsByFlow($article, $periodStart, $periodEnd, $pageFilter = [])
    {
        @list($flowType, $itemId, $palTypeId) = explode('-', $article);

        if ($flowType && $itemId) {
            $searchModel = &$this->itemsSearchModel;
            $searchModel->accountingOperationsOnly = Yii::$app->request->post('accountingOperationsOnly');
            $searchModel->pal_type_id = $palTypeId;
            $searchModel->pageFilters = $pageFilter;

            if ($flowType == 'income') {
                if (in_array($itemId, ['industry_id', 'sale_point_id', 'contractor_id', 'project_id'])) {
                    $searchModel->isRevenue = true;
                    $searchModel->{$itemId} = explode('_', $palTypeId); // третий параметр = строка id1_id2_id3
                } elseif ($itemId == 'revenue_flow') {
                    $searchModel->isRevenue = true;
                } else {
                    $searchModel->income_item_id = $itemId;
                }
            } elseif ($flowType == 'expense') {

                if ($itemId == InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT) {
                    $searchModel->onlyNotForSaleProducts = true;
                    $searchModel->expenditure_item_id = $itemId;
                } elseif (in_array($itemId, [InvoiceExpenditureItem::ITEM_PRIME_COST_PRODUCT, InvoiceExpenditureItem::ITEM_PRIME_COST_SERVICE])) {
                    $searchModel->expenditure_item_id = null;
                    $searchModel->isRevenue = true;
                    $searchModel->isPrimeCosts = true;
                    $searchModel->primeCostsArticle = $itemId;
                    $searchModel->ioType = Documents::IO_TYPE_OUT;
                } else {
                    $searchModel->expenditure_item_id = $itemId;
                }

            }

            return $searchModel->searchItemsByFlows($periodStart, $periodEnd);
        }

        throw new \yii\db\Exception('Page not found!');
    }

    /**
     * @param int $activeTab
     * @return mixed|string|Response
     * @throws \Throwable
     */
    public function actionCreatePlanItem($activeTab = PaymentCalendarSearch::TAB_BY_PURSE)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var Company $company */
        $company = Yii::$app->user->identity->company;
        $checkingAccountant = $company->mainCheckingAccountant;
        $cashbox = $company->getCashboxes()->andWhere(['is_main' => true])->one();
        $emoney = $company->getEmoneys()->andWhere(['is_main' => true])->one();

        $model = new PlanCashFlows();
        $model->company_id = $company->id;
        $model->flow_type = CashFlowsBase::FLOW_TYPE_INCOME;
        $model->checking_accountant_id = ($checkingAccountant) ? $checkingAccountant->id : null;
        $model->cashbox_id = ($cashbox) ? $cashbox->id : null;
        $model->emoney_id = ($emoney) ? $emoney->id : null;

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->_save())
                return [
                    'success' => 'true',
                    'msg' => ($model->is_repeated ? 'Плановые операции добавлены' : 'Плановая операциия добавлена')];
            else
                return [
                    'success' => false,
                    'msg' => 'Ошибка при создании '.($model->is_repeated ? 'плановых операций' : 'плановой операции')];
        }

        // todo: not needed
        $subTab = Yii::$app->request->get('subTab');
        $year = Yii::$app->request->get('year', date('Y'));

        return $this->renderAjax('_partial/plan_item_form', [
            'model' => $model,
            'company' => $company,
            'activeTab' => $activeTab,
            'subTab' => $subTab,
            'year' => $year,
        ]);
    }

    /**
     * @param $id
     * @param int $activeTab
     * @return array|mixed|string
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionUpdatePlanItem($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $company = Yii::$app->user->identity->company;
        $model = PlanCashFlows::findOne($id);
        if ($model == null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->_update())
                return ['success' => true, 'msg' => 'Плановая операция обновлена.'];
            else
                return ['success' => false, 'msg' => 'Ошибка при обновлении плановой операции.'];
        }

        return $this->renderAjax(self::VIEW . '_partial/plan_item_form', [
            'model' => $model,
            'company' => $company,
        ]);
    }

    /**
     * @param $id
     * @return array
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteFlowItem($id, $tb)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findFlowItem($tb, $id);

        if ($model == null) {
            return ['success' => 'false', 'msg' => 'The requested page does not exist.'];
        }

        if ($model->delete()) {
            return ['success' => 'true', 'msg' => ($tb == 'plan_cash_flow' ? 'Плановая операция' : 'Операция') . ' удалена.'];
        }

        return ['success' => 'false', 'msg' => 'Произошла ошибка при удалении ' . ($tb == 'plan_cash_flow' ? 'плановой' : '') . ' операции.'];
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionManyDeleteFlowItem()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $data = Yii::$app->request->post('flowId', []);

        foreach ($data as $tb => $idArray) {
            foreach ($idArray as $id) {
                if ($model = $this->findFlowItem($tb, $id, true))
                    $model->delete();
            }
        }

        return ['success' => 'true', 'msg' => 'Операции удалены.'];
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionManyDeleteDocItem()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $data = Yii::$app->request->post('docId', []);

        foreach ($data as $tb => $idArray) {
            foreach ($idArray as $id) {
                $model = $this->findDocItem($tb, $id);
                switch($tb) {
                    case 'act':
                        $invoices = $model->invoices;
                        OrderAct::deleteByAct($model->id);
                        LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT);
                        foreach ($invoices as $invoice)
                            InvoiceHelper::checkForAct($invoice->id, $invoices);
                        break;
                    case 'packing_list':
                        $invoice = $model->invoice;
                        LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT);
                        InvoiceHelper::checkForPackingList($invoice->id);
                        break;
                    case 'upd':
                        $invoices = $model->invoices;
                        LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT);
                        foreach ($invoices as $invoice) InvoiceHelper::checkForUpd($invoice->id, $invoices);
                        break;
                    case 'goods_cancellation':
                        LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT);
                        break;
                }
            }
        }

        return ['success' => 'true', 'msg' => 'Операции удалены.'];
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionManyFlowItem()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $data = Yii::$app->request->post('flowId', []);
        $incomeItemID = Yii::$app->request->post('incomeItemIdManyItem');
        $expenseItemID = Yii::$app->request->post('expenditureItemIdManyItem');
        foreach ($data as $tb => $idArray) {
            foreach ($idArray as $id) {
                $model = $this->findFlowItem($tb, $id);
                $attribute = $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? 'income_item_id' : 'expenditure_item_id';
                $value = $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? $incomeItemID : $expenseItemID;
                if ($model instanceof CashBankFlows) {
                    $model->setScenario('update');
                }
                $model->$attribute = $value;
                if (!$model->save(true, [$attribute])) {
                    return ['success' => false, 'msg' => 'Ошибка при обновлении операций'];
                }
            }
        }

        return ['success' => true, 'msg' => 'Статьи по операциям изменены'];
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionManyFlowDate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $data = Yii::$app->request->post('flowId', []);
        $searchModelData = Yii::$app->request->post('PaymentCalendarSearch');
        $incomeDate = ArrayHelper::getValue($searchModelData, 'incomeManyDate');
        $expenditureDate = ArrayHelper::getValue($searchModelData, 'expenditureManyDate');

        foreach ($data as $tb => $idArray) {

            // update plan operations only
            if ($tb != 'plan_cash_flows')
                continue;

            foreach ($idArray as $id) {
                $model = $this->findFlowItem($tb, $id);
                $attribute = 'date';
                $value = $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? $incomeDate : $expenditureDate;
                $model->$attribute = DateHelper::format($value, 'Y-m-d', 'd.m.Y');
                if (!$model->save(true, [$attribute])) {
                    return ['success' => false, 'msg' => 'Ошибка при обновлении операций', 'errors' => $model->getErrors()];
                }
            }
        }

        return ['success' => true, 'msg' => 'Статьи по операциям изменены'];
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionManyFlowRecognitionDate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $data = Yii::$app->request->post('flowId', []);
        $searchModelData = Yii::$app->request->post('ProfitAndLossSearchModel') ?: Yii::$app->request->post('OddsSearch');

        $recognitionDate = ArrayHelper::getValue($searchModelData, 'recognitionDateManyItem');

        foreach ($data as $tb => $idArray) {

            foreach ($idArray as $id) {
                $model = $this->findFlowItem($tb, $id);
                $attribute = 'recognition_date';

                if ($model instanceof CashBankFlows || $model instanceof CashOrderFlows || $model instanceof CashEmoneyFlows) {
                    $model->recognitionDateInput = $recognitionDate;
                } else {
                    $model->$attribute = DateHelper::format($recognitionDate, 'Y-m-d', 'd.m.Y');
                }

                if ($model instanceof CashBankFlows) {
                    $model->setScenario('update');
                }

                if (!$model->save(true, [$attribute])) {
                    return ['success' => false, 'msg' => 'Ошибка при обновлении операций', 'errors' => $model->getErrors()];
                }
            }
        }

        return ['success' => true, 'msg' => 'Дата признания по операциям изменена'];
    }


    /**
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionManyDocItem()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $data = Yii::$app->request->post('docId', []);
        $incomeItemID = Yii::$app->request->post('incomeItemIdManyItem');
        $expenseItemID = Yii::$app->request->post('expenditureItemIdManyItem');

        $cnt = 0;
        foreach ($data as $tb => $idArray) {
            foreach ($idArray as $id) {
                /** @var Act|PackingList|Upd $model */
                $model = $this->findDocItem($tb, $id);
                $isOut = $model->type == Documents::IO_TYPE_OUT;

                $isSaved = null;

                switch($tb) {
                    case 'act':
                    case 'upd':
                        foreach ($model->invoices as $invoice) {
                            if ($isOut) {
                                $invoice->invoice_income_item_id = (int)$incomeItemID;
                            } else {
                                $invoice->invoice_expenditure_item_id = (int)$expenseItemID;
                            }
                            $isSaved = LogHelper::save($invoice, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE, function (Invoice $invoice) {
                                return $invoice->save(true, ['invoice_income_item_id', 'invoice_expenditure_item_id']);
                            });
                        }
                        break;
                    case 'packing_list':
                        $invoice = $model->invoice;
                        if ($isOut) {
                            $invoice->invoice_income_item_id = (int)$incomeItemID;
                        } else {
                            $invoice->invoice_expenditure_item_id = (int)$expenseItemID;
                        }

                        $isSaved = LogHelper::save($invoice, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE, function (Invoice $invoice) {
                            return $invoice->save(true, ['invoice_income_item_id', 'invoice_expenditure_item_id']);
                        });
                        break;
                }

                if ($isSaved !== null && !$isSaved) {
                    return ['success' => false, 'msg' => 'Ошибка при обновлении документов'];
                } else {
                    $cnt++;
                }
            }
        }

        return ['success' => true, 'msg' => 'Статьи по документам изменены', 'cnt' => $cnt];
    }

    /**
     * @param $activeTab
     * @param string $periodSize
     * @return string
     * @throws Exception
     */
    public function actionPaymentCalendarPart($activeTab, $periodSize = 'months')
    {
        $searchModel = new PaymentCalendarSearch();
        $tableData = $searchModel->search2($activeTab, Yii::$app->request->post(), $periodSize == 'days');

        if ($activeTab == $searchModel::TAB_CALENDAR) {
            $viewTable = '_partial/payment_calendar_calendar';
        } elseif ($activeTab == $searchModel::TAB_BY_PURSE_DETAILED) {
            $viewTable = '_partial/payment_calendar_table_detailed';
        } else {
            $viewTable = '_partial/payment_calendar_table';
        }

        return $this->render(self::VIEW . $viewTable . ($periodSize == 'days' ? '_days' : ''), [
            'searchModel' => $searchModel,
            'activeTab' => $activeTab,
            'userConfig' => Yii::$app->user->identity->config,
            'data' => &$tableData['data'],
            'totalData' => &$tableData['totalData'],
            'growingData' => &$tableData['growingData'],
            'floorMap' => Yii::$app->request->post('floorMap', [])
        ]);
    }

    /**
     * @param int $activeTab
     * @return string
     * @throws \yii\base\Exception|\Exception
     */
    public function actionOddsPart($activeTab = OddsSearch::TAB_ODDS, $periodSize = 'months')
    {
        $searchModel = new OddsSearch();

        $pageFilter = Yii::$app->request->post('pageFilter');
        $_get = Yii::$app->request->get();
        $_filters = [
            'filters' => [
                'industry_id' => $pageFilter['industry_id'] ?? null,
                'sale_point_id' => $pageFilter['sale_point_id'] ?? null,
                'project_id' => $pageFilter['project_id'] ?? null,
            ]
        ];

        $dataFull = ($periodSize == 'days') ?
            $searchModel->searchByDays($activeTab, $_get + $_filters) :
            $searchModel->search($activeTab, $_get + $_filters);
        $data = &$dataFull['data'];
        $growingData = &$dataFull['growingData'];
        $totalData = &$dataFull['totalData'];

        if ($activeTab == OddsSearch::TAB_ODDS_BY_PURSE_DETAILED) {
            $viewTable = '_partial/odds2_table_detailed';
        } else {
            $viewTable = '_partial/odds2_table';
        }

        return $this->render(self::VIEW . $viewTable . ($periodSize == 'days' ? '_days' : ''), [
            'searchModel' => $searchModel,
            'data' => $data,
            'growingData' => $growingData,
            'totalData' => $totalData,
            'activeTab' => $activeTab,
            'periodSize' => $periodSize,
            'userConfig' => Yii::$app->user->identity->config,
            'warnings' => [],
            'floorMap' => Yii::$app->request->post('floorMap', [])
        ]);
    }

    /**
     * @param int $activeTab
     * @return string
     * @throws \Exception
     */
    public function actionExpensesPart($activeTab = ExpensesSearch::TAB_BY_ACTIVITY)
    {
        $searchModel = new ExpensesSearch();
        $data = $searchModel->search($activeTab, Yii::$app->request->get());
        $currentMonthNumber = date('n');
        $currentQuarter = (int)ceil(date('m') / 3);
        $showPercentColumns =
            Yii::$app->user->identity->config->report_odds_expense_percent ||
            Yii::$app->user->identity->config->report_odds_expense_percent_2;

        $tableShowBy = ($activeTab == ExpensesSearch::TAB_BY_ACTIVITY)
            ? ('income_expenses_by_activity' . ($showPercentColumns ? '_percent' : ''))
            : ('income_expenses_by_purse' . ($showPercentColumns ? '_percent' : ''));

        $incomeByMonth = (Yii::$app->user->identity->config->report_odds_expense_percent_2)
            ? $searchModel->getIncomeByMonth($activeTab)
            : [];

        return $this->renderPartial(self::VIEW . '_partial/' . $tableShowBy, [
            'isIncome' => false,
            'activeTab' => $activeTab,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'searchModel' => $searchModel,
            'data' => $data,
            'dataIncomeByMonth' => $incomeByMonth,
            'floorMap' => Yii::$app->request->post('floorMap', []),
        ]);
    }

    /**
     * @param int $activeTab
     * @return string
     * @throws \Exception
     */
    public function actionIncomePart($activeTab = IncomeSearch::TAB_BY_ACTIVITY)
    {
        $searchModel = new IncomeSearch();
        $data = $searchModel->search($activeTab, Yii::$app->request->get());
        $currentMonthNumber = date('n');
        $currentQuarter = (int)ceil(date('m') / 3);
        $showPercentColumns = Yii::$app->user->identity->config->report_odds_income_percent ?? false;
        $tableShowBy = ($activeTab == IncomeSearch::TAB_BY_ACTIVITY)
            ? ('income_expenses_by_activity' . ($showPercentColumns ? '_percent' : ''))
            : ('income_expenses_by_purse' . ($showPercentColumns ? '_percent' : ''));

        return $this->renderPartial(self::VIEW . '_partial/' . $tableShowBy, [
            'isIncome' => true,
            'activeTab' => $activeTab,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'searchModel' => $searchModel,
            'data' => $data,
            'floorMap' => Yii::$app->request->post('floorMap', [])
        ]);
    }

    /**
     * @param int $activeTab
     * @return string
     */
    public function actionProfitAndLossPart($activeTab = ProfitAndLossSearchModel::TAB_ALL_OPERATIONS)
    {
        $pageFilter = Yii::$app->request->post('pageFilter');
        $palPageFilter = [
            'pageFilters' => [
                'industry_id' => $pageFilter['industry_id'] ?? null,
                'sale_point_id' => $pageFilter['sale_point_id'] ?? null,
                'project_id' => $pageFilter['project_id'] ?? null,
            ]
        ];
        $searchModel = new ProfitAndLossSearchModel($palPageFilter);
        $currentMonthNumber = date('n');
        $currentQuarter = (int)ceil(date('m') / 3);
        $searchModel->load(Yii::$app->request->get());
        $data = $searchModel->handleItems();

        return $this->renderPartial(self::VIEW . '_partial/profit_and_loss_table', [
            'activeTab' => $activeTab,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'searchModel' => $searchModel,
            'data' => $data,
            'floorMap' => Yii::$app->request->post('floorMap', []),
            'pageFilter' => $palPageFilter['pageFilters'],
        ]);
    }

    /**
     * @return string|null
     */
    public function actionGetPlanFactData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('chart-plan-fact-ajax')) {

            $period = Yii::$app->request->post('period');
            $offset = Yii::$app->request->post('offset');
            $purse = Yii::$app->request->post('purse');
            $activity = Yii::$app->request->post('activity');
            $article = Yii::$app->request->post('article', []);
            $onPage = Yii::$app->request->post('onPage');
            $pageFilter = Yii::$app->request->post('pageFilter');
            $chart = Yii::$app->request->post('chart');

            $this->_savePeriodState($onPage, $period);

            $viewFile = ($onPage == 'PLAN_FACT') ? '_charts/_chart_plan_fact_days_2' : '_charts/_chart_plan_fact_days';

            return $this->renderPartial(self::VIEW . $viewFile, [
                'customOffset' => $offset,
                'customPeriod' => $period,
                'customPurse' => $purse,
                'customActivity' => $activity,
                'customArticle' => $article,
                'PAGE_FILTER' => $pageFilter,
                'LEFT_DATE_OFFSET' => $chart['leftDateOffset'] ?? null,
                'RIGHT_DATE_OFFSET' => $chart['rightDateOffset'] ?? null,
            ]);
        }

        return null;
    }

    /**
     * @return string|null
     */
    public function actionGetBalanceData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('chart-balance-ajax')) {

            //$year = Yii::$app->request->post('year');
            $offset = Yii::$app->request->post('offset');
            $article = Yii::$app->request->post('article');

            $model = new BalanceSearch(['company' => Yii::$app->user->identity->company]);
            $model->setYear($year ?? date('Y'));

            return $this->renderPartial(self::VIEW . '_charts/_chart_balance', [
                'customOffset' => $offset,
                'customArticle' => $article,
                'model' => $model
            ]);
        }

        return null;
    }

    /**
     * @return string|null
     */
    public function actionGetBalanceStackedData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('chart-balance-ajax')) {

            $offset = Yii::$app->request->post('offset');
            $article = Yii::$app->request->post('article');
            $dateTo = Yii::$app->request->post('dateTo');

            //$model = new BalanceSearch(['company' => Yii::$app->user->identity->company]);
            //$model->setYear($year ?? date('Y'));

            return $this->renderPartial(self::VIEW . '_charts/_chart_balance_stacked', [
                'customOffset' => $offset,
                'customArticle' => $article,
                //'model' => $model,
                'dateTo' => $dateTo
            ]);
        }

        return null;
    }

    /**
     * @return string|null
     */
    public function actionGetBalanceStructureData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('chart-balance-structure-ajax')) {

            $year = Yii::$app->request->post('year', date('Y'));
            $month = Yii::$app->request->post('month');

            $model = new BalanceSearch(['company' => Yii::$app->user->identity->company]);
            $model->year = $year;

            return $this->renderPartial(self::VIEW . '_charts/_chart_balance_structure', [
                'customMonth' => $month,
                'model' => $model
            ]);
        }

        return null;
    }

    public function actionGetBalanceArticleGrid($type, $category = null)
    {
        $searchModel = new BalanceArticleInitialSearch($type);
        $searchModel->category = $category;
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->pagination->pageSize = PageSize::get();

        return $this->render(self::VIEW . '_partial/_balance_initial/grid_balance_article', [
            'type' => (int)$type,
            'category' => $category,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGetBalanceStoreGrid()
    {
        $searchModel = new BalanceStoreInitialSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->pagination->pageSize = PageSize::get();

        return $this->render(self::VIEW . '_partial/_balance_initial/grid_store', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string|null
     * @throws Exception
     */
    public function actionGetExpenseChartsData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('chart-expense-ajax')) {

            $chart = Yii::$app->request->post('chart');
            $year = Yii::$app->request->post('year', date('Y'));
            $month = Yii::$app->request->post('month', date('m'));
            $period = Yii::$app->request->post('period');
            $offset = Yii::$app->request->post('offset');
            $purse = Yii::$app->request->post('purse');
            $article = Yii::$app->request->post('article');
            $client = Yii::$app->request->post('client');

            $model = new ExpensesSearch();
            $model->year = $year;

            switch ($chart) {
                case 'main':
                    $chartView = '_chart_expense_main';
                    break;
                case 'expense-income':
                    $chartView = '_chart_expense_income';
                    break;
                case 'expense-structure-1':
                    $chartView = '_chart_expense_structure_1';
                    break;
                case 'expense-structure-2':
                    $chartView = '_chart_expense_structure_2';
                    break;
                default:
                    throw new Exception('chart type not found');
            }

            return $this->renderPartial(self::VIEW . '_charts/' . $chartView, [
                'model' => $model,
                'customOffset' => $offset,
                'customPeriod' => $period,
                'customPurse' => $purse,
                'customMonth' => $month,
                'customArticle' => $article,
                'customClient' => $client
            ]);
        }

        return null;
    }

    /**
     * @return string|null
     * @throws Exception
     */
    public function actionGetIncomeChartsData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('chart-income-ajax')) {

            $chart = Yii::$app->request->post('chart');
            $year = Yii::$app->request->post('year', date('Y'));
            $month = Yii::$app->request->post('month', date('m'));
            $period = Yii::$app->request->post('period');
            $offset = Yii::$app->request->post('offset');
            $purse = Yii::$app->request->post('purse');
            $article = Yii::$app->request->post('article');
            $client = Yii::$app->request->post('client');
            $product = Yii::$app->request->post('product');
            $employee = Yii::$app->request->post('employee');

            $model = new IncomeSearch();
            $model->year = $year;

            switch ($chart) {
                case 'main':
                    $chartView = '_chart_income_main';
                    break;
                case 'income-structure-1':
                    $chartView = '_chart_income_structure_1';
                    break;
                case 'cash':
                    $chartView = '_chart_income_cash';
                    break;
                case 'top':
                    $chartView = '_chart_income_top';
                    break;
                default:
                    throw new Exception('chart type not found');
            }

            return $this->renderPartial(self::VIEW . '_charts/' . $chartView, [
                'model' => $model,
                'customOffset' => $offset,
                'customPeriod' => $period,
                'customPurse' => $purse,
                'customMonth' => $month,
                'customArticle' => $article,
                'customClient' => $client,
                'customProduct' => $product,
                'customEmployee' => $employee
            ]);
        }

        return null;
    }

    /**
     * @return string|null
     */
    public function actionGetProfitLossChartsData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('chart-profit-loss-ajax')) {

            $chartViewType = Yii::$app->request->post('chartViewType');
            $chartsGroup = Yii::$app->request->post('chartsGroup');
            $year = Yii::$app->request->post('year', date('Y'));
            $month = Yii::$app->request->post('month', date('m'));
            $offset = Yii::$app->request->post('offset');
            $period = Yii::$app->request->post('period');
            $amountKey = Yii::$app->request->post('amountKey', 'amount');
            $pageFilter = Yii::$app->request->post('pageFilter');
            $palPageFilter = [
                'pageFilters' => [
                    'industry_id' => $pageFilter['industry_id'] ?? null,
                    'sale_point_id' => $pageFilter['sale_point_id'] ?? null,
                    'project_id' => $pageFilter['project_id'] ?? null,
                ]
            ];
            $model = new ProfitAndLossSearchModel($palPageFilter);
            $model->year = $year;
            // $model->setYearFilter([$year]); // todo: needed current year + offset year
            $model->handleItems();

            switch ($chartViewType) {
                case 'profit_loss_compare':
                    $view = '_chart_profit_loss_compare';
                    break;
                default:
                    $view = '_chart_profit_loss';
                    break;
            }

            return $this->renderPartial(self::VIEW . '_charts/' . $view, [
                'customChartsGroup' => $chartsGroup,
                'customPeriod' => $period,
                'customMonth' => $month,
                'customOffset' => $offset,
                'customAmountKey' => $amountKey,
                'model' => $model
            ]);
        }

        return null;
    }

    public function actionGetOperationalEfficiencyChartsData() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('chart-operational-efficiency-ajax')) {
            $year = Yii::$app->request->post('year', date('Y'));
            $month = Yii::$app->request->post('month', date('m'));
            $offset = Yii::$app->request->post('offset');
            $model = new OperationalEfficiencySearchModel();
            $model->year = $year;
            $model->handleItems();

            return $this->renderPartial(self::VIEW . '_charts/_chart_operational_efficiency', [
                'customMonth' => $month,
                'customOffset' => $offset,
                'model' => $model
            ]);
        }

        return null;
    }

    /**
     * @return string|null
     * @throws Exception
     */
    public function actionGetDebtorChartsData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('chart-debtor-ajax')) {

            $type = Yii::$app->request->post('type');

            if (!in_array($type, [Documents::IO_TYPE_IN, Documents::IO_TYPE_OUT]))
                throw new NotFoundHttpException('The requested page does not exist.');

            $chart = Yii::$app->request->post('chart');
            $year = Yii::$app->request->post('year', date('Y'));
            $period = Yii::$app->request->post('period');
            $offset = Yii::$app->request->post('offset');
            $debtorPeriod = Yii::$app->request->post('debtor-period');
            $contractor = Yii::$app->request->post('contractor');
            $employee = Yii::$app->request->post('employee');
            $searchBy = Yii::$app->request->post('search-by');
            $reportType = Yii::$app->request->post('report-type');
            DebtorHelper2::$SEARCH_BY = $searchBy;

            $model = new DebtReportSearch2();
            $model->year = $year;
            $model->search_by = $searchBy;

            switch ($chart) {
                case 'main':
                    $chartView = '_chart_debtor_main';
                    break;
                case 'main2':
                    $chartView = '_chart_debtor_main_2';
                    $contractor = null;
                    break;
                case 'net-total':
                    $chartView = '_chart_debtor_net_total';
                    break;
                default:
                    throw new Exception('chart not found');
            }

            return $this->renderPartial(self::VIEW . '_charts/' . $chartView, [
                'model' => $model,
                'type' => $type,
                'reportType' => $reportType,
                'searchBy' => $searchBy,
                'customOffset' => $offset,
                'customPeriod' => $period,
                'customDebtPeriod' => $debtorPeriod,
                'customContractor' => $contractor,
                'customEmployee' => $employee,
            ]);
        }

        return null;
    }

    /**
     * @return string|null
     * @throws Exception
     */
    public function actionGetStocksChartData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('chart-stocks-ajax')) {

            $chart = Yii::$app->request->post('chart');
            $year = Yii::$app->request->post('year', date('Y'));
            $month = Yii::$app->request->post('month', date('m'));
            $period = Yii::$app->request->post('period');
            $offset = Yii::$app->request->post('offset');
            $group  = Yii::$app->request->post('group');

            switch ($chart) {
                case 'main':
                    $chartView = '_chart_stocks_main';
                    break;
                case 'structure':
                    $chartView = '_chart_stocks_structure';
                    break;
                case 'squares':
                    $chartView = '_chart_stocks_squares';
                    break;
                default:
                    throw new Exception('chart not found');
            }

            $model = new StocksSearch();
            $model->year = $year;

            return $this->renderPartial(self::VIEW . '_charts/' . $chartView, [
                'model' => $model,
                'userConfig' => Yii::$app->user->identity->config,
                'customOffset' => $offset,
                'customPeriod' => $period,
                'customGroup' => $group,
                'customMonth' => $month
            ]);
        }

        return null;
    }

    /**
     * @param $block
     * @return string
     * @throws Exception
     */
    public function actionAddArticleToBreakEven($block)
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect(Yii::$app->request->referrer);
        }

        if (!in_array($block, [BreakEvenSearchModel::BLOCK_VARIABLE_COSTS, BreakEvenSearchModel::BLOCK_FIXED_COSTS]))
            throw new Exception('Block type not found');

        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $model = new ArticleDropDownForm($user, ArticlesSearch::TYPE_EXPENSE);
        $model->breakEvenBlock = (int)$block;
        // Update BreakEven block type only

        if ($customerInputsMap = Yii::$app->request->post('customerInputsMap'))
            Yii::$app->session->set('breakEven.customerInputsMap', $customerInputsMap);

        // Articles save
        if ($model->load(Yii::$app->request->post()) && ($items = $model->save())) {
            return "<script>$('#break-even-article-modal').modal('hide'); location.href = location.href; </script>";
        }

        return $this->renderAjax(self::VIEW . '_partial/break_even_modal', [
            'model' => $model,
            'block' => $block,
            'type' => ArticlesSearch::TYPE_EXPENSE
        ]);
    }

    /**
     * @param $type
     * @return array
     */
    public function actionUpdatePrepaymentArticles($type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (!Yii::$app->request->isAjax) {
            $this->redirect(Yii::$app->request->referrer);
        }
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $model = new ArticleDropDownForm($user, $type);
        $model->isNew = ArticleDropDownForm::TYPE_UPDATE_PREPAYMENT_COLUMN;

        // Articles save
        $model->load(Yii::$app->request->post());
        if ($items = $model->save()) {

            if ($modelPrepaymentWallets = CompanyPrepaymentWallets::findOne(['company_id' => Yii::$app->user->identity->company_id])) {
                $modelPrepaymentWallets->bank = 0;
                $modelPrepaymentWallets->order = 0;
                $modelPrepaymentWallets->emoney = 0;
                $modelPrepaymentWallets->load(Yii::$app->request->post());
                if ($modelPrepaymentWallets->save()) {
                    Yii::$app->session->setFlash('success', 'Статьи обновлены');

                    return ['success' => 1];
                }
            }

            return ['success' => 0, 'errors' => $modelPrepaymentWallets->getErrors()];
        }

        return ['success' => 0, 'errors' => $model->getErrors()];
    }

    public function actionUpdateProfitAndLossArticles()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (!Yii::$app->request->isAjax) {
            $this->redirect(Yii::$app->request->referrer);
        }

        $user = Yii::$app->user->identity;
        $company = $user->currentEmployeeCompany->company;

        $model = new AnalyticsArticleForm($company);

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            Yii::$app->session->setFlash('success', 'Настройки сохранены');

            if ($model->hasChangedOptions) {
                AnalyticsOptions::setPalResetButtonEnable($company->id);
            }

            return ['success' => 1];
        }

        return ['success' => 0, 'errors' => $model->getErrors()];
    }

    public function actionResetProfitAndLossArticlesOptions()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (!Yii::$app->request->isAjax) {
            $this->redirect(Yii::$app->request->referrer);
        }

        $user = Yii::$app->user->identity;
        $company = $user->currentEmployeeCompany->company;

        $model = new AnalyticsArticleDefaults($company);

        if ($model->resetToDefaults()) {
            Yii::$app->session->setFlash('success', 'Настройки сброшены до установок КУБ24');

            AnalyticsOptions::setPalResetButtonDisable($company->id);

            return ['success' => 1];
        }

        return ['success' => 0, 'errors' => $model->getErrrors()];
    }

    /**
     * @param $onPage
     * @param $period
     */
    protected function _savePeriodState($onPage, $period)
    {
        if ($onPage == 'PK' || $onPage == 'PLAN_FACT')
            $attr = 'report_pc_chart_period';
        elseif ($onPage == 'ODDS')
            $attr = 'report_odds_chart_period';
        else
            $attr = null;

        if ($attr) {
            /** @var Config $config */
            $config = Yii::$app->user->identity->config;

            if ($period == "days" && $config->$attr != 0)
                $config->updateAttributes([$attr => 0]);
            elseif ($period == "months" && $config->$attr != 1)
                $config->updateAttributes([$attr => 1]);
        }
    }

    /**
     * @param $tableName
     * @param $id
     * @param $skipNotFound
     * @return CashBankFlows|CashOrderFlows|CashEmoneyFlows
     * @throws NotFoundHttpException
     */
    private function findFlowItem($tableName, $id, $skipNotFound = false)
    {
        switch ($tableName) {
            case CashBankFlows::tableName():
                $className = CashBankFlows::class;
                break;
            case CashOrderFlows::tableName():
                $className = CashOrderFlows::class;
                break;
            case CashEmoneyFlows::tableName():
                $className = CashEmoneyFlows::class;
                break;
            case PlanCashFlows::tableName():
                $className = PlanCashFlows::class;
                break;
            case AcquiringOperation::tableName():
                $className = AcquiringOperation::class;
                break;
            case CardOperation::tableName():
                $className = CardOperation::class;
                break;
            default:
                throw new NotFoundHttpException('The requested page does not exist.');
        }
        $model = $className::find()
            ->andWhere(['id' => $id])
            ->andWhere(['company_id' => Yii::$app->user->identity->company_id])
            ->one();
        if ($model === null && !$skipNotFound) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $model;
    }

    /**
     * @param $tableName
     * @param $id
     * @return CashBankFlows|CashOrderFlows|CashEmoneyFlows
     * @throws NotFoundHttpException
     */
    private function findDocItem($tableName, $id)
    {
        switch ($tableName) {
            case Act::tableName():
                $className = Act::class;
                break;
            case PackingList::tableName():
                $className = PackingList::class;
                break;
            case Upd::tableName():
                $className = Upd::class;
                break;
            case GoodsCancellation::tableName():
                $className = GoodsCancellation::class;
                break;
            default:
                throw new NotFoundHttpException('The requested page does not exist.');
        }

        if ($tableName == GoodsCancellation::tableName()) {
            $model = $className::find()
                ->andWhere(['id' => $id])
                ->andWhere(['company_id' => Yii::$app->user->identity->company_id])
                ->one();
        } else {
            $model = $className::find()
                ->joinWith('invoice')
                ->andWhere(['invoice.company_id' => Yii::$app->user->identity->company_id])
                ->andWhere(["{$tableName}.id" => $id])
                ->one();
        }

        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $model;
    }

    /**
     * @return string|null
     */
    public function actionGetIndustryCompareData()
    {
        Yii::$app->response->format = Response::FORMAT_HTML;

        if (Yii::$app->request->post('chart-industry-compare-ajax')) {

            $company = Yii::$app->user->identity->company;
            $onPage = (string)Yii::$app->request->post('on-page');
            $chartType = (int)Yii::$app->request->post('chart-type');
            $offset = (int)Yii::$app->request->post('offset');
            $currentYear = (string)Yii::$app->session->get('modules.reports.finance.year', date('Y'));
            $analyticsModel = new AnalyticsSimpleSearch($company);
            $palModel = AnalyticsManager::getPalModel([
                'revenueRuleGroup' => AnalyticsArticleForm::REVENUE_RULE_GROUP_CUSTOMER,
                'monthGroupBy' => AnalyticsArticleForm::MONTH_GROUP_INDUSTRY // need for charts
            ]);

            return $this->renderPartial(self::VIEW . '_charts/_chart_industry_compare', [
                'isAjax' => true,
                'palModel' => $palModel ?? null,
                'analyticsModel' => $analyticsModel ?? null,
                'customChartType' => $chartType,
                'customOffset' => $offset,
                'onPage' => $onPage,
                'currentYear' => $currentYear
            ]);
        }

        return null;
    }

    /**
     * @return string|null
     */
    public function actionGetProjectCompareData()
    {
        Yii::$app->response->format = Response::FORMAT_HTML;

        if (Yii::$app->request->post('chart-project-compare-ajax')) {

            $company = Yii::$app->user->identity->company;
            $onPage = (string)Yii::$app->request->post('on-page');
            $chartType = (int)Yii::$app->request->post('chart-type');
            $offset = (int)Yii::$app->request->post('offset');
            $projectsStr = (array)Yii::$app->request->post('projects');

            // sanitize ids
            $projects = [];
            foreach ($projectsStr as $strKey => $strValue)
                $projects[str_replace('project_','', $strKey)] = $strValue;

            $analyticsModel = new AnalyticsSimpleSearch($company);
            $palModel = AnalyticsManager::getPalModel([
                'monthGroupBy' => AnalyticsArticleForm::MONTH_GROUP_PROJECT
            ]);

            return $this->renderPartial(self::VIEW . '_charts/_chart_project_compare', [
                'isAjax' => true,
                'palModel' => $palModel,
                'analyticsModel' => $analyticsModel,
                'items' => $projects,
                'customChartType' => $chartType,
                'customOffset' => $offset,
                'onPage' => $onPage,
            ]);
        }

        return null;
    }    
}