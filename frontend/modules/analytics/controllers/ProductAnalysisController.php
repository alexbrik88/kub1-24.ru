<?php

namespace frontend\modules\analytics\controllers;

use common\components\filters\AccessControl;
use common\models\product\ProductTurnover;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\components\PageSize;
use frontend\modules\analytics\models\productAnalysis\ProductAnalysisCharts;
use frontend\modules\analytics\models\ProductAnalysisABC;
use frontend\modules\analytics\models\ProductAnalysisSearch;
use frontend\modules\analytics\models\ProductAnalysisSellingSpeedSearch;
use Yii;
use yii\helpers\ArrayHelper;
use \yii\helpers\Url;

/**
 * ProductAnalysisController
 */
class ProductAnalysisController extends FrontendController
{
    public $layout = 'product_analysis';

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function($rule, $action) {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_PRODUCTS);
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * @param \yii\base\Action $action
     * @return bool|\yii\web\Response
     */
    public function beforeAction($action)
    {
        if ($action->id == 'abc' && ($showGroups = Yii::$app->request->get('defaultSorting'))) {
            Yii::$app->user->identity->config->updateAttributes([
                'report_abc_show_groups' => ($showGroups == 'show_groups') ? 1 : 0
            ]);

            return $this->redirect(Url::current(['defaultSorting' => null]));
        }

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        return $this->redirect('/reports/product-analysis/abc');
    }

    /**
     * @return mixed
     */
    public function actionXyz()
    {
        $searchModel = new ProductAnalysisSearch(['company' => Yii::$app->user->identity->company]);
        $searchModel->setYear(date('Y'));

        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        $dataProvider->pagination->pageSize = PageSize::get();

        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 143, true);

        return $this->render('xyz', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionAbc()
    {
        $searchModel = new ProductAnalysisABC(['company' => Yii::$app->user->identity->company]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 142, true);

        return $this->render('abc', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @return mixed
     */
    public function actionDashboard()
    {
        $company = Yii::$app->user->identity->company;

        $searchModel = new ProductAnalysisCharts();
        $searchModel->yearMonth = ArrayHelper::getValue(Yii::$app->request->get('ProductAnalysisCharts'), 'yearMonth', date('Ym'));

        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 141, true);

        return $this->render('dashboard', [
            'company' => $company,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionSalesSpeed()
    {
        $searchModel = new ProductAnalysisSellingSpeedSearch(['company' => Yii::$app->user->identity->company]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 144, true);

        return $this->render('sales-speed', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @return mixed
     */
    public function actionWayIn()
    {
        $this->layout = 'way';

        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 145, true);

        return $this->render('in_developing');
    }

    /**
     * @return mixed
     */
    public function actionWayOut()
    {
        $this->layout = 'way';

        return $this->render('in_developing');
    }
}
