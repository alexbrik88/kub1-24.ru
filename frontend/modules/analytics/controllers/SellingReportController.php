<?php
namespace frontend\modules\analytics\controllers;

use Yii;
use yii\base\Exception;
use yii\helpers\Url;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use frontend\components\FrontendController;
use common\components\filters\AccessControl;
use frontend\components\BusinessAnalyticsAccess;
use frontend\modules\analytics\models\SellingReportSearch2;
use common\components\TextHelper;

/**
 * Class SellingReportController
 * @package frontend\modules\analytics\controllers
 */
class SellingReportController extends FrontendController
{
    /**
     * @var string
     */
    public $layout = 'selling-report';

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'get-xls',
                            'get-chart-data',
                            'get-table-data',
                            'change-view'
                        ], // for use on project page
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function($rule, $action) {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_SALES);
                        },
                    ],
                ],
            ],
        ]);
    }

    public function beforeAction($action)
    {
        if (!Yii::$app->request->isAjax) {
            $class = 'SellingReportSearch2';
            if (\Yii::$app->request->get($class)) {
                $year = \common\components\helpers\ArrayHelper::getValue(\Yii::$app->request->get($class), 'year');
                if ($year > 2010 && $year < 2100) {
                    \Yii::$app->session->set('modules.reports.finance.year', $year);
                    $getParams = \Yii::$app->request->queryParams;
                    unset($getParams[$class]);
                    return $this->redirect(Url::toRoute([Url::base()] + $getParams));
                }
            }
        }

        return parent::beforeAction($action);
    }

    /**
     * @return string
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $searchModel = new SellingReportSearch2();

        try {
            $searchModel->searchBy = (int)Yii::$app->user->identity->config->selling_report_search_by;
            $searchModel->type = (int)Yii::$app->user->identity->config->selling_report_type;
            $searchModel->groupByContractor = (bool)Yii::$app->user->identity->config->selling_report_by_contractor;
        } catch (\Throwable $e) {
            $searchModel->searchBy = SellingReportSearch2::BY_INVOICES;
            $searchModel->type = SellingReportSearch2::TYPE_ALL;
            $searchModel->groupByContractor = false;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $searchModel->search(Yii::$app->request->get())
        ]);
    }

    public function actionChangeView()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $config = Yii::$app->user->identity->config;
        $searchBy = Yii::$app->request->get('selling_report_search_by');
        $type = Yii::$app->request->get('selling_report_type');
        $groupByContractor = Yii::$app->request->get('group_by_contractor');
        $showPercent = Yii::$app->request->get('show_percent');
        $showPercentTotal = Yii::$app->request->get('show_percent_total');

        if ($searchBy !== null && in_array($searchBy, [SellingReportSearch2::BY_INVOICES, SellingReportSearch2::BY_FLOWS])) {
            $config->updateAttributes(['selling_report_search_by' => (int)$searchBy]);
            return ['updated' => true];
        }
        if ($type !== null && in_array($type, [SellingReportSearch2::TYPE_ALL, SellingReportSearch2::TYPE_NOT_PAID, SellingReportSearch2::TYPE_PAID])) {
            $config->updateAttributes(['selling_report_type' => (int)$type]);
            return ['updated' => true];
        }
        if ($groupByContractor !== null) {
            $config->updateAttributes(['selling_report_by_contractor' => (int)$groupByContractor]);
            return ['updated' => true];
        }
        if ($showPercent !== null) {
            $config->updateAttributes(['selling_report_percent' => (int)$showPercent]);
            return ['updated' => true];
        }
        if ($showPercentTotal !== null) {
            $config->updateAttributes(['selling_report_percent_total' => (int)$showPercentTotal]);
            return ['updated' => true];
        }

        return ['updated' => false];
    }

    public function actionGetChartData()
    {
        $searchModel = new SellingReportSearch2();
        $searchModel->project_id = (int)Yii::$app->request->post('project') ?: null;
        $searchModel->year = Yii::$app->request->post('year', date('Y'));

        try {
            $searchModel->searchBy = (int)Yii::$app->user->identity->config->selling_report_search_by;
            $searchModel->type = (int)Yii::$app->user->identity->config->selling_report_type;
            $searchModel->groupByContractor = (bool)Yii::$app->user->identity->config->selling_report_by_contractor;
        } catch (\Throwable $e) {
            $searchModel->searchBy = SellingReportSearch2::BY_INVOICES;
            $searchModel->type = SellingReportSearch2::TYPE_ALL;
            $searchModel->groupByContractor = false;
        }

        // post params
        $chart = Yii::$app->request->post('chart');
        $customOffset = (int)Yii::$app->request->post('offset');
        $customMonth = (int)Yii::$app->request->post('month');

        if ($chart == 'main') {
            $view = '_chart_main';
        } elseif ($chart == 'structure') {
            $view = '_chart_structure';
        } else {
            throw new Exception('Unknown chart type');
        }

        return $this->render($view, [
            'searchModel' => $searchModel,
            'customOffset' => $customOffset,
            'customMonth' => $customMonth
        ]);
    }

    public function actionGetTableData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $tableGroupBy = Yii::$app->request->post('group_by');
        $projectId = (int)Yii::$app->request->post('project') ?: null;
        $contractorId = (int)Yii::$app->request->post('contractor') ?: null;
        $year = Yii::$app->request->post('year', date('Y'));

        if ($tableGroupBy == 'contractor' && $contractorId) {
            $searchModel = new SellingReportSearch2();
            $searchModel->contractor_id = $contractorId;
            $searchModel->project_id = $projectId;
            $searchModel->year = $year;

            try {
                $searchModel->searchBy = (int)Yii::$app->user->identity->config->selling_report_search_by;
                $searchModel->type = (int)Yii::$app->user->identity->config->selling_report_type;
            } catch (\Throwable $e) {
                $searchModel->searchBy = SellingReportSearch2::BY_INVOICES;
                $searchModel->type = SellingReportSearch2::TYPE_ALL;
            }

            $dataProvider = $searchModel->search(Yii::$app->request->get());
            $dataProvider->pagination->pageSize = -1;
            if ($sessionSort = Yii::$app->session->get('modules.reports.selling_report.sort')) {
                $dataProvider->sort->setAttributeOrders($sessionSort);
            }

        } else {
            throw new Exception('Unknown group_by type');
        }

        $_strlenAmount = strlen(SellingReportSearch2::PREFIX_AMOUNT);

        $products = $dataProvider->getModels();
        foreach ($products as $num => $product)
            foreach ($product as $columnKey => $columnValue) {

                if ($columnKey === 'item_name')
                    continue;

                if (substr($columnKey, 0, $_strlenAmount) === SellingReportSearch2::PREFIX_AMOUNT)
                    $products[$num][$columnKey] = TextHelper::invoiceMoneyFormat($columnValue, 2);
                else
                    $products[$num][$columnKey] = TextHelper::numberFormat($columnValue, 2);
        }

        if ($searchModel->searchBy == $searchModel::BY_FLOWS) {
            $undistributed = $searchModel->getUndistributedFlows();
            if (!empty($undistributed['amount_y'])) { // sum by year
                $undistributed['item_name'] = 'Неразнесенные оплаты';
                foreach ($undistributed as $columnKey => $columnValue) {

                    if ($columnKey === 'item_name')
                        continue;

                    if (substr($columnKey, 0, $_strlenAmount) === SellingReportSearch2::PREFIX_AMOUNT)
                        $undistributed[$columnKey] = TextHelper::invoiceMoneyFormat($columnValue, 2);
                    else
                        $undistributed[$columnKey] = TextHelper::numberFormat($columnValue, 2);
                }
            } else {
                $undistributed = null;
            }
        }

        return [
            'products' => $products,
            'undistributed' => !empty($undistributed) ? [$undistributed] : null
        ];
    }


    /**
     * @param $year
     * @return Response
     */
    public function actionGetXls($year)
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        $searchModel = new SellingReportSearch2();
        $searchModel->project_id = (int)Yii::$app->request->get('project_id');
        $searchModel->year = $year;

        try {
            $searchModel->searchBy = (int)Yii::$app->user->identity->config->selling_report_search_by;
            $searchModel->type = (int)Yii::$app->user->identity->config->selling_report_type;
            $searchModel->groupByContractor = (bool)Yii::$app->user->identity->config->selling_report_by_contractor;
        } catch (\Throwable $e) {
            $searchModel->searchBy = SellingReportSearch2::BY_INVOICES;
            $searchModel->type = SellingReportSearch2::TYPE_ALL;
            $searchModel->groupByContractor = false;
        }

        Yii::$app->response->format = Response::FORMAT_RAW;
        $searchModel->generateXls();
    }
}