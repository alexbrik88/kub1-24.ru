<?php

namespace frontend\modules\analytics\controllers;

use frontend\modules\analytics\models\financePlan\chart\FinancePlanChart;
use frontend\modules\analytics\models\financePlan\FinancePlanGroup;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use frontend\components\FrontendController;
use common\components\helpers\ArrayHelper;
use frontend\components\BusinessAnalyticsAccess;
use common\components\filters\AccessControl;

/**
 * Class FinancePlanAjaxController
 */
class FinancePlanAjaxController extends FrontendController
{
    const VIEW = '@frontend/modules/analytics/views/finance-plan/';

    public $enableCsrfValidation = false;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'get-chart-data',
                        ],
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function($rule, $action) {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_FINANCE);
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string|null
     */
    public function actionGetChartData($groupID)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModelGroup($groupID);

        if (Yii::$app->request->post('chart-finance-plan-ajax')) {

            $chart = Yii::$app->request->post('chart');
            $offset = Yii::$app->request->post('offset');
            $plan = Yii::$app->request->post('plan');

            return $this->renderPartial(self::VIEW . 'partial/_chart', [
                'model' => new FinancePlanChart($model),
                'customOffset' => $offset,
                'customPlan' => $plan,
            ]);
        }

        return null;
    }

    /**
     * @param $id
     * @return FinancePlanGroup
     * @throws NotFoundHttpException
     */
    protected function findModelGroup($id)
    {
        $employee = Yii::$app->user->identity;
        /** @var FinancePlanGroup $model */
        $model = FinancePlanGroup::find()
            ->with([
                'expenditureItems',
                'financePlans',
                'financePlans.incomeItems',
                'financePlans.expenditureItems',
                'financePlans.primeCostItems',
                'financePlans.oddsIncomeItems',
                'financePlans.oddsExpenditureItems',
            ])
            ->where(['id' => $id])
            ->andWhere(['company_id' => $employee->company->id])
            ->one();

        $model->mainIndustryId = $model->mainFinancePlan->industry_id;
        $model->mainIndustryName = $model->mainFinancePlan->name;

        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Запрошенная страница не существует.');
    }
}