<?php

namespace frontend\modules\analytics\controllers;

use kartik\form\ActiveForm;
use PhpOffice\PhpSpreadsheet\Calculation\MathTrig\Sign;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Connection;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use frontend\components\PageSize;
use common\models\Company;
use common\models\employee\Employee;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use common\components\filters\AccessControl;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use common\components\date\DateHelper;
use common\models\cash\CashFlowsBase;
use frontend\modules\analytics\models\PaymentCalendarSearch;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequest;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestSearch;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestStatus as Status;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestOrder as Order;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestHistory as History;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestChat as Chat;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestChatUser as ChatUser;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestSignTemplate as SignTemplate;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestSignTemplateStep as SignTemplateStep;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestSignStep as SignStep;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestOrderApproved as OrderApproved;

/**
 * Class PlanPaymentsRequestController
 * @package frontend\modules\analytics\controllers
 */
class PlanPaymentsRequestController extends FrontendController
{
    const TAB_FLOWS = '_flows';
    const TAB_CHAT = '_chat';
    const TAB_HISTORY = '_history';

    /**
     * @var string
     */
    public $layout = 'finance';

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            // always allowed ajax actions
                        ],
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\Reports::VIEW,
                        ],
                    ],
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function($rule, $action) {

                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_FINANCE);
                        },
                    ],
                ],
            ],
        ]);
    }

    public function beforeAction($action)
    {
        if (in_array($action->id, ['create', 'update', 'view']))
            $this->layout = null;

        return parent::beforeAction($action);
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    public function actionIndex()
    {
        $user = Yii::$app->user->identity;

        $searchModel = new PaymentsRequestSearch([
            'company_id' => $user->company->id
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->pagination->pageSize = PageSize::get('per-page', 50);

        \common\models\company\CompanyFirstEvent::checkEvent($user->company, 114, true);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @param null $tab
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionView($id, $tab = null)
    {
        $user = Yii::$app->user->identity;
        $company = $user->company;
        /** @var PaymentsRequest $model */
        $model = $this->loadModel($id);
        /** @var ChatUser $chatUser */
        $chatUser = $this->loadChatUser($model);

        switch ($tab) {
            case self::TAB_HISTORY:
                $tabData = [];
                break;
            case self::TAB_CHAT:
                $tabData = [];
                $chatUser->updateAttributes(['last_read_at' => time()]);
                break;
            case self::TAB_FLOWS:
            default:
                $planFlowsIds = ArrayHelper::getColumn($model->orders, 'plan_operation_id') ?: [-1];
                $searchModel = new PaymentCalendarSearch([
                    'company_id' => $company->id,
                    'byIds' => $planFlowsIds,
                    'futurePeriod' => true
                ]);
                $dataProvider = $searchModel->searchItems(Yii::$app->request->get());
                $dataProvider->pagination->pageSize = PageSize::get();

                $tab = self::TAB_FLOWS;
                $tabData = [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider
                ];

                break;
        }

        return $this->render('view', [
            'user' => $user,
            'company' => $company,
            'model' => $model,
            'chatUser' => $chatUser,
            'tab' => $tab,
            'tabData' => $tabData
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $returnUrl = Url::to(['view', 'id' => $id]);

        /* @var PaymentsRequest $model */
        $model = $this->loadModel($id);
        $_signTemplateID = $model->sign_template_id;

        if ($model->load(Yii::$app->request->post())) {
            $orderArray = (array)Yii::$app->request->post('orderArray');
            $removeOrdersIds = ArrayHelper::getColumn($model->orders, 'plan_operation_id');
            $needUpdateSignSteps = $model->sign_template_id != $_signTemplateID;

            $number = 0;
            $orders = [];
            foreach ($orderArray as $o) {
                $order = Order::findOne([
                    'payments_request_id' => $model->id,
                    'plan_operation_id' => $o['plan_operation_id'],
                ]) ?: new Order([
                    'payments_request_id' => null,
                    'plan_operation_id' => $o['plan_operation_id'],
                ]);

                $order->number = ++$number;

                $orders[] = $order;

                if (($_key = array_search($order->plan_operation_id, $removeOrdersIds)) !== false) {
                    unset($removeOrdersIds[$_key]);
                }
            }

            if ($model->validate()) {

                $isSaved = LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE, function (PaymentsRequest $model) use ($user, $orders, $removeOrdersIds, $needUpdateSignSteps) {

                    if (empty($orders)) {
                        $model->addError('isOrdersExists', 'Заявка не может быть пустой');
                        return false;
                    }

                    if ($needUpdateSignSteps)
                        $model->unlinkSignStatuses();


                    if (!$model->save()) {
                        return false;
                    }

                    // remove old
                    foreach ($model->orders as $removeOrder) {
                        if (in_array($removeOrder->plan_operation_id, $removeOrdersIds)) {
                            History::deleteOrder($removeOrder);
                            // LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE);
                        }
                    }

                    // update/add new
                    foreach ($orders as $order) {
                        if ($order->isNewRecord) {
                            $order->payments_request_id = $model->id;
                            History::addOrder($order);
                        } else {
                            $order->save();
                        }
                    }

                    // update sign steps
                    if ($needUpdateSignSteps) {
                        if ($model->linkSignStatuses($model->signTemplate->steps)) {
                            History::removeSignStepsHistory($model);
                            return true;
                        }

                        return false;
                    }

                    return true;
                });

                if ($isSaved) {
                    Yii::$app->session->setFlash('success', 'Заявка обновлена');
                    $returnUrl = Url::to(['view', 'id' => $model->id]);
                    return $this->redirect($returnUrl);
                }
            }

            $model->populateRelation('orders', $orders);
        }

        return $this->render('update', [
            'model' => $model,
            'company' => $company,
            'returnUrl' => $returnUrl,
        ]);
    }

    /**
     * @return string|Response
     * @throws \Exception
     */
    public function actionCreate()
    {
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $returnUrl = Url::to(['index']);

        /* @var PaymentsRequest $model */
        $model = new PaymentsRequest([
            'company_id' => $company->id,
            'author_id' => $user->id,
            'status_id' => Status::STATUS_APPROVAL,
            'document_date' => date('Y-m-d'),
            'payment_date' => date('Y-m-d'),
            'document_number' => PaymentsRequest::getNextDocumentNumber($company->id, null, date('Y-m-d'))
        ]);

        if ($model->load(Yii::$app->request->post())) {
            $orderArray = (array)Yii::$app->request->post('orderArray');
            $number = 0;
            $orders = [];
            foreach ($orderArray as $o) {
                $orders[] = new Order([
                    'payments_request_id' => null,
                    'plan_operation_id' => $o['plan_operation_id'],
                    'number' => ++$number,
                ]);
            }

            if ($model->validate()) {
                $isSaved = LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function (PaymentsRequest $model) use ($orders) {

                    if (empty($orders)) {
                        $model->addError('isOrdersExists', 'Заявка не может быть пустой');
                        return false;
                    }

                    if (!History::createRequest($model)) {
                        return false;
                    }

                    foreach ($orders as $order) {
                        $order->payments_request_id = $model->primaryKey;
                        if (!$order->save()) {
                            return false;
                        }
                    }

                    if ($model->linkSignStatuses($model->signTemplate->steps)) {
                        return true;
                    }

                    return false;
                });

                if ($isSaved) {
                    Yii::$app->session->setFlash('success', 'Заявка добавлена');
                    $returnUrl = Url::to(['view', 'id' => $model->id]);
                    return $this->redirect($returnUrl);
                }
            }

            $model->populateRelation('orders', $orders);
        }

        if ($fromFlows = Yii::$app->request->post('fromFlows')) {
            $number = 0;
            $orders = [];
            foreach ($fromFlows as $flowId) {
                $orders[] = new Order([
                    'payments_request_id' => null,
                    'plan_operation_id' => (int)$flowId,
                    'number' => ++$number,
                ]);
            }

            $model->populateRelation('orders', $orders);
        }

        return $this->render('create', [
            'model' => $model,
            'company' => $company,
            'returnUrl' => $returnUrl,
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT);

        Yii::$app->session->setFlash('success', 'Заявки успешно удалена');

        return $this->redirect(['index']);
    }

    /**
     * @return Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionManyDelete()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $requests = Yii::$app->request->post('PaymentsRequest');
        foreach ($requests as $id => $request) {
            if ($request['checked']) {
                $model = $this->loadModel($id);
                LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT);
            }
        }

        Yii::$app->session->setFlash('success', 'Заявки успешно удалены');

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index']);
    }

    /**
     * @param $payments_request_id
     * @return Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionManyDeletePlanFlows($payments_request_id)
    {
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $message = 'Операции успешно удалены';

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        /** @var PaymentsRequest $model */
        $model = $this->loadModel($payments_request_id);
        $flows = Yii::$app->request->post('PlanCashFlows');
        foreach ($flows as $id => $flow) {
            if ($flow['checked']) {
                foreach ($model->orders as $order) {
                    if ($order->plan_operation_id == $id) {
                        if (count($order->approved)) {
                            $message = 'Нельзя удалить согласованные операции';
                        } else {
                            History::deleteOrder($order);
                        }
                        // LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE);
                    }
                }
            }
        }

        Yii::$app->session->setFlash('success', $message);

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index']);
    }

    /**
     * @param $payments_request_id
     * @param $approved
     * @return array
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionManyApprovePlanFlows($payments_request_id, $approved = null)
    {
        $user = Yii::$app->user->identity;
        $company = $user->company;

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        /** @var PaymentsRequest $model */
        $model = $this->loadModel($payments_request_id);
        $flows = Yii::$app->request->post('PlanCashFlows');
        foreach ($flows as $id => $flow) {
            if ($flow['checked']) {
                foreach ($model->orders as $order) {
                    if ($order->plan_operation_id == $id) {
                        /** @var OrderApproved $approvedModel */
                        $approvedModel = $order->getApprovedModel($user);

                        if ($approved) {
                            $message = 'Платежи согласованы';
                            $approvedModel->approved = 1;
                            $approvedModel->approved_at = time();
                            if (!$approvedModel->save()) {
                                $message = 'Не удалось согласовать платежи';
                                break;
                            }
                        } else {
                            $message = 'Cогласование платежей отменено';
                            if (!$approvedModel->delete()) {
                                $message = 'Не удалось отменить согласование платежей';
                                break;
                            }
                        }
                        // LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE);
                    }
                }
            }
        }

        return ['success' => 1, 'message' => $message];
    }


    /**
     * @return array|string
     * @throws \Exception
     */
    public function actionGetPlanFlowsModal()
    {
        $user = Yii::$app->user->identity;
        $company = $user->company;

        if ($in_order = Yii::$app->request->post('in_order')) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $flowsData = [];
            foreach ($in_order as $flow_id) {
                if ($model = PlanCashFlows::findOne(['id' => $flow_id, 'company_id' => $company->id])) {
                    $flowsData[] = Order::flow2json($model);
                }
            }

            return $flowsData;
        }

        $searchModel = new PaymentCalendarSearch([
            'company_id' => $company->id,
            'status_type' => PaymentCalendarSearch::STATUS_PLAN,
            'futurePeriod' => true
        ]);

        $dataProvider = $searchModel->searchItems(Yii::$app->request->get());
        $dataProvider->pagination->pageSize = PageSize::get();

        $this->layout = 'empty';

        return $this->render('form/_modal_flows_list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAddChatMessage($payments_request_id)
    {
        $user = Yii::$app->user->identity;
        $company = $user->company;

        /** @var PaymentsRequest $model */
        $model = $this->loadModel($payments_request_id);
        /** @var ChatUser $chatUser */
        $chatUser = $this->loadChatUser($model);

        $chat = new Chat([
            'request_id' => $model->id,
            'company_id' => $model->company_id,
            'author_id' => $user->id,
            'created_at' => time()
        ]);

        if ($chat->load(Yii::$app->request->post())) {
            if ($chat->validate() && $chat->save()) {
                //
            } else {
                $tabData['chat'] = $chat;
            }
        }

        return $this->render('view', [
            'user' => $user,
            'company' => $company,
            'model' => $model,
            'chatUser' => $chatUser,
            'tab' => self::TAB_CHAT,
            'tabData' => $tabData ?? []
        ]);
    }

    /**
     * @param null $id
     * @param null $ioType
     * @return ActiveRecord
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    protected function loadModel($id = null)
    {
        if ($id === null) {
            $id = Yii::$app->request->getQueryParam('id');
        }

        $model = PaymentsRequest::findOne([
            'id' => $id,
            'company_id' => Yii::$app->user->identity->company_id
        ]);

        if ($model === null) {
            throw new NotFoundHttpException('Model loading failed. Model not found!');
        }

        return $model;
    }

    /**
     * @param PaymentsRequest $request
     * @return ChatUser
     */
    protected function loadChatUser(PaymentsRequest $request)
    {
        $chatUser = ChatUser::findOne([
            'request_id' => $request->id,
            'reader_id' => Yii::$app->user->identity->id
        ]) ?: new ChatUser([
            'request_id' => $request->id,
            'reader_id' => Yii::$app->user->identity->id
        ]);

        if ($chatUser->isNewRecord)
            $chatUser->save();

        return $chatUser;
    }

    // AJAX

    public function actionAddPlanFlowModal()
    {
        /** @var Company $company */
        $company = Yii::$app->user->identity->company;

        $checkingAccountant = $company->mainCheckingAccountant;
        $cashbox = $company->getCashboxes()->andWhere(['is_main' => true])->one();
        $emoney = $company->getEmoneys()->andWhere(['is_main' => true])->one();

        $model = new PlanCashFlows();
        $model->company_id = $company->id;
        $model->flow_type = CashFlowsBase::FLOW_TYPE_EXPENSE;
        $model->payment_type = PlanCashFlows::PAYMENT_TYPE_BANK;
        $model->checking_accountant_id = ($checkingAccountant) ? $checkingAccountant->id : null;
        $model->cashbox_id = ($cashbox) ? $cashbox->id : null;
        $model->emoney_id = ($emoney) ? $emoney->id : null;

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())) {

            if ($multiWallet = Yii::$app->request->post('wallet')) {
                @list($paymentType, $accountId) = explode('_', $multiWallet);
                $model->payment_type = $paymentType;
                switch ($paymentType) {
                    case PlanCashFlows::PAYMENT_TYPE_BANK:
                        $model->checking_accountant_id = $accountId;
                        break;
                    case PlanCashFlows::PAYMENT_TYPE_ORDER:
                        $model->cashbox_id = $accountId;
                        break;
                    case PlanCashFlows::PAYMENT_TYPE_EMONEY:
                        $model->emoney_id = $accountId;
                        break;
                    default:
                }
            }

            if ($model->_save()) {

                Yii::$app->response->format = Response::FORMAT_JSON;

                return Order::flow2json($model);
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('@frontend/modules/cash/views/plan/plan_item_form', [
                'model' => $model,
                'company' => $company,
                'createUrl' => Url::current(),
                'flowTypes' => [CashFlowsBase::FLOW_TYPE_EXPENSE => 'Расход'],
                'disableRepeatedFlow' => true
            ]);
        }

        return $this->render('@frontend/modules/cash/views/plan/plan_item_form', [
            'model' => $model,
            'company' => $company,
            'createUrl' => Url::current(),
            'flowTypes' => [CashFlowsBase::FLOW_TYPE_EXPENSE => 'Расход'],
            'disableRepeatedFlow' => true
        ]);
    }

    // SIGN TEMPLATE

    public function actionAddSignTemplate()
    {
        $user = Yii::$app->user->identity;
        $company = $user->company;

        /** @var SignTemplate $template */
        $template = new SignTemplate([
            'company_id' => $company->id,
            'is_active' => true,
            'name' => null,
            'comment' => null,
        ]);

        $templateSteps = [];
        for ($step = 1; $step <= (SignTemplate::MAX_STEPS_COUNT); $step++) {
            $templateSteps[] = new SignTemplateStep([
                'id' => '_new_' . $step,
                'isNewRecord' => false,
                'name' => null,
                'template_id' => null,
                'employee_id' => null,
                'isVisible' => ($step == 1)
            ]);
        }

        if (Yii::$app->request->post('ajax') !== null) {

            $template->load(Yii::$app->request->post());
            $templateValidation = ActiveForm::validate($template);

            $hasSteps = false;
            $stepsValidation = [];
            /** @var SignTemplateStep $step */
            foreach ($templateSteps as $step) {
                $step->load(Yii::$app->request->post());
                if (trim($step->name)) {
                    $hasSteps = true;
                    $stepsValidation = array_merge($stepsValidation, ActiveForm::validate($step));
                }
            }

            $noStepsValidation = (!$hasSteps)
                ? [Html::getInputId($template, 'hasSteps') => ['Необходимо добавить как минимум один шаг подписания']]
                : [];

            Yii::$app->response->format = Response::FORMAT_JSON;

            return array_merge($templateValidation, $noStepsValidation, $stepsValidation);
        }

        if ($template->load(Yii::$app->request->post())) {

            $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($template, $templateSteps) {
                if ($template->validate() && $template->save()) {
                    $stepNum = 0;
                    /** @var SignTemplateStep $step */
                    foreach ($templateSteps as $step) {
                        if ($step->load(Yii::$app->request->post()) && trim($step->name)) {
                            $step->id = null;
                            $step->isNewRecord = true;
                            $step->step = ++$stepNum;
                            $step->template_id = $template->primaryKey;
                            if ($step->save()) {
                                continue;
                            } else {
                                $db->transaction->rollBack();
                                return false;
                            }
                        }
                    }

                    return true;
                }

                $db->transaction->rollBack();
                return false;
            });

            if ($isSaved) {
                Yii::$app->session->setFlash('success', 'Шаблон подписания создан');
            } else {
                $debug = serialize($template->getErrors()) . serialize(array_map(function($step) { return $step->getErrors(); }, $templateSteps));
                Yii::$app->session->setFlash('error', 'Не удалось создать шаблон подписания ' . $debug);
            }

            return $this->redirect(Yii::$app->request->referrer ?: ['index']);
        }

        // $template->populateRelation('steps', $templateSteps);

        return $this->renderAjax('index/_sign_template_form', [
            'user' => $user,
            'company' => $company,
            'template' => $template,
            'templateSteps' => $templateSteps
        ]);
    }

    public function actionUpdateSignTemplate($id)
    {
        $user = Yii::$app->user->identity;
        $company = $user->company;

        /** @var SignTemplate $template */
        $template = $this->loadTemplate($id);

        $templateSteps = $template->steps;
        for ($step = count($template->steps); $step < (SignTemplate::MAX_STEPS_COUNT); $step++) {
            $templateSteps[] = new SignTemplateStep([
                'id' => '_new_' . $step,
                'isNewRecord' => false,
                'name' => null,
                'template_id' => null,
                'employee_id' => null,
                'isVisible' => ($step == 1)
            ]);
        }

        if (Yii::$app->request->post('ajax') !== null) {

            $template->load(Yii::$app->request->post());
            $templateValidation = ActiveForm::validate($template);

            $hasSteps = false;
            $stepsValidation = [];
            /** @var SignTemplateStep $step */
            foreach ($templateSteps as $step) {
                $step->load(Yii::$app->request->post());
                if ($step->name) {
                    $hasSteps = true;
                    $stepsValidation = array_merge($stepsValidation, ActiveForm::validate($step));
                }
            }

            $noStepsValidation = (!$hasSteps)
                ? [Html::getInputId($template, 'hasSteps') => ['Необходимо добавить как минимум один шаг подписания']]
                : [];

            Yii::$app->response->format = Response::FORMAT_JSON;

            return array_merge($templateValidation, $noStepsValidation, $stepsValidation);
        }

        if ($template->load(Yii::$app->request->post())) {

            $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($template, $templateSteps) {
                if ($template->validate() && $template->save()) {
                    $stepNum = 0;
                    /** @var SignTemplateStep $step */
                    foreach ($templateSteps as $step) {
                        if ($step->load(Yii::$app->request->post()) && $step->name) {

                            if ($step->getIsNew()) {
                                $step->id = null;
                                $step->isNewRecord = true;
                            }

                            $step->step = ++$stepNum;
                            $step->template_id = $template->primaryKey;
                            if (!$step->save()) {
                                $db->transaction->rollBack();
                                return false;
                            }
                        }
                    }

                    return true;
                }

                $db->transaction->rollBack();
                return false;
            });

            if ($isSaved) {
                Yii::$app->session->setFlash('success', 'Шаблон подписания изменен');
            } else {
                $debug = serialize($template->getErrors()) . serialize(array_map(function($step) { return $step->getErrors(); }, $templateSteps));
                Yii::$app->session->setFlash('error', 'Не удалось обновить шаблон подписания ' . $debug);
            }

            return $this->redirect(Yii::$app->request->referrer ?: ['index']);
        }

        // $template->populateRelation('steps', $templateSteps);

        return $this->renderAjax('index/_sign_template_form', [
            'user' => $user,
            'company' => $company,
            'template' => $template,
            'templateSteps' => $templateSteps
        ]);
    }

    public function actionDeleteSignTemplate($id)
    {
        /** @var SignTemplate $template */
        $template = $this->loadTemplate($id);

        if ($template->isPresentInRequests) {
            $template->setAttributes(['is_active' => 0]);
            Yii::$app->session->setFlash('success', 'Шаблон перемещен в архив');
        } else {
            if ($template->delete())
                Yii::$app->session->setFlash('success', 'Шаблон удален');
        }

        return $this->redirect(Yii::$app->request->referrer ?: ['index']);
    }

    // SIGN

    public function actionSignPaymentsRequest($id, $step)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $user = Yii::$app->user->identity;

        /** @var PaymentsRequest $model */
        $model = $this->loadModel($id);
        foreach ($model->signSteps as $signStep) {
            if ($signStep->step == $step) {
                if ($user->id === $signStep->employee_id) {
                    if (History::signStep($signStep)) {
                        if ($signStep->prev()) {
                            $signStep->prev()->updateAttributes(['can_unsign' => 0]);
                        }
                        if ($signStep->next()) {
                            $signStep->next()->updateAttributes(['can_sign' => 1]);
                        }
                        return [
                            'success' => 1,
                            'status_name' => $signStep->name,
                            'signed_name' => 'Подписано',
                            'signed_at' => date('d.m.Y H:i', $signStep->signed_at)
                        ];
                    }
                }
            }
        }

        return [];
    }

    public function actionUnsignPaymentsRequest($id, $step)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $user = Yii::$app->user->identity;
        /** @var PaymentsRequest $model */
        $model = $this->loadModel($id);

        foreach ($model->signSteps as $signStep) {
            if ($signStep->step == $step) {
                if ($user->id === $signStep->employee_id) {
                    if (History::unsignStep($signStep)) {
                        if ($signStep->prev()) {
                            $signStep->prev()->updateAttributes(['can_unsign' => 1]);
                        }
                        if ($signStep->next()) {
                            $signStep->next()->updateAttributes(['can_sign' => 0]);
                        }
                        return [
                            'success' => 1,
                            'status_name' => ($signStep->prev()) ? $signStep->prev()->name : $model->status->name,
                            'signed_name' => 'Ожидает подписания',
                            'signed_at' => null
                        ];
                    }
                }
            }
        }

        return [];
    }

    /**
     * @param null $id
     * @param null $ioType
     * @return ActiveRecord
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    protected function loadTemplate($id = null)
    {
        if ($id === null) {
            $id = Yii::$app->request->getQueryParam('id');
        }

        $model = SignTemplate::findOne([
            'id' => $id,
            'company_id' => Yii::$app->user->identity->company_id
        ]);

        if ($model === null) {
            throw new NotFoundHttpException('Template model loading failed. Model not found!');
        }

        return $model;
    }
}