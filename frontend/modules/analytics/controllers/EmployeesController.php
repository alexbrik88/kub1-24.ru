<?php

namespace frontend\modules\analytics\controllers;

use common\components\filters\AccessControl;
use common\models\employee\Employee;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\modules\analytics\models\EmployeesReportSearch;
use frontend\rbac\UserRole;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Response;

/**
 * Class EmployeesController
 * @package frontend\modules\analytics\controllers
 */
class EmployeesController extends FrontendController
{
    /**
     * @var string
     */
    public $layout = 'employees';

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function($rule, $action) {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_SALES);
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new EmployeesReportSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
