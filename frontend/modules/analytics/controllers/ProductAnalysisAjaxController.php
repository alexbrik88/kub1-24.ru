<?php

namespace frontend\modules\analytics\controllers;

use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\modules\analytics\models\productAnalysis\ProductAnalysisCharts;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use common\components\filters\AccessControl;

/**
 * Class FinanceAjaxController
 */
class ProductAnalysisAjaxController extends FrontendController
{
    const VIEW = '@frontend/modules/reports/views/product-analysis/';
    static $monthsByQuarter = [1 => [1,3], 2 => [4,6], 3 => [7,9], 4 => [10,12]];

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function($rule, $action) {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_PRODUCTS);
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string|null
     */
    public function actionGetPlanFactData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('chart-plan-fact-ajax')) {

            $yearMonth = Yii::$app->request->post('yearMonth');
            $period = Yii::$app->request->post('period');
            $offset = Yii::$app->request->post('offset');
            $group = Yii::$app->request->post('group');

            $searchModel = new ProductAnalysisCharts();
            $searchModel->yearMonth = $yearMonth;

            return $this->renderPartial(self::VIEW . 'charts/chart_plan_fact_days', [
                'searchModel' => $searchModel,
                'customOffset' => $offset,
                'customPeriod' => $period,
                'customGroup' => $group
            ]);
        }

        return null;
    }
}