<?php

namespace frontend\modules\analytics\controllers;

use common\models\employee\Employee;
use frontend\modules\analytics\models\financeModel\FinanceModelSearch;
use Yii;
use common\components\filters\AccessControl;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\components\StatisticPeriod;
use frontend\modules\analytics\models\ScenarioAnalysisSearch;
use frontend\modules\analytics\models\WhatIfSearch;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\NotFoundHttpException;

class PlanningController extends FrontendController
{
    /**
     * @var string
     */
    public $layout = 'planning';

    /**
     * @return array
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function ($rule, $action) {

                            if (BusinessAnalyticsAccess::matchEmployee(BusinessAnalyticsAccess::SECTION_SALES))
                                if (BusinessAnalyticsAccess::matchSubscribe(BusinessAnalyticsAccess::SECTION_SALES))
                                    if (BusinessAnalyticsAccess::matchLimit(BusinessAnalyticsAccess::SECTION_SALES))
                                        return true;

                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_FINANCE_PLUS);
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * @param \yii\base\Action $action
     * @return bool|\yii\web\Response
     */
    public function beforeAction($action)
    {
        if ($action->id == 'what-if' && ($showGroups = Yii::$app->request->get('defaultSorting'))) {
            Yii::$app->user->identity->config->updateAttributes([
                'report_abc_show_groups' => ($showGroups == 'show_groups') ? 1 : 0
            ]);

            return $this->redirect(Url::current(['defaultSorting' => null]));
        }

        return parent::beforeAction($action);
    }

    public function actionWhatIf()
    {
        $company = Yii::$app->user->identity->company;
        $searchModel = new WhatIfSearch($company, [
            'showGroups' => Yii::$app->user->identity->config->report_abc_show_groups,
        ]);
        $dataProvider = $searchModel->search(array_merge(Yii::$app->request->get(), Yii::$app->request->post()));

        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 131, true);

        return $this->render('what-if', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionScenario()
    {
        $company = Yii::$app->user->identity->company;
        $searchModel = new ScenarioAnalysisSearch($company);
        $data = $searchModel->search(array_merge(Yii::$app->request->get(), Yii::$app->request->post()));

        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 132, true);

        return $this->render('scenario', [
            'searchModel' => $searchModel,
            'data' => $data,
        ]);
    }

    public function actionFinancialModel()
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $searchModel = new FinanceModelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('finance-model', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
        ]);
    }

    public function actionIndex()
    {
        return $this->render('empty', ['title' => 'Планирование']);
    }

    public function actionSalesFunnel()
    {
        return $this->render('empty', [
            'title' => 'Воронка продаж',
            'subtitle' => 'Для включения интеграции с вашей CRM напишите нам в тех. поддержку.'
        ]);
    }
}
