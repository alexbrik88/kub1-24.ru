<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.04.2018
 * Time: 18:43
 */

namespace frontend\modules\analytics\controllers;

use common\components\filters\AccessControl;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\modules\analytics\models\ProfitSearch;
use frontend\rbac\UserRole;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class ProfitController
 * @package frontend\modules\analytics\controllers
 */
class ProfitController extends FrontendController
{
    public $layout = 'finance';

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function($rule, $action) {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_FINANCE);
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ProfitSearch(Yii::$app->request->get());
        $receiptData = $searchModel->calculateReceiptData();
        $priceData = $searchModel->calculatePriceData();
        $grossProfit =  $searchModel->calculateGrossProfit($receiptData, $priceData);
        $consumptionData = $searchModel->calculateConsumptionData();
        $profitOrLesion = $searchModel->calculateProfitOrLesion($grossProfit, $consumptionData['total']);
        $tax = $searchModel->calculateTax($profitOrLesion);
        $netProfit = $searchModel->calculateNetProfit($profitOrLesion, $tax);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'receiptData' => $receiptData,
            'priceData' => $priceData,
            'grossProfit' => $grossProfit,
            'consumptionData' => $consumptionData,
            'profitOrLesion' => $profitOrLesion,
            'tax' => $tax,
            'netProfit' => $netProfit,
        ]);
    }
}
