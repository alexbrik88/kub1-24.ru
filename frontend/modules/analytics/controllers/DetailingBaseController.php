<?php

namespace frontend\modules\analytics\controllers;

use common\components\filters\AccessControl;
use common\models\company\CompanyIndustry;
use common\models\company\CompanyInfoIndustry;
use common\models\companyStructure\SalePoint;
use common\models\companyStructure\SalePointType;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\employee\Employee;
use common\models\product\Store;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\components\PageSize;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\analytics\models\AnalyticsArticleForm;
use frontend\modules\analytics\models\detailing\CompanyIndustrySearch;
use frontend\modules\analytics\models\detailing\SalePointSearch;
use frontend\modules\analytics\models\OddsSearch;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class DetailingController
 * @package frontend\modules\analytics\controllers
 */
class DetailingBaseController extends FrontendController
{
    const PAGE_INDUSTRY = 'industry';
    const PAGE_SALE_POINT = 'sale_point';
    const PAGE_PROJECT = 'project';

    protected function _getDashboardOddsModel($oddsPageFilter): OddsSearch
    {
        $oddsModel = new OddsSearch();
        // todo: $oddsModel->disableMultiCompanyMode();

        return $oddsModel;
    }

    protected function _getDashboardPalModel($palPageFilter): ProfitAndLossSearchModel
    {
        $palModel = new ProfitAndLossSearchModel($palPageFilter);
        $palModel->activeTab = (string)ProfitAndLossSearchModel::TAB_ALL_OPERATIONS;
        $palModel->disableMultiCompanyMode();
        $palModel->setYearFilter([$palModel->year]);
        $palModel->setUserOption('monthGroupBy', AnalyticsArticleForm::MONTH_GROUP_UNSET);
        $palModel->setUserOption('revenueRuleGroup', AnalyticsArticleForm::REVENUE_RULE_GROUP_UNSET);
        $palModel->setUserOption('revenueRule', AnalyticsArticleForm::REVENUE_RULE_UNSET);
        $palModel->setUserOption('calcWithoutNds', false);
        $palModel->handleItems();

        return $palModel;
    }

    protected function _getIndexPagePalModel(string $onPage): ProfitAndLossSearchModel
    {
        switch ($onPage) {
            case self::PAGE_INDUSTRY:
                $groupBy = AnalyticsArticleForm::MONTH_GROUP_INDUSTRY;
                break;
            case self::PAGE_SALE_POINT:
                $groupBy = AnalyticsArticleForm::MONTH_GROUP_SALE_POINT;
                break;
            case self::PAGE_PROJECT:
            default:
                $groupBy = AnalyticsArticleForm::MONTH_GROUP_PROJECT;
                break;
        }

        $palModel = new ProfitAndLossSearchModel();
        $palModel->activeTab = (string)ProfitAndLossSearchModel::TAB_ALL_OPERATIONS;
        $palModel->disableMultiCompanyMode();
        //$palModel->setYearFilter([$palModel->year]);
        $palModel->setUserOption('revenueRuleGroup', AnalyticsArticleForm::REVENUE_RULE_GROUP_UNSET);
        $palModel->setUserOption('revenueRule', AnalyticsArticleForm::REVENUE_RULE_UNSET);
        $palModel->setUserOption('calcWithoutNds', false);
        $palModel->setUserOption('monthGroupBy', $groupBy);
        $palModel->handleItems();

        return $palModel;
    }
}