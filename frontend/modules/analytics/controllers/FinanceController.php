<?php

namespace frontend\modules\analytics\controllers;

use common\components\date\DateHelper;
use common\components\debts\DebtsHelper;
use common\components\filters\AccessControl;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use common\models\company\CompanyFirstEvent;
use common\models\currency\Currency;
use common\models\document\InvoiceExpenditureItem;
use common\models\employee\Employee;
use common\models\ExpenseItemFlowOfFunds;
use common\models\IncomeItemFlowOfFunds;
use common\models\product\Product;
use common\models\service\SubscribeTariffGroup;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\components\PageSize;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\analytics\models\AnalyticsArticleForm;
use frontend\modules\analytics\models\balance\BalanceInitial;
use frontend\modules\analytics\models\BreakEvenSearchModel;
use frontend\modules\analytics\models\credits\CreditRepository;
use frontend\modules\analytics\models\credits\CreditSelectForm;
use frontend\modules\analytics\models\credits\CreditSelectFormFactory;
use frontend\modules\analytics\models\credits\RepositoryFactory;
use frontend\modules\analytics\models\DebtReportSearchAsBalance;
use frontend\modules\analytics\models\OddsSearch;
use frontend\modules\analytics\models\OlapTables;
use frontend\modules\analytics\models\operationalEfficiency\OperationalEfficiencySearchModel;
use frontend\modules\analytics\models\StocksSearch;
use frontend\modules\cash\models\CashContractorType;
use frontend\modules\analytics\models\AnalysisSearch;
use frontend\modules\analytics\models\BalanceCompanyItem;
use frontend\modules\analytics\models\BalanceItemAmount;
use frontend\modules\analytics\models\BalanceSearch;
use frontend\modules\analytics\models\CashFlowsAutoPlan;
use frontend\modules\analytics\models\debtor\DebtorHelper2;
use frontend\modules\analytics\models\DebtReportSearch2;
use frontend\modules\analytics\models\ExpensesSearch;
use frontend\modules\analytics\models\IncomeSearch;
use frontend\modules\analytics\models\FlowOfFundsReportSearch;
use frontend\modules\analytics\models\PaymentCalendarSearch;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\modules\analytics\models\PlanFactSearch;
use frontend\modules\analytics\models\ProfitAndLossCompanyItem;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;
use frontend\rbac\UserRole;
use Yii;
use yii\base\InvalidParamException;
use yii\db\ActiveRecord;
use yii\db\Connection;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class FinanceController
 * @package frontend\modules\analytics\controllers
 */
class FinanceController extends FrontendController
{
    /**
     * @var
     */
    public $year;

    /**
     * @var string
     */
    public $layout = 'finance';

    /**
     * @var array
     */
    static private $analyticsClasses = [
        'FlowOfFundsReportSearch',
        'ProfitAndLossSearchModel',
        'PlanFactSearch',
        'BalanceSearch', // todo: delete (after 20-354)
        'BalanceSearch2', // todo: rename in BalanceSearch (after 20-354)
        'ExpensesSearch',
        'IncomeSearch',
        'PaymentCalendarSearch',
        'DebtReportSearch2',
        'StocksSearch',
        'OddsSearch',
        'OperationalEfficiencySearchModel',
    ];

    static private $optionsClasses = [
        'AnalyticsArticleForm'
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'item-list', // get docs/flows list
                            'debtor-change-view' // change table view
                        ],
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\Reports::VIEW,
                        ],
                    ],
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function($rule, $action) {

                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_FINANCE);
                        },
                    ],
                ],
            ],
        ]);
    }

    public function beforeAction($action)
    {
        $user = Yii::$app->user->identity;
        $company = $user->currentEmployeeCompany->company;

        if (!Yii::$app->request->isAjax) {
            foreach (self::$analyticsClasses as $class) {
                if (\Yii::$app->request->get($class)) {
                    $year = \common\components\helpers\ArrayHelper::getValue(\Yii::$app->request->get($class), 'year');
                    if ($year > 2010 && $year < 2100) {
                        \Yii::$app->session->set('modules.reports.finance.year', $year);
                        $getParams = \Yii::$app->request->queryParams;
                        unset($getParams[$class]);
                        return $this->redirect(Url::toRoute([Url::base()] + $getParams));
                    }
                }
            }
            foreach (self::$optionsClasses as $class) {
                if (\Yii::$app->request->get($class)) {
                    $optionsForm = new AnalyticsArticleForm($company);
                    if ($optionsForm->load(Yii::$app->request->get()) && $optionsForm->validate()) {
                        $optionsForm->saveAdditionalOptions();
                        Yii::$app->session->setFlash('success', 'Настройки сохранены');
                    }
                    $getParams = \Yii::$app->request->queryParams;
                    unset($getParams[$class]);
                    return $this->redirect(Url::toRoute([Url::base()] + $getParams));
                }
            }
        }

        return parent::beforeAction($action);
    }

    public function actionRefresh()
    {
        ini_set('max_execution_time', 1800);

        OlapTables::refresh(Yii::$app->user->identity->company->id);

        Yii::$app->session->setFlash('success', 'Данные успешно обновлены');

        return $this->redirect(Yii::$app->request->referrer ?: ['/analytics/finance/odds']);
    }

    /**
     * @return mixed
     */
    public function actionXyz()
    {
        $searchModel = new AnalysisSearch([
            'company' => Yii::$app->user->identity->company,
            'dateRange' => StatisticPeriod::getSessionPeriod(),
        ]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        $dataProvider->pagination->pageSize = PageSize::get();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param string $periodSize
     * @return string
     * @throws \Exception
     */
    public function actionOdds($currency = null, $periodSize = 'months')
    {
        if (empty($currency) || !in_array($currency, Currency::$currencyList)) {
            $currencyName = Currency::DEFAULT_NAME;
        } else {
            $currencyName = $currency;
        }
        $currencyId = array_search($currencyName, Currency::$currencyList);

        $employee = Yii::$app->user->identity;
        $activeTab = (string)Yii::$app->request->get('activeTab', $employee->config->report_odds_active_tab);

        $searchModel = new OddsSearch();
        $data = ($periodSize == 'days') ?
            $searchModel->searchByDays($activeTab, Yii::$app->request->get()) :
            $searchModel->search($activeTab, Yii::$app->request->get());

        $warnings = $searchModel->generateWarnings(ArrayHelper::getValue($data['totalData'], 'growing.data'), $activeTab == OddsSearch::TAB_ODDS ? $data['data'] : []);

        self::updateOddsActiveTab($employee);
        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 108, true);
        CompanyFirstEvent::checkEvent($employee->company, 60);

        return $this->render('odds2', [
            'searchModel' => $searchModel,
            'data' => $data['data'],
            'growingData' => $data['growingData'],
            'totalData' => $data['totalData'],
            'activeTab' => $activeTab,
            'periodSize' => $periodSize,
            'warnings' => $warnings,
            'currencyId' => $currencyId,
            'currencyName' => $currencyName,
        ]);
    }

    /**
     * @return bool
     */
    public function actionChangeBalanceBlock()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $items = Yii::$app->request->post('items', []);
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        foreach ($items as $itemID => $sort) {
            /* @var $balanceCompanyItem BalanceCompanyItem */
            $balanceCompanyItem = BalanceCompanyItem::find()
                ->andWhere(['and',
                    ['company_id' => $company->id],
                    ['item_id' => $itemID],
                ])->one();
            if ($balanceCompanyItem) {
                $balanceCompanyItem->sort = (int)$sort;
                $balanceCompanyItem->save(true, ['sort']);
            }
        }
        return true;
    }

    /**
     * @return array
     */
    public function actionChangeProfitAndLossBlock()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $itemID = Yii::$app->request->post('item_id');
        $expenseType = Yii::$app->request->post('expense_type');
        $year = Yii::$app->request->post('year');
        if ($itemID && in_array($expenseType, array_keys(ProfitAndLossCompanyItem::$expenseTypes)) && $year) {
            /* @var $model ProfitAndLossCompanyItem */
            $model = ProfitAndLossCompanyItem::find()
                ->andWhere(['and',
                    ['company_id' => $company->id],
                    ['item_id' => $itemID],
                    ['can_update' => true],
                ])->one();
            if ($model) {
                $model->expense_type = $expenseType;
                if ($model->save()) {
                    $searchModel = new ProfitAndLossSearchModel();
                    $searchModel->year = $year;
                    $currentMonthNumber = date('n');
                    $currentQuarter = (int)ceil(date('m') / 3);
                    $data = $searchModel->handleItems();

                    return [
                        'result' => true,
                        'html' => $this->renderPartial('_partial/profit_and_loss_table', [
                            'searchModel' => $searchModel,
                            'currentMonthNumber' => $currentMonthNumber,
                            'currentQuarter' => $currentQuarter,
                            'data' => $data,
                        ]),
                    ];
                }
            }
        }

        return [
            'result' => false,
        ];
    }

    /**
     * @return array
     * @throws \yii\base\Exception
     */
    public function actionChangeBlock()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        /* @var $company Company */
        $company = $user->company;
        $itemID = Yii::$app->request->post('item_id');
        $blockTypeID = Yii::$app->request->post('block_type_id');
        $type = Yii::$app->request->post('type');
        $year = Yii::$app->request->post('year');
        $reportType = Yii::$app->request->post('reportType');
        $periodSize = Yii::$app->request->post('periodSize', 'months');
        if ($itemID !== null && $blockTypeID !== null && $type !== null && $year !== null && $reportType) {
            /* @var $className IncomeItemFlowOfFunds|ExpenseItemFlowOfFunds */
            $className = null;
            $block = FlowOfFundsReportSearch::$blockByType[$blockTypeID];
            if ($type == 'income') {
                $className = IncomeItemFlowOfFunds::className();
            } elseif ($type == 'expense') {
                $className = ExpenseItemFlowOfFunds::className();
            } else {
                return ['result' => false];
            }
            /* @var $model IncomeItemFlowOfFunds|ExpenseItemFlowOfFunds */
            $model = $className::find()->andWhere(['and',
                ['company_id' => $user->company->id],
                [$type . '_item_id' => $itemID],
            ])->one();
            if ($model) {
                $model->flow_of_funds_block = $block;
                if ($model->save()) {
                    if ($reportType == 'odds') {

                        $searchModel = new OddsSearch();
                        $searchModel->year = $year;
                        $activeTab = FlowOfFundsReportSearch::TAB_ODDS;

                        $dataFull = ($periodSize == 'days') ?
                            $searchModel->searchByDays($activeTab, Yii::$app->request->get()) :
                            $searchModel->search($activeTab, Yii::$app->request->get());

                        return [
                            'result' => true,
                            'html' => $this->renderPartial('_partial/odds2_table' . ($periodSize == 'days' ? '_days' : ''), [
                                'searchModel' => $searchModel,
                                'activeTab' => $activeTab,
                                'userConfig' => Yii::$app->user->identity->config,
                                'currentMonthNumber' => date('n'),
                                'currentQuarter' => (int)ceil(date('m') / 3),
                                'data' => &$dataFull['data'],
                                'growingData' => &$dataFull['growingData'],
                                'totalData' => &$dataFull['totalData'],
                                'floorMap' => Yii::$app->request->post('floorMap', [])
                            ]),
                        ];
                    } else {
                        $searchModel = new PaymentCalendarSearch();
                        $searchModel->year = $year;
                        $tableData = $searchModel->searchByActivity2($periodSize == 'days');

                        return [
                            'result' => true,
                            'html' => $this->renderPartial('_partial/payment_calendar_table', [
                                'searchModel' => $searchModel,
                                'activeTab' => PaymentCalendarSearch::TAB_BY_ACTIVITY,
                                'userConfig' => $user->config,
                                'data' => &$tableData['data'],
                                'totalData' => &$tableData['totalData'],
                                'growingData' => &$tableData['growingData'],
                                'floorMap' => Yii::$app->request->post('floorMap', [])
                            ]),
                        ];
                    }
                }
            }
        }

        return ['result' => false];
    }

    /**
     * @return bool|string
     * @throws \Exception
     */
    public function actionItemList()
    {
        $items = Yii::$app->request->post('items');
        $items = json_decode($items, true);
        $tableFilterItems = Yii::$app->request->get('FlowOfFundsReportSearch');

        if ($items) {

            $reportType = $items['reportType'] ?? 'pk';
            $activeTab = $items['activeTab'] ?? 4;

            if ($reportType == 'odds' || $reportType == 'expenses' || $reportType == 'income') {
                $searchModel = new FlowOfFundsReportSearch();
                if (strlen($items['industry_id'] ?? '')) {
                    $searchModel->industry_id = $items['industry_id'];
                } elseif (strlen($tableFilterItems['industry_id'] ?? '')) {
                    $searchModel->industry_id = $tableFilterItems['industry_id'];
                }
                if (strlen($items['sale_point_id'] ?? '')) {
                    $searchModel->sale_point_id = $items['sale_point_id'];
                } elseif (strlen($tableFilterItems['sale_point_id'] ?? '')) {
                    $searchModel->sale_point_id = $tableFilterItems['sale_point_id'];
                }
                if (strlen($items['project_id'] ?? '')) {
                    $searchModel->project_id = $items['project_id'];
                } elseif (strlen($tableFilterItems['project_id'] ?? '')) {
                    $searchModel->project_id = $tableFilterItems['project_id'];
                }

                $searchModel->income_item_id = $items['income'] ?? null;
                $searchModel->expenditure_item_id = $items['expense'] ?? null;

                // glue consolidated companies subitems (by name)
                if (is_array($searchModel->income_item_id)) {
                    foreach ($searchModel->income_item_id as $income_item_id)
                        if (strpos($income_item_id, '-'))
                            $searchModel->income_item_id = array_merge($searchModel->income_item_id, explode('-', $income_item_id));
                }
                if (is_array($searchModel->expenditure_item_id)) {
                    foreach ($searchModel->expenditure_item_id as $expenditure_item_id)
                        if (strpos($expenditure_item_id, '-'))
                            $searchModel->expenditure_item_id = array_merge($searchModel->expenditure_item_id, explode('-', $expenditure_item_id));
                }
                
                $view = '_partial/item_table';
            } else {
                $searchModel = new PaymentCalendarSearch();
                $searchModel->payment_type = $items['payment_type'] ?? null;
                $searchModel->industry_id = $items['industry_id'] ?? null;
                $searchModel->sale_point_id = $items['sale_point_id'] ?? null;
                $searchModel->income_item_id = $items['income'] ?? null;
                $searchModel->expenditure_item_id = $items['expense'] ?? null;

                $view = '_partial/plan_item_table';
            }

            $searchModel->period = isset($items['period']) ? $items['period'] : null;
            $searchModel->group = isset($items['group']) ? $items['group'] : null;
            $searchModel->flow_type = isset($items['flow_type']) ? $items['flow_type'] : null;
            $searchModel->wallet_id = isset($items['wallet_id']) ? $items['wallet_id'] : null;
            if (array_key_exists('currency_payment_type', $items)) {
                @list($currency_name, $payment_type) = explode('_', $items['currency_payment_type']);
                $searchModel->payment_type = $payment_type;
                if (property_exists($searchModel, 'currency_name')) {
                    $searchModel->currency_name = $currency_name;
                }
            } else {
                $searchModel->payment_type = isset($items['payment_type']) ? $items['payment_type'] : null;
                if (property_exists($searchModel, 'currency_name')) {
                    $searchModel->currency_name = null;
                }
            }

            $itemsDataProvider = $searchModel->searchItems(Yii::$app->request->get());
            $itemsDataProvider->pagination->pageSize = \frontend\components\PageSize::get('per-page', 50);

            return $this->render($view, [
                'searchModel' => $searchModel,
                'itemsDataProvider' => $itemsDataProvider,
                'activeTab' => $activeTab,
                'subTab' => null
            ]);
        }

        return false;
    }

    /**
     * @return string
     */
    public function actionBalanceOld()
    {
        $model = new BalanceSearch();

        if (Yii::$app->request->get('BalanceSearch')) {
            $bSearch = Yii::$app->request->get('BalanceSearch');
            $model->setYear(ArrayHelper::getValue($bSearch, 'year'));
        }
        $balance = $model->search(Yii::$app->request->queryParams);
        $this->year = $model->year;

        BalanceCompanyItem::loadCompanyItems();

        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 121, true);

        return $this->render('balance', [
            'model' => $model,
            'balance' => $balance,
        ]);
    }

    /**
     * @return string
     */
    public function actionBalance()
    {
        $model = new \frontend\modules\analytics\models\BalanceSearch2();
        $balance = $model->search();

        return $this->render('balance', [
            'model' => $model,
            'balance' => $balance,
        ]);
    }

    public function actionBalanceInitial()
    {
        $model = new \frontend\modules\analytics\models\BalanceSearch2();
        $initialModel = $this->getModelBalanceInitial();

        return $this->render('balance-initial', [
            'model' => $model,
            'initialModel' => $initialModel,
            'balance' => $model->searchInitial()
        ]);
    }

    public function actionUpdateBalanceInitial($param)
    {
        $company = Yii::$app->user->identity->company;
        $initialModel = $this->getModelBalanceInitial();
        $value = Yii::$app->request->post($param);

        $updated = false;
        switch ($param) {
            case BalanceInitial::MAIN_DATE:
                $initialModel->date = DateHelper::format($value, 'Y-m-d', 'd.m.Y');
                $updated = $initialModel->save();
                break;
            case BalanceInitial::UNDISTRIBUTED_PROFIT:
                $initialModel->undistributed_profit = round($value * 100);
                $updated = $initialModel->save();
                break;
            case BalanceInitial::CAPITAL:
                $company->capital = round($value * 100);
                if ($company->validate(['capital'])) {
                    $updated = $company->updateAttributes(['capital' => $company->capital]);
                }
            case BalanceInitial::FIXED_ASSETS_OTHER:
                $initialModel->fixed_assets_other = round($value * 100);
                $updated = $initialModel->save();
                break;
            case 'newProductInitialBalanceDate':
                $productsIds = (array)Yii::$app->request->post('ProductInitialBalance');
                foreach ($productsIds as $productId => $checked) {
                    if ($product = Product::findOne(['id' => $productId, 'company_id' => $company->id])) {
                        if ($productInitialBalance = $product->initialBalance) {
                            $productInitialBalance->date = DateHelper::format($value, 'Y-m-d', 'd.m.Y');
                            $updated = $productInitialBalance->save();
                        }
                    }
                }
        }

        if ($updated)
            Yii::$app->session->setFlash('success', 'Данные успешно обновлены');
        else
            Yii::$app->session->setFlash('error', 'Не удалось обновить данные');

        return $this->redirect(Yii::$app->request->referrer ?: ['balance-initial']);
    }

    public function actionChangeBalancePage($page, $activeCheckboxes)
    {
        Yii::$app->session->set('balance.activeCheckboxes', explode(',', $activeCheckboxes));
        return $this->redirect($page);
    }

    public function actionRefreshBalancePage($activeCheckboxes)
    {
        Yii::$app->session->set('balance.activeCheckboxes', explode(',', $activeCheckboxes));
        return $this->redirect(Yii::$app->request->referrer ?: 'balance');
    }

    /**
     * @return Response
     * @throws \Throwable
     */
    public function actionUpdateBalanceItem()
    {
        $year = Yii::$app->request->post('year');
        $itemID = Yii::$app->request->post('itemID');
        $itemAmount = Yii::$app->request->post('BalanceItemAmount');
        Yii::$app->session->set('activeCheckbox', Yii::$app->request->post('activeCheckbox'));
        if ($itemID && $year && $itemAmount) {
            if (BalanceItemAmount::loadItems($itemAmount, $itemID, $year)) {
                Yii::$app->session->setFlash('success', 'Данные обновлены.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при обновлении данных.');
            }
        }

        return $this->redirect(['balance', 'year' => $year]);
    }

    /**
     * @param int $activeTab
     * @param string $periodSize
     * @return string
     * @throws \Exception
     */
    public function actionPaymentCalendar($activeTab = PaymentCalendarSearch::TAB_BY_ACTIVITY, $periodSize = 'months')
    {
        $activeTab = (string)$activeTab;
        $subTab = Yii::$app->request->get('subTab', 'table');
        $searchModel = new PaymentCalendarSearch();

        $tableData = ($subTab == 'table') ?
            $searchModel->search2($activeTab, Yii::$app->request->get(), $periodSize == 'days') : [];

        $registerProvider = ($subTab == 'register') ?
            $searchModel->searchItems(Yii::$app->request->get()) : null;

        $this->year = $searchModel->year; // wtf??

        if ($activeTab != PaymentCalendarSearch::TAB_CALENDAR)
            Yii::$app->session->set('payment_calendar.activeTab', $activeTab);

        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 109, true);

        return $this->render('payment-calendar', [
            'activeTab' => $activeTab,
            'subTab' => $subTab,
            'periodSize' => $periodSize,
            'searchModel' => $searchModel,
            'tableData' => $tableData,
            'itemsDataProvider' => $registerProvider
        ]);
    }

    /**
     * @param int $activeTab
     * @return string
     * @throws \Exception
     */
    public function actionBreakEvenPoint()
    {
        $searchModel = new BreakEvenSearchModel();
        $searchModel->load(Yii::$app->request->get());
        $this->year = $searchModel->year;
        $data = $searchModel->handleItems();

        // ProfitAndLossCompanyItem::loadCompanyItems();

        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 119, true);

        return $this->render('break-even-point', [
            'searchModel' => $searchModel,
            'data' => $data,
            'customerInputsMap' => Yii::$app->session->remove('breakEven.customerInputsMap')
        ]);
    }

    /**
     * @param int $activeTab
     * @return string
     * @throws \Exception
     */
    public function actionIncome($activeTab = IncomeSearch::TAB_BY_ACTIVITY)
    {
        $searchModel = new IncomeSearch();
        $data = $searchModel->search($activeTab, Yii::$app->request->get());
        $this->year = $searchModel->year;
        $currentMonthNumber = date('n');
        $currentQuarter = (int)ceil(date('m') / 3);

        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 111, true);

        return $this->render('income', [
            'activeTab' => $activeTab,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'searchModel' => $searchModel,
            'data' => $data,
        ]);
    }

    /**
     * @param int $activeTab
     * @return string
     * @throws \Exception
     */
    public function actionExpenses($activeTab = ExpensesSearch::TAB_BY_ACTIVITY)
    {
        $searchModel = new ExpensesSearch();
        $data = $searchModel->search($activeTab, Yii::$app->request->get());
        $this->year = $searchModel->year;
        $currentMonthNumber = date('n');
        $currentQuarter = (int)ceil(date('m') / 3);
        $incomeByMonth = (Yii::$app->user->identity->config->report_odds_expense_percent_2)
            ? $searchModel->getIncomeByMonth($activeTab)
            : [];

        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 112, true);

        return $this->render('expenses', [
            'activeTab' => $activeTab,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'searchModel' => $searchModel,
            'data' => $data,
            'dataIncomeByMonth' => $incomeByMonth
        ]);
    }

    /**
     * @param int $activeTab
     * @return string
     */
    public function actionProfitAndLoss($activeTab = ProfitAndLossSearchModel::TAB_ALL_OPERATIONS)
    {
        // temp
        set_time_limit(60);

        ProfitAndLossSearchModel::refreshAnalyticsArticles();

        $searchModel = new ProfitAndLossSearchModel();
        $searchModel->activeTab = $activeTab;
        $searchModel->load(Yii::$app->request->get());
        $this->year = $searchModel->year;
        $data = $searchModel->handleItems();

        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 118, true);

        return $this->render('profit-and-loss', [
            'searchModel' => $searchModel,
            'data' => $data,
            'activeTab' => $activeTab
        ]);
    }

    /**
     * @return string|Response
     * @throws \yii\db\Exception
     */
    public function actionStocks()
    {
        $userConfig = Yii::$app->user->identity->config;
        $searchModel = new StocksSearch();

        // change table view
        if ($showGroups = Yii::$app->request->get('defaultSorting')) {
            $userConfig->updateAttributes([
                'report_abc_show_groups' => ($showGroups == 'show_groups') ? 1 : 0
            ]);
            \Yii::$app->session->remove('modules.reports.finance.stocks.has_groups');

            return $this->redirect(Url::current(['defaultSorting' => null]));
        }

        // show without groups
        if (!$searchModel::$HAS_GROUPS && $userConfig->report_abc_show_groups) {
            $userConfig->updateAttributes([
                'report_abc_show_groups' => 0
            ]);

            return $this->redirect(Url::current(['defaultSorting' => null]));
        }

        $dataProvider = $searchModel->search(Yii::$app->request->get());

        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 122, true);

        return $this->render('stocks', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataTotals' => $searchModel->getTotals()
        ]);
    }

    /**
     * @return string
     */
    public function actionLoans()
    {
        $form = (new CreditSelectFormFactory)->createForm();
        $post = Yii::$app->request->post();

        if ($form->load($post)) {
            if ($form->updateModels()) {
                Yii::$app->session->setFlash('success', 'Удалено кредитов: ' . count($form->credits));
            }

            return $this->refresh();
        }

        /** @var CreditRepository $repository */
        $repository = (new RepositoryFactory)->createRepository(CreditRepository::class);
        $repository->dateFrom = StatisticPeriod::getSessionPeriod()['from'];
        $repository->dateTo = StatisticPeriod::getSessionPeriod()['to'];
        $repository->load(Yii::$app->request->get());

        if (!$repository->validate()) {
            $repository = (new RepositoryFactory)->createRepository(CreditRepository::class);
        }

        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 117, true);

        return $this->render('credits', compact('repository'));
    }

    /**
     * @param $type
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionDebtor($type)
    {
        if (!in_array($type, [Documents::IO_TYPE_IN, Documents::IO_TYPE_OUT]))
            throw new NotFoundHttpException('The requested page does not exist #1.');

        $reportType = Yii::$app->request->get('report', DebtReportSearch2::REPORT_TYPE_MONEY);
        $searchBy = Yii::$app->request->get('search_by', DebtReportSearch2::SEARCH_BY_DOCUMENTS);

        $searchModel = new DebtReportSearch2();
        $searchModel->search_by = $searchBy;
        $searchModel->report_type = $reportType;

        $searchModel2 = new DebtReportSearchAsBalance();
        $searchModel2->search_by = $searchBy;
        $searchModel2->report_type = $reportType;

        switch ($type) {
            case Documents::IO_TYPE_IN:
                \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 116, true);
                break;
            case Documents::IO_TYPE_OUT:
                \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 115, true);
                break;
        }

        return $this->render('debtor', [
            'type' => $type,
            'reportType' => $reportType,
            'searchBy' => $searchBy,
            'searchModel' => $searchModel,
            'searchModel2' => $searchModel2
        ]);
    }

    public function actionDebtorChangeView()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($reportDebtorByPeriodType = Yii::$app->request->get('report_debtor_period_type')) {
            $userConfig = Yii::$app->user->identity->config;
            $userConfig->updateAttributes([
                'report_debtor_period_type' =>
                    ($reportDebtorByPeriodType == DebtReportSearch2::SEARCH_BY_DEBT_PERIODS_BY_DOCUMENTS) ?
                        DebtReportSearch2::SEARCH_BY_DEBT_PERIODS_BY_DOCUMENTS :
                        DebtReportSearch2::SEARCH_BY_DEBT_PERIODS_BY_INVOICES
            ]);

            return ['updated' => true];
        }

        return ['updated' => false];
    }

    /**
     * @param int $activeTab
     * @return mixed|string|Response
     * @throws \Throwable
     */
    public function actionCreatePlanItem($activeTab = PaymentCalendarSearch::TAB_BY_PURSE)
    {
        /** @var Company $company */
        $company = Yii::$app->user->identity->company;
        $checkingAccountant = $company->mainCheckingAccountant;
        $cashbox = $company->getCashboxes()->andWhere(['is_main' => true])->one();
        $emoney = $company->getEmoneys()->andWhere(['is_main' => true])->one();

        $model = new PlanCashFlows();
        $model->company_id = $company->id;
        $model->flow_type = CashFlowsBase::FLOW_TYPE_INCOME;
        $model->checking_accountant_id = ($checkingAccountant) ? $checkingAccountant->id : null;
        $model->cashbox_id = ($cashbox) ? $cashbox->id : null;
        $model->emoney_id = ($emoney) ? $emoney->id : null;

        $subTab = Yii::$app->request->get('subTab');
        $year = Yii::$app->request->get('year', date('Y'));

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->_save()) {
            Yii::$app->session->setFlash('success', 'Плановая операция добавлена.');

            return $this->redirect(['payment-calendar', 'activeTab' => $activeTab, 'subTab' => $subTab, 'PaymentCalendarSearch' => [
                'year' => $year,
            ]]);
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_partial/plan_item_form', [
                'model' => $model,
                'company' => $company,
                'activeTab' => $activeTab,
                'subTab' => Yii::$app->request->get('subTab'),
                'year' => $year,
            ]);
        }

        return $this->render('_partial/plan_item_form', [
            'model' => $model,
            'company' => $company,
            'activeTab' => $activeTab,
            'subTab' => Yii::$app->request->get('subTab'),
            'year' => $year,
        ]);
    }

    /**
     * @param $id
     * @param int $activeTab
     * @return mixed|string|Response
     * @throws \Throwable
     */
    public function actionUpdatePlanItem($id, $activeTab = PaymentCalendarSearch::TAB_BY_PURSE)
    {
        $company = Yii::$app->user->identity->company;
        $model = PlanCashFlows::findOne($id);
        if ($model == null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $year = Yii::$app->request->get('year');
        $year = $year ? $year : date('Y');
        $subTab = Yii::$app->request->get('subTab');

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->_update()) {
            Yii::$app->session->setFlash('success', 'Плановая операция обновлена.');

            return $this->redirect(['payment-calendar',
                'activeTab' => $activeTab,
                'PaymentCalendarSearch' => [
                    'year' => $year,
                ],
                'subTab' => $subTab
            ]);
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_partial/plan_item_form', [
                'model' => $model,
                'company' => $company,
                'activeTab' => $activeTab,
                'year' => $year,
                'subTab' => $subTab
            ]);
        }

        return $this->render('_partial/plan_item_form', [
            'model' => $model,
            'company' => $company,
            'activeTab' => $activeTab,
            'year' => $year,
            'subTab' => $subTab
        ]);
    }

    /**
     * @param $id
     * @param int $activeTab
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeletePlanItem($id, $activeTab = PaymentCalendarSearch::TAB_BY_PURSE)
    {
        $model = PlanCashFlows::findOne($id);
        $year = Yii::$app->request->get('year');
        $year = $year ? $year : date('Y');
        $subTab = Yii::$app->request->get('subTab');

        if ($model == null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        if ($model->delete()) {
            Yii::$app->session->setFlash('success', 'Плановая операция удалена.');
        } else {
            Yii::$app->session->setFlash('error', 'Произошла ошибка при удалении плановой операции.');
        }

        return $this->redirect(['payment-calendar',
            'activeTab' => $activeTab,
            'PaymentCalendarSearch' => [
                'year' => $year,
            ],
            'subTab' => $subTab
        ]);
    }

    /**
     * @param $id
     * @param $tb
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteFlowItem($id, $tb)
    {
        $model = $this->findFlowItem($tb, $id);
        // prevent pjax call
        if (!Yii::$app->request->isPjax) {
            if ($model->delete()) {
                Yii::$app->session->setFlash('success', 'Операция удалена.');
            } else {
                Yii::$app->session->setFlash('error', 'Произошла ошибка при удалении операции.');
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     */
    public function actionManyDelete()
    {
        $idArray = Yii::$app->request->post('flowId', []);
        foreach ($idArray as $id) {
            $model = PlanCashFlows::findOne(['id' => $id, 'company_id' => Yii::$app->user->identity->company_id]);
            if ($model == null) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
            $model->delete();
        }
        Yii::$app->session->setFlash('success', 'Плановые операции удалены.');

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     */
    public function actionManyDeleteFlowItem()
    {
        $data = Yii::$app->request->post('flowId', []);
        foreach ($data as $tb => $idArray) {
            foreach ($idArray as $id) {
                $model = $this->findFlowItem($tb, $id);
                $model->delete();
            }
        }
        Yii::$app->session->setFlash('success', 'Операции удалены.');

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionManyFlowItem()
    {
        $data = Yii::$app->request->post('flowId', []);
        $incomeItemID = Yii::$app->request->post('incomeItemIdManyItem');
        $expenseItemID = Yii::$app->request->post('expenditureItemIdManyItem');
        foreach ($data as $tb => $idArray) {
            foreach ($idArray as $id) {
                $model = $this->findFlowItem($tb, $id);
                $attribute = $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? 'income_item_id' : 'expenditure_item_id';
                $value = $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? $incomeItemID : $expenseItemID;
                if ($model instanceof CashBankFlows) {
                    $model->setScenario('update');
                }
                $model->$attribute = $value;
                $model->save(true, [$attribute]);
            }
        }
        Yii::$app->session->setFlash('success', 'Статьи по операциям изменены.');

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param int $activeTab
     * @return string
     * @throws \Exception
     */
    public function actionPlanFact($activeTab = PlanFactSearch::TAB_BY_ACTIVITY)
    {
        $userConfig = Yii::$app->user->identity->config;
        $searchModel = new PlanFactSearch();
        $activeTab = (string)$activeTab;

        // change table view
        if ($showColumns = Yii::$app->request->get('defaultSorting')) {
            $userConfig->updateAttributes([
                'report_plan_fact_expanded' => ($showColumns == 'expanded') ? 1 : 0
            ]);

            return $this->redirect(Url::current(['defaultSorting' => null]));
        }

        $dataFull = $searchModel->search($activeTab, Yii::$app->request->get());
        $planDataFull = $searchModel->searchPlan($activeTab, Yii::$app->request->get());
        $searchModel::populateArticles($activeTab, $dataFull['data'], $planDataFull['data']);

        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 110, true);

        return $this->render('plan-fact', [
            'activeTab' => $activeTab,
            'searchModel' => $searchModel,
            // fact
            'data' => &$dataFull['data'],
            'growingData' => &$dataFull['growingData'],
            'totalData' => &$dataFull['totalData'],
            // plan
            'plan' => &$planDataFull['data'],
            'growingPlan' => &$planDataFull['growingData'],
            'totalPlan' => &$planDataFull['totalData'],
        ]);
    }

    /**
     * @param $activeTab
     * @param $year
     * @return Response
     * @throws \Throwable
     */
    public function actionUpdateAutoPlan($activeTab, $year)
    {
        $data = Yii::$app->request->post('CashFlowsAutoPlan', []);
        $result = Yii::$app->db->transaction(function (Connection $db) use ($data) {
            foreach ($data as $item) {
                $isDeleted = (int)$item['is_deleted'];
                if ($isDeleted) {
                    continue;
                }
                $model = new CashFlowsAutoPlan();
                $model->company_id = Yii::$app->user->identity->company_id;
                $model->flow_type = $item['flow_type'];
                $model->payment_type = $item['payment_type'];
                $model->item = $item['item'];
                $model->contractor_id = $item['contractor_id'];
                $model->amount = $item['amount'];
                $model->plan_date = $item['plan_date'];
                $model->end_plan_date = $item['end_plan_date'];
                $model->is_deleted = $isDeleted;
                if (in_array($model->contractor_id, [
                    CashContractorType::BANK_TEXT,
                    CashContractorType::ORDER_TEXT,
                    CashContractorType::EMONEY_TEXT
                ])) {
                    $model->item = InvoiceExpenditureItem::ITEM_OTHER;
                }
                if (!$model->save()) {
                    $db->transaction->rollBack();
                    return false;
                }
                if (!$model->is_deleted) {
                    $planCashFlows = new PlanCashFlows(['isAutoPlan' => true]);
                    $planCashFlows->company_id = Yii::$app->user->identity->company_id;
                    $planCashFlows->contractor_id = $model->contractor_id;
                    $planCashFlows->flow_type = $model->flow_type;
                    $planCashFlows->payment_type = $model->payment_type;
                    $planCashFlows->amount = $item['amount'];
                    $planCashFlows->expenditure_item_id = $model->item;
                    $planCashFlows->is_repeated = true;
                    $planCashFlows->date = $model->plan_date;
                    $planCashFlows->planEndDate = $model->end_plan_date;
                    $planCashFlows->period = PlanCashFlows::PERIOD_MONTH;
                    $planCashFlows->description = 'Создано АвтоПланированием';
                    if (!$planCashFlows->_save()) {
                        $db->transaction->rollBack();
                        return false;
                    }
                }
            }

            return true;
        });
        if ($result) {
            Yii::$app->session->setFlash('success', 'Изменения по автопланированию сохранены.');
        } else {
            Yii::$app->session->setFlash('error', 'Произошла ошибка при изменении автопланирования.');
        }

        return $this->redirect(['payment-calendar', 'activeTab' => $activeTab, ['PaymentCalendarSearch' => ['year' => $year]]]);
    }

    /**
     * @param $type
     * @param $year
     * @return string
     * @throws \Exception
     */
    public function actionGetXls($type, $year)
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        Yii::$app->response->format = Response::FORMAT_RAW;

        if (in_array($type, [OddsSearch::TAB_ODDS, OddsSearch::TAB_ODDS_BY_PURSE, OddsSearch::TAB_ODDS_BY_PURSE_DETAILED])) {
            $searchModel = new OddsSearch();
            $searchModel->year = $year;
            $params = ($projectId = Yii::$app->request->get('project_id'))
                ? ['filters' => ['project_id' => (int)$projectId]]
                : [];
            $searchModel->generateXls($type, $params);
            exit;
        } elseif (in_array($type, [PaymentCalendarSearch::TAB_BY_PURSE, PaymentCalendarSearch::TAB_BY_ACTIVITY])) {
            $searchModel = new PaymentCalendarSearch();
            if ('register' === Yii::$app->request->get('sub_tab')) {
                $searchModel->exportPlanFlowsRegister = true;
            }
        } elseif (in_array($type, [PlanFactSearch::TAB_BY_PURSE, PlanFactSearch::TAB_BY_ACTIVITY])) {
            $searchModel = new PlanFactSearch();
        } elseif (in_array($type, [ExpensesSearch::TAB_BY_PURSE, ExpensesSearch::TAB_BY_ACTIVITY])) {
            $searchModel = new ExpensesSearch();
        } elseif (in_array($type, [IncomeSearch::TAB_BY_PURSE, IncomeSearch::TAB_BY_ACTIVITY])) {
            $searchModel = new IncomeSearch();
        } elseif (in_array($type, [BalanceSearch::TAB_BALANCE])) {
            $searchModel = new BalanceSearch(['company' => Yii::$app->user->identity->company]);
        } elseif (in_array($type, [ProfitAndLossSearchModel::TAB_ALL_OPERATIONS, ProfitAndLossSearchModel::TAB_ACCOUNTING_OPERATIONS_ONLY])) {
            $searchModel = new ProfitAndLossSearchModel();
        } elseif (in_array($type, [DebtReportSearch2::REPORT_ID])) {
            $searchBy = Yii::$app->request->get('search_by', 1);
            $reportType = Yii::$app->request->get('report_type', 1);
            if ($searchBy == DebtReportSearch2::SEARCH_BY_DEBT_PERIODS || $reportType == DebtReportSearch2::REPORT_TYPE_NET_TOTAL)
                $searchModel = new DebtReportSearch2();
            else
                $searchModel = new DebtReportSearchAsBalance();
        } elseif (in_array($type, [StocksSearch::REPORT_ID])) {
            $searchModel = new StocksSearch();
        } elseif (in_array($type, [OperationalEfficiencySearchModel::REPORT_ID])) {
            $searchModel = new OperationalEfficiencySearchModel();
        } else {
            throw new InvalidParamException('Invalid type param.');
        }

        $searchModel->year = $year;
        $searchModel->generateXls($type);

        exit;
    }

    public function actionOperationalEfficiency(): string {
        $user = Yii::$app->user->identity;
        $searchModel = new OperationalEfficiencySearchModel();
        $searchModel->load(Yii::$app->request->get());
        $this->year = $searchModel->year;
        $activeTab = OperationalEfficiencySearchModel::REPORT_ID;
        $searchModel->handleItems();

        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 120, true);

        return $this->render('operational-efficiency', [
            'user' => $user,
            'searchModel' => $searchModel,
            'activeTab' => $activeTab,
        ]);
    }

    /**
     * @param $flowType
     * @param $paymentType
     * @param $item
     * @param $contractorID
     * @return CashFlowsAutoPlan|ActiveRecord|null
     */
    private function findCashFlowsAutoPlan($flowType, $paymentType, $item, $contractorID)
    {
        return CashFlowsAutoPlan::find()
            ->andWhere(['and',
                ['company_id' => Yii::$app->user->identity->company_id],
                ['flow_type' => $flowType],
                ['payment_type' => $paymentType],
                ['item' => $item],
                ['contractor_id' => $contractorID],
            ])->one();
    }

    /**
     * @param $tableName
     * @param $id
     * @return CashBankFlows|CashOrderFlows|CashEmoneyFlows
     * @throws NotFoundHttpException
     */
    private function findFlowItem($tableName, $id)
    {
        switch ($tableName) {
            case CashBankFlows::tableName():
                $className = CashBankFlows::className();
                break;
            case CashOrderFlows::tableName():
                $className = CashOrderFlows::className();
                break;
            case CashEmoneyFlows::tableName():
                $className = CashEmoneyFlows::className();
                break;
            case PlanCashFlows::tableName():
                $className = PlanCashFlows::className();
                break;
            default:
                throw new NotFoundHttpException('The requested flow does not exist.');
        }
        $model = $className::find()
            ->andWhere(['id' => $id])
            ->andWhere(['company_id' => Yii::$app->user->identity->company_id])
            ->one();
        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $model;
    }

    private function updateOddsActiveTab($employee)
    {
        if ($activeTab = Yii::$app->request->get('activeTab')) {
            if (in_array($activeTab, [OddsSearch::TAB_ODDS, OddsSearch::TAB_ODDS_BY_PURSE, OddsSearch::TAB_ODDS_BY_PURSE_DETAILED]))
                if ($employee->config->report_odds_active_tab != $activeTab)
                    $employee->config->updateAttributes(['report_odds_active_tab' => (int)$activeTab]);
        }
    }

    private function getModelBalanceInitial(): BalanceInitial
    {
        $company = Yii::$app->user->identity->company;
        return
            BalanceInitial::findOne(['company_id' => $company->id])
                ?: new BalanceInitial(['company_id' => $company->id, 'date' => date('Y-m-d', $company->created_at)]);
    }
}