<?php

namespace frontend\modules\analytics\controllers;

use Yii;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\rbac\permissions;
use frontend\components\FrontendController;
use common\components\filters\AccessControl;
use frontend\components\BusinessAnalyticsAccess;
use frontend\modules\analytics\models\AnalyticsMultiCompanyForm;

/**
 * Class MultiCompanyController
 * @package frontend\modules\analytics\controllers
 */
class MultiCompanyController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['change-mode'],
                        'allow' => true,
                        'roles' => [
                            permissions\BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function($rule, $action) {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_FINANCE);
                        },
                    ]
                ],
            ],
        ]);
    }

    public function actionChangeMode()
    {
        $currentEmployee = Yii::$app->user->identity;
        $currentCompany = Yii::$app->user->identity->company;

        $form = new AnalyticsMultiCompanyForm([
            'employee_id' => $currentEmployee->id,
            'primary_company_id' => $currentCompany->id,
        ]);

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $form->load(Yii::$app->request->post());
            return ActiveForm::validate($form);
        }

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            if (!$form->isModeEnabled) {
                if ($form->saveCompanies()) {
                    $form->setModeOn();
                    Yii::$app->session->setFlash('success', 'Режим консолидированной отчетности включен');
                } else {
                    Yii::$app->session->setFlash('error', 'Не удалось влючить режим консолидированной отчетности');
                }
            } else {
                $form->setModeOff();
                Yii::$app->session->setFlash('success', 'Режим консолидированной отчетности выключен');
            }
        }

        return $this->redirect(Yii::$app->request->referrer ?: ['/site/index']);
    }
}