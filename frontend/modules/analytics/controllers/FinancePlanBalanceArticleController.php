<?php

namespace frontend\modules\analytics\controllers;

use common\components\date\DateHelper;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use frontend\modules\analytics\models\financePlan\FinancePlan;
use frontend\modules\analytics\models\financePlan\FinancePlanBalanceArticle;
use frontend\modules\analytics\models\financePlan\FinancePlanCredit;
use frontend\modules\analytics\models\financePlan\FinancePlanGroup;
use frontend\modules\analytics\models\financePlan\odds\FinancePlanOddsExpenditureItem;
use frontend\modules\analytics\models\financePlan\odds\FinancePlanOddsIncomeItem;
use frontend\modules\analytics\models\financePlan\FinancePlanBalanceArticle as BalanceArticle;
use frontend\modules\reference\models\BalanceArticlesCategories;
use Yii;
use yii\base\Exception;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use frontend\components\FrontendController;
use common\components\helpers\ArrayHelper;
use frontend\components\BusinessAnalyticsAccess;
use common\components\filters\AccessControl;

/**
 * Class FinancePlanBalanceArticleController
 */
class FinancePlanBalanceArticleController extends FrontendController
{
    const VIEW = '@frontend/modules/analytics/views/finance-plan/';

    public $enableCsrfValidation = false;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'create',
                            'update',
                            'delete',
                            'test'
                        ],
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function($rule, $action) {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_FINANCE);
                        },
                    ],
                ],
            ],
        ]);
    }

    public function actionDelete($group, $plan, $article)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $financeGroup = $this->findModelGroup($group);
        $financePlan = $financeGroup->getActivePlan($plan);
        if (!$financePlan)
            throw new Exception('Finance plan not found!');

        $article = FinancePlanBalanceArticle::findOne([
            'id' => (int)$article,
            'model_id' => $financePlan->id
        ]);

        if ($article && $article->delete()) {
            return ['success' => 1, 'message' => 'Удалено'];
        }

        return ['success' => 0, 'message' => 'Не удалось удалить ' . $article->typeAsText];
    }

    public function actionUpdate($group, $plan, $article = null)
    {
        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $financeGroup = $this->findModelGroup($group);
        $financePlan = $financeGroup->getActivePlan($plan);

        if (!$financePlan)
            throw new Exception('Finance plan not found!');

        $article = BalanceArticle::findOne([
            'model_id' => $financePlan->id,
            'id' => $article,
        ]);

        if (!$article)
            throw new Exception('Article not found!');

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($article);
        }

        if ($article->load(Yii::$app->request->post()) && $article->save()) {

            $financeGroup->getSessionData();

            if ($this->_refreshBalanceArticleRows($financePlan, $article)) {

                $financeGroup->setSessionData();

                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ['success' => 1, 'message' => 'Сохранено'];
                } else {
                    Yii::$app->session->setFlash('success', 'Сохранено');
                    return $this->redirect(Yii::$app->request->referrer);
                }
            }

            // rollback
            $article->delete();

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => 0, 'message' => 'Не удалось сохранить ' . $article->typeAsText];
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось сохранить ' . $article->typeAsText);
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        return $this->renderAjax(self::VIEW . '/partial/balance_article/_form', [
            'company' => $company,
            'employee' => $employee,
            'model' => $article,
        ]);
    }

    public function actionCreate($group, $plan, $item = null)
    {
        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $financeGroup = $this->findModelGroup($group);
        $financePlan = $financeGroup->getActivePlan($plan);
        if ($item == InvoiceExpenditureItem::ITEM_BALANCE_ARTICLE_INTANGIBLE) {
            $articleType = BalanceArticle::TYPE_INTANGIBLE_ASSETS;
            $articleCategory = BalanceArticlesCategories::SOFTWARE;
        } else {
            $articleType = BalanceArticle::TYPE_FIXED_ASSERTS;
            $articleCategory = BalanceArticlesCategories::REAL_ESTATE;
        }

        if (!$financePlan)
            throw new Exception('Finance plan not found!');

        $article = new BalanceArticle([
            'model_id' => $financePlan->id,
            'type' => $articleType,
            'category' => $articleCategory,
            'status' => BalanceArticle::STATUS_ON_BALANCE,
        ]);

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($article);
        }

        if ($article->load(Yii::$app->request->post()) && $article->save()) {

            $financeGroup->getSessionData();

            if ($this->_refreshBalanceArticleRows($financePlan, $article)) {

                $financeGroup->setSessionData();

                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ['success' => 1, 'message' => 'Сохранено'];
                } else {
                    Yii::$app->session->setFlash('success', 'Сохранено');
                    return $this->redirect(Yii::$app->request->referrer);
                }
            }

            // rollback
            $article->delete();

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => 0, 'message' => 'Не удалось сохранить ' . $article->typeAsText];
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось сохранить ' . $article->typeAsText);
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        return $this->renderAjax(self::VIEW . '/partial/balance_article/_form', [
            'company' => $company,
            'employee' => $employee,
            'model' => $article,
        ]);
    }

    private function _refreshBalanceArticleRows(FinancePlan $plan, BalanceArticle $article)
    {
        $expenditureItemId = ($article->type == BalanceArticle::TYPE_INTANGIBLE_ASSETS)
            ? InvoiceExpenditureItem::ITEM_BALANCE_ARTICLE_INTANGIBLE
            : InvoiceExpenditureItem::ITEM_BALANCE_ARTICLE_FIXED;

        $expense =
            FinancePlanOddsExpenditureItem::findOne([
                'model_id' => $plan->id,
                'balance_article_id' => $article->id,
                'expenditure_item_id' => $expenditureItemId,
                'type' => FinancePlan::ODDS_ROW_TYPE_INVESTMENTS,
            ])
                ?: new FinancePlanOddsExpenditureItem([
                'model_id' => $plan->id,
                'balance_article_id' => $article->id,
                'expenditure_item_id' => $expenditureItemId,
                'type' => FinancePlan::ODDS_ROW_TYPE_INVESTMENTS,
                'can_edit' => 0,
            ]);

        $rowExpense = [];
        foreach ($plan->modelGroup->monthes as $ym) {
            if ($ym == date('Ym', strtotime($article->purchased_at))) {
                $rowExpense[$ym] = $article->amount;
            } else {
                $rowExpense[$ym] = 0;
            }
        }

        $expense->data = json_encode($rowExpense);

        $isNewRecords = $expense->isNewRecord;

        if ($expense->save()) {
            if ($isNewRecords) {
                $plan->setNewSessionRelatedModel($expense, FinancePlanGroup::SESS_ODDS_EXPENDITURE_ITEMS);
            } else {
                foreach ($plan->oddsExpenditureItems as $o) {
                    if ($o->balance_article_id === $article->id) {
                        $o->data = $expense->data;
                    }
                }
            }

            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @return FinancePlanGroup
     * @throws NotFoundHttpException
     */
    protected function findModelGroup($id)
    {
        $employee = Yii::$app->user->identity;
        /** @var FinancePlanGroup $model */
        $model = FinancePlanGroup::find()
            ->with([
                'expenditureItems',
                'financePlans',
                'financePlans.incomeItems',
                'financePlans.expenditureItems',
                'financePlans.primeCostItems',
                'financePlans.oddsIncomeItems',
                'financePlans.oddsExpenditureItems',
            ])
            ->where(['id' => $id])
            ->andWhere(['company_id' => $employee->company->id])
            ->one();

        $model->mainIndustryId = $model->mainFinancePlan->industry_id;
        $model->mainIndustryName = $model->mainFinancePlan->name;

        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Запрошенная страница не существует.');
    }
}