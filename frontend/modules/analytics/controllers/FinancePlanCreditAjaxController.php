<?php

namespace frontend\modules\analytics\controllers;

use common\components\date\DateHelper;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\analytics\models\credits\Date;
use frontend\modules\analytics\models\credits\DebtCalculatorFactory;
use frontend\modules\analytics\models\financePlan\chart\FinancePlanChart;
use frontend\modules\analytics\models\financePlan\FinancePlan;
use frontend\modules\analytics\models\financePlan\FinancePlanCredit;
use frontend\modules\analytics\models\financePlan\FinancePlanGroup;
use frontend\modules\analytics\models\financePlan\odds\FinancePlanOddsExpenditureItem;
use frontend\modules\analytics\models\financePlan\odds\FinancePlanOddsIncomeItem;
use Yii;
use yii\base\Exception;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use frontend\components\FrontendController;
use common\components\helpers\ArrayHelper;
use frontend\components\BusinessAnalyticsAccess;
use common\components\filters\AccessControl;

/**
 * Class FinancePlanCreditAjaxController
 */
class FinancePlanCreditAjaxController extends FrontendController
{
    const VIEW = '@frontend/modules/analytics/views/finance-plan/';

    public $enableCsrfValidation = false;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'create',
                            'update',
                            'delete',
                            'test'
                        ],
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function($rule, $action) {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_FINANCE);
                        },
                    ],
                ],
            ],
        ]);
    }

    public function actionDelete($group, $plan, $credit)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $financeGroup = $this->findModelGroup($group);
        $financePlan = $financeGroup->getActivePlan($plan);
        if (!$financePlan)
            throw new Exception('Finance plan not found!');

        $credit = FinancePlanCredit::findOne([
            'id' => (int)$credit,
            'model_id' => $financePlan->id
        ]);

        if ($credit && $credit->delete()) {
            return ['success' => 1, 'message' => 'Удалено'];
        }

        return ['success' => 0, 'message' => 'Не удалось удалить кредит'];
    }

    public function actionUpdate($group, $plan, $credit = null)
    {
        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $financeGroup = $this->findModelGroup($group);
        $financePlan = $financeGroup->getActivePlan($plan);

        if (!$financePlan)
            throw new Exception('Finance plan not found!');

        $credit = FinancePlanCredit::findOne([
            'id' => (int)$credit,
            'model_id' => $financePlan->id
        ]);

        if (!$credit)
            throw new Exception('Credit not found!');

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($credit);
        }

        if ($credit->load(Yii::$app->request->post()) && $credit->save()) {

            $financeGroup->getSessionData();

            if ($this->_refreshCreditRows($financePlan, $credit)) {

                $financeGroup->setSessionData();

                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ['success' => 1, 'message' => 'Сохранено'];
                } else {
                    Yii::$app->session->setFlash('success', 'Сохранено');
                    return $this->redirect(Yii::$app->request->referrer);
                }
            }

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => 0, 'message' => 'Не удалось сохранить кредит'];
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось сохранить кредит');
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        return $this->renderAjax(self::VIEW . '/partial/credit/_form_credit', [
            'company' => $company,
            'employee' => $employee,
            'model' => $credit,
        ]);
    }

    public function actionCreate($group, $plan, $item = null)
    {
        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $financeGroup = $this->findModelGroup($group);
        $financePlan = $financeGroup->getActivePlan($plan);

        if (!$financePlan)
            throw new Exception('Finance plan not found!');

        $credit = new FinancePlanCredit([
            //'scenario' => $scenario,
            'model_id' => $financePlan->id,
            'credit_status' => FinancePlanCredit::STATUS_ACTIVE,
            'payment_type' => FinancePlanCredit::PAYMENT_TYPE_AT_END,
            'payment_first' => FinancePlanCredit::PAYMENT_FIRST_DEFAULT,
            'payment_day' => FinancePlanCredit::PAYMENT_DAY_LAST,
            'payment_mode' => FinancePlanCredit::PAYMENT_MODE_DEFAULT,
            'credit_year_length' => 0,
            'credit_commission_rate' => FinancePlanCredit::EMPTY_RATE,
            'credit_expiration_rate' => FinancePlanCredit::EMPTY_RATE,
            'credit_tranche_depth' => 0,
            'agreement_date' => date(DateHelper::FORMAT_USER_DATE),
            'credit_type' => ($item == InvoiceIncomeItem::ITEM_LOAN)
                ? FinancePlanCredit::TYPE_LOAN
                : FinancePlanCredit::TYPE_CREDIT,
        ]);

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($credit);
        }

        if ($credit->load(Yii::$app->request->post()) && $credit->save()) {

            $financeGroup->getSessionData();

            if ($this->_refreshCreditRows($financePlan, $credit)) {

                $financeGroup->setSessionData();

                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ['success' => 1, 'message' => 'Сохранено'];
                } else {
                    Yii::$app->session->setFlash('success', 'Сохранено');
                    return $this->redirect(Yii::$app->request->referrer);
                }
            }

            $credit->delete();

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => 0, 'message' => 'Не удалось сохранить кредит'];
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось сохранить кредит');
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        return $this->renderAjax(self::VIEW . '/partial/credit/_form_credit', [
            'company' => $company,
            'employee' => $employee,
            'model' => $credit,
        ]);
    }

    private function _refreshCreditRows(FinancePlan $plan, FinancePlanCredit $credit)
    {
        if ($periods = self::_getCreditShedulePeriods($credit)) {

            $rowIncome = [];
            $rowExpense = [];
            $rowExpenseInterest = [];
            foreach ($plan->modelGroup->monthes as $ym) {
                $rowIncome[$ym] = 100 * ($periods[$ym]['income'] ?? 0);
                $rowExpense[$ym] = 100 * ($periods[$ym]['repaid'] ?? 0);
                $rowExpenseInterest[$ym] = 100 * ($periods[$ym]['interest'] ?? 0);
            }

            if ($credit->credit_type == FinancePlanCredit::TYPE_LOAN) {
                $incomeItemId = InvoiceIncomeItem::ITEM_LOAN;
                $expenditureItemId = InvoiceExpenditureItem::ITEM_LOAN_REPAYMENT;
                $interestExpenditureItemId = InvoiceExpenditureItem::ITEM_PAYMENT_PERCENT;
            } else {
                $incomeItemId = InvoiceIncomeItem::ITEM_CREDIT;
                $expenditureItemId = InvoiceExpenditureItem::ITEM_REPAYMENT_CREDIT;
                $interestExpenditureItemId = InvoiceExpenditureItem::ITEM_PAYMENT_PERCENT;
            }

            $income =
                FinancePlanOddsIncomeItem::findOne([
                    'model_id' => $plan->id,
                    'credit_id' => $credit->id,
                    'type' => FinancePlan::ODDS_ROW_TYPE_FINANCES,
                ])
                ?: new FinancePlanOddsIncomeItem([
                    'model_id' => $plan->id,
                    'credit_id' => $credit->id,
                    'income_item_id' => $incomeItemId,
                    'type' => FinancePlan::ODDS_ROW_TYPE_FINANCES,
                    'can_edit' => 0,
                ]);

            $expense =
                FinancePlanOddsExpenditureItem::findOne([
                    'model_id' => $plan->id,
                    'credit_id' => $credit->id,
                    'expenditure_item_id' => $expenditureItemId,
                    'type' => FinancePlan::ODDS_ROW_TYPE_FINANCES,
                ])
                ?: new FinancePlanOddsExpenditureItem([
                    'model_id' => $plan->id,
                    'credit_id' => $credit->id,
                    'expenditure_item_id' => $expenditureItemId,
                    'type' => FinancePlan::ODDS_ROW_TYPE_FINANCES,
                    'can_edit' => 0,
                ]);

            $expenseInterest =
                FinancePlanOddsExpenditureItem::findOne([
                    'model_id' => $plan->id,
                    'credit_id' => $credit->id,
                    'expenditure_item_id' => $interestExpenditureItemId,
                    'type' => FinancePlan::ODDS_ROW_TYPE_FINANCES,
                ])
                ?: new FinancePlanOddsExpenditureItem([
                    'model_id' => $plan->id,
                    'credit_id' => $credit->id,
                    'expenditure_item_id' => $interestExpenditureItemId,
                    'type' => FinancePlan::ODDS_ROW_TYPE_FINANCES,
                    'can_edit' => 0,
                ]);

            $income->data = json_encode($rowIncome);
            $expense->data = json_encode($rowExpense);
            $expenseInterest->data = json_encode($rowExpenseInterest);

            $isNewRecords = $income->isNewRecord || $expense->isNewRecord || $expenseInterest->isNewRecord;

            if ($income->save())
                if ($expense->save())
                    if ($expenseInterest->save()) {
                        if ($isNewRecords) {
                            $plan->setNewSessionRelatedModel($income, FinancePlanGroup::SESS_ODDS_INCOME_ITEMS);
                            $plan->setNewSessionRelatedModel($expense, FinancePlanGroup::SESS_ODDS_EXPENDITURE_ITEMS);
                            $plan->setNewSessionRelatedModel($expenseInterest, FinancePlanGroup::SESS_ODDS_EXPENDITURE_ITEMS);
                        } else {
                            foreach ($plan->oddsIncomeItems as $o) {
                                if ($o->credit_id === $credit->id)
                                    $o->data = $income->data;
                            }
                            foreach ($plan->oddsExpenditureItems as $o) {
                                if ($o->credit_id === $credit->id) {
                                    if ($o->expenditure_item_id === $expenditureItemId)
                                        $o->data = $expense->data;
                                    if ($o->expenditure_item_id === $interestExpenditureItemId)
                                        $o->data = $expenseInterest->data;
                                }
                            }
                        }

                        return true;
                    }
        }

        return false;
    }

    /**
     * @param FinancePlanCredit $planCredit
     * @return array
     */
    private static function _getCreditShedulePeriods(FinancePlanCredit $planCredit)
    {
        try {
            $credit = new Credit();
            $credit->setAttributes($planCredit->getAttributes(), false);
            $calculator = (new DebtCalculatorFactory)->createCalculator($credit);
            $ret = [];
            foreach ($calculator->schedule->periods as $p) {
                $ret[$p->lastDate->format('Ym')] = [
                    'number' => $p->number, // номер платежа
                    'lastDate' => $p->lastDate->format('Y-m-d'), // дата платежа
                    'repaid' => $p->repaid, // платеж по основному долгу
                    'interest' => $p->interest, // платеж по процентам
                    'amount' => $p->amount // остаток
                ];
            }

            $incomeDate = Date::createFromFormat(DateHelper::FORMAT_DATE, $credit->credit_first_date);
            $ret[$incomeDate->format('Ym')]['income'] = $credit->credit_amount;

            return $ret;

        } catch (\Exception $e) {

            return [];
        }
    }

    /**
     * @param $id
     * @return FinancePlanGroup
     * @throws NotFoundHttpException
     */
    protected function findModelGroup($id)
    {
        $employee = Yii::$app->user->identity;
        /** @var FinancePlanGroup $model */
        $model = FinancePlanGroup::find()
            ->with([
                'expenditureItems',
                'financePlans',
                'financePlans.incomeItems',
                'financePlans.expenditureItems',
                'financePlans.primeCostItems',
                'financePlans.oddsIncomeItems',
                'financePlans.oddsExpenditureItems',
            ])
            ->where(['id' => $id])
            ->andWhere(['company_id' => $employee->company->id])
            ->one();

        $model->mainIndustryId = $model->mainFinancePlan->industry_id;
        $model->mainIndustryName = $model->mainFinancePlan->name;

        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Запрошенная страница не существует.');
    }
}