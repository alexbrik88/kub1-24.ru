<?php

namespace frontend\modules\analytics\controllers;

use common\components\filters\AccessControl;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\modules\analytics\models\ExpensesPaymentsSearch;
use frontend\rbac\UserRole;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * TemplateController implements the CRUD actions for Template model.
 */
class ExpensesReportController extends FrontendController
{
    public $layout = 'finance';

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function($rule, $action) {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_FINANCE);
                        },
                    ],
                ],
            ],
        ]);
    }

    public function actionPayments()
    {
        $searchModel = new ExpensesPaymentsSearch([
            'companyId' => Yii::$app->user->identity->company->id,
        ]);
        $dataArray = $searchModel->getPaymentsDataArray(\Yii::$app->request->get());

        \common\models\company\CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 57);

        return $this->render('payments', [
            'searchModel' => $searchModel,
            'dataArray' => $dataArray,
        ]);
    }

    public function actionDocuments()
    {
        return $this->render('documents', []);
    }
}
