<?php

namespace frontend\modules\analytics\controllers;

use common\components\filters\AccessControl;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\components\PageSize;
use frontend\components\StatisticPeriod;
use frontend\modules\analytics\models\AnalysisSearch;
use frontend\rbac\UserRole;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * AnalysisController
 */
class AnalysisController extends \frontend\components\FrontendController
{
    public $layout = 'debt-report';

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function($rule, $action) {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_SALES);
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AnalysisSearch([
            'company' => Yii::$app->user->identity->company,
            'dateRange' => StatisticPeriod::getSessionPeriod(),
        ]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        $dataProvider->pagination->pageSize = PageSize::get();

        \common\models\company\CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 58);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
