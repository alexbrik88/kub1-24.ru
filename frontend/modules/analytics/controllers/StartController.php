<?php

namespace frontend\modules\analytics\controllers;

use common\components\DadataClient;
use common\components\filters\AccessControl;
use common\components\getResponse\GetResponseApi;
use common\models\company\CompanyInfoIndustry;
use common\models\employee\EmployeeInfoRole;
use console\components\sender\Findirector;
use frontend\modules\analytics\models\ReportsStartForm;
use frontend\rbac\UserRole;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * StartController controller for the `reports` module
 */
class StartController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
            'ajax' => [
                'class' => 'yii\filters\AjaxFilter',
                'except' => [
                    'banking-redirect',
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionCode()
    {
        $employee = Yii::$app->user->identity;

        $model = new ReportsStartForm($employee, [
            'scenario' => ReportsStartForm::SCENARIO_CODE,
        ]);

        //no need to confirm email
        if (true || $employee->is_email_confirmed) {
            return $this->renderAjax('account', ['model' => $model]);
        } elseif (Yii::$app->request->getIsGet() && !Yii::$app->session->remove('email_confirm_code_sent')) {
            $employee->sendEmailConfirmCode(Findirector::$from);
        }

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->renderAjax('account', ['model' => $model]);
        }

        return $this->renderAjax('code', ['model' => $model]);
    }

    /**
     * @return mixed
     */
    public function actionAccount()
    {
        $user = Yii::$app->user->identity;

        $model = new ReportsStartForm($user, [
            'scenario' => ReportsStartForm::SCENARIO_ACCOUNT,
        ]);

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            // Update contact in GetResponse service
            $gr = new GetResponseApi();
            $gr->updateGetResponseContact($user, [
                'company_type' => ($industry = CompanyInfoIndustry::findOne($model->info_industry_id)) ? $industry->name : null,
                'product_type' => ArrayHelper::getValue(GetResponseApi::PRODUCT_TYPES, $model->info_industry_id, GetResponseApi::PRODUCT_TYPE_OTHER),
                'reg_user_type' => ArrayHelper::getValue(GetResponseApi::REG_USER_TYPES, $model->info_role_id, GetResponseApi::REG_USER_TYPE_OTHER),
            ]);

            return $this->renderAjax('taxation', ['model' => $model]);
        }

        return $this->renderAjax('account', ['model' => $model]);
    }

    /**
     * @return mixed
     */
    public function actionTaxation()
    {
        $model = new ReportsStartForm(Yii::$app->user->identity, [
            'scenario' => ReportsStartForm::SCENARIO_TAXATION,
        ]);

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($model->getRedirectUrl());
            // return $this->renderAjax('modules', ['model' => $model]);
        }

        return $this->renderAjax('taxation', ['model' => $model]);
    }

    /**
     * @return mixed
     */
    public function actionModules()
    {
        $model = new ReportsStartForm(Yii::$app->user->identity, [
            'scenario' => ReportsStartForm::SCENARIO_MODULES,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($model->getRedirectUrl());
            // return $this->renderAjax('description', ['model' => $model]);
        }

        return $this->renderAjax('modules', ['model' => $model]);
    }

    /**
     * @return mixed
     */
    public function actionDescription()
    {
        return $this->renderAjax('description', ['model' => $model]);
    }

    /**
     * Performs ajax validation
     * @param Model $model
     * @return mixed
     */
    protected function ajaxValidate($model)
    {
        $model->load(Yii::$app->request->post());
        Yii::$app->response->format = Response::FORMAT_JSON;

        return ActiveForm::validate($model);
    }
}
