<?php

namespace frontend\modules\analytics\controllers;

use common\components\filters\AccessControl;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\Employee;
use common\models\ExpenseItemFlowOfFunds;
use common\models\IncomeItemFlowOfFunds;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\modules\reference\models\AddArticleToContractorForm;
use frontend\modules\reference\models\ArticleDropDownForm;
use frontend\modules\reference\models\ArticleForm;
use frontend\modules\reference\models\ArticlesSearch;
use frontend\modules\reference\models\ManyChangeItemsForm;
use frontend\modules\reference\models\SetDefaultContractorItem;
use frontend\modules\reference\models\UpdateArticleForm;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ArticlesController
 * @package frontend\modules\reference\controllers
 */
class ArticlesController extends FrontendController
{
    const TAB_INCOME = 0;
    const TAB_EXPENSE = 1;
    const TAB_BY_ACTIVITY = 2;

    private static $contractorsOnPage = 100;

    /**
     * @var string
     */
    public $layout = 'finance';

    /**
     * @return array
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                            'ajax-list',
                            'is-has-operations-with-another-items',
                            'get-xls',
                        ],
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function($rule, $action) {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_FINANCE);
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'create',
                            'create-from-dropdown',
                            'update',
                            'add-item-to-contractor',
                            'set-default-item',
                            'many-change-items',
                            'toggle-is-visible',
                            'delete',
                            'delete-contractor',
                            'many-delete-contractors',
                            'change-contractor',
                            'add-modal-contractor',
                        ],
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::ADMIN,
                        ],
                        'matchCallback' => function($rule, $action) {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_FINANCE);
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();

        $actions['add-modal-contractor'] = [
            'class' => 'frontend\components\AddModalContractorAction',
        ];

        return $actions;
    }

    public function beforeAction($action)
    {
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        if ($user && $user->is_old_kub_theme) {
            return $this->redirect(['/site/only-new-design', 'redirectUri' => Yii::$app->request->absoluteUrl])->send();
        }

        if (parent::beforeAction($action)) {
            return true;
        }

        return false;
    }

    public function actionIndex($type)
    {
        ini_set('memory_limit', '8192M');

        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $searchModel = new ArticlesSearch($type);
        $data = $searchModel->search(Yii::$app->request->get());
        $setDefaultItemForm = new SetDefaultContractorItem($user->currentEmployeeCompany->company);
        $manyChangeItemsForm = new ManyChangeItemsForm($user);

        \common\models\company\CompanyFirstEvent::checkEvent($user->company, 113, true);

        return $this->render('index', [
            'user' => $user,
            'searchModel' => $searchModel,
            'data' => $data,
            'activeTab' => (int)$type,
            'setDefaultItemForm' => $setDefaultItemForm,
            'manyChangeItemsForm' => $manyChangeItemsForm,
            'contractorsOnPage' => self::$contractorsOnPage
        ]);
    }

    public function actionCreate()
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect(Yii::$app->request->referrer ?? ['/reference/articles/index', 'type' => ArticlesSearch::TYPE_BY_ACTIVITY]);
        }

        $type = Yii::$app->request->get('type');
        $activeTab = Yii::$app->request->get('activeTab');
        $parentId = Yii::$app->request->get('parent');

        if ($type > 1)
            $type = null;

        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $model = new ArticleForm($user);
        $isSaved = $model->load(Yii::$app->request->post()) && $model->save();

        if ($parentId) {
            if ($type == 1) {
                $parentModel = InvoiceExpenditureItem::find()
                    ->andWhere(['id' => $parentId])
                    ->andWhere(['or', ['company_id' => null], ['company_id' => $user->currentEmployeeCompany->company_id]])
                    ->one();
            }
            if ($type == 0) {
                $parentModel = InvoiceIncomeItem::find()
                    ->andWhere(['id' => $parentId])
                    ->andWhere(['or', ['company_id' => null], ['company_id' => $user->currentEmployeeCompany->company_id]])
                    ->one();
            }
        }

        return $this->renderAjax('_partial/_form', [
            'type' => $type,
            'model' => $model,
            'activeTab' => $activeTab,
            'parentModel' => $parentModel ?? null,
            'updateList' => $isSaved,
        ]);
    }

    public function actionCreateFromDropdown($type, $inputSelector)
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect(Yii::$app->request->referrer ?? ['/reference/articles/index', 'type' => ArticlesSearch::TYPE_BY_ACTIVITY]);
        }

        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $model = new ArticleDropDownForm($user, $type);
        if ($model->load(Yii::$app->request->post()) && ($items = $model->save())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $jsExpression = null;
            foreach ($items as $item) {
                $jsExpression .= "$('#{$inputSelector}').append(new Option('{$item['name']}', {$item['id']}, true, true));";
            }

            $jsExpression .= "$('#{$inputSelector}').trigger('change'); $('#article-dropdown-modal-container').modal('hide');";

            return "<script>{$jsExpression}</script>";
        }

        return $this->renderAjax('_partial/_form_dropdown', [
            'model' => $model,
            'type' => $type,
            'inputSelector' => $inputSelector,
        ]);
    }

    public function actionUpdate($id, $type, $activeTab)
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect(Yii::$app->request->referrer ?? ['/reference/articles/index', 'type' => ArticlesSearch::TYPE_BY_ACTIVITY]);
        }

        if ($childrenId = Yii::$app->request->post('articleChildrenId')) {
            $parentId = $id;
            $id = $childrenId;
        }

        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $model = $this->findModel($id, $type);
        $form = new UpdateArticleForm($model, $user);
        $onlyContractor = Yii::$app->request->get('onlyContractor', false);
        $isSaved = $form->load(Yii::$app->request->post()) && $form->save();

        // fix bug: prevent fields blink after pjax reload
        if ($isSaved && isset($parentId)) {
            $model = $this->findModel($parentId, $type);
            $form = new UpdateArticleForm($model, $user);
        }

        return $this->renderAjax('_partial/_update_form', [
            'model' => $form,
            'onlyContractor' => $onlyContractor,
            'activeTab' => (int)$activeTab,
            'updateList' => $isSaved,
        ]);
    }

    public function actionAddItemToContractor($id, $type, $activeTab)
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect(Yii::$app->request->referrer ?? ['/reference/articles/index', 'type' => ArticlesSearch::TYPE_BY_ACTIVITY]);
        }

        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $contractorId = $id;
        $form = new AddArticleToContractorForm($contractorId, $user, (int)$type);
        $isSaved = $form->load(Yii::$app->request->post()) && $form->save();

        return $this->renderAjax('_partial/_add_item_to_contractor_form', [
            'model' => $form,
            'type' => (int)$type,
            'activeTab' => (int)$activeTab,
            'updateList' => $isSaved,
        ]);
    }

    public function actionAjaxList($type) {
        if (!Yii::$app->request->isAjax) {
            $this->redirect(Yii::$app->request->referrer ?? ['/reference/articles/index', 'type' => $type]);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $searchModel = new ArticlesSearch($type);
        $data = $searchModel->search();
        $html = $this->renderPartial('articles-list', [
            'searchModel' => $searchModel,
            'data' => $data,
            'type' => (int)$type,
            'contractorsOnPage' => self::$contractorsOnPage
        ]);

        return [
            'result' => true,
            'html' => $html,
        ];
    }

    public function actionIsHasOperationsWithAnotherItems(int $type): bool
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $contractorsIds = Yii::$app->request->post('contractors', []);
        $itemId = Yii::$app->request->post('itemId');
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        if ($type === ArticlesSearch::TYPE_INCOME) {
            $item = InvoiceIncomeItem::findOne([
                ['id' => $itemId],
                ['company_id' => $user->currentEmployeeCompany->company_id],
            ]);
            $operationField = 'income_item_id';
            $operationFlowType = CashBankFlows::FLOW_TYPE_INCOME;
        } else {
            $item = InvoiceExpenditureItem::findOne([
                ['id' => $itemId],
                ['company_id' => $user->currentEmployeeCompany->company_id],
            ]);
            $operationField = 'expenditure_item_id';
            $operationFlowType = CashBankFlows::FLOW_TYPE_EXPENSE;
        }

        if ($item === null) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }

        return CashBankFlows::find()
                ->andWhere([
                    'AND',
                    ['flow_type' => $operationFlowType],
                    [
                        'OR',
                        ['NOT', [$operationField => $itemId]],
                        [$operationField => null],
                    ],
                    ['company_id' => $user->currentEmployeeCompany->company_id],
                    ['IN', 'contractor_id', $contractorsIds],
                ])->exists()
            || CashOrderFlows::find()
                ->andWhere([
                    'AND',
                    ['flow_type' => $operationFlowType],
                    [
                        'OR',
                        ['NOT', [$operationField => $itemId]],
                        [$operationField => null],
                    ],
                    ['company_id' => $user->currentEmployeeCompany->company_id],
                    ['IN', 'contractor_id', $contractorsIds],
                ])->exists()
            || CashEmoneyFlows::find()
                ->andWhere([
                    'AND',
                    ['flow_type' => $operationFlowType],
                    [
                        'OR',
                        ['NOT', [$operationField => $itemId]],
                        [$operationField => null],
                    ],
                    ['company_id' => $user->currentEmployeeCompany->company_id],
                    ['IN', 'contractor_id', $contractorsIds],
                ])->exists();
    }

    public function actionSetDefaultItem($type)
    {
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $form = new SetDefaultContractorItem($user->currentEmployeeCompany->company);
        if ($form->load(Yii::$app->request->post()) && $form->save()) {
            Yii::$app->session->setFlash('success', 'Правила успешно обновлены');
        } else {
            Yii::$app->session->setFlash('error', 'Произошла ошибка при обновлении правил');
        }

        return $this->redirect(['index', 'type' => $type]);
    }

    public function actionManyChangeItems($type, $activeTab)
    {
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $form = new ManyChangeItemsForm($user);
        if ($form->load(Yii::$app->request->post()) && $form->save()) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $searchModel = new ArticlesSearch($activeTab);
            $data = $searchModel->search();
            $html = $this->renderPartial('articles-list', [
                'searchModel' => $searchModel,
                'data' => $data,
                'type' => (int)$activeTab,
                'contractorsOnPage' => self::$contractorsOnPage
            ]);

            return [
                'result' => true,
                'html' => $html,
            ];
        }

        Yii::$app->session->setFlash('error', 'Произошла ошибка при изменении статей');

        return $this->redirect(['index', 'type' => $type]);
    }

    public function actionToggleIsVisible($id, $type, $filterType)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id, $type);
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        if ((int)$type === ArticlesSearch::TYPE_INCOME) {
            $class = IncomeItemFlowOfFunds::class;
            $attributeName = 'income_item_id';
            $itemGetter = 'incomeItem';
            $children = InvoiceIncomeItem::find()
                ->where(['parent_id' => $model->id, 'company_id' => $user->currentEmployeeCompany->company_id])
                ->all();
        } else {
            $class = ExpenseItemFlowOfFunds::class;
            $attributeName = 'expense_item_id';
            $itemGetter = 'expenseItem';
            $children = InvoiceExpenditureItem::find()
                ->where(['parent_id' => $model->id, 'company_id' => $user->currentEmployeeCompany->company_id])
                ->all();
        }

        /** @var IncomeItemFlowOfFunds|ExpenseItemFlowOfFunds $item */
        $item = $class::findOne([
            $attributeName => $model->id,
            'company_id' => $user->currentEmployeeCompany->company_id,
        ]);
        if ($item === null) {
            return [
                'result' => false,
                'message' => 'Произошла ошибка при изменении видимости статьи.',
            ];
        }

        if ($item->is_visible && !$model->getCanToggleVisibility()) {
            return [
                'result' => false,
                'message' => 'Статью нельзя отключить, так как она используется минимум в одной операции.',
            ];
        }
        $item->is_visible = !$item->is_visible;
        if (!$item->save()) {
            return [
                'result' => false,
                'message' => 'Произошла ошибка при изменении видимости статьи.',
            ];
        } else {
            if ($children) {
                foreach ($children as $child) {
                    if ($fofChild = $child->getFlowOfFundsItemByCompany($user->currentEmployeeCompany->company_id)->one()) {
                        $fofChild->updateAttributes(['is_visible' => $item->is_visible]);
                    }
                }
            }
        }

        $searchModel = new ArticlesSearch($filterType);
        $data = $searchModel->search();
        $html = $this->renderPartial('articles-list', [
            'searchModel' => $searchModel,
            'data' => $data,
            'type' => (int)$filterType,
            'contractorsOnPage' => self::$contractorsOnPage
        ]);

        return [
            'result' => true,
            'html' => $html,
            'title' => ($item->is_visible ? 'Отключить' : 'Включить') . ' статью для использования в работе',
            'titleWithName' => ($item->is_visible ? 'Отключить' : 'Включить') . " статью \"{$item->$itemGetter->name}\" для использования в работе?",
        ];
    }

    /**
     * @param $id
     * @param $type
     * @param $filterType
     * @return array
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id, $type, $filterType)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id, $type);

        if ($model->company_id === null)
            throw new NotFoundHttpException('Нельзя удалять общие статьи.');

        if ((int)$type === ArticlesSearch::TYPE_INCOME) {
            $flowOfFundsItems = $model->incomeItemsFlowOfFunds;
        } else {
            $flowOfFundsItems = $model->expenseItemsFlowOfFunds;
            foreach ($model->profitAndLossCompanyItems as $profitAndLossCompanyItem) {
                $profitAndLossCompanyItem->delete();
            }
        }

        foreach ($flowOfFundsItems as $item) {
            $item->delete();
        }

        if (!$model->delete()) {
            return [
                'result' => false,
                'message' => 'Произошла ошибка при удалении статьи.',
            ];
        }

        $searchModel = new ArticlesSearch($filterType);
        $data = $searchModel->search();
        $html = $this->renderPartial('articles-list', [
            'searchModel' => $searchModel,
            'data' => $data,
            'type' => (int)$filterType,
            'contractorsOnPage' => self::$contractorsOnPage
        ]);

        return [
            'result' => true,
            'html' => $html,
        ];
    }

    public function actionDeleteContractor($id, $type, $filterType)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $contractor = Contractor::findOne(['id' => $id]);
        if ($contractor === null) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }

        if ((int)$type === ArticlesSearch::TYPE_INCOME) {
            $attribute = 'invoice_income_item_id';
        } else {
            $attribute = 'invoice_expenditure_item_id';
        }

        $contractor->$attribute = null;
        if (!$contractor->save(true, [$attribute])) {
            return [
                'result' => false,
                'message' => 'Произошла ошибка при удалении статьи у контрагента.',
            ];
        }

        $searchModel = new ArticlesSearch($filterType);
        $data = $searchModel->search();
        $html = $this->renderPartial('articles-list', [
            'searchModel' => $searchModel,
            'data' => $data,
            'type' => (int)$filterType,
            'contractorsOnPage' => self::$contractorsOnPage
        ]);

        return [
            'result' => true,
            'html' => $html,
        ];
    }

    public function actionManyDeleteContractors($type)
    {
        $idArray = [];
        $models = Yii::$app->request->post('contractors', []);
        foreach ($models as $id => $model) {
            if ($model['checked']) {
                $idArray[] = $id;
            }
        }

        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        Contractor::updateAll([
            'invoice_income_item_id' => null,
            'invoice_expenditure_item_id' => null,
        ], [
            'AND',
            ['company_id' => $user->currentEmployeeCompany->company_id],
            ['IN', 'id', $idArray],
        ]);

        Yii::$app->session->setFlash('success', 'Статьи успешно удалены у выбранных контрагентов.');

        return $this->redirect(['index', 'type' => $type]);
    }

    public function actionChangeContractor()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $contractorId = Yii::$app->request->post('contractorId');
        $articleId = Yii::$app->request->post('articleId');
        $type = Yii::$app->request->post('type');
        $filterType = Yii::$app->request->post('filterType');
        $contractor = Contractor::findOne(['id' => $contractorId]);
        if ($contractor === null) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }

        $article = $this->findModel($articleId, $type);
        if ((int)$type === ArticlesSearch::TYPE_INCOME) {
            $attribute = 'invoice_income_item_id';
        } else {
            $attribute = 'invoice_expenditure_item_id';
        }

        $contractor->$attribute = $article->id;
        $result = $contractor->save(true, [$attribute]);
        $html = null;
        if ($result === true) {
            $searchModel = new ArticlesSearch($filterType);
            $data = $searchModel->search();
            $html = $this->renderPartial('articles-list', [
                'searchModel' => $searchModel,
                'data' => $data,
                'type' => (int)$filterType,
                'contractorsOnPage' => self::$contractorsOnPage
            ]);
        }

        return [
            'result' => $result,
            'html' => $html,
        ];
    }

    public function actionGetXls()
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        Yii::$app->response->format = Response::FORMAT_RAW;
        $searchModel = new ArticlesSearch(ArticlesSearch::TYPE_BY_ACTIVITY);
        $searchModel->generateXls();
    }

    /**
     * @param $id
     * @param $type
     * @return InvoiceIncomeItem|InvoiceExpenditureItem|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id, $type)
    {
        if ((int)$type === ArticlesSearch::TYPE_INCOME) {
            $class = InvoiceIncomeItem::class;
        } else {
            $class = InvoiceExpenditureItem::class;
        }

        /** @var InvoiceIncomeItem|InvoiceExpenditureItem $class */
        $model = $class::findOne(['id' => $id]);
        if ($model === null) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }

        return $model;
    }
}