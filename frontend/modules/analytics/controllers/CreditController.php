<?php

namespace frontend\modules\analytics\controllers;

use common\models\document\AbstractDocument;
use common\models\document\OrderDocument;
use common\models\file\actions\FileDeleteAction;
use common\models\file\actions\FileListAction;
use common\models\file\actions\FileUploadAction;
use common\models\file\actions\GetFileAction;
use common\models\file\File;
use frontend\models\Documents;
use frontend\modules\analytics\models\credits\CreditDocument;
use frontend\modules\analytics\models\credits\CreditRepository;
use frontend\modules\analytics\models\credits\DebtCalculatorFactory;
use frontend\modules\analytics\models\credits\DebtPlanning;
use frontend\rbac\permissions\BusinessAnalytics;
use yii\base\BaseObject;
use yii\filters\AccessControl;
use frontend\components\BusinessAnalyticsAccess;
use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\analytics\models\credits\CreditFlowRepository;
use frontend\modules\analytics\models\credits\CreditFormFactory;
use frontend\modules\analytics\models\credits\FlowFormFactory;
use frontend\modules\analytics\models\credits\FlowSelectFormFactory;
use frontend\modules\analytics\models\credits\RepositoryFactory;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;
use yii\db\Exception;

class CreditController extends Controller
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var Response
     */
    private $response;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'file-get' => [
                'class' => GetFileAction::class,
                'model' => File::class,
                'idParam' => 'file-id',
                'fileNameField' => 'filename_full',
                'folderPath' => function($model) {
                    return $model->getUploadPath();
                },
            ],

            'file-list' => [
                'class' => FileListAction::class,
                'model' => CreditDocument::class,
                'idField' => 'credit_id',
                'fileLinkCallback' => function (File $file, AbstractDocument $ownerModel) {
                    return Url::to([
                        'file-get',
                        'type' => $ownerModel instanceof OrderDocument ? Documents::IO_TYPE_OUT : $ownerModel->type,
                        'id' => $ownerModel->id,
                        'file-id' => $file->id
                    ]);
                },
            ],

            'file-delete' => [
                'class' => FileDeleteAction::class,
                'idField' => 'credit_id',
                'model' => CreditDocument::class,
            ],

            'file-upload' => [
                'class' => FileUploadAction::class,
                'maxFileCount' => 6,
                'idField' => 'credit_id',
                'model' => CreditDocument::class,
                'noLimitCountFile' => true,
                'folderPath' => 'documents' . DIRECTORY_SEPARATOR . CreditDocument::$uploadDirectory,
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [BusinessAnalytics::INDEX],
                        'matchCallback' => function() {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_FINANCE);
                        },
                    ],
                ],
            ],
            'verbFilter' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'file-list' => ['get'],
                    'file-upload' => ['post'],
                    'file-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->request = Yii::$app->request;
        $this->response = Yii::$app->response;
        $this->view->params['is_finance_reports_module'] = true;
    }

    /**
     *
     */
    public function actionCreate()
    {
        $form = (new CreditFormFactory)->modelCreate();

        if ($form->load($this->request->post()) && $form->validate() && $form->saveModel()) {

            $debtPlanning = new DebtPlanning(['credit' => $form->credit]);
            $debtPlanning->createPlanFlows();

            return $this->redirect(['credit/schedule', 'credit_id' => $form->credit->credit_id]);
        }

        return $this->render('create', compact('form'));
    }

    /**
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionUpdate()
    {
        $form = (new CreditFormFactory)->modelUpdate();
        $credit_id = $this->request->get('credit_id');

        if (!$form->loadModel($credit_id)) {
            throw new NotFoundHttpException();
        }

        if ($form->load($this->request->post()) && $form->validate() && $form->saveModel()) {

            $debtPlanning = new DebtPlanning(['credit' => $form->credit]);
            $debtPlanning->updatePlanFlows();

            return $this->redirect(['credit/schedule', 'credit_id' => $credit_id]);
        }

        return $this->render('update', compact('form'));
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionSchedule()
    {
        $debug = (YII_DEBUG && $this->request->get('debug'));
        $form = (new CreditFormFactory)->modelUpdate();
        $credit_id = $this->request->get('credit_id');

        if (!$form->loadModel($credit_id)) {
            throw new NotFoundHttpException('Кредит не найден.');
        }

        return $this->render('schedule', ['credit' => $form->credit, 'debug' => $debug]);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionRegistry()
    {
        $credit = $this->getCredit();
        $form = (new FlowSelectFormFactory)->createForm($credit);

        if ($form->load($this->request->post())) {
            if ($form->validate() && $form->updateModels()) {
                $credit->save(false);
            }

            return $this->refresh();
        }

        $repository = $this->createCreditFlowRepository($credit);
        $repository->load($this->request->get());

        if (!$repository->validate()) {
            $repository = $this->createCreditFlowRepository($credit);
        }

        return $this->render('registry', compact('credit', 'repository'));
    }

    /**
     * @return string
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionCreateFlow()
    {
        $type = $this->getFlowType();
        $credit = $this->getCredit();
        $form = (new FlowFormFactory)->modelCreate($type, $credit);

        if ($form->load($this->request->post()) && $form->validate() && $form->saveModel()) {
            $credit->save(false);

            return '';
        }

        $repository = new CreditRepository([
            'company_id' => $credit->company_id,
            'onlyStatuses' => [Credit::STATUS_ACTIVE, Credit::STATUS_EXPIRED],
        ]);

        $credits = $repository->getQuery()->indexBy('credit_id')->all();
        $credits[$credit->credit_id] = $credit;

        return $this->renderAjax('widgets/flow-form', [
            'form' => $form,
            'credits' => $credits,
        ]);
    }

    /**
     * @return string
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionUpdateFlow()
    {
        $type = $this->getFlowType();
        $credit = $this->getCredit();
        $form = (new FlowFormFactory)->modelUpdate($type, $credit);

        if (!$form->loadModel($this->request->get('credit_flow_id'))) {
            throw new NotFoundHttpException();
        }

        if ($form->load($this->request->post()) && $form->validate() && $form->saveModel()) {
            $credit->save(false);

            return '';
        }

        $repository = new CreditRepository([
            'company_id' => $credit->company_id,
            'onlyStatuses' => [Credit::STATUS_ACTIVE, Credit::STATUS_EXPIRED],
        ]);

        $credits = $repository->getQuery()->indexBy('credit_id')->all();
        $credits[$credit->credit_id] = $credit;

        return $this->renderAjax('widgets/flow-form', [
            'form' => $form,
            'credits' => $credits,
        ]);
    }

    /**
     * @return string
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionDeleteFlow()
    {
        $type = $this->getFlowType();
        $credit = $this->getCredit();
        $form = (new FlowFormFactory)->modelDelete($type, $credit);

        if (!$form->loadModel($this->request->get('credit_flow_id'))) {
            throw new NotFoundHttpException();
        }

        if ($this->request->isPost && $form->removeModel()) {
            $credit->save(false);

            return '';
        }

        return $this->renderAjax('widgets/flow-delete', compact('form'));
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @deprecated
     */
    public function actionData()
    {
        $credit = $this->getCredit();

        return $this->asJson($credit->toArray($credit->fields(), $credit->extraFields()));
    }

    /**
     * @param Credit $credit
     * @return CreditFlowRepository
     */
    private function createCreditFlowRepository(Credit $credit): CreditFlowRepository
    {
        /** @var CreditFlowRepository $repository */
        $repository = (new RepositoryFactory)->createRepository(CreditFlowRepository::class);
        $repository->setAttributes(['credit_id' => $credit->credit_id], false);

        return $repository;
    }

    /**
     * @return Credit
     * @throws NotFoundHttpException
     */
    private function getCredit(): Credit
    {
        $form = (new CreditFormFactory)->modelUpdate();
        $credit_id = $this->request->get('credit_id');

        if (!$form->loadModel($credit_id)) {
            throw new NotFoundHttpException('Кредит не найден.');
        }

        return $form->credit;
    }

    /**
     * @return int
     * @throws BadRequestHttpException
     */
    private function getFlowType(): int
    {
        $type = $this->request->get('type');

        if (!isset(FlowFormFactory::FORM_LIST[$type])) {
            throw new BadRequestHttpException('Invalid type.');
        }

        return $type;
    }
}
