<?php

namespace frontend\modules\analytics\controllers;

use frontend\modules\analytics\models\financePlan\chart\FinancePlanChart;
use frontend\modules\analytics\models\financePlan\FinancePlanGroupExpenditureItem;
use frontend\modules\analytics\models\financePlan\FinancePlanIncomeItem;
use frontend\modules\analytics\models\financePlan\odds\FinancePlanOddsItem;
use Yii;
use yii\web\Response;
use yii\db\Connection;
use yii\base\Exception;
use common\models\User;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use common\models\employee\Employee;
use frontend\components\FrontendController;
use common\components\filters\AccessControl;
use frontend\components\BusinessAnalyticsAccess;
use frontend\rbac\permissions\BusinessAnalytics;
use frontend\modules\analytics\models\financePlan\FinancePlan;
use frontend\modules\analytics\models\financePlan\FinancePlanGroup;
use frontend\modules\analytics\models\financePlan\FinancePlanGroupSearch;
use frontend\modules\analytics\models\financePlan\FinancePlanExpenditureItem;
use frontend\modules\analytics\models\financePlan\FinancePlanItem;
use frontend\modules\analytics\models\financePlan\FinancePlanPrimeCostItem;
use frontend\modules\analytics\models\financePlan\odds\FinancePlanOddsExpenditureItem;
use frontend\modules\analytics\models\financePlan\odds\FinancePlanOddsIncomeItem;

class FinancePlanController extends FrontendController
{
    const ERROR_MESSAGE_EDIT = 'Произошла ошибка во время редактирования. Попробуйте повторить попытку или обратитесь в службу технической поддержки';

    /**
     * @var string
     */
    public $layout = 'finance-plan';

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function ($rule, $action) {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_FINANCE);
                        },
                    ],
                ],
            ],
        ]);
    }

    public function actionIndex()
    {
        /* @var $user User */
        $user = Yii::$app->user;
        $searchModel = new FinancePlanGroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 129, true);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
        ]);
    }

    /**
     * @param $id
     * @param $tab
     * @param $plan
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id, $tab = null, $plan = null)
    {
        /* @var $employee Employee */
        $employee = Yii::$app->user->identity;
        $model = $this->findModel($id);

        switch ($tab) {
            case FinancePlanGroup::TAB_EXPENSES:
                $tab = FinancePlanGroup::TAB_EXPENSES;
                $activePlan = new FinancePlan(['model_group_id' => $model->id]);
                break;
            case FinancePlanGroup::TAB_PLAN:
            default:
                $tab = FinancePlanGroup::TAB_PLAN;
                $activePlan = $model->getActivePlan($plan);
                break;
        }

        return $this->render('view', [
            'model' => $model,
            'tab' => $tab,
            'employee' => $employee,
            'activePlan' => $activePlan,
            'modelChart' => new FinancePlanChart($model)
        ]);
    }

    /**
     * @return array|Response
     * @throws \Throwable
     */
    public function actionCreate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* @var $employee Employee */
        $employee = Yii::$app->user->identity;

        $model = new FinancePlanGroup([
            'company_id' => $employee->company->id,
            'employee_id' => $employee->id
        ]);
        $model->load(Yii::$app->request->post());

        if (Yii::$app->request->post('ajax')) {
            return ActiveForm::validate($model);
        }

        $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($model) {
            if ($model->save()) {
                if ($modelPlan = $model->createMainPlan()) {
                    if ($modelPlan->createEmptyIncomeItems()
                        && $modelPlan->createEmptyExpenseItems()
                        && $modelPlan->createOddsEmptyIncomeItems()
                        && $modelPlan->createOddsEmptyExpenseItems()
                    ) {
                        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 130, true);

                        return true;
                    }
                }
            }

            Yii::$app->db->transaction->rollBack();

            return false;
        });

        if ($isSaved) {
            Yii::$app->session->setFlash('success', 'Финансовая модель сохранена');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {

            Yii::$app->session->setFlash('success', 'Не удалось сохранить Финансовую Модель');
        }

        return $this->redirect(Yii::$app->request->referrer ? : ['financial-model']);
    }

    /**
     * @param $id
     * @return array|Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionUpdate($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);
        $model->load(Yii::$app->request->post());

        if (Yii::$app->request->post('ajax')) {

            $plansValidation = [];
            foreach ($model->financePlans as $plan) {
                if ($plan->load(Yii::$app->request->post()))
                    $plansValidation = array_merge($plansValidation, ActiveForm::validate($plan));
            }

            return array_merge(ActiveForm::validate($model), $plansValidation);
        }

        $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($model) {

            $removedPlans = (array)Yii::$app->request->post('removedPlans');

            if ($model->save() && $model->updateMonthsRange()) {

                foreach ($model->financePlans as $plan) {

                    if (in_array($plan->id, $removedPlans)) {
                        $plan->delete();
                        continue;
                    }

                    if ($plan->load(Yii::$app->request->post())) {
                        $plan->save();
                    }
                }

                $model->updateTotals();

                return true;
            }

            $db->transaction->rollBack();
            return false;
        });

        if ($isSaved) {
            Yii::$app->session->setFlash('success', 'Финансовая модель обновлена');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {

            Yii::$app->session->setFlash('success', 'Не удалось обновить Финансовую Модель');
        }

        return $this->redirect(Yii::$app->request->referrer ? : ['view', 'id' => $model->id]);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionManyDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $models = Yii::$app->request->post('FinancePlanGroup');
        $noDeleted = [];
        $deleted = [];
        foreach ($models as $id => $model) {
            if ($model['checked']) {
                $model = $this->findModel($id);
                $isDeleted = Yii::$app->db->transaction(function (Connection $db) use ($model) {
                    return (bool)$model->delete();
                });
                if ($isDeleted) {
                    $deleted[] = 'Модель ' . $model->name . ' от ' . date('d.m.Y', strtotime($model->created_at));
                } else {
                    $noDeleted[] = 'Модель ' . $model->name . ' от ' . date('d.m.Y', strtotime($model->created_at));
                }
            }
        }

        if (count($noDeleted) > 0) {
            Yii::$app->session->setFlash('error', 'Модели не могут быть удалены:<br>' . implode('<br>', $noDeleted));
        }
        if (count($deleted) > 0) {
            Yii::$app->session->setFlash('success', 'Модели успешно удалены:<br>' . implode('<br>', $deleted));
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['/analytics/finance-model']);
    }

    /*
     * Plan
     */

    public function actionCreatePlan($group_id)
    {
        $groupModel = $this->findModel($group_id);
        $model = new FinancePlan(['model_group_id' => $groupModel->id]);
        $clonedModel = ($clonedPlanID = Yii::$app->request->post('clonedPlanID'))
            ? FinancePlan::findOne(['id' => $clonedPlanID])
            : null;

        if (Yii::$app->request->post('ajax')) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->load(Yii::$app->request->post());
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {

            $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($model, $clonedModel) {
                if ($model->save()) {
                    if ($model->createEmptyIncomeItems()
                    && $model->createEmptyExpenseItems()
                    && $model->createOddsEmptyIncomeItems()
                    && $model->createOddsEmptyExpenseItems())

                        if ($clonedModel) {
                            $this->clonePlanItems($model, $clonedModel);
                        }

                        return true;
                }

                Yii::$app->db->transaction->rollBack();

                return false;
            });

            if ($isSaved) {
                Yii::$app->session->setFlash('success', 'Направление добавлено');
            } else {
                Yii::$app->session->setFlash('success', 'Не удалось добавить направление');
            }
        }

        return $this->redirect(['view', 'id' => $groupModel->id, 'plan' => $model->id ?: null]);
    }

    /**
     * @param FinancePlan $model
     * @param FinancePlan $clonedModel
     */
    public function clonePlanItems(FinancePlan $model, FinancePlan $clonedModel)
    {
        foreach ($clonedModel->expenditureItems as $clonedItem)
        {
            $item = new FinancePlanExpenditureItem(['model_id' => $model->id, 'data' => $model->emptyRow]);
            $item->setAttributes($clonedItem->getAttributes(['expenditure_item_id', 'type', 'relation_type', 'action']), false);
            $item->save();
        }
        foreach ($clonedModel->primeCostItems as $clonedItem)
        {
            $item = new FinancePlanPrimeCostItem(['model_id' => $model->id, 'data' => $model->emptyRow]);
            $item->setAttributes($clonedItem->getAttributes(['prime_cost_item_id', 'relation_type', 'action']), false);
            $item->save();
        }
    }

    /**
     * @param $group
     * @param null $plan
     * @return string|Response
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionUpdatePlan($group, $plan = null)
    {
        $employee = Yii::$app->user->identity;
        $model = $this->findModel($group);

        if (Yii::$app->request->get('save')) {

            // FinancePlanGroup.sessionPlans
            $updatedPlans = $model->removeSessionData();

            if (is_array($updatedPlans)) {
                /** @var FinancePlan $p */
                foreach ($updatedPlans as $p) {
                    foreach ($p->incomeItems as $item)
                        if (!$item->save()) {throw new Exception(self::ERROR_MESSAGE_EDIT);}
                    foreach ($p->expenditureItems as $item)
                        if (!$item->save()) {throw new Exception(self::ERROR_MESSAGE_EDIT);}
                    foreach ($p->primeCostItems as $item)
                        if (!$item->save()) {throw new Exception(self::ERROR_MESSAGE_EDIT);}
                    foreach ($p->oddsIncomeItems as $item)
                        if (!$item->save()) { throw new Exception(self::ERROR_MESSAGE_EDIT);}
                    foreach ($p->oddsExpenditureItems as $item)
                        if (!$item->save()) {throw new Exception(self::ERROR_MESSAGE_EDIT);}
                }

                // group expenses
                foreach ($model->expenditureItems as $item)
                    if (!$item->save()) {throw new Exception(self::ERROR_MESSAGE_EDIT);}
            }

            $model->updateTotals($updatedPlans);

            return $this->redirect(['view', 'id' => $model->id, 'plan' => $plan]);
        }

        // FinancePlanGroup.sessionPlans
        $model->setSessionData();

        return $this->render('view', [
            'IS_EDIT_MODE' => true,
            'tab' => FinancePlanGroup::TAB_PLAN,
            'model' => $model,
            'activePlan' => $model->getActivePlan($plan),
            'employee' => $employee,
        ]);
    }

    /**
     * @param $group
     * @param null $plan
     * @return string|Response
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionUpdateExpenses($group)
    {
        $employee = Yii::$app->user->identity;
        $model = $this->findModel($group);

        return $this->render('view', [
            'IS_EDIT_MODE' => true,
            'tab' => FinancePlanGroup::TAB_EXPENSES,
            'model' => $model,
            'employee' => $employee,
            'activePlan' => new FinancePlan(['model_group_id' => $model->id]),
        ]);
    }

    public function actionUpdatePlanAjax($group, $plan)
    {
        $employee = Yii::$app->user->identity;
        $model = $this->findModel($group);

        // FinancePlanGroup.sessionPlans
        $updatedPlans = $model->getSessionData();
        $activePlan = $model->getActivePlan($plan);

        if (isset($activePlan) && $activePlan instanceof FinancePlan) {
            if ($userData = Yii::$app->request->post('userData', [])) {
                $activePlan->loadUserData($userData);
            }
            if ($autofillData = Yii::$app->request->post('autofill', [])) {
                $activePlan->loadAutofillData($autofillData);
            }
        } else {
            throw new Exception(self::ERROR_MESSAGE_EDIT);
        }

        // FinancePlanGroup.sessionPlans
        $model->setSessionData();

        return $this->renderPartial('view', [
            'IS_EDIT_MODE' => true,
            'tab' => FinancePlanGroup::TAB_PLAN,
            'model' => $model,
            'activePlan' => $activePlan,
            'employee' => $employee,
        ]);
    }

    public function actionUpdateExpensesAjax($group)
    {
        $employee = Yii::$app->user->identity;
        $model = $this->findModel($group);

        // FinancePlanGroup.sessionExpenses
        $model->getSessionData();

        if ($userData = Yii::$app->request->post('userData', [])) {
            $model->loadUserData($userData);
        }
        if ($autofillData = Yii::$app->request->post('autofill', [])) {
            $model->loadAutofillData($autofillData);
        }

        // FinancePlanGroup.sessionExpenses
        $model->setSessionData();

        return $this->renderPartial('view', [
            'IS_EDIT_MODE' => true,
            'tab' => FinancePlanGroup::TAB_EXPENSES,
            'model' => $model,
            'employee' => $employee,
            'activePlan' => new FinancePlan(['model_group_id' => $model->id]),
        ]);
    }

    /*
     * Item
     */

    public function actionEditIncomeItemAjax($group, $plan, $row_type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $modelGroup = $this->findModel($group);

        // FinancePlanGroup.sessionPlans
        $updatedPlans = $modelGroup->getSessionData();
        $activePlan = $modelGroup->getActivePlan($plan);

        $return = false;
        if (isset($activePlan) && $activePlan instanceof FinancePlan) {
            switch ($row_type) {
                case FinancePlan::ROW_TYPE_INCOME_OTHER:
                    $return = $this->_editIncomeItem($activePlan, $row_type);
                    break;
            }
        }
        // FinancePlanGroup.sessionPlans
        $modelGroup->setSessionData();

        return $return;
    }

    public function actionEditExpenseItemAjax($group, $plan, $row_type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $modelGroup = $this->findModel($group);

        // FinancePlanGroup.sessionPlans
        $updatedPlans = $modelGroup->getSessionData();
        $activePlan = $modelGroup->getActivePlan($plan);

        $return = false;
        if (isset($activePlan) && $activePlan instanceof FinancePlan) {
            try {
                switch ($row_type) {
                    case FinancePlan::ROW_TYPE_EXPENSE_PRIME_COST:
                        $return = $this->_editPrimeCost($activePlan);
                        break;
                    case FinancePlan::ROW_TYPE_EXPENSE_VARIABLE:
                        $return = $this->_editVariableCost($activePlan);
                        break;
                    case FinancePlan::ROW_TYPE_EXPENSE_FIXED:
                        $return = $this->_editFixedCost($activePlan);
                        break;
                    case FinancePlan::ROW_TYPE_EXPENSE_TAX:
                        $return = $this->_editTaxCost($activePlan);
                        break;
                    case FinancePlan::ROW_TYPE_EXPENSE_OTHER:
                        $return = $this->_editExpenseItem($activePlan, $row_type);
                        break;
                }
            } catch (\Throwable $e) {}
        }
        // FinancePlanGroup.sessionPlans
        $modelGroup->setSessionData();

        return $return;
    }

    public function actionEditGroupItemAjax($group, $io_type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $modelGroup = $this->findModel($group);

        // FinancePlanGroup.sessionPlans
        $updatedPlans = $modelGroup->getSessionData();

        $return = false;
        switch ($io_type) {
            case FinancePlan::GROUP_IO_TYPE_EXPENSE:
                $return = $this->_editGroupExpenseItem($modelGroup);
                break;
        }
        // FinancePlanGroup.sessionPlans
        $modelGroup->setSessionData();

        return $return;
    }

    /**
     * @param FinancePlan $plan
     * @return array|bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function _editIncomeItem(FinancePlan $plan, $rowType)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($rowID = Yii::$app->request->post('row_id')) {
            foreach ($plan->incomeItems as $item) {
                if ($item->id == $rowID) {
                    $activeItem = $item;
                    break;
                }
            }
        } elseif ($rowType) {
            $activeItem = new FinancePlanIncomeItem([
                'model_id' => $plan->id,
                'type' => $rowType
            ]);
        } else {
            throw new Exception('Неизвестная строка в отчете ОПиУ');
        }

        if (isset($activeItem) && $activeItem->load(Yii::$app->request->post())) {

            // Delete
            if ($isDeleted = Yii::$app->request->post('is_deleted')) {
                if (!$activeItem->delete()) {
                    return false;
                }

                return true;
            }

            // Validate
            if (Yii::$app->request->post('ajax')) {
                return ActiveForm::validate($activeItem);
            }

            // Check is not system item
            //if (in_array($activeItem->income_item_id, FinancePlan::SYSTEM_INCOME_ITEMS_IDS))
            //    return false;

            // Create
            if ($activeItem->isNewRecord) {
                $activeItem->income_item_id = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanIncomeItem.income_item_id');
                $activeItem->action = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanIncomeItem.action');
                $activeItem->relation_type = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanIncomeItem.relation_type');
                $activeItem->data = json_encode($plan->getEmptyRow());
                if ($activeItem->save()) {
                    $plan->populateRelation('incomeItems', array_merge($plan->incomeItems, [$activeItem]));
                    if ($autofillData = Yii::$app->request->post('autofill', [])) {
                        $autofillData['attr'] = "income_{$activeItem->id}";
                        $autofillData['row_id'] = $activeItem->id;
                        $plan->loadAutofillData($autofillData);
                    }
                } else {
                    return false;
                }

                $plan->setNewSessionRelatedModel($activeItem, FinancePlanGroup::SESS_INCOME_ITEMS);
            }

            return true;
        }

        return false;
    }
    
    /**
     * @param FinancePlan $plan
     * @return array|bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function _editExpenseItem(FinancePlan $plan, $rowType)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($rowID = Yii::$app->request->post('row_id')) {
            foreach ($plan->expenditureItems as $item) {
                if ($item->id == $rowID) {
                    $activeItem = $item;
                    break;
                }
            }
        } elseif ($rowType) {
            $activeItem = new FinancePlanExpenditureItem([
                'model_id' => $plan->id,
                'type' => $rowType
            ]);
        } else {
            throw new Exception('Неизвестная строка в отчете ОПиУ');
        }

        if (isset($activeItem) && $activeItem->load(Yii::$app->request->post())) {

            // Delete
            if ($isDeleted = Yii::$app->request->post('is_deleted')) {
                if (!$activeItem->delete()) {
                    return false;
                }

                return true;
            }

            // Validate
            if (Yii::$app->request->post('ajax')) {
                return ActiveForm::validate($activeItem);
            }

            // Check is not system item
            //if (in_array($activeItem->expenditure_item_id, FinancePlan::SYSTEM_EXPENDITURE_ITEMS_IDS))
            //    return false;

            // Create
            if ($activeItem->isNewRecord) {
                $activeItem->expenditure_item_id = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanExpenditureItem.expenditure_item_id');
                $activeItem->action = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanExpenditureItem.action');
                $activeItem->relation_type = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanExpenditureItem.relation_type');
                $activeItem->data = json_encode($plan->getEmptyRow());
                if ($activeItem->save()) {
                    $plan->populateRelation('expenditureItems', array_merge($plan->expenditureItems, [$activeItem]));
                    if ($autofillData = Yii::$app->request->post('autofill', [])) {
                        $autofillData['attr'] = "expense_{$activeItem->id}";
                        $autofillData['row_id'] = $activeItem->id;
                        $plan->loadAutofillData($autofillData);
                    }
                } else {
                    return false;
                }

                $plan->setNewSessionRelatedModel($activeItem, FinancePlanGroup::SESS_EXPENDITURE_ITEMS);
            }

            return true;
        }

        return false;
    }

    /*
     * Item
     */

    public function actionEditOddsItemAjax($group, $plan, $io_type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $modelGroup = $this->findModel($group);

        // FinancePlanGroup.sessionPlans
        $updatedPlans = $modelGroup->getSessionData();
        $activePlan = $modelGroup->getActivePlan($plan);

        $return = false;
        if (isset($activePlan) && $activePlan instanceof FinancePlan) {
            switch ($io_type) {
                case FinancePlan::ODDS_IO_TYPE_INCOME:
                    $return = $this->_editOddsIncomeItem($activePlan);
                    break;
                case FinancePlan::ODDS_IO_TYPE_EXPENSE:
                    $return = $this->_editOddsExpenseItem($activePlan);
                    break;
            }
        }
        // FinancePlanGroup.sessionPlans
        $modelGroup->setSessionData();

        return $return;
    }

    public function actionEditOddsSystemItemAjax($group, $plan, $io_type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $modelGroup = $this->findModel($group);

        // FinancePlanGroup.sessionPlans
        $updatedPlans = $modelGroup->getSessionData();
        $activePlan = $modelGroup->getActivePlan($plan);

        $return = false;
        if (isset($activePlan) && $activePlan instanceof FinancePlan) {
            switch ($io_type) {
                case FinancePlan::ODDS_IO_TYPE_INCOME:
                    $return = $this->_editOddsSystemIncomeItem($activePlan);
                    break;
                case FinancePlan::ODDS_IO_TYPE_EXPENSE:
                    $return = $this->_editOddsSystemExpenseItem($activePlan);
                    break;
            }
        }
        // FinancePlanGroup.sessionPlans
        $modelGroup->setSessionData();

        return $return;
    }

    /**
     * @param FinancePlan $plan
     * @return array|bool
     * @throws \Throwable
     */
    public function _editPrimeCost(FinancePlan $plan)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($rowID = Yii::$app->request->post('row_id')) {
            foreach ($plan->primeCostItems as $item) {
                if ($item->id == $rowID) {
                    $activeItem = $item;
                    break;
                }
            }
        } else {
            $activeItem = new FinancePlanPrimeCostItem(['model_id' => $plan->id]);
        }

        if (isset($activeItem) && $activeItem->load(Yii::$app->request->post())) {

            // Delete
            if ($isDeleted = Yii::$app->request->post('is_deleted')) {
                if (!$activeItem->delete()) {
                    return false;
                }

                return true;
            }

            // Validate
            if (Yii::$app->request->post('ajax')) {
                return ActiveForm::validate($activeItem);
            }

            // Create
            if ($activeItem->isNewRecord) {
                $activeItem->prime_cost_item_id = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanPrimeCostItem.prime_cost_item_id');
                $activeItem->action = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanPrimeCostItem.action');
                $activeItem->relation_type = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanPrimeCostItem.relation_type');
                $activeItem->data = json_encode($plan->getEmptyRow());
                if (!$activeItem->save()) {
                    return false;
                }

                $plan->setNewSessionRelatedModel($activeItem, FinancePlanGroup::SESS_PRIME_COST_ITEMS);
            }

            return true;
        }

        return false;
    }

    /**
     * @param FinancePlan $plan
     * @return array|bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function _editVariableCost(FinancePlan $plan)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($rowID = Yii::$app->request->post('row_id')) {
            foreach ($plan->expenditureItems as $item) {
                
                if ($item->type != FinancePlan::ROW_TYPE_EXPENSE_VARIABLE)
                    continue;
                
                if ($item->id == $rowID) {
                    $activeItem = $item;
                    break;
                }
            }
        } else {
            $activeItem = new FinancePlanExpenditureItem(['model_id' => $plan->id, 'type' => FinancePlan::ROW_TYPE_EXPENSE_VARIABLE]);
        }

        if (isset($activeItem) && $activeItem->load(Yii::$app->request->post())) {

            // Delete
            if ($isDeleted = Yii::$app->request->post('is_deleted')) {
                if (!$activeItem->delete()) {
                    return false;
                }

                return true;
            }

            // Validate
            if (Yii::$app->request->post('ajax')) {
                return ActiveForm::validate($activeItem);
            }

            // Create
            if ($activeItem->isNewRecord) {
                $activeItem->expenditure_item_id = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanExpenditureItem.expenditure_item_id');
                $activeItem->action = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanExpenditureItem.action');
                $activeItem->relation_type = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanExpenditureItem.relation_type');
                $activeItem->data = json_encode($plan->getEmptyRow());
                if (!$activeItem->save()) {
                    return false;
                }

                $plan->setNewSessionRelatedModel($activeItem, FinancePlanGroup::SESS_EXPENDITURE_ITEMS);
            }

            return true;
        }

        return false;
    }

    /**
     * @param FinancePlan $plan
     * @return array|bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function _editFixedCost(FinancePlan $plan)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($rowID = Yii::$app->request->post('row_id')) {
            foreach ($plan->expenditureItems as $item) {

                if ($item->type != FinancePlan::ROW_TYPE_EXPENSE_FIXED)
                    continue;

                if ($item->id == $rowID) {
                    $activeItem = $item;
                    break;
                }
            }
        } else {
            $activeItem = new FinancePlanExpenditureItem(['model_id' => $plan->id, 'type' => FinancePlan::ROW_TYPE_EXPENSE_FIXED]);
        }

        if (isset($activeItem) && $activeItem->load(Yii::$app->request->post())) {

            // Delete
            if ($isDeleted = Yii::$app->request->post('is_deleted')) {
                if (!$activeItem->delete()) {
                    return false;
                }

                return true;
            }

            // Validate
            if (Yii::$app->request->post('ajax')) {
                return ActiveForm::validate($activeItem);
            }

            // Create
            if ($activeItem->isNewRecord) {
                $activeItem->expenditure_item_id = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanExpenditureItem.expenditure_item_id');
                $activeItem->action = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanExpenditureItem.action');
                $activeItem->relation_type = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanExpenditureItem.relation_type');
                $activeItem->data = json_encode($plan->getEmptyRow());
                if ($activeItem->save()) {
                    $plan->populateRelation('expenditureItems', array_merge($plan->expenditureItems, [$activeItem]));
                    if ($autofillData = Yii::$app->request->post('autofill', [])) {
                        $autofillData['attr'] = "expense_{$activeItem->id}";
                        $autofillData['row_id'] = $activeItem->id;
                        $plan->loadAutofillData($autofillData);
                    }
                } else {
                    return false;
                }

                $plan->setNewSessionRelatedModel($activeItem, FinancePlanGroup::SESS_EXPENDITURE_ITEMS);
            }

            return true;
        }

        return false;
    }

    /**
     * @param FinancePlan $plan
     * @return array|bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function _editTaxCost(FinancePlan $plan)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        foreach ($plan->expenditureItems as $item) {
            if ($item->type == FinancePlan::ROW_TYPE_EXPENSE_TAX) {
                $activeItem = $item;
                break;
            }
        }
        if (isset($activeItem) && $activeItem->load(Yii::$app->request->post())) {
            // Validate
            if (Yii::$app->request->post('ajax')) {
                return ActiveForm::validate($activeItem);
            }

            if ($activeItem->relation_type == FinancePlanItem::RELATION_TYPE_UNSET) {
                $activeItem->setRowData($plan->getFilledRow((float)$activeItem->action));
            }
        }

        return false;
    }

    /**
     * @param FinancePlan $plan
     * @return array|bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function _editOddsIncomeItem(FinancePlan $plan)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($rowID = Yii::$app->request->post('row_id')) {
            foreach ($plan->oddsIncomeItems as $item) {

                if ($item->id == $rowID) {
                    $activeItem = $item;
                    break;
                }
            }
        } elseif ($rowType = Yii::$app->request->post('row_type')) {
            $activeItem = new FinancePlanOddsIncomeItem([
                'model_id' => $plan->id,
                'type' => $rowType
            ]);
        } else {
            throw new Exception('Неизвестная строка в отчете ОДДС');
        }

        if (isset($activeItem) && $activeItem->load(Yii::$app->request->post())) {

            // Delete
            if ($isDeleted = Yii::$app->request->post('is_deleted')) {
                if (!$activeItem->delete()) {
                    return false;
                }

                return true;
            }

            // Validate
            if (Yii::$app->request->post('ajax')) {
                return ActiveForm::validate($activeItem);
            }

            // Check is not system item
            if (in_array($activeItem->income_item_id, FinancePlan::SYSTEM_INCOME_ITEMS_IDS))
                return false;

            // Create
            if ($activeItem->isNewRecord) {
                $activeItem->income_item_id = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanOddsIncomeItem.income_item_id');
                $activeItem->action = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanOddsIncomeItem.action');
                $activeItem->relation_type = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanOddsIncomeItem.relation_type');
                $activeItem->data = json_encode($plan->getEmptyRow());
                if (!$activeItem->save()) {
                    return false;
                }

                $plan->setNewSessionRelatedModel($activeItem, FinancePlanGroup::SESS_ODDS_INCOME_ITEMS);
            }

            return true;
        }

        return false;
    }

    /**
     * @param FinancePlan $plan
     * @return array|bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function _editOddsExpenseItem(FinancePlan $plan)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($rowID = Yii::$app->request->post('row_id')) {
            foreach ($plan->oddsExpenditureItems as $item) {

                if ($item->id == $rowID) {
                    $activeItem = $item;
                    break;
                }
            }
        } elseif ($rowType = Yii::$app->request->post('row_type')) {
            $activeItem = new FinancePlanOddsExpenditureItem([
                'model_id' => $plan->id,
                'type' => $rowType
            ]);
        } else {
            throw new Exception('Неизвестная строка в отчете ОДДС');
        }

        if (isset($activeItem) && $activeItem->load(Yii::$app->request->post())) {

            // Delete
            if ($isDeleted = Yii::$app->request->post('is_deleted')) {
                if (!$activeItem->delete()) {
                    return false;
                }

                return true;
            }

            // Validate
            if (Yii::$app->request->post('ajax')) {
                return ActiveForm::validate($activeItem);
            }

            // Check is not system item
            if (in_array($activeItem->expenditure_item_id, FinancePlan::SYSTEM_EXPENDITURE_ITEMS_IDS))
                return false;

            // Create
            if ($activeItem->isNewRecord) {
                $activeItem->expenditure_item_id = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanOddsExpenditureItem.expenditure_item_id');
                $activeItem->action = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanOddsExpenditureItem.action');
                $activeItem->relation_type = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanOddsExpenditureItem.relation_type');
                $activeItem->data = json_encode($plan->getEmptyRow());
                if (!$activeItem->save()) {
                    return false;
                }

                $plan->setNewSessionRelatedModel($activeItem, FinancePlanGroup::SESS_ODDS_EXPENDITURE_ITEMS);
            }

            return true;
        }

        return false;
    }

    /**
     * @param FinancePlanGroup $group
     * @return array|bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function _editGroupExpenseItem(FinancePlanGroup $group)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($rowID = Yii::$app->request->post('row_id')) {
            foreach ($group->expenditureItems as $item) {
                if ($item->id == $rowID) {
                    $activeItem = $item;
                    break;
                }
            }
        } else {
            $activeItem = new FinancePlanGroupExpenditureItem([
                'model_group_id' => $group->id,
                'fill_type' => FinancePlanGroupExpenditureItem::FILL_TYPE_AUTOFILL
            ]);
        }

        if (isset($activeItem) && $activeItem->load(Yii::$app->request->post())) {

            // Delete
            if ($isDeleted = Yii::$app->request->post('is_deleted')) {
                if (!$activeItem->delete()) {
                    return false;
                }

                return true;
            }

            // Validate
            if (Yii::$app->request->post('ajax')) {
                return ActiveForm::validate($activeItem);
            }

            // Create
            if ($activeItem->isNewRecord) {
                $activeItem->expenditure_item_id = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanGroupExpenditureItem.expenditure_item_id');
                $activeItem->action = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanGroupExpenditureItem.action');
                $activeItem->relation_type = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanGroupExpenditureItem.relation_type');
                $activeItem->data = json_encode($group->getEmptyRow());
                if ($activeItem->save()) {
                    $group->populateRelation('expenditureItems', array_merge($group->expenditureItems, [$activeItem]));
                    if ($autofillData = Yii::$app->request->post('autofill', [])) {
                        $autofillData['attr'] = "expense_{$activeItem->id}";
                        $autofillData['row_id'] = $activeItem->id;
                        $group->loadAutofillData($autofillData);
                    }
                } else {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @return FinancePlanGroup
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $employee = Yii::$app->user->identity;
        /** @var FinancePlanGroup $model */
        $model = FinancePlanGroup::find()
            ->with([
                'expenditureItems',
                'financePlans',
                'financePlans.incomeItems',
                'financePlans.expenditureItems',
                'financePlans.primeCostItems',
                'financePlans.oddsIncomeItems',
                'financePlans.oddsExpenditureItems',
            ])
            ->where(['id' => $id])
            ->andWhere(['company_id' => $employee->company->id])
            ->one();

        if ($model) {
            $model->mainIndustryId = $model->mainFinancePlan->industry_id;
            $model->mainIndustryName = $model->mainFinancePlan->name;
        }

        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Запрошенная страница не существует.');
    }

    /**
     * @param FinancePlan $plan
     * @return array|bool
     * @throws Exception
     * @throws \Throwable
     */
    public function _editOddsSystemIncomeItem(FinancePlan $plan)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($rowID = Yii::$app->request->post('row_id')) {
            foreach ($plan->oddsIncomeItems as $item) {
                if ($item->id == $rowID) {
                    $activeItem = $item;
                    break;
                }
            }
        } else {
            throw new Exception('Неизвестная строка в отчете ОДДС');
        }

        if (isset($activeItem) && $activeItem->load(Yii::$app->request->post())) {

            // Validate
            if (Yii::$app->request->post('ajax')) {
                return ActiveForm::validate($activeItem);
            }

            if ($activeItem->payment_type == FinancePlanOddsItem::PAYMENT_TYPE_PREPAYMENT) {
                foreach ($plan->oddsIncomeItems as $item) {
                    if ($item->type == FinancePlan::ODDS_ROW_TYPE_OPERATIONS && $item->payment_type == FinancePlanOddsItem::PAYMENT_TYPE_SURCHARGE) {
                        $oppositeItem = $item;
                        break;
                    }
                }
            } elseif ($activeItem->payment_type == FinancePlanOddsItem::PAYMENT_TYPE_SURCHARGE) {
                foreach ($plan->oddsIncomeItems as $item) {
                    if ($item->type == FinancePlan::ODDS_ROW_TYPE_OPERATIONS && $item->payment_type == FinancePlanOddsItem::PAYMENT_TYPE_PREPAYMENT) {
                        $oppositeItem = $item;
                        break;
                    }
                }
            }

            // Update system items
            if (isset($oppositeItem)) {
                $activeItem->action = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanOddsIncomeItem.action');
                $activeItem->payment_delay = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanOddsIncomeItem.payment_delay');
                $oppositeItem->action = 100 - $activeItem->action;

                $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($activeItem, $oppositeItem) {
                    if ($activeItem->save() && $oppositeItem->save()) {
                        return true;
                    }
                    Yii::$app->db->transaction->rollBack();
                    return false;
                });

                if ($isSaved)
                    return true;
            }

            return false;
        }

        return false;
    }

    /**
     * @param FinancePlan $plan
     * @return array|bool
     * @throws Exception
     * @throws \Throwable
     */
    public function _editOddsSystemExpenseItem(FinancePlan $plan)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($rowID = Yii::$app->request->post('row_id')) {
            foreach ($plan->oddsExpenditureItems as $item) {
                if ($item->id == $rowID) {
                    $activeItem = $item;
                    break;
                }
            }
        } else {
            throw new Exception('Неизвестная строка в отчете ОДДС');
        }

        if (isset($activeItem) && $activeItem->load(Yii::$app->request->post())) {

            // Validate
            if (Yii::$app->request->post('ajax')) {
                return ActiveForm::validate($activeItem);
            }

            if ($activeItem->payment_type == FinancePlanOddsItem::PAYMENT_TYPE_PREPAYMENT) {
                foreach ($plan->oddsExpenditureItems as $item) {
                    if ($item->type == FinancePlan::ODDS_ROW_TYPE_OPERATIONS && $item->payment_type == FinancePlanOddsItem::PAYMENT_TYPE_SURCHARGE) {
                        $oppositeItem = $item;
                        break;
                    }
                }
            } elseif ($activeItem->payment_type == FinancePlanOddsItem::PAYMENT_TYPE_SURCHARGE) {
                foreach ($plan->oddsExpenditureItems as $item) {
                    if ($item->type == FinancePlan::ODDS_ROW_TYPE_OPERATIONS && $item->payment_type == FinancePlanOddsItem::PAYMENT_TYPE_PREPAYMENT) {
                        $oppositeItem = $item;
                        break;
                    }
                }
            }

            // Update system items
            if (isset($oppositeItem)) {
                $activeItem->action = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanOddsExpenditureItem.action');
                $activeItem->payment_delay = ArrayHelper::getValue(Yii::$app->request->post(), 'FinancePlanOddsExpenditureItem.payment_delay');
                $oppositeItem->action = 100 - $activeItem->action;

                $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($activeItem, $oppositeItem) {
                    if ($activeItem->save() && $oppositeItem->save()) {
                        return true;
                    }
                    Yii::$app->db->transaction->rollBack();
                    return false;
                });

                if ($isSaved)
                    return true;
            }

            return false;
        }

        return false;
    }
}
