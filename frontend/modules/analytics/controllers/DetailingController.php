<?php

namespace frontend\modules\analytics\controllers;

use common\models\project\Project;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use common\models\company\Event;
use common\models\company\CompanyFirstEvent;
use common\components\filters\AccessControl;
use common\models\company\CompanyIndustry;
use common\models\company\CompanyInfoIndustry;
use common\models\companyStructure\SalePoint;
use common\models\companyStructure\SalePointType;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\employee\Employee;
use common\models\product\Store;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\PageSize;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\analytics\models\AnalyticsSimpleSearch;
use frontend\modules\analytics\models\detailing\ProjectSearch;
use frontend\modules\analytics\models\detailing\CompanyIndustrySearch;
use frontend\modules\analytics\models\detailing\DetailingUserConfig as Config;
use frontend\modules\analytics\models\detailing\SalePointSearch;
use frontend\modules\analytics\models\OddsSearch;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;

/**
 * Class DetailingController
 * @package frontend\modules\analytics\controllers
 */
class DetailingController extends DetailingBaseController
{
    const TAB_INDUSTRY = 'industry';
    const TAB_SALE_POINT = 'sale_point';
    const TAB_PROJECT = 'project';

    const TAB_VIEW_DASHBOARD = 'dashboard';
    const TAB_VIEW_ODDS = 'odds';
    const TAB_VIEW_PAL = 'pal';
    const TAB_VIEW_DEBTOR = 'debtor';
    const TAB_VIEW_PROJECTS = 'projects';

    /**
     * @var
     */
    public $year;

    /**
     * @var string
     */
    public $layout = 'detailing';

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'item-list', // get docs/flows list
                            'debtor-change-view' // change table view
                        ],
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\Reports::VIEW,
                        ],
                    ],
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function ($rule, $action) {

                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_FINANCE);
                        },
                    ],
                ],
            ],
        ]);
    }

    private static $analyticsClasses = [
        'CompanyIndustrySearch',
        'ProfitAndLossSearchModel',
        'OddsSearch',
        'ProjectSearch'
    ];

    public function beforeAction($action)
    {
        if (!Yii::$app->request->isAjax) {

            foreach (self::$analyticsClasses as $class) {
                if (\Yii::$app->request->get($class)) {
                    $year = ArrayHelper::getValue(\Yii::$app->request->get($class), 'year');
                    if ($year > 1999 && $year < 2999) {
                        \Yii::$app->session->set('modules.reports.finance.year', $year);
                        $getParams = \Yii::$app->request->queryParams;
                        unset($getParams[$class]);
                        return $this->redirect(Url::toRoute([Url::base()] + $getParams));
                    }
                }
            }
        }

        $this->year = Yii::$app->session->get('modules.reports.finance.year', date('Y'));

        return parent::beforeAction($action);
    }

    /**
     * @param null $tab
     * @return string
     */
    public function actionIndex($tab = null)
    {
        if (!$tab || !in_array($tab, [self::TAB_INDUSTRY, self::TAB_SALE_POINT, self::TAB_PROJECT])) {
            $tab = self::TAB_INDUSTRY;
        }

        self::_checkEvent($tab);

        switch ($tab) {
            case self::TAB_PROJECT:
                return $this->actionIndexProject();
            case self::TAB_SALE_POINT:
                return $this->actionIndexSalePoint();
            case self::TAB_INDUSTRY:
            default:
                return $this->actionIndexIndustry();
        }
    }

    // INDUSTRY //

    public function actionIndexIndustry()
    {
        $searchModel = new CompanyIndustrySearch();
        $searchModel->setYear($this->year);
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $userConfig = Yii::$app->user->identity->config;
        $dataProvider->pagination->pageSize = PageSize::get();

        list($palModel, $analyticsModel) = $this->_getModelsIndexChart(self::PAGE_INDUSTRY);

        return $this->render('industry/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'userConfig' => $userConfig,
            'analyticsModel' => $analyticsModel ?? null,
            'palModel' => $palModel ?? null,
            'currentYear' => $searchModel->getYear(),
            'onPage' => self::PAGE_INDUSTRY
        ]);
    }

    private function _getModelsIndexChart($onPage)
    {
        $company = $userConfig = Yii::$app->user->identity->company;
        $customChartType = Config::getIndexChartType();

        switch ($customChartType) {
            case Config::CHART_TYPE_INCOME:
                $analyticsModel = new AnalyticsSimpleSearch($company);
                break;
            case Config::CHART_TYPE_EXPENSE:
                $analyticsModel = new AnalyticsSimpleSearch($company);
                break;
            case Config::CHART_TYPE_REVENUE:
                $palModel = $this->_getIndexPagePalModel($onPage);
                break;
            case Config::CHART_TYPE_PROFIT:
                $palModel = $this->_getIndexPagePalModel($onPage);
                break;
            default:
                $analyticsModel = new AnalyticsSimpleSearch($company);
                $palModel = $this->_getIndexPagePalModel($onPage);
                break;
        }

        return [
            $palModel ?? null,
            $analyticsModel ?? null,
        ];
    }

    /**
     * @param $id
     * @param null $tab
     * @param null $type
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionViewIndustry($id, $tab = null, $type = null)
    {
        /** @var CompanyIndustry $model */
        $model = $this->findIndustryModel($id);
        $this->layout = 'detailing_item';

        if (!in_array($tab, [self::TAB_VIEW_DASHBOARD, self::TAB_VIEW_ODDS, self::TAB_VIEW_PAL, self::TAB_VIEW_DEBTOR, self::TAB_VIEW_PROJECTS]))
            $tab = self::TAB_VIEW_DASHBOARD;

        if (!in_array($type, [Contractor::TYPE_CUSTOMER, Contractor::TYPE_SELLER]))
            $type = Contractor::TYPE_CUSTOMER;

        // todo: merge filters structure
        $oddsPageFilter = [
            'filters' => ['industry_id' => $model->id]
        ];
        $palPageFilter = [
            'pageFilters' => ['industry_id' => $model->id]
        ];

        switch ($tab) {
            case self::TAB_VIEW_DASHBOARD:
                $title = $model->name;
                $tabData = $this->_getTabDataDashboard($title, $oddsPageFilter, $palPageFilter);
                break;
            case self::TAB_VIEW_ODDS:
                $title = $model->name;
                $tabData = $this->_getTabDataOdds($title, $oddsPageFilter);
                break;
            case self::TAB_VIEW_PAL:
                $title = $model->name;

                $tabData = $this->_getTabDataProfitLoss($title, $palPageFilter);
                break;
            case self::TAB_VIEW_DEBTOR:
                $tabData = [
                    'tabFile' => 'view/tab_in_development',
                    'title' => ($type == Contractor::TYPE_CUSTOMER) ? 'Нам должны' : 'Мы должны',
                    'type' => $type
                ];
                break;
            case self::TAB_VIEW_PROJECTS:
                $tabData = [
                    'tabFile' => 'view/tab_in_development',
                    'title' => 'Проекты',
                ];
                break;
            default:
                $tabData = [
                    'tabFile' => 'view/tab_in_development',
                    'title' => '',
                ];
                break;
        }

        return $this->render('industry/view', [
            'tab' => $tab,
            'tabData' => $tabData,
            'model' => $model,
            'activeYear' => $this->year
        ]);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionChangeIndustryStatus() {
        $ids = \Yii::$app->request->post('id');
        $status = \Yii::$app->request->post('status');

        if (strlen($status)) {
            foreach ((array)$ids as $id) {
                $industry = $this->findIndustryModel($id);
                $industry->status_id = intval($status);
                if ($industry->save()) {
                    LogHelper::log($industry, LogEntityType::TYPE_COMPANY_INDUSTRY, LogEvent::LOG_EVENT_UPDATE_STATUS);
                    Yii::$app->session->setFlash('success', 'Статус изменен.');
                }
            }
        }

        return $this->redirect(\Yii::$app->request->referrer);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionChangeIndustryType() {
        $ids = \Yii::$app->request->post('id');
        $type = \Yii::$app->request->post('industry_type');

        if (strlen($type)) {
            foreach ((array)$ids as $id) {
                $industry = $this->findIndustryModel($id);
                $industry->industry_type_id = intval($type);
                if ($industry->save()) {
                    Yii::$app->session->setFlash('success', 'Тип изменен.');
                }
            }
        }

        return $this->redirect(\Yii::$app->request->referrer);
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function actionCreateIndustry()
    {
        $isSaved = false;
        $user = Yii::$app->user->identity;
        $company = Yii::$app->user->identity->company;
        $model = new CompanyIndustry([
            'company_id' => $company->id,
            'employee_id' => $user->id
        ]);

        if ($model->load(Yii::$app->request->post())) {

            if ($model->validate()) {
                $isSaved = LogHelper::save($model, LogEntityType::TYPE_COMPANY_INDUSTRY, LogEvent::LOG_EVENT_CREATE);
            }
        }

        if ($isSaved) {
            Yii::$app->session->setFlash('success', 'Направление добавлено');
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->renderAjax('industry/partial/_modal_add_industry', [
            'model' => $model,
            'company' => $company,
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdateIndustry($id)
    {
        $isSaved = false;
        $user = Yii::$app->user->identity;
        $company = Yii::$app->user->identity->company;
        $model = $this->findIndustryModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $isSaved = LogHelper::save($model, LogEntityType::TYPE_COMPANY_INDUSTRY, LogEvent::LOG_EVENT_UPDATE);
            }
        }

        if ($isSaved) {
            Yii::$app->session->setFlash('success', 'Направление обновлено');
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->renderAjax('industry/partial/_modal_add_industry', [
            'model' => $model,
            'company' => $company,
        ]);
    }    
    
    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionManyDeleteIndustry() {
        $ids = \Yii::$app->request->post('id');
        $message = '';
        if (!empty($ids)) {
            foreach ((array)$ids as $id) {
                $industry = $this->findIndustryModel($id);
                if ($industry->is_main) {
                    $message .= 'Нельзя удалить основное направление "' .Html::encode($industry->name). '"<br/>';
                } elseif ($industry->hasMovement()) {
                    $message .= 'Не удалось удалить направление "' .Html::encode($industry->name). '", есть привязанные операции <br/>';
                } else {
                    $isDeleted = LogHelper::delete($industry, LogEntityType::TYPE_COMPANY_INDUSTRY);
                    if ($isDeleted) {
                        $message .= 'Направление "' .Html::encode($industry->name). '" удалено<br/>';
                    } else {
                        $message .= 'Не удалось удалить направление "' .Html::encode($industry->name). '"<br/>';
                    }
                }
            }
        }

        Yii::$app->session->setFlash('success', $message);

        return $this->redirect(\Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return CompanyIndustry|null
     * @throws NotFoundHttpException
     */
    protected function findIndustryModel($id)
    {
        if ($id) {
            $model = CompanyIndustry::findOne([
                'id' => $id,
                'company_id' => Yii::$app->user->identity->company->id]);
        } else {
            $model = new CompanyIndustry([
                'id' => 0,
                'company_id' => Yii::$app->user->identity->company->id,
                'name' => 'Без направления',
            ]);
            $model->populateRelation('industryType', new CompanyInfoIndustry);
        }

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    // SALE POINT //

    public function actionIndexSalePoint()
    {
        $searchModel = new SalePointSearch();
        $searchModel->setYear($this->year);
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $userConfig = Yii::$app->user->identity->config;
        $dataProvider->pagination->pageSize = PageSize::get();

        list($palModel, $analyticsModel) = $this->_getModelsIndexChart(self::PAGE_SALE_POINT);

        return $this->render('sale_point/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'userConfig' => $userConfig,
            'analyticsModel' => $analyticsModel ?? null,
            'palModel' => $palModel ?? null,
            'currentYear' => $searchModel->getYear(),
            'onPage' => self::PAGE_SALE_POINT
        ]);
    }

    /**
     * @param $id
     * @param null $tab
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionViewSalePoint($id, $tab = null, $type = null)
    {
        /** @var SalePoint $model */
        $model = $this->findSalePointModel($id);
        $this->layout = 'detailing_item';

        if (!in_array($tab, [self::TAB_VIEW_DASHBOARD, self::TAB_VIEW_ODDS, self::TAB_VIEW_PAL, self::TAB_VIEW_DEBTOR]))
            $tab = self::TAB_VIEW_ODDS;

        if (!in_array($type, [Contractor::TYPE_CUSTOMER, Contractor::TYPE_SELLER]))
            $type = Contractor::TYPE_CUSTOMER;

        switch ($tab) {
            case self::TAB_VIEW_DASHBOARD:
                $tabData = [
                    'tabFile' => 'view/tab_in_development',
                    'title' => 'Дашборд',
                ];
                break;
            case self::TAB_VIEW_ODDS:
                $title = $model->name;
                $pageFilter = ['filters' => ['sale_point_id' => $model->id]];
                $tabData = $this->_getTabDataOdds($title, $pageFilter);
                break;
            case self::TAB_VIEW_PAL:
                $title = $model->name;
                $pageFilter = ['pageFilters' => ['sale_point_id' => $model->id]];
                $tabData = $this->_getTabDataProfitLoss($title, $pageFilter);
                break;
            case self::TAB_VIEW_DEBTOR:
                $tabData = [
                    'tabFile' => 'view/tab_in_development',
                    'title' => ($type == Contractor::TYPE_CUSTOMER) ? 'Нам должны' : 'Мы должны',
                    'type' => $type
                ];
                break;
            default:
                $tabData = [
                    'tabFile' => 'view/tab_in_development',
                    'title' => '',
                ];
                break;
        }

        return $this->render('sale_point/view', [
            'tab' => $tab,
            'tabData' => $tabData,
            'model' => $model,
            'activeYear' => $this->year
        ]);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionChangeSalePointStatus() {
        $ids = \Yii::$app->request->post('id');
        $status = \Yii::$app->request->post('status');

        if (strlen($status)) {
            foreach ((array)$ids as $id) {
                $point = $this->findSalePointModel($id);
                $point->status_id = intval($status);
                if ($point->save()) {
                    LogHelper::log($point, LogEntityType::TYPE_SALE_POINT, LogEvent::LOG_EVENT_UPDATE_STATUS);
                    Yii::$app->session->setFlash('success', 'Статус изменен.');
                }
            }
        }

        return $this->redirect(\Yii::$app->request->referrer);
    }

    public function actionChangeSalePointEmployers()
    {
        $company = Yii::$app->user->identity->company;

        $ids = \Yii::$app->request->post('id');
        $employersIds = \Yii::$app->request->post('employers');
        $employers = ($employersIds)
            ? Employee::find()->where(['company_id' => $company->id, 'id' => $employersIds])->all()
            : [];

        if (empty($employersIds) || !empty($employers)) {
            foreach ((array)$ids as $id) {
                $point = $this->findSalePointModel($id);

                $isSaved = \Yii::$app->db->transaction(function ($db) use ($point, $employers) {

                    $point->unlinkAll('employers', true);
                    foreach ($employers as $val) {
                        $point->link('employers', $val);
                    }

                    if ($point->save()) {
                        LogHelper::log($point, LogEntityType::TYPE_SALE_POINT, LogEvent::LOG_EVENT_UPDATE);
                        return true;
                    }

                    if ($db->getTransaction()->isActive) {
                        $db->getTransaction()->rollBack();
                    }

                    return false;
                });

                if ($isSaved) {
                    Yii::$app->session->setFlash('success', 'Сотрудники изменены.');
                } else {
                    Yii::$app->session->setFlash('success', 'Не удалось изменить сотрудников');
                    break;
                }
            }
        }

        return $this->redirect(\Yii::$app->request->referrer);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionManyDeleteSalePoint() {
        $ids = \Yii::$app->request->post('id');
        $message = '';
        if (!empty($ids)) {
            foreach ((array)$ids as $id) {
                $point = $this->findSalePointModel($id);
                if ($point->hasMovement()) {
                    $message .= 'Не удалось удалить точку продаж "' .Html::encode($point->name). '", есть привязанные операции <br/>';
                } else {
                    $isDeleted = LogHelper::delete($point, LogEntityType::TYPE_SALE_POINT);
                    if ($isDeleted) {
                        $message .= 'Точка продаж "' .Html::encode($point->name). '" удалена<br/>';
                    } else {
                        $message .= 'Не удалось удалить точку продаж "' .Html::encode($point->name). '"<br/>';
                    }
                }
            }
        }

        Yii::$app->session->setFlash('success', $message);

        return $this->redirect(\Yii::$app->request->referrer);
    }    

    /**
     * @param $id
     * @return SalePoint|null
     * @throws NotFoundHttpException
     */
    protected function findSalePointModel($id)
    {
        if ($id) {
            $model = SalePoint::findOne([
                'id' => $id,
                'company_id' => Yii::$app->user->identity->company->id]);
        } else {
            $model = new SalePoint([
                'id' => 0,
                'company_id' => Yii::$app->user->identity->company->id,
                'name' => 'Без точки продаж',
            ]);
            $model->populateRelation('type', new SalePointType);
            $model->populateRelation('store', new Store);
        }

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /// PROJECT ///

    public function actionIndexProject()
    {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $userConfig = Yii::$app->user->identity->config;
        $dataProvider->pagination->pageSize = PageSize::get();

        list($palModel, $analyticsModel) = $this->_getModelsIndexChart(self::PAGE_PROJECT);

        return $this->render('project/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'userConfig' => $userConfig,
            'analyticsModel' => $analyticsModel ?? null,
            'palModel' => $palModel ?? null,
            'onPage' => self::PAGE_PROJECT
        ]);
    }

    /**
     * @param $id
     * @param null $tab
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionViewProject($id, $tab = null, $type = null)
    {
        /** @var SalePoint $model */
        $model = $this->findProjectModel($id);
        $this->layout = 'detailing_item';

        if (!in_array($tab, [self::TAB_VIEW_DASHBOARD, self::TAB_VIEW_ODDS, self::TAB_VIEW_PAL, self::TAB_VIEW_DEBTOR]))
            $tab = self::TAB_VIEW_ODDS;

        if (!in_array($type, [Contractor::TYPE_CUSTOMER, Contractor::TYPE_SELLER]))
            $type = Contractor::TYPE_CUSTOMER;

        switch ($tab) {
            case self::TAB_VIEW_DASHBOARD:
                $tabData = [
                    'tabFile' => 'view/tab_in_development',
                    'title' => 'Дашборд',
                ];
                break;
            case self::TAB_VIEW_ODDS:
                $title = $model->name;
                $pageFilter = ['filters' => ['project_id' => $model->id]];
                $tabData = $this->_getTabDataOdds($title, $pageFilter);
                break;
            case self::TAB_VIEW_PAL:
                $title = $model->name;
                $pageFilter = ['pageFilters' => ['project_id' => $model->id]];
                $tabData = $this->_getTabDataProfitLoss($title, $pageFilter);
                break;
            case self::TAB_VIEW_DEBTOR:
                $tabData = [
                    'tabFile' => 'view/tab_in_development',
                    'title' => ($type == Contractor::TYPE_CUSTOMER) ? 'Нам должны' : 'Мы должны',
                    'type' => $type
                ];
                break;
            default:
                $tabData = [
                    'tabFile' => 'view/tab_in_development',
                    'title' => '',
                ];
                break;
        }

        return $this->render('project/view', [
            'tab' => $tab,
            'tabData' => $tabData,
            'model' => $model,
            'activeYear' => $this->year
        ]);
    }

    /**
     * @param $id
     * @return Project|null
     * @throws NotFoundHttpException
     */
    protected function findProjectModel($id)
    {
        if ($id) {
            $model = Project::findOne([
                'id' => $id,
                'company_id' => Yii::$app->user->identity->company->id]);
        } else {
            $model = new Project([
                'id' => 0,
                'company_id' => Yii::$app->user->identity->company->id,
                'name' => 'Без проекта',
            ]);
        }

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    // TAB DATA
    private function _getTabDataDashboard($title, $oddsPageFilter, $palPageFilter)
    {
        $oddsModel = $this->_getDashboardOddsModel($oddsPageFilter);
        $palModel = $this->_getDashboardPalModel($palPageFilter);

        return [
            'tabFile' => 'view/tab_dashboard',
            'oddsModel' => $oddsModel,
            'palModel' => $palModel,
            'pageFilter' => $oddsPageFilter['filters'],
            //'currencyId' => Currency::DEFAULT_ID,
            //'currencyName' => Currency::DEFAULT_NAME,
        ];
    }

    private function _getTabDataOdds($title, $oddsPageFilter)
    {
        $_get = Yii::$app->request->get();

        $periodSize = (string)ArrayHelper::getValue($_get, 'periodSize', 'months');
        $activeTab = (string)ArrayHelper::getValue($_get, 'activeTab', OddsSearch::TAB_ODDS);
        $searchModel = new OddsSearch();

        $data = ($periodSize == 'days') ?
            $searchModel->searchByDays($activeTab, $oddsPageFilter) :
            $searchModel->search($activeTab, $oddsPageFilter);

        return [
            'tabFile' => '@frontend/modules/analytics/views/finance/odds2',
            'searchModel' => $searchModel,
            'pageFilter' => $oddsPageFilter['filters'],
            'data' => $data['data'],
            'growingData' => $data['growingData'],
            'totalData' => $data['totalData'],
            'activeTab' => $activeTab,
            'periodSize' => $periodSize,
            'warnings' => [],
            'currencyId' => Currency::DEFAULT_ID,
            'currencyName' => Currency::DEFAULT_NAME,
            'show' => [
                'text' => [
                    'title' => $title,
                ],
                'button' => [
                    'multicompany' => 0,
                    'chart' => 1,
                    'help' => 0,
                    'video' => 0,
                    'options' => 0,
                    'excel' => 0,
                    'table_view' => 1,
                    'currency' => 0,
                ],
                'select' => [
                    'by_activity' => 1,
                    'by_purse' => 1,
                    'by_purse_type' => 0,
                    'by_industry' => 0,
                    'by_sale_point' => 0,
                    'by_currency' => 0,
                    'year' => 0
                ]
            ]
        ];
    }

    private function _getTabDataProfitLoss($title, $palPageFilter)
    {
        ProfitAndLossSearchModel::refreshAnalyticsArticles();

        $_get = Yii::$app->request->get();
        $activeTab = (string)ArrayHelper::getValue($_get, 'activeTab', ProfitAndLossSearchModel::TAB_ALL_OPERATIONS);

        $searchModel = new ProfitAndLossSearchModel($palPageFilter);
        $searchModel->activeTab = $activeTab;
        $searchModel->disableMultiCompanyMode();
        $searchModel->load(Yii::$app->request->get());
        $data = $searchModel->handleItems();

        return [
            'tabFile' => '@frontend/modules/analytics/views/finance/profit-and-loss',
            'searchModel' => $searchModel,
            'pageFilter' => $palPageFilter['pageFilters'],
            'activeTab' => $activeTab,
            'data' => $data,
            'show' => [
                'text' => [
                    'title' => $title,
                ],
                'button' => [
                    'multicompany' => 0,
                    'chart' => 1,
                    'help' => 0,
                    'video' => 0,
                    'options' => 1,
                    'excel' => 0,
                    'table_view' => 1,
                    'month_group_by' => 0
                ],
                'modal_options' => [
                    'part_articles' => 0,
                    'part_additional' => 1,
                    'part_revenue_analytics' => 0,
                    'part_bottom_text' => 0
                ],
                'select' => [
                    'year' => 0
                ]
            ]
        ];
    }

    private function _checkEvent($tab)
    {
        switch ($tab) {
            case self::TAB_PROJECT:
                $eventId = Event::FINANCE_DETAILING_PROJECT;
                break;
            case self::TAB_SALE_POINT:
                $eventId = Event::FINANCE_DETAILING_SALE_POINT;
                break;
            case self::TAB_INDUSTRY:
            default:
                $eventId = Event::FINANCE_DETAILING_INDUSTRY;
                break;
        }

        CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, $eventId, true);
    }
}