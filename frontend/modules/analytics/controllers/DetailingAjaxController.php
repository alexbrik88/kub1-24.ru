<?php

namespace frontend\modules\analytics\controllers;

use common\components\filters\AccessControl;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\modules\analytics\models\AnalyticsSimpleSearch;
use frontend\modules\analytics\models\detailing\DetailingUserConfig;
use frontend\modules\analytics\models\detailing\DetailingUserConfig as Config;
use frontend\modules\analytics\models\OddsSearch;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class DetailingAjaxController
 * @package frontend\modules\analytics\controllers
 */
class DetailingAjaxController extends DetailingBaseController
{
    const VIEW = '@frontend/modules/analytics/views/detailing/';

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function ($rule, $action) {

                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_FINANCE);
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string|null
     */
    public function actionGetDashboardPieData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('chart-dashboard-pie-ajax')) {

            $purse = (string)Yii::$app->request->post('purse');
            $pageFilter = (array)Yii::$app->request->post('pageFilter');
            $chartType = (string)Yii::$app->request->post('chartType');
            $date = (array)Yii::$app->request->post('date');

            $oddsPageFilter = [
                'filters' => $pageFilter
            ];
            $palPageFilter = [
                'pageFilters' => $pageFilter
            ];

            switch ($chartType) {
                case 'income':
                    $oddsModel = $this->_getDashboardOddsModel($oddsPageFilter);
                    $view = '_charts/_dashboard_pie_chart_income';
                    break;
                case 'expense':
                    $oddsModel = $this->_getDashboardOddsModel($oddsPageFilter);
                    $view = '_charts/_dashboard_pie_chart_expense';
                    break;
                case 'revenue':
                    $palModel = $this->_getDashboardPalModel($palPageFilter);
                    $view = '_charts/_dashboard_pie_profit_loss_revenue';
                    break;
                default:
                    return 'unknown chart type!';
            }

            return $this->renderPartial(self::VIEW . $view, [
                'oddsModel' => $oddsModel ?? null,
                'palModel' => $palModel ?? null,
                'dateFrom' => $date['from'] ?? null,
                'dateTo' => $date['to'] ?? null,
                'customPurse' => $purse,
                'pageFilter' => $pageFilter,
            ]);
        }

        return null;
    }

    /**
     * @return string|null
     */
    public function actionGetIndexPageData()
    {
        Yii::$app->response->format = Response::FORMAT_HTML;

        if (Yii::$app->request->post('chart-detailing-index-ajax')) {

            $company = Yii::$app->user->identity->company;
            $onPage = (string)Yii::$app->request->post('on-page');
            $chartType = (int)Yii::$app->request->post('chart-type');
            $offset = (int)Yii::$app->request->post('offset');
            $currentYear = (string)Yii::$app->session->get('modules.reports.finance.year', date('Y'));

            switch ($chartType) {
                case Config::CHART_TYPE_INCOME:
                    $analyticsModel = new AnalyticsSimpleSearch($company);
                    break;
                case Config::CHART_TYPE_EXPENSE:
                    $analyticsModel = new AnalyticsSimpleSearch($company);
                    break;
                case Config::CHART_TYPE_REVENUE:
                    $palModel = $this->_getIndexPagePalModel($onPage);
                    break;
                case Config::CHART_TYPE_PROFIT:
                    $palModel = $this->_getIndexPagePalModel($onPage);
                    break;
                default:
                    $analyticsModel = new AnalyticsSimpleSearch($company);
                    $palModel = $this->_getIndexPagePalModel($onPage);
                    break;
            }

            return $this->renderPartial(self::VIEW . '_charts/_index_charts', [
                'isAjax' => true,
                'palModel' => $palModel ?? null,
                'analyticsModel' => $analyticsModel ?? null,
                'customChartType' => $chartType,
                'customOffset' => $offset,
                'onPage' => $onPage,
                'currentYear' => $currentYear
            ]);
        }

        return null;
    }

    /**
     * @return array
     */
    public function actionSetIndexPageChartType()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $onPage = (string)Yii::$app->request->post('on-page');
        $chartType = (int)Yii::$app->request->post('chart-type');

        if (in_array($chartType, array_keys(DetailingUserConfig::$chartTypeName))) {
            $config = Yii::$app->user->identity->config;
            $config->updateAttributes([
                'detailing_index_chart_type' => (int)$chartType
            ]);

            return ['success' => 1];
        }

        return ['success' => 0];
    }

    /**
     * For project-page only
     * @return array
     */
    public function actionSetUserChartSeriesFilter()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $onPage = (string)Yii::$app->request->post('on-page');
        $checked = (array)Yii::$app->request->post('checked');

        foreach ($checked as &$c)
            if (!intval($c))
                unset($c);

        if (!$checked || in_array('all', $checked))
            Yii::$app->response->cookies->remove('project_chart_series_user_filter');
        else
            Yii::$app->response->cookies->add(new \yii\web\Cookie([
                'name' => 'project_chart_series_user_filter',
                'value' => $checked,
            ]));

        return ['success' => 1];
    }
}