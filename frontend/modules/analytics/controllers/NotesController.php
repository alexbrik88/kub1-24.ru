<?php

namespace frontend\modules\analytics\controllers;

use common\components\filters\AccessControl;
use common\models\employee\Employee;
use Exception;
use frontend\components\FrontendController;
use frontend\modules\analytics\models\Notes;
use frontend\rbac\UserRole;
use frontend\rbac\permissions;
use Throwable;
use Yii;
use yii\db\ActiveRecord;
use yii\db\StaleObjectException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class NotesController
 * @package frontend\modules\analytics\controllers
 */
class NotesController extends FrontendController
{
    const NOTES_LIMIT = 50;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                        'matchCallback' => function() {
                            return Yii::$app->user->can(permissions\BusinessAnalytics::INDEX);
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $employee = Yii::$app->user->identity;
        /** @var Employee $employee */
        $company = $employee->company;

        $notes = Notes::find()
            ->andWhere(['company_id' => $company->id])
            ->andWhere(['employee_id' => $employee->id])
            ->limit(self::NOTES_LIMIT)
            ->orderBy('index')
            ->all();

        $countNotes = Notes::find()
            ->andWhere(['company_id' => $company->id])
            ->andWhere(['employee_id' => $employee->id])
            ->orderBy('index')
            ->count();

        return $this->render('index', [
            'notes' => $notes,
            'limit' => $countNotes >= self::NOTES_LIMIT,
        ]);
    }

    /**
     * @return array
     */
    public function actionValidate()
    {
        $model = new Notes();

        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            return ActiveForm::validate($model);
        }

        return [];
    }


    /**
     * @return string
     */
    public function actionCreate()
    {
        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        $model = new Notes([
            'company_id' => $company->id,
            'employee_id' => $employee->id,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Заметка обновлена.');

            return $this->redirect(['index']);
        }

        return $this->renderAjax('partial/modal', [
            'model' => $model,
            'company' => $company,
            'employee' => $employee,
        ]);
    }

    /**
     * @return mixed|Response
     * @throws HttpException
     */
    public function actionUpdate()
    {
        $id = (int)Yii::$app->request->post('id', null);

        $model = $this->findModel($id);

        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Заметка обновлена.');

            return $this->redirect(['index']);
        }

        return $this->renderAjax('partial/modal', [
            'model' => $model,
            'company' => $company,
            'employee' => $employee,
        ]);
    }

    /**
     * @return false|string
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete()
    {
        $id = (int)Yii::$app->request->post('id', null);

        $model = $this->findModel($id);

        if ($model->delete()) {
            Yii::$app->session->setFlash('success', 'Заметка удалена.');
            return json_encode(['success' => 'true']);
        }

        return json_encode(['success' => 'false']);
    }

    /**
     * @return false|string
     */
    public function actionSort()
    {
        $from = (int)Yii::$app->request->post('from', false);
        $to = (int)Yii::$app->request->post('to', false);

        if (false === $from || false === $to) {
            return json_encode(['success' => 'false']);
        }

        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        /** @var Notes[] $notes */
        $notes = Notes::find()
            ->andWhere(['company_id' => $company->id])
            ->andWhere(['employee_id' => $employee->id])
            ->orderBy('`index` ASC')
            ->all();

        $db = Yii::$app->getDb();
        $transaction = $db->beginTransaction();
        try {
            $tmpIndex = 0;
            $toIndex = $notes[$to]->index;

            for ($i = $from; (($from < $to) ? $i <= $to : $i >= $to); (($from < $to) ? $i++ : $i--)) {
                $index = $tmpIndex ?: $notes[$i]->index;

                if ($from < $to && ($i == $to)) {
                    $notes[$from]->updateAttributes(['index' => $toIndex]);
                } else if ($from > $to && ($i == $to)) {
                    $notes[$from]->updateAttributes(['index' => $toIndex]);
                } else if ($from < $to && isset($notes[$i + 1])) {
                    $tmpIndex = $notes[$i + 1]->index;
                    $notes[$i + 1]->updateAttributes(['index' => $index]);
                } else if ($from > $to && isset($notes[$i - 1])) {
                    $tmpIndex = $notes[$i - 1]->index;
                    $notes[$i - 1]->updateAttributes(['index' => $index]);
                }
            }

            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();

            return json_encode(['success' => 'false']);
        }

        return json_encode(['success' => 'true']);
    }

    /**
     * @param $id
     * @return array|ActiveRecord
     * @throws NotFoundHttpException
     */
    private function findModel($id)
    {
        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        $model = Notes::find()
            ->andWhere(['and',
                ['id' => (int)$id],
                ['company_id' => $company->id],
                ['employee_id' => $employee->id]
            ])
            ->one();

        if (empty($model)) {
            throw new NotFoundHttpException("Запрошенная страница не существует", 404);
        }

        return $model;
    }

}
