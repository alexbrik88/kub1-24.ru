<?php

namespace frontend\modules\analytics\controllers;

use Yii;
use common\models\document\InvoiceExpenditureItem;
use common\models\employee\Employee;
use common\models\User;
use frontend\modules\analytics\models\financeModel\FinanceModel;
use frontend\modules\analytics\models\financeModel\FinanceModelFixedCost;
use frontend\modules\analytics\models\financeModel\FinanceModelOtherCost;
use frontend\modules\analytics\models\financeModel\FinanceModelRevenue;
use frontend\modules\analytics\models\financeModel\FinanceModelSearch;
use frontend\modules\analytics\models\financeModel\FinanceModelShop;
use frontend\modules\analytics\models\financeModel\FinanceModelVariableCost;
use frontend\modules\analytics\models\financeModel\helpers\FinanceModelCalcHelper;
use frontend\modules\reference\models\ArticleDropDownForm;
use frontend\modules\reference\models\ArticlesSearch;
use common\components\filters\AccessControl;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use yii\base\Exception;
use yii\db\Connection;
use yii\db\Query;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\widgets\ActiveForm;
use frontend\rbac\permissions\BusinessAnalytics;
use yii\helpers\ArrayHelper;

class FinanceModelController extends FrontendController
{
    /**
     * @var string
     */
    public $layout = 'finance-model';

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function ($rule, $action) {
                            return in_array(Yii::$app->user->identity->company_id, [83537, 80906, 83968, 486, 1, 83968]);
                                //|| BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_FINANCE_PLUS);
                        },
                    ],
                ],
            ],
        ]);
    }

    public function actionIndex()
    {
        /* @var $user User */
        $user = Yii::$app->user;
        $searchModel = new FinanceModelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
        ]);
    }

    /**
     * @param $id
     * @param $tab
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id, $tab = '')
    {
        /* @var $employee Employee */
        $employee = Yii::$app->user->identity;
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
            'employee' => $employee,
            'tab' => $tab
        ]);
    }

    /**
     * @return array|Response
     * @throws \Throwable
     */
    public function actionCreate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* @var $employee Employee */
        $employee = Yii::$app->user->identity;

        $model = new FinanceModel([
            'company_id' => $employee->company->id,
            'employee_id' => $employee->id
        ]);
        $model->load(Yii::$app->request->post());

        if (Yii::$app->request->post('ajax')) {
            return ActiveForm::validate($model);
        }

        $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($model) {
            if ($model->save()) {
                if (FinanceModelOtherCost::saveMonthes($model, [], true))
                    return true;
            }

            return false;
        });

        if ($isSaved) {
            Yii::$app->session->setFlash('success', 'Финансовая модель сохранена');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {

            Yii::$app->session->setFlash('success', 'Не удалось сохранить Финансовую Модель');
        }

        return $this->redirect(Yii::$app->request->referrer ? : ['financial-model']);
    }

    /**
     * @param $id
     * @return array|Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionUpdate($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);
        $model->load(Yii::$app->request->post());

        if (Yii::$app->request->post('ajax')) {
            return ActiveForm::validate($model);
        }

        $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($model) {

                if ($model->save()
                    && FinanceModelRevenue::updateMonthsRange($model)
                    && FinanceModelOtherCost::updateMonthsRange($model)
                    && FinanceModelFixedCost::updateMonthsRange($model)
                    && FinanceModelVariableCost::updateMonthsRange($model)
                ){

                return true;
            }

            $db->transaction->rollBack();
            return false;
        });

        if ($isSaved) {
            FinanceModelCalcHelper::recalcTotals($model);
            Yii::$app->session->setFlash('success', 'Финансовая модель обновлена');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {

            Yii::$app->session->setFlash('success', 'Не удалось обновить Финансовую Модель');
        }

        return $this->redirect(Yii::$app->request->referrer ? : ['financial-model']);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionManyDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $models = Yii::$app->request->post('FinanceModel');
        $noDeleted = [];
        $deleted = [];
        foreach ($models as $id => $model) {
            if ($model['checked']) {
                $model = $this->findModel($id);
                $isDeleted = Yii::$app->db->transaction(function (Connection $db) use ($model) {
                    return (bool)$model->delete();
                });
                if ($isDeleted) {
                    $deleted[] = 'Модель ' . $model->name . ' от ' . date('d.m.Y', strtotime($model->created_at));
                } else {
                    $noDeleted[] = 'Модель ' . $model->name . ' от ' . date('d.m.Y', strtotime($model->created_at));
                }
            }
        }

        if (count($noDeleted) > 0) {
            Yii::$app->session->setFlash('error', 'Модели не могут быть удалены:<br>' . implode('<br>', $noDeleted));
        }
        if (count($deleted) > 0) {
            Yii::$app->session->setFlash('success', 'Модели успешно удалены:<br>' . implode('<br>', $deleted));
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['/analytics/finance-model']);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionCopy($id)
    {
        /* @var $employee Employee */
        $employee = Yii::$app->user->identity;

        $model = $this->findModel($id);
        $copiedId = $this->copyModel($model, $employee);

        if ($copiedId) {
            Yii::$app->session->setFlash('success', 'Модель успешно скопирована');
            return $this->redirect(['view', 'id' => $copiedId]);
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось скопировать Модель');
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['/analytics/finance-model']);
    }

    /*
     * Shop
     */

    /**
     * @param $model_id
     * @param $shop_id
     * @return array|string|Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionCreateShop($model_id, $shop_id = null)
    {
        $financeModel = $this->findModel($model_id);
        $model = new FinanceModelShop(['model_id' => $financeModel->id]);

        if (Yii::$app->request->post('ajax')) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->load(Yii::$app->request->post());
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {

            $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($model) {

                if ($model->save())
                    if (FinanceModelRevenue::saveShopMonthes($model, Yii::$app->request->post(), true))
                        return true;

                $db->transaction->rollBack();
                return false;
            });

            if ($isSaved) {
                FinanceModelCalcHelper::recalcTotals($financeModel);
                Yii::$app->session->setFlash('success', 'Точка продаж сохранена');
                return $this->redirect(['view', 'id' => $financeModel->id]);
            } else {
                Yii::$app->session->setFlash('success', 'Не удалось сохранить Точку продаж');
            }
        }

        if ($shop_id) {
            if ($copiedModel = FinanceModelShop::findOne(['id' => $shop_id, 'model_id' => $model_id])) {
                FinanceModel::fillShopByShop($model, $copiedModel);
            }
        }

        return $this->render('shop/create', [
            'financeModel' => $financeModel,
            'model' => $model
        ]);
    }

    /**
     * @param $model_id
     * @param $shop_id
     * @return array|string|Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionUpdateShop($model_id, $shop_id)
    {
        $financeModel = $this->findModel($model_id);
        $model = $this->findShop($model_id, $shop_id);

        if (Yii::$app->request->post('ajax')) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->load(Yii::$app->request->post());
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {

            $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($model) {
                if ($model->save()) {
                    if (FinanceModelRevenue::saveShopMonthes($model, Yii::$app->request->post())) {
                        if (FinanceModelVariableCost::saveShopMonthes($model))
                            return true;
                    }
                }
                $db->transaction->rollBack();
                return false;
            });

            if ($isSaved) {
                FinanceModelCalcHelper::recalcTotals($financeModel);
                Yii::$app->session->setFlash('success', 'Точка продаж обновлена');
                return $this->redirect(['view', 'id' => $financeModel->id]);
            } else {
                Yii::$app->session->setFlash('success', 'Не удалось обновить Точку продаж');
            }
        }

        return $this->render('shop/update', [
            'financeModel' => $financeModel,
            'model' => $model
        ]);
    }

    /**
     * @param $model_id
     * @param $shop_id
     * @param $type
     * @return array|string|Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionUpdateShopCosts($model_id, $shop_id, $type)
    {
        $financeModel = $this->findModel($model_id);
        $model = $this->findShop($model_id, $shop_id);

        if (Yii::$app->request->post('ajax')) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->load(Yii::$app->request->post());
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($model, $type) {
                if ($model->save()) {
                    switch ($type) {
                        case FinanceModel::TYPE_FIXED_COSTS:
                            return FinanceModelFixedCost::saveShopMonthes($model, Yii::$app->request->post());
                        case FinanceModel::TYPE_VARIABLE_COSTS:
                            return FinanceModelVariableCost::saveShopMonthes($model, Yii::$app->request->post());
                    }
                }

                $db->transaction->rollBack();
                return false;
            });

            if ($isSaved) {
                FinanceModelCalcHelper::recalcTotals($financeModel);
                Yii::$app->session->setFlash('success', ($type == 2 ? 'Постоянные':'Переменные') . ' расходы обновлены');
                return $this->redirect(['view', 'id' => $financeModel->id]);
            } else {
                Yii::$app->session->setFlash('success', 'Не удалось обновить ' . ($type == 2 ? 'Постоянные':'Переменные') . ' расходы');
            }
        }

        return $this->render('shop/update-costs', [
            'financeModel' => $financeModel,
            'model' => $model,
            'type' => $type
        ]);
    }

    /**
     * @param $model_id
     * @param $shop_id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionDeleteShop($model_id, $shop_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $financeModel = $this->findModel($model_id);
        $model = $this->findShop($model_id, $shop_id);
        $isDeleted = Yii::$app->db->transaction(function (Connection $db) use ($model) {
            return (bool)$model->delete();
        });

        if ($isDeleted) {
            FinanceModelCalcHelper::recalcTotals($financeModel);
            Yii::$app->session->setFlash('success', 'Точка продаж успешно удалена');
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось удалить Точку продаж');
        }

        return $this->redirect(['/analytics/finance-model/view', 'id' => $model_id]);
    }

    /**
     * @param $model_id
     * @param $shop_id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionGetShopMonthsAjax($model_id, $shop_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findShop($model_id, $shop_id);
        return $model->getRevenueMonthsData();
    }

    /**
     * @param $model_id
     * @param $shop_id
     * @param $type
     * @return string
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionAddExpenditureItem($model_id, $shop_id, $type)
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect(Yii::$app->request->referrer);
        }

        if (!in_array($type, [FinanceModel::TYPE_VARIABLE_COSTS, FinanceModel::TYPE_FIXED_COSTS]))
            throw new Exception('Type not found');

        $financeModel = $this->findModel($model_id);
        $shopModel = $this->findShop($model_id, $shop_id);

        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;

        /** @var ArticleDropDownForm $model */
        $model = new ArticleDropDownForm($employee, ArticlesSearch::TYPE_EXPENSE);

        if ($model->load(Yii::$app->request->post())) {

            $post = ArrayHelper::getValue(Yii::$app->request->post(), 'ArticleDropDownForm');
            $addItems = array_filter(array_merge(
                (array)ArrayHelper::getValue($post, 'items'),
                (array)ArrayHelper::getValue($post, 'items2')
            ));
            $toShops = array_keys((array)ArrayHelper::getValue($post, 'shops'));

            if ($model->validate()) {

                $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($type, $financeModel, $post, $addItems, $toShops) {

                    if ($post['isNew'] && $post['name']) {
                        $m = new InvoiceExpenditureItem();
                        $m->company_id = $financeModel->company_id;
                        $m->name = $post['name'];
                        if (!$m->save()) {
                            throw new \yii\db\Exception('Error insert new expenditure item!');
                        }
                        $addItems[] = $m->id;
                    }

                    switch ($type) {
                        case FinanceModel::TYPE_VARIABLE_COSTS:
                            return FinanceModelVariableCost::addExpenditureItems($financeModel, $toShops, $addItems);
                        case FinanceModel::TYPE_FIXED_COSTS:
                            return FinanceModelFixedCost::addExpenditureItems($financeModel, $toShops, $addItems);
                    }

                    $db->transaction->rollBack();
                    return false;
                });

                if ($isSaved) {
                    Yii::$app->session->setFlash('success', 'Статьи добавлены');
                } else {
                    Yii::$app->session->setFlash('success', 'Не удалось добавить Статьи');
                }

            } else {
                Yii::$app->session->setFlash('success', 'Не удалось добавить Статьи');
            }

            return "<script>$('#break-even-article-modal').modal('hide'); location.href = location.href; </script>";
        }

        $selectedItems = ($type == FinanceModel::TYPE_FIXED_COSTS) ?
            FinanceModelFixedCost::find()->where(['shop_id' => $shopModel->id])->select('item_id')->distinct()->column() :
            FinanceModelVariableCost::find()->where(['shop_id' => $shopModel->id])->select('item_id')->distinct()->column();

        return $this->renderAjax('shop/_expenditure_item_form', [
            'financeModel' => $financeModel,
            'shopModel' => $shopModel,
            'model' => $model,
            'blockType' => $type,
            'flowType' => ArticlesSearch::TYPE_EXPENSE,
            'selectedShops' => $toShops ?? [],
            'selectedItems' => $selectedItems ?? []
        ]);
    }

    /**
     * @param $model_id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionUpdateOtherCostItem($model_id)
    {
        $model = $this->findModel($model_id);

        $attr = Yii::$app->request->post('attr');
        $amountByMonth = Yii::$app->request->post('amount');

        if ($attr && $amountByMonth) {

            $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($model, $attr, $amountByMonth) {
                if (FinanceModelOtherCost::saveMonthes($model, [$attr => $amountByMonth])) {
                    return true;
                }
                $db->transaction->rollBack();
                return false;
            });

            if ($isSaved) {
                FinanceModelCalcHelper::recalcTotals($model);
                Yii::$app->session->setFlash('success', 'Прочие расходы обновлены');
            } else {
                Yii::$app->session->setFlash('success', 'Не удалось обновить Прочие расходы');
            }
        }

        return $this->redirect(Yii::$app->request->referrer ?: ['view', 'id' => $model->id]);
    }

    /**
     * @param int $id
     * @return FinanceModel
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        /* @var $employee Employee */
        $employee = Yii::$app->user->identity;
        $model = FinanceModel::find()->where(['id' => $id])
            ->andWhere(['company_id' => $employee->company->id])
            ->one();
        if ($model !== null) {
            return $model;
        }
        throw new NotFoundHttpException('Запрошенная страница не существует.');
    }

    /**
     * @param int $modelId
     * @param int $id
     * @return FinanceModelShop
     * @throws NotFoundHttpException
     */
    protected function findShop($modelId, $id)
    {
        /* @var $employee Employee */
        $employee = Yii::$app->user->identity;
        $model = FinanceModelShop::find()->where(['id' => $id])
            ->andWhere(['model_id' => $modelId])
            ->one();
        if ($model !== null) {
            return $model;
        }
        throw new NotFoundHttpException('Запрошенная страница не существует.');
    }

    /**
     * @param FinanceModel $model
     * @param Employee $employee
     * @return mixed
     * @throws \Throwable
     */
    protected function copyModel(FinanceModel $model, Employee $employee)
    {
        $copy = new FinanceModel();

        $copiedId = Yii::$app->db->transaction(function (Connection $db) use ($model, $copy, $employee)
        {
            $copy->attributes = $model->attributes;
            $copy->employee_id = $employee->id;
            $copy->company_id = $employee->company_id;
            $copy->name .= ' - Копия';
            unset($copy->id);

            if (!$copy->save()) {
                $db->transaction->rollBack();
                return false;
            }

            foreach ($model->otherCosts as $modelMonth) {
                $copyMonth = new FinanceModelOtherCost();
                $copyMonth->attributes = $modelMonth->attributes;
                $copyMonth->model_id = $copy->id;
                if (!$copyMonth->save()) {
                    $db->transaction->rollBack();
                    return false;
                }
            }

            foreach ($model->shops as $shop) {
                $copyShop = new FinanceModelShop();
                $copyShop->attributes = $shop->attributes;
                $copyShop->model_id = $copy->id;
                unset($copyShop->id);

                if (!$copyShop->save()) {
                    $db->transaction->rollBack();
                    return false;
                }

                foreach ($shop->revenueMonths as $modelMonth) {
                    $copyMonth = new FinanceModelRevenue();
                    $copyMonth->attributes = $modelMonth->attributes;
                    $copyMonth->shop_id = $copyShop->id;
                    if (!$copyMonth->save()) {
                        $db->transaction->rollBack();
                        return false;
                    }
                }

                foreach ($shop->variableCostMonths as $modelMonth) {
                    $copyMonth = new FinanceModelVariableCost();
                    $copyMonth->attributes = $modelMonth->attributes;
                    $copyMonth->shop_id = $copyShop->id;
                    if (!$copyMonth->save()) {
                        $db->transaction->rollBack();
                        return false;
                    }
                }

                foreach ($shop->fixedCostMonths as $modelMonth) {
                    $copyMonth = new FinanceModelFixedCost();
                    $copyMonth->attributes = $modelMonth->attributes;
                    $copyMonth->shop_id = $copyShop->id;
                    if (!$copyMonth->save()) {
                        $db->transaction->rollBack();
                        return false;
                    }
                }
            }

            return $copy->id;
        });

        return $copiedId;
    }
}
