<?php
namespace frontend\modules\analytics\controllers;

use backend\models\Bank;
use common\components\filters\AccessControl;
use common\models\cash\Cashbox;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyType;
use common\models\company\MenuItem;
use common\models\companyStructure\AccountingSystemType;
use common\models\companyStructure\CompanyToAcquiring;
use common\models\companyStructure\NewSalePointTypeForm;
use common\models\companyStructure\OfdType;
use common\models\companyStructure\OnlinePaymentType;
use common\models\companyStructure\SalePoint;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\product\Store;
use common\models\TimeZone;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\rbac\UserRole;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use Yii;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class ProfitController
 * @package frontend\modules\analytics\controllers
 */
class OptionsController extends FrontendController
{
    public $layout = 'options';
    private $_redirectAction = '/analytics/options/start';
    private $_allowedActions = [
        'create-online-payment-type',
        'create-accounting-system-type',
        'add-sale-point',
        'update-sale-point',
        'delete-sale-point',
        'add-store',
        'add-cashbox',
        'update-cashbox',
        'delete-cashbox',
        'add-employee',
        'send-proposal',
        'send-signup-to-webinar',
        'create-checking-accountant',
        'send-proposal-crm',
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'start'
                        ],
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::START,
                        ],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                            'instruction',
                            'levels',
                            'company',
                            'company-structure',
                            'reports',
                            'statements',
                            'notifications',
                            'referrals',
                        ],
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::INDEX,
                        ],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'create-online-payment-type',
                            'create-accounting-system-type',
                            'add-sale-point',
                            'add-store',
                            'add-cashbox',
                            'add-employee',
                            'update-sale-point',
                            'delete-sale-point',
                            'update-cashbox',
                            'delete-cashbox',
                            'send-proposal',
                            'send-proposal-crm',
                            'send-signup-to-webinar',
                            'create-checking-accountant',
                        ],
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::ADMIN,
                        ],
                    ],
                ],
            ],
        ]);
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {

            if (in_array($action->id, $this->_allowedActions)) {
                return true;
            }

            if (!$this->actionIsAllowed($action->id)) {
                $this->redirect($this->_redirectAction)->send();

                return false;
            }

            /* @var $menuItem MenuItem */
            $menuItem = Yii::$app->user->identity->menuItem;
            if (!$menuItem->analytics_item) {
                $menuItem->analytics_item = true;
                $menuItem->save(false, ['accountant_item']);
            }

            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
        ]);
    }

    /**
     * STEP 1
     * @return string
     */
    public function actionStart()
    {
        return $this->render('start', [
            'company' => Yii::$app->user->identity->company,
        ]);
    }

    /**
     * STEP 2 (old)
     * @return string
     */
    public function actionInstruction()
    {
        return $this->render('instruction', [
            'company' => Yii::$app->user->identity->company,
        ]);
    }

    /**
     * STEP 2 (new)
     * @return string
     */
    public function actionLevels()
    {
        return $this->render('levels', [
            'company' => Yii::$app->user->identity->company,
        ]);
    }

    /**
     * STEP 2
     * @return string
     */
    public function actionCompany()
    {
        CheckingAccountant::$formNameModify = false;

        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $model = $user->company;
        $taxation = $model->companyTaxationType;
        $accounts = $model->getCheckingAccountants()->orderBy([
            'type' => SORT_ASC,
            'id' => SORT_ASC,
        ])->all();
        if (empty($accounts)) {
            $accounts[] = new CheckingAccountant([
                'company_id' => $model->id,
                'type' => CheckingAccountant::TYPE_MAIN,
            ]);
        }
        if ($accounts[0]->type != CheckingAccountant::TYPE_MAIN) {
            $accounts[0]->type = CheckingAccountant::TYPE_MAIN;
        }

        $model->scenario = $model->company_type_id == CompanyType::TYPE_IP ?
            Company::SCENARIO_ANALYTICS_IP :
            Company::SCENARIO_ANALYTICS_OOO;

        // To delete accounts
        $deleteRs = Yii::$app->request->post('delete_rs');
        if (!empty($deleteRs)) {
            foreach ((array)$deleteRs as $deleteId) {
                $deleteModel = CheckingAccountant::findOne($deleteId);
                if ($deleteModel) {
                    try {
                        $deleteModel->delete();
                    } catch (\Exception $e) {
                        Yii::$app->session->setFlash('error', "Невозможно удалить р/с " . $deleteModel->rs);
                    }
                }
            }
        }

        if (Yii::$app->request->getIsPost() && Yii::$app->user->can(\frontend\rbac\permissions\BusinessAnalytics::ADMIN)) {
            $newCount = max(0, count(Yii::$app->request->post('CheckingAccountant')) - count($accounts));
            if ($newCount) {
                // Creatin new CheckingAccountant instances
                for ($i=0; $i < $newCount; $i++) {
                    $accounts[] = new CheckingAccountant([
                        'company_id' => $model->id,
                        'type' => CheckingAccountant::TYPE_ADDITIONAL,
                    ]);
                }
            }

            $isLoad = $model->load(Yii::$app->request->post());
            $isLoad = $taxation->load(Yii::$app->request->post()) && $isLoad;
            $isLoad = Model::loadMultiple($accounts, Yii::$app->request->post()) && $isLoad;

            if (Yii::$app->request->post('ajax')) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $result = array_merge(ActiveForm::validate($model, $taxation), ActiveForm::validateMultiple($accounts));

                return $result;
            }

            $ofdTypesIds = ArrayHelper::getValue(Yii::$app->request->post('Company'), 'ofdTypes');
            $acquiringAccountsIds = ArrayHelper::getValue(Yii::$app->request->post('Company'), 'acquiringAccounts');
            $onlinePaymentTypesIds = ArrayHelper::getValue(Yii::$app->request->post('Company'), 'onlinePaymentTypes');
            $accountingSystemTypesIds = ArrayHelper::getValue(Yii::$app->request->post('Company'), 'accountingSystemTypes');

            $ofdTypes = OfdType::findAll($ofdTypesIds);
            $acquiringAccounts = CheckingAccountant::findAll($acquiringAccountsIds);
            $onlinePaymentTypes = OnlinePaymentType::findAll($onlinePaymentTypesIds);
            $accountingSystemTypes = AccountingSystemType::findAll($accountingSystemTypesIds);

            if ($isLoad && Model::validateMultiple($accounts)) {
                $isSaved = \Yii::$app->db->transaction(function ($db) use ($model, $taxation, $accounts, $ofdTypes, $acquiringAccounts, $onlinePaymentTypes, $accountingSystemTypes) {

                    $model->unlinkAll('ofdTypes', true);
                    $model->unlinkAll('acquiringAccounts', true);
                    $model->unlinkAll('onlinePaymentTypes', true);
                    $model->unlinkAll('accountingSystemTypes', true);
                    foreach ($ofdTypes as $val) { $model->link('ofdTypes', $val); }
                    foreach ($acquiringAccounts as $val) { $model->link('acquiringAccounts', $val); }
                    foreach ($onlinePaymentTypes as $val) { $model->link('onlinePaymentTypes', $val); }
                    foreach ($accountingSystemTypes as $val) { $model->link('accountingSystemTypes', $val); }

                    if ($model->save() && $taxation->save()) {
                        foreach ($accounts as $account) {
                            $account->save();
                        }

                        return true;
                    }

                    if ($db->getTransaction()->isActive) {
                        $db->getTransaction()->rollBack();
                    }

                    return false;
                });

                if ($isSaved) {
                    return $this->redirect("levels");
                }
            }
        }

        return $this->render('company', [
            'model' => $model,
            'taxation' => $taxation,
            'accounts' => $accounts,
        ]);
    }

    /**
     * STEP 2.2
     * @return string
     */
    public function actionCompanyStructure()
    {
        $company = Yii::$app->user->identity->company;

        if (Yii::$app->request->post()) {
            return $this->redirect('statements');
        }

        return $this->render('company-structure', [
            'company' => $company
        ]);
    }

    /**
     * STEP 3
     * @return string
     */
    public function actionReports()
    {
        if (Yii::$app->request->post()) {
            return $this->redirect('statements');
        }

        return $this->render('reports', [
        ]);
    }

    /**
     * STEP 4
     * @return string
     */
    public function actionStatements()
    {
        if (Yii::$app->request->post()) {
            return $this->redirect('statements');
        }
        return $this->render('statements', [
        ]);
    }

    /**
     * STEP 5
     * @return string
     */
    public function actionNotifications()
    {
        return $this->render('notifications', [
        ]);
    }

    /**
     * STEP 6
     * @return string
     */
    public function actionReferrals()
    {
        return $this->render('referrals', [
        ]);
    }

    /**
     * @return boolean
     */
    private function actionIsAllowed($action)
    {
        $setFlash = ($action === 'index') ? false : true;

        /** @var Company $company */
        $company = Yii::$app->user->identity->company;

        if (!$company->analytics_module_activated && $action !== 'start') {
            $this->_redirectAction = '/analytics/options/start';

            return false;
        }

        if ($action == 'start' || $action == 'levels' || $action == 'company' || $action == 'company-structure') {
            return true;
        }

        $company->scenario = $company->company_type_id == CompanyType::TYPE_IP ?
            Company::SCENARIO_ANALYTICS_IP :
            Company::SCENARIO_ANALYTICS_OOO;

        if (!$company->validate() || $company->mainCheckingAccountant === null || !$company->companyTaxationType->validate()) {
            $this->_redirectAction = '/analytics/options/company';
            if ($setFlash) {
                Yii::$app->session->setFlash('error', "Необходимо заполнить реквизиты.");
            }
            //return false;
        }
        if ($action == 'reports') {
            return true;
        }

        if ($action == 'statements') {
            return true;
        }

        if ($action == 'referrals' || $action == 'notifications') {
            return true;
        }

        $this->_redirectAction = '/analytics/options/start';

        return false;
    }

    public function actionCreateOnlinePaymentType()
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect('company');
        }

        $company = Yii::$app->user->identity->company;
        $model = new OnlinePaymentType([
            'company_id' => $company->id,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

                return $this->renderPartial('parts_company/_modal_online_payment_type', [
                    'model' => $model,
                ]);

        }

        return $this->renderPartial('parts_company/_modal_online_payment_type', [
            'model' => $model,
        ]);
    }

    public function actionCreateAccountingSystemType()
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect('company');
        }

        $company = Yii::$app->user->identity->company;
        $model = new AccountingSystemType([
            'company_id' => $company->id,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->renderPartial('parts_company/_modal_accounting_system_type', [
                'model' => $model,
            ]);

        }

        return $this->renderPartial('parts_company/_modal_accounting_system_type', [
            'model' => $model,
        ]);
    }

    public function actionAddSalePoint()
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect('company-structure');
        }

        $isSaved = false;
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $typeId = Yii::$app->request->get('type_id', 1);
        $cashboxes = $employers = [];
        $model = new SalePoint([
            'company_id' => $company->id,
            'responsible_employee_id' => $user->id,
            'type_id' => $typeId,
            'online_payment_type_id' => $company->onlinePaymentTypes ? $company->onlinePaymentTypes[0]->id : null
        ]);

        if ($model->load(Yii::$app->request->post())) {

            $cashboxesIds = ArrayHelper::getValue(Yii::$app->request->post('SalePoint'), 'cashboxes');
            $employersIds = ArrayHelper::getValue(Yii::$app->request->post('SalePoint'), 'employers');
            $cashboxes = Cashbox::find()->where(['company_id' => $company->id, 'id' => $cashboxesIds])->all();
            $employers = Employee::find()->where(['company_id' => $company->id, 'id' => $employersIds])->all();

            if ($model->isCashboxRequired() && empty($cashboxes)) {
                $model->addError('cashboxes', 'Необходимо выбрать кассу');
            }

            if ($model->validate(null, false)) {

                $isSaved = \Yii::$app->db->transaction(function ($db) use ($model, $cashboxes, $employers) {

                    $model->unlinkAll('cashboxes', true);
                    $model->unlinkAll('employers', true);

                    if ($model->save()) {

                        $cashboxes = array_slice($cashboxes, 0, SalePoint::MAX_CASHBOXES);
                        foreach ($cashboxes as $val) { $model->link('cashboxes', $val); }
                        foreach ($employers as $val) { $model->link('employers', $val); }

                        LogHelper::log($model, LogEntityType::TYPE_SALE_POINT, LogEvent::LOG_EVENT_CREATE);

                        return true;
                    }

                    if ($db->getTransaction()->isActive) {
                        $db->getTransaction()->rollBack();
                    }

                    return false;
                });
            }
        }

        if ($isSaved) {
            return $this->redirect('company-structure');
        }

        return $this->renderAjax('parts_company_structure/_modal_sale_point', [
            'model' => $model,
            'company' => $company,
            'isSaved' => $isSaved,
            'newCashboxes' => $cashboxes,
            'newEmployers' => $employers
        ]);
    }

    public function actionAddStore()
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect('company-structure');
        }

        $company = Yii::$app->user->identity->company;
        $model = new Store([
            'company_id' => $company->id,
        ]);

        if ($model->load(Yii::$app->request->post())) {

            $model->save();
        }

        return $this->renderAjax('parts_company_structure/_modal_store', [
            'model' => $model,
            'company' => $company,
        ]);
    }

    public function actionAddCashbox()
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect('company-structure');
        }

        /** @var Company $company */
        $company = Yii::$app->user->identity->company;
        $model = new Cashbox([
            'company_id' => $company->id,
            'is_accounting' => true,
            'ofd_type_id' => $company->ofdTypes ? $company->ofdTypes[0] : null
        ]);

        if ($model->load(Yii::$app->request->post())) {

            $model->save();
        }

        return $this->renderAjax('parts_company_structure/_modal_cashbox', [
            'model' => $model,
            'company' => $company,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionAddEmployee()
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect('company-structure');
        }

        $company = Yii::$app->user->identity->company;
        $isSaved = false;

        $model = new EmployeeCompany([
            'scenario' => EmployeeCompany::SCENARIO_CREATE_SALE_POINT,
            'send_email' => false,
            'company_id' => $company->id,
            'employee_role_id' => EmployeeRole::ROLE_EMPLOYEE,
            'time_zone_id' => TimeZone::DEFAULT_TIME_ZONE,
            'is_product_admin' => false,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {

            $isSaved = true;

        }

        return $this->renderAjax('parts_company_structure/_modal_employee', [
            'model' => $model,
            'isSaved' => $isSaved
        ]);
    }

    public function actionUpdateSalePoint($id)
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect('company-structure');
        }

        $isUpdate = false;
        $isSaved = false;
        $company = Yii::$app->user->identity->company;

        $cashboxes = $employers = [];
        $model = $this->findSalePoint($id, $company->id);

        if ($model->load(Yii::$app->request->post())) {

            $isUpdate = true;
            $cashboxesIds = ArrayHelper::getValue(Yii::$app->request->post('SalePoint'), 'cashboxes');
            $employersIds = ArrayHelper::getValue(Yii::$app->request->post('SalePoint'), 'employers');
            $cashboxes = Cashbox::find()->where(['company_id' => $company->id, 'id' => $cashboxesIds])->all();
            $employers = Employee::find()->where(['company_id' => $company->id, 'id' => $employersIds])->all();

            if ($model->isCashboxRequired() && empty($cashboxes)) {
                $model->addError('cashboxes', 'Необходимо выбрать кассу');
                $isValidate = false;
            } else {
                $isValidate = $model->validate();
            }

            if ($isValidate) {

                $isSaved = \Yii::$app->db->transaction(function ($db) use ($model, $cashboxes, $employers) {

                    $model->unlinkAll('cashboxes', true);
                    $model->unlinkAll('employers', true);

                    if ($model->save()) {

                        $cashboxes = array_slice($cashboxes, 0, SalePoint::MAX_CASHBOXES);
                        foreach ($cashboxes as $val) { $model->link('cashboxes', $val); }
                        foreach ($employers as $val) { $model->link('employers', $val); }

                        LogHelper::log($model, LogEntityType::TYPE_SALE_POINT, LogEvent::LOG_EVENT_UPDATE);

                        return true;
                    }

                    if ($db->getTransaction()->isActive) {
                        $db->getTransaction()->rollBack();
                    }

                    return false;
                });
            }
        }

        if ($isSaved) {
            return $this->redirect('company-structure');
        }

        return $this->renderAjax('parts_company_structure/_modal_sale_point', [
            'model' => $model,
            'company' => $company,
            'isSaved' => $isSaved,
            'isUpdate' => $isUpdate,
            'newCashboxes' => $cashboxes,
            'newEmployers' => $employers,
        ]);
    }

    public function actionDeleteSalePoint($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $company = Yii::$app->user->identity->company;
        $model = $this->findSalePoint($id, $company->id);

        if (LogHelper::delete($model, LogEntityType::TYPE_SALE_POINT)) {
            return ['result' => true];
        }

        Yii::$app->session->setFlash('error', "Не удалось удалить отдел");

        return $this->redirect(['company-structure']);
    }

    public function actionUpdateCashbox($id)
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect('company-structure');
        }

        /** @var Company $company */
        $company = Yii::$app->user->identity->company;
        $model = $this->findCashbox($id, $company->id);

        if ($model->load(Yii::$app->request->post())) {

            $model->save();
        }

        return $this->renderAjax('parts_company_structure/_modal_cashbox', [
            'model' => $model,
            'company' => $company,
            'wasUpdated' => true
        ]);
    }

    public function actionDeleteCashbox($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $company = Yii::$app->user->identity->company;
        $model = $this->findCashbox($id, $company->id);

        if ($model->delete()) {
            return ['result' => true];
        }

        return ['result' => false];
    }

    public function actionSendProposal()
    {
        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $model = new NewSalePointTypeForm();
        $isSended = false;
        if (Yii::$app->request->method == 'POST') {

            $model->description = ArrayHelper::getValue(Yii::$app->request->post('NewSalePointTypeForm'), 'description');

            if ($model->validate()) {
                \Yii::$app->mailer->compose([
                    'html' => 'system/new-sale-point-type/html',
                    'text' => 'system/new-sale-point-type/text',
                ], [
                    'subject' => 'Предложение добавить структуру компании',
                    'form' => $model,
                    'userFIO' => $employee->getFio(),
                    'email' => $company->email,
                    'phone' => $company->phone,
                ])
                    ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
                    ->setTo(\Yii::$app->params['emailList']['support'])
                    ->setSubject('Предложение добавить структуру компании')
                    ->send();

                $isSended = true;
            }
        }

        return $this->renderAjax('parts_company_structure/_modal_proposal', [
            'model' => $model,
            'company' => $company,
            'isSended' => $isSended
        ]);
    }

    public function actionSendProposalCrm()
    {
        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $model = new NewSalePointTypeForm();
        $isSended = false;
        if (Yii::$app->request->method == 'POST') {

            $model->description = ArrayHelper::getValue(Yii::$app->request->post('NewSalePointTypeForm'), 'description');

            if ($model->validate()) {
                \Yii::$app->mailer->compose([
                    'html' => 'system/new-crm-type/html',
                    'text' => 'system/new-crm-type/text',
                ], [
                    'subject' => 'Запрос на подключение CRM',
                    'form' => $model,
                    'userFIO' => $employee->getFio(),
                    'email' => $company->email,
                    'phone' => $company->phone,
                ])
                    ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
                    ->setTo(\Yii::$app->params['emailList']['support'])
                    ->setSubject('Запрос на подключение CRM')
                    ->send();

                $isSended = true;
            }
        }

        return $this->renderAjax('levels/__modal_proposal_crm', [
            'model' => $model,
            'company' => $company,
            'isSended' => $isSended
        ]);
    }

    public function actionSendSignupToWebinar()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;
        $phone = Yii::$app->request->post('phone');
        $isSended = false;

        if ($phone) {

            \Yii::$app->mailer->compose([
                'html' => 'system/signup-to-webinar/html',
                'text' => 'system/signup-to-webinar/text',
            ], [
                'subject' => 'Запись на вебинар "Начало работы с КУБ.ФинДиректор"',
                'phone' => $phone,
            ])
                ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
                ->setTo(\Yii::$app->params['emailList']['support'])
                ->setSubject('Запись на вебинар "Начало работы с КУБ.ФинДиректор"')
                ->send();

            $isSended = true;

            Yii::$app->response->cookies->add(new \yii\web\Cookie([
                'name' => 'signup-to-webinar',
                'value' => '1',
                'expire' => time() + 3600 * 24
            ]));
        }

        return ['result' => $isSended];
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     * @throws \Exception
     */
    public function actionCreateCheckingAccountant()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            /* @var $company Company */
            $company = Yii::$app->user->identity->company;
            /* @var $model CheckingAccountant */
            $model = new CheckingAccountant([
                'company_id' => $company->id,
            ]);
            if (Yii::$app->request->post('ajax')) {
                return $this->ajaxValidate($model);
            }
            if ($model->load(Yii::$app->request->post()) && $model->save()) {

                $canIntegration = $bankingAlias = null;

                $bankArray = Bank::find()->where([
                    'is_blocked' => 0,
                    'bik' => Banking::bikList(),
                ])->indexBy('bik')->all();

                foreach (Banking::$modelClassArray as $banking) {
                    $bank = \common\components\helpers\ArrayHelper::getValue($bankArray, $banking::BIK);
                    $userHasBik = in_array($model->bik, $banking::$bikList);
                    if (!$bank) continue;

                    if ($userHasBik) {
                        $canIntegration = true;
                        $bankingAlias = $banking::ALIAS;
                        break;
                    }
                }

                return [
                    'result' => true,
                    'account_id' => $model->primaryKey,
                    'can_integration' => $canIntegration,
                    'banking_alias' => $bankingAlias,
                    'upload_statement_href' => Url::to([
                        '/cash/banking/default/index',
                        'p' => Banking::routeEncode(['/analytics/options/levels', 'rs' => $model->rs]),
                    ])
                ];
            }

            return [
                'result' => false,
                'html' => $this->renderAjax('@frontend/views/company/form/modal_rs/_create', [
                    'checkingAccountant' => $model,
                ]),
            ];
        } else {
            throw new BadRequestHttpException();
        }
    }


    /**
     * @param $id
     * @return array|\yii\db\ActiveRecord|null
     * @throws NotFoundHttpException
     */
    protected function findSalePoint($id, $company_id)
    {
        $model = SalePoint::find()
            ->andWhere([SalePoint::tableName() . '.id' => $id,])
            ->andWhere(['company_id' => $company_id])
            ->one();
        if ($model !== null) {
            return $model;
        }
        throw new NotFoundHttpException('Отдел не найден');
    }

    /**
     * @param $id
     * @return array|\yii\db\ActiveRecord|null
     * @throws NotFoundHttpException
     */
    protected function findCashbox($id, $company_id)
    {
        $model = Cashbox::find()
            ->andWhere([Cashbox::tableName() . '.id' => $id,])
            ->andWhere(['company_id' => $company_id])
            ->one();
        if ($model !== null) {
            return $model;
        }
        throw new NotFoundHttpException('Касса не найдена');
    }
}
