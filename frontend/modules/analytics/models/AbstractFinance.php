<?php
namespace frontend\modules\analytics\models;

use common\components\date\DateHelper;
use common\components\excel\Excel;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\document\OrderAct;
use common\models\document\OrderPackingList;
use common\models\document\OrderUpd;
use common\models\document\PackingList;
use common\models\document\Upd;
use common\models\ExpenseItemFlowOfFunds;
use common\models\IncomeItemFlowOfFunds;
use common\modules\acquiring\models\AcquiringOperation;
use common\modules\cards\models\CardOperation;
use frontend\modules\cash\models\CashContractorType;
use yii\base\InvalidParamException;
use yii\base\Model;
use common\models\Company;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class AbstractFinance
 * @package frontend\modules\analytics\models
 *
 * @property Company $company
 * @property array $multiCompanyIds
 * @property bool $isCurrentYear
 */
abstract class AbstractFinance extends Model
{
    const TABLE_OLAP_DOCS = 'olap_documents';
    const TABLE_OLAP_FLOWS = 'olap_flows';
    const TABLE_OLAP_INVOICES = 'olap_invoices';
    const TABLE_PRODUCT_TURNOVER = 'product_turnover';

    const JANUARY = '01';
    const FEBRUARY = '02';
    const MARCH = '03';
    const APRIL = '04';
    const MAY = '05';
    const JUNE = '06';
    const JULY = '07';
    const AUGUST = '08';
    const SEPTEMBER = '09';
    const OCTOBER = '10';
    const NOVEMBER = '11';
    const DECEMBER = '12';

    const RECEIPT_FINANCING_TYPE_FIRST = 1;
    const RECEIPT_FINANCING_TYPE_SECOND = 2;
    const INCOME_OPERATING_ACTIVITIES = 3;
    const WITHOUT_TYPE = 0;
    const INCOME_INVESTMENT_ACTIVITIES = 4;
    const EXPENSE_INVESTMENT_ACTIVITIES = 5;
    const OWN_FUNDS_INCOME = 6;
    const OWN_FUNDS_EXPENSE = 7;

    const INCOME_CASH_BANK = 1;
    const EXPENSE_CASH_BANK = 2;
    const INCOME_CASH_ORDER = 3;
    const EXPENSE_CASH_ORDER = 4;
    const INCOME_CASH_EMONEY = 5;
    const EXPENSE_CASH_EMONEY = 6;
    const INCOME_ACQUIRING = 7;
    const EXPENSE_ACQUIRING = 8;
    const INCOME_CARD = 9;
    const EXPENSE_CARD = 10;
    const INCOME_CASH_FOREIGN_BANK = 21;
    const EXPENSE_CASH_FOREIGN_BANK = 22;
    const INCOME_CASH_FOREIGN_ORDER = 23;
    const EXPENSE_CASH_FOREIGN_ORDER = 24;
    const INCOME_CASH_FOREIGN_EMONEY = 25;
    const EXPENSE_CASH_FOREIGN_EMONEY = 26;

    const FINANCIAL_OPERATIONS_BLOCK = 1;
    const OPERATING_ACTIVITIES_BLOCK = 2;
    const INVESTMENT_ACTIVITIES_BLOCK = 3;
    const OWN_FUNDS_BLOCK = 4;

    const CASH_BANK_BLOCK = 1;
    const CASH_ORDER_BLOCK = 2;
    const CASH_EMONEY_BLOCK = 3;
    const CASH_ACQUIRING_BLOCK = 4;
    const CASH_CARD_BLOCK = 5;
    const CASH_FOREIGN_BANK_BLOCK = 11;
    const CASH_FOREIGN_ORDER_BLOCK = 12;
    const CASH_FOREIGN_EMONEY_BLOCK = 13;

    const GROUP_DATE = 1;
    const GROUP_CONTRACTOR = 2;
    const GROUP_PAYMENT_TYPE = 3;

    const TYPE_BY_ACTIVITY = 1;
    const TYPE_BY_PURSE = 2;

    const ACT_BLOCK = 1;
    const PACKING_LIST_BLOCK = 2;
    const UPD_BLOCK = 3;
    const GOODS_CANCELLATION_BLOCK = 4;

    const MOVEMENT_TYPE_FLOWS = 'flows';
    const MOVEMENT_TYPE_DOCS = 'docs';
    const MOVEMENT_TYPE_DOCS_AND_FLOWS = 'docs_flows';

    /**
     * @var array
     */
    public static $_DEFAULT_PREPAYMENT_ITEMS = [
        CashFlowsBase::FLOW_TYPE_INCOME => [1,14],
        CashFlowsBase::FLOW_TYPE_EXPENSE => [1,2,6,7,8,9,10,11,12,13,19,22,23,24,25,26,27,30,31,32,33,34,35,36,40,41,42,43,52,54,55,56,57,58,59,60,63,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,87,88]
    ];

    /**
     * @var array
     */
    public static $month = [
        self::JANUARY => 'Январь',
        self::FEBRUARY => 'Февраль',
        self::MARCH => 'Март',
        self::APRIL => 'Апрель',
        self::MAY => 'Май',
        self::JUNE => 'Июнь',
        self::JULY => 'Июль',
        self::AUGUST => 'Август',
        self::SEPTEMBER => 'Сентябрь',
        self::OCTOBER => 'Октябрь',
        self::NOVEMBER => 'Ноябрь',
        self::DECEMBER => 'Декабрь',
    ];

    /**
     * @var array
     */
    public static $monthIn = [
        self::JANUARY => 'январе',
        self::FEBRUARY => 'феврале',
        self::MARCH => 'марте',
        self::APRIL => 'апреле',
        self::MAY => 'мае',
        self::JUNE => 'июне',
        self::JULY => 'июле',
        self::AUGUST => 'августе',
        self::SEPTEMBER => 'сентябре',
        self::OCTOBER => 'октябре',
        self::NOVEMBER => 'ноябре',
        self::DECEMBER => 'декабре',
    ];

    /**
     * @var array
     */
    public static $monthEnd = [
        self::JANUARY => 'января',
        self::FEBRUARY => 'февраля',
        self::MARCH => 'марта',
        self::APRIL => 'апреля',
        self::MAY => 'мая',
        self::JUNE => 'июня',
        self::JULY => 'июля',
        self::AUGUST => 'августа',
        self::SEPTEMBER => 'сентября',
        self::OCTOBER => 'октября',
        self::NOVEMBER => 'ноября',
        self::DECEMBER => 'декабря',
    ];

    /**
     * @var array
     */
    public static $blocks = [
        self::FINANCIAL_OPERATIONS_BLOCK => 'Финансовая деятельность',
        self::OPERATING_ACTIVITIES_BLOCK => 'Операционная деятельность',
        self::INVESTMENT_ACTIVITIES_BLOCK => 'Инвестиционная деятельность',
    ];

    /**
     * @var array
     */
    public static $purseBlocks = [
        self::CASH_BANK_BLOCK => 'Банк',
        self::CASH_ORDER_BLOCK => 'Касса',
        self::CASH_EMONEY_BLOCK => 'E-money',
        self::CASH_ACQUIRING_BLOCK => 'Интернет-эквайринг',
        self::CASH_CARD_BLOCK => 'Карты',
    ];

    /**
     * @var array
     */
    public static $purseBlocksClass = [
        self::CASH_BANK_BLOCK => CashBankFlows::class,
        self::CASH_ORDER_BLOCK => CashOrderFlows::class,
        self::CASH_EMONEY_BLOCK => CashEmoneyFlows::class
    ];

    public static $purseTypesClass = [
        self::INCOME_CASH_BANK => CashBankFlows::class,
        self::EXPENSE_CASH_BANK => CashBankFlows::class,
        self::INCOME_CASH_ORDER => CashOrderFlows::class,
        self::EXPENSE_CASH_ORDER => CashOrderFlows::class,
        self::INCOME_CASH_EMONEY => CashEmoneyFlows::class,
        self::EXPENSE_CASH_EMONEY => CashEmoneyFlows::class
    ];

    /**
     * @var array
     */
    public static $types = [
        self::INCOME_OPERATING_ACTIVITIES => 'Приход',
        self::WITHOUT_TYPE => 'Расход',
        self::RECEIPT_FINANCING_TYPE_FIRST => 'Приход',
        self::RECEIPT_FINANCING_TYPE_SECOND => 'Расход',
        self::INCOME_INVESTMENT_ACTIVITIES => 'Приход',
        self::EXPENSE_INVESTMENT_ACTIVITIES => 'Расход',
    ];

    /**
     * @var array
     */
    public static $purseTypes = [
        self::INCOME_CASH_BANK => 'Приход',
        self::EXPENSE_CASH_BANK => 'Расход',
        self::INCOME_CASH_ORDER => 'Приход',
        self::EXPENSE_CASH_ORDER => 'Расход',
        self::INCOME_CASH_EMONEY => 'Приход',
        self::EXPENSE_CASH_EMONEY => 'Расход',
        self::INCOME_ACQUIRING => 'Приход',
        self::EXPENSE_ACQUIRING => 'Расход',
        self::INCOME_CARD => 'Приход',
        self::EXPENSE_CARD => 'Расход',
    ];

    /**
     * @var array
     */
    public static $purseGrowingBalanceLabelByType = [
        self::EXPENSE_CASH_BANK => 'Остаток по банку',
        self::EXPENSE_CASH_ORDER => 'Остаток по кассе',
        self::EXPENSE_CASH_EMONEY => 'Остаток по E-money',
    ];

    /**
     * @var array
     */
    public static $growingBalanceLabelByType = [
        self::RECEIPT_FINANCING_TYPE_SECOND => 'Остаток по фин. деятельности',
        self::WITHOUT_TYPE => 'Остаток по оп. деятельности',
        self::EXPENSE_INVESTMENT_ACTIVITIES => 'Остаток по инвест. деятельности',
    ];

    /**
     * @var array
     */
    public static $blockByType = [
        self::RECEIPT_FINANCING_TYPE_FIRST => self::FINANCIAL_OPERATIONS_BLOCK,
        self::RECEIPT_FINANCING_TYPE_SECOND => self::FINANCIAL_OPERATIONS_BLOCK,
        self::INCOME_OPERATING_ACTIVITIES => self::OPERATING_ACTIVITIES_BLOCK,
        self::WITHOUT_TYPE => self::OPERATING_ACTIVITIES_BLOCK,
        self::INCOME_INVESTMENT_ACTIVITIES => self::INVESTMENT_ACTIVITIES_BLOCK,
        self::EXPENSE_INVESTMENT_ACTIVITIES => self::INVESTMENT_ACTIVITIES_BLOCK,
        self::OWN_FUNDS_INCOME => self::OWN_FUNDS_BLOCK,
        self::OWN_FUNDS_EXPENSE => self::OWN_FUNDS_BLOCK
    ];

    /**
     * @var array
     */
    public static $purseBlockByType = [
        self::INCOME_CASH_BANK => self::CASH_BANK_BLOCK,
        self::EXPENSE_CASH_BANK => self::CASH_BANK_BLOCK,
        self::INCOME_CASH_ORDER => self::CASH_ORDER_BLOCK,
        self::EXPENSE_CASH_ORDER => self::CASH_ORDER_BLOCK,
        self::INCOME_CASH_EMONEY => self::CASH_EMONEY_BLOCK,
        self::EXPENSE_CASH_EMONEY => self::CASH_EMONEY_BLOCK,
        self::INCOME_ACQUIRING => self::CASH_ACQUIRING_BLOCK,
        self::EXPENSE_ACQUIRING => self::CASH_ACQUIRING_BLOCK,
        self::INCOME_CARD => self::CASH_CARD_BLOCK,
        self::EXPENSE_CARD => self::CASH_CARD_BLOCK,
    ];

    /**
     * @var array
     */
    public static $flowTypeByPurseType = [
        self::INCOME_CASH_BANK => CashFlowsBase::FLOW_TYPE_INCOME,
        self::INCOME_CASH_ORDER => CashFlowsBase::FLOW_TYPE_INCOME,
        self::INCOME_CASH_EMONEY => CashFlowsBase::FLOW_TYPE_INCOME,
        self::INCOME_ACQUIRING => CashFlowsBase::FLOW_TYPE_INCOME,
        self::INCOME_CARD => CashFlowsBase::FLOW_TYPE_INCOME,
        self::EXPENSE_CASH_BANK => CashFlowsBase::FLOW_TYPE_EXPENSE,
        self::EXPENSE_CASH_ORDER => CashFlowsBase::FLOW_TYPE_EXPENSE,
        self::EXPENSE_CASH_EMONEY => CashFlowsBase::FLOW_TYPE_EXPENSE,
        self::EXPENSE_ACQUIRING => CashFlowsBase::FLOW_TYPE_EXPENSE,
        self::EXPENSE_CARD => CashFlowsBase::FLOW_TYPE_EXPENSE,
    ];

    /**
     * @var array
     */
    public static $flowTypeByActivityType = [
        self::RECEIPT_FINANCING_TYPE_FIRST => CashFlowsBase::FLOW_TYPE_INCOME,
        self::INCOME_OPERATING_ACTIVITIES => CashFlowsBase::FLOW_TYPE_INCOME,
        self::INCOME_INVESTMENT_ACTIVITIES => CashFlowsBase::FLOW_TYPE_INCOME,
        self::RECEIPT_FINANCING_TYPE_SECOND => CashFlowsBase::FLOW_TYPE_EXPENSE,
        self::WITHOUT_TYPE => CashFlowsBase::FLOW_TYPE_EXPENSE,
        self::EXPENSE_INVESTMENT_ACTIVITIES => CashFlowsBase::FLOW_TYPE_EXPENSE,
    ];

    /**
     * @var array
     */
    public static $cashContractorTypeByTableName = [
        CashBankFlows::class => [
            CashContractorType::ORDER_TEXT,
            CashContractorType::EMONEY_TEXT,
        ],
        CashOrderFlows::class => [
            CashContractorType::BANK_TEXT,
            CashContractorType::EMONEY_TEXT,
        ],
        CashEmoneyFlows::class => [
            CashContractorType::ORDER_TEXT,
            CashContractorType::BANK_TEXT,
        ],
        AcquiringOperation::class => [],
        CardOperation::class => [],
    ];

    /**
     * @var array
     */
    public static $cashContractorTextByType = [
        CashFlowsBase::FLOW_TYPE_INCOME => [
            CashContractorType::BANK_TEXT => 'Банка',
            CashContractorType::ORDER_TEXT => 'Кассы',
            CashContractorType::EMONEY_TEXT => 'E-money',
        ],
        CashFlowsBase::FLOW_TYPE_EXPENSE => [
            CashContractorType::BANK_TEXT => 'Банк',
            CashContractorType::ORDER_TEXT => 'Кассу',
            CashContractorType::EMONEY_TEXT => 'E-money',
        ],
    ];

    /**
     * @var array
     */
    public static $typeItem = [
        AbstractFinance::INCOME_OPERATING_ACTIVITIES => [],
        AbstractFinance::WITHOUT_TYPE => [],
        AbstractFinance::RECEIPT_FINANCING_TYPE_FIRST => [
            InvoiceIncomeItem::ITEM_LOAN,
            InvoiceIncomeItem::ITEM_CREDIT,
            InvoiceIncomeItem::ITEM_FROM_FOUNDER,
            InvoiceIncomeItem::ITEM_BORROWING_REDEMPTION,
            InvoiceIncomeItem::ITEM_RECEIVED_PERCENTS,
            InvoiceIncomeItem::ITEM_RECEIVED_PERCENTS_BY_DEPOSITS,
            InvoiceIncomeItem::ITEM_INPUT_MONEY,
            InvoiceIncomeItem::ITEM_BORROWING_DEPOSIT,
        ],
        AbstractFinance::RECEIPT_FINANCING_TYPE_SECOND => [
            InvoiceExpenditureItem::ITEM_LOAN_REPAYMENT,
            InvoiceExpenditureItem::ITEM_REPAYMENT_CREDIT,
            InvoiceExpenditureItem::ITEM_PAYMENT_PERCENT,
            InvoiceExpenditureItem::ITEM_RETURN_FOUNDER,
            InvoiceExpenditureItem::ITEM_BORROWING_EXTRADITION,
            InvoiceExpenditureItem::ITEM_DIVIDEND_PAYMENT,
            InvoiceExpenditureItem::ITEM_OUTPUT_MONEY,
            InvoiceExpenditureItem::ITEM_OUTPUT_PROFIT,
            InvoiceExpenditureItem::ITEM_DEPOSIT,
        ],
        AbstractFinance::INCOME_INVESTMENT_ACTIVITIES => [],
        AbstractFinance::EXPENSE_INVESTMENT_ACTIVITIES => [
            InvoiceExpenditureItem::ITEM_BALANCE_ARTICLE_FIXED,
            InvoiceExpenditureItem::ITEM_BALANCE_ARTICLE_INTANGIBLE,
        ],
    ];

    public static $typesByBlock = [
        self::FINANCIAL_OPERATIONS_BLOCK => [
            self::RECEIPT_FINANCING_TYPE_FIRST,
            self::RECEIPT_FINANCING_TYPE_SECOND
        ],
        self::OPERATING_ACTIVITIES_BLOCK => [
            self::INCOME_OPERATING_ACTIVITIES,
            self::WITHOUT_TYPE
        ],
        self::INVESTMENT_ACTIVITIES_BLOCK => [
            self::INCOME_INVESTMENT_ACTIVITIES,
            self::EXPENSE_INVESTMENT_ACTIVITIES
        ],
        self::OWN_FUNDS_BLOCK => [
            self::OWN_FUNDS_INCOME,
            self::OWN_FUNDS_EXPENSE
        ],

    ];

    public static $docOrdersTables = [
        'acts' => [
            'class' => OrderAct::class,
            'name' => 'order_act',
            'key' => 'act_id'
        ],
        'packingLists' => [
            'class' => OrderPackingList::class,
            'name' => 'order_packing_list',
            'key' => 'packing_list_id'
        ],
        'upds' => [
            'class' => OrderUpd::class,
            'name' => 'order_upd',
            'key' => 'upd_id'
        ]
    ];

    /**
     * @var int
     */
    public $year = null;

    /**
     * @var
     */
    public $expenditure_item_id;

    /**
     * @var
     */
    public $income_item_id;

    /**
     * @var
     */
    public $cash_contractor;

    /**
     * @var Company
     */
    protected $_company = null;

    /**
     * @var AnalyticsMultiCompanyManager
     */
    public $multiCompanyManager;

    /**
     * @var array
     */
    protected $_multiCompanyIds = null;

    /**
     * @var ActiveQuery
     */
    protected $_filterQuery;

    /**
     * @var
     */
    private static $_yearFilter;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['year',], 'integer'],
        ];
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->year = min(
            \Yii::$app->session->get('modules.reports.finance.year', date('Y')),
            date('Y'));

        // old
        $this->_company = \Yii::$app->multiCompanyManager->getPrimaryCompany();

        // new
        $this->multiCompanyManager = \Yii::$app->multiCompanyManager;
        $this->_multiCompanyIds = $this->multiCompanyManager->getCompaniesIds();
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @return array
     */
    public function getMultiCompanyIds()
    {
        return $this->_multiCompanyIds;
    }

    /**
     * @return bool
     */
    public function disableMultiCompanyMode()
    {
        if (Yii::$app->user && Yii::$app->user->identity && Yii::$app->user->identity->company) {
            $this->_multiCompanyIds = [Yii::$app->user->identity->company->id];
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function getIsCurrentYear()
    {
        return date('Y') == $this->year;
    }

    /**
     * @return array
     */
    public static function flowClassArray()
    {
        return [
            CashBankFlows::class,
            CashOrderFlows::class,
            CashEmoneyFlows::class,
            AcquiringOperation::class,
            CardOperation::class
        ];
    }

    /**
     * @return array
     */
    public static function docClassArray()
    {
        return [
            'acts' => Act::class,
            'packingLists' => PackingList::class,
            'upds' => Upd::class,
        ];
    }

    /**
     *
     */
    public function checkCashContractor()
    {
        foreach ([CashContractorType::BANK_TEXT, CashContractorType::ORDER_TEXT, CashContractorType::EMONEY_TEXT] as $cashContractor) {
            if (isset($this->income_item_id[$cashContractor])) {
                $this->cash_contractor[$cashContractor] = $cashContractor;
            }
            if (isset($this->expenditure_item_id[$cashContractor])) {
                $this->cash_contractor[$cashContractor] = $cashContractor;
            }
        }
    }



    public static function getDaysInMonth($year, $month)
    {
        if (strlen($month) == 1) { $month = '0'.$month; }
        $start = (new \DateTime())->createFromFormat('Y-m-d', $year.'-'.$month.'-'.'01');
        $curr = $start;
        $endDate = $start->format('mt');
        $ret = [];
        $err = 1;
        while(true) {
            $date = $curr->format('md');
            $ret[$date] = $curr->format('d');
            $curr = $curr->modify("+1 day");

            if ($date >= $endDate)
                break;

            if (++$err > 31) {
                echo 'getDaysInMonth: error';
                exit;
            }
        }

        return $ret;
    }

    protected function _getNotAccountingContractors()
    {
        return Contractor::find()
            ->where([
                'company_id' => $this->multiCompanyIds,
            ])
            ->select('id')
            ->column();
    }

    /**
     * @param $flowType
     * @param $companyId
     * @return array
     */
    public static function getArticles2Parents($flowType, $companyId)
    {
        if ($flowType == CashFlowsBase::FLOW_TYPE_INCOME)
            return InvoiceIncomeItem::find()
                ->select(['parent_id', 'id'])
                ->where(['company_id' => $companyId])
                ->andWhere(['not', ['parent_id' => null]])
                ->indexBy('id')
                ->column();

        if ($flowType == CashFlowsBase::FLOW_TYPE_EXPENSE)
            return InvoiceExpenditureItem::find()
                ->select(['parent_id', 'id'])
                ->where(['company_id' => $companyId])
                ->andWhere(['not', ['parent_id' => null]])
                ->indexBy('id')
                ->column();

        return [];
    }

    /**
     * @param $itemsIds
     * @param $flowType
     * @param $companyId
     * @return array
     */
    public static function getArticlesWithChildren($itemsIds, $flowType, $companyId)
    {
        if ($flowType == CashFlowsBase::FLOW_TYPE_INCOME)
            foreach ((array)$itemsIds as $itemId) {
                $children = InvoiceIncomeItem::find()
                    ->select('id')
                    ->where(['company_id' => $companyId])
                    ->andWhere(['parent_id' => $itemId])
                    ->column();

                if ($children)
                    $itemsIds = array_merge(array_values((array)$itemsIds), $children);
            }

        if ($flowType == CashFlowsBase::FLOW_TYPE_EXPENSE)
            foreach ((array)$itemsIds as $itemId) {
                $children = InvoiceExpenditureItem::find()
                    ->select('id')
                    ->where(['company_id' => $companyId])
                    ->andWhere(['parent_id' => $itemId])
                    ->column();

                if ($children)
                    $itemsIds = array_merge(array_values((array)$itemsIds), $children);
            }

        return $itemsIds;
    }

    /**
     * @return array
     */
    public static function getYearFilterByCompany(Company $company):array
    {
        if (empty(self::$_yearFilter)) {
            $range = [];
            $cashOrderMinDate = CashOrderFlows::find()
                ->byCompany($company->id)
                ->min('date');
            $cashBankMinDate = CashBankFlows::find()
                ->byCompany($company->id)
                ->min('date');
            $cashEmoneyMinDate = CashEmoneyFlows::find()
                ->byCompany($company->id)
                ->min('date');
            $minDates = [];

            if ($cashOrderMinDate) $minDates[] = $cashOrderMinDate;
            if ($cashBankMinDate) $minDates[] = $cashBankMinDate;
            if ($cashEmoneyMinDate) $minDates[] = $cashEmoneyMinDate;

            $minCashDate = !empty($minDates) ? max(2000, min($minDates)) : date(DateHelper::FORMAT_DATE);
            $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
            $currentYear = date('Y');
            foreach (range($registrationYear, $currentYear) as $value) {
                $range[$value] = $value;
            }
            arsort($range);
            self::$_yearFilter = $range;
        }

        return self::$_yearFilter;
    }
}