<?php

namespace frontend\modules\analytics\models\paymentsRequest;

use common\components\TextHelper;
use frontend\models\log\ILogMessage;
use Yii;
use common\models\Company;
use common\models\employee\Employee;

/**
 * This is the model class for table "payments_request_history".
 *
 * @property int $id
 * @property int $created_at
 * @property int $author_id
 * @property int $company_id
 * @property int $request_id
 * @property int|null $event_id
 * @property string|null $message
 *
 * @property Employee $author
 * @property Company $company
 * @property Company $request
 */
class PaymentsRequestHistory extends \yii\db\ActiveRecord
{
    const EVENT_CREATE = 1;
    const EVENT_ADD_ORDER = 20;
    const EVENT_DELETE_ORDER = 21;
    const EVENT_SIGN_STEP = 30;
    const EVENT_UNSIGN_STEP = 31;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments_request_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'author_id', 'company_id', 'request_id'], 'required'],
            [['created_at', 'author_id', 'company_id', 'request_id', 'event_id'], 'integer'],
            [['message'], 'string', 'max' => 2048],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::class, 'targetAttribute' => ['author_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentsRequest::class, 'targetAttribute' => ['request_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'author_id' => 'Author ID',
            'company_id' => 'Company ID',
            'event_id' => 'Event ID',
            'message' => 'Message',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Employee::class, ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(PaymentsRequest::class, ['id' => 'request_id']);
    }

    // MESSAGES

    public static function createRequest(PaymentsRequest $request)
    {
        $company = $request->company;
        $author = self::getHistoryAuthor($request);
        $history = self::getNewModel($company, $author);
        $history->event_id = self::EVENT_CREATE;
        $history->message = sprintf('Создана');

        if ($request->save()) {
            $history->request_id = $request->primaryKey;
            return $history->save();
        }

        return false;
    }

    public static function addOrder(PaymentsRequestOrder $order)
    {
        $company = $order->paymentsRequest->company;
        $author = self::getHistoryAuthor($order->paymentsRequest);
        $history = self::getNewModel($company, $author);
        $history->request_id = $order->paymentsRequest->id;
        $history->event_id = self::EVENT_ADD_ORDER;
        $history->message = sprintf('Платеж на сумму %s добавлен', TextHelper::invoiceMoneyFormat($order->planCashFlow->amount, 2));

        if ($history->save()) {
            return (bool)$order->save();
        }

        return false;
    }

    public static function deleteOrder(PaymentsRequestOrder $order)
    {
        $company = $order->paymentsRequest->company;
        $author = self::getHistoryAuthor($order->paymentsRequest);
        $history = self::getNewModel($company, $author);
        $history->request_id = $order->paymentsRequest->id;
        $history->event_id = self::EVENT_DELETE_ORDER;
        $history->message = sprintf('Платеж на сумму %s удален', TextHelper::invoiceMoneyFormat($order->planCashFlow->amount, 2));

        if ($history->save()) {
            return (bool)$order->delete();
        }

        return false;
    }

    public static function signStep(PaymentsRequestSignStep $signStep)
    {
        $company = $signStep->request->company;
        $author = self::getHistoryAuthor($signStep->request);
        $history = self::getNewModel($company, $author);
        $history->request_id = $signStep->request_id;
        $history->event_id = self::EVENT_SIGN_STEP;
        $history->message = sprintf('Шаг №%s подписан', $signStep->step);
        if ($signStep->can_sign && $history->save()) {
            return $signStep->sign();
        }

        return false;
    }

    public static function unsignStep(PaymentsRequestSignStep $signStep)
    {
        $company = $signStep->request->company;
        $author = self::getHistoryAuthor($signStep->request);
        $history = self::getNewModel($company, $author);
        $history->request_id = $signStep->request_id;
        $history->event_id = self::EVENT_UNSIGN_STEP;
        $history->message = sprintf('Шаг №%s подпись снята', $signStep->step);
        if ($signStep->can_unsign && $history->save()) {
            return $signStep->unsign();
        }

        return false;
    }

    public static function removeSignStepsHistory(PaymentsRequest $request)
    {
        self::deleteAll([
            'request_id' => $request->id,
            'event_id' => [self::EVENT_SIGN_STEP, self::EVENT_UNSIGN_STEP]
        ]);
    }

    private static function getNewModel(Company $company, Employee $author)
    {
        return new self([
            'company_id' => $company->id,
            'author_id' => $author->id,
            'created_at' => time(),
        ]);
    }

    private static function getHistoryAuthor(ILogMessage $model)
    {
        if ($model->isNewRecord) {
            return $model->author;
        } elseif (Yii::$app->id == 'app-frontend' && $model->company->getEmployeeCompanies()->andWhere(['employee_id' => Yii::$app->user->id])->exists()) {
            return Yii::$app->user->identity;
        } else {
            return $model->company->employeeChief;
        }
    }
}
