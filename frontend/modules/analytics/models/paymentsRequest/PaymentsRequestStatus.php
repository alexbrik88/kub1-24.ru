<?php

namespace frontend\modules\analytics\models\paymentsRequest;

use Yii;

/**
 * This is the model class for table "payments_request_status".
 *
 * @property int $id
 * @property string $name Название
 *
 * @property PaymentsRequest[] $paymentsRequests
 */
class PaymentsRequestStatus extends \yii\db\ActiveRecord
{
    const STATUS_APPROVAL = 1;
    const STATUS_SIGNING = 2;
    const STATUS_REJECTED = 3;
    const STATUS_SIGNED = 4;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments_request_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    /**
     * Gets query for [[PaymentsRequests]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentsRequests()
    {
        return $this->hasMany(PaymentsRequest::class, ['status_id' => 'id']);
    }
}
