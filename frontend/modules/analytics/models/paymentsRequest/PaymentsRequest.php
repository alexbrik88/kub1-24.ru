<?php

namespace frontend\modules\analytics\models\paymentsRequest;

use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\components\TextHelper;
use common\models\document\AbstractDocument;
use common\models\document\Invoice;
use common\models\document\status\PackingListStatus;
use frontend\models\Documents;
use frontend\models\log\ILogMessage;
use frontend\models\log\Log;
use frontend\models\log\LogEvent;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestSignStep as SignStep;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\modules\crm\behaviors\PotentialClientBehavior;
use frontend\modules\telegram\behaviors\NotificationBehavior;
use Yii;
use common\models\Company;
use common\models\employee\Employee;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "payments_request".
 *
 * @property int $id
 * @property string $document_date
 * @property string $payment_date
 * @property string $document_number
 * @property string|null $document_additional_number
 * @property int $company_id
 * @property int $author_id
 * @property int $status_id
 * @property int $sign_template_id
 * @property string $payment_source
 *
 * @property string $fullNumber
 *
 * @property Company $company
 * @property Employee $author
 * @property PaymentsRequestStatus $status
 * @property PaymentsRequestOrder[] $orders
 * @property PlanCashFlows[] $planFlows
 * @property PaymentsRequestChat $chat
 * @property PaymentsRequestChatUser[] $chatUsers
 * @property PaymentsRequestSignTemplate $signTemplate
 * @property PaymentsRequestSignStep[] $signSteps
 */
class PaymentsRequest extends \yii\db\ActiveRecord implements ILogMessage
{
    const PAYMENT_SOURCE_BANK = 'bank';
    const PAYMENT_SOURCE_CASHBOX = 'cashbox';
    const PAYMENT_SOURCE_EMONEY = 'emoney';

    public $isOrdersExists;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments_request';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'author_id', 'status_id', 'sign_template_id', 'document_number', 'document_date', 'payment_date', 'payment_source'], 'required'],
            [['company_id', 'author_id', 'status_id', 'sign_template_id'], 'integer'],
            [['document_number', 'document_additional_number'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::class, 'targetAttribute' => ['author_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentsRequestStatus::class, 'targetAttribute' => ['status_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['sign_template_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentsRequestSignTemplate::class, 'targetAttribute' => ['sign_template_id' => 'id']],
            [['document_date', 'payment_date', 'payment_source', 'isOrdersExists'], 'safe'],
            [['document_number'], 'unique', 'targetAttribute' => ['company_id', 'document_number', 'document_additional_number'],
                'filter' => function ($query) {
                    $query->andWhere('YEAR(`document_date`) = :year', [':year' => date('Y', strtotime($this->document_date))]);
                    return $query;
                },
                'message' => 'Заявка с таким номером уже существует',
            ],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => DatePickerFormatBehavior::class,
                'attributes' => [
                    'document_date' => [
                        'message' => 'Дата заявки указана неверно.',
                    ],
                    'payment_date' => [
                        'message' => 'Дата платежа указана неверно.',
                    ],
                ],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'document_date' => 'Дата заявки',
            'document_number' => 'Номер заявки',
            'document_additional_number' => 'Дополнительный номер заявки',
            'author_id' => 'Инициатор',
            'status_id' => 'Статус',
            'sign_template_id' => 'Шаблон подписания',
            'payment_date' => 'Дата платежа',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Employee::class, ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(PaymentsRequestStatus::class, ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(PaymentsRequestOrder::class, ['payments_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanFlows()
    {
        return $this->hasMany(PlanCashFlows::class, ['id' => 'plan_operation_id'])
            ->via('orders');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSignTemplate()
    {
        return $this->hasOne(PaymentsRequestSignTemplate::class, ['id' => 'sign_template_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSignSteps()
    {
        return $this->hasMany(PaymentsRequestSignStep::class, ['request_id' => 'id'])
            ->orderBy(['step' => SORT_ASC]);
    }

    /**
     * @inheritdoc
     */
    public function getFullNumber()
    {
        return $this->document_number . $this->document_additional_number;
    }

    /**
     * @return bool
     * @throws \Throwable
     */
    public function unlinkSignStatuses()
    {
        foreach ($this->signSteps as $signStep) {
            if (!$signStep->delete())
                return false;
        }

        return true;
    }

    /**
     * @return bool
     * @var PaymentsRequestSignTemplateStep[] $templateSteps
     */
    public function linkSignStatuses(array $templateSteps)
    {
        foreach ($templateSteps as $templateStep) {
            $signStep = new SignStep([
                'request_id' => $this->primaryKey,
                'employee_id' => $templateStep->employee_id,
                'step' => $templateStep->step,
                'name' => $templateStep->name,
                'can_sign' => ($templateStep->step == 1),
                'can_unsign' => false,
                'is_signed' => false,
                'signed_at' => null
            ]);

            if (!$signStep->save()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return
            'Заявка на платежи № ' .
            $this->fullNumber . ' на ' .
            DateHelper::format($this->payment_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
    }

    /**
     * @param $company
     * @param null $number
     * @param null $date
     * @return int|null
     */
    public static function getNextDocumentNumber($company, $number = null, $date = null)
    {
        $companyId = ($company instanceof Company) ? $company->id : $company;
        $numQuery = self::find()->andWhere(['company_id' => $companyId]);
        if ($date) {
            $numQuery->andWhere('YEAR(`document_date`) = :year', [':year' => date('Y', strtotime($date))]);
        }

        $numQuery->having('CHAR_LENGTH(`document_number`) <= ' . AbstractDocument::MAX_INCREMENTED_DOCUMENT_NUMBER_LENGTH);

        $lastNumber = $number ? $number : (int)$numQuery->max('(document_number * 1)');
        $nextNumber = 1 + $lastNumber;

        $existNumQuery = static::find()->select('document_number')->andWhere(['company_id' => $companyId]);

        if ($date) {
            $existNumQuery->andWhere('YEAR(`document_date`) = :year', [':year' => date('Y', strtotime($date))]);
        }

        $numArray = $existNumQuery->column();

        if (in_array($nextNumber, $numArray)) {
            return static::getNextDocumentNumber($company, $nextNumber, $date);
        } else {
            return $nextNumber;
        }
    }

    // CHAT //

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChat()
    {
        return $this->hasOne(PaymentsRequestChat::class, ['request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChatUsers()
    {
        return $this->hasMany(PaymentsRequestChatUser::class, ['request_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getStatusName()
    {
        $statusName = $this->status->name;
        foreach ($this->signSteps as $signStep)
            if ($signStep->signed_at)
                $statusName = $signStep->name;

        return $statusName;
    }

    /**
     * @param Employee $employee
     * @return bool
     */
    public function canApprovePlanFlows(Employee $employee)
    {
        return in_array($employee->id, ArrayHelper::getColumn($this->signSteps, 'employee_id'));
    }

    // LOG //

    public function getLogMessage(Log $log)
    {
        $title = 'Заявка на платежи №';

        $ioType = $log->getModelAttributeNew('type');
        if ($ioType == Documents::IO_TYPE_IN) {
            $title .= $log->getModelAttributeNew('document_number');
        } else {
            $title .= $log->getModelAttributeNew('document_number') . ' ' . $log->getModelAttributeNew('document_additional_number');
        }

        $link = Html::a($title, [
            '/analytics/plan-payments-request/view',
            'id' => $log->getModelAttributeNew('id'),
        ]);

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . ' была создана.';
            case LogEvent::LOG_EVENT_DELETE:
                return $title . ' была удалена.';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . ' была изменена.';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                return $link . ' статус "' . PackingListStatus::findOne($log->getModelAttributeNew('status_id'))->name . '"';
            default:
                return $log->message;
        }
    }

    public function getLogIcon(Log $log)
    {
        $icon = ['label-info', 'fa fa-file-text-o'];
        return $icon;
    }
}
