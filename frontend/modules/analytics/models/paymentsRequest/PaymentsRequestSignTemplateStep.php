<?php

namespace frontend\modules\analytics\models\paymentsRequest;

use Yii;
use common\models\employee\Employee;

/**
 * This is the model class for table "payments_request_sign_template_step".
 *
 * @property int $id
 * @property int $template_id
 * @property int $employee_id
 * @property string $name
 * @property int $step
 *
 * @property Employee $employee
 * @property PaymentsRequestSignTemplate $template
 * @property bool $isVisible
 * @property bool $isNew
 */
class PaymentsRequestSignTemplateStep extends \yii\db\ActiveRecord
{
    private $_isVisibleInForm;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments_request_sign_template_step';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['template_id', 'employee_id', 'name', 'step'], 'required'],
            [['template_id', 'employee_id', 'step'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::class, 'targetAttribute' => ['employee_id' => 'id']],
            [['template_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentsRequestSignTemplate::class, 'targetAttribute' => ['template_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'template_id' => 'Шаблон подписания',
            'employee_id' => 'Подписант',
            'name' => 'Название шага',
            'step' => '№ шага',
        ];
    }

    /**
     * @inheritdoc
     */
    public function formName()
    {
        return parent::formName() . '_' . $this->id;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::class, ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(PaymentsRequestSignTemplate::class, ['id' => 'template_id']);
    }

    public function getIsNew()
    {
        return (!$this->id || !is_numeric($this->id));
    }

    public function setIsVisible($val)
    {
        $this->_isVisibleInForm = (bool)$val;
    }

    public function getIsVisible()
    {
        if ($this->getIsNew()) {
            return (bool)$this->_isVisibleInForm;
        }

        return true;
    }
}
