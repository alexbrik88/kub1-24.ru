<?php
namespace frontend\modules\analytics\models\paymentsRequest;

use Yii;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\models\currency\Currency;
use common\models\employee\Employee;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class PaymentsRequestStatisticsHelper {

    const BANK = 1; // PlanCashFlows::PAYMENT_TYPE_BANK
    const ORDER = 2; // PlanCashFlows::PAYMENT_TYPE_ORDER
    const EMONEY = 3; // PlanCashFlows::PAYMENT_TYPE_EMONEY
    
    public static function getPeriodStatisticInfo(PaymentsRequest $model)
    {
        /** @var Employee $employee */
        $user = Yii::$app->user;
        $employee = $user->identity;
        $company = $employee->company;
        @list($walletType, $walletId) = explode('_', $model->payment_source);

        $planAmounts = [
            self::BANK => self::_getPlanFlowsAmountByWallet($model, self::BANK, 'checking_accountant_id'),
            self::ORDER => self::_getPlanFlowsAmountByWallet($model, self::ORDER, 'cashbox_id'),
            self::EMONEY => self::_getPlanFlowsAmountByWallet($model, self::EMONEY, 'emoney_id'),
        ];


        if ($walletType == PaymentsRequest::PAYMENT_SOURCE_BANK) {
            $bankArray = $company
                ->getRubleAccounts()
                ->andFilterWhere(['id' => $walletId])
                ->select('name')
                ->groupBy('id')
                ->orderBy(['type' => SORT_ASC, 'name' => SORT_ASC])
                ->indexBy('id')
                ->column();
        }
        if ($walletType == PaymentsRequest::PAYMENT_SOURCE_CASHBOX) {
            $cashboxArray = $employee
                ->getCashboxes()
                ->andFilterWhere(['id' => $walletId])
                ->andWhere(['currency_id' => Currency::DEFAULT_ID])
                ->select('name')
                ->orderBy(['is_main' => SORT_DESC])
                ->indexBy('id')
                ->column();
        }
        if ($walletType == PaymentsRequest::PAYMENT_SOURCE_EMONEY) {
            $emoneyArray = $company
                ->getRubleEmoneys()
                ->andFilterWhere(['id' => $walletId])
                ->select('name')
                ->orderBy(['is_main' => SORT_DESC])
                ->indexBy('id')
                ->column();
        }

        $data = [
            self::BANK => [],
            self::ORDER => [],
            self::EMONEY => [],
        ];

        if (!empty($bankArray)) {
            $bankItems = array_merge([''], array_keys($bankArray));
            foreach ($bankItems as $bank_id) {
                if ($bank_id) {
                    $bankModel = new CashBankFlows(['account_id' => $bank_id]);
                    $factEndBalance = $bankModel->getBalanceAtEnd($model->payment_date);
                    $planEndBalance = $planAmounts[self::BANK][$bank_id] ?? 0;
                } else {
                    $factEndBalance = $planEndBalance = 0;
                }

                $data[self::BANK][] = [
                    'typeName' => $bank_id ? $bankArray[$bank_id] : 'Банк',
                    //'bik' => $bik ?: null,
                    'factEndBalance' => $factEndBalance,
                    'planEndBalance' => $planEndBalance,
                    'diffBalance' => $factEndBalance - $planEndBalance,
                    'inTotal' => $bank_id ? false : true,
                    'target' => $bank_id ? null : '.cash-bank-substring',
                    'cssClass' => $bank_id ? 'cash-bank-substring' : 'toggle-string',
                ];
            }
        }

        if (!empty($cashboxArray)) {
            $cashboxItems = array_merge([''], array_keys($cashboxArray));
            foreach ($cashboxItems as $cashbox_id) {
                if ($cashbox_id) {
                    $orderModel = new CashOrderFlows(['cashbox_id' => $cashbox_id]);
                    $factEndBalance = $orderModel->getBalanceAtEnd($model->payment_date);
                    $planEndBalance = $planAmounts[self::ORDER][$cashbox_id] ?? 0;
                } else {
                    $factEndBalance = $planEndBalance = 0;
                }

                $data[self::ORDER][] = [
                    'typeName' => $cashbox_id ? $cashboxArray[$cashbox_id] : 'Касса',
                    'cashboxId' => $cashbox_id ?: null,
                    'factEndBalance' => $factEndBalance,
                    'planEndBalance' => $planEndBalance,
                    'diffBalance' => $factEndBalance - $planEndBalance,
                    'inTotal' => $cashbox_id ? false : true,
                    'target' => $cashbox_id ? null : '.cash-order-substring',
                    'cssClass' => $cashbox_id ? 'cash-order-substring' : 'toggle-string',
                ];
            }
        }

        if (!empty($emoneyArray)) {
            $emoneyItems = array_merge([''], array_keys($emoneyArray));
            foreach ($emoneyItems as $emoney_id) {
                if ($emoney_id) {
                    $emoneyModel = new CashEmoneyFlows(['emoney_id' => $emoney_id]);
                    $factEndBalance = $emoneyModel->getBalanceAtEnd($model->payment_date);
                    $planEndBalance = $planAmounts[self::EMONEY][$emoney_id] ?? 0;
                } else {
                    $factEndBalance = $planEndBalance = 0;
                }

                $data[self::EMONEY][] = [
                    'typeName' => $emoney_id ? $emoneyArray[$emoney_id] : 'E-money',
                    'emoneyId' => $emoney_id ?: null,
                    'factEndBalance' => $factEndBalance,
                    'planEndBalance' => $planEndBalance,
                    'diffBalance' => $factEndBalance - $planEndBalance,
                    'inTotal' => $emoney_id ? false : true,
                    'target' => $emoney_id ? null : '.cash-emoney-substring',
                    'cssClass' => $emoney_id ? 'cash-emoney-substring' : 'toggle-string',
                ];
            }
        }

        // calc totals
        foreach ($data as $wallet => $dt) {
            foreach ($dt as $d) {
                if ($d['inTotal']) continue;
                $data[$wallet][0]['factEndBalance'] += $d['factEndBalance'];
                $data[$wallet][0]['planEndBalance'] += $d['planEndBalance'];
                $data[$wallet][0]['diffBalance'] += $d['diffBalance'];
            }
        }

        $data = array_merge($data[self::BANK], $data[self::ORDER], $data[self::EMONEY]);

        if ($index = count($data)) {
            $data[$index] = [
                'typeName' => 'Итого',
                'target' => null,
            ];

            foreach ($data as $row) {
                if (!empty($row['inTotal'])) {
                    foreach ($row as $column => $value) {
                        if (in_array($column, ['factEndBalance', 'planEndBalance', 'diffBalance'])) {
                            if (array_key_exists($column, $data[$index])) {
                                $data[$index][$column] += $value;
                            } else {
                                $data[$index][$column] = $value;
                            }
                        }
                    }
                }
            }

            $data[$index]['cssClass'] = 'summ';
        }

        return $data;
    }

    private static function _getPlanFlowsAmountByWallet(PaymentsRequest $model, $walletID, $groupBy)
    {
        return $model
            ->getPlanFlows()
            ->andWhere(['plan_cash_flows.payment_type' => $walletID])
            ->groupBy($groupBy)
            ->indexBy($groupBy)
            ->select(new Expression("SUM(`amount`) amount, `{$groupBy}`"))
            ->column();
    }
}