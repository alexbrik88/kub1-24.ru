<?php

namespace frontend\modules\analytics\models\paymentsRequest;

use Yii;
use common\models\Company;

/**
 * This is the model class for table "payments_request_sign_template".
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property string|null $comment
 * @property int|null $is_active
 *
 * @property Company $company
 * @property PaymentsRequestSignTemplateStep[] $steps
 * @property bool $isPresentInRequests
 */
class PaymentsRequestSignTemplate extends \yii\db\ActiveRecord
{
    const MAX_STEPS_COUNT = 7;

    public $hasSteps;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments_request_sign_template';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'name'], 'required'],
            [['is_active'], 'filter', 'filter' => function($value) { return (int)$value; }],
            [['company_id', 'is_active'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['comment'], 'string', 'max' => 2048],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [
                ['name'], 'unique', 'filter' => ['company_id' => $this->company_id],
                'message' => 'Шаблон с таким названием уже существует',
            ],
            [['hasSteps'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'name' => 'Название',
            'comment' => 'Комментарий',
            'is_active' => 'Is Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSteps()
    {
        return $this->hasMany(PaymentsRequestSignTemplateStep::class, ['template_id' => 'id']);
    }

    public function getIsPresentInRequests()
    {
        return $this->id && PaymentsRequest::find()->where(['sign_template_id' => $this->id])->exists();
    }
}
