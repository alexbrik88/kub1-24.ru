<?php

namespace frontend\modules\analytics\models\paymentsRequest;

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\employee\Employee;
use Yii;
use frontend\modules\analytics\models\PlanCashFlows;
use yii\helpers\Html;

/**
 * This is the model class for table "payments_request_order".
 *
 * @property int $id
 * @property int $payments_request_id
 * @property int $plan_operation_id
 * @property int|null $number
 *
 * @property PaymentsRequest $paymentsRequest
 * @property PlanCashFlows $planOperation
 * @property PlanCashFlows $planCashFlow
 * @property PaymentsRequestOrderApproved[] $approved
 */
class PaymentsRequestOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments_request_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['payments_request_id', 'plan_operation_id'], 'required'],
            [['payments_request_id', 'plan_operation_id', 'number'], 'integer'],
            [['payments_request_id', 'plan_operation_id'], 'unique', 'targetAttribute' => ['payments_request_id', 'plan_operation_id']],
            [['plan_operation_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlanCashFlows::class, 'targetAttribute' => ['plan_operation_id' => 'id']],
            [['payments_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentsRequest::class, 'targetAttribute' => ['payments_request_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'payments_request_id' => 'Заявка',
            'plan_operation_id' => 'Платеж',
            'number' => 'Порядок',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentsRequest()
    {
        return $this->hasOne(PaymentsRequest::class, ['id' => 'payments_request_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanOperation()
    {
        return $this->hasOne(PlanCashFlows::class, ['id' => 'plan_operation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanCashFlow()
    {
        return $this->hasOne(PlanCashFlows::class, ['id' => 'plan_operation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApproved()
    {
        return $this->hasMany(PaymentsRequestOrderApproved::class, ['order_id' => 'id']);
    }

    /**
     * @param Employee $employee
     * @return PaymentsRequestOrderApproved
     */
    public function getApprovedModel(Employee $employee)
    {
        /** @var PaymentsRequestOrderApproved $exists */
        $exists = $this->getApproved()
            ->where(['employee_id' => $employee->id])
            ->one();

        return $exists ?: new PaymentsRequestOrderApproved([
            'order_id' => $this->id,
            'employee_id' => $employee->id,
            'approved' => 0
        ]);
    }

    public static function flow2json(PlanCashFlows $flow)
    {
        return  [
            'id' => $flow->id,
            'date' => DateHelper::format($flow->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'amount' => TextHelper::invoiceMoneyFormat($flow->amount, 2),
            'contractor' => Html::encode(($flow->contractor) ? $flow->contractor->getShortName(true) : ''),
            'description' => Html::encode($flow->description),
            'invoice' => ($flow->invoice) ? $flow->invoice->fullNumber : '',
            'industry' => Html::encode(($flow->industry) ? $flow->industry->name : ''),
            'sale_point' => Html::encode(($flow->salePoint) ? $flow->salePoint->name : ''),
            'project' => Html::encode(($flow->project) ? $flow->project->name : ''),
            'expenditure_item' => Html::encode(($flow->expenditureItem) ? $flow->expenditureItem->name : ''),
            'payment_type' => PlanCashFlows::$paymentTypes[$flow->payment_type] ?? '',
            'payment_priority' => ($flow->contractor) ? $flow->contractor->payment_priority : '',
        ];
    }    
}
