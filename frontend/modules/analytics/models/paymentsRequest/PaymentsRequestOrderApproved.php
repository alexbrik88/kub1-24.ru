<?php

namespace frontend\modules\analytics\models\paymentsRequest;

use Yii;
use common\models\employee\Employee;

/**
 * This is the model class for table "payments_request_order_approved".
 *
 * @property int|null $order_id
 * @property int|null $employee_id
 * @property int|null $approved
 * @property int|null $approved_at
 *
 * @property Employee $employee
 * @property PaymentsRequestOrder $order
 */
class PaymentsRequestOrderApproved extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments_request_order_approved';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'employee_id', 'approved'], 'required'],
            [['order_id', 'employee_id', 'approved', 'approved_at'], 'integer'],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::class, 'targetAttribute' => ['employee_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentsRequestOrder::class, 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'employee_id' => 'Employee ID',
            'approved' => 'Approved',
            'approved_at' => 'Approved At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::class, ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(PaymentsRequestOrder::class, ['id' => 'order_id']);
    }
}
