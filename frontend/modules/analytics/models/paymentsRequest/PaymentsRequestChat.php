<?php

namespace frontend\modules\analytics\models\paymentsRequest;

use Yii;
use common\models\employee\Employee;
use common\models\Company;

/**
 * This is the model class for table "payments_request_chat".
 *
 * @property int $id
 * @property int $created_at
 * @property int $author_id
 * @property int $company_id
 * @property int $request_id
 * @property string|null $message
 *
 * @property Employee $author
 * @property Company $company
 * @property PaymentsRequest $request
 */
class PaymentsRequestChat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments_request_chat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'author_id', 'company_id', 'request_id', 'message'], 'required'],
            [['created_at', 'author_id', 'company_id', 'request_id'], 'integer'],
            [['message'], 'string', 'min' => '3', 'max' => 2048],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::class, 'targetAttribute' => ['author_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentsRequest::class, 'targetAttribute' => ['request_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'author_id' => 'Author ID',
            'company_id' => 'Company ID',
            'request_id' => 'Request ID',
            'message' => 'Сообщение',
        ];
    }

    /**
     * Gets query for [[Author]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Employee::class, ['id' => 'author_id']);
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Gets query for [[Request]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(PaymentsRequest::class, ['id' => 'request_id']);
    }
}
