<?php

namespace frontend\modules\analytics\models\paymentsRequest;

use common\models\cash\CashBankFlows;
use common\models\cash\CashOrderFlows;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestSignTemplate as SignTemplate;
use frontend\modules\analytics\models\PlanCashFlows;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestStatus as Status;
use frontend\modules\analytics\models\paymentsRequest\PaymentsRequestOrder as Order;

/**
 * PaymentsRequestSearch represents the model behind the search form about `common\models\PaymentsRequest`.
 */
class PaymentsRequestSearch extends PaymentsRequest
{
    /**
     * @var
     */
    public $company_id;
    public $byNumber;

    public $payments_status;
    public $payments_sum;
    public $payments_count;

    protected $query;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status_id', 'author_id'], 'integer'],
            [['document_date', 'byNumber'], 'safe'],
            [['industry_id', 'sale_point_id', 'project_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param $params
     * @param null $dateRange
     * @return ActiveDataProvider
     * @throws Exception
     */
    public function search($params, $dateRange = null)
    {
        if (!$this->company_id) {
            throw new Exception('no company id');
        }

        $this->load($params);
        $dateRange = $dateRange ?: StatisticPeriod::getSessionPeriod();

        $query = self::find()
            ->andWhere([self::tableName() . '.company_id' => $this->company_id])
            ->addSelect([
                PaymentsRequest::tableName() . '.*',
                new Expression('COUNT(plan_cash_flows.amount) AS payments_count'),
                new Expression('SUM(plan_cash_flows.amount) AS payments_sum'),
                new Expression('COUNT(DISTINCT payments_request.id) AS requests_count'),
            ])
            ->joinWith([
                'status',
                'orders.planCashFlow',
            ])
            ->groupBy(self::tableName() . '.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'date_init' => [
                        'desc' => [
                            self::tableName() . '.`document_date`' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'document_date',
                    'document_number' => [
                        'asc' => [
                            self::tableName() . '.`document_number` * 1' => SORT_ASC,
                            self::tableName() . '.`document_additional_number` * 1' => SORT_ASC,
                        ],
                        'desc' => [
                            self::tableName() . '.`document_number` * 1' => SORT_DESC,
                            self::tableName() . '.`document_additional_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'payments_sum',
                    'payments_count',
                ],
                'defaultOrder' => ['document_number' => SORT_DESC],
            ],
        ]);

        $query->andFilterWhere([
            'id' => $this->id,
            'document_date' => $this->document_date,
            'status_id' => $this->status_id,
        ]);

        $query->andWhere(['between', self::tableName() . '.document_date', $dateRange['from'], $dateRange['to']]);

        if (!empty($this->byNumber)) {
            $paramArray = explode(' ', preg_replace('/\s{2,}/', ' ', trim($this->byNumber)));
            foreach ($paramArray as $param) {
                $query->andFilterWhere([
                    'or',
                    ['like', self::tableName() . '.document_number', $param],
                    ['like', self::tableName() . '.document_additional_number', $param],
                ]);
            }
        }

        $this->query = $query;

        return $dataProvider;
    }

    public function getQuery()
    {
        return clone $this->query;
    }

    /**
     * @return array
     */
    public function filterAuthor()
    {
        $query = clone $this->getQuery();
        $idsArray = $query
            ->select('author_id')
            ->distinct()
            ->column() ?: [-1];

        $employee = Employee::find()
            ->andWhere(['id' => $idsArray])
            ->all();

        return ['' => 'Все'] + ArrayHelper::map($employee, 'id', 'shortFio');
    }

    /**
     * @return array
     */
    public function filterStatus()
    {
        return [
            '' => 'Все',
            Status::STATUS_APPROVAL => 'Согласование',
            Status::STATUS_SIGNING => 'Подписание',
            Status::STATUS_REJECTED => 'Отклонена',
            Status::STATUS_SIGNED => 'Подписана',
        ];
    }

    /**
     * @return array
     */
    public function filterPaymentsStatus()
    {
        return [
            '' => 'Все',
        ];
    }

    /**
     * @return array
     */
    public function filterSignTemplate()
    {
        return ['' => 'Все'] + SignTemplate::find()
                ->where(['company_id' => $this->company->id])
                ->orderBy(['name' => SORT_ASC])
                ->select(['name', 'id'])
                ->indexBy('id')
                ->column();
    }

    /**
     * @return array
     */
    public function getStat()
    {
        $statData = (new \yii\db\Query)
            ->select(['status_id', 'requests_count count', 'payments_sum sum'])
            ->from(['t' => $this->getQuery()->groupBy('status_id')])
            ->indexBy('status_id')
            ->all();

        return [
            Status::STATUS_APPROVAL => [
                'title' => 'На согласовании',
                'class' => 'count-card_green',
                'sum' => $statData[Status::STATUS_APPROVAL]['sum'] ?? 0,
                'count' => $statData[Status::STATUS_APPROVAL]['count'] ?? 0,
            ],
            Status::STATUS_SIGNING => [
                'title' => 'На подписании',
                'class' => 'count-card_yellow',
                'sum' => $statData[Status::STATUS_SIGNING]['sum'] ?? 0,
                'count' => $statData[Status::STATUS_SIGNING]['count'] ?? 0,
            ],
            Status::STATUS_REJECTED => [
                'title' => 'Отклоненные',
                'class' => 'count-card_red',
                'sum' => $statData[Status::STATUS_REJECTED]['sum'] ?? 0,
                'count' => $statData[Status::STATUS_REJECTED]['count'] ?? 0,
            ],
            Status::STATUS_SIGNED => [
                'title' => 'Подписанные',
                'class' => 'count-card_turquoise',
                'sum' => $statData[Status::STATUS_SIGNED]['sum'] ?? 0,
                'count' => $statData[Status::STATUS_SIGNED]['count'] ?? 0,
            ]
        ];
    }
}
