<?php

namespace frontend\modules\analytics\models\paymentsRequest;

use Yii;
use common\models\employee\Employee;

/**
 * This is the model class for table "payments_request_sign_step".
 *
 * @property int $request_id
 * @property int $employee_id
 * @property int|null $can_sign
 * @property int|null $can_unsign
 * @property int|null $is_signed
 * @property int|null $signed_at
 * @property int $step
 * @property string $name
 *
 * @property Employee $employee
 * @property PaymentsRequest $request
 */
class PaymentsRequestSignStep extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments_request_sign_step';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_id', 'employee_id', 'step', 'name'], 'required'],
            [['can_sign', 'can_unsign', 'is_signed'], 'filter', 'filter' => function($val) { return (int)$val; }],
            [['request_id', 'employee_id', 'can_sign', 'can_unsign', 'is_signed', 'signed_at', 'step'], 'integer'],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::class, 'targetAttribute' => ['employee_id' => 'id']],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentsRequest::class, 'targetAttribute' => ['request_id' => 'id']],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'request_id' => 'Request ID',
            'employee_id' => 'Employee ID',
            'can_sign' => 'Can Sign',
            'can_unsign' => 'Can Unsign',
            'is_signed' => 'Is Signed',
            'signed_at' => 'Signed At',
            'step' => 'Step',
            'name' => 'Name'
        ];
    }

    /**
     * Gets query for [[Employee]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::class, ['id' => 'employee_id']);
    }

    /**
     * Gets query for [[Request]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(PaymentsRequest::class, ['id' => 'request_id']);
    }

    public function sign()
    {
        if ($this->can_sign) {
            $this->updateAttributes([
                'can_sign' => false,
                'can_unsign' => true,
                'signed_at' => time()
            ]);

            return true;
        }

        return false;
    }

    public function unsign()
    {
        if ($this->can_unsign) {
            $this->updateAttributes([
                'can_sign' => true,
                'can_unsign' => false,
                'signed_at' => 0
            ]);

            return true;
        }

        return false;
    }

    public function next()
    {
        foreach ($this->request->signSteps as $signStep) {
            if ($signStep->step > $this->step)
                return $signStep;
        }

        return null;
    }

    public function prev()
    {
        foreach (array_reverse($this->request->signSteps) as $signStep) {
            if ($signStep->step < $this->step)
                return $signStep;
        }

        return null;
    }
}
