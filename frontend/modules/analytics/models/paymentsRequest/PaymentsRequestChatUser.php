<?php

namespace frontend\modules\analytics\models\paymentsRequest;

use Yii;
use common\models\employee\Employee;

/**
 * This is the model class for table "payments_request_chat_user".
 *
 * @property int $request_id
 * @property int $reader_id
 * @property int|null $last_read_at
 *
 * @property Employee $reader
 * @property PaymentsRequest $request
 */
class PaymentsRequestChatUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments_request_chat_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_id', 'reader_id'], 'required'],
            [['request_id', 'reader_id', 'last_read_at'], 'integer'],
            [['last_read_at'], 'default', 'value' => 0],
            [['reader_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::class, 'targetAttribute' => ['reader_id' => 'id']],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentsRequest::class, 'targetAttribute' => ['request_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'request_id' => 'Request ID',
            'reader_id' => 'Reader ID',
            'last_read_at' => 'Last Read At',
        ];
    }

    /**
     * Gets query for [[Reader]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReader()
    {
        return $this->hasOne(Employee::class, ['id' => 'reader_id']);
    }

    /**
     * Gets query for [[Request]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(PaymentsRequest::class, ['id' => 'request_id']);
    }

    /**
     * @return int|string
     */
    public function getUnreadMessagesCount()
    {
        return PaymentsRequestChat::find()
            ->where(['request_id' => $this->request_id])
            ->andWhere(['not', ['author_id' => $this->reader_id]])
            ->andWhere(['>=', 'created_at', $this->last_read_at])
            ->count();
    }
}
