<?php

namespace frontend\modules\analytics\models;

use common\models\cash\CashBankFlows;
use common\models\cash\CashBankFlowToInvoice;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyFlowToInvoice;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderFlowToInvoice;
use common\models\Company;
use common\models\cash\CashFlowsBase;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceAct;
use common\models\document\InvoiceUpd;
use common\models\document\Order;
use common\models\document\PackingList;
use common\models\document\status\ActStatus;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\PackingListStatus;
use common\models\document\status\UpdStatus;
use common\models\document\Upd;
use common\models\product\Product;
use common\models\product\ProductInitialBalance;
use frontend\models\Documents;
use Yii;
use yii\db\Expression;
use yii\db\Query;

class OlapTables
{
    const TABLE_OLAP_DOCS = 'olap_documents';
    const TABLE_OLAP_FLOWS = 'olap_flows';
    const TABLE_OLAP_INVOICES = 'olap_invoices';
    const TABLE_PRODUCT_TURNOVER = 'product_turnover'; // todo: refresh turnover table

    /**
     * @param $companyId
     * @return bool
     */
    public static function refresh($companyId)
    {
        self::refreshInvoicesOLAP($companyId);
        self::refreshFlowsOLAP($companyId);
        self::refreshDocsOLAP($companyId);

        return true;
    }

    private static function refreshFlowsOLAP($companyId)
    {
        $table = self::TABLE_OLAP_FLOWS;
        $queryBank = (new Query())
            ->select(new Expression('
                cbf.id,
                cbf.company_id,
                cbf.contractor_id,
                '.(CashFlowsBase::WALLET_BANK).' AS wallet,
                cbf.flow_type AS type,
                1 AS is_accounting,
                IF (cbf.flow_type = 0, cbf.expenditure_item_id, cbf.income_item_id) AS item_id,
                cbf.date,
                cbf.recognition_date,
                cbf.is_prepaid_expense,
                cbf.amount,
                IF (COUNT(invoice.id), 1, 0) AS has_invoice,
                SUM(IFNULL(order.base_price_with_vat * order.quantity, 0)) AS invoice_amount,
                IF (SUM(invoice.has_act) + SUM(invoice.has_packing_list) + SUM(invoice.has_upd), 1, 0) AS has_doc,
                IF (SUM( IF(invoice.need_act, 0, 1)) + SUM( IF(invoice.need_packing_list, 0, 1)) + SUM( IF(invoice.need_upd, 0, 1)), 0, 1) AS need_doc
            '))
            ->from(['cbf' => CashBankFlows::tableName()])
            ->leftJoin(['cbf_to_invoice' => CashBankFlowToInvoice::tableName()], 'cbf.id = cbf_to_invoice.flow_id')
            ->leftJoin(['invoice' => Invoice::tableName()], 'invoice.id = cbf_to_invoice.invoice_id')
            ->leftJoin(['order' => Order::tableName()], 'order.invoice_id = invoice.id')
            ->leftJoin(['product' => Product::tableName()], 'order.product_id = product.id')
            ->groupBy(['cbf.id'])
            ->where(['cbf.company_id' => $companyId]); // todo: REFRESH ONLY FOR ONE COMPANY

        $queryOrder = (new Query())
            ->select(new Expression('
                cof.id,
                cof.company_id,
                cof.contractor_id,
                '.(CashFlowsBase::WALLET_CASHBOX).' AS wallet,
                cof.flow_type AS type,
                cof.is_accounting,
                IF (cof.flow_type = 0, cof.expenditure_item_id, cof.income_item_id) AS item_id,
                cof.date,
                cof.recognition_date,
                cof.is_prepaid_expense,
                cof.amount,
                IF (COUNT(invoice.id), 1, 0) AS has_invoice,
                SUM(IFNULL(order.base_price_with_vat * order.quantity, 0)) AS invoice_amount,
                IF (SUM(invoice.has_act) + SUM(invoice.has_packing_list) + SUM(invoice.has_upd), 1, 0) AS has_doc,
                IF (SUM( IF(invoice.need_act, 0, 1)) + SUM( IF(invoice.need_packing_list, 0, 1)) + SUM( IF(invoice.need_upd, 0, 1)), 0, 1) AS need_doc
            '))
            ->from(['cof' => CashOrderFlows::tableName()])
            ->leftJoin(['cof_to_invoice' => CashOrderFlowToInvoice::tableName()], 'cof.id = cof_to_invoice.flow_id')
            ->leftJoin(['invoice' => Invoice::tableName()], 'invoice.id = cof_to_invoice.invoice_id')
            ->leftJoin(['order' => Order::tableName()], 'order.invoice_id = invoice.id')
            ->leftJoin(['product' => Product::tableName()], 'order.product_id = product.id')
            ->groupBy(['cof.id'])
            ->where(['cof.company_id' => $companyId]); // todo: REFRESH ONLY FOR ONE COMPANY

        $queryEmoney = (new Query())
            ->select(new Expression('
                cef.id,
                cef.company_id,
                cef.contractor_id,
                '.(CashFlowsBase::WALLET_EMONEY).' AS wallet,
                cef.flow_type AS type,
                cef.is_accounting,
                IF (cef.flow_type = 0, cef.expenditure_item_id, cef.income_item_id) AS item_id,
                cef.date,
                cef.recognition_date,
                cef.is_prepaid_expense,
                cef.amount,
                IF (COUNT(invoice.id), 1, 0) AS has_invoice,
                SUM(IFNULL(order.base_price_with_vat * order.quantity, 0)) AS invoice_amount,
                IF (SUM(invoice.has_act) + SUM(invoice.has_packing_list) + SUM(invoice.has_upd), 1, 0) AS has_doc,
                IF (SUM( IF(invoice.need_act, 0, 1)) + SUM( IF(invoice.need_packing_list, 0, 1)) + SUM( IF(invoice.need_upd, 0, 1)), 0, 1) AS need_doc
            '))
            ->from(['cef' => CashEmoneyFlows::tableName()])
            ->leftJoin(['cef_to_invoice' => CashEmoneyFlowToInvoice::tableName()], 'cef.id = cef_to_invoice.flow_id')
            ->leftJoin(['invoice' => Invoice::tableName()], 'invoice.id = cef_to_invoice.invoice_id')
            ->leftJoin(['order' => Order::tableName()], 'order.invoice_id = invoice.id')
            ->leftJoin(['product' => Product::tableName()], 'order.product_id = product.id')
            ->groupBy(['cef.id'])
            ->where(['cef.company_id' => $companyId]); /** REFRESH ONLY FOR ONE COMPANY */

        $query = $queryBank->union($queryOrder, true)->union($queryEmoney, true);

        Yii::$app->db->createCommand("DELETE FROM {$table} WHERE `company_id` = " . $companyId)->execute();
        Yii::$app->db->createCommand("INSERT INTO {$table} ({$query->createCommand()->rawSql})")->execute();
    }

    private static function refreshDocsOLAP($companyId)
    {
        $table = self::TABLE_OLAP_DOCS;

        $select = [
            'invoice.company_id',
            'invoice.contractor_id',
            'by_document',
            'document.id',
            'document.type',
            'document.document_date AS date',
            'SUM( IF(document.type = 2, `order_main`.selling_price_with_vat, `order_main`.purchase_price_with_vat) * `order`.quantity) AS amount',
            'IF (contractor.not_accounting, 0, 1) AS is_accounting',
            'invoice.invoice_expenditure_item_id',
        ];

        $selectInitialBalance = new Expression('
            product.company_id,
            NULL AS contractor_id,
            "0" AS by_document,
            product.id AS id,
            1 AS type,
            IFNULL(pib.date, "'.date('Y-m-d').'") AS date,
            0 AS amount,
            1 AS is_accounting,
            NULL AS invoice_expenditure_item_id
        ');

        // оборот по товарным накладным
        $query1 = (new Query)
            ->select($select)
            ->addSelect(['by_document' => new Expression('"' . Documents::DOCUMENT_PACKING_LIST . '"')])
            ->from(['order' => 'order_packing_list'])
            ->leftJoin(['document' => PackingList::tableName()], '{{document}}.[[id]] = {{order}}.[[packing_list_id]]')
            ->leftJoin(['invoice' => Invoice::tableName()], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
            ->leftJoin(['product' => Product::tableName()], '{{product}}.[[id]] = {{order}}.[[product_id]]')
            ->leftJoin(['order_main' => Order::tableName()], '{{order_main}}.[[id]] = {{order}}.[[order_id]]')
            ->leftJoin(['contractor' => Contractor::tableName()], 'contractor.id = invoice.contractor_id')
            ->andWhere([
                'invoice.company_id' => $companyId, // todo: REFRESH ONLY FOR ONE COMPANY
                'invoice.is_deleted' => false,
                'invoice.invoice_status_id' => InvoiceStatus::$validInvoices,
            ])
            ->andWhere([
                'or',
                ['document.status_out_id' => PackingListStatus::$turmoverStatus],
                ['document.status_out_id' => null],
            ])
            ->groupBy('document.id');

        // оборот по УПД
        $query2 = (new Query)
            ->select($select)
            ->addSelect(['by_document' => new Expression('"'.Documents::DOCUMENT_UPD.'"')])
            ->from(['order' => 'order_upd'])
            ->leftJoin(['document' => Upd::tableName()], '{{document}}.[[id]] = {{order}}.[[upd_id]]')
            ->leftJoin(['invoice_upd' => InvoiceUpd::tableName()], '{{invoice_upd}}.[[upd_id]] = {{document}}.[[id]]')
            ->leftJoin(['invoice' => Invoice::tableName()], '{{invoice_upd}}.[[invoice_id]] = {{invoice}}.[[id]]')
            ->leftJoin(['product' => Product::tableName()], '{{product}}.[[id]] = {{order}}.[[product_id]]')
            ->leftJoin(['order_main' => Order::tableName()], '{{order_main}}.[[id]] = {{order}}.[[order_id]]')
            ->leftJoin(['contractor' => Contractor::tableName()], 'contractor.id = invoice.contractor_id')
            ->andWhere([
                'invoice.company_id' => $companyId, // todo: REFRESH ONLY FOR ONE COMPANY
                'invoice.is_deleted' => false,
                'invoice.invoice_status_id' => InvoiceStatus::$validInvoices,
            ])
            ->andWhere([
                'or',
                ['document.status_out_id' => UpdStatus::$turmoverStatus],
                ['document.status_out_id' => null],
            ])
            ->groupBy('invoice.id, document.id');

        // оборот по Актам
        $query3 = (new Query)
            ->select($select)
            ->addSelect(['by_document' => new Expression('"'.Documents::DOCUMENT_ACT.'"')])
            ->from(['order' => 'order_act'])
            ->leftJoin(['document' => Act::tableName()], '{{document}}.[[id]] = {{order}}.[[act_id]]')
            ->leftJoin(['invoice_act' => InvoiceAct::tableName()], '{{invoice_act}}.[[act_id]] = {{document}}.[[id]]')
            ->leftJoin(['invoice' => Invoice::tableName()], '{{invoice_act}}.[[invoice_id]] = {{invoice}}.[[id]]')
            ->leftJoin(['product' => Product::tableName()], '{{product}}.[[id]] = {{order}}.[[product_id]]')
            ->leftJoin(['order_main' => Order::tableName()], '{{order_main}}.[[id]] = {{order}}.[[order_id]]')
            ->leftJoin(['contractor' => Contractor::tableName()], 'contractor.id = invoice.contractor_id')
            ->andWhere([
                'invoice.company_id' => $companyId, // todo: REFRESH ONLY FOR ONE COMPANY
                'invoice.is_deleted' => false,
                'invoice.invoice_status_id' => InvoiceStatus::$validInvoices,
            ])
            ->andWhere([
                'or',
                ['document.status_out_id' => ActStatus::$turmoverStatus],
                ['document.status_out_id' => null],
            ])
            ->groupBy('invoice.id, document.id');

        // Начальные остатки
        $query4 = (new Query)
            ->select($selectInitialBalance)
            ->from('product')
            ->leftJoin(['store' => 'product_store'], 'product.id = store.product_id')
            ->leftJoin(['pib' => ProductInitialBalance::tableName()], 'product.id = pib.product_id')
            ->where([
                'product.company_id' => $companyId, // todo: REFRESH ONLY FOR ONE COMPANY
                'product.production_type' => Product::PRODUCTION_TYPE_GOODS,
                'product.is_deleted' => false,
            ])
            ->andWhere(['not', ['product.status' => Product::DELETED]])
            ->groupBy('product.id');

        $query = $query1->union($query2)->union($query3)->union($query4, true); // not "all" for prevent double primary

        Yii::$app->db->createCommand("DELETE FROM {$table} WHERE `company_id` = " . $companyId)->execute();
        Yii::$app->db->createCommand("INSERT INTO {$table} ({$query->createCommand()->rawSql})")->execute();
    }

    private static function refreshInvoicesOLAP($companyId)
    {
        $table = self::TABLE_OLAP_INVOICES;
        $query = (new Query())
            ->select(new Expression('
                i.id,
                i.company_id,
                i.contractor_id,
                i.invoice_expenditure_item_id AS expenditure_item_id,
                i.type,
                i.document_date AS date,
                i.total_amount_with_nds,
                i.payment_partial_amount,
                i.invoice_status_id AS status_id,
                i.invoice_status_updated_at AS status_updated_at,
                i.payment_limit_date,
                IF(IF (i.has_act, 1, 0) + IF (i.has_packing_list, 1, 0) + IF(i.has_upd, 1, 0), 1, 0) AS has_doc,
                IF(IF (i.need_act, 0, 1) + IF (i.need_packing_list, 0, 1) + IF(i.need_upd, 0, 1), 1, 0) AS not_need_doc,
                IF(IF (i.need_act, 1, 0) * IF (i.need_packing_list, 1, 0) * IF(i.need_upd, 1, 0), 1, 0) AS need_all_docs,
                (
                    SELECT MAX(document_date) AS max_date FROM packing_list p WHERE p.invoice_id = i.id 
                    UNION ALL
                    SELECT MAX(document_date) AS max_date FROM act a LEFT JOIN invoice_act r ON a.id = r.act_id WHERE r.invoice_id = i.id 
                    UNION ALL
                    SELECT MAX(document_date) AS max_date FROM upd u LEFT JOIN invoice_upd r ON u.id = r.upd_id WHERE r.invoice_id = i.id                    
                    ORDER BY max_date DESC 
                    LIMIT 1
                ) AS doc_date,
                i.is_deleted
            '))
            ->from(['i' => Invoice::tableName()])
            ->groupBy(['i.id'])
            ->where(['i.company_id' => $companyId]) // todo: REFRESH ONLY FOR ONE COMPANY
            ->andWhere(['i.is_deleted' => false]);

        Yii::$app->db->createCommand("DELETE FROM {$table} WHERE `company_id` = " . $companyId)->execute();
        Yii::$app->db->createCommand("INSERT INTO {$table} ({$query->createCommand()->rawSql})")->execute();
    }
}