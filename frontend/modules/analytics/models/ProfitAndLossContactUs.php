<?php
namespace frontend\modules\analytics\models;

use common\components\validators\PhoneValidator;

class ProfitAndLossContactUs extends \yii\base\Model {

    public $name;
    public $phone;
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['name', 'phone', 'email'], 'required'],
            [['name', 'phone'], 'string', 'max' => 45],
            [['phone'], PhoneValidator::class],
            [['email'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'Email',
        ];
    }
}