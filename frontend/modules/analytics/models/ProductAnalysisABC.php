<?php

namespace frontend\modules\analytics\models;

use common\components\date\DateHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductRecommendationAbc;
use common\models\product\ProductTurnover;
use frontend\models\Documents;
use frontend\modules\analytics\components\ProductTurnoverHelper;
use frontend\modules\analytics\models\productAnalysis\TurnoverColumn;
use frontend\modules\integration\models\vkAds\VkAdsAdReportSearch;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class ProductAnalysisSearch
 */
class ProductAnalysisABC extends Model
{
    const GROUP_A = 1;
    const GROUP_B = 2;
    const GROUP_C = 3;

    public $id;
    public $title;
    public $article;
    public $group_id;

    protected $_company;
    protected $_year;
    protected $_query;

    public $datePrev;
    public $dateStart;
    public $dateEnd;

    // extra data
    public $prevPeriodData;
    public $overallData;
    public $overallMargin;
    public $totalCount;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'title', 'group_id', 'year'], 'safe'],
        ];
    }

    public function __construct($config)
    {
        parent::__construct($config);

        $this->year = Yii::$app->session->get('productAnalysis.ABC.ym', date('Ym'));
    }

    public function __destruct()
    {
        Yii::$app->session->set('productAnalysis.ABC.ym', $this->year);
    }

    protected function createTmpTable()
    {
        $db = Yii::$app->db2;

        $query = (new \yii\db\Query())
            ->select([
                'year',
                'month',
                'product_id',
                'quantity' => 'SUM([[quantity]])',
                'purchase' => 'SUM([[purchase_amount]])',
                'total' => 'SUM([[total_amount]])',
                'margin' => 'SUM([[margin]])',
                'margin_percent' => '(SUM([[margin]]) / GREATEST(SUM([[total_amount]]), 1))',
                'sales' => 'COUNT(DISTINCT([[invoice_id]]))',
            ])
            ->from(ProductTurnover::tableName())
            ->andWhere([
                'company_id' => $this->_company->id,
                'type' => Documents::IO_TYPE_OUT,
                'production_type' => Product::PRODUCTION_TYPE_GOODS,
                'is_invoice_actual' => 1,
                'is_document_actual' => 1,
            ])
            ->andWhere([
                'between',
                'date',
                $this->datePrev->format('Y-m-d'),
                $this->dateEnd->format('Y-m-d'),
            ])
            ->groupBy([
                'year',
                'month',
                'product_id',
            ]);

        $sql = $query->createCommand()->rawSql;

        $comand = $db->createCommand("DROP TEMPORARY TABLE IF EXISTS __turnover_abs");
        $comand->execute();
        $comand = $db->createCommand("CREATE TEMPORARY TABLE __turnover_abs {$sql}");
        $comand->execute();

        $from = (clone $this->dateStart)->modify('first day of -12 month');
        $till = (clone $this->dateStart)->modify('last day of -1 month');
        $query = (new \yii\db\Query())
            ->select([
                'period' => 'DATE_FORMAT([[date]], "%Y%m")',
                'product_id',
                'quantity' => 'SUM([[quantity]])',
            ])
            ->from(ProductTurnover::tableName())
            ->andWhere([
                'company_id' => $this->_company->id,
                'type' => Documents::IO_TYPE_OUT,
                'production_type' => Product::PRODUCTION_TYPE_GOODS,
                'is_invoice_actual' => 1,
                'is_document_actual' => 1,
            ])
            ->andWhere([
                'between',
                'date',
                $from->format('Y-m-d'),
                $till->format('Y-m-d'),
            ])
            ->groupBy([
                'period',
                'product_id',
            ]);

        $sql = $query->createCommand()->rawSql;

        $comand = $db->createCommand("DROP TEMPORARY TABLE IF EXISTS __turnover_xyz");
        $comand->execute();
        $comand = $db->createCommand("CREATE TEMPORARY TABLE __turnover_xyz {$sql}");
        $comand->execute();
    }

    public function search($params = [])
    {
        $this->load($params);

        $this->createTmpTable();

        $select = [
            'p.group_id',
            't.product_id',
            'p.title',
            'p.article',
            'turnover' => 't.total',
            'selling_price' => '({{t}}.[[total]] / {{t}}.[[quantity]])',
            'cost_price' => 't.purchase',
            'purchase_price' => '({{t}}.[[purchase]] / {{t}}.[[quantity]])',
            't.margin',
            't.margin_percent',
            'selling_quantity' => 't.quantity',
            'group_margin_percent' => '({{t}}.[[margin]] / :overallMargin)',
            'number_of_sales' => 't.sales',
            'average_check' => '({{t}}.[[total]] / {{t}}.[[sales]])',
            'first_date' => 'p.created_at',
        ];
        $where1 = [
            't.year' => (int) $this->dateStart->format('Y'),
            't.month' => (int) $this->dateStart->format('n'),
        ];
        $where2 = [
            't.year' => (int) $this->datePrev->format('Y'),
            't.month' => (int) $this->datePrev->format('n'),
        ];

        // OVERALL RESULT
        $overallMargin = $this->overallMargin = (new Query)
            ->from(['t' => '__turnover_abs'])
            ->where($where1)
            ->sum('t.margin', Yii::$app->db2);
        $totalCount = $this->totalCount = (new Query)
            ->from(['t' => '__turnover_abs'])
            ->where($where1)
            ->count('*', Yii::$app->db2);
        /////////////////

        /// ABC /////////////
        $groupA = 1;
        $groupB = 2;
        $groupC = 3;

        $thresholdA = ceil(20/100 * $totalCount);
        $thresholdB = ceil(30/100 * $totalCount);
        $thresholdC = $totalCount - $thresholdA - $thresholdB;


        $xyzQuery = (new Query())
            ->select([
                'product_id',
                'coefficient' => 'STDDEV([[quantity]])/AVG([[quantity]])*100',
            ])
            ->from('__turnover_xyz')
            ->groupBy(['product_id']);

        $query = (new Query)
            ->select($select)
            ->addParams([':overallMargin' => $overallMargin > 0 ? $overallMargin : 9E9])
            ->addSelect([
                'variation_coefficient' => 'x.coefficient',
            ])
            ->addSelect(['abc' => new Expression("CONCAT(
                IF (
                    ROW_NUMBER() OVER ( ORDER BY {{t}}.[[margin]] DESC ) <= {$thresholdA},
                    {$groupA},
                    IF (
                        ROW_NUMBER() OVER ( ORDER BY {{t}}.[[margin]] DESC ) <= {$thresholdB},
                        {$groupB},
                        {$groupC}
                    )
                )
                ,
                IF (
                    ROW_NUMBER() OVER ( ORDER BY {{t}}.[[margin_percent]] DESC ) <= {$thresholdA},
                    {$groupA},
                    IF (
                        ROW_NUMBER() OVER ( ORDER BY {{t}}.[[margin_percent]] DESC ) <= {$thresholdB},
                        {$groupB},
                        {$groupC}
                    )
                )
                ,
                IF (
                    ROW_NUMBER() OVER ( ORDER BY {{t}}.[[quantity]] DESC ) <= {$thresholdA},
                    {$groupA},
                    IF (
                        ROW_NUMBER() OVER ( ORDER BY {{t}}.[[quantity]] DESC ) <= {$thresholdB},
                        {$groupB},
                        {$groupC}
                    )
                )
            )")])
            ->from(['t' => '__turnover_abs'])
            ->leftJoin(['x' => $xyzQuery], '{{x}}.[[product_id]] = {{t}}.[[product_id]]')
            ->leftJoin(['p' => Product::tableName()], '{{p}}.[[id]] = {{t}}.[[product_id]]')
            ->where($where1)
            ->indexBy('product_id');

        $query->andFilterWhere(['t.product_id' => $this->id]);

        if ($this->title) {
            $query->andWhere(['or', ['like', 'title', $this->title], ['like', 'article', $this->title]]);
        }

        $this->_query = clone $query;

        // PREV PERIOD
        $prevQuery = (new Query)
            ->select($select)
            ->addParams([':overallMargin' => $overallMargin > 0 ? $overallMargin : 9E9])
            ->from(['t' => '__turnover_abs'])
            ->leftJoin(['p' => Product::tableName()], '{{p}}.[[id]] = {{t}}.[[product_id]]')
            ->where($where2)
            ->indexBy('product_id');

        $prevQuery->andFilterWhere(['t.product_id' => $this->id]);

        if ($this->title) {
            $prevQuery->andWhere(['or', ['like', 'title', $this->title], ['like', 'article', $this->title]]);
        }

        $this->prevPeriodData = $prevQuery->all(Yii::$app->db2);
        //////////////

        $dataProvider = new ActiveDataProvider([
            'db' => \Yii::$app->db2,
            'query' => $query,
            'totalCount' => $totalCount,
            'pagination' => [
                'pageSize' => 0,
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'title',
                'turnover',
                'selling_price',
                'cost_price',
                'purchase_price',
                'margin',
                'margin_percent',
                'selling_quantity',
                'group_margin_percent',
                'number_of_sales',
                'average_check',
                'variation_coefficient',
                'abc'
            ],
            'defaultOrder' => [
                'abc' => SORT_ASC,
            ],
        ]);

        return $dataProvider;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->_company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    public function setYear($value)
    {
        if (strlen($value) != 6 || !is_numeric($value)) {
            $value = date('Ym');
        }

        $this->_year = $value;

        $year = substr($value, 0, 4);
        $month = substr($value, -2, 2);

        $this->dateStart = date_create_from_format('Y-m-d', "{$year}-{$month}-01");
        $this->datePrev = (clone $this->dateStart)->modify('first day of -1 month');
        $this->dateEnd = (clone $this->dateStart)->modify('last day of');
    }

    public function getYear()
    {
        return $this->_year;
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $range = [];
        $invoiceMinDate = Invoice::find()
            ->byCompany($company->id)
            ->min('document_date');

        $minDates = [];

        if ($invoiceMinDate) $minDates[] = $invoiceMinDate;

        $minCashDate = !empty($minDates) ? min($minDates) : date('Y-m-d');
        $registrationDate = date_create_from_format('Y-m-d', $minCashDate);
        $currentDate = (clone $registrationDate)->modify('first day of this month');

        while ($currentDate->format('Ym') <= date('Ym')) {
            $range[$currentDate->format('Ym')] = ArrayHelper::getValue(AbstractFinance::$month, substr($currentDate->format('Ym'), -2, 2)) . ' ' . substr($currentDate->format('Ym'), 0, 4);
            $currentDate = $currentDate->modify('last day of this month')->setTime(23, 59, 59);
            $currentDate = $currentDate->modify("+1 second");
        }
        $range = array_reverse($range, true);

        return $range;
    }

    ////////////////////////////////////////////// HELPERS ////////////////////////////////////////////////////////////

    private static function _getFromCurrentMonthsPeriods($left, $right, $offsetYear = 0)
    {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month");

        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {

            if ($curr->format('Ym') < date('Ym'))
                $ret[] = $curr->format('Ym');

            $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }

    public static function _getDateFromYM($ym, $endOfMonth = false)
    {
        if (strlen($ym) != 6 || $ym != (int)$ym)
            throw new Exception('_getDateFromYM: $ym not found');

        $year = substr($ym, 0, 4);
        $month = substr($ym, -2, 2);
        $day = ($endOfMonth) ? cal_days_in_month(CAL_GREGORIAN, $month, $year) : "01";

        return "{$year}-{$month}-{$day}";
    }

    public static function getFluidColspan($userConfig, $columnGroup = 'cell-data')
    {
        if ($columnGroup == 'cell-data') {
            return 10 - (
                !$userConfig->report_abc_cost_price +
                !$userConfig->report_abc_group_margin +
                !$userConfig->report_abc_number_of_sales +
                !$userConfig->report_abc_average_check +
                !$userConfig->report_abc_purchase_price +
                !$userConfig->report_abc_selling_price
            );
        }

        return 0;
    }

    public function getGroupsByProducts($products, $calcCoefs = false)
    {
        // fill groups
        $groups = [];
        foreach ($products as $key => $product) {
            //if ($product['group_id'] == 1) { // Без группы
            //
            //    $groups['product_' . $product['product_id']] = $product;
            //
            //} else {
                if (!isset($groups['group_' . $product['group_id']])) {

                    $modelGroup = ProductGroup::findOne($product['group_id']);

                    $groups['group_' . $product['group_id']] = [
                        'group_id' => null,
                        'product_id' => null,
                        'title' => $modelGroup ? $modelGroup->title : '---',
                        'article' => null,
                        'turnover' => 0,
                        'selling_price' => 0,
                        'cost_price' => 0,
                        'purchase_price' => 0,
                        'margin' => 0,
                        'margin_percent' => 0,
                        'selling_quantity' => 0,
                        'group_margin_percent' => 0,
                        'number_of_sales' => 0,
                        'average_check' => 0,
                        'variation_coefficient' => 0,
                        'abc' => '',
                        'first_date' => null,
                        'items' => []
                    ];
                }

                $groups['group_' . $product['group_id']]['items'][] = $product;
            //}
        }

        // calculate totals by group: SUM()
        foreach ($groups as $key => $group) {
            if (isset($group['items'])) {
                foreach(['turnover', 'cost_price', 'margin', 'selling_quantity', 'number_of_sales', 'balance_at_start', 'balance_at_end'] as $attr) {
                    $groups[$key][$attr] = array_sum(array_column($group['items'], $attr));
                }
            }
        }

        // calculate totals by group: AVG()
        $overallMargin = $this->overallMargin;
        foreach ($groups as $key => $group) {
            if (isset($group['items'])) {
                $groups[$key]['selling_price'] = $group['turnover'] / ($group['selling_quantity'] ?: 9E9);
                $groups[$key]['purchase_price'] = $group['cost_price'] / ($group['selling_quantity'] ?: 9E9);
                $groups[$key]['margin_percent'] = $group['margin'] / ($group['turnover'] ?: 9E9);
                $groups[$key]['group_margin_percent'] = $group['margin'] / ($overallMargin ?: 9E9);
                $groups[$key]['average_check'] = $group['turnover'] / ($group['number_of_sales'] ?: 9E9);
                $groups[$key]['variation_coefficient'] = array_sum(array_column($group['items'], 'variation_coefficient')) / count($group['items']);

            }
        }

        if (!$calcCoefs)
            return $groups;

        // calculate ABC
        $totalCount = count($groups);
        $thresholdA = ceil(20/100 * $totalCount);
        $thresholdB = ceil(30/100 * $totalCount);
        $thresholdC = $totalCount - $thresholdA - $thresholdB;

        foreach(['margin', 'margin_percent', 'selling_quantity'] as $attr) {
            $i = 0;
            uasort($groups, function ($a, $b) use ($attr) {
                return $b[$attr] <=> $a[$attr];
            });
            foreach ($groups as $key => &$group) {
                $i++;
                if (empty($group['items']))
                    continue;

                if ($i <= $thresholdA)
                    $group['abc'] .= self::GROUP_A;
                elseif ($i <= $thresholdB)
                    $group['abc'] .= self::GROUP_B;
                else
                    $group['abc'] .= self::GROUP_C;
            }
        }

        // sort
        $sort = Yii::$app->request->get('sort', 'abc');
        if (strlen($sort) != strlen(trim($sort, '-'))) {
            // desc
            $sort = trim($sort, '-');
            uasort($groups, function ($a, $b) use ($sort) {
                return $b[$sort] <=> $a[$sort];
            });
        } else {
            // asc
            uasort($groups, function ($a, $b) use ($sort) {
                return $a[$sort] <=> $b[$sort];
            });
        }

        return $groups;
    }

    public static function getRecommendations($abc, $variationCoef)
    {
        $key = str_replace([1,2,3], ['A', 'B', 'C'], $abc);
        $key .= ($variationCoef <= 10) ? 'X' : ($variationCoef > 25 ? 'Z' : 'Y');

        if (strlen($key) == 4 && strlen(str_replace(['A','B','C','X','Y','Z'], '', $key)) == 0) {

            return ProductRecommendationAbc::find()->where(['key' => $key])->asArray()->one();
        }

        return [];
    }
}
