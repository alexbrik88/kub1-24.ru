<?php namespace frontend\modules\analytics\models\financeDashboard\tabKpi;

use frontend\modules\cash\components\CashStatisticInfo;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboardSearch as BaseSearch;

class CashSearch extends BaseSearch {

    public static function getCashStatisticInfo($showBanks): array {
        return CashStatisticInfo::getPeriodStatisticInfo([], $showBanks);
    }

    public static function getForaignCashStatisticInfo($showBanks): array {
        return CashStatisticInfo::getForeignCurrensyStatistic([], $showBanks);
    }

}