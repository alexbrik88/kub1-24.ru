<?php namespace frontend\modules\analytics\models\financeDashboard\tabKpi;

use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboardSearch as BaseSearch;

class CreditSearch extends BaseSearch {

    /**
     * @return Credit[]
     */
    public function getModels(): array
    {
        return Credit::find()
            ->andWhere(['company_id' => $this->company->id])
            ->andWhere(['<=', 'credit_first_date', $this->dateTo])
            ->andWhere(['>=', 'credit_last_date', $this->dateFrom])
            ->all();
    }
}