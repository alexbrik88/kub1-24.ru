<?php namespace frontend\modules\analytics\models\financeDashboard\tabKpi;

use common\models\currency\Currency;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboardSearch as BaseSearch;

class CurrencySearch extends BaseSearch {

    /**
     * @return Currency[]
     */
    public function getModels(): array
    {
        return Currency::find()->where(['and', ['name' => ['USD', 'EUR']]])->all();
    }

}