<?php namespace frontend\modules\analytics\models\financeDashboard\tabKpi;

use common\modules\analytics\AnalyticsManager;
use frontend\modules\analytics\models\ExpensesSearch;
use frontend\modules\analytics\models\IncomeSearch;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboardSearch as BaseSearch;

class ChartSearch extends BaseSearch {

    public function getIncomeSearchModel(): IncomeSearch
    {
        $model = AnalyticsManager::getIncomeSearchModel();

        return $model;
    }

    public function getExpensesSearchModel(): ExpensesSearch
    {
        $model = AnalyticsManager::getExpensesSearchModel();

        return $model;
    }
}