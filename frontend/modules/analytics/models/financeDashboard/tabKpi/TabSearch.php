<?php namespace frontend\modules\analytics\models\financeDashboard\tabKpi;

use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboardSearch as BaseSearch;

/**
 * @property StatisticsSearch $statistics
 * @property CurrencySearch $currency
 * @property ChartSearch $chart
 * @property CashSearch $cash
 * @property CreditSearch $credit
 * @property DebtorSearch $debtor
 */
class TabSearch extends BaseSearch {

    const CURRENT_TAB = FinanceDashboard::TAB_KPI;

    private ?StatisticsSearch $_statistics = null;
    private ?CurrencySearch $_currency = null;
    private ?ChartSearch $_chart = null;
    private ?CashSearch $_cash = null;
    private ?CreditSearch $_credit = null;
    private ?DebtorSearch $_debtor = null;

    public function getCredit(): CreditSearch {
        if ($this->_credit === null)
            $this->_credit = (new CreditSearch())
                ->setCompany($this->company)
                ->setEmployee($this->employee);

        return $this->_credit;
    }

    public function getStatistics(): StatisticsSearch {
        if ($this->_statistics === null)
            $this->_statistics = (new StatisticsSearch())
                ->setCompany($this->company)
                ->setEmployee($this->employee);

        return $this->_statistics;
    }

    public function getCurrency(): CurrencySearch {
        if ($this->_currency === null)
            $this->_currency = (new CurrencySearch())
                ->setCompany($this->company)
                ->setEmployee($this->employee);

        return $this->_currency;
    }

    public function getChart(): ChartSearch {
        if ($this->_chart === null)
            $this->_chart = (new ChartSearch())
                ->setCompany($this->company)
                ->setEmployee($this->employee);

        return $this->_chart;
    }

    public function getCash(): CashSearch {
        if ($this->_cash === null)
            $this->_cash = (new CashSearch())
                ->setCompany($this->company)
                ->setEmployee($this->employee);

        return $this->_cash;
    }

    public function getDebtor(): DebtorSearch {
        if ($this->_debtor === null)
            $this->_debtor = (new DebtorSearch())
                ->setCompany($this->company)
                ->setEmployee($this->employee);

        return $this->_debtor;
    }
}