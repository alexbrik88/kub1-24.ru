<?php namespace frontend\modules\analytics\models\financeDashboard\tabKpi;

use common\modules\analytics\AnalyticsManager;
use frontend\modules\analytics\models\AnalyticsArticleForm;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboardSearch as BaseSearch;

class StatisticsSearch extends BaseSearch {

    const STATISTICS_REVENUE = 'sells';
    const STATISTICS_EXPENSES = 'expenses';
    const STATISTICS_PROFIT = 'profit';

    public function getPeriodSum(string $type): int
    {
        return $this->_getPalSum($type, $this->dateFrom, $this->dateTo);
    }

    public function getPrevPeriodSum(string $type): int
    {
        return $this->_getPalSum($type, $this->prevDateFrom, $this->prevDateTo);
    }

    public function getTooltipStatistics(string $type): array
    {
        switch ($type) {
            case self::STATISTICS_REVENUE:
                return $this->_getTooltipRevenue();
            case self::STATISTICS_EXPENSES:
                return $this->_getTooltipExpenses();
            case self::STATISTICS_PROFIT:
                return $this->_getTooltipProfit();
            default:
                return [];
        }
    }

    private function _getPalModel()
    {
        return AnalyticsManager::getPalModel(['revenueRuleGroup' => AnalyticsArticleForm::REVENUE_RULE_GROUP_CUSTOMER]);
    }

    private function _getPalSum(string $type, string $dateFrom, string $dateTo): int
    {
        $palModel = self::_getPalModel();

        switch ($type) {
            case self::STATISTICS_REVENUE:
                $palRows = ['totalRevenue'];
                break;
            case self::STATISTICS_EXPENSES:
                $palRows = [
                    'totalFixedCosts',
                    'totalVariableCosts',
                    'totalPrimeCosts',
                    'totalAnotherCosts',
                    'totalAmortization',
                    'totalPaymentPercent',
                    'tax'
                ];
                break;
            case self::STATISTICS_PROFIT:
                $palRows = ['netIncomeLoss'];
                break;
            default:
                $palRows = [];
                break;
        }

        return $palModel->getPeriodSum($palRows, $dateFrom, $dateTo);
    }

    private function _getTooltipRevenue(): array
    {
        $palModel = self::_getPalModel();
        $revenueItems = $palModel->getRevenueDetalizationItems();

        $ret = [];
        foreach ($revenueItems as $palKey => $name) {
            $ret[] = ['name' => $name, 'percent' => null,
                'sum' => $palModel->getPeriodSum([$palKey], $this->dateFrom, $this->dateTo)];
        }

        $ret = array_filter($ret, fn($var) => $var['sum'] ?? 0);
        usort($ret, fn($b, $a) => ($a['sum'] ?? 0) <=> ($b['sum'] ?? 0));

        return $ret;
    }

    private function _getTooltipExpenses(): array
    {
        $palModel = self::_getPalModel();

        return [
            ['name' => 'Себестоимость', 'percent' => null,
                'sum' => $palModel->getPeriodSum(['totalPrimeCosts'], $this->dateFrom, $this->dateTo)],
            ['name' => 'Переменные расходы', 'percent' => null,
                'sum' => $palModel->getPeriodSum(['totalVariableCosts'], $this->dateFrom, $this->dateTo)],
            ['name' => 'Постоянные расходы', 'percent' => null,
                'sum' => $palModel->getPeriodSum(['totalFixedCosts'], $this->dateFrom, $this->dateTo)],
            ['name' => 'Другие расходы', 'percent' => null,
                'sum' => $palModel->getPeriodSum(['totalAnotherCosts'], $this->dateFrom, $this->dateTo)],
            ['name' => 'Амортизация', 'percent' => null,
                'sum' => $palModel->getPeriodSum(['totalAmortization'], $this->dateFrom, $this->dateTo)],
            ['name' => 'Проценты уплаченные', 'percent' => null,
                'sum' => $palModel->getPeriodSum(['totalPaymentPercent'], $this->dateFrom, $this->dateTo)],
            ['name' => 'Налог на прибыль', 'percent' => null,
                'sum' => $palModel->getPeriodSum(['tax'], $this->dateFrom, $this->dateTo)],
        ];
    }

    private function _getTooltipProfit(): array
    {
        $palModel = self::_getPalModel();

        return [
            ['name' => 'Маржин. доход', 'percent' => null,
                'sum' => $palModel->getPeriodSum(['marginalIncome'], $this->dateFrom, $this->dateTo)],
            ['name' => 'Валовая прибыль', 'percent' => null,
                'sum' => $palModel->getPeriodSum(['grossProfit'], $this->dateFrom, $this->dateTo)],
            ['name' => 'Другие доходы', 'percent' => null, 'color' => 'rgba(46,159,191,1)',
                'sum' => $palModel->getPeriodSum(['totalAnotherIncome'], $this->dateFrom, $this->dateTo)],
            ['name' => 'EBIDTA', 'percent' => null,
                'sum' => $palModel->getPeriodSum(['ebitda'], $this->dateFrom, $this->dateTo)],
            ['name' => 'Прибыль до налогообложения', 'percent' => null,
                'sum' => $palModel->getPeriodSum(['profitBeforeTax'], $this->dateFrom, $this->dateTo)],
        ];
    }
}