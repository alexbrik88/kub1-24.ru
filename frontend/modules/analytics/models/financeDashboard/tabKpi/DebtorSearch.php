<?php namespace frontend\modules\analytics\models\financeDashboard\tabKpi;

use frontend\modules\analytics\models\DebtReportSearch2;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboardSearch as BaseSearch;

class DebtorSearch extends BaseSearch {

    public function prepareAndGetModel(int $ioType, string $onDate): DebtReportSearch2
    {
        $model = new DebtReportSearch2();
        $model->search_by = DebtReportSearch2::REPORT_TYPE_MONEY;
        $model->report_type = DebtReportSearch2::SEARCH_BY_INVOICES;
        $model->type = $ioType;
        $model->findDebtor([], $ioType, $onDate);

        return $model;
    }
}