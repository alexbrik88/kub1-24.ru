<?php namespace frontend\modules\analytics\models\financeDashboard\tabFinances;

use common\modules\analytics\AnalyticsManager;
use frontend\modules\analytics\models\AnalyticsArticleForm;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboardSearch as BaseSearch;
use yii\helpers\ArrayHelper;

class StatisticsSearch extends BaseSearch {

    const STATISTICS_REVENUE = 'revenue';
    const STATISTICS_PROFIT = 'profit';
    const STATISTICS_ROIC = 'roic';

    public function getPeriodSum(string $type): int
    {
        if ($type === self::STATISTICS_ROIC)
            return $this->_getRoicSum($type, $this->dateFrom, $this->dateTo);

        return $this->_getPalSum($type, $this->dateFrom, $this->dateTo);
    }

    public function getPrevPeriodSum(string $type): int
    {
        if ($type === self::STATISTICS_ROIC)
            return $this->_getRoicSum($type, $this->prevDateFrom, $this->prevDateTo);

        return $this->_getPalSum($type, $this->prevDateFrom, $this->prevDateTo);
    }

    public function getTooltipStatistics(string $type): array
    {
        switch ($type) {
            case self::STATISTICS_REVENUE:
                return $this->_getTooltipRevenue();
            case self::STATISTICS_PROFIT:
                return $this->_getTooltipProfit();
            default:
                return [];
        }
    }

    public function getCustomTooltipStatistics(string $type): array
    {
        switch ($type) {
            case self::STATISTICS_ROIC:
                return $this->_getCustomTooltipRoic();
        }

        return [];
    }

    private function _getPalModel()
    {
        return AnalyticsManager::getPalModel([
            'revenueRuleGroup' => AnalyticsArticleForm::REVENUE_RULE_GROUP_CUSTOMER,
            'monthGroupBy' => AnalyticsArticleForm::MONTH_GROUP_INDUSTRY // need for charts
        ]);
    }

    private function _getBalanceModel(int $balanceYear)
    {
        return AnalyticsManager::getBalanceModel($balanceYear);
    }

    private function _getRoicSum(string $type, string $dateFrom, string $dateTo): int
    {
        $dateTo = ($dateTo > date('Y-m-d')) ? date('Y-m-d') : $dateTo;

        if ($type == self::STATISTICS_ROIC) {
            $balanceYear = substr($dateTo, 0, 4);
            $balanceModel = self::_getBalanceModel($balanceYear);
            $profitAmount = $this->_getPalSum(self::STATISTICS_PROFIT, $dateFrom, $dateTo);
            $capitalAmount = $balanceModel->getCapital(date_create_from_format('Y-m-d', $dateTo));

            return (int) 100 * $profitAmount / ($capitalAmount ?: 9E99);
        }

        return 0;
    }

    private function _getPalSum(string $type, string $dateFrom, string $dateTo): int
    {
        $palModel = self::_getPalModel();

        switch ($type) {
            case self::STATISTICS_REVENUE:
                $palRows = ['totalRevenue'];
                break;
            case self::STATISTICS_PROFIT:
                $palRows = ['netIncomeLoss'];
                break;
            case self::STATISTICS_ROIC:
            default:
                $palRows = [];
                break;
        }

        return $palModel->getPeriodSum($palRows, $dateFrom, $dateTo);
    }

    private function _getTooltipRevenue(): array
    {
        $palModel = self::_getPalModel();
        $revenueItems = $palModel->getRevenueDetalizationItems();

        $ret = [];
        foreach ($revenueItems as $palKey => $name) {
            $ret[] = ['name' => $name, 'percent' => null,
                'sum' => $palModel->getPeriodSum([$palKey], $this->dateFrom, $this->dateTo)];
        }

        $ret = array_filter($ret, fn($var) => $var['sum'] ?? 0);
        usort($ret, fn($b, $a) => ($a['sum'] ?? 0) <=> ($b['sum'] ?? 0));

        return $ret;
    }

    private function _getCustomTooltipRoic(): array
    {
        $dateTo = ($this->dateTo > date('Y-m-d')) ? date('Y-m-d') : $this->dateTo;

        $ret = [];
        $d = date_create_from_format('Y-m-d', $dateTo);
        for ($i=0; $i<=6; $i++) {

            $balanceYear = $d->format('Y');
            $balanceModel = self::_getBalanceModel($balanceYear);
            $profitAmount = $this->_getPalSum(self::STATISTICS_PROFIT, $d->format('Y-m-d'), $d->format('Y-m-d'));
            $capitalAmount = $balanceModel->getCapital($d);
            $ret[] = [
                'x' => ArrayHelper::getValue(self::$shortMonth, $d->format('m') - 1),
                'y' => (int) 100 * $profitAmount / ($capitalAmount ?: 9E99),
                // temp
                '_year' => $d->format('Y'),
                '_month' => $d->format('m'),
                '_profit' => $profitAmount,
                '_capital' => $capitalAmount
            ];

            $d->modify('first day of')
                ->setTime(0, 0, 0)
                ->modify('-1 second');
        }

        return array_reverse($ret);
    }

    private function _getTooltipProfit(): array
    {
        $palModel = self::_getPalModel();

        return [
            ['name' => 'Маржин. доход', 'percent' => null,
                'sum' => $palModel->getPeriodSum(['marginalIncome'], $this->dateFrom, $this->dateTo)],
            ['name' => 'Валовая прибыль', 'percent' => null,
                'sum' => $palModel->getPeriodSum(['grossProfit'], $this->dateFrom, $this->dateTo)],
            ['name' => 'Другие доходы', 'percent' => null, 'color' => 'rgba(46,159,191,1)',
                'sum' => $palModel->getPeriodSum(['totalAnotherIncome'], $this->dateFrom, $this->dateTo)],
            ['name' => 'EBIDTA', 'percent' => null,
                'sum' => $palModel->getPeriodSum(['ebitda'], $this->dateFrom, $this->dateTo)],
            ['name' => 'Прибыль до налогообложения', 'percent' => null,
                'sum' => $palModel->getPeriodSum(['profitBeforeTax'], $this->dateFrom, $this->dateTo)],
        ];
    }
}