<?php namespace frontend\modules\analytics\models\financeDashboard\tabFinances;

use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboardSearch as BaseSearch;

/**
 * @property StatisticsSearch $statistics
 * @property CurrencySearch $currency
 * @property ChartSearch $chart
 */
class TabSearch extends BaseSearch {

    const CURRENT_TAB = FinanceDashboard::TAB_FINANCES;

    private ?StatisticsSearch $_statistics = null;
    private ?CurrencySearch $_currency = null;
    private ?ChartSearch $_chart = null;

    public function getStatistics(): StatisticsSearch {
        if ($this->_statistics === null)
            $this->_statistics = (new StatisticsSearch())
                ->setCompany($this->company)
                ->setEmployee($this->employee);

        return $this->_statistics;
    }

    public function getCurrency(): CurrencySearch {
        if ($this->_currency === null)
            $this->_currency = (new CurrencySearch())
                ->setCompany($this->company)
                ->setEmployee($this->employee);

        return $this->_currency;
    }

    public function getChart(): ChartSearch {
        if ($this->_chart === null)
            $this->_chart = (new ChartSearch)
                ->setCompany($this->company)
                ->setEmployee($this->employee);

        return $this->_chart;
    }
}