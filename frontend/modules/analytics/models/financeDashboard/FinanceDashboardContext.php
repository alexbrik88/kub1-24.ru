<?php namespace frontend\modules\analytics\models\financeDashboard;
use Yii;

class FinanceDashboardContext implements \yii\base\ViewContextInterface {

    private string $tab = '';

    public function __construct(string $tab)
    {
        $this->tab = $tab;
    }

    public function getViewPath(): string
    {
        return Yii::getAlias('@frontend/modules/analytics/views/finance-dashboard/tab' . ucfirst($this->tab));
    }

    public function showErrorBlock(int $nestedLevel): string
    {
        $text = yii\helpers\Html::tag('span', 'Блок временно недоступен', ['class' => 'text-grey font-14']);
        switch ($nestedLevel) {
            case 2:
                return yii\helpers\Html::tag('div', yii\helpers\Html::tag('div', $text, ['class' => 'dashboard-card wrap']), [
                    'class' => 'dashboard-card-column col-6'
                ]);
            case 1:
                return yii\helpers\Html::tag('div', $text, ['class' => 'dashboard-card wrap']);
            case 0:
            default:
                return $text;
        }
    }
}