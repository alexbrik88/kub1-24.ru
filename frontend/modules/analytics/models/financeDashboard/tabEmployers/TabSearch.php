<?php namespace frontend\modules\analytics\models\financeDashboard\tabEmployers;

use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboardSearch as BaseSearch;

class TabSearch extends BaseSearch {

    const CURRENT_TAB = FinanceDashboard::TAB_EMPLOYERS;
}