<?php

namespace frontend\modules\analytics\models\financeDashboard;

use Yii;
use common\models\Company;
use yii\helpers\Url;

/**
 * This is the model class for table "out_finance_dashboard".
 *
 * @property int $id
 * @property int $company_id
 * @property string $uid
 *
 * @property Company $company
 */
class OutFinanceDashboard extends \yii\db\ActiveRecord
{
    private static ?OutFinanceDashboard $_instance = null;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'out_finance_dashboard';
    }

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'uid'], 'required'],
            [['company_id'], 'integer'],
            [['uid'], 'string', 'max' => 16],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'uid' => 'Uid',
        ];
    }

    public static function findOrCreate(Company $company)
    {
        if (empty(self::$_instance)) {
            self::$_instance = self::findOne(['company_id' => $company->id]);
            if (empty(self::$_instance)) {
                self::$_instance = new self();
                self::$_instance->company_id = $company->id;
                self::$_instance->uid = self::generateUid();
                self::$_instance->save();
            }
        }

        return self::$_instance;
    }

    public static function findByUid($uid = null)
    {
        $uid = $uid ?? Yii::$app->request->get('uid');
        self::$_instance = self::findOne(['uid' => $uid]);
        return self::$_instance;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    public function getLink(?string $tab = null): string
    {
        if (empty($this->uid))
            $this->uid = self::generateUid();

        return Yii::$app->urlManager->createAbsoluteUrl(Url::to(['/out-analytics-dashboard', 'uid' => $this->uid, 'tab' => $tab]));
    }

    public static function generateUid(): string
    {
        $query = new \yii\db\Query;
        $query->select('id')->from(static::tableName())->where('[[uid]]=:uid');

        do {
            $uid = substr(bin2hex(random_bytes(16)), 0, 16);
        } while ($query->params([':uid' => $uid])->exists());

        return $uid;
    }

    public static function refreshLink(Company $company): bool
    {
        $model = self::findOrCreate($company);
        $model->updateAttributes(['uid' => self::generateUid()]);
        return true;
    }
}