<?php namespace frontend\modules\analytics\models\financeDashboard;

use Yii;
use common\models\Company;
use common\models\employee\Employee;
use yii\base\Exception;
use common\models\EmployeeCompany;
use yii\base\Model;
use yii\base\ViewContextInterface;
use yii\helpers\VarDumper;

/**
 * @var FinanceDashboardSearch $search
 * @var FinanceDashboardContext $context
 */
class FinanceDashboard extends Model {

    const TAB_KPI = 'kpi';
    const TAB_TODAY_PAYMENTS = 'today';
    const TAB_SELLS = 'sells';
    const TAB_PROJECTS = 'projects';
    const TAB_MARKETING = 'marketing';
    const TAB_STOCKS = 'stocks';
    const TAB_FINANCES = 'finances';
    const TAB_EMPLOYERS = 'employers';

    const DEBUG_RENDER = 0;
    const RENDER_BLOCK_LAYOUT = 2;
    const RENDER_BLOCK = 1;
    const RENDER_BLOCK_BODY = 0;
    
    const TABS = [
        self::TAB_KPI,
        self::TAB_TODAY_PAYMENTS,
        self::TAB_SELLS,
        self::TAB_PROJECTS,
        self::TAB_MARKETING,
        self::TAB_STOCKS,
        self::TAB_FINANCES,
        self::TAB_EMPLOYERS
    ];

    const TAB_NAME = [
        self::TAB_KPI => 'Основные KPI',
        self::TAB_TODAY_PAYMENTS => 'Платежи на сегодня',
        self::TAB_SELLS => 'Продажи',
        self::TAB_PROJECTS => 'Проекты',
        self::TAB_MARKETING => 'Маркетинг',
        self::TAB_STOCKS => 'Сбыт и запасы',
        self::TAB_FINANCES => 'Финансы',
        self::TAB_EMPLOYERS => 'Сотрудники'
    ];

    /**
     * @var
     */
    public Employee $employee;
    public Company $company;
    public string $tab;

    /**
     *  @var
     */
    private ?FinanceDashboardContext $viewContext = null;

    /**
     * @var array
     */
    private static array $tabSearchModel = [];

    public function __construct(?string $tab = '', ?Employee $employee = null, ?Company $company = null)
    {
        parent::__construct();

        $this->employee = $employee ?: Yii::$app->user->identity;
        $this->company = $company  ?: Yii::$app->user->identity->company;
        $this->tab = self::sanitizeTab($tab);

        if (!$this->employee || !$this->company || !$this->tab)
            throw new Exception('Dashboard init error');
    }

    public static function sanitizeTab(?string $tab): string
    {
        if (empty($tab))
            return self::TAB_KPI;
        if (in_array($tab, self::TABS))
            return $tab;

        return '';
    }

    public function getTab(): string
    {
        return $this->tab;
    }

    public function getEmployee(): Employee
    {
        return $this->employee;
    }

    public function getCompany(): Company
    {
        return $this->company;
    }

    public function getSearchModel(): FinanceDashboardSearch
    {
        return $this->getTabModel('TabSearch');
    }

    public function getContext(): FinanceDashboardContext
    {
        return $this->viewContext ?: new FinanceDashboardContext($this->tab);
    }

    public function renderBlock(yii\web\View $view, string $file, array $params = [], int $nestedLevel = self::RENDER_BLOCK_LAYOUT): string
    {
        if (self::DEBUG_RENDER)
            return $view->render($file, $params, $this->context);

        try {
            return $view->render($file, $params, $this->context);
        } catch (\Throwable $e) {
            Yii::error(VarDumper::dumpAsString($e), 'analytics');
        }

        return $this->context->showErrorBlock($nestedLevel);
    }

    private function getTabModel(string $className = 'TabSearch'): FinanceDashboardSearch
    {
        $model = self::getTabModelInstance($this->tab, $className);
        $model->setCompany($this->company);
        $model->setEmployee($this->employee);
        if ($this->tab == self::TAB_PROJECTS) {
            $model->setProjects(
                (array)Yii::$app->request->cookies->getValue('project_chart_series_user_filter')
            );
        }

        return $model;
    }

    private static function getTabModelInstance(string $tab, string $className = 'TabSearch'): FinanceDashboardSearch
    {
        if (empty(self::$tabSearchModel[$tab])) {
            $namespace = "frontend\\modules\\analytics\\models\\financeDashboard\\tab".ucfirst($tab)."\\";
            $class = $namespace.$className;
            self::$tabSearchModel[$tab] = new $class;
        }

        return self::$tabSearchModel[$tab];
    }
}