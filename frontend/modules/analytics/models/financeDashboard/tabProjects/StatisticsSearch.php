<?php namespace frontend\modules\analytics\models\financeDashboard\tabProjects;

use common\models\project\Project;
use common\modules\analytics\AnalyticsManager;
use frontend\modules\analytics\models\AnalyticsArticleForm;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboardSearch as BaseSearch;

class StatisticsSearch extends BaseSearch {

    const STATISTICS_PROJECTS_COUNT = 'projects_count';
    const STATISTICS_REVENUE = 'revenue';
    const STATISTICS_PROFIT = 'profit';

    private array $_selectedProjectsIds = [];

    public function setProjects(array $projectsIds): void
    {
        $this->_selectedProjectsIds = $projectsIds;
    }

    public function getPeriodSum(string $type): int
    {
        if ($type === self::STATISTICS_PROJECTS_COUNT)
            return 1E2 * $this->_getProjectsCount($this->dateFrom, $this->dateTo);

        return $this->_getPalSum($type, $this->dateFrom, $this->dateTo);
    }

    public function getPrevPeriodSum(string $type): int
    {
        if ($type === self::STATISTICS_PROJECTS_COUNT)
            return 1E2 * $this->_getProjectsCount($this->prevDateFrom, $this->prevDateTo);

        return $this->_getPalSum($type, $this->prevDateFrom, $this->prevDateTo);
    }

    public function getTooltipStatistics(string $type): array
    {
        switch ($type) {
            case self::STATISTICS_PROJECTS_COUNT:
                return $this->_getTooltipProjectsCount();
            case self::STATISTICS_REVENUE:
                return $this->_getTooltipRevenue();
            case self::STATISTICS_PROFIT:
                return $this->_getTooltipProfit();
            default:
                return [];
        }
    }

    private function _getProjectsCount($dateFrom, $dateTo): int
    {
        return (int)Project::find()
            ->where(['company_id' => $this->company->id])
            ->andWhere(['or',
                ['>=', 'project.end_date', $dateFrom],
                //['project.end_date' => null]
            ])
            ->andFilterWhere(['project.id' => $this->_selectedProjectsIds])
            ->count();
    }

    private function _getPalModel()
    {
        return AnalyticsManager::getPalModel([
            'monthGroupBy' => AnalyticsArticleForm::MONTH_GROUP_PROJECT
        ]);
    }

    private function _getPalSum(string $type, string $dateFrom, string $dateTo): int
    {
        switch ($type) {
            case self::STATISTICS_REVENUE:
                $revenueItems = $this->_getPalPeriodSum('totalRevenue', $dateFrom, $dateTo);
                break;
            case self::STATISTICS_PROFIT:
                $revenueItems = $this->_getPalPeriodSum('netIncomeLoss', $dateFrom, $dateTo);
                break;
            default:
                $revenueItems = [];
                break;
        }

        return (int)array_sum($revenueItems);
    }

    private function _getTooltipRevenue(): array
    {
        $revenueItems = $this->_getPalPeriodSum('totalRevenue', $this->dateFrom, $this->dateTo);

        $ret = [];
        foreach ($revenueItems as $projectId => $sum) {
            $ret[] = [
                'name' => Project::find()->where(['id' => $projectId])->select('name')->scalar(),
                'percent' => null,
                'sum' => $sum
            ];
        }

        $ret = array_filter($ret, fn($var) => $var['sum'] ?? 0);
        usort($ret, fn($b, $a) => ($a['sum'] ?? 0) <=> ($b['sum'] ?? 0));

        return $ret;
    }

    private function _getTooltipProjectsCount(): array
    {
        $projectItems = Project::find()
            ->where(['company_id' => $this->company->id])
            ->andWhere(['or',
                ['>=', 'project.end_date', $this->dateFrom],
                //['project.end_date' => null]
            ])
            ->andFilterWhere(['project.id' => $this->_selectedProjectsIds])
            ->select(['id', 'name', 'end_date'])
            ->asArray()
            ->all();

        $ret = [];
        foreach ($projectItems as $p) {
            $dateNow = new \DateTime;
            $dateProjectEnd = date_create_from_format('Y-m-d', $p['end_date']);
            $ret[] = [
                'name' => $p['name'],
                'percent' => null,
                'sum' => 1E2 * date_diff($dateNow, $dateProjectEnd)->format('%r%a')
            ];
        }

        usort($ret, function($a, $b) {
            return ($a['name'] ?? 'Zzz') <=> ($b['name'] ?? 'Zzz');
        });

        usort($ret, function($a, $b) {
            return ($a['sum'] ?? 0) <=> ($b['sum'] ?? 0);
        });

        return $ret;
    }

    private function _getTooltipProfit(): array
    {
        $revenueItems = $this->_getPalPeriodSum('netIncomeLoss', $this->dateFrom, $this->dateTo);

        $ret = [];
        foreach ($revenueItems as $projectId => $sum) {
            $ret[] = [
                'name' => Project::find()->where(['id' => $projectId])->select('name')->scalar(),
                'percent' => null,
                'sum' => $sum
            ];
        }

        $ret = array_filter($ret, fn($var) => $var['sum'] ?? 0);
        usort($ret, fn($b, $a) => ($a['sum'] ?? 0) <=> ($b['sum'] ?? 0));

        return $ret;
    }

    private function _getPalPeriodSum($totalKey, $dateFrom, $dateTo)
    {
        $palModel = self::_getPalModel();
        $items = $palModel->getPeriodSumGroupedByMonth($totalKey, $dateFrom, $dateTo);

        if (isset($items[0]))
            unset($items[0]);

        if ($this->_selectedProjectsIds) {
            foreach ($items as $pid => $amount)
                if (!in_array($pid, $this->_selectedProjectsIds))
                    unset($items[$pid]);
        }

        return $items;
    }
}