<?php namespace frontend\modules\analytics\models\financeDashboard\tabProjects;

use common\models\cash\CashFlowsBase;
use common\models\project\Project;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboardSearch as BaseSearch;
use yii\db\Expression;

/**
 * @property StatisticsSearch $statistics
 * @property ChartSearch $chart
 */
class TabSearch extends BaseSearch {

    const CURRENT_TAB = FinanceDashboard::TAB_PROJECTS;

    private ?StatisticsSearch $_statistics = null;
    private ?ChartSearch $_chart = null;

    private array $_selectedProjectsIds = [];
    private array $_projectsList = [];

    public function setProjects(array $projectsIds): void
    {
        $this->_selectedProjectsIds = $projectsIds;
    }

    public function getStatistics(): StatisticsSearch {
        if ($this->_statistics === null) {
            $this->_statistics = (new StatisticsSearch)
                ->setCompany($this->company)
                ->setEmployee($this->employee);

            if ($this->_selectedProjectsIds)
                $this->_statistics->setProjects($this->_selectedProjectsIds);
        }

        return $this->_statistics;
    }

    public function getChart(): ChartSearch {
        if ($this->_chart === null)
            $this->_chart = (new ChartSearch)
                ->setCompany($this->company)
                ->setEmployee($this->employee);

        return $this->_chart;
    }

    public function getProjectsList(): array
    {
        if (!empty($this->_projectsList))
            return $this->_projectsList;

        $this->_projectsList = Project::find()
            ->leftJoin('olap_flows', 'project.id = olap_flows.project_id')
            ->where(['project.company_id' => $this->company->id])
            ->andWhere(['>=', 'project.end_date', $this->dateFrom])
            ->andWhere(['olap_flows.type' => CashFlowsBase::FLOW_TYPE_INCOME])
            ->select(new Expression('project.name, project.id, SUM(IFNULL(olap_flows.original_amount, 0)) AS amount'))
            ->groupBy('project.id')
            ->indexBy('project.id')
            ->orderBy(['amount' => SORT_DESC])
            ->column();

        if ($this->_selectedProjectsIds)
            foreach ($this->_projectsList as $pId => $pName)
                if (!in_array($pId, $this->_selectedProjectsIds))
                    unset($this->_projectsList[$pId]);

        return $this->_projectsList;
    }

    public function getFullProjectsList(): array
    {
        return Project::find()
            ->where(['company_id' => $this->company->id])
            ->select('name')
            ->orderBy('name')
            ->indexBy('id')
            ->column();
    }
}