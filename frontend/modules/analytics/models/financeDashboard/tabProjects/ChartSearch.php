<?php namespace frontend\modules\analytics\models\financeDashboard\tabProjects;

use common\modules\analytics\AnalyticsManager;
use frontend\modules\analytics\models\AnalyticsArticleForm;
use frontend\modules\analytics\models\AnalyticsSimpleSearch;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboardSearch as BaseSearch;
use frontend\modules\analytics\models\OddsSearch;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;

class ChartSearch extends BaseSearch {

    public function getPalModel(): ProfitAndLossSearchModel
    {
        $model = AnalyticsManager::getPalModel([
            'monthGroupBy' => AnalyticsArticleForm::MONTH_GROUP_INDUSTRY
        ]);

        return $model;
    }

    public function getOddsModel(): OddsSearch
    {
        $model = AnalyticsManager::getOddsModel();

        return $model;
    }

    public function getAnalyticsSimpleSearchModel(): AnalyticsSimpleSearch
    {
        $model = AnalyticsManager::getAnalyticsSimpleSearchModel($this->company);

        return $model;
    }
}