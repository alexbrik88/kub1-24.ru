<?php namespace frontend\modules\analytics\models\financeDashboard;

use common\components\date\DateHelper;
use frontend\components\StatisticPeriod;
use frontend\modules\analytics\models\AbstractFinance;
use yii\base\Model;
use common\models\Company;
use common\models\employee\Employee;
use yii\helpers\ArrayHelper;

/**
 * @property string $dateFrom
 * @property string $dateTo
 * @property string $prevDateFrom
 * @property string $prevDateTo
 * @property string $subtitlePeriod
 * @property string $subtitleLastDate
 */
class FinanceDashboardSearch extends Model {

    /**
     * @var
     */
    protected Employee $employee;
    protected Company $company;

    private static array $dateRange = [
        'from' => null,
        'to' => null
    ];

    public function setEmployee(Employee $employee): self {
        $this->employee = $employee;
        return $this;
    }

    public function setCompany(Company $company): self {
        $this->company = $company;
        return $this;
    }

    public function setProjects(array $projectsIds): void {
        // see tabProjects\TabSearch
    }

    public function setDateRange($from, $to): bool
    {
        self::$dateRange['from'] = $from;
        self::$dateRange['to'] = $to;
        return true;
    }

    public function getDateRange(): array
    {
        return (self::$dateRange['from'] && self::$dateRange['to'])
            ? self::$dateRange
            : StatisticPeriod::getSessionPeriod();
    }

    public function getDateFrom(): string
    {
        return $this->getDateRange()['from'];
    }

    public function getDateTo(): string
    {
        return $this->getDateRange()['to'];
    }

    public function getPrevDateFrom(): string
    {
        $diff = $this->getDatesDiffInMonths();
        $diff++;

        return (date_create_from_format('Y-m-d', $this->dateFrom))
            ->modify("-{$diff} month")
            ->modify('first day of')
            ->format('Y-m-d');
    }

    public function getPrevDateTo(): string
    {
        $diff = $this->getDatesDiffInMonths();
        $diff++;

        return (date_create_from_format('Y-m-d', $this->dateTo))
            ->modify('first day of')
            ->modify("-{$diff} month")
            ->modify('last day of')
            ->format('Y-m-d');
    }

    public function getDatesDiffInMonths(): int
    {
        $from = date_create_from_format('Y-m-d', $this->dateFrom)->modify('first day of');
        $to = date_create_from_format('Y-m-d', $this->dateTo)->modify('first day of');
        return (int)$from->diff($to)->format('%m');
    }

    public function getSubtitlePeriod(): string
    {
        $from = $this->getDateFrom();
        $to = $this->getDateTo();
        $now = new \DateTime;

        $period = null;
        $defaultPeriod = 'за период';

        $isMonth = substr($from, 0, 7) === substr($to, 0, 7);
        $isQuarter = substr($from, 0, 4) === substr($to, 0, 4)
            && ceil(substr($from, 5, 2) / 3) === ceil(substr($to, 5, 2) / 3);
        $isYear = substr($from, 0, 4) === substr($to, 0, 4)
            && substr($from, 5, 2) === '01' && substr($to, 5, 2) === '12';

        if ($isMonth)
            $period = mb_strtolower(ArrayHelper::getValue(AbstractFinance::$month, substr($to, 5, 2)) . ' ' . substr($to, 0, 4));
        elseif ($isQuarter)
            $period = ceil(substr($to, 5, 2) / 3) . ' кв. ' . substr($to, 0, 4);
        elseif ($isYear)
            $period = substr($to, 0, 4);

        return ($period) ? " за {$period} г." : " {$defaultPeriod}";
    }

    public function getSubtitleLastDate(): string
    {
        $lastDate = ($this->dateTo <= date('Y-m-d')) ? $this->dateTo : date('Y-m-d');
        return DateHelper::format($lastDate, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
    }

    public function getChartCurrentOffset(string $period, $left = 0, $right = 0): int
    {
        $toYear = substr($this->dateTo, 0, 4);
        $toMonth = substr($this->dateTo, 5, 2);

        if ($period === "months" && $toYear < date('Y')) {
            $curr = new \DateTime();
            $new = date_create_from_format('Y-m-d', $this->dateTo);
            return 12 * date_diff($curr, $new)->format('%r%y') + 1 * date_diff($curr, $new)->format('%r%m') - $right;
        }

        if ($period === "days" && $toYear < date('Y')) {
            $curr = new \DateTime();
            $new = date_create_from_format('Y-m-d', $this->dateTo);
            return date_diff($curr, $new)->format('%r%a') - $right;
        }

        return 0;
    }

    public static $shortMonth = ['янв', 'фев', 'март', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'];
}