<?php namespace frontend\modules\analytics\models\financeDashboard\tabSells;

use frontend\models\Documents;
use frontend\modules\analytics\models\debtor\DebtorHelper2;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboardSearch as BaseSearch;
use yii\db\Query;

/**
 * Class IndicatorSearch
 * @package frontend\modules\analytics\models\financeDashboard\tabSells
 * @property Query $_query
 */
class IndicatorSearch extends BaseSearch {

    public function getTotalBuyersCount(): int
    {
        return (int)
            (new Query)->from($this->_query)->count('DISTINCT contractor_id');
    }

    public function getPaidBuyersCount(): int
    {
        return (int)
            (new Query)->from($this->_query)->having('flow_sum >= invoice_sum')->count('DISTINCT contractor_id');
    }

    public function getPaidBuyersAmount(): int
    {
        return (int)
            (new Query)->from($this->_query)->sum('flow_sum');
    }

    public function getAvgPaymentDelay(): int
    {
        return (int)
            (new Query)->from($this->_query)->select(new \yii\db\Expression('avg(datediff(flow_date, invoice_date))'))->scalar();
    }

    // todo: to run this query only one time you can make tmp table
    protected function get_query()
    {
        $companyId = $this->company->id;
        return DebtorHelper2::getQueryPeriodsAvg([$companyId], Documents::IO_TYPE_OUT, $this->dateFrom, $this->dateTo);
    }
}