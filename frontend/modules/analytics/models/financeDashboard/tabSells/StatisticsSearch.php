<?php namespace frontend\modules\analytics\models\financeDashboard\tabSells;

use common\models\cash\CashFlowsBase;
use common\models\contractor\ContractorHelper;
use common\models\document\InvoiceIncomeItem;
use common\modules\analytics\AnalyticsManager;
use frontend\modules\analytics\models\AnalyticsArticleForm;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboardSearch as BaseSearch;

class StatisticsSearch extends BaseSearch {

    const STATISTICS_REVENUE = 'sells';
    const STATISTICS_MONEY = 'money';
    const STATISTICS_MARGIN = 'margin';

    private array $_cache = [self::STATISTICS_MONEY => []];

    public function getPeriodSum(string $type): int
    {
        return $this->_getPalSum($type, $this->dateFrom, $this->dateTo);
    }

    public function getPrevPeriodSum(string $type): int
    {
        return $this->_getPalSum($type, $this->prevDateFrom, $this->prevDateTo);
    }

    public function getTooltipStatistics(string $type): array
    {
        switch ($type) {
            case self::STATISTICS_REVENUE:
                return $this->_getTooltipRevenue();
            case self::STATISTICS_MONEY:
                return $this->_getTooltipMoney();
            case self::STATISTICS_MARGIN:
                return $this->_getTooltipMargin();
            default:
                return [];
        }
    }

    private function _getPalModel()
    {
        return AnalyticsManager::getPalModel([
            'revenueRuleGroup' => AnalyticsArticleForm::REVENUE_RULE_GROUP_CUSTOMER
        ]);
    }

    private function _getOddsModel()
    {
        return AnalyticsManager::getOddsModel();
    }

    private function _getPalSum(string $type, string $dateFrom, string $dateTo): int
    {
        $palModel = self::_getPalModel();
        $oddsModel = self::_getOddsModel();

        switch ($type) {
            case self::STATISTICS_REVENUE:
                return $palModel->getPeriodSum(['totalRevenue'], $dateFrom, $dateTo);
            case self::STATISTICS_MONEY:
                $contractorsMoneyArray = &$this->_cache[self::STATISTICS_MONEY];
                if (empty($contractorsMoneyArray[md5($dateFrom.$dateTo)]))
                    $contractorsMoneyArray[md5($dateFrom.$dateTo)] = $oddsModel->getFlowsPeriodSumByContractors( CashFlowsBase::FLOW_TYPE_INCOME, $dateFrom, $dateTo, [
                        InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER,
                        InvoiceIncomeItem::ITEM_AGENT_FEE
                    ]);
                return (int)array_sum(array_column($contractorsMoneyArray[md5($dateFrom.$dateTo)], 'amount'));
            case self::STATISTICS_MARGIN:
                return $palModel->getPeriodSum(['marginalIncome'], $dateFrom, $dateTo);
        }

        return 0;
    }

    private function _getTooltipRevenue(): array
    {
        $palModel = self::_getPalModel();
        $revenueItems = $palModel->getRevenueDetalizationItems();

        $ret = [];
        foreach ($revenueItems as $palKey => $name) {
            $ret[] = ['name' => $name, 'percent' => null,
                'sum' => $palModel->getPeriodSum([$palKey], $this->dateFrom, $this->dateTo)];
        }

        $ret = array_filter($ret, fn($var) => $var['sum'] ?? 0);
        usort($ret, fn($b, $a) => ($a['sum'] ?? 0) <=> ($b['sum'] ?? 0));

        return $ret;
    }

    private function _getTooltipMoney(): array
    {
        $contractorsMoneyArray = &$this->_cache[self::STATISTICS_MONEY];
        $ret = [];
        foreach (($contractorsMoneyArray[md5($this->dateFrom.$this->dateTo)] ?? []) as $key => $data) {
            if ($key >= 10) break;
            $ret[] = ['name' => ContractorHelper::getShortNameById($data['contractor_id']), 'percent' => null,
                'sum' => $data['amount']];
        }

        $ret = array_filter($ret, fn($var) => $var['sum'] ?? 0);
        usort($ret, fn($b, $a) => ($a['sum'] ?? 0) <=> ($b['sum'] ?? 0));

        return $ret;
    }

    private function _getTooltipMargin(): array
    {
        $palModel = self::_getPalModel();

        return [
            ['name' => 'Маржин. доход', 'percent' => null,
                'sum' => $palModel->getPeriodSum(['marginalIncome'], $this->dateFrom, $this->dateTo)],
            ['name' => 'Валовая прибыль', 'percent' => null,
                'sum' => $palModel->getPeriodSum(['grossProfit'], $this->dateFrom, $this->dateTo)],
            ['name' => 'Другие доходы', 'percent' => null, 'color' => 'rgba(46,159,191,1)',
                'sum' => $palModel->getPeriodSum(['totalAnotherIncome'], $this->dateFrom, $this->dateTo)],
            ['name' => 'EBIDTA', 'percent' => null,
                'sum' => $palModel->getPeriodSum(['ebitda'], $this->dateFrom, $this->dateTo)],
            ['name' => 'Прибыль до налогообложения', 'percent' => null,
                'sum' => $palModel->getPeriodSum(['profitBeforeTax'], $this->dateFrom, $this->dateTo)],
        ];
    }
}