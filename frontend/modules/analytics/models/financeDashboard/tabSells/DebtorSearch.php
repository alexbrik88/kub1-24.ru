<?php namespace frontend\modules\analytics\models\financeDashboard\tabSells;

use frontend\modules\analytics\models\DebtReportSearch2;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboardSearch as BaseSearch;

class DebtorSearch extends BaseSearch {

    public function prepareAndGetModel(int $ioType, string $onDate): DebtReportSearch2
    {
        $model = new DebtReportSearch2();
        $model->report_type = DebtReportSearch2::REPORT_TYPE_MONEY;
        $model->search_by = DebtReportSearch2::SEARCH_BY_DOCUMENTS;
        $model->type = $ioType;
        $model->findDebtor([], $ioType, $onDate);

        return $model;
    }
}