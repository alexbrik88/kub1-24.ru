<?php namespace frontend\modules\analytics\models\financeDashboard\tabSells;

use common\modules\analytics\AnalyticsManager;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboardSearch as BaseSearch;
use frontend\modules\analytics\models\IncomeSearch;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;

class ChartSearch extends BaseSearch {

    public function getPalModel(): ProfitAndLossSearchModel
    {
        $model = AnalyticsManager::getPalModel();

        return $model;
    }

    public function getIncomeSearchModel(): IncomeSearch
    {
        $model = AnalyticsManager::getIncomeSearchModel();

        return $model;
    }
}