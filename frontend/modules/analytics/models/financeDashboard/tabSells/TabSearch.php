<?php namespace frontend\modules\analytics\models\financeDashboard\tabSells;

use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboardSearch as BaseSearch;

/**
 * @property StatisticsSearch $statistics
 * @property ChartSearch $chart
 * @property DebtorSearch $debtor
 * @property IndicatorSearch $indicators
 * @property CurrencySearch $currency
 */
class TabSearch extends BaseSearch {

    const CURRENT_TAB = FinanceDashboard::TAB_SELLS;

    private ?StatisticsSearch $_statistics = null;
    private ?ChartSearch $_chart = null;
    private ?DebtorSearch $_debtor = null;
    private ?IndicatorSearch $_indicators = null;
    private ?CurrencySearch $_currency = null;

    public function getStatistics(): StatisticsSearch
    {
        if ($this->_statistics === null)
            $this->_statistics = (new StatisticsSearch)
                ->setCompany($this->company)
                ->setEmployee($this->employee);

        return $this->_statistics;
    }

    public function getChart(): ChartSearch {
        if ($this->_chart === null)
            $this->_chart = (new ChartSearch)
                ->setCompany($this->company)
                ->setEmployee($this->employee);

        return $this->_chart;
    }

    public function getDebtor(): DebtorSearch {
        if ($this->_debtor === null)
            $this->_debtor = (new DebtorSearch)
                ->setCompany($this->company)
                ->setEmployee($this->employee);

        return $this->_debtor;
    }

    public function getIndicators(): IndicatorSearch {
        if ($this->_indicators === null)
            $this->_indicators = (new IndicatorSearch)
                ->setCompany($this->company)
                ->setEmployee($this->employee);

        return $this->_indicators;
    }

    public function getCurrency(): CurrencySearch {
        if ($this->_currency === null)
            $this->_currency = (new CurrencySearch())
                ->setCompany($this->company)
                ->setEmployee($this->employee);

        return $this->_currency;
    }
}