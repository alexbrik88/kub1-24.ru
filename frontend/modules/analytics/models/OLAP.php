<?php

namespace frontend\modules\analytics\models;

use common\components\date\DateHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use frontend\modules\cash\models\CashContractorType;
use yii\base\Model;
use yii\db\Query;

class OLAP extends Model {

    const MIN_REPORT_DATE = '2000-01-01';

    const TABLE_OLAP_DOCS = 'olap_documents';
    const TABLE_OLAP_FLOWS = 'olap_flows';
    const TABLE_OLAP_INVOICES = 'olap_invoices';
    const TABLE_PRODUCT_TURNOVER = 'product_turnover';

    // need except this contractor_id in all revenue-like queries
    public static $ownFlowContractorType = [
        '"' . CashContractorType::BANK_TEXT . '"',
        '"' . CashContractorType::ORDER_TEXT . '"',
        '"' . CashContractorType::EMONEY_TEXT . '"',
        '"' . CashContractorType::BALANCE_TEXT . '"'
    ];

    /**
     * @var AnalyticsMultiCompanyManager
     */
    public $multiCompanyManager;

    public $year;

    /**
     * @var
     */
    protected $_multiCompanyIds;
    protected $_accountsCurrencyList;

    const EMPTY_YEAR = ['01' => 0, '02' => 0, '03' => 0, '04' => 0, '05' => 0, '06' => 0, '07' => 0, '08' => 0, '09' => 0, '10' => 0, '11' => 0, '12' => 0];
    private static $_EMPTY_YEAR_IN_DAYS = [];

    public function init()
    {
        parent::init();

        $this->year = min(
            \Yii::$app->session->get('modules.reports.finance.year', date('Y')),
            date('Y'));

        $this->multiCompanyManager = \Yii::$app->multiCompanyManager;
        $this->_multiCompanyIds = $this->multiCompanyManager->getCompaniesIds();
        $this->_accountsCurrencyList = self::getAccountsCurrencyList($this->_multiCompanyIds);
    }

    /**
     * @param $year
     * @return string
     */
    public static function getSeq($year)
    {
        return 'seq_0_to_' . ($year == date('Y') ? (date('n') - 1) : 11);
    }

    /**
     * @param $year
     * @return string
     */
    public static function getInterval($year)
    {
        $startDate = $year . '-01-31';

        if ($year == date('Y')) {
            $lastDate = date('Y-m-d');
            $replacedDate = date('Y-m-t');

            return "IF (('{$startDate}' + INTERVAL (seq) MONTH) = '{$replacedDate}', '{$lastDate}', ('{$startDate}' + INTERVAL (seq) MONTH))";
        }

        return "('{$startDate}' + INTERVAL (seq) MONTH)";
    }

    /**
     * @param $year
     * @return string
     */
    public static function getIntervalLastInFuture($year)
    {
        $startDate = $year . '-01-31';

        if ($year == date('Y')) {
            $lastDate = date('2999-m-01');
            $replacedDate = date('Y-m-t');

            return "IF (('{$startDate}' + INTERVAL (seq) MONTH) = '{$replacedDate}', '{$lastDate}', ('{$startDate}' + INTERVAL (seq) MONTH))";
        }

        return "('{$startDate}' + INTERVAL (seq) MONTH)";
    }

    public static function getEmptyYearInDays($year)
    {
        if (empty(self::$_EMPTY_YEAR_IN_DAYS)) {
            for ($m=1; $m<=12; $m++) {
                $month = str_pad($m, 2, "0", STR_PAD_LEFT);
                $daysCount = (new \DateTime("{$year}{$month}01"))->format('t');
                for ($d=1; $d<=$daysCount; $d++) {
                    $day = str_pad($d, 2, "0", STR_PAD_LEFT);
                    self::$_EMPTY_YEAR_IN_DAYS[$month.$day] = 0;
                }
            }
        }

        return self::$_EMPTY_YEAR_IN_DAYS;
    }

    public function getFromCurrentMonthsPeriods($left, $right, $offsetYear = 0, $offsetMonth = "0") {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month")->modify("{$offsetMonth} month");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr->format('Y-m-01'),
                'to' => $curr->format('Y-m-t'),
            ];
            $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }

    public function getFromCurrentDaysPeriods($left, $right, $offsetYear = 0, $offsetDay = "0") {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} days")->modify("{$offsetDay} days");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr->format('Y-m-d'),
                'to' => $curr->format('Y-m-d'),
            ];
            $curr = $curr->modify("+1 days");
        }

        return $ret;
    }

    public static function getAccountsCurrencyList($companyIds)
    {
        return (new Query)->select('currency_name')->distinct()->from('{{%olap_flows}}')->where([
            'company_id' => $companyIds,
        ])->column();
    }

    public function getFromCurrentMonthsDates($left, $right, $offsetYear = 0, $offsetMonth = "0") {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month")->modify("{$offsetMonth} month");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = clone ($curr->modify('last day of this month')->setTime(23, 59, 59));
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        $range = [];
        $minDates = [];

        $cashOrderMinDate = CashOrderFlows::find()
            ->byCompany($this->_multiCompanyIds)
            ->min('date');
        $cashBankMinDate = CashBankFlows::find()
            ->byCompany($this->_multiCompanyIds)
            ->min('date');
        $cashEmoneyMinDate = CashEmoneyFlows::find()
            ->byCompany($this->_multiCompanyIds)
            ->min('date');

        if ($cashOrderMinDate)
            $minDates[] = $cashOrderMinDate;
        if ($cashBankMinDate)
            $minDates[] = $cashBankMinDate;
        if ($cashEmoneyMinDate)
            $minDates[] = $cashEmoneyMinDate;

        $minCashDate = !empty($minDates) ? max(self::MIN_REPORT_DATE, min($minDates)) : date(DateHelper::FORMAT_DATE);
        $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
        $currentYear = date('Y');
        foreach (range($registrationYear, $currentYear) as $value) {
            $range[$value] = $value;
        }
        arsort($range);

        return $range;
    }
}