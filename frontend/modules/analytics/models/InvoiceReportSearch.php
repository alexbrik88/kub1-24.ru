<?php

namespace frontend\modules\analytics\models;

use common\components\date\DateHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlowToInvoice;
use common\models\cash\CashOrderFlowToInvoice;
use common\models\cash\CashBankFlowToInvoice;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFactory;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\Contractor;
use common\models\company\CompanyType;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\InvoiceInToInvoiceOut;
use common\models\document\PackingList;
use common\models\document\query\InvoiceQuery;
use common\models\document\status\InvoiceStatus;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\file\File;
use common\models\product\Product;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\cash\models\CashContractorType;
use frontend\rbac\permissions;
use Mpdf\Tag\Q;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use frontend\modules\documents\models\InvoiceSearch;

/**
 * InvoiceSearch represents the model behind the search form about `common\models\Invoice`.
 *
 * @property ActiveQuery $query
 * @property ActiveQuery $wrapQuery
 */
class InvoiceReportSearch extends Invoice
{
    public $dateRange;
    public $paymentType;
    public $payment_form_id;
    public $flow_way;
    public $flow_date;
    public $flow_sum;

    public $paymentTypeItems = [
        '' => 'Все',
        CashFactory::TYPE_BANK => 'Банк',
        CashFactory::TYPE_ORDER => 'Касса',
        CashFactory::TYPE_EMONEY => 'E-money',
        -1 => 'Не оплачен',
    ];

    public $paymentTypeNewClients = [
        '' => 'Все',
        CashFactory::TYPE_BANK => 'Банк',
        CashFactory::TYPE_ORDER => 'Касса',
        CashFactory::TYPE_EMONEY => 'E-money',
    ];

    public $flowWays = [
        CashFactory::TYPE_BANK => 'Банк',
        CashFactory::TYPE_ORDER => 'Касса',
        CashFactory::TYPE_EMONEY => 'E-money',
    ];

    // by linked invoices report
    protected $wrapQuery;
    public $defaultOrderStr;
    public $defaultOrderArr;
    public $in_contractor_id;
    public $out_contractor_id;
    public $in_invoice_status_id;
    public $out_invoice_status_id;

    protected $query;
    protected $notPayedStatisticData = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['document_date', 'document_number', 'contractor_id', 'invoice_status_id', 'paymentType',
                'document_author_id', 'payment_form_id', 'responsible_employee_id'], 'integer'],
            [['total_amount_with_nds'], 'number'],
            [['payment_limit_date', 'sort_index', 'flow_way'], 'safe'],
            [['in_contractor_id', 'out_contractor_id', 'in_invoice_status_id', 'out_invoice_status_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveQuery
     */
    public function queryAddPaymentType($query)
    {
        $tInvoice = self::tableName();
        $bankFlow = CashBankFlowToInvoice::tableName();
        $orderFlow = CashOrderFlows::tableName();
        $emoneyFlow = CashEmoneyFlows::tableName();
        $statusPayed = InvoiceStatus::STATUS_PAYED;

        $query
            ->leftJoin($bankFlow, "$tInvoice.id = $bankFlow.invoice_id")
            ->leftJoin($orderFlow, "$tInvoice.id = $orderFlow.invoice_id")
            ->leftJoin($emoneyFlow, "$tInvoice.id = $emoneyFlow.invoice_id")
            ->addSelect(["IF($tInvoice.invoice_status_id = $statusPayed, IF($bankFlow.flow_id IS NOT NULL, 1, IF($orderFlow.id IS NOT NULL, 2, IF($emoneyFlow.id IS NOT NULL, 3, NULL))), -1) AS paymentType"]);

        if (Yii::$app->user->identity->currentRoleId == EmployeeRole::ROLE_MANAGER) {
            $query->andWhere([
                'or',
                ['invoice.document_author_id' => Yii::$app->user->id],
                ['contractor.responsible_employee_id' => Yii::$app->user->id],
            ]);
        }

        return $query;
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @param $dateRange
     * @return ActiveDataProvider
     */
    public function created($params, $dateRange = null)
    {
        $tInvoice = self::tableName();
        $tContractor = Contractor::tableName();
        $tType = CompanyType::tableName();

        /* @var InvoiceQuery $query */
        $query = self::find()
            ->select(["`$tInvoice`.*"])
            ->leftJoin($tContractor, "`$tInvoice`.`contractor_id` = `$tContractor`.`id`")
            ->leftJoin($tType, "`$tContractor`.`company_type_id` = `$tType`.`id`")
            ->byCompany($this->company_id)
            ->byIOType($this->type)
            ->byDeleted(false)
            ->andWhere(['not', ["$tInvoice.invoice_status_id" => InvoiceStatus::STATUS_REJECTED]])
            ->andWhere(['between', "$tInvoice.document_date", $this->dateRange['from'], $this->dateRange['to']])
            ->groupBy('invoice.id');

        $query = $this->queryAddPaymentType($query);

        if (!Yii::$app->user->can(permissions\document\Document::VIEW)) {
            $query->andWhere(["$tInvoice.document_author_id" => Yii::$app->user->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'document_date' => [
                        'asc' => ["`$tInvoice`.`document_date`" => SORT_ASC],
                        'desc' => ["`$tInvoice`.`document_date`" => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                    'document_number' => [
                        'asc' => ["`$tInvoice`.`document_number` * 1" => SORT_ASC],
                        'desc' => ["`$tInvoice`.`document_number` * 1" => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                    'total_amount_with_nds' => [
                        'asc' => ["`$tInvoice`.`total_amount_with_nds`" => SORT_ASC],
                        'desc' => ["`$tInvoice`.`total_amount_with_nds`" => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                    'contractor_id' => [
                        'asc' => [
                            "ISNULL(`$tType`.`name_short`)" => SORT_ASC,
                            "$tType.name_short" => SORT_ASC,
                            "$tContractor.name" => SORT_ASC,
                        ],
                        'desc' => [
                            "ISNULL(`$tType`.`name_short`)" => SORT_DESC,
                            "$tType.name_short" => SORT_DESC,
                            "$tContractor.name" => SORT_DESC,
                        ],
                        'default' => SORT_ASC
                    ],
                ],
                'defaultOrder' => [
                    'contractor_id' => SORT_ASC,
                    'document_date' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        $query->andFilterWhere([
            "$tInvoice.contractor_id" => $this->contractor_id,
            "$tInvoice.document_author_id" => $this->document_author_id,
        ]);

        if ($this->paymentType) {
            $query->having(['paymentType' => $this->paymentType]);
        }

        $this->query = $query;

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @param $dateRange
     * @return ActiveDataProvider
     */
    public function createdStatistic()
    {
        $tInvoice = Invoice::tableName();
        $tEmployee = Employee::tableName();

        return Employee::find()
            ->select([
                "$tEmployee.id",
                "$tEmployee.lastname",
                "$tEmployee.firstname",
                "$tEmployee.patronymic",
                "IFNULL(SUM(`allInvoices`.`total_amount_with_nds`), 0) AS all_total_amount",
                "IFNULL(SUM(`payed`.`total_amount_with_nds`), 0) - IFNULL(SUM(`payed`.`remaining_amount`), 0) AS all_payed_amount",
                "IFNULL(SUM(`notpayed`.`total_amount_with_nds`), 0) - IFNULL(SUM(`notpayed`.`payment_partial_amount`), 0) AS all_nopayed_amount",
            ])
            ->leftJoin(['allInvoices' => $this->query], "$tEmployee.id = allInvoices.document_author_id")
            ->leftJoin("$tInvoice AS payed", "allInvoices.id = payed.id AND (payed.invoice_status_id IN (:payed, :payed_partial) OR
             (payed.invoice_status_id = :overdue AND payed.remaining_amount IS NOT NULL))", [
                ':payed' => InvoiceStatus::STATUS_PAYED,
                ':payed_partial' => InvoiceStatus::STATUS_PAYED_PARTIAL,
                ':overdue' => InvoiceStatus::STATUS_OVERDUE,
            ])
            ->leftJoin("$tInvoice AS notpayed", "allInvoices.id = notpayed.id AND notpayed.invoice_status_id != :payed", [':payed' => InvoiceStatus::STATUS_PAYED])
            ->andWhere(['not', ['allInvoices.id' => null]])
            ->groupBy("$tEmployee.id")
            ->orderBy([
                "$tEmployee.lastname" => SORT_ASC,
                "$tEmployee.firstname" => SORT_ASC,
                "$tEmployee.patronymic" => SORT_ASC,
            ])
            ->asArray()
            ->all();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @param $dateRange
     * @return ActiveDataProvider
     */
    public function payed($params, $dateRange = null)
    {
        $paySelect = ['link.amount', 'link.invoice_id', 'flow.date'];
        $payWhere = [
            'and',
            ['flow.company_id' => $this->company_id],
            ['flow.flow_type' => CashFlowsBase::FLOW_TYPE_INCOME],
            ['between', 'flow.date', $this->dateRange['from'], $this->dateRange['to']],
        ];
        $payQuery = new Query;
        $payQuery->select([
            'invoice_id',
            'flow_way',
            'flow_date' => 'date',
            'flow_sum' => 'SUM([[amount]])',
        ])->from(['t' => CashFlowsBase::getAllFlows($paySelect, $payWhere, true, 'innerJoin')])->groupBy('t.invoice_id');

        /* @var InvoiceQuery $query */
        $query = self::find()->alias('invoice')
            ->select(['invoice.*', 'pay.*'])
            ->innerJoin(['pay' => $payQuery], "{{invoice}}.[[id]] = {{pay}}.[[invoice_id]]")
            ->leftJoin(['contractor' => Contractor::tableName()], "{{invoice}}.[[contractor_id]] = {{contractor}}.[[id]]")
            ->leftJoin(['compType' => CompanyType::tableName()], "{{contractor}}.[[company_type_id]] = {{compType}}.[[id]]")
            ->andWhere([
                'invoice.company_id' => $this->company->id,
                'invoice.type' => $this->type,
                'invoice.is_deleted' => false,
                'invoice.invoice_status_id' => InvoiceStatus::$validInvoices,
            ])
            ->groupBy('invoice.id');

        if (!Yii::$app->user->can(permissions\document\Document::VIEW)) {
            $query->andWhere(["invoice.document_author_id" => Yii::$app->user->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'document_date' => [
                        'asc' => ["invoice.document_date" => SORT_ASC],
                        'desc' => ["invoice.document_date" => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                    'document_number' => [
                        'asc' => ["({{invoice}}.[[document_number]] * 1)" => SORT_ASC],
                        'desc' => ["({{invoice}}.[[document_number]] * 1)" => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                    'total_amount_with_nds' => [
                        'asc' => ["invoice.total_amount_with_nds" => SORT_ASC],
                        'desc' => ["invoice.total_amount_with_nds" => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                    'contractor_id' => [
                        'asc' => [
                            "ISNULL(compType.name_short)" => SORT_ASC,
                            "compType.name_short" => SORT_ASC,
                            "contractor.name" => SORT_ASC,
                        ],
                        'desc' => [
                            "ISNULL(compType.name_short)" => SORT_DESC,
                            "compType.name_short" => SORT_DESC,
                            "contractor.name" => SORT_DESC,
                        ],
                        'default' => SORT_ASC
                    ],
                    'invoice_status_updated_at' => [
                        'asc' => ["invoice.invoice_status_updated_at" => SORT_ASC],
                        'desc' => ["invoice.invoice_status_updated_at" => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                    'flow_sum' => [
                        'default' => SORT_DESC,
                    ],
                    'flow_date',
                ],
                'defaultOrder' => [
                    'contractor_id' => SORT_ASC,
                    'document_date' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        $query->andFilterWhere([
            "invoice.contractor_id" => $this->contractor_id,
            "invoice.document_author_id" => $this->document_author_id,
            'pay.flow_way' => $this->flow_way,
        ]);

        $this->query = clone $query;

        return $dataProvider;
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function payedStatistic()
    {
        $allPaidPay = clone $this->query;
        $allPaidPay->select(['invoice.document_author_id', 'pay_sum' => 'SUM({{pay}}.[[flow_sum]])']);
        $allPaid = new Query;
        $allPaid->select([
            'document_author_id',
            'sum' => 'SUM([[pay_sum]])',
        ])->from(['t' => $allPaidPay])->groupBy('document_author_id');

        $paySelect = ['link.amount', 'link.invoice_id'];
        $payWhere = [
            'and',
            ['flow.company_id' => $this->company_id],
            ['flow.flow_type' => CashFlowsBase::FLOW_TYPE_INCOME],
            ['<', 'flow.date', $this->dateRange['from']],
        ];
        $payQuery = new Query;
        $payQuery->select([
            'invoice_id',
            'flow_sum' => 'SUM([[amount]])',
        ])->from(['t' => CashFlowsBase::getAllFlows($paySelect, $payWhere, true, 'innerJoin')])->groupBy('t.invoice_id');
        $debtStart = Invoice::find()
            ->select(['document_author_id', 'sum' => '(SUM([[total_amount_with_nds]]) - IFNULL(SUM({{pay}}.[[flow_sum]]), 0))'])
            ->leftJoin(['pay' => $payQuery], '{{pay}}.[[invoice_id]] = {{invoice}}.[[id]]')
            ->byCompany($this->company_id)
            ->byIOType($this->type)
            ->byDeleted(false)
            ->andWhere(['invoice.invoice_status_id' => InvoiceStatus::$validInvoices])
            ->andWhere(['<', 'document_date', $this->dateRange['from']])
            ->groupBy('document_author_id');

        $created = Invoice::find()
            ->select(['document_author_id', 'sum' => 'SUM([[total_amount_with_nds]])'])
            ->byCompany($this->company_id)
            ->byIOType($this->type)
            ->byDeleted(false)
            ->andWhere(['invoice.invoice_status_id' => InvoiceStatus::$validInvoices])
            ->andWhere(['between', 'document_date', $this->dateRange['from'], $this->dateRange['to']])
            ->groupBy('document_author_id');

        $query = EmployeeCompany::find()->alias('empCompany')
            ->select([
                'allPaid.document_author_id',
                'empCompany.lastname',
                'empCompany.firstname',
                'empCompany.patronymic',
                'all_payed_amount' => "IFNULL({{allPaid}}.[[sum]], 0)",
                'all_debtStart_amount' => "IFNULL({{debtStart}}.[[sum]], 0)",
                'all_created_amount' => "IFNULL({{created}}.[[sum]], 0)",
            ])
            ->rightJoin(['allPaid' => $allPaid], [
                'and',
                '{{allPaid}}.[[document_author_id]] ={{empCompany}}.[[employee_id]]',
                ['empCompany.company_id' => $this->company_id],
            ])
            ->leftJoin(['debtStart' => $debtStart], '{{debtStart}}.[[document_author_id]] = {{empCompany}}.[[employee_id]]')
            ->leftJoin(['created' => $created], '{{created}}.[[document_author_id]] = {{empCompany}}.[[employee_id]]')
            ->orderBy([
                'ISNULL({{empCompany}}.[[lastname]])' => SORT_ASC,
                "empCompany.lastname" => SORT_ASC,
                "empCompany.firstname" => SORT_ASC,
                "empCompany.patronymic" => SORT_ASC,
            ])
            ->asArray();

        return $query->all();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @param $dateRange
     * @return ActiveDataProvider
     */
    public function notPayed($params, $dateRange = null)
    {
        $tInvoice = self::tableName();
        $tContractor = Contractor::tableName();
        $tType = CompanyType::tableName();

        /* @var InvoiceQuery $query */
        $query = self::find()
            ->leftJoin($tContractor, "`$tInvoice`.`contractor_id` = `$tContractor`.`id`")
            ->leftJoin($tType, "`$tContractor`.`company_type_id` = `$tType`.`id`")
            ->byCompany($this->company_id)
            ->byIOType($this->type)
            ->byDeleted(false)
            ->andWhere(['not', ["$tInvoice.invoice_status_id" => InvoiceStatus::STATUS_REJECTED]])
            ->andWhere([
                'or',
                [
                    'and',
                    ["$tInvoice.invoice_status_id" => InvoiceStatus::STATUS_PAYED],
                    [
                        'or',
                        [
                            'and',
                            ['<', "$tInvoice.document_date", $this->dateRange['from']],
                            ['between', "$tInvoice.invoice_status_updated_at", strtotime($this->dateRange['from']), strtotime($this->dateRange['to']) + 86400 - 1],
                        ],
                        [
                            'and',
                            ['<=', "$tInvoice.document_date", $this->dateRange['to']],
                            ['>=', "$tInvoice.invoice_status_updated_at", strtotime($this->dateRange['to']) + 86400],
                        ]
                    ]
                ],
                [
                    'and',
                    ['not', ["$tInvoice.invoice_status_id" => InvoiceStatus::STATUS_PAYED]],
                    ['<=', "$tInvoice.document_date", $this->dateRange['to']],
                ]
            ])
            ->groupBy('invoice.id');

        if (!Yii::$app->user->can(permissions\document\Document::VIEW)) {
            $query->andWhere(["$tInvoice.document_author_id" => Yii::$app->user->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'document_date' => [
                        'asc' => ["`$tInvoice`.`document_date`" => SORT_ASC],
                        'desc' => ["`$tInvoice`.`document_date`" => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                    'document_number' => [
                        'asc' => ["`$tInvoice`.`document_number` * 1" => SORT_ASC],
                        'desc' => ["`$tInvoice`.`document_number` * 1" => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                    'total_amount_with_nds' => [
                        'asc' => ["`$tInvoice`.`total_amount_with_nds`" => SORT_ASC],
                        'desc' => ["`$tInvoice`.`total_amount_with_nds`" => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                    'contractor_id' => [
                        'asc' => [
                            "ISNULL(`$tType`.`name_short`)" => SORT_ASC,
                            "$tType.name_short" => SORT_ASC,
                            "$tContractor.name" => SORT_ASC,
                        ],
                        'desc' => [
                            "ISNULL(`$tType`.`name_short`)" => SORT_DESC,
                            "$tType.name_short" => SORT_DESC,
                            "$tContractor.name" => SORT_DESC,
                        ],
                        'default' => SORT_ASC
                    ],
                ],
                'defaultOrder' => [
                    'contractor_id' => SORT_ASC,
                    'document_date' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        $query->andFilterWhere([
            "$tInvoice.contractor_id" => $this->contractor_id,
            "$tInvoice.document_author_id" => $this->document_author_id,
            "$tInvoice.invoice_status_id" => $this->invoice_status_id,
        ]);

        $this->query = $query;

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getEmployees()
    {
        $key = 'employees';
        if (!array_key_exists($key, $this->notPayedStatisticData)) {
            $tEmployee = Employee::tableName();
            $tInvoice = Invoice::tableName();
            $query = clone $this->query;
            $this->notPayedStatisticData[$key] = $query
                ->select([
                    "{$tInvoice}.document_author_id",
                    "{$tEmployee}.lastname",
                    "{$tEmployee}.firstname",
                    "{$tEmployee}.patronymic",
                ])
                ->leftJoin("{$tEmployee}", "{$tEmployee}.id = {$tInvoice}.document_author_id")
                ->groupBy("{$tInvoice}.document_author_id")
                ->orderBy([
                    "{$tEmployee}.lastname" => SORT_ASC,
                    "{$tEmployee}.firstname" => SORT_ASC,
                    "{$tEmployee}.patronymic" => SORT_ASC,
                ])
                ->asArray()
                ->all();
        }

        return $this->notPayedStatisticData[$key];
    }

    /**
     * @param $employeeID
     * @return mixed
     */
    public function getDebtStartAmount($employeeID)
    {
        if (!isset($this->notPayedStatisticData['amount']['debt_start_amount'][$employeeID])) {
            $tInvoice = Invoice::tableName();
            $query = clone $this->query;
            $this->notPayedStatisticData['amount']['debt_start_amount'][$employeeID] = $query
                ->select([
                    'IFNULL(SUM(`debtStart`.`total_amount_with_nds`), 0) - IFNULL(SUM(`debtStart`.`payment_partial_amount`), 0) as startAmount',
                ])
                ->andWhere(["{$tInvoice}.document_author_id" => $employeeID])
                ->leftJoin("{$tInvoice} AS debtStart", [
                    'and',
                    "{$tInvoice}.id = debtStart.id",
                    ['<', 'debtStart.document_date', $this->dateRange['from']],
                ])
                ->sum('startAmount');
        }

        return $this->notPayedStatisticData['amount']['debt_start_amount'][$employeeID];
    }

    /**
     * @param $employeeID
     * @return mixed
     */
    public function getCreatedAmount($employeeID)
    {
        if (!isset($this->notPayedStatisticData['amount']['created_amount'][$employeeID])) {
            $tInvoice = Invoice::tableName();
            $query = clone $this->query;
            $this->notPayedStatisticData['amount']['created_amount'][$employeeID] = $query
                ->select(['IFNULL(SUM(`created`.`total_amount_with_nds`), 0) - IFNULL(SUM(`created`.`payment_partial_amount`), 0) as createdAmount'])
                ->andWhere(["{$tInvoice}.document_author_id" => $employeeID])
                ->leftJoin("{$tInvoice} AS created", [
                    'and',
                    "{$tInvoice}.id = created.id",
                    ['between', 'created.document_date', $this->dateRange['from'], $this->dateRange['to']],
                ])
                ->sum('createdAmount');
        }

        return $this->notPayedStatisticData['amount']['created_amount'][$employeeID];
    }

    /**
     * @param $employeeID
     * @return mixed
     */
    public function getPayedAmount($employeeID)
    {
        $cashFlowsTables = [
            CashBankFlowToInvoice::tableName() => CashBankFlows::tableName(),
            CashOrderFlowToInvoice::tableName() => CashOrderFlows::tableName(),
            CashEmoneyFlowToInvoice::tableName() => CashEmoneyFlows::tableName(),
        ];
        if (!isset($this->notPayedStatisticData['amount']['payed_amount'][$employeeID])) {
            $tInvoice = Invoice::tableName();
            $this->notPayedStatisticData['amount']['payed_amount'][$employeeID] = 0;
            foreach ($cashFlowsTables as $flowToInvoice => $flow) {
                $query = clone $this->query;
                $this->notPayedStatisticData['amount']['payed_amount'][$employeeID] += $query
                    ->select(['IFNULL(SUM(`cashFlow`.`amount`), 0) as cashFlowAmount'])
                    ->andWhere(["{$tInvoice}.document_author_id" => $employeeID])
                    ->leftJoin("$flowToInvoice as cashToInvoice", "$tInvoice.id = cashToInvoice.invoice_id")
                    ->leftJoin("$flow as cashFlow", [
                        'and',
                        "cashToInvoice.flow_id = cashFlow.id",
                        ['between', 'cashFlow.date', $this->dateRange['from'], $this->dateRange['to']],
                    ])
                    ->andWhere(['<', "{$tInvoice}.document_date", $this->dateRange['from']])
                    ->groupBy("{$tInvoice}.id")
                    ->sum('cashFlowAmount');
            }
        }

        return $this->notPayedStatisticData['amount']['payed_amount'][$employeeID];
    }

    /**
     * @param $employeeID
     * @return mixed
     */
    public function getDebtEndAmount($employeeID)
    {
        if (!isset($this->notPayedStatisticData['amount']['debt_end_amount'][$employeeID])) {
            $this->notPayedStatisticData['amount']['debt_end_amount'][$employeeID] =
                $this->getDebtStartAmount($employeeID) -
                $this->getPayedAmount($employeeID) +
                $this->getCreatedAmount($employeeID);
        }

        return $this->notPayedStatisticData['amount']['debt_end_amount'][$employeeID];
    }

    /**
     * @return int|mixed
     */
    public function getTotalDebtStartAmount()
    {
        if (!isset($this->notPayedStatisticData['total_debt_start_amount'])) {
            if (isset($this->notPayedStatisticData['amount']['debt_start_amount'])) {
                $this->notPayedStatisticData['total_debt_start_amount'] = 0;
                foreach ($this->notPayedStatisticData['amount']['debt_start_amount'] as $amount) {
                    $this->notPayedStatisticData['total_debt_start_amount'] += $amount;
                }
            } else {
                return 0;
            }
        }

        return $this->notPayedStatisticData['total_debt_start_amount'];
    }

    /**
     * @return int|mixed
     */
    public function getTotalPayedAmount()
    {
        if (!isset($this->notPayedStatisticData['total_payed_amount'])) {
            if (isset($this->notPayedStatisticData['amount']['payed_amount'])) {
                $this->notPayedStatisticData['total_payed_amount'] = 0;
                foreach ($this->notPayedStatisticData['amount']['payed_amount'] as $amount) {
                    $this->notPayedStatisticData['total_payed_amount'] += $amount;
                }
            } else {
                return 0;
            }
        }

        return $this->notPayedStatisticData['total_payed_amount'];
    }

    /**
     * @return int|mixed
     */
    public function getTotalCreatedAmount()
    {
        if (!isset($this->notPayedStatisticData['total_created_amount'])) {
            if (isset($this->notPayedStatisticData['amount']['created_amount'])) {
                $this->notPayedStatisticData['total_created_amount'] = 0;
                foreach ($this->notPayedStatisticData['amount']['created_amount'] as $amount) {
                    $this->notPayedStatisticData['total_created_amount'] += $amount;
                }
            } else {
                return 0;
            }
        }

        return $this->notPayedStatisticData['total_created_amount'];
    }

    /**
     * @return int|mixed
     */
    public function getTotalDebtEndAmount()
    {
        if (!isset($this->notPayedStatisticData['total_debt_end_amount'])) {
            $this->notPayedStatisticData['total_debt_end_amount'] = 0;
            foreach ($this->getEmployees() as $employee) {
                $this->notPayedStatisticData['total_debt_end_amount'] += $this->getDebtEndAmount($employee['document_author_id']);
            }
        }

        return $this->notPayedStatisticData['total_debt_end_amount'];
    }

    /**
     * @param $employeeID
     * @return float|int
     */
    public function getPercentDebtEndAmount($employeeID)
    {
        $employeeDebtEndAmount = $this->getDebtEndAmount($employeeID);
        $totalDebtEndAmount = $this->getTotalDebtEndAmount();

        return $totalDebtEndAmount ?
            round($employeeDebtEndAmount * 100 / $totalDebtEndAmount, 2):
            0;
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function notPayedStatistic()
    {
        $tInvoice = Invoice::tableName();
        $tEmployee = Employee::tableName();
        $tCashBankFlowToInvoice = CashBankFlowToInvoice::tableName();
        $tCashBankFlows = CashBankFlows::tableName();
        $tCashOrderFlowToInvoice = CashOrderFlowToInvoice::tableName();
        $tCashOrderFlows = CashOrderFlows::tableName();
        $tCashEmoneyFlowToInvoice = CashEmoneyFlowToInvoice::tableName();
        $tCashEmoneyFlows = CashEmoneyFlows::tableName();

        return Employee::find()
            ->select([
                "$tEmployee.id",
                "$tEmployee.lastname",
                "$tEmployee.firstname",
                "$tEmployee.patronymic",
                "IFNULL(SUM(`debtStart`.`total_amount_with_nds`), 0) - IFNULL(SUM(`debtStart`.`payment_partial_amount`), 0) AS all_debtStart_amount",
                "IFNULL(SUM(`created`.`total_amount_with_nds`), 0) - IFNULL(SUM(`created`.`payment_partial_amount`), 0) AS all_created_amount",
                "IFNULL(SUM(`cashBankFlow`.`amount`), 0) + IFNULL(SUM(`cashOrderFlow`.`amount`), 0) + IFNULL(SUM(`cashEmoneyFlow`.`amount`), 0) as all_payed_amount",
                "IFNULL(SUM(`debtStart`.`total_amount_with_nds`), 0) - IFNULL(SUM(`debtStart`.`payment_partial_amount`), 0) -
                 IFNULL(SUM(`cashBankFlow`.`amount`), 0) + IFNULL(SUM(`cashOrderFlow`.`amount`), 0) + IFNULL(SUM(`cashEmoneyFlow`.`amount`), 0) +
                 IFNULL(SUM(`created`.`total_amount_with_nds`), 0) - IFNULL(SUM(`created`.`payment_partial_amount`), 0)
                 as all_debtEnd_amount",
            ])
            ->leftJoin(['allInvoices' => $this->query], "$tEmployee.id = allInvoices.document_author_id")
            ->leftJoin("$tInvoice AS debtStart", [
                'and',
                "allInvoices.id = debtStart.id",
                ['<', 'debtStart.document_date', $this->dateRange['from']],
            ])
            ->leftJoin("$tInvoice AS created", [
                'and',
                "allInvoices.id = created.id",
                ['between', 'created.document_date', $this->dateRange['from'], $this->dateRange['to']],
            ])
            ->leftJoin("$tCashBankFlowToInvoice as cashBankToInvoice", "allInvoices.id = cashBankToInvoice.invoice_id")
            ->leftJoin("$tCashBankFlows as cashBankFlow", [
                'and',
                "cashBankToInvoice.flow_id = cashBankFlow.id",
                ['between', 'cashBankFlow.date', $this->dateRange['from'], $this->dateRange['to']],
            ])
            ->leftJoin("$tCashOrderFlowToInvoice as cashOrderToInvoice", "allInvoices.id = cashOrderToInvoice.invoice_id")
            ->leftJoin("$tCashOrderFlows as cashOrderFlow", [
                'and',
                "cashOrderToInvoice.flow_id = cashOrderFlow.id",
                ['between', 'cashOrderFlow.date', $this->dateRange['from'], $this->dateRange['to']],
            ])
            ->leftJoin("$tCashEmoneyFlowToInvoice as cashEmoneyToInvoice", "allInvoices.id = cashEmoneyToInvoice.invoice_id")
            ->leftJoin("$tCashEmoneyFlows as cashEmoneyFlow", [
                'and',
                "cashEmoneyToInvoice.flow_id = cashEmoneyFlow.id",
                ['between', 'cashEmoneyFlow.date', $this->dateRange['from'], $this->dateRange['to']],
            ])
            ->leftJoin("$tInvoice AS debtEnd", [
                'and',
                "allInvoices.id = debtEnd.id",
                [
                    'or',
                    ['not', ["debtEnd.invoice_status_id" => InvoiceStatus::STATUS_PAYED]],
                    [
                        'and',
                        ["debtEnd.invoice_status_id" => InvoiceStatus::STATUS_PAYED],
                        ['>=', 'debtEnd.invoice_status_updated_at', strtotime($this->dateRange['to']) + 86400]
                    ]
                ]
            ])
            ->andWhere(['not', ['allInvoices.id' => null]])
            ->groupBy("$tEmployee.id")
            ->orderBy([
                "$tEmployee.lastname" => SORT_ASC,
                "$tEmployee.firstname" => SORT_ASC,
                "$tEmployee.patronymic" => SORT_ASC,
            ])
            ->asArray()
            ->all();
    }

    /**
     * @param $dateRange
     * @param $params
     * @return ActiveDataProvider
     */
    public function newClients($dateRange, $params)
    {
        $query = $this->getNewClientsFilter($dateRange);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => Yii::$app->request->get('per-page'),
            ],
            'sort' => [
                'attributes' => [
                    'contractor_id' => [
                        'asc' => [
                            'ISNULL(`' . CompanyType::tableName() . '`.`name_short`)' => SORT_ASC,
                            CompanyType::tableName() . '.name_short' => SORT_ASC,
                            Contractor::tableName() . '.name' => SORT_ASC,
                        ],
                        'desc' => [
                            'ISNULL(`' . CompanyType::tableName() . '`.`name_short`)' => SORT_DESC,
                            CompanyType::tableName() . '.name_short' => SORT_DESC,
                            Contractor::tableName() . '.name' => SORT_DESC,
                        ],
                        'default' => SORT_ASC,
                    ],
                    'document_date',
                    'document_number',
                    'total_amount_with_nds',
                    'invoice_status_updated_at',
                ],
            ],
        ]);

        $this->load($params);

        if ($this->payment_form_id) {
            $query->having(['paymentType' => $this->payment_form_id]);
        }

        $query->andFilterWhere(['document_author_id' => $this->document_author_id]);

        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     * @return array
     */
    public function getStatusItemsByQuery(ActiveQuery $query)
    {
        $searchQuery = clone $query;
        $statusQuery = InvoiceStatus::find()
            ->select(['name', 'id'])
            ->where([
                'id' => $searchQuery->select(Invoice::tableName() . '.invoice_status_id')->column(),
            ])
            ->groupBy('id')
            ->indexBy('id');

        return (['' => 'Все'] + $statusQuery->column());
    }

    /**
     * @return array
     */
    public function getAuthorFilter()
    {
        $query = $this->getNewClientsFilter(StatisticPeriod::getSessionPeriod());

        $query->joinWith('documentAuthor');

        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map($query->orderBy(['lastname' => SORT_ASC, 'firstname' => SORT_ASC, 'patronymic' => SORT_ASC])->all(), 'documentAuthor.id', 'documentAuthor.shortFio'));
    }

    /**
     * @param $dateRange
     * @return ActiveQuery
     */
    public function getNewClientsFilter($dateRange)
    {
        $allFirstPayedInvoices = Invoice::find()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byStatus([InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL])
            ->byCompany(Yii::$app->user->identity->company->id)
            ->groupBy('contractor_id')
            ->column();

        $needFirstPayedInvoices = Invoice::find()
            ->select(['contractor_id'])
            ->where(['in', Invoice::tableName() . '.id', $allFirstPayedInvoices])
            ->andWhere(['between', 'date(from_unixtime(`' . Invoice::tableName() . '`.`invoice_status_updated_at`))', $dateRange['from'], $dateRange['to']])
            ->column();

        $query = Invoice::find()
            ->select([Invoice::tableName() . '.*'])
            ->joinWith('contractor')
            ->leftJoin(CompanyType::tableName(), '`' . Contractor::tableName() . '`.`company_type_id` = `' . CompanyType::tableName() . '`.`id`')
            ->where(['in', Invoice::tableName() . '.contractor_id', $needFirstPayedInvoices])
            ->andWhere(['between', 'date(from_unixtime(`' . Invoice::tableName() . '`.`invoice_status_updated_at`))', $dateRange['from'], $dateRange['to']]);

        return $this->queryAddPaymentType($query);
    }

    /**
     * @return string
     */
    public function getFlowWay()
    {
        return isset($this->flowWays[$this->flow_way]) ? $this->flowWays[$this->flow_way] : '';
    }


    /**
     * @return array
     */
    public function linkedStatistic()
    {
        $query = clone $this->query;

        $tEmployee = EmployeeCompany::tableName();

        $wrapQuery = (new Query)
            ->select([
                't.responsible_employee_id',
                'in_total_amount_with_nds' => 'SUM(t.in_total_amount_with_nds)',
                'out_total_amount_with_nds' => 'SUM(t.out_total_amount_with_nds)',
                'margin_amount' => 'SUM(IFNULL(t.out_total_amount_with_nds, 0)) - SUM(IFNULL(t.in_total_amount_with_nds, 0))',
                'margin_percent' => '100 * (SUM(IFNULL(t.out_total_amount_with_nds, 0)) - SUM(IFNULL(t.in_total_amount_with_nds, 0))) / SUM(IFNULL(t.out_total_amount_with_nds, 0))',
                "$tEmployee.lastname",
                "$tEmployee.firstname",
                "$tEmployee.patronymic",
            ])
            ->from(['t' => $query])
            ->leftJoin($tEmployee, "$tEmployee.employee_id = t.responsible_employee_id AND $tEmployee.company_id = t.company_id")
            ->groupBy(['t.responsible_employee_id']);

        return $wrapQuery->all(\Yii::$app->db2);
    }

    /**
     * @param array $params
     * @return SqlDataProvider
     */
    public function linked($params)
    {
        $this->defaultOrderStr = '-out_document_date';
        $this->defaultOrderArr = ['out_document_date' => SORT_DESC];

        $tInvoice = self::tableName();
        $tLink = InvoiceInToInvoiceOut::tableName();
        $tContractor = Contractor::tableName();
        $tType = CompanyType::tableName();
        $tPay = 'pay';

        $this->load($params);

        $payQuery = $this->getLinkedPayQuery();

        /* @var InvoiceQuery $query */
        $query = self::find()
            ->leftJoin($tContractor, "`$tInvoice`.`contractor_id` = `$tContractor`.`id`")
            ->leftJoin($tType, "`$tContractor`.`company_type_id` = `$tType`.`id`")
            ->leftJoin($tLink, "`$tInvoice`.`id` = `$tLink`.`invoice_id` AND `$tLink`.`company_id` = " . $this->company_id)
            ->leftJoin([$tPay => $payQuery], "`$tInvoice`.`id` = `$tPay`.`invoice_id`")
            ->select([
                'id' => "`$tInvoice`.`id`",
                'type' => "`$tInvoice`.`type`",
                'number' => "`$tLink`.`number`",
                'company_id' => "`$tInvoice`.`company_id`",
                'responsible_employee_id' => "`$tInvoice`.`responsible_employee_id`",
                'in_document_date' => "IF(`$tInvoice`.`type` = 1, `$tInvoice`.`document_date`, NULL)",
                'out_document_date' => "IF(`$tInvoice`.`type` = 2, `$tInvoice`.`document_date`, NULL)",
                'in_document_number' => "IF(`$tInvoice`.`type` = 1, `$tInvoice`.`document_number`, NULL)",
                'out_document_number' => "IF(`$tInvoice`.`type` = 2, `$tInvoice`.`document_number`, NULL)",
                'in_total_amount_with_nds' => "IF(`$tInvoice`.`type` = 1, `$tInvoice`.`total_amount_with_nds`, NULL)",
                'out_total_amount_with_nds' => "IF(`$tInvoice`.`type` = 2, `$tInvoice`.`total_amount_with_nds`, NULL)",
                'in_contractor_id' => "IF(`$tInvoice`.`type` = 1, `$tInvoice`.`contractor_id`, NULL)",
                'out_contractor_id' => "IF(`$tInvoice`.`type` = 2, `$tInvoice`.`contractor_id`, NULL)",
                'in_invoice_status_id' => "IF(`$tInvoice`.`type` = 1, `$tInvoice`.`invoice_status_id`, NULL)",
                'out_invoice_status_id' => "IF(`$tInvoice`.`type` = 2, `$tInvoice`.`invoice_status_id`, NULL)",
                'in_payment_date' => "IF(`$tInvoice`.`type` = 1, `pay`.`flow_date`, NULL)",
                'out_payment_date' => "IF(`$tInvoice`.`type` = 2, `pay`.`flow_date`, NULL)",
            ])
            ->byCompany($this->company_id)
            ->byDeleted(false)
            ->andWhere(['not', ["$tInvoice.invoice_status_id" => InvoiceStatus::STATUS_REJECTED]])
            ->andWhere(['between', "$tInvoice.document_date", $this->dateRange['from'], $this->dateRange['to']])
            //////////////////////////////////////////////
            ->andWhere(['not', ["$tLink.number" => null]])
            //////////////////////////////////////////////
            ->asArray()
            ->groupBy('invoice.id');

        if ($this->in_contractor_id) {
            $query->andWhere(['and',
                ["$tInvoice.type" => Documents::IO_TYPE_IN],
                ["$tInvoice.contractor_id" => $this->in_contractor_id],
            ]);
        }
        if ($this->out_contractor_id) {
            $query->andWhere(['and',
                ["$tInvoice.type" => Documents::IO_TYPE_OUT],
                ["$tInvoice.contractor_id" => $this->out_contractor_id],
            ]);
        }
        if ($this->in_invoice_status_id) {
            $query->andWhere(['and',
                ["$tInvoice.type" => Documents::IO_TYPE_IN],
                ["$tInvoice.invoice_status_id" => $this->in_invoice_status_id],
            ]);
        }
        if ($this->out_invoice_status_id) {
            $query->andWhere(['and',
                ["$tInvoice.type" => Documents::IO_TYPE_OUT],
                ["$tInvoice.invoice_status_id" => $this->out_invoice_status_id],
            ]);
        }

        $query->andFilterWhere([
            "$tInvoice.responsible_employee_id" => $this->responsible_employee_id,
        ]);

        if ($this->paymentType) {
            $query->having(['paymentType' => $this->paymentType]);
        }

        $this->query = $query;

        $currentSort = Yii::$app->request->get('sort', $this->defaultOrderStr);
        $isDesc = substr($currentSort, 0, 1) === '-';

        $wrapQuery = (new Query)
            ->select([
                't.number',
                'in_document_date' => ($isDesc) ? 'MAX(t.in_document_date)' : 'MIN(t.in_document_date)',
                'out_document_date' => ($isDesc) ? 'MAX(t.out_document_date)' : 'MIN(t.out_document_date)',
                'in_document_number' => ($isDesc) ? 'MAX(t.in_document_number)' : 'MIN(t.in_document_number)',
                'out_document_number' => ($isDesc) ? 'MAX(t.out_document_number)' : 'MIN(t.out_document_number)',
                'in_total_amount_with_nds' => ($isDesc) ? 'MAX(t.in_total_amount_with_nds)' : 'MIN(t.in_total_amount_with_nds)',
                'out_total_amount_with_nds' => ($isDesc) ? 'MAX(t.out_total_amount_with_nds)' : 'MIN(t.out_total_amount_with_nds)',
                'in_payment_date' => ($isDesc) ? 'MAX(t.in_payment_date)' : 'MIN(t.in_payment_date)',
                'out_payment_date' => ($isDesc) ? 'MAX(t.out_payment_date)' : 'MIN(t.out_payment_date)',
                'margin_amount' => 'SUM(IFNULL(t.out_total_amount_with_nds, 0)) - SUM(IFNULL(t.in_total_amount_with_nds, 0))',
                'margin_percent' => '100 * (SUM(IFNULL(t.out_total_amount_with_nds, 0)) - SUM(IFNULL(t.in_total_amount_with_nds, 0))) / SUM(IFNULL(t.out_total_amount_with_nds, 0))',
                't.responsible_employee_id'
            ])
            ->from(['t' => $query])
            ->groupBy(['t.number']);

        $this->wrapQuery = $wrapQuery;

        $dataProvider = new SqlDataProvider([
            'sql' => $wrapQuery->createCommand()->rawSql,
            'totalCount' => $wrapQuery->count(),
            'sort' => [
                'attributes' => [
                    'margin_amount',
                    'margin_percent',
                    'in_document_number',
                    'out_document_number',
                    'in_document_date',
                    'out_document_date',
                    'in_total_amount_with_nds',
                    'out_total_amount_with_nds',
                    'in_payment_date',
                    'out_payment_date',
                ],
                'defaultOrder' => $this->defaultOrderArr
            ],
        ]);

        return $dataProvider;
    }

    /**
     * @return Query
     */
    public function getLinkedPayQuery()
    {
        $paySelect = [
            'invoice_id' => 'link.invoice_id',
            'flow_date' => 'flow.date'
        ];
        $payWhere = ['and',
            ['flow.company_id' => $this->company_id],
        ];
        $payQuery = new Query;
        $payQuery->select([
            'invoice_id',
            'flow_date' => 'MAX([[flow_date]])',
        ])->from(['t' => CashFlowsBase::getAllFlows($paySelect, $payWhere, true, 'leftJoin')])->groupBy('t.invoice_id');

        return $payQuery;
    }

    /**
     * @return array
     */
    public function getResponsibleEmployersFilter()
    {
        $query = clone $this->query;

        $tEmployee = EmployeeCompany::tableName();
        $tInvoice = Invoice::tableName();

        return ArrayHelper::map($query
            ->select([
                "{$tInvoice}.responsible_employee_id AS id",
                "CONCAT({$tEmployee}.lastname, ' ', {$tEmployee}.firstname, ' ', {$tEmployee}.patronymic) AS name",
                "{$tEmployee}.lastname",
                "{$tEmployee}.firstname",
                "{$tEmployee}.patronymic",
            ])
            ->leftJoin("{$tEmployee}", "{$tEmployee}.employee_id = {$tInvoice}.responsible_employee_id AND {$tEmployee}.company_id = {$tInvoice}.company_id")
            ->groupBy("{$tInvoice}.responsible_employee_id")
            ->orderBy([
                "{$tEmployee}.lastname" => SORT_ASC,
                "{$tEmployee}.firstname" => SORT_ASC,
                "{$tEmployee}.patronymic" => SORT_ASC,
            ])
            ->asArray()
            ->all(\Yii::$app->db2), 'id', 'name');
    }

    /**
     * @return array
     */
    public function getContractorsFilter($ioType)
    {
        $query = clone $this->query;

        $tInvoice = Invoice::tableName();
        $tContractor = Contractor::tableName();
        $tCashContractor = CashContractorType::tableName();

        $contractorIdArray = $query
            ->distinct()
            ->select("$tInvoice.contractor_id")
            ->andWhere(["$tInvoice.type" => $ioType])
            ->column(Yii::$app->db2);

        $cashContractorArray = \common\components\helpers\ArrayHelper::map(
            CashContractorType::find()
                ->andWhere(["$tCashContractor.name" => $contractorIdArray])
                ->all(Yii::$app->db2),
            'name',
            'text'
        );

        $contractorArray = ArrayHelper::map(
            Contractor::getSorted()
                ->andWhere(["$tContractor.id" => $contractorIdArray])
                ->all(Yii::$app->db2),
            'id',
            'shortName'
        );

        return $cashContractorArray + $contractorArray;
    }

    /**
     * @return array
     */
    public function getInvoicesStatusesFilter($ioType)
    {
        $query = clone $this->query;

        $tInvoice = Invoice::tableName();
        $tInvoiceStatus = InvoiceStatus::tableName();

        $idArray = $query
            ->distinct()
            ->select("$tInvoice.invoice_status_id")
            ->andWhere(["$tInvoice.type" => $ioType])
            ->column(Yii::$app->db2);

        return ArrayHelper::map(InvoiceStatus::find()->andWhere(['id' => $idArray])->all(Yii::$app->db2), 'id', 'name');
    }
}
