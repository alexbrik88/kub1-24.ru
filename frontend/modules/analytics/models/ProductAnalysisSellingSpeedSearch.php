<?php

namespace frontend\modules\analytics\models;

use common\components\helpers\ArrayHelper;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductStore;
use common\models\product\ProductStoreDaily;
use common\models\product\ProductTurnoverSearch;
use common\models\product\ProductUnit;
use common\models\product\Store;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class ProductAnalysisSellingSpeed
 * @package frontend\modules\analytics\models
 */
class ProductAnalysisSellingSpeedSearch extends Product
{
    public $search;

    public $company;

    public $title;

    public $unit;

    public $group;

    public $provider;

    public $storeName;

    public $storeId;

    public $lastOrderQuantity;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['search'], 'safe'],
            [['daysOfInventory'], 'safe'],
            [['lastOrderQuantity'], 'safe'],
            [['title', 'unit', 'group', 'provider', 'storeId', 'storeName'], 'safe'],
        ];
    }

    public function search($params = [])
    {
        $this->load($params);

        $query = $this->getBaseQuery();

        $dataProvider = new ActiveDataProvider([
            'db' => \Yii::$app->db2,
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'daysOfInventory',
                    'sellingSpeed',
                    'remainder',
                    'soldDuringPeriod',
                    'article',
                ],
                'defaultOrder' => [
                    'sellingSpeed' => SORT_DESC,
                    'daysOfInventory' => SORT_ASC,
                ],
            ]
        ]);

        return $dataProvider;
    }

    public function getBaseQuery()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        $storeDaily = ProductStoreDaily::find()
            ->select([
                'product_id',
                'SUM(initial_quantity) as initial_quantity',
                'SUM(quantity) as quantity'
            ])
            ->groupBy('product_id, date')
        ;

        $countDays = ProductStoreDaily::find()
            ->select([
                ProductStoreDaily::tableName() . '.product_id',
                'IF((storeDaily.initial_quantity <= 0 AND storeDaily.quantity >= 0) OR (storeDaily.initial_quantity >= 0 AND storeDaily.quantity <= 0), '
                . 0.5
                . ', IF(storeDaily.initial_quantity > 0 AND storeDaily.quantity > 0, 1, 0)) '
                . ' as count',
            ])
            ->leftJoin(['storeDaily' => $storeDaily], ProductStoreDaily::tableName() . '.product_id = storeDaily.product_id')
            ->groupBy('product_id')
        ;

        $productTurnover = (new Query())
            ->select([
                'product_turnover.*',
                'SUM(quantity) as sumQuantity',
                'SUM(price_one * quantity) as soldDuringPeriod'
            ])
            ->from('product_turnover')
            ->andWhere(['and',
                ['type' => Documents::IO_TYPE_OUT]
            ])
            ->groupBy('`product_id`, `date`')
        ;

        $quantityProductTurnover = (new Query())
            ->select([
                'product_id',
                'quantity as quantity',
            ])
            ->from('product_turnover')
            ->andWhere(['type' => Documents::IO_TYPE_IN])
            ->groupBy(['order_id', 'product_id'])
            ->orderBy('order_id DESC, product_id ASC')
            ->limit(1)
        ;

        $productStore = ProductStore::find()
            ->select([
                'product_id',
                'SUM(quantity) as quantity',
                'COUNT(quantity) as count'
            ])
            ->andWhere(['>', 'quantity', new Expression(0)])
            ->groupBy('product_id')
        ;

        $query = Product::find()
            ->distinct()
            ->select([
                Product::tableName() . '.*',
                'productTurnover.invoice_id as invoice_id',
//                '(countDays.count) as sellingSpeed',
                '(productTurnover.sumQuantity / countDays.count) as sellingSpeed',
                '(productStore.quantity / (productTurnover.sumQuantity / countDays.count)) as daysOfInventory',
                'productTurnover.soldDuringPeriod as soldDuringPeriod',
                'productStore.quantity as remainder',
                Store::tableName() . '.id as storeId',
                'IF(productStore.count > 1, "Все склады",' . Store::tableName() . '.name) as storeName',
                'quantityProductTurnover.quantity as lastOrderQuantity',
            ])
            ->joinWith('productStoresDaily', true, 'INNER JOIN')
            ->joinWith('productStores.store')
            ->leftJoin(['productStore' => $productStore], Product::tableName() . '.id = productStore.product_id')
            ->leftJoin(['productTurnover' => $productTurnover], Product::tableName() . '.id = productTurnover.product_id')
            ->leftJoin(['quantityProductTurnover' => $quantityProductTurnover], 'product.id = quantityProductTurnover.product_id')
            ->leftJoin(['countDays' => $countDays], 'product.id = countDays.product_id')
            ->andWhere([Product::tableName() . '.company_id' => $this->company->id])
            ->andWhere(ProductStoreDaily::tableName() . '.date = productTurnover.date')
            ->andWhere(['between', ProductStoreDaily::tableName() . '.date', $dateRange['from'], $dateRange['to']])
            ->andWhere(['between', 'productTurnover.date', $dateRange['from'], $dateRange['to']])
            ->andWhere(['productTurnover.type' => Documents::IO_TYPE_OUT])
            ->orderBy('productTurnover.product_id')
            ->groupBy([Product::tableName() . '.id'])
        ;

        if ($this->title) {
            $query->andFilterWhere([Product::tableName() . '.id' => $this->title]);
        }

        if ($this->unit) {
            $query->andFilterWhere(['product_unit_id' => $this->unit]);
        }

        if ($this->group) {
            $query->andFilterWhere(['group_id' => $this->group]);
        }

        if ($this->provider) {
            $query
                ->joinWith('invoices.contractor')
                ->andFilterWhere([Contractor::tableName().'.id' => $this->provider]);
        }

        if ($this->storeName) {
            $query->andFilterWhere([Store::tableName() . '.name' => $this->storeName]);
        }

//        echo $query->createCommand()->getRawSql();die;

        return $query;
    }

    public function filterProductName()
    {
        $query = clone $this->getBaseQuery();
        $products = $query
            ->select([
                Product::tableName() . '.id',
                Product::tableName() . '.title',
            ])
            ->indexBy('id')
            ->all();

        return ['' => 'Все'] + ArrayHelper::map($products, 'id', 'title');
    }

    public function filterUnitProduct()
    {
        $query = clone $this->getBaseQuery();
        $result = $query
            ->leftJoin(['productUnit' => ProductUnit::tableName()], 'product.product_unit_id = productUnit.id')
            ->select([
                'productUnit.id as id',
                'productUnit.name as name',
            ])
            ->asArray()
            ->all();

        return ['' => 'Все'] + ArrayHelper::map($result, 'id', 'name');
    }

    public function filterGroup()
    {
        $query = clone $this->getBaseQuery();
        $result = $query
            ->leftJoin(['group' => ProductGroup::tableName()], 'product.group_id = group.id')
            ->select([
                'group.id as id',
                'group.title as title',
            ])
            ->asArray()
            ->all();

        return ['' => 'Все'] + ArrayHelper::map($result, 'id', 'title');
    }

    public function filterSupplier()
    {
        $query = clone $this->getBaseQuery();
        $result = $query
            ->joinWith('invoices.contractor')
            ->select([
                Contractor::tableName() . '.id as id',
                Contractor::tableName() . '.name as name',
            ])
            ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_IN])
            ->orderBy(Invoice::tableName() . '.id DESC')
            ->groupBy(Invoice::tableName() . '.id')
            ->asArray()
            ->all();

        return ['' => 'Все'] + ArrayHelper::map($result, 'id', 'name');
    }

    public function filterStorage()
    {
        $query = clone $this->getBaseQuery();
        $result = $query
            ->select([
                Store::tableName() . '.id as id',
                Store::tableName() . '.name as name',
            ])
            ->asArray()
            ->all();

        return ['' => 'Все'] + ArrayHelper::map($result, 'name', 'name');
    }
}
