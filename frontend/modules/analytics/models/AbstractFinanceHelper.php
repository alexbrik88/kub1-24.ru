<?php
/**
 * Created by PhpStorm.
 * User: MaxFouri
 * Date: 04.11.2020
 * Time: 20:20
 */

namespace frontend\modules\analytics\models;

use Yii;
use common\models\Company;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashFlowsBase;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\ExpenseItemFlowOfFunds;
use common\models\IncomeItemFlowOfFunds;
use yii\base\Exception;

class AbstractFinanceHelper
{
    public static $incomeItemName = [];
    public static $expenditureItemName = [];
    public static $incomeItemSort = [];
    public static $expenditureItemSort = [];
    public static $walletName = [
        CashFlowsBase::WALLET_BANK => [],
        CashFlowsBase::WALLET_CASHBOX => [],
        CashFlowsBase::WALLET_EMONEY => []
    ];

    const EMPTY_YEAR = ['01' => 0, '02' => 0, '03' => 0, '04' => 0, '05' => 0, '06' => 0, '07' => 0, '08' => 0, '09' => 0, '10' => 0, '11' => 0, '12' => 0];
    private static $_EMPTY_YEAR_IN_DAYS = [];

    public static function init($multiCompanyIds)
    {
        // todo: temp, delete after update all analytics reports to multi-company
        if ($multiCompanyIds instanceof Company)
            $multiCompanyIds = [$multiCompanyIds];

        self::$incomeItemName = InvoiceIncomeItem::find()
            ->select('name')
            ->where(['or', ['company_id' => null], ['company_id' => $multiCompanyIds]])
            ->indexBy('id')
            ->column(Yii::$app->db2);
        self::$expenditureItemName = InvoiceExpenditureItem::find()
            ->select('name')
            ->where(['or', ['company_id' => null], ['company_id' => $multiCompanyIds]])
            ->indexBy('id')
            ->column(Yii::$app->db2);
        self::$incomeItemSort = IncomeItemFlowOfFunds::find()
            ->select('flow_of_funds_block')
            ->where(['company_id' => $multiCompanyIds])
            ->andWhere(['not', ['flow_of_funds_block' => null]])
            ->indexBy('income_item_id')
            ->column(Yii::$app->db2);
        self::$expenditureItemSort = ExpenseItemFlowOfFunds::find()
            ->select('flow_of_funds_block')
            ->where(['company_id' => $multiCompanyIds])
            ->andWhere(['not', ['flow_of_funds_block' => null]])
            ->indexBy('expense_item_id')
            ->column(Yii::$app->db2);

        foreach ($multiCompanyIds as $companyIds) {
            if ($company = Company::findOne(['id' => $companyIds])) {
                $_tmpBankNames = $company->getCheckingAccountants()
                    ->select('name')
                    ->orderBy(['type' => SORT_ASC, 'name' => SORT_ASC])
                    ->indexBy('rs')
                    ->column();
                $_tmpCashboxNames = $company->getCashboxes()
                    ->select('name')
                    ->orderBy(['is_main' => SORT_DESC, 'name' => SORT_ASC])
                    ->indexBy('id')
                    ->column();
                $_tmpEmoneyNames =$company->getEmoneys()
                    ->select('name')
                    ->orderBy(['is_main' => SORT_DESC, 'name' => SORT_ASC])
                    ->indexBy('id')
                    ->column();

                if (count($multiCompanyIds) > 1) {
                    foreach ($_tmpBankNames as &$name) $name = ($company->getShortName()) . ' ' . $name;
                    foreach ($_tmpCashboxNames as &$name) $name = ($company->getShortName()) . ' ' . $name;
                    foreach ($_tmpEmoneyNames as &$name) $name = ($company->getShortName()) . ' ' . $name;
                }

                self::$walletName[CashFlowsBase::WALLET_BANK] += $_tmpBankNames;
                self::$walletName[CashFlowsBase::WALLET_CASHBOX] += $_tmpCashboxNames;
                self::$walletName[CashFlowsBase::WALLET_EMONEY] += $_tmpEmoneyNames;
            }
        }
    }

    public static function getByActivityEmptyData($year, $byDays = false)
    {
        return [
            AbstractFinance::OPERATING_ACTIVITIES_BLOCK => [
                'title' => 'Операционная деятельность',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                'levels' => [
                    AbstractFinance::INCOME_OPERATING_ACTIVITIES => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::WITHOUT_TYPE => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ],
                'question' => '#tooltip_operation_activities_block',
            ],
            AbstractFinance::FINANCIAL_OPERATIONS_BLOCK => [
                'title' => 'Финансовая деятельность',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                'levels' => [
                    AbstractFinance::RECEIPT_FINANCING_TYPE_FIRST => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::RECEIPT_FINANCING_TYPE_SECOND => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ],
                'question' => '#tooltip_financial_operations_block',
            ],
            AbstractFinance::INVESTMENT_ACTIVITIES_BLOCK => [
                'title' => 'Инвестиционная деятельность',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                'levels' => [
                    AbstractFinance::INCOME_INVESTMENT_ACTIVITIES => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::EXPENSE_INVESTMENT_ACTIVITIES => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ],
                'question' => '#tooltip_investment_activities_block',
            ],
            AbstractFinance::OWN_FUNDS_BLOCK => [
                'title' => 'Перемещение денег',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                'levels' => [
                    AbstractFinance::OWN_FUNDS_INCOME => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::OWN_FUNDS_EXPENSE => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ],
                'question' => '#tooltip_own_funds_block',
            ],
        ];
    }

    public static function getByActivityEmptyGrowingData($year, $byDays = false)
    {
        return [
            AbstractFinance::OPERATING_ACTIVITIES_BLOCK => [
                'title' => 'Остаток по оп. деятельности',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
            ],
            AbstractFinance::FINANCIAL_OPERATIONS_BLOCK => [
                'title' => 'Остаток по фин. деятельности',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
            ],
            AbstractFinance::INVESTMENT_ACTIVITIES_BLOCK => [
                'title' => 'Остаток по инвест. деятельности',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
            ],
            AbstractFinance::OWN_FUNDS_BLOCK => [
                'title' => 'Остаток по перемещению денег',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                'hide' => true
            ]
        ];
    }

    public static function getByPurseEmptyData($year, $byDays = false)
    {
        return [
            AbstractFinance::CASH_BANK_BLOCK => [
                'title' => 'Банк',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                'levels' => [
                    AbstractFinance::INCOME_CASH_BANK => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::EXPENSE_CASH_BANK => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ]
            ],
            AbstractFinance::CASH_ORDER_BLOCK => [
                'title' => 'Касса',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                'levels' => [
                    AbstractFinance::INCOME_CASH_ORDER => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::EXPENSE_CASH_ORDER => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ]
            ],
            AbstractFinance::CASH_EMONEY_BLOCK => [
                'title' => 'E-money',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                'levels' => [
                    AbstractFinance::INCOME_CASH_EMONEY => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::EXPENSE_CASH_EMONEY => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                        'levels' => []
                    ]
                ],
            ],
            AbstractFinance::CASH_ACQUIRING_BLOCK => [
                'title' => 'Интернет-эквайринг',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                'levels' => [
                    AbstractFinance::INCOME_ACQUIRING => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::EXPENSE_ACQUIRING => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ],
            ],
            AbstractFinance::CASH_CARD_BLOCK => [
                'title' => 'Карты',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                'levels' => [
                    AbstractFinance::INCOME_CARD => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::EXPENSE_CARD => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ],
            ],
        ];
    }

    public static function getByFlowAttributeEmptyData($year, $byDays = false, $params = [])
    {
        $attr = $params['attr'] ?? 'wallet';
        $isGrowing = $params['is_growing'] ?? false;
        $companyId = $params['company_id'] ?? -1;

        switch ($attr) {
            case 'wallet':
                return ($isGrowing)
                    ? self::getByPurseEmptyData($year, $byDays)
                    : self::getByPurseEmptyGrowingData($year, $byDays);
            case 'industry_id':
                $list = CompanyIndustry::find()
                    ->where(['company_id' => $companyId])
                    ->select(['id', 'name'])
                    ->orderBy('name')
                    ->asArray()
                    ->all();
                $list = array_merge($list, [['id' => 0, 'name' => 'Без направления']]);
                break;
            case 'sale_point_id':
                $list = SalePoint::find()
                    ->where(['company_id' => $companyId])
                    ->select(['id', 'name'])
                    ->orderBy('name')
                    ->asArray()
                    ->all();
                $list = array_merge($list, [['id' => 0, 'name' => 'Без точки продаж']]);
                break;
            default:
                $list = [];
                break;
        }

        $ret = [];
        foreach ($list as $l) {
            $itemID = $l['id'];
            $itemName = $l['name'];
            $ret[$itemID] = [
                'title' => ($isGrowing) ? "Остаток по {$itemName}" : $itemName,
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                'levels' => [
                    CashFlowsBase::FLOW_TYPE_INCOME => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    CashFlowsBase::FLOW_TYPE_EXPENSE => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ],
            ];
        }

        return $ret;
    }

    public static function getByPurseEmptyGrowingData($year, $byDays = false)
    {
        return [
            AbstractFinance::CASH_BANK_BLOCK => [
                'title' => 'Остаток по банку',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
            ],
            AbstractFinance::CASH_ORDER_BLOCK => [
                'title' => 'Остаток по кассе',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
            ],
            AbstractFinance::CASH_EMONEY_BLOCK => [
                'title' => 'Остаток по E-money',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
            ],
            AbstractFinance::CASH_ACQUIRING_BLOCK => [
                'title' => 'Остаток по интернет-эквайрингу',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
            ],
            AbstractFinance::CASH_CARD_BLOCK => [
                'title' => 'Остаток по картам',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
            ],
        ];
    }

    public static function getEmptyTotalData($year, $byDays = false)
    {
        return [
            'month' => [
                'title' => 'Результат по месяцу',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
            ],
            'growing' => [
                'title' => 'Остаток на конец месяца',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
            ]
        ];
    }

    public static function getEmptyYearInDays($year)
    {
        if (empty(self::$_EMPTY_YEAR_IN_DAYS)) {
            for ($m=1; $m<=12; $m++) {
                $month = str_pad($m, 2, "0", STR_PAD_LEFT);
                $daysCount = (new \DateTime("{$year}{$month}01"))->format('t');
                for ($d=1; $d<=$daysCount; $d++) {
                    $day = str_pad($d, 2, "0", STR_PAD_LEFT);
                    self::$_EMPTY_YEAR_IN_DAYS[$month.$day] = 0;
                }
            }
        }

        return self::$_EMPTY_YEAR_IN_DAYS;
    }

    public static function getActivityBlockType($type, $itemId)
    {
        if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
            if ($itemId == InvoiceIncomeItem::ITEM_OWN_FOUNDS) {
                return AbstractFinance::OWN_FUNDS_INCOME;
            } elseif (isset(self::$incomeItemSort[$itemId])) {
                return AbstractFinance::$typesByBlock[self::$incomeItemSort[$itemId]][0];
            } elseif (in_array($itemId, AbstractFinance::$typeItem[AbstractFinance::RECEIPT_FINANCING_TYPE_FIRST])) {
                return AbstractFinance::RECEIPT_FINANCING_TYPE_FIRST;
            } else {
                return AbstractFinance::INCOME_OPERATING_ACTIVITIES;
            }
        }
        if ($type == CashFlowsBase::FLOW_TYPE_EXPENSE) {
            if ($itemId == InvoiceExpenditureItem::ITEM_OWN_FOUNDS) {
                return AbstractFinance::OWN_FUNDS_EXPENSE;
            } elseif (isset(self::$expenditureItemSort[$itemId])) {
                return AbstractFinance::$typesByBlock[self::$expenditureItemSort[$itemId]][1];
            } elseif (in_array($itemId, AbstractFinance::$typeItem[AbstractFinance::RECEIPT_FINANCING_TYPE_SECOND])) {
                return AbstractFinance::RECEIPT_FINANCING_TYPE_SECOND;

            } else {
                return AbstractFinance::WITHOUT_TYPE;
            }
        }

        return AbstractFinance::FINANCIAL_OPERATIONS_BLOCK; // default
    }

    public static function getCashContractorName($type, $itemId, $contractorId = null)
    {
        if (in_array($contractorId, ['bank', 'order', 'emoney'])) {
            return ($type == 0 ? 'Перевод в ' : 'Приход из ') .
                ArrayHelper::getValue(AbstractFinance::$cashContractorTextByType, "{$type}.{$contractorId}");
        }

        return ArrayHelper::getValue(($type == 0) ? self::$expenditureItemName : self::$incomeItemName, $itemId);
    }

    public static function getByPurseEmptyDataDetailed($rawData, $year, $byDays = false)
    {
        $ret = [
            AbstractFinance::CASH_BANK_BLOCK => [
                'title' => 'Банк',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                'children' => [],
            ],
            AbstractFinance::CASH_ORDER_BLOCK => [
                'title' => 'Касса',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                'children' => [],
            ],
            AbstractFinance::CASH_EMONEY_BLOCK => [
                'title' => 'E-money',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                'children' => [],
            ],
            AbstractFinance::CASH_ACQUIRING_BLOCK => [
                'title' => 'Интернет-эквайринг',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                'children' => [],
            ],
            AbstractFinance::CASH_CARD_BLOCK => [
                'title' => 'Карты',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                'children' => [],
            ],
        ];

        foreach ($rawData as $data)
        {
            $walletTypeID = $data['wallet'];
            $walletID = $data['wallet_id'];

            if (!isset($ret[$walletTypeID])) {
                throw new Exception('Unknown wallet type ' . $walletTypeID);
            }

            if (!isset($ret[$walletTypeID]['children'][$walletID])) {

                $ret[$walletTypeID]['children'][$walletID] = [
                    'title' => ArrayHelper::getValue(self::$walletName, "{$walletTypeID}.{$walletID}", $walletID),
                    'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                    'levels' => [
                        CashFlowsBase::FLOW_TYPE_INCOME => [
                            'title' => 'Приход',
                            'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                            'levels' => []
                        ],
                        CashFlowsBase::FLOW_TYPE_EXPENSE => [
                            'title' => 'Расход',
                            'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                            'levels' => []
                        ],
                    ],
                ];
            }
        }

        return $ret;
    }

    public static function getByPurseEmptyGrowingDataDetailed($rawData, $year, $byDays = false)
    {
        // todo: дублирует ф-цию getByPurseEmptyDataDetailed() - только title другие
        $ret = [
            AbstractFinance::CASH_BANK_BLOCK => [
                'title' => 'Остаток по Банку',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                'children' => [],
            ],
            AbstractFinance::CASH_ORDER_BLOCK => [
                'title' => 'Остаток по Кассе',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                'children' => [],
            ],
            AbstractFinance::CASH_EMONEY_BLOCK => [
                'title' => 'Остаток по E-money',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                'children' => [],
            ],
            AbstractFinance::CASH_ACQUIRING_BLOCK => [
                'title' => 'Остаток по интернет-эквайрингу',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
            ],
            AbstractFinance::CASH_CARD_BLOCK => [
                'title' => 'Остаток по картам',
                'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
            ],
        ];

        foreach ($rawData as $data)
        {
            $walletTypeID = $data['wallet'];
            $walletID = $data['wallet_id'];

            if (!isset($ret[$walletTypeID])) {
                throw new Exception('Остаток по "Unknown wallet type"' . $walletTypeID);
            }

            if (!isset($ret[$walletTypeID]['children'][$walletID])) {
                $ret[$walletTypeID]['children'][$walletID] = [
                    'title' => 'Остаток по ' . ArrayHelper::getValue(self::$walletName, "{$walletTypeID}.{$walletID}", $walletID),
                    'data' => ($byDays) ? self::getEmptyYearInDays($year) : self::EMPTY_YEAR,
                    'levels' => []
                ];
            }
        }

        return $ret;
    }
}