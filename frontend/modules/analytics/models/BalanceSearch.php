<?php
/* 20-272 */
namespace frontend\modules\analytics\models;

use common\components\date\DateHelper;
use common\components\excel\Excel;
use common\components\helpers\Month;
use common\components\ImageHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use common\models\cash\CashFlowsBase;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\document\status\InvoiceStatus;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\modules\analytics\models\debtor\DebtorFilterHelper3;
use frontend\modules\analytics\models\debtor\DebtorHelper2;
use kartik\checkbox\CheckboxX;
use Yii;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class BalanceSearch
 * @package frontend\modules\analytics\models
 *
 */
class BalanceSearch extends AbstractFinance
{
    const MIN_REPORT_DATE = '2000-01-01';

    const TAB_BALANCE = 9;

    const TYPE_ASSETS = 1;
    const TYPE_PASSIVE = 2;

    protected $data = [];
    protected $_company;
    protected $_year;
    protected $_months;

    protected $_balanceItemAmountArray = [];

    // to calc undestributed profits
    protected $_modelProfitAndLoss;

    public function rules()
    {
        return [[['year'], 'safe']];
    }

    public function init()
    {
        parent::init();

        $this->initCustomBalanceItems();

        // to calc undestributed profits + tax
        $this->_initProfitAndLossModel();
    }

    private function _initProfitAndLossModel()
    {
        $this->_modelProfitAndLoss = new ProfitAndLossSearchModel();

        $this->_modelProfitAndLoss->handleItems();
    }

    /////// OLAP ///////////////////////////////////////////////
    private function initCustomBalanceItems()
    {
        $arr = (new Query())
            ->select(['company_id', 'item_id', 'month', 'year', 'amount'])
            ->from(BalanceItemAmount::tableName())
            ->where(['company_id' => $this->multiCompanyIds])->all(Yii::$app->db2);

        $b = &$this->_balanceItemAmountArray;

        foreach ($arr as $a) {
            if (!isset($b[$a['item_id']]))
                $b[$a['item_id']] = [];
            if (!isset($b[$a['item_id']][$a['year'].$a['month']]))
                $b[$a['item_id']][$a['year'].$a['month']] = 0;

            $b[$a['item_id']][$a['year'].$a['month']] += $a['amount'];
        }
    }

    ////// OLAP METHODS ///////////////////////////////////////
    private function __calcUndestributedProfits($key, $Y)
    {
        /** @var ProfitAndLossSearchModel $model */
        $model = &$this->_modelProfitAndLoss;

        for ($i=1; $i<=12; $i++) {
            $monthNumber = str_pad($i, 2, "0", STR_PAD_LEFT);
            $this->data[$key][$monthNumber] = $model->getValue('undestributedProfits', $Y, $monthNumber, 'amount');
        }
    }

    public function getCapitalUndestributedProfits($date)
    {
        $Y = $date->format('Y');
        $m = $date->format('m');

        $key = 'getCapitalUndestributedProfits' . $Y;

        if (!array_key_exists($key, $this->data)) {

            $this->__calcUndestributedProfits($key, $Y);
        }

        return ArrayHelper::getValue($this->data[$key], $m);
    }

    public function getUndestributedProfitsPrev($date)
    {
        $Y = $date->format('Y') - 1;
        $m = $date->format('m');

        $key = 'getUndestributedProfitsPrev' . $Y;

        if (!array_key_exists($key, $this->data)) {

            $this->__calcUndestributedProfits($key, $Y);
        }

        return ArrayHelper::getValue($this->data[$key], 12, 0);
    }

    public function getBalanceItemAmount($itemID, $date)
    {
        return  ArrayHelper::getValue(ArrayHelper::getValue($this->_balanceItemAmountArray, $itemID), $date->format('Ymd'), 0);
    }

    public function getStocks($date)
    {
        $Y = $date->format('Y');
        $m = $date->format('m');

        $key = 'getStocks' . $Y;

        if (!array_key_exists($key, $this->data)) {

            $companyIds = implode(',', $this->multiCompanyIds);
            $productTable = Product::tableName();
            $turnoverTable = self::TABLE_PRODUCT_TURNOVER;
            $seq = self::__getSeq($Y);
            $interval = self::__getInterval($Y);

            $monthSum = Yii::$app->db2->createCommand("
                SELECT MONTH({$interval}) m,
                SUM(IF(`type` = 2, -1, 1) * IFNULL(p.price_for_buy_with_nds,0) * t.quantity) AS amount
                FROM {$seq}
                JOIN {$turnoverTable} t 
                  ON  t.company_id IN ({$companyIds})
                  AND t.date <= {$interval}
                  AND t.production_type = 1
                  AND t.is_document_actual = TRUE
                  AND t.is_invoice_actual = TRUE
                LEFT JOIN {$productTable} p ON p.id = t.product_id
                GROUP BY {$interval}
           ")->queryAll();

            $this->data[$key] = [];
            foreach ($monthSum as $sum) {
                if ($Y == date('Y') && $sum['m'] > date('m'))
                    continue;

                $this->data[$key][str_pad($sum['m'], 2, "0", STR_PAD_LEFT)] = $sum['amount'];
            }
        }

        return ArrayHelper::getValue($this->data[$key], $m);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private static function __getInterval($year)
    {
        $startDate = $year . '-01-31';

        if ($year == date('Y')) {
            $lastDate = date('Y-m-d');
            $replacedDate = date('Y-m-t');

            return "IF (('{$startDate}' + INTERVAL (seq) MONTH) = '{$replacedDate}', '{$lastDate}', ('{$startDate}' + INTERVAL (seq) MONTH))";
        }

        return "('{$startDate}' + INTERVAL (seq) MONTH)";
    }

    private static function __getSeq($year)
    {
        return 'seq_0_to_' . ($year == date('Y') ? (date('n') - 1) : 11);
    }

    private function __getPrepaidByDocs($date, $docType)
    {
        $Y = $date->format('Y');

        $companyIds = implode(',', $this->multiCompanyIds);
        $table = self::TABLE_OLAP_INVOICES;
        $seq = self::__getSeq($Y);
        $interval = self::__getInterval($Y);

        $monthSum = Yii::$app->db2->createCommand("
            SELECT MONTH({$interval}) m,
            IFNULL(SUM(t.total_amount_with_nds), 0) amount
            FROM {$seq}
            JOIN {$table} t 
                ON  (t.company_id IN ({$companyIds}))
                AND (FROM_UNIXTIME(t.status_updated_at, '%Y-%m-%d') <= {$interval})
                AND (t.is_deleted = FALSE)
                AND (t.type = {$docType}) 
                AND (t.status_id = ".InvoiceStatus::STATUS_PAYED.")
                AND (t.has_doc = FALSE OR (t.has_doc = TRUE AND t.doc_date > {$interval}))
                AND (t.need_all_docs = TRUE)
            GROUP BY ({$interval})
       ")->queryAll();

        $ret = [];
        foreach ($monthSum as $m) {
            if ($Y == date('Y') && $m['m'] > date('m'))
                continue;

            $ret[str_pad($m['m'], 2, "0", STR_PAD_LEFT)] = $m['amount'];
        }

        return $ret;
    }

    private function __getPrepaidByFlows($date, $flowType)
    {
        $Y = $date->format('Y');

        $companyIds = implode(',', $this->multiCompanyIds);
        $table = self::TABLE_OLAP_FLOWS;
        $seq = self::__getSeq($Y);
        $interval = self::__getInterval($Y);

        $excludeItems = [
            CashFlowsBase::FLOW_TYPE_EXPENSE => [
                InvoiceExpenditureItem::ITEM_ENSURE_PAYMENT
            ]
        ];
        $SQL_FILTER_ITEMS = DebtorFilterHelper3::getSqlFilterItems($this->multiCompanyIds, $flowType, $excludeItems);
        $SQL_FILTER_WALLETS = DebtorFilterHelper3::getSqlFilterWallets($this->multiCompanyIds);

        $monthSum = Yii::$app->db2->createCommand("
            SELECT MONTH({$interval}) m,
            IFNULL(SUM(t.amount - t.invoice_amount), 0) amount
            FROM {$seq}
            JOIN {$table} t 
                ON  (t.company_id IN ({$companyIds}))
                AND (t.date <= {$interval})
                AND (t.type = {$flowType}) 
                AND (t.contractor_id > 0) AND (t.invoice_amount < t.amount)
                {$SQL_FILTER_ITEMS}
                {$SQL_FILTER_WALLETS}
            GROUP BY ({$interval})
       ")->queryAll();

        $ret = [];
        foreach ($monthSum as $m) {
            if ($Y == date('Y') && $m['m'] > date('m'))
                continue;

            $ret[str_pad($m['m'], 2, "0", STR_PAD_LEFT)] = $m['amount'];
        }

        return $ret;
    }

    private function getPrepaidAmount($date, $documentType, $flowType)
    {
        $Y = $date->format('Y');
        $m = $date->format('m');

        $key = 'getPrepaidAmount' . "_{$documentType}" . "_{$flowType}_" . $Y;

        if (!array_key_exists($key, $this->data)) {

            $byDocs = $this->__getPrepaidByDocs($date, $documentType);
            $byFlows = $this->__getPrepaidByFlows($date, $flowType);

            foreach ($this->months as $d) {
                $this->data[$key][$d->format('m')] =
                    ArrayHelper::getValue($byDocs, $d->format('m')) +
                    ArrayHelper::getValue($byFlows, $d->format('m'));
            }
        }

        return ArrayHelper::getValue($this->data[$key], $m);
    }

    private function _getOverdueInvoices($date, $type)
    {
        $Y = $date->format('Y');
        $m = $date->format('m');

        $key = 'getOverdueInvoices' . "_{$type}_" . $Y;

        if (!array_key_exists($key, $this->data)) {

            DebtorHelper2::$SEARCH_BY = DebtorHelper2::SEARCH_BY_DOCUMENTS;
            $subQuery = DebtorHelper2::getQueryByDocs($this->multiCompanyIds, $type);
            $calc = [];
            for ($i = 1; $i <= 12; $i++) {
                $month = str_pad($i, 2, "0", STR_PAD_LEFT);
                $calc[$i] = "IF(DATE_FORMAT(t.date, '%Y%m') <= {$Y}{$month}, invoice_sum - flow_sum, 0)";
            }

            $query = "
                SELECT
                  contractor_id,
                  GREATEST(SUM({$calc[1]}), 0)  `01`,
                  GREATEST(SUM({$calc[2]}), 0)  `02`,
                  GREATEST(SUM({$calc[3]}), 0)  `03`,
                  GREATEST(SUM({$calc[4]}), 0)  `04`,
                  GREATEST(SUM({$calc[5]}), 0)  `05`,
                  GREATEST(SUM({$calc[6]}), 0)  `06`,
                  GREATEST(SUM({$calc[7]}), 0)  `07`,
                  GREATEST(SUM({$calc[8]}), 0)  `08`,
                  GREATEST(SUM({$calc[9]}), 0)  `09`,
                  GREATEST(SUM({$calc[10]}), 0) `10`,
                  GREATEST(SUM({$calc[11]}), 0) `11`,
                  GREATEST(SUM({$calc[12]}), 0) `12`
                FROM (" . $subQuery->createCommand()->rawSql . ") t
                GROUP BY contractor_id
            ";

            $this->data[$key] = ['01' => 0, '02' => 0, '03' => 0, '04' => 0, '05' => 0, '06' => 0, '07' => 0, '08' => 0, '09' => 0, '10' => 0, '11' => 0, '12' => 0];
            $_totalByYear = Yii::$app->db2->createCommand($query)->queryAll();
            foreach ((array)$_totalByYear as $_tmpRow)
                foreach ($_tmpRow as $_month => $_amount) {
                    if ($_month == 'contractor_id')
                        continue;
                    $this->data[$key][$_month] += $_amount;
                }
        }

        return ArrayHelper::getValue($this->data[$key], $m);
    }

    public function getVendorInvoicesPayable($date)
    {
        return $this->_getOverdueInvoices($date, Documents::IO_TYPE_IN);
    }

    public function getTradeReceivables($date)
    {
        return $this->_getOverdueInvoices($date, Documents::IO_TYPE_OUT);
    }

    // FLOWS
    private function _getWallet($date, $wallet)
    {
        $Y = $date->format('Y');
        $m = $date->format('m');

        switch ($wallet) {
            case CashFlowsBase::WALLET_BANK:
                $key = 'getBankAccounts' . $Y;
                break;
            case CashFlowsBase::WALLET_CASHBOX:
                $key = 'getCashbox' . $Y;
                break;
            case CashFlowsBase::WALLET_EMONEY:
                $key = 'getEmoney' . $Y;
                break;
            default:
                throw new Exception('Unknown wallet id');
        }

        if (!array_key_exists($key, $this->data)) {

            $startSum = (new Query)->select(new Expression('
                    SUM(IF(t.type = 1, t.amount, -1 * t.amount)) AS amount'))
                ->from(['t' => self::TABLE_OLAP_FLOWS])
                ->where(['t.company_id' => $this->multiCompanyIds])
                ->andWhere(['t.wallet' => $wallet])
                ->andWhere(['<', 't.date', "{$Y}-01-01"])
                ->scalar(Yii::$app->db2);

            $monthSum = (new Query)->select(new Expression(' 
                    SUM(IF(t.type = 1, t.amount, -1 * t.amount)) AS amount, 
                    DATE_FORMAT(t.date,"%m") AS month'))
                ->from(['t' => self::TABLE_OLAP_FLOWS])
                ->indexBy('month')
                ->groupBy(['YEAR(t.date)', 'MONTH(t.date)'])
                ->where(['t.company_id' => $this->multiCompanyIds])
                ->andWhere(['t.wallet' => $wallet])
                ->andWhere(['between', 't.date', "{$Y}-01-01", "{$Y}-12-31"])
                ->all(Yii::$app->db2);

            // growing
            foreach ($this->months as $d) {
                $sum = $startSum + ArrayHelper::getValue(ArrayHelper::getValue($monthSum, $d->format('m')), 'amount');
                $startSum = $sum;
                $this->data[$key][$d->format('m')] = $sum;
            }

            // fill current month
            if ($Y == date('Y') && empty($this->data[$key][date('m')]) && !empty($this->data[$key])) {
                $this->data[$key][date('m')] = $this->data[$key][array_key_last($this->data[$key])];
            }
        }

        return ArrayHelper::getValue($this->data[$key], $m);
    }

    private function _getAllWalletsByItem($date, $incomeItemId, $expenditureItemId, $isInverted = false)
    {
        $Y = $date->format('Y');
        $m = $date->format('m');

        if (is_array($incomeItemId) || is_array($expenditureItemId)) {
            $key = 'getWalletsByItem_' . $Y . '_' . implode('.', (array)$incomeItemId) . '_' .  implode('.', (array)$expenditureItemId);
        } else {
            $key = 'getWalletsByItem_' . $Y . '_' . "{$incomeItemId}_$expenditureItemId";
        }

        if (!array_key_exists($key, $this->data)) {

            $startIncomeSum = (new Query)->select(new Expression('SUM(t.amount) AS amount'))
                ->from(['t' => self::TABLE_OLAP_FLOWS])
                ->where(['t.company_id' => $this->multiCompanyIds])
                ->andWhere(['t.type' => CashFlowsBase::FLOW_TYPE_INCOME])
                ->andWhere(['t.item_id' => $incomeItemId])
                ->andWhere(['<', 't.date', "{$Y}-01-01"])
                ->scalar(Yii::$app->db2);
            $startExpenseSum = (new Query)->select(new Expression('SUM(t.amount) AS amount'))
                ->from(['t' => self::TABLE_OLAP_FLOWS])
                ->where(['t.company_id' => $this->multiCompanyIds])
                ->andWhere(['t.type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
                ->andWhere(['t.item_id' => $expenditureItemId])
                ->andWhere(['<', 't.date', "{$Y}-01-01"])
                ->scalar(Yii::$app->db2);

            $monthIncomeSum = (new Query)->select(new Expression(' 
                    SUM(t.amount) AS amount, 
                    DATE_FORMAT(t.date,"%m") AS month'))
                ->from(['t' => self::TABLE_OLAP_FLOWS])
                ->indexBy('month')
                ->groupBy(['YEAR(t.date)', 'MONTH(t.date)'])
                ->where(['t.company_id' => $this->multiCompanyIds])
                ->andWhere(['t.type' => CashFlowsBase::FLOW_TYPE_INCOME])
                ->andWhere(['t.item_id' => $incomeItemId])
                ->andWhere(['between', 't.date', "{$Y}-01-01", "{$Y}-12-31"])
                ->all(Yii::$app->db2);
            $monthExpenseSum = (new Query)->select(new Expression(' 
                    SUM(t.amount) AS amount, 
                    DATE_FORMAT(t.date,"%m") AS month'))
                ->from(['t' => self::TABLE_OLAP_FLOWS])
                ->indexBy('month')
                ->groupBy(['YEAR(t.date)', 'MONTH(t.date)'])
                ->where(['t.company_id' => $this->multiCompanyIds])
                ->andWhere(['t.type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
                ->andWhere(['t.item_id' => $expenditureItemId])
                ->andWhere(['between', 't.date', "{$Y}-01-01", "{$Y}-12-31"])
                ->all(Yii::$app->db2);

            // growing
            $startSum = $isInverted ? ($startExpenseSum - $startIncomeSum) : ($startIncomeSum - $startExpenseSum);
            foreach ($this->months as $d) {
                $sum = $startSum
                    + ArrayHelper::getValue(ArrayHelper::getValue($isInverted ? $monthExpenseSum : $monthIncomeSum, $d->format('m')), 'amount')
                    - ArrayHelper::getValue(ArrayHelper::getValue($isInverted ? $monthIncomeSum : $monthExpenseSum, $d->format('m')), 'amount');

                $startSum = max(0, $sum);
                $this->data[$key][$d->format('m')] = max(0, $sum);
            }

            // fill current month
            if ($Y == date('Y') && empty($this->data[$key][date('m')]) && !empty($this->data[$key])) {
                $this->data[$key][date('m')] = $this->data[$key][array_key_last($this->data[$key])];
            }
        }

        return ArrayHelper::getValue($this->data[$key], $m);
    }

    public function getBankAccounts($date)
    {
        return $this->_getWallet($date, CashFlowsBase::WALLET_BANK);
    }

    public function getCashbox($date)
    {
        return $this->_getWallet($date, CashFlowsBase::WALLET_CASHBOX);
    }

    public function getEmoney($date)
    {
        return $this->_getWallet($date, CashFlowsBase::WALLET_EMONEY);
    }

    public function getLoansIssued($date)
    {
        return $this->_getAllWalletsByItem($date,
            InvoiceIncomeItem::ITEM_BORROWING_REDEMPTION,
            InvoiceExpenditureItem::ITEM_BORROWING_EXTRADITION,
            true);
    }

    public function getRentalDeposit($date)
    {
        return $this->_getAllWalletsByItem($date,
            InvoiceIncomeItem::ITEM_ENSURE_PAYMENT,
            InvoiceExpenditureItem::ITEM_ENSURE_PAYMENT,
            true);
    }

    public function getShorttermBorrowings($date)
    {
        return $this->_getAllWalletsByItem($date,
            [InvoiceIncomeItem::ITEM_LOAN, InvoiceIncomeItem::ITEM_CREDIT],
            [InvoiceExpenditureItem::ITEM_REPAYMENT_CREDIT, InvoiceExpenditureItem::ITEM_LOAN_REPAYMENT]);
    }

    public function getPrepaymentsToSuppliers($date)
    {
        return $this->getPrepaidAmount($date, Documents::IO_TYPE_IN, CashFlowsBase::FLOW_TYPE_EXPENSE);
    }

    /**
     * @return integer|null
     */
    public function getPrepaymentOfCustomers($date)
    {
        return $this->getPrepaidAmount($date, Documents::IO_TYPE_OUT, CashFlowsBase::FLOW_TYPE_INCOME);
    }
    ///////////////////////////////////////////////////////////

    /**
     * @inheritdoc
     */
    public function handleItems($itemsArray)
    {
        $result = [];
        foreach ($itemsArray as $item) {

            $getter = ArrayHelper::remove($item, 'getter');
            $items = ArrayHelper::remove($item, 'items');

            foreach ($this->months as $date) {
                $item['data'][$date->format('n')] = $this->$getter($date);
            }

            $existsMonthCount = count($item['data']);
            if ($existsMonthCount < 12) {
                for ($i = 1; $i <= 12 - $existsMonthCount; $i++) {
                    $item['data'][$existsMonthCount + $i] = null;
                }
            }

            $quarterAmount = null;
            $totalAmount = null;
            $newItemData = [];
            foreach ($item['data'] as $key => $oneAmount) {
                $newItemData[$key] = $oneAmount;
                if ($key > date('n') && $this->year == date('Y')) {
                    $totalAmount = $item['data'][date('n')];
                } else {
                    $totalAmount = $oneAmount;
                }
                if ($key > date('n') && ceil($key / 3) == ceil(date('n') / 3) && $this->year == date('Y')) {
                    $quarterAmount = $item['data'][date('n')];
                } else {
                    $quarterAmount = $oneAmount;
                }
                if (is_int($key / 3)) {
                    $newItemData['quarter-' . ($key / 3)] = $quarterAmount;
                    $quarterAmount = 0;
                }
            }
            $newItemData['total'] = $totalAmount;
            $item['data'] = $newItemData;
            $item['items'] = $items ? $this->handleItems($items) : null;
            $result[] = $item;
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getMonths()
    {
        if ($this->_months === null) {
            $this->_months = [];
            $currentYear = date('Y');
            $toMonth = $this->year == $currentYear ? date('n') : 12;
            $date = date_create("{$this->year}-01-01");
            $date->modify('last day of this month')->setTime(23, 59, 59);

            $this->_months[] = clone $date;
            while ($date->format('n') != $toMonth) {
                $date->modify('last day of +1 month');
                $this->_months[] = $this->year == $currentYear && $date->format('n') == date('n') ? date_create() : clone $date;
            }
        }

        $result = [];
        foreach ($this->_months as $m) {
            $result[] = clone $m;
        }

        return $result;
    }

    /**
     * @param $params
     * @return array
     */
    public function search($params)
    {
        $this->_load($params, '');

        $result = $this->handleItems($this->buildItems());

        return $result;
    }

    /**
     * @return integer|null
     */
    public function getAssets($date)
    {
        $key = 'getAssets' . $date->format('Ymd');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getFixedAssets(clone $date) +
                $this->getCurrentAssets(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getFixedAssets($date)
    {
        $key = 'getFixedAssets' . $date->format('Ymd');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getRealEstateObjects(clone $date) +
                $this->getEquipmentAssets(clone $date) +
                $this->getTransportAssets(clone $date) +
                $this->getIntangibleAssets(clone $date) +
                $this->getOtherAssets(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getRealEstateObjects($date)
    {
        return $this->getBalanceItemAmount(BalanceItem::ITEM_REAL_ESTATE_OBJECT, $date);
    }

    /**
     * @return integer|null
     */
    public function getEquipmentAssets($date)
    {
        return $this->getBalanceItemAmount(BalanceItem::ITEM_EQUIPMENT_ASSETS, $date);
    }

    /**
     * @return integer|null
     */
    public function getTransportAssets($date)
    {
        return $this->getBalanceItemAmount(BalanceItem::ITEM_TRANSPORT_ASSETS, $date);
    }

    /**
     * @param $date
     * @return null|string
     */
    public function getIntangibleAssets($date)
    {
        return $this->getBalanceItemAmount(BalanceItem::ITEM_INTANGIBLE_ASSETS, $date);
    }

    /**
     * @return integer|null
     */
    public function getOtherAssets($date)
    {
        return $this->getBalanceItemAmount(BalanceItem::ITEM_OTHER_ASSETS, $date);
    }

    /**
     * @return integer|null
     */
    public function getCurrentAssets($date)
    {
        $key = 'getCurrentAssets' . $date->format('Ymd');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getStocks(clone $date) +
                $this->getReceivables(clone $date) +
                $this->getCash(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getReceivables($date)
    {
        $key = 'getReceivables' . $date->format('Ymd');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getTradeReceivables(clone $date) +
                $this->getPrepaymentsToSuppliers(clone $date) +
                $this->getBudgetOverpayment(clone $date) +
                $this->getLoansIssued(clone $date) +
                $this->getOtherInvestments(clone $date) +
                $this->getRentalDeposit(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getBudgetOverpayment($date)
    {
        return $this->getBalanceItemAmount(BalanceItem::ITEM_BUDGET_OVERPAYMENT, $date);
    }

    /**
     * @return integer|null
     */
    public function getOtherInvestments($date)
    {
        return $this->getBalanceItemAmount(BalanceItem::ITEM_OTHER_INVESTMENTS, $date);
    }

    /**
     * @return integer|null
     */
    public function getCash($date)
    {
        $key = 'getCash' . $date->format('Ymd');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getBankAccounts(clone $date) +
                $this->getCashbox(clone $date) +
                $this->getEmoney(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getLiabilities($date)
    {
        $key = 'getLiabilities' . $date->format('Ymd');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getCapital(clone $date) +
                $this->getLongtermDuties(clone $date) +
                $this->getShorttermLiabilities(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getCapital($date)
    {
        $key = 'getCapital' . $date->format('Ymd');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getAuthorizedCapital(clone $date) +
                $this->getCapitalUndestributedProfits(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getAuthorizedCapital($date)
    {
        return $this->getBalanceItemAmount(BalanceItem::ITEM_AUTHORIZED_CAPITAL, $date) > 0 ?
            $this->getBalanceItemAmount(BalanceItem::ITEM_AUTHORIZED_CAPITAL, $date) :
            $this->company->capital;
    }

    /**
     * @return integer|null
     */
    public function getUndestributedProfits($date)
    {
        return $this->getCapitalUndestributedProfits($date) - $this->getUndestributedProfitsPrev($date);
    }

    /**
     * @return integer|null
     */
    public function getLongtermDuties($date)
    {
        $key = 'getLongtermDuties' . $date->format('Ymd');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getLongtermLoans(clone $date) +
                $this->getAccountsPayable(clone $date) +
                $this->getTargetedFinancing(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getLongtermLoans($date)
    {
        return null;
    }

    /**
     * @return integer|null
     */
    public function getAccountsPayable($date)
    {
        return null;
    }

    /**
     * @return integer|null
     */
    public function getTargetedFinancing($date)
    {
        return null;
    }

    /**
     * @return integer|null
     */
    public function getShorttermLiabilities($date)
    {
        $key = 'getShorttermLiabilities' . $date->format('Ymd');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getShorttermBorrowings(clone $date) +
                $this->getShorttermAccountsPayable(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @param $date
     * @return mixed
     */
    public function getBalanceCheck($date)
    {
        $key = 'getBalanceCheck' . $date->format('Ymd');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getAssets(clone $date) - $this->getLiabilities(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getShorttermAccountsPayable($date)
    {
        $key = 'getShorttermAccountsPayable' . $date->format('Ymd');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getVendorInvoicesPayable(clone $date) +
                $this->getPrepaymentOfCustomers(clone $date) +
                $this->getCommodityLoans(clone $date) +
                $this->getRentalArrears(clone $date) +
                $this->getSalaryArrears(clone $date) +
                $this->getBudgetArrears(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getCommodityLoans($date)
    {
        return null;
    }

    /**
     * @return integer|null
     */
    public function getRentalArrears($date)
    {
        return null;
    }

    /**
     * @return integer|null
     */
    public function getSalaryArrears($date)
    {
        return null;
    }

    /**
     * @return integer|null
     */
    public function getBudgetArrears($date)
    {
        $Y = $date->format('Y');
        $m = $date->format('m');

        $key = 'getBudgetArrears' . $Y;

        if (!array_key_exists($key, $this->data)) {

            /** @var ProfitAndLossSearchModel $model */
            $model = &$this->_modelProfitAndLoss;
            $taxYears = $model->getResultYears();
            $taxes = $this->__getTaxes($taxYears, $model);
            $paidTaxes = $this->__getPaidTaxes($taxYears, $model);

            $growingTax = 0;
            foreach ($taxes as $yearMonth => $taxAmount) {

                $year = substr($yearMonth, 0, 4);
                $month = substr($yearMonth, 4, 2);
                $monthTax = $taxAmount + $growingTax;
                $growingTax = $monthTax;

                // collapse paid taxes
                foreach ($paidTaxes as $paidYear => $codes)
                    foreach ($codes as $code => $periods)
                        foreach ($periods as $period => $taxPile)
                            if ($taxPile['amount'] > 0 && $yearMonth >= $taxPile['fromYM']) {
                                if ($taxPile['amount'] >= $growingTax) {
                                    $paidTaxes[$year][$code][$period]['amount'] -= $growingTax;
                                    $growingTax = 0;
                                } else {
                                    $growingTax -= $taxPile['amount'];
                                    $paidTaxes[$year][$code][$period]['amount'] = 0;
                                }
                            }

                if ($year == $Y)
                    $this->data[$key][$month] = $growingTax;
            }
        }

        return ArrayHelper::getValue($this->data[$key] ?? [], $m);
    }
    
    protected function __getTaxes($years, ProfitAndLossSearchModel $model)
    {
        $taxes = [];

        foreach ($years as $year) {
            for ($i=1; $i<=12; $i++) {
                $monthNumber = str_pad($i, 2, "0", STR_PAD_LEFT);
                $taxes[$year.$monthNumber] = $model->getValue('tax', $year, $monthNumber, 'amount');
            }
        }

        return $taxes;
    }
    
    protected function __getPaidTaxes($years, ProfitAndLossSearchModel $model)
    {
        $paidTaxes = [];
        foreach ($years as $year) {
            $paidTaxes[$year] = [
                'ГД' => [
                    '00' => ['amount' => 0, 'fromYM' => "{$year}01"],
                ],
                'ПЛ' => [
                    '01' => ['amount' => 0, 'fromYM' => "{$year}01"],
                    '02' => ['amount' => 0, 'fromYM' => "{$year}07"],
                ],
                'КВ' => [
                    '01' => ['amount' => 0, 'fromYM' => "{$year}01"],
                    '02' => ['amount' => 0, 'fromYM' => "{$year}04"],
                    '03' => ['amount' => 0, 'fromYM' => "{$year}07"],
                    '04' => ['amount' => 0, 'fromYM' => "{$year}10"],
                ],
                'МС' => [
                    '01' => ['amount' => 0, 'fromYM' => "{$year}01"],
                    '02' => ['amount' => 0, 'fromYM' => "{$year}02"],
                    '03' => ['amount' => 0, 'fromYM' => "{$year}03"],
                    '04' => ['amount' => 0, 'fromYM' => "{$year}04"],
                    '05' => ['amount' => 0, 'fromYM' => "{$year}05"],
                    '06' => ['amount' => 0, 'fromYM' => "{$year}06"],
                    '07' => ['amount' => 0, 'fromYM' => "{$year}07"],
                    '08' => ['amount' => 0, 'fromYM' => "{$year}08"],
                    '09' => ['amount' => 0, 'fromYM' => "{$year}09"],
                    '10' => ['amount' => 0, 'fromYM' => "{$year}10"],
                    '11' => ['amount' => 0, 'fromYM' => "{$year}11"],
                    '12' => ['amount' => 0, 'fromYM' => "{$year}12"],
                ],
            ];
        }

        $taxFlows = (new Query)
            ->select(new Expression('
                    t.tax_period_code,
                    t.amount
                '))
            ->from(['t' => CashBankFlows::tableName()])
            ->where(['t.company_id' => $this->multiCompanyIds])
            ->andWhere(['t.flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
            ->andWhere(['t.expenditure_item_id' => [
                InvoiceExpenditureItem::ITEM_TAX_AND_PROFIT,
                InvoiceExpenditureItem::ITEM_TAX_USN_6,
                InvoiceExpenditureItem::ITEM_TAX_USN_15
            ]])
            ->all(Yii::$app->db2);

        foreach ($taxFlows as $flow) {

            $code  = mb_substr($flow['tax_period_code'], 0, 2);
            $part = mb_substr($flow['tax_period_code'], 3, 2);
            $year  = mb_substr($flow['tax_period_code'], 6, 4);

            if ($code && $part && $year) {

                if ($code == 'ГД' && isset($paidTaxes[$year][$code]['00']))
                    $paidTaxes[$year][$code]['00']['amount'] += $flow['amount'];
                elseif ($code == 'ПЛ' && isset($paidTaxes[$year][$code][$part]))
                    $paidTaxes[$year][$code][$part]['amount'] += $flow['amount'];
                elseif ($code == 'КВ' && isset($paidTaxes[$year][$code][$part]))
                    $paidTaxes[$year][$code][$part]['amount'] += $flow['amount'];
                elseif (isset($paidTaxes[$year]['МС'][$part]))
                    $paidTaxes[$year]['МС'][$part]['amount'] += $flow['amount'];
            }
        }

        return $paidTaxes;
    }

    public function getIncompleteProjects($date) {
        return 0;
    }

    /**
     * @param $row
     * @return string
     * @throws \Exception
     */
    public function renderRow($row)
    {
        $currentQuarter = (int)ceil(date('m') / 3);
        $isCurrentYear = date('Y') == $this->year;

        $options = ArrayHelper::remove($row, 'options', []);
        $skipEmpty = ArrayHelper::remove($row, 'skipEmpty', false);
        $canUpdate = ArrayHelper::remove($row, 'canUpdate', false);
        if ($skipEmpty && array_sum($row['data']) === 0) {
            Html::addCssClass($options, 'hidden');
        }
        $content = Html::beginTag('tr', $options);
        $label = null;
        if (isset($row['addCheckboxX']) && $row['addCheckboxX']) {
            $label = CheckboxX::widget([
                    'name' => 'balance-type',
                    'value' => true,
                    'options' => [
                        'class' => 'balance-type',
                    ],
                    'pluginOptions' => [
                        'size' => 'xs',
                        'threeState' => false,
                        'inline' => false,
                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                    ],
                ]) . $row['label'];
        } else {
            if (stristr($row['label'], 'Активы') === false && stristr($row['label'], 'Пассивы') === false &&
                stristr($row['label'], 'АКТИВ - ПАССИВ') === false) {
                $label = ImageHelper::getThumb('img/menu-humburger.png', [15, 10], [
                    'class' => 'sortable-row-icon',
                    'style' => 'margin-right: 15px;',
                ]);
            }
            $label .= $row['label'];
            if ($canUpdate) {
                $label .= '<span class="glyphicon glyphicon-pencil pull-right update-balance-item" style="cursor: pointer; display:inline-block; color:#5b9bd1"></span>';
            }
        }
        $style = isset($options['style']) ? $options['style'] : null;
        $content .= Html::tag('td', $label, [
            'style' => 'width: 270px; ' . $style,
        ]);
        foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthName) {
            $quarter = ceil($monthNumber / 3);
            $val = $row['data'][(int)$monthNumber];
            $content .= Html::tag('td', number_format($val / 100, 2, ',', ' '), [
                'class' => 'value-cell quarter-month-' . $quarter,
                'style' => 'width: 117px; display: ' . ($currentQuarter == $quarter && $isCurrentYear ? null : 'none'),
            ]);
            $quarter = (int)$monthNumber / 3;
            if (is_int($quarter)) {
                $val = $row['data']['quarter-' . $quarter];
                $content .= Html::tag('td', number_format($val / 100, 2, ',', ' '), [
                    'class' => 'value-cell quarter-' . $quarter,
                    'style' => 'width: 172px; display: ' . ($currentQuarter == $quarter && $isCurrentYear ? 'none' : null),
                ]);
            }
        }
        $val = $row['data']['total'];
        $content .= Html::tag('td', number_format($val / 100, 2, ',', ' '), [
            'class' => 'value-cell total-value',
        ]);
        $content .= Html::endTag('tr') . "\n";
        if (isset($row['items'])) {
            foreach ($row['items'] as $item) {
                $content .= $this->renderRow($item);
            }
        }

        return $content;
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        $range = [];
        $cashOrderMinDate = CashOrderFlows::find()
            ->byCompany($this->multiCompanyIds)
            ->min('date');
        $cashBankMinDate = CashBankFlows::find()
            ->byCompany($this->multiCompanyIds)
            ->min('date');
        $cashEmoneyMinDate = CashEmoneyFlows::find()
            ->byCompany($this->multiCompanyIds)
            ->min('date');
        $minDates = [];

        if ($cashOrderMinDate) $minDates[] = $cashOrderMinDate;
        if ($cashBankMinDate) $minDates[] = $cashBankMinDate;
        if ($cashEmoneyMinDate) $minDates[] = $cashEmoneyMinDate;

        $minCashDate = !empty($minDates) ? max(self::MIN_REPORT_DATE, min($minDates)) : date(DateHelper::FORMAT_DATE);
        $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
        $currentYear = date('Y');
        foreach (range($registrationYear, $currentYear) as $value) {
            $range[$value] = $value;
        }
        arsort($range);

        return $range;
    }

    /**
     * @return array
     */
    public function buildItems()
    {
        $fixedAssetItems = $this->getFormattedItems([
            BalanceItem::ITEM_REAL_ESTATE_OBJECT,
            BalanceItem::ITEM_EQUIPMENT_ASSETS,
            BalanceItem::ITEM_TRANSPORT_ASSETS,
            BalanceItem::ITEM_INTANGIBLE_ASSETS,
            BalanceItem::ITEM_OTHER_ASSETS,
        ], [
            'class' => 'hidden fixed-assets',
        ]);
        $receivablesItems = $this->getFormattedItems([
            BalanceItem::ITEM_TRADE_RECEIVABLES,
            BalanceItem::ITEM_PREPAYMENTS_TO_SUPPLIERS,
            BalanceItem::ITEM_BUDGET_OVERPAYMENT,
            BalanceItem::ITEM_LOANS_ISSUED,
            BalanceItem::ITEM_OTHER_INVESTMENTS,
            BalanceItem::ITEM_RENTAL_DEPOSIT,
        ], [
            'class' => 'hidden receivable',
            'style' => 'padding-left: 25px;',
        ]);
        $cashItems = $this->getFormattedItems([
            BalanceItem::ITEM_BANK_ACCOUNTS,
            BalanceItem::ITEM_CASHBOX,
            BalanceItem::ITEM_EMONEY,
        ], [
            'class' => 'hidden cash',
            'style' => 'padding-left: 25px;',
        ]);
        $capitalUndestributedProfitsItems = $this->getFormattedItems([
            BalanceItem::ITEM_UNDESTRIBUTED_PROFITS,
            BalanceItem::ITEM_UNDESTRIBUTED_PROFITS_PREV,
        ], [
            'class' => 'hidden capital-undestributed-profits',
            'style' => 'padding-left: 25px;',
        ]);
        $shortTermAccountsPayableItems = $this->getFormattedItems([
            BalanceItem::ITEM_VENDOR_INVOICES_PAYABLE,
            BalanceItem::ITEM_PREPAYMENT_OF_CUSTOMERS,
            BalanceItem::ITEM_SALARY_ARREARS,
            BalanceItem::ITEM_BUDGET_ARREARS,
        ], [
            'class' => 'hidden shorttermAccountsPayable',
            'style' => 'padding-left: 25px;',
        ]);

        return [
            [
                'label' => 'АКТИВЫ ' .
                    '<span class="tooltip-balance ico-question valign-middle" data-tooltip-content="#tooltip-balance-assets"></span>',
                'getter' => 'getAssets',
                'options' => [
                    'class' => 'assets-row text-bold',
                ],
                'items' => [
                    [
                        'label' => 'Основные средства',
                        'addCheckboxX' => true,
                        'getter' => 'getFixedAssets',
                        'options' => [
                            'class' => 'text-bold',
                            'data-children' => 'fixed-assets',
                        ],
                        'items' => $fixedAssetItems,
                    ],
                    [
                        'label' => 'Оборотные активы',
                        'addCheckboxX' => true,
                        'getter' => 'getCurrentAssets',
                        'options' => [
                            'class' => 'text-bold',
                            'data-children' => 'current-assets',
                        ],
                        'items' => [
                            [
                                'label' => BalanceItem::findOne(BalanceItem::ITEM_STOCKS)->name,
                                'options' => [
                                    'class' => 'hidden current-assets',
                                ],
                                'getter' => BalanceItem::$itemGetter[BalanceItem::ITEM_STOCKS],
                            ],
                            [
                                'label' => BalanceItem::findOne(BalanceItem::ITEM_INCOMPLETE_PROJECTS)->name,
                                'options' => [
                                    'class' => 'hidden current-assets',
                                ],
                                'getter' => BalanceItem::$itemGetter[BalanceItem::ITEM_INCOMPLETE_PROJECTS],
                            ],
                            [
                                'label' => 'Дебиторская задолженность',
                                'addCheckboxX' => true,
                                'getter' => 'getReceivables',
                                'options' => [
                                    'class' => 'hidden text-bold',
                                    'data-children' => 'receivable',
                                    'style' => 'padding-left: 25px;',
                                ],
                                'items' => $receivablesItems,
                            ],
                            [
                                'label' => 'Денежные средства',
                                'addCheckboxX' => true,
                                'getter' => 'getCash',
                                'options' => [
                                    'class' => 'hidden text-bold',
                                    'data-children' => 'cash',
                                    'style' => 'padding-left: 25px;',
                                ],
                                'items' => $cashItems,
                            ],
                        ],
                    ],
                ],
            ],
            [
                'label' => 'ПАССИВЫ ' .
                    '<span class="tooltip-balance ico-question valign-middle" data-tooltip-content="#tooltip-balance-liabilities"></span>',
                'getter' => 'getLiabilities',
                'options' => [
                    'class' => 'liabilities-row text-bold',
                ],
                'items' => [
                    [
                        'label' => 'Собственный капитал',
                        'addCheckboxX' => true,
                        'getter' => 'getCapital',
                        'options' => [
                            'class' => 'text-bold',
                            'data-children' => 'capital',
                        ],
                        'items' => [
                            [
                                'label' => BalanceItem::findOne(BalanceItem::ITEM_AUTHORIZED_CAPITAL)->name,
                                'options' => [
                                    'class' => 'hidden capital',
                                ],
                                'getter' => BalanceItem::$itemGetter[BalanceItem::ITEM_AUTHORIZED_CAPITAL],
                            ],
                            [
                                'label' => 'Нераспределенная прибыль',
                                'addCheckboxX' => true,
                                'getter' => 'getCapitalUndestributedProfits',
                                'options' => [
                                    'class' => 'hidden capital text-bold',
                                    'data-children' => 'capital-undestributed-profits',
                                    'style' => 'padding-left: 25px;',
                                ],
                                'items' => $capitalUndestributedProfitsItems,
                            ],
                        ],
                    ],
                    [
                        'label' => 'Долгосрочные обязательства',
                        'addCheckboxX' => true,
                        'getter' => 'getLongtermDuties',
                        'options' => [
                            'class' => 'text-bold',
                            'data-children' => 'longtermDuties',
                        ],
                        'items' => [
                            [
                                'label' => BalanceItem::findOne(BalanceItem::ITEM_LONG_TERM_LOANS)->name,
                                'options' => [
                                    'class' => 'hidden longtermDuties',
                                ],
                                'getter' => BalanceItem::$itemGetter[BalanceItem::ITEM_LONG_TERM_LOANS],
                            ],
                        ],
                    ],
                    [
                        'label' => 'Краткосрочные обязательства',
                        'addCheckboxX' => true,
                        'getter' => 'getShorttermLiabilities',
                        'options' => [
                            'class' => 'text-bold',
                            'data-children' => 'shorttermLiabilities',
                        ],
                        'items' => [
                            [
                                'label' => BalanceItem::findOne(BalanceItem::ITEM_SHORT_TERM_BORROWINGS)->name,
                                'options' => [
                                    'class' => 'hidden shorttermLiabilities',
                                ],
                                'getter' => BalanceItem::$itemGetter[BalanceItem::ITEM_SHORT_TERM_BORROWINGS],
                            ],
                            [
                                'label' => 'Кредиторская задолженность',
                                'addCheckboxX' => true,
                                'getter' => 'getShorttermAccountsPayable',
                                'options' => [
                                    'class' => 'hidden text-bold',
                                    'data-children' => 'shorttermAccountsPayable',
                                    'style' => 'padding-left: 25px;',
                                ],
                                'items' => $shortTermAccountsPayableItems,
                            ],
                        ],
                    ],
                ],
            ],
            [
                'label' => 'АКТИВ - ПАССИВ',
                'getter' => 'getBalanceCheck',
                'options' => [
                    'class' => 'text-bold',
                ],
            ],
        ];
    }

    /**
     * @param $items
     * @param array $options
     * @return array
     */
    public function getFormattedItems($items, $options = [])
    {
        // todo: check in 20-281
        $formattedItems = [];
        $company = Yii::$app->user->identity->company;
        /* @var $items \frontend\modules\analytics\models\BalanceItem[] */
        $items = BalanceItem::find()
            ->leftJoin(['companyItem' => BalanceCompanyItem::tableName()], "
                companyItem.item_id = balance_item.id
                AND companyItem.company_id = {$company->id}
            ")
            ->andWhere(['in', 'id', $items])
            ->orderBy(['companyItem.sort' => SORT_ASC, 'companyItem.item_id' => SORT_ASC])
            ->all();
        foreach ($items as $item) {
            $formattedItems[] = [
                'label' => $item->subname ?: $item->name,
                'canUpdate' => in_array($item->id, [
                    BalanceItem::ITEM_REAL_ESTATE_OBJECT,
                    BalanceItem::ITEM_EQUIPMENT_ASSETS,
                    BalanceItem::ITEM_TRANSPORT_ASSETS,
                    BalanceItem::ITEM_INTANGIBLE_ASSETS,
                    BalanceItem::ITEM_OTHER_ASSETS,
                    BalanceItem::ITEM_BUDGET_OVERPAYMENT,
                    BalanceItem::ITEM_OTHER_INVESTMENTS,
                    BalanceItem::ITEM_AUTHORIZED_CAPITAL,
                    BalanceItem::ITEM_BUDGET_ARREARS,
                ]),
                'options' => array_merge($options, [
                    'data-itemid' => $item->id,
                ]),
                'getter' => BalanceItem::$itemGetter[$item->id],
            ];
        }

        return $formattedItems;
    }

    /**
     * @param $data
     * @param null $formName
     */
    public function _load($data, $formName = null)
    {
        if ($this->load($data, $formName)) {
            if (!in_array($this->year, $this->getYearFilter())) {
                $this->_year = date('Y');
            }
        }
    }

    /**
     * @param bool $text
     * @return array
     */
    public function getChartPeriod($text = true)
    {
        $month = [];
        $currMonth = $this->year == date('Y') ? date('m') : 12;
        $currYear = $this->year;
        for ($i = $currMonth + 1; $i <= 12; $i++) {
            $monthNumber = $i > 9 ? $i : "0{$i}";
            $month[] = $text ?
                Month::$monthShort[$i] . '.' . ($currYear - 1) :
                date_create($currYear - 1 . '-' . $monthNumber . '-01');
        }
        for ($i = 1; $i <= $currMonth; $i++) {
            $monthNumber = $i > 9 ? $i : "0{$i}";
            $month[] = $text ?
                Month::$monthShort[$i] . '.' . ($currYear) :
                date_create($currYear . '-' . $monthNumber . '-01');
        }

        return $month;
    }

    public function getChartAmount($type = self::TYPE_ASSETS)
    {
        $result = [];
        $items = $type == self::TYPE_ASSETS ? [
            [
                'name' => 'Запасы',
                'items' => [
                    BalanceItem::ITEM_STOCKS,
                ],
                'color' => 'rgba(183,225,115,1)',
            ],
            [
                'name' => 'Дебиторы',
                'items' => [
                    BalanceItem::ITEM_TRADE_RECEIVABLES,
                    BalanceItem::ITEM_PREPAYMENTS_TO_SUPPLIERS,
                    BalanceItem::ITEM_BUDGET_OVERPAYMENT,
                    BalanceItem::ITEM_LOANS_ISSUED,
                    BalanceItem::ITEM_OTHER_INVESTMENTS,
                    BalanceItem::ITEM_RENTAL_DEPOSIT,
                ],
                'color' => 'rgba(160,64,0,1)',
            ],
            [
                'name' => 'Деньги',
                'items' => [
                    BalanceItem::ITEM_BANK_ACCOUNTS,
                    BalanceItem::ITEM_CASHBOX,
                    BalanceItem::ITEM_EMONEY,
                ],
                'color' => 'rgba(17,122,101,1)',
            ],
            [
                'name' => 'Внеоборотные активы',
                'items' => [
                    BalanceItem::ITEM_REAL_ESTATE_OBJECT,
                    BalanceItem::ITEM_EQUIPMENT_ASSETS,
                    BalanceItem::ITEM_TRANSPORT_ASSETS,
                    BalanceItem::ITEM_INTANGIBLE_ASSETS,
                    BalanceItem::ITEM_OTHER_ASSETS,
                ],
                'color' => 'rgba(41,138,188,1)',
            ],
        ] : [
            [
                'name' => 'Капитал',
                'items' => [
                    BalanceItem::ITEM_AUTHORIZED_CAPITAL,
                    BalanceItem::ITEM_UNDESTRIBUTED_PROFITS,
                ],
                'color' => 'rgba(183,225,115,1)',
            ],
            [
                'name' => 'Кредиторы',
                'items' => [
                    BalanceItem::ITEM_VENDOR_INVOICES_PAYABLE,
                    BalanceItem::ITEM_PREPAYMENT_OF_CUSTOMERS,
                    BalanceItem::ITEM_SALARY_ARREARS,
                    BalanceItem::ITEM_BUDGET_ARREARS,
                ],
                'color' => 'rgba(160,64,0,1)',
            ],
            [
                'name' => 'Кредиты / Займы',
                'items' => [
                    BalanceItem::ITEM_LONG_TERM_LOANS,
                    BalanceItem::ITEM_SHORT_TERM_BORROWINGS,
                ],
                'color' => 'rgba(41,138,188,1)',
            ],
        ];

        $i = 0;
        foreach ($items as $item) {
            $formattedItems = ArrayHelper::map($this->getFormattedItems($item['items']), 'label', 'getter');
            foreach ($formattedItems as $label => $getter) {
                if (!isset($result[$i])) {
                    $result[$i] = [
                        'name' => $item['name'],
                        'data' => [],
                    ];
                }
                foreach ($this->getChartPeriod(false) as $key => $date) {
                    $amount = $this->$getter($date) / 100;
                    if (!isset($result[$i]['data'][$key])) {
                        $result[$i]['data'][$key] = $amount;
                    } else {
                        $result[$i]['data'][$key] += $amount;
                    }
                }
            }
            $i++;
        }

        return $result;
    }

    ////////////////////////////////
    ///////////// CHART ////////////
    ////////////////////////////////

    public function getFromCurrentMonthsDates($left, $right, $offsetYear = 0, $offsetMonth = "0") {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month")->modify("{$offsetMonth} month");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = clone ($curr->modify('last day of this month')->setTime(23, 59, 59));
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }

    /**
     * @param $type
     * @throws \Exception
     */
    public function generateXls($type)
    {
        $balance = $this->search($type, Yii::$app->request->get());

        // header
        $columns = [];
        $columns[] = [
            'attribute' => 'itemName',
            'header' => 'Статьи',
        ];
        foreach (AbstractFinance::$month as $monthNumber => $monthText) {
            $monthNumber = (int)$monthNumber;
            $columns[] = [
                'attribute' => "item{$monthNumber}",
                'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                'header' => $monthText,
            ];
        }
        $columns[] = [
            'attribute' => 'dataYear',
            'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
            'header' => $this->year,
        ];

        // data
        $formattedData = [];
        foreach ($balance as $level1) {
            $formattedData[] = $this->_buildXlsRow($level1, 1);
            if ($level1['items']) foreach ($level1['items'] as $level2) {
                $formattedData[] = $this->_buildXlsRow($level2, 2);
                if ($level2['items']) foreach ($level2['items'] as $level3) {
                    $formattedData[] = $this->_buildXlsRow($level3, 3);
                    if ($level3['items']) foreach ($level3['items'] as $level4) {
                        $formattedData[] = $this->_buildXlsRow($level4, 4);
                    }
                }
            }
        }

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $formattedData,
            'title' => "Баланс за {$this->year} год",
            'rangeHeader' => range('A', 'N'),
            'columns' => $columns,
            'format' => 'Xlsx',
            'fileName' => "Баланс за {$this->year} год",
        ]);
    }

    protected function _buildXlsRow($data, $level = 1)
    {
        $row = [
            'itemName' => str_repeat(' ', ($level-1) * 5) . trim(strip_tags($data['label'] ?? '')),
            'dataYear' => ($data['data']['total'] ?? '0.00') / 100
        ];
        for ($i=1;$i<=12;$i++)
            $row['item'.$i] = ($data['data'][$i] ?? '0.00') / 100;

        return $row;
    }


    /**
     * @inheritdoc
     */
    public function setCompany(Company $company)
    {
        $this->_company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        if ($this->_company === null) {
            throw new InvalidConfigException('The "company" property is not set.');
        }

        return $this->_company;
    }

    /**
     * @inheritdoc
     */
    public function setYear($value)
    {
        $years = $this->getYearFilter();
        if (!in_array($value, $years)) {
            $value = date('Y');
        }

        $this->_year = $value;
    }

    /**
     * @inheritdoc
     */
    public function getYear()
    {
        return $this->_year ?: date('Y');
    }
}
