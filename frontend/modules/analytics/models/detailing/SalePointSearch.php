<?php

namespace frontend\modules\analytics\models\detailing;

use common\components\date\DateHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use Yii;
use yii\data\ArrayDataProvider;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashFlowsBase;
use common\models\employee\Employee;
use frontend\components\StatisticPeriod;
use frontend\modules\analytics\models\OLAP;
use common\models\companyStructure\SalePoint;

/**
 * Class SalePointSearch
 */
class SalePointSearch extends SalePoint
{
    const MIN_REPORT_DATE = '2000';

    public $query;

    public $searchBy;

    private $_yearFilter;

    private $_year;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['searchBy'], 'safe'],
            [['searchBy'], 'trim'],
            [['year'], 'integer']
        ];
    }

    public function init(){
        $this->_year = date('Y');
    }

    public function getYear() {
        return $this->_year;
    }

    public function setYear($val) {
        $this->_year = (int)$val ?: date('Y');
    }

    public function search($params = [])
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $dateRange = StatisticPeriod::getSessionPeriod();

        $query = SalePoint::find()
            ->joinWith('type')
            ->where([SalePoint::tableName() . '.company_id' => $user->company->id]);

        $this->load($params);

        if ($this->searchBy) {
            $query->andFilterWhere(['or',
                ['like', SalePoint::tableName() . '.name', $this->searchBy],
            ]);
        }

        $this->query = clone $query;

        $query->groupBy([
            SalePoint::tableName() . '.id',
        ]);

        $amountData = $this->getAmountBySalePoint($dateRange);
        $models = [];

        /* @var SalePoint $point */
        foreach ($query->all() as $point) {
            $models[$point->id] = [
                'id' => $point->id,
                'name' => $point->name,
                'type' => $point->type->name,
                'status_id' => $point->status_id,
                'cashboxes' => ArrayHelper::map($point->cashboxes, 'id', 'name'),
                'store' => ($point->store) ? $point->store->name : null,
                'employers' => ArrayHelper::map($point->employers, 'id', function($e) { return $e->getShortFio(); }),
                'income_amount' => ArrayHelper::getValue($amountData, CashFlowsBase::FLOW_TYPE_INCOME . '.' . $point->id, 0),
                'expense_amount' => ArrayHelper::getValue($amountData, CashFlowsBase::FLOW_TYPE_EXPENSE . '.' . $point->id, 0),
                'icon' => $point->getIcon()
            ];
        }

        $models[0] = [
            'id' => 0,
            'name' => 'Без точки продаж',
            'income_amount' => ArrayHelper::getValue($amountData, CashFlowsBase::FLOW_TYPE_INCOME . '.' . 0, 0),
            'expense_amount' => ArrayHelper::getValue($amountData, CashFlowsBase::FLOW_TYPE_EXPENSE . '.' . 0, 0)
        ];

        return new ArrayDataProvider([
            'allModels' => $models,
            'pagination' => [
                'pageSize' => \Yii::$app->request->get('per-page', 10),
            ],
            'sort' => [
                'attributes' => [
                    'name',
                    'type',
                    'income_amount',
                    'expense_amount'
                ],
                'defaultOrder' => [
                    'name' => SORT_ASC,
                ],
            ],
        ]);
    }

    public function getAmountBySalePoint($dateRange)
    {
        $table = OLAP::TABLE_OLAP_FLOWS;
        $companyIds = implode(',', [Yii::$app->user->identity->company->id]);

        $query = "
            SELECT
              IFNULL(t.sale_point_id, 0) sale_point_id,                   
              t.type,
              SUM(IF(t.has_tin_parent, t.tin_child_amount, IF(t.has_tin_children, 0, t.amount))) amount
            FROM `{$table}` t
            WHERE
              t.company_id IN ({$companyIds})
              AND t.date BETWEEN '{$dateRange['from']}' AND '{$dateRange['to']}'
            GROUP BY t.sale_point_id, t.type
          ";

        $rawData = Yii::$app->db2->createCommand($query)->queryAll();
        $retData = [
            CashFlowsBase::FLOW_TYPE_INCOME => [],
            CashFlowsBase::FLOW_TYPE_EXPENSE => [],
        ];
        foreach ($rawData as $row) {
            $retData[$row['type']][$row['sale_point_id']] = $row['amount'];
        }

        return $retData;
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        $user = Yii::$app->user->identity;

        if (empty($this->_yearFilter)) {
            $range = [];
            $cashOrderMinDate = CashOrderFlows::find()
                ->byCompany($user->company->id)
                ->min('date');
            $cashBankMinDate = CashBankFlows::find()
                ->byCompany($user->company->id)
                ->min('date');
            $cashEmoneyMinDate = CashEmoneyFlows::find()
                ->byCompany($user->company->id)
                ->min('date');
            $minDates = [];

            if ($cashOrderMinDate) $minDates[] = $cashOrderMinDate;
            if ($cashBankMinDate) $minDates[] = $cashBankMinDate;
            if ($cashEmoneyMinDate) $minDates[] = $cashEmoneyMinDate;

            $minCashDate = !empty($minDates) ? max(self::MIN_REPORT_DATE, min($minDates)) : date(DateHelper::FORMAT_DATE);
            $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
            $currentYear = date('Y');
            foreach (range($registrationYear, $currentYear) as $value) {
                $range[$value] = $value;
            }
            arsort($range);
            $this->_yearFilter = $range;
        }

        return $this->_yearFilter;
    }
}