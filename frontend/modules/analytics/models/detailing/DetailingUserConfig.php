<?php

namespace frontend\modules\analytics\models\detailing;

use Yii;
use common\models\employee\Config;

class DetailingUserConfig {

    const CHART_TYPE_INCOME = 1;
    const CHART_TYPE_EXPENSE = 2;
    const CHART_TYPE_REVENUE = 3;
    const CHART_TYPE_PROFIT = 4;
    const CHART_TYPE_PROFITABILITY = 5;

    public static array $chartTypeName = [
        self::CHART_TYPE_INCOME => 'Приход',
        self::CHART_TYPE_EXPENSE => 'Расход',
        self::CHART_TYPE_REVENUE => 'Выручка',
        self::CHART_TYPE_PROFIT => 'Прибыль',
        self::CHART_TYPE_PROFITABILITY => 'Рентабельность'
    ];

    public static function getIndexChartType(): int
    {
        $config = Yii::$app->user->identity->config;
        $chartType = $config->detailing_index_chart_type ?? null;

        switch ($chartType) {
            case self::CHART_TYPE_PROFITABILITY:
                return self::CHART_TYPE_PROFITABILITY;
            case self::CHART_TYPE_PROFIT:
                return self::CHART_TYPE_PROFIT;
            case self::CHART_TYPE_REVENUE:
                return self::CHART_TYPE_REVENUE;
            case self::CHART_TYPE_EXPENSE:
                return self::CHART_TYPE_EXPENSE;
            case self::CHART_TYPE_INCOME:
            default:
                return self::CHART_TYPE_INCOME;
        }
    }
}