<?php

namespace frontend\modules\analytics\models\detailing;

use common\components\date\DateHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\models\AnalyticsArticleForm;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;
use frontend\modules\cash\models\CashContractorType;
use Yii;
use yii\data\ArrayDataProvider;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashFlowsBase;
use common\models\company\CompanyIndustry;
use common\models\employee\Employee;
use frontend\components\StatisticPeriod;
use frontend\modules\analytics\models\OLAP;

/**
 * Class CompanyIndustrySearch
 */
class CompanyIndustrySearch extends CompanyIndustry
{
    const MIN_REPORT_DATE = '2000';

    public $query;

    public $searchBy;
    
    private $_yearFilter;

    private $_year;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['searchBy', 'status_id', 'industry_type_id', 'is_main'], 'safe'],
            [['searchBy'], 'trim'],
            [['year'], 'integer']
        ];
    }

    public function init(){
        $this->_year = date('Y');
    }

    public function getYear() {
        return $this->_year;
    }

    public function setYear($val) {
        $this->_year = (int)$val ?: date('Y');
    }

    public function search($params = [])
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $dateRange = StatisticPeriod::getSessionPeriod();

        $query = CompanyIndustry::find()
            ->where([CompanyIndustry::tableName() . '.company_id' => $user->company->id]);

        $this->load($params);

        if ($this->searchBy) {
            $query->andFilterWhere(['or',
                ['like', CompanyIndustry::tableName() . '.name', $this->searchBy],
            ]);
        }

        $query->andFilterWhere([CompanyIndustry::tableName() . '.status_id' => $this->status_id]);
        $query->andFilterWhere([CompanyIndustry::tableName() . '.industry_type_id' => $this->industry_type_id]);
        if (strlen($this->is_main))
            $query->andWhere([CompanyIndustry::tableName() . '.is_main' => $this->is_main]);

        $this->query = clone $query;

        $query->groupBy([
            CompanyIndustry::tableName() . '.id',
        ]);

        $amounts = $this->getIndustriesAmounts($dateRange);
        $palData = $this->getProfitAndLossTotalData($dateRange);

        $models = [];

        /* @var CompanyIndustry $industry */
        foreach ($query->all() as $industry) {
            $models[$industry->id] = $industry->getAttributes([
                'id', 'name', 'created_at', 'status_id', 'industry_type_id', 'is_main'
            ]);
            $models[$industry->id]['income_amount'] = ArrayHelper::getValue($amounts, CashFlowsBase::FLOW_TYPE_INCOME . '.' . $industry->id, 0);
            $models[$industry->id]['expense_amount'] = ArrayHelper::getValue($amounts, CashFlowsBase::FLOW_TYPE_EXPENSE . '.' . $industry->id, 0);
            $models[$industry->id]['pal_net_income_loss'] = ArrayHelper::getValue($palData, 'netIncomeLoss' . '.' . $industry->id, 0);
            $models[$industry->id]['pal_revenue'] = ArrayHelper::getValue($palData, 'totalRevenue' . '.' . $industry->id, 0);
            $models[$industry->id]['pal_margin'] = ArrayHelper::getValue($palData, 'marginalIncome' . '.' . $industry->id, 0);
            $models[$industry->id]['pal_undistributed_profit'] = ArrayHelper::getValue($palData, 'undestributedProfits' . '.' . $industry->id, 0);
        }

        if (!strlen($this->is_main) && !strlen($this->industry_type_id) && !strlen($this->status_id)) {
            $models[0] = [
                'id' => 0,
                'name' => 'Без направления',
                'is_main' => null,
                'created_at' => null,
                'status_id' => null,
                'industry_type_id' => null,
                'income_amount' => ArrayHelper::getValue($amounts, CashFlowsBase::FLOW_TYPE_INCOME . '.' . 0, 0),
                'expense_amount' => ArrayHelper::getValue($amounts, CashFlowsBase::FLOW_TYPE_EXPENSE . '.' . 0, 0),
                'pal_net_income_loss' => ArrayHelper::getValue($palData, 'netIncomeLoss' . '.' . 0, 0),
                'pal_revenue' => ArrayHelper::getValue($palData, 'totalRevenue' . '.' . 0, 0),
                'pal_margin' => ArrayHelper::getValue($palData, 'marginalIncome' . '.' . 0, 0),
                'pal_undistributed_profit' => ArrayHelper::getValue($palData, 'undestributedProfits' . '.' . 0, 0),
            ];
        }

        return new ArrayDataProvider([
            'allModels' => $models,
            'pagination' => [
                'pageSize' => \Yii::$app->request->get('per-page', 10),
            ],
            'sort' => [
                'attributes' => [
                    'name',
                    'created_at',
                    'income_amount',
                    'expense_amount',
                    'pal_net_income_loss',
                    'pal_revenue',
                    'pal_margin',
                    'pal_undistributed_profit',
                ],
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        $user = Yii::$app->user->identity;
        
        if (empty($this->_yearFilter)) {
            $range = [];
            $cashOrderMinDate = CashOrderFlows::find()
                ->byCompany($user->company->id)
                ->min('date');
            $cashBankMinDate = CashBankFlows::find()
                ->byCompany($user->company->id)
                ->min('date');
            $cashEmoneyMinDate = CashEmoneyFlows::find()
                ->byCompany($user->company->id)
                ->min('date');
            $minDates = [];

            if ($cashOrderMinDate) $minDates[] = $cashOrderMinDate;
            if ($cashBankMinDate) $minDates[] = $cashBankMinDate;
            if ($cashEmoneyMinDate) $minDates[] = $cashEmoneyMinDate;

            $minCashDate = !empty($minDates) ? max(self::MIN_REPORT_DATE, min($minDates)) : date(DateHelper::FORMAT_DATE);
            $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
            $currentYear = date('Y');
            foreach (range($registrationYear, $currentYear) as $value) {
                $range[$value] = $value;
            }
            arsort($range);
            $this->_yearFilter = $range;
        }

        return $this->_yearFilter;
    }    
    
    public function getStatusFilter()
    {
        return ArrayHelper::merge([null => 'Все'], [
            self::STATUS_ACTIVE => self::STRING_ACTIVE,
            self::STATUS_CLOSED => self::STRING_CLOSED,
        ]);
    }

    public function getIndustryTypeFilter()
    {
        return [null => 'Все'] +
            (clone $this->query)
            ->joinWith('industryType')
            ->select('company_info_industry.name')
            ->indexBy('company_info_industry.id')
            ->orderBy('company_info_industry.name')
            ->column();
    }

    private function getIndustriesAmounts($dateRange)
    {
        $table = OLAP::TABLE_OLAP_FLOWS;
        $companyIds = implode(',', [Yii::$app->user->identity->company->id]);
        $exceptOwnFlowsContractors = implode(',', OLAP::$ownFlowContractorType);

        $query = "
            SELECT
              IFNULL(t.industry_id, 0) industry_id,                   
              t.type,
              SUM(IF(t.has_tin_parent, t.tin_child_amount, IF(t.has_tin_children, 0, t.amount))) amount
            FROM `{$table}` t
            WHERE
              t.company_id IN ({$companyIds})
              AND t.date BETWEEN '{$dateRange['from']}' AND '{$dateRange['to']}'
              AND t.contractor_id NOT IN ({$exceptOwnFlowsContractors})
            GROUP BY t.industry_id, t.type
          ";

        $rawData = Yii::$app->db2->createCommand($query)->queryAll();
        $retData = [
            CashFlowsBase::FLOW_TYPE_INCOME => [],
            CashFlowsBase::FLOW_TYPE_EXPENSE => [],
        ];
        foreach ($rawData as $row) {
            $retData[$row['type']][$row['industry_id']] = $row['amount'];
        }

        return $retData;
    }


    private function getProfitAndLossTotalData($dateRange)
    {
        // little precaching profit-and-loss data for table sorting buttons
        if (Yii::$app->request->isPjax) {
            if ($palData = Yii::$app->session->get('detailing.pal.data'))
                // from cache
                return Yii::$app->session->get('detailing.pal.data');
        }

        // from db
        $palData = self::_getProfitAndLossTotalData($dateRange);
        Yii::$app->session->set('detailing.pal.data', $palData);

        return $palData;
    }
    
    /**
     * @param $dateRange
     * @return array
     */
    private static function _getProfitAndLossTotalData($dateRange)
    {
        try {
            $pal = new ProfitAndLossSearchModel();
            $pal->setUserOption('monthGroupBy', AnalyticsArticleForm::MONTH_GROUP_INDUSTRY);
            $pal->setUserOption('revenueRuleGroup', AnalyticsArticleForm::REVENUE_RULE_GROUP_UNSET);
            $pal->setUserOption('revenueRule', AnalyticsArticleForm::REVENUE_RULE_UNSET);
            $pal->setUserOption('calcWithoutNds', false);
            $pal->disableMultiCompanyMode();
            $pal->handleItems();

            return [
                'totalRevenue' => $pal->getPeriodSumGroupedByMonth('totalRevenue', $dateRange['from'], $dateRange['to']),
                'marginalIncome' => $pal->getPeriodSumGroupedByMonth('marginalIncome', $dateRange['from'], $dateRange['to']),
                'netIncomeLoss' => $pal->getPeriodSumGroupedByMonth('netIncomeLoss', $dateRange['from'], $dateRange['to']),
                'undestributedProfits' => $pal->getPeriodSumGroupedByMonth('undestributedProfits', $dateRange['from'], $dateRange['to']),
            ];

        } catch (\Throwable $e) {
            if (YII_ENV_DEV) {
                var_dump($e);
                exit;
            }
        }

        return [
            'totalRevenue' => [],
            'marginalIncome' => [],
            'netIncomeLoss' => [],
            'undestributedProfits' => []
        ];
    }    

    public function getTypeFilter()
    {
        return ['' => 'Все', '1' => 'Основное', '0' => 'Дополнительное'];
    }
}