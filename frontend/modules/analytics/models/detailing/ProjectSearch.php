<?php

namespace frontend\modules\analytics\models\detailing;

use common\models\project\Project;

/**
 * Class ProjectSearch
 * @package frontend\models
 *
 * @property Project[] $project
 */
class ProjectSearch extends \common\models\project\ProjectSearch {}