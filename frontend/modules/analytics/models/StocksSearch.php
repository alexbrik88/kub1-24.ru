<?php

namespace frontend\modules\analytics\models;

use common\components\date\DateHelper;
use common\components\excel\Excel;
use common\components\helpers\ArrayHelper;
use common\models\product\Product;
use common\models\product\ProductGroup;
use frontend\components\PageSize;
use Yii;
use yii\data\SqlDataProvider;

/**
 * Class StocksSearch
 * @package frontend\modules\analytics\models
 */
class StocksSearch extends OLAP
{
    /**
     *
     */
    const REPORT_ID = 16;

    /**
     *
     */
    const MIN_REPORT_DATE = '2010';

    /**
     *
     */
    public static $HAS_GROUPS;

    /**
     * @var
     */
    public $year;

    /**
     * @var
     */
    private $_company;

    /**
     * @var
     */
    public $title;

    /**
     * @var
     */
    public $byGroups = true;

    /**
     * @var bool
     */
    public $byQuantity = false;

    /**
     * @var string
     */
    public $sort = 'title';

    /**
     * @var array
     */
    public $totals = [];

    /**
     * @var
     */
    public $showZeroesQuantity;

    /**
     * @var
     */
    private $_cacheStructureChartData;

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['title'], 'string'],
        ]);
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->year = \Yii::$app->session->get('modules.reports.finance.year', date('Y'));
        $this->_company = Yii::$app->user->identity->company;
        $this->byGroups = Yii::$app->user->identity->config->report_abc_show_groups;
        $this->byQuantity = Yii::$app->request->get('quantity');

        // has groups
        if (\Yii::$app->session->get('modules.reports.finance.stocks.has_groups')) {
            self::$HAS_GROUPS = \Yii::$app->session->get('modules.reports.finance.stocks.has_groups');
        } else {
            self::$HAS_GROUPS = (ProductGroup::find()->where(['company_id' => $this->_company->id])->count('*', \Yii::$app->db2) > 0);
        }

        // sort
        if ($sort = Yii::$app->request->get('sort')) {
            Yii::$app->session->set('modules.reports.finance.stocks.sort', $sort);
        }
        $this->sort = Yii::$app->session->get('modules.reports.finance.stocks.sort', 'title');

        // show zeroes quantity
        $this->showZeroesQuantity = Yii::$app->user->identity->config->product_zeroes_turnover ?? false;
    }

    /**
     * @return string
     */
    public function getSortProducts()
    {
        $rawSort = $this->sort;
        $sort = str_replace('-', '', $this->sort);

        return "ORDER BY `{$sort}` " . ($rawSort == $sort ? 'ASC' : 'DESC');
    }

    /**
     * @return array
     */
    public function getSortGroups()
    {
        $rawSort = $this->sort;
        $sort = str_replace('-', '', $this->sort);

        return ["{$sort}" => ($rawSort == $sort) ? SORT_ASC : SORT_DESC];
    }

    /**
     * @param array $params
     * @return SqlDataProvider
     * @throws \yii\db\Exception
     */
    public function search($params = [])
    {
        return ($this->byGroups) ?
            $this->searchByGroups($params) :
            $this->searchByProducts($params);
    }

    public function searchByProducts($params = [])
    {
        $this->load($params);
        $productTable = Product::tableName();
        $turnoverTable = self::TABLE_PRODUCT_TURNOVER;
        $companyId = $this->_company->id;
        $currYear = $this->year;
        $nextYear = $this->year + 1;
        $SQL_TITLE_FILTER = $this->__getSqlTitleFilter();
        $SQL_ZEROES_QUANTITY = $this->__getSqlZeroesQuantity();

        $calc = ($this->byQuantity) ?
            'IF(t.type = 2, -1, 1) * t.quantity * 100' :
            'IF(t.type = 2, -1, 1) * t.quantity * IFNULL(p.price_for_buy_with_nds, 0)';

        $query = "
                SELECT
                p.id,
                p.title,
                SUM(IF(t.date < '{$currYear}-02-01', {$calc}, 0)) `01`,
                SUM(IF(t.date < '{$currYear}-03-01', {$calc}, 0)) `02`,
                SUM(IF(t.date < '{$currYear}-04-01', {$calc}, 0)) `03`,
                SUM(IF(t.date < '{$currYear}-05-01', {$calc}, 0)) `04`,
                SUM(IF(t.date < '{$currYear}-06-01', {$calc}, 0)) `05`,
                SUM(IF(t.date < '{$currYear}-07-01', {$calc}, 0)) `06`,
                SUM(IF(t.date < '{$currYear}-08-01', {$calc}, 0)) `07`,
                SUM(IF(t.date < '{$currYear}-09-01', {$calc}, 0)) `08`,
                SUM(IF(t.date < '{$currYear}-10-01', {$calc}, 0)) `09`,
                SUM(IF(t.date < '{$currYear}-11-01', {$calc}, 0)) `10`,
                SUM(IF(t.date < '{$currYear}-12-01', {$calc}, 0)) `11`,
                SUM(IF(t.date < '{$nextYear}-01-01', {$calc}, 0)) `12`,
                SUM(IF(t.date BETWEEN '{$currYear}-01-01' AND '{$currYear}-12-31', {$calc}, 0)) `total_by_year`
                FROM {$productTable} p                
                JOIN {$turnoverTable} t ON t.product_id = p.id
                WHERE (p.company_id = {$companyId})
                    AND (p.production_type = 1) 
                    AND (t.date < '$nextYear-01-01')
                    AND (t.is_document_actual = TRUE)
                    AND (t.is_invoice_actual = TRUE)
                    {$SQL_TITLE_FILTER}
                GROUP BY p.id
                {$SQL_ZEROES_QUANTITY}
        ";

        $provider = new SqlDataProvider([
            'db' => Yii::$app->db2,
            'sql' => $query,
            'totalCount' => $this->__getProductsTotal($companyId, $nextYear),
            'pagination' => [
                'pageSize' => PageSize::get(),
            ],
            'sort' => [
                'attributes' => ['title', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
                'defaultOrder' => $this->sortGroups
            ],
        ]);

        return $provider;
    }

    public function searchByGroups($params = [])
    {
        $this->load($params);
        $groupTable = ProductGroup::tableName();
        $productTable = Product::tableName();
        $turnoverTable = self::TABLE_PRODUCT_TURNOVER;
        $companyId = $this->_company->id;
        $currYear = $this->year;
        $nextYear = $this->year + 1;
        $SQL_TITLE_FILTER = $this->__getSqlTitleFilter();
        $SQL_ZEROES_QUANTITY = $this->__getSqlZeroesQuantity();

        $calc = ($this->byQuantity) ?
            'IF(t.type = 2, -1, 1) * t.quantity * 100' :
            'IF(t.type = 2, -1, 1) * t.quantity * IFNULL(p.price_for_buy_with_nds, 0)';

        $query = "
                SELECT
                g.id,
                g.title,
                SUM(IF(t.date < '{$currYear}-02-01', {$calc}, 0)) `01`,
                SUM(IF(t.date < '{$currYear}-03-01', {$calc}, 0)) `02`,
                SUM(IF(t.date < '{$currYear}-04-01', {$calc}, 0)) `03`,
                SUM(IF(t.date < '{$currYear}-05-01', {$calc}, 0)) `04`,
                SUM(IF(t.date < '{$currYear}-06-01', {$calc}, 0)) `05`,
                SUM(IF(t.date < '{$currYear}-07-01', {$calc}, 0)) `06`,
                SUM(IF(t.date < '{$currYear}-08-01', {$calc}, 0)) `07`,
                SUM(IF(t.date < '{$currYear}-09-01', {$calc}, 0)) `08`,
                SUM(IF(t.date < '{$currYear}-10-01', {$calc}, 0)) `09`,
                SUM(IF(t.date < '{$currYear}-11-01', {$calc}, 0)) `10`,
                SUM(IF(t.date < '{$currYear}-12-01', {$calc}, 0)) `11`,
                SUM(IF(t.date < '{$nextYear}-01-01', {$calc}, 0)) `12`,
                SUM(IF(t.date BETWEEN '{$currYear}-01-01' AND '{$currYear}-12-31', {$calc}, 0)) `total_by_year`
                FROM {$productTable} p 
                LEFT JOIN {$groupTable} g ON g.id = p.group_id                
                JOIN {$turnoverTable} t ON t.product_id = p.id
                WHERE (p.company_id = {$companyId})
                    AND (p.production_type = 1) 
                    AND (t.date < '$nextYear-01-01')
                    AND (t.is_document_actual = TRUE)
                    AND (t.is_invoice_actual = TRUE)
                    {$SQL_TITLE_FILTER}
                GROUP BY g.id
                {$SQL_ZEROES_QUANTITY}
        ";

        $provider = new SqlDataProvider([
            'db' => Yii::$app->db2,
            'sql' => $query,
            'totalCount' => $this->__getGroupsTotal($companyId, $nextYear),
            'pagination' => [
                'pageSize' => PageSize::get(),
            ],
            'sort' => [
                'attributes' => ['title', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
                'defaultOrder' => $this->sortGroups
            ],
        ]);

        return $provider;
    }

    public function searchProductsByGroup($groupId)
    {
        $productTable = Product::tableName();
        $turnoverTable = self::TABLE_PRODUCT_TURNOVER;
        $companyId = $this->_company->id;
        $currYear = $this->year;
        $nextYear = $this->year + 1;
        $SQL_TITLE_FILTER = $this->__getSqlTitleFilter();
        $SQL_ORDER_PRODUCTS_BY = $this->sortProducts;
        $SQL_ZEROES_QUANTITY = $this->__getSqlZeroesQuantity();

        $calc = ($this->byQuantity) ?
            'IF(t.type = 2, -1, 1) * t.quantity * 100' :
            'IF(t.type = 2, -1, 1) * t.quantity * IFNULL(p.price_for_buy_with_nds, 0)';

        $query = "
                SELECT
                p.id,
                p.title,
                SUM(IF(t.date < '{$currYear}-02-01', {$calc}, 0)) `01`,
                SUM(IF(t.date < '{$currYear}-03-01', {$calc}, 0)) `02`,
                SUM(IF(t.date < '{$currYear}-04-01', {$calc}, 0)) `03`,
                SUM(IF(t.date < '{$currYear}-05-01', {$calc}, 0)) `04`,
                SUM(IF(t.date < '{$currYear}-06-01', {$calc}, 0)) `05`,
                SUM(IF(t.date < '{$currYear}-07-01', {$calc}, 0)) `06`,
                SUM(IF(t.date < '{$currYear}-08-01', {$calc}, 0)) `07`,
                SUM(IF(t.date < '{$currYear}-09-01', {$calc}, 0)) `08`,
                SUM(IF(t.date < '{$currYear}-10-01', {$calc}, 0)) `09`,
                SUM(IF(t.date < '{$currYear}-11-01', {$calc}, 0)) `10`,
                SUM(IF(t.date < '{$currYear}-12-01', {$calc}, 0)) `11`,
                SUM(IF(t.date < '{$nextYear}-01-01', {$calc}, 0)) `12`,
                SUM(IF(t.date BETWEEN '{$currYear}-01-01' AND '{$currYear}-12-31', {$calc}, 0)) `total_by_year`
                FROM {$productTable} p                 
                JOIN {$turnoverTable} t ON t.product_id = p.id
                WHERE p.group_id = {$groupId}
                    AND (p.company_id = {$companyId})
                    AND (p.production_type = 1) 
                    AND (t.date < '$nextYear-01-01')
                    AND (t.is_document_actual = TRUE)
                    AND (t.is_invoice_actual = TRUE)
                    {$SQL_TITLE_FILTER}
                GROUP BY p.id
                {$SQL_ZEROES_QUANTITY}
                {$SQL_ORDER_PRODUCTS_BY}
        ";

        return Yii::$app->db2->createCommand($query)->queryAll();
    }

    public function getTotals()
    {
        $productTable = Product::tableName();
        $turnoverTable = self::TABLE_PRODUCT_TURNOVER;
        $companyId = $this->_company->id;
        $currYear = $this->year;
        $nextYear = $this->year + 1;
        $SQL_TITLE_FILTER = $this->__getSqlTitleFilter();

        $calc = ($this->byQuantity) ?
            'IF(t.type = 2, -1, 1) * t.quantity * 100' :
            'IF(t.type = 2, -1, 1) * t.quantity * IFNULL(p.price_for_buy_with_nds, 0)';

        $query = "
                SELECT
                SUM(IF(t.date < '{$currYear}-02-01', {$calc}, 0)) `01`,
                SUM(IF(t.date < '{$currYear}-03-01', {$calc}, 0)) `02`,
                SUM(IF(t.date < '{$currYear}-04-01', {$calc}, 0)) `03`,
                SUM(IF(t.date < '{$currYear}-05-01', {$calc}, 0)) `04`,
                SUM(IF(t.date < '{$currYear}-06-01', {$calc}, 0)) `05`,
                SUM(IF(t.date < '{$currYear}-07-01', {$calc}, 0)) `06`,
                SUM(IF(t.date < '{$currYear}-08-01', {$calc}, 0)) `07`,
                SUM(IF(t.date < '{$currYear}-09-01', {$calc}, 0)) `08`,
                SUM(IF(t.date < '{$currYear}-10-01', {$calc}, 0)) `09`,
                SUM(IF(t.date < '{$currYear}-11-01', {$calc}, 0)) `10`,
                SUM(IF(t.date < '{$currYear}-12-01', {$calc}, 0)) `11`,
                SUM(IF(t.date < '{$nextYear}-01-01', {$calc}, 0)) `12`
                FROM {$productTable} p                 
                JOIN {$turnoverTable} t ON t.product_id = p.id
                WHERE (p.company_id = {$companyId})
                    AND (p.production_type = 1) 
                    AND (t.date < '$nextYear-01-01')
                    AND (t.is_document_actual = TRUE)
                    AND (t.is_invoice_actual = TRUE)
                    {$SQL_TITLE_FILTER}
        ";

        return Yii::$app->db->createCommand($query)->queryOne();
    }

    private function __getSqlTitleFilter()
    {
        return ($this->title) ? ' AND p.title LIKE "'.str_replace('"', '', $this->title).'%" ' : '';
    }

    private function __getSqlZeroesQuantity()
    {
        return $this->showZeroesQuantity ? null : 'HAVING (`01`<>0 OR `02`<>0 OR `03`<>0 OR `04`<>0 OR `05`<>0 OR `06`<>0 OR `07`<>0 OR `08`<>0 OR `09`<>0 OR `10`<>0 OR `11`<>0 OR `12`<>0)';
    }

    /**
     * @param $companyId
     * @param $period
     * @return false|string|null
     * @throws \yii\db\Exception
     */
    private function __getProductsTotal($companyId, $nextYear)
    {
        $turnoverTable = self::TABLE_PRODUCT_TURNOVER;
        $productTable = Product::tableName();
        $SQL_TITLE_FILTER = $this->__getSqlTitleFilter();
        $query = "
                SELECT COUNT(DISTINCT t.product_id)               
                FROM {$turnoverTable} t
                LEFT JOIN {$productTable} p ON p.id = t.product_id
                WHERE (t.company_id = {$companyId})
                    AND (t.production_type = 1) 
                    AND (t.date < '{$nextYear}-01-01')
                    AND (t.is_document_actual = TRUE)
                    AND (t.is_invoice_actual = TRUE)
                    {$SQL_TITLE_FILTER}
        ";

        return Yii::$app->db2->createCommand($query)->queryScalar();
    }

    /**
     * @param $companyId
     * @param $period
     * @return false|string|null
     * @throws \yii\db\Exception
     */
    private function __getGroupsTotal($companyId, $nextYear)
    {
        $turnoverTable = self::TABLE_PRODUCT_TURNOVER;
        $productTable = Product::tableName();
        $groupTable = ProductGroup::tableName();
        $SQL_TITLE_FILTER = $this->__getSqlTitleFilter();
        $query = "
                SELECT COUNT(DISTINCT g.id)     
                FROM {$turnoverTable} t
                LEFT JOIN {$productTable} p ON p.id = t.product_id
                LEFT JOIN {$groupTable} g ON g.id = p.group_id
                WHERE (t.company_id = {$companyId})
                    AND (t.production_type = 1) 
                    AND (t.date < '{$nextYear}-01-01')
                    AND (t.is_document_actual = TRUE)
                    AND (t.is_invoice_actual = TRUE)
                    {$SQL_TITLE_FILTER}
        ";

        return Yii::$app->db2->createCommand($query)->queryScalar();
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        $minDate = Yii::$app->db2->createCommand("SELECT MIN(`date`) FROM product_turnover WHERE company_id = " . $this->_company->id)->queryScalar();
        $minCashDate = !empty($minDate) ? max(self::MIN_REPORT_DATE, $minDate) : date(DateHelper::FORMAT_DATE);
        $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
        $currentYear = date('Y');
        foreach (range($registrationYear, $currentYear) as $value) {
            $range[$value] = $value;
        }
        arsort($range);

        return $range;
    }

    public function getIsCurrentYear()
    {
        return date('Y') == $this->year;
    }

    public function getCompany()
    {
        return $this->_company;
    }

    /** CHARTS */

    private static $_mainChartData = [];

    public function getFromCurrentMonthsPeriods($left, $right, $offsetYear = 0, $offsetMonth = "0") {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month")->modify("{$offsetMonth} month");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr->format('Y-m-01'),
                'to' => $curr->format('Y-m-t'),
            ];
            $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }

    /**
     * @param $dateFrom
     * @param $monthsCount
     * @param null $customGroup
     * @return bool
     */
    public function prepareMainChartData($dateFrom, $monthsCount, $customGroup = null)
    {
        $groupTable = ProductGroup::tableName();
        $productTable = Product::tableName();
        $turnoverTable = self::TABLE_PRODUCT_TURNOVER;
        $companyId = $this->_company->id;

        $seq = "seq_0_to_{$monthsCount}";
        $intervalCalc = "IF (('{$dateFrom}' + INTERVAL (seq) MONTH) >= '" . date('Y-m-01') . "', '" . date('Y-m-d') . "', LAST_DAY('{$dateFrom}' + INTERVAL (seq) MONTH))";
        $interval = "('{$dateFrom}' + INTERVAL (seq) MONTH)";

        if ($this->byGroups)
            $SQL_GROUP_FILTER = ($customGroup) ? (' AND g.id = ' . (int)$customGroup) : '';
        else
            $SQL_GROUP_FILTER = ($customGroup) ? (' AND p.id = ' . (int)$customGroup) : '';

        $monthSum = \Yii::$app->db2->createCommand("
                SELECT
                YEAR({$interval}) y,
                DATE_FORMAT({$interval}, '%m') m,
                SUM(IF(t.type = 2, -1, 1) * t.quantity * IFNULL(p.price_for_buy_with_nds, 0)) AS amount
                FROM {$seq}
                LEFT JOIN {$turnoverTable} t ON t.company_id = {$companyId} AND t.date <= {$intervalCalc}
                LEFT JOIN {$productTable} p ON p.id = t.product_id
                LEFT JOIN {$groupTable} g ON g.id = p.group_id                
                WHERE (t.production_type = 1)
                    AND (t.is_document_actual = TRUE)
                    AND (t.is_invoice_actual = TRUE)                
                    {$SQL_GROUP_FILTER}
                
                GROUP BY YEAR({$interval}), MONTH({$interval})
            ")->queryAll();

        self::$_mainChartData = [];
        foreach ($monthSum as $sum) {
            self::$_mainChartData[$sum['y'].'-'.$sum['m']] = (float)$sum['amount'] * 1 / 100;
        }

        return true;
    }

    /**
     * @param $dates
     * @return array
     */
    public function getMainChartData($dates)
    {
        $ret = [];
        foreach ($dates as $date) {

            $pos = substr($date['from'], 0, 7);

            if ($pos > date('Y-m')) {
                continue;
            }

            $ret[] = ArrayHelper::getValue(self::$_mainChartData, $pos, 0);
        }

        return $ret;
    }

    public function getStructureChartData($dateFrom, $dateTo)
    {
        if ($this->_cacheStructureChartData === null) {

            $groupTable = ProductGroup::tableName();
            $productTable = Product::tableName();
            $turnoverTable = self::TABLE_PRODUCT_TURNOVER;
            $companyId = $this->_company->id;
            if ($this->byGroups) {
                $query = "
                SELECT
                g.id,
                g.title,
                SUM(IF(t.type = 2, -1, 1) * t.quantity * IFNULL(p.price_for_buy_with_nds, 0)) AS amount
                FROM {$productTable} p 
                LEFT JOIN {$groupTable} g ON g.id = p.group_id
                JOIN {$turnoverTable} t ON t.product_id = p.id
                WHERE (p.company_id = {$companyId})
                    AND (p.production_type = 1) 
                    AND (t.date <= '$dateTo')
                    AND (t.is_document_actual = TRUE)
                    AND (t.is_invoice_actual = TRUE)
                GROUP BY g.id
                ORDER BY amount DESC
            ";
            } else {
                $query = "
                SELECT
                p.id,
                p.title,
                SUM(IF(t.type = 2, -1, 1) * t.quantity * IFNULL(p.price_for_buy_with_nds, 0)) AS amount
                FROM {$productTable} p 
                JOIN {$turnoverTable} t ON t.product_id = p.id
                WHERE (p.company_id = {$companyId})
                    AND (p.production_type = 1) 
                    AND (t.date <= '$dateTo')
                    AND (t.is_document_actual = TRUE)
                    AND (t.is_invoice_actual = TRUE)
                GROUP BY p.id
                ORDER BY amount DESC
            ";
            }

            $this->_cacheStructureChartData = Yii::$app->db2->createCommand($query)->queryAll();
            foreach ($this->_cacheStructureChartData as &$d)
                $d['amount'] = round($d['amount'] / 100, 2);
        }

        return $this->_cacheStructureChartData;
    }

    /**
     * @return array
     */
    public function getAllGroups()
    {
        return ArrayHelper::map(ProductGroup::find()->where(['company_id' => $this->_company->id])->all(Yii::$app->db2), 'id', 'title');
    }

    // XLS ////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @param $type
     * @throws \Exception
     */
    public function generateXls($type = null)
    {
        $dataProvider = $this->search();
        $dataProvider->setPagination(false);
        $data = $dataProvider->getModels();

        // header
        $columns = [];
        $columns[] = [
            'attribute' => 'title',
            'header' => ($this->byGroups) ? 'Группа' : 'Продукт',
        ];
        foreach (AbstractFinance::$month as $monthNumber => $monthText) {
            $columns[] = [
                'attribute' => $monthNumber,
                'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                'header' => $monthText,
            ];
        }

        // data
        $formattedData = [];
        foreach ($data as $d) {
            $formattedData[] = $this->_buildXlsRow($d);
        }

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $formattedData,
            'title' => "Запасы за {$this->year} год",
            'rangeHeader' => range('A', 'M'),
            'columns' => $columns,
            'format' => 'Xlsx',
            'fileName' => "Запасы за {$this->year} год",
        ]);
    }

    protected function _buildXlsRow($d)
    {
        return [
            'title' => $d['title'],
            '01' => round($d['01'] / 100, 2),
            '02' => round($d['02'] / 100, 2),
            '03' => round($d['03'] / 100, 2),
            '04' => round($d['04'] / 100, 2),
            '05' => round($d['05'] / 100, 2),
            '06' => round($d['06'] / 100, 2),
            '07' => round($d['07'] / 100, 2),
            '08' => round($d['08'] / 100, 2),
            '09' => round($d['09'] / 100, 2),
            '10' => round($d['10'] / 100, 2),
            '11' => round($d['11'] / 100, 2),
            '12' => round($d['12'] / 100, 2)
        ];
    }
}