<?php

namespace frontend\modules\analytics\models;

use common\models\EmployeeCompany;
use Yii;
use yii\db\Connection;
use common\models\Company;
use common\models\employee\Employee;
use yii\db\Expression;

class AnalyticsMultiCompanyForm extends AnalyticsMultiCompany {

    const CHANGE_MODE_URL = '/analytics/multi-company/change-mode';

    public $isModeEnabled;
    public $selectedCompaniesIds;

    public function rules()
    {
        return [
            [['employee_id', 'primary_company_id'], 'required'],
            [['employee_id', 'primary_company_id'], 'integer'],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
            [['primary_company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['primary_company_id' => 'id']],
            [['isModeEnabled'], 'boolean'],
            [['selectedCompaniesIds'], 'required', 'when' => function($model) { return !$model->isModeEnabled; }, 'message' => 'Нужно выбрать минимум 2 компании'],
            [['selectedCompaniesIds'], 'validateSelectedCompanies']
        ];
    }

    public function validateSelectedCompanies($attr, $params)
    {
        if ($this->isModeEnabled)
            return true;

        if (!is_array($this->{$attr}) || count($this->{$attr}) < self::MIN_COMPANIES_COUNT) {
            $this->addError($attr, 'Нужно выбрать минимум 2 компании');
            return false;
        }

        foreach ($this->{$attr} as $selectedCompanyId)
        {
            $employeeCompany = EmployeeCompany::find()
                ->where(['employee_id' => $this->employee_id, 'company_id' => $selectedCompanyId])
                ->one();

            if (!$employeeCompany) {
                $this->addError($attr, 'Нельзя выбрать компанию id="' . $selectedCompanyId . '"');
                return false;
            }
        }

        return true;
    }


    public function init()
    {
        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;
        /** @var Company $company */
        $primaryCompany = Yii::$app->user->identity->company;

        $this->selectedCompaniesIds = AnalyticsMultiCompany::find()
            ->where(['employee_id' => $employee->id])
            ->andWhere(['primary_company_id' => $primaryCompany->id])
            ->select(['company_id'])
            ->column();

        $this->isModeEnabled = (bool)$employee->currentEmployeeCompany->analytics_multi_company;
    }

    public function setModeOn()
    {
        $this->employee->currentEmployeeCompany->updateAttributes(['analytics_multi_company' => true]);
    }

    public function setModeOff()
    {
        $this->employee->currentEmployeeCompany->updateAttributes(['analytics_multi_company' => false]);
    }

    public function saveCompanies()
    {
        $result = Yii::$app->db->transaction(function(Connection $db) {

            $oldSelectedCompanies = AnalyticsMultiCompany::find()->where([
                'employee_id' => $this->employee_id,
                'primary_company_id' => $this->primary_company_id,
            ])->all();

            foreach ($oldSelectedCompanies as $oldSelectedCompany) {
                if (!in_array($oldSelectedCompany, (array)$this->selectedCompaniesIds))
                    if (!$oldSelectedCompany->delete()) {
                        $db->transaction->rollBack();
                        return false;
                    }
            }

            foreach ((array)$this->selectedCompaniesIds as $selectedCompanyId) {
                if (!AnalyticsMultiCompany::find()->where([
                    'employee_id' => $this->employee_id,
                    'primary_company_id' => $this->primary_company_id,
                    'company_id' => $selectedCompanyId,
                ])->exists()) {

                    $opt = new AnalyticsMultiCompany([
                        'employee_id' => $this->employee_id,
                        'primary_company_id' => $this->primary_company_id,
                        'company_id' => $selectedCompanyId,
                    ]);

                    if (!$opt->save()) {
                        $db->transaction->rollBack();
                        return false;
                    }
                }
            }

            return true;
        });

        return $result;
    }
}