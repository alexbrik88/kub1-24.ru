<?php
namespace frontend\modules\analytics\models\profitAndLoss;

use common\models\cash\CashFlowsBase as Cash;
use frontend\modules\analytics\models\AnalyticsArticle as Article;
use frontend\modules\analytics\models\ProfitAndLossCompanyItem as Item;

/// docs: сумма входящих Актов, ТН и УПД, у которых указанные статьи и дата которых попадает в нужный месяц
/// flows: есть привязка к счетам, у которых НЕТ Актов/ТН/УПД или у них стоит "Без Акта/ТН/УПД" и "Дата признания дохода" в нужном нам месяце +
/// flows: нет привязки к счетам и "Дата признания дохода" нужном месяце

trait VariableCostsTrait {

    public function getVariableCosts($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__TRAIT__, $companyId);

        if (!array_key_exists($cachePrefix . $year, $this->data)) {

            $docs = $this->rawDocs[$companyId][Article::PAL_EXPENSES_VARIABLE] ?? [];
            $flows = $this->rawFlows[$companyId][Article::PAL_EXPENSES_VARIABLE] ?? [];

            foreach ([$year] as $byYear) {

                $parent = $parentItem;
                $result = [];

                foreach ($docs as $doc) {
                    if ($byYear == $doc['y']) {
                        self::_addToResult($doc, $result, Cash::FLOW_TYPE_EXPENSE, self::MOVEMENT_TYPE_DOCS_AND_FLOWS, Item::VARIABLE_EXPENSE_TYPE);
                        self::_addToParent($doc, $parent);
                    }
                }

                foreach ($flows as $flow) {
                    if ($byYear == $flow['y']) {
                        self::_addToResult($flow, $result, Cash::FLOW_TYPE_EXPENSE, self::MOVEMENT_TYPE_DOCS_AND_FLOWS, Item::VARIABLE_EXPENSE_TYPE);
                        self::_addToParent($flow, $parent);
                    }
                }

                self::_sortResult($result);
                $result = ['totalVariableCosts' => $parent] + $result;
                $this->data[$cachePrefix . $byYear] = $this->calculateQuarterAndTotalAmount($result);
            }
        }

        return $this->data[$cachePrefix . $year];
    }
}
