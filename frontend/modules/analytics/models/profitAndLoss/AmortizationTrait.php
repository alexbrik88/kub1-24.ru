<?php

namespace frontend\modules\analytics\models\profitAndLoss;

use common\models\balance\BalanceArticle;
use frontend\modules\analytics\models\AnalyticsArticle as Article;
use frontend\modules\analytics\models\AnalyticsArticleForm as Form;

trait AmortizationTrait {

    public function getAmortization($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__TRAIT__, $companyId);
        $groupedBy = $this->getMonthGroupBy();

        if (!array_key_exists($cachePrefix . $year, $this->data)) {

            foreach ([$year] as $byYear) {

                $parent = $parentItem;
                $result = [
                    'amortization_os' => self::_getBlankItem('amortization', 'Амортизация ОС', null, 'expense-amortization_os', null),
                    'amortization_nma' => self::_getBlankItem('amortization', 'Амортизация НМА', null, 'expense-amortization_nma', null)
                ];

                foreach ($result as $amortizationKey => $amortizationData) {
                    if ($this->userOptions['showAmortization']) {
                        if ($amortizationKey === 'amortization_os') {
                            $items = self::_getAmortizationItems($companyId, BalanceArticle::TYPE_FIXED_ASSERTS);
                        }
                        if ($amortizationKey === 'amortization_nma') {
                            $items = self::_getAmortizationItems($companyId, BalanceArticle::TYPE_INTANGIBLE_ASSETS);
                        }
                    } else {
                        continue;
                    }

                    /** @var BalanceArticle $item */
                    foreach ($items as $item) {
                        foreach ($item->getAmortizationScheme() as $ym => $amount) {
                            $y = substr($ym, 0, 4);
                            $m = substr($ym, 4, 2);
                            if ($y == $year) {
                                $result[$amortizationKey]['amount'][$m] += $amount;
                                $result[$amortizationKey]['amountTax'][$m] += $amount;
                                // GROUPED BY MONTH
                                if ($groupedBy) {
                                    $result[$amortizationKey]['groupedAmount'][self::$monthGroupZero][$m] += $amount;
                                    $result[$amortizationKey]['groupedAmountTax'][self::$monthGroupZero][$m] += $amount;
                                }

                                $this->_addAmortizationToParent($parent, $m, $amount);
                            }
                        }
                    }

                }

                $result = ['totalAmortization' => $parent] + $result;

                $this->data[$cachePrefix . $byYear] = $this->calculateQuarterAndTotalAmount($result);
            }
        }

        return $this->data[$cachePrefix . $year];
    }

    private static int $monthGroupZero = 0;

    private static function _getAmortizationItems($companyId, int $type): array
    {
        return BalanceArticle::find()
            ->andWhere(['company_id' => $companyId])
            ->andWhere(['type' => $type])
            ->all();
    }

    private function _addAmortizationToParent(&$parent, string $m, int $amount): void
    {
        $valueAdapter = [
            'm' => $m,
            'industry_id' => self::$monthGroupZero,
            'sale_point_id' => self::$monthGroupZero,
            'project_id' => self::$monthGroupZero,
            'amount' => $amount,
            'amountTax' => $amount,
        ];

        $this->_addToParent($valueAdapter, $parent);
    }
}