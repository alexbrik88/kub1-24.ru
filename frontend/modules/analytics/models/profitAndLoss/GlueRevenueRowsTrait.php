<?php
namespace frontend\modules\analytics\models\profitAndLoss;

trait GlueRevenueRowsTrait
{
    private function _glueRevenueRowsByTop($TOP_LIMIT = 10)
    {
        $num = 0;
        foreach ($this->result as $year => $level1) {

            if ($year != $this->year)
                continue;

            foreach ($level1 as $itemKey => $level2) {

                if ($itemKey === 'revenueByNoDocs')
                    continue;

                if (substr($itemKey, 0, 7) === 'revenue') {
                    $num++;

                    if ($num == 1 + $TOP_LIMIT) {
                        $lastItemKey = $itemKey;
                        $this->result[$year][$itemKey]['label'] = 'Остальные';
                    } elseif ($num > 1 + $TOP_LIMIT && isset($lastItemKey)) {

                        @list($_flowType, $_itemKey, $_itemId) = explode('-', $level2['data-article']);

                        // add id
                        $this->result[$year][$lastItemKey]['data-article'] .= '_' . $_itemId;
                        // add sum
                        foreach ($level2['amount'] as $m => $amount)
                            $this->result[$year][$lastItemKey]['amount'][$m] += $amount;
                        foreach ($level2['amountTax'] as $m => $amountTax)
                            $this->result[$year][$lastItemKey]['amountTax'][$m] += $amountTax;
                        // unset
                        unset($this->result[$year][$itemKey]);
                        continue;
                    }
                }
            }
        }
    }

    private function _glueRevenueRowsByPercent($PERCENT = 5)
    {
        foreach ($this->result as $year => $level1) {

            if ($year != $this->year)
                continue;

            $TOTAL = $level1['totalRevenue']['amount']['total'] ?? 9E9;

            if (!$TOTAL)
                return;

            foreach ($level1 as $itemKey => $level2) {

                if ($itemKey === 'revenueByNoDocs')
                    continue;

                if (substr($itemKey, 0, 7) === 'revenue') {

                    if ($level2['amount']['total'] / $TOTAL * 100 <= $PERCENT) {
                        if (!isset($lastItemKey)) {
                            $lastItemKey = $itemKey;
                            $this->result[$year][$lastItemKey]['label'] = 'Остальные';
                        } else {

                            @list($_flowType, $_itemKey, $_itemId) = explode('-', $level2['data-article']);

                            // add id
                            $this->result[$year][$lastItemKey]['data-article'] .= '_' . $_itemId;
                            // add sum
                            foreach ($level2['amount'] as $m => $amount)
                                $this->result[$year][$lastItemKey]['amount'][$m] += $amount;
                            foreach ($level2['amountTax'] as $m => $amountTax)
                                $this->result[$year][$lastItemKey]['amountTax'][$m] += $amountTax;
                            // unset
                            unset($this->result[$year][$itemKey]);
                            continue;
                        }
                    }
                }
            }
        }
    }
}