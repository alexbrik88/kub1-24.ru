<?php

namespace frontend\modules\analytics\models\profitAndLoss;

use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashFlowsBase as Cash;
use common\models\document\InvoiceIncomeItem;
use frontend\modules\analytics\models\AnalyticsArticle as Article;
use yii\db\Expression;

trait AnotherIncomeTrait {

    /**
     * @param $parentItem
     * @param $year
     * @return mixed
     */
    public function getAnotherIncome($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__TRAIT__, $companyId);

        if (!array_key_exists($cachePrefix . $year, $this->data)) {

            $docs = $this->rawDocs[$companyId][Article::PAL_INCOME_OTHER] ?? [];
            $flows = $this->rawFlows[$companyId][Article::PAL_INCOME_OTHER] ?? [];

            foreach ([$year] as $byYear) {

                $result = [];
                $parent = $parentItem;

                foreach ($docs as $doc) {
                    if ($byYear == $doc['y']) {
                        self::_addToResult($doc, $result, Cash::FLOW_TYPE_INCOME, self::MOVEMENT_TYPE_FLOWS);
                        self::_addToParent($doc, $parent);
                    }
                }

                foreach ($flows as $flow) {
                    if ($byYear == $flow['y']) {
                        self::_addToResult($flow, $result, Cash::FLOW_TYPE_INCOME, self::MOVEMENT_TYPE_FLOWS);
                        self::_addToParent($flow, $parent);
                    }
                }

                $this->_addExchangeDifferenceToResult($parent, $result, 'income');

                self::_sortResult($result);
                $result = ['totalAnotherIncome' => $parent] + $result;
                $this->data[$cachePrefix . $byYear] = $this->calculateQuarterAndTotalAmount($result);
            }
        }

        return $this->data[$cachePrefix . $year];
    }
}