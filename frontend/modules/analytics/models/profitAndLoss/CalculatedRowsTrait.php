<?php

namespace frontend\modules\analytics\models\profitAndLoss;
use common\models\company\CompanyTaxationType;

trait CalculatedRowsTrait {


    /**
     * @param $parentItem
     * @return mixed
     */
    public function getMarginalIncome($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__revenue = self::getCachePrefix(RevenueTrait::class, $companyId);
        $__primeCosts = self::getCachePrefix(PrimeCostsTrait::class, $companyId);

        $revenueData = current($this->data[$__revenue . $year]);
        $primeCostsData = current($this->data[$__primeCosts . $year]);
        foreach (self::$month as $monthNumber => $monthText) {
            $parentItem['amount'][$monthNumber] = $revenueData['amount'][$monthNumber] - $primeCostsData['amount'][$monthNumber];
            $parentItem['amountTax'][$monthNumber] = $revenueData['amountTax'][$monthNumber] - $primeCostsData['amountTax'][$monthNumber];
        }

        // GROUPED BY MONTH
        $groupNames = $this->getGroupByNames();
        foreach ($groupNames as $groupId => $groupName) {
            foreach (self::$month as $monthNumber => $monthText) {
                $parentItem['groupedAmount'][$groupId][$monthNumber] = $revenueData['groupedAmount'][$groupId][$monthNumber] - $primeCostsData['groupedAmount'][$groupId][$monthNumber];
                $parentItem['groupedAmountTax'][$groupId][$monthNumber] = $revenueData['groupedAmountTax'][$groupId][$monthNumber] - $primeCostsData['groupedAmountTax'][$groupId][$monthNumber];
            }
        }

        $this->data[$cachePrefix . $year] = $this->calculateQuarterAndTotalAmount(['marginalIncome' => $parentItem]);

        return $this->data[$cachePrefix . $year];
    }

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getMarginality($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__revenue = self::getCachePrefix(RevenueTrait::class, $companyId);
        $__marginalIncome = self::getCachePrefix('getMarginalIncome', $companyId);

        $revenueData = current($this->data[$__revenue . $year]);
        $marginalIncomeData = current($this->data[$__marginalIncome . $year]);
        foreach ($revenueData['amount'] as $key => $amount) {
            $revenueAmount = $amount > 0 ? $amount : 9E99;
            $parentItem['amount'][$key] = round($marginalIncomeData['amount'][$key] / $revenueAmount * 10000, 2);
        }
        foreach ($revenueData['amountTax'] as $key => $amount) {
            $revenueAmount = $amount > 0 ? $amount : 9E99;
            $parentItem['amountTax'][$key] = round($marginalIncomeData['amountTax'][$key] / $revenueAmount * 10000, 2);
        }

        // GROUPED BY MONTH
        $groupNames = $this->getGroupByNames();
        foreach ($groupNames as $groupId => $groupName) {
            foreach (self::$month as $monthNumber => $monthText) {
                $amount = $revenueData['groupedAmount'][$groupId][$monthNumber];
                $amountTax = $revenueData['groupedAmountTax'][$groupId][$monthNumber];
                $revenueAmount = $amount > 0 ? $amount : 9E99;
                $revenueAmountTax = $amountTax > 0 ? $amountTax : 9E99;
                $parentItem['groupedAmount'][$groupId][$monthNumber] = round($marginalIncomeData['groupedAmount'][$groupId][$monthNumber] / $revenueAmount * 100 * 100, 2);
                $parentItem['groupedAmountTax'][$groupId][$monthNumber] = round($marginalIncomeData['groupedAmountTax'][$groupId][$monthNumber] / $revenueAmountTax * 100 * 100, 2);
                if ($monthNumber % 3 == 0) {
                    $quarterNumber = "quarter-" . (int)ceil($monthNumber / 3);
                    $revenueQuarterAmount = $revenueData['groupedAmount'][$groupId][$quarterNumber] ?: 9E99;
                    $revenueQuarterAmountTax = $revenueData['groupedAmountTax'][$groupId][$quarterNumber] ?: 9E99;
                    $parentItem['groupedAmount'][$groupId][$quarterNumber] = round($marginalIncomeData['groupedAmount'][$groupId][$quarterNumber] / $revenueQuarterAmount * 100 * 100, 2);
                    $parentItem['groupedAmountTax'][$groupId][$quarterNumber] = round($marginalIncomeData['groupedAmountTax'][$groupId][$quarterNumber] / $revenueQuarterAmountTax * 100 * 100, 2);
                }
            }
        }

        $this->data[$cachePrefix . $year]['marginality'] = $parentItem;

        return $this->data[$cachePrefix . $year];
    }

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getOperatingProfit($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__grossProfit = self::getCachePrefix('getGrossProfit', $companyId);
        $__fixedCosts = self::getCachePrefix(FixedCostsTrait::class, $companyId);

        $grossProfitData = current($this->data[$__grossProfit . $year]);
        $fixedCostsData = current($this->data[$__fixedCosts . $year]);
        foreach (self::$month as $monthNumber => $monthText) {
            $parentItem['amount'][$monthNumber] = $grossProfitData['amount'][$monthNumber] - $fixedCostsData['amount'][$monthNumber];
            $parentItem['amountTax'][$monthNumber] = $grossProfitData['amountTax'][$monthNumber] - $fixedCostsData['amountTax'][$monthNumber];
        }

        // GROUPED BY MONTH
        $groupNames = $this->getGroupByNames();
        foreach ($groupNames as $groupId => $groupName) {
            foreach (self::$month as $monthNumber => $monthText) {
                $parentItem['groupedAmount'][$groupId][$monthNumber] = $grossProfitData['groupedAmount'][$groupId][$monthNumber] - $fixedCostsData['groupedAmount'][$groupId][$monthNumber];
                $parentItem['groupedAmountTax'][$groupId][$monthNumber] = $grossProfitData['groupedAmountTax'][$groupId][$monthNumber] - $fixedCostsData['groupedAmountTax'][$groupId][$monthNumber];
            }
        }

        $this->data[$cachePrefix . $year] = $this->calculateQuarterAndTotalAmount(['operatingProfit' => $parentItem]);

        return $this->data[$cachePrefix . $year];
    }


    /**
     * @param $parentItem
     * @return mixed
     */
    public function getEbidta($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__operatingProfit = self::getCachePrefix('getOperatingProfit', $companyId);
        $__anotherIncome = self::getCachePrefix(AnotherIncomeTrait::class, $companyId);
        $__anotherCosts = self::getCachePrefix(AnotherCostsTrait::class, $companyId);

        $operatingProfitData = current($this->data[$__operatingProfit . $year]);
        $anotherIncomeData = current($this->data[$__anotherIncome . $year]);
        $anotherCostsData = current($this->data[$__anotherCosts . $year]);

        foreach (self::$month as $monthNumber => $monthText) {
            $parentItem['amount'][$monthNumber] = $operatingProfitData['amount'][$monthNumber] + $anotherIncomeData['amount'][$monthNumber] - $anotherCostsData['amount'][$monthNumber];
            $parentItem['amountTax'][$monthNumber] = $operatingProfitData['amountTax'][$monthNumber] + $anotherIncomeData['amountTax'][$monthNumber] - $anotherCostsData['amountTax'][$monthNumber];
        }

        // GROUPED BY MONTH
        $groupNames = $this->getGroupByNames();
        foreach ($groupNames as $groupId => $groupName) {
            foreach (self::$month as $monthNumber => $monthText) {
                $parentItem['groupedAmount'][$groupId][$monthNumber] = $operatingProfitData['groupedAmount'][$groupId][$monthNumber] + $anotherIncomeData['groupedAmount'][$groupId][$monthNumber] - $anotherCostsData['groupedAmount'][$groupId][$monthNumber];
                $parentItem['groupedAmountTax'][$groupId][$monthNumber] = $operatingProfitData['groupedAmountTax'][$groupId][$monthNumber] + $anotherIncomeData['groupedAmount'][$groupId][$monthNumber] - $anotherCostsData['groupedAmountTax'][$groupId][$monthNumber];
            }
        }

        $this->data[$cachePrefix . $year] = $this->calculateQuarterAndTotalAmount(['ebitda' => $parentItem]);

        return $this->data[$cachePrefix . $year];
    }

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getEbit($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__ebidta = self::getCachePrefix('getEbidta', $companyId);
        $__amortization = self::getCachePrefix('getAmortization', $companyId);

        $ebidtaData = current($this->data[$__ebidta . $year]);
        $amortizationData = current($this->data[$__amortization . $year]);

        foreach (self::$month as $monthNumber => $monthText) {
            $parentItem['amount'][$monthNumber] = $ebidtaData['amount'][$monthNumber] - $amortizationData['amount'][$monthNumber];
            $parentItem['amountTax'][$monthNumber] = $ebidtaData['amountTax'][$monthNumber] - $amortizationData['amountTax'][$monthNumber];
        }

        // GROUPED BY MONTH
        $groupNames = $this->getGroupByNames();
        foreach ($groupNames as $groupId => $groupName) {
            foreach (self::$month as $monthNumber => $monthText) {
                $parentItem['groupedAmount'][$groupId][$monthNumber] = $ebidtaData['groupedAmount'][$groupId][$monthNumber] - $amortizationData['groupedAmount'][$groupId][$monthNumber];
                $parentItem['groupedAmountTax'][$groupId][$monthNumber] = $ebidtaData['groupedAmountTax'][$groupId][$monthNumber] - $amortizationData['groupedAmountTax'][$groupId][$monthNumber];
            }
        }

        $this->data[$cachePrefix . $year] = $this->calculateQuarterAndTotalAmount(['ebit' => $parentItem]);

        return $this->data[$cachePrefix . $year];
    }

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getGrossProfit($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__marginalIncome = self::getCachePrefix('getMarginalIncome', $companyId);
        $__variableCosts = self::getCachePrefix(VariableCostsTrait::class, $companyId);

        $marginalData = current($this->data[$__marginalIncome . $year]);
        $variableCostsData = current($this->data[$__variableCosts . $year]);
        foreach (self::$month as $monthNumber => $monthText) {
            $parentItem['amount'][$monthNumber] = $marginalData['amount'][$monthNumber] - $variableCostsData['amount'][$monthNumber];
            $parentItem['amountTax'][$monthNumber] = $marginalData['amountTax'][$monthNumber] - $variableCostsData['amountTax'][$monthNumber];
        }

        // GROUPED BY MONTH
        $groupNames = $this->getGroupByNames();
        foreach ($groupNames as $groupId => $groupName) {
            foreach (self::$month as $monthNumber => $monthText) {
                $parentItem['groupedAmount'][$groupId][$monthNumber] = $marginalData['groupedAmount'][$groupId][$monthNumber] - $variableCostsData['groupedAmount'][$groupId][$monthNumber];
                $parentItem['groupedAmountTax'][$groupId][$monthNumber] = $marginalData['groupedAmountTax'][$groupId][$monthNumber] - $variableCostsData['groupedAmountTax'][$groupId][$monthNumber];
            }
        }

        $this->data[$cachePrefix . $year] = $this->calculateQuarterAndTotalAmount(['grossProfit' => $parentItem]);

        return $this->data[$cachePrefix . $year];
    }

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getProfitabilityByGrossProfit($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__revenue = self::getCachePrefix(RevenueTrait::class, $companyId);
        $__grossProfit = self::getCachePrefix('getGrossProfit', $companyId);

        $revenueData = current($this->data[$__revenue . $year]);
        $grossProfitData = current($this->data[$__grossProfit . $year]);
        foreach ($revenueData['amount'] as $key => $amount) {
            $revenueAmount = $amount > 0 ? $amount : 9E99;
            $parentItem['amount'][$key] = round($grossProfitData['amount'][$key] / $revenueAmount * 100 * 100, 2);
        }
        foreach ($revenueData['amountTax'] as $key => $amount) {
            $revenueAmount = $amount > 0 ? $amount : 9E99;
            $parentItem['amountTax'][$key] = round($grossProfitData['amountTax'][$key] / $revenueAmount * 100 * 100, 2);
        }

        // GROUPED BY MONTH
        $groupNames = $this->getGroupByNames();
        foreach ($groupNames as $groupId => $groupName) {
            foreach (self::$month as $monthNumber => $monthText) {
                $amount = $revenueData['groupedAmount'][$groupId][$monthNumber];
                $amountTax = $revenueData['groupedAmountTax'][$groupId][$monthNumber];
                $revenueAmount = $amount > 0 ? $amount : 9E99;
                $revenueAmountTax = $amountTax > 0 ? $amountTax : 9E99;
                $parentItem['groupedAmount'][$groupId][$monthNumber] = round($grossProfitData['groupedAmount'][$groupId][$monthNumber] / $revenueAmount * 100 * 100, 2);
                $parentItem['groupedAmountTax'][$groupId][$monthNumber] = round($grossProfitData['groupedAmountTax'][$groupId][$monthNumber] / $revenueAmountTax * 100 * 100, 2);
                if ($monthNumber % 3 == 0) {
                    $quarterNumber = "quarter-" . (int)ceil($monthNumber / 3);
                    $revenueQuarterAmount = $revenueData['groupedAmount'][$groupId][$quarterNumber] ?: 9E99;
                    $revenueQuarterAmountTax = $revenueData['groupedAmountTax'][$groupId][$quarterNumber] ?: 9E99;
                    $parentItem['groupedAmount'][$groupId][$quarterNumber] = round($grossProfitData['groupedAmount'][$groupId][$quarterNumber] / $revenueQuarterAmount * 100 * 100, 2);
                    $parentItem['groupedAmountTax'][$groupId][$quarterNumber] = round($grossProfitData['groupedAmountTax'][$groupId][$quarterNumber] / $revenueQuarterAmountTax * 100 * 100, 2);
                }
            }
        }

        $this->data[$cachePrefix . $year]['profitabilityByGrossProfit'] = $parentItem;

        return $this->data[$cachePrefix . $year];
    }

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getProfitBeforeTax($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__ebit = self::getCachePrefix('getEbit', $companyId);
        $__paymentPercent = self::getCachePrefix(PaymentPercentTrait::class, $companyId);
        $__receivedPercent = self::getCachePrefix(ReceivedPercentTrait::class, $companyId);

        //  "Прибыль до налогообложения" = EBIT + "Проценты полученные" - "Проценты уплаченные""
        $ebit = current($this->data[$__ebit . $year]);
        $paymentPercent = current($this->data[$__paymentPercent . $year]);
        $receivedPercent = current($this->data[$__receivedPercent . $year]);

        foreach (self::$month as $monthNumber => $monthText) {
            $parentItem['amount'][$monthNumber] = $ebit['amount'][$monthNumber] + ($receivedPercent['amount'][$monthNumber] - $paymentPercent['amount'][$monthNumber]);
            $parentItem['amountTax'][$monthNumber] = $ebit['amountTax'][$monthNumber] + ($receivedPercent['amountTax'][$monthNumber] - $paymentPercent['amountTax'][$monthNumber]);
        }

        // GROUPED BY MONTH
        $groupNames = $this->getGroupByNames();
        foreach ($groupNames as $groupId => $groupName) {
            foreach (self::$month as $monthNumber => $monthText) {
                $parentItem['groupedAmount'][$groupId][$monthNumber] = $ebit['groupedAmount'][$groupId][$monthNumber] + ($receivedPercent['groupedAmount'][$groupId][$monthNumber] - $paymentPercent['groupedAmount'][$groupId][$monthNumber]);
                $parentItem['groupedAmountTax'][$groupId][$monthNumber] = $ebit['groupedAmountTax'][$groupId][$monthNumber] + ($receivedPercent['groupedAmount'][$groupId][$monthNumber] - $paymentPercent['groupedAmountTax'][$groupId][$monthNumber]);
            }
        }

        $this->data[$cachePrefix . $year] = $this->calculateQuarterAndTotalAmount(['profitBeforeTax' => $parentItem]);

        return $this->data[$cachePrefix . $year];
    }

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getTax($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__revenue = self::getCachePrefix(RevenueTrait::class, $companyId);
        $__ebidta = self::getCachePrefix('getEbidta', $companyId);
        $__profitBeforeTax = self::getCachePrefix('getProfitBeforeTax', $companyId);

        $companyTaxationType = $this->multiCompanyManager->getCompanyTaxationType($companyId);

        $groupNames = $this->getGroupByNames();

        if ($this->userOptions['skipTaxes']) {
            $revenueData = current($this->data[$__revenue . $year]);
            foreach ($revenueData['amountTax'] as $key => $amount) {
                $parentItem['amount'][$key] = 0;
                $parentItem['amountTax'][$key] = 0;
            }

            // GROUPED BY MONTH
            foreach ($groupNames as $groupId => $groupName) {
                foreach (self::$month as $monthNumber => $monthText) {
                    $parentItem['groupedAmount'][$groupId][$monthNumber] = 0;
                    $parentItem['groupedAmountTax'][$groupId][$monthNumber] = 0;
                }
            }

        } elseif ($companyTaxationType['usn']) {
            if ($companyTaxationType['usn_type'] == CompanyTaxationType::INCOME) {
                $revenueData = current($this->data[$__revenue . $year]);
                foreach ($revenueData['amountTax'] as $key => $amount) {
                    $parentItem['amount'][$key] = max(0, round($revenueData['amountTax'][$key] * $companyTaxationType['usn_percent'] / 100, 2));
                    $parentItem['amountTax'][$key] = max(0, round($revenueData['amountTax'][$key] * $companyTaxationType['usn_percent'] / 100, 2));
                }

                // GROUPED BY MONTH
                foreach ($groupNames as $groupId => $groupName) {
                    foreach (self::$month as $monthNumber => $monthText) {
                        $parentItem['groupedAmount'][$groupId][$monthNumber] = round($revenueData['groupedAmountTax'][$groupId][$monthNumber] * $companyTaxationType['usn_percent'] / 100, 2);
                        $parentItem['groupedAmountTax'][$groupId][$monthNumber] = round($revenueData['groupedAmountTax'][$groupId][$monthNumber] * $companyTaxationType['usn_percent'] / 100, 2);
                    }
                }

            } else {
                $profitBeforeTax = current($this->data[$__profitBeforeTax . $year]);
                foreach ($profitBeforeTax['amountTax'] as $key => $amount) {
                    $parentItem['amount'][$key] = max(0, round($profitBeforeTax['amountTax'][$key] * $companyTaxationType['usn_percent'] / 100, 2));
                    $parentItem['amountTax'][$key] = max(0, round($profitBeforeTax['amountTax'][$key] * $companyTaxationType['usn_percent'] / 100, 2));
                }

                // GROUPED BY MONTH
                foreach ($groupNames as $groupId => $groupName) {
                    foreach (self::$month as $monthNumber => $monthText) {
                        $parentItem['groupedAmount'][$groupId][$monthNumber] = round($profitBeforeTax['groupedAmountTax'][$groupId][$monthNumber] * $companyTaxationType['usn_percent'] / 100, 2);
                        $parentItem['groupedAmountTax'][$groupId][$monthNumber] = round($profitBeforeTax['groupedAmountTax'][$groupId][$monthNumber] * $companyTaxationType['usn_percent'] / 100, 2);
                    }
                }
            }
        } elseif ($companyTaxationType['osno']) {
            $PERCENT_20 = 20;
            $profitBeforeTax = current($this->data[$__profitBeforeTax . $year]);
            foreach ($profitBeforeTax['amountTax'] as $key => $amount) {
                $parentItem['amount'][$key] = max(0, round($profitBeforeTax['amountTax'][$key] * $PERCENT_20 / 100, 2));
                $parentItem['amountTax'][$key] = max(0, round($profitBeforeTax['amountTax'][$key] * $PERCENT_20 / 100, 2));
            }

            // GROUPED BY MONTH
            foreach ($groupNames as $groupId => $groupName) {
                foreach (self::$month as $monthNumber => $monthText) {
                    $parentItem['groupedAmount'][$groupId][$monthNumber] = round($profitBeforeTax['groupedAmountTax'][$groupId][$monthNumber] * $PERCENT_20 / 100, 2);
                    $parentItem['groupedAmountTax'][$groupId][$monthNumber] = round($profitBeforeTax['groupedAmountTax'][$groupId][$monthNumber] * $PERCENT_20 / 100, 2);
                }
            }
        }

        $this->data[$cachePrefix . $year] = $this->calculateQuarterAndTotalAmount(['tax' => $parentItem]);

        return $this->data[$cachePrefix . $year];
    }

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getNetIncomeLoss($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__profitBeforeTax = self::getCachePrefix('getProfitBeforeTax', $companyId);
        $__tax = self::getCachePrefix('getTax', $companyId);

        $profitBeforeTax = current($this->data[$__profitBeforeTax . $year]);
        $taxData = current($this->data[$__tax . $year]);

        foreach ($profitBeforeTax['amount'] as $key => $amount) {
            $parentItem['amount'][$key] = $profitBeforeTax['amount'][$key] - $taxData['amount'][$key];
        }
        foreach ($profitBeforeTax['amount'] as $key => $amount) {
            $parentItem['amountTax'][$key] = $profitBeforeTax['amountTax'][$key] - $taxData['amountTax'][$key];
        }

        // GROUPED BY MONTH
        $groupNames = $this->getGroupByNames();
        foreach ($groupNames as $groupId => $groupName) {
            foreach (self::$month as $monthNumber => $monthText) {
                $parentItem['groupedAmount'][$groupId][$monthNumber] = $profitBeforeTax['groupedAmount'][$groupId][$monthNumber] - $taxData['groupedAmount'][$groupId][$monthNumber];
                $parentItem['groupedAmountTax'][$groupId][$monthNumber] = $profitBeforeTax['groupedAmountTax'][$groupId][$monthNumber] - $taxData['groupedAmountTax'][$groupId][$monthNumber];
            }
        }

        $this->data[$cachePrefix . $year] = $this->calculateQuarterAndTotalAmount(['netIncomeLoss' => $parentItem]);

        return $this->data[$cachePrefix . $year];
    }


    /**
     * @param $parentItem
     * @return mixed
     */
    public function getProfitabilityByNetIncomeLoss($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__revenue = self::getCachePrefix(RevenueTrait::class, $companyId);
        $__netIncomeLoss = self::getCachePrefix('getNetIncomeLoss', $companyId);

        $revenueData = current($this->data[$__revenue . $year]);
        $netIncomeLossData = current($this->data[$__netIncomeLoss . $year]);
        foreach ($revenueData['amount'] as $key => $amount) {
            $revenueAmount = $amount > 0 ? $amount : 9E99;
            $parentItem['amount'][$key] = round($netIncomeLossData['amount'][$key] / $revenueAmount * 100 * 100, 2);
        }
        foreach ($revenueData['amountTax'] as $key => $amount) {
            $revenueAmount = $amount > 0 ? $amount : 9E99;
            $parentItem['amountTax'][$key] = round($netIncomeLossData['amountTax'][$key] / $revenueAmount * 100 * 100, 2);
        }

        // GROUPED BY MONTH
        $groupNames = $this->getGroupByNames();
        foreach ($groupNames as $groupId => $groupName) {
            foreach (self::$month as $monthNumber => $monthText) {
                $amount = $revenueData['groupedAmount'][$groupId][$monthNumber];
                $amountTax = $revenueData['groupedAmountTax'][$groupId][$monthNumber];
                $revenueAmount = $amount > 0 ? $amount : 9E99;
                $revenueAmountTax = $amountTax > 0 ? $amountTax : 9E99;
                $parentItem['groupedAmount'][$groupId][$monthNumber] = round($netIncomeLossData['groupedAmount'][$groupId][$monthNumber] / $revenueAmount * 100 * 100, 2);
                $parentItem['groupedAmountTax'][$groupId][$monthNumber] = round($netIncomeLossData['groupedAmountTax'][$groupId][$monthNumber] / $revenueAmountTax * 100 * 100, 2);
                if ($monthNumber % 3 == 0) {
                    $quarterNumber = "quarter-" . (int)ceil($monthNumber / 3);
                    $revenueQuarterAmount = $revenueData['groupedAmount'][$groupId][$quarterNumber] ?: 9E99;
                    $revenueQuarterAmountTax = $revenueData['groupedAmountTax'][$groupId][$quarterNumber] ?: 9E99;
                    $parentItem['groupedAmount'][$groupId][$quarterNumber] = round($netIncomeLossData['groupedAmount'][$groupId][$quarterNumber] / $revenueQuarterAmount * 100 * 100, 2);
                    $parentItem['groupedAmountTax'][$groupId][$quarterNumber] = round($netIncomeLossData['groupedAmountTax'][$groupId][$quarterNumber] / $revenueQuarterAmountTax * 100 * 100, 2);
                }
            }
        }

        $this->data[$cachePrefix . $year]['profitabilityByNetIncomeLoss'] = $parentItem;

        return $this->data[$cachePrefix . $year];
    }    
    
     /**
     * @param $parentItem
     * @return mixed
     */
    public function getUndestributedProfits($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__netIncomeLoss = self::getCachePrefix('getNetIncomeLoss', $companyId);
        $__paymentDividend = self::getCachePrefix('paymentDividend', $companyId);

        $netIncomeLoss = current($this->data[$__netIncomeLoss . $year]);
        $paymentDividend = current($this->data[$__paymentDividend . $year]);

        $groupNames = $this->getGroupByNames();

        foreach (self::$month as $monthNumber => $monthText) {

            $parentItem['amount'][$monthNumber] = $netIncomeLoss['amount'][$monthNumber] - $paymentDividend['amount'][$monthNumber];
            $parentItem['amountTax'][$monthNumber] = $netIncomeLoss['amountTax'][$monthNumber] - $paymentDividend['amountTax'][$monthNumber];

            // GROUPED BY MONTH
            foreach ($groupNames as $groupId => $groupName) {
                $parentItem['groupedAmount'][$groupId][$monthNumber] = $netIncomeLoss['groupedAmount'][$groupId][$monthNumber] - $paymentDividend['groupedAmount'][$groupId][$monthNumber];
                $parentItem['groupedAmountTax'][$groupId][$monthNumber] = $netIncomeLoss['groupedAmountTax'][$groupId][$monthNumber] - $paymentDividend['groupedAmountTax'][$groupId][$monthNumber];
            }

            if ($monthNumber - 1 > 0) {
                $monthNumberPrev = str_pad($monthNumber - 1, 2, "0", STR_PAD_LEFT);
                $parentItem['amount'][$monthNumber] += $parentItem['amount'][$monthNumberPrev];
                $parentItem['amountTax'][$monthNumber] += $parentItem['amountTax'][$monthNumberPrev];

                // GROUPED BY MONTH
                foreach ($groupNames as $groupId => $groupName) {
                    $parentItem['groupedAmount'][$groupId][$monthNumber] += $parentItem['groupedAmount'][$groupId][$monthNumberPrev];
                    $parentItem['groupedAmountTax'][$groupId][$monthNumber] += $parentItem['groupedAmountTax'][$groupId][$monthNumberPrev];
                }
            } else {
                // january current + december prev
                if (isset($this->resultByCompany[$companyId][$year - 1])) {
                    if ($prevUndestributedProfit = $this->resultByCompany[$companyId][$year - 1]['undestributedProfits'] ?? null) {
                        $parentItem['amount'][$monthNumber] += $prevUndestributedProfit['amount']['total'] ?? 0;
                        $parentItem['amountTax'][$monthNumber] += $prevUndestributedProfit['amountTax']['total'] ?? 0;

                        // GROUPED BY MONTH
                        foreach ($groupNames as $groupId => $groupName) {
                            $parentItem['groupedAmount'][$groupId][$monthNumber] += $prevUndestributedProfit['groupedAmount'][$groupId]['total'] ?? 0;
                            $parentItem['groupedAmountTax'][$groupId][$monthNumber] += $prevUndestributedProfit['groupedAmountTax'][$groupId]['total'] ?? 0;
                        }
                    }
                }
            }
        }

        // floor quarters/total
        foreach ([1 => "03", 2 => "06", 3 => "09", 4 => "12"] as $q => $m) {
            $quarterNumber = 'quarter-' . $q;
            $parentItem['amount'][$quarterNumber] = $parentItem['amount'][$m];
            $parentItem['amountTax'][$quarterNumber] = $parentItem['amountTax'][$m];

            // GROUPED BY MONTH
            foreach ($groupNames as $groupId => $groupName) {
                $parentItem['groupedAmount'][$groupId][$quarterNumber] = $parentItem['groupedAmount'][$groupId][$m];
                $parentItem['groupedAmountTax'][$groupId][$quarterNumber] = $parentItem['groupedAmountTax'][$groupId][$m];
            }
        }
        $parentItem['amount']['total'] = $parentItem['amount']["12"];
        $parentItem['amountTax']['total'] = $parentItem['amountTax']["12"];
        // GROUPED BY MONTH
        foreach ($groupNames as $groupId => $groupName) {
            $parentItem['groupedAmount'][$groupId]['total'] = $parentItem['groupedAmount'][$groupId]['12'];
            $parentItem['groupedAmountTax'][$groupId]['total'] = $parentItem['groupedAmountTax'][$groupId]['12'];
        }

        $this->data[$cachePrefix . $year]['undestributedProfits'] = $parentItem;

        return $this->data[$cachePrefix . $year];
    }
}