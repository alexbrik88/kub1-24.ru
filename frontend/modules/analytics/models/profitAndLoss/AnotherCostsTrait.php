<?php

namespace frontend\modules\analytics\models\profitAndLoss;

use common\models\cash\CashFlowsBase as Cash;
use frontend\modules\analytics\models\AnalyticsArticle as Article;
use frontend\modules\analytics\models\ProfitAndLossCompanyItem as Item;

trait AnotherCostsTrait {

    /**
     * @param $parentItem
     * @param $year
     * @return mixed
     */
    public function getAnotherCosts($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__TRAIT__, $companyId);

        if (!array_key_exists($cachePrefix . $year, $this->data)) {

            $docs = $this->rawDocs[$companyId][Article::PAL_EXPENSES_OTHER] ?? [];
            $flows = $this->rawFlows[$companyId][Article::PAL_EXPENSES_OTHER] ?? [];

            foreach ([$year] as $byYear) {

                $parent = $parentItem;
                $result = [];

                foreach ($docs as $doc) {
                    if ($byYear == $doc['y']) {
                        self::_addToResult($doc, $result, Cash::FLOW_TYPE_EXPENSE, self::MOVEMENT_TYPE_DOCS_AND_FLOWS, Item::OTHER_EXPENSE_TYPE);
                        self::_addToParent($doc, $parent);
                    }
                }

                foreach ($flows as $flow) {
                    if ($byYear == $flow['y']) {
                        self::_addToResult($flow, $result, Cash::FLOW_TYPE_EXPENSE, self::MOVEMENT_TYPE_DOCS_AND_FLOWS, Item::OTHER_EXPENSE_TYPE);
                        self::_addToParent($flow, $parent);
                    }
                }

                $this->_addExchangeDifferenceToResult($parent, $result, 'expense');

                self::_sortResult($result);
                $result = ['totalAnotherCosts' => $parent] + $result;
                $this->data[$cachePrefix . $byYear] = $this->calculateQuarterAndTotalAmount($result);
            }
        }

        return $this->data[$cachePrefix . $year];
    }
}