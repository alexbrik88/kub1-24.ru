<?php

namespace frontend\modules\analytics\models\profitAndLoss;

use Yii;
use common\models\cash\CashFlowsBase;
use common\models\product\Product;
use frontend\modules\analytics\models\AnalyticsArticle as Article;
use frontend\modules\cash\models\CashContractorType;
use common\models\Contractor;
use frontend\modules\analytics\models\PlanCashFlows;

trait ChartsTrait {

    private static $_byRevenueArticles;

    /**
     * @param $dateFrom
     * @param $dateTo
     * @param string $customAmountKey
     * @return array
     */
    public function getChartRevenueByClients($dateFrom, $dateTo, $customAmountKey = 'amount')
    {
        $result = [];

        if (!strtotime($dateFrom) || !strtotime($dateTo))
            return [['name' => 'Период не определен', 'amountFact' => 0, 'amountPlan' => 0]];

        $fact = $this->_getChartRevenueByClientsFact($dateFrom, $dateTo);
        $plan = $this->_getChartRevenueByClientsPlan($dateFrom, $dateTo);

        foreach ($fact as $c => $r) {
            $result[$c] = [
                'name' => ($contractor = Contractor::findOne($c)) ? $contractor->getTitle(true) : '--',
                'amountFact' => round ($r[$customAmountKey] / 100, 2),
                'amountPlan' => 0
            ];
        }

        foreach ($plan as $c => $r) {
            if (!isset($result[$c])) {
                $result[$c] = [
                    'name' => ($contractor = Contractor::findOne($c)) ? $contractor->getTitle(true) : '--',
                    'amountFact' => 0,
                    'amountPlan' => 0
                ];
            }

            $result[$c]['amountPlan'] += round($r[$customAmountKey] / 100, 2);
        }

        uasort($result, function ($a, $b) {
            if ($a['amountFact'] == $b['amountFact']) return 0;
            return ($a['amountFact'] > $b['amountFact']) ? -1 : 1;
        });

        foreach ($result as &$res)
            if ($res['amountFact'] == 0 && $res['amountPlan'] == 0)
                unset($res);

        return $result ?: [['name' => 'Нет данных', 'amountFact' => 0, 'amountPlan' => 0]];
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @param string $customAmountKey
     * @return array
     */
    public function getChartMarginByProducts($dateFrom, $dateTo, $customAmountKey = 'amount')
    {
        $result = [];

        $fact = $this->_getChartMarginByProducts($dateFrom, $dateTo);

        $customSellKey  = ('amount' == $customAmountKey) ? '_sell'  : '_sellTax';
        $customBuyKey  = ('amount' == $customAmountKey) ? '_buy'  : '_buyTax';
        $customQuantityKey = ('amount' == $customAmountKey) ? 'quantity' : 'quantityTax';

        foreach ($fact as $p => $r) {
            $result[$p] = [
                'name' => $r['title'] ?: '--',
                'margin' => round ($r[$customAmountKey] / 100, 2),
                'quantity' => (float)$r[$customQuantityKey],
                'marginPercent' =>  ($r[$customBuyKey]) > 0 ?
                    round (100 * ((float)$r[$customSellKey] - (float)$r[$customBuyKey]) / (float)$r[$customBuyKey], 0) : "-"
            ];
        }

        uasort($result, function ($a, $b) {
            if ($a['margin'] == $b['margin']) return 0;
            return ($a['margin'] > $b['margin']) ? -1 : 1;
        });

        foreach ($result as &$res)
            if ($res['margin'] == 0)
                unset($res);

        return $result ?: [['name' => 'Нет данных', 'margin' => 0, 'quantity' => 0]];
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @return array
     */
    private function _getChartRevenueByClientsFact($dateFrom, $dateTo)
    {
        $result = [];

        $olapDocs = self::TABLE_OLAP_DOCS;
        $olapFlows = self::TABLE_OLAP_FLOWS;
        $multiCompanyIds = implode(',', $this->multiCompanyIds) ?: [-1];
        $byRevenueArticles = self::_getByRevenueArticles() ?: [-1];
        $withoutNds = (bool)$this->getNdsGroupBy();
        $AND_HIDE_TIN_PARENT = $this->getSqlHideTinParent();
        $AND_FILTERS = $this->getSqlFilters();

        //////////// BY NO DOC ////////////
        ///  1) есть привязка к счетам, у которых НЕТ Актов/ТН/УПД или у них стоит "Без Акта/ТН/УПД" и "Дата признания дохода" в нужном месяце +
        ///  2) нет привязки к счетам и "Дата признания дохода" нужном месяце
        $queryFlows = "
            SELECT
              t.contractor_id,
              SUM(IF(t.has_tin_parent, t.tin_child_amount, t.amount)) amount,
              SUM(IF(t.is_accounting, IF(t.has_tin_parent, t.tin_child_amount, t.amount), 0)) amountTax
            FROM {$olapFlows} t
            WHERE  t.company_id IN ({$multiCompanyIds})
              AND (t.has_invoice = FALSE OR t.has_doc = FALSE OR t.need_doc = FALSE)
              AND (t.recognition_date BETWEEN '{$dateFrom}' AND '{$dateTo}')
              AND (t.item_id IN ({$byRevenueArticles}))
              AND (t.type = 1)
              {$AND_HIDE_TIN_PARENT}
              {$AND_FILTERS}
            GROUP BY contractor_id
        ";

        //////////// BY DOC ////////////
        /// сумма всех Актов, ТН и УПД, дата которых в нужном месяце
        $queryDocs = "
            SELECT
              t.contractor_id,
              SUM(".($withoutNds ? 't.amount - t.amount_nds' : 't.amount').") amount,
              SUM(IF(t.is_accounting, ".($withoutNds ? 't.amount - t.amount_nds' : 't.amount').", 0)) amountTax
            FROM {$olapDocs} t
            WHERE  t.company_id IN ({$multiCompanyIds})
              AND (t.date BETWEEN '{$dateFrom}' AND '{$dateTo}')
              AND (t.invoice_income_item_id IN ({$byRevenueArticles}))            
              AND (t.type = 2)
              {$AND_FILTERS}
            GROUP BY contractor_id
        ";

        try {
            $flows = \Yii::$app->db->createCommand($queryFlows)->queryAll();
            $docs = \Yii::$app->db->createCommand($queryDocs)->queryAll();
        } catch (\Throwable $e) {
            Yii::error(__METHOD__."\n".$e->getMessage(), 'analytics');
            $flows = [];
            $docs = [];
        }

        foreach ($flows as $res) {
            $contractorId = $res['contractor_id'];
            if (!isset($result[$contractorId]))
                $result[$contractorId] = [
                    'amount' => 0,
                    'amountTax' => 0
                ];
            $result[$contractorId]['amount'] += ($withoutNds ? 100/120 : 1) * $res['amount'];
            $result[$contractorId]['amountTax'] += ($withoutNds ? 100/120 : 1) * $res['amountTax'];
        }

        foreach ($docs as $res) {
            $contractorId = $res['contractor_id'];
            if (!isset($result[$contractorId]))
                $result[$contractorId] = [
                    'amount' => 0,
                    'amountTax' => 0
                ];
            $result[$contractorId]['amount'] += $res['amount'];
            $result[$contractorId]['amountTax'] += $res['amountTax'];
        }

        return $result;
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @return array
     */
    private function _getChartRevenueByClientsPlan($dateFrom, $dateTo)
    {
        $result = [];

        $exceptContractors = [
            CashContractorType::BANK_TEXT,
            CashContractorType::ORDER_TEXT,
            CashContractorType::EMONEY_TEXT,
            CashContractorType::BALANCE_TEXT,
        ];

        $includeRevenueArticles = implode(',', Article::getArticlesBySection($this->multiCompanyIds, Article::FLOW_TYPE_INCOME, Article::PAL_INCOME_REVENUE) ?: [0]);

        $flows = PlanCashFlows::find()
            ->select(['contractor_id', 'income_item_id', 'payment_type', 'amount'])
            ->andWhere(['company_id' => $this->multiCompanyIds])
            ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
            ->andWhere(['between', 'date', $dateFrom, $dateTo])
            ->andWhere(['not', ['contractor_id' => $exceptContractors]])
            ->andWhere(['income_item_id' => $includeRevenueArticles])
            ->asArray()
            ->all(\Yii::$app->db2);

        foreach ($flows as $flow) {
            $contractorId = $flow['contractor_id'];
            if (!isset($result[$contractorId])) {
                $result[$contractorId] = [
                    'amount' => 0,
                    'amountTax' => 0
                ];
            }
            $result[$contractorId]['amount'] += $flow['amount'];

            // amount taxable
            $isContractorAccounting = !in_array($flow['contractor_id'], self::$notAccountingContractors);
            if ($isContractorAccounting || $flow['payment_type'] == PlanCashFlows::PAYMENT_TYPE_BANK) {
                $result[$contractorId]['amountTax'] += $flow['amount'];
            }
        }

        return $result;
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @return array
     */
    private function _getChartMarginByProducts($dateFrom, $dateTo)
    {
        $result = [];
        $byRevenueArticles = self::_getByRevenueArticles() ?: [-1];
        $contractorTable = Contractor::tableName();
        $productTable = Product::tableName();
        $turnoverTable = self::TABLE_PRODUCT_TURNOVER;
        $withoutNds = (bool)$this->getNdsGroupBy();

        $multiCompanyIds = implode(',', $this->multiCompanyIds) ?: [-1];

        //////////// BY DOC ////////////
        /// сумма всех Актов, ТН и УПД, дата которых в нужном месяце
        $queryDocs = "
            SELECT
              t.product_id,
              p.title,
              SUM(IFNULL(t.quantity, 0)) AS quantity,
              SUM(IF(c.not_accounting, 0, 1) * IFNULL(t.quantity, 0)) AS quantityTax,                               
              SUM(IFNULL(".($withoutNds ? 't.total_amount - t.total_amount_nds' : 't.total_amount').", 0)) AS sumSell,
              SUM(IF(c.not_accounting, 0, 1) * IFNULL(".($withoutNds ? 't.total_amount - t.total_amount_nds' : 't.total_amount').", 0)) AS sumSellTax,
              SUM(IFNULL(".($withoutNds ? 't.purchase_price * ((t.total_amount - t.total_amount_nds) / t.total_amount)' : 't.purchase_price').", 0) * t.quantity) AS sumBuy,
              SUM(IF(c.not_accounting, 0, 1) * IFNULL(".($withoutNds ? 't.purchase_price * ((t.total_amount - t.total_amount_nds) / t.total_amount)' : 't.purchase_price').", 0) * t.quantity) AS sumBuyTax
            FROM {$turnoverTable} t
            LEFT JOIN {$contractorTable} c ON c.id = t.contractor_id
            LEFT JOIN {$productTable} p ON p.id = t.product_id
            WHERE  t.company_id IN ({$multiCompanyIds})
              AND (t.date BETWEEN '{$dateFrom}' AND '{$dateTo}')
              AND t.type = 2                  
              AND t.is_document_actual = TRUE
              AND t.is_invoice_actual = TRUE            
            GROUP BY product_id
       ";

        try {
            $productsByDocs = Yii::$app->db->createCommand($queryDocs)->queryAll();
            $productsByFlows = []; // todo
        } catch (\Throwable $e) {
            Yii::error(__METHOD__."\n".$e->getMessage(), 'analytics');
            $productsByDocs = [];
            $productsByFlows = [];
        }

        foreach (array_merge($productsByDocs, $productsByFlows) as $res) {

            $productId = $res['product_id'];

            if (!isset($result[$productId]))
                $result[$productId] = [
                    'title' => $res['title'],
                    'amount' => 0,
                    'amountTax' => 0,
                    'quantity' => 0,
                    'quantityTax' => 0,
                    '_sell' => 0,
                    '_sellTax' => 0,
                    '_buy' => 0,
                    '_buyTax' => 0,
                ];

            $sellPrice = $res['sumSell'];
            $buyPrice  = $res['sumBuy'];
            $quantity  = $res['quantity'];
            $result[$productId]['amount'] += $sellPrice - $buyPrice;
            $result[$productId]['quantity'] += $quantity;
            $result[$productId]['_sell'] += $sellPrice;
            $result[$productId]['_buy'] += $buyPrice;

            $sellPriceTax = $res['sumSellTax'];
            $buyPriceTax  = $res['sumBuyTax'];
            $quantityTax  = $res['quantityTax'];
            $result[$productId]['amountTax'] += $sellPriceTax - $buyPriceTax;
            $result[$productId]['quantityTax'] += $quantityTax;
            $result[$productId]['_sellTax'] += $sellPriceTax;
            $result[$productId]['_buyTax'] += $buyPriceTax;
        }

        return $result;
    }

    /**
     * @return mixed|string
     */
    private function _getByRevenueArticles()
    {
        if (self::$_byRevenueArticles === null) {
            self::$_byRevenueArticles = implode(',', Article::getArticlesBySection($this->multiCompanyIds, Article::FLOW_TYPE_INCOME, Article::PAL_INCOME_REVENUE));
        }

        return self::$_byRevenueArticles;
    }
}