<?php

namespace frontend\modules\analytics\models\profitAndLoss;

use common\models\cash\CashFlowsBase;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use common\models\Contractor;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\project\Project;
use frontend\models\Documents;
use common\modules\acquiring\models\AcquiringOperation;
use frontend\modules\analytics\models\AnalyticsArticle as Article;
use frontend\modules\analytics\models\AnalyticsArticleForm as Form;
use frontend\modules\cash\models\CashContractorType;
use yii\base\Model;

/// docs: сумма всех Актов, ТН и УПД, дата которых в нужном месяце
/// flows: есть привязка к счетам, у которых НЕТ Актов/ТН/УПД или у них стоит "Без Акта/ТН/УПД" и "Дата признания дохода" в нужном месяце +
/// flows: нет привязки к счетам и "Дата признания дохода" нужном месяце

trait RevenueTrait {

    /**
     * @param $parentItem
     * @param $year
     * @return mixed
     */
    public function getRevenue($parentItem, $year, $companyId)
    {
        switch ($this->userOptions['revenueRuleGroup']) {
            case Form::REVENUE_RULE_GROUP_INDUSTRY:
            case Form::REVENUE_RULE_GROUP_SALE_POINT:
            case Form::REVENUE_RULE_GROUP_PROJECT:
            case Form::REVENUE_RULE_GROUP_CUSTOMER:
                return $this->_getRevenueDetalized($parentItem, $year, $companyId);
            case Form::REVENUE_RULE_GROUP_PRODUCT_GROUP:
            case Form::REVENUE_RULE_GROUP_PRODUCT:
                return $this->_getRevenueDetalizedByProduct($parentItem, $year, $companyId);
            case Form::REVENUE_RULE_GROUP_UNSET:
            default:
                return $this->_getRevenue($parentItem, $year, $companyId);
        }
    }

    private function _getRevenue($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__TRAIT__, $companyId);
        $groupedBy = $this->getMonthGroupBy();

        if (!array_key_exists($cachePrefix . $year, $this->data)) {

            $docs = $this->rawDocs[$companyId][Article::PAL_INCOME_REVENUE] ?? [];
            $flows = $this->rawFlows[$companyId][Article::PAL_INCOME_REVENUE] ?? [];

            foreach ([$year] as $byYear) {

                $parent = $parentItem;
                $result = [
                    'revenueByDocs' => self::_getBlankItem('revenue', 'Выручка по документам', null, 'income-revenue_doc', self::MOVEMENT_TYPE_DOCS),
                    'revenueByNoDocs' => self::_getBlankItem('revenue', 'Выручка без документов', null, 'income-revenue_flow', self::MOVEMENT_TYPE_FLOWS)
                ];

                // docs
                foreach ($docs as $value) {
                    if ($byYear == $value['y']) {
                        $month = $value['m'];
                        foreach (['amount', 'amountTax'] as $amountKey) {
                            $result["revenueByDocs"][$amountKey][$month] += $value[$amountKey];
                        }
                        // GROUPED BY MONTH
                        if ($groupedBy) {
                            foreach (['amount' => 'groupedAmount', 'amountTax' => 'groupedAmountTax'] as $amountKey => $groupedAmountKey) {
                                $result["revenueByDocs"][$groupedAmountKey][$value[$groupedBy]][$month] += $value[$amountKey];
                            }
                        }
                        self::_addToParent($value, $parent);
                    }
                }

                // flows
                foreach ($flows as $value) {
                    if ($byYear == $value['y']) {
                        $month = $value['m'];
                        foreach (['amount', 'amountTax'] as $amountKey) {
                            $result["revenueByNoDocs"][$amountKey][$month] += $value[$amountKey];
                        }
                        // GROUPED BY MONTH
                        if ($groupedBy) {
                            foreach (['amount' => 'groupedAmount', 'amountTax' => 'groupedAmountTax'] as $amountKey => $groupedAmountKey) {
                                $result["revenueByNoDocs"][$groupedAmountKey][$value[$groupedBy]][$month] += $value[$amountKey];
                            }
                        }
                        self::_addToParent($value, $parent);
                    }
                }

                $result = ['totalRevenue' => $parent] + $result;

                $this->data[$cachePrefix . $byYear] = $this->calculateQuarterAndTotalAmount($result);
            }
        }

        return $this->data[$cachePrefix . $year];
    }

    private function _getRevenueDetalized($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__TRAIT__, $companyId);
        $monthGroupBy = $this->getMonthGroupBy();
        $revenueGroupBy = $this->getRevenueGroupBy();

        if (!array_key_exists($cachePrefix . $year, $this->data)) {

            $docsGrouped = $this->rawDocsRevenue[$companyId] ?? [];
            $flowsGrouped = $this->rawFlowsRevenue[$companyId] ?? [];

            foreach ([$year] as $byYear) {

                $parent = $parentItem;
                $result = [];

                // docs
                foreach ($docsGrouped as $itemId => $docs) {
                    $itemKey = "revenue_{$itemId}";
                    if (!isset($result[$itemKey])) {
                        $itemName = $this->_getItemName($itemId);
                        $result[$itemKey] = self::_getBlankItem('revenue', $itemName, null, "income-{$revenueGroupBy}-{$itemId}", self::MOVEMENT_TYPE_DOCS_AND_FLOWS);
                    }

                    foreach ($docs as $value) {
                        if ($byYear == $value['y']) {
                            $month = $value['m'];
                            foreach (['amount', 'amountTax'] as $amountKey) {
                                $result[$itemKey][$amountKey][$month] += $value[$amountKey];
                            }
                            // GROUPED BY MONTH
                            if ($monthGroupBy) {
                                foreach (['amount' => 'groupedAmount', 'amountTax' => 'groupedAmountTax'] as $amountKey => $groupedAmountKey) {
                                    $result[$itemKey][$groupedAmountKey][$value[$monthGroupBy]][$month] += $value[$amountKey];
                                }
                            }
                            self::_addToParent($value, $parent);
                        }
                    }
                }

                // flows
                foreach ($flowsGrouped as $itemId => $flows) {
                    $itemKey = "revenue_{$itemId}";
                    if (!isset($result[$itemKey])) {
                        $itemName = $this->_getItemName($itemId);
                        $result[$itemKey] = self::_getBlankItem('revenue', $itemName, null, "income-{$revenueGroupBy}-{$itemId}", self::MOVEMENT_TYPE_DOCS_AND_FLOWS);
                    }

                    foreach ($flows as $value) {
                        if ($byYear == $value['y']) {
                            $month = $value['m'];
                            foreach (['amount', 'amountTax'] as $amountKey) {
                                $result[$itemKey][$amountKey][$month] += $value[$amountKey];
                            }
                            // GROUPED BY MONTH
                            if ($monthGroupBy) {
                                foreach (['amount' => 'groupedAmount', 'amountTax' => 'groupedAmountTax'] as $amountKey => $groupedAmountKey) {
                                    $result[$itemKey][$groupedAmountKey][$value[$monthGroupBy]][$month] += $value[$amountKey];
                                }
                            }
                            self::_addToParent($value, $parent);
                        }
                    }
                }

                self::_sortResultByYear($result);
                // todo: переместить сортировку по пред. месяцу после склеивания в "Остальные"
                //if ($byYear == date('Y')) {
                //    self::_sortResultByMonth($result);
                //} else {
                //    self::_sortResultByYear($result);
                //}

                $result = ['totalRevenue' => $parent] + $result;

                $this->data[$cachePrefix . $byYear] = $this->calculateQuarterAndTotalAmount($result);
            }
        }

        return $this->data[$cachePrefix . $year];
    }

    private function _getRevenueDetalizedByProduct($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__TRAIT__, $companyId);
        $revenueGroupBy = $this->getRevenueGroupedByProduct();
        $monthGroupBy = $this->getMonthGroupBy();

        if (!array_key_exists($cachePrefix . $year, $this->data)) {

            $docsGrouped = $this->rawDocsRevenue[$companyId];
            $flowsGrouped = $this->rawFlowsRevenue[$companyId];

            foreach ([$year] as $byYear) {

                $parent = $parentItem;
                $result = [];

                // docs
                foreach ($docsGrouped as $itemId => $docs) {
                    $itemKey = "revenue_{$itemId}";
                    if (!isset($result[$itemKey])) {
                        $itemName = $this->_getItemName($itemId);
                        $result[$itemKey] = self::_getBlankItem('revenue', $itemName, null, "income-{$revenueGroupBy}-{$itemId}", self::MOVEMENT_TYPE_DOCS);
                    }

                    foreach ($docs as $value) {
                        if ($byYear == $value['y']) {
                            $month = $value['m'];
                            foreach (['amount', 'amountTax'] as $amountKey) {
                                $result[$itemKey][$amountKey][$month] += $value[$amountKey];
                            }
                            // GROUPED BY MONTH
                            if ($monthGroupBy) {
                                foreach (['amount' => 'groupedAmount', 'amountTax' => 'groupedAmountTax'] as $amountKey => $groupedAmountKey) {
                                    $result[$itemKey][$groupedAmountKey][$value[$monthGroupBy]][$month] += $value[$amountKey];
                                }
                            }
                            self::_addToParent($value, $parent);
                        }
                    }
                }

                self::_sortResultByYear($result);

                // flows
                $result['revenueByNoDocs'] = self::_getBlankItem('revenue', 'Выручка без документов', null, 'income-revenue_flow', self::MOVEMENT_TYPE_FLOWS);
                foreach ($flowsGrouped as $itemId => $flows) {
                    $itemKey = 'revenueByNoDocs';
                    foreach ($flows as $value) {
                        if ($byYear == $value['y']) {
                            $month = $value['m'];
                            foreach (['amount', 'amountTax'] as $amountKey) {
                                $result[$itemKey][$amountKey][$month] += $value[$amountKey];
                                $parent[$amountKey][$month] += $value[$amountKey];
                            }
                        }
                    }
                }

                $result = ['totalRevenue' => $parent] + $result;

                $this->data[$cachePrefix . $byYear] = $this->calculateQuarterAndTotalAmount($result);
            }
        }

        return $this->data[$cachePrefix . $year];
    }

    private function _getItemName($itemId)
    {
        switch ($this->userOptions['revenueRuleGroup']) {
            case Form::REVENUE_RULE_GROUP_INDUSTRY:
                return ($item = CompanyIndustry::findOne([$itemId])) ? $item->name : 'Без направления';
            case Form::REVENUE_RULE_GROUP_SALE_POINT:
                return ($item = SalePoint::findOne([$itemId])) ? $item->name : 'Без точки продаж';
            case Form::REVENUE_RULE_GROUP_CUSTOMER:
                return ($item = Contractor::findOne([$itemId])) ? $item->getShortName() : (
                ($cashItem = CashContractorType::findOne(['name' => $itemId])) ? $cashItem->text : 'Без контрагента');
            case Form::REVENUE_RULE_GROUP_PROJECT:
                return ($item = Project::findOne([$itemId])) ? $item->name : 'Без проекта';
            case Form::REVENUE_RULE_GROUP_PRODUCT_GROUP:
                return ($item = ProductGroup::findOne([$itemId])) ? $item->title : 'Без группы';
            case Form::REVENUE_RULE_GROUP_PRODUCT:
                return ($item = Product::findOne([$itemId])) ? $item->title : 'Без продукта';
            case Form::REVENUE_RULE_GROUP_UNSET:
            default:
                return '---';
        }
    }
}