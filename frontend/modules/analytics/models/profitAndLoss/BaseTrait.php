<?php

namespace frontend\modules\analytics\models\profitAndLoss;

use common\models\document\GoodsCancellation;
use common\models\document\Invoice;
use common\models\product\ProductTurnover;
use frontend\models\Documents;
use frontend\modules\analytics\models\AnalyticsArticle;
use frontend\modules\analytics\models\AnalyticsArticle as Article;
use frontend\modules\analytics\models\AnalyticsArticleForm as Form;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\currency\CurrencyRate;
use Yii;
use common\models\cash\CashFlowsBase as Cash;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;

trait BaseTrait {

    public function getNdsGroupBy()
    {
        return ($this->userOptions['calcWithoutNds']) 
            ? 'contractor_id'
            : '';
    }

    public function getMonthGroupBy()
    {
        switch ($this->userOptions['monthGroupBy']) {
            case Form::MONTH_GROUP_PROJECT:
                return 'project_id';
            case Form::MONTH_GROUP_SALE_POINT:
                return 'sale_point_id';
            case Form::MONTH_GROUP_INDUSTRY:
                return 'industry_id';
            case Form::MONTH_GROUP_UNSET:
            default:
                return '';
        }
    }

    public function getRevenueGroupBy()
    {
        switch ($this->userOptions['revenueRuleGroup']) {
            case Form::REVENUE_RULE_GROUP_INDUSTRY:
                return 'industry_id';
            case Form::REVENUE_RULE_GROUP_SALE_POINT:
                return 'sale_point_id';
            case Form::REVENUE_RULE_GROUP_PROJECT:
                return 'project_id';
            case Form::REVENUE_RULE_GROUP_CUSTOMER:
                return 'contractor_id';
            case Form::REVENUE_RULE_GROUP_UNSET:
            case Form::REVENUE_RULE_GROUP_PRODUCT: // get data from product_turnover
            case Form::REVENUE_RULE_GROUP_PRODUCT_GROUP: // get data from product_turnover
            default:
               return '';
        }
    }

    public function getRevenueGroupedByProduct()
    {
        return ($this->userOptions['revenueRuleGroup'] == Form::REVENUE_RULE_GROUP_PRODUCT_GROUP)
            ? 'product_group_id'
            : 'product_id';
    }

    private function getSqlFilters()
    {
        $ret = '';
        $params = &$this->pageFilters;
        foreach ($params as $attr => $value) {
            if (in_array($attr, ['project_id', 'industry_id', 'sale_point_id'])) {
                if (strlen($value)) {
                    $value = (int)$value;
                    if ($value > 0)
                        $ret .= " AND t.{$attr} = '{$value}'";
                    elseif ($value === 0)
                        $ret .= " AND t.{$attr} IS NULL";
                }
            }
        }

        return $ret;
    }

    private function initRawFlows($companyId, $year)
    {
        $olapTable = self::TABLE_OLAP_FLOWS;
        $articlesTable = Article::tableName();
        $revenueGroupBy = $this->getRevenueGroupBy();
        $monthGroupBy = $this->getMonthGroupBy();
        $ndsGroupBy = $this->getNdsGroupBy();
        $AND_HIDE_TIN_PARENT = $this->getSqlHideTinParent();
        $AND_FILTERS = $this->getSqlFilters();
        $tplQuery = "
            SELECT
              t.type,
              a.profit_and_loss_type AS pal_type,
              t.item_id,
              IFNULL(t.contractor_id, 0) AS contractor_id,
              IFNULL(t.project_id, 0) AS project_id, 
              IFNULL(t.sale_point_id, 0) AS sale_point_id, 
              IFNULL(t.industry_id, 0) AS industry_id,
              DATE_FORMAT(t.recognition_date, '%m') m,
              DATE_FORMAT(t.recognition_date, '%Y') y,
              SUM(IF(t.has_tin_parent, t.tin_child_amount, t.amount)) amount,
              SUM(IF(t.is_accounting, IF(t.has_tin_parent, t.tin_child_amount, t.amount), 0)) amountTax
            FROM {$olapTable} t
            LEFT JOIN {$articlesTable} a ON t.company_id = a.company_id AND t.type = a.type AND t.item_id = a.item_id
            WHERE  t.company_id = %%COMPANY_ID%%
              AND (t.recognition_date BETWEEN '{$year}-01-01' AND '{$year}-12-31')                   
              AND (t.has_invoice = FALSE OR t.has_doc = FALSE OR t.need_doc = FALSE)
              AND (t.is_prepaid_expense = 0)
              {$AND_HIDE_TIN_PARENT}
              {$AND_FILTERS}
            GROUP BY ".($monthGroupBy ?: 'NULL').", ".($revenueGroupBy ?: 'NULL').", ".($ndsGroupBy ?: 'NULL').", t.type, t.item_id, y, m;
        ";

        if ($this->userOptions['skipPrepayments']) {
            $rows = [];
        } else {
            $query = str_replace('%%COMPANY_ID%%', $companyId, $tplQuery);
            $rows = Yii::$app->db2->createCommand($query)->queryAll();
        }

        $this->rawFlows[$companyId] = [
            Article::PAL_UNSET => [],
            Article::PAL_EXPENSES_VARIABLE => [],
            Article::PAL_EXPENSES_FIXED => [],
            Article::PAL_EXPENSES_OTHER => [],
            Article::PAL_EXPENSES_PERCENT => [],
            Article::PAL_EXPENSES_DIVIDEND => [],
            Article::PAL_EXPENSES_PRIME_COST => [],
            Article::PAL_EXPENSES_AMORTIZATION => [],
            Article::PAL_INCOME_REVENUE => [],
            Article::PAL_INCOME_OTHER => [],
            Article::PAL_INCOME_PERCENT => [],
        ];
        $this->rawFlowsRevenue[$companyId] = [];

        if ($ndsGroupBy) {
            foreach ($rows as $k => $r) {
                if ($r['pal_type'] == Article::PAL_INCOME_REVENUE || in_array($r['contractor_id'], self::$ndsContractors)) {
                    $rows[$k]['amount'] = 1 / 1.2 * $rows[$k]['amount'];
                    $rows[$k]['amountTax'] = 1 / 1.2 * $rows[$k]['amountTax'];
                }
            }
        }

        foreach ($rows as $row) {
            $this->rawFlows[$companyId][$row['pal_type']][] = $row;
            if ($revenueGroupBy && $row['pal_type'] == Article::PAL_INCOME_REVENUE) {
                $this->rawFlowsRevenue[$companyId][$row[$revenueGroupBy]][] = $row;
            }
        }
    }

    private function initExchangeDifference($companyId, $year)
    {
        $monthGroupBy = $this->getMonthGroupBy();
        $groupBySql = ($monthGroupBy)
            ? "{$monthGroupBy}, recognition_date, currency_name, type"
            : "recognition_date, currency_name, type";

        $dateStart = date_create_from_format('Y-m-d|', $year.'-01-01');
        if (!$dateStart) {
            return;
        }
        $dateBefore = (clone $dateStart)->modify('-1 days');
        $dateEnd = date_create_from_format('Y-m-d|', $year.'-12-31');

        $query = (new \yii\db\Query)->from([
            't' => self::TABLE_OLAP_FLOWS,
        ])->select([
            'currency_name' => 't.currency_name',
            'recognition_date' => "DATE(IF(t.recognition_date < '{$dateStart->format('Y-m-d')}', '{$dateBefore->format('Y-m-d')}', t.recognition_date))",
            'type' => 't.type',
            'original_amount' => 'SUM(t.original_amount)',
            'industry_id' => 'IFNULL(t.industry_id, 0)',
            'sale_point_id' => 'IFNULL(t.sale_point_id, 0)',
            'project_id' => 'IFNULL(t.project_id, 0)',
        ])->andWhere([
            'company_id' => $companyId,
        ])->andWhere([
            '<=', 't.recognition_date', $dateEnd->format('Y-m-d'),
        ])->andWhere([
            '<>', 't.currency_name', Currency::DEFAULT_NAME,
        ])->groupBy($groupBySql)->orderBy([
            'recognition_date' => SORT_ASC,
        ]);

        // pageFilters
        if (strlen($this->pageFilters['industry_id'] ?? null)) {
            $query->andWhere(['industry_id' => $this->pageFilters['industry_id'] ?: null]);
        }
        if (strlen($this->pageFilters['sale_point_id'] ?? null)) {
            $query->andWhere(['sale_point_id' => $this->pageFilters['sale_point_id'] ?: null]);
        }
        if (strlen($this->pageFilters['project_id'] ?? null)) {
            $query->andWhere(['project_id' => $this->pageFilters['project_id'] ?: null]);
        }

        if ($this->activeTab == self::TAB_ACCOUNTING_OPERATIONS_ONLY) {
            $query->andWhere([
                't.is_accounting' => true,
            ]);
        }

        $flowData = [];
        $groupedFlowData = [];
        $emptyValues = [];
        while ($dateStart <= $dateEnd) {
            $emptyValues[$dateStart->format('md')] = 0;
            $dateStart->modify('+1 days');
        }

        foreach ($query->all() as $key => $row) {
            list($y, $m, $d) = explode('-', $row['recognition_date']);
            $currency = $row['currency_name'];
            $type = $row['type'];
            if (!isset($flowData[$currency])) {
                $flowData[$currency] = [0 => 0] + $emptyValues;
            }
            $period = ($y < $year) ? 0 : $m.$d;
            $flowData[$currency][$period] += Cash::FLOW_TYPE_EXPENSE == $type ? $row['original_amount'] : -$row['original_amount'];

            // GROUPED
            if ($monthGroupBy) {
                $groupBy = $row[$monthGroupBy];
                if (!isset($groupedFlowData[$groupBy][$currency])) {
                    $groupedFlowData[$groupBy][$currency] = [0 => 0] + $emptyValues;
                }
                $groupedFlowData[$groupBy][$currency][$period] += Cash::FLOW_TYPE_EXPENSE == $type ? $row['original_amount'] : -$row['original_amount'];
            }
        }

        $lastP = 0;
        $noRate = [
            'amount' => 1,
            'value' => 1,
        ];
        $balanceData = [];
        $difference = [];
        $rateData = [];
        foreach ($emptyValues as $key => $value) {
            $date = date_create_from_format('Ymd', $year.$key);
            $rateData[$key] = (array) ($date ? CurrencyRate::getRateOnDate($date) : []);
        }

        $groupedBalanceData = [];
        $groupedDifference = [];

        $currentPeriod = date('md');
        $currentYear = date('Y');
        foreach ($rateData as $period => $rateList) {
            $isValidPeriod = $year <= $currentYear && $period <= $currentPeriod;
            $lastRateList = $lastP == 0 ? $rateList : $rateData[$lastP];
            foreach ($flowData as $currency => $cData) {
                if ($isValidPeriod) {
                    $rate = $rateList[$currency] ?? $noRate;
                    $lastRate = $lastRateList[$currency] ?? $noRate;

                    if ($lastP === 0) {
                        $balanceData[$currency][$lastP] = $cData[$lastP];
                    }

                    $balanceData[$currency][$period] = $balanceData[$currency][$lastP] + $cData[$period];

                    if ($balanceData[$currency][$lastP] == 0 || ($rate['amount'] == $lastRate['amount'] && $rate['value'] == $lastRate['value'])) {
                        $difference[$currency][$period] = 0;
                    } else {
                        $difference[$currency][$period] = (int) round(
                            ($balanceData[$currency][$lastP]/$rate['amount']*$rate['value'])
                            -
                            ($balanceData[$currency][$lastP]/$lastRate['amount']*$lastRate['value'])
                        );
                    }
                } else {
                    $difference[$currency][$period] = 0;
                }
            }

            // GROUPED
            if ($monthGroupBy) {
                foreach ($groupedFlowData as $groupId => $groupFlowData) {
                    foreach ($groupFlowData as $currency => $cData) {
                        if ($isValidPeriod) {
                            $rate = $rateList[$currency] ?? $noRate;
                            $lastRate = $lastRateList[$currency] ?? $noRate;

                            if ($lastP === 0) {
                                $groupedBalanceData[$groupId][$currency][$lastP] = $cData[$lastP];
                            }

                            $groupedBalanceData[$groupId][$currency][$period] = $groupedBalanceData[$groupId][$currency][$lastP] + $cData[$period];

                            if ($groupedBalanceData[$groupId][$currency][$lastP] == 0 || ($rate['amount'] == $lastRate['amount'] && $rate['value'] == $lastRate['value'])) {
                                $groupedDifference[$groupId][$currency][$period] = 0;
                            } else {
                                $groupedDifference[$groupId][$currency][$period] = (int)round(
                                    ($groupedBalanceData[$groupId][$currency][$lastP] / $rate['amount'] * $rate['value'])
                                    -
                                    ($groupedBalanceData[$groupId][$currency][$lastP] / $lastRate['amount'] * $lastRate['value'])
                                );
                            }
                        } else {
                            $groupedDifference[$groupId][$currency][$period] = 0;
                        }
                    }
                }
            }

            $lastP = $period;
        }

        $differenceData = [];
        foreach ($difference as $currency => $cData) {
            if (!isset($differenceData[$currency])) {
                $differenceData[$currency] = self::_zeroAmountArray();
                $this->exchangeDifference[$currency] = [
                    'income' => self::_zeroAmountArray(),
                    'expense' => self::_zeroAmountArray(),
                ];
            }

            foreach ($cData as $period => $amount) {
                $m = substr($period, 0, 2);
                $q = (int) ceil($m/3);
                $differenceData[$currency][$m] += $amount;
                $differenceData[$currency]['quarter-'.$q] += $amount;
                $differenceData[$currency]['total'] += $amount;
            }
        }

        foreach ($differenceData as $currency => $cData) {
            foreach ($cData as $key => $value) {
                if ($value > 0) {
                    $this->exchangeDifference[$currency]['income'][$key] = $value;
                } elseif ($value < 0) {
                    $this->exchangeDifference[$currency]['expense'][$key] = -$value;
                }
            }
        }


        // GROUPED
        if ($monthGroupBy) {

            $this->exchangeDifference['grouped'] = [];

            $groupedDifferenceData = [];
            foreach ($groupedDifference as $groupId => $groupDiffData)
                foreach ($groupDiffData as $currency => $cData) {
                    if (!isset($groupedDifferenceData[$groupId][$currency])) {
                        $groupedDifferenceData[$groupId][$currency] = self::_zeroAmountArray();
                        $this->exchangeDifference['grouped'][$groupId][$currency] = [
                            'income' => self::_zeroAmountArray(),
                            'expense' => self::_zeroAmountArray(),
                        ];
                    }

                    foreach ($cData as $period => $amount) {
                        $m = substr($period, 0, 2);
                        $q = (int) ceil($m/3);
                        $groupedDifferenceData[$groupId][$currency][$m] += $amount;
                        $groupedDifferenceData[$groupId][$currency]['quarter-'.$q] += $amount;
                        $groupedDifferenceData[$groupId][$currency]['total'] += $amount;
                    }
                }

            foreach ($groupedDifferenceData as $groupId => $groupDiffData) {
                foreach ($groupDiffData as $currency => $cData) {
                    foreach ($cData as $key => $value) {
                        if ($value > 0) {
                            $this->exchangeDifference['grouped'][$groupId][$currency]['income'][$key] = $value;
                        } elseif ($value < 0) {
                            $this->exchangeDifference['grouped'][$groupId][$currency]['expense'][$key] = -$value;
                        }
                    }
                }
            }
        }
    }

    private function initRawDocs($companyId, $year)
    {
        $olapTable = self::TABLE_OLAP_DOCS;
        $articlesTable = Article::tableName();
        $revenueGroupBy = $this->getRevenueGroupBy();
        $monthGroupBy = $this->getMonthGroupBy();
        $withoutNds = (bool)$this->getNdsGroupBy();
        $AND_FILTERS = $this->getSqlFilters();

        $tplQuery = "
            SELECT
              t.type,
              IFNULL(a.profit_and_loss_type, 0) AS pal_type,
              IF(t.type = 2, invoice_income_item_id, invoice_expenditure_item_id) AS item_id,
              IFNULL(t.contractor_id, 0) AS contractor_id,
              IFNULL(t.project_id, 0) AS project_id,                   
              IFNULL(t.sale_point_id, 0) AS sale_point_id,
              IFNULL(t.industry_id, 0) AS industry_id,                   
              DATE_FORMAT(t.date, '%m') m,
              DATE_FORMAT(t.date, '%Y') y,
              SUM(".($withoutNds ? 't.amount - t.amount_nds' : 't.amount').") amount,
              SUM(IF(t.is_accounting, ".($withoutNds ? 't.amount - t.amount_nds' : 't.amount').", 0)) amountTax
            FROM {$olapTable} t
            LEFT JOIN {$articlesTable} a ON t.company_id = a.company_id AND t.type = (1 + a.type) AND IF(t.type = 2, invoice_income_item_id, invoice_expenditure_item_id) = a.item_id            
            WHERE t.company_id = %%COMPANY_ID%%
              AND (t.date BETWEEN '{$year}-01-01' AND '{$year}-12-31')
              {$AND_FILTERS}
            GROUP BY ".($monthGroupBy ?: 'NULL').", ".($revenueGroupBy ?: 'NULL').", t.type, item_id, y, m;
        ";

        //foreach ($this->multiCompanyIds as $companyId) {

        $query = str_replace('%%COMPANY_ID%%', $companyId, $tplQuery);
        $rows = Yii::$app->db2->createCommand($query)->queryAll();

        $this->rawDocs[$companyId] = [
            Article::PAL_UNSET => [],
            Article::PAL_EXPENSES_VARIABLE => [],
            Article::PAL_EXPENSES_FIXED => [],
            Article::PAL_EXPENSES_OTHER => [],
            Article::PAL_EXPENSES_PERCENT => [],
            Article::PAL_EXPENSES_DIVIDEND => [],
            Article::PAL_EXPENSES_PRIME_COST => [],
            Article::PAL_EXPENSES_AMORTIZATION => [],
            Article::PAL_INCOME_REVENUE => [],
            Article::PAL_INCOME_OTHER => [],
            Article::PAL_INCOME_PERCENT => [],
        ];
        $this->rawDocsRevenue[$companyId] = [];

        foreach ($rows as $row) {
            $this->rawDocs[$companyId][$row['pal_type']][] = $row;
            if ($revenueGroupBy && $row['pal_type'] == Article::PAL_INCOME_REVENUE) {
                $this->rawDocsRevenue[$companyId][$row[$revenueGroupBy]][] = $row;
            }
        }
        //}
    }

    private function initPrimeCostsByDocs($companyId, $year)
    {
        $invoiceTable = Invoice::tableName();
        $contractorTable = Contractor::tableName();
        $turnoverTable = ProductTurnover::tableName();
        $itemId = [
            'product' => InvoiceExpenditureItem::ITEM_PRIME_COST_PRODUCT,
            'service' => InvoiceExpenditureItem::ITEM_PRIME_COST_SERVICE
        ];
        $palType = [
            'product' => AnalyticsArticle::PAL_EXPENSES_PRIME_COST,
            'service' => AnalyticsArticle::PAL_EXPENSES_PRIME_COST,
        ];

        $monthGroupBy = $this->getMonthGroupBy();
        $withoutNds = (bool)$this->getNdsGroupBy();
        $AND_FILTERS = $this->getSqlFilters();

        $byRevenueArticles = implode(',', Article::getArticlesBySection($companyId, Article::FLOW_TYPE_INCOME, Article::PAL_INCOME_REVENUE) ?: [0]);

        //////////// BY DOC ////////////
        /// сумма всех Актов, ТН и УПД, дата которых в нужном месяце
        $tplQuery = "
            SELECT
                invoice_id,
              DATE_FORMAT(t.date, '%m') m,
              DATE_FORMAT(t.date, '%Y') y,
              IFNULL(t.project_id, 0) AS project_id, 
              IFNULL(t.sale_point_id, 0) AS sale_point_id, 
              IFNULL(t.industry_id, 0) AS industry_id,                   
              SUM(IFNULL(".($withoutNds ? 't.purchase_amount * ((t.total_amount - t.total_amount_nds) / t.total_amount)' : 't.purchase_amount').", 0)) AS amount,
              SUM(IF(c.not_accounting, 0, 1) * IFNULL(".($withoutNds ? 't.purchase_amount * ((t.total_amount - t.total_amount_nds) / t.total_amount)' : 't.purchase_amount').", 0) * 1) AS amountTax,              
              IF (t.production_type = 1, {$itemId['product']}, {$itemId['service']}) AS item_id
            FROM {$turnoverTable} t 
            LEFT JOIN {$invoiceTable} i ON i.id = t.invoice_id
            LEFT JOIN {$contractorTable} c ON c.id = t.contractor_id
            WHERE t.company_id = %%COMPANY_ID%%
              AND (t.date BETWEEN '{$year}-01-01' AND '{$year}-12-31')            
              AND t.type = 2
              AND t.is_document_actual = TRUE
              AND t.is_invoice_actual = TRUE          
              {$AND_FILTERS}  
              AND i.invoice_income_item_id IN ({$byRevenueArticles})
            GROUP BY ".($monthGroupBy ?: 'NULL').", y, m, t.production_type
       ";

        //foreach ($this->multiCompanyIds as $companyId) {

        $query = str_replace('%%COMPANY_ID%%', $companyId, $tplQuery);
        $rows = Yii::$app->db2->createCommand($query)->queryAll();

        //if ($year == 2021) {
        //    var_dump(Yii::$app->db->createCommand($query)->rawSql);
        //    var_dump($rows);
        //    exit;
        //}

        foreach ($rows as $row) {
            $row['pal_type'] = ($row['item_id'] == $itemId['product']) ? $palType['product'] : $palType['service'];
            $this->rawDocs[$companyId][$row['pal_type']][] = $row;
        }
       //}
    }

    private function initPrimeCostsByFlows($companyId, $year)
    {
        $flowsTable = self::TABLE_OLAP_FLOWS;
        $monthGroupBy = $this->getMonthGroupBy();
        $itemId = [
            'product' => InvoiceExpenditureItem::ITEM_PRIME_COST_PRODUCT,
            'service' => InvoiceExpenditureItem::ITEM_PRIME_COST_SERVICE
        ];
        $palType = [
            'product' => AnalyticsArticle::PAL_EXPENSES_PRIME_COST,
            'service' => AnalyticsArticle::PAL_EXPENSES_PRIME_COST,
        ];
        $AND_FILTERS = $this->getSqlFilters();

        //////////// BY NO DOC ////////////
        ///  1) есть привязка к счетам, у которых НЕТ Актов/ТН/УПД или у них стоит "Без Акта/ТН/УПД" и "Дата признания дохода" в нужном месяце

        //foreach ($this->multiCompanyIds as $companyId) {

            $byRevenueArticles = implode(',', Article::getArticlesBySection($companyId, Article::FLOW_TYPE_INCOME, Article::PAL_INCOME_REVENUE) ?: [0]);

            $subQuery = "
                SELECT 
                  r.invoice_id,
                  MAX(t.recognition_date) recognition_date,
                  IFNULL(t.project_id, 0) AS project_id, 
                  IFNULL(t.sale_point_id, 0) AS sale_point_id, 
                  IFNULL(t.industry_id, 0) AS industry_id,                   
                  MAX(t.is_accounting) is_accounting
                FROM {$flowsTable} t 
                JOIN `cash_bank_flow_to_invoice` r ON r.flow_id = t.id
                JOIN `invoice` i ON r.invoice_id = i.id 
                WHERE t.company_id = {$companyId}
                  AND t.recognition_date BETWEEN '{$year}-01-01' AND '{$year}-12-31'
                  AND t.wallet = 1
                  AND t.type = 1
                  AND t.item_id IN ({$byRevenueArticles})
                  AND t.has_invoice = TRUE AND (t.has_doc = FALSE OR t.need_doc = FALSE)
                  {$AND_FILTERS}
                  GROUP BY r.invoice_id  
                UNION ALL            
                SELECT 
                  r.invoice_id,                       
                  MAX(t.recognition_date) recognition_date,
                  IFNULL(t.project_id, 0) AS project_id, 
                  IFNULL(t.sale_point_id, 0) AS sale_point_id, 
                  IFNULL(t.industry_id, 0) AS industry_id,                   
                  MAX(t.is_accounting) is_accounting
                FROM {$flowsTable} t
                LEFT JOIN `cash_order_flow_to_invoice` r ON r.flow_id = t.id             
                WHERE t.company_id = {$companyId}
                  AND t.recognition_date BETWEEN '{$year}-01-01' AND '{$year}-12-31'                      
                  AND t.wallet = 2
                  AND t.type = 1
                  AND t.item_id IN ({$byRevenueArticles})
                  AND t.has_invoice = TRUE AND (t.has_doc = FALSE OR t.need_doc = FALSE)
                  {$AND_FILTERS}
                  GROUP BY r.invoice_id
                UNION ALL            
                SELECT 
                  MAX(t.recognition_date) recognition_date,
                  r.invoice_id,
                  IFNULL(t.project_id, 0) AS project_id, 
                  IFNULL(t.sale_point_id, 0) AS sale_point_id, 
                  IFNULL(t.industry_id, 0) AS industry_id,
                  MAX(t.is_accounting) is_accounting
                FROM {$flowsTable} t
                LEFT JOIN `cash_emoney_flow_to_invoice` r ON r.flow_id = t.id             
                WHERE t.company_id = {$companyId}
                  AND t.recognition_date BETWEEN '{$year}-01-01' AND '{$year}-12-31'                  
                  AND t.wallet = 3            
                  AND t.type = 1
                  AND t.item_id IN ({$byRevenueArticles})
                  AND t.has_invoice = TRUE AND (t.has_doc = FALSE OR t.need_doc = FALSE)
                  {$AND_FILTERS}
                  GROUP BY r.invoice_id
            ";

            $query = "
              SELECT 
                t.invoice_id, 
                DATE_FORMAT(t.recognition_date, '%m') m,
                DATE_FORMAT(t.recognition_date, '%Y') y,
                t.project_id,
                t.sale_point_id, 
                t.industry_id,              
                SUM(IFNULL(p.price_for_buy_with_nds,0) * o.quantity) AS amount,
                SUM(IF(t.is_accounting, IFNULL(p.price_for_buy_with_nds,0) * o.quantity, 0)) AS amountTax,
                IF (p.production_type = 1, {$itemId['product']}, {$itemId['service']}) AS item_id                     
                FROM ({$subQuery}) t
                LEFT JOIN `order` o ON t.invoice_id = o.invoice_id
                LEFT JOIN `product` p ON p.id = o.product_id
                GROUP BY ".($monthGroupBy ?: 'NULL').", y, m, p.production_type
            ";

            $rows = Yii::$app->db2->createCommand($query)->queryAll();

            if (Yii::$app->request->get('debug')) {
                var_dump(Yii::$app->db2->createCommand($query)->rawSql);
                var_dump($rows);
                exit;
            }

            foreach ($rows as $row) {
                $row['pal_type'] = ($row['item_id'] == $itemId['product']) ? $palType['product'] : $palType['service'];
                $this->rawFlows[$companyId][$row['pal_type']][] = $row;
            }
        //}
    }

    private function initGoodsCancellations($companyId, $year)
    {
        $CONST_TYPE = Documents::IO_TYPE_OUT;
        $CONST_ITEM_ID = InvoiceExpenditureItem::ITEM_GOODS_CANCELLATION;
        $table = GoodsCancellation::tableName();
        $revenueGroupBy = $this->getRevenueGroupBy();
        $monthGroupBy = $this->getMonthGroupBy();
        $withoutNds = (bool)$this->getNdsGroupBy();
        $AND_FILTERS = $this->getSqlFilters();

        if (empty($this->rawDocs))
            return;

        $tplQuery = "
            SELECT
              {$CONST_TYPE} AS type,
              {$CONST_ITEM_ID} AS item_id,
              t.contractor_id,
              IFNULL(t.project_id, 0) AS project_id,
              IFNULL(t.sale_point_id, 0) AS sale_point_id,
              IFNULL(t.industry_id, 0) AS industry_id,
              CASE
                  WHEN t.profit_loss_type = " . GoodsCancellation::PRIME_COST_EXPENSE_TYPE . " THEN " . Article::PAL_EXPENSES_PRIME_COST . "
                  WHEN t.profit_loss_type = " . GoodsCancellation::CONSTANT_EXPENSE_TYPE . " THEN " . Article::PAL_EXPENSES_FIXED . "
                  WHEN t.profit_loss_type = " . GoodsCancellation::OPERATING_EXPENSE_TYPE . " THEN " . Article::PAL_EXPENSES_FIXED . "
                  WHEN t.profit_loss_type = " . GoodsCancellation::OTHER_EXPENSE_TYPE . " THEN " . Article::PAL_EXPENSES_OTHER . "                  
                  ELSE " . Article::PAL_UNSET . "
              END AS pal_type,
 
              DATE_FORMAT(t.document_date, '%m') m,
              DATE_FORMAT(t.document_date, '%Y') y,
              SUM(".($withoutNds ? 't.total_amount_no_nds' : 't.total_amount_with_nds').") amount,
              SUM(".($withoutNds ? 't.total_amount_no_nds' : 't.total_amount_with_nds').") amountTax
            FROM {$table} t
            WHERE t.company_id = %%COMPANY_ID%%
              AND (t.document_date BETWEEN '{$year}-01-01' AND '{$year}-12-31')            
              {$AND_FILTERS}
            
            GROUP BY ".($monthGroupBy ?: 'NULL').", ".($revenueGroupBy ?: 'NULL').", y, m, pal_type;
        ";

        foreach ($this->multiCompanyIds as $companyId) {

            $query = str_replace('%%COMPANY_ID%%', $companyId, $tplQuery);
            $rows = Yii::$app->db2->createCommand($query)->queryAll();

            foreach ($rows as $row) {
                $this->rawDocs[$companyId][$row['pal_type']][] = $row;
            }
        }
    }

    private function initRawRevenueGroupedByProduct($companyId, $year)
    {
        $olapTable = self::TABLE_PRODUCT_TURNOVER;
        $contractorTable = Contractor::tableName();
        $invoiceTable = Invoice::tableName();
        $ioType = Documents::IO_TYPE_OUT;
        $revenueGroupBy = $this->getRevenueGroupedByProduct();
        $monthGroupBy = $this->getMonthGroupBy();
        $AND_FILTERS = $this->getSqlFilters();
        $invoiceIncomeItemIds = $this->_getItemsByPalOptions(AnalyticsArticle::PAL_INCOME_REVENUE);

        $tplQuery = "
            SELECT
              t.product_group_id, 
              t.product_id, 
              t.type,
              t.contractor_id,
              IFNULL(t.project_id, 0) AS project_id,
              IFNULL(t.sale_point_id, 0) AS sale_point_id,
              IFNULL(t.industry_id, 0) AS industry_id,                   
              LPAD(t.month, 2, '0') AS m,
              t.year AS y,
              SUM(t.total_amount) amount,
              SUM(IF(c.not_accounting, 0, t.total_amount)) amountTax
            FROM {$olapTable} t
            LEFT JOIN {$contractorTable} c ON t.contractor_id = c.id                          
            LEFT JOIN {$invoiceTable} i ON t.invoice_id = i.id
            WHERE t.company_id = %%COMPANY_ID%%
              AND t.date BETWEEN '{$year}-01-01' AND '{$year}-12-31'  
              AND t.type = {$ioType}
              AND t.is_invoice_actual = 1
              AND t.is_document_actual = 1
              AND t.document_table <> 'goods_cancellation'
              AND (t.invoice_id IS NOT NULL AND i.invoice_income_item_id IN (".implode(',', $invoiceIncomeItemIds).") OR t.invoice_id IS NULL)
              {$AND_FILTERS}
            GROUP BY ".($monthGroupBy ?: 'NULL').", ".($revenueGroupBy ?: 'NULL').", y, m;
        ";

        $query = str_replace('%%COMPANY_ID%%', $companyId, $tplQuery);
        $rows = Yii::$app->db2->createCommand($query)->queryAll();

        // docs
        $this->rawDocsRevenue[$companyId] = [];
        foreach ($rows as $row) {
            $this->rawDocsRevenue[$companyId][$row[$revenueGroupBy]][] = $row;
        }

        // flows
        foreach ($this->rawFlows[$companyId] as $palType => $palData)
            if ($palType == AnalyticsArticle::PAL_INCOME_REVENUE)
                foreach ($palData as $row)
                    $this->rawFlowsRevenue[$companyId]['revenueByNoDocs'][] = $row;
    }

    private function _addToParent($data, &$parent)
    {
        $groupedBy = $this->getMonthGroupBy();
        $month = $data['m'];

        foreach (['amount', 'amountTax'] as $amountKey) {
            $parent[$amountKey][$month] += $data[$amountKey];
        }
        // GROUPED BY MONTH PARENT
        if ($groupedBy) {
            foreach (['amount' => 'groupedAmount', 'amountTax' => 'groupedAmountTax'] as $amountKey => $groupedAmountKey) {
                if (!isset($parent[$groupedAmountKey][$data[$groupedBy]])) {
                    $parent[$groupedAmountKey][$data[$groupedBy]] = self::_zeroAmountArray();
                }
                $parent[$groupedAmountKey][$data[$groupedBy]][$month] += $data[$amountKey];
            }
        }
    }

    private function _addToResult($data, &$result, $type, $movementType = self::MOVEMENT_TYPE_DOCS_AND_FLOWS, $palBlock = null)
    {
        $month = $data['m'];
        $groupedBy = $this->getMonthGroupBy();

        // item
        $item = $data['item_id'] . ($palBlock ? "-{$palBlock}" : "");
        $key = ($type == Cash::FLOW_TYPE_INCOME) ? "income-{$item}" : "expense-{$item}";

        // subitem
        if (isset(self::$articlesToParents[$data['item_id']])) {
            $subItem = $item;
            $subKey = $key;
            $item = self::$articlesToParents[$data['item_id']];
            $key = ($type == Cash::FLOW_TYPE_INCOME) ? "income-{$item}" : "expense-{$item}";
        } else {
            $subKey = null;
            $subItem = null;
        }

        if (!isset($result[$key])) {
            $itemName = ($type == Cash::FLOW_TYPE_INCOME) ?
                InvoiceIncomeItem::findOne($item)->name ?? "income_item_id={$item}" :
                InvoiceExpenditureItem::findOne($item)->name ?? "expense_item_id={$item}";

            $result[$key] = self::_getBlankItem(null, $itemName, null, $key, $movementType);
        }

        if ($subItem) {
            if (!isset($result[$key]['items'][$subKey])) {
                $subItemName = ($type == Cash::FLOW_TYPE_INCOME) ?
                    InvoiceIncomeItem::findOne($subItem)->name ?? "income_item_id={$subItem}" :
                    InvoiceExpenditureItem::findOne($subItem)->name ?? "expense_item_id={$subItem}";

                $result[$key]['items'][$subKey] = self::_getBlankItem(null, $subItemName, null, $subKey, $movementType);
            }

            foreach (['amount', 'amountTax'] as $amountKey) {
                $result[$key]['items'][$subKey][$amountKey][$month] += $data[$amountKey];
            }
        }

        foreach (['amount', 'amountTax'] as $amountKey) {
            $result[$key][$amountKey][$month] += $data[$amountKey];
        }

        // GROUPED BY MONTH
        if ($groupedBy) {
            foreach (['amount' => 'groupedAmount', 'amountTax' => 'groupedAmountTax'] as $amountKey => $groupedAmountKey) {
                if (!isset($result[$key][$groupedAmountKey][$data[$groupedBy]])) {
                    $result[$key][$groupedAmountKey][$data[$groupedBy]] = self::_zeroAmountArray();
                }
                $result[$key][$groupedAmountKey][$data[$groupedBy]][$month] += $data[$amountKey];
                if (!empty($result[$key]['items'])) {
                    $result[$key]['items'][$subKey][$groupedAmountKey][$data[$groupedBy]][$month] += $data[$amountKey];
                }
            }
        }

        return $key;
    }

    private function _addExchangeDifferenceToResult(&$parent, &$result, $type)
    {
        $groupedBy = $this->getMonthGroupBy();

        $itemNameMap = [
            'income' => 'Положительная курсовая разница ',
            'expense' => 'Отрицательная курсовая разница ',
        ];

        foreach ($this->exchangeDifference as $c => $data) {
            $key = $type.'-exchangeDifference-'.$c;

            // SKIP GROUPED
            if ($c == 'grouped' && $groupedBy) {
                continue;
            }

            if (!isset($result[$key])) {
                $itemName = $itemNameMap[$type].$c;
                $result[$key] = self::_getBlankItem(null, $itemName, null);
                $result[$key]['data-hover_text'] = 'Расчетная величина';
                $result[$key]['amount'] = $data[$type];
                $result[$key]['amountTax'] = $data[$type];

                foreach ($data[$type] as $period => $amount) {
                    if (array_key_exists($period, $parent['amount'])) {
                        $parent['amount'][$period] += $amount;
                        $parent['amountTax'][$period] += $amount;
                    }
                }
            }
        }

        if ($groupedBy) {

            foreach ($this->exchangeDifference['grouped'] as $groupId => $groupData)
            foreach ($groupData as $c => $data) {
                $key = $type.'-exchangeDifference-'.$c;

                if (isset($result[$key])) {
                    $result[$key]['groupedAmount'][$groupId] = $data[$type];
                    $result[$key]['groupedAmountTax'][$groupId] = $data[$type];

                    foreach ($data[$type] as $period => $amount) {
                        if (array_key_exists($period, $parent['amount'])) {
                            $parent['groupedAmount'][$groupId][$period] += $amount;
                            $parent['groupedAmount'][$groupId][$period] += $amount;
                        }
                    }
                }
            }

        }
    }

    /**
     * @param $palType
     * @return array
     */
    private function _getItemsByPalOptions($palType) {
        return AnalyticsArticle::find()
            ->where(['company_id' => $this->multiCompanyIds])
            ->andWhere(['profit_and_loss_type' => $palType])
            ->select('item_id')
            ->column() ?: [-1];
    }

    /**
     * @param $type
     * @param $itemId
     * @return int
     */
    private function _getItemPalType($flowType, $itemId) {
        return AnalyticsArticle::find()
            ->where(['company_id' => $this->multiCompanyIds])
            ->andWhere(['type' => $flowType])
            ->andWhere(['item_id' => $itemId])
            ->select('profit_and_loss_type')
            ->scalar() ?: 0;
    }

    private static function _sortResult(&$result)
    {
        uasort($result, function ($a, $b) {
            return $a['label'] <=> $b['label'];
        });

        foreach ($result as $k => $v)
            if (isset($result[$k]['items']))
                uasort($result[$k]['items'], function ($a, $b) {
                    return $a['label'] <=> $b['label'];
                });
    }

    private static function _sortResultByMonth(&$result)
    {
        $month = (date('m') > 1)
            ? date('m', strtotime('-1 months'))
            : '01';

        uasort($result, function ($a, $b) use ($month) {
            return -1 * (($a['amount'][$month] ?? 0) <=> ($b['amount'][$month] ?? 0));
        });
    }

    private static function _sortResultByYear(&$result)
    {
        uasort($result, function ($a, $b) {
            return (array_sum($b['amount'] ?? []) <=> array_sum($a['amount'] ?? []));
        });
    }

    protected function getSqlHideTinParent()
    {
        return " AND t.has_tin_children = 0 ";
    }

    protected function _getNdsContractors()
    {
        return Contractor::find()
            ->where([
                'company_id' => $this->multiCompanyIds,
                'taxation_system' => Contractor::WITH_NDS
            ])
            ->select('id')
            ->column();
    }
}