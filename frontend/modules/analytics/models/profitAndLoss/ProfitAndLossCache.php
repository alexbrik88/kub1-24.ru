<?php
namespace frontend\modules\analytics\models\profitAndLoss;

use Yii;
use frontend\modules\analytics\models\profitAndLoss\models\OlapProfitAndLossCache as CacheRow;

class ProfitAndLossCache {

    public $year;

    public $companyId;

    private $reportYear;

    private $uniqueHash;

    // PUBLIC METHODS

    public function validate() {

        return (YII_ENV_DEV || YII_ENV_PROD)
            && ($this->year < $this->reportYear)
            && ($this->reportYear > 1900)
            && ($this->reportYear < 2100);
    }

    public function get() {

        /** @var CacheRow[] $cacheRows */
        $cacheRows = CacheRow::find()
            ->where(['company_id' => $this->companyId])
            ->andWhere(['year' => $this->year])
            ->all();

        foreach ($cacheRows as $cacheRow) {

            if ($cacheRow->hash === $this->uniqueHash) {

                $expired =
                    ($this->_isSelfExpired($cacheRow)) || 
                    ($this->_getHasUpdatedDocs($cacheRow)) ||
                    ($this->_getHasUpdatedFlows($cacheRow)) ||
                    ($this->_getHasDeletedDocs($cacheRow)) ||
                    ($this->_getHasDeletedFlows($cacheRow));
                
                if (!$expired) {
                    $cachedData = $cacheRow->getDataAsArray();
                    if (isset($cachedData['totalRevenue'])) {
                        // success cached
                        return $cachedData;
                    }
                }
            }


            try {
                $cacheRow->deleteCascade();
            } catch (\Throwable $e) {}
        }

        return [];
    }

    public function put($data) {

        $cacheRow = new CacheRow([
            'company_id' => $this->companyId,
            'year' => $this->year,
            'hash' => $this->uniqueHash,
            'docs' => $this->__getDocsCountNow($this->year),
            'flows' => $this->__getFlowsCountNow($this->year)
        ]);

        $cacheRow->setDataAsArray($data);

        try {
            if ($cacheRow->save()) {
                // success cached
                return true;
            } else {
                // todo: log validation errors
            }
        } catch (\Throwable $e) {}

        return false;
    }

    public function uniqueBy(...$args)
    {
        $str = '';
        foreach ($args as $arg)
            $str .= is_array($arg) ? serialize($arg) : $arg;

        $this->uniqueHash = md5($str);

        return $this;
    }

    public function nonCachedYear($reportYear)
    {
        $this->reportYear = $reportYear;

        return $this;
    }

    public function setCompany($companyId)
    {
        $this->companyId = $companyId;

        return $this;
    }

    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    // PRIVATE METHODS

    private function _isSelfExpired(CacheRow $cacheRow)
    {
        return date('Y-m-d H:i:s') >= $cacheRow->expired_at;
    }

    private function _getHasUpdatedDocs(CacheRow $cacheRow)
    {
        return $cacheRow->year >= $this->__getDocsUpdatedYear($cacheRow->year);
    }

    private function _getHasUpdatedFlows(CacheRow $cacheRow)
    {
        return $cacheRow->year >= $this->__getFlowsUpdatedYear($cacheRow->year);
    }

    private function _getHasDeletedDocs(CacheRow $cacheRow)
    {
        return $cacheRow->docs > $this->__getDocsCountNow($cacheRow->year);
    }

    private function _getHasDeletedFlows(CacheRow $cacheRow)
    {
        return $cacheRow->flows > $this->__getFlowsCountNow($cacheRow->year);
    }

    // PRIVATE LOW LEVEL METHODS

    private $__docsCountPerYear = [];

    private $__flowsCountPerYear = [];

    private function __getFlowsUpdatedYear($year)
    {
        $companyId = (int)$this->companyId;
        $query = " SELECT MIN(YEAR(`recognition_date`)) FROM `olap_flows` WHERE `company_id` = {$companyId} AND updated_at > (
            SELECT MIN(`created_at`) FROM `olap_profit_and_loss_cache` WHERE `company_id` = {$companyId} AND `year` = {$year} )";

        $flowYear = Yii::$app->db->createCommand($query)->queryScalar() ?: date('Y');

        return min($flowYear, date('Y'));
    }

    private function __getDocsUpdatedYear($year)
    {
        $companyId = (int)$this->companyId;
        $query = " SELECT MIN(YEAR(`date`)) FROM `olap_documents` WHERE `company_id` = {$companyId} AND updated_at > (
            SELECT MIN(`created_at`) FROM `olap_profit_and_loss_cache` WHERE `company_id` = {$companyId} AND `year` = {$year} )";

        $docYear = Yii::$app->db->createCommand($query)->queryScalar() ?: date('Y');

        return min($docYear, date('Y'));
    }

    private function __getDocsCountNow($year)
    {
        $companyId = (int)$this->companyId;
        if (empty($this->__docsCountPerYear)) {
            $query = "SELECT YEAR(`date`) AS `y`, count(*) AS `cnt` FROM `olap_documents` WHERE `company_id` = {$companyId} GROUP BY `y`";
            $docsPerYear = Yii::$app->db->createCommand($query)->queryAll();
            foreach ($docsPerYear as $data)
                $this->__docsCountPerYear[$data['y']] = $data['cnt'];
        }

        return $this->__docsCountPerYear[$year] ?? 0;
    }

    private function __getFlowsCountNow($year)
    {
        $companyId = (int)$this->companyId;
        if (empty($this->__flowsCountPerYear)) {
            $query = "SELECT YEAR(`recognition_date`) AS `y`, count(*) AS `cnt` FROM `olap_flows` WHERE `company_id` = {$companyId} GROUP BY `y`";
            $flowsPerYear = Yii::$app->db->createCommand($query)->queryAll();
            foreach ($flowsPerYear as $data)
                $this->__flowsCountPerYear[$data['y']] = $data['cnt'];
        }

        return $this->__flowsCountPerYear[$year] ?? 0;
    }
}