<?php

namespace frontend\modules\analytics\models\profitAndLoss\models;

use Yii;
use common\models\Company;

/**
 * This is the model class for table "olap_profit_and_loss_cache".
 *
 * @property int $company_id
 * @property int $year
 * @property string $hash
 * @property int $flows
 * @property int $docs
 * @property string|null $data
 * @property string|null $created_at
 * @property string|null $expired_at
 *
 * @property Company $company
 */
class OlapProfitAndLossCache extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'olap_profit_and_loss_cache';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'year', 'hash'], 'required'],
            [['company_id', 'year', 'docs', 'flows'], 'integer'],
            [['data'], 'string'],
            [['created_at', 'expired_at'], 'safe'],
            [['hash'], 'string', 'max' => 32],
            [['company_id', 'year', 'hash'], 'unique', 'targetAttribute' => ['company_id', 'year', 'hash']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    public function getDataAsArray()
    {
        return (!empty($this->data))
            ? (array)json_decode($this->data, true)
            : [];
    }

    public function setDataAsArray($dataArr)
    {
        if (is_array($dataArr))
            $this->data = json_encode($dataArr);
    }

    public function deleteCascade()
    {
        self::deleteAll(['and',
            ['company_id' => $this->company_id],
            ['>=', 'year', $this->year]
        ]);
    }
}