<?php

namespace frontend\modules\analytics\models\profitAndLoss;

use common\models\cash\CashBankFlows;
use common\models\cash\CashBankFlowToInvoice;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyFlowToInvoice;
use common\models\cash\CashFlowsBase as Cash;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderFlowToInvoice;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use frontend\modules\analytics\models\AnalyticsArticle as Article;
use frontend\modules\analytics\models\ProfitAndLossCompanyItem as Item;
use yii\db\Expression;
use yii\db\Query;

/// docs: сумма входящих Актов, ТН и УПД, у которых указанные статьи и дата которых попадает в нужный месяц
/// flows: есть привязка к счетам, у которых НЕТ Актов/ТН/УПД или у них стоит "Без Акта/ТН/УПД" и "Дата признания дохода" в нужном нам месяце +
/// flows: нет привязки к счетам и "Дата признания дохода" нужном месяце
///
/// docs2+flows2: если в "Оплата поставщику" покупка Товара, то выводим, если закуплен товар который с галочкой "Не для перепродажи"
/// docs2+flows2: если в "Оплата поставщику" покупка Услуги

trait OperatingCostsTrait {

    /**
     * @param $parentItem
     * @param $year
     * @return mixed
     */
    public function getOperatingCosts($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__TRAIT__, $companyId);

        if (!array_key_exists($cachePrefix . $year, $this->data)) {

            $docs = $this->rawDocs[$companyId][Article::PAL_EXPENSES_OPERATING] ?? [];
            $flows = $this->rawFlows[$companyId][Article::PAL_EXPENSES_OPERATING] ?? [];

            if ($docs2 = $this->__getSuppliersOperatingCostsByDocs($companyId))
                $docs = array_merge($docs, $docs2);

            if ($flows2 = $this->__getSuppliersOperatingCostsByFlows($companyId))
                $flows = array_merge($flows, $flows2);

            foreach ([$year] as $byYear) {

                $parent = $parentItem;
                $result = [];

                foreach ($docs as $doc) {
                    if ($byYear == $doc['y']) {
                        self::_addToResult($doc, $result, Cash::FLOW_TYPE_EXPENSE, self::MOVEMENT_TYPE_DOCS_AND_FLOWS, Item::OPERATING_EXPENSE_TYPE);
                        self::_addToParent($doc, $parent);
                    }
                }

                foreach ($flows as $flow) {
                    if ($byYear == $flow['y']) {
                        self::_addToResult($flow, $result, Cash::FLOW_TYPE_EXPENSE, self::MOVEMENT_TYPE_DOCS_AND_FLOWS, Item::OPERATING_EXPENSE_TYPE);
                        self::_addToParent($flow, $parent);
                    }
                }

                self::_sortResult($result);
                $result = ['totalOperatingCosts' => $parent] + $result;
                $this->data[$cachePrefix . $byYear] = $this->calculateQuarterAndTotalAmount($result);
            }

        }

        return $this->data[$cachePrefix . $year];
    }

    public function __getSuppliersOperatingCostsByDocs($companyId)
    {
        $table = self::TABLE_PRODUCT_TURNOVER;

        $result = \Yii::$app->db2->createCommand("
            SELECT 
              DATE_FORMAT(t.date, '%m') m,
              DATE_FORMAT(t.date, '%Y') y,                
              SUM(t.total_amount) amount,
              IFNULL(t.contractor_id, 0) AS contractor_id,
              IFNULL(t.project_id, 0) AS project_id, 
              IFNULL(t.sale_point_id, 0) AS sale_point_id, 
              IFNULL(t.industry_id, 0) AS industry_id               
              -- 0 AS amountTax,
              -- 1 AS item_id
            FROM {$table} t
            LEFT JOIN `invoice` i ON t.invoice_id = i.id
            WHERE t.company_id = {$companyId}
              AND t.type = 1
              AND (t.production_type = 0 OR t.not_for_sale = 1)              
              AND i.invoice_expenditure_item_id = " . InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT . "
            GROUP BY contractor_id, YEAR(t.date), MONTH(t.date)
        ")->queryAll();

        $ret = [];
        foreach ($result as $res)
        {
            $ret[] = [
                'm' => $res['m'],
                'y' => $res['y'],
                'amount' => $res['amount'],
                'amountTax' => (in_array($res['contractor_id'], self::$notAccountingContractors)) ? 0 : $res['amount'],
                'item_id' => InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT,
                'industry_id' => $res['industry_id'],
                'sale_point_id' => $res['sale_point_id'],
                'project_id' => $res['project_id'],
            ];
        }

        return $ret;
    }

    public function __getSuppliersOperatingCostsByFlows($companyId)
    {
        $where = [
            'and',
            ['i.company_id' => $companyId],
            ['i.is_deleted' => 0],
            ['i.invoice_expenditure_item_id' => InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT],
        ];

        $where2 = [
            'or',
            ['and', ['has_act' => 0], ['has_packing_list' => 0], ['has_upd' => 0]],
            ['or', ['need_act' => 0], ['need_packing_list' => 0], ['need_upd' => 0]],
        ];

        $select = new Expression('
              DATE_FORMAT(f.recognition_date, "%m") m,
              DATE_FORMAT(f.recognition_date, "%Y") y,                
              t.amount,
              i.id AS invoice_id,
              IFNULL(i.contractor_id, 0) AS contractor_id,
              IFNULL(i.project_id, 0) AS project_id, 
              IFNULL(i.sale_point_id, 0) AS sale_point_id, 
              IFNULL(i.industry_id, 0) AS industry_id        
        ');

        $query1 = (new Query())->select($select)
            ->from(['i' => Invoice::tableName()])
            ->innerJoin(['t' => CashBankFlowToInvoice::tableName()], '{{t}}.[[invoice_id]] = {{i}}.[[id]]')
            ->leftJoin(['f' => CashBankFlows::tableName()], '{{t}}.[[flow_id]] = {{f}}.[[id]]')->andWhere($where)->andWhere($where2);

        $query2 = (new Query())->select($select)
            ->from(['i' => Invoice::tableName()])
            ->innerJoin(['t' => CashOrderFlowToInvoice::tableName()], '{{t}}.[[invoice_id]] = {{i}}.[[id]]')
            ->leftJoin(['f' => CashOrderFlows::tableName()], '{{t}}.[[flow_id]] = {{f}}.[[id]]')->andWhere($where)->andWhere($where2);

        $query3 = (new Query())->select($select)
            ->from(['i' => Invoice::tableName()])
            ->innerJoin(['t' => CashEmoneyFlowToInvoice::tableName()], '{{t}}.[[invoice_id]] = {{i}}.[[id]]')
            ->leftJoin(['f' => CashEmoneyFlows::tableName()], '{{t}}.[[flow_id]] = {{f}}.[[id]]')->andWhere($where)->andWhere($where2);

        $query = (new Query)->select(new Expression('
            o.contractor_id,
            o.industry_id,
            o.sale_point_id,
            o.project_id,
            o.m,
            o.y,
            SUM(o.amount) amount
        '))->from(['o' => $query1->union($query2, true)->union($query3, true)])
            ->leftJoin(['r' => 'order'], 'r.invoice_id = o.invoice_id')
            ->leftJoin(['p' => 'product'], 'p.id = r.product_id')
            ->andWhere(['or',
                ['p.production_type' => 0],
                ['p.not_for_sale' => 1]
            ])
            ->groupBy(['contractor_id', 'm', 'y']);

        $result = $query->all();

        $ret = [];
        foreach ($result as $res)
        {
            $ret[] = [
                'm' => $res['m'],
                'y' => $res['y'],
                'amount' => $res['amount'],
                'amountTax' => (in_array($res['contractor_id'], self::$notAccountingContractors)) ? 0 : $res['amount'],
                'item_id' => InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT,
                'industry_id' => $res['industry_id'],
                'sale_point_id' => $res['sale_point_id'],
                'project_id' => $res['project_id'],
            ];
        }

        return $ret;
    }
}