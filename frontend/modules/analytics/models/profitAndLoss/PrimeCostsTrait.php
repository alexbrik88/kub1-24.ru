<?php
namespace frontend\modules\analytics\models\profitAndLoss;

use common\models\cash\CashFlowsBase as Cash;
use common\models\document\InvoiceExpenditureItem;
use common\models\product\ProductTurnover;
use frontend\modules\analytics\models\AnalyticsArticle as Article;
use Yii;
use common\models\Contractor;
use frontend\modules\analytics\models\ProfitAndLossCompanyItem as Item;

/// docs, flows: себестоимость товаров и услуг

trait PrimeCostsTrait {

      public function getPrimeCosts($parentItem, $year, $companyId)
      {
          $cachePrefix = self::getCachePrefix(__TRAIT__, $companyId);

          if (!array_key_exists($cachePrefix . $year, $this->data)) {

              $docs = $this->rawDocs[$companyId][Article::PAL_EXPENSES_PRIME_COST] ?? [];
              $flows = $this->rawFlows[$companyId][Article::PAL_EXPENSES_PRIME_COST] ?? [];

              foreach ([$year] as $byYear) {

                  $parent = $parentItem;
                  $result = [];

                  foreach ($docs as $doc) {
                      if ($byYear == $doc['y']) {
                          //$movementType = (self::_isPrimeCostItemClickable($doc['item_id']))
                          //    ? self::MOVEMENT_TYPE_DOCS_AND_FLOWS : null;
                          self::_addToResult($doc, $result, Cash::FLOW_TYPE_EXPENSE, self::MOVEMENT_TYPE_DOCS_AND_FLOWS, Item::PRIME_COST_EXPENSE_TYPE);
                          self::_addToParent($doc, $parent);
                      }
                  }

                  foreach ($flows as $flow) {
                      if ($byYear == $flow['y']) {
                          //$movementType = (self::_isPrimeCostItemClickable($flow['item_id']))
                          //    ? self::MOVEMENT_TYPE_DOCS_AND_FLOWS : null;
                          self::_addToResult($flow, $result, Cash::FLOW_TYPE_EXPENSE, self::MOVEMENT_TYPE_DOCS_AND_FLOWS, Item::PRIME_COST_EXPENSE_TYPE);
                          self::_addToParent($flow, $parent);
                      }
                  }

                  self::_sortResult($result);
                  $result = ['totalPrimeCosts' => $parent] + $result;
                  $this->data[$cachePrefix . $byYear] = $this->calculateQuarterAndTotalAmount($result);
              }
          }

          return $this->data[$cachePrefix . $year];
    }

    //private function _isPrimeCostItemClickable($itemId)
    //{
    //    return
    //        $itemId != InvoiceExpenditureItem::ITEM_PRIME_COST_PRODUCT &&
    //        $itemId != InvoiceExpenditureItem::ITEM_PRIME_COST_SERVICE;
    //}
}
