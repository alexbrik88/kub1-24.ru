<?php

namespace frontend\modules\analytics\models\profitAndLoss;

use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\ExpenseItemFlowOfFunds;
use common\models\IncomeItemFlowOfFunds;
use common\models\product\Product;
use common\models\product\ProductTurnover;
use frontend\models\Documents;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\models\ProfitAndLossCompanyItem;
use yii\db\Expression;
use Yii;
use common\models\cash\CashBankFlowToInvoice;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyFlowToInvoice;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderFlowToInvoice;
use yii\db\Query;

trait BaseOlapTrait {

    private function getOlapFromDate()
    {
        return isset($this->dateFrom) ? $this->dateFrom : $this->_yearFilter[array_key_last($this->_yearFilter)] . "-01-01";
    }

    private function getOlapToDate()
    {
        return isset($this->dateTo) ? $this->dateTo : $this->_yearFilter[array_key_first($this->_yearFilter)] . "-12-31";
    }

    private function getCompanyExpenditureItems($companyId, $expenseType, $exceptFlowOfFundsBlock = null)
    {
        $query = ProfitAndLossCompanyItem::find()
            ->joinWith('item')
            ->andWhere(['profit_and_loss_company_item.expense_type' => $expenseType])
            ->andWhere(['profit_and_loss_company_item.company_id' => $companyId])
            ->indexBy(['item_id'])
            ->orderBy(InvoiceExpenditureItem::tableName() . '.name');

        if ($exceptFlowOfFundsBlock) {
            if ($exceptArticles = ExpenseItemFlowOfFunds::find()
                ->where(['flow_of_funds_block' => $exceptFlowOfFundsBlock])
                ->andWhere(['not', ['expense_item_id' => InvoiceExpenditureItem::ITEM_DIVIDEND_PAYMENT]])
                ->select(['expense_item_id'])
                ->column()) {

                $query->andWhere(['not', ['item_id' => $exceptArticles]]);
            }
        }

        return $query->asArray()->all(\Yii::$app->db2);
    }

    private function getUndestributedFlows($companyId, $type, $includeItemId = [])
    {
        $table = self::TABLE_OLAP_FLOWS;
        $dateFrom = $this->getOlapFromDate();
        $dateTo = $this->getOlapToDate();

        $exceptIncomeItemId = ($type == CashFlowsBase::FLOW_TYPE_INCOME && !$includeItemId) ?
            self::getExceptUndestributedIncomeItems($companyId, AbstractFinance::FINANCIAL_OPERATIONS_BLOCK) : [];

        return Yii::$app->db2->createCommand("
                SELECT 
                  DATE_FORMAT(t.recognition_date, '%m') m,
                  DATE_FORMAT(t.recognition_date, '%Y') y,
                  SUM(t.amount) amount,
                  SUM(IF(t.is_accounting, t.amount, 0)) amountTax,
                  item_id
                FROM {$table} t 
                WHERE  t.company_id = {$companyId}
                  AND t.recognition_date BETWEEN '{$dateFrom}' AND '{$dateTo}'
                  AND t.type = {$type}
                  AND (t.has_invoice = FALSE OR t.has_doc = FALSE OR t.need_doc = FALSE) ".
            ($exceptIncomeItemId ? (" AND t.item_id NOT IN (".implode(',', $exceptIncomeItemId).")") : "").
            ($includeItemId ? (" AND t.item_id IN (".implode(',', $includeItemId).")") : "")."
                GROUP BY YEAR(t.recognition_date), MONTH(t.recognition_date)". ($includeItemId ? ", t.item_id" : ""))->queryAll();
    }

    private function getUndestributedDocs($companyId, $type, $includeExpenditureItemId = [])
    {
        $table = self::TABLE_OLAP_DOCS;
        $dateFrom = $this->getOlapFromDate();
        $dateTo = $this->getOlapToDate();

        return Yii::$app->db2->createCommand("
                SELECT 
                  DATE_FORMAT(t.date, '%m') m,
                  DATE_FORMAT(t.date, '%Y') y,                
                  SUM(t.amount) amount,
                  SUM(IF(t.is_accounting, t.amount, 0)) amountTax,
                  invoice_expenditure_item_id AS item_id
                FROM {$table} t
                WHERE t.company_id = {$companyId}
                  AND t.date BETWEEN '{$dateFrom}' AND '{$dateTo}'
                  AND t.type = {$type}".
            ($includeExpenditureItemId ? (" AND t.invoice_expenditure_item_id IN (".implode(',', $includeExpenditureItemId).")") : "")."
                GROUP BY YEAR(t.date), MONTH(t.date)". ($includeExpenditureItemId ? ", t.invoice_expenditure_item_id" : ""))->queryAll();
    }

    /**
     * @return array
     */
    public static function getExceptUndestributedIncomeItems($companyId = null, $flowOfFundsBlock = null)
    {
        $articles = [
            InvoiceIncomeItem::ITEM_STARTING_BALANCE,
            InvoiceIncomeItem::ITEM_RETURN,
            InvoiceIncomeItem::ITEM_BORROWING_REDEMPTION,
            InvoiceIncomeItem::ITEM_BUDGET_RETURN,
            InvoiceIncomeItem::ITEM_LOAN,
            InvoiceIncomeItem::ITEM_CREDIT,
            InvoiceIncomeItem::ITEM_ENSURE_PAYMENT,
            InvoiceIncomeItem::ITEM_FROM_FOUNDER,
            InvoiceIncomeItem::ITEM_OWN_FOUNDS,
            InvoiceIncomeItem::ITEM_OTHER,
            InvoiceIncomeItem::ITEM_RECEIVED_PERCENTS,
            InvoiceIncomeItem::ITEM_RECEIVED_PERCENTS_BY_DEPOSITS,
            InvoiceIncomeItem::ITEM_INPUT_MONEY
        ];

        $companyArticles = [];
        if ($companyId && $flowOfFundsBlock) {
            $companyArticles = IncomeItemFlowOfFunds::find()
                ->where(['flow_of_funds_block' => $flowOfFundsBlock])
                ->andWhere(['company_id' => $companyId])
                ->select(['income_item_id'])
                ->column();
        }

        return array_merge($articles, $companyArticles);
    }

    private function __getPrimeCostsByDocs($companyId)
    {
        $productTable = Product::tableName();
        $contractorTable = Contractor::tableName();
        $turnoverTable = ProductTurnover::tableName();
        $dateFrom = $this->getOlapFromDate();
        $dateTo = $this->getOlapToDate();

        //////////// BY DOC ////////////
        /// сумма всех Актов, ТН и УПД, дата которых в нужном месяце
        $query = "
            SELECT
              DATE_FORMAT(t.date, '%m') m,
              DATE_FORMAT(t.date, '%Y') y,
              SUM(IFNULL(p.price_for_buy_with_nds,0) * t.quantity) AS amount,
              SUM(IF(c.not_accounting, 0, 1) * IFNULL(p.price_for_buy_with_nds,0) * t.quantity) AS amountTax
            FROM {$turnoverTable} t 
            LEFT JOIN {$productTable} p ON p.id = t.product_id
            LEFT JOIN {$contractorTable} c ON c.id = t.contractor_id
            WHERE t.company_id = {$companyId}
              AND t.date BETWEEN '{$dateFrom}' AND '{$dateTo}'
              AND t.type = 2                  
              AND t.is_document_actual = TRUE
              AND t.is_invoice_actual = TRUE            
            GROUP BY YEAR(t.date), MONTH(t.date)

       ";

        return Yii::$app->db2->createCommand($query)->queryAll();
    }

    private function __getPrimeCostsByFlows($companyId)
    {
        $flowsTable = self::TABLE_OLAP_FLOWS;
        $dateFrom = $this->getOlapFromDate();
        $dateTo = $this->getOlapToDate();

        //////////// BY NO DOC ////////////
        ///  1) есть привязка к счетам, у которых НЕТ Актов/ТН/УПД или у них стоит "Без Акта/ТН/УПД" и "Дата признания дохода" в нужном месяце
        $exceptIncomeFlowsArticles = implode(',', BaseOlapTrait::getExceptUndestributedIncomeItems($companyId, AbstractFinance::FINANCIAL_OPERATIONS_BLOCK));

        $subQuery = "
            SELECT 
              t.recognition_date,
              r.invoice_id,
              t.is_accounting
            FROM {$flowsTable} t
            LEFT JOIN `cash_bank_flow_to_invoice` r ON r.flow_id = t.id              
            WHERE t.company_id = {$companyId}
              AND t.wallet = 1
              AND t.type = 1
              AND t.item_id NOT IN ({$exceptIncomeFlowsArticles})
              AND t.has_invoice = TRUE AND (t.has_doc = FALSE OR t.need_doc = FALSE)
            
            UNION
            
            SELECT 
              t.recognition_date,
              r.invoice_id,
              t.is_accounting
            FROM {$flowsTable} t
            LEFT JOIN `cash_order_flow_to_invoice` r ON r.flow_id = t.id             
            WHERE t.company_id = {$companyId}
              AND t.wallet = 2
              AND t.type = 1
              AND t.item_id NOT IN ({$exceptIncomeFlowsArticles})
              AND t.has_invoice = TRUE AND (t.has_doc = FALSE OR t.need_doc = FALSE)
              
            UNION
            
            SELECT 
              t.recognition_date,
              r.invoice_id,
              t.is_accounting
            FROM {$flowsTable} t
            LEFT JOIN `cash_emoney_flow_to_invoice` r ON r.flow_id = t.id             
            WHERE t.company_id = {$companyId}
              AND t.wallet = 3            
              AND t.type = 1
              AND t.item_id NOT IN ({$exceptIncomeFlowsArticles})
              AND t.has_invoice = TRUE AND (t.has_doc = FALSE OR t.need_doc = FALSE)
        ";

        $query = "
            SELECT 
              DATE_FORMAT(t.recognition_date, '%m') m,
              DATE_FORMAT(t.recognition_date, '%Y') y,     
              SUM(IFNULL(p.price_for_buy_with_nds,0) * o.quantity) AS amount,
              SUM(IF(t.is_accounting, IFNULL(p.price_for_buy_with_nds,0) * o.quantity, 0)) AS amountTax
              FROM (
                SELECT 
                  tt.invoice_id, 
                  MAX(tt.recognition_date) recognition_date, 
                  MAX(tt.is_accounting) is_accounting 
                FROM ({$subQuery}) tt GROUP BY tt.invoice_id) t
              LEFT JOIN `order` o ON t.invoice_id = o.invoice_id
              LEFT JOIN `product` p ON p.id = o.product_id
              WHERE t.recognition_date BETWEEN '{$dateFrom}' AND '{$dateTo}'
              GROUP BY YEAR(t.recognition_date), MONTH(t.recognition_date)
        ";

        return Yii::$app->db2->createCommand($query)->queryAll();
    }


    public function __getSuppliersOperatingCostsByDocs($companyId)
    {
        $table = self::TABLE_PRODUCT_TURNOVER;
        $dateFrom = $this->getOlapFromDate();
        $dateTo = $this->getOlapToDate();

        $result = \Yii::$app->db2->createCommand("
            SELECT 
              DATE_FORMAT(t.date, '%m') m,
              DATE_FORMAT(t.date, '%Y') y,                
              SUM(t.total_amount) amount,
              t.contractor_id
              -- 0 AS amountTax,
              -- 1 AS item_id
            FROM {$table} t
            LEFT JOIN `invoice` i ON t.invoice_id = i.id
            WHERE t.company_id = {$companyId}
              AND t.date BETWEEN '{$dateFrom}' AND '{$dateTo}'
              AND t.type = 1
              AND (t.production_type = 0 OR t.not_for_sale = 1)              
              AND i.invoice_expenditure_item_id = " . InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT . "
            GROUP BY contractor_id, YEAR(t.date), MONTH(t.date)
        ")->queryAll();

        $ret = [];
        foreach ($result as $res)
        {
            $ret[] = [
                'm' => $res['m'],
                'y' => $res['y'],
                'amount' => $res['amount'],
                'amountTax' => (in_array($res['contractor_id'], self::$notAccountingContractors)) ? 0 : $res['amount'],
                'item_id' => InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT
            ];
        }

        return $ret;
    }

    public function __getSuppliersOperatingCostsByFlows($companyId)
    {
        $dateFrom = $this->getOlapFromDate();
        $dateTo = $this->getOlapToDate();

        $where = [
            'and',
            ['i.company_id' => $companyId],
            ['i.is_deleted' => 0],
            ['i.invoice_expenditure_item_id' => InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT],
            ['between', 'f.recognition_date', $dateFrom, $dateTo],
        ];

        $where2 = [
            'or',
            ['and', ['has_act' => 0], ['has_packing_list' => 0], ['has_upd' => 0]],
            ['or', ['need_act' => 0], ['need_packing_list' => 0], ['need_upd' => 0]],
        ];

        $select = new Expression('
              DATE_FORMAT(f.recognition_date, "%m") m,
              DATE_FORMAT(f.recognition_date, "%Y") y,                
              t.amount,
              i.id AS invoice_id,
              i.contractor_id
        ');

        $query1 = (new Query())->select($select)
            ->from(['i' => Invoice::tableName()])
            ->innerJoin(['t' => CashBankFlowToInvoice::tableName()], '{{t}}.[[invoice_id]] = {{i}}.[[id]]')
            ->leftJoin(['f' => CashBankFlows::tableName()], '{{t}}.[[flow_id]] = {{f}}.[[id]]')->andWhere($where)->andWhere($where2);

        $query2 = (new Query())->select($select)
            ->from(['i' => Invoice::tableName()])
            ->innerJoin(['t' => CashOrderFlowToInvoice::tableName()], '{{t}}.[[invoice_id]] = {{i}}.[[id]]')
            ->leftJoin(['f' => CashOrderFlows::tableName()], '{{t}}.[[flow_id]] = {{f}}.[[id]]')->andWhere($where)->andWhere($where2);

        $query3 = (new Query())->select($select)
            ->from(['i' => Invoice::tableName()])
            ->innerJoin(['t' => CashEmoneyFlowToInvoice::tableName()], '{{t}}.[[invoice_id]] = {{i}}.[[id]]')
            ->leftJoin(['f' => CashEmoneyFlows::tableName()], '{{t}}.[[flow_id]] = {{f}}.[[id]]')->andWhere($where)->andWhere($where2);

        $query = (new Query)->select(new Expression('
            o.contractor_id,
            o.m,
            o.y,
            SUM(o.amount) amount
        '))->from(['o' => $query1->union($query2, true)->union($query3, true)])
           ->leftJoin(['r' => 'order'], 'r.invoice_id = o.invoice_id')
           ->leftJoin(['p' => 'product'], 'p.id = r.product_id')
           ->andWhere(['or',
               ['p.production_type' => 0],
               ['p.not_for_sale' => 1]
           ])
           ->groupBy(['contractor_id', 'm', 'y']);

        $result = $query->all();

        $ret = [];
        foreach ($result as $res)
        {
            $ret[] = [
                'm' => $res['m'],
                'y' => $res['y'],
                'amount' => $res['amount'],
                'amountTax' => (in_array($res['contractor_id'], self::$notAccountingContractors)) ? 0 : $res['amount'],
                'item_id' => InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT
            ];
        }

        return $ret;
    }
}