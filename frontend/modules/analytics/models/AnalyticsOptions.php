<?php

namespace frontend\modules\analytics\models;

use Yii;
use common\models\Company;

/**
 * This is the model class for table "analytics_options".
 *
 * @property int $company_id
 * @property int $key
 * @property string|null $value
 *
 * @property Company $company
 */
class AnalyticsOptions extends \yii\db\ActiveRecord
{
    const KEY_PAL_RESET_BUTTON_ENABLE = 1;
    const KEY_PAL_SKIP_PREPAYMENTS = 2;
    const KEY_PAL_SKIP_TAXES = 3;
    const KEY_PAL_REVENUE_RULE_GROUP = 4;
    const KEY_PAL_REVENUE_RULE = 5;
    const KEY_PAL_SKIP_ZEROES_ROWS = 6;
    const KEY_PAL_SKIP_CALCULATE_PRIME_COSTS = 7;
    const KEY_PAL_MONTH_GROUP_BY = 8;
    const KEY_PAL_SHOW_AMORTIZATION = 9;
    const KEY_PAL_SHOW_EBIT = 10;
    const KEY_PAL_CALC_WITHOUT_NDS = 11;

    protected static $_data = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'analytics_options';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'key'], 'required'],
            [['company_id', 'key'], 'integer'],
            [['value'], 'string', 'max' => 16],
            [['company_id', 'key'], 'unique', 'targetAttribute' => ['company_id', 'key']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'key' => 'Key',
            'value' => 'Value',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @param $companyId
     * @param $key
     * @param $value
     * @return int
     * @throws \yii\db\Exception
     */
    public static function setOption($companyId, $key, $value)
    {
        return Yii::$app->db->createCommand()->upsert(self::tableName(),
            ['company_id' => $companyId, 'key' => $key, 'value' => $value],
            ['value' => $value]
        )->execute();
    }

    /**
     * @param $companyId
     * @param $key
     * @return false|string|null
     */
    public static function getOption($companyId, $key)
    {
        if (!isset(self::$_data['getOption'][$companyId])) {
            self::$_data['getOption'][$companyId] = self::find()->where([
                'company_id' => $companyId,
            ])->select('value')->indexBy('key')->column();
        }

        return self::$_data['getOption'][$companyId][$key] ?? null;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function setPalResetButtonEnable($companyId)
    {
        return self::setOption($companyId, self::KEY_PAL_RESET_BUTTON_ENABLE, 1);
    }

    public static function setPalResetButtonDisable($companyId)
    {
        return self::setOption($companyId, self::KEY_PAL_RESET_BUTTON_ENABLE, 0);
    }

    public static function isPalResetButtonEnable($companyId)
    {
        return (bool)self::getOption($companyId, self::KEY_PAL_RESET_BUTTON_ENABLE);
    }
}
