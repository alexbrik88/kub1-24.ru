<?php

namespace frontend\modules\analytics\models;

use Yii;
use common\models\Company;
use common\models\employee\Employee;

/**
 * This is the model class for table "analytics_multi_company".
 *
 * @property int $employee_id
 * @property int $primary_company_id
 * @property int $company_id
 *
 * @property Company $company
 * @property Employee $employee
 * @property Company $primaryCompany
 */
class AnalyticsMultiCompany extends \yii\db\ActiveRecord
{
    const MIN_COMPANIES_COUNT = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'analytics_multi_company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employee_id', 'primary_company_id', 'company_id'], 'required'],
            [['employee_id', 'primary_company_id', 'company_id'], 'integer'],
            [['employee_id', 'primary_company_id', 'company_id'], 'unique', 'targetAttribute' => ['employee_id', 'primary_company_id', 'company_id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
            [['primary_company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['primary_company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'employee_id' => 'Employee ID',
            'primary_company_id' => 'Primary Company ID',
            'company_id' => 'Company ID',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * Gets query for [[Employee]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * Gets query for [[PrimaryCompany]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPrimaryCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'primary_company_id']);
    }

    public static function updateCompanies(array $companiesIds)
    {

    }
}
