<?php

namespace frontend\modules\analytics\models\credits;

use common\models\employee\Employee;
use RuntimeException;
use Yii;
use yii\base\Model;

class RepositoryFactory implements RepositoryFactoryInterface
{
    /**
     * @inheritDoc
     * @return RepositoryInterface|Model
     */
    public function createRepository(string $class): RepositoryInterface
    {
        if (!is_a($class, RepositoryInterface::class, true)) {
            throw new RuntimeException('Class not found.');
        }

        if (property_exists($class, 'company_id')) {
            /** @var Employee $employee */
            $employee = Yii::$app->user->identity;

            return new $class(['company_id' => $employee->company->id]);
        }

        return new $class;
    }
}
