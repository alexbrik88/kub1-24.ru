<?php

namespace frontend\modules\analytics\models\credits;

use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\company\CheckingAccountant;

class BankFlowForm extends AbstractFlowForm
{
    /**
     * @inheritDoc
     * @return CashBankFlows
     */
    protected function createCashFlow(): CashFlowsBase
    {
        /** @var CheckingAccountant $model */
        $checkingAccountant = (new RepositoryFactory)
            ->createRepository(CheckingAccountantRepository::class)
            ->getQuery()
            ->andWhere(['id' => $this->checking_accountant_id])
            ->one();

        return new CashBankFlows([
            'account_id' => $checkingAccountant->id,
            'rs' => $checkingAccountant->rs,
            'bank_name' => $checkingAccountant->bank_name,
        ]);
    }

    /**
     * @inheritDoc
     */
    protected function getWalletId(): int
    {
        return CashFlowsBase::WALLET_BANK;
    }

    /**
     * @inheritDoc
     */
    protected function getRules(): array
    {
        return [
            [['checking_accountant_id'], 'required'],
        ];
    }
}
