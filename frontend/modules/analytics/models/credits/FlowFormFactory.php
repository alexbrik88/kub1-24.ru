<?php

namespace frontend\modules\analytics\models\credits;

use common\components\date\DateHelper;
use common\models\cash\CashFlowsBase;
use common\models\employee\Employee;
use Yii;

use RuntimeException;

class FlowFormFactory
{
    /** @var string[] */
    public const FORM_LIST = [
        CashFlowsBase::WALLET_BANK => BankFlowForm::class,
        CashFlowsBase::WALLET_CASHBOX => OrderFlowForm::class,
        CashFlowsBase::WALLET_EMONEY => EmoneyFlowForm::class,
    ];

    /**
     * @param int $type
     * @param Credit $credit
     * @return AbstractFlowForm
     */
    public function modelCreate(int $type, Credit $credit): AbstractFlowForm
    {
        return $this->createForm($type, AbstractFlowForm::SCENARIO_CREATE, $credit);
    }

    /**
     * @param int $type
     * @param Credit $credit
     * @return AbstractFlowForm
     */
    public function modelUpdate(int $type, Credit $credit): AbstractFlowForm
    {
        return $this->createForm($type, AbstractFlowForm::SCENARIO_UPDATE, $credit);
    }

    /**
     * @param int $type
     * @param Credit $credit
     * @return AbstractFlowForm
     */
    public function modelDelete(int $type, Credit $credit): AbstractFlowForm
    {
        return $this->createForm($type, AbstractFlowForm::SCENARIO_DELETE, $credit);
    }

    /**
     * @param int $type
     * @param string $scenario
     * @param Credit $credit
     * @return AbstractFlowForm
     */
    private function createForm(int $type, string $scenario, Credit $credit): AbstractFlowForm
    {
        if (!isset(self::FORM_LIST[$type])) {
            throw new RuntimeException('Invalid type.');
        }

        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;
        $class = self::FORM_LIST[$type];
        /** @var AbstractFlowForm $form */
        $form = new $class([
            'scenario' => $scenario,
            'employee_id' => $employee->id,
            'company_id' => $employee->company->id,
            'contractor_id' => $credit->contractor_id,
            'credit_id' => $credit->credit_id,
            'credit_amount' => $credit->amountWithCurrency,
            'currency_id' => $credit->currency_id,
            'checking_accountant_id' => ($credit->wallet_type == CashFlowsBase::WALLET_BANK) ? $credit->wallet_id : null,
            'cashbox_id' => ($credit->wallet_type == CashFlowsBase::WALLET_CASHBOX) ? $credit->wallet_id : null,
            'emoney_id' => ($credit->wallet_type == CashFlowsBase::WALLET_EMONEY) ? $credit->wallet_id : null,
            'flow_type' => CashFlowsBase::FLOW_TYPE_INCOME,
            'date' => date(DateHelper::FORMAT_USER_DATE),
            'is_prepaid_expense' => false,
        ]);

        return $form;
    }
}
