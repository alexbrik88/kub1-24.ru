<?php

namespace frontend\modules\analytics\models\credits;

use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\ActiveQuery;

interface RepositoryInterface
{
    /**
     * @return ActiveDataProvider
     */
    public function getProvider(): ActiveDataProvider;

    /**
     * @return ActiveQuery
     */
    public function getQuery(): ActiveQuery;

    /**
     * @return Sort
     */
    public function getSort(): Sort;
}
