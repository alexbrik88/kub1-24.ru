<?php

namespace frontend\modules\analytics\models\credits;

use common\components\date\DateHelper;
use common\models\cash\Cashbox;
use common\models\cash\CashFlowsBase;
use common\models\cash\Emoney;
use common\models\cash\WalletInterface;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\employee\Employee;
use frontend\modules\analytics\behaviors\credits\CreditDebtBehavior;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * @property-read int $credit_id
 * @property int $credit_type Вид кредита
 * @property int $credit_status Статус кредита
 * @property float $credit_amount Сумма кредита
 * @property string $credit_first_date Дата получения
 * @property string $credit_last_date Дата последнего платежа (Дата погашения)
 * @property string $credit_day_count Количество дней
 * @property float $credit_percent_rate Ставка, %-годовых
 * @property float $credit_commission_rate Комиссия/Дисконт
 * @property float $credit_expiration_rate Ставка при просрочке
 * @property int $credit_year_length Число дней в году по договору
 * @property int $credit_tranche_depth Глубина транша (дней)
 * @property int $payment_type Вид платежа
 * @property int $payment_mode Тип ежемесячного платёжа
 * @property int $payment_day День платежа
 * @property int $payment_first Первый ежемесячный платеж
 * @property int $payment_amount Выплаты по основному долгу на дату уплаты процента
 * @property int $payment_debt Задолжность на текущую дату
 * @property string $payment_date Дата следующего платежа
 * @property int $wallet_id Идентификатор кошелька
 * @property int $wallet_type Тип кошелька
 * @property int $company_id Идентификатор компании
 * @property int $employee_id Идентификатор сотрудника
 * @property int $contractor_id Идентификатор контрагента
 * @property string $currency_id
 * @property int $agreement_number Номер договора
 * @property string $agreement_name Доп поле
 * @property string $agreement_date Дата договора
 * @property float $debt_amount Сумма долга
 * @property float $debt_repaid Выплаты основного долга
 * @property float $debt_diff Остаток долга
 * @property float $interest_amount Сумма процентов по долгу
 * @property float $interest_repaid Сумма выплаченные процентов по долгу
 * @property float $interest_diff Остаток процентов по долгу
 * @property-read string $created_at Дата создания записи
 * @property-read string $updated_at Дата обновления записи
 * @property-read Company $company
 * @property-read Employee $employee Сотрудник
 * @property-read Contractor $contractor
 * @property-read CreditDocument $document
 * @property-read CheckingAccountant|Cashbox|Emoney $wallet
 * @property-read CheckingAccountant $checkingAccountant
 * @property-read Cashbox $cashbox
 * @property-read Currency $currency
 * @property-read Emoney $emoney
 * @property-read string $statusTitle Название статуса кредита
 * @property-read string $typeTitle Название типа кредита
 * @property-read string $contractorTitle Название кредитора
 * @property-read string $walletTitle Название кошелька
 * @property-read string $paymentTypeTitle Название вида платежа
 * @property-read string $paymentModeTitle Название типа ежемесячного платёжа
 * @property-read string $amount Сумма кредита
 * @property-read string $amountWithCurrency
 * @property-read string $currencyName
 * @property-read int $daysLeft
 * @property-read CreditFlow[] $creditFlows
 */
class Credit extends ActiveRecord
{
    /** @var mixed[] */
    public const DECIMAL_FORMAT = ['decimal', 2];

    /** @var mixed[] */
    public const PERCENT_FORMAT = ['decimal', 2];

    /** @var float */
    public const EMPTY_RATE = 0.0;

    /** @var int Кредит */
    public const TYPE_CREDIT = 1;

    /** @var int Займ */
    public const TYPE_LOAN = 2;

    /** @var int Овердрафт */
    public const TYPE_OVERDRAFT = 3;

    /** @var int Действует */
    public const STATUS_ACTIVE = 1;

    /** @var int Просрочен */
    public const STATUS_EXPIRED = 2;

    /** @var int Погашен */
    public const STATUS_ARCHIVE = 0;

    /** @var int В конце срока */
    public const PAYMENT_TYPE_AT_END = 1;

    /** @var int Дифференцированный */
    public const PAYMENT_TYPE_DIFFERENTIATED = 2;

    /** @var int Аннуитетный */
    public const PAYMENT_TYPE_ANNUITY = 3;

    public const PAYMENT_TYPE_END_WITH_PERCENTS = 4;

    /** @var int Первый платеж стандартный */
    public const PAYMENT_FIRST_DEFAULT = 0;

    /** @var int Первый платеж только % */
    public const PAYMENT_FIRST_ONLY_PERCENT = 1;

    /** @var int Платеж со второго месяца */
    public const PAYMENT_FIRST_SKIP = 2;

    /** @var int Последний день месяца */
    public const PAYMENT_DAY_LAST = 0;

    /** @var int Последний день месяца */
    public const PAYMENT_MODE_DEFAULT = 1;

    /** @var int Число месяца получения кредита */
    public const PAYMENT_MODE_CREDIT = 2;

    /** @var int Число месяца определено по договору */
    public const PAYMENT_MODE_AGREEMENT = 3;

    /** @var int */
    public const YEAR_LENGTH_DEFAULT = 0;

    /** @var int */
    public const YEAR_LENGTH_365 = 365;

    /** @var int */
    public const YEAR_LENGTH_360 = 360;

    /** @var string[] Виды кредита */
    public const TYPE_LIST = [
        self::TYPE_CREDIT => 'Кредит',
        self::TYPE_LOAN => 'Займ',
        self::TYPE_OVERDRAFT => 'Овердрафт',
    ];

    /** @var string[] Статусы кредита */
    public const STATUS_LIST = [
        self::STATUS_ACTIVE => 'Действует',
        self::STATUS_EXPIRED => 'Просрочен',
        self::STATUS_ARCHIVE => 'Погашен',
    ];

    /** @var string[] */
    public const YEAR_LENGTH_LIST = [
        self::YEAR_LENGTH_DEFAULT => 'По календарю',
        self::YEAR_LENGTH_365 => '365 дней',
        self::YEAR_LENGTH_360 => '360 дней',
    ];

    /** @var string[] Виды платежа */
    public const PAYMENT_TYPE_LIST = [
        self::PAYMENT_TYPE_AT_END => 'В конце срока кредит',
        self::PAYMENT_TYPE_END_WITH_PERCENTS => 'В конце срока кредит и %% по кредиту',
        self::PAYMENT_TYPE_DIFFERENTIATED => 'Дифференцированный',
        self::PAYMENT_TYPE_ANNUITY => 'Аннуитетный',
    ];

    /** @var string[] Виды первого ежемесячного платежа */
    public const PAYMENT_FIRST_LIST = [
        self::PAYMENT_FIRST_DEFAULT => 'Стандартно',
        self::PAYMENT_FIRST_ONLY_PERCENT => 'Первый платеж только %',
        self::PAYMENT_FIRST_SKIP => 'Платеж со второго месяца',
    ];

    /** @var string[] Виды ежемесячного платежа */
    public const PAYMENT_MODE_LIST = [
        self::PAYMENT_MODE_DEFAULT => 'Последний день месяца',
        self::PAYMENT_MODE_CREDIT => 'Число месяца получения кредита',
        self::PAYMENT_MODE_AGREEMENT => 'Число месяца определено по договору',
    ];

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'creditDebt' => [
                'class' => CreditDebtBehavior::class,
                'credit' => $this,
            ],
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
            'attribute' => [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    self::EVENT_INIT => [
                        'debt_amount',
                        'debt_repaid',
                        'debt_diff',
                        'interest_amount',
                        'interest_repaid',
                        'interest_diff',
                    ],
                ],
                'value' => 0.0,
            ],
        ];
    }

    /**
     * @inheritDoc
     * @deprecated
     */
    public function extraFields()
    {
        return [
            'amount',
            'currencyName',
            'amountWithCurrency',
        ];
    }

    /**
     * @return string
     */
    public function getAmount(): string
    {
        return Yii::$app->formatter->format($this->credit_amount, self::DECIMAL_FORMAT);
    }

    /**
     * @return string
     */
    public function getAmountWithCurrency(): string
    {
        return sprintf('%s %s', $this->getAmount(), $this->getCurrencyName());
    }

    /**
     * @return string
     */
    public function getCurrencyName(): string
    {
        return $this->currency->name;
    }

    /**
     * @return ActiveQuery
     */
    public function getContractor(): ActiveQuery
    {
        return $this->hasOne(Contractor::class, ['id' => 'contractor_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCompany(): ActiveQuery
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCurrency(): ActiveQuery
    {
        return $this->hasOne(Currency::class, ['id' => 'currency_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEmployee(): ActiveQuery
    {
        return $this->hasOne(Employee::class, ['id' => 'employee_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDocument(): ActiveQuery
    {
        return $this->hasOne(CreditDocument::class, ['credit_id' => 'credit_id']);
    }

    /**
     * @return string
     */
    public function getContractorTitle(): string
    {
        return $this->contractor->nameWithType;
    }

    /**
     * @return string
     */
    public function getPaymentModeTitle(): string
    {
        return self::PAYMENT_MODE_LIST[$this->payment_mode];
    }

    /**
     * @return string
     */
    public function getPaymentTypeTitle(): string
    {
        return self::PAYMENT_TYPE_LIST[$this->payment_type];
    }

    /**
     * @return string
     */
    public function getStatusTitle(): string
    {
        return self::STATUS_LIST[$this->credit_status];
    }

    /**
     * @return string
     */
    public function getTypeTitle(): string
    {
        return self::TYPE_LIST[$this->credit_type];
    }

    /**
     * @return string
     */
    public function getWalletTitle(): string
    {
        return $this->wallet->getWalletName();
    }

    /**
     * @return CheckingAccountant|Cashbox|Emoney
     */
    public function getWallet(): WalletInterface
    {
        $types = [
            CashFlowsBase::WALLET_BANK => 'checkingAccountant',
            CashFlowsBase::WALLET_CASHBOX => 'cashbox',
            CashFlowsBase::WALLET_EMONEY => 'emoney',
        ];

        $attribute = $types[$this->wallet_type];

        return $this->$attribute;
    }

    /**
     * @return ActiveQuery
     */
    public function getCheckingAccountant(): ActiveQuery
    {
        return $this->hasOne(CheckingAccountant::class, ['id' => 'wallet_id', 'company_id' => 'company_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCashbox(): ActiveQuery
    {
        return $this->hasOne(Cashbox::class, ['id' => 'wallet_id', 'company_id' => 'company_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEmoney(): ActiveQuery
    {
        return $this->hasOne(Emoney::class, ['id' => 'wallet_id', 'company_id' => 'company_id']);
    }

    /**
     * @return int
     */
    public function getDaysLeft(): int
    {
        $interval = Date::createFromFormat(DateHelper::FORMAT_DATE, $this->credit_last_date)->diff(new Date);
        $days = ($interval->invert > 0) ? $interval->days : -($interval->days);

        return $days;
    }

    /**
     * @return ActiveQuery
     */
    public function getCreditFlows(): ActiveQuery
    {
        return $this->hasMany(CreditFlow::class, ['credit_id' => 'credit_id']);
    }

    /**
     * @return string
     */
    public function getAgreementTitle(): string
    {
        return sprintf(
            'Договор № %s от %s',
            $this->agreement_number,
            DateHelper::format($this->agreement_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)
        );
    }

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%credit}}';
    }

}
