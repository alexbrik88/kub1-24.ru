<?php

namespace frontend\modules\analytics\models\credits;

use common\db\SortBuilder;
use common\models\cash\CashFlowsBase;
use frontend\components\PageSize;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

class CreditFlowRepository extends Model implements RepositoryInterface
{
    /** @var string[] */
    public const WALLET_LIST = [
        CashFlowsBase::WALLET_BANK => 'Банк',
        CashFlowsBase::WALLET_CASHBOX => 'Касса',
        CashFlowsBase::WALLET_EMONEY => 'E-money',
    ];

    /**
     * @var int
     */
    public $company_id;

    /**
     * @var int
     */
    public $credit_id;

    /**
     * @var int
     */
    public $flow_type;

    /**
     * @var int
     */
    public $wallet_id;

    /**
     * @var int
     */
    public $item_id;

    /**
     * @var string|null
     */
    public $search;

    /**
     * @var int
     */
    public $defaultPageSize = 100;

    /**
     * @var array
     */
    private $_invoiceItems;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->company_id)) {
            throw new InvalidConfigException();
        }

        $factory = new ListFactory();
        $incomeItems = $factory->createList(IncomeItemRepository::class)->getItems();
        $expenditureItems = $factory->createList(ExpenditureItemRepository::class)->getItems();
        $this->_invoiceItems = ArrayHelper::merge($incomeItems, $expenditureItems);
        asort($this->_invoiceItems);
    }

    /**
     * @inheritDoc
     */
    public function getProvider($pagination = false): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $this->getQuery()->with(['cashBankFlow', 'cashOrderFlow', 'cashEmoneyFlow']),
            'sort' => $this->getSort(),
            'pagination' => ($pagination)
                ? ['pageSize' => PageSize::get()]
                : false
        ]);
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['flow_type'], 'in', 'range' => array_keys(CashFlowsBase::getFlowTypes())],
            [['wallet_id'], 'in', 'range' => array_keys(self::WALLET_LIST)],
            [['item_id'], 'in', 'range' => array_keys($this->getInvoiceItems())],
            [['search'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getQuery(): ActiveQuery
    {
        $query = CreditFlow::find()
            ->joinWith(['flow', 'credit'])
            ->andFilterWhere([
                Credit::tableName() . '.company_id' => $this->company_id,
                Credit::tableName() . '.credit_id' => $this->credit_id,
                CreditFlow::tableName() . '.wallet_id' => $this->wallet_id,
                Flow::tableName() . '.item_id' => $this->item_id,
                Flow::tableName() . '.type' => $this->flow_type,
            ]);

        if (strlen($this->search) > 0) {
            $query
                ->joinWith(['cashBankFlow', 'cashOrderFlow', 'cashEmoneyFlow'])
                ->andWhere(['or',
                    ['like', 'cash_bank_flows.description', $this->search],
                    ['like', 'cash_order_flows.description', $this->search],
                    ['like', 'cash_emoney_flows.description', $this->search],
                ]);
        }

        return $query;
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        $builder = new SortBuilder();
        $builder->addOrder('flow.date', Flow::tableName(), 'date');
        $builder->addOrder('flow.incomeAmount', Flow::tableName(), 'amount');
        $builder->addOrder('flow.expenseAmount', Flow::tableName(), 'amount');
        $builder->addOrders(
            ['flow.date', 'flow.incomeAmount', 'flow.expenseAmount'],
            CreditFlow::tableName(),
            'credit_flow_id'
        );
        $builder->addDefaultOrder('flow.date', SORT_DESC);

        return $builder->buildSort();
    }

    /**
     * @return string[]
     */
    public function getInvoiceItems(): array
    {
        return $this->_invoiceItems;
    }

    /**
     * @return int
     */
    public function getAllCount(): int
    {
        return CreditFlow::find()
            ->andWhere(['credit_id' => $this->credit_id])
            ->count();
    }
}
