<?php

namespace frontend\modules\analytics\models\credits;

use common\models\Contractor;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\ActiveQuery;

class ContractorRepository extends Model implements RepositoryInterface, ListInterface
{
    /**
     * @var int
     */
    public $company_id;

    /**
     * @var int
     */
    public $status = Contractor::ACTIVE;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->company_id)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function getItems(): array
    {
        $data = [];

        /** @var Contractor $model */
        foreach ($this->getProvider()->getModels() as $model) {
            $data[$model->id] = $model->nameWithType;
        }

        return $data;
    }

    /**
     * @inheritDoc
     */
    public function getProvider(): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $this->getQuery(),
            'sort' => $this->getSort(),
            'pagination' => false,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getQuery(): ActiveQuery
    {
        return Contractor::find()
            ->andWhere([Contractor::tableName() . '.company_id' => $this->company_id])
            ->andWhere([Contractor::tableName() . '.is_deleted' => false])
            ->andFilterWhere([Contractor::tableName() . '.status' => $this->status]);
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        return new Sort(['defaultOrder' => ['name' => SORT_ASC]]);
    }
}
