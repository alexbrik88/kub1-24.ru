<?php

namespace frontend\modules\analytics\models\credits;

use common\models\ICompanyStrictQuery;
use yii\db\ActiveQuery;

class CreditDocumentQuery extends ActiveQuery implements ICompanyStrictQuery
{
    /**
     * @inheritDoc
     */
    public function byCompany($companyId)
    {
        return $this->andWhere([Credit::tableName() . '.company_id' => $companyId]);
    }
}
