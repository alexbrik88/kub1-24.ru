<?php

namespace frontend\modules\analytics\models\credits;

use common\components\date\DateHelper;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\ActiveQuery;
use yii\db\Expression;

class CreditRepository extends Model implements RepositoryInterface, ListInterface
{
    /**
     * @var int
     */
    public $company_id;

    /**
     * @var int|null
     */
    public $contractor_id;

    /**
     * @var int|null
     */
    public $credit_status;

    /**
     * @var int|null
     */
    public $credit_type;

    /**
     * @var string
     */
    public $dateFrom;

    /**
     * @var string
     */
    public $dateTo;

    /**
     * @var bool
     */
    public $onlyDeletable = false;

    /**
     * @var string
     */
    public $search;

    /**
     * @var int
     */
    public $currency_id;

    /**
     * @var int
     */
    public $defaultPageSize = 100;

    /**
     * @var int[]
     */
    public array $onlyStatuses = [];

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->company_id)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @return int
     */
    public function getAllCount(): int
    {
        return Credit::find()->andWhere(['company_id' => $this->company_id])->count();
    }

    /**
     * @return float[]
     * @throws
     */
    public function getSummary()
    {
        $expression = new Expression('
            COUNT(credit.credit_id) count,
            SUM(credit.debt_amount) debt_amount,
            SUM(credit.debt_diff) debt_diff,
            SUM(credit.debt_repaid) debt_repaid,
            SUM(credit.interest_amount) interest_amount,
            SUM(credit.interest_diff) interest_diff,
            SUM(credit.interest_repaid) interest_repaid,
            SUM(credit.payment_amount) payment_amount,
            SUM(credit.payment_debt) payment_debt
        ');

        return array_map(function (?float $value) : float {
            if ($value === null) {
                return 0.0;
            }

            return round($value, 2, PHP_ROUND_HALF_UP);
        }, $this->getQuery()->select($expression)->createCommand()->queryOne());
    }

    /**
     * @inheritDoc
     */
    public function getItems(): array
    {
        $provider = new ActiveDataProvider([
            'query' => $this->getQuery(),
            'pagination' => false,
        ]);
        $items = [];

        /** @var Credit $model */
        foreach ($provider->getModels() as $model) {
            $items[$model->credit_id] = $model->getAgreementTitle();
        }

        return $items;
    }

    /**
     * @inheritDoc
     */
    public function getProvider(): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $this->getQuery()->with(['cashbox', 'checkingAccountant', 'currency']),
            'sort' => $this->getSort(),
            'pagination' => ['defaultPageSize' => $this->defaultPageSize],
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getQuery(): ActiveQuery
    {
        $query = Credit::find()
            ->andWhere(['company_id' => $this->company_id])
            ->andFilterWhere([
                'credit_status' => $this->credit_status,
                'credit_type' => $this->credit_type,
                'contractor_id' => $this->contractor_id,
            ])->andFilterWhere(['in', 'credit_status', $this->onlyStatuses]);

        /**
         * Цитата из ТЗ:
         * Отображаем все кредиты, которые не погашены в заданном периоде.
         * Т.е. если хотя бы один день из заданного периода цепляет период погашенного кредита, то его выводим.
         * И выводим ВСЕ кредиты, у которых статус "Просрочен" независимо от даты их погашения.
         */
        if (!empty($this->dateFrom) && !empty($this->dateTo)) {
            $begin = $this->dateFrom . ' 00:00:00';
            $end = $this->dateTo . ' 23:59:59';
            $condition = [
                'or',
                ['between', 'credit_first_date', $begin, $end],
                ['between', 'credit_last_date', $begin, $end],
                ['and', ['<', 'credit_first_date', $begin], ['>', 'credit_last_date', $end]]
            ];

            if (empty($this->credit_status)) {
                $condition[] = ['credit_status' => Credit::STATUS_EXPIRED];
            }

            $query->andWhere($condition);
        }

        if (!empty($this->search)) {
            $query->andWhere(['agreement_number' => $this->search]);
        }

        if ($this->onlyDeletable) {
            $on = [CreditFlow::tableName() . '.credit_id', Credit::tableName(). '.credit_id'];
            $query
                ->leftJoin(CreditFlow::tableName(), implode(' = ', $on))
                ->andWhere([CreditFlow::tableName() . '.credit_flow_id' => null]);
        }

        return $query;
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        return new Sort(['defaultOrder' => 'created_at']);
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['credit_status'], 'in', 'range' => array_keys(Credit::STATUS_LIST)],
            [['credit_type'], 'in', 'range' => array_keys(Credit::TYPE_LIST)],
            [['contractor_id'], 'integer', 'min' => 1],
            [['currency_id'], 'integer', 'min' => 1],
            [['search'], 'string', 'max' => 64],
        ];
    }
}
