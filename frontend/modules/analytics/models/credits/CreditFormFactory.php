<?php

namespace frontend\modules\analytics\models\credits;

use common\components\date\DateHelper;
use common\models\cash\CashFlowsBase;
use common\models\currency\Currency;
use common\models\employee\Employee;
use Yii;

class CreditFormFactory
{
    /**
     * @return CreditForm
     */
    public function modelCreate(): CreditForm
    {
        return $this->createForm(CreditForm::SCENARIO_CREATE);
    }

    /**
     * @return CreditForm
     */
    public function modelUpdate(): CreditForm
    {
        return $this->createForm(CreditForm::SCENARIO_UPDATE);
    }

    /**
     * @param string $scenario
     * @return CreditForm
     */
    private function createForm(string $scenario): CreditForm
    {
        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;
        /** @var CheckingAccountantRepository $repository */
        $repository = (new RepositoryFactory)->createRepository(CheckingAccountantRepository::class);

        return new CreditForm([
            'scenario' => $scenario,
            'company_id' => $employee->company->id,
            'employee_id' => $employee->id,
            'credit_type' => Credit::TYPE_CREDIT,
            'credit_status' => Credit::STATUS_ACTIVE,
            'payment_type' => Credit::PAYMENT_TYPE_AT_END,
            'payment_first' => Credit::PAYMENT_FIRST_DEFAULT,
            'payment_day' => Credit::PAYMENT_DAY_LAST,
            'payment_mode' => Credit::PAYMENT_MODE_DEFAULT,
            'credit_year_length' => 0,
            'credit_commission_rate' => Credit::EMPTY_RATE,
            'credit_expiration_rate' => Credit::EMPTY_RATE,
            'credit_tranche_depth' => 0,
            'agreement_date' => date(DateHelper::FORMAT_USER_DATE),
            'wallet_value' => sprintf('%d_%d', CashFlowsBase::WALLET_BANK, $repository->getMain()->id),
            'currency_id' => Currency::DEFAULT_ID,
        ]);
    }
}
