<?php

namespace frontend\modules\analytics\models\credits;

use common\components\date\DateHelper;
use common\models\cash\CashFlowsBase;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\validators\InlineValidator;
use Yii;

/**
 * @property Credit $credit
 */
class CreditForm extends Model
{
    use FormListItemsTrait;

    /** @var string */
    public const SCENARIO_CREATE = 'create';

    /** @var string */
    public const SCENARIO_UPDATE = 'update';

    /** @var string */
    private const DATE_FORMAT = 'php:d.m.Y';

    /** @var string[] */
    public const WALLET_LIST = [
        CashFlowsBase::WALLET_BANK => 'checking_accountant_id',
        CashFlowsBase::WALLET_CASHBOX => 'cashbox_id',
        CashFlowsBase::WALLET_EMONEY => 'emoney_id',
    ];

    /**
     * @var int
     */
    public $company_id;

    /**
     * @var int
     */
    public $employee_id;

    /**
     * @var string
     */
    public $currency_id;

    /**
     * @var int
     */
    public $credit_type;

    /**
     * @var int
     */
     public $credit_status;

    /**
     * @var int
     */
    public $credit_amount;

    /**
     * @var string
     */
    public $credit_first_date;

    /**
     * @var string
     */
    public $credit_last_date;

    /**
     * @var float
     */
    public $credit_percent_rate;

    /**
     * @var float
     */
    public $credit_commission_rate;

    /**
     * @var float
     */
    public $credit_expiration_rate;

    /**
     * @var int
     */
    public $credit_year_length;

    /**
     * @var int
     */
    public $credit_tranche_depth;

    /**
     * @var int
     */
    public $payment_type;

    /**
     * @var int
     */
    public $payment_day;

    /**
     * @var int
     */
    public $payment_mode;

    /**
     * @var int
     */
    public $payment_first;

    /**
     * @var string
     */
    public $wallet_value;

    /**
     * @var int
     */
    public $contractor_id;

    /**
     * @var int
     */
    public $agreement_number;

    /**
     * @var string
     */
    public $agreement_name;

    /**
     * @var string
     */
    public $agreement_date;

    /**
     * @var Credit
     */
    private $_credit;

    /**
     * @var string[]
     */
    private $dates = ['credit_first_date', 'credit_last_date', 'agreement_date'];

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->company_id)) {
            throw new InvalidConfigException();
        }

        $this->_credit = new Credit();
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'credit_type' => 'Вид кредита',
            'credit_status' => 'Статус кредита',
            'credit_amount' => 'Сумма кредита',
            'credit_first_date' => 'Дата получения',
            'credit_last_date' => 'Дата погашения',
            'credit_percent_rate' => 'Ставка, %-годовых',
            'credit_commission_rate' => 'Комиссия/Дисконт',
            'credit_expiration_rate' => 'Ставка при просрочке',
            'credit_year_length' => 'Число дней в году по договору',
            'credit_tranche_depth' => 'Глубина транша (дней)',
            'payment_type' => 'Вид платежа по кредиту',
            'payment_day' => 'День платежа',
            'payment_mode' => 'Ежемесячные платежи',
            'payment_first' => 'Первый ежемесячный платеж',
            'wallet_value' => 'Счет для зачисления',
            'contractor_id' => 'Кредитор',
            'agreement_number' => 'Номер договора',
            'agreement_name' => 'Доп. поле',
            'agreement_date' => 'Дата договора',
            'checking_accountant_id' => 'Расчётные счёта',
            'cashbox_id' => 'Кассы',
            'emoney_id' => 'E-money',
            'currency_id' => 'Валюта',
        ];
    }

    /**
     * @param mixed $id
     * @return bool
     */
    public function loadModel($id): bool
    {
        if (is_scalar($id) && !ctype_digit($id)) {
            return false;
        }

        /** @var Credit $credit */
        $credit = (new RepositoryFactory)
            ->createRepository(CreditRepository::class)
            ->getQuery()
            ->andWhere(['credit_id' => $id])
            ->one();

        if ($credit) {
            $this->_credit = $credit;
            $this->setAttributes($credit->getAttributes(), false);
            $this->loadWalletValue();
            array_walk($this->dates, [$this, 'loadDate']);

            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function saveModel(): bool
    {
        $attributes = $this->getAttributes($this->activeAttributes());
        $this->credit->setAttributes($attributes, false);
        $this->credit->payment_day = $this->getPaymentDay();
        $this->credit->credit_day_count = $this->getCreditDayCount();
        $this->credit->company_id = $this->company_id;
        $this->credit->employee_id = $this->employee_id;
        $this->saveWalletValue();

        if ($this->credit_type != Credit::TYPE_LOAN) {
            $this->credit->credit_tranche_depth = 0;
        }

        array_walk($this->dates, [$this, 'saveDate']);

        if ($this->credit->save(false)) {
            $this->credit->contractor->opposite = true;
            $this->credit->contractor->save(false);

            return true;
        }

        return false;
    }

    /**
     * @return bool
     * @throws
     */
    public function removeModel(): bool
    {
        /** @var CreditFlowRepository $repository */
        $repository = (new RepositoryFactory)->createRepository(CreditFlowRepository::class);
        $repository->credit_id = $this->credit->credit_id;

        if ($repository->getProvider()->getTotalCount() > 0) {
            return false;
        }

        return $this->credit->delete();
    }

    /**
     * @return Credit
     */
    public function getCredit(): Credit
    {
        return $this->_credit;
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        $when = function (): bool {
            return ($this->payment_mode == Credit::PAYMENT_MODE_AGREEMENT);
        };

        return [
            [['credit_type'], 'required'],
            [['credit_amount'], 'required'],
            [['credit_first_date'], 'required'],
            [['credit_last_date'], 'required'],
            [['credit_percent_rate'], 'required'],
            [['credit_commission_rate'], 'required'],
            [['credit_expiration_rate'], 'required'],
            [['credit_year_length'], 'required'],
            [['credit_tranche_depth'], 'required'],
            [['payment_type'], 'required'],
            [['payment_day'], 'required', 'when' => $when],
            [['payment_first'], 'required'],
            [['wallet'], 'required'],
            [['agreement_number'], 'required'],
            [['agreement_date'], 'required'],
            [['contractor_id'], 'required'],
            [['currency_id'], 'required'],

            [['credit_type'], 'in', 'range' => array_keys(Credit::TYPE_LIST)],
            [['credit_amount'], 'double', 'min' => 0],
            [['credit_first_date'], 'date', 'format' => self::DATE_FORMAT],
            [['credit_last_date'], 'date', 'format' => self::DATE_FORMAT],
            [['credit_percent_rate'], 'double', 'min' => 0],
            [['credit_commission_rate'], 'double', 'min' => 0],
            [['credit_expiration_rate'], 'double', 'min' => 0],
            [['credit_year_length'], 'in', 'range' => array_keys(Credit::YEAR_LENGTH_LIST)],
            [['credit_tranche_depth'], 'integer', 'min' => 0],
            [['payment_type'], 'in', 'range' => array_keys(Credit::PAYMENT_TYPE_LIST)],
            [['payment_mode'], 'in', 'range' => array_keys(Credit::PAYMENT_MODE_LIST)],
            [['payment_day'], 'integer', 'min' => 1, 'max' => 31, 'when' => $when, 'enableClientValidation' => false],
            [['payment_first'], 'in', 'range' => array_keys(Credit::PAYMENT_FIRST_LIST)],
            [['payment_mode'], 'in', 'range' => array_keys(Credit::PAYMENT_MODE_LIST)],
            [['wallet_value'], 'match', 'pattern' => '/^([0-9]+)[_]([0-9]+)$/'],
            [['wallet_value'], 'validateWalletValue'],
            [['agreement_number'], 'integer', 'min' => 1],
            [['agreement_name'], 'string', 'max' => 64],
            [['agreement_name'], 'trim'],
            [['agreement_name'], 'match', 'pattern' => '#^([А-ЯЁ0-9_/-]+)$#ui'],
            [['agreement_date'], 'date', 'format' => self::DATE_FORMAT],
            [['contractor_id'], 'integer'],
            [['contractor_id'], 'in', 'range' => array_keys($this->getItems('contractor_id')), 'enableClientValidation' => false],
            [['currency_id'], 'in', 'range' => array_keys($this->getItems('currency_id'))],
            [['credit_last_date'], 'validateCreditLastDate'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => [
                'credit_type',
                'credit_amount',
                'credit_first_date',
                'credit_last_date',
                'credit_percent_rate',
                'credit_commission_rate',
                'credit_expiration_rate',
                'credit_year_length',
                'credit_tranche_depth',
                'payment_type',
                'payment_day',
                'payment_mode',
                'payment_first',
                'wallet_value',
                'contractor_id',
                'agreement_number',
                'agreement_name',
                'agreement_date',
                // TODO: 'currency_id',
            ],
            self::SCENARIO_UPDATE => [
                'credit_type',
                'credit_amount',
                'credit_first_date',
                'credit_last_date',
                'credit_percent_rate',
                'credit_commission_rate',
                'credit_expiration_rate',
                'credit_year_length',
                'credit_tranche_depth',
                'payment_type',
                'payment_day',
                'payment_mode',
                'payment_first',
                'wallet_value',
                'contractor_id',
                'agreement_number',
                'agreement_name',
                'agreement_date',
            ],
        ];
    }

    /**
     * @param int $type
     * @return string[]
     */
    public function getWalletItems(int $type): array
    {
        $attribute = self::WALLET_LIST[$type];
        $items = [];

        foreach ($this->getItems($attribute) as $key => $value) {
            $items[implode('_', [$type, $key])] = $value;
        }

        return $items;
    }

    /**
     * @param string $attribute
     * @return void
     */
    public function loadDate(string $attribute): void
    {
        $value = $this->credit->getAttribute($attribute);

        if (!empty($value)) {
            $this->$attribute = DateHelper::format($value, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        }
    }

    /**
     * @return void
     */
    public function loadWalletValue(): void
    {
        $this->wallet_value = sprintf('%d_%d', $this->credit->wallet_type, $this->credit->wallet_id);
    }

    /**
     * @param string $attribute
     * @return void
     */
    public function saveDate(string $attribute): void
    {
        $value = $this->$attribute;

        if (!empty($value)) {
            $this->credit->setAttribute(
                $attribute,
                DateHelper::format($this->$attribute, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE)
            );
        }
    }

    /**
     * @return void
     */
    public function saveWalletValue(): void
    {
        list($this->credit->wallet_type, $this->credit->wallet_id) = explode('_', $this->wallet_value);
    }

    /**
     * @param string $attribute
     * @param mixed[]|null $params
     * @param InlineValidator $validator
     * @return void
     */
    public function validateCreditLastDate(string $attribute, ?array $params, InlineValidator $validator): void
    {
        if (!$this->hasErrors('credit_first_date') && !$this->hasErrors('credit_last_date')) {
            $min = DateHelper::format($this->credit_first_date, 'U', DateHelper::FORMAT_USER_DATE);
            $max = DateHelper::format($this->credit_last_date, 'U', DateHelper::FORMAT_USER_DATE);

            if ($min >= $max) {
                $message = Yii::t(
                    'yii',
                    '{attribute} must be greater than "{compareValueOrAttribute}".',
                    ['compareValueOrAttribute' => $this->getAttributeLabel('credit_first_date')]
                );
                $validator->addError($this, $attribute, $message);
            }
        }
    }

    /**
     * @param string $attribute
     * @param mixed[]|null $params
     * @param InlineValidator $validator
     * @return void
     */
    public function validateWalletValue(string $attribute, ?array $params, InlineValidator $validator): void
    {
        if (!$this->hasErrors($attribute)) {
            $type = explode('_', $this->wallet_value)[0];

            if (isset(self::WALLET_LIST[$type]) && array_key_exists($this->$attribute, $this->getWalletItems($type))) {
                return;
            }

            $message = Yii::t('yii', '{attribute} is invalid.');
            $validator->addError($this, $attribute, $message);
        }
    }

    /**
     * Получить день платежа
     *
     * @return int
     */
    private function getPaymentDay(): int
    {
        if ($this->payment_mode == Credit::PAYMENT_MODE_AGREEMENT) {
            return $this->payment_day;
        }

        if ($this->payment_mode == Credit::PAYMENT_MODE_CREDIT) {
            return explode('.', $this->credit_first_date)[0];
        }

        return Credit::PAYMENT_DAY_LAST;
    }

    /**
     * @return int
     * @throws
     */
    private function getCreditDayCount(): int
    {
        $first = Date::createFromFormat(DateHelper::FORMAT_USER_DATE, $this->credit_first_date);
        $last = Date::createFromFormat(DateHelper::FORMAT_USER_DATE, $this->credit_last_date);

        return $last->diff($first)->days;
    }
}
