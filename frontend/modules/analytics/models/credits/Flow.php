<?php

namespace frontend\modules\analytics\models\credits;

use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceIncomeItem as Income;
use common\models\document\InvoiceExpenditureItem as Expense;
use yii\db\ActiveRecord;

/**
 * @property-read int $amount
 * @property-read int $type
 * @property-read int $wallet
 * @property-read int|null $item_id
 * @property-read float $incomeAmount
 * @property-read float $expenseAmount
 * @property-read string $date
 */
class Flow extends ActiveRecord
{
    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%olap_flows}}';
    }

    /**
     * @return float
     */
    public function getIncomeAmount(): float
    {
        if (($this->type == CashFlowsBase::FLOW_TYPE_INCOME)) {
            return $this->amount / 100;
        }

        return 0.0;
    }

    /**
     * @return float
     */
    public function getExpenseAmount(): float
    {
        if (($this->type == CashFlowsBase::FLOW_TYPE_EXPENSE)) {
            return $this->amount / 100;
        }

        return 0.0;
    }

    /**
     * @return bool
     */
    public function isCreditBody(): bool
    {
        if (($this->type == CashFlowsBase::FLOW_TYPE_INCOME))
            if (in_array($this->item_id, [Income::ITEM_CREDIT, Income::ITEM_LOAN]))
                return true;

        if (($this->type == CashFlowsBase::FLOW_TYPE_EXPENSE))
            if (in_array($this->item_id, [Expense::ITEM_REPAYMENT_CREDIT, Expense::ITEM_LOAN_REPAYMENT]))
                return true;

        return false;
    }

    /**
     * @return bool
     */
    public function isCreditPercent(): bool
    {
        if (($this->type == CashFlowsBase::FLOW_TYPE_EXPENSE))
            if ($this->item_id == Expense::ITEM_PAYMENT_PERCENT)
                return true;

        return false;
    }
}
