<?php

namespace frontend\modules\analytics\models\credits;

use common\models\cash\CashFlowsBase;
use common\models\company\CheckingAccountant;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use frontend\modules\analytics\models\PlanCashFlows;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

class DebtPlanning extends Model
{
    /**
     * @var Credit
     */
    public $credit;

    /**
     * @var DebtPeriod[]
     */
    public $periods;

    /**
     * @var CheckingAccountant
     */
    public $mainCheckingAccountant;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->credit)) {
            throw new InvalidConfigException();
        }

        $calculator = (new DebtCalculatorFactory())->createCalculator($this->credit);
        $this->periods = $calculator->schedule->periods;

        if (empty($this->periods) || !is_array($this->periods)) {
            throw new InvalidConfigException();
        }

        $this->mainCheckingAccountant = $this->credit->company->mainCheckingAccountant;

        if (!($this->mainCheckingAccountant instanceof CheckingAccountant)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function createPlanFlows()
    {
        $this->_createIncomeFlowReceipt(); // полученный кредит
        $this->_createExpenseFlowsRepaid();  // платежи по основному долгу
        $this->_createExpenseFlowsInterest();  // платежи по процентам
    }

    /**
     * @inheritDoc
     */
    public function updatePlanFlows()
    {
        if ($this->deletePlanFlows()) {
            $this->createPlanFlows();
        } else {
            throw new Exception('Невозможно удалить плановые операции!');
        }
    }

    /**
     * @inheritDoc
     */
    public function deletePlanFlows()
    {
        $flows = PlanCashFlows::find()
            ->where(['credit_id' => $this->credit->credit_id])
            ->andWhere(['company_id' => $this->credit->company_id])
            ->all();

        foreach ($flows as $flow) {
            if (!$flow->delete())
                return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    private function _createIncomeFlowReceipt()
    {
        if ($this->credit->credit_first_date < date('Y-m-d'))
            return;

        $this->_createPlanFlow([
            'date' => $this->credit->credit_first_date,
            'amount' => $this->credit->credit_amount,
            'flow_type' => CashFlowsBase::FLOW_TYPE_INCOME,
            'description' => CreditLabelHelper::getTitle($this->credit->credit_type) .' '. $this->credit->agreement_number,
            'income_item_id' => ($this->credit->credit_type == Credit::TYPE_LOAN)
                ? InvoiceIncomeItem::ITEM_LOAN
                : InvoiceIncomeItem::ITEM_CREDIT
        ]);
    }

    /**
     * @inheritDoc
     */
    private function _createExpenseFlowsRepaid()
    {
        /** @var DebtPeriod $period */
        foreach ($this->periods as $period) {

            if ($period->repaid <= 0
            || ($period->lastDate->format('Y-m-d') <= date('Y-m-d')))
                continue;

            $this->_createPlanFlow([
                'date' => $period->lastDate->format('Y-m-d'),
                'amount' => $period->repaid,
                'flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE,
                'description' => CreditLabelHelper::getPaymentDescription('repaid', $this->credit->credit_type) .' '. $this->credit->agreement_number,
                'expenditure_item_id' => ($this->credit->credit_type == Credit::TYPE_LOAN)
                    ? InvoiceExpenditureItem::ITEM_LOAN_REPAYMENT
                    : InvoiceExpenditureItem::ITEM_REPAYMENT_CREDIT
            ]);
        }
    }

    /**
     * @inheritDoc
     */
    private function _createExpenseFlowsInterest()
    {
        /** @var DebtPeriod $period */
        foreach ($this->periods as $period) {

            if ($period->interest <= 0
            || ($period->lastDate->format('Y-m-d') <= date('Y-m-d')))
                continue;

            $this->_createPlanFlow([
                'date' => $period->lastDate->format('Y-m-d'),
                'amount' => $period->interest,
                'flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE,
                'description' => CreditLabelHelper::getPaymentDescription('interest', $this->credit->credit_type) .' '. $this->credit->agreement_number,
                'expenditure_item_id' => InvoiceExpenditureItem::ITEM_PAYMENT_PERCENT
            ]);
        }
    }

    /**
     * @param $periodAttributes
     * @return bool
     */
    private function _createPlanFlow($periodAttributes)
    {
        $flow = new PlanCashFlows([
            'credit_id' => $this->credit->primaryKey,
            'company_id' => $this->credit->company_id,
            'contractor_id' => $this->credit->contractor_id,
            'payment_type' => PlanCashFlows::PAYMENT_TYPE_BANK,
            'checking_accountant_id' => $this->mainCheckingAccountant->id,
        ] + $periodAttributes);

        if (!$flow->save()) {
            \Yii::error(__METHOD__."\n".\yii\helpers\VarDumper::dumpAsString($flow->getErrors()), 'validation');
            return false;
        }

        return true;
    }
}