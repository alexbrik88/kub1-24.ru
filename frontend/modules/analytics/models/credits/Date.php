<?php

namespace frontend\modules\analytics\models\credits;

use DateTimeImmutable;
use DateTimeZone;

class Date extends DateTimeImmutable
{
    /**
     * @param string $time
     * @param null $timezone
     * @throws
     */
    public function __construct($time = "now", $timezone = NULL)
    {
        parent::__construct($time, $timezone);
    }

    /**
     * @inheritDoc
     * @return static
     * @throws
     */
    public static function createFromFormat($format, $time, DateTimeZone $timezone = null)
    {
        $timestamp = parent::createFromFormat($format, $time, $timezone)->getTimestamp();
        $date = (new static)->setTimestamp($timestamp);

        return $date;
    }

    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->format('Y');
    }

    /**
     * @return int
     */
    public function getMonth(): int
    {
        return $this->format('n');
    }

    /**
     * @return int
     */
    public function getDay(): int
    {
        return $this->format('j');
    }

    /**
     * @param int $month
     * @return static
     */
    public function setMonth(int $month): self
    {
        return $this->setDate($this->getYear(), $month, $this->getDay());
    }

    /**
     * @param int $day
     * @return static
     */
    public function setDay(int $day): self
    {
        return $this->setDate($this->getYear(), $this->getMonth(), $day);
    }

    /**
     * @return int
     */
    public function getDaysInMonth(): int
    {
        return cal_days_in_month(CAL_GREGORIAN, $this->getMonth(), $this->getYear());
    }

    /**
     * @return int
     */
    public function getYearLength(): int
    {
        if (cal_days_in_month(CAL_GREGORIAN, 2, $this->getYear()) > 28) {
            return 366;
        }

        return 365;
    }
}
