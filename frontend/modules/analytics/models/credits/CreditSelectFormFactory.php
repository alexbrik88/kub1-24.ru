<?php

namespace frontend\modules\analytics\models\credits;

use common\models\employee\Employee;
use Yii;

class CreditSelectFormFactory
{
    /**
     * @return CreditSelectForm
     */
    public function createForm(): CreditSelectForm
    {
        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;

        return new CreditSelectForm([
            'company_id' => $employee->company->id,
        ]);
    }
}
