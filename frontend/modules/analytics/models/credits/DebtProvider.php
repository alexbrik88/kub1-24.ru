<?php

namespace frontend\modules\analytics\models\credits;

use yii\base\InvalidConfigException;
use yii\data\ArrayDataProvider;

class DebtProvider extends ArrayDataProvider
{
    /**
     * @var DebtSchedule
     */
    public $debt;

    /**
     * @inheritDoc
     */
    public $pagination = false;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!is_a($this->debt, DebtSchedule::class, false)) {
            throw new InvalidConfigException();
        }

        $this->allModels = $this->debt->periods;
    }
}
