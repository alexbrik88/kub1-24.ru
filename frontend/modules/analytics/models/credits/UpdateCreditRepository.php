<?php

namespace frontend\modules\analytics\models\credits;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\ActiveQuery;

class UpdateCreditRepository extends Model implements RepositoryInterface
{
    /**
     * @return ActiveDataProvider
     */
    public function getProvider(): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $this->getQuery(),
            'sort' => $this->getSort(),
            'pagination' => false,
        ]);
    }

    /**
     * @return ActiveQuery
     */
    public function getQuery(): ActiveQuery
    {
        return Credit::find()->andWhere(['!=', 'credit_status', Credit::STATUS_ARCHIVE]);
    }

    /**
     * @return Sort
     */
    public function getSort(): Sort
    {
        return new Sort(['defaultOrder' => ['credit_id' => SORT_ASC]]);
    }
}
