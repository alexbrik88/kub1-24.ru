<?php

namespace frontend\modules\analytics\models\credits;

use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\Emoney;

class EmoneyFlowForm extends AbstractFlowForm
{
    /**
     * @inheritDoc
     * @return Emoney
     */
    protected function createCashFlow(): CashFlowsBase
    {
        /** @var Emoney $emoney */
        $emoney = (new RepositoryFactory)
            ->createRepository(EmoneyRepository::class)
            ->getQuery()
            ->andWhere(['id' => $this->emoney_id])
            ->one();

        return new CashEmoneyFlows([
            'is_accounting' => $emoney->is_accounting,
        ]);
    }

    /**
     * @inheritDoc
     */
    protected function getWalletId(): int
    {
        return CashFlowsBase::WALLET_EMONEY;
    }

    /**
     * @inheritDoc
     */
    protected function getRules(): array
    {
        return [
            [['emoney_id'], 'required'],
        ];
    }
}
