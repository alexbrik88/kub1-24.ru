<?php

namespace frontend\modules\analytics\models\credits;

interface RepositoryFactoryInterface
{
    /**
     * @param string $class
     * @return RepositoryInterface
     */
    public function createRepository(string $class): RepositoryInterface;
}
