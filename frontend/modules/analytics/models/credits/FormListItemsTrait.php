<?php

namespace frontend\modules\analytics\models\credits;

trait FormListItemsTrait
{
    /**
     * @param string $attribute
     * @return string[]
     */
    public function getItems(string $attribute): array
    {
        static $items = [];

        if (!isset($items[$attribute])) {
            $items[$attribute] = $this->createList($attribute)->getItems();
        }

        return $items[$attribute];
    }

    /**
     * @param string $attribute
     * @return ListInterface
     */
    protected function createList(string $attribute): ListInterface
    {
        $classes = [
            'credit_id' => CreditRepository::class,
            'contractor_id' => ContractorRepository::class,
            'creditor_id' => CreditorRepository::class,
            'checking_accountant_id' => CheckingAccountantRepository::class,
            'emoney_id' => EmoneyRepository::class,
            'cashbox_id' => CashboxRepository::class,
            'project_id' => ProjectRepository::class,
            'income_item_id' => IncomeItemRepository::class,
            'expenditure_item_id' => ExpenditureItemRepository::class,
            'currency_id' => CurrencyRepository::class,
        ];

        return (new ListFactory)->createList($classes[$attribute]);
    }
}
