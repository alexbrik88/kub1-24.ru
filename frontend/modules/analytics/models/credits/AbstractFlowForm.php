<?php

namespace frontend\modules\analytics\models\credits;

use common\components\date\DateHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use yii\base\InvalidConfigException;
use yii\base\Model;

abstract class AbstractFlowForm extends Model
{
    use FormListItemsTrait;

    /** @var string */
    public const SCENARIO_CREATE = 'create';

    /** @var string */
    public const SCENARIO_UPDATE = 'update';

    /** @var string */
    public const SCENARIO_DELETE = 'delete';

    /**
     * @var int
     */
    public $company_id;

    /**
     * @var int
     */
    public $employee_id;

    /**
     * @var int
     */
    public $credit_id;

    /**
     * @var int
     */
    public $project_id;

    /**
     * @var int
     */
    public $contractor_id;

    /**
     * @var int|null
     */
    public $checking_accountant_id;

    /**
     * @var int|null
     */
    public $cashbox_id;

    /**
     * @var int|null
     */
    public $emoney_id;

    /**
     * @var int|null
     */
    public $income_item_id;

    /**
     * @var int|null
     */
    public $expenditure_item_id;

    /**
     * @var int
     */
    public $flow_type;

    /**
     * @var float
     */
    public $amount;

    /**
     * @var string
     */
    public $date;

    /**
     * @var string
     */
    public $recognitionDate;

    /**
     * @var string
     */
    public $description;

    /**
     * @var string
     */
    public $credit_amount;

    /**
     * @var int
     */
    public $payment_order_number;

    /**
     * @var bool
     */
    public $is_prepaid_expense;

    /**
     * @var string
     */
    public $currency_id;

    /**
     * @var string[][]
     */
    private $_items = [];

    /**
     * @var CreditFlow
     */
    private $_creditFlow;

    /**
     * @return string[]
     */
    public function attributeLabels()
    {
        return [
            'flow_type' => 'Тип',
            'credit_id' => 'Кредитный договор',
            'emoney_id' => 'E-money',
            'cashbox_id' => 'Кошелек',
            'checking_accountant_id' => 'Расчётный счёт',
            'project_id' => 'Проект',
            'contractor_id' => 'Кредитор',
            'date' => 'Дата оплаты',
            'recognitionDate' => 'Дата признания дохода',
            'payment_order_number' => 'Номер ПП',
            'description' => 'Назначение',
            'amount' => 'Сумма',
            'credit_amount' => 'На сумму',
            'income_item_id' => 'Статья прихода',
            'expenditure_item_id' => 'Статья расхода',
            'is_prepaid_expense' => 'Авансовый платеж',
        ];
    }

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->company_id) || empty($this->credit_id)) {
            throw new InvalidConfigException();
        }

        $this->_creditFlow = new CreditFlow();
    }

    /**
     * @param int $id
     * @return bool
     */
    public function loadModel($id): bool
    {
        if (is_scalar($id) && ctype_digit($id)) {
            /** @var CreditFlowRepository $repository */
            $repository = (new RepositoryFactory)->createRepository(CreditFlowRepository::class);
            $repository->wallet_id = $this->getWalletId();

            /** @var CreditFlow $creditFlow */
            $creditFlow = $repository->getQuery()->andWhere(['credit_flow_id' => $id])->one();

            if ($creditFlow) {
                $attributes = $creditFlow->cashFlow->getAttributes();
                $attributes['amount'] = $attributes['amount'] / 100;
                $attributes['date'] = DateHelper::format(
                    $attributes['date'],
                    DateHelper::FORMAT_USER_DATE,
                    DateHelper::FORMAT_DATE
                );
                $attributes['recognitionDate'] = DateHelper::format(
                    $attributes['recognition_date'],
                    DateHelper::FORMAT_USER_DATE,
                    DateHelper::FORMAT_DATE
                );
                $this->setAttributes($attributes, false);
                $this->_creditFlow = $creditFlow;

                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function saveModel(): bool
    {
        $attributes = $this->getAttributes($this->activeAttributes());
        $attributes['is_prepaid_expense'] = (int) $this->is_prepaid_expense;
        $attributes['author_id'] = $this->employee_id;
        $attributes['amount'] = str_replace(',', '.', $attributes['amount']) * 100.0;
        $cashFlow = $this->_creditFlow->isNewRecord ? $this->createCashFlow() : $this->_creditFlow->cashFlow;
        $cashFlow->setAttributes($attributes, false);
        $cashFlow->credit_id = $this->credit_id;

        return $cashFlow->save(false);
    }

    /**
     * @return bool
     * @throws
     */
    public function removeModel(): bool
    {
        return $this->_creditFlow->delete();
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return array_merge([
            [['contractor_id'], 'required'],
            [['credit_id'], 'required'],
            [['date'], 'required'],
            [['amount'], 'required'],
            [['description'], 'required'],
            [['income_item_id'], 'required', 'enableClientValidation' => false, 'when' => function (): bool {
                return ($this->flow_type == CashFlowsBase::FLOW_TYPE_INCOME);
            }],
            [['expenditure_item_id'], 'required', 'enableClientValidation' => false, 'when' => function (): bool {
                return ($this->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE);
            }],

            [['amount'], 'trim'],

            [['flow_type'], 'in', 'range' => array_keys($this->getFlowTypes())],
            [['credit_id'], 'in', 'range' => array_keys($this->getItems('credit_id'))],
            [['emoney_id'], 'in', 'range' => array_keys($this->getItems('emoney_id'))],
            [['cashbox_id'], 'in', 'range' => array_keys($this->getItems('cashbox_id'))],
            [['checking_accountant_id'], 'in', 'range' => array_keys($this->getItems('checking_accountant_id'))],
            [['project_id'], 'in', 'range' => array_keys($this->getItems('project_id'))],
            [['contractor_id'], 'in', 'range' => array_keys($this->getItems('contractor_id'))],
            [['income_item_id'], 'in', 'range' => array_keys($this->getItems('income_item_id'))],
            [['expenditure_item_id'], 'in', 'range' => array_keys($this->getItems('expenditure_item_id'))],
            [['date'], 'date', 'format' => 'php:' . DateHelper::FORMAT_USER_DATE],
            [['recognitionDate'], 'date', 'format' => 'php:' . DateHelper::FORMAT_USER_DATE],
            [['payment_order_number'], 'integer', 'min' => 1],
            [['amount'], 'match', 'pattern' => '/^([0-9]+)([,.][0-9]+)*$/'],
            [['is_prepaid_expense'], 'boolean'],
        ], $this->getRules());
    }

    /**
     * @inheritDoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => [
                '!company_id',
                '!employee_id',
                '!credit_id',
                'emoney_id',
                'project_id',
                'contractor_id',
                'cashbox_id',
                'checking_accountant_id',
                'income_item_id',
                'expenditure_item_id',
                'flow_type',
                'amount',
                'date',
                'recognitionDate',
                'description',
                'credit_amount',
                'payment_order_number',
                'is_prepaid_expense',
            ],
            self::SCENARIO_UPDATE => [
                '!company_id',
                '!employee_id',
                '!credit_id',
                'emoney_id',
                'project_id',
                'contractor_id',
                'cashbox_id',
                'checking_accountant_id',
                'income_item_id',
                'expenditure_item_id',
                'amount',
                'date',
                'recognitionDate',
                'description',
                'credit_amount',
                'payment_order_number',
                'is_prepaid_expense',
            ],
            self::SCENARIO_DELETE => [
                '!company_id',
                '!employee_id',
                '!credit_id',
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function getFlowTypes(): array
    {
        return array_filter(CashBankFlows::getFlowTypes(), function (int $type): bool {
            if ($this->scenario == AbstractFlowForm::SCENARIO_UPDATE && $type != $this->flow_type) {
                return false;
            }

            return true;
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * @return CashFlowsBase
     */
    abstract protected function createCashFlow(): CashFlowsBase;

    /**
     * @return int
     */
    abstract protected function getWalletId(): int;

    /**
     * @return mixed[][]
     */
    abstract protected function getRules(): array;
}
