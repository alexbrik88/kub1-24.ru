<?php

namespace frontend\modules\analytics\models\credits;

use common\models\currency\Currency;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\ActiveQuery;

class CurrencyRepository extends Model implements RepositoryInterface, ListInterface
{
    /**
     * @inheritDoc
     */
    public function getItems(): array
    {
        /** @var Currency[] $models */
        $models = $this->getProvider()->getModels();
        $items = [];

        foreach ($models as $model) {
            $items[$model->id] = sprintf('%s, %s', $model->label, $model->name);
        }

        return $items;
    }

    /**
     * @inheritDoc
     */
    public function getProvider(): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'pagination' => false,
            'query' => $this->getQuery(),
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getQuery(): ActiveQuery
    {
        return Currency::find();
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        return new Sort(['defaultOrder' => ['id' => SORT_ASC]]);
    }
}
