<?php

namespace frontend\modules\analytics\models\credits;

class DebtAnnuityStrategy implements DebtStrategyInterface
{
    /**
     * @inheritDoc
     */
    public function calculate(DebtSchedule $schedule, DebtPeriod $period, float $amount): void
    {
        $credit = $schedule->credit;
        $yearLength = $credit->credit_year_length ?: $period->lastDate->getYearLength();
        $count = $this->getPeriodCount($schedule);
        $rate = $credit->credit_percent_rate / 12 / 100;

        $total = $rate / (pow(1.0 + $rate, $count) - 1);
        $total += $rate;
        $total *= $credit->credit_amount;

        $period->interest = $amount;
        $period->interest *= $credit->credit_percent_rate / 100;
        $period->interest /= $yearLength;
        $period->interest *= $period->days;

        $period->repaid = $total - $period->interest;
        $period->amount = $amount - $period->repaid;

        if ($period->amount < 0.01) {
            $period->repaid += $period->amount;
            $period->amount = 0;
        }
    }

    /**
     * @inheritDoc
     */
    public function mutateOnlyPercent(DebtSchedule $schedule, DebtPeriod $period): void
    {
        $period->amount += $period->repaid;
        $period->repaid = 0;
    }

    /**
     * @inheritDoc
     */
    public function mutateSkipPayment(DebtSchedule $schedule, DebtPeriod $period): void
    {
        $period->amount += $period->repaid;
        $period->repaid = 0;
        $period->interest = 0;
    }

    /**
     * @inheritDoc
     */
    public function mutateSchedule(DebtSchedule $schedule): void
    {
        $periodsCount = count($schedule->periods);
        $annuityCount = ceil($schedule->credit->credit_day_count / 365 * 12);

        if ($schedule->credit->payment_first != Credit::PAYMENT_FIRST_DEFAULT || $periodsCount <= $annuityCount) {
            return;
        }

        $prev = $schedule->periods[$periodsCount - 2];
        $last = $schedule->periods[$periodsCount - 1];

        $prev->lastDate = $last->lastDate;

        unset($schedule->periods[$periodsCount - 1]);
    }

    /**
     * @param DebtSchedule $schedule
     * @return int
     */
    private function getPeriodCount(DebtSchedule $schedule): int
    {
        $count = count($schedule->periods);

        if ($schedule->credit->payment_first != Credit::PAYMENT_FIRST_DEFAULT) {
            $count--;
        }

        return $count;
    }
}
