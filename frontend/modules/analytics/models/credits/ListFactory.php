<?php

namespace frontend\modules\analytics\models\credits;

use common\models\employee\Employee;
use RuntimeException;
use Yii;

class ListFactory implements ListFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function createList(string $class): ListInterface
    {
        if (!is_a($class, ListInterface::class, true)) {
            throw new RuntimeException('Class not found.');
        }

        if (property_exists($class, 'company_id')) {
            /** @var Employee $employee */
            $employee = Yii::$app->user->identity;

            return new $class(['company_id' => $employee->company->id]);
        }

        return new $class();
    }
}
