<?php
namespace frontend\modules\analytics\models\credits;

class CreditLabelHelper {

    public static function getTitle($creditType)
    {
        switch ($creditType) {
            case Credit::TYPE_CREDIT:
                return 'Кредитный договор';
            case Credit::TYPE_LOAN:
                return 'Договор займа';
            case Credit::TYPE_OVERDRAFT:
                return 'Договор овердрафта';
        }

        return '';
    }

    public static function getLabel($attribute, $creditType)
    {
        switch ($attribute) {
            case 'credit_amount':
                return self::_getLabelCreditAmount($creditType);
            case 'payment_type':
                return self::_getLabelPaymentType($creditType);
            case 'debt_diff':
                return self::_getLabelDebtDiff($creditType);
            case 'wallet_value':
                return self::_getLabelWalletValue($creditType);
        }

        return '';
    }

    public static function getPaymentDescription($attribute, $creditType)
    {
        switch ($attribute) {
            case 'repaid':
                return self::_getPaymentDescriptionRepaid($creditType);
            case 'interest':
                return self::_getPaymentDescriptionInterest($creditType);
        }

        return '';
    }

    private static function _getLabelCreditAmount($creditType)
    {
        switch ($creditType) {
            case Credit::TYPE_CREDIT:
                return 'Сумма кредита';
            case Credit::TYPE_LOAN:
                return 'Сумма займа';
            case Credit::TYPE_OVERDRAFT:
                return 'Сумма овердрафта';
        }

        return '';
    }

    private static function _getLabelPaymentType($creditType)
    {
        switch ($creditType) {
            case Credit::TYPE_CREDIT:
                return 'Вид платежа по кредиту';
            case Credit::TYPE_LOAN:
                return 'Вид платежа по займу';
            case Credit::TYPE_OVERDRAFT:
                return 'Вид платежа по овердрафту';
        }

        return '';
    }

    private static function _getLabelDebtDiff($creditType)
    {
        switch ($creditType) {
            case Credit::TYPE_CREDIT:
                return 'Остаток по кредиту';
            case Credit::TYPE_LOAN:
                return 'Остаток по займу';
            case Credit::TYPE_OVERDRAFT:
                return 'Остаток по овердрафту';
        }

        return '';
    }

    private static function _getPaymentDescriptionRepaid($creditType)
    {
        switch ($creditType) {
            case Credit::TYPE_CREDIT:
                return 'Платеж по основному долгу по Кредитному договору';
            case Credit::TYPE_LOAN:
                return 'Платеж по основному долгу по Договору займа';
            case Credit::TYPE_OVERDRAFT:
                return 'Платеж по основному долгу по Договору овердрафта';
        }

        return '';
    }

    private static function _getPaymentDescriptionInterest($creditType)
    {
        switch ($creditType) {
            case Credit::TYPE_CREDIT:
                return 'Платеж по процентам по Кредитному договору';
            case Credit::TYPE_LOAN:
                return 'Платеж по процентам по Договору займа';
            case Credit::TYPE_OVERDRAFT:
                return 'Платеж по процентам по Договору овердрафта';
        }

        return '';
    }

    private static function _getLabelWalletValue($creditType)
    {
        switch ($creditType) {
            case Credit::TYPE_CREDIT:
                return 'Счет для зачисления кредита';
            case Credit::TYPE_LOAN:
                return 'Счет для зачисления займа';
            case Credit::TYPE_OVERDRAFT:
                return 'Счет для зачисления овердрафта';
        }

        return '';
    }
}