<?php

namespace frontend\modules\analytics\models\credits;

class DebtAtEndStrategy implements DebtStrategyInterface
{
    /**
     * @inheritDoc
     */
    public function calculate(DebtSchedule $schedule, DebtPeriod $period, float $amount): void
    {
        $credit = $schedule->credit;
        $period->amount = $amount;
        $yearLength = $credit->credit_year_length ?: $period->lastDate->getYearLength();
        $rate = $credit->credit_percent_rate / 100;

        $period->interest = $amount;
        $period->interest *= $rate;
        $period->interest /= $yearLength;
        $period->interest *= $period->days;

        if ($schedule->isLastPeriod($period)) {
            $period->amount = 0;
            $period->repaid = $amount;
        }
    }

    /**
     * @inheritDoc
     */
    public function mutateOnlyPercent(DebtSchedule $schedule, DebtPeriod $period): void
    {

    }

    /**
     * @inheritDoc
     */
    public function mutateSkipPayment(DebtSchedule $schedule, DebtPeriod $period): void
    {
        $period->repaid = 0;
        $period->interest = 0;
    }

    /**
     * @inheritDoc
     */
    public function mutateSchedule(DebtSchedule $schedule): void
    {

    }
}
