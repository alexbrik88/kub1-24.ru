<?php

namespace frontend\modules\analytics\models\credits;

use common\models\Contractor;
use yii\db\ActiveQuery;

class CreditorRepository extends ContractorRepository
{
    /**
     * @inheritDoc
     */
    public function getQuery(): ActiveQuery
    {
        $on = Credit::tableName() . '.contractor_id = ' . Contractor::tableName() . '.id';

        return parent::getQuery()
            ->innerJoin(Credit::tableName(), $on)
            ->andWhere([Credit::tableName() . '.company_id' => $this->company_id]);
    }
}
