<?php

namespace frontend\modules\analytics\models\credits;

use yii\base\InvalidConfigException;
use yii\base\Model;

/**
 * @property-read Credit[] $credits
 */
class CreditSelectForm extends Model
{
    /**
     * @var int
     */
    public $company_id;

    /**
     * @var int[]
     */
    public $credit_id;

    /**
     * @var string
     */
    public $delete;

    /**
     * @var Credit[]
     */
    private $_credits;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->company_id)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['credit_id'], 'required'],
            [['credit_id'], 'each', 'rule' => ['integer']],
            [['delete'], 'string'],
        ];
    }

    /**
     * @return Credit[]
     */
    public function getCredits(): array
    {
        if ($this->_credits == null) {
            /** @var CreditRepository $repository */
            $repository = (new RepositoryFactory)->createRepository(CreditRepository::class);
            $repository->company_id = $this->company_id;
            $repository->onlyDeletable = true;
            $this->_credits = $repository
                ->getQuery()
                ->andWhere([Credit::tableName() . '.credit_id' => $this->credit_id])
                ->all();
        }

        return $this->_credits;
    }

    /**
     * @return bool
     * @throws
     */
    public function updateModels(): bool
    {
        if (!$this->credits) {
            return false;
        }

        foreach ($this->credits as $credit) {
            if (strlen($this->delete)) {
                $credit->delete();
            }
        }

        return true;
    }
}
