<?php

namespace frontend\modules\analytics\models\credits;

use yii\base\Model;

/**
 * @property-read float $payments Сумма платежей
 * @property-read float $total Сумма платежа итого
 * @property-read int $days количество дней в периоде
 * @property-read float $endBalance Баланс в конце периода
 */
class DebtPeriod extends Model
{
    /**
     * @var Date Начало периода
     */
    public Date $firstDate;

    /**
     * @var Date Дата платежа
     */
    public Date $lastDate;

    /**
     * @var int
     */
    public int $number = 0;

    /**
     * @var float Остаток долга
     */
    public float $amount = 0;

    /**
     * @var float Платеж по основному долгу
     */
    public float $repaid = 0;

    /**
     * @var float Платеж по процентам
     */
    public float $interest = 0;

    /**
     * @var float Баланс в начале периода
     */
    public float $startBalance = 0;

    /**
     * @var float Сумма платежей по %
     */
    public float $paymentsInterest = 0;

    /**
     * @var float Сумма платежей погашения
     */
    public float $paymentsRepaid = 0;

    /**
     * @param int $number
     * @param Date $firstDate
     * @param Date $lastDate
     */
    public function __construct(int $number, Date $firstDate, Date $lastDate)
    {
        $this->number = $number;
        $this->firstDate = $firstDate;
        $this->lastDate = $lastDate;

        parent::__construct();
    }

    /**
     * @return int
     */
    public function getDays(): int
    {
        $days = $this->lastDate->diff($this->firstDate)->days;

        if ($days) {
            $days++;
        }

        return $days;
    }

    /**
     * @return float
     */
    public function getTotal(): float
    {
        return $this->repaid + $this->interest;
    }

    /**
     * @return float
     */
    public function getEndBalance(): float
    {
        $balance = round($this->payments + $this->startBalance - $this->total, 2, PHP_ROUND_HALF_UP);

        if ($balance == -0) { // TODO: wtf
            $balance = 0.0;
        }

        return $balance;
    }

    /**
     * @return float
     */
    public function getPayments(): float
    {
        return $this->paymentsRepaid + $this->paymentsInterest;
    }
}
