<?php

namespace frontend\modules\analytics\models\credits;

use common\models\Company;
use common\models\document\AbstractDocument;
use frontend\models\Documents;
use frontend\models\log\Log;
use Yii;
use yii\db\ActiveQuery;

/**
 * @property-read int $id
 * @property-read int $credit_id
 * @property-read Company $company
 */
class CreditDocument extends AbstractDocument
{
    /** @var int */
    public const MAX_FILE_COUNT = 6;

    public $type = Documents::IO_TYPE_IN;

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritDoc
     * @throws
     */
    public static function find()
    {
        return Yii::createObject(CreditDocumentQuery::class, [get_called_class()]);
    }

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->trigger(self::EVENT_INIT);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->credit_id;
    }

    /**
     * @return ActiveQuery
     */
    public function getCompany(): ActiveQuery
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @inheritDoc
     */
    public function getFullNumber()
    {

    }

    /**
     * @inheritDoc
     */
    public function getFile()
    {

    }

    /**
     * @inheritDoc
     */
    public function getLogMessage(Log $log)
    {
        return $log->message;
    }

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%credit}}';
    }
}
