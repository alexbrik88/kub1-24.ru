<?php

namespace frontend\modules\analytics\models\credits;

use common\components\date\DateHelper;
use yii\helpers\ArrayHelper;
use common\models\cash\CashFlowsBase;

class DebtSchedule // TODO: Эта сущность не должна принимать и хранить в себе экземпляр класса Credit
{
    /**
     * @var Credit
     */
    public Credit $credit;

    /**
     * @var Date
     */
    public Date $firstDate;

    /**
     * @var Date
     */
    public Date $lastDate;

    /**
     * @var DebtPeriod[]
     */
    public array $periods;

    /**
     * @var DebtStrategyInterface
     */
    public DebtStrategyInterface $debtStrategy;

    /**
     * @var FirstDateFactory
     */
    private FirstDateFactory $firstDateFactory;

    /**
     * @var LastDateFactory
     */
    private LastDateFactory $lastDateFactory;

    /**
     * @param Credit $credit
     * @param DebtStrategyInterface $debtStrategy
     */
    public function __construct(Credit $credit, DebtStrategyInterface $debtStrategy)
    {
        $this->credit = $credit;
        $this->debtStrategy = $debtStrategy;
        $this->firstDate = Date::createFromFormat(DateHelper::FORMAT_DATE, $this->credit->credit_first_date);
        $this->lastDate = Date::createFromFormat(DateHelper::FORMAT_DATE, $this->credit->credit_last_date);

        $this->firstDateFactory = new FirstDateFactory();
        $this->lastDateFactory = new LastDateFactory($this);
        $this->periods = $this->createPeriods();

        $this->debtStrategy->mutateSchedule($this);
        $this->calculatePeriods();
    }

    /**
     * @return DebtPeriod
     * @throws
     */
    public function getActualPeriod(): DebtPeriod
    {
        $date = new Date();

        return $this->getPeriodByDate($date);
    }

    /**
     * @param Date $date
     * @return DebtPeriod
     */
    public function getPeriodByDate(Date $date): DebtPeriod
    {
        $timestamp = $date->getTimestamp();

        foreach ($this->periods as $period) {
            if ($timestamp >= $period->firstDate->getTimestamp() && $timestamp <= $period->lastDate->getTimestamp()) {
                return $period;
            }
        }

        if ($timestamp <= $this->firstDate->getTimestamp()) {
            return $this->periods[0];
        }

        return $this->periods[count($this->periods) - 1];
    }

    /**
     * @param DebtPeriod $period
     * @return bool
     */
    public function isLastPeriod(DebtPeriod $period): bool
    {
        $interval = $this->lastDate->diff($period->lastDate);

        if ($interval->invert) {
            return false;
        }

        return true;
    }

    /**
     * @return void
     */
    private function calculatePeriods(): void
    {
        /** @var DebtPeriod $previous */
        $previous = null;
        $credit = $this->credit;
        $dateFullyPaid = ($flowBodyFullyPaid = $this->getFlowBodyFullyPaid())
            ? Date::createFromFormat(DateHelper::FORMAT_DATE, $flowBodyFullyPaid->date)
            : null;

        foreach ($this->periods as $period) {

            // Цитата: брал Кредит до конца года, но погасил досрочно; пересчитать проценты
            if ($dateFullyPaid) {
                $periodYM = $period->lastDate->format('Ym');
                $paidYM = $dateFullyPaid->format('Ym');
                if ($periodYM == $paidYM)
                    $period->lastDate = $dateFullyPaid;
                if ($periodYM > $paidYM) {
                    $previous->amount = 0;
                }
            }

            $this->debtStrategy->calculate($this, $period, $previous ? $previous->amount : $credit->credit_amount);

            if ($previous === null && $credit->payment_first == Credit::PAYMENT_FIRST_ONLY_PERCENT) {
                $this->debtStrategy->mutateOnlyPercent($this, $period);
            }

            if ($previous === null && $credit->payment_first == Credit::PAYMENT_FIRST_SKIP) {
                $this->debtStrategy->mutateSkipPayment($this, $period);
            }

            $previous = $period;
        }
    }

    /**
     * @return DebtPeriod[]
     */
    private function createPeriods(): array
    {
        $period = $this->createFirstPeriod();
        $periods[] = $period;

        while (!$this->isLastPeriod($period)) {
            $period = $this->createPeriod($period);
            $periods[] = $period;
        }

        return $periods;
    }

    /**
     * @return DebtPeriod
     * @throws
     */
    private function createFirstPeriod(): DebtPeriod
    {
        $firstDate = $this->firstDateFactory->createDate($this->firstDate);
        $lastDate = $this->lastDateFactory->createDate($this->firstDate);

        return new DebtPeriod(
            ($this->credit->payment_first == Credit::PAYMENT_FIRST_SKIP) ? 0 : 1,
            ($this->credit->payment_first == Credit::PAYMENT_FIRST_SKIP) ? $lastDate : $firstDate,
            $lastDate
        );
    }

    /**
     * @param DebtPeriod $previous
     * @return DebtPeriod
     * @throws
     */
    private function createPeriod(DebtPeriod $previous): DebtPeriod
    {

        $firstDate = $this->firstDateFactory->createDate($previous->lastDate);
        $lastDate = $this->lastDateFactory->createDate($firstDate);

        if ($this->credit->payment_first == Credit::PAYMENT_FIRST_SKIP && $previous->number < 1) {
            $firstDate = $this->firstDateFactory->createDate($this->firstDate);
        }

        return new DebtPeriod($previous->number + 1, $firstDate, $lastDate);
    }

    public function getFlowBodyFullyPaid(): ?Flow
    {
        if (empty($this->credit->credit_id)) {
            return null;
        }

        /** @var CreditFlowRepository $repository */
        $repository = new CreditFlowRepository([
            'credit_id' => $this->credit->credit_id,
            'company_id' => $this->credit->company_id,
        ]);

        $models = $repository->getProvider()->getModels();
        $flows = array_filter(
            ArrayHelper::getColumn($models, 'flow'),
            function (Flow $flow): bool {
                return ($flow->isCreditBody());
            });

        $debt = 0;
        foreach ($flows as $f) {
            if ($f->type == CashFlowsBase::FLOW_TYPE_INCOME)
                $debt += $f->amount;
            if ($f->type == CashFlowsBase::FLOW_TYPE_EXPENSE)
                $debt -= $f->amount;
        }

        if ($debt <= 0 && !empty($flows))
            return reset($flows);

        return null;
    }

    public function getSheduleYmFormatted()
    {
        $ymLine = [];
        foreach ($this->periods as $period) {
            $ymLine[$period->lastDate->format('Ym')] = [
                'amount' => $period->amount,
                'interest' => $period->interest
            ];
        }
        return $ymLine;
    }

    public function getTotalDebtOnDate(string $date, $withCreditBody = false, $withCreditPercent = false): float
    {
        $balanceBody = 0;
        $balanceInterest = 0;

        if ($this->periods[0]->firstDate->format('Y-m-d') > $date)
            return 0;
        if ($this->periods[count($this->periods)-1]->lastDate->format('Y-m-d') < $date)
            return 0;

        foreach ($this->periods as $period) {
            if ($period->firstDate->format('Y-m-d') > $date)
                break;

            if ($withCreditBody) {
                $balanceBody += $period->repaid;
                $balanceBody -= $period->paymentsRepaid;
            }
            if ($withCreditPercent) {
                $balanceInterest += $period->interest;
                $balanceInterest -= $period->paymentsInterest;
            }
        }

        return (float)max(0, $balanceBody + $balanceInterest);
    }
}
