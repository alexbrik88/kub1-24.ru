<?php

namespace frontend\modules\analytics\models\credits;

use common\models\employee\Employee;
use Yii;

class FlowSelectFormFactory
{
    /**
     * @param Credit $credit
     * @return FlowSelectForm
     */
    public function createForm(Credit $credit): FlowSelectForm
    {
        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;

        return new FlowSelectForm([
            'credit_id' => $credit->credit_id,
            'company_id' => $employee->company->id,
        ]);
    }
}
