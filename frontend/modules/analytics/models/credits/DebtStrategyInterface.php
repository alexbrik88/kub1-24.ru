<?php

namespace frontend\modules\analytics\models\credits;

// TODO: у всех стратегий должен быть конструктор с параметрами кредита
// TODO: стратегии не должны работать напрямую с экземпляром класса Credit

interface DebtStrategyInterface
{
    /**
     * @param DebtSchedule $schedule
     * @param DebtPeriod $period
     * @param float $amount
     * @return void
     */
    public function calculate(DebtSchedule $schedule, DebtPeriod $period, float $amount): void;

    /**
     * @param DebtSchedule $schedule
     * @param DebtPeriod $period
     * @return void
     */
    public function mutateOnlyPercent(DebtSchedule $schedule, DebtPeriod $period): void;

    /**
     * @param DebtSchedule $schedule
     * @param DebtPeriod $period
     * @return void
     */
    public function mutateSkipPayment(DebtSchedule $schedule, DebtPeriod $period): void;

    /**
     * @param DebtSchedule $schedule
     * @return void
     */
    public function mutateSchedule(DebtSchedule $schedule): void;
}
