<?php

namespace frontend\modules\analytics\models\credits;

use common\components\date\DateHelper;
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceExpenditureItem as Expense;
use yii\helpers\ArrayHelper;

class DebtCalculator
{
    /**
     * @var DebtSchedule
     */
    public $schedule;

    /**
     * @var Flow[]
     */
    public array $flows;

    /**
     * @param DebtSchedule $schedule
     */
    public function __construct(DebtSchedule $schedule)
    {
        $this->schedule = $schedule;
        $this->flows = $this->getFlows();

        array_map([$this, 'applyFlow'], $this->flows);

        $this->applyBalance();
    }

    /**
     * @return void
     */
    public function updateCredit(): void
    {
        $credit = $this->schedule->credit;
        $flowsIncome = array_filter($this->flows, function (Flow $flow) {
            return $flow->type == CashFlowsBase::FLOW_TYPE_INCOME;
        });

        $credit->interest_amount = self::getColumnSum($this->schedule->periods, 'interest');
        $credit->interest_repaid = self::getColumnSum($this->schedule->periods, 'paymentsInterest');
        $credit->interest_diff = $credit->interest_amount - $credit->interest_repaid;

        $credit->debt_amount = self::getColumnSum($flowsIncome, 'incomeAmount');
        $credit->debt_repaid = self::getColumnSum($this->schedule->periods, 'paymentsRepaid');
        $credit->debt_diff = $credit->debt_amount - $credit->debt_repaid;

        // after calc all up rows
        $credit->credit_status = $this->getCreditStatus();

        $paymentNextDate = $this->getPaymentNextDate();
        $credit->payment_date = $paymentNextDate ? $paymentNextDate->format(DateHelper::FORMAT_DATE) : null;
        $credit->payment_amount = $this->calculatePaymentAmount();
        $credit->payment_debt = $this->calculatePaymentDebt();
    }

    /**
     * @return float
     */
    private function calculatePaymentDebt(): float
    {
        $actual = $this->schedule->getActualPeriod();

        if ($actual->endBalance < 0.0) {
            return abs($actual->endBalance);
        }

        return 0.0;
    }

    /**
     * @return float
     */
    private function calculatePaymentAmount(): float
    {
        $schedule = clone $this->schedule;
        $actual = $schedule->getActualPeriod();
        $periods = array_filter($schedule->periods, function (DebtPeriod $period) use ($actual) {
            return $actual->firstDate->diff($period->lastDate)->invert;
        });

        return self::getColumnSum($periods, 'paymentsRepaid');
    }

    /**
     * @return Date|null
     */
    private function getPaymentNextDate(): ?Date
    {
        foreach ($this->schedule->periods as $period) {
            if ($period->endBalance < 0.0) {
                return $period->lastDate;
            }
        }

        return null;
    }

    /**
     * @return int
     */
    private function getCreditStatus(): int
    {
        $credit = $this->schedule->credit;
        $actual = $this->schedule->getActualPeriod();
        $date = new Date();

        // TODO: это костыль
        if ($credit->debt_amount > 0.0 && $credit->debt_diff <= 0.0) {
            return Credit::STATUS_ARCHIVE;
        }

        if ($date->diff($this->schedule->lastDate)->invert) { // Кредитный договор истёк
            if ($actual->endBalance < 0.0) { // Осталась задолжность
                return Credit::STATUS_EXPIRED;
            }

            return Credit::STATUS_ARCHIVE;
        }

        if ($actual->startBalance < 0 && $actual->endBalance < 0) { // Задолжность предыдущего периода не погашена
            return Credit::STATUS_EXPIRED;
        }

        return Credit::STATUS_ACTIVE;
    }

    /**
     * @param Flow $flow
     * @return void
     */
    private function applyFlow(Flow $flow): void
    {
        if ($flow->type == CashFlowsBase::FLOW_TYPE_EXPENSE) {
            $date = Date::createFromFormat(DateHelper::FORMAT_DATE, $flow->date);
            $period = $this->schedule->getPeriodByDate($date);

            if (in_array($flow->item_id, [Expense::ITEM_REPAYMENT_CREDIT, Expense::ITEM_LOAN_REPAYMENT])) {
                $period->paymentsRepaid += $flow->getExpenseAmount();
            }

            if (in_array($flow->item_id, [Expense::ITEM_PAYMENT_PERCENT])) {
                $period->paymentsInterest += $flow->getExpenseAmount();
            }
        }
    }

    /**
     * @return void
     */
    private function applyBalance(): void
    {
        $endBalance = 0;

        foreach ($this->schedule->periods as $period) {
            $period->startBalance = $endBalance;
            $endBalance = $period->endBalance;
        }
    }

    /**
     * @return Flow[]
     */
    private function getFlows(): array
    {
        if (empty($this->schedule->credit->credit_id)) {
            return [];
        }

        $repository = new CreditFlowRepository([
            'credit_id' => $this->schedule->credit->credit_id,
            'company_id' => $this->schedule->credit->company_id,
        ]);
        $models = array_reverse($repository->getProvider()->getModels());

        return ArrayHelper::getColumn($models, 'flow');
    }

    /**
     * @param mixed[] $array
     * @param string $column
     * @return float
     */
    private static function getColumnSum(array $array, string $column): float
    {
        return array_sum(ArrayHelper::getColumn($array, $column));
    }
}
