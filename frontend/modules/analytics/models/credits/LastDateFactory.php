<?php

namespace frontend\modules\analytics\models\credits;

class LastDateFactory
{
    /**
     * @var int
     */
    private $days;

    /**
     * @var DebtSchedule
     */
    private $schedule;

    /**
     * @param DebtSchedule $schedule
     */
    public function __construct(DebtSchedule $schedule)
    {
        $this->schedule = $schedule;
        $this->days = [
            Credit::PAYMENT_MODE_DEFAULT => 31,
            Credit::PAYMENT_MODE_CREDIT => $this->schedule->firstDate->getDay(),
            Credit::PAYMENT_MODE_AGREEMENT => $this->schedule->credit->payment_day,
        ];
    }

    /**
     * @inheritDoc
     */
    public function createDate(Date $firstDate): Date
    {
        $day = $this->days[$this->schedule->credit->payment_mode];
        $date = $firstDate->setDay($day > $firstDate->getDaysInMonth() ? $firstDate->getDaysInMonth() : $day);

        if ($date->getMonth() == $firstDate->getMonth() && $date->getDay() <= $firstDate->getDay()) {
            $date = $firstDate->setDate($firstDate->getYear(), $firstDate->getMonth() + 1, 1);
            $date = $date->setDay($day > $date->getDaysInMonth() ? $date->getDaysInMonth() : $day);
        }

        if ($date->diff($this->schedule->lastDate)->invert) {
            return $this->schedule->lastDate;
        }

        return $date;
    }
}
