<?php

namespace frontend\modules\analytics\models\credits;

use common\models\cash\Cashbox;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;

class OrderFlowForm extends AbstractFlowForm
{
    /**
     * @inheritDoc
     * @return CashOrderFlows
     */
    protected function createCashFlow(): CashFlowsBase
    {
        /** @var Cashbox $cashbox */
        $cashbox = (new RepositoryFactory)
            ->createRepository(CashboxRepository::class)
            ->getQuery()
            ->andWhere(['id' => $this->cashbox_id])
            ->one();

        return new CashOrderFlows([
            'number' => CashOrderFlows::getNextNumber($this->company_id, $this->flow_type),
            'is_accounting' => $cashbox->is_accounting,
        ]);
    }

    /**
     * @inheritDoc
     */
    protected function getWalletId(): int
    {
        return CashFlowsBase::WALLET_CASHBOX;
    }

    /**
     * @inheritDoc
     */
    protected function getRules(): array
    {
        return [
            [['cashbox_id'], 'required'],
        ];
    }
}
