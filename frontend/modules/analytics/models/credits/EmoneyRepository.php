<?php

namespace frontend\modules\analytics\models\credits;

use common\models\cash\Emoney;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

class EmoneyRepository extends Model implements RepositoryInterface, ListInterface
{
    /**
     * @var int
     */
    public $company_id;

    /**
     * @var bool
     */
    public $is_closed = false;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->company_id)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function getItems(): array
    {
        $models = $this->getProvider()->getModels();
        $data = ArrayHelper::index($models, 'id');
        $items = ArrayHelper::getColumn($data, 'name');

        return $items;
    }

    /**
     * @inheritDoc
     */
    public function getProvider(): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $this->getQuery(),
            'sort' => $this->getSort(),
            'pagination' => false,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getQuery(): ActiveQuery
    {
        return Emoney::find()->andWhere([
            'company_id' => $this->company_id,
            'is_closed' => $this->is_closed,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        return new Sort(['defaultOrder' => ['name' => SORT_ASC]]);
    }
}
