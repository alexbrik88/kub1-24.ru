<?php

namespace frontend\modules\analytics\models\credits;

interface ListFactoryInterface
{
    /**
     * @param string $class
     * @return ListInterface
     */
    public function createList(string $class): ListInterface;
}
