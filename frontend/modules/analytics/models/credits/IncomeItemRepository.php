<?php

namespace frontend\modules\analytics\models\credits;

use common\models\document\InvoiceIncomeItem;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

class IncomeItemRepository extends Model implements RepositoryInterface, ListInterface
{
    /**
     * @var int[]
     */
    public $item_id = [
        InvoiceIncomeItem::ITEM_LOAN,
        InvoiceIncomeItem::ITEM_CREDIT,
    ];

    /**
     * @inheritDoc
     */
    public function getItems(): array
    {
        $models = $this->getProvider()->getModels();
        $data = ArrayHelper::index($models, 'id');
        $data = ArrayHelper::getColumn($data, 'name');

        return $data;
    }

    /**
     * @inheritDoc
     */
    public function getProvider(): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $this->getQuery(),
            'sort' => $this->getSort(),
            'pagination' => false,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getQuery(): ActiveQuery
    {
        return InvoiceIncomeItem::find()->andWhere(['id' => $this->item_id]);
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        return new Sort(['defaultOrder' => ['id' => SORT_ASC]]);
    }
}
