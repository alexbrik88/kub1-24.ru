<?php

namespace frontend\modules\analytics\models\credits;

use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property-read int $credit_flow_id
 * @property int $credit_id
 * @property int|null $cash_bank_flow_id
 * @property int|null $cash_order_flow_id
 * @property int|null $cash_emoney_flow_id
 * @property int $olap_flow_id
 * @property int $wallet_id
 * @property-read Flow $flow
 * @property-read Credit $credit
 * @property-read CashBankFlows $cashBankFlow
 * @property-read CashOrderFlows $cashOrderFlow
 * @property-read CashEmoneyFlows $cashEmoneyFlow
 * @property-read CashFlowsBase $cashFlow
 */
class CreditFlow extends ActiveRecord
{
    /**
     * @return ActiveQuery
     */
    public function getFlow(): ActiveQuery
    {
        return $this->hasOne(Flow::class, ['id' => 'olap_flow_id', 'wallet' => 'wallet_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCashBankFlow(): ActiveQuery
    {
        return $this->hasOne(CashBankFlows::class, ['id' => 'cash_bank_flow_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCashOrderFlow(): ActiveQuery
    {
        return $this->hasOne(CashOrderFlows::class, ['id' => 'cash_order_flow_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCashEmoneyFlow(): ActiveQuery
    {
        return $this->hasOne(CashEmoneyFlows::class, ['id' => 'cash_emoney_flow_id']);
    }

    /**
     * @return CashFlowsBase
     */
    public function getCashFlow(): CashFlowsBase
    {
        $attributes = [
            CashFlowsBase::WALLET_BANK => 'cashBankFlow',
            CashFlowsBase::WALLET_CASHBOX => 'cashOrderFlow',
            CashFlowsBase::WALLET_EMONEY => 'cashEmoneyFlow',
        ];

        $attribute = $attributes[$this->wallet_id];
        /** @var CashFlowsBase $cashFlow */
        $cashFlow = $this->$attribute;

        return $cashFlow;
    }

    /**
     * @return ActiveQuery
     */
    public function getCredit(): ActiveQuery
    {
        return $this->hasOne(Credit::class, ['credit_id' => 'credit_id']);
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->cashFlow->description;
    }

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%credit_flow}}';
    }
}
