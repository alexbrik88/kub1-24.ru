<?php

namespace frontend\modules\analytics\models\credits;

class DebtEndWithPercentsStrategy implements DebtStrategyInterface
{
    /**
     * @inheritDoc
     */
    public function calculate(DebtSchedule $schedule, DebtPeriod $period, float $amount): void
    {
        $credit = $schedule->credit;
        $period->amount = $amount;
        $yearLength = $credit->credit_year_length ?: $period->lastDate->getYearLength();
        $rate = $credit->credit_percent_rate / 100;


        if ($schedule->isLastPeriod($period)) {
            $period->amount = 0;
            $period->interest = 0;
            $period->repaid = $amount;

            foreach ($schedule->periods as $p) {
                $period->interest += ($amount * $rate * $p->days / $yearLength);
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function mutateOnlyPercent(DebtSchedule $schedule, DebtPeriod $period): void
    {

    }

    /**
     * @inheritDoc
     */
    public function mutateSkipPayment(DebtSchedule $schedule, DebtPeriod $period): void
    {
        $period->repaid = 0;
        $period->interest = 0;
    }

    /**
     * @inheritDoc
     */
    public function mutateSchedule(DebtSchedule $schedule): void
    {

    }
}
