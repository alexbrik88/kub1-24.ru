<?php

namespace frontend\modules\analytics\models\credits;

interface DebtFactoryInterface
{
    /**
     * @param Credit $credit
     * @return DebtSchedule
     */
    public function createDebt(Credit $credit): DebtSchedule;
}
