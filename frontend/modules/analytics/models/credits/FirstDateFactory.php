<?php

namespace frontend\modules\analytics\models\credits;

use DateInterval;

class FirstDateFactory
{
    /**
     * @param Date $date
     * @return Date
     */
    public function createDate(Date $date): Date
    {
        return $date->add(new DateInterval('P1D'));
    }
}
