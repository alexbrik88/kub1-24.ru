<?php

namespace frontend\modules\analytics\models\credits;

class DebtDifferentiatedStrategy implements DebtStrategyInterface
{
    /**
     * @inheritDoc
     */
    public function calculate(DebtSchedule $schedule, DebtPeriod $period, float $amount): void
    {
        if ($amount <= 0) {
            return;
        }

        $credit = $schedule->credit;
        $yearLength = $credit->credit_year_length ?: $period->lastDate->getYearLength();
        $rate = $credit->credit_percent_rate / 100;
        $period->repaid = $credit->credit_amount / $this->getPeriodCount($schedule);
        $period->amount = $amount - $period->repaid;

        $period->interest = $amount;
        $period->interest *= $rate;
        $period->interest /= $yearLength;
        $period->interest *= $period->days;

        if ($period->amount < 0.01) {
            $period->amount = 0;
        }
    }

    /**
     * @inheritDoc
     */
    public function mutateOnlyPercent(DebtSchedule $schedule, DebtPeriod $period): void
    {
        $period->amount += $period->repaid;
        $period->repaid = 0;
    }

    /**
     * @inheritDoc
     */
    public function mutateSkipPayment(DebtSchedule $schedule, DebtPeriod $period): void
    {
        $period->amount += $period->repaid;
        $period->repaid = 0;
        $period->interest = 0;
    }

    /**
     * @inheritDoc
     */
    public function mutateSchedule(DebtSchedule $schedule): void
    {

    }

    /**
     * @param DebtSchedule $schedule
     * @return int
     */
    private function getPeriodCount(DebtSchedule $schedule): int
    {
        $count = count($schedule->periods);

        if ($schedule->credit->payment_first != Credit::PAYMENT_FIRST_DEFAULT) {
            $count--;
        }

        return $count;
    }
}
