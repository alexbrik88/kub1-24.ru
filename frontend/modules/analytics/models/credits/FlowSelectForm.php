<?php

namespace frontend\modules\analytics\models\credits;

use common\models\cash\CashFlowsBase;
use yii\base\InvalidConfigException;
use yii\base\Model;

/**
 * @property-read CreditFlow[] $creditFlows
 */
class FlowSelectForm extends Model
{
    use FormListItemsTrait;

    /**
     * @var int[]
     */
    public $credit_flow_id;

    /**
     * @var int
     */
    public $company_id;

    /**
     * @var int
     */
    public $credit_id;

    /**
     * @var string
     */
    public $delete;

    /**
     * @var int|null
     */
    public $income_item_id;

    /**
     * @var int|null
     */
    public $expenditure_item_id;

    /**
     * @var Flow[]
     */
    private $_creditFlows;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->company_id)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'income_item_id' => 'Статья прихода',
            'expenditure_item_id' => 'Статья расхода',
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['credit_flow_id'], 'required'],
            [['credit_flow_id'], 'each', 'rule' => ['integer']],

            [['income_item_id'], 'in', 'range' => array_keys($this->getItems('income_item_id'))],
            [['expenditure_item_id'], 'in', 'range' => array_keys($this->getItems('expenditure_item_id'))],

            [['delete'], 'string'],
        ];
    }

    /**
     * @return CreditFlow[]
     */
    public function getCreditFlows(): array
    {
        if ($this->_creditFlows == null) {
            /** @var CreditFlowRepository $repository */
            $repository = (new RepositoryFactory)->createRepository(CreditFlowRepository::class);
            $repository->credit_id = $this->credit_id;
            $this->_creditFlows = $repository->getQuery()->andWhere(['credit_flow_id' => $this->credit_flow_id])->all();
        }

        return $this->_creditFlows;
    }

    /**
     * @return bool
     */
    public function updateModels(): bool
    {
        if (!$this->creditFlows) {
            return false;
        }

        foreach ($this->creditFlows as $creditFlow) {
            $cashFlow = $creditFlow->cashFlow;

            if ($cashFlow->flow_type == CashFlowsBase::FLOW_TYPE_INCOME) {
                $cashFlow->income_item_id = $this->income_item_id ?: $cashFlow->income_item_id;
            }

            if ($cashFlow->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE) {
                $cashFlow->expenditure_item_id = $this->expenditure_item_id ?: $cashFlow->expenditure_item_id;
            }

            if (strlen($this->delete)) {
                $cashFlow->delete();
            } else {
                $cashFlow->save(false);
            }
        }

        return true;
    }
}
