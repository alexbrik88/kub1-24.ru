<?php

namespace frontend\modules\analytics\models\credits;

use common\models\company\CheckingAccountant;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\ActiveQuery;

class CheckingAccountantRepository extends Model implements RepositoryInterface, ListInterface
{
    /**
     * @var int
     */
    public $company_id;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->company_id)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function getItems(): array
    {
        $data = [];

        /** @var CheckingAccountant $model */
        foreach ($this->getProvider()->getModels() as $model) {
            $data[$model->id] = sprintf('%s, %s', $model->rs, $model->name);
        }

        return $data;
    }

    /**
     * @inheritDoc
     */
    public function getProvider(): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $this->getQuery(),
            'sort' => $this->getSort(),
            'pagination' => false,
        ]);
    }

    /**
     * @return CheckingAccountant
     */
    public function getMain(): CheckingAccountant
    {
        /** @var CheckingAccountant $checkingAccountant */
        $checkingAccountant = $this->getQuery()->andWhere(['type' => CheckingAccountant::TYPE_MAIN])->one();

        return $checkingAccountant;
    }

    /**
     * @inheritDoc
     */
    public function getQuery(): ActiveQuery
    {
        return CheckingAccountant::find()
            ->andWhere(['company_id' => $this->company_id])
            ->andWhere(['!=', 'type', CheckingAccountant::TYPE_CLOSED]);
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        return new Sort(['defaultOrder' => ['bank_name' => SORT_ASC]]);
    }
}
