<?php

namespace frontend\modules\analytics\models\credits;

use common\models\cash\Cashbox;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\ActiveQuery;

class CashboxRepository extends Model implements RepositoryInterface, ListInterface
{
    /**
     * @var int
     */
    public $company_id;

    /**
     * @var int
     */
    public $cashbox_id;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->company_id)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function getItems(): array
    {
        $data = [];

        /** @var Cashbox $model */
        foreach ($this->getProvider()->getModels() as $model) {
            $data[$model->id] = $model->name;
        }

        return $data;
    }

    /**
     * @inheritDoc
     */
    public function getQuery(): ActiveQuery
    {
        return Cashbox::find()
            ->andWhere(['company_id' => $this->company_id, 'is_closed' => false])
            ->andFilterWhere(['id' => $this->cashbox_id]);
    }

    /**
     * @inheritDoc
     */
    public function getProvider(): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $this->getQuery(),
            'sort' => $this->getSort(),
            'pagination' => false,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        return new Sort(['defaultOrder' => ['is_main' => SORT_DESC]]);
    }
}
