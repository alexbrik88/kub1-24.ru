<?php

namespace frontend\modules\analytics\models\credits;

use yii\base\Model;

class DebtCalculatorFactory extends Model
{
    /**
     * @var string[]
     */
    private const DEBT_STRATEGY_CLASSES = [
        Credit::PAYMENT_TYPE_AT_END => DebtAtEndStrategy::class,
        Credit::PAYMENT_TYPE_END_WITH_PERCENTS => DebtEndWithPercentsStrategy::class,
        Credit::PAYMENT_TYPE_DIFFERENTIATED => DebtDifferentiatedStrategy::class,
        Credit::PAYMENT_TYPE_ANNUITY => DebtAnnuityStrategy::class,
    ];

    /**
     * @param Credit $credit
     * @return DebtCalculator
     */
    public function createCalculator(Credit $credit): DebtCalculator
    {
        $strategyClass = self::DEBT_STRATEGY_CLASSES[$credit->payment_type];

        return new DebtCalculator(new DebtSchedule($credit, new $strategyClass()));
    }
}
