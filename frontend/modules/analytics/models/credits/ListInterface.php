<?php

namespace frontend\modules\analytics\models\credits;

interface ListInterface
{
    /**
     * @return string[]
     */
    public function getItems(): array;
}
