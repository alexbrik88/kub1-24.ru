<?php
namespace frontend\modules\analytics\models;

use Yii;
use yii\base\Model;
use common\models\Company;
use common\models\IncomeItemFlowOfFunds;
use common\models\ExpenseItemFlowOfFunds;
use common\models\document\InvoiceIncomeItem;
use common\models\document\InvoiceExpenditureItem;

class AnalyticsArticleDefaults extends AnalyticsArticle {

    private $_company;
    private $_incomeItems;
    private $_expenseItems;
    private $_exceptFinanceOperationsArticlesIds;

    private $_errors = [];

    public function __construct(Company $company, $config = [])
    {
        parent::__construct($config);

        $this->_company = $company;

        $this->_incomeItems = AnalyticsArticle::find()
            ->alias('a')
            ->joinWith(['invoiceIncomeItem i'])
            ->andWhere(['a.company_id' => $company->id])
            ->andWhere(['a.type' => self::FLOW_TYPE_INCOME])
            ->andWhere(['i.parent_id' => null])
            ->all();

        $this->_expenseItems = AnalyticsArticle::find()
            ->alias('a')
            ->joinWith(['invoiceExpenditureItem i'])
            ->where(['a.company_id' => $company->id])
            ->andWhere(['a.type' => self::FLOW_TYPE_EXPENSE])
            ->andWhere(['i.parent_id' => null])
            ->all();

        $this->_exceptFinanceOperationsArticlesIds = [
            self::FLOW_TYPE_INCOME => 
                IncomeItemFlowOfFunds::find()
                    ->where(['flow_of_funds_block' => AbstractFinance::FINANCIAL_OPERATIONS_BLOCK])
                    ->andWhere(['company_id' => $this->_company->id])
                    ->select(['income_item_id'])
                    ->column(),
            self::FLOW_TYPE_EXPENSE =>
                ExpenseItemFlowOfFunds::find()
                    ->where(['flow_of_funds_block' => AbstractFinance::FINANCIAL_OPERATIONS_BLOCK])
                    ->andWhere(['company_id' => $this->_company->id])
                    ->select(['expense_item_id'])
                    ->column()
        ];
    }

    public function resetToDefaults()
    {
        $this->_resetIncomeTypes();
        $this->_resetExpenseTypes();
        $this->_saveChanges();

        return empty($this->_errors);
    }

    private function _resetIncomeTypes()
    {
        /** @var AnalyticsArticle $model */

        foreach ($this->_incomeItems as $model) {
            if ($this->_isAlwaysUnset($model)) {
                $model->profit_and_loss_type = self::PAL_UNSET;
            }
            elseif ($this->_isIncomeRevenue($model)) {
                $model->profit_and_loss_type = self::PAL_INCOME_REVENUE;
            }
            elseif ($this->_isIncomeOther($model)) {
                $model->profit_and_loss_type = self::PAL_INCOME_OTHER;
            }
            elseif ($this->_isIncomePercent($model)) {
                $model->profit_and_loss_type = self::PAL_INCOME_PERCENT;
            }
            else {
                $model->profit_and_loss_type = self::PAL_UNSET;
            }
        }
    }

    private function _resetExpenseTypes()
    {
        /** @var AnalyticsArticle $model */

        foreach ($this->_expenseItems as $model) {
            if ($this->_isAlwaysUnset($model)) {
                $model->profit_and_loss_type = self::PAL_UNSET;
            }
            elseif ($this->_isExpenseVariable($model)) {
                $model->profit_and_loss_type = self::PAL_EXPENSES_VARIABLE;
            }
            elseif ($this->_isExpenseFixed($model)) {
                $model->profit_and_loss_type = self::PAL_EXPENSES_FIXED;
            }
            elseif ($this->_isExpenseOther($model)) {
                $model->profit_and_loss_type = self::PAL_EXPENSES_OTHER;
            }
            elseif ($this->_isExpensePercent($model)) {
                $model->profit_and_loss_type = self::PAL_EXPENSES_PERCENT;
            }
            elseif ($this->_isExpenseDividend($model)) {
                $model->profit_and_loss_type = self::PAL_EXPENSES_DIVIDEND;
            }
            elseif ($this->_isExpensePrimeCost($model)) {
                $model->profit_and_loss_type = self::PAL_EXPENSES_PRIME_COST;
            }
            else {
                $model->profit_and_loss_type = self::PAL_UNSET;
            }
        }
    }

    private function _saveChanges()
    {
        foreach ($this->_incomeItems as $model) {
            if (!$model->save()) {
                $this->_errors[$model->item_id] = $model->getErrors();
            }
        }
        foreach ($this->_expenseItems as $model) {
            if (!$model->save()) {
                $this->_errors[$model->item_id] = $model->getErrors();
            }
        }
    }

    /* EXPENSES */

    private function _isExpenseVariable(AnalyticsArticle $model)
    {
        $includeArticles = [
            InvoiceExpenditureItem::ITEM_DELIVERY //
        ];

        return in_array($model->item_id, $includeArticles);
    }

    private function _isExpenseFixed(AnalyticsArticle $model)
    {
        $exceptArticles = $this->_exceptFinanceOperationsArticlesIds[self::FLOW_TYPE_EXPENSE];

        $includeArticles = [
            InvoiceExpenditureItem::ITEM_IT, // АйТи
            InvoiceExpenditureItem::ITEM_LEASE, //  Аренда
            InvoiceExpenditureItem::ITEM_SALARY, // Зарплата
            InvoiceExpenditureItem::ITEM_SALARY_TAX, // Налоги на ЗП
            InvoiceExpenditureItem::ITEM_ADVERTISING, // Реклама
            InvoiceExpenditureItem::ITEM_PAYMENT_ACCOUNTABLE_PERSONS, // Выплата подотчетным лицам
            InvoiceExpenditureItem::ITEM_PROJECT_1, // Проект 1
            InvoiceExpenditureItem::ITEM_PROJECT_2, // Проект 2
            InvoiceExpenditureItem::ITEM_PROJECT_3, // Проект 3
            //
            InvoiceExpenditureItem::ITEM_BOOKKEEPING, // Бухгалтерские услуги
            InvoiceExpenditureItem::ITEM_INTERNET, // Интернет
            InvoiceExpenditureItem::ITEM_BANK_COMMISSION, // Комиссия банка
            InvoiceExpenditureItem::ITEM_FURNITURE, // Мебель
            InvoiceExpenditureItem::ITEM_OFFICE_EQUIPMENT, // Оргтехника
            InvoiceExpenditureItem::ITEM_PROGRAMS, // Программное обеспечение
            InvoiceExpenditureItem::ITEM_TELEPHONY, // Телефония
            InvoiceExpenditureItem::ITEM_SERVICE, // Услуги
            InvoiceExpenditureItem::ITEM_LAWYER_SERVICE, // Юридические услуги
            InvoiceExpenditureItem::ITEM_HOUSEHOLD_EXPENSES, // Хоз. расходы
        ];

        $isExclude = in_array($model->item_id, $exceptArticles);
        $isInclude = in_array($model->item_id, $includeArticles);
        $isSelfArticle = $model->invoiceExpenditureItem && $model->invoiceExpenditureItem->company_id;

        return !$isExclude && $isInclude || $isSelfArticle;
    }

    private function _isAlwaysUnset(AnalyticsArticle $model)
    {
        if (isset($model::UNSET_ALWAYS_ARTICLES[$model->type]))
            if (in_array($model->item_id, $model::UNSET_ALWAYS_ARTICLES[$model->type]))
                return true;

        return false;
    }

    private function _isExpenseOther(AnalyticsArticle $model)
    {
        $exceptArticles = $this->_exceptFinanceOperationsArticlesIds[self::FLOW_TYPE_EXPENSE];

        $includeArticles = [
            InvoiceExpenditureItem::ITEM_OTHER, // Прочие расходы
            InvoiceExpenditureItem::ITEM_OUTPUT_MONEY // Вывод денег
        ];

        return !in_array($model->item_id, $exceptArticles) &&
                in_array($model->item_id, $includeArticles);
    }

    private function _isExpensePercent(AnalyticsArticle $model)
    {
        $includeArticles = [
            InvoiceExpenditureItem::ITEM_PAYMENT_PERCENT // Проценты уплаченные
        ];

        return in_array($model->item_id, $includeArticles);
    }

    private function _isExpenseDividend(AnalyticsArticle $model)
    {
        $includeArticles = [
            InvoiceExpenditureItem::ITEM_DIVIDEND_PAYMENT, // Выплата дивидендов
            InvoiceExpenditureItem::ITEM_OUTPUT_PROFIT  // Вывод прибыли
        ];

        return in_array($model->item_id, $includeArticles);
    }

    private function _isExpensePrimeCost(AnalyticsArticle $model)
    {
        $includeArticles = [
            InvoiceExpenditureItem::ITEM_DELIVERY, // Доставка
            InvoiceExpenditureItem::ITEM_PRIME_COST_PRODUCT, // Себестоимость товара
            InvoiceExpenditureItem::ITEM_PRIME_COST_SERVICE, // Себестоимость услуг
            InvoiceExpenditureItem::ITEM_GOODS_CANCELLATION, // Списание товара
        ];

        return in_array($model->item_id, $includeArticles);
    }

    /* INCOME */

    private function _isIncomeRevenue(AnalyticsArticle $model)
    {
        $exceptArticles = $this->_exceptFinanceOperationsArticlesIds[self::FLOW_TYPE_INCOME];

        $exceptArticlesByType = [
            InvoiceIncomeItem::ITEM_STARTING_BALANCE, // Баланс начальный
            InvoiceIncomeItem::ITEM_RETURN, // Возврат
            InvoiceIncomeItem::ITEM_BORROWING_REDEMPTION, // Возврат займа
            InvoiceIncomeItem::ITEM_BUDGET_RETURN, // Возврат средств из бюджета
            InvoiceIncomeItem::ITEM_LOAN, // Займ
            InvoiceIncomeItem::ITEM_CREDIT, // Кредит
            InvoiceIncomeItem::ITEM_ENSURE_PAYMENT, // Обеспечительный платёж
            InvoiceIncomeItem::ITEM_FROM_FOUNDER, // Взнос от учредителя
            InvoiceIncomeItem::ITEM_OWN_FOUNDS, // Перевод между счетами
            InvoiceIncomeItem::ITEM_OTHER, // Прочие доходы
            InvoiceIncomeItem::ITEM_RECEIVED_PERCENTS, // Проценты полученные по депозитам
            InvoiceIncomeItem::ITEM_RECEIVED_PERCENTS_BY_DEPOSITS, //
            InvoiceIncomeItem::ITEM_INPUT_MONEY // Ввод денег
        ];

        $exceptArticles = array_merge($exceptArticles, $exceptArticlesByType);

        return !in_array($model->item_id, $exceptArticles);
    }

    private function _isIncomeOther(AnalyticsArticle $model)
    {
        $includeArticles = [
            InvoiceIncomeItem::ITEM_OTHER, // Прочие доходы
            InvoiceIncomeItem::ITEM_INPUT_MONEY, // Ввод денег
            InvoiceIncomeItem::ITEM_RETURN, // Возврат
            InvoiceIncomeItem::ITEM_BUDGET_RETURN, // Возврат средств из бюджета
        ];

        return in_array($model->item_id, $includeArticles);
    }

    private function _isIncomePercent(AnalyticsArticle $model)
    {
        $includeArticles = [
            InvoiceIncomeItem::ITEM_RECEIVED_PERCENTS, // Проценты полученные по займам
            InvoiceIncomeItem::ITEM_RECEIVED_PERCENTS_BY_DEPOSITS // Проценты полученные по депозитам
        ];

        return in_array($model->item_id, $includeArticles);
    }

    public function getErrrors()
    {
        return $this->_errors;
    }
}