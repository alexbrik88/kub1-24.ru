<?php
namespace frontend\modules\analytics\models;

use Yii;
use yii\db\Exception;
use common\components\date\DateHelper;
use common\components\excel\Excel;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\project\Project;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use common\models\document\InvoiceIncomeItem;
use common\models\document\InvoiceExpenditureItem;
use frontend\modules\analytics\models\AnalyticsArticle as Article;
use frontend\modules\analytics\models\AnalyticsArticleForm as Form;
use frontend\modules\analytics\models\profitAndLoss\ProfitAndLossCache;

/**
 * Class ProfitAndLossSearchModel
 * @package frontend\modules\analytics\models
 */
class ProfitAndLossSearchModel extends AbstractFinance
{
    use profitAndLoss\BaseTrait;
    use profitAndLoss\ChartsTrait;
    use profitAndLoss\RevenueTrait;
    use profitAndLoss\FixedCostsTrait;
    use profitAndLoss\AnotherCostsTrait;
    use profitAndLoss\AnotherIncomeTrait;
    use profitAndLoss\VariableCostsTrait;
    use profitAndLoss\PrimeCostsTrait;
    use profitAndLoss\OperatingCostsTrait; // not used
    use profitAndLoss\PaymentPercentTrait;
    use profitAndLoss\CalculatedRowsTrait;
    use profitAndLoss\ReceivedPercentTrait;
    use profitAndLoss\PaymentDividendTrait;
    use profitAndLoss\GlueRevenueRowsTrait;
    use profitAndLoss\AmortizationTrait;

    const MIN_REPORT_DATE = '2000-01-01';

    const TAB_ALL_OPERATIONS = 10;
    const TAB_ACCOUNTING_OPERATIONS_ONLY = 11;

    // modals params
    public $incomeItemIdManyItem;
    public $expenditureItemIdManyItem;
    public $recognitionDateManyItem;
    public $activeTab;

    // user options
    public $userOptions = [
        'skipPrepayments' => null,
        'skipTaxes' => null,
        'skipZeroesRows' => null,
        'skipCalculatePrimeCosts' => null,
        'showAmortization' => null,
        'calcWithoutNds' => null,
        'showEbit' => null,
        'revenueRuleGroup' => null,
        'revenueRule' => null,
        'monthGroupBy' => null
    ];

    // page filters
    public $pageFilters = [
        'industry_id' => null,
        'sale_point_id' => null,
        'project_id' => null
    ];

    public $cacheVersion = '1.04'; // increase version to reset cache

    public static $items = [
        [
            'label' => 'Выручка',
            'addCheckboxX' => true,
            'getter' => 'getRevenue',
            'options' => [
                'class' => 'text-bold expenditure_type',
                'data-children' => 'revenue',
            ],
        ],
        [
            'label' => 'Себестоимость',
            'addCheckboxX' => true,
            'getter' => 'getPrimeCosts',
            'options' => [
                'class' => 'text-bold expenditure_type expenses-block',
                'data-block' => ProfitAndLossCompanyItem::PRIME_COST_EXPENSE_TYPE,
                'data-children' => 'prime-costs',
            ]
        ],
        [
            'label' => 'Маржинальный доход',
            'getter' => 'getMarginalIncome',
            'options' => [
                'class' => 'text-bold expenditure_type',
            ],
        ],
        [
            'label' => 'Маржинальность',
            'getter' => 'getMarginality',
            'suffix' => '%',
            'options' => [
                'class' => 'expenditure_type text-grey',
            ],
        ],
        [
            'label' => 'Переменные расходы',
            'addCheckboxX' => true,
            'getter' => 'getVariableCosts',
            'options' => [
                'class' => 'text-bold expenditure_type expenses-block',
                'data-block' => ProfitAndLossCompanyItem::VARIABLE_EXPENSE_TYPE,
                'data-children' => 'variable-costs',
            ]
        ],
        [
            'label' => 'Валовая прибыль',
            'getter' => 'getGrossProfit',
            'options' => [
                'class' => 'text-bold expenditure_type',
            ],
        ],
        [
            'label' => 'Рентабельность по валовой прибыли',
            'getter' => 'getProfitabilityByGrossProfit',
            'suffix' => '%',
            'options' => [
                'class' => 'expenditure_type text-grey hidden',
            ],
        ],
        [
            'label' => 'Постоянные расходы',
            'addCheckboxX' => true,
            'getter' => 'getFixedCosts',
            'options' => [
                'class' => 'text-bold expenditure_type expenses-block',
                'data-block' => ProfitAndLossCompanyItem::CONSTANT_EXPENSE_TYPE,
                'data-children' => 'fixed-costs',
            ],
        ],
        //[
        //    'label' => 'Операционные расходы',
        //    'addCheckboxX' => true,
        //    'getter' => 'getOperatingCosts',
        //    'options' => [
        //        'class' => 'text-bold expenditure_type expenses-block',
        //        'data-block' => ProfitAndLossCompanyItem::OPERATING_EXPENSE_TYPE,
        //        'data-children' => 'operating-costs',
        //    ],
        //],
        [
            'label' => 'Операционная прибыль',
            'getter' => 'getOperatingProfit',
            'options' => [
                'class' => 'text-bold expenditure_type',
            ],
            'isGetterOnly' => true,
        ],
        [
            'label' => 'Другие доходы',
            'addCheckboxX' => true,
            'getter' => 'getAnotherIncome',
            'options' => [
                'class' => 'text-bold expenditure_type',
                'data-children' => 'another-income',
            ],
        ],
        [
            'label' => 'Другие расходы',
            'addCheckboxX' => true,
            'getter' => 'getAnotherCosts',
            'options' => [
                'class' => 'text-bold expenditure_type expenses-block',
                'data-block' => ProfitAndLossCompanyItem::OTHER_EXPENSE_TYPE,
                'data-children' => 'another-costs',
            ],
        ],
        [
            'label' => 'EBITDA',
            'getter' => 'getEbidta',
            'options' => [
                'class' => 'text-bold expenditure_type',
            ],
        ],
        [
            'label' => 'Амортизация',
            'addCheckboxX' => true,
            'getter' => 'getAmortization',
            'options' => [
                'class' => 'expenditure_type',
            ],
            'showByUserOption' => true,
            'userOption' => 'showAmortization'
        ],
        [
            'label' => 'EBIT',
            'getter' => 'getEbit',
            'options' => [
                'class' => 'text-bold expenditure_type',
            ],
            'showByUserOption' => true,
            'userOption' => 'showEbit'
        ],
        [
            'label' => 'Проценты полученные',
            'addCheckboxX' => true,
            'getter' => 'getReceivedPercent',
            'options' => [
                'class' => 'expenditure_type',
            ],
        ],
        [
            'label' => 'Проценты уплаченные',
            'addCheckboxX' => true,
            'getter' => 'getPaymentPercent',
            'options' => [
                'class' => 'expenditure_type',
            ],
        ],
        [
             'label' => 'Прибыль до налогообложения',
             'getter' => 'getProfitBeforeTax',
             'options' => [
                 'class' => 'expenditure_type',
             ],
        ],
        [
            'label' => 'Налог на прибыль',
            'getter' => 'getTax',
            'options' => [
                'class' => 'expenditure_type',
            ],
        ],
        [
            'label' => 'Дивиденды',
            'getter' => 'getPaymentDividend',
            'addCheckboxX' => true,
            'options' => [
                'class' => 'expenditure_type',
            ],
            'isGetterOnly' => true,
        ],
        [
            'label' => 'Чистая прибыль',
            'getter' => 'getNetIncomeLoss',
            'options' => [
                'class' => 'text-bold expenditure_type',
            ],
        ],
        [
            'label' => 'Рентабельность по чистой прибыли',
            'getter' => 'getProfitabilityByNetIncomeLoss',
            'suffix' => '%',
            'options' => [
                'class' => 'expenditure_type text-grey',
            ],
        ],
        [
            'getter' => 'getPaymentDividend',
        ],
        [
            'label' => 'Нераспределенная прибыль',
            'getter' => 'getUndestributedProfits',
            'options' => [
                'class' => 'expenditure_type',
            ],
        ]
    ];

    /**
     * @var
     */
    public $dateFrom;
    /**
     * @var
     */
    public $dateTo;

    /**
     * @var
     */
    protected $_yearFilter;

    /**
     * used like cache
     */
    protected $data = [];
    /**
     * @var string
     */
    protected $_minCashYearMonth;

    /**
     * @var array
     */
    protected static $notAccountingContractors = [];

    /**
     * @var array
     */
    protected static $ndsContractors = [];

    /**
     * @var array
     */
    protected static $articlesToParents = [];

    /**
     * @var array
     */
    protected static $groupByNames = [];

    /**
     * raw flows & docs data
     */
    protected $rawFlows = [];
    protected $rawDocs = [];
    protected $rawFlowsRevenue = [];
    protected $rawDocsRevenue = [];
    protected $exchangeDifference = [];

    /**
     * final results
     */
    protected $result = []; // total
    protected $resultByCompany = []; // per company

    /**
     * @var $params
     */
    public function init() {

        parent::init();

        $this->initUserOptions();  // 1
        $this->initOtherParams();  // 2
        $this->initGroupByNames(); // 3
    }

    private function _initYearDB($companyId, $year)
    {
        $this->initRawFlows($companyId, $year);
        $this->initRawDocs($companyId, $year);
        $this->initGoodsCancellations($companyId, $year);

        if (!$this->userOptions['skipCalculatePrimeCosts']) {
            $this->initPrimeCostsByDocs($companyId, $year);
            $this->initPrimeCostsByFlows($companyId, $year);
        }

        if (in_array($this->userOptions['revenueRuleGroup'], [Form::REVENUE_RULE_GROUP_PRODUCT, Form::REVENUE_RULE_GROUP_PRODUCT_GROUP])) {
            $this->initRawRevenueGroupedByProduct($companyId, $year);
        }

        $this->initExchangeDifference($companyId, $year);
    }

    /**
     * @param array $items
     * @return array|mixed
     */
    public function handleItems($items = [])
    {
        //////////////////////////////////////////////////
        $isExternalUse = $items || $this->hasPageFilter();
        //////////////////////////////////////////////////

        $items = empty($items) ? static::$items : $items;
        $years = array_reverse($this->getYearFilter());

        $cache = (new ProfitAndLossCache)
            ->uniqueBy($this->userOptions, $this->cacheVersion)
            ->nonCachedYear($this->year);

        foreach ($this->multiCompanyIds as $companyId) {

            $this->resultByCompany[$companyId] = [];

            foreach ($years as $year) { // cycle need for calc taxes per year

                if ($year > $this->year)
                    break;

                // enable cache only on main profit-and-loss report page
                if (!$isExternalUse) {
                    $_cached = $cache->setCompany($companyId)->setYear($year)->validate();
                    // get from Cache
                    if ($_cached) {
                        if ($this->resultByCompany[$companyId][$year] = $cache->get()) {
                            continue;
                        }
                    }
                }

                // get from DB
                $this->resultByCompany[$companyId][$year] = [];
                $this->_initYearDB($companyId, $year);

                foreach ($items as $item) {

                    $getter = ArrayHelper::remove($item, 'getter');
                    $item = self::_getBlankParent($item);

                    $rowData = call_user_func_array(
                        [$this, $getter],
                        [$item, $year, $companyId]
                    );

                    if (isset($item['isGetterOnly'])) {
                        // add results in cache to calc next values
                        continue;
                    }

                    if (isset($item['showByUserOption'])) {
                        if (!$this->userOptions[$item['userOption']] ?? false)
                            continue;
                    }

                    $this->resultByCompany[$companyId][$year] = array_merge($this->resultByCompany[$companyId][$year], $rowData);
                }

                // put in Cache
                if (isset($_cached) && $_cached) {
                    $cache->put($this->resultByCompany[$companyId][$year]);
                }
            }
        }

        $this->sumCompaniesData();

        $this->glueRevenueRows();

        $this->hideZeroesRows();

        return $this->result[$this->year] ?? [];
    }

    /**
     * Global filter for detailing reports
     * @param $filters
     */
    public function setPageFilters($filters)
    {
        $this->pageFilters = [
            'industry_id' => isset($filters['industry_id']) ? $filters['industry_id'] : null,
            'sale_point_id' => isset($filters['sale_point_id']) ? $filters['sale_point_id'] : null,
            'project_id' => isset($filters['project_id']) ? $filters['project_id'] : null
        ];
    }

    public function isExternalUse($items)
    {
        return
            $this->hasPageFilter() || // use on detalization pages (Project, Industry, SalePoint)
            md5(serialize($items)) === md5(serialize(static::$items)); // use in OperationalEfficiencySearchModel
    }

    public function hasPageFilter()
    {
        return strlen(implode('', $this->pageFilters));
    }

    public function getGroupByNames()
    {
        return self::$groupByNames;
    }

    private function initGroupByNames()
    {
        switch ($this->userOptions['monthGroupBy']) {
            case Form::MONTH_GROUP_PROJECT:
                self::$groupByNames = Project::find()
                    ->where(['company_id' => $this->_multiCompanyIds])
                    ->select(['name', 'id'])
                    ->orderBy('name')
                    ->indexBy('id')
                    ->asArray()
                    ->column() + [0 => 'Без проекта'];
                break;
            case Form::MONTH_GROUP_SALE_POINT:
                self::$groupByNames = SalePoint::find()
                        ->where(['company_id' => $this->_multiCompanyIds])
                        ->select(['name', 'id'])
                        ->orderBy('name')
                        ->indexBy('id')
                        ->asArray()
                        ->column() + [0 => 'Без точки продаж'];
                break;
            case Form::MONTH_GROUP_INDUSTRY:
                self::$groupByNames = CompanyIndustry::find()
                        ->where(['company_id' => $this->_multiCompanyIds])
                        ->select(['name', 'id'])
                        ->orderBy('name')
                        ->indexBy('id')
                        ->asArray()
                        ->column() + [0 => 'Без направления'];
                break;
            case Form::MONTH_GROUP_UNSET:
            default:
                self::$groupByNames = [];
                break;
        }
    }
    
    private function initUserOptions()
    {
        $this->userOptions['skipPrepayments'] = AnalyticsOptions::getOption($this->company->id, AnalyticsOptions::KEY_PAL_SKIP_PREPAYMENTS);
        $this->userOptions['skipTaxes'] = AnalyticsOptions::getOption($this->company->id, AnalyticsOptions::KEY_PAL_SKIP_TAXES);
        $this->userOptions['skipZeroesRows'] = AnalyticsOptions::getOption($this->company->id, AnalyticsOptions::KEY_PAL_SKIP_ZEROES_ROWS);
        $this->userOptions['skipCalculatePrimeCosts'] = AnalyticsOptions::getOption($this->company->id, AnalyticsOptions::KEY_PAL_SKIP_CALCULATE_PRIME_COSTS);
        $this->userOptions['calcWithoutNds'] = AnalyticsOptions::getOption($this->company->id, AnalyticsOptions::KEY_PAL_CALC_WITHOUT_NDS);
        $this->userOptions['showAmortization'] = AnalyticsOptions::getOption($this->company->id, AnalyticsOptions::KEY_PAL_SHOW_AMORTIZATION);
        $this->userOptions['showEbit'] = AnalyticsOptions::getOption($this->company->id, AnalyticsOptions::KEY_PAL_SHOW_EBIT);

        if (!$this->hasPageFilter()) { // detailing by single industry/sale_point page
            $this->userOptions['monthGroupBy'] = AnalyticsOptions::getOption($this->company->id, AnalyticsOptions::KEY_PAL_MONTH_GROUP_BY);
        }

        if (!$this->hasPageFilter()) { // todo: remove after adding industry_id, sale_point_id into product_turnover
            $this->userOptions['revenueRuleGroup'] = AnalyticsOptions::getOption($this->company->id, AnalyticsOptions::KEY_PAL_REVENUE_RULE_GROUP);
            $this->userOptions['revenueRule'] = AnalyticsOptions::getOption($this->company->id, AnalyticsOptions::KEY_PAL_REVENUE_RULE);
        }
    }

    /**
     * @param $key
     * @return string|null
     */
    public function getUserOption($key)
    {
        return $this->userOptions[$key] ?? null;
    }

    public function setUserOption($key, $value)
    {
        if (array_key_exists($key, $this->userOptions)) {
            $this->userOptions[$key] = $value;
            if ($key === 'monthGroupBy')
                $this->initGroupByNames();
        } else {
            throw new \yii\base\Exception("No user option found: {$key}");
        }
    }

    private function initOtherParams()
    {
        self::$notAccountingContractors = $this->_getNotAccountingContractors();
        self::$articlesToParents = self::getArticles2Parents(CashFlowsBase::FLOW_TYPE_EXPENSE, $this->multiCompanyIds);
        if ($this->userOptions['calcWithoutNds'])
            self::$ndsContractors = $this->_getNdsContractors(); // all flows are multiplied by 100/120

        $this->_minCashYearMonth = date('Ym');

        if ($this->_company->companyTaxationType->osno) {
            if (isset(self::$items[0]['label'])) {
                self::$items[0]['label'] .= ($this->userOptions['calcWithoutNds']
                    ? ' (без НДС)'
                    : ' (с НДС)'
                );
            }
        }
    }

    public static function getCachePrefix($className, $companyId)
    {
        $className = explode('\\', $className);
        $className = array_pop($className);
        if ('get' == substr($className, 0, 3))
            $className = substr($className, 3);
        if ('Trait' == substr($className, -5))
            $className = substr($className, 0, -5);

        return lcfirst($className) .'_'. $companyId . '_';
    }

    public function glueRevenueRows()
    {
        switch ($this->userOptions['revenueRule']) {
            case Form::REVENUE_RULE_TOP:
                $this->_glueRevenueRowsByTop(10);
                break;
            case Form::REVENUE_RULE_LAST_5:
                $this->_glueRevenueRowsByPercent(5);
                break;
            case Form::REVENUE_RULE_LAST_10:
                $this->_glueRevenueRowsByPercent(10);
                break;
            case Form::REVENUE_RULE_LAST_20:
                $this->_glueRevenueRowsByPercent(20);
                break;
            case Form::REVENUE_RULE_UNSET:
            default:
                break;
        }
    }

    public function hideZeroesRows()
    {
        if ($this->userOptions['skipZeroesRows']) {

            $hideRows = [
                'totalAnotherIncome',
                'totalAnotherCosts',
                'totalReceivedPercent',
                'totalPaymentPercent',
                'tax',
                'totalPaymentDividend',
                'totalPrimeCosts'
            ];

            $hideRowsByRows = [
                'totalPrimeCosts' => [
                    'marginalIncome',
                    'marginality'
                ]
            ];

            $showRowsByRows = [
                'totalPrimeCosts' => [
                    'profitabilityByGrossProfit'
                ]
            ];

            foreach ($this->result[$this->year] as $rowKey => $rowValue) {
                if (in_array($rowKey, $hideRows)) {
                    if (isset($rowValue['amount']) && array_sum($rowValue['amount']) == 0) {
                        // hide row
                        unset($this->result[$this->year][$rowKey]);
                        // hide related rows
                        if (isset($hideRowsByRows[$rowKey])) {
                            foreach ($hideRowsByRows[$rowKey] as $rKey) {
                                unset($this->result[$this->year][$rKey]);
                            }
                        }
                        // show related rows
                        if (isset($showRowsByRows[$rowKey])) {
                            $showRowsFn = function(&$v, $k) use ($showRowsByRows, $rowKey) {
                                if (in_array($k, $showRowsByRows[$rowKey])) {
                                    $v['options']['class'] = str_replace('hidden', '', $v['options']['class']);
                                }
                            };
                            array_walk($this->result[$this->year], $showRowsFn);
                        }
                    }
                }
            }
        }
    }

    public function sumCompaniesData()
    {
        foreach ($this->resultByCompany as $companyId => $level1)
        {
            foreach ($level1 as $year => $level2)
            {
                foreach ($level2 as $rowKey => $data)
                {
                    if (!isset($this->result[$year])) {
                        $this->result[$year] = [];
                    }

                    if (!isset($this->result[$year][$rowKey]))
                    {
                        // parent item
                        $this->result[$year][$rowKey] = [
                            'label' => $data['label'],
                            'addCheckboxX' => $data['addCheckboxX'] ?? false,
                            'options' => $data['options'] ?? [],
                            'suffix' => $data['suffix'] ?? null,
                            'amount' => $data['amount'],
                            'amountTax' => $data['amountTax'],
                            'groupedAmount' => $data['groupedAmount'] ?? [],
                            'groupedAmountTax' => $data['groupedAmountTax'] ?? [],
                            'items' => []
                        ];

                        // item
                        if (!$this->result[$year][$rowKey]['addCheckboxX']) {
                            $this->result[$year][$rowKey]['data-block'] = $data['data-block'] ?? null;
                            $this->result[$year][$rowKey]['data-article'] = $data['data-article'] ?? null;
                            $this->result[$year][$rowKey]['data-movement-type'] = $data['data-movement-type'] ?? null;
                            $this->result[$year][$rowKey]['data-hover_text'] = $data['data-hover_text'] ?? null;
                        }

                        // sub items
                        if (!empty($data['items'])) {
                            $this->result[$year][$rowKey]['items'] = $data['items'];
                        }

                    } else {

                        foreach ($data['amount'] as $amountKey => $amountValue) {
                            $this->result[$year][$rowKey]['amount'][$amountKey] += $amountValue;
                        }
                        foreach ($data['amountTax'] as $amountKey => $amountValue) {
                            $this->result[$year][$rowKey]['amountTax'][$amountKey] += $amountValue;
                        }
                        if (!empty($data['items'])) {
                            // subitems unique per company
                            $this->result[$year][$rowKey]['items'] = $data['items'];
                        }
                    }
                }
            }
        }
    }

    /**
     * For use in Dashboard
     */
    public function getRevenueDetalizationItems(): array
    {
        $ret = [];
        foreach ($this->result as $year => $data) {
            foreach ($data as $k => $v) {
                if (substr($k, 0, 8) === 'revenue_') {
                    $ret[$k] = $v['label'];
                }
            }
        }
        return $ret;
    }

    /**
     * For external use in BalanceSearch
     */
    public function getResultYears()
    {
        return array_keys($this->result);
    }

    /**
     * For use in Charts
     */
    public function getProfitAndLossItem($name, $year)
    {
        if (isset($this->result[$year]) && isset($this->result[$year][$name]))
            return $this->result[$year][$name];

        return [];
    }

    /**
     * For external use in BalanceSearch & charts
     */
    public function getRow($name, $year, $amountKey = 'amount')
    {
        if (isset($this->result[$year]) && isset($this->result[$year][$name]) && isset($this->result[$year][$name][$amountKey]))
            return $this->result[$year][$name][$amountKey];

        return [];
    }

    /**
     * For external use in BalanceSearch & charts
     */
    public function getValue($name, $year, $month, $amountKey = 'amount')
    {
        $month = str_pad($month, 2, "0", STR_PAD_LEFT);

        if (isset($this->result[$year]) &&
            isset($this->result[$year][$name]) &&
            isset($this->result[$year][$name][$amountKey]) &&
            isset($this->result[$year][$name][$amountKey][$month]))

                return $this->result[$year][$name][$amountKey][$month];

        return 0;
    }

    /**
     * For external use in tables & charts
     */
    public function getPeriodSum($names, $dateFrom, $dateTo, $amountKey = 'amount'): int
    {
        $start = date_create_from_format('Y-m-d', $dateFrom);
        $end = date_create_from_format('Y-m-d', $dateTo);

        $ret = 0;
        foreach ((array)$names as $name) {
            $curr = clone $start;
            while (true) {
                $year = $curr->format('Y');
                $month = $curr->format('m');
                if (isset($this->result[$year]) &&
                    isset($this->result[$year][$name]) &&
                    isset($this->result[$year][$name][$amountKey]) &&
                    isset($this->result[$year][$name][$amountKey][$month]))

                    $ret += $this->result[$year][$name][$amountKey][$month];

                $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
                $curr = $curr->modify("+1 second");

                if ($curr->format('Ym') > $end->format('Ym'))
                    break;
            }
        }

        return $ret;
    }

    /**
     * For external use in DetailingController
     */
    public function getTotalGroupedByMonth($year, $totalKey)
    {
        $ret = [];

        if (isset($this->result[$year]) &&
            isset($this->result[$year][$totalKey]) &&
            isset($this->result[$year][$totalKey]['groupedAmount'])) {

                foreach ($this->result[$year][$totalKey]['groupedAmount'] as $groupedBy => $monthData) {
                    $ret[$groupedBy] = $monthData['total'];
                }
        }

        return $ret;
    }

    /**
     * For external use in project\ProjectSearch
     */
    public function getPeriodSumGroupedByMonth($totalKey, $dateFrom, $dateTo)
    {
        $ret = [];

        $start = date_create_from_format('Y-m-d', $dateFrom);
        $end = date_create_from_format('Y-m-d', $dateTo);

        $curr = $start;
        while(true) {
            $year = $curr->format('Y');
            $month = $curr->format('m');
            if (isset($this->result[$year]) &&
                isset($this->result[$year][$totalKey]) &&
                isset($this->result[$year][$totalKey]['groupedAmount'])) {

                foreach ($this->result[$year][$totalKey]['groupedAmount'] as $groupedBy => $monthData) {
                    if (!isset($ret[$groupedBy]))
                        $ret[$groupedBy] = 0;

                    $ret[$groupedBy] += $monthData[$month];
                }
            }

            $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
            $curr = $curr->modify("+1 second");

            if ($curr->format('Ym') > $end->format('Ym'))
                break;
        }

        return $ret;
    }

    /**
     * For external use in DetailingController
     */
    public function getMonthDataGroupedByMonth($year, $totalKey)
    {
        $ret = [];

        if (isset($this->result[$year]) &&
            isset($this->result[$year][$totalKey]) &&
            isset($this->result[$year][$totalKey]['groupedAmount'])) {

            return $this->result[$year][$totalKey]['groupedAmount'];
        }

        return [];
    }

    public function getFromCurrentMonthsPeriods($left, $right, $offsetYear = 0, $offsetMonth = "0") {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month")->modify("{$offsetMonth} month");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr->format('Y-m-01'),
                'to' => $curr->format('Y-m-t'),
            ];
            $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        if (!isset($this->_yearFilter)) {
            $range = [];
            $cashOrderMinDate = CashOrderFlows::find()
                ->byCompany($this->multiCompanyIds)
                ->min('date');
            $cashBankMinDate = CashBankFlows::find()
                ->byCompany($this->multiCompanyIds)
                ->min('date');
            $cashEmoneyMinDate = CashEmoneyFlows::find()
                ->byCompany($this->multiCompanyIds)
                ->min('date');
            $minDates = [];

            if ($cashOrderMinDate) $minDates[] = $cashOrderMinDate;
            if ($cashBankMinDate) $minDates[] = $cashBankMinDate;
            if ($cashEmoneyMinDate) $minDates[] = $cashEmoneyMinDate;

            $minCashDate = !empty($minDates) ? max(self::MIN_REPORT_DATE, min($minDates)) : date(DateHelper::FORMAT_DATE);
            $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
            $currentYear = date('Y');
            foreach (range($registrationYear, $currentYear) as $value) {
                $range[$value] = $value;
            }
            arsort($range);
            $this->_yearFilter = $range;
            $this->_minCashYearMonth = DateHelper::format($minCashDate, 'Ym', DateHelper::FORMAT_DATE);
        }

        return $this->_yearFilter;
    }

    public function setYearFilter($yearsArray)
    {
        $this->_yearFilter = $yearsArray;
    }

    /**
     * @param $items
     * @return mixed
     */
    private function calculateQuarterAndTotalAmount($items)
    {
        // clear before calc
        foreach ($items as $ki => $data) {
            if (strpos($ki, 'exchangeDifference') !== false) {
                continue;
            }
            foreach (['amount', 'amountTax'] as $ka) {

                if (!isset($data[$ka])) {
                    continue;
                }

                foreach ($data[$ka] as $month => $oneAmount) {
                    if (in_array($month, ['quarter-1', 'quarter-2', 'quarter-3', 'quarter-4', 'total']))
                        unset($items[$ki][$ka][$month]);
                }
            }
        }

        foreach ($items as $ki => $data) {
            foreach (['amount', 'amountTax'] as $ka) {

                if (!isset($data[$ka])) {
                    continue;
                }

                foreach ($data[$ka] as $month => $oneAmount) {

                    if ((int)$month < 1 || (int)$month > 12)
                        continue;

                    if (!isset($items[$ki][$ka]['quarter-' . ceil($month / 3)])) {
                        $items[$ki][$ka]['quarter-' . ceil($month / 3)] = $oneAmount;
                    } else {
                        $items[$ki][$ka]['quarter-' . ceil($month / 3)] += $oneAmount;
                    }
                    if (!isset($items[$ki][$ka]['total'])) {
                        $items[$ki][$ka]['total'] = $oneAmount;
                    } else {
                        $items[$ki][$ka]['total'] += $oneAmount;
                    }
                }
            }
        }

        foreach ($items as $k => $v) {
            if (isset($items[$k]['items']))
                $items[$k]['items'] = $this->calculateQuarterAndTotalAmount($items[$k]['items']);
        }

        // month groups
        foreach ($items as $ki => $data) {
            foreach (['groupedAmount', 'groupedAmountTax'] as $kaGrouped) {

                if (!isset($data[$kaGrouped])) {
                    continue;
                }

                foreach ($data[$kaGrouped] as $gid => $kaData) {

                    foreach ($kaData as $month => $oneAmount) {

                        if ((int)$month < 1 || (int)$month > 12)
                            continue;

                        if (!isset($items[$ki][$kaGrouped][$gid]['quarter-' . ceil($month / 3)])) {
                            $items[$ki][$kaGrouped][$gid]['quarter-' . ceil($month / 3)] = $oneAmount;
                        } else {
                            $items[$ki][$kaGrouped][$gid]['quarter-' . ceil($month / 3)] += $oneAmount;
                        }
                        if (!isset($items[$ki][$kaGrouped][$gid]['total'])) {
                            $items[$ki][$kaGrouped][$gid]['total'] = $oneAmount;
                        } else {
                            $items[$ki][$kaGrouped][$gid]['total'] += $oneAmount;
                        }
                    }
                }
            }
        }

        return $items;
    }

    public static function _getBlankParent($item)
    {
        foreach (self::$month as $monthNumber => $monthText) {
            $item['amount'][$monthNumber] = 0;
            $item['amountTax'][$monthNumber] = 0;
        }

        $item['groupedAmount'] = [];
        $item['groupedAmountTax'] = [];

        if (self::$groupByNames) {
            foreach (self::$groupByNames as $id => $name) {
                $item['groupedAmount'][$id] = self::_zeroAmountArray();
                $item['groupedAmountTax'][$id] = self::_zeroAmountArray();
            }
        }

        return $item;
    }

    private static function _zeroAmountArray()
    {
        return ['01' => 0, '02' => 0, '03' => 0, '04' => 0, '05' => 0, '06' => 0, '07' => 0, '08' => 0, '09' => 0, '10' => 0, '11' => 0, '12' => 0] +
            ['quarter-1' => 0, 'quarter-2' => 0, 'quarter-3' => 0, 'quarter-4' => 0] +
            ['total' => 0];
    }

    private static function _getBlankItem($class, $name, $dataBlockId = null, $dataArticle = null, $movementType = null)
    {
        $item = [
            'label' => htmlspecialchars($name),
            'options' => [
                'class' => 'hidden ' . $class,
                'style' => 'padding-left: 25px;'
            ],
            'data-block' => $dataBlockId, // todo: rename param (like revenueGroupedItemId)
            'data-article' => $dataArticle,
            'data-movement-type' => $movementType,
            'amount' => self::_zeroAmountArray(),
            'amountTax' => self::_zeroAmountArray(),
            'groupedAmount' => [],
            'groupedAmountTax' => [],
            'items' => []
        ];

        if (self::$groupByNames) {
            foreach (self::$groupByNames as $id => $name) {
                $item['groupedAmount'][$id] = self::_zeroAmountArray();
                $item['groupedAmountTax'][$id] = self::_zeroAmountArray();
            }
        }

        return $item;
    }

    public function getMinCashYearMonth()
    {
        if (!isset($this->_yearFilter))
            $this->getYearFilter();

        return $this->_minCashYearMonth;
    }

    /// XLS

    public function generateXls($type)
    {
        $data = $this->handleItems();

        // header
        $columns = [];
        $columns[] = [
            'attribute' => 'itemName',
            'header' => 'Статьи',
        ];
        foreach (AbstractFinance::$month as $monthNumber => $monthText) {
            $columns[] = [
                'attribute' => "item{$monthNumber}",
                'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                'header' => $monthText,
            ];
        }
        $columns[] = [
            'attribute' => 'dataYear',
            'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
            'header' => $this->year,
        ];

        // data
        $formattedData = [];
        foreach ($data as $row) {
            $formattedData[] = $this->_buildXlsRow($row, $type);
        }

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $formattedData,
            'title' => "ОПиУ за {$this->year} год",
            'rangeHeader' => range('A', 'N'),
            'columns' => $columns,
            'format' => 'Xlsx',
            'fileName' => "ОПиУ за {$this->year} год",
        ]);
    }

    protected function _buildXlsRow($data, $type)
    {
        $amountKey = ($type == self::TAB_ACCOUNTING_OPERATIONS_ONLY) ? 'amountTax' : 'amount';
        $row = [
            'itemName' => trim(strip_tags($data['label'] ?? '')),
            'dataYear' => ($data[$amountKey]['total'] ?? '0.00') / 100
        ];
        foreach (AbstractFinance::$month as $monthNumber => $monthText) {
            $row['item' . $monthNumber] = ($data[$amountKey][$monthNumber] ?? '0.00') / 100;
        }

        return $row;
    }

    //////
    /**
     *
     */
    public static function refreshAnalyticsArticles()
    {
        $company = Yii::$app->user->identity->company;

        $isFirstRun = !(AnalyticsArticle::find()->where(['company_id' => $company->id])->exists());

        self::_refreshAnalyticsArticles($company->id, CashFlowsBase::FLOW_TYPE_EXPENSE);
        self::_refreshAnalyticsArticles($company->id, CashFlowsBase::FLOW_TYPE_INCOME);

        if ($isFirstRun) {
            $model = new AnalyticsArticleDefaults($company);
            $model->resetToDefaults();

            AnalyticsOptions::setPalResetButtonDisable($company->id);
        }

//        todo: drop ProfitAndLossCompanyItem
//
//        foreach (static::$expenseTypes as $expenseType => $expenditureItems) {
//
//            $expenditureItemsWithChildren = AbstractFinance::getArticlesWithChildren($expenditureItems, CashFlowsBase::FLOW_TYPE_EXPENSE, $company->id);
//
//            $items = InvoiceExpenditureItem::find()
//                ->select([
//                    'id',
//                    'company_id',
//                ])
//                ->andWhere(['in', 'id', $expenditureItemsWithChildren]);
//            if ($expenseType == static::CONSTANT_EXPENSE_TYPE) {
//                $items->orWhere(['company_id' => $company->id]);
//            }
//            foreach ($items->asArray()->all() as $item) {
//                if (!self::find()
//                    ->andWhere(['and',
//                        ['company_id' => $company->id],
//                        ['item_id' => $item['id']],
//                    ])->exists()) {
//                    $balanceCompanyItem = new ProfitAndLossCompanyItem();
//                    $balanceCompanyItem->company_id = $company->id;
//                    $balanceCompanyItem->item_id = $item['id'];
//                    $balanceCompanyItem->expense_type = $expenseType;
//                    $balanceCompanyItem->can_update = in_array($item['id'], static::$canUpdate) || $item['company_id'] == $company->id;
//                    $balanceCompanyItem->save();
//                }
//            }
//        }
    }

    private static function _refreshAnalyticsArticles($companyId, $type)
    {
        $articlesOptionsTable = AnalyticsArticle::tableName();
        $itemsTable = ($type == CashFlowsBase::FLOW_TYPE_INCOME) ?
            InvoiceIncomeItem::tableName() :
            InvoiceExpenditureItem::tableName();

        $nonExistsItems = Yii::$app->db->createCommand("
            SELECT i.id
            FROM `{$itemsTable}` i 
            LEFT JOIN `{$articlesOptionsTable}` a 
            ON i.`id` = a.`item_id` AND a.`type` = {$type} AND a.`company_id` = {$companyId} 
            WHERE (i.`company_id` IS NULL OR i.`company_id` = {$companyId}) 
              AND a.`item_id` IS NULL
              AND i.`id` > (SELECT IFNULL(MAX(`item_id`), 0) FROM `{$articlesOptionsTable}` WHERE `company_id` = {$companyId} AND `type` = {$type})
        ")->queryColumn();

        foreach ($nonExistsItems as $itemId) {
            $model = new AnalyticsArticle([
                'company_id' => $companyId,
                'type' => $type,
                'item_id' => $itemId,
                'profit_and_loss_type' => AnalyticsArticle::PAL_UNSET
            ]);

            if (!$model->save()) {
                throw new Exception('Can\'t save analytics article item');
            }
        }

        return true;
    }
}