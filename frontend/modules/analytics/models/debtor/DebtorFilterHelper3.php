<?php

namespace frontend\modules\analytics\models\debtor;

use common\models\cash\CashFlowsBase;
use common\models\company\CompanyPrepaymentWallets;
use common\models\ExpenseItemFlowOfFunds;
use common\models\IncomeItemFlowOfFunds;
use frontend\modules\analytics\models\AbstractFinance;

class DebtorFilterHelper3 {

    private static $_INCLUDE_PREPAYMENT_ITEMS = [
        CashFlowsBase::FLOW_TYPE_INCOME => [],
        CashFlowsBase::FLOW_TYPE_EXPENSE => []
    ];

    private static $_INCLUDE_PREPAYMENT_WALLETS = [];

    public static function getSqlFilterItems($companyIds, $flowType, $excludeItems = [])
    {
        $ret = '';

        if (empty(self::$_INCLUDE_PREPAYMENT_ITEMS[$flowType])) {

            self::$_INCLUDE_PREPAYMENT_ITEMS[CashFlowsBase::FLOW_TYPE_INCOME] =
                IncomeItemFlowOfFunds::find()
                    ->select('income_item_id')
                    ->distinct()
                    ->where(['company_id' => $companyIds])
                    ->andWhere(['is_prepayment' => true])
                    ->column(\Yii::$app->db2);

            self::$_INCLUDE_PREPAYMENT_ITEMS[CashFlowsBase::FLOW_TYPE_EXPENSE] =
                ExpenseItemFlowOfFunds::find()
                    ->select('expense_item_id')
                    ->distinct()
                    ->where(['company_id' => $companyIds])
                    ->andWhere(['is_prepayment' => true])
                    ->column(\Yii::$app->db2);

            if (array_key_exists(CashFlowsBase::FLOW_TYPE_INCOME, $excludeItems)) {
                self::$_INCLUDE_PREPAYMENT_ITEMS[CashFlowsBase::FLOW_TYPE_INCOME] =
                    array_diff(
                        self::$_INCLUDE_PREPAYMENT_ITEMS[CashFlowsBase::FLOW_TYPE_INCOME],
                        $excludeItems[CashFlowsBase::FLOW_TYPE_INCOME]);
            }

            if (array_key_exists(CashFlowsBase::FLOW_TYPE_EXPENSE, $excludeItems)) {
                self::$_INCLUDE_PREPAYMENT_ITEMS[CashFlowsBase::FLOW_TYPE_EXPENSE] =
                    array_diff(
                        self::$_INCLUDE_PREPAYMENT_ITEMS[CashFlowsBase::FLOW_TYPE_EXPENSE],
                        $excludeItems[CashFlowsBase::FLOW_TYPE_EXPENSE]);
            }
        }

        if (!empty(self::$_INCLUDE_PREPAYMENT_ITEMS[$flowType]))
            $ret .= ' AND t.item_id IN (' . implode(',', self::$_INCLUDE_PREPAYMENT_ITEMS[$flowType]) . ')';
        else
            $ret .= ' AND 1<>1';

        return $ret;
    }

    public static function getSqlFilterWallets($companyIds)
    {
        $ret = [];

        if (empty(self::$_INCLUDE_PREPAYMENT_WALLETS)) {

            /** @var CompanyPrepaymentWallets $wallet */
            $walletsList = CompanyPrepaymentWallets::find()
                ->where(['company_id' => $companyIds])
                ->all();

            foreach ($walletsList as $wallet) 
            {
                $companyId = $wallet->company_id;
                
                if ($wallet->bank && $wallet->order && $wallet->emoney)
                    continue;
                elseif (!$wallet->bank && !$wallet->order && !$wallet->emoney)
                    self::$_INCLUDE_PREPAYMENT_WALLETS[$companyId] = [];
                else {
                    self::$_INCLUDE_PREPAYMENT_WALLETS[$companyId] = [];
                    if ($wallet->bank)
                        self::$_INCLUDE_PREPAYMENT_WALLETS[$companyId][] = AbstractFinance::CASH_BANK_BLOCK;
                    if ($wallet->order)
                        self::$_INCLUDE_PREPAYMENT_WALLETS[$companyId][] = AbstractFinance::CASH_ORDER_BLOCK;
                    if ($wallet->emoney)
                        self::$_INCLUDE_PREPAYMENT_WALLETS[$companyId][] = AbstractFinance::CASH_EMONEY_BLOCK;
                }
            }
        }

        if (!empty(self::$_INCLUDE_PREPAYMENT_WALLETS)) {
            foreach (self::$_INCLUDE_PREPAYMENT_WALLETS as $companyId => $includedCompanyWallets) {
                if (!$includedCompanyWallets)
                    $ret[] = '(t.company_id <> ' . $companyId . ')';
                else
                    $ret[] = '(t.company_id = ' . $companyId . ' AND t.wallet IN (' . implode(',', $includedCompanyWallets) . '))';
            }
        }

        return ($ret) ? ' AND (' . implode(' OR ', $ret). ') ' : '';
    }
}