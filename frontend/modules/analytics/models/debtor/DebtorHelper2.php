<?php

namespace frontend\modules\analytics\models\debtor;

use common\models\cash\CashFlowsBase;
use common\models\Company;
use common\models\company\CompanyPrepaymentWallets;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\ExpenseItemFlowOfFunds;
use common\models\IncomeItemFlowOfFunds;
use frontend\modules\analytics\models\AbstractFinance;
use Yii;
use common\models\document\status\InvoiceStatus;
use frontend\models\Documents;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class DebtorHelper2 {

    /**
     *
     */
    const SEARCH_BY_INVOICES = 1;
    const SEARCH_BY_DOCUMENTS = 2;
    const SEARCH_BY_DEBT_PERIODS = 3;
    static $SEARCH_BY = self::SEARCH_BY_INVOICES;

    /**
     *
     */
    const REPORT_TYPE_MONEY = 1;
    const REPORT_TYPE_GUARANTY = 2;
    const REPORT_TYPE_NET_TOTAL = 3;
    static $REPORT_TYPE = self::REPORT_TYPE_MONEY;

    /**
     * Periods
     */
    const PERIOD_0_10 = 1;
    const PERIOD_11_30 = 2;
    const PERIOD_31_60 = 3;
    const PERIOD_61_90 = 4;
    const PERIOD_MORE_90 = 5;
    const PERIOD_0_MORE_90 = 6;
    const PERIOD_ACTUAL = 7;
    const PERIOD_OVERDUE = 8;

    private static $_mainChartsData;

    /**
     * load before get
     */
    private static $_INCLUDE_PREPAYMENT_ITEMS = [
        CashFlowsBase::FLOW_TYPE_INCOME => [],
        CashFlowsBase::FLOW_TYPE_EXPENSE => []
    ];
    /**
     ** load before get
     */
    private static $_INCLUDE_PREPAYMENT_WALLETS = [];

    public static function getQueryByInvoices($companyIds, $documentType = null, $customContractor = null, $customEmployee = null)
    {
        // by invoice payment_limit_date
        $queryInvoice1 = (new Query)
            ->select([
                'id' => 'invoice.id',
                'type' => 'invoice.type',
                'contractor_id' => 'invoice.contractor_id',
                'invoice_expenditure_item_id' => 'invoice.invoice_expenditure_item_id',
                'date' => 'invoice.document_date',
                'payment_limit_date' => 'invoice.payment_limit_date',
                'invoice_sum' => '1/100 * IFNULL(invoice.payment_limit_percent, 100) * IFNULL(invoice.total_amount_with_nds, 0)',
                new Expression('0 AS flow_sum'),
            ])
            ->from(['invoice' => 'invoice'])
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $companyIds,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => [InvoiceStatus::STATUS_REJECTED, InvoiceStatus::STATUS_AUTOINVOICE]]])
            ->andWhere(['invoice.payment_limit_rule' => [Invoice::PAYMENT_LIMIT_RULE_INVOICE, Invoice::PAYMENT_LIMIT_RULE_MIXED]])
            ->andFilterWhere(['invoice.type' => $documentType]);

        // by related doc payment_limit_date
        $queryInvoice2 = (new Query)
            ->select([
                'id' => 'invoice.id',
                'type' => 'invoice.type',
                'contractor_id' => 'invoice.contractor_id',
                'invoice_expenditure_item_id' => 'invoice.invoice_expenditure_item_id',
                'date' => 'invoice.max_related_document_date',
                'payment_limit_date' => 'invoice.related_document_payment_limit_date',
                'invoice_sum' => '1/100 * IFNULL(invoice.related_document_payment_limit_percent, 0) * IFNULL(invoice.total_amount_with_nds, 0)',
                new Expression('0 AS flow_sum'),
            ])
            ->from(['invoice' => 'invoice'])
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $companyIds,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => [InvoiceStatus::STATUS_REJECTED, InvoiceStatus::STATUS_AUTOINVOICE]]])
            ->andWhere(['invoice.payment_limit_rule' => [Invoice::PAYMENT_LIMIT_RULE_DOCUMENT, Invoice::PAYMENT_LIMIT_RULE_MIXED]])
            ->andFilterWhere(['invoice.type' => $documentType]);

        $queryBank = (new Query)
            ->select([
                'id' => 'cbf.id',
                'type' => 'invoice.type',
                'contractor_id' => 'invoice.contractor_id',
                'invoice_expenditure_item_id' => 'invoice.invoice_expenditure_item_id',
                'date' => 'cbf.recognition_date',
                'payment_limit_date' => 'invoice.payment_limit_date',
                new Expression('0 AS invoice_sum'),
                'flow_sum' => 'IF(cbf.parent_id > 0, cbf_relation.tin_child_amount, cbf_relation.amount)'
            ])
            ->from(['invoice' => 'invoice'])
            ->leftJoin(['cbf_relation' => 'cash_bank_flow_to_invoice'], 'cbf_relation.invoice_id = invoice.id')
            ->leftJoin(['cbf' => 'cash_bank_flows'], 'cbf.id = cbf_relation.flow_id')
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $companyIds,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => [InvoiceStatus::STATUS_REJECTED, InvoiceStatus::STATUS_AUTOINVOICE]]])
            ->andFilterWhere(['invoice.type' => $documentType])
            ->andWhere(['cbf.has_tin_children' => 0]);

        $queryCashbox = (new Query)
            ->select([
                'id' => 'cof.id',
                'type' => 'invoice.type',
                'contractor_id' => 'invoice.contractor_id',
                'invoice_expenditure_item_id' => 'invoice.invoice_expenditure_item_id',
                'date' => 'cof.recognition_date',
                'payment_limit_date' => 'invoice.payment_limit_date',
                new Expression('0 AS invoice_sum'),
                'flow_sum' => 'IFNULL(cof_relation.amount, 0)'
            ])
            ->from(['invoice' => 'invoice'])
            ->leftJoin(['cof_relation' => 'cash_order_flow_to_invoice'], 'cof_relation.invoice_id = invoice.id')
            ->leftJoin(['cof' => 'cash_order_flows'], 'cof.id = cof_relation.flow_id')
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $companyIds,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => [InvoiceStatus::STATUS_REJECTED, InvoiceStatus::STATUS_AUTOINVOICE]]])
            ->andFilterWhere(['invoice.type' => $documentType])
            ->andWhere(['>', 'cof.amount', 0]);

        $queryEmoney = (new Query)
            ->select([
                'id' => 'cef.id',
                'type' => 'invoice.type',
                'contractor_id' => 'invoice.contractor_id',
                'invoice_expenditure_item_id' => 'invoice.invoice_expenditure_item_id',
                'date' => 'cef.recognition_date',
                'payment_limit_date' => 'invoice.payment_limit_date',
                new Expression('0 AS invoice_sum'),
                'flow_sum' => 'IFNULL(cef_relation.amount, 0)'
            ])
            ->from(['invoice' => 'invoice'])
            ->leftJoin(['cef_relation' => 'cash_emoney_flow_to_invoice'], 'cef_relation.invoice_id = invoice.id')
            ->leftJoin(['cef' => 'cash_emoney_flows'], 'cef.id = cef_relation.flow_id')
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $companyIds,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => [InvoiceStatus::STATUS_REJECTED, InvoiceStatus::STATUS_AUTOINVOICE]]])
            ->andFilterWhere(['invoice.type' => $documentType])
            ->andWhere(['>', 'cef.amount', 0]);

        // filter by Contractor
        if ($customContractor) {
            $queryInvoice1
                ->andWhere(['invoice.contractor_id' => $customContractor]);
            $queryInvoice2
                ->andWhere(['invoice.contractor_id' => $customContractor]);
            $queryBank
                ->andWhere(['invoice.contractor_id' => $customContractor]);
            $queryCashbox
                ->andWhere(['invoice.contractor_id' => $customContractor]);
            $queryEmoney
                ->andWhere(['invoice.contractor_id' => $customContractor]);
        }

        // filter by Responsible Employee
        if ($customEmployee) {
            $queryInvoice1
                ->addSelect(['responsible_employee_id' => 'contractor.responsible_employee_id'])
                ->leftJoin(['contractor' => 'contractor'], 'invoice.contractor_id = contractor.id')
                ->andWhere(['contractor.responsible_employee_id' => $customEmployee]);
            $queryInvoice2
                ->addSelect(['responsible_employee_id' => 'contractor.responsible_employee_id'])
                ->leftJoin(['contractor' => 'contractor'], 'invoice.contractor_id = contractor.id')
                ->andWhere(['contractor.responsible_employee_id' => $customEmployee]);
            $queryBank
                ->addSelect(['responsible_employee_id' => 'contractor.responsible_employee_id'])
                ->leftJoin(['contractor' => 'contractor'], 'invoice.contractor_id = contractor.id')
                ->andWhere(['contractor.responsible_employee_id' => $customEmployee]);
            $queryCashbox
                ->addSelect(['responsible_employee_id' => 'contractor.responsible_employee_id'])
                ->leftJoin(['contractor' => 'contractor'], 'invoice.contractor_id = contractor.id')
                ->andWhere(['contractor.responsible_employee_id' => $customEmployee]);
            $queryEmoney
                ->addSelect(['responsible_employee_id' => 'contractor.responsible_employee_id'])
                ->leftJoin(['contractor' => 'contractor'], 'invoice.contractor_id = contractor.id')
                ->andWhere(['contractor.responsible_employee_id' => $customEmployee]);
        }

        return $queryInvoice1->union($queryInvoice2, true)->union($queryBank, true)->union($queryCashbox, true)->union($queryEmoney, true);
    }

    public static function getQueryByDocs($companyIds, $documentType = null, $customContractor = null, $customEmployee = null)
    {
        $queryInvoice = (new Query)
            ->select([
                'id' => 'invoice.id',
                'type' => 'invoice.type',
                'contractor_id' => 'invoice.contractor_id',
                'invoice_expenditure_item_id' => 'invoice.invoice_expenditure_item_id',
                'date' => 'invoice.document_date',
                'payment_limit_date' => 'invoice.payment_limit_date',
                'invoice_sum' => 'IFNULL(invoice.total_amount_with_nds, 0)',
                new Expression('0 AS flow_sum'),
            ])
            ->from(['invoice' => 'invoice'])
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $companyIds,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => [InvoiceStatus::STATUS_REJECTED, InvoiceStatus::STATUS_AUTOINVOICE]]])
            ->andWhere(['or', ['invoice.need_act' => false], ['invoice.need_packing_list' => false], ['invoice.need_upd' => false]])
            ->andFilterWhere(['invoice.type' => $documentType]);

        $queryPackingList = (new Query)
            ->select([
                'id' => 'packing_list.id',
                'type' => 'packing_list.type',
                'contractor_id' => 'invoice.contractor_id',
                'invoice_expenditure_item_id' => 'invoice.invoice_expenditure_item_id',
                //'date' => 'packing_list.document_date',
                //'payment_limit_date' => 'invoice.payment_limit_date',
                'date' => new Expression('IF (invoice.payment_limit_rule = 1, packing_list.document_date, invoice.max_related_document_date)'),
                'payment_limit_date' => new Expression('IF (invoice.payment_limit_rule = 1, invoice.payment_limit_date, invoice.related_document_payment_limit_date)'),
                'invoice_sum' => 'IFNULL(packing_list.orders_sum, 0)',
                new Expression('0 AS flow_sum'),
            ])
            ->from(['invoice' => 'invoice'])
            ->leftJoin('packing_list', 'packing_list.invoice_id = invoice.id')
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $companyIds,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => [InvoiceStatus::STATUS_REJECTED, InvoiceStatus::STATUS_AUTOINVOICE]]])
            ->andFilterWhere(['invoice.type' => $documentType])
            ->groupBy('packing_list.id');

        $queryAct = (new Query)
            ->select([
                'id' => 'act.id',
                'type' => 'act.type',
                'contractor_id' => 'invoice.contractor_id',
                'invoice_expenditure_item_id' => 'invoice.invoice_expenditure_item_id',
                //'date' => 'act.document_date',
                //'payment_limit_date' => 'invoice.payment_limit_date',
                'date' => new Expression('IF (invoice.payment_limit_rule = 1, act.document_date, invoice.max_related_document_date)'),
                'payment_limit_date' => new Expression('IF (invoice.payment_limit_rule = 1, invoice.payment_limit_date, invoice.related_document_payment_limit_date)'),
                'invoice_sum' => 'IFNULL(act.order_sum, 0)',
                new Expression('0 AS flow_sum'),
            ])
            ->from(['invoice' => 'invoice'])
            ->leftJoin('invoice_act', 'invoice_act.invoice_id = invoice.id')
            ->leftJoin('act', 'invoice_act.act_id = act.id')
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $companyIds,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => [InvoiceStatus::STATUS_REJECTED, InvoiceStatus::STATUS_AUTOINVOICE]]])
            ->andFilterWhere(['invoice.type' => $documentType])
            ->groupBy('act.id');

        $queryUpd = (new Query)
            ->select([
                'id' => 'upd.id',
                'type' => 'upd.type',
                'contractor_id' => 'invoice.contractor_id',
                'invoice_expenditure_item_id' => 'invoice.invoice_expenditure_item_id',
                //'date' => 'upd.document_date',
                //'payment_limit_date' => 'invoice.payment_limit_date',
                'date' => new Expression('IF (invoice.payment_limit_rule = 1, upd.document_date, invoice.max_related_document_date)'),
                'payment_limit_date' => new Expression('IF (invoice.payment_limit_rule = 1, invoice.payment_limit_date, invoice.related_document_payment_limit_date)'),
                'invoice_sum' => 'IFNULL(upd.orders_sum, 0)',
                new Expression('0 AS flow_sum'),
            ])
            ->from(['invoice' => 'invoice'])
            ->leftJoin('invoice_upd', 'invoice_upd.invoice_id = invoice.id')
            ->leftJoin('upd', 'invoice_upd.upd_id = upd.id')
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $companyIds,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => [InvoiceStatus::STATUS_REJECTED, InvoiceStatus::STATUS_AUTOINVOICE]]])
            ->andFilterWhere(['invoice.type' => $documentType])
            ->groupBy('upd.id');

        $queryBank = (new Query)
            ->select([
                'id' => 'cbf.id',
                'type' => 'invoice.type',
                'contractor_id' => 'invoice.contractor_id',
                'invoice_expenditure_item_id' => 'invoice.invoice_expenditure_item_id',
                'date' => 'cbf.recognition_date',
                'payment_limit_date' => 'invoice.payment_limit_date',
                new Expression('0 AS invoice_sum'),
                'flow_sum' => 'IF(cbf.parent_id > 0, cbf_relation.tin_child_amount, cbf_relation.amount)'
            ])
            ->from(['invoice' => 'invoice'])
            ->leftJoin(['cbf_relation' => 'cash_bank_flow_to_invoice'], 'cbf_relation.invoice_id = invoice.id')
            ->leftJoin(['cbf' => 'cash_bank_flows'], 'cbf.id = cbf_relation.flow_id')
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $companyIds,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => [InvoiceStatus::STATUS_REJECTED, InvoiceStatus::STATUS_AUTOINVOICE]]])
            ->andFilterWhere(['invoice.type' => $documentType])
            ->andWhere(['cbf.has_tin_children' => 0]);

        $queryCashbox = (new Query)
            ->select([
                'id' => 'cof.id',
                'type' => 'invoice.type',
                'contractor_id' => 'invoice.contractor_id',
                'invoice_expenditure_item_id' => 'invoice.invoice_expenditure_item_id',
                'date' => 'cof.recognition_date',
                'payment_limit_date' => 'invoice.payment_limit_date',
                new Expression('0 AS invoice_sum'),
                'flow_sum' => 'IFNULL(cof_relation.amount, 0)'
            ])
            ->from(['invoice' => 'invoice'])
            ->leftJoin(['cof_relation' => 'cash_order_flow_to_invoice'], 'cof_relation.invoice_id = invoice.id')
            ->leftJoin(['cof' => 'cash_order_flows'], 'cof.id = cof_relation.flow_id')
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $companyIds,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => [InvoiceStatus::STATUS_REJECTED, InvoiceStatus::STATUS_AUTOINVOICE]]])
            ->andFilterWhere(['invoice.type' => $documentType])
            ->andWhere(['>', 'cof.amount', 0]);

        $queryEmoney = (new Query)
            ->select([
                'id' => 'cef.id',
                'type' => 'invoice.type',
                'contractor_id' => 'invoice.contractor_id',
                'invoice_expenditure_item_id' => 'invoice.invoice_expenditure_item_id',
                'date' => 'cef.recognition_date',
                'payment_limit_date' => 'invoice.payment_limit_date',
                new Expression('0 AS invoice_sum'),
                'flow_sum' => 'IFNULL(cef_relation.amount, 0)'
            ])
            ->from(['invoice' => 'invoice'])
            ->leftJoin(['cef_relation' => 'cash_emoney_flow_to_invoice'], 'cef_relation.invoice_id = invoice.id')
            ->leftJoin(['cef' => 'cash_emoney_flows'], 'cef.id = cef_relation.flow_id')
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $companyIds,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => [InvoiceStatus::STATUS_REJECTED, InvoiceStatus::STATUS_AUTOINVOICE]]])
            ->andFilterWhere(['invoice.type' => $documentType])
            ->andWhere(['>', 'cef.amount', 0]);

        // filter by Contractor
        if ($customContractor) {
            $queryPackingList
                ->andWhere(['invoice.contractor_id' => $customContractor]);
            $queryAct
                ->andWhere(['invoice.contractor_id' => $customContractor]);
            $queryUpd
                ->andWhere(['invoice.contractor_id' => $customContractor]);
            $queryBank
                ->andWhere(['invoice.contractor_id' => $customContractor]);
            $queryCashbox
                ->andWhere(['invoice.contractor_id' => $customContractor]);
            $queryEmoney
                ->andWhere(['invoice.contractor_id' => $customContractor]);
        }

        // filter by Responsible Employee
        if ($customEmployee) {
            $queryInvoice
                ->addSelect(['responsible_employee_id' => 'contractor.responsible_employee_id'])
                ->leftJoin(['contractor' => 'contractor'], 'invoice.contractor_id = contractor.id')
                ->andWhere(['contractor.responsible_employee_id' => $customEmployee]);
            $queryPackingList
                ->addSelect(['responsible_employee_id' => 'contractor.responsible_employee_id'])
                ->leftJoin(['contractor' => 'contractor'], 'invoice.contractor_id = contractor.id')
                ->andWhere(['contractor.responsible_employee_id' => $customEmployee]);
            $queryAct
                ->addSelect(['responsible_employee_id' => 'contractor.responsible_employee_id'])
                ->leftJoin(['contractor' => 'contractor'], 'invoice.contractor_id = contractor.id')
                ->andWhere(['contractor.responsible_employee_id' => $customEmployee]);
            $queryUpd
                ->addSelect(['responsible_employee_id' => 'contractor.responsible_employee_id'])
                ->leftJoin(['contractor' => 'contractor'], 'invoice.contractor_id = contractor.id')
                ->andWhere(['contractor.responsible_employee_id' => $customEmployee]);
            $queryBank
                ->addSelect(['responsible_employee_id' => 'contractor.responsible_employee_id'])
                ->leftJoin(['contractor' => 'contractor'], 'invoice.contractor_id = contractor.id')
                ->andWhere(['contractor.responsible_employee_id' => $customEmployee]);
            $queryCashbox
                ->addSelect(['responsible_employee_id' => 'contractor.responsible_employee_id'])
                ->leftJoin(['contractor' => 'contractor'], 'invoice.contractor_id = contractor.id')
                ->andWhere(['contractor.responsible_employee_id' => $customEmployee]);
            $queryEmoney
                ->addSelect(['responsible_employee_id' => 'contractor.responsible_employee_id'])
                ->leftJoin(['contractor' => 'contractor'], 'invoice.contractor_id = contractor.id')
                ->andWhere(['contractor.responsible_employee_id' => $customEmployee]);
        }

        $queryBank->andWhere(['or',
            ['or', ['invoice.has_act' => true], ['invoice.has_packing_list' => true], ['invoice.has_upd' => true]],
            ['or', ['invoice.need_act' => false], ['invoice.need_packing_list' => false], ['invoice.need_upd' => false]],
        ]);
        $queryCashbox->andWhere(['or',
            ['or', ['invoice.has_act' => true], ['invoice.has_packing_list' => true], ['invoice.has_upd' => true]],
            ['or', ['invoice.need_act' => false], ['invoice.need_packing_list' => false], ['invoice.need_upd' => false]],
        ]);
        $queryEmoney->andWhere(['or',
            ['or', ['invoice.has_act' => true], ['invoice.has_packing_list' => true], ['invoice.has_upd' => true]],
            ['or', ['invoice.need_act' => false], ['invoice.need_packing_list' => false], ['invoice.need_upd' => false]],
        ]);

        return $queryInvoice
            ->union($queryPackingList, true)
            ->union($queryAct, true)
            ->union($queryUpd, true)
            ->union($queryBank, true)
            ->union($queryCashbox, true)
            ->union($queryEmoney, true);
    }

    public static function getPaymentLimitRule($customDebtType, $intervalCalc)
    {
        switch ($customDebtType) {
            case self::PERIOD_ACTUAL:
                $paymentLimitRule = "payment_limit_date >= ($intervalCalc)";
                break;
            case self::PERIOD_OVERDUE:
                $paymentLimitRule = "payment_limit_date <  ($intervalCalc)";
                break;
            case self::PERIOD_0_10:
                $paymentLimitRule = "(payment_limit_date <= ($intervalCalc) - INTERVAL  1 DAY) AND (payment_limit_date >= ($intervalCalc) - INTERVAL 10 DAY)";
                break;
            case self::PERIOD_11_30:
                $paymentLimitRule = "(payment_limit_date <= ($intervalCalc) - INTERVAL 11 DAY) AND (payment_limit_date >= ($intervalCalc) - INTERVAL 30 DAY)";
                break;
            case self::PERIOD_31_60:
                $paymentLimitRule = "(payment_limit_date <= ($intervalCalc) - INTERVAL 31 DAY) AND (payment_limit_date >= ($intervalCalc) - INTERVAL 60 DAY)";
                break;
            case self::PERIOD_61_90:
                $paymentLimitRule = "(payment_limit_date <= ($intervalCalc) - INTERVAL 61 DAY) AND (payment_limit_date >= ($intervalCalc) - INTERVAL 90 DAY)";
                break;
            case self::PERIOD_MORE_90:
                $paymentLimitRule = "(payment_limit_date <= ($intervalCalc) - INTERVAL 91 DAY)";
                break;
            default:
                $paymentLimitRule = "1 = 1";
                break;
        }

        return $paymentLimitRule;
    }

    /** PREPAYMENTS */

    public static function getGuarantyLimitRule($customDebtType, $intervalCalc, $docType)
    {
        $attr = $docType == Documents::IO_TYPE_IN ? 'seller_guaranty_delay' : 'customer_guaranty_delay';
        switch ($customDebtType) {
            case self::PERIOD_ACTUAL:
                $paymentLimitRule = "t.date >= ($intervalCalc) - INTERVAL c.{$attr} DAY";
                break;
            case self::PERIOD_OVERDUE:
                $paymentLimitRule = "t.date <  ($intervalCalc) - INTERVAL c.{$attr} DAY";
                break;
            case self::PERIOD_0_10:
                $paymentLimitRule = "(t.date <= ($intervalCalc) - INTERVAL  1 DAY) AND (t.date >= ($intervalCalc) - INTERVAL 10 DAY)";
                break;
            case self::PERIOD_11_30:
                $paymentLimitRule = "(t.date <= ($intervalCalc) - INTERVAL 11 DAY) AND (t.date >= ($intervalCalc) - INTERVAL 30 DAY)";
                break;
            case self::PERIOD_31_60:
                $paymentLimitRule = "(t.date <= ($intervalCalc) - INTERVAL 31 DAY) AND (t.date >= ($intervalCalc) - INTERVAL 60 DAY)";
                break;
            case self::PERIOD_61_90:
                $paymentLimitRule = "(t.date <= ($intervalCalc) - INTERVAL 61 DAY) AND (t.date >= ($intervalCalc) - INTERVAL 90 DAY)";
                break;
            case self::PERIOD_MORE_90:
                $paymentLimitRule = "(t.date <= ($intervalCalc) - INTERVAL 91 DAY)";
                break;
            default:
                $paymentLimitRule = "1 = 1";
                break;
        }

        return $paymentLimitRule;
    }

    public static function getPrepayments($customDebtType, $customContractor, $customEmployee, $dateFrom, $monthsCount, $type = Documents::IO_TYPE_OUT)
    {
        $companyId = \Yii::$app->user->identity->company->id;

        if (empty(self::$_mainChartsData)) {

            // SWITCH TYPE BY REPORT
            if (self::$REPORT_TYPE == self::REPORT_TYPE_GUARANTY) {
                $docType = ($type == 2) ? 1 : 2;
                $flowType = ($type == 2) ? 0 : 1;
            } else {
                $docType = $type;
                $flowType = ($type == 2) ? 1 : 0;
            }

            $tableDocs = 'olap_invoices';
            $tableFlows = 'olap_flows';
            $tableContractor = 'contractor';
            $seq = "seq_0_to_{$monthsCount}";
            $intervalCalc = "IF (('{$dateFrom}' + INTERVAL (seq) MONTH) >= '" . date('Y-m-01') . "', '" . date('Y-m-d') . "', LAST_DAY('{$dateFrom}' + INTERVAL (seq) MONTH))";
            $interval = "('{$dateFrom}' + INTERVAL (seq) MONTH)";

            $paymentLimitRule = self::getGuarantyLimitRule($customDebtType, $intervalCalc, $docType);

            $SQL_CONTRACTOR_FILTER = $SQL_EMPLOYEE_FILTER = '';
            if ($customContractor) {
                $SQL_CONTRACTOR_FILTER = ' AND contractor_id = ' . (int)$customContractor;
            }
            if ($customEmployee) {
                $contractorsIds = Contractor::find()
                    ->where(['company_id' => $companyId])
                    ->andWhere(['responsible_employee_id' => (int)$customEmployee])
                    ->select('id')
                    ->column(Yii::$app->db2);
                if (empty($contractorsIds))
                    $contractorsIds = [-1];
                $SQL_EMPLOYEE_FILTER = ' AND contractor_id IN (' . implode(',', $contractorsIds).')';
            }

            $SQL_FILTER_ITEMS = self::getSqlFilterItems($flowType);
            $SQL_FILTER_WALLETS = self::getSqlFilterWallets();
            $AND_HIDE_TIN_PARENT = self::getSqlHideTinParent();

            $queryDocs = "
                SELECT
                YEAR({$interval}) y,
                MONTH({$interval}) m,
                contractor_id,
                SUM(IF({$paymentLimitRule}, t.total_amount_with_nds, 0)) AS amount,
                SUM(t.total_amount_with_nds) AS total
                FROM {$seq}
                LEFT JOIN {$tableDocs} t
                    ON  (t.company_id = {$companyId})
                    AND (FROM_UNIXTIME(t.status_updated_at, '%Y-%m-%d') <= {$intervalCalc})
                    AND (t.is_deleted = FALSE)
                    AND (t.type = {$docType})
                    AND (t.status_id = ".InvoiceStatus::STATUS_PAYED.")
                    AND (t.has_doc = FALSE OR (t.has_doc = TRUE AND t.doc_date > {$intervalCalc}))
                    AND (t.need_all_docs = TRUE)
                    {$SQL_CONTRACTOR_FILTER}
                    {$SQL_EMPLOYEE_FILTER}
                LEFT JOIN {$tableContractor} c ON t.contractor_id = c.id
                GROUP BY contractor_id, {$interval}
           ";

           $queryFlows = "
                SELECT
                YEAR({$interval}) y,
                MONTH({$interval}) m,
                contractor_id,
                SUM(IF({$paymentLimitRule}, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS amount,
                SUM(IF(t.has_tin_parent = 1, t.tin_child_amount, t.amount)) AS total
                FROM {$seq}
                JOIN {$tableFlows} t
                    ON  (t.company_id = {$companyId})
                    AND (t.recognition_date <= {$intervalCalc})
                    AND (t.type = {$flowType})
                    AND (t.contractor_id > 0)
                    {$SQL_FILTER_ITEMS}
                    {$SQL_FILTER_WALLETS}
                    {$SQL_CONTRACTOR_FILTER}
                    {$SQL_EMPLOYEE_FILTER}
                    {$AND_HIDE_TIN_PARENT}
                LEFT JOIN {$tableContractor} c ON t.contractor_id = c.id
                GROUP BY contractor_id, {$interval}
           ";

           $query = "
                SELECT y, m, COUNT(DISTINCT CASE WHEN amount > 0 THEN contractor_id END) AS contractors_count, SUM(amount) AS amount, SUM(total) AS total
                FROM ({$queryDocs} UNION ALL {$queryFlows}) z
                GROUP BY y, m
           ";

            $monthSum = Yii::$app->db->createCommand($query)->queryAll();

              self::$_mainChartsData = [
                'fact' => [],
                'percent' => [],
                'contractors_count' => []
            ];

            foreach ($monthSum as $sum) {
                if ($sum['y'] == date('Y') && $sum['m'] > date('m') || $sum['y'] > date('Y')) {
                    continue;
                }

                self::$_mainChartsData['fact'][] = (float)$sum['amount'] * 1 / 100;
                self::$_mainChartsData['percent'][] = (float)($sum['total'] > 0) ? (100 * $sum['amount'] / $sum['total']) : 0;
                self::$_mainChartsData['contractors_count'][] = (float)$sum['contractors_count'];
            }
        }

        return self::$_mainChartsData;
    }


    public static function getSqlFilterItems($flowType, $excludeItems = [])
    {
        $ret = '';

        if (empty(self::$_INCLUDE_PREPAYMENT_ITEMS[$flowType])) {
            $companyId = \Yii::$app->user->identity->company->id;
            self::$_INCLUDE_PREPAYMENT_ITEMS[CashFlowsBase::FLOW_TYPE_INCOME] =
                IncomeItemFlowOfFunds::find()
                    ->select('income_item_id')
                    ->where(['company_id' => $companyId])
                    ->andWhere(['is_prepayment' => true])
                    ->column(\Yii::$app->db2);
            self::$_INCLUDE_PREPAYMENT_ITEMS[CashFlowsBase::FLOW_TYPE_EXPENSE] =
                ExpenseItemFlowOfFunds::find()
                    ->select('expense_item_id')
                    ->where(['company_id' => $companyId])
                    ->andWhere(['is_prepayment' => true])
                    ->column(\Yii::$app->db2);

            if (array_key_exists(CashFlowsBase::FLOW_TYPE_INCOME, $excludeItems)) {
                self::$_INCLUDE_PREPAYMENT_ITEMS[CashFlowsBase::FLOW_TYPE_INCOME] =
                    array_diff(
                        self::$_INCLUDE_PREPAYMENT_ITEMS[CashFlowsBase::FLOW_TYPE_INCOME],
                        $excludeItems[CashFlowsBase::FLOW_TYPE_INCOME]);
            }
            if (array_key_exists(CashFlowsBase::FLOW_TYPE_EXPENSE, $excludeItems)) {
                self::$_INCLUDE_PREPAYMENT_ITEMS[CashFlowsBase::FLOW_TYPE_EXPENSE] =
                    array_diff(
                        self::$_INCLUDE_PREPAYMENT_ITEMS[CashFlowsBase::FLOW_TYPE_EXPENSE],
                        $excludeItems[CashFlowsBase::FLOW_TYPE_EXPENSE]);
            }
        }

        if (!empty(self::$_INCLUDE_PREPAYMENT_ITEMS[$flowType]))
            $ret .= ' AND t.item_id IN (' . implode(',', DebtorHelper2::$_INCLUDE_PREPAYMENT_ITEMS[$flowType]) . ')';
        else
            $ret .= ' AND 1<>1';

        return $ret;
    }

    public static function getSqlFilterWallets()
    {
        $ret = '';
        if (empty(self::$_INCLUDE_PREPAYMENT_WALLETS)) {
            $companyId = \Yii::$app->user->identity->company->id;
            $wallets = CompanyPrepaymentWallets::find()
                ->where(['company_id' => $companyId])
                ->one();

            if (!$wallets) {
                return '';
            } else if ($wallets->bank && $wallets->order && $wallets->emoney)
                return '';
            elseif (!$wallets->bank && !$wallets->order && !$wallets->emoney)
                DebtorHelper2::$_INCLUDE_PREPAYMENT_WALLETS[] = '-1';
            else {
                if ($wallets->bank)
                    DebtorHelper2::$_INCLUDE_PREPAYMENT_WALLETS[] = AbstractFinance::CASH_BANK_BLOCK;
                if ($wallets->order)
                    DebtorHelper2::$_INCLUDE_PREPAYMENT_WALLETS[] = AbstractFinance::CASH_ORDER_BLOCK;
                if ($wallets->emoney)
                    DebtorHelper2::$_INCLUDE_PREPAYMENT_WALLETS[] = AbstractFinance::CASH_EMONEY_BLOCK;
            }
        }

        if (!empty(self::$_INCLUDE_PREPAYMENT_WALLETS))
            $ret .= ' AND t.wallet IN ( '. implode(',', DebtorHelper2::$_INCLUDE_PREPAYMENT_WALLETS) .')';

        return $ret;
    }

    public static function getDebts($customDebtType, $customContractor, $customEmployee, $dateFrom, $monthsCount, $documentType = Documents::IO_TYPE_OUT)
    {
        $companyId = \Yii::$app->user->identity->company->id;

        if (empty(self::$_mainChartsData)) {
            $seq = "seq_0_to_{$monthsCount}";
            $intervalCalc = "IF (('{$dateFrom}' + INTERVAL (seq) MONTH) >= '" . date('Y-m-01') . "', '" . date('Y-m-d') . "', LAST_DAY('{$dateFrom}' + INTERVAL (seq) MONTH))";
            $interval = "('{$dateFrom}' + INTERVAL (seq) MONTH)";
            $paymentLimitRule = self::getPaymentLimitRule($customDebtType, $intervalCalc);

            if (self::$SEARCH_BY == self::SEARCH_BY_INVOICES) {
                $subQuery = self::getQueryByInvoices($companyId, $documentType, $customContractor, $customEmployee);
            } else {
                $subQuery = self::getQueryByDocs($companyId, $documentType, $customContractor, $customEmployee);
            }

            $query = "
                SELECT
                YEAR({$interval}) y,
                MONTH({$interval}) m,
                contractor_id,
                SUM(invoice_sum - flow_sum) AS total,
                SUM(IF({$paymentLimitRule}, invoice_sum - flow_sum, 0)) AS amount
                FROM {$seq}
                LEFT JOIN (" . $subQuery->createCommand()->rawSql . ") t ON (t.date) <= {$intervalCalc}
                GROUP BY contractor_id, YEAR({$interval}), MONTH({$interval})
                ORDER BY YEAR({$interval}), MONTH({$interval})
            ";

            $monthSum = \Yii::$app->db2->createCommand("
                SELECT 
                y, 
                m, 
                COUNT(DISTINCT CASE WHEN amount > 0 THEN contractor_id END) AS contractors_count, 
                SUM(CASE WHEN amount > 0 THEN amount END) AS amount, 
                SUM(CASE WHEN total > 0 THEN total END) AS total
                FROM ({$query}) z
                GROUP BY y, m
            ")->queryAll();

            self::$_mainChartsData = [
                'fact' => [],
                'percent' => [],
                'contractors_count' => []
            ];

            foreach ($monthSum as $sum) {
                if ($sum['y'] == date('Y') && $sum['m'] > date('m') || $sum['y'] > date('Y')) {
                    continue;
                }

                self::$_mainChartsData['fact'][] = (float)$sum['amount'] * 1 / 100;
                self::$_mainChartsData['percent'][] = (float)($sum['total'] > 0) ? (100 * $sum['amount'] / $sum['total']) : 0;
                self::$_mainChartsData['contractors_count'][] = (float)$sum['contractors_count'];
            }
        }

        return self::$_mainChartsData;
    }

    public static function getTotalNet($dateFrom, $monthsCount)
    {
        $companyId = \Yii::$app->user->identity->company->id;

        $seq = "seq_0_to_{$monthsCount}";
        $intervalCalc = "IF (('{$dateFrom}' + INTERVAL (seq) MONTH) >= '" . date('Y-m-01') . "', '" . date('Y-m-d') . "', LAST_DAY('{$dateFrom}' + INTERVAL (seq) MONTH))";
        $interval = "('{$dateFrom}' + INTERVAL (seq) MONTH)";
        $subQuery = self::__getTotalNetQuery($companyId);
        $query = "
            SELECT
            DATE_FORMAT({$interval}, '%Y%m') AS ym,
            SUM(t.money_1 + t.guarancy_2) AS total_1,
            SUM(t.money_2 + t.guarancy_1) AS total_2,
            SUM(t.money_2 - t.money_1 + t.guarancy_1 - t.guarancy_2) AS total_diff
            FROM {$seq}
            LEFT JOIN (" . $subQuery . ") t ON (t.date) <= {$intervalCalc}
            GROUP BY YEAR({$interval}), MONTH({$interval})
            ORDER BY YEAR({$interval}), MONTH({$interval})
        ";

        $monthSum = \Yii::$app->db2->createCommand($query)->queryAll();

        $ret = [
            'in' => [],
            'out' => [],
            'diff' => []
        ];

        foreach ($monthSum as $sum) {

            if (($ym = $sum['ym']) > date('Ym'))
                continue;

            $ret['out'][$ym] = (float)$sum['total_2'] * 1 / 100;
            $ret['in'][$ym] = (float)$sum['total_1'] * 1 / 100;
            $ret['diff'][$ym] = (float)$sum['total_diff'] * 1 / 100;
        }

        return [
            'in' => array_values($ret['in']),
            'out' => array_values($ret['out']),
            'diff' => array_values($ret['diff']),
        ];
    }

    private static function __getTotalNetQuery($companyId)
    {
        $tableFlows = 'olap_flows';
        $tableInvoices = 'olap_invoices';

        $_FLOW_ITEMS_IN = self::getSqlFilterItems(0);
        $_FLOW_ITEMS_OUT = self::getSqlFilterItems(1);
        $SQL_FILTER_FLOW_ITEMS = "AND (t.type = 0 {$_FLOW_ITEMS_IN} OR t.type = 1 {$_FLOW_ITEMS_OUT})";
        $SQL_FILTER_WALLETS = self::getSqlFilterWallets();
        $AND_HIDE_TIN_PARENT = self::getSqlHideTinParent();

        return "

                SELECT
                  t.date,
                  SUM(IF(t.type = 1, t.total_amount_with_nds - t.payment_partial_amount, 0)) AS money_1,
                  SUM(IF(t.type = 2, t.total_amount_with_nds - t.payment_partial_amount, 0)) AS money_2,
                  0 AS guarancy_1,
                  0 AS guarancy_2
                FROM {$tableInvoices} t
                WHERE t.company_id = {$companyId}
                  AND t.status_id NOT IN (5,7,9)
                  AND t.is_deleted = 0
                  AND (t.has_doc = 1 OR t.need_all_docs = 0)
                GROUP BY t.date

                UNION ALL

                SELECT
                  FROM_UNIXTIME(t.status_updated_at, '%Y-%m-%d') AS `date`,
                  0 AS money_1,
                  0 AS money_2,
                  SUM(IF(t.type = 1, t.total_amount_with_nds, 0)) AS guarancy_1,
                  SUM(IF(t.type = 2, t.total_amount_with_nds, 0)) AS guarancy_2
                FROM {$tableInvoices} t
                WHERE t.company_id = {$companyId}
                  AND t.is_deleted = FALSE
                  AND t.status_id = " . InvoiceStatus::STATUS_PAYED . "
                  AND t.has_doc = FALSE
                  AND t.need_all_docs = TRUE
                GROUP BY `date`

                UNION ALL

                SELECT
                  t.date,
                  0 AS money_1,
                  0 AS money_2,
                  SUM(IF(t.type = 0, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS guarancy_1,
                  SUM(IF(t.type = 1, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS guarancy_2
                FROM {$tableFlows} t
                WHERE t.company_id = {$companyId}
                  AND (t.contractor_id > 0)
                  {$SQL_FILTER_FLOW_ITEMS}
                  {$SQL_FILTER_WALLETS}
                  {$AND_HIDE_TIN_PARENT}
                GROUP BY t.date

        ";
    }

    public static function getSqlHideTinParent()
    {
        return " AND t.has_tin_children = 0 ";
    }

    // INDICATORS

    public static function getQueryPeriodsAvg(array $companyIds, int $documentType, string $dateFrom, string $dateTo): Query
    {
        $queryInvoice = (new Query)
            ->select([
                'invoice_id' => 'invoice.id',
                'contractor_id' => 'invoice.contractor_id',
                'date' => 'invoice.document_date',
                'invoice_date' => 'invoice.document_date',
                new Expression('NULL AS flow_date'),
                'invoice_sum' => 'IFNULL(invoice.total_amount_with_nds, 0)',
                new Expression('0 AS flow_sum'),
            ])
            ->from(['invoice' => 'invoice'])
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $companyIds,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => [InvoiceStatus::STATUS_REJECTED, InvoiceStatus::STATUS_AUTOINVOICE]]])
            ->andFilterWhere(['invoice.type' => $documentType])
            ->andWhere(['between', 'invoice.document_date', $dateFrom, $dateTo]);

        $queryBank = (new Query)
            ->select([
                'invoice_id' => 'invoice.id',
                'contractor_id' => 'invoice.contractor_id',
                'date' => 'cbf.recognition_date',
                new Expression('NULL AS invoice_date'),
                'flow_date' => 'cbf.recognition_date',
                new Expression('0 AS invoice_sum'),
                'flow_sum' => 'IF(cbf.parent_id > 0, cbf_relation.tin_child_amount, cbf_relation.amount)'
            ])
            ->from(['invoice' => 'invoice'])
            ->leftJoin(['cbf_relation' => 'cash_bank_flow_to_invoice'], 'cbf_relation.invoice_id = invoice.id')
            ->leftJoin(['cbf' => 'cash_bank_flows'], 'cbf.id = cbf_relation.flow_id')
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $companyIds,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => [InvoiceStatus::STATUS_REJECTED, InvoiceStatus::STATUS_AUTOINVOICE]]])
            ->andFilterWhere(['invoice.type' => $documentType])
            ->andWhere(['cbf.has_tin_children' => 0])
            //->andWhere(['between', 'cbf.recognition_date', $dateFrom, $dateTo])
            ->andWhere(['between', 'invoice.document_date', $dateFrom, $dateTo]);

        $queryCashbox = (new Query)
            ->select([
                'invoice_id' => 'invoice.id',
                'contractor_id' => 'invoice.contractor_id',
                'date' => 'cof.recognition_date',
                new Expression('NULL AS invoice_date'),
                'flow_date' => 'cof.recognition_date',
                new Expression('0 AS invoice_sum'),
                'flow_sum' => 'IFNULL(cof_relation.amount, 0)'
            ])
            ->from(['invoice' => 'invoice'])
            ->leftJoin(['cof_relation' => 'cash_order_flow_to_invoice'], 'cof_relation.invoice_id = invoice.id')
            ->leftJoin(['cof' => 'cash_order_flows'], 'cof.id = cof_relation.flow_id')
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $companyIds,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => [InvoiceStatus::STATUS_REJECTED, InvoiceStatus::STATUS_AUTOINVOICE]]])
            ->andFilterWhere(['invoice.type' => $documentType])
            ->andWhere(['>', 'cof.amount', 0])
            //->andWhere(['between', 'cof.recognition_date', $dateFrom, $dateTo])
            ->andWhere(['between', 'invoice.document_date', $dateFrom, $dateTo]);

        $queryEmoney = (new Query)
            ->select([
                'invoice_id' => 'invoice.id',
                'contractor_id' => 'invoice.contractor_id',
                'date' => 'cef.recognition_date',
                new Expression('NULL AS invoice_date'),
                'flow_date' => 'cef.recognition_date',
                new Expression('0 AS invoice_sum'),
                'flow_sum' => 'IFNULL(cef_relation.amount, 0)'
            ])
            ->from(['invoice' => 'invoice'])
            ->leftJoin(['cef_relation' => 'cash_emoney_flow_to_invoice'], 'cef_relation.invoice_id = invoice.id')
            ->leftJoin(['cef' => 'cash_emoney_flows'], 'cef.id = cef_relation.flow_id')
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $companyIds,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => [InvoiceStatus::STATUS_REJECTED, InvoiceStatus::STATUS_AUTOINVOICE]]])
            ->andFilterWhere(['invoice.type' => $documentType])
            ->andWhere(['>', 'cef.amount', 0])
            //->andWhere(['between', 'cef.recognition_date', $dateFrom, $dateTo])
            ->andWhere(['between', 'invoice.document_date', $dateFrom, $dateTo]);

        $query = $queryInvoice
            ->union($queryBank, true)
            ->union($queryCashbox, true)
            ->union($queryEmoney, true);

        return (new Query)
            ->select([
                'contractor_id',
                'invoice_id',
                'invoice_sum' => 'SUM(q.invoice_sum)',
                'flow_sum' => 'SUM(q.flow_sum)',
                'invoice_date' => 'invoice_date',
                'flow_date' => 'MAX(flow_date)'
            ])
            ->from(['q' => $query])
            ->groupBy(['contractor_id', 'invoice_id']);
    }

    // HOME PAGE DEBT DYNAMIC CHART

    public static function getWeekActualDebts($documentType, $onDate = null, $customContractor = null, $customEmployee = null)
    {
        $companyId = \Yii::$app->user->identity->company->id;
        $onDate = $onDate ?? date('Y-m-d');
        $onDates = [];
        for ($d = 0; $d < 8; $d++) {
            $onDates[] = date_create_from_format('Y-m-d', $onDate)->modify("-{$d} day")->format('Y-m-d');
        }
        $subQuery = self::getQueryByInvoices($companyId, $documentType, $customContractor, $customEmployee)->createCommand()->rawSql;
        $query = "
            SELECT
            SUM(IF(payment_limit_date < '{$onDates[0]}', invoice_sum - flow_sum, 0)) AS debt_8,
            SUM(IF(payment_limit_date < '{$onDates[1]}', invoice_sum - flow_sum, 0)) AS debt_7,            
            SUM(IF(payment_limit_date < '{$onDates[2]}', invoice_sum - flow_sum, 0)) AS debt_6,
            SUM(IF(payment_limit_date < '{$onDates[3]}', invoice_sum - flow_sum, 0)) AS debt_5,
            SUM(IF(payment_limit_date < '{$onDates[4]}', invoice_sum - flow_sum, 0)) AS debt_4,
            SUM(IF(payment_limit_date < '{$onDates[5]}', invoice_sum - flow_sum, 0)) AS debt_3,
            SUM(IF(payment_limit_date < '{$onDates[6]}', invoice_sum - flow_sum, 0)) AS debt_2,
            SUM(IF(payment_limit_date < '{$onDates[7]}', invoice_sum - flow_sum, 0)) AS debt_1,
            '{$onDates[0]}' AS date_8,
            '{$onDates[1]}' AS date_7,
            '{$onDates[2]}' AS date_6,
            '{$onDates[3]}' AS date_5,
            '{$onDates[4]}' AS date_4,
            '{$onDates[5]}' AS date_3,
            '{$onDates[6]}' AS date_2,
            '{$onDates[7]}' AS date_1
            FROM
            ({$subQuery}) flows_invoices_mix
        ";

        $ret = Yii::$app->db2->createCommand($query)->queryOne();

        return [
            $ret['date_1'] => $ret['debt_1'],
            $ret['date_2'] => $ret['debt_2'],
            $ret['date_3'] => $ret['debt_3'],
            $ret['date_4'] => $ret['debt_4'],
            $ret['date_5'] => $ret['debt_5'],
            $ret['date_6'] => $ret['debt_6'],
            $ret['date_7'] => $ret['debt_7'],
            $ret['date_8'] => $ret['debt_8'],
        ];
    }
}