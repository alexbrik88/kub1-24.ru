<?php

namespace frontend\modules\analytics\models\breakEven;

use common\models\cash\CashFlowsBase;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\ExpenseItemFlowOfFunds;
use common\models\IncomeItemFlowOfFunds;
use common\models\product\Product;
use common\models\product\ProductTurnover;
use common\modules\acquiring\models\AcquiringOperation;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\models\ProfitAndLossCompanyItem;
use Yii;

trait BaseOlapTrait {

    /**
     * @param $expenseType
     * @param $exceptFlowOfFundsBlock
     * @return array
     */
    private function getCompanyExpenditureItems($expenseType, $exceptFlowOfFundsBlock = null)
    {
        $query = ProfitAndLossCompanyItem::find()
            ->joinWith('item')
            ->andWhere(['profit_and_loss_company_item.expense_type' => $expenseType])
            ->andWhere(['profit_and_loss_company_item.company_id' => $this->company->id])
            ->indexBy(['item_id'])
            ->orderBy(InvoiceExpenditureItem::tableName() . '.name');

        if ($exceptFlowOfFundsBlock) {
            if ($exceptArticles = ExpenseItemFlowOfFunds::find()
                ->where(['flow_of_funds_block' => $exceptFlowOfFundsBlock])
                ->andWhere(['not', ['expense_item_id' => InvoiceExpenditureItem::ITEM_DIVIDEND_PAYMENT]])
                ->select(['expense_item_id'])
                ->column()) {

                $query->andWhere(['not', ['item_id' => $exceptArticles]]);
            }
        }

        return $query->asArray()->all(\Yii::$app->db2);
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @param $type
     * @param array $includeItemId
     * @return mixed
     */
    private function getUndestributedFlows($dateFrom, $dateTo, $type, $includeItemId = [])
    {
        $table = self::TABLE_OLAP_FLOWS;
        $companyId = $this->company->id;

        $exceptIncomeItemId = ($type == CashFlowsBase::FLOW_TYPE_INCOME && !$includeItemId) ?
            self::getExceptUndestributedIncomeItems($companyId, AbstractFinance::FINANCIAL_OPERATIONS_BLOCK) : [];

        return Yii::$app->db2->createCommand("
                SELECT 
                  DATE_FORMAT(t.recognition_date, '%m') m,
                  DATE_FORMAT(t.recognition_date, '%Y') y,
                  SUM(t.amount) amount,
                  SUM(IF(t.is_accounting, t.amount, 0)) amountTax,
                  item_id
                FROM {$table} t 
                WHERE  t.company_id = {$companyId}
                  AND t.recognition_date BETWEEN '{$dateFrom}' AND '{$dateTo}'
                  AND t.type = {$type}
                  AND (t.has_invoice = FALSE OR t.has_doc = FALSE OR t.need_doc = FALSE) ".
            ($exceptIncomeItemId ? (" AND t.item_id NOT IN (".implode(',', $exceptIncomeItemId).")") : "").
            ($includeItemId ? (" AND t.item_id IN (".implode(',', $includeItemId).")") : "")."
                GROUP BY YEAR(t.recognition_date), MONTH(t.recognition_date)". ($includeItemId ? ", t.item_id" : ""))->queryAll();
    }

    private function getUndestributedAcquiringOperations($dateFrom, $dateTo, $type, $includeItemId = []) {
        $table = AcquiringOperation::tableName();
        $companyId = $this->company->id;
        $exceptIncomeItemId = ($type == CashFlowsBase::FLOW_TYPE_INCOME && !$includeItemId) ?
            self::getExceptUndestributedIncomeItems($companyId, AbstractFinance::FINANCIAL_OPERATIONS_BLOCK) : [];

        return Yii::$app->db->createCommand("
                SELECT 
                  DATE_FORMAT(t.recognition_date, '%m') m,
                  DATE_FORMAT(t.recognition_date, '%Y') y,
                  SUM(t.amount) amount,
                  0 amountTax,
                  IF(t.flow_type = 0, t.expenditure_item_id, t.income_item_id) item_id
                FROM {$table} t
                WHERE  t.company_id = {$companyId}
                  AND t.recognition_date BETWEEN '{$dateFrom}' AND '{$dateTo}'
                  AND t.flow_type = {$type} ".
            ($exceptIncomeItemId ? (" AND IF(t.flow_type = 0, t.expenditure_item_id, t.income_item_id) NOT IN (".implode(',', $exceptIncomeItemId).")") : "").
            ($includeItemId ? (" AND IF(t.flow_type = 0, t.expenditure_item_id, t.income_item_id) IN (".implode(',', $includeItemId).")") : "")."
                GROUP BY YEAR(t.recognition_date), MONTH(t.recognition_date)". ($includeItemId ? ", IF(t.flow_type = 0, t.expenditure_item_id, t.income_item_id)" : ""))->queryAll();
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @param $type
     * @param array $includeExpenditureItemId
     * @return mixed
     */
    private function getUndestributedDocs($dateFrom, $dateTo, $type, $includeExpenditureItemId = [])
    {
        $table = self::TABLE_OLAP_DOCS;
        $companyId = $this->company->id;

        return Yii::$app->db2->createCommand("
                SELECT 
                  DATE_FORMAT(t.date, '%m') m,
                  DATE_FORMAT(t.date, '%Y') y,                
                  SUM(t.amount) amount,
                  SUM(IF(t.is_accounting, t.amount, 0)) amountTax,
                  invoice_expenditure_item_id AS item_id
                FROM {$table} t
                WHERE t.company_id = {$companyId}
                  AND t.date BETWEEN '{$dateFrom}' AND '{$dateTo}'
                  AND t.type = {$type}".
            ($includeExpenditureItemId ? (" AND t.invoice_expenditure_item_id IN (".implode(',', $includeExpenditureItemId).")") : "")."
                GROUP BY YEAR(t.date), MONTH(t.date)". ($includeExpenditureItemId ? ", t.invoice_expenditure_item_id" : ""))->queryAll();
    }

    /**
     * @return array
     */
    public static function getExceptUndestributedIncomeItems($companyId = null, $flowOfFundsBlock = null)
    {
        $articles = [
            InvoiceIncomeItem::ITEM_STARTING_BALANCE,
            InvoiceIncomeItem::ITEM_RETURN,
            InvoiceIncomeItem::ITEM_BORROWING_REDEMPTION,
            InvoiceIncomeItem::ITEM_BUDGET_RETURN,
            InvoiceIncomeItem::ITEM_LOAN,
            InvoiceIncomeItem::ITEM_CREDIT,
            InvoiceIncomeItem::ITEM_ENSURE_PAYMENT,
            InvoiceIncomeItem::ITEM_FROM_FOUNDER,
            InvoiceIncomeItem::ITEM_OWN_FOUNDS,
            InvoiceIncomeItem::ITEM_OTHER,
            InvoiceIncomeItem::ITEM_RECEIVED_PERCENTS,
            InvoiceIncomeItem::ITEM_RECEIVED_PERCENTS_BY_DEPOSITS,
            InvoiceIncomeItem::ITEM_INPUT_MONEY
        ];

        $companyArticles = [];
        if ($companyId && $flowOfFundsBlock) {
            $companyArticles = IncomeItemFlowOfFunds::find()
                ->where(['flow_of_funds_block' => $flowOfFundsBlock])
                ->andWhere(['company_id' => $companyId])
                ->select(['income_item_id'])
                ->column();
        }

        return array_merge($articles, $companyArticles);
    }


    //

    private function __getPrimeCostsByDocs($dateFrom, $dateTo)
    {
        $productTable = Product::tableName();
        $contractorTable = Contractor::tableName();
        $turnoverTable = ProductTurnover::tableName();
        $companyId = $this->_company->id;

        //////////// BY DOC ////////////
        /// сумма всех Актов, ТН и УПД, дата которых в нужном месяце
        $query = "
            SELECT
              DATE_FORMAT(t.date, '%m') m,
              DATE_FORMAT(t.date, '%Y') y,
              SUM(IFNULL(p.price_for_buy_with_nds,0) * t.quantity) AS amount,
              SUM(IF(c.not_accounting, 0, 1) * IFNULL(p.price_for_buy_with_nds,0) * t.quantity) AS amountTax
            FROM {$turnoverTable} t 
            LEFT JOIN {$productTable} p ON p.id = t.product_id
            LEFT JOIN {$contractorTable} c ON c.id = t.contractor_id
            WHERE t.company_id = {$companyId}
              AND t.date BETWEEN '{$dateFrom}' AND '{$dateTo}'
              AND t.type = 2                  
              AND t.is_document_actual = TRUE
              AND t.is_invoice_actual = TRUE            
            GROUP BY YEAR(t.date), MONTH(t.date)

       ";

        return Yii::$app->db2->createCommand($query)->queryAll();
    }

    private function __getPrimeCostsByFlows($dateFrom, $dateTo)
    {
        $flowsTable = self::TABLE_OLAP_FLOWS;
        $companyId = $this->_company->id;

        //////////// BY NO DOC ////////////
        ///  1) есть привязка к счетам, у которых НЕТ Актов/ТН/УПД или у них стоит "Без Акта/ТН/УПД" и "Дата признания дохода" в нужном месяце
        $exceptIncomeFlowsArticles = implode(',', BaseOlapTrait::getExceptUndestributedIncomeItems($companyId, AbstractFinance::FINANCIAL_OPERATIONS_BLOCK));

        $subQuery = "
            SELECT 
              t.recognition_date,
              r.invoice_id,
              t.is_accounting
            FROM {$flowsTable} t
            LEFT JOIN `cash_bank_flow_to_invoice` r ON r.flow_id = t.id              
            WHERE t.company_id = {$companyId}
              AND t.wallet = 1
              AND t.type = 1
              AND t.item_id NOT IN ({$exceptIncomeFlowsArticles})
              AND t.has_invoice = TRUE AND (t.has_doc = FALSE OR t.need_doc = FALSE)
            
            UNION
            
            SELECT 
              t.recognition_date,
              r.invoice_id,
              t.is_accounting
            FROM {$flowsTable} t
            LEFT JOIN `cash_order_flow_to_invoice` r ON r.flow_id = t.id             
            WHERE t.company_id = {$companyId}
              AND t.wallet = 2
              AND t.type = 1
              AND t.item_id NOT IN ({$exceptIncomeFlowsArticles})
              AND t.has_invoice = TRUE AND (t.has_doc = FALSE OR t.need_doc = FALSE)
              
            UNION
            
            SELECT 
              t.recognition_date,
              r.invoice_id,
              t.is_accounting
            FROM {$flowsTable} t
            LEFT JOIN `cash_emoney_flow_to_invoice` r ON r.flow_id = t.id             
            WHERE t.company_id = {$companyId}
              AND t.wallet = 3            
              AND t.type = 1
              AND t.item_id NOT IN ({$exceptIncomeFlowsArticles})
              AND t.has_invoice = TRUE AND (t.has_doc = FALSE OR t.need_doc = FALSE)
        ";

        $query = "
            SELECT 
              DATE_FORMAT(t.recognition_date, '%m') m,
              DATE_FORMAT(t.recognition_date, '%Y') y,     
              SUM(IFNULL(p.price_for_buy_with_nds,0) * o.quantity) AS amount,
              SUM(IF(t.is_accounting, IFNULL(p.price_for_buy_with_nds,0) * o.quantity, 0)) AS amountTax
              FROM (
                SELECT 
                  tt.invoice_id, 
                  MAX(tt.recognition_date) recognition_date, 
                  MAX(tt.is_accounting) is_accounting 
                FROM ({$subQuery}) tt GROUP BY tt.invoice_id) t
              LEFT JOIN `order` o ON t.invoice_id = o.invoice_id
              LEFT JOIN `product` p ON p.id = o.product_id
              WHERE t.recognition_date BETWEEN '{$dateFrom}' AND '{$dateTo}'
              GROUP BY YEAR(t.recognition_date), MONTH(t.recognition_date)
        ";

        return Yii::$app->db2->createCommand($query)->queryAll();
    }

}