<?php

namespace frontend\modules\analytics\models;

use common\components\date\DateHelper;
use common\components\excel\Excel;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use common\models\Contractor;
use common\models\document\InvoiceIncomeItem;
use common\models\document\status\InvoiceStatus;
use common\models\EmployeeCompany;
use DateTime;
use frontend\models\Documents;
use frontend\modules\cash\models\CashContractorType;
use frontend\modules\analytics\models\income\TopChartsQuery;
use common\models\contractor\ContractorHelper;
use yii\base\InvalidParamException;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use common\models\IncomeItemFlowOfFunds;
use common\models\ExpenseItemFlowOfFunds;
use yii\db\ActiveQuery;
use common\models\document\InvoiceExpenditureItem;
use common\modules\acquiring\models\AcquiringOperation;
use common\modules\cards\models\CardOperation;

/**
 * Class IncomeSearch
 * @package frontend\modules\analytics\models
 */
class IncomeSearch extends AbstractFinance implements FinanceReport
{
    const SORT_BY_NAME = 'name';
    const SORT_BY_YEAR = 'year';
    const SORT_BY_QUARTER = 'quarter';
    const SORT_BY_MONTH = 'month';

    /**
     *
     */
    const BLOCK_INCOME = 'income';
    /**
     *
     */
    const TAB_BY_ACTIVITY = 12;
    /**
     *
     */
    const TAB_BY_PURSE = 13;

    public $incomeItemIdManyItem;
    public $expenditureItemIdManyItem;
    public $recognitionDateManyItem;

    /**
     *
     */
    public $sortKey;
    public $sortValue;
    public $sortReverse;

    public $tableWithPercents;

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['sort'], 'string'],
        ]);
    }

    public function init() {

        parent::init();

        $this->tableWithPercents =
            Yii::$app->user->identity->config->report_odds_expense_percent ||
            Yii::$app->user->identity->config->report_odds_expense_percent_2;
    }

    /**
     * @param int $type
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function search($type, $params = [])
    {
        if ($type == self::TAB_BY_PURSE) {
            return $this->searchByPurse($params);
        } elseif ($type == self::TAB_BY_ACTIVITY) {
            return $this->searchByActivity($params);
        }
        throw new InvalidParamException('Invalid type param.');
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function searchByPurse($params = [])
    {
        $this->_load($params);
        $result = [];
        $result['growingBalance']['totalFlowSum'] = 0;

        $dateFrom = $this->year . '-01-01';
        $dateTo = $this->year . '-12-31';

        $subArticles = AbstractFinance::getArticles2Parents(CashFlowsBase::FLOW_TYPE_INCOME, $this->multiCompanyIds);

        foreach (self::$month as $monthNumber => $monthText) {
            $result['growingBalance'][$monthNumber] = 0;
        }
        foreach (self::$purseTypes as $typeID => $typeName) {
            if (in_array($typeID, [
                self::EXPENSE_CASH_BANK,
                self::EXPENSE_CASH_EMONEY,
                self::EXPENSE_CASH_ORDER,
                self::EXPENSE_ACQUIRING,
                self::EXPENSE_CARD
            ])) {
                continue;
            }
            /* @var $flowClassName CashBankFlows|CashOrderFlows|CashEmoneyFlows */
            $flowClassName = $this->getFlowClassNameByType(self::$purseBlockByType[$typeID]);
            $params = $this->getParamsByFlowType(CashFlowsBase::FLOW_TYPE_INCOME);

            $query = $this->getDefaultCashFlowsQuery($flowClassName, $params, $dateFrom, $dateTo);
            $items = $query
                ->select([
                    $flowClassName::tableName() . ".{$params['itemAttrName']}",
                    'SUM(' . $flowClassName::tableName() . '.amount) as flowSum',
                    'date'
                ])
                ->andWhere(['not', ['contractor_id' => [CashContractorType::BANK_TEXT, CashContractorType::ORDER_TEXT, CashContractorType::EMONEY_TEXT]]])
                ->groupBy([$flowClassName::tableName() . ".{$params['itemAttrName']}", "date"])
                ->asArray()
                ->all(Yii::$app->db2);

            $itemsByCashContractor = $this->getDefaultItemsByCashFlowContractorQuery(
                $flowClassName,
                CashFlowsBase::FLOW_TYPE_INCOME,
                self::$cashContractorTypeByTableName[$flowClassName],
                $dateFrom,
                $dateTo
            )
                ->groupBy([$flowClassName::tableName() . '.contractor_id', 'date'])
                ->asArray()
                ->all(Yii::$app->db2);
            foreach ($itemsByCashContractor as $itemByCashContractor) {
                $items[] = $itemByCashContractor;
            }

            foreach ($items as $item) {

                $monthNumber = substr($item['date'], 5, 2);
                $quarterNumber = 'q' . ceil($monthNumber / 3);

                if (isset($item['contractorType'])) {
                    $itemID = $item['contractorType'];
                    $flowSum = (int)$item['flowSum'];
                    $itemName = 'Перевод из ' . self::$cashContractorTextByType[CashFlowsBase::FLOW_TYPE_INCOME][$itemID];
                } else {
                    $itemID = $item[$params['itemAttrName']];
                    $flowSum = (int)$item['flowSum'];
                    $itemName = InvoiceIncomeItem::findOne($item['income_item_id'])->name ?? '---';
                }

                $subItemID = null;
                $subItemName = null;
                if (isset($subArticles[$itemID])) {
                    $subItemID = $itemID;
                    $subItemName = $itemName;
                    $itemID = $subArticles[$itemID];
                    $itemName = null;
                }

                // Сумма по статьям помесячно
                if (isset($result[$typeID][$itemID][$monthNumber])) {
                    $result[$typeID][$itemID][$monthNumber]['flowSum'] += $flowSum;
                } else {
                    $result[$typeID][$itemID][$monthNumber] = ['flowSum' => $flowSum,];
                }

                // Сумма по статьям поквартально
                if (isset($result[$typeID][$itemID][$quarterNumber])) {
                    $result[$typeID][$itemID][$quarterNumber]['flowSum'] += $flowSum;
                } else {
                    $result[$typeID][$itemID][$quarterNumber] = ['flowSum' => $flowSum];
                }

                // Подстатьи
                if ($subItemID) {
                    if (isset($result[$typeID][$itemID]['subItems'][$subItemID][$monthNumber])) {
                        $result[$typeID][$itemID]['subItems'][$subItemID][$monthNumber]['flowSum'] += $flowSum;
                        $result[$typeID][$itemID]['subItems'][$subItemID][$quarterNumber]['flowSum'] += $flowSum;
                        $result[$typeID][$itemID]['subItems'][$subItemID]['totalFlowSum'] += $flowSum;
                    } else {
                        $result['itemName'][$typeID][$subItemID] = $subItemName;
                        $result[$typeID][$itemID]['subItems'][$subItemID][$monthNumber] = ['flowSum' => $flowSum];
                        $result[$typeID][$itemID]['subItems'][$subItemID][$quarterNumber] = ['flowSum' => $flowSum];
                        $result[$typeID][$itemID]['subItems'][$subItemID]['totalFlowSum'] = $flowSum;
                        $result[$typeID][$itemID]['subItems'][$subItemID]['name'] = $subItemName;
                    }
                }

                // Итого по статьям
                if (isset($result[$typeID][$itemID]['totalFlowSum'])) {
                    $result[$typeID][$itemID]['totalFlowSum'] += $flowSum;
                } else {
                    $result[$typeID][$itemID]['totalFlowSum'] = $flowSum;
                }
                // Существующие статьи
                $result['itemName'][$typeID][$itemID] = $itemName ?: InvoiceIncomeItem::findOne($itemID)->name ?? '---';
                // Сумма по типам помесячно
                if (isset($result['types'][$typeID][$monthNumber]['flowSum'])) {
                    $result['types'][$typeID][$monthNumber]['flowSum'] += $flowSum;
                } else {
                    $result['types'][$typeID][$monthNumber]['flowSum'] = $flowSum;
                }
                // Итого по типам
                if (isset($result['types'][$typeID]['totalFlowSum'])) {
                    $result['types'][$typeID]['totalFlowSum'] += $flowSum;
                } else {
                    $result['types'][$typeID]['totalFlowSum'] = $flowSum;
                }

                // Итого помесячно
                if (isset($result['totals'][$monthNumber]['flowSum'])) {
                    $result['totals'][$monthNumber]['flowSum'] += $flowSum;
                } else {
                    $result['totals'][$monthNumber]['flowSum'] = $flowSum;
                }

                $result['growingBalance'][$monthNumber] += $flowSum;
                $result['growingBalance']['totalFlowSum'] += $flowSum;
            }

            // Сортировка по статьям/подстатьям
            if (!empty($result['itemName'][$typeID])) {
                $this->sortRows($result, $typeID);
                foreach ($result['itemName'][$typeID] as $itemID => $itemName) {
                    if (!empty($result[$typeID][$itemID]['subItems'])) {
                        $this->sortSubRows($result[$typeID][$itemID]['subItems']);
                    }
                }
            }            
        }

        return $result;
    }

    /**
     * @param array $params
     * @return array
     */
    public function searchByActivity($params = [])
    {
        $this->_load($params);
        $result = [];
        $result['growingBalance']['totalFlowSum'] = 0;

        $dateFrom = $this->year . '-01-01';
        $dateTo = $this->year . '-12-31';

        foreach (self::$month as $monthNumber => $monthText) {
            $result['growingBalance'][$monthNumber] = 0;
        }
        $params = $this->getParamsByFlowType(CashFlowsBase::FLOW_TYPE_INCOME);

        $subArticles = AbstractFinance::getArticles2Parents(CashFlowsBase::FLOW_TYPE_INCOME, $this->multiCompanyIds);

        /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
        foreach (self::flowClassArray() as $className) {
            $items = $this->getDefaultCashFlowsQuery($className, $params, $dateFrom, $dateTo)
                ->select([
                    $className::tableName() . ".{$params['itemAttrName']}",
                    //'invoiceItem.name as itemName',
                    //'itemFlowOfFunds.flow_of_funds_block as flowOfFundsBlock',
                    'SUM(' . $className::tableName() . '.amount) as flowSum',
                    'date'
                ])
                ->andWhere(['not', ['contractor_id' => [
                    CashContractorType::BANK_TEXT,
                    CashContractorType::ORDER_TEXT,
                    CashContractorType::EMONEY_TEXT,
                    CashContractorType::BALANCE_TEXT,
                ]]])
                ->groupBy([$className::tableName() . ".{$params['itemAttrName']}", 'date'])
                ->asArray()
                ->all(Yii::$app->db2);

            foreach ($items as $item) {

                $monthNumber = substr($item['date'], 5, 2);
                $quarterNumber = 'q' . ceil($monthNumber / 3);

                $itemID = $item[$params['itemAttrName']];
                $itemName = InvoiceIncomeItem::findOne($item['income_item_id'])->name ?? '---';
                $flowSum = (int)$item['flowSum'];

                $subItemID = null;
                $subItemName = null;
                if (isset($subArticles[$itemID])) {
                    $subItemID = $itemID;
                    $subItemName = $itemName;
                    $itemID = $subArticles[$itemID];
                    $itemName = null;
                }

                // Сумма по статьям помесячно
                if (isset($result[self::BLOCK_INCOME][$itemID][$monthNumber])) {
                    $result[self::BLOCK_INCOME][$itemID][$monthNumber]['flowSum'] += $flowSum;
                } else {
                    $result[self::BLOCK_INCOME][$itemID][$monthNumber] = ['flowSum' => $flowSum];
                }

                // Сумма по статьям поквартально
                if (isset($result[self::BLOCK_INCOME][$itemID][$quarterNumber])) {
                    $result[self::BLOCK_INCOME][$itemID][$quarterNumber]['flowSum'] += $flowSum;
                } else {
                    $result[self::BLOCK_INCOME][$itemID][$quarterNumber] = ['flowSum' => $flowSum];
                }

                // Подстатьи
                if ($subItemID) {
                    if (isset($result[self::BLOCK_INCOME][$itemID]['subItems'][$subItemID][$monthNumber])) {
                        $result[self::BLOCK_INCOME][$itemID]['subItems'][$subItemID][$monthNumber]['flowSum'] += $flowSum;
                        $result[self::BLOCK_INCOME][$itemID]['subItems'][$subItemID][$quarterNumber]['flowSum'] += $flowSum;
                        $result[self::BLOCK_INCOME][$itemID]['subItems'][$subItemID]['totalFlowSum'] += $flowSum;
                    } else {
                        $result['itemName'][self::BLOCK_INCOME][$subItemID] = $subItemName;
                        $result[self::BLOCK_INCOME][$itemID]['subItems'][$subItemID][$monthNumber] = ['flowSum' => $flowSum];
                        $result[self::BLOCK_INCOME][$itemID]['subItems'][$subItemID][$quarterNumber] = ['flowSum' => $flowSum];
                        $result[self::BLOCK_INCOME][$itemID]['subItems'][$subItemID]['totalFlowSum'] = $flowSum;
                        $result[self::BLOCK_INCOME][$itemID]['subItems'][$subItemID]['name'] = $subItemName;
                    }
                }

                // Итого по статьям
                if (isset($result[self::BLOCK_INCOME][$itemID]['totalFlowSum'])) {
                    $result[self::BLOCK_INCOME][$itemID]['totalFlowSum'] += $flowSum;
                } else {
                    $result[self::BLOCK_INCOME][$itemID]['totalFlowSum'] = $flowSum;
                }

                // Существующие статьи
                $result['itemName'][self::BLOCK_INCOME][$itemID] = $itemName ?: InvoiceIncomeItem::findOne($itemID)->name ?? '---';

                // Сумма по типам помесячно
                if (isset($result['types'][self::BLOCK_INCOME][$monthNumber]['flowSum'])) {
                    $result['types'][self::BLOCK_INCOME][$monthNumber]['flowSum'] += $flowSum;
                } else {
                    $result['types'][self::BLOCK_INCOME][$monthNumber]['flowSum'] = $flowSum;
                }

                // Итого по типам
                if (isset($result['types'][self::BLOCK_INCOME]['totalFlowSum'])) {
                    $result['types'][self::BLOCK_INCOME]['totalFlowSum'] += $flowSum;
                } else {
                    $result['types'][self::BLOCK_INCOME]['totalFlowSum'] = $flowSum;
                }

                $result['growingBalance'][$monthNumber] += $flowSum;
                $result['growingBalance']['totalFlowSum'] += $flowSum;
            }
        }

        // Сортировка по статьям/подстатьям
        if (!empty($result['itemName'][self::BLOCK_INCOME])) {
            $this->sortRows($result, self::BLOCK_INCOME);
            foreach ($result['itemName'][self::BLOCK_INCOME] as $itemID => $itemName) {
                if (!empty($result[self::BLOCK_INCOME][$itemID]['subItems'])) {
                    $this->sortSubRows($result[self::BLOCK_INCOME][$itemID]['subItems']);
                }
            }
        }

        return $result;
    }

    /**
     * @param $result
     * @param $BLOCK
     */
    public function sortRows(&$result, $BLOCK)
    {
        if ($this->sortKey) {
            switch ($this->sortKey) {
                case self::SORT_BY_YEAR:
                    uksort($result['itemName'][$BLOCK], function ($item1, $item2) use ($result, $BLOCK) {
                        $flowSum1 = $result[$BLOCK][$item1]['totalFlowSum'] ?? 0;
                        $flowSum2 = $result[$BLOCK][$item2]['totalFlowSum'] ?? 0;
                        return ($this->sortReverse)
                            ? $flowSum1 < $flowSum2
                            : $flowSum1 > $flowSum2;
                    });
                    break;
                case self::SORT_BY_QUARTER:
                    uksort($result['itemName'][$BLOCK], function ($item1, $item2) use ($result, $BLOCK) {
                        $flowSum1 = $result[$BLOCK][$item1]['q'.$this->sortValue]['flowSum'] ?? 0;
                        $flowSum2 = $result[$BLOCK][$item2]['q'.$this->sortValue]['flowSum'] ?? 0;
                        return ($this->sortReverse)
                            ? $flowSum1 < $flowSum2
                            : $flowSum1 > $flowSum2;
                    });
                    break;
                case self::SORT_BY_MONTH:
                    uksort($result['itemName'][$BLOCK], function ($item1, $item2) use ($result, $BLOCK) {
                        $flowSum1 = $result[$BLOCK][$item1][$this->sortValue]['flowSum'] ?? 0;
                        $flowSum2 = $result[$BLOCK][$item2][$this->sortValue]['flowSum'] ?? 0;
                        return ($this->sortReverse)
                            ? $flowSum1 < $flowSum2
                            : $flowSum1 > $flowSum2;
                    });
                    break;
                case self::SORT_BY_NAME:
                    if ($this->sortReverse) {
                        arsort($result['itemName'][$BLOCK]);
                    } else {
                        asort($result['itemName'][$BLOCK]);
                    }
                    break;
            }
        } else {
            asort($result['itemName'][$BLOCK]);
        }
    }

    /**
     * @param $result
     */
    public function sortSubRows(&$result)
    {
        if ($this->sortKey) {
            switch ($this->sortKey) {
                case self::SORT_BY_YEAR:
                    uksort($result, function ($item1, $item2) use ($result) {
                        $flowSum1 = $result[$item1]['totalFlowSum'] ?? 0;
                        $flowSum2 = $result[$item2]['totalFlowSum'] ?? 0;
                        return ($this->sortReverse)
                            ? $flowSum1 < $flowSum2
                            : $flowSum1 > $flowSum2;
                    });
                    break;
                case self::SORT_BY_QUARTER:
                    uksort($result, function ($item1, $item2) use ($result) {
                        $flowSum1 = $result[$item1]['q'.$this->sortValue]['flowSum'] ?? 0;
                        $flowSum2 = $result[$item2]['q'.$this->sortValue]['flowSum'] ?? 0;
                        return ($this->sortReverse)
                            ? $flowSum1 < $flowSum2
                            : $flowSum1 > $flowSum2;
                    });
                    break;
                case self::SORT_BY_MONTH:
                    uksort($result, function ($item1, $item2) use ($result) {
                        $flowSum1 = $result[$item1][$this->sortValue]['flowSum'] ?? 0;
                        $flowSum2 = $result[$item2][$this->sortValue]['flowSum'] ?? 0;
                        return ($this->sortReverse)
                            ? $flowSum1 < $flowSum2
                            : $flowSum1 > $flowSum2;
                    });
                    break;
                case self::SORT_BY_NAME:
                    uksort($result, function ($item1, $item2) use ($result) {
                        $name1 = $result[$item1]['name'] ?? "AAA";
                        $name2 = $result[$item2]['name'] ?? "ZZZ";
                        return ($this->sortReverse)
                            ? $name1 < $name2
                            : $name1 > $name2;
                    });
                    break;
            }
        } else {
            uksort($result, function ($item1, $item2) use ($result) {
                $name1 = $result[$item1]['name'] ?? "AAA";
                $name2 = $result[$item2]['name'] ?? "ZZZ";
                return ($this->sortReverse)
                    ? $name1 < $name2
                    : $name1 > $name2;
            });
        }
    }    
    
    /**
     * @param $type
     * @throws \Exception
     */
    public function generateXls($type)
    {
        $data = $this->search($type, Yii::$app->request->get());
        $this->buildXls($type, $data, "Приходы за {$this->year} год");
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        $range = [];
        $cashOrderMinDate = CashOrderFlows::find()
            ->byCompany($this->multiCompanyIds)
            ->min('date', Yii::$app->db2);
        $cashBankMinDate = CashBankFlows::find()
            ->byCompany($this->multiCompanyIds)
            ->min('date', Yii::$app->db2);
        $cashEmoneyMinDate = CashEmoneyFlows::find()
            ->byCompany($this->multiCompanyIds)
            ->min('date', Yii::$app->db2);
        $minDates = [];

        if ($cashOrderMinDate) $minDates[] = $cashOrderMinDate;
        if ($cashBankMinDate) $minDates[] = $cashBankMinDate;
        if ($cashEmoneyMinDate) $minDates[] = $cashEmoneyMinDate;

        $minCashDate = !empty($minDates) ? min($minDates) : date(DateHelper::FORMAT_DATE);
        $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
        $currentYear = date('Y');
        foreach (range($registrationYear, $currentYear) as $value) {
            $range[$value] = $value;
        }
        arsort($range);

        return $range;
    }

    /**
     * @param $type
     * @param $data
     * @param $title
     */
    public function buildXls($type, $data, $title)
    {
        $blocks = isset($data['blocks']) ? $data['blocks'] : [];
        $types = isset($data['types']) ? $data['types'] : [];
        $expenditureItems = isset($data['itemName']) ? $data['itemName'] : [];
        $growingBalance = isset($data['growingBalance']) ? $data['growingBalance'] : 0;
        asort($expenditureItems);
        if ($type == self::TAB_BY_ACTIVITY) {
            $oddsTypes = [
                self::BLOCK_INCOME => 'Приходы',
            ];
            $oddsBlocks = $blockByType = [];
        } else {
            $oddsBlocks = self::$purseBlocks;
            $blockByType = self::$purseBlockByType;
            $oddsTypes = self::$purseTypes;
        }
        foreach ($oddsTypes as $typeID => $typeName) {
            $typeName = $type == self::TAB_BY_PURSE ? ('    ' . $typeName) : $typeName;
            if (in_array($typeID, [
                self::EXPENSE_CASH_BANK,
                self::EXPENSE_CASH_ORDER,
                self::EXPENSE_CASH_EMONEY,
            ])) {
                continue;
            }
            if ($type == self::TAB_BY_PURSE) {
                $itemData = isset($blocks[$blockByType[$typeID]]) ? $blocks[$blockByType[$typeID]] : [];
                $formattedData[] = $this->buildItem($oddsBlocks[$blockByType[$typeID]], $itemData);
            }
            $itemData = isset($types[$typeID]) ? $types[$typeID] : [];
            $formattedData[] = $this->buildItem($typeName, $itemData);
            if (isset($data[$typeID])) {
                foreach ($expenditureItems[$typeID] as $incomeItemId => $expenditureItemName) {
                    if (isset($data[$typeID][$incomeItemId])) {
                        $formattedData[] = $this->buildItem('        ' . $expenditureItemName, $data[$typeID][$incomeItemId]);
                    }
                }
            }
        }
        $formattedData[] = $this->buildItem('Остаток на конец месяца', $growingBalance);

        $columns[] = [
            'attribute' => 'itemName',
            'header' => 'Статьи',
        ];
        foreach (self::$month as $monthNumber => $monthText) {
            $columns[] = [
                'attribute' => "item{$monthNumber}",
                'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                'header' => $monthText,
            ];
        }
        $columns[] = [
            'attribute' => 'dataYear',
            'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
            'header' => $this->year,
        ];

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $formattedData,
            'title' => $title,
            'rangeHeader' => range('A', 'N'),
            'columns' => $columns,
            'format' => 'Xlsx',
            'fileName' => $title,
        ]);
    }

    /**
     * @param bool $plan
     * @return array
     */
    public function getFlowAmountByPeriod($plan = false)
    {
        $result = [];
        foreach (self::$month as $monthNumber => $monthName) {
            if ($this->year == date('Y') && $monthNumber > date('m') && !$plan) {
                $result[] = 0;
                continue;
            }
            $dateFrom = $this->year . '-' . $monthNumber . '-01';
            $dateTo = $this->year . '-' . $monthNumber . '-' . cal_days_in_month(CAL_GREGORIAN, $monthNumber, $this->year);
            if ($plan) {
                $result[] = PlanCashFlows::find()
                        ->andWhere(['company_id' => $this->multiCompanyIds])
                        ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
                        ->andWhere(['between', 'date', $dateFrom, $dateTo])
                        ->andWhere(['not', ['contractor_id' => [
                            CashContractorType::BANK_TEXT,
                            CashContractorType::ORDER_TEXT,
                            CashContractorType::EMONEY_TEXT,
                            CashContractorType::BALANCE_TEXT,
                        ]]])
                        ->sum('amount', Yii::$app->db2) / 100;
            } else {
                $amount = 0;
                foreach (self::flowClassArray() as $flowClassName) {
                    $amount += $flowClassName::find()
                        ->andWhere(['company_id' => $this->multiCompanyIds])
                        ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
                        ->andWhere(['between', 'date', $dateFrom, $dateTo])
                        ->andWhere(['not', ['contractor_id' => [
                            CashContractorType::BANK_TEXT,
                            CashContractorType::ORDER_TEXT,
                            CashContractorType::EMONEY_TEXT,
                            CashContractorType::BALANCE_TEXT,
                        ]]])
                        ->sum('amount', Yii::$app->db2);
                }
                $result[] = $amount / 100;
            }
        }

        return $result;
    }

    /**
     * @param bool $plan
     * @return array
     */
    public function getFlowAmountByItems($plan = false)
    {
        $dateFrom = $this->year . '-01-01';
        $dateTo = $this->year . '-12-31';
        if ($this->year == date('Y')) {
            $dateTo = $this->year . '-' . date('m') . '-' . cal_days_in_month(CAL_GREGORIAN, date('m'), $this->year);
        }
        $maxItems = $this->getMaxItemsFlow();
        $result = [];
        foreach ($maxItems as $maxItem) {
            if ($plan) {
                $query = PlanCashFlows::find()
                    ->andWhere(['company_id' => $this->multiCompanyIds])
                    ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
                    ->andWhere(['income_item_id' => $maxItem])
                    ->andWhere(['between', 'date', $dateFrom, $dateTo])
                    ->andWhere(['not', ['contractor_id' => [
                        CashContractorType::BANK_TEXT,
                        CashContractorType::ORDER_TEXT,
                        CashContractorType::EMONEY_TEXT,
                        CashContractorType::BALANCE_TEXT,
                    ]]]);
                $result[] = $query->sum('amount', Yii::$app->db2) / 100;
            } else {
                $amount = 0;
                foreach (self::flowClassArray() as $flowClassName) {
                    $query = $flowClassName::find()
                        ->andWhere(['company_id' => $this->multiCompanyIds])
                        ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
                        ->andWhere(['income_item_id' => $maxItem])
                        ->andWhere(['between', 'date', $dateFrom, $dateTo])
                        ->andWhere(['not', ['contractor_id' => [
                            CashContractorType::BANK_TEXT,
                            CashContractorType::ORDER_TEXT,
                            CashContractorType::EMONEY_TEXT,
                            CashContractorType::BALANCE_TEXT,
                        ]]]);
                    $amount += $query->sum('amount', Yii::$app->db2);
                }
                $result[] = $amount / 100;
            }
        }
        if ($plan) {
            $query = PlanCashFlows::find()
                ->andWhere(['company_id' => $this->multiCompanyIds])
                ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
                ->andWhere(['not', ['in', 'income_item_id', $maxItems]])
                ->andWhere(['between', 'date', $dateFrom, $dateTo])
                ->andWhere(['not', ['contractor_id' => [
                    CashContractorType::BANK_TEXT,
                    CashContractorType::ORDER_TEXT,
                    CashContractorType::EMONEY_TEXT,
                    CashContractorType::BALANCE_TEXT,
                ]]]);
            $result[] = $query->sum('amount', Yii::$app->db2) / 100;
        } else {
            $amount = 0;
            foreach (self::flowClassArray() as $flowClassName) {
                $query = $flowClassName::find()
                    ->andWhere(['company_id' => $this->multiCompanyIds])
                    ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
                    ->andWhere(['not', ['in', 'income_item_id', $maxItems]])
                    ->andWhere(['between', 'date', $dateFrom, $dateTo])
                    ->andWhere(['not', ['contractor_id' => [
                        CashContractorType::BANK_TEXT,
                        CashContractorType::ORDER_TEXT,
                        CashContractorType::EMONEY_TEXT,
                        CashContractorType::BALANCE_TEXT,
                    ]]]);
                $amount += $query->sum('amount', Yii::$app->db2);
            }
            $result[] = $amount / 100;
        }

        return $result;
    }

    /**
     * @param bool $name
     * @return array
     */
    public function getMaxItemsFlow($name = false)
    {
        $items = [];
        $cbf = CashBankFlows::find()
            ->select([
                'income_item_id',
                'amount',
            ])
            ->andWhere(['company_id' => $this->multiCompanyIds])
            ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
            ->andWhere(['between', 'date', $this->year . '-01-01', $this->year . '-12-31'])
            ->andWhere(['not', ['contractor_id' => [
                CashContractorType::BANK_TEXT,
                CashContractorType::ORDER_TEXT,
                CashContractorType::EMONEY_TEXT,
                CashContractorType::BALANCE_TEXT,
            ]]]);
        $cof = CashOrderFlows::find()
            ->select([
                'income_item_id',
                'amount',
            ])
            ->andWhere(['company_id' => $this->multiCompanyIds])
            ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
            ->andWhere(['between', 'date', $this->year . '-01-01', $this->year . '-12-31'])
            ->andWhere(['not', ['contractor_id' => [
                CashContractorType::BANK_TEXT,
                CashContractorType::ORDER_TEXT,
                CashContractorType::EMONEY_TEXT,
                CashContractorType::BALANCE_TEXT,
            ]]]);
        $query = CashEmoneyFlows::find()
            ->select([
                'income_item_id',
                'amount',
            ])
            ->andWhere(['company_id' => $this->multiCompanyIds])
            ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
            ->andWhere(['not', ['contractor_id' => [
                CashContractorType::BANK_TEXT,
                CashContractorType::ORDER_TEXT,
                CashContractorType::EMONEY_TEXT,
                CashContractorType::BALANCE_TEXT,
            ]]])
            ->union($cbf)
            ->union($cof)
            ->asArray()
            ->all(Yii::$app->db2);
        foreach ($query as $item) {
            if (!isset($items[$item['income_item_id']])) {
                $items[$item['income_item_id']] = 0;
            }
            $items[$item['income_item_id']] += $item['amount'];
        }
        arsort($items);
        $keys = array_keys(array_slice($items, 0, 5, true));
        if ($name) {
            $items = [];
            foreach ($keys as $key) {
                $items[] = InvoiceIncomeItem::findOne($key)->name;
            }
            $items[] = 'Остальное';

            return $items;
        }

        return $keys;
    }

    /**
     * @param $data
     */
    public function _load($data)
    {
        $sort = $data['sort'] ?? null;

        if (!$sort && $this->tableWithPercents) {
            if ($this->isCurrentYear && date('m') <= 3) {
                $sort = $_GET['sort'] = '-month_01';
            } else {
                $sort = $_GET['sort'] = '-quarter_1';
            }
        }

        if ($sort) {
            if (substr($sort, 0, 1) === '-') {
                $sort = substr($sort, 1);
                $this->sortReverse = true;
            }
            if (substr($sort, 0, 4) === 'name') {
                $this->sortKey = self::SORT_BY_NAME;
            }
            elseif (substr($sort, 0, 4) === 'year') {
                $this->sortKey = self::SORT_BY_YEAR;
            }
            elseif (substr($sort, 0, 5) === 'month') {
                $sortValue = substr($sort, 6);
                if (in_array($sortValue, ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'])) {
                    $this->sortKey = self::SORT_BY_MONTH;
                    $this->sortValue = $sortValue;
                }
            }
            elseif (substr($sort, 0, 7) === 'quarter') {
                $sortValue = substr($sort, 8);
                if (in_array($sortValue, ['01', '02', '03', '04'])) {
                    $this->sortKey = self::SORT_BY_QUARTER;
                    $this->sortValue = $sortValue;
                }
            }
        }
    }

    /**
     * @param $type
     * @return string
     * @throws \Exception
     */
    private function getFlowClassNameByType($type)
    {
        switch ($type) {
            case self::CASH_BANK_BLOCK:
                $className = CashBankFlows::className();
                break;
            case self::CASH_ORDER_BLOCK:
                $className = CashOrderFlows::className();
                break;
            case self::CASH_EMONEY_BLOCK:
                $className = CashEmoneyFlows::className();
                break;
            case self::CASH_ACQUIRING_BLOCK:
                $className = AcquiringOperation::className();
                break;
            case self::CASH_CARD_BLOCK:
                $className = CardOperation::className();
                break;
            default:
                throw new \Exception('Invalid type.' .$type);
                break;
        }

        return $className;
    }


    ///////////////
    /// Charts ////
    ///////////////

    /**
     * @param $dates
     * @param int $purse
     * @param string $period
     * @param int $incomeItemId
     * @param int $buyerId
     * @return array
     */
    public function getPlanFactSeriesData($dates, $purse = null, $period = "months", $incomeItemId = null, $buyerId = null, $productId = null, $employeeId = null)
    {
        $resultFact = [];
        $resultPlan = [];
        $resultBalance = [];
        $resultCurrMonthPlanBalance = 0;

        $INCOME = CashFlowsBase::FLOW_TYPE_INCOME;
        $OUTCOME = CashFlowsBase::FLOW_TYPE_EXPENSE;
        $EXCLUDE_CONTRACTORS = [
            CashContractorType::BANK_TEXT,
            CashContractorType::ORDER_TEXT,
            CashContractorType::EMONEY_TEXT,
            CashContractorType::BALANCE_TEXT
        ];

        switch ($purse) {
            case self::CASH_BANK_BLOCK:
                $flowArray = [CashBankFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case self::CASH_ORDER_BLOCK:
                $flowArray = [CashOrderFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case self::CASH_EMONEY_BLOCK:
                $flowArray = [CashEmoneyFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case self::CASH_ACQUIRING_BLOCK:
                $flowArray = [AcquiringOperation::class];
                $planFlowArray = [];
                break;
            case self::CASH_CARD_BLOCK:
                $flowArray = [CardOperation::class];
                $planFlowArray = [];
                break;
            case "cash":
                $flowArray = [CashBankFlows::class, CashEmoneyFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                $purse = [1,2];
                break;
            case "cashless":
                $flowArray = [CashOrderFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                $purse = [3];
                break;
            default:
                $flowArray = self::flowClassArray();
                $planFlowArray = [PlanCashFlows::class];
                break;
        }

        $dateStart = $dates[0]['from'];
        $dateEnd = $dates[count($dates)-1]['to'];

        // 0. Zeroes
        $dateKeyLength = ($period == "months") ? 7 : 10;
        foreach ($dates as $date) {

            $pos = substr($date['from'], 0, $dateKeyLength);

            $resultFact[$INCOME][$pos] = 0;
            $resultFact[$OUTCOME][$pos] = 0;
            $resultPlan[$INCOME][$pos] = 0;
            $resultPlan[$OUTCOME][$pos] = 0;
            //$resultBalance[$pos] = 0;
        }

        // 1. Fact
        foreach ($flowArray as $flowClassName) {
            /** @var CashBankFlows $flowClassName */
            $query = $flowClassName::find()
                ->alias('flow')
                ->andWhere(['flow.company_id' => $this->multiCompanyIds])
                ->andWhere(['between', 'flow.date', $dateStart, $dateEnd])
                ->asArray();

            if ($productId || $employeeId) {
                switch ($flowClassName) {
                    case CashBankFlows::class:
                        $query->innerJoin(['link' => 'cash_bank_flow_to_invoice'], 'link.flow_id = flow.id');
                        $hasLink = true;
                        break;
                    case CashOrderFlows::class:
                        $query->innerJoin(['link' => 'cash_order_flow_to_invoice'], 'link.flow_id = flow.id');
                        $hasLink = true;
                        break;
                    case CashEmoneyFlows::class:
                        $query->innerJoin(['link' => 'cash_emoney_flow_to_invoice'], 'link.flow_id = flow.id');
                        $hasLink = true;
                        break;
                    default:
                        $query->andWhere(new Expression('1<>1'));
                        $hasLink = false;
                        break;
                }
                if ($hasLink) {
                    $query->innerJoin('invoice', 'link.invoice_id = invoice.id')
                        ->innerJoin('order', 'invoice.id = order.invoice_id')
                        ->andFilterWhere(['order.product_id' => $productId])
                        ->andFilterWhere(['invoice.document_author_id' => $employeeId])
                        ->select(['amount' => 'SUM(`order`.`quantity` * `order`.`selling_price_with_vat`)', 'flow.flow_type', 'flow.date', 'flow.contractor_id', 'flow.income_item_id'])
                        ->groupBy('invoice.id');
                }
            } else {
                $query->select(['amount', 'flow_type', 'date', 'contractor_id', 'income_item_id']);
            }

            $flows = $query->all(Yii::$app->db2);

            foreach ($flows as $f) {

                if (empty($f['flow_type'])) {
                    continue;
                }

                $pos = substr($f['date'], 0, $dateKeyLength);
                if (in_array($f['contractor_id'], $EXCLUDE_CONTRACTORS)) {
                    continue;
                }

                if ($f['flow_type'] == $INCOME) {
                    if ($incomeItemId && $f['income_item_id'] != $incomeItemId)
                        continue;
                    if ($buyerId && $f['contractor_id'] != $buyerId)
                        continue;
                }

                $resultFact[$f['flow_type']][$pos] += (float)bcdiv($f['amount'], 100, 2);
            }
        }

        // 2. Plan
        foreach ($planFlowArray as $flowClassName) {

            if ($productId || $employeeId)
                continue;

            /** @var PlanCashFlows $flowClassName */
            $flows = $flowClassName::find()
                ->andWhere(['company_id' => $this->multiCompanyIds])
                ->andWhere(['not', ['contractor_id' => $EXCLUDE_CONTRACTORS]])
                ->andWhere(['between', 'date', $dateStart, $dateEnd])
                ->andFilterWhere(['payment_type' => $purse]) // !
                ->select(['amount', 'flow_type', 'date', 'contractor_id', 'income_item_id'])
                ->asArray()
                ->all(Yii::$app->db2);

            foreach ($flows as $f) {
                $pos = substr($f['date'], 0, $dateKeyLength);

                if ($f['flow_type'] == $INCOME) {
                    if ($incomeItemId && $f['income_item_id'] != $incomeItemId)
                        continue;
                    if ($buyerId && $f['contractor_id'] != $buyerId)
                        continue;
                }

                $resultPlan[$f['flow_type']][$pos] += (float)bcdiv($f['amount'], 100, 2);

                // curr month (future days) plan sum
                if ("months" == $period
                    && date('Ym', strtotime($f['date'])) == date('Ym')
                    && date('d', strtotime($f['date'])) > date('d')
                ) {
                    $resultCurrMonthPlanBalance += (float)bcdiv(($f['flow_type'] == $INCOME ? '' : '-') . $f['amount'], 100, 2);
                }
            }
        }

        return [
            'incomeFlowsFact' => array_values($resultFact[$INCOME]),
            'outcomeFlowsFact' => array_values($resultFact[$OUTCOME]),
            'incomeFlowsPlan' => array_values($resultPlan[$INCOME]),
            'outcomeFlowsPlan' => array_values($resultPlan[$OUTCOME]),
        ];
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @return array
     */
    public function getChartStructureByArticles($dateFrom, $dateTo, $purse = null, $contractor = null, $product = null, $employee = null)
    {
        $exceptContractors = [
            CashContractorType::BANK_TEXT,
            CashContractorType::ORDER_TEXT,
            CashContractorType::EMONEY_TEXT,
            CashContractorType::BALANCE_TEXT,
        ];

        switch ($purse) {
            case self::CASH_BANK_BLOCK:
                $flowArray = [CashBankFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case self::CASH_ORDER_BLOCK:
                $flowArray = [CashOrderFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case self::CASH_EMONEY_BLOCK:
                $flowArray = [CashEmoneyFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case self::CASH_ACQUIRING_BLOCK:
                $flowArray = [AcquiringOperation::class];
                $planFlowArray = [];
                break;
            case self::CASH_CARD_BLOCK:
                $flowArray = [CardOperation::class];
                $planFlowArray = [];
                break;
            default:
                $flowArray = self::flowClassArray();
                $planFlowArray = [PlanCashFlows::class];
                break;
        }

        // 1. Fact
        $amountsFact = [];
        foreach ($flowArray as $flowClassName) {

            $query = $flowClassName::find()
                ->alias('flow')
                ->select(['flow.income_item_id', 'flow.amount'])
                ->andWhere(['flow.company_id' => $this->multiCompanyIds])
                ->andWhere(['flow.flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
                ->andWhere(['between', 'flow.date', $dateFrom, $dateTo])
                ->andWhere(['not', ['flow.contractor_id' => $exceptContractors]])
                ->andWhere(['not', ['flow.income_item_id' => InvoiceIncomeItem::ITEM_OWN_FOUNDS]])
                ->andFilterWhere(['flow.contractor_id' => $contractor])
                ->asArray();

            if ($product || $employee) {
                switch ($flowClassName) {
                    case CashBankFlows::class:
                        $query->innerJoin(['link' => 'cash_bank_flow_to_invoice'], 'link.flow_id = flow.id');
                        $hasLink = true;
                        break;
                    case CashOrderFlows::class:
                        $query->innerJoin(['link' => 'cash_order_flow_to_invoice'], 'link.flow_id = flow.id');
                        $hasLink = true;
                        break;
                    case CashEmoneyFlows::class:
                        $query->innerJoin(['link' => 'cash_emoney_flow_to_invoice'], 'link.flow_id = flow.id');
                        $hasLink = true;
                        break;
                    default:
                        $query->andWhere(new Expression('1<>1'));
                        $hasLink = false;
                        break;
                }
                if ($hasLink) {
                    $query->innerJoin('invoice', 'link.invoice_id = invoice.id')
                        ->innerJoin('order', 'invoice.id = order.invoice_id')
                        ->andFilterWhere(['order.product_id' => $product])
                        ->andFilterWhere(['invoice.document_author_id' => $employee])
                        ->select(['amount' => 'SUM(`order`.`quantity` * `order`.`selling_price_with_vat`)', 'flow.income_item_id'])
                        ->groupBy('invoice.id');
                }
            } else {
                $query->select(['amount', 'income_item_id']);
            }

            $flows = $query->all(Yii::$app->db2);

            foreach ($flows as $f) {

                if (empty($f['income_item_id'])) {
                    continue;
                }

                if (!isset($amountsFact[$f['income_item_id']])) {
                    $amountsFact[$f['income_item_id']] = 0;
                }
                $amountsFact[$f['income_item_id']] += $f['amount'];
            }
        }

        // 2. Plan
        $amountsPlan = [];
        foreach ($planFlowArray as $flowClassName) {

            if ($product || $employee)
                continue;

            $flows = $flowClassName::find()
                ->select(['income_item_id', 'amount'])
                ->andWhere(['company_id' => $this->multiCompanyIds])
                ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
                ->andWhere(['between', 'date', $dateFrom, $dateTo])
                ->andWhere(['not', ['contractor_id' => $exceptContractors]])
                ->andWhere(['not', ['income_item_id' => InvoiceIncomeItem::ITEM_OWN_FOUNDS]])
                ->andFilterWhere(['payment_type' => $purse]) // !
                ->andFilterWhere(['contractor_id' => $contractor])
                ->asArray()
                ->all(Yii::$app->db2);

            foreach ($flows as $f) {
                if (!isset($amountsPlan[$f['income_item_id']])) {
                    $amountsPlan[$f['income_item_id']] = 0;
                }
                $amountsPlan[$f['income_item_id']] += $f['amount'];
            }
        }

        // 3. Names + merge amounts
        $items = [];
        foreach ($amountsFact as $itemId => $amount) {
            $items[$itemId] = [
                'id' => $itemId,
                'name' => ($itemModel = InvoiceIncomeItem::findOne($itemId)) ? $itemModel->name : '---',
                'amountFact' => round($amount / 100, 2),
                'amountPlan' => 0
            ];
        }
        foreach ($amountsPlan as $itemId => $amount) {
            if (!isset($items[$itemId])) {
                $items[$itemId] = [
                    'id' => $itemId,
                    'name' => ($itemModel = InvoiceIncomeItem::findOne($itemId)) ? $itemModel->name : '---',
                    'amountFact' => 0,
                    'amountPlan' => 0
                ];
            }
            $items[$itemId]['amountPlan'] = round($amount / 100, 2);
        }

        uasort($items, function ($a, $b) {
            if ($a['amountFact'] == $b['amountFact']) return 0;
            return ($a['amountFact'] > $b['amountFact']) ? -1 : 1;
        });

        return $items ?: [['name' => 'Нет данных', 'amountFact' => 0, 'amountPlan' => 0]];
    }

    /**
     * @param $month
     * @param null $purse
     * @return array
     */
    public function getChartStructureByBuyers($month = null, $purse = null, $article = null, $product = null, $employee = null, $period = [])
    {
        if (!$month)
            $month = ($this->year == date('Y') ? date('m') : 12);

        $date = date_create_from_format('d.m.Y H:i:s', "01.{$month}.{$this->year} 23:59:59");
        $dateFrom = $date->format('Y-m-d');
        $dateTo = $date->modify('last day of this month')->format('Y-m-d');

        if (isset($period['from']) && isset($period['to'])) {
            $dateFrom = $period['from'];
            $dateTo = $period['to'];
        }

        $exceptContractors = [
            CashContractorType::BANK_TEXT,
            CashContractorType::ORDER_TEXT,
            CashContractorType::EMONEY_TEXT,
            CashContractorType::BALANCE_TEXT,
            CashContractorType::COMPANY_TEXT,
        ];

        switch ($purse) {
            case 1:
                $flowArray = [CashBankFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case 2:
                $flowArray = [CashOrderFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case 3:
                $flowArray = [CashEmoneyFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            default:
                $flowArray = self::flowClassArray();
                $planFlowArray = [PlanCashFlows::class];
                break;
        }

        // 1. Fact
        $amountsFact = [];
        foreach ($flowArray as $flowClassName) {
            $query = $flowClassName::find()
                ->alias('flow')
                ->andWhere(['flow.company_id' => $this->multiCompanyIds])
                ->andWhere(['flow.flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
                ->andWhere(['between', 'flow.date', $dateFrom, $dateTo])
                ->andWhere(['not', ['flow.contractor_id' => $exceptContractors]])
                ->andWhere(['not', ['flow.contractor_id' => InvoiceIncomeItem::ITEM_OWN_FOUNDS]])
                ->andFilterWhere(['flow.income_item_id' => $article])
                ->asArray();

            if ($product || $employee) {
                switch ($flowClassName) {
                    case CashBankFlows::class:
                        $query->innerJoin(['link' => 'cash_bank_flow_to_invoice'], 'link.flow_id = flow.id');
                        $hasLink = true;
                        break;
                    case CashOrderFlows::class:
                        $query->innerJoin(['link' => 'cash_order_flow_to_invoice'], 'link.flow_id = flow.id');
                        $hasLink = true;
                        break;
                    case CashEmoneyFlows::class:
                        $query->innerJoin(['link' => 'cash_emoney_flow_to_invoice'], 'link.flow_id = flow.id');
                        $hasLink = true;
                        break;
                    default:
                        $query->andWhere(new Expression('1<>1'));
                        $hasLink = false;
                        break;
                }
                if ($hasLink) {
                    $query->innerJoin('invoice', 'link.invoice_id = invoice.id')
                        ->innerJoin('order', 'invoice.id = order.invoice_id')
                        ->andFilterWhere(['order.product_id' => $product])
                        ->andFilterWhere(['invoice.document_author_id' => $employee])
                        ->select(['amount' => 'SUM(`order`.`quantity` * `order`.`selling_price_with_vat`)', 'flow.contractor_id'])
                        ->groupBy('invoice.id');
                }
            } else {
                $query->select(['amount', 'contractor_id']);
            }


            $flows = $query->all(Yii::$app->db2);

            foreach ($flows as $f) {

                if (empty($f['contractor_id']))
                    continue;

                if (!isset($amountsFact[$f['contractor_id']])) {
                    $amountsFact[$f['contractor_id']] = 0;
                }
                $amountsFact[$f['contractor_id']] += $f['amount'];
            }
        }

        // 2. Plan
        $amountsPlan = [];
        foreach ($planFlowArray as $flowClassName) {

            if ($product || $employee)
                continue;

            $flows = $flowClassName::find()
                ->select(['contractor_id', 'amount'])
                ->andWhere(['company_id' => $this->multiCompanyIds])
                ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
                ->andWhere(['between', 'date', $dateFrom, $dateTo])
                ->andWhere(['not', ['contractor_id' => $exceptContractors]])
                ->andWhere(['not', ['contractor_id' => InvoiceIncomeItem::ITEM_OWN_FOUNDS]])
                ->andFilterWhere(['income_item_id' => $article])
                ->andFilterWhere(['payment_type' => $purse]) // !
                ->asArray()
                ->all(Yii::$app->db2);

            foreach ($flows as $f) {
                if (!isset($amountsPlan[$f['contractor_id']])) {
                    $amountsPlan[$f['contractor_id']] = 0;
                }
                $amountsPlan[$f['contractor_id']] += $f['amount'];
            }
        }

        // 3. Names + merge amounts
        $items = [];
        foreach ($amountsFact as $itemId => $amount) {
            $items[$itemId] = [
                'id' => $itemId,
                'name' => ContractorHelper::getShortNameById($itemId),
                'amountFact' => round($amount / 100, 2),
                'amountPlan' => 0
            ];
        }
        foreach ($amountsPlan as $itemId => $amount) {
            if (!isset($items[$itemId])) {
                $items[$itemId] = [
                    'id' => $itemId,
                    'name' => ContractorHelper::getShortNameById($itemId),
                    'amountFact' => 0,
                    'amountPlan' => 0
                ];
            }
            $items[$itemId]['amountPlan'] = round($amount / 100, 2);
        }

        uasort($items, function ($a, $b) {
            if ($a['amountFact'] == $b['amountFact']) return 0;
            return ($a['amountFact'] > $b['amountFact']) ? -1 : 1;
        });

        return $items ?: [['name' => 'Нет данных', 'amountFact' => 0, 'amountPlan' => 0]];
    }

    public function getTopProductSeriesData($month = null, $purse = null, $article = null, $contractor = null, $product = null, $employee = null)
    {
        $topProducts = [];
        $topEmployers = [];

        $yearMonth = $this->year.$month;

        if (strlen($yearMonth) === 6) {
            $dateStart = date_create_from_format('Ymd', $yearMonth . '01');
            $dateEnd = (clone $dateStart)->modify('last day of');
        } elseif (strlen($yearMonth) === 4) {
            $dateStart =  date_create_from_format('Ymd', $yearMonth . '0101');
            $dateEnd   =  date_create_from_format('Ymd', $yearMonth . '1231');
        } else {
            $dateStart = $dateEnd = new DateTime;
        }

        $payQuery = TopChartsQuery::payQuery($this->company, $dateStart->format('Y-m-d'), $dateEnd->format('Y-m-d'), $article);

        $invoiceQuery = TopChartsQuery::invoiceQuery($this->company)
            ->innerJoin(['pay' => $payQuery], "invoice.id = pay.invoice_id");

        $employeeIncomes = (clone $invoiceQuery)
            ->select(['amount' => 'SUM(flow_sum)', 'employee_id' => 'document_author_id'])
            ->leftJoin('order', 'order.invoice_id = invoice.id')
            ->andFilterWhere(['contractor_id' => $contractor])
            ->andFilterWhere(['document_author_id' => $employee])
            ->andFilterWhere(['order.product_id' => $product])
            ->groupBy('document_author_id')
            ->orderBy(['amount' => SORT_DESC])
            ->asArray()
            ->all(Yii::$app->db2);

        $productIncomes = (clone $invoiceQuery)
            ->select(['amount' => 'SUM(`order`.`quantity` * `order`.`selling_price_with_vat`)', 'product_id' => 'product.id', 'title' => 'product.title'])
            ->andWhere(['invoice.invoice_status_id' => InvoiceStatus::STATUS_PAYED])
            ->andFilterWhere(['contractor_id' => $contractor])
            ->leftJoin('order', 'order.invoice_id = invoice.id')
            ->leftJoin('product', 'order.product_id = product.id')
            ->andFilterWhere(['document_author_id' => $employee])
            ->andFilterWhere(['order.product_id' => $product])
            ->groupBy(['product_id'])
            ->orderBy(['amount' => SORT_DESC])
            ->asArray()
            ->all(Yii::$app->db2);

        foreach ($employeeIncomes as $v) {
            $topEmployers[$v['employee_id']] = [
                'name' => ($e = EmployeeCompany::findOne(['employee_id' => $v['employee_id'], 'company_id' => $this->multiCompanyIds])) ? $e->getShortFio() : '---',
                'amount' => $v['amount'] / 100
            ];
        }

        foreach ($productIncomes as $v) {
            $topProducts[$v['product_id']] = [
                'name' => $v['title'],
                'amount' => $v['amount'] / 100
            ];
        }

        return [
            'topProducts' => $topProducts,
            'topEmployers' => $topEmployers,
        ];
    }

    public function getFromCurrentMonthsPeriods($left, $right, $offsetYear = 0, $offsetMonth = "0") {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month")->modify("{$offsetMonth} month");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr->format('Y-m-01'),
                'to' => $curr->format('Y-m-t'),
            ];
            $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }

    public function getRawPeriodSellsGroupedByContractor($dateFrom, $dateTo)
    {
        $query = (new Query)
            ->select([
                't.contractor_id',
                'turnover' => 'SUM(t.total_amount)',
                'margin' => 'SUM(t.margin)',
                'number_of_sales' => 'COUNT(DISTINCT(document_id))',
            ])
            ->from(['t' => 'product_turnover'])
            ->andWhere(['t.company_id' => $this->multiCompanyIds])
            ->andWhere(['t.type' => Documents::IO_TYPE_OUT])
            ->andWhere(['between', 't.date',  $dateFrom, $dateTo])
            ->groupBy('contractor_id');

        return $query->all(Yii::$app->db2);
    }

    public function getRawPeriodSellsGroupedByEmployee($dateFrom, $dateTo)
    {
        $query = (new Query)
            ->select([
                'i.responsible_employee_id',
                'turnover' => 'SUM(t.total_amount)',
                'margin' => 'SUM(t.margin)',
                'number_of_sales' => 'COUNT(DISTINCT(document_id))',
            ])
            ->from(['t' => 'product_turnover'])
            ->leftJoin('invoice i', 't.invoice_id = i.id')
            ->andWhere(['t.company_id' => $this->multiCompanyIds])
            ->andWhere(['t.type' => Documents::IO_TYPE_OUT])
            ->andWhere(['>', 't.invoice_id', 0])
            ->andWhere(['between', 't.date',  $dateFrom, $dateTo])
            ->groupBy('i.responsible_employee_id');

        return $query->all(Yii::$app->db2);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// OLD METHODS 2019 (from AbstractFinance) todo: need update
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param $flowType
     * @return mixed
     */
    public function getParamsByFlowType($flowType)
    {
        $result['flowType'] = $flowType;
        if ($flowType == CashFlowsBase::FLOW_TYPE_INCOME) {
            $result['itemFlowOfFundsTableName'] = IncomeItemFlowOfFunds::tableName();
            $result['invoiceItemTableName'] = InvoiceIncomeItem::tableName();
            $result['itemAttrName'] = $result['flowOfFundItemName'] = 'income_item_id';
            $result['withoutItem'] = InvoiceIncomeItem::ITEM_OWN_FOUNDS;
            $result['flowTypeText'] = 'ПРИХОДЫ';
            $result['prevSumAttrName'] = 'incomePrevSum';
        } elseif ($flowType == CashFlowsBase::FLOW_TYPE_EXPENSE) {
            $result['itemFlowOfFundsTableName'] = ExpenseItemFlowOfFunds::tableName();
            $result['invoiceItemTableName'] = InvoiceExpenditureItem::tableName();
            $result['itemAttrName'] = 'expenditure_item_id';
            $result['flowOfFundItemName'] = 'expense_item_id';
            $result['withoutItem'] = InvoiceExpenditureItem::ITEM_OWN_FOUNDS;
            $result['flowTypeText'] = 'РАСХОДЫ';
            $result['prevSumAttrName'] = 'expenditurePrevSum';
        } else {
            throw new InvalidParamException('Invalid flow type param.');
        }

        return $result;
    }

    /**
     * @param $flowType
     * @return array
     */
    public function getItemsByFlowType($flowType)
    {
        if ($flowType == CashFlowsBase::FLOW_TYPE_INCOME) {
            $items = InvoiceIncomeItem::find()
                ->andWhere(['or',
                    ['company_id' => null],
                    ['company_id' => $this->multiCompanyIds],
                ])
                ->andWhere(['not', ['id' => InvoiceIncomeItem::ITEM_OWN_FOUNDS]])
                ->column();
        } elseif ($flowType == CashFlowsBase::FLOW_TYPE_EXPENSE) {
            $items = InvoiceExpenditureItem::find()
                ->andWhere(['or',
                    ['company_id' => null],
                    ['company_id' => $this->multiCompanyIds],
                ])
                ->andWhere(['not', ['id' => InvoiceExpenditureItem::ITEM_OWN_FOUNDS]])
                ->column();
        } else {
            throw new InvalidParamException('Invalid flow type param.');
        }

        return $items;
    }

    /**
     * @param $className CashBankFlows|CashOrderFlows|CashEmoneyFlows|PlanCashFlows
     * @param $flowTypeParams
     * @param $dateFrom
     * @param $dateTo
     * @return ActiveQuery
     */
    public function getDefaultCashFlowsQuery($className, $flowTypeParams, $dateFrom, $dateTo)
    {
        return $className::find()
            ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
            ->andWhere(['not', [$className::tableName() . ".{$flowTypeParams['itemAttrName']}" => $flowTypeParams['withoutItem']]])
            ->andWhere(['between', $className::tableName() . '.date', $dateFrom, $dateTo])
            ->andWhere([$className::tableName() . '.company_id' => $this->multiCompanyIds]);
    }

    /**
     * @param $className CashBankFlows|CashOrderFlows|CashEmoneyFlows|PlanCashFlows
     * @param $flowType
     * @param $contractors
     * @param $dateFrom
     * @param $dateTo
     * @return ActiveQuery
     */
    public function getDefaultItemsByCashFlowContractorQuery($className, $flowType, $contractors, $dateFrom, $dateTo)
    {
        return $className::find()
            ->select([
                $className::tableName() . '.contractor_id as contractorType',
                'SUM(' . $className::tableName() . '.amount) as flowSum',
                'date',
            ])->andWhere(['and',
                ['flow_type' => $flowType],
                ['in', 'contractor_id', $contractors],
                [$className::tableName() . '.company_id' => $this->multiCompanyIds],
                ['between', $className::tableName() . '.date', $dateFrom, $dateTo],
            ]);
    }

    /**
     * @param $itemName
     * @param $data
     * @return mixed
     */
    public function buildItem($itemName, $data)
    {
        $itemData['itemName'] = $itemName;
        foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText) {
            if (isset($data[$monthNumber])) {
                if (is_array($data[$monthNumber])) {
                    $itemData["item{$monthNumber}"] = $data[$monthNumber]['flowSum'] / 100;
                } else {
                    $itemData["item{$monthNumber}"] = $data[$monthNumber] / 100;
                }
            } else {
                $itemData["item{$monthNumber}"] = 0;
            }
        }
        if (isset($data['totalFlowSum'])) {
            if (is_array($data['totalFlowSum'])) {
                $itemData['dataYear'] = isset($data['totalFlowSum']['flowSum']) ? $data['totalFlowSum']['flowSum'] / 100 : 0;
            } else {
                $itemData['dataYear'] = $data['totalFlowSum'] / 100;
            }
        } else {
            $itemData['dataYear'] = 0;
        }

        return $itemData;
    }
}