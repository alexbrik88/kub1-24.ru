<?php

namespace frontend\modules\analytics\models;

use common\models\Company;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use DateTime;
use frontend\models\Documents;
use Yii;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class AnalysisSearch
 */
class AnalysisSearch extends Contractor
{
    const GROUP_A = 1;
    const GROUP_B = 2;
    const GROUP_C = 3;

    const TMP_TABLE_NAME = 'analysis_tmp';

    /**
     * @var attributes
     */
    public $paid_count;
    public $paid_sum;
    public $part;
    public $group;
    public $average;
    public $first;
    public $last;

    protected $_company = null;
    protected $_dateFrom = null;
    protected $_dateTo = null;
    protected $_totalSum = null;
    protected $_tmpRowCount = null;
    protected $_overallResult = [];

    public static $groups = [
        self::GROUP_A => 'A',
        self::GROUP_B => 'B',
        self::GROUP_C => 'C',
    ];

    public static $groupsLabel = [
        self::GROUP_A => 'Ключевые (A)',
        self::GROUP_B => 'Второстепенные (B)',
        self::GROUP_C => 'Прочие (C)',
    ];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'group'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Контрагент',
            'paid_sum' => 'Сумма платежей',
            'paid_count' => 'Кол-во платежей',
            'part' => 'Доля %',
            'group' => 'Группа',
            'average' => 'Средний чек',
            'first' => 'Первый платеж',
            'last' => 'Последний платеж',
        ];
    }

    /**
     * @param  array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $this->createTmpAbcTable();

        $this->load($params);

        $firstQuery = $this->getBaseQuery()
            ->select('invoice.invoice_status_updated_at')
            ->andWhere([
                'invoice.contractor_id' => new Expression("{{contractor}}.[[id]]")
            ])
            ->orderBy(['invoice.invoice_status_updated_at' => SORT_ASC])
            ->limit(1);
        $lastQuery = $this->getBaseQuery()
            ->select('invoice.invoice_status_updated_at')
            ->andWhere([
                'invoice.contractor_id' => new Expression("{{contractor}}.[[id]]")
            ])
            ->orderBy(['invoice.invoice_status_updated_at' => SORT_DESC])
            ->limit(1);

        $query = self::find()
            ->addSelect([
                "contractor.*",
                "abc.*",
                'average' => new Expression("IF({{abc}}.[[paid_count]] = 0, 0, ({{abc}}.[[paid_sum]]/{{abc}}.[[paid_count]]))"),
                'first' => $firstQuery,
                'last' => $lastQuery,
            ])
            ->innerJoin(['abc' => self::TMP_TABLE_NAME], "{{contractor}}.[[id]] = {{abc}}.[[contractor_id]]");

        $query->andFilterWhere([
            'contractor.id' => $this->id,
            'abc.group' => $this->group,
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'paid_count',
                'paid_sum',
                'part',
                'average',
                'first',
                'last',
            ],
            'defaultOrder' => [
                'paid_sum' => SORT_DESC,
            ],
        ]);

        return $dataProvider;
    }

    /**
     * @param $contractorID
     * @return false|null|string
     * @throws InvalidConfigException
     */
    public function getContractorGroup($contractorID)
    {
        $this->createTmpAbcTable();

        return self::find()
            ->addSelect([
                "abc.group",
            ])
            ->innerJoin(['abc' => self::TMP_TABLE_NAME], "{{contractor}}.[[id]] = {{abc}}.[[contractor_id]]")
            ->andWhere(['contractor.id' => $contractorID,])
            ->scalar();
    }

    /**
     * create temporary ABC analysis table
     */
    public function createTmpAbcTable()
    {
        if (!$this->company) {
            throw new InvalidConfigException('The "company", "dateFrom", "dateTo" properties mast by set.');
        }

        $tableName = self::TMP_TABLE_NAME;
        $groupA = self::GROUP_A;
        $groupB = self::GROUP_B;
        $groupC = self::GROUP_C;
        $query = (new Query())->select([
            'contractor_id',
            'paid_count',
            'paid_sum',
            'part',
            'group' => new Expression("
                IF((@stock:=(@stock + {{search}}.[[part]])) > 20, {$groupA}, IF(@stock > 5, {$groupB}, {$groupC}))
            "),
        ])->from(['search' => $this->getSearchQuery()]);

        Yii::$app->db->createCommand('SET @stock=0')->execute();
        Yii::$app->db->createCommand("
            CREATE TEMPORARY TABLE IF NOT EXISTS {{{$tableName}}} AS ({$query->createCommand()->rawSql})
        ")->execute();

        //overall result data
        $this->_overallResult = (new Query())
            ->select([
                'group',
                'group_sum' => new Expression("SUM([[paid_sum]])"),
                'group_sum_part' => new Expression($this->totalSum ? "(SUM([[paid_sum]]) / {$this->totalSum} * 100)" : "0"),
                'contractor_count' => new Expression("COUNT([[group]])"),
                'contractor_count_part' => new Expression($this->totalSum ? "(COUNT([[group]]) / {$this->tmpRowCount} * 100)" : "0"),
            ])
            ->from(self::TMP_TABLE_NAME)
            ->groupBy('group')
            ->orderBy('group')
            ->all();
    }

    /**
     * @return integer
     */
    public function getTotalSum()
    {
        if ($this->_totalSum === null) {
            $this->_totalSum = $this->getBaseQuery()
                ->innerJoin(['paid' => $this->getPaymentQuery()], "{{invoice}}.[[id]] = {{paid}}.[[invoice_id]]")
                ->sum('paid.amount');
        }

        return (int)$this->_totalSum;
    }

    /**
     * @return integer
     */
    public function getTmpRowCount()
    {
        if ($this->_tmpRowCount === null) {
            $this->_tmpRowCount = (new Query())->from(self::TMP_TABLE_NAME)->count();
        }

        return (int)$this->_tmpRowCount;
    }

    /**
     * @return \yii\db\Query
     */
    public function getSearchQuery()
    {
        $query = (new Query())
            ->select([
                'contractor_id',
                'paid_count',
                'paid_sum',
                'name' => new Expression("
                    IF(
                        {{type}}.[[id]] IS NULL,
                        {{contractor}}.[[name]],
                        IF(
                            {{type}}.[[id]] = 1,
                            CONCAT({{type}}.[[name_short]], ' ', {{contractor}}.[[name]]),
                            CONCAT({{type}}.[[name_short]], ' \"', {{contractor}}.[[name]], '\"')
                        )
                    )
                "),
                'part' => new Expression($this->totalSum ? "(@part:=([[paid_sum]] / {$this->totalSum} * 100))" : "0"),
            ])
            ->from(['by_pay' => $this->getQueryByPayment()])
            ->leftJoin('contractor', '{{by_pay}}.[[contractor_id]] = {{contractor}}.[[id]]')
            ->leftJoin(['type' => 'company_type'], '{{contractor}}.[[company_type_id]] = {{type}}.[[id]]')
            ->orderBy(['paid_sum' => SORT_ASC]);

        return $query;
    }

    /**
     * union all payments
     *
     * @return \yii\db\Query
     */
    public function getPaymentQuery()
    {
        $bankQuery = (new Query())->select(['invoice_id', 'amount'])->from('cash_bank_flow_to_invoice');
        $orderQuery = (new Query())->select(['invoice_id', 'amount'])->from('cash_order_flow_to_invoice');
        $emoneyQuery = (new Query())->select(['invoice_id', 'amount'])->from('cash_emoney_flow_to_invoice');
        $query = $bankQuery->union($orderQuery, true)->union($emoneyQuery, true);

        return $query;
    }

    /**
     * invoice paid amount query
     *
     * @return \yii\db\Query
     */
    public function getInvoicePaidQuery()
    {
        $query = (new Query())
            ->select([
                'invoice_id',
                'paid_amount' => new Expression("SUM([[amount]])"),
            ])
            ->from(['t' => $this->getPaymentQuery()])
            ->groupBy('invoice_id');

        return $query;
    }

    /**
     * @return \yii\db\Query
     */
    public function getQueryByPayment()
    {
        $query = $this->getBaseQuery()
            ->select([
                'invoice.contractor_id',
                'paid_count' => new Expression("COUNT({{invoice}}.[[contractor_id]])"),
                'paid_sum' => new Expression("SUM({{paid}}.[[paid_amount]])"),
            ])
            ->innerJoin(['paid' => $this->getInvoicePaidQuery()], "{{invoice}}.[[id]] = {{paid}}.[[invoice_id]]")
            ->groupBy('invoice.contractor_id');

        return $query;
    }

    /**
     * @return \yii\db\Query
     */
    public function getBaseQuery()
    {
        $query = (new Query())
            ->from('invoice')
            ->andWhere([
                'invoice.company_id' => $this->company->id,
                'invoice.type' => Documents::IO_TYPE_OUT,
                'invoice.is_deleted' => false,
                'invoice.invoice_status_id' => [InvoiceStatus::STATUS_PAYED_PARTIAL, InvoiceStatus::STATUS_PAYED],
            ]);

        if ($this->dateFrom && $this->_dateTo) {
            $query->andWhere([
                'between',
                'invoice.document_date',
                $this->dateFrom->format('Y-m-d'),
                $this->dateTo->format('Y-m-d'),
            ]);
        }

        return $query;
    }

    /**
     * @return array
     */
    public function getOverallResult()
    {
        return (array)$this->_overallResult;
    }

    /**
     * @return array
     */
    public function getContractorFilterItems()
    {
        $idArray = (new Query())->select('c.contractor_id')->distinct()->from(['c' => $this->getBaseQuery()])->column();
        $contractorArray = Contractor::getSorted()->andWhere(['contractor.id' => $idArray])->all();

        return ['' => 'Все'] + ArrayHelper::map($contractorArray, 'id', 'nameWithType');
    }

    /**
     * @param array $dateRange
     */
    public function setDateRange($dateRange)
    {
        $this->_dateFrom = empty($dateRange['from']) ? null : new DateTime($dateRange['from']);
        $this->_dateTo = empty($dateRange['to']) ? null : new DateTime($dateRange['to']);
       // if (!$this->_dateFrom || !$this->_dateTo) {
       //     throw new InvalidConfigException('The "dateRange" option is invalid.');
       // }
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->_company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @return DateTime
     */
    public function getDateFrom()
    {
        return $this->_dateFrom;
    }

    /**
     * @return DateTime
     */
    public function getDateTo()
    {
        return $this->_dateTo;
    }

    /**
     * @return string
     */
    public function getGroupValue($i = null)
    {
        return isset(self::$groups[$i ?: $this->group]) ? self::$groups[$i ?: $this->group] : '';
    }

    /**
     * @return string
     */
    public function getGroupLabel($i = null)
    {
        return isset(self::$groupsLabel[$i ?: $this->group]) ? self::$groupsLabel[$i ?: $this->group] : '';
    }

    /**
     * @return string
     */
    public function getGroupColor($i = null)
    {
        $colorArray = [
            self::GROUP_A => '#7dde81',
            self::GROUP_B => '#dfba49',
            self::GROUP_C => '#f3555d',
        ];

        return isset($colorArray[$i ?: $this->group]) ? $colorArray[$i ?: $this->group] : '';
    }
}
