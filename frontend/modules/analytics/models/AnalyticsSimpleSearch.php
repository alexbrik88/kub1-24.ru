<?php namespace frontend\modules\analytics\models;

use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use Yii;
use common\models\cash\CashFlowsBase;
use common\models\Company;
use yii\base\Exception;

class AnalyticsSimpleSearch {

    const GROUP_BY_INDUSTRY = 'industry_id';
    const GROUP_BY_SALE_POINT = 'sale_point_id';
    const GROUP_BY_PROJECT = 'project_id';

    const GROUP_BY_INCOME_ITEM = 'income_item_id';
    const GROUP_BY_EXPENDITURE_ITEM = 'expenditure_item_id';

    private static $availableGroupBy = [
        self::GROUP_BY_INCOME_ITEM,
        self::GROUP_BY_EXPENDITURE_ITEM,
    ];

    private static $_incomeColors = [
        'rgba(46,159,191,1)', // default chart blue
        'rgba(103,148,220,1)',
        'rgba(103,113,220,1)',
        'rgba(128,103,220,1)',
        'rgba(163,103,220,1)',
        'rgba(199,103,220,1)',
        'rgba(220,103,206,1)',
    ];

    private static $_expenditureColors = [
        'rgba(243,183,46,1)', // default chart yellow
        'rgba(228,228,103,1)',
        'rgba(220,213,103,1)',
        'rgba(220,203,128,1)',
        'rgba(220,203,163,1)',
        'rgba(220,203,199,1)',
        'rgba(206,203,220,1)',
    ];

    private static $_defaultColors = [
        '#fd6562',
        '#816eee',
        '#61df30',
        '#2a59d0',
        '#c10a4b',
        '#0d7db2',
        '#a39e15',
        '#941871',
        '#148540',
        '#92b00e',
    ];

    private $_company;

    public function __construct(Company $company)
    {
        $this->_company = $company;
    }

    public function getPieColor(int $topPosition, string $groupBy)
    {
        switch ($groupBy) {
            case self::GROUP_BY_INCOME_ITEM:
                return self::$_incomeColors[$topPosition] ?? self::$_incomeColors[count(self::$_incomeColors) - 1];
            case self::GROUP_BY_EXPENDITURE_ITEM:
                return self::$_expenditureColors[$topPosition] ?? self::$_expenditureColors[count(self::$_expenditureColors) - 1];
            default:
                return self::$_defaultColors[$topPosition] ?? self::$_defaultColors[count(self::$_defaultColors) - 1];
        }
    }

    public function getPieByFlows(string $dateFrom, string $dateTo, string $customPurse, array $pageFilter, string $groupBy): array
    {
        if (!self::testDate($dateFrom) || !self::testDate($dateTo) || !self::testPurse($customPurse) || !self::testGroupBy($groupBy)) {
            throw new Exception('Error format');
        }

        $tableOlapFlows = AbstractFinance::TABLE_OLAP_FLOWS;
        $companyId = $this->_company->id;
        $exceptOwnFlowsContractors = implode(',', OLAP::$ownFlowContractorType);

        switch ($groupBy) {
            case self::GROUP_BY_INCOME_ITEM:
                $groupBy = 'item_id';
                $type = CashFlowsBase::FLOW_TYPE_INCOME;
                break;
            case self::GROUP_BY_EXPENDITURE_ITEM:
                $groupBy = 'item_id';
                $type = CashFlowsBase::FLOW_TYPE_EXPENSE;
                break;
            default:
                $groupBy = 'NULL';
                $type = '-1';
                break;
        }

        if (empty($customPurse))
            $customPurse = 't.wallet';

        $sqlPageFilter = '';
        if (strlen($pageFilter['industry_id'] ?? "")) {
            $sqlPageFilter .= ($pageFilter['industry_id'])
                ? ' AND t.industry_id = ' . $pageFilter['industry_id']
                : ' AND t.industry_id IS NULL';
        }
        if (strlen($pageFilter['sale_point_id'] ?? "")) {
            $sqlPageFilter .= ($pageFilter['sale_point_id'])
                ? ' AND t.sale_point_id = ' . $pageFilter['sale_point_id']
                : ' AND t.sale_point_id IS NULL';
        }
        if (strlen($pageFilter['project_id'] ?? "")) {
            $sqlPageFilter .= ($pageFilter['project_id'])
                ? ' AND t.project_id = ' . $pageFilter['project_id']
                : ' AND t.project_id IS NULL';
        }

        $query = "
          SELECT
            t.{$groupBy},
            SUM(IF(t.has_tin_parent, t.tin_child_amount, t.amount)) amount
          FROM {$tableOlapFlows} t
          WHERE
            t.company_id IN ({$companyId})
            AND t.date BETWEEN '{$dateFrom}' AND '{$dateTo}'
            AND t.type = {$type}
            AND t.wallet = {$customPurse}
            AND t.has_tin_children = 0
            AND t.contractor_id NOT IN ({$exceptOwnFlowsContractors})
            {$sqlPageFilter}
          GROUP BY {$groupBy}
          ORDER BY amount DESC  
        ";

        return Yii::$app->db2->createCommand($query)->queryAll();
    }

    public function getAllLinesByFlows(string $dateFrom, string $dateTo, string $flowType, string $groupBy): array
    {
        if (!self::testDate($dateFrom) || !self::testDate($dateTo)) {
            throw new Exception('Error format');
        }

        $tableOlapFlows = AbstractFinance::TABLE_OLAP_FLOWS;
        $companyId = $this->_company->id;
        $exceptOwnFlowsContractors = implode(',', OLAP::$ownFlowContractorType);

        switch ($groupBy) {
            case self::GROUP_BY_INDUSTRY:
                $type = $flowType;
                break;
            case self::GROUP_BY_SALE_POINT:
                $type = $flowType;
                break;
            case self::GROUP_BY_PROJECT:
                $type = $flowType;
                break;
            default:
                $groupBy = 'NULL';
                $type = '-1';
                break;
        }

        $query = "
          SELECT
            IFNULL(t.{$groupBy}, 0) AS {$groupBy},
            DATE_FORMAT(t.`recognition_date`, '%Y%m') ym,
            SUM(IF(t.has_tin_parent, t.tin_child_amount, t.amount)) amount
          FROM {$tableOlapFlows} t
          WHERE
            t.company_id IN ({$companyId})
            AND t.date BETWEEN '{$dateFrom}' AND '{$dateTo}'
            AND t.type = {$type}
            AND t.has_tin_children = 0
            AND t.contractor_id NOT IN ({$exceptOwnFlowsContractors})
          GROUP BY {$groupBy}, ym
          ORDER BY ym ASC
        ";

        $result = Yii::$app->db2->createCommand($query)->queryAll();

        $data = [];
        foreach ($result as $r) {
            if (!isset($data[$r[$groupBy]]))
                $data[$r[$groupBy]] = [];

            $data[$r[$groupBy]][$r['ym']] = round($r['amount'] / 100, 2);
        }

        return $data;
    }

    public function getAllPiesByFlowsByPeriod(string $dateFrom, string $dateTo, string $flowType, string $groupBy): array
    {
        if (!self::testDate($dateFrom) || !self::testDate($dateTo)) {
            throw new Exception('Error format');
        }

        $tableOlapFlows = AbstractFinance::TABLE_OLAP_FLOWS;
        $companyId = $this->_company->id;
        $exceptOwnFlowsContractors = implode(',', OLAP::$ownFlowContractorType);

        switch ($groupBy) {
            case self::GROUP_BY_INDUSTRY:
                $type = $flowType;
                break;
            case self::GROUP_BY_SALE_POINT:
                $type = $flowType;
                break;
            case self::GROUP_BY_PROJECT:
                $type = $flowType;
                break;
            default:
                $groupBy = 'NULL';
                $type = '-1';
                break;
        }

        $query = "
          SELECT
            IFNULL(t.{$groupBy}, 0) AS {$groupBy},
            SUM(IF(t.has_tin_parent, t.tin_child_amount, t.amount)) amount
          FROM {$tableOlapFlows} t
          WHERE
            t.company_id IN ({$companyId})
            AND t.date BETWEEN '{$dateFrom}' AND '{$dateTo}'
            AND t.type = {$type}
            AND t.has_tin_children = 0
            AND t.contractor_id NOT IN ({$exceptOwnFlowsContractors})
          GROUP BY {$groupBy}
          ORDER BY amount DESC
        ";

        $result = Yii::$app->db2->createCommand($query)->queryAll();

        $data = [];
        foreach ($result as $r) {
            if (!isset($data[$r[$groupBy]]))
                $data[$r[$groupBy]] = [];

            $data[$r[$groupBy]] = round($r['amount'] / 100, 2);
        }

        return $data;
    }

    public function getAllPiesByFlows(string $year, string $flowType, string $groupBy): array
    {
        $tableOlapFlows = AbstractFinance::TABLE_OLAP_FLOWS;
        $companyId = $this->_company->id;
        $exceptOwnFlowsContractors = implode(',', OLAP::$ownFlowContractorType);

        switch ($groupBy) {
            case self::GROUP_BY_INDUSTRY:
                $type = $flowType;
                break;
            case self::GROUP_BY_SALE_POINT:
                $type = $flowType;
                break;
            case self::GROUP_BY_PROJECT:
                $type = $flowType;
                break;
            default:
                $groupBy = 'NULL';
                $type = '-1';
                break;
        }

        $query = "
          SELECT
            IFNULL(t.{$groupBy}, 0) AS {$groupBy},
            SUM(IF(t.has_tin_parent, t.tin_child_amount, t.amount)) amount
          FROM {$tableOlapFlows} t
          WHERE
            t.company_id IN ({$companyId})
            AND t.date BETWEEN '{$year}-01-01' AND '{$year}-12-31'
            AND t.type = {$type}
            AND t.has_tin_children = 0
            AND t.contractor_id NOT IN ({$exceptOwnFlowsContractors})
          GROUP BY {$groupBy}
          ORDER BY amount DESC
        ";

        $result = Yii::$app->db2->createCommand($query)->queryAll();

        $data = [];
        foreach ($result as $r) {
            if (!isset($data[$r[$groupBy]]))
                $data[$r[$groupBy]] = [];

            $data[$r[$groupBy]] = round($r['amount'] / 100, 2);
        }

        return $data;
    }

    private static function testDate(string $date):bool
    {
        return (bool)preg_match("/^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$/", $date);
    }

    private static function testPurse(string $purse):bool
    {
        return strlen($purse) === 0 || in_array($purse, [
            AbstractFinance::CASH_BANK_BLOCK,
            AbstractFinance::CASH_ORDER_BLOCK,
            AbstractFinance::CASH_EMONEY_BLOCK,
            AbstractFinance::CASH_ACQUIRING_BLOCK,
            AbstractFinance::CASH_CARD_BLOCK,
        ]);
    }

    private static function testGroupBy(string $groupBy):bool
    {
        return in_array($groupBy, self::$availableGroupBy);
    }
}