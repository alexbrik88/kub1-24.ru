<?php
/**
 * Created by PhpStorm.
 * User: �����
 * Date: 26.10.2017
 * Time: 8:29
 */

namespace frontend\modules\analytics\models;


use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use common\models\Company;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\modules\acquiring\models\AcquiringOperation;
use common\modules\cards\models\CardOperation;
use frontend\modules\cash\models\CashContractorType;
use frontend\modules\documents\components\InvoiceStatistic;
use yii\base\InvalidParamException;
use Yii;
use common\components\debts\DebtsHelper;
use common\components\TextHelper;
use common\models\document\Invoice;
use frontend\models\Documents;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;
use common\models\IncomeItemFlowOfFunds;
use common\models\ExpenseItemFlowOfFunds;
use yii\db\ActiveQuery;
use common\components\excel\Excel;
use common\models\Contractor;
use frontend\modules\cash\models\cashSearch\FilterItemsTrait;
use frontend\modules\cash\models\cashSearch\FilterNamesTrait;

/**
 * Class FlowOfFundsReportSearch
 * @package frontend\modules\analytics\models
 */
class FlowOfFundsReportSearch extends AbstractFinance implements FinanceReport
{
    use FilterItemsTrait;
    use FilterNamesTrait;

    const TAB_ODDS = 1;
    const TAB_ODDS_BY_PURSE = 2;
    const TAB_ODDS_BY_PURSE_DETAILED = 3;

    public $contractor_id;
    public $reason_id;
    public $period;
    public $group;
    public $flow_type;
    public $payment_type;
    public $wallet_id;
    public $company_id;
    public $project_id;
    public $industry_id;
    public $sale_point_id;
    public $currency_name;

    public $incomeItemIdManyItem;
    public $expenditureItemIdManyItem;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['company_id', 'contractor_id', 'reason_id', 'payment_type'], 'integer'],
            [['project_id', 'industry_id', 'sale_point_id'], 'integer'],
            [['wallet_id', 'currency_name'], 'string']
        ]);
    }

    /**
     * @param $type
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function search($type, $params = [])
    {
        if ($type == self::TAB_ODDS_BY_PURSE) {
            return $this->searchByPurse($params);
        } elseif ($type == self::TAB_ODDS) {
            return $this->searchByActivity($params);
        }
        throw new InvalidParamException('Invalid type param.');
    }

    /**
     * @param $type
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function searchByDays($type, $params = [])
    {
        if ($type == self::TAB_ODDS_BY_PURSE) {
            $params['searchInMonth'] = true;
            return $this->searchByPurseDays($params, $params['dateFrom'], $params['dateTo']);
        } elseif ($type == self::TAB_ODDS) {
            $params['searchInMonth'] = true;
            return $this->searchByActivityDays($params, $params['dateFrom'], $params['dateTo']);
        }
        throw new InvalidParamException('Invalid type param.');
    }

    /**
     * @param $type
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function searchInMonth($type, $params = [])
    {
        if ($type == self::TAB_ODDS_BY_PURSE) {
            $params['searchInMonth'] = true;
            return $this->searchByPurse($params);
        } elseif ($type == self::TAB_ODDS) {
            $params['searchInMonth'] = true;
            return $this->searchByActivity($params);
        }
        throw new InvalidParamException('Invalid type param.');
    }

    /**
     * @param array $params
     * @return array
     */
    public function searchByActivity($params = [])
    {
        $this->_load($params);
        $result = [];
        $prevSum = [
            'incomePrevSum' => 0,
            'expenditurePrevSum' => 0,
        ];
        $result['balance']['totalFlowSum'] = 0;
        $statisticsPeriod = self::$month;

        foreach ($statisticsPeriod as $monthNumber => $monthText) {

            $dateFrom = $this->year . '-' . $monthNumber . '-01';
            $dateTo = $this->year . '-' . $monthNumber . '-' . cal_days_in_month(CAL_GREGORIAN, $monthNumber, $this->year);

            if (!isset($result['growingBalance'][$monthNumber])) {
                $result['growingBalance'][$monthNumber] = 0;
            }
            if (!isset($result['growingBalanceContractorBalance'][$monthNumber])) {
                $result['growingBalanceContractorBalance'][$monthNumber] = 0;
            }
            /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
            foreach (self::flowClassArray() as $className) {
                $result['growingBalanceContractorBalance'][$monthNumber] += $this->getCashFlowsContractorBalanceAmount($className, $dateFrom, $dateTo);
            }
        }
        foreach (self::$typeItem as $typeID => $itemIDs) {
            $type = self::$flowTypeByActivityType[$typeID];
            $params = $this->getParamsByFlowType($type);
            foreach ($statisticsPeriod as $monthNumber => $monthText) {
                $dateFrom = $this->year . '-' . $monthNumber . '-01';
                $dateTo = $this->year . '-' . $monthNumber . '-' . cal_days_in_month(CAL_GREGORIAN, $monthNumber, $this->year);
                /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
                foreach (self::flowClassArray() as $className) {
                    if (!isset($result['balance'][$monthNumber])) {
                        $result['balance'][$monthNumber] = 0;
                    }
                    $query = $this->getDefaultCashFlowsQuery($className, $params, $dateFrom, $dateTo);
                    $items = $this->filterByActivityQuery($query, $params, $typeID)
                        ->select([
                            $className::tableName() . ".{$params['itemAttrName']}",
                            'invoiceItem.name as itemName',
                            'itemFlowOfFunds.flow_of_funds_block as flowOfFundsBlock',
                            'SUM(' . $className::tableName() . '.amount) as flowSum'
                        ])
                        ->groupBy([$className::tableName() . ".{$params['itemAttrName']}"])
                        ->asArray()
                        ->all();
                    foreach ($items as $item) {
                        $itemID = $item[$params['itemAttrName']];
                        $flowSum = (int)$item['flowSum'];
                        $itemName = $item['itemName'];
                        // Сумма по статьям помесячно
                        if (isset($result[$typeID][$itemID][$monthNumber])) {
                            $result[$typeID][$itemID][$monthNumber]['flowSum'] += $flowSum;
                        } else {
                            $result[$typeID][$itemID][$monthNumber] = [
                                'flowSum' => $flowSum,
                            ];
                        }
                        // Итого по статьям
                        if (isset($result[$typeID][$itemID]['totalFlowSum'])) {
                            $result[$typeID][$itemID]['totalFlowSum'] += $flowSum;
                        } else {
                            $result[$typeID][$itemID]['totalFlowSum'] = $flowSum;
                        }
                        // Существующие статьи
                        $result['itemName'][$typeID][$itemID] = $itemName;
                        // Сумма по типам помесячно
                        if (isset($result['types'][$typeID][$monthNumber]['flowSum'])) {
                            $result['types'][$typeID][$monthNumber]['flowSum'] += $flowSum;
                        } else {
                            $result['types'][$typeID][$monthNumber]['flowSum'] = $flowSum;
                        }
                        // Итого по типам
                        if (isset($result['types'][$typeID]['totalFlowSum'])) {
                            $result['types'][$typeID]['totalFlowSum'] += $flowSum;
                        } else {
                            $result['types'][$typeID]['totalFlowSum'] = $flowSum;
                        }
                    }
                }
                // Сальдо
                $sum = isset($result['types'][$typeID][$monthNumber]['flowSum']) ?
                    (int)$result['types'][$typeID][$monthNumber]['flowSum'] : 0;
                if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                    $result['balance'][$monthNumber] += $sum;
                } else {
                    $result['balance'][$monthNumber] -= $sum;
                }
                // Сальдо нарастающим
                $previousMonth = intval($monthNumber) - 1;
                $previousMonth = $previousMonth > 9 ? $previousMonth : ('0' . $previousMonth);

                $result['growingBalance'][$monthNumber] = $result['balance'][$monthNumber] +
                    $result['growingBalanceContractorBalance'][$monthNumber] +
                    (isset($result['growingBalance'][$previousMonth]) ? $result['growingBalance'][$previousMonth] : 0);
            }
            $sum = isset($result['types'][$typeID]['totalFlowSum']) ?
                $result['types'][$typeID]['totalFlowSum'] : 0;
            if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                $result['balance']['totalFlowSum'] += $sum;
            } else {
                $result['balance']['totalFlowSum'] -= $sum;
            }
            if (isset($result['itemName'][$typeID])) {
                asort($result['itemName'][$typeID]);
            }
        }
        if ($this->year !== min($this->getYearFilter())) {
            $dateFrom = min($this->getYearFilter()) . '-01-01';
            $dateTo = ($this->year - 1) . '-12-31';

            foreach (self::$typeItem as $typeID => $itemIDs) {
                $type = self::$flowTypeByActivityType[$typeID];
                $params = $this->getParamsByFlowType($type);
                foreach (self::flowClassArray() as $className) {
                    $query = $this->getDefaultCashFlowsQuery($className, $params, $dateFrom, $dateTo);
                    $items = $this->filterByActivityQuery($query, $params, $typeID)
                        ->select([
                            $className::tableName() . ".{$params['itemAttrName']}",
                            'invoiceItem.name as itemName',
                            'itemFlowOfFunds.flow_of_funds_block as flowOfFundsBlock',
                            'SUM(' . $className::tableName() . '.amount) as flowSum'
                        ])
                        ->groupBy([$className::tableName() . ".{$params['itemAttrName']}"])
                        ->asArray()
                        ->all();
                    foreach ($items as $item) {
                        $prevSum[$params['prevSumAttrName']] += (int)$item['flowSum'];
                        if (!isset($prevSum[self::$blockByType[$typeID]][$params['prevSumAttrName']])) {
                            $prevSum[self::$blockByType[$typeID]][$params['prevSumAttrName']] = $item['flowSum'];
                        } else {
                            $prevSum[self::$blockByType[$typeID]][$params['prevSumAttrName']] += $item['flowSum'];
                        }
                    }
                }
            }
            $balanceSum = 0;
            /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
            foreach (self::flowClassArray() as $className) {
                $balanceSum += $this->getCashFlowsContractorBalanceAmount($className, $dateFrom, $dateTo);
            }
            $previousBalance = $prevSum['incomePrevSum'] - $prevSum['expenditurePrevSum'] + $balanceSum;
            foreach ($result['growingBalance'] as $key => $value) {
                $result['growingBalance'][$key] += $previousBalance;
            }
        }

        // Calculate Block Sum
        $incomeBlocks = [self::RECEIPT_FINANCING_TYPE_FIRST, self::INCOME_OPERATING_ACTIVITIES, self::INCOME_INVESTMENT_ACTIVITIES];
        $result['blocks'] = [];
        if (isset($result['types'])) {
            foreach ($result['types'] as $key => $data) {
                foreach ($data as $month => $value) {
                    $formattedValue = $month == 'totalFlowSum' ? $value : $value['flowSum'];
                    if (!isset($result['blocks'][self::$blockByType[$key]][$month]['flowSum'])) {
                        $result['blocks'][self::$blockByType[$key]][$month]['flowSum'] = 0;
                    }
                    if (in_array($key, $incomeBlocks)) {
                        $result['blocks'][self::$blockByType[$key]][$month]['flowSum'] += $formattedValue;

                        // add balance
                        //if ($key == self::INCOME_OPERATING_ACTIVITIES && $month != 'totalFlowSum') {
                        //    $result['blocks'][self::$blockByType[$key]][$month]['flowSum'] += $result['growingBalanceContractorBalance'][$month];
                        //}

                    } else {
                        $result['blocks'][self::$blockByType[$key]][$month]['flowSum'] -= $formattedValue;
                    }
                }
            }
        }

        // Calculate Growing Balance By Block
        foreach (self::$blocks as $blockType => $blockName) {
            $key = 0;
            foreach ($statisticsPeriod as $monthNumber => $monthText) {
                $formattedValue = isset($result['blocks'][$blockType][$monthNumber]['flowSum']) ?
                    $result['blocks'][$blockType][$monthNumber]['flowSum'] : 0;
                $prevFlowSum = 0;
                if (!isset($growingBalanceByBlock[$blockType][$monthNumber]['flowSum'])) {
                    $result['growingBalanceByBlock'][$blockType][$monthNumber]['flowSum'] = $formattedValue;
                }

                if ($key === 0) {
                    if (isset($prevSum[$blockType]['incomePrevSum'])) {
                        $prevFlowSum += $prevSum[$blockType]['incomePrevSum'];
                    }
                    if (isset($prevSum[$blockType]['expenditurePrevSum'])) {
                        $prevFlowSum -= $prevSum[$blockType]['expenditurePrevSum'];
                    }
                } else {
                    $prevMonth = $monthNumber - 1;
                    $prevMonth = $prevMonth < 10 ? (0 . $prevMonth) : $prevMonth;
                    $prevFlowSum = $result['growingBalanceByBlock'][$blockType][$prevMonth]['flowSum'];
                }
                $result['growingBalanceByBlock'][$blockType][$monthNumber]['flowSum'] += $prevFlowSum;
                $key++;
            }
        }

        return $result;
    }

    /**
     * @param array $params
     * @param null $dateFrom
     * @param null $dateTo
     * @return array
     */
    public function searchByActivityDays($params = [], $dateFrom = null, $dateTo = null)
    {
        $this->_load($params);
        $result = [];
        $resultDAY = [];

        $prevSum = [
            'incomePrevSum' => 0,
            'expenditurePrevSum' => 0,
        ];

        if (!$dateFrom) {
            $dateFrom = date('Y-01-01');
        }

        if (!$dateTo) {
            $dateTo = date('Y-12-31');
        }

        $result['balance'] = ["01" => 0, "02" => 0, "03" => 0, "04" => 0, "05" => 0, "06" => 0, "07" => 0, "08" => 0, "09" => 0, "10" => 0, "11" => 0, "12" => 0];
        $result['growingBalanceContractorBalance'] = ["01" => 0, "02" => 0, "03" => 0, "04" => 0, "05" => 0, "06" => 0, "07" => 0, "08" => 0, "09" => 0, "10" => 0, "11" => 0, "12" => 0];
        $result['balance']['totalFlowSum'] = 0;
        for ($m=1; $m <= 12; $m++) {
            foreach ($this->getDaysInMonth($this->year, $m) as $k=>$v) {
                $resultDAY['balance'][$k] = 0;
                $resultDAY['growingBalanceContractorBalance'][$k] = 0;
            }
        }
        $resultDAY['balance']['totalFlowSum'] = 0;

        $statisticsPeriod = self::$month;

        ///foreach ($statisticsPeriod as $monthNumber => $monthText) {

            /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
            foreach (self::flowClassArray() as $className) {

                //$result['growingBalanceContractorBalance'][$monthNumber] += $this->getCashFlowsContractorBalanceAmount($className, $dateFrom, $dateTo);

                $growingBalanceContractorArr = $className::find()
                    ->andWhere(['company_id' => $this->multiCompanyIds])
                    ->andWhere(['contractor_id' => CashContractorType::BALANCE_TEXT])
                    ->andWhere(['between', 'date', $dateFrom, $dateTo])
                    ->select(['amount', 'date'])->asArray()->all();
                foreach ($growingBalanceContractorArr as $item) {

                    $monthNumber = substr($item['date'], 5, 2);
                    $dayNumber = $item['date'];

                    $result['growingBalanceContractorBalance'][$monthNumber] += $item['amount'];
                    $resultDAY['growingBalanceContractorBalance'][$dayNumber] += $item['amount'];
                }
            }

        ///}

        foreach (self::$typeItem as $typeID => $itemIDs) {
            $type = self::$flowTypeByActivityType[$typeID];
            $params = $this->getParamsByFlowType($type);

            ///foreach ($statisticsPeriod as $monthNumber => $monthText) {

                /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
                foreach (self::flowClassArray() as $className) {
                    $query = $this->getDefaultCashFlowsQuery($className, $params, $dateFrom, $dateTo);
                    $items = $this->filterByActivityQuery($query, $params, $typeID)
                        ->select([
                            $className::tableName() . ".{$params['itemAttrName']}",
                            'invoiceItem.name as itemName',
                            'itemFlowOfFunds.flow_of_funds_block as flowOfFundsBlock',
                            $className::tableName() . '.amount as flowSum',
                            'date'
                        ])
                        //->groupBy([$className::tableName() . ".{$params['itemAttrName']}"])
                        ->asArray()
                        ->all();

                    // prepare
                    $fs0 = ['flowSum' => 0];
                    foreach ($items as $item) {
                        $itemID = $item[$params['itemAttrName']];
                        if (!isset($result[$typeID][$itemID])) $result[$typeID][$itemID] = ["01" => $fs0, "02" => $fs0, "03" => $fs0, "04" => $fs0, "05" => $fs0, "06" => $fs0, "07" => $fs0, "08" => $fs0, "09" => $fs0, "10" => $fs0, "11" => $fs0, "12" => $fs0];
                        if (!isset($result['types'][$typeID])) $result['types'][$typeID] = ["01" => $fs0, "02" => $fs0, "03" => $fs0, "04" => $fs0, "05" => $fs0, "06" => $fs0, "07" => $fs0, "08" => $fs0, "09" => $fs0, "10" => $fs0, "11" => $fs0, "12" => $fs0];
                        for ($m=1; $m <= 12; $m++) {
                            foreach ($this->getDaysInMonth($this->year, $m) as $k=>$v) {
                                if (!isset($resultDAY[$typeID][$itemID][$k])) $resultDAY[$typeID][$itemID][$k] = $fs0;
                                if (!isset($resultDAY['types'][$typeID][$k])) $resultDAY['types'][$typeID][$k] = $fs0;
                            }
                        }
                        if (!isset($result[$typeID][$itemID]['totalFlowSum'])) $result[$typeID][$itemID]['totalFlowSum'] = 0;
                        if (!isset($result['types'][$typeID]['totalFlowSum'])) $result['types'][$typeID]['totalFlowSum'] = 0;
                    }

                    foreach ($items as $item) {
                        $itemID = $item[$params['itemAttrName']];
                        $flowSum = (int)$item['flowSum'];
                        $itemName = $item['itemName'];

                        $monthNumber = substr($item['date'], 5, 2);
                        $dayNumber = false === strpos($item['date'], '-') ? $item['date'] : substr(str_replace('-', '', $item['date']), 4);

                        // Сумма по статьям помесячно
                        $result[$typeID][$itemID][$monthNumber]['flowSum'] += $flowSum;
                        $resultDAY[$typeID][$itemID][$dayNumber]['flowSum'] += $flowSum;

                        // Итого по статьям
                        $result[$typeID][$itemID]['totalFlowSum'] += $flowSum;

                        // Существующие статьи
                        $result['itemName'][$typeID][$itemID] = $itemName;

                        // Сумма по типам помесячно
                        $result['types'][$typeID][$monthNumber]['flowSum'] += $flowSum;
                        $resultDAY['types'][$typeID][$dayNumber]['flowSum'] += $flowSum;

                        // Итого по типам
                        $result['types'][$typeID]['totalFlowSum'] += $flowSum;
                    }
                }

                // Сальдо
                foreach (self::$month as $monthNumber => $monthName) {
                    $sum = isset($result['types'][$typeID][$monthNumber]['flowSum']) ? (int)$result['types'][$typeID][$monthNumber]['flowSum'] : 0;
                    if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $result['balance'][$monthNumber] += $sum;
                    } else {
                        $result['balance'][$monthNumber] -= $sum;
                    }
                    // Сальдо нарастающим
                    $previousMonth = intval($monthNumber) - 1;
                    $previousMonth = $previousMonth > 9 ? $previousMonth : ('0' . $previousMonth);

                    $result['growingBalance'][$monthNumber] = $result['balance'][$monthNumber] +
                        // $result['growingBalanceContractorBalance'][$monthNumber] +
                        (isset($result['growingBalance'][$previousMonth]) ? $result['growingBalance'][$previousMonth] : 0);
                }
                // Сальдо по дням
                for ($m=1; $m<=12; $m++) foreach ($this->getDaysInMonth($this->year, $m) as $date => $dayName) {
                    $sum = isset($resultDAY['types'][$typeID][$date]['flowSum']) ? (int)$resultDAY['types'][$typeID][$date]['flowSum'] : 0;
                    if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $resultDAY['balance'][$date] += $sum;
                    } else {
                        $resultDAY['balance'][$date] -= $sum;
                    }
                    // Сальдо нарастающим
                    $prevDate = date('Y-m-d', strtotime($date) - 24 * 3600);

                    $resultDAY['growingBalance'][$date] = $resultDAY['balance'][$date] +
                        // $resultDAY['growingBalanceContractorBalance'][$date] +
                        (isset($resultDAY['growingBalance'][$prevDate]) ? $resultDAY['growingBalance'][$prevDate] : 0);
                }

            ////}

            $sum = isset($result['types'][$typeID]['totalFlowSum']) ? $result['types'][$typeID]['totalFlowSum'] : 0;
            if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                $result['balance']['totalFlowSum'] += $sum;
                $resultDAY['balance']['totalFlowSum'] += $sum;
            } else {
                $result['balance']['totalFlowSum'] -= $sum;
                $resultDAY['balance']['totalFlowSum'] -= $sum;
            }
            if (isset($result['itemName'][$typeID])) {
                asort($result['itemName'][$typeID]);
            }
        }

        ////// PREV YEARS
        if ($this->year !== min($this->getYearFilter())) {

            $dateFrom = empty($dateFrom) ? min($this->getYearFilter()) . '-01-01' : $dateFrom;
            $dateTo = empty($dateTo) ? ($this->year - 1) . '-12-31' : $dateTo;

            foreach (self::$typeItem as $typeID => $itemIDs) {
                $type = self::$flowTypeByActivityType[$typeID];
                $params = $this->getParamsByFlowType($type);
                foreach (self::flowClassArray() as $className) {
                    $query = $this->getDefaultCashFlowsQuery($className, $params, $dateFrom, $dateTo);
                    $items = $this->filterByActivityQuery($query, $params, $typeID)
                        ->select([
                            $className::tableName() . ".{$params['itemAttrName']}",
                            'invoiceItem.name as itemName',
                            'itemFlowOfFunds.flow_of_funds_block as flowOfFundsBlock',
                            'SUM(' . $className::tableName() . '.amount) as flowSum'
                        ])
                        ->groupBy([$className::tableName() . ".{$params['itemAttrName']}"])
                        ->asArray()
                        ->all();
                    foreach ($items as $item) {
                        $prevSum[$params['prevSumAttrName']] += (int)$item['flowSum'];
                        if (!isset($prevSum[self::$blockByType[$typeID]][$params['prevSumAttrName']])) {
                            $prevSum[self::$blockByType[$typeID]][$params['prevSumAttrName']] = $item['flowSum'];
                        } else {
                            $prevSum[self::$blockByType[$typeID]][$params['prevSumAttrName']] += $item['flowSum'];
                        }
                    }
                }
            }

            $balanceSum = 0;
            // /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
            // foreach (self::flowClassArray() as $className) {
            //     $balanceSum += $this->getCashFlowsContractorBalanceAmount($className, $dateFrom, $dateTo);
            // }
            $previousBalance = $prevSum['incomePrevSum'] - $prevSum['expenditurePrevSum'] + $balanceSum;

            // Sum with prev years
            foreach ($result['growingBalance'] as $key => $value) {
                $result['growingBalance'][$key] += $previousBalance;
            }
            // Sum with prev years by days
            foreach ($resultDAY['growingBalance'] as $key => $value) {
                $resultDAY['growingBalance'][$key] += $previousBalance;
            }
        }

        // Calculate Block Sum
        $incomeBlocks = [self::RECEIPT_FINANCING_TYPE_FIRST, self::INCOME_OPERATING_ACTIVITIES, self::INCOME_INVESTMENT_ACTIVITIES];
        $result['blocks'] = [];
        if (isset($result['types'])) {
            foreach ($result['types'] as $key => $data) {
                foreach ($data as $month => $value) {
                    $formattedValue = $month == 'totalFlowSum' ? $value : $value['flowSum'];
                    if (!isset($result['blocks'][self::$blockByType[$key]][$month]['flowSum'])) {
                        $result['blocks'][self::$blockByType[$key]][$month]['flowSum'] = 0;
                    }
                    if (in_array($key, $incomeBlocks)) {
                        $result['blocks'][self::$blockByType[$key]][$month]['flowSum'] += $formattedValue;

                        // add balance
                        //if ($key == self::INCOME_OPERATING_ACTIVITIES && $month != 'totalFlowSum') {
                        //    $result['blocks'][self::$blockByType[$key]][$month]['flowSum'] += $result['growingBalanceContractorBalance'][$month];
                        //}

                    } else {
                        $result['blocks'][self::$blockByType[$key]][$month]['flowSum'] -= $formattedValue;
                    }
                }
            }
        }
        // Calculate Block Sum by day
        $resultDAY['blocks'] = [];
        if (isset($resultDAY['types'])) {
            foreach ($resultDAY['types'] as $key => $data) {
                foreach ($data as $date => $value) {
                    $formattedValue = $date == 'totalFlowSum' ? $value : $value['flowSum'];
                    if (!isset($resultDAY['blocks'][self::$blockByType[$key]][$date]['flowSum'])) {
                        $resultDAY['blocks'][self::$blockByType[$key]][$date]['flowSum'] = 0;
                    }
                    if (in_array($key, $incomeBlocks)) {
                        $resultDAY['blocks'][self::$blockByType[$key]][$date]['flowSum'] += $formattedValue;

                        // add balance
                        //if ($key == self::INCOME_OPERATING_ACTIVITIES && $date != 'totalFlowSum') {
                        //    $resultDAY['blocks'][self::$blockByType[$key]][$date]['flowSum'] += $resultDAY['growingBalanceContractorBalance'][$date];
                        //}

                    } else {
                        $resultDAY['blocks'][self::$blockByType[$key]][$date]['flowSum'] -= $formattedValue;
                    }
                }
            }
        }

        // Calculate Growing Balance By Block
        foreach (self::$blocks as $blockType => $blockName) {
            $key = 0;
            foreach ($statisticsPeriod as $monthNumber => $monthText) {
                $formattedValue = isset($result['blocks'][$blockType][$monthNumber]['flowSum']) ? $result['blocks'][$blockType][$monthNumber]['flowSum'] : 0;
                $prevFlowSum = 0;
                if (!isset($growingBalanceByBlock[$blockType][$monthNumber]['flowSum'])) {
                    $result['growingBalanceByBlock'][$blockType][$monthNumber]['flowSum'] = $formattedValue;
                }

                if ($key === 0) {
                    if (isset($prevSum[$blockType]['incomePrevSum'])) {
                        $prevFlowSum += $prevSum[$blockType]['incomePrevSum'];
                    }
                    if (isset($prevSum[$blockType]['expenditurePrevSum'])) {
                        $prevFlowSum -= $prevSum[$blockType]['expenditurePrevSum'];
                    }
                } else {
                    $prevMonth = $monthNumber - 1;
                    $prevMonth = $prevMonth < 10 ? (0 . $prevMonth) : $prevMonth;
                    $prevFlowSum = $result['growingBalanceByBlock'][$blockType][$prevMonth]['flowSum'];
                }
                $result['growingBalanceByBlock'][$blockType][$monthNumber]['flowSum'] += $prevFlowSum;
                $key++;
            }
        }
        // Calculate Growing Balance By Block by day
        foreach (self::$blocks as $blockType => $blockName) {
            $key = 0;
            for($m=1; $m<=12; $m++) foreach ($this->getDaysInMonth($this->year, $m) as $date => $dayText) {
                $prevDate = date('md', strtotime($date) - 24 * 3600);
                $formattedValue = isset($resultDAY['blocks'][$blockType][$date]['flowSum']) ? $resultDAY['blocks'][$blockType][$date]['flowSum'] : 0;
                $prevFlowSum = 0;
                if (!isset($resultDAY['growingBalanceByBlock'][$blockType][$prevDate]['flowSum'])) {
                    $resultDAY['growingBalanceByBlock'][$blockType][$prevDate]['flowSum'] = 0;
                }
                if (!isset($growingBalanceByBlock[$blockType][$date]['flowSum'])) {
                    $resultDAY['growingBalanceByBlock'][$blockType][$date]['flowSum'] = $formattedValue;
                }

                if ($key === 0) {
                    if (isset($prevSum[$blockType]['incomePrevSum'])) {
                        $prevFlowSum += $prevSum[$blockType]['incomePrevSum'];
                    }
                    if (isset($prevSum[$blockType]['expenditurePrevSum'])) {
                        $prevFlowSum -= $prevSum[$blockType]['expenditurePrevSum'];
                    }
                } else {
                    $prevFlowSum = $resultDAY['growingBalanceByBlock'][$blockType][$prevDate]['flowSum'];
                }
                $resultDAY['growingBalanceByBlock'][$blockType][$date]['flowSum'] += $prevFlowSum;
                $key++;
            }
        }

        return ['result' => $result, 'resultDAY' => $resultDAY];
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function searchByPurse($params = [])
    {
        $this->_load($params);
        $result = [];
        $prevSum = [
            'incomePrevSum' => 0,
            'expenditurePrevSum' => 0,
        ];
        $result['balance']['totalFlowSum'] = 0;

        $statisticsPeriod = self::$month;

        foreach ($statisticsPeriod as $monthNumber => $monthText) {
            if (!isset($result['growingBalance'][$monthNumber])) {
                $result['growingBalance'][$monthNumber] = 0;
            }
        }
        foreach (self::$purseTypes as $typeID => $typeName) {
            /* @var $flowClassName CashBankFlows|CashOrderFlows|CashEmoneyFlows */
            $flowClassName = $this->getFlowClassNameByType(self::$purseBlockByType[$typeID]);
            $type = self::$flowTypeByPurseType[$typeID];
            $params = $this->getParamsByFlowType($type);
            foreach ($statisticsPeriod as $monthNumber => $monthText) {
                $dateFrom = $this->year . '-' . $monthNumber . '-01';
                $dateTo = $this->year . '-' . $monthNumber . '-' . cal_days_in_month(CAL_GREGORIAN, $monthNumber, $this->year);
                if (!isset($result['balance'][$monthNumber])) {
                    $result['balance'][$monthNumber] = 0;
                }
                $query = $this->getDefaultCashFlowsQuery($flowClassName, $params, $dateFrom, $dateTo);
                $items = $this->filterByPurseQuery($query)
                    ->select([
                        $flowClassName::tableName() . ".{$params['itemAttrName']}",
                        'invoiceItem.name as itemName',
                        'SUM(' . $flowClassName::tableName() . '.amount) as flowSum'
                    ])
                    ->groupBy([$flowClassName::tableName() . ".{$params['itemAttrName']}"])
                    ->asArray()
                    ->all();
                $itemsByCashContractor = $this->getDefaultItemsByCashFlowContractorQuery(
                    $flowClassName, $type, self::$cashContractorTypeByTableName[$flowClassName], $dateFrom, $dateTo
                )
                    ->groupBy([$flowClassName::tableName() . '.contractor_id'])
                    ->asArray()
                    ->all();
                foreach ($itemsByCashContractor as $itemByCashContractor) {
                    $items[] = $itemByCashContractor;
                }
                foreach ($items as $item) {
                    if (isset($item['contractorType'])) {
                        $itemID = $item['contractorType'];
                        $flowSum = (int)$item['flowSum'];
                        if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                            $itemName = 'Приход из ' . self::$cashContractorTextByType[$type][$itemID];
                        } else {
                            $itemName = 'Перевод в ' . self::$cashContractorTextByType[$type][$itemID];
                        }
                    } else {
                        $itemID = $item[$params['itemAttrName']];
                        $flowSum = (int)$item['flowSum'];
                        $itemName = $item['itemName'];
                    }
                    // Сумма по статьям помесячно
                    if (isset($result[$typeID][$itemID][$monthNumber])) {
                        $result[$typeID][$itemID][$monthNumber]['flowSum'] += $flowSum;
                    } else {
                        $result[$typeID][$itemID][$monthNumber] = ['flowSum' => $flowSum,];
                    }
                    // Итого по статьям
                    if (isset($result[$typeID][$itemID]['totalFlowSum'])) {
                        $result[$typeID][$itemID]['totalFlowSum'] += $flowSum;
                    } else {
                        $result[$typeID][$itemID]['totalFlowSum'] = $flowSum;
                    }
                    // Существующие статьи
                    $result['itemName'][$typeID][$itemID] = $itemName;
                    // Сумма по типам помесячно
                    if (isset($result['types'][$typeID][$monthNumber]['flowSum'])) {
                        $result['types'][$typeID][$monthNumber]['flowSum'] += $flowSum;
                    } else {
                        $result['types'][$typeID][$monthNumber]['flowSum'] = $flowSum;
                    }
                    // Итого по типам
                    if (isset($result['types'][$typeID]['totalFlowSum'])) {
                        $result['types'][$typeID]['totalFlowSum'] += $flowSum;
                    } else {
                        $result['types'][$typeID]['totalFlowSum'] = $flowSum;
                    }
                }
                // Сальдо
                $sum = isset($result['types'][$typeID][$monthNumber]['flowSum']) ?
                    (int)$result['types'][$typeID][$monthNumber]['flowSum'] : 0;
                if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                    $result['balance'][$monthNumber] += $sum;
                } else {
                    $result['balance'][$monthNumber] -= $sum;
                }
                // Сальдо нарастающим
                $previousMonth = intval($monthNumber) - 1;
                $previousMonth = $previousMonth > 9 ? $previousMonth : ('0' . $previousMonth);

                $result['growingBalance'][$monthNumber] = $result['balance'][$monthNumber] +
                    (isset($result['growingBalance'][$previousMonth]) ? $result['growingBalance'][$previousMonth] : 0);
            }
            $sum = isset($result['types'][$typeID]['totalFlowSum']) ?
                $result['types'][$typeID]['totalFlowSum'] : 0;
            if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                $result['balance']['totalFlowSum'] += $sum;
            } else {
                $result['balance']['totalFlowSum'] -= $sum;
            }
            if (isset($result['itemName'][$typeID])) {
                asort($result['itemName'][$typeID]);
            }
        }
        if ($this->year !== min($this->getYearFilter())) {
            $dateFrom = min($this->getYearFilter()) . '-01-01';
            $dateTo = ($this->year - 1) . '-12-31';

            foreach (self::$purseTypes as $typeID => $typeName) {
                /* @var $flowClassName CashBankFlows|CashOrderFlows|CashEmoneyFlows */
                $flowClassName = $this->getFlowClassNameByType(self::$purseBlockByType[$typeID]);
                $type = self::$flowTypeByPurseType[$typeID];
                $params = $this->getParamsByFlowType($type);
                $query = $this->getDefaultCashFlowsQuery($flowClassName, $params, $dateFrom, $dateTo);
                $items = $this->filterByPurseQuery($query)
                    ->select([
                        $flowClassName::tableName() . ".{$params['itemAttrName']}",
                        'invoiceItem.name as itemName',
                        'SUM(' . $flowClassName::tableName() . '.amount) as flowSum'
                    ])
                    ->groupBy([$flowClassName::tableName() . ".{$params['itemAttrName']}"])
                    ->asArray()
                    ->all();
                $itemsByCashContractor = $this->getDefaultItemsByCashFlowContractorQuery(
                    $flowClassName, $type, self::$cashContractorTypeByTableName[$flowClassName], $dateFrom, $dateTo
                )
                    ->groupBy([$flowClassName::tableName() . '.contractor_id'])
                    ->asArray()
                    ->all();
                foreach ($itemsByCashContractor as $itemByCashContractor) {
                    $items[] = $itemByCashContractor;
                }
                foreach ($items as $item) {
                    $prevSum[$params['prevSumAttrName']] += (int)$item['flowSum'];
                    if (!isset($prevSum[self::$purseBlockByType[$typeID]][$params['prevSumAttrName']])) {
                        $prevSum[self::$purseBlockByType[$typeID]][$params['prevSumAttrName']] = $item['flowSum'];
                    } else {
                        $prevSum[self::$purseBlockByType[$typeID]][$params['prevSumAttrName']] += $item['flowSum'];
                    }
                }
            }
            $previousBalance = $prevSum['incomePrevSum'] - $prevSum['expenditurePrevSum'];
            foreach ($result['growingBalance'] as $key => $value) {
                $result['growingBalance'][$key] += $previousBalance;
            }
        }

        // Calculate Block Sum
        $incomeBlocks = [self::INCOME_CASH_BANK, self::INCOME_CASH_ORDER, self::INCOME_CASH_EMONEY];
        $result['blocks'] = [];
        if (isset($result['types'])) {
            foreach ($result['types'] as $key => $data) {
                foreach ($data as $month => $value) {
                    $formattedValue = $month == 'totalFlowSum' ? $value : $value['flowSum'];
                    if (!isset($result['blocks'][self::$purseBlockByType[$key]][$month]['flowSum'])) {
                        $result['blocks'][self::$purseBlockByType[$key]][$month]['flowSum'] = 0;
                    }
                    if (in_array($key, $incomeBlocks)) {
                        $result['blocks'][self::$purseBlockByType[$key]][$month]['flowSum'] += $formattedValue;
                    } else {
                        $result['blocks'][self::$purseBlockByType[$key]][$month]['flowSum'] -= $formattedValue;
                    }
                }
            }
        }

        // Calculate Growing Balance By Block
        foreach (self::$purseBlocks as $blockType => $blockName) {
            $key = 0;
            foreach ($statisticsPeriod as $monthNumber => $monthText) {
                $formattedValue = isset($result['blocks'][$blockType][$monthNumber]['flowSum']) ?
                    $result['blocks'][$blockType][$monthNumber]['flowSum'] : 0;
                $prevFlowSum = 0;
                if (!isset($growingBalanceByBlock[$blockType][$monthNumber]['flowSum'])) {
                    $result['growingBalanceByBlock'][$blockType][$monthNumber]['flowSum'] = $formattedValue;
                }

                if ($key === 0) {
                    if (isset($prevSum[$blockType]['incomePrevSum'])) {
                        $prevFlowSum += $prevSum[$blockType]['incomePrevSum'];
                    }
                    if (isset($prevSum[$blockType]['expenditurePrevSum'])) {
                        $prevFlowSum -= $prevSum[$blockType]['expenditurePrevSum'];
                    }
                } else {
                    $prevMonth = $monthNumber - 1;
                    $prevMonth = $prevMonth < 10 ? (0 . $prevMonth) : $prevMonth;
                    $prevFlowSum = $result['growingBalanceByBlock'][$blockType][$prevMonth]['flowSum'];
                }
                $result['growingBalanceByBlock'][$blockType][$monthNumber]['flowSum'] += $prevFlowSum;
                $key++;
            }
        }


        return $result;
    }

    /**
     * @param array $params
     * @param null $dateFrom
     * @param null $dateTo
     * @return array
     * @throws \Exception
     */
    public function searchByPurseDays($params = [], $dateFrom = null, $dateTo = null)
    {
        $this->_load($params);
        $result = [];
        $resultDAY = [];

        $prevSum = [
            'incomePrevSum' => 0,
            'expenditurePrevSum' => 0,
        ];

        $result['balance'] = ["01" => 0, "02" => 0, "03" => 0, "04" => 0, "05" => 0, "06" => 0, "07" => 0, "08" => 0, "09" => 0, "10" => 0, "11" => 0, "12" => 0];
        $result['growingBalanceContractorBalance'] = ["01" => 0, "02" => 0, "03" => 0, "04" => 0, "05" => 0, "06" => 0, "07" => 0, "08" => 0, "09" => 0, "10" => 0, "11" => 0, "12" => 0];
        $result['balance']['totalFlowSum'] = 0;
        for ($m=1; $m <= 12; $m++) {
            foreach ($this->getDaysInMonth($this->year, $m) as $k=>$v) {
                $resultDAY['balance'][$k] = 0;
                $resultDAY['growingBalanceContractorBalance'][$k] = 0;
            }
        }
        $resultDAY['balance']['totalFlowSum'] = 0;

        $statisticsPeriod = self::$month;

        foreach (self::$purseTypes as $typeID => $typeName) {
            /* @var $flowClassName CashBankFlows|CashOrderFlows|CashEmoneyFlows */
            $flowClassName = $this->getFlowClassNameByType(self::$purseBlockByType[$typeID]);
            $type = self::$flowTypeByPurseType[$typeID];
            $params = $this->getParamsByFlowType($type);

            ////foreach ($statisticsPeriod as $monthNumber => $monthText) {

                $dateFrom = empty($dateFrom) ? $this->year . '-01-01' : $dateFrom;
                $dateTo = empty($dateTo) ? $this->year . '-12-31' : $dateTo;

                //$query = $this->getDefaultCashFlowsQuery($flowClassName, $params, $dateFrom, $dateTo);

                $query = $flowClassName::find()
                    ->leftJoin(['itemFlowOfFunds' => $params['itemFlowOfFundsTableName']],
                        "itemFlowOfFunds.{$params['flowOfFundItemName']} = " . $flowClassName::tableName() . ".{$params['itemAttrName']} AND
                    itemFlowOfFunds.company_id = " . $this->multiCompanyIds)
                    ->leftJoin(['invoiceItem' => $params['invoiceItemTableName']],
                        "invoiceItem.id = " . $flowClassName::tableName() . ".{$params['itemAttrName']}")
                    ->andWhere(['not', [$flowClassName::tableName() . ".{$params['itemAttrName']}" => null]])
                    //->andWhere(['not', ['contractor_id' => 'company']])
                    ->andWhere(['between', $flowClassName::tableName() . '.date', $dateFrom, $dateTo])
                    ->andWhere([$flowClassName::tableName() . '.company_id' => $this->multiCompanyIds]);

                $items = $this->filterByPurseQuery($query)
                    ->select([
                        $flowClassName::tableName() . ".{$params['itemAttrName']}",
                        'invoiceItem.name as itemName',
                        $flowClassName::tableName() . '.amount as flowSum',
                        'date'
                    ])
                    //->groupBy([$flowClassName::tableName() . ".{$params['itemAttrName']}"])
                    ->asArray()
                    ->all();

                // prepare
                $fs0 = ['flowSum' => 0];
                foreach ($items as $item) {
                    $itemID = $item[$params['itemAttrName']];
                    if (!isset($result[$typeID][$itemID])) $result[$typeID][$itemID] = ["01" => $fs0, "02" => $fs0, "03" => $fs0, "04" => $fs0, "05" => $fs0, "06" => $fs0, "07" => $fs0, "08" => $fs0, "09" => $fs0, "10" => $fs0, "11" => $fs0, "12" => $fs0];
                    if (!isset($result['types'][$typeID])) $result['types'][$typeID] = ["01" => $fs0, "02" => $fs0, "03" => $fs0, "04" => $fs0, "05" => $fs0, "06" => $fs0, "07" => $fs0, "08" => $fs0, "09" => $fs0, "10" => $fs0, "11" => $fs0, "12" => $fs0];
                    for ($m=1; $m <= 12; $m++) {
                        foreach ($this->getDaysInMonth($this->year, $m) as $k=>$v) {
                            if (!isset($resultDAY[$typeID][$itemID][$k])) $resultDAY[$typeID][$itemID][$k] = $fs0;
                            if (!isset($resultDAY['types'][$typeID][$k])) $resultDAY['types'][$typeID][$k] = $fs0;
                        }
                    }
                    if (!isset($result[$typeID][$itemID]['totalFlowSum'])) $result[$typeID][$itemID]['totalFlowSum'] = 0;
                    if (!isset($result['types'][$typeID]['totalFlowSum'])) $result['types'][$typeID]['totalFlowSum'] = 0;
                }

                $itemsByCashContractor =
                    $this->getDefaultItemsByCashFlowContractorQuery($flowClassName, $type, self::$cashContractorTypeByTableName[$flowClassName], $dateFrom, $dateTo)
                        ->groupBy([$flowClassName::tableName() . '.contractor_id', 'date'])
                        ->asArray()
                        ->all();

                // prepare 2
                foreach ($itemsByCashContractor as $itemByCashContractor) {
                    $itemID = $itemByCashContractor['contractorType'];
                    $items[] = $itemByCashContractor;
                    if (!isset($result[$typeID][$itemID])) $result[$typeID][$itemID] = ["01" => $fs0, "02" => $fs0, "03" => $fs0, "04" => $fs0, "05" => $fs0, "06" => $fs0, "07" => $fs0, "08" => $fs0, "09" => $fs0, "10" => $fs0, "11" => $fs0, "12" => $fs0];
                    if (!isset($result['types'][$typeID])) $result['types'][$typeID] = ["01" => $fs0, "02" => $fs0, "03" => $fs0, "04" => $fs0, "05" => $fs0, "06" => $fs0, "07" => $fs0, "08" => $fs0, "09" => $fs0, "10" => $fs0, "11" => $fs0, "12" => $fs0];
                    for ($m=1; $m <= 12; $m++) {
                        foreach ($this->getDaysInMonth($this->year, $m) as $k=>$v) {
                            if (!isset($resultDAY[$typeID][$itemID][$k])) $resultDAY[$typeID][$itemID][$k] = $fs0;
                            if (!isset($resultDAY['types'][$typeID][$k])) $resultDAY['types'][$typeID][$k] = $fs0;
                        }
                    }
                    if (!isset($result[$typeID][$itemID]['totalFlowSum'])) $result[$typeID][$itemID]['totalFlowSum'] = 0;
                    if (!isset($result['types'][$typeID]['totalFlowSum'])) $result['types'][$typeID]['totalFlowSum'] = 0;
                }

                foreach ($items as $item) {
                    if (isset($item['contractorType'])) {
                        $itemID = $item['contractorType'];
                        $flowSum = (int)$item['flowSum'];
                        if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                            $itemName = 'Приход из ' . self::$cashContractorTextByType[$type][$itemID];
                        } else {
                            $itemName = 'Перевод в ' . self::$cashContractorTextByType[$type][$itemID];
                        }
                    } else {
                        $itemID = $item[$params['itemAttrName']];
                        $flowSum = (int)$item['flowSum'];
                        $itemName = $item['itemName'];
                    }

                    $monthNumber = substr($item['date'], 5, 2);
                    $dayNumber = $item['date'];

                    // Сумма по статьям помесячно
                    $result[$typeID][$itemID][$monthNumber]['flowSum'] += $flowSum;
                    $resultDAY[$typeID][$itemID][$dayNumber]['flowSum'] += $flowSum;

                    // Итого по статьям
                    $result[$typeID][$itemID]['totalFlowSum'] += $flowSum;

                    // Существующие статьи
                    $result['itemName'][$typeID][$itemID] = $itemName;

                    // Сумма по типам помесячно
                    $result['types'][$typeID][$monthNumber]['flowSum'] += $flowSum;
                    $resultDAY['types'][$typeID][$dayNumber]['flowSum'] += $flowSum;

                    // Итого по типам
                    $result['types'][$typeID]['totalFlowSum'] += $flowSum;
                }

                // Сальдо
                foreach (self::$month as $monthNumber => $monthName) {
                    $sum = isset($result['types'][$typeID][$monthNumber]['flowSum']) ?
                        (int)$result['types'][$typeID][$monthNumber]['flowSum'] : 0;
                    if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $result['balance'][$monthNumber] += $sum;
                    } else {
                        $result['balance'][$monthNumber] -= $sum;
                    }
                    // Сальдо нарастающим
                    $previousMonth = intval($monthNumber) - 1;
                    $previousMonth = $previousMonth > 9 ? $previousMonth : ('0' . $previousMonth);

                    $result['growingBalance'][$monthNumber] = $result['balance'][$monthNumber] +
                        (isset($result['growingBalance'][$previousMonth]) ? $result['growingBalance'][$previousMonth] : 0);
                }
                for ($m=1; $m<=12; $m++) foreach ($this->getDaysInMonth($this->year, $m) as $date => $dayName) {
                    $sum = isset($resultDAY['types'][$typeID][$date]['flowSum']) ?
                        (int)$resultDAY['types'][$typeID][$date]['flowSum'] : 0;
                    if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $resultDAY['balance'][$date] += $sum;
                    } else {
                        $resultDAY['balance'][$date] -= $sum;
                    }
                    // Сальдо нарастающим
                    $prevDate = date('Y-m-d', strtotime($date) - 24 * 3600);

                    $resultDAY['growingBalance'][$date] = $resultDAY['balance'][$date] +
                        (isset($resultDAY['growingBalance'][$prevDate]) ? $resultDAY['growingBalance'][$prevDate] : 0);
                }

            /////}

            $sum = isset($result['types'][$typeID]['totalFlowSum']) ? $result['types'][$typeID]['totalFlowSum'] : 0;
            if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                $result['balance']['totalFlowSum'] += $sum;
                $resultDAY['balance']['totalFlowSum'] += $sum;
            } else {
                $result['balance']['totalFlowSum'] -= $sum;
                $resultDAY['balance']['totalFlowSum'] -= $sum;
            }
            if (isset($result['itemName'][$typeID])) {
                asort($result['itemName'][$typeID]);
            }
        }

        ////// PREV YEARS
        if ($this->year !== min($this->getYearFilter())) {

            $dateFrom = empty($dateFrom) ? min($this->getYearFilter()) . '-01-01' : $dateFrom;
            $dateTo = empty($dateTo) ? ($this->year - 1) . '-12-31' : $dateTo;

            foreach (self::$purseTypes as $typeID => $typeName) {
                /* @var $flowClassName CashBankFlows|CashOrderFlows|CashEmoneyFlows */
                $flowClassName = $this->getFlowClassNameByType(self::$purseBlockByType[$typeID]);
                $type = self::$flowTypeByPurseType[$typeID];
                $params = $this->getParamsByFlowType($type);
                $query = $this->getDefaultCashFlowsQuery($flowClassName, $params, $dateFrom, $dateTo);
                $items = $this->filterByPurseQuery($query)
                    ->select([
                        $flowClassName::tableName() . ".{$params['itemAttrName']}",
                        'invoiceItem.name as itemName',
                        'SUM(' . $flowClassName::tableName() . '.amount) as flowSum'
                    ])
                    ->groupBy([$flowClassName::tableName() . ".{$params['itemAttrName']}"])
                    ->asArray()
                    ->all();
                $itemsByCashContractor = $this->getDefaultItemsByCashFlowContractorQuery(
                    $flowClassName, $type, self::$cashContractorTypeByTableName[$flowClassName], $dateFrom, $dateTo
                )
                    ->groupBy([$flowClassName::tableName() . '.contractor_id'])
                    ->asArray()
                    ->all();
                foreach ($itemsByCashContractor as $itemByCashContractor) {
                    $items[] = $itemByCashContractor;
                }
                foreach ($items as $item) {
                    $prevSum[$params['prevSumAttrName']] += (int)$item['flowSum'];
                    if (!isset($prevSum[self::$purseBlockByType[$typeID]][$params['prevSumAttrName']])) {
                        $prevSum[self::$purseBlockByType[$typeID]][$params['prevSumAttrName']] = $item['flowSum'];
                    } else {
                        $prevSum[self::$purseBlockByType[$typeID]][$params['prevSumAttrName']] += $item['flowSum'];
                    }
                }
            }
            $previousBalance = $prevSum['incomePrevSum'] - $prevSum['expenditurePrevSum'];

            // Sum with prev years
            foreach ($result['growingBalance'] as $key => $value) {
                $result['growingBalance'][$key] += $previousBalance;
            }
            // Sum with prev years by days
            foreach ($resultDAY['growingBalance'] as $key => $value) {
                $resultDAY['growingBalance'][$key] += $previousBalance;
            }
        }

        // Calculate Block Sum
        $incomeBlocks = [self::INCOME_CASH_BANK, self::INCOME_CASH_ORDER, self::INCOME_CASH_EMONEY];
        $result['blocks'] = [];
        if (isset($result['types'])) {
            foreach ($result['types'] as $key => $data) {
                foreach ($data as $month => $value) {
                    $formattedValue = $month == 'totalFlowSum' ? $value : $value['flowSum'];
                    if (!isset($result['blocks'][self::$purseBlockByType[$key]][$month]['flowSum'])) {
                        $result['blocks'][self::$purseBlockByType[$key]][$month]['flowSum'] = 0;
                    }
                    if (in_array($key, $incomeBlocks)) {
                        $result['blocks'][self::$purseBlockByType[$key]][$month]['flowSum'] += $formattedValue;
                    } else {
                        $result['blocks'][self::$purseBlockByType[$key]][$month]['flowSum'] -= $formattedValue;
                    }
                }
            }
        }
        // Calculate Block Sum by days
        $resultDAY['blocks'] = [];
        if (isset($resultDAY['types'])) {
            foreach ($resultDAY['types'] as $key => $data) {
                foreach ($data as $date => $value) {
                    $formattedValue = $date == 'totalFlowSum' ? $value : $value['flowSum'];
                    if (!isset($resultDAY['blocks'][self::$purseBlockByType[$key]][$date]['flowSum'])) {
                        $resultDAY['blocks'][self::$purseBlockByType[$key]][$date]['flowSum'] = 0;
                    }
                    if (in_array($key, $incomeBlocks)) {
                        $resultDAY['blocks'][self::$purseBlockByType[$key]][$date]['flowSum'] += $formattedValue;
                    } else {
                        $resultDAY['blocks'][self::$purseBlockByType[$key]][$date]['flowSum'] -= $formattedValue;
                    }
                }
            }
        }

        // Calculate Growing Balance By Block
        foreach (self::$purseBlocks as $blockType => $blockName) {
            $key = 0;
            foreach ($statisticsPeriod as $monthNumber => $monthText) {
                $formattedValue = isset($result['blocks'][$blockType][$monthNumber]['flowSum']) ?
                    $result['blocks'][$blockType][$monthNumber]['flowSum'] : 0;
                $prevFlowSum = 0;
                if (!isset($growingBalanceByBlock[$blockType][$monthNumber]['flowSum'])) {
                    $result['growingBalanceByBlock'][$blockType][$monthNumber]['flowSum'] = $formattedValue;
                }

                if ($key === 0) {
                    if (isset($prevSum[$blockType]['incomePrevSum'])) {
                        $prevFlowSum += $prevSum[$blockType]['incomePrevSum'];
                    }
                    if (isset($prevSum[$blockType]['expenditurePrevSum'])) {
                        $prevFlowSum -= $prevSum[$blockType]['expenditurePrevSum'];
                    }
                } else {
                    $prevMonth = $monthNumber - 1;
                    $prevMonth = $prevMonth < 10 ? (0 . $prevMonth) : $prevMonth;
                    $prevFlowSum = $result['growingBalanceByBlock'][$blockType][$prevMonth]['flowSum'];
                }
                $result['growingBalanceByBlock'][$blockType][$monthNumber]['flowSum'] += $prevFlowSum;
                $key++;
            }
        }
        // Calculate Growing Balance By Block by day
        foreach (self::$purseBlocks as $blockType => $blockName) {
            $key = 0;
            for($m=1; $m<=12; $m++) foreach ($this->getDaysInMonth($this->year, $m) as $date => $dayText) {
                $formattedValue = isset($resultDAY['blocks'][$blockType][$date]['flowSum']) ?
                    $resultDAY['blocks'][$blockType][$date]['flowSum'] : 0;
                $prevFlowSum = 0;
                if (!isset($growingBalanceByBlock[$blockType][$date]['flowSum'])) {
                    $resultDAY['growingBalanceByBlock'][$blockType][$date]['flowSum'] = $formattedValue;
                }

                if ($key === 0) {
                    if (isset($prevSum[$blockType]['incomePrevSum'])) {
                        $prevFlowSum += $prevSum[$blockType]['incomePrevSum'];
                    }
                    if (isset($prevSum[$blockType]['expenditurePrevSum'])) {
                        $prevFlowSum -= $prevSum[$blockType]['expenditurePrevSum'];
                    }
                } else {
                    $prevDate = date('Y-m-d', strtotime($date) - 24 * 3600);
                    $prevFlowSum = $resultDAY['growingBalanceByBlock'][$blockType][$prevDate]['flowSum'];
                }
                $resultDAY['growingBalanceByBlock'][$blockType][$date]['flowSum'] += $prevFlowSum;
                $key++;
            }
        }

        return ['result' => $result, 'resultDAY' => $resultDAY];
    }

    /**
     * @param Company $company
     * @param $growingBalance
     * @param $types
     * @return array
     * @throws \yii\base\Exception
     */
    public function generateWarnings(Company $company, $growingBalance, $types)
    {
        $warnings = [];
        if ($growingBalance) {
            foreach ($growingBalance as $monthNumber => $oneAmount) {
                if ($oneAmount < 0) {
                    if (isset($warnings[1])) {
                        $warnings[1]['moreThenOne'] = true;
                    } else {
                        $warnings[1] = [
                            'amount' => abs($oneAmount),
                            'month' => self::$monthEnd[$monthNumber],
                            'moreThenOne' => false,
                        ];
                    }
                }
            }
        }

        $totalDebtSum = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_0_MORE_90, true, false);
        if ($totalDebtSum) {
            $incomeOperatingActivitiesAmount = 1;
            if (isset($types[self::INCOME_OPERATING_ACTIVITIES][date('m')]['flowSum'])
            && $types[self::INCOME_OPERATING_ACTIVITIES][date('m')]['flowSum'] > 0) {
                $incomeOperatingActivitiesAmount = $types[self::INCOME_OPERATING_ACTIVITIES][date('m')]['flowSum'];
            }
            $incomePercent = round(($totalDebtSum / 2 / $incomeOperatingActivitiesAmount) * 100, 2);
            $warnings[2] = [
                'totalDebtSum' => $totalDebtSum,
                'incomePercent' => $incomePercent,
            ];
        }
        $inInvoiceDebtSum = Invoice::find()
            ->select([
                'SUM(' . Invoice::tableName() . '.total_amount_with_nds) - IFNULL(SUM(' . Invoice::tableName() . '.payment_partial_amount), 0) AS sum',
            ])
            ->byIOType(Documents::IO_TYPE_IN)
            ->byDeleted()
            ->byStatus(InvoiceStatistic::$invoiceStatuses[InvoiceStatistic::NOT_PAID][Documents::IO_TYPE_IN])
            ->byCompany($company->id)
            ->asArray()
            ->one();
        $inInvoiceDebtSum = $inInvoiceDebtSum ? $inInvoiceDebtSum['sum'] : 0;
        if ($inInvoiceDebtSum) {
            $warnings[3] = [
                'inInvoiceDebtSum' => $inInvoiceDebtSum,
            ];
            $growingBalanceAmount = 0;
            if (isset($growingBalance[date('m')])) {
                $growingBalanceAmount = $growingBalance[date('m')];
            }
            if ($growingBalanceAmount >= $inInvoiceDebtSum) {
                $warnings[3]['message'] = 'Текущего остатка денежных средств хватит полностью оплатить все счета поставщикам.';
            } else {
                $canPayedPercent = round(($growingBalanceAmount / $inInvoiceDebtSum) * 100, 2);
                $warnings[3]['message'] = 'Текущего остатка денежных средств НЕ хватит полностью оплатить все счета
                 поставщикам. Можно оплатить только ' . $canPayedPercent . '% задолженности перед поставщиками.';
                if ($totalDebtSum) {
                    $zRaznitsa = $inInvoiceDebtSum - $growingBalanceAmount;
                    if ($zRaznitsa <= $totalDebtSum) {
                        $warnings[4] = [
                            'message' => 'Собрав ' . round(($zRaznitsa / $totalDebtSum) * 100, 2) . '% долгов ваших клиентов, вы сможете полностью оплатить долги перед поставщиками.',
                        ];
                    } else {
                        $warnings[4] = [
                            'message' => '<span style="color: red;">Плохие новости!</span><br>
                            Даже собрав 100% задолженности покупателей, вы не сможете рассчитаться с поставщиками.<br>
                            Не хватает ' . TextHelper::invoiceMoneyFormat($zRaznitsa - $totalDebtSum, 2) . ' <i class="fa fa-rub"></i>.
                            Выход:
                            <ul style="padding-left: 17px;">
                                <li>
                                    Увеличить продажи;
                                </li>
                                <li>
                                    Внести собственные деньги в компанию (дать займ);
                                </li>
                                <li>
                                    Взять кредит.
                                </li>
                            </ul>',
                            'debtPercent' => round(($zRaznitsa / $totalDebtSum) * 100, 2),
                        ];
                    }
                }
            }
        }

        return $warnings;
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     * @throws \Exception
     */
    public function searchItems($params = [])
    {
        $this->load($params);
        $this->checkCashContractor();
        $periodStart = $periodEnd = null;
        if ($this->period) {
            if (isset($this->period['day'])) {
                $day = ($this->period['day'] > 9) ? $this->period['day'] : ('0' . $this->period['day']);
                $periodStart = $this->year . '-' . $this->period['start'] . '-' . $day;
                $periodEnd = $this->year . '-' . $this->period['end'] . '-' . $day;
            } else {
                $periodStart = $this->year . '-' . $this->period['start'] . '-01';
                $periodEnd = $this->year . '-' . $this->period['end'] . '-' . cal_days_in_month(CAL_GREGORIAN, $this->period['end'], $this->year);
            }
        }

        // payment_type: 1-bank, 2-order, 3-emoney, 4-acquiring, 5-card
        if ($this->payment_type == self::CASH_CARD_BLOCK) {
            $query = (new Query)->from(['t' => $this->getCardQuery($this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id')]);
        } elseif ($this->payment_type == self::CASH_ACQUIRING_BLOCK) {
            $query = (new Query)->from(['t' => $this->getAcquiringQuery($this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id')]);
        } elseif ($this->payment_type) {
            if ($this->currency_name) {
                if ($this->currency_name === "RUB") {
                    $query = (new Query)
                        ->from(['t' =>
                            $this->getFlowQuery($this->payment_type, $this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id')]);
                } else {
                    $query = (new Query)
                        ->from(['t' =>
                            $this->getForeignFlowQuery($this->payment_type, $this->multiCompanyIds, $periodStart, $periodEnd, $this->currency_name)->groupBy('t.id')]);
                }
            } else {
                $query = (new Query)
                    ->from(['t' =>
                        $this->getFlowQuery($this->payment_type, $this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id')
                            ->union($this->getForeignFlowQuery($this->payment_type, $this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id'))
                    ]);
            }
        } else {
            $cbf = $this->getFlowQuery(self::CASH_BANK_BLOCK, $this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id');
            $cef = $this->getFlowQuery(self::CASH_EMONEY_BLOCK, $this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id');
            $cof = $this->getFlowQuery(self::CASH_ORDER_BLOCK, $this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id');
            $a = $this->getAcquiringQuery($this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id');
            $c = $this->getCardQuery($this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id');
            $cbf2 = $this->getForeignFlowQuery(self::CASH_BANK_BLOCK, $this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id');
            $cef2 = $this->getForeignFlowQuery(self::CASH_EMONEY_BLOCK, $this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id');
            $cof2 = $this->getForeignFlowQuery(self::CASH_ORDER_BLOCK, $this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id');

            $query = (new Query)
                ->from(['t' =>
                    $cbf->union($cef, true)
                        ->union($cof, true)
                        ->union($a, true)
                        ->union($c, true)
                        ->union($cbf2, true)
                        ->union($cef2, true)
                        ->union($cof2, true)
                ]);
        }

        $query->addSelect([
            't.id',
            't.company_id',
            't.tb',
            'SUM(amountExpense) as amountExpense',
            'SUM(amountIncome) as amountIncome',
            't.rs',
            't.created_at',
            't.date',
            't.flow_type',
            't.amount',
            't.amount_rub',
            't.currency_name',
            't.contractor_id',
            't.description',
            't.expenditure_item_id',
            't.income_item_id',
            't.document_number',
            't.document_additional_number',
            't.incomeItemName',
            't.expenseItemName',
            't.wallet_id',
            't.project_id',
            't.industry_id',
            't.sale_point_id',
            't.tin_child_amount',
        ]);

        switch ($this->group) {
            case self::GROUP_DATE:
                $query->groupBy('t.date');
                break;
            case self::GROUP_CONTRACTOR:
                $query->groupBy('t.contractor_id');
                break;
            case self::GROUP_PAYMENT_TYPE:
                $query->groupBy(['t.tb', 't.rs']);
                break;
            default:
                $query->groupBy(['t.wallet_id', 't.id']);
                break;
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'date' => [
                        'asc' => [
                            'date' => SORT_ASC,
                            'created_at' => SORT_ASC,
                        ],
                        'desc' => [
                            'date' => SORT_DESC,
                            'created_at' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'amountIncome' => [
                        'asc' => [
                            'flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'amountExpense' => [
                        'asc' => [
                            'flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'contractor_id',
                    'description',
                    'created_at',
                    'billPaying' => [
                        'asc' => ['document_number' => SORT_ASC, 'document_additional_number' => SORT_ASC],
                        'desc' => ['document_number' => SORT_DESC, 'document_additional_number' => SORT_ASC],
                        'default' => SORT_ASC
                    ],
                ],
                'defaultOrder' => [
                    'date' => SORT_DESC,
                ],
            ],
        ]);

        $query->andFilterWhere(['t.contractor_id' => $this->contractor_id]);
        $query->andFilterWhere(['t.company_id' => $this->company_id]);

        if (!empty($this->income_item_id) && !empty($this->expenditure_item_id)) {
            $query->andWhere(['or',
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_EXPENSE],
                    ['in', 'expenditure_item_id', $this->expenditure_item_id],
                ],
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_INCOME],
                    ['in', 'income_item_id', $this->income_item_id],
                ],
                $this->cash_contractor ? ['in', 'contractor_id', $this->cash_contractor] : [],
            ]);
        } elseif (!empty($this->income_item_id)) {

            $this->income_item_id =
                self::getArticlesWithChildren($this->income_item_id, CashBankFlows::FLOW_TYPE_INCOME, $this->multiCompanyIds);

            $query->andWhere(['or',
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_INCOME],
                    ['in', 'income_item_id', $this->income_item_id],
                ],
                $this->cash_contractor ?
                    ['and',
                        ['flow_type' => CashBankFlows::FLOW_TYPE_INCOME],
                        ['in', 'contractor_id', $this->cash_contractor]
                    ] : [],
            ]);
        } elseif (!empty($this->expenditure_item_id)) {

            $this->expenditure_item_id =
                self::getArticlesWithChildren($this->expenditure_item_id, CashBankFlows::FLOW_TYPE_EXPENSE, $this->multiCompanyIds);

            $query->andWhere(['or',
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_EXPENSE],
                    ['in', 'expenditure_item_id', $this->expenditure_item_id],
                ],
                $this->cash_contractor ?
                    ['and',
                        ['flow_type' => CashBankFlows::FLOW_TYPE_EXPENSE],
                        ['in', 'contractor_id', $this->cash_contractor]
                    ] : [],
            ]);
        } else {
            if ($this->cash_contractor) {
                $query->andWhere(['in', 'contractor_id', $this->cash_contractor]);
            } else {
                $query->andWhere(['flow_type' => null]);
            }
        }

        if ($this->reason_id == 'empty') {
            $query->andWhere(['or',
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_EXPENSE],
                    ['expenditure_item_id' => null],
                ],
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_INCOME],
                    ['income_item_id' => null],
                ],
            ]);
        } elseif (!empty($this->reason_id)) {
            $this->expenditure_item_id = $this->income_item_id = null;
            if (substr($this->reason_id, 0, 2) === 'e_') {
                $this->expenditure_item_id = substr($this->reason_id, 2);
            } elseif (substr($this->reason_id, 0, 2) === 'i_') {
                $this->income_item_id = substr($this->reason_id, 2);
            }
            $query->andFilterWhere([
                'expenditure_item_id' => $this->expenditure_item_id,
                'income_item_id' => $this->income_item_id,
            ]);
        }

        $query->andFilterWhere(['t.wallet_id' => $this->wallet_id]);

        if (strlen($this->project_id))
            $query->andWhere(['t.project_id' => $this->project_id ?: null]);
        if (strlen($this->industry_id))
            $query->andWhere(['t.industry_id' => $this->industry_id ?: null]);
        if (strlen($this->sale_point_id))
            $query->andWhere(['t.sale_point_id' => $this->sale_point_id ?: null]);

        $this->_filterQuery = clone $query;

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $range = [];
        $cashOrderMinDate = CashOrderFlows::find()
            ->byCompany($company->id)
            ->min('date');
        $cashBankMinDate = CashBankFlows::find()
            ->byCompany($company->id)
            ->min('date');
        $cashEmoneyMinDate = CashEmoneyFlows::find()
            ->byCompany($company->id)
            ->min('date');
        $minDates = [];

        if ($cashOrderMinDate) $minDates[] = $cashOrderMinDate;
        if ($cashBankMinDate) $minDates[] = $cashBankMinDate;
        if ($cashEmoneyMinDate) $minDates[] = $cashEmoneyMinDate;

        $minCashDate = !empty($minDates) ? min($minDates) : date(DateHelper::FORMAT_DATE);
        $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
        $currentYear = date('Y');
        foreach (range($registrationYear, $currentYear) as $value) {
            $range[$value] = $value;
        }
        arsort($range);

        return $range;
    }

    /**
     * @param $type
     * @throws \Exception
     */
    public function generateXls($type)
    {
        $data = $this->search($type, Yii::$app->request->get());
        $this->buildXls($type, $data, "ОДДС за {$this->year} год");
    }

    /**
     * @param $data
     * @param null $formName
     */
    public function _load($data, $formName = null)
    {
        if ($this->load($data, $formName)) {
            if (!in_array($this->year, $this->getYearFilter())) {
                $this->year = date('Y');
            }
        }
    }

    /**
     * @param $type
     * @return string
     * @throws \Exception
     */
    private function getFlowClassNameByType($type)
    {
        switch ($type) {
            case self::CASH_BANK_BLOCK:
                $className = CashBankFlows::class;
                break;
            case self::CASH_ORDER_BLOCK:
                $className = CashOrderFlows::class;
                break;
            case self::CASH_EMONEY_BLOCK:
                $className = CashEmoneyFlows::class;
                break;
            case self::CASH_ACQUIRING_BLOCK:
                $className = AcquiringOperation::class;
                break;
            case self::CASH_CARD_BLOCK:
                $className = CardOperation::class;
                break;
            default:
                throw new \Exception('Invalid type.');
                break;
        }

        return $className;
    }

    /**
     * @param $type
     * @return string
     * @throws \Exception
     */
    private function getForeignFlowClassNameByType($type)
    {
        switch ($type) {
            case self::CASH_BANK_BLOCK:
            case self::CASH_FOREIGN_BANK_BLOCK:
                $className = CashBankForeignCurrencyFlows::class;
                break;
            case self::CASH_ORDER_BLOCK:
            case self::CASH_FOREIGN_ORDER_BLOCK:
                $className = CashOrderForeignCurrencyFlows::class;
                break;
            case self::CASH_EMONEY_BLOCK:
            case self::CASH_FOREIGN_EMONEY_BLOCK:
                $className = CashEmoneyForeignCurrencyFlows::class;
                break;
            default:
                throw new \Exception('Invalid type.');
                break;
        }

        return $className;
    }

    /**
     * @param $cashBlock
     * @param $companyID
     * @param $periodStart
     * @param $periodEnd
     * @return \common\models\cash\query\CashFlowBase|\common\models\cash\query\CashOrderFlow
     * @throws \Exception
     */
    private function getFlowQuery($cashBlock, $companyID, $periodStart, $periodEnd)
    {
        /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
        $className = $this->getFlowClassNameByType($cashBlock);
        $tableName = $className::tableName();

        $query = $className::find()
            ->select([
                't.id',
                't.company_id',
                new Expression('"' . $tableName . '" as tb'),
                "IF({{t}}.[[flow_type]] = 0, SUM({{t}}.[[amount]]), 0) as amountExpense",
                "IF({{t}}.[[flow_type]] = 1, SUM({{t}}.[[amount]]), 0) as amountIncome",
                $cashBlock == self::CASH_BANK_BLOCK ? 't.rs as rs' : new Expression('null as rs'),
                't.created_at',
                't.date',
                't.flow_type',
                't.amount',
                new Expression('t.amount as amount_rub'),
                new Expression('"RUB" as currency_name'),
                't.contractor_id',
                't.description',
                't.expenditure_item_id',
                't.income_item_id',
                Invoice::tableName() . '.document_number',
                Invoice::tableName() . '.document_additional_number',
                InvoiceIncomeItem::tableName() . '.name incomeItemName',
                InvoiceExpenditureItem::tableName() . '.name expenseItemName',
                ($cashBlock == self::CASH_BANK_BLOCK ? 't.rs as wallet_id' : (
                  $cashBlock == self::CASH_ORDER_BLOCK ? 't.cashbox_id as wallet_id' : (
                    $cashBlock == self::CASH_EMONEY_BLOCK ? 't.emoney_id as wallet_id' :
                      new Expression('null as wallet_id')))),
                't.project_id',
                't.industry_id',
                't.sale_point_id',
                $cashBlock == self::CASH_BANK_BLOCK ? 't.tin_child_amount as tin_child_amount' : new Expression('null as tin_child_amount'),
            ])
            ->from(['t' => $tableName])
            ->joinWith('invoices')
            ->joinWith('incomeReason')
            ->joinWith('expenditureReason')
            ->andWhere(['t.company_id' => $companyID])
            ->andWhere(['between', 't.date', $periodStart, $periodEnd]);

        if ($cashBlock == self::CASH_BANK_BLOCK) {
            $query->andWhere(['t.has_tin_children' => 0]);
        }

        return $query;
    }

    private function getForeignFlowQuery($cashBlock, $companyID, $periodStart, $periodEnd, $currencyName = null)
    {
        /* @var $className CashBankForeignCurrencyFlows|CashOrderForeignCurrencyFlows|CashEmoneyForeignCurrencyFlows */
        $className = $this->getForeignFlowClassNameByType($cashBlock);
        $tableName = $className::tableName();

        $query = $className::find()
            ->select([
                't.id',
                't.company_id',
                new Expression('"' . $tableName . '" as tb'),
                "IF({{t}}.[[flow_type]] = 0, SUM({{t}}.[[amount]]), 0) as amountExpense",
                "IF({{t}}.[[flow_type]] = 1, SUM({{t}}.[[amount]]), 0) as amountIncome",
                $cashBlock == self::CASH_BANK_BLOCK ? 't.rs as rs' : new Expression('null as rs'),
                't.created_at',
                't.date',
                't.flow_type',
                't.amount',
                't.amount_rub',
                't.currency_name',
                't.contractor_id',
                't.description',
                't.expenditure_item_id',
                't.income_item_id',
                new Expression('null as document_number'),
                new Expression('null as document_additional_number'),
                InvoiceIncomeItem::tableName() . '.name incomeItemName',
                InvoiceExpenditureItem::tableName() . '.name expenseItemName',
                ($cashBlock == self::CASH_BANK_BLOCK ? 't.rs as wallet_id' : (
                $cashBlock == self::CASH_ORDER_BLOCK ? 't.cashbox_id as wallet_id' : (
                $cashBlock == self::CASH_EMONEY_BLOCK ? 't.emoney_id as wallet_id' :
                    new Expression('null as wallet_id')))),
                't.project_id',
                't.industry_id',
                't.sale_point_id',
                $cashBlock == self::CASH_BANK_BLOCK ? 't.tin_child_amount as tin_child_amount' : new Expression('null as tin_child_amount'),
            ])
            ->from(['t' => $tableName])
            ->joinWith('incomeReason')
            ->joinWith('expenditureReason')
            ->andWhere(['t.company_id' => $companyID])
            ->andWhere(['between', 't.date', $periodStart, $periodEnd]);

        if ($currencyName) {
            $query->andWhere(['t.currency_name' => $currencyName]);
        }

        if ($cashBlock == self::CASH_BANK_BLOCK) {
            $query->andWhere(['t.has_tin_children' => 0]);
        }

        return $query;
    }

    /**
     * @param $companyID
     * @param $periodStart
     * @param $periodEnd
     * @return \common\modules\acquiring\query\AcquiringQuery
     */
    private function getAcquiringQuery($companyID,  $periodStart, $periodEnd) {
        $tableName = AcquiringOperation::tableName();

        return AcquiringOperation::find()
            ->select([
                't.id',
                't.company_id',
                new Expression('"' . $tableName . '" as tb'),
                "IF({{t}}.[[flow_type]] = 0, SUM({{t}}.[[amount]]), 0) as amountExpense",
                "IF({{t}}.[[flow_type]] = 1, SUM({{t}}.[[amount]]), 0) as amountIncome",
                new Expression('null as rs'),
                new Expression('null as created_at'),
                't.date',
                't.flow_type',
                't.amount',
                new Expression('t.amount as amount_rub'),
                new Expression('"RUB" as currency_name'),
                't.contractor_id',
                't.description',
                't.expenditure_item_id',
                't.income_item_id',
                new Expression('null as document_number'),
                new Expression('null as document_additional_number'),
                InvoiceIncomeItem::tableName() . '.name incomeItemName',
                InvoiceExpenditureItem::tableName() . '.name expenseItemName',
                new Expression('t.acquiring_identifier as wallet_id'),
                't.project_id',
                't.industry_id',
                't.sale_point_id',
                new Expression('null as tin_child_amount'),
            ])
            ->from(['t' => $tableName])
            ->joinWith('incomeItem')
            ->joinWith('expenditureItem')
            ->andWhere(['t.company_id' => $companyID])
            ->andWhere(['between', 't.date', $periodStart, $periodEnd]);
    }

    /**
     * @param $companyID
     * @param $periodStart
     * @param $periodEnd
     * @return ActiveQuery
     */
    private function getCardQuery($companyID,  $periodStart, $periodEnd) {
        $tableName = CardOperation::tableName();

        return CardOperation::find()
            ->select([
                't.id',
                't.company_id',
                new Expression('"' . $tableName . '" as tb'),
                "IF({{t}}.[[flow_type]] = 0, SUM({{t}}.[[amount]]), 0) as amountExpense",
                "IF({{t}}.[[flow_type]] = 1, SUM({{t}}.[[amount]]), 0) as amountIncome",
                new Expression('null as rs'),
                new Expression('null as created_at'),
                't.date',
                't.flow_type',
                't.amount',
                new Expression('t.amount as amount_rub'),
                new Expression('"RUB" as currency_name'),
                't.contractor_id',
                't.description',
                't.expenditure_item_id',
                't.income_item_id',
                new Expression('null as document_number'),
                new Expression('null as document_additional_number'),
                InvoiceIncomeItem::tableName() . '.name incomeItemName',
                InvoiceExpenditureItem::tableName() . '.name expenseItemName',
                new Expression('t.account_id as wallet_id'),
                't.project_id',
                't.industry_id',
                't.sale_point_id',
                new Expression('null as tin_child_amount'),
            ])
            ->from(['t' => $tableName])
            ->joinWith('incomeItem')
            ->joinWith('expenditureItem')
            ->andWhere(['t.company_id' => $companyID])
            ->andWhere(['between', 't.date', $periodStart, $periodEnd]);
    }

    /**
     * @param $dates
     * @param bool $plan
     * @return array
     */
    public function searchByActivityByDate($dates, $plan = false)
    {
        $result = [];

        $flowClassList = (!$plan) ?
            self::flowClassArray() :
            [PlanCashFlows::class];

        foreach ($dates as $date) {
            $dateFrom = $date;
            $dateTo = $date;
            if (!isset($result['growingBalance'][$date])) {
                $result['growingBalance'][$date] = 0;
            }
            if (!isset($result['growingBalanceContractorBalance'][$date])) {
                $result['growingBalanceContractorBalance'][$date] = 0;
            }
            /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
            foreach ($flowClassList as $className) {
                $result['growingBalanceContractorBalance'][$date] += $this->getCashFlowsContractorBalanceAmount($className, $dateFrom, $dateTo);
            }

            /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
            foreach ($flowClassList as $className) {
                if (!isset($result['balance'][$date])) {
                    $result['balance'][$date] = 0;
                }
                $query = $className::find()
                    ->andWhere(['between', $className::tableName() . '.date', $dateFrom, $dateTo])
                    ->andWhere([$className::tableName() . '.company_id' => $this->multiCompanyIds])
                    ->andWhere(['not', ['contractor_id' =>
                        [
                            CashContractorType::BANK_TEXT,
                            CashContractorType::ORDER_TEXT,
                            CashContractorType::EMONEY_TEXT,
                        ]
                    ]]);
                $items = $query
                    ->select([
                        $className::tableName() . ".flow_type",
                        'SUM(' . $className::tableName() . '.amount) as flowSum'
                    ])
                    ->groupBy([$className::tableName() . ".flow_type"])
                    ->asArray()
                    ->all();

                foreach ($items as $item) {
                    $flowSum = (int)$item['flowSum'];

                    if ($item['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $result['balance'][$date] += $flowSum;
                    } else {
                        $result['balance'][$date] -= $flowSum;
                    }
                }
            }
        }

        $result['prevBalance'] = 0;
        $result['prevBalanceContractorBalance'] = 0;
        if (str_replace('-', '', $this->getDateFirstFlowFilter()) < str_replace('-', '', reset($dates))) {
            $dateFrom = $this->getDateFirstFlowFilter();
            $currDate = \DateTime::createFromFormat('Y-m-d', reset($dates));
            $currDate->modify("-1 day");
            $dateTo = $currDate->format('Y-m-d');

            /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
            foreach ($flowClassList as $className) {
                $result['prevBalanceContractorBalance'] += $this->getCashFlowsContractorBalanceAmount($className, $dateFrom, $dateTo);
            }

            foreach ($flowClassList as $className) {
                $query = $className::find()
                    ->andWhere(['between', $className::tableName() . '.date', $dateFrom, $dateTo])
                    ->andWhere([$className::tableName() . '.company_id' => $this->multiCompanyIds])
                    ->andWhere(['not', ['contractor_id' =>
                        [
                            CashContractorType::BANK_TEXT,
                            CashContractorType::ORDER_TEXT,
                            CashContractorType::EMONEY_TEXT,
                        ]
                    ]]);
                $items = $query
                    ->select([
                        $className::tableName() . ".flow_type",
                        'SUM(' . $className::tableName() . '.amount) as flowSum'
                    ])
                    ->groupBy([$className::tableName() . ".flow_type"])
                    ->asArray()
                    ->all();

                foreach ($items as $item) {
                    $flowSum = (int)$item['flowSum'];

                    if ($item['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $result['prevBalance'] += $flowSum;
                    } else {
                        $result['prevBalance'] -= $flowSum;
                    }
                }
            }
        }

        // add current periods
        foreach ($dates as $date) {
            $currDate = \DateTime::createFromFormat('Y-m-d', $date);
            $currDate->modify("-1 day");
            $previousDate = $currDate->format('Y-m-d');

            $result['growingBalance'][$date] = $result['balance'][$date] +
                $result['growingBalanceContractorBalance'][$date] +
                (isset($result['growingBalance'][$previousDate]) ? $result['growingBalance'][$previousDate] : 0);
        }

        // add all prev years
        foreach ($dates as $date) {
            $result['growingBalance'][$date] += $result['prevBalance'];
            $result['growingBalance'][$date] += $result['prevBalanceContractorBalance'];
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getDateFirstFlowFilter()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $range = [];
        $cashOrderMinDate = CashOrderFlows::find()
            ->byCompany($company->id)
            ->min('date');
        $cashBankMinDate = CashBankFlows::find()
            ->byCompany($company->id)
            ->min('date');
        $cashEmoneyMinDate = CashEmoneyFlows::find()
            ->byCompany($company->id)
            ->min('date');
        $minDates = [];

        if ($cashOrderMinDate) $minDates[] = $cashOrderMinDate;
        if ($cashBankMinDate) $minDates[] = $cashBankMinDate;
        if ($cashEmoneyMinDate) $minDates[] = $cashEmoneyMinDate;

        $minCashDate = !empty($minDates) ? min($minDates) : date(DateHelper::FORMAT_DATE);

        return $minCashDate;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// OLD METHODS 2019 (from AbstractFinance) todo: need update
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param $flowType
     * @return mixed
     */
    public function getParamsByFlowType($flowType)
    {
        $result['flowType'] = $flowType;
        if ($flowType == CashFlowsBase::FLOW_TYPE_INCOME) {
            $result['itemFlowOfFundsTableName'] = IncomeItemFlowOfFunds::tableName();
            $result['invoiceItemTableName'] = InvoiceIncomeItem::tableName();
            $result['itemAttrName'] = $result['flowOfFundItemName'] = 'income_item_id';
            $result['withoutItem'] = InvoiceIncomeItem::ITEM_OWN_FOUNDS;
            $result['flowTypeText'] = 'ПРИХОДЫ';
            $result['prevSumAttrName'] = 'incomePrevSum';
        } elseif ($flowType == CashFlowsBase::FLOW_TYPE_EXPENSE) {
            $result['itemFlowOfFundsTableName'] = ExpenseItemFlowOfFunds::tableName();
            $result['invoiceItemTableName'] = InvoiceExpenditureItem::tableName();
            $result['itemAttrName'] = 'expenditure_item_id';
            $result['flowOfFundItemName'] = 'expense_item_id';
            $result['withoutItem'] = InvoiceExpenditureItem::ITEM_OWN_FOUNDS;
            $result['flowTypeText'] = 'РАСХОДЫ';
            $result['prevSumAttrName'] = 'expenditurePrevSum';
        } else {
            throw new InvalidParamException('Invalid flow type param.');
        }

        return $result;
    }

    /**
     * @param $flowType
     * @return array
     */
    public function getItemsByFlowType($flowType)
    {
        if ($flowType == CashFlowsBase::FLOW_TYPE_INCOME) {
            $items = InvoiceIncomeItem::find()
                ->andWhere(['or',
                    ['company_id' => null],
                    ['company_id' => $this->multiCompanyIds],
                ])
                ->andWhere(['not', ['id' => InvoiceIncomeItem::ITEM_OWN_FOUNDS]])
                ->column();
        } elseif ($flowType == CashFlowsBase::FLOW_TYPE_EXPENSE) {
            $items = InvoiceExpenditureItem::find()
                ->andWhere(['or',
                    ['company_id' => null],
                    ['company_id' => $this->multiCompanyIds],
                ])
                ->andWhere(['not', ['id' => InvoiceExpenditureItem::ITEM_OWN_FOUNDS]])
                ->column();
        } else {
            throw new InvalidParamException('Invalid flow type param.');
        }

        return $items;
    }

    /**
     * @param $className CashBankFlows|CashOrderFlows|CashEmoneyFlows|PlanCashFlows
     * @param $flowTypeParams
     * @param $dateFrom
     * @param $dateTo
     * @return ActiveQuery
     */
    public function getDefaultCashFlowsQuery($className, $flowTypeParams, $dateFrom, $dateTo)
    {
        return $className::find()
            ->leftJoin(['itemFlowOfFunds' => $flowTypeParams['itemFlowOfFundsTableName']],
                "itemFlowOfFunds.{$flowTypeParams['flowOfFundItemName']} = " . $className::tableName() . ".{$flowTypeParams['itemAttrName']} AND
                itemFlowOfFunds.company_id = " . $this->multiCompanyIds)
            ->leftJoin(['invoiceItem' => $flowTypeParams['invoiceItemTableName']],
                "invoiceItem.id = " . $className::tableName() . ".{$flowTypeParams['itemAttrName']}")
            ->andWhere(['not', [$className::tableName() . ".{$flowTypeParams['itemAttrName']}" => $flowTypeParams['withoutItem']]])
            ->andWhere(['between', $className::tableName() . '.date', $dateFrom, $dateTo])
            ->andWhere([$className::tableName() . '.company_id' => $this->multiCompanyIds]);
    }


    /**
     * @param $itemName
     * @param $data
     * @return mixed
     */
    public function buildItem($itemName, $data)
    {
        $itemData['itemName'] = $itemName;
        foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText) {
            if (isset($data[$monthNumber])) {
                if (is_array($data[$monthNumber])) {
                    $itemData["item{$monthNumber}"] = $data[$monthNumber]['flowSum'] / 100;
                } else {
                    $itemData["item{$monthNumber}"] = $data[$monthNumber] / 100;
                }
            } else {
                $itemData["item{$monthNumber}"] = 0;
            }
        }
        if (isset($data['totalFlowSum'])) {
            if (is_array($data['totalFlowSum'])) {
                $itemData['dataYear'] = isset($data['totalFlowSum']['flowSum']) ? $data['totalFlowSum']['flowSum'] / 100 : 0;
            } else {
                $itemData['dataYear'] = $data['totalFlowSum'] / 100;
            }
        } else {
            $itemData['dataYear'] = 0;
        }

        return $itemData;
    }

    /**
     * @param $type
     * @param $data
     * @param $title
     */
    public function buildXls($type, $data, $title)
    {
        $formattedData = [];
        if (in_array($type, [
            PaymentCalendarSearch::TAB_BY_ACTIVITY,
            FlowOfFundsReportSearch::TAB_ODDS,
            PlanFactSearch::TAB_BY_ACTIVITY
        ])) {
            $oddsTypes = self::$types;
            $incomeTypes = [
                self::RECEIPT_FINANCING_TYPE_FIRST,
                self::INCOME_OPERATING_ACTIVITIES,
                self::INCOME_INVESTMENT_ACTIVITIES
            ];
            $oddsBlocks = self::$blocks;
            $blockByType = self::$blockByType;
            $growingBalanceLabel = self::$growingBalanceLabelByType;
        } elseif (in_array($type, [
            PaymentCalendarSearch::TAB_BY_PURSE,
            FlowOfFundsReportSearch::TAB_ODDS_BY_PURSE,
            PlanFactSearch::TAB_BY_PURSE
        ])) {
            $oddsTypes = self::$purseTypes;
            $incomeTypes = [
                self::INCOME_CASH_BANK,
                self::INCOME_CASH_ORDER,
                self::INCOME_CASH_EMONEY
            ];
            $oddsBlocks = self::$purseBlocks;
            $blockByType = self::$purseBlockByType;
            $growingBalanceLabel = self::$purseGrowingBalanceLabelByType;
        } else {
            throw new InvalidParamException('Invalid type param.');
        }
        $blocks = isset($data['blocks']) ? $data['blocks'] : [];
        $types = isset($data['types']) ? $data['types'] : [];
        $expenditureItems = isset($data['itemName']) ? $data['itemName'] : [];
        $balance = isset($data['balance']) ? $data['balance'] : 0;
        $growingBalance = isset($data['growingBalance']) ? $data['growingBalance'] : 0;
        asort($expenditureItems);

        foreach ($oddsTypes as $typeID => $typeName) {
            $class = in_array($typeID, $incomeTypes) ? 'income' : 'expense';
            if ($class == 'income') {
                $itemData = isset($blocks[$blockByType[$typeID]]) ? $blocks[$blockByType[$typeID]] : [];
                $formattedData[] = $this->buildItem($oddsBlocks[$blockByType[$typeID]], $itemData);
            }
            $itemData = isset($types[$typeID]) ? $types[$typeID] : [];
            $formattedData[] = $this->buildItem('    ' . $typeName, $itemData);
            if (isset($data[$typeID])) {
                foreach ($expenditureItems[$typeID] as $expenditureItemID => $expenditureItemName) {
                    if (isset($data[$typeID][$expenditureItemID])) {
                        $formattedData[] = $this->buildItem('        ' . $expenditureItemName, $data[$typeID][$expenditureItemID]);
                    }
                }
            }
            if ($class == 'expense') {
                if (isset($growingBalanceLabel[$typeID]))
                    $formattedData[] = $this->buildItem($growingBalanceLabel[$typeID], $data['growingBalanceByBlock'][$blockByType[$typeID]]);
            }
        }
        $formattedData[] = $this->buildItem('Результат по месяцу', $balance);
        $formattedData[] = $this->buildItem('Остаток на конец месяца', $growingBalance);
        $columns[] = [
            'attribute' => 'itemName',
            'header' => 'Статьи',
        ];
        foreach (self::$month as $monthNumber => $monthText) {
            $columns[] = [
                'attribute' => "item{$monthNumber}",
                'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                'header' => $monthText,
            ];
        }
        $columns[] = [
            'attribute' => 'dataYear',
            'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
            'header' => $this->year,
        ];

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $formattedData,
            'title' => $title,
            'rangeHeader' => range('A', 'N'),
            'columns' => $columns,
            'format' => 'Xlsx',
            'fileName' => $title,
        ]);
    }

    /**
     * @param ActiveQuery $query
     * @param $flowTypeParams
     * @param $typeID
     * @return ActiveQuery
     */
    public function filterByActivityQuery(ActiveQuery $query, $flowTypeParams, $typeID)
    {
        $block = self::$blockByType[$typeID];
        $itemIDs = self::$typeItem[$typeID];
        if (in_array($typeID, [self::INCOME_OPERATING_ACTIVITIES, self::WITHOUT_TYPE])) {
            if ($flowTypeParams['flowType'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                $itemIDs = InvoiceIncomeItem::find()
                    ->andWhere(['company_id' => null])
                    ->andWhere(['not', ['in', 'id', self::$typeItem[self::RECEIPT_FINANCING_TYPE_FIRST]]])
                    ->andWhere(['not', ['id' => $flowTypeParams['withoutItem']]])
                    ->column();
            } else {
                $itemIDs = InvoiceExpenditureItem::find()
                    ->andWhere(['company_id' => null])
                    ->andWhere(['not', ['in', 'id', self::$typeItem[self::RECEIPT_FINANCING_TYPE_SECOND]]])
                    ->andWhere(['not', ['id' => $flowTypeParams['withoutItem']]])
                    ->column();
            }
        }

        return $query->andWhere(['and',
            ['not', ['contractor_id' => [
                CashContractorType::BANK_TEXT,
                CashContractorType::ORDER_TEXT,
                CashContractorType::EMONEY_TEXT,
                //CashContractorType::BALANCE_TEXT,
            ]]],
            ['or',
                ['itemFlowOfFunds.flow_of_funds_block' => $block],
                ['and',
                    ['itemFlowOfFunds.flow_of_funds_block' => null],
                    ['in', "itemFlowOfFunds.{$flowTypeParams['flowOfFundItemName']}", $itemIDs],
                ],
                $typeID == self::INCOME_OPERATING_ACTIVITIES || $typeID == self::WITHOUT_TYPE ?
                    ['and',
                        ['itemFlowOfFunds.flow_of_funds_block' => null],
                        ['not', ['invoiceItem.company_id' => null]],
                    ] : [],
            ],
        ]);
    }

    /**
     * @param ActiveQuery $query
     * @param $flowTypeParams
     * @param $type
     * @return ActiveQuery
     */
    public function filterByPurseQuery(ActiveQuery $query)
    {
        return $query->andWhere(['not',
                ['contractor_id' =>
                    [
                        CashContractorType::BANK_TEXT,
                        CashContractorType::ORDER_TEXT,
                        CashContractorType::EMONEY_TEXT,
                    ],
                ],
            ]
        )->andWhere(['or',
            ['invoiceItem.company_id' => null],
            ['invoiceItem.company_id' => $this->multiCompanyIds],
        ]);
    }

    /**
     * @param $className CashBankFlows|CashOrderFlows|CashEmoneyFlows|PlanCashFlows
     * @param $flowType
     * @param $contractors
     * @param $dateFrom
     * @param $dateTo
     * @return ActiveQuery
     */
    public function getDefaultItemsByCashFlowContractorQuery($className, $flowType, $contractors, $dateFrom, $dateTo)
    {
        return $className::find()
            ->select([
                $className::tableName() . '.contractor_id as contractorType',
                'SUM(' . $className::tableName() . '.amount) as flowSum',
                'date',
            ])->andWhere(['and',
                ['flow_type' => $flowType],
                ['in', 'contractor_id', $contractors],
                [$className::tableName() . '.company_id' => $this->multiCompanyIds],
                ['between', $className::tableName() . '.date', $dateFrom, $dateTo],
            ]);
    }

    /**
     * @param $className CashBankFlows|CashOrderFlows|CashEmoneyFlows|PlanCashFlows
     * @param $from
     * @param $to
     * @return mixed
     */
    public function getCashFlowsContractorBalanceAmount($className, $from, $to)
    {
        return $className::find()
            ->andWhere(['company_id' => $this->multiCompanyIds])
            ->andWhere(['contractor_id' => CashContractorType::BALANCE_TEXT])
            ->andWhere(['between', 'date', $from, $to])
            ->sum('amount', Yii::$app->db2);
    }

    /**
     * @return array
     */
    public function getContractorFilterItems()
    {
        $query = clone $this->_filterQuery;

        $tContractor = Contractor::tableName();
        $tCashContractor = CashContractorType::tableName();
        $contractorIdArray = $query
            ->distinct()
            ->select("t.contractor_id")
            ->column(Yii::$app->db);
        $cashContractorArray = ArrayHelper::map(
            CashContractorType::find()
                ->andWhere(["$tCashContractor.name" => $contractorIdArray])
                ->all(Yii::$app->db),
            'name',
            'text'
        );
        $contractorArray = ArrayHelper::map(
            Contractor::getSorted()
                ->andWhere(["$tContractor.id" => $contractorIdArray])
                ->all(Yii::$app->db),
            'id',
            'shortName'
        );

        return (['' => 'Все контрагенты'] + $cashContractorArray + $contractorArray);
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function getReasonFilterItems()
    {
        $query = clone $this->_filterQuery;
        $e_reasonArray = $query
            ->distinct()
            ->select(new Expression('
                CONCAT("e_", `expenditure`.`id`) `reason_id`, 
                IF (LENGTH(`expenditureParent`.`name`) > 0, CONCAT(`expenditureParent`.`name`, " - ", `expenditure`.`name`), `expenditure`.`name`) `reason_name`
            '))
            ->leftJoin(InvoiceExpenditureItem::tableName() . ' `expenditure`', '`expenditure`.`id` = `expenditure_item_id`')
            ->leftJoin(InvoiceExpenditureItem::tableName() . ' `expenditureParent`', '`expenditureParent`.`id` = `expenditure`.`parent_id`')
            ->andWhere(['not', ['expenditure.id' => null]])
            ->createCommand()
            ->queryAll();

        $query = clone $this->_filterQuery;
        $i_reasonArray = $query
            ->distinct()
            ->select(new Expression('
                CONCAT("i_", `income`.`id`) `reason_id`, 
                IF (LENGTH(`incomeParent`.`name`) > 0, CONCAT(`incomeParent`.`name`, " - ", `income`.`name`), `income`.`name`) `reason_name`
            '))
            ->leftJoin(InvoiceIncomeItem::tableName() . ' `income`', '`income`.`id` = `income_item_id`')
            ->leftJoin(InvoiceIncomeItem::tableName() . ' `incomeParent`', '`incomeParent`.`id` = `income`.`parent_id`')
            ->andWhere(['not', ['income.id' => null]])
            ->createCommand()
            ->queryAll();

        $reasonArray = ArrayHelper::map(array_merge($e_reasonArray, $i_reasonArray), 'reason_id', 'reason_name');
        natsort($reasonArray);

        return $reasonArray;
    }


}