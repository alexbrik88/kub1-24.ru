<?php

namespace frontend\modules\analytics\models;

use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\Contractor;
use common\models\Company;
use Yii;

/**
 * This is the model class for table "plan_cash_rule".
 *
 * @property int $id
 * @property int $company_id
 * @property int $flow_type
 * @property int|null $income_item_id
 * @property int|null $expenditure_item_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Company $company
 */
class PlanCashRule extends \yii\db\ActiveRecord
{
    const ALL_INCOME_ITEMS = null;
    const ALL_EXPENDITURE_ITEMS = null;

    const INCOME_ITEMS_IDS = [
        InvoiceIncomeItem::ITEM_AGENT_FEE,
        InvoiceIncomeItem::ITEM_COMMISSION,
        InvoiceIncomeItem::ITEM_ENSURE_PAYMENT,
        InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER,
        InvoiceIncomeItem::ITEM_SALE_FIEXD_ASSETS,
        InvoiceIncomeItem::ITEM_OTHER,
        InvoiceIncomeItem::ITEM_PRODUCT,
        InvoiceIncomeItem::ITEM_TRADE_RECEIPTS,
    ];

    const EXPENDITURE_ITEMS_IDS = [
        InvoiceExpenditureItem::ITEM_LEASING_PREPAID,
        InvoiceExpenditureItem::ITEM_AGENT_FEE,
        InvoiceExpenditureItem::ITEM_IT,
        InvoiceExpenditureItem::ITEM_LEASE,
        InvoiceExpenditureItem::ITEM_BOOKKEEPING,
        InvoiceExpenditureItem::ITEM_DELIVERY,
        InvoiceExpenditureItem::ITEM_INTERNET,
        InvoiceExpenditureItem::ITEM_COMMUNAL_EXPENSES,
        InvoiceExpenditureItem::ITEM_LEASING,
        InvoiceExpenditureItem::ITEM_STATIONERY,
        InvoiceExpenditureItem::ITEM_FURNITURE,
        InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT,
        InvoiceExpenditureItem::ITEM_OFFICE_EQUIPMENT,
        InvoiceExpenditureItem::ITEM_PROGRAMS,
        InvoiceExpenditureItem::ITEM_PRODUCT,
        InvoiceExpenditureItem::ITEM_SERVICE,
        InvoiceExpenditureItem::ITEM_HOUSEHOLD_EXPENSES,
        InvoiceExpenditureItem::ITEM_ENSURE_PAYMENT,
        InvoiceExpenditureItem::ITEM_EDUCATION,
        InvoiceExpenditureItem::ITEM_BOUNTY_CONTRACTORS,
        InvoiceExpenditureItem::ITEM_BOUNTY_EMPLOYERS,
        InvoiceExpenditureItem::ITEM_BALANCE_ARTICLE_FIXED,
        InvoiceExpenditureItem::ITEM_BALANCE_ARTICLE_INTANGIBLE,
        InvoiceExpenditureItem::ITEM_TRAVEL,
        InvoiceExpenditureItem::ITEM_OTHER,
        InvoiceExpenditureItem::ITEM_ADVERTISING,
        InvoiceExpenditureItem::ITEM_ADVERTISING_PR,
        InvoiceExpenditureItem::ITEM_ADVERTISING_PRODUCTION,
        InvoiceExpenditureItem::ITEM_REPAIR,
        InvoiceExpenditureItem::ITEM_REPAIR_VEHICLE,
        InvoiceExpenditureItem::ITEM_REPAIR_OFFICE,
        InvoiceExpenditureItem::ITEM_INSURANCE,
        InvoiceExpenditureItem::ITEM_INSURANCE_VEHICLE,
        InvoiceExpenditureItem::ITEM_INSURANCE_HEALTH,
        InvoiceExpenditureItem::ITEM_LEASING_INSURANCE,
        InvoiceExpenditureItem::ITEM_CHOKY_SERVICE,
        InvoiceExpenditureItem::ITEM_TELEPHONY,
        InvoiceExpenditureItem::ITEM_FUEL,
        InvoiceExpenditureItem::ITEM_RECRUITMENT_AGENCY_SERVICE,
        InvoiceExpenditureItem::ITEM_ELECTRIC_POWER,
        InvoiceExpenditureItem::ITEM_LAWYER_SERVICE
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plan_cash_rule';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'flow_type', 'created_at', 'updated_at'], 'required'],
            [['company_id', 'flow_type', 'created_at', 'updated_at'], 'integer'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['income_item_id', 'expenditure_item_id'], 'safe'],
            [['income_item_id', 'expenditure_item_id'], 'filter', 'filter' => function($value) { return $value ?: null; }],
            [['expenditure_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceExpenditureItem::class, 'targetAttribute' => ['expenditure_item_id' => 'id']],
            [['income_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceIncomeItem::class, 'targetAttribute' => ['income_item_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'flow_type' => 'Flow Type',
            'income_item_id' => 'Income Item ID',
            'expenditure_item_id' => 'Expenditure Item ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    public function deletePlanContractors() {

        $contractors = PlanCashContractor::find()->where(['rule_id' => $this->id])->all();
        foreach ($contractors as $c) {
            $c->deleteFlows();
            $c->delete();
        }

        return true;
    }

    public function addPlanContractors()
    {
        $ioType = $this->flow_type + 1;

        $contractorsIds = Contractor::find()
            ->where(['company_id' => $this->company_id])
            ->andWhere(['type' => $ioType])
            ->byIsDeleted(false)
            ->byStatus(Contractor::ACTIVE)
            ->andFilterWhere(($ioType == Contractor::TYPE_SELLER) ?
                ['invoice_expenditure_item_id' => $this->expenditure_item_id] :
                ['invoice_income_item_id' => $this->income_item_id])
            ->select('id')
            ->distinct()
            ->column();

        $query2 = PlanCashContractor::find()
            ->where(['company_id' => $this->company_id])
            ->andWhere(['flow_type' => $this->flow_type])
            ->andWhere(['contractor_id' => $contractorsIds])
            ->select('contractor_id')
            ->distinct();

        $planContractorIds = $query2->column();

        foreach ($contractorsIds as $id) {

            if (in_array($id, $planContractorIds)) {
                // contractor present in plan
                continue;
            }

            if ($contractor = Contractor::findOne($id)) {

                $model = new PlanCashContractor([
                    'company_id' => $this->company_id,
                    'contractor_id' => $contractor->id,
                    'plan_type' => PlanCashContractor::PLAN_BY_FUTURE_INVOICES,
                    'flow_type' => $this->flow_type,
                    'payment_type' => PlanCashContractor::getPaymentType($contractor),
                    'item_id' => ($ioType == Contractor::TYPE_SELLER) ? $contractor->invoice_expenditure_item_id : $contractor->invoice_income_item_id,
                    'payment_delay' => ($ioType == Contractor::TYPE_SELLER) ? $contractor->seller_payment_delay : $contractor->customer_payment_delay,
                    'created_at' => time(),
                    'updated_at' => time(),
                    'rule_id' => $this->id
                ]);

                if (!$model->save()) {

                    // var_dump('addPlanContractors: ', $model->getErrors()); exit;

                    return false;
                }

                //////////////////////////
                /// CREATE AUTOPLAN FLOWS
                $model->createFlowsByActualInvoices();
                /////////////////////////

            }
        }

        return true;
    }

    /**
     * Get rules articles by flows
     * @param $company_id
     * @return array
     */
    public static function getArticles($company_id)
    {
        /** @var PlanCashRule[] $articles */
        $articles = self::find()
            ->where(['company_id' => $company_id])
            ->all();

        $ret = [0 => [], 1 => []];
        foreach ($articles as $v) {

            if (!isset($ret[$v->flow_type]))
                $ret[$v->flow_type] = [];

            $itemId = ($v->flow_type == 0) ? $v->expenditure_item_id : $v->income_item_id;
            $ret[$v->flow_type][$itemId] = $itemId;
        }

        return $ret;
    }
}
