<?php

namespace frontend\modules\analytics\models;

use common\components\date\DatePickerFormatBehavior;
use common\components\TextHelper;
use Yii;
use common\models\Company;
use common\models\Contractor;

/**
 * This is the model class for table "cash_flows_auto_plan".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $flow_type
 * @property integer $payment_type
 * @property integer $item
 * @property string $contractor_id
 * @property string $amount
 * @property string $plan_date
 * @property string $end_plan_date
 * @property boolean $is_deleted
 *
 * @property Company $company
 * @property Contractor $contractor
 */
class CashFlowsAutoPlan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cash_flows_auto_plan';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => DatePickerFormatBehavior::className(),
                'attributes' => [
                    'plan_date' => [
                        'message' => 'Дата расхода указана неверно.',
                    ],
                    'end_plan_date' => [
                        'message' => 'Дата окончания планирования указана неверно.',
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'flow_type', 'payment_type', 'item', 'contractor_id'], 'required'],
            [['company_id', 'flow_type', 'payment_type', 'item', 'is_deleted'], 'integer'],
            [['amount'],
                'number',
                'numberPattern' => Yii::$app->params['numberPattern'],
                'max' => Yii::$app->params['maxCashSum'] * 100, // with kopeck
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
            [['plan_date', 'end_plan_date', 'contractor_id'], 'safe'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'flow_type' => 'Flow Type',
            'payment_type' => 'Payment Type',
            'item' => 'Item',
            'contractor_id' => 'Contractor ID',
            'amount' => 'Amount',
            'plan_date' => 'Plan Date',
            'end_plan_date' => 'End Plan Date',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->amount = TextHelper::parseMoneyInput($this->amount);

        if ($this->isAttributeChanged('amount', false)) {
            $this->amount = round($this->amount * 100);
        }

        return parent::beforeValidate();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
