<?php

namespace frontend\modules\analytics\models;

use common\components\date\DateHelper;
use common\components\helpers\Month;
use common\models\Contractor;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\models\document\InvoiceExpenditureItem;
use DateTime;
use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

class ExpensesPaymentsSearch extends Model
{
    /**
     * period ID constants
     */
    const PERIOD_LAST_QUARTER = 1;
    const PERIOD_PREV_QUARTER = 2;
    const PERIOD_LAST_YEAR_M = 3;
    const PERIOD_LAST_YEAR_Q = 4;
    const PERIOD_LAST_4_QUARTERS = 5;
    const PERIOD_PREV_YEAR_M = 6;
    const PERIOD_PREV_YEAR_Q = 7;
    const PERIOD_LAST_4_MONTH = 8;

    /**
     * period step constants
     */
    const PERIOD_STEP_MONTH = 'MONTH';
    const PERIOD_STEP_QUARTER = 'QUARTER';

    protected $_periodParams;
    protected $_dataArray;

    public $companyId;
    public $period;
    public $accounting = '';

    public static $periodArray = [
        self::PERIOD_LAST_4_MONTH => 'Последние 4 месяца',
        self::PERIOD_LAST_QUARTER => 'Этот квартал',
        self::PERIOD_PREV_QUARTER => 'Предыдущий квартал',
        // self::PERIOD_LAST_YEAR_M => 'Месяца текущего года',
        self::PERIOD_LAST_YEAR_Q => 'Кварталы текущего года',
        self::PERIOD_LAST_4_QUARTERS => 'Последние 4 квартала',
        // self::PERIOD_PREV_YEAR_M => 'Месяца прошлого года',
        self::PERIOD_PREV_YEAR_Q => 'Кварталы прошлого года',
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['period'], 'filter', 'filter' => function ($value) {
                return (int)$value;
            }],
            [
                ['period'], 'in', 'range' => array_keys(self::$periodArray),
                'message' => 'Не верно выбран период.'
            ],
            [['accounting'], 'safe'],
        ];
    }

    public static function flowClassArray()
    {
        return [
            CashBankFlows::className(),
            CashOrderFlows::className(),
            CashEmoneyFlows::className(),
        ];
    }

    public function getPaymentsDataArray($params)
    {
        $flowArray = [];

        $this->load($params, '');
        if (!$this->validate()) {
            $this->period = self::PERIOD_LAST_4_MONTH;
        }

        $this->setPeriodParams($this->period);
        $periodFrom = $this->periodParams['range']['from'];
        $periodTill = $this->periodParams['range']['till'];
        $step = $this->periodParams['step'];

        $flowArray = $this->getAllFlows($periodFrom, $periodTill, $step);

        $dataArray = [];
        foreach ($flowArray as $flow) {
            isset($dataArray[$flow['expenses_id']][$flow['contractor_id']][$flow['period_step']]) ?
                $dataArray[$flow['expenses_id']][$flow['contractor_id']][$flow['period_step']] += $flow['amount'] :
                $dataArray[$flow['expenses_id']][$flow['contractor_id']][$flow['period_step']] = (int)$flow['amount'];
        }

        $this->_dataArray = $this->buildDataArray($dataArray);

        return $this->_dataArray;
    }

    public function buildDataArray($dataArray = [])
    {
        $dataChartArray = [
            0 => [
                'sum' => 0,
            ],
        ];
        $dataRowArray = [];
        $totalRow = [
            'row_type' => 'total',
            'expenses' => 'Итого',
            'summary' => 0,
            'summaryPrior' => 0,
            'summaryPriorStep' => 0,
        ];
        foreach ($this->subPeriodColumns as $column) {
            $totalRow[$column['attribute']] = 0;
            $dataChartArray[0][$column['attribute']] = 0;
        }
        $periodPriorLastStep = $this->periodPriorLastColumn['attribute'];

        foreach ($this->expensesItems as $expensesId => $expensesName) {
            if (isset($dataArray[$expensesId])) {
                $expensesRow = [
                    'row_type' => 'expenses',
                    'expenses_id' => $expensesId,
                    'expenses' => $expensesName,
                    'summary' => 0,
                    'summaryPrior' => 0,
                    'summaryPriorStep' => 0,
                ];
                foreach ($this->subPeriodColumns as $column) {
                    $expensesRow[$column['attribute']] = 0;
                }
                $contractorRowArray = [];
                $contractorIdArray = array_keys($dataArray[$expensesId]);
                $contractorArray = Contractor::getSorted()->indexBy('id')->andWhere([Contractor::tableName() . '.id' => $contractorIdArray])->all();
                foreach ($contractorArray as $contractor) {
                    if ($contractor->type == Contractor::TYPE_SELLER) {
                        $content = Html::a($contractor->shortName, ['/contractor/view', 'type' => Contractor::TYPE_SELLER, 'id' => $contractor->id]);
                    } else {
                        $content = $contractor->name;
                    }
                    $contractorRow = [
                        'row_type' => 'contractor',
                        'expenses_id' => $expensesId,
                        'expenses' => $content,
                        'summary' => 0,
                        'summaryPrior' => 0,
                        'summaryPriorStep' => 0,
                    ];
                    $priorFlowArray = $this->getAllFlows(
                        $this->periodParams['rangePrior']['from'],
                        $this->periodParams['rangePrior']['till'],
                        $this->periodParams['step'],
                        $expensesId,
                        $contractor->id
                    );

                    foreach ($priorFlowArray as $flow) {
                        $contractorRow['summaryPrior'] += $flow['amount'];
                        if ($flow['period_step'] == $periodPriorLastStep) {
                            $contractorRow['summaryPriorStep'] += $flow['amount'];
                        }
                    }

                    foreach ($dataArray[$expensesId][$contractor->id] as $attribute => $value) {
                        $contractorRow[$attribute] = $value;
                        $expensesRow[$attribute] += $value;
                        $totalRow[$attribute] += $value;
                        $contractorRow['summary'] += $value;
                    }
                    $expensesRow['summary'] += $contractorRow['summary'];
                    $expensesRow['summaryPrior'] += $contractorRow['summaryPrior'];
                    $expensesRow['summaryPriorStep'] += $contractorRow['summaryPriorStep'];
                    $totalRow['summary'] += $contractorRow['summary'];
                    $totalRow['summaryPrior'] += $contractorRow['summaryPrior'];
                    $totalRow['summaryPriorStep'] += $contractorRow['summaryPriorStep'];
                    $contractorRowArray[] = $contractorRow;
                }
                foreach ($this->subPeriodColumns as $key => $column) {
                    if (in_array((int)$expensesId, [3, 4])) {
                        $dataChartArray[0][$column['attribute']] += $expensesRow[$column['attribute']];
                    } else {
                        $dataChartArray[$expensesId][$column['attribute']] = $expensesRow[$column['attribute']];
                    }
                }
                if (in_array((int)$expensesId, [3, 4])) {
                    $dataChartArray[0]['sum'] += $expensesRow['summary'];
                } else {
                    $dataChartArray[$expensesId]['sum'] = $expensesRow['summary'];
                }
                $dataRowArray = array_merge($dataRowArray, [$expensesRow], $contractorRowArray);
            }
        }
        uasort($dataChartArray, function ($a, $b) {
            return $b['sum'] - $a['sum'];
        });

        return ['dataRowArray' => $dataRowArray, 'totalRow' => $totalRow, 'dataChartArray' => $dataChartArray];
    }

    public function getAllFlows($periodFrom, $periodTill, $step = self::PERIOD_STEP_MONTH, $expensesId = null, $contractorId = null)
    {
        $flowArray = [];

        foreach (self::flowClassArray() as $className) {
            $flowArray = array_merge($flowArray, $this->getFlowsOfClass($className, $periodFrom, $periodTill, $step, $expensesId, $contractorId));
        }

        return $flowArray;
    }

    public function getFlowsOfClass($className, $periodFrom, $periodTill, $step, $expensesId, $contractorId)
    {
        $flowTable = $className::tableName();
        $contractorTable = Contractor::tableName();
        $query = $className::find()
            ->leftJoin($contractorTable, "{{{$contractorTable}}}.[[id]] = {{{$flowTable}}}.[[contractor_id]]")
            ->select([
                "IFNULL({{{$flowTable}}}.[[expenditure_item_id]],0) [[expenses_id]]",
                "$flowTable.contractor_id",
                "$flowTable.amount",
                "CONCAT( YEAR({{{$flowTable}}}.[[date]]), {$step}({{{$flowTable}}}.[[date]]) ) [[period_step]]"
            ])
            ->where([
                'and',
                ["$flowTable.company_id" => $this->companyId, "$flowTable.flow_type" => CashFlowsBase::FLOW_TYPE_EXPENSE],
                ["between", "$flowTable.date", $periodFrom, $periodTill],
                ['not', ["$contractorTable.id" => null]],
                ['not', ["$flowTable.expenditure_item_id" => InvoiceExpenditureItem::ITEM_OWN_FOUNDS]],
            ])
            ->asArray();

        if ($contractorId) {
            $query->andWhere([
                "$flowTable.contractor_id" => $contractorId,
                "$flowTable.expenditure_item_id" => $expensesId ? $expensesId : null,
            ]);
        }

        if ($className != CashBankFlows::className()) {
            $query->andFilterWhere(['is_accounting' => $this->accounting]);
        } elseif ($this->accounting === '0') {
            return [];
        }

        return $query->all();
    }

    public function getPeriodName()
    {
        return self::$periodArray[$this->period];
    }

    public function setPeriodParams($periodId)
    {
        switch ($periodId) {
            case self::PERIOD_PREV_QUARTER:
                $date = new DateTime('first day of -3 month');
                $month = $date->format('n');
                $querter = ceil($month / 3);
                $year = $date->format('Y');
                $title = $querter . '-й квартал, ' . $year . ' г.';

                $monthNumber = $month - ($querter - 1) * 3;

                $offset = 1 - $monthNumber;
                $date->modify("$offset month");
                $datePrior = clone $date;
                $range = [
                    'from' => $date->modify('first day of this month')->format('Y-m-d'),
                    'till' => $date->modify('last day of + 2 month')->format('Y-m-d'),
                ];
                $rangePrior = [
                    'from' => $datePrior->modify('first day of - 3 month')->format('Y-m-d'),
                    'till' => $datePrior->modify('last day of + 2 month')->format('Y-m-d'),
                ];
                $step = self::PERIOD_STEP_MONTH;
                break;

            case self::PERIOD_LAST_YEAR_M:
                $date = new DateTime();
                $year = $date->format('Y');
                $title = $year . ' г.';
                $yearPrior = $year - 1;
                $range = [
                    'from' => $date->modify("first day of Jan $year")->format('Y-m-d'),
                    'till' => $date->modify("last day of Dec $year")->format('Y-m-d'),
                ];
                $rangePrior = [
                    'from' => $date->modify("first day of Jan $yearPrior")->format('Y-m-d'),
                    'till' => $date->modify("last day of Dec $yearPrior")->format('Y-m-d'),
                ];
                $step = self::PERIOD_STEP_MONTH;
                break;

            case self::PERIOD_LAST_YEAR_Q:
                $date = new DateTime();
                $year = $date->format('Y');
                $title = $year . ' г.';
                $yearPrior = $year - 1;
                $range = [
                    'from' => $date->modify("first day of Jan $year")->format('Y-m-d'),
                    'till' => $date->modify("last day of Dec $year")->format('Y-m-d'),
                ];
                $rangePrior = [
                    'from' => $date->modify("first day of Jan $yearPrior")->format('Y-m-d'),
                    'till' => $date->modify("last day of Dec $yearPrior")->format('Y-m-d'),
                ];
                $step = self::PERIOD_STEP_QUARTER;
                break;

            case self::PERIOD_LAST_4_QUARTERS:
                $offset = (date('n') % 3) + 8;
                $date = new DateTime("- $offset month");
                $title = 'Последние 4 квартала';
                $datePrior = clone $date;
                $range = [
                    'from' => $date->modify("first day of this month")->format('Y-m-d'),
                    'till' => $date->modify("last day of +11 month")->format('Y-m-d'),
                ];
                $rangePrior = [
                    'from' => $datePrior->modify("first day of -12 month")->format('Y-m-d'),
                    'till' => $datePrior->modify("last day of +11 month")->format('Y-m-d'),
                ];
                $step = self::PERIOD_STEP_QUARTER;
                break;

            case self::PERIOD_PREV_YEAR_M:
                $date = new DateTime('-1 year');
                $year = $date->format('Y');
                $title = $year . ' г.';
                $yearPrior = $year - 1;
                $range = [
                    'from' => $date->modify("first day of Jan $year")->format('Y-m-d'),
                    'till' => $date->modify("last day of Dec $year")->format('Y-m-d'),
                ];
                $rangePrior = [
                    'from' => $date->modify("first day of Jan $yearPrior")->format('Y-m-d'),
                    'till' => $date->modify("last day of Dec $yearPrior")->format('Y-m-d'),
                ];
                $step = self::PERIOD_STEP_MONTH;
                break;

            case self::PERIOD_PREV_YEAR_Q:
                $date = new DateTime('-1 year');
                $year = $date->format('Y');
                $title = $year . ' г.';
                $yearPrior = $year - 1;
                $range = [
                    'from' => $date->modify("first day of Jan $year")->format('Y-m-d'),
                    'till' => $date->modify("last day of Dec $year")->format('Y-m-d'),
                ];
                $rangePrior = [
                    'from' => $date->modify("first day of Jan $yearPrior")->format('Y-m-d'),
                    'till' => $date->modify("last day of Dec $yearPrior")->format('Y-m-d'),
                ];
                $step = self::PERIOD_STEP_QUARTER;
                break;

            case self::PERIOD_LAST_4_MONTH:
                $date = new DateTime('-3 month');
                $title = 'Последние 4 месяца';
                $datePrior = clone $date;
                $range = [
                    'from' => $date->modify("first day of this month")->format('Y-m-d'),
                    'till' => $date->modify("last day of + 3 month")->format('Y-m-d'),
                ];
                $rangePrior = [
                    'from' => $datePrior->modify("first day of - 4 month")->format('Y-m-d'),
                    'till' => $datePrior->modify("last day of + 3 month")->format('Y-m-d'),
                ];
                $step = self::PERIOD_STEP_MONTH;
                break;

            case self::PERIOD_LAST_QUARTER:
            default:
                $date = new DateTime;
                $month = $date->format('n');
                $querter = ceil($month / 3);
                $year = $date->format('Y');
                $title = $querter . '-й квартал, ' . $year . ' г.';

                $monthNumber = $month - ($querter - 1) * 3;
                $offset = 1 - $monthNumber;
                $date->modify("$offset month");
                $datePrior = clone $date;
                $range = [
                    'from' => $date->modify('first day of this month')->format('Y-m-d'),
                    'till' => $date->modify('last day of + 2 month')->format('Y-m-d'),
                ];
                $rangePrior = [
                    'from' => $datePrior->modify('first day of - 3 month')->format('Y-m-d'),
                    'till' => $datePrior->modify('last day of + 2 month')->format('Y-m-d'),
                ];
                $step = self::PERIOD_STEP_MONTH;
                break;
        }

        $this->_periodParams = [
            'id' => $periodId,
            'title' => $title,
            'range' => $range,
            'rangePrior' => $rangePrior,
            'step' => $step,
        ];
    }

    public function getPeriodParams()
    {
        return $this->_periodParams;
    }

    public function getSubPeriodColumns()
    {
        $from = $this->periodParams['range']['from'];
        $till = $this->periodParams['range']['till'];
        $step = $this->periodParams['step'];

        return $this->getPeriodColumnsByStep($from, $till, $step);
    }

    public function getPeriodPriorLastColumn()
    {
        $from = $this->periodParams['rangePrior']['from'];
        $till = $this->periodParams['rangePrior']['till'];
        $step = $this->periodParams['step'];

        $columns = $this->getPeriodColumnsByStep($from, $till, $step);

        return end($columns);
    }

    public function getPeriodColumnsByStep($from, $till, $step)
    {
        $columns = [];
        $date = new DateTime($from);
        $dateTill = new DateTime($till);
        $modify = $step == self::PERIOD_STEP_MONTH ? '+1 month' : '+3 month';
        $index = 0;

        for ($date; $date < $dateTill; $date->modify($modify)) {
            $monthNumber = $date->format('n');
            $columns[] = [
                'label' => (
                    $step == self::PERIOD_STEP_MONTH ?
                        mb_convert_case(Month::$monthFullRU[$monthNumber], MB_CASE_TITLE) :
                        ceil($monthNumber / 3) . '-й квартал'
                    ) . ', ' . $date->format('Y') . ' г.',

                'attribute' => $date->format('Y') . (
                    $step == self::PERIOD_STEP_MONTH ? $monthNumber : ceil($monthNumber / 3)
                    ),
                'type' => 'amount',
            ];
        }

        return $columns;
    }

    public function getTableColumns()
    {
        $expensesColumn = [
            [
                'label' => 'Статьи затрат',
                'attribute' => 'expenses',
                'type' => 'name',
            ],
        ];
        $summaryColumn = [
            [
                'label' => $this->periodParams['title'],
                'attribute' => 'summary',
                'type' => 'amount',
            ],
        ];
        $columnArray = array_merge($expensesColumn, $this->subPeriodColumns, $summaryColumn);
        $compareArrtibute = 'summaryPriorStep';
        foreach ($columnArray as $key => $column) {
            if ($column['type'] == 'amount') {
                if ($column['attribute'] == 'summary') {
                    $columnArray[$key]['compareArrtibute'] = 'summaryPrior';
                } else {
                    $columnArray[$key]['compareArrtibute'] = $compareArrtibute;
                    $compareArrtibute = $column['attribute'];
                }
            }
        }

        return $columnArray;
    }

    public function getExpensesItems()
    {
        return InvoiceExpenditureItem::find()
                ->select(['name', 'id'])
                ->andWhere([
                    'or',
                    ['company_id' => null],
                    ['company_id' => $this->companyId],
                ])
                ->andWhere(['not', ['id' => InvoiceExpenditureItem::ITEM_OWN_FOUNDS]])
                ->orderBy(['sort' => SORT_ASC, 'name' => SORT_ASC])
                ->indexBy('id')
                ->column()
            + [0 => 'Без статьи затрат'];
    }

    public function getPeriodItems()
    {
        $periodItems = [];
        foreach (self::$periodArray as $id => $name) {
            $periodItems[] = [
                'label' => $name,
                'url' => ['/reports/expenses-report/payments', 'period' => $id],
                'options' => ($this->period == $id) ? ['class' => 'active'] : [],
                'linkOptions' => ['class' => 'expenses-pjax-link']
            ];
        }

        return $periodItems;
    }

    public function getHighchartsOptions()
    {
        $totalRow = $this->_dataArray['totalRow'];
        $dataChartArray = $this->_dataArray['dataChartArray'];

        $categories = [];
        $otherData = [];
        foreach ($this->subPeriodColumns as $key => $column) {
            $otherData[$column['attribute']] = 0;
            $categories[] = [$column['label']];
        }
        $series = [];

        foreach ($dataChartArray as $id => $item) {
            $data = [];
            unset($item['sum']);
            foreach ($item as $key => $value) {
                $other = ($totalRow[$key] && ($value / $totalRow[$key] * 100 >= 5)) ? false : true;
                $value /= 100;
                if ($other) {
                    $otherData[$key] += $value;
                } else {
                }
                $data[] = $other ? null : $value;
            }
            if (array_sum($data)) {
                $series[$id] = [
                    'name' => $id == 0 ? 'ЗП и Налоги' : $this->expensesItems[$id],
                    'data' => $data,
                ];
                if ($id === 0) {
                    $series[$id]['color'] = 'rgb(255,238,88)';
                }
                if ($id == 2) {
                    $series[$id]['color'] = 'rgb(92,107,192)';
                }
            }
        }
        if (array_sum($otherData)) {
            $data = [];
            foreach ($otherData as $key => $value) {
                $data[] = $value ? $value : null;
            }
            $series[-1] = [
                'name' => 'Прочее',
                'data' => $data,
            ];
        }

        // move to end by key 0 and 2
        if (isset($series[0])) {
            $item = $series[0];
            unset($series[0]);
            $series[0] = $item;
        }
        if (isset($series[2])) {
            $item = $series[2];
            unset($series[2]);
            $series[2] = $item;
        }

        return [
            'chart' => [
                'type' => 'column',
            ],
            'title' => [
                'text' => '',
            ],
            'lang' => [
                'printChart' => 'На печать',
                'downloadPNG' => 'Скачать PNG',
                'downloadJPEG' => 'Скачать JPEG',
                'downloadPDF' => 'Скачать PDF',
                'downloadSVG' => 'Скачать SVG',
                'contextButtonTitle' => 'Меню',
            ],
            'xAxis' => [
                'categories' => $categories,
            ],
            'yAxis' => [
                'min' => 0,
                'title' => ['text' => 'Сумма'],
                'stackLabels' => [
                    'allowOverlap' => true,
                    'enabled' => true,
                    'style' => [
                        'fontWeight' => '500',
                        'color' => '#333333',
                        'fontSize' => '20px;'
                    ],
                    'formatter' => new JsExpression('function() {
                        return Highcharts.numberFormat(this.total, 2, ",", " ");
                    }'),
                ],
            ],
            'series' => array_values($series),
            'plotOptions' => [
                'column' => [
                    'stacking' => 'normal',
                    'borderWidth' => 0,
                    'dataLabels' => [
                        'enabled' => true,
                        'formatter' => new JsExpression('function() {
                            return Highcharts.numberFormat(this.percentage, 2, ",", " ") + " %";
                        }'),
                        'style' => [
                            'color' => '#333333',
                            'textOutline' => null
                        ],
                    ],
                    'tooltip' => [
                        'valueDecimals' => 2,
                    ],
                ],
            ],
        ];
    }
}
