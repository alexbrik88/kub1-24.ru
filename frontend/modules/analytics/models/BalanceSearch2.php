<?php

namespace frontend\modules\analytics\models;

use frontend\modules\amocrm\modules\iframe\helpers\Url;
use frontend\modules\analytics\models\balance\BalanceInitial;
use yii\helpers\ArrayHelper;
use frontend\modules\analytics\models\balance\BalanceRow;
use frontend\modules\analytics\models\balance\BalanceStructure;
use frontend\modules\analytics\models\balance\BalanceRowFactory;
use frontend\modules\analytics\models\balance\BalanceSimpleGettersTrait;

/**
 * Class BalanceSearch
 * @package frontend\modules\analytics\models
 *
 */
class BalanceSearch2 extends OLAP {

    use BalanceSimpleGettersTrait;

    private array $_resultBalanceRows = [];

    public function search(): array
    {
        if (empty($this->_resultBalanceRows)) {
            $this->_resultBalanceRows = $this->handleItems();
        }
        return $this->_resultBalanceRows;
    }

    public function searchInitial(): array
    {
        $initialItems = $this->buildInitialItems();
        $this->calcRowsFromInsideOutInitial($initialItems);
        $this->calcSummaryRowInitial($initialItems);

        return $initialItems;
    }

    public function handleItems(): array
    {
        $balanceItems = $this->buildItems();
        $this->calcRowsFromInsideOut($balanceItems);
        $this->calcSummaryRow($balanceItems);

        return $balanceItems;
    }

    private function buildItems($itemsArray = null):array
    {
        // first run
        if ($itemsArray === null)
            $itemsArray = BalanceStructure::buildItems();

        $result = [];
        foreach ($itemsArray as $item) {
            $getter = ArrayHelper::remove($item, 'getter', '');
            $items = ArrayHelper::remove($item, 'items', []);
            $itemsGetter = ArrayHelper::remove($item, 'itemsGetter', []);
            $getterClass = BalanceRowFactory::build($getter, $this->_multiCompanyIds);
            $item['data'] = $getterClass->getYearData($this->year);

            foreach (BalanceRow::EMPTY_YEAR as $key => $zeroes) {
                $amount = $item['data'][$key] ?? 0;
                // quarter current year
                if ($this->year == date('Y') && $key > date('n') && ceil($key / 3) == ceil(date('n') / 3)) {
                    $quarterAmount = $item['data'][date('n')] ?? 0;
                } else {
                    $quarterAmount = $amount;
                }
                if (is_int($key / 3)) {
                    $item['data']['quarter-' . ($key / 3)] = $quarterAmount;
                }
            }

            if ($items) {
                $item['items'] = $this->buildItems($items);
            } elseif ($itemsGetter) {
                $item['items'] = (new $itemsGetter)
                    ->setCompany($this->_multiCompanyIds)
                    ->setYear($this->year)
                    ->load()
                    ->getItems();
            }

            $result[] = $item;
        }

        return $result;
    }

    private function calcRowsFromInsideOut(array &$balanceItems, array &$parentData = null)
    {
        foreach ($balanceItems as &$item) {

            if (isset($item['items'])) {
                $this->calcRowsFromInsideOut($item['items'], $item['data']);
            }

            if (!isset($item['calculated'])) {
                if (isset($parentData)) {
                    foreach ((BalanceRow::EMPTY_YEAR + BalanceRow::EMPTY_QUARTERS) as $m => $z) {
                        $parentData[$m] = ($parentData[$m] ?? 0) + ($item['data'][$m] ?? 0);
                    }
                    $item['calculated'] = 1;
                }
            }
        }
    }

    private function calcSummaryRow(array &$balanceItems)
    {
        $actives = $this->getAssetsRow($balanceItems);
        $passives = $this->getLiabilitiesRow($balanceItems);
        $summaryRow = &$balanceItems[array_key_last($balanceItems)];
        foreach ((BalanceRow::EMPTY_YEAR + BalanceRow::EMPTY_QUARTERS) as $m => $z) {
            $summaryRow['data'][$m] = ($actives['data'][$m] ?? 0) - ($passives['data'][$m] ?? 0);
        }
    }

    // initial
    private function buildInitialItems($itemsArray = null):array
    {
        // only current company
        $primaryCompanyId = \Yii::$app->user->identity->company->id;

        $initialDate = BalanceInitial::getInitialDate();
        
        // first run
        if ($itemsArray === null)
            $itemsArray = BalanceStructure::buildItems();

        $result = [];
        foreach ($itemsArray as $item) {
            $getter = ArrayHelper::remove($item, 'getter', '');
            $items = ArrayHelper::remove($item, 'items', []);
            $itemsGetter = ArrayHelper::remove($item, 'itemsGetter', []);
            // todo: load initial values
            $getterClass = BalanceRowFactory::build($getter, [$primaryCompanyId]);
            $item['dataInitial'] = $getterClass->getInitialData($initialDate);

            if ($items) {
                $item['items'] = $this->buildInitialItems($items);
            } elseif ($itemsGetter) {
                $item['items'] = (new $itemsGetter)
                    ->setCompany([$primaryCompanyId])
                    ->setYear($this->year)
                    ->loadInitial() // load initial
                    ->getItems();
            }

            $result[] = $item;
        }

        return $result;
    }

    private function calcRowsFromInsideOutInitial(array &$initialItems, array &$parentData = null)
    {
        foreach ($initialItems as &$item) {
            if (isset($item['items'])) {
                $this->calcRowsFromInsideOutInitial($item['items'], $item['dataInitial']);
            }
            if (!isset($item['calculated'])) {
                if (isset($parentData)) {
                    $parentData['amount'] =
                        (int)($parentData['amount'] ?? 0) +
                        (int)($item['dataInitial']['amount'] ?? 0);
                    $item['calculated'] = 1;
                }
            }
        }
    }

    private function calcSummaryRowInitial(array &$initialItems)
    {
        $actives = $this->getAssetsRow($initialItems);
        $passives = $this->getLiabilitiesRow($initialItems);
        $summaryRow = &$initialItems[array_key_last($initialItems)];

        $summaryRow['dataInitial']['amount'] =
            ($actives['dataInitial']['amount'] ?? 0) -
            ($passives['dataInitial']['amount'] ?? 0);
    }
}