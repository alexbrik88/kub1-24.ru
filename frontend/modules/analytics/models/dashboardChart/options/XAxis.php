<?php

namespace frontend\modules\analytics\models\dashboardChart\options;

use common\components\helpers\Month;
use yii\web\JsExpression;

trait XAxis {

    public static function getXAxisHours($left, $right, $offset)
    {
        $datePeriods = self::__getFromCurrentHoursPeriods($left, $right, 2, $offset);

        if ($offset + $left < 0)
            $currDayPos = -1;
        elseif ($offset - $right > 0)
            $currDayPos = 9999;
        else
            $currDayPos = 'Infinity';

        $x = $labelsX = [];

        foreach ($datePeriods as $i => $date) {
            $day = date('d', $date['from']);
            $hour = date('H', $date['from']);
            $x[] = $hour . ':00';
            $labelsX[] = $hour . ':00';

            if (date('H') == $hour && date('d') == $day)
                $currDayPos = $i;
        }

        return [
            'x' => $x,
            'labelsX' => $labelsX,
            'currDayPos' => $currDayPos,
            'datePeriods' => $datePeriods
        ];
    }

    public static function getXAxisDays($left, $right, $offset)
    {
        $datePeriods = self::__getFromCurrentDaysPeriods($left, $right, $offset);

        if ($offset + $left < 0)
            $currDayPos = -1;
        elseif ($offset - $right > 0)
            $currDayPos = 9999;
        else
            $currDayPos = 'Infinity';

        $x = $labelsX = $freeDays = [];

        foreach ($datePeriods as $i => $date) {
            $dateArr = explode('-', $date['from']);
            $day = $dateArr[2];
            $x[] = $day;
            $freeDays[] = (in_array(date('w', strtotime($date['from'])), [0,6]));
            $labelsX[] = $day . ' ' . \php_rutils\RUtils::dt()->ruStrFTime(['format' => 'F', 'monthInflected' => true, 'date' => $date['from']]);

            if ($date['from'] == date('Y-m-d'))
                $currDayPos = $i;
        }

        return [
            'x' => $x,
            'labelsX' => $labelsX,
            'freeDays' => $freeDays,
            'currDayPos' => $currDayPos,
            'datePeriods' => $datePeriods
        ];
    }

    public static function getXAxisDaysWithWrapPoints($left, $right, $offset, $offsetYear = "0")
    {
        $datePeriods = self::__getFromCurrentDaysPeriods($left, $right, $offset, $offsetYear);

        if ($offset + $left < 0)
            $currDayPos = -1;
        elseif ($offset - $right > 0)
            $currDayPos = 9999;
        else
            $currDayPos = $left;

        $daysPeriods = $labelsX = $freeDays = $wrapPointPos = [];
        $monthPrev = $datePeriods[0];

        foreach ($datePeriods as $i => $date) {
            $dateArr = explode('-', $date['from']);
            $year  = (int)$dateArr[0];
            $month = (int)$dateArr[1];
            $day   = (int)$dateArr[2];

            if ($monthPrev != $month && ($i > 1 && $i < ($left + $right))) {
                $wrapPointPos[$i] = [
                    'prev' => Month::$monthFullRU[($month > 1) ? $month - 1 : 12],
                    'next' => Month::$monthFullRU[$month]
                ];
                // emulate align right
                switch ($month) {
                    case 1: $wrapPointPos[$i]['leftMargin'] = '-350%'; break;
                    case 2: $wrapPointPos[$i]['leftMargin'] = '-300%'; break;
                    case 3: $wrapPointPos[$i]['leftMargin'] = '-375%'; break;
                    case 4: $wrapPointPos[$i]['leftMargin'] = '-180%'; break; // march
                    case 5: $wrapPointPos[$i]['leftMargin'] = '-285%'; break;
                    case 6: $wrapPointPos[$i]['leftMargin'] = '-125%'; break;
                    case 7: $wrapPointPos[$i]['leftMargin'] = '-190%'; break;
                    case 8: $wrapPointPos[$i]['leftMargin'] = '-190%'; break;
                    case 9: $wrapPointPos[$i]['leftMargin'] = '-250%'; break;
                    case 10: $wrapPointPos[$i]['leftMargin'] = '-420%'; break;
                    case 11: $wrapPointPos[$i]['leftMargin'] = '-350%'; break;
                    case 12: $wrapPointPos[$i]['leftMargin'] = '-290%'; break;
                }
            }

            $monthPrev = $month;

            $daysPeriods[] = $day;
            $freeDays[] = (in_array(date('w', strtotime($date['from'])), [0, 6]));
            $labelsX[] = $day . ' ' . \php_rutils\RUtils::dt()->ruStrFTime([
                'format' => 'F',
                'monthInflected' => true,
                'date' => $date['from'],
            ]);
        }

        return [
            'x' => $daysPeriods,
            'labelsX' => $labelsX,
            'freeDays' => $freeDays,
            'currDayPos' => $currDayPos,
            'datePeriods' => $datePeriods,
            'wrapPointPos' => $wrapPointPos,
        ];
    }

    public static function getMonthYearName($year, $month)
    {
        return self::mb_ucfirst(\php_rutils\RUtils::dt()->ruStrFTime(['format' => 'F', 'monthInflected' => false, 'date' => "{$year}-{$month}-01"])) . ", {$year}";
    }

    private static function __getFromCurrentDaysPeriods($left, $right, $offsetDay = "0", $offsetYear = "0")
    {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} days")->modify("{$offsetDay} days");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr->format('Y-m-d'),
                'to' => $curr->format('Y-m-d'),
            ];
            $curr = $curr->modify("+1 days");
        }

        return $ret;
    }

    private static function __getFromCurrentHoursPeriods($left, $right, $hoursInterval = 4, $offset = 0)
    {
        $ret = [];
        $start = strtotime(date('d.m.Y H:00:00'));
        $curr = $start - (3600 * $left * $hoursInterval) + (3600 * $offset * $hoursInterval);
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr,
                'to' => $curr + (3600 * $hoursInterval) - 1,
            ];
            $curr = $curr + (3600 * $hoursInterval);
        }

        return $ret;
    }

    private static function mb_ucfirst($string, $encoding = 'utf-8')
    {
        $firstChar = mb_substr($string, 0, 1, $encoding);
        $then = mb_substr($string, 1, null, $encoding);
        return mb_strtoupper($firstChar, $encoding) . $then;
    }
}