<?php

namespace frontend\modules\analytics\models\dashboardChart\options;

use yii\web\JsExpression;

trait TooltipPositioner {

    public static function getChartTopCreditorsTooltipPositioner()
    {
        return new JsExpression('function (boxWidth, boxHeight, point) { return {x:point.plotX * 0.1 - 10,y:point.plotY + 15}; }');
    }

    public static function getInvertedChartTooltipPositioner()
    {
        return new JsExpression('function (boxWidth, boxHeight, point) { return {x:point.plotX * 0.5 - 10,y:point.plotY + 42}; }');
    }

    public static function getPositionerChartPlanFactTop($jsModel)
    {
        return new JsExpression("
            function(boxWidth, boxHeight, point) {
                if ({$jsModel}.period === 'months') {
                    return {x: point.plotX, y: point.plotY - boxHeight / 2};
                } else {
                    return {x: point.plotX + 69, y: point.plotY - 45};
                }
            }        
        ");
    }
}