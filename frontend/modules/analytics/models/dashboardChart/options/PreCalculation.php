<?php

namespace frontend\modules\analytics\models\dashboardChart\options;

use common\components\helpers\Month;
use common\components\TextHelper;
use common\models\cash\CashFlowsBase;
use frontend\modules\analytics\models\PaymentCalendarSearch;
use php_rutils\RUtils;
use yii\helpers\ArrayHelper;

trait PreCalculation {

    public static function calcChartColumnHeight($columnsCount, $customHeaderHeight = null)
    {
        $HEADER_HEIGHT = $customHeaderHeight ?: 54;
        $COLUMN_HEIGHT = 30;
        return $HEADER_HEIGHT + max($COLUMN_HEIGHT * $columnsCount, $COLUMN_HEIGHT);
    }

    public static function recalcChartPlanFactSeries(PaymentCalendarSearch $model, $x, $mainData, $prevMainData, $customPeriod = "months")
    {
        $y['incomeFlowsFactPlan'] = &$mainData['incomeFlowsFact'];
        $y['outcomeFlowsFactPlan'] = &$mainData['outcomeFlowsFact'];
        $y['incomeFlowsPlan'] = &$mainData['incomeFlowsPlan'];
        $y['outcomeFlowsPlan'] = &$mainData['outcomeFlowsPlan'];
        $y['balanceFact'] = &$mainData['balanceFact'];
        $y['balanceFactPrevYear'] = &$prevMainData['balanceFact'];
        $y['balancePlanCurrMonth'] = &$mainData['balancePlanCurrMonth'];
        $y['balancePlan'] = [];

        if ($x['currDayPos'] >= 0 && $x['currDayPos'] < 9999) {
            // chart1
            for ($i = $x['currDayPos'] + 1; $i < count($x['datePeriods']); $i++) {
                $y['incomeFlowsFactPlan'][$i] = $y['incomeFlowsPlan'][$i];
                $y['outcomeFlowsFactPlan'][$i] = $y['outcomeFlowsPlan'][$i];
                $y['incomeFlowsPlan'][$i] = null;
                $y['outcomeFlowsPlan'][$i] = null;
            }

            // chart2: connect plan/fact edges
            for ($i = 0; $i < $x['currDayPos']; $i++) {
                $y['balancePlan'][$i] = null;
            }

            //$y['balancePlan'][$x['currDayPos']] = $y['balanceFact'][$x['currDayPos']];
            // 20-472
            $_todayPlanPart = ($customPeriod === 'days')
                ? ($y['incomeFlowsPlan'][$x['currDayPos']] - $y['outcomeFlowsPlan'][$x['currDayPos']])
                : $y['balancePlanCurrMonth'];
            $y['balancePlan'][$x['currDayPos']] = ($x['currDayPos'] > 0)
                ? $y['balanceFact'][$x['currDayPos'] - 1] + $_todayPlanPart
                : $y['balanceFact'][$x['currDayPos']];

            for ($i = $x['currDayPos'] + 1; $i < count($x['datePeriods']); $i++) {
                $y['balanceFact'][$i] = null;
                $y['balancePlan'][$i] = $y['balancePlan'][$i - 1] + ($y['incomeFlowsFactPlan'][$i] - $y['outcomeFlowsFactPlan'][$i]);
                // curr month
                // if ($i == $x['currDayPos'] + 1) {
                //     $y['balancePlan'][$i] += $y['balancePlanCurrMonth'];
                // }
            }
        } elseif ($x['currDayPos'] == -1) {
            for ($i = 0; $i < count($x['datePeriods']); $i++) {
                $y['balancePlan'][$i] = null;
            }
            $y['balancePlan'][0] = 0; // fix highcharts bug
        }
        elseif ($x['currDayPos'] === 9999) {
            $y['balancePlan'][0] = $model->getChartPlanBalanceFuturePoint($x['datePeriods'][0]['from'], $customPurse = null);
            for ($i = 1; $i < count($x['datePeriods']); $i++) {
                $y['balancePlan'][$i] = $y['balancePlan'][$i - 1] + ($y['incomeFlowsPlan'][$i] - $y['outcomeFlowsPlan'][$i]);
            }
            for ($i = 0; $i < count($x['datePeriods']); $i++) {
                $y['balanceFact'][$i] = null;
                $y['incomeFlowsFactPlan'][$i] = $y['incomeFlowsPlan'][$i];
                $y['outcomeFlowsFactPlan'][$i] = $y['outcomeFlowsPlan'][$i];
            }
            $y['balanceFact'][$i] = 0; // fix highcharts bug
            $y['incomeFlowsPlan'] = $y['outcomeFlowsPlan'] = [];
        }

        return $y;
    }

    public static function getChartPlanFactTooltipData(PaymentCalendarSearch $model, $xAxis, $inTooltipMaxRows = 5)
    {
        $tooltip['arrIncome'] = [];
        $tooltip['arrOutcome'] = [];
        $tooltip['arrIncomeTotal'] = [];
        $tooltip['arrOutcomeTotal'] = [];
        $tooltip['arrDate'] = [];

        $tooltipDateFrom = $xAxis['datePeriods'][0]['from'];
        $tooltipDateTo = $xAxis['datePeriods'][count($xAxis['datePeriods']) - 1]['to'];
        $incomeTooltipData = $model->getTooltipStructureByContractors($tooltipDateFrom, $tooltipDateTo, $customPurse = null, CashFlowsBase::FLOW_TYPE_INCOME);
        $outcomeTooltipData = $model->getTooltipStructureByContractors($tooltipDateFrom, $tooltipDateTo, $customPurse = null, CashFlowsBase::FLOW_TYPE_EXPENSE);

        foreach ($xAxis['datePeriods'] as $pos => $date) {

            $amountKey = (($pos > $xAxis['currDayPos'] && $xAxis['currDayPos'] != -1) || $xAxis['currDayPos'] == 9999) ? 'amountPlan' : 'amountFact';
            $countKey = (($pos > $xAxis['currDayPos'] && $xAxis['currDayPos'] != -1) || $xAxis['currDayPos'] == 9999) ? 'countPlan' : 'countFact';

            $tooltip['arrIncome'][$pos] = null;
            $tooltip['arrOutcome'][$pos] = null;
            $tooltip['arrIncomeTotal'][$pos] = null;
            $tooltip['arrOutcomeTotal'][$pos] = null;
            $tooltip['arrDate'][$pos] = null;

            if ($income = ArrayHelper::getValue($incomeTooltipData, $date['from'])) {
                $maxAmount = max(array_column($income, $amountKey)) ?: 9E9;
                $row = 0;
                foreach ($income as $i) {
                    if ($i[$amountKey] == 0 || $inTooltipMaxRows < ++$row) continue;
                    $tooltip['arrIncome'][$pos][] = [
                        'name' => $i['name'],
                        'percent' => round($i[$amountKey] / $maxAmount * 100),
                        'sum' => $i[$amountKey]
                    ];
                }
                $total = array_sum(array_column($income, $countKey));
                $tooltip['arrIncomeTotal'][$pos] = $total . ' ' . RUtils::numeral()->choosePlural($total, ['платеж', 'платежа', 'платежей']);
            }

            if ($outcome = ArrayHelper::getValue($outcomeTooltipData, $date['from'])) {
                $maxAmount = max(array_column($outcome, $amountKey));
                $row = 0;
                foreach ($outcome as $o) {
                    if ($o[$amountKey] == 0 || $inTooltipMaxRows < ++$row) continue;
                    $tooltip['arrOutcome'][$pos][] = [
                        'name' => $o['name'],
                        'percent' => round($o[$amountKey] / $maxAmount * 100),
                        'sum' => $o[$amountKey]
                    ];
                }
                $total = array_sum(array_column($outcome, $countKey));
                $tooltip['arrOutcomeTotal'][$pos] = $total . ' ' . RUtils::numeral()->choosePlural($total, ['платеж', 'платежа', 'платежей']);
            }

            $dateArr = explode('-', $date['from']);
            //$year = $dateArr[0];
            //$month = $dateArr[1];
            $day = $dateArr[2];
            $tooltip['arrDate'][$pos] = (int)$day . ' ' . mb_strtolower(ArrayHelper::getValue(Month::$monthGenitiveRU, substr($date['from'], 5, 2)));
        }

        return $tooltip;
    }

    public static function getChartTopCreditorsTooltipData($topData)
    {
        // tooltip credit
        $jsTopData = [];
        foreach ($topData as $data) {

            $max_sum = max(1, $data['debt_0_10_sum'], $data['debt_11_30_sum'], $data['debt_31_60_sum'], $data['debt_61_90_sum'], $data['debt_more_90_sum']);

            $jsTopData[] = [
                ['period' => '0-10 дней', 'percent' => $max_sum ? round(100 * $data['debt_0_10_sum'] / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($data['debt_0_10_sum'], 2)],
                ['period' => '11-30 дней', 'percent' => $max_sum ? round(100 * $data['debt_11_30_sum'] / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($data['debt_11_30_sum'], 2)],
                ['period' => '31-60 дней', 'percent' => $max_sum ? round(100 * $data['debt_31_60_sum'] / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($data['debt_31_60_sum'], 2)],
                ['period' => '61-90 дней', 'percent' => $max_sum ? round(100 * $data['debt_61_90_sum'] / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($data['debt_61_90_sum'], 2)],
                ['period' => 'Больше 90 дней', 'percent' => $max_sum ? round(100 * $data['debt_more_90_sum'] / $max_sum) : 0, 'sum' => TextHelper::invoiceMoneyFormat($data['debt_more_90_sum'], 2)],
            ];
        }

        return json_encode($jsTopData);
    }
}