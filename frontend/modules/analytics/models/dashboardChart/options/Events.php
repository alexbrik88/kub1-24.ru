<?php

namespace frontend\modules\analytics\models\dashboardChart\options;

use yii\web\JsExpression;

trait Events {

    public static function getEvents($chartID, $eventsArr, $params = [])
    {
        $jsEvents = 0;
        $jsEventsCode = "const chart = $('#{$chartID}').highcharts();";

        foreach ($eventsArr as $eventName) {

            switch ($eventName) {
                case 'alignRightColumnDataLabels':
                    $jsEventsCode .= self::alignRightColumnDataLabels($params) . "\n";
                    $jsEvents++;
                    break;
                case 'redrawPlanMonths':
                    $jsEventsCode .= self::redrawPlanMonths($chartID) . "\n";
                    $jsEvents++;
                    break;
            }
        }

        return ($jsEvents) ?
            new JsExpression("function() { {$jsEventsCode} }") : null;
    }

    public static function alignRightColumnDataLabels($params)
    {
        $offset = $params['offset'] ?? 0;
        return "    
        $.each(chart.series[0].data,function(i,data){
            var offset = {$offset};
            var left = chart.plotWidth - data.dataLabel.width + offset;
            data.dataLabel.attr({
                x: left
            });
        });";
    }

    public static function redrawPlanMonths($chartID)
    {
        $jsModel = self::getJsModel($chartID);

        return "
        
            const color1 = {$jsModel}.color1 || '#000';
            const color2 = {$jsModel}.color2 || '#000';
            
            let chartToLoad = window.setInterval(function () {

                if (typeof(chart) !== 'undefined') {
 
                    const custom_pattern = function (color) {
                        return {
                            pattern: {
                                path: 'M 0 0 L 10 10 M 9 -1 L 11 1 M -1 9 L 1 11',
                                width: 10,
                                height: 10,
                                color: color
                            }
                        }
                    };
                    if (typeof(chart) !== 'undefined') {

                        let pos = {$jsModel}.currDayPos;
                        if (pos == 9999)
                            pos = 0; // all plan
                        if (pos == -1)
                            return; // all fact
                    
                        for (let i = (1+pos); i < {$jsModel}.labelsX.length; i++) {
                            chart.series[0].points[i].color = custom_pattern(color1);
                            chart.series[1].points[i].color = custom_pattern(color2);
                        }
                        chart.series[0].redraw();
                        chart.series[1].redraw();
                    }
 
                    window.clearInterval(chartToLoad);
                }
            }, 100);        
        ";
    }
}