<?php

namespace frontend\modules\analytics\models\dashboardChart\options;

trait Serie {

    public static function getSerieState($color)
    {
        return [
            'hover' => [
                'color' => [
                    'pattern' => [
                        'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                        'color' => $color,
                        'width' => 5,
                        'height' => 5
                    ]
                ]
            ],
        ];
    }
}