<?php

namespace frontend\modules\analytics\models\dashboardChart\options;

use yii\web\JsExpression;

trait TooltipFormatter {

    public static function getTooltipSimple($opt = self::TOOLTIP_RUB)
    {
        $units = $opt['units'] ?? '₽';
        $precision = $opt['precision'] ?? 2;

        return new JsExpression("
            function(args) {

                const index = this.point.index;
                const serie_index = this.series.index;
          
                return ('<table class=\"indicators\">' +
                        '<tr>' +
                            '<td class=\"gray-text\">' + args.chart.series[serie_index].name + ': ' + '</td>' +
                            '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[serie_index].data[index].y, {$precision}, ',', ' ') + ' {$units}</td>' +
                        '</tr>' +
                    '</table>');
            }
        ");
    }

    public static function getTooltipWithTitle($opt = self::TOOLTIP_RUB)
    {
        $units = $opt['units'] ?? '₽';
        $precision = $opt['precision'] ?? 2;
        $total = $opt['total'] ?? null;

        return new JsExpression("
            function(args) {
                return ('<table class=\"indicators\">' +
                        '<span class=\"title\">' + this.point.category + '</span>' + 
                        '<tr>' +
                            '<td class=\"gray-text\">' + this.series.name + ': ' + '</td>' +
                            '<td class=\"gray-text-b\">' + Highcharts.numberFormat(this.y, {$precision}, ',', ' ') + ' {$units}</td>' +
                        '</tr>' + ".
                        ($total === null ? "" :
                        "'<tr>' +
                            '<td class=\"gray-text\">Доля: ' + '</td>' +
                            '<td class=\"gray-text-b\">' + Highcharts.numberFormat(100 * this.y / ({$total} || 9E9), 2, ',', ' ') + ' %</td>' +
                        '</tr>' + ")."
                    '</table>');
                }
        ");
    }

    public static function getTooltipDays($jsModel, $opt = self::TOOLTIP_RUB)
    {
        $units = $opt['units'] ?? '₽';
        $precision = $opt['precision'] ?? 2;

        return new JsExpression("
            function(args) {
          
                const index = this.point.index;
                const serie_index = this.series.index;
          
                return '<span class=\"title\">' + {$jsModel}.labelsX[index] + '</span>' +
                    ('<table class=\"indicators\">' +
                        '<tr>' +
                            '<td class=\"gray-text\">' + args.chart.series[serie_index].name + ': ' + '</td>' +
                            '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[serie_index].data[index].y, {$precision}, ',', ' ') + ' {$units}</td>' +
                        '</tr>' +
                    '</table>');
            }
        ");
    }

    public static function getTooltipPie($opt = self::TOOLTIP_RUB)
    {
        $units = $opt['units'] ?? '₽';
        $precision = $opt['precision'] ?? 2;

        return new JsExpression("
            function(args) {

                const index = this.point.index;
                const serie_index = this.series.index;
          
                return ('<table class=\"indicators\">' +
                        '<tr>' +
                            '<td class=\"gray-text\">' + this.point.name + ': ' + '</td>' +
                            '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[serie_index].data[index].y, {$precision}, ',', ' ') + ' {$units}</td>' +
                        '</tr>' +
                    '</table>');
            }
        ");
    }

    public static function getTooltipChartPlanFactTop($jsModel)
    {
        return new JsExpression("
        
            function(args) {

            let idx = this.point.index;
            let isIncome = (this.series.index === 0 || this.series.index === 2);
            let isPlanPeriod = (idx > {$jsModel}.currDayPos && {$jsModel}.currDayPos != -1) || {$jsModel}.currDayPos == 9999;

            if ({$jsModel}.period === 'months') {

                if (isPlanPeriod) {
                    return '<span class=\"title\">' + {$jsModel}.labelsX[this.point.index] + '</span>' +
                        '<table class=\"indicators\">' +
                        '<tr>' + '<td class=\"gray-text\">' + args.chart.series[2].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[idx].y, 2, ',', ' ') + ' ₽</td></tr>' +
                        '<tr>' + '<td class=\"gray-text\">' + args.chart.series[3].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[1].data[idx].y, 2, ',', ' ') + ' ₽</td></tr>' +
                        '</table>';
                }

                return '<span class=\"title\">' + {$jsModel}.labelsX[this.point.index] + '</span>' +
                    ((this.series.index === 0 || this.series.index === 2) ?
                        ('<table class=\"indicators\">' +
                            '<tr>' + '<td class=\"gray-text\">' + args.chart.series[0].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[idx].y, 2, ',', ' ') + ' ₽</td></tr>' +
                            '<tr>' + '<td class=\"gray-text\">' + args.chart.series[2].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[2].data[idx].y, 2, ',', ' ') + ' ₽</td></tr>' +
                            '</table>') :
                        ('<table class=\"indicators\">' +
                            '<tr>' + '<td class=\"gray-text\">' + args.chart.series[1].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[1].data[idx].y, 2, ',', ' ') + ' ₽</td></tr>' +
                            '<tr>' + '<td class=\"gray-text\">' + args.chart.series[3].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[3].data[idx].y, 2, ',', ' ') + ' ₽</td></tr>' +
                            '</table>')
                    );

            } else {


                //if((this.series.index === 0 || this.series.index === 2) && this.series.chart.series[0].points[idx].y === 0)
                //    return false;
                //if ((this.series.index === 1 || this.series.index === 3) && this.series.chart.series[1].points[idx].y === 0)
                //    return false;

                let arrIncome = {$jsModel}.tooltipData.jsArrIncome;
                let arrOutcome = {$jsModel}.tooltipData.jsArrOutcome;
                let arrIncomeTotal = {$jsModel}.tooltipData.jsArrIncomeTotal;
                let arrOutcomeTotal = {$jsModel}.tooltipData.jsArrOutcomeTotal;
                let arrDate = {$jsModel}.tooltipData.jsArrDate;
                let htmlHeaderPlan = {$jsModel}.tooltipTemplate.htmlHeaderPlan;
                let htmlHeaderFact = {$jsModel}.tooltipTemplate.htmlHeaderFact;
                let htmlData = {$jsModel}.tooltipTemplate.htmlData;
                let htmlFooter = {$jsModel}.tooltipTemplate.htmlFooter;

                let arr = (isIncome) ? arrIncome : arrOutcome;
                let arrTotals = (isIncome) ? arrIncomeTotal : arrOutcomeTotal;
                let chart = this.series.chart;
                let planY;
                let factY;

                if (isPlanPeriod) {
                    factY = 0;
                    planY = (isIncome) ? chart.series[0].points[idx].y : chart.series[1].points[idx].y;
                } else {
                    planY = (isIncome) ? chart.series[2].points[idx].y : chart.series[3].points[idx].y;
                    factY = (isIncome) ? chart.series[0].points[idx].y : chart.series[1].points[idx].y;
                }

                let tooltipHtml = (isPlanPeriod) ? htmlHeaderPlan : htmlHeaderFact;

                tooltipHtml = tooltipHtml
                    .replace('{title}', isIncome ? 'Приход за' : 'Расход за')
                    .replace('{point.x}', arrDate[idx])
                    .replace('{plan.y}', Highcharts.numberFormat(planY, 0, ',', ' '))
                    .replace('{fact.y}', Highcharts.numberFormat(factY, 0, ',', ' '));

                if (arr[idx]) {
                    arr[idx].forEach(function(data) {
                        tooltipHtml += htmlData
                            .replace('{name}', data.name)
                            .replace('{percent}', data.percent)
                            .replace('{sum}', Highcharts.numberFormat(data.sum, 0, ',', ' '))
                            .replace('{color_css}', (isIncome ? 'blue' : 'yellow') + (isPlanPeriod ? ' plan' : ''))
                    });
                } else {
                    tooltipHtml += '<tr><td colspan=\"3\"></td></tr>';
                }
                if (arrTotals[idx]) {
                    tooltipHtml += htmlFooter.replace('{total}', arrTotals[idx]);
                }

                return tooltipHtml;
            }
        }        
        
        ");
    }
    
    public static function getTooltipChartPlanFactBottom($jsModel)
    {
        return new JsExpression("
            function(args) {
                let index = this.series.data.indexOf( this.point );
                let series_index = this.series.index;
                let factColor = args.chart.series[2].data[index].y >= 0 ? 'green-text' : 'red-text';
                let factColorB = args.chart.series[2].data[index].y >= 0 ? 'gray-text-b' : 'red-text-b';
                let planColor = args.chart.series[1].data[index].y >= 0 ? 'green-text' : 'red-text';
                let planColorB = args.chart.series[1].data[index].y >= 0 ? 'gray-text-b' : 'red-text-b';

                let toggleFactLines = args.chart.series[0].data[index].y > args.chart.series[2].data[index].y;
                let togglePlanLines = args.chart.series[0].data[index].y > args.chart.series[1].data[index].y;

                let titleLine = '<span class=\"title\">' + {$jsModel}.labelsX[this.point.index] + '</span>';
                let factLine = '<tr>' + '<td class=\"' + factColor + '\">' + args.chart.series[2].name + ': ' + '</td>' + '<td class=\"' + factColorB + '\">' + Highcharts.numberFormat(args.chart.series[2].data[index].y, 2, ',', ' ') + ' ₽</td></tr>';
                let planLine = '<tr>' + '<td class=\"' + planColor + '\">' + args.chart.series[1].name + ': ' + '</td>' + '<td class=\"' + planColorB + '\">' + Highcharts.numberFormat(args.chart.series[1].data[index].y, 2, ',', ' ') + ' ₽</td></tr>';
                let prevYearLine = '<tr>' + '<td class=\"gray-text\">' + args.chart.series[0].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 2, ',', ' ') + ' ₽</td></tr>';

                if (index === {$jsModel}.currDayPos) {
                    if (series_index === 2)
                        return titleLine + '<table class=\"indicators\">' + (toggleFactLines ? (prevYearLine + factLine) : (factLine + prevYearLine)) + '</table>';                                            
                    if (series_index === 1)
                        return titleLine + '<table class=\"indicators\">' + (togglePlanLines ? (prevYearLine + planLine) : (planLine + prevYearLine)) + '</table>';                  
                }

                if ((index > {$jsModel}.currDayPos && {$jsModel}.currDayPos != -1) || {$jsModel}.currDayPos == 9999) {
                    return titleLine + '<table class=\"indicators\">' + (togglePlanLines ? (prevYearLine + planLine) : (planLine + prevYearLine)) + '</table>';
                }

                return titleLine + '<table class=\"indicators\">' + (toggleFactLines ? (prevYearLine + factLine) : (factLine + prevYearLine)) + '</table>';
            }
        ");
    }

    public static function getTooltipChartExpensesByArticles($jsModel)
    {
        return new jsExpression("
            function(args) {

                var index = this.series.data.indexOf( this.point );
                var series_index = this.series.index;
                var totalAmount = ({$jsModel}.expenseTotalAmount > 0) ? {$jsModel}.expenseTotalAmount : 9E9;

                return '<span class=\"title\">' + this.point.category + '</span>' +
                    '<table class=\"indicators\">' +
                        ('<tr>' + '<td class=\"gray-text\">' + args.chart.series[0].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[0].data[index].y, 2, ',', ' ') + ' ₽</td></tr>') +
                        ('<tr>' + '<td class=\"gray-text\">' + args.chart.series[1].name + ': ' + '</td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(args.chart.series[1].data[index].y, 2, ',', ' ') + ' ₽</td></tr>') +
                        ('<tr>' + '<td class=\"gray-text\">Доля в расходах: </td>' + '<td class=\"gray-text-b\">' + Highcharts.numberFormat(100 * args.chart.series[0].data[index].y / totalAmount, 0, ',', ' ') + ' %</td></tr>') +
                    '</table>';
            }
        ");
    }

    public static function getTooltipChartTopCreditors($jsModel, $color)
    {
        $tooltipTemplate = self::getChartTopCreditorsTooltipTemplate($color);
        return new JsExpression('
            function (a) {
                
                 if(1 == this.point.series.index) 
                     return false;
            
                 const idx = this.point.index;
                 const arr = '.$jsModel.'.topData;
                               
                 let tooltipHtml;       
        
                 tooltipHtml = \''.$tooltipTemplate['htmlHeader'].'\'.replace("{serie_name}", this.point.category);
                 if (arr[idx] != undefined) {
                     arr[idx].forEach(function(data) {
                      tooltipHtml += \''.$tooltipTemplate['htmlDataBlue'].'\'
                          .replace("{period}", data.period)
                          .replace("{percent}", data.percent)
                          .replace("{sum}", data.sum);              
                     });
                 } else {
                     tooltipHtml += \'<tr><td colspan="3">Нет данных</td></tr>\';
                 }
                 
                 tooltipHtml += \''.$tooltipTemplate['htmlFooter'].'\';
        
                 return tooltipHtml.replace();
            }');
    }
}