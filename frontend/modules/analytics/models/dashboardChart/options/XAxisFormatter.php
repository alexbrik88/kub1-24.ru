<?php

namespace frontend\modules\analytics\models\dashboardChart\options;

use yii\web\JsExpression;

trait XAxisFormatter {

    public static function getXAxisFormatterHours($jsModel)
    {
        return new JsExpression("
            function() {
                return this.pos == {$jsModel}.currDayPos ?
                    ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                    ('<span class=\"x-axis\">' + this.value + '</span>');
            }");
    }

    public static function getXAxisFormatterDays($jsModel)
    {
        return new JsExpression("
            function() {
                return this.pos == {$jsModel}.currDayPos ?
                    ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                    ({$jsModel}.freeDays[this.pos] ?
                        ('<span class=\"x-axis free-date\">' + this.value + '</span>') :
                        ('<span class=\"x-axis\">' + this.value + '</span>')
                    );
            }");
    }

    public static function getXAxisFormatterDaysWithWrapPoints($jsModel)
    {
        return new JsExpression("
            function() {
                result = (this.pos == {$jsModel}.currDayPos) ?
                    ('<span class=\"x-axis red-date\">' + this.value + '</span>') :
                    ({$jsModel}.freeDays[this.pos] ? ('<span class=\"x-axis free-date\">' + this.value + '</span>') : ('<span class=\"x-axis\">' + this.value + '</span>'));

                if ({$jsModel}.wrapPointPos) {
                    result += {$jsModel}.getWrapPointXLabel(this.pos);
                }

                return result;   
            }     
        ");
    }

    public static function jsGetXAxisWrapPointXLabel($chartID)
    {
        return "
function(x) {
        const chart = $({$chartID}).highcharts();
        const colsCount = chart.xAxis[0].categories.length - 2;
        const colWidth = (chart.xAxis[0].width) / (colsCount);
        let name, left, txtLine, txtLabel;
        let k = (this.period == 'months') ? 0.725 : 0.625;
        
        
        
        if (this.wrapPointPos[x + 1] && (x > 1)) {
            name = this.wrapPointPos[x + 1].prev;
            left = this.wrapPointPos[x + 1].leftMargin;

            txtLine = '<div style=\"position:absolute;top:0px;width:2px;height:50px; left:' + (k * colWidth) + 'px;background-color: #e6e6e6\"></div>';
            txtLabel = '<div style=\"position:absolute; top:20px; left:' + left + '; font-size: 14px; color:#899098; font-style:\'Corpid E3 SCd\', sans-serif;\">' + name + '</div>';

            return txtLine + txtLabel;
        }
        if (this.wrapPointPos[x] && (x < colsCount)) {
            name = this.wrapPointPos[x].next;

            txtLabel = '<div style=\"position:absolute; top:20px; left:0; font-size: 14px; color:#9198a0; font-style:\'Corpid E3 SCd\', sans-serif;\">' + name + '</div>';

            return txtLabel;
        }
        return '';
    }        
        ";
    }
}