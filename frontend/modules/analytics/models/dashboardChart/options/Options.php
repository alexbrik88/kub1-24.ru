<?php

namespace frontend\modules\analytics\models\dashboardChart\options;

use frontend\modules\analytics\models\dashboardChart\DashboardChart;
use yii\web\JsExpression;

trait Options {

    public static function mergeWithDefault($options, $chartType)
    {
        return array_replace_recursive(self::getDefaultOptions(), self::getOptionsByType($chartType), $options);
    }

    public static function getDefaultOptions()
    {
        return [
            //'id' => %ID,
            'scripts' => [
                'themes/grid-light',
                'modules/pattern-fill',
                'highcharts-more'
            ],
            'options' => [
                'chart' => [
                    'type' => 'line',
                    'style' => [
                        'fontFamily' => '"Corpid E3 SCd", sans-serif'
                    ],
                    'events' => [],
                    'marginTop' => '46'

                ],
                //'series' => [%SERIES],
                'title' => [
                    //'text' => %TITLE,
                    'align' => 'left',
                    'floating' => true,
                    'style' => [
                        'font-size' => '15px',
                        'color' => '#9198a0',
                        'text-transform' => 'unset',
                        'font-weight' => 'unset'
                    ],
                    'x' => 0,
                    'y' => 18
                ],
                'legend' => [
                    'layout' => 'horizontal',
                    'align' => 'right',
                    'verticalAlign' => 'top',
                    'backgroundColor' => '#fff',
                    'itemStyle' => [
                        'fontSize' => '12px',
                        'color' => '#9198a0',
                        'fontWeight' => '400'
                    ],
                    'symbolRadius' => 2
                ],
                'xAxis' => [
                    //'categories' => [%CATEGORIES],
                    'labels' => [
                        'useHTML' => true,
                        'autoRotation' => false,
                        'style' => [
                            'color' => '#001424'
                        ]
                        //'formatter' => %FORMATTER,
                    ],
                    'minorGridLineWidth' => 0,
                ],
                'yAxis' => [
                    'min' => 0,
                    'index' => 0,
                    'title' => '',
                    'minorGridLineWidth' => 0,
                    'labels' => [
                        'useHTML' => true,
                        'style' => [
                            'fontWeight' => '300',
                            'fontSize' => '13px',
                            'whiteSpace' => 'nowrap'
                        ]
                    ]
                ],
                'tooltip' => [
                    'useHTML' => true,
                    'shared' => false,
                    'backgroundColor' => "rgba(255,255,255,1)",
                    'borderColor' => '#ddd',
                    'borderWidth' => '1',
                    'borderRadius' => 8,
                    //'formatter' => %FORMATTER
                ],
                'plotOptions' => [
                    'scatter' => [
                        'tooltip' => [
                            'enabled' => false
                        ],
                        'marker' => [
                            'symbol' => 'c-rect',
                            'lineWidth' => 3,
                            'lineColor' => 'rgba(50,50,50,1)',
                            'radius' => 8.5
                        ],
                        'lineWidth' => 0,
                        'states' => [
                            'hover' => [
                                'lineWidthPlus' => 0
                            ]
                        ],
                        'pointPlacement' => 0.0,
                        'stickyTracking' => false,
                    ],
                    'series' => [
                        'states' => [
                            'inactive' => [
                                'opacity' => 1
                            ]
                        ],
                    ],
                ],
                'credits' => false,
                'exporting' => false,
            ],
        ];
    }
    
    public static function getOptionsByType($chartType)
    {
        switch ($chartType) {
            case DashboardChart::CHART_GAUGE:
                return self::__getDefaultOptionsGauge();
            case DashboardChart::CHART_PIE:
                return self::__getDefaultOptionsPie();
            case DashboardChart::CHART_COLUMN:
                return self::__getDefaultOptionsColumn();
            case DashboardChart::CHART_LINE:
            default:
                return [];
        }
    }

    private static function __getDefaultOptionsColumn()
    {
        return [
            'options' => [
                'plotOptions' => [
                    'column' => [
                        'pointWidth' => 18,
                        'grouping' => false,
                        'shadow' => false,
                        'groupPadding' => 0.1,
                        'pointPadding' => 0.14,
                        'borderRadius' => 3,
                    ]
                ]
            ]
        ];
    }


    private static function __getDefaultOptionsGauge()
    {
        return [
            'options' => [
                'chart' => [
                    'type' => 'gauge',
                    'plotBackgroundColor' => null,
                    'plotBackgroundImage' => null,
                    'plotBorderWidth' => 0,
                    'plotShadow' => false
                ],

                'pane' => [
                    'size' => '100%',
                    'startAngle' => -110,
                    'endAngle' => 110,
                    'background' => null
                ],

                'yAxis' => [
                    'min' => 0,
                    'max' => 125,

                    'minorTickInterval' => 'auto',
                    'minorTickWidth' => 0,
                    'minorTickLength' => 10,
                    'minorTickPosition' => 'inside',
                    'minorTickColor' => '#FFF',
                    'tickInterval' => 12.5,
                    'tickWidth' => 1.5,
                    'tickPosition' => 'inside',
                    'tickLength' => 15,
                    'tickColor' => '#FFF',
                    'labels' => [
                        'step' => 2,
                        //'rotation' => 'auto'
                        'distance' => 13,
                        'style' => [
                            'color' => '#9198a0',
                            'font-size' => '14px',
                        ]
                    ],
                    'title' => [
                        //'text' => %TITLE,
                        'style' => [
                            'color' => '#9198a0',
                            'fontSize' => '14px',
                            'textTransform' => 'unset',
                        ],
                        'align' => 'middle',
                        'floating' => true,
                        'x' => 0,
                        'y' => 15

                    ],
                    'plotBands' => [
                        [
                            'from' => 0,
                            'to' => 25,
                            'size' => 50,
                            'color' => '#a80000', // red
                            'thickness' => '35%'
                        ],
                        [
                            'from' => 25,
                            'to' => 50,
                            'color' => '#ee4020', // red
                            'thickness' => '35%'
                        ],
                        [
                            'from' => 50,
                            'to' => 75,
                            'color' => '#eeb620', // yellow
                            'thickness' => '35%'
                        ],
                        [
                            'from' => 75,
                            'to' => 100,
                            'color' => '#2ba12e', // green
                            'thickness' => '35%'
                        ],
                        [
                            'from' => 100,
                            'to' => 125,
                            'color' => '#4bdf4e', // green
                            'thickness' => '35%'
                        ]
                    ]
                ],
                //'series' => [
                //    [
                //        'name' => %NAME,
                //        'data' => [%NUM]
                //    ]
                //],
                'plotOptions' => [
                    'gauge' => [
                        'dataLabels' => [
                            'borderWidth' => 0,
                            'format' => "{y}%",
                            'style' => [
                                'border' => 'unset',
                                'color' => '#001424',
                                'fontSize' => '34px',
                                'fontWeight' => '300',
                                'fontFamily' => '"Corpid E3 SCd", sans-serif'
                            ]
                        ],
                    ]
                ]
            ]
        ];
    }

    private static function __getDefaultOptionsPie()
    {
        return [
            'options' => [
                'chart' => [
                    'type' => 'pie',
                    'plotShadow' => false,
                    'plotBorderWidth' => null,
                    'plotBackgroundColor' => null,
                ],
                'legend' => [
                    'align' => 'center',
                    'verticalAlign' => 'bottom',
                    'x' => 0,
                    'y' => 0
                ],
                'plotOptions' => [
                    'pie' => [
                        'allowPointSelect' => true,
                        'borderWidth' => 8,
                        'size' => '135%',
                        'innerSize' => '30%',
                        'cursor' => 'pointer',
                        'dataLabels' => [
                            'enabled' => true,
                            'distance' => -30,
                            'formatter' => new \yii\web\JsExpression("function() { return Highcharts.numberFormat(this.y, 0, ',', ' ') + '%'; }"),
                            'style' => [
                                'fontSize' => '12px',
                                'fontWeight' => '400',
                                'color' => '#FFF',
                                'fontFamily' => '"Corpid E3 SCd", sans-serif',
                                'textOutline' => false,
                            ]
                        ],
                        'showInLegend' => true
                    ]
                ],
            ]
        ];
    }
}