<?php

namespace frontend\modules\analytics\models\dashboardChart\options;

trait TooltipTemplate
{
    public static function getChartPlanFactTooltipTemplate()
    {
        $template['htmlHeaderFact'] = <<<HTML
    <table class="ht-in-table">
        <tr class="title">
            <th>{title}</th>
            <th></th>
            <th>{point.x}</th>
        </tr>
        <tr>
            <th>План</th>
            <th></th>
            <th>{plan.y} ₽</th>
        </tr>
        <tr>
            <th>Факт</th>
            <th></th>
            <th>{fact.y} ₽</th>
        </tr>
HTML;
        $template['htmlHeaderPlan'] = <<<HTML
    <table class="ht-in-table">
        <tr class="title">
            <th>{title}</th>
            <th></th>
            <th>{point.x}</th>
        </tr>
        <tr>
            <th>План</th>
            <th></th>
            <th>{plan.y} ₽</th>
        </tr>
HTML;
        $template['htmlData'] = <<<HTML
        <tr>
            <td><div class="ht-title">{name}</div></td>
            <td><div class="ht-chart-wrap"><div class="ht-chart {color_css}" style="width: {percent}%"></div></div></td>
            <td><div class="ht-total">{sum} ₽</div></td>
        </tr>
HTML;
        $template['htmlFooter'] = <<<HTML
    </table>
    <table class="ht-in-footer-table">
        <tr>
            <td><strong>Всего {total}</strong></td>
        </tr>
    </table>
HTML;

        $template['htmlHeaderFact'] = str_replace(["\r", "\n", "'"], "", $template['htmlHeaderFact']);
        $template['htmlHeaderPlan'] = str_replace(["\r", "\n", "'"], "", $template['htmlHeaderPlan']);
        $template['htmlData'] = str_replace(["\r", "\n", "'"], "", $template['htmlData']);
        $template['htmlFooter'] = str_replace(["\r", "\n", "'"], "", $template['htmlFooter']);

        return $template;
    }

    public static function getChartTopCreditorsTooltipTemplate($color)
    {
        $template['htmlHeader'] = <<<HTML
    
    <table class="ht-in-table">
        <tr>
            <th colspan="3" class="ht-title text-left black" style="max-width:250px; padding-bottom:2px;">{serie_name}</th>
        </tr>
        <tr>
            <th class="red">Просрочка на</th>
            <th colspan="2"></th>
        </tr>
HTML;
        $template['htmlDataBlue'] = <<<HTML
        <tr>
            <td><div class="ht-title">{period}</div></td>
            <td><div class="ht-chart-wrap"><div class="ht-chart" style="background-color:{$color}; width: {percent}%"></div></div></td>
            <td><div class="ht-total">{sum}</div></td>
        </tr>
HTML;
        $template['htmlFooter'] = <<<HTML
    </table>
HTML;

        $template['htmlHeader'] = str_replace(["\r", "\n", "'"], "", $template['htmlHeader']);
        $template['htmlDataBlue'] = str_replace(["\r", "\n", "'"], "", $template['htmlDataBlue']);
        $template['htmlFooter'] = str_replace(["\r", "\n", "'"], "", $template['htmlFooter']);

        return $template;
    }
}