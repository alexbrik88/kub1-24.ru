<?php

namespace frontend\modules\analytics\models\dashboardChart;

use frontend\modules\analytics\models\dashboardChart\options\Options;
use frontend\modules\analytics\models\dashboardChart\options\Events;
use frontend\modules\analytics\models\dashboardChart\options\Serie;
use frontend\modules\analytics\models\dashboardChart\options\TooltipPositioner;
use frontend\modules\analytics\models\dashboardChart\options\XAxis;
use frontend\modules\analytics\models\dashboardChart\options\XAxisFormatter;
use frontend\modules\analytics\models\dashboardChart\options\TooltipTemplate;
use frontend\modules\analytics\models\dashboardChart\options\TooltipFormatter;
use frontend\modules\analytics\models\dashboardChart\options\PreCalculation;


class DashboardChart {

    use Options;
    use XAxis;
    use Serie;
    use Events;
    use XAxisFormatter;
    use PreCalculation;
    use TooltipFormatter;
    use TooltipTemplate;
    use TooltipPositioner;

    const CHART_LINE = 'line';
    const CHART_COLUMN = 'column';
    const CHART_GAUGE = 'gauge';
    const CHART_PIE = 'pie';

    const TOOLTIP_RUB = ['units' => '₽', 'precision' => 2];
    const TOOLTIP_PERCENT = ['units' => '%', 'precision' => 0];
    const TOOLTIP_NO_UNITS = ['units' => '', 'precision' => 0];

    const EVENT_LOAD = 'load';
    const EVENT_REDRAW = 'redraw';

    public static function getOptions($options, $chartType = self::CHART_LINE)
    {
        return Options::mergeWithDefault($options, $chartType);
    }

    public static function getJsModel($chartId)
    {
        return "window.ChartGroup.{$chartId}";
    }
}