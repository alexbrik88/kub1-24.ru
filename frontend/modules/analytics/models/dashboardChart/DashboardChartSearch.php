<?php

namespace frontend\modules\analytics\models\dashboardChart;

use Yii;
use common\models\cash\CashFlowsBase;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\OrderAct;
use common\models\document\OrderPackingList;
use common\models\document\OrderUpd;
use common\models\document\PackingList;
use common\models\document\status\ActStatus;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\PackingListStatus;
use common\models\document\status\UpdStatus;
use common\models\document\Upd;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\modules\analytics\models\profitAndLoss\BaseOlapTrait;
use frontend\modules\cash\models\CashContractorType;
use yii\db\Expression;
use yii\db\Query;

class DashboardChartSearch {

    const TABLE_OLAP_FLOWS = 'olap_flows';

    private $_company;

    public function __construct($company)
    {
        $this->_company = $company;
    }

    public function getIncomeCashByMonthScalar($year, $month)
    {
        return (new Query)
            ->select(new Expression('SUM(t.amount) AS amount'))
            ->from(['t' => self::TABLE_OLAP_FLOWS])
            ->where(['t.company_id' => $this->_company->id])
            ->andWhere(['t.type' => CashFlowsBase::FLOW_TYPE_INCOME])
            ->andWhere(['=', 'YEAR(t.date)', $year])
            ->andWhere(['=', 'MONTH(t.date)', $month])
            ->andWhere(['not', ['contractor_id' => ['bank', 'order', 'emoney', 'balance']]])
            ->scalar(Yii::$app->db2);
    }

    public function getSalesByMonthScalar($year, $month)
    {
        $year = (int)$year ?: date('Y');
        $month = (int)$month ?: date('m');
        $turnoverQuery = $this->getTurnoverQuery();

        $fact = (new Query)->select(new Expression('SUM(amount) AS amount'))
            ->from($turnoverQuery)
            ->andWhere(new Expression("YEAR(document_date) = {$year}"))
            ->andWhere(new Expression("MONTH(document_date) = {$month}"))
            ->scalar(Yii::$app->db2);

        $plan = (new Query)->select(new Expression('SUM(amount) AS amount'))
            ->from(PlanCashFlows::tableName())
            ->andWhere(new Expression("YEAR(date) = {$year}"))
            ->andWhere(new Expression("MONTH(date) = {$month}"))
            ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
            ->andWhere(['not', ['contractor_id' => [
                CashContractorType::BANK_TEXT,
                CashContractorType::ORDER_TEXT,
                CashContractorType::EMONEY_TEXT,
                CashContractorType::BALANCE_TEXT,
            ]]])
            ->andWhere(['not', ['income_item_id' => BaseOlapTrait::getExceptUndestributedIncomeItems()]])
            ->scalar(Yii::$app->db2);

        return [
            'fact' => $fact,
            'plan' => $plan
        ];
    }

    public function getSalesToday($periods)
    {
        $result = array_fill(0, count($periods), 0);

        $timestampFrom = $periods[0]['from'];
        $timestampTill = $periods[count($periods)-1]['to'];
        $turnoverQuery = $this->getTurnoverQuery();
        $docs = (new Query)->select(new Expression('created_at, SUM(amount) AS amount'))
            ->from($turnoverQuery)
            ->andWhere(['between', 'created_at', $timestampFrom, $timestampTill])
            ->groupBy('created_at')
            ->all(Yii::$app->db2);

        foreach ($docs as $doc) {
            foreach ($periods as $i => $p) {
                if ($doc['created_at'] >= $p['from'] && $doc['created_at'] <= $p['to']) {
                    $result[$i] += $doc['amount'] / 100;
                    continue 2;
                }
            }
        }

        return $result;
    }

    public function getSalesByDays($periods)
    {
        $result = [
            'fact' => array_fill(0, count($periods), 0),
            'plan' => array_fill(0, count($periods), 0),
        ];

        $dateFrom = $periods[0]['from'];
        $dateTill = $periods[count($periods)-1]['to'];
        $turnoverQuery = $this->getTurnoverQuery();

        $fact = (new Query)->select(new Expression('document_date, SUM(amount) AS amount'))
            ->from($turnoverQuery)
            ->andWhere(['between', 'document_date', $dateFrom, $dateTill])
            ->groupBy('document_date')
            ->all(Yii::$app->db2);

        foreach ($fact as $doc) {
            foreach ($periods as $i => $p) {
                if ($doc['document_date'] == $p['from']) {
                    $result['fact'][$i] += $doc['amount'] / 100;
                    continue 2;
                }
            }
        }

        $plan = (new Query)->select(new Expression('date, SUM(amount) AS amount'))
            ->from(PlanCashFlows::tableName())
            ->andWhere(['between', 'date', $dateFrom, $dateTill])
            ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
            ->andWhere(['not', ['contractor_id' => [
                CashContractorType::BANK_TEXT,
                CashContractorType::ORDER_TEXT,
                CashContractorType::EMONEY_TEXT,
                CashContractorType::BALANCE_TEXT,
            ]]])
            ->andWhere(['not', ['income_item_id' => BaseOlapTrait::getExceptUndestributedIncomeItems()]])
            ->groupBy('date')
            ->all(Yii::$app->db2);

        foreach ($plan as $doc) {
            foreach ($periods as $i => $p) {
                if ($doc['date'] == $p['from']) {
                    $result['plan'][$i] += $doc['amount'] / 100;
                    continue 2;
                }
            }
        }

        return $result;
    }

    public function getAverageCheckByDays($periods)
    {
        $result = [
            'fact' => array_fill(0, count($periods), 0),
            'plan' => array_fill(0, count($periods), 0),
        ];

        $dateFrom = $periods[0]['from'];
        $dateTill = $periods[count($periods)-1]['to'];
        $turnoverQuery = $this->getTurnoverQuery();

        $fact = (new Query)->select(new Expression('document_date, document_id, AVG(amount) AS amount'))
            ->from($turnoverQuery)
            ->andWhere(['between', 'document_date', $dateFrom, $dateTill])
            ->groupBy('document_date, document_id')
            ->all(Yii::$app->db2);

        foreach ($fact as $doc) {
            foreach ($periods as $i => $p) {
                if ($doc['document_date'] == $p['from']) {
                    $result['fact'][$i] += $doc['amount'] / 100;
                    continue 2;
                }
            }
        }

        $plan = (new Query)->select(new Expression('date, AVG(amount) AS amount'))
            ->from(PlanCashFlows::tableName())
            ->andWhere(['between', 'date', $dateFrom, $dateTill])
            ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
            ->andWhere(['not', ['contractor_id' => [
                CashContractorType::BANK_TEXT,
                CashContractorType::ORDER_TEXT,
                CashContractorType::EMONEY_TEXT,
                CashContractorType::BALANCE_TEXT,
            ]]])
            ->andWhere(['not', ['income_item_id' => BaseOlapTrait::getExceptUndestributedIncomeItems()]])
            ->groupBy('date')
            ->all(Yii::$app->db2);

        foreach ($plan as $doc) {
            foreach ($periods as $i => $p) {
                if ($doc['date'] == $p['from']) {
                    $result['plan'][$i] += $doc['amount'] / 100;
                    continue 2;
                }
            }
        }

        return $result;
    }

    private function getTurnoverQuery($type = Documents::IO_TYPE_OUT)
    {
        $select = '
            document.document_date,
            document.created_at,
            order.quantity * order_main.selling_price_with_vat AS amount,
            document.id as document_id
        ';

        $query1 = OrderPackingList::find()->alias('order')
            ->select(new Expression($select))
            ->addSelect(['by_document' => new Expression('"packing-list"')])
            ->leftJoin(['document' => PackingList::tableName()], '{{document}}.[[id]] = {{order}}.[[packing_list_id]]')
            ->leftJoin(['invoice' => Invoice::tableName()], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
            ->leftJoin(['order_main' => Order::tableName()], '{{order_main}}.[[id]] = {{order}}.[[order_id]]')
            ->andWhere(['invoice.company_id' => $this->_company->id])
            ->andWhere(['invoice.invoice_status_id' => InvoiceStatus::$validInvoices])
            ->andWhere(['document.type' => $type])
            ->andWhere(['or',
                ['document.status_out_id' => PackingListStatus::$turmoverStatus],
                ['document.status_out_id' => null],
            ]);

        $query2 = OrderAct::find()->alias('order')
            ->select(new Expression($select))
            ->addSelect(['by_document' => new Expression('"act"')])
            ->leftJoin(['document' => Act::tableName()], '{{document}}.[[id]] = {{order}}.[[act_id]]')
            ->leftJoin(['invoice' => Invoice::tableName()], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
            ->leftJoin(['order_main' => Order::tableName()], '{{order_main}}.[[id]] = {{order}}.[[order_id]]')
            ->andWhere(['invoice.company_id' => $this->_company->id])
            ->andWhere(['invoice.invoice_status_id' => InvoiceStatus::$validInvoices])
            ->andWhere(['document.type' => $type])
            ->andWhere(['or',
                ['document.status_out_id' => ActStatus::$turmoverStatus],
                ['document.status_out_id' => null],
            ]);

        $query3 = OrderUpd::find()->alias('order')
            ->select(new Expression($select))
            ->addSelect(['by_document' => new Expression('"upd"')])
            ->leftJoin(['document' => Upd::tableName()], '{{document}}.[[id]] = {{order}}.[[upd_id]]')
            ->leftJoin(['invoice' => Invoice::tableName()], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
            ->leftJoin(['product' => Product::tableName()], '{{product}}.[[id]] = {{order}}.[[product_id]]')
            ->leftJoin(['order_main' => Order::tableName()], '{{order_main}}.[[id]] = {{order}}.[[order_id]]')
            ->andWhere(['invoice.company_id' => $this->_company->id])
            ->andWhere(['invoice.invoice_status_id' => InvoiceStatus::$validInvoices])
            ->andWhere(['document.type' => $type])
            ->andWhere(['or',
                ['document.status_out_id' => UpdStatus::$turmoverStatus],
                ['document.status_out_id' => null],
            ]);

        return $query1->union($query2, true)->union($query3, true);
    }
}