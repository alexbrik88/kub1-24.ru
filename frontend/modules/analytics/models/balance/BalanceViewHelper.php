<?php namespace frontend\modules\analytics\models\balance;

class BalanceViewHelper {

    public static function flattenTable(array $tableHierarchical): array
    {
        $tableFlat = [];
        $floor1 = $floor2 = $floor3 = 0;

        foreach ($tableHierarchical as $row) {
            $tableFlat[] = [
                'level' => 1,
                'label' => $row['label'],
                'options' => $row['options'] ?? [],
                'data' => $row['data'] ?? [],
                'dataInitial' => $row['dataInitial'] ?? [],
                'floorId' => '',
                'floorTarget' => '',
                'includedClass' => 'm-l-repo',
                'jsActions' => $row['jsActions'] ?? []
            ];
            if (!empty($row['items'])) {
                foreach ($row['items'] as $row2) {
                    $floor1++;
                    $tableFlat[] = [
                        'level' => 2,
                        'label' => $row2['label'],
                        'options' => $row2['options'] ?? [],
                        'data' => $row2['data'] ?? [],
                        'dataInitial' => $row2['dataInitial'] ?? [],
                        'addCheckboxX' => $row2['addCheckboxX'] ?? false,
                        'floorId' => '',
                        'floorTarget' => "floor-1-{$floor1}",
                        'includedClass' => '',
                        'jsActions' => $row2['jsActions'] ?? []
                    ];
                    if (!empty($row2['items'])) {
                        foreach ($row2['items'] as $row3) {
                            $floor2++;
                            $tableFlat[] = [
                                'level' => 3,
                                'label' => $row3['label'],
                                'options' => $row3['options'] ?? [],
                                'data' => $row3['data'] ?? [],
                                'dataInitial' => $row3['dataInitial'] ?? [],
                                'addCheckboxX' => $row3['addCheckboxX'] ?? false,
                                'floorId' => "floor-1-{$floor1}",
                                'floorTarget' => "floor-2-{$floor2}",
                                'includedClass' => isset($row3['addCheckboxX']) ? 'm-l-repo-plus' : 'm-l-repo',
                                'jsActions' => $row3['jsActions'] ?? []
                            ];

                            if (!empty($row3['items'])) {
                                foreach ($row3['items'] as $row4) {
                                    $floor3++;
                                    $tableFlat[] = [
                                        'level' => 4,
                                        'label' => $row4['label'],
                                        'options' => $row4['options'] ?? [],
                                        'data' => $row4['data'] ?? [],
                                        'dataInitial' => $row4['dataInitial'] ?? [],
                                        'addCheckboxX' => $row4['addCheckboxX'] ?? false,
                                        'floorId' => "floor-2-{$floor2}",
                                        'floorTarget' => "floor-3-{$floor3}",
                                        'includedClass' => isset($row4['addCheckboxX']) ? 'm-l-repo-plus-2' : 'm-l-repo-2',
                                        'jsActions' => $row4['jsActions'] ?? []
                                    ];

                                    if (!empty($row4['items'])) {
                                        foreach ($row4['items'] as $row5) {
                                            $tableFlat[] = [
                                                'level' => 5,
                                                'label' => $row5['label'],
                                                'options' => $row5['options'] ?? [],
                                                'data' => $row5['data'],
                                                'dataInitial' => $row5['dataInitial'] ?? [],
                                                'floorId' => "floor-3-{$floor3}",
                                                'floorTarget' => '',
                                                'includedClass' =>'m-l-repo-3',
                                                'jsActions' => $row5['jsActions'] ?? []
                                            ];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $tableFlat;
    }
}