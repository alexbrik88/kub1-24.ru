<?php

namespace frontend\modules\analytics\models\balance;

use Yii;
use common\models\Company;

/**
 * This is the model class for table "balance_initial".
 *
 * @property int $company_id
 * @property string $date
 * @property int $undistributed_profit
 * @property int $fixed_assets_other
 *
 * @property Company $company
 */
class BalanceInitial extends \yii\db\ActiveRecord
{
    const MAIN_DATE = 'main-date';
    const UNDISTRIBUTED_PROFIT = 'undistributed-profit';
    const CAPITAL = 'capital';
    const FIXED_ASSETS_OTHER = 'fixed-assets-other';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'balance_initial';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date'], 'required'],
            [['date'], 'date', 'format' => 'yyyy-M-d'],
            [['undistributed_profit', 'fixed_assets_other'], 'default', 'value' => 0],
            [['undistributed_profit', 'fixed_assets_other'], 'integer', 'min' => 0],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'company_id' => 'Company ID',
            'date' => 'Дата',
            'undistributed_profit' => 'Нераспределенная прибыль',
            'fixed_assets_other' => 'Прочие внеоборотные активы'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany(): \yii\db\ActiveQuery
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    public static function getInitialModel(): self
    {
        $company = Yii::$app->user->identity->company;
        return BalanceInitial::findOne(['company_id' => $company->id])
            ?: new BalanceInitial(['company_id' => $company->id, 'date' => date('Y-m-d', $company->created_at)]);
    }

    /**
     * @return string
     */
    public static function getInitialDate(): string
    {
        $ba = self::getInitialModel();
        return $ba->date;
    }
}
