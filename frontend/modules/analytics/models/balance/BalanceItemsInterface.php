<?php namespace frontend\modules\analytics\models\balance;

interface BalanceItemsInterface {

    const EMPTY_YEAR = [
        '1' => 0, '2' => 0, '3' => 0, '4' => 0, '5' => 0, '6' => 0,
        '7' => 0, '8' => 0, '9' => 0, '10' => 0, '11' => 0, '12' => 0
    ];

    const EMPTY_QUARTERS = ['quarter-1' => 0, 'quarter-2' => 0, 'quarter-3' => 0, 'quarter-4' => 0];

    public function setCompany(array $companyIds); // :self;
    public function load(); // :self;
    public function loadInitial(); // :self;
    public function setYear(int $year); // :self;

}