<?php namespace frontend\modules\analytics\models\balance;

use frontend\modules\analytics\models\balance\actives\current\cash\BalanceCashRows;
use frontend\modules\analytics\models\balance\actives\current\IncompleteProjectsRow;
use frontend\modules\analytics\models\balance\actives\current\receviables\BudgetOverpaymentRow;
use frontend\modules\analytics\models\balance\actives\current\receviables\LoansIssuedRow;
use frontend\modules\analytics\models\balance\actives\current\receviables\OtherInvestmentsRow;
use frontend\modules\analytics\models\balance\actives\current\receviables\PrepaymentsToSuppliersRow;
use frontend\modules\analytics\models\balance\actives\current\receviables\RentalDepositRow;
use frontend\modules\analytics\models\balance\actives\current\receviables\TradeReceviablesRow;
use frontend\modules\analytics\models\balance\actives\current\StocksRow;
use frontend\modules\analytics\models\balance\actives\fixed\BalanceFixedActivesRows;
use frontend\modules\analytics\models\balance\passives\capital\AuthorizedCapitalRow;
use frontend\modules\analytics\models\balance\passives\capital\UndestributedProfitsPrevRow;
use frontend\modules\analytics\models\balance\passives\capital\UndestributedProfitsRow;
use frontend\modules\analytics\models\balance\passives\long\LongTermLoansRow;
use frontend\modules\analytics\models\balance\passives\short\BudgetArrearsRow;
use frontend\modules\analytics\models\balance\passives\short\PrepaymentOfCustomersRow;
use frontend\modules\analytics\models\balance\passives\short\SalaryArrearsRow;
use frontend\modules\analytics\models\balance\passives\short\ShortTermLoansRow;
use frontend\modules\analytics\models\balance\passives\short\VendorInvoicesPayableRow;
use yii\helpers\Url;

class BalanceStructure {

    public static function buildItems(): array
    {
        return [
            [
                'label' => 'АКТИВЫ',
                'options' => ['class' => 'row-assets'],
                'items' => [
                    [
                        'label' => 'Внеоборотные активы',
                        'addCheckboxX' => true,
                        'options' => ['class' => 'row-fixed-assets'],
                        'itemsGetter' => BalanceFixedActivesRows::class,
                        'items' => []
                    ],
                    [
                        'label' => 'Оборотные активы',
                        'addCheckboxX' => true,
                        'options' => ['class' => 'row-current-assets'],
                        'items' => [
                            [
                                // Складские запасы
                                'label' => StocksRow::$label,
                                'getter' => StocksRow::$getter,
                                'options' => ['class' => 'hidden row-stocks'],
                                'jsActions' => [
                                    'editInitialAmount' => 'js-change-balance-initial-store',
                                    'urlEditInitialAmount' => 'javascript:void(0)',
                                    'cellClickClass' => 'balance-store-grid-link',
                                    'cellClickUrl' => Url::to(['/analytics/finance-ajax/get-balance-store-grid'])
                                ],
                            ],
                            [
                                // Незавершенные проекты
                                'label' => IncompleteProjectsRow::$label,
                                'getter' => IncompleteProjectsRow::$getter,
                                'options' => ['class' => 'hidden row-incomplete-projects'],
                            ],
                            [
                                'label' => 'Дебиторская задолженность',
                                'addCheckboxX' => true,
                                'options' => ['class' => 'hidden row-receivables'],
                                'items' => [
                                    [
                                        // Задолженность покупателей
                                        'label' => TradeReceviablesRow::$label,
                                        'getter' => TradeReceviablesRow::$getter,
                                        'options' => ['class' => 'hidden row-trade-receviables'],
                                    ],
                                    [
                                        // Предоплата поставщикам (авансы)
                                        'label' => PrepaymentsToSuppliersRow::$label,
                                        'getter' => PrepaymentsToSuppliersRow::$getter,
                                        'options' => ['class' => 'hidden row-prepayments-to-suppliers'],
                                    ],
                                    [
                                        // Депозит по аренде
                                        'label' => RentalDepositRow::$label,
                                        'getter' => RentalDepositRow::$getter,
                                        'options' => ['class' => 'hidden row-rental-deposit'],
                                    ],
                                    [
                                        // Переплата в бюджет
                                        'label' => BudgetOverpaymentRow::$label,
                                        'getter' => BudgetOverpaymentRow::$getter,
                                        'options' => ['class' => 'hidden row-budget-overpayment'],
                                    ],
                                    [
                                        // Займы выданные и проценты к получению
                                        'label' => LoansIssuedRow::$label,
                                        'getter' => LoansIssuedRow::$getter,
                                        'options' => ['class' => 'hidden row-loans-issued'],
                                    ],
                                    [
                                        // Прочие финансовые вложения
                                        'label' => OtherInvestmentsRow::$label,
                                        'getter' => OtherInvestmentsRow::$getter,
                                        'options' => ['class' => 'hidden row-other-investments'],
                                    ],
                                ],
                            ],
                            [
                                'label' => 'Денежные средства',
                                'addCheckboxX' => true,
                                'options' => ['class' => 'hidden row-cash'],
                                'itemsGetter' => BalanceCashRows::class,
                                'items' => []
                            ]
                        ]
                    ],
                ],
            ],
            [
                'label' => 'ПАССИВЫ',
                'options' => ['class' => 'row-liabilities'],
                'items' => [
                    [
                        'label' => 'Собственный капитал',
                        'addCheckboxX' => true,
                        'options' => ['class' => 'row-capital'],
                        'items' => [
                            [
                                // Уставный капитал
                                'label' => AuthorizedCapitalRow::$label,
                                'getter' => AuthorizedCapitalRow::$getter,
                                'options' => ['class' => 'hidden row-balance-initial-capital'],
                                'jsActions' => [
                                    'editInitialAmount' => 'js-change-balance-initial-capital',
                                    'urlEditInitialAmount' => 'javascript:void(0)'
                                ]
                            ],
                            [
                                'label' => 'Нераспределенная прибыль',
                                'addCheckboxX' => true,
                                'options' => ['class' => 'hidden row-undestributed-capital'],
                                'items' => [
                                    [
                                        // Текущий год
                                        'label' => UndestributedProfitsRow::$label,
                                        'getter' => UndestributedProfitsRow::$getter,
                                        'options' => ['class' => 'hidden row-undestributed-profits'],
                                    ],
                                    [
                                        // Прошлые периоды
                                        'label' => UndestributedProfitsPrevRow::$label,
                                        'getter' => UndestributedProfitsPrevRow::$getter,
                                        'options' => ['class' => 'hidden row-balance-initial-undistributed-profit'],
                                        'jsActions' => [
                                            'editInitialAmount' => 'js-change-balance-initial-undistributed-profit',
                                            'urlEditInitialAmount' => 'javascript:void(0)'
                                        ]
                                    ],
                                ],
                            ],
                        ],
                    ],
                    [
                        'label' => 'Долгосрочные обязательства',
                        'addCheckboxX' => true,
                        'options' => ['class' => 'row-long-term-duties'],
                        'items' => [
                            [
                                // Кредиты и займы сроком более 12 месяцев
                                'label' => LongTermLoansRow::$label,
                                'getter' => LongTermLoansRow::$getter,
                                'options' => ['class' => 'hidden row-long-term-loans'],
                                'jsActions' => [
                                    'editInitialAmount' => 'js-change-balance-initial-credit',
                                    'urlEditInitialAmount' => 'javascript:void(0)',
                                ],
                            ],
                        ],
                    ],
                    [
                        'label' => 'Краткосрочные обязательства',
                        'addCheckboxX' => true,
                        'options' => ['class' => 'row-short-term-duties'],
                        'items' => [
                            [
                                // Кредиты и займы сроком до 12 месяцев
                                'label' => ShortTermLoansRow::$label,
                                'getter' => ShortTermLoansRow::$getter,
                                'options' => ['class' => 'hidden row-short-term-loans'],
                                'jsActions' => [
                                    'editInitialAmount' => 'js-change-balance-initial-credit',
                                    'urlEditInitialAmount' => 'javascript:void(0)',
                                ],
                            ],
                            [
                                'label' => 'Кредиторская задолженность',
                                'addCheckboxX' => true,
                                'options' => ['class' => 'hidden row-short-term-accounts-payable'],
                                'items' => [
                                    [
                                        // Задолженность перед поставщиками
                                        'label' => VendorInvoicesPayableRow::$label,
                                        'getter' => VendorInvoicesPayableRow::$getter,
                                        'options' => ['class' => 'hidden row-vendor-invoices-payable'],
                                    ],
                                    [
                                        // Предоплата покупателей (авансы)
                                        'label' => PrepaymentOfCustomersRow::$label,
                                        'getter' => PrepaymentOfCustomersRow::$getter,
                                        'options' => ['class' => 'hidden row-prepayment-of-customers'],
                                    ],
                                    [
                                        // Задолженность по зарплате
                                        'label' => SalaryArrearsRow::$label,
                                        'getter' => SalaryArrearsRow::$getter,
                                        'options' => ['class' => 'hidden row-salary-arrears'],
                                    ],
                                    [
                                        // Задолженность перед бюджетом
                                        'label' => BudgetArrearsRow::$label,
                                        'getter' => BudgetArrearsRow::$getter,
                                        'options' => ['class' => 'hidden row-budget-arrears'],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            [
                'label' => 'АКТИВ - ПАССИВ',
                'getter' => 'getBalanceCheck',
                'options' => ['class' => 'assets-not-equal-liabilities'],
            ],
        ];
    }

    public static function flattenTable(array $tableHierarchical): array
    {
        $tableFlat = [];

        $floor1 = 0;
        $floor2 = 0;
        $floor3 = 0;
        foreach ($tableHierarchical as $row) {
            $tableFlat[] = [
                'label' => $row['label'],
                'options' => $row['options'] ?? [],
                'data' => $row['data'] ?? [],
                'floorId' => '',
                'floorTarget' => '',
                'includedClass' => 'm-l-repo'
            ];
            if (!empty($row['items'])) {
                foreach ($row['items'] as $row2) {
                    $floor1++;
                    $tableFlat[] = [
                        'label' => $row2['label'],
                        'options' => $row2['options'] ?? [],
                        'data' => $row2['data'] ?? [],
                        'addCheckboxX' => $row2['addCheckboxX'] ?? false,
                        'floorId' => '',
                        'floorTarget' => "floor-1-{$floor1}",
                        'includedClass' => ''
                    ];
                    if (!empty($row2['items'])) {
                        foreach ($row2['items'] as $row3) {
                            $floor2++;
                            $tableFlat[] = [
                                'label' => $row3['label'],
                                'options' => $row3['options'] ?? [],
                                'data' => $row3['data'] ?? [],
                                'addCheckboxX' => $row3['addCheckboxX'] ?? false,
                                'floorId' => "floor-1-{$floor1}",
                                'floorTarget' => "floor-2-{$floor2}",
                                'includedClass' => isset($row3['addCheckboxX']) ? 'm-l-repo-plus' : 'm-l-repo'
                            ];

                            if (!empty($row3['items'])) {
                                foreach ($row3['items'] as $row4) {
                                    $floor3++;
                                    $tableFlat[] = [
                                        'label' => $row4['label'],
                                        'options' => $row4['options'] ?? [],
                                        'data' => $row4['data'] ?? [],
                                        'addCheckboxX' => $row4['addCheckboxX'] ?? false,
                                        'floorId' => "floor-2-{$floor2}",
                                        'floorTarget' => "floor-3-{$floor3}",
                                        'includedClass' => isset($row4['addCheckboxX']) ? 'm-l-repo-plus-2' : 'm-l-repo-2'
                                    ];

                                    if (!empty($row4['items'])) {
                                        foreach ($row4['items'] as $row5) {
                                            $tableFlat[] = [
                                                'label' => $row5['label'],
                                                'options' => $row5['options'] ?? [],
                                                'data' => $row5['data'],
                                                'floorId' => "floor-3-{$floor3}",
                                                'floorTarget' => '',
                                                'includedClass' =>'m-l-repo-3'
                                            ];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $tableFlat;
    }
}