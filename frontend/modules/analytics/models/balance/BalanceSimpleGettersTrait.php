<?php namespace frontend\modules\analytics\models\balance;

use common\components\helpers\ArrayHelper;

trait BalanceSimpleGettersTrait {

    public function getAssets(\DateTime $date): int
    {
        return $this->__getMonthAmount('row-assets', $date);
    }

    public function getLiabilities(\DateTime $date): int
    {
        return $this->__getMonthAmount('row-liabilities', $date);
    }

    public function getReceivables(\DateTime $date): int
    {
        return $this->__getMonthAmount('row-receivables', $date);
    }

    public function getStocks(\DateTime $date): int
    {
        return $this->__getMonthAmount('row-stocks', $date);
    }

    public function getCash(\DateTime $date): int
    {
        return $this->__getMonthAmount('row-cash', $date);
    }

    public function getIncompleteProjects(\DateTime $date): int
    {
        return $this->__getMonthAmount('row-incomplete-projects', $date);
    }

    public function getFixedAssets(\DateTime $date): int
    {
        return $this->__getMonthAmount('row-fixed-assets', $date);
    }

    public function getShorttermAccountsPayable(\DateTime $date): int
    {
        return $this->__getMonthAmount('row-short-term-accounts-payable', $date);
    }

    public function getShorttermBorrowings(\DateTime $date): int
    {
        return $this->__getMonthAmount('row-short-term-loans', $date);
    }

    public function getLongtermDuties(\DateTime $date): int
    {
        return $this->__getMonthAmount('row-long-term-duties', $date);
    }

    public function getLongtermLoans(\DateTime $date): int
    {
        return $this->__getMonthAmount('row-long-term-loans', $date);
    }

    public function getCapital(\DateTime $date): int
    {
        return $this->__getMonthAmount('row-capital', $date);
    }

    public function getBalanceCheck(\DateTime $date): int
    {
        return $this->getAssets(clone $date) - $this->getLiabilities(clone $date);
    }

    public static function getAssetsRow(array $balanceItems): array
    {
        return self::__getRow($balanceItems, 'row-assets');
    }

    public static function getLiabilitiesRow(array $balanceItems): array
    {
        return self::__getRow($balanceItems, 'row-liabilities');
    }

    private static function __getRow(array $balanceItems, string $class): array
    {
        foreach ($balanceItems as $level1) {
            // level 1
            if (self::__hasRowClass($class, $level1))
                return $level1;
        }

        return [];
    }



    private function __getMonthAmount(string $class, \DateTime $date): int
    {
        $month = $date->format('n');

        foreach ($this->_resultBalanceRows as $level1) {
            // level 1
            if (self::__hasRowClass($class, $level1))
                return self::__getRowAmount($month, $level1);
            // level 2
            if (!empty($level1['items'])) {
                foreach ($level1['items'] as $level2) {
                    if (self::__hasRowClass($class, $level2))
                        return self::__getRowAmount($month, $level2);
                    // level3
                    if (!empty($level2['items'])) {
                        foreach ($level2['items'] as $level3) {
                            if (self::__hasRowClass($class, $level3))
                                return self::__getRowAmount($month, $level3);
                        }
                    }
                }
            }
        }

        return 0;
    }

    private static function __hasRowClass(string $class, array $row): bool
    {
        $rowClasses = explode(' ', ArrayHelper::getValue($row, 'options.class', ''));
        return in_array($class, $rowClasses);
    }

    private static function __getRowAmount(int $month, array $row): int
    {
        return (int)ArrayHelper::getValue($row['data'] ?? [], $month);
    }
}