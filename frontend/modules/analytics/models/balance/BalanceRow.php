<?php namespace frontend\modules\analytics\models\balance;

use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\status\InvoiceStatus;
use common\models\product\Product;
use frontend\modules\analytics\models\debtor\DebtorFilterHelper3;
use frontend\modules\analytics\models\debtor\DebtorHelper2;
use frontend\modules\analytics\models\OLAP;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\base\Model;
use Yii;

class BalanceRow extends Model {

    const EMPTY_YEAR = ['1' => 0, '2' => 0, '3' => 0, '4' => 0, '5' => 0, '6' => 0, '7' => 0, '8' => 0, '9' => 0, '10' => 0, '11' => 0, '12' => 0];
    const EMPTY_QUARTERS = ['quarter-1' => 0, 'quarter-2' => 0, 'quarter-3' => 0, 'quarter-4' => 0];
    const EMPTY_INITIAL = ['amount' => 0, 'date' => null];

    public static string $label  = '---';
    public static string $getter = self::class;

    protected array $_multiCompanyIds;

    protected static ProfitAndLossSearchModel $_palModel;

    public function getPalModel(): ProfitAndLossSearchModel
    {
        if (!isset(self::$_palModel)) {
            self::$_palModel = new ProfitAndLossSearchModel();
            self::$_palModel->handleItems();
        }

        return self::$_palModel;
    }

    public function setCompany(array $multiCompanyIds): void
    {
        $this->_multiCompanyIds = $multiCompanyIds;
    }
    
    public function getYearData(int $Y): array
    {
        return self::EMPTY_YEAR;
    }

    public function getInitialData(string $onDate): array
    {
        return self::EMPTY_INITIAL;
    }

    protected static function __getInterval($year): string
    {
        $startDate = $year . '-01-31';

        if ($year == date('Y')) {
            $lastDate = date('Y-m-d');
            $replacedDate = date('Y-m-t');

            return "IF (('{$startDate}' + INTERVAL (seq) MONTH) = '{$replacedDate}', '{$lastDate}', ('{$startDate}' + INTERVAL (seq) MONTH))";
        }

        return "('{$startDate}' + INTERVAL (seq) MONTH)";
    }

    protected static function __getSeq($year): string
    {
        return 'seq_0_to_' . ($year == date('Y') ? (date('n') - 1) : 11);
    }

    protected function __getOverdueInvoices($Y, $type): array
    {
        $ret = self::EMPTY_YEAR;

        DebtorHelper2::$SEARCH_BY = DebtorHelper2::SEARCH_BY_DOCUMENTS;
        $subQuery = DebtorHelper2::getQueryByDocs($this->_multiCompanyIds, $type);
        $calc = [];
        for ($i = 1; $i <= 12; $i++) {
            $month = str_pad($i, 2, "0", STR_PAD_LEFT);
            $calc[$i] = "IF(DATE_FORMAT(t.date, '%Y%m') <= {$Y}{$month}, invoice_sum - flow_sum, 0)";
        }

        $query = "
            SELECT
              contractor_id,
              GREATEST(SUM({$calc[1]}), 0)   `1`,
              GREATEST(SUM({$calc[2]}), 0)   `2`,
              GREATEST(SUM({$calc[3]}), 0)   `3`,
              GREATEST(SUM({$calc[4]}), 0)   `4`,
              GREATEST(SUM({$calc[5]}), 0)   `5`,
              GREATEST(SUM({$calc[6]}), 0)   `6`,
              GREATEST(SUM({$calc[7]}), 0)   `7`,
              GREATEST(SUM({$calc[8]}), 0)   `8`,
              GREATEST(SUM({$calc[9]}), 0)   `9`,
              GREATEST(SUM({$calc[10]}), 0) `10`,
              GREATEST(SUM({$calc[11]}), 0) `11`,
              GREATEST(SUM({$calc[12]}), 0) `12`
            FROM (" . $subQuery->createCommand()->rawSql . ") t
            GROUP BY contractor_id
        ";

        $_totalByYear = Yii::$app->db2->createCommand($query)->queryAll();
        foreach ((array)$_totalByYear as $_tmpRow) {
            foreach ($_tmpRow as $_month => $_amount) {
                if ($_month == 'contractor_id')
                    continue;

                $ret[$_month] += $_amount;

                if ($Y == date('Y') && $_month == date('m'))
                    break;
            }
        }

        return $ret;
    }

    protected function __getPrepaidByDocs($Y, $docType): array
    {
        $companyIds = implode(',', $this->_multiCompanyIds);
        $table = OLAP::TABLE_OLAP_INVOICES;
        $seq = self::__getSeq($Y);
        $interval = self::__getInterval($Y);
        $monthSum = Yii::$app->db2->createCommand("
            SELECT MONTH({$interval}) m,
            IFNULL(SUM(t.total_amount_with_nds), 0) amount
            FROM {$seq}
            JOIN {$table} t 
                ON  (t.company_id IN ({$companyIds}))
                AND (FROM_UNIXTIME(t.status_updated_at, '%Y-%m-%d') <= {$interval})
                AND (t.is_deleted = FALSE)
                AND (t.type = {$docType}) 
                AND (t.status_id = ".InvoiceStatus::STATUS_PAYED.")
                AND (t.has_doc = FALSE OR (t.has_doc = TRUE AND t.doc_date > {$interval}))
                AND (t.need_all_docs = TRUE)
            GROUP BY ({$interval})
       ")->queryAll();

        $ret = [];
        foreach ($monthSum as $m) {
            $ret[$m['m']] = $m['amount'];
        }

        return $ret;
    }

    protected function __getPrepaidByFlows($Y, $flowType): array
    {
        $companyIds = implode(',', $this->_multiCompanyIds);
        $table = OLAP::TABLE_OLAP_FLOWS;
        $seq = self::__getSeq($Y);
        $interval = self::__getInterval($Y);
        $excludeItems = [
            CashFlowsBase::FLOW_TYPE_EXPENSE => [
                InvoiceExpenditureItem::ITEM_ENSURE_PAYMENT
            ]
        ];
        $SQL_FILTER_ITEMS = DebtorFilterHelper3::getSqlFilterItems($this->_multiCompanyIds, $flowType, $excludeItems);
        $SQL_FILTER_WALLETS = DebtorFilterHelper3::getSqlFilterWallets($this->_multiCompanyIds);
        $monthSum = Yii::$app->db2->createCommand("
            SELECT MONTH({$interval}) m,
            IFNULL(SUM(t.amount - t.invoice_amount), 0) amount
            FROM {$seq}
            JOIN {$table} t 
                ON  (t.company_id IN ({$companyIds}))
                AND (t.date <= {$interval})
                AND (t.type = {$flowType}) 
                AND (t.contractor_id > 0) AND (t.invoice_amount < t.amount)
                {$SQL_FILTER_ITEMS}
                {$SQL_FILTER_WALLETS}
            GROUP BY ({$interval})
       ")->queryAll();

        $ret = [];
        foreach ($monthSum as $m) {
            $ret[$m['m']] = $m['amount'];
        }

        return $ret;
    }

    protected function __getStocks($Y): array
    {
        $ret = [];
        $companyIds = implode(',', $this->_multiCompanyIds);
        $productTable = Product::tableName();
        $turnoverTable = OLAP::TABLE_PRODUCT_TURNOVER;
        $interval = self::__getInterval($Y);
        $seq = self::__getSeq($Y);
        $monthSum = Yii::$app->db2->createCommand("
            SELECT MONTH({$interval}) m,
            SUM(IF(`type` = 2, -1, 1) * IFNULL(p.price_for_buy_with_nds,0) * t.quantity) AS amount
            FROM {$seq}
            JOIN {$turnoverTable} t 
              ON  t.company_id IN ({$companyIds})
              AND t.date <= {$interval}
              AND t.production_type = 1
              AND t.is_document_actual = TRUE
              AND t.is_invoice_actual = TRUE
            LEFT JOIN {$productTable} p ON p.id = t.product_id
            GROUP BY {$interval}
       ")->queryAll();

        foreach ($monthSum as $sum) {
            $ret[$sum['m']] = $sum['amount'];
        }

        return $ret;
    }

    private function __getWallet($Y, $wallet): array
    {
        $startSum = (new Query)
            ->select(new Expression('
                SUM(IF(t.type = 1, t.amount, -1 * t.amount)) AS amount'))
            ->from(['t' => OLAP::TABLE_OLAP_FLOWS])
            ->where(['t.company_id' => $this->_multiCompanyIds])
            ->andWhere(['t.wallet' => $wallet])
            ->andWhere(['<', 't.date', "{$Y}-01-01"])
            ->scalar(Yii::$app->db2);

        $monthSum = (new Query)
            ->select(new Expression(' 
                SUM(IF(t.type = 1, t.amount, -1 * t.amount)) AS amount, 
                MONTH(t.date) AS month'))
            ->from(['t' => OLAP::TABLE_OLAP_FLOWS])
            ->indexBy('month')
            ->groupBy(['YEAR(t.date)', 'MONTH(t.date)'])
            ->where(['t.company_id' => $this->_multiCompanyIds])
            ->andWhere(['t.wallet' => $wallet])
            ->andWhere(['between', 't.date', "{$Y}-01-01", "{$Y}-12-31"])
            ->all(Yii::$app->db2);

        // growing
        $ret = [];
        foreach (self::EMPTY_YEAR as $month => $zeroes) {
            $sum = $startSum + ArrayHelper::getValue(ArrayHelper::getValue($monthSum, $month), 'amount');
            $startSum = $sum;
            $ret[$month] = $sum;

            if ($Y == date('Y') && $month == date('m'))
                break;
        }

        return $ret;
    }

    protected function __getAllWalletsByItem($Y, $incomeItemId, $expenditureItemId, $isInverted = false): array
    {
        $startIncomeSum = (new Query)
            ->select(new Expression('SUM(t.amount) AS amount'))
            ->from(['t' => OLAP::TABLE_OLAP_FLOWS])
            ->where(['t.company_id' => $this->_multiCompanyIds])
            ->andWhere(['t.type' => CashFlowsBase::FLOW_TYPE_INCOME])
            ->andWhere(['t.item_id' => $incomeItemId])
            ->andWhere(['<', 't.date', "{$Y}-01-01"])
            ->scalar(Yii::$app->db2);
        
        $startExpenseSum = (new Query)->select(new Expression('SUM(t.amount) AS amount'))
            ->from(['t' => OLAP::TABLE_OLAP_FLOWS])
            ->where(['t.company_id' => $this->_multiCompanyIds])
            ->andWhere(['t.type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
            ->andWhere(['t.item_id' => $expenditureItemId])
            ->andWhere(['<', 't.date', "{$Y}-01-01"])
            ->scalar(Yii::$app->db2);

        $monthIncomeSum = (new Query)->select(new Expression(' 
                SUM(t.amount) AS amount, 
                MONTH(t.date) AS month'))
            ->from(['t' => OLAP::TABLE_OLAP_FLOWS])
            ->indexBy('month')
            ->groupBy(['YEAR(t.date)', 'MONTH(t.date)'])
            ->where(['t.company_id' => $this->_multiCompanyIds])
            ->andWhere(['t.type' => CashFlowsBase::FLOW_TYPE_INCOME])
            ->andWhere(['t.item_id' => $incomeItemId])
            ->andWhere(['between', 't.date', "{$Y}-01-01", "{$Y}-12-31"])
            ->all(Yii::$app->db2);

        $monthExpenseSum = (new Query)->select(new Expression(' 
                SUM(t.amount) AS amount, 
                MONTH(t.date) AS month'))
            ->from(['t' => OLAP::TABLE_OLAP_FLOWS])
            ->indexBy('month')
            ->groupBy(['YEAR(t.date)', 'MONTH(t.date)'])
            ->where(['t.company_id' => $this->_multiCompanyIds])
            ->andWhere(['t.type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
            ->andWhere(['t.item_id' => $expenditureItemId])
            ->andWhere(['between', 't.date', "{$Y}-01-01", "{$Y}-12-31"])
            ->all(Yii::$app->db2);

        // growing
        $ret = [];
        $startSum = $isInverted ? ($startExpenseSum - $startIncomeSum) : ($startIncomeSum - $startExpenseSum);
        foreach (self::EMPTY_YEAR as $month => $zeroes) {
            $sum = $startSum
                + ArrayHelper::getValue(ArrayHelper::getValue($isInverted ? $monthExpenseSum : $monthIncomeSum, $month), 'amount')
                - ArrayHelper::getValue(ArrayHelper::getValue($isInverted ? $monthIncomeSum : $monthExpenseSum, $month), 'amount');

            $startSum = max(0, $sum);
            $ret[$month] = max(0, $sum);

            if ($Y == date('Y') && $month == date('m'))
                break;
        }

        return $ret;
    }

    protected function __calcUndestributedProfits($Y): array
    {
        $palModel = self::getPalModel();

        $ret = [];
        for ($i=1; $i<=12; $i++) {
            $monthNumber = str_pad($i, 2, "0", STR_PAD_LEFT);
            $ret[$i] = $palModel->getValue('undestributedProfits', $Y, $monthNumber, 'amount');
            if ($Y == date('Y') && $i == date('m'))
                break;
        }
        
        return $ret;
    }
    
}