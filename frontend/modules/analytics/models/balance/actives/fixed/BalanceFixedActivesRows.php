<?php namespace frontend\modules\analytics\models\balance\actives\fixed;

use Carbon\Carbon;
use yii\helpers\Url;
use common\models\balance\BalanceArticle;
use frontend\modules\analytics\models\balance\BalanceInitial;
use frontend\modules\analytics\models\balance\BalanceItemsInterface;
use frontend\modules\reference\models\BalanceArticlesCategories;

class BalanceFixedActivesRows implements BalanceItemsInterface {

    private array $companyIds;
    private int $year;

    private static string $initialDate = '';
    private static string $initialOtherAmount = '';

    private static array $nmaStat = [];
    private static array $osStat = [];
    private static array $nmaInitial = [];
    private static array $osInitial = [];

    public function load(): BalanceFixedActivesRows
    {
        if (empty(self::$nmaStat))
            self::loadNmaStat($this->companyIds, $this->year);
        if (empty(self::$osStat))
            self::loadOsStat($this->companyIds, $this->year);

        return $this;
    }

    private static function loadOsStat(array $companyIds, int $reportYear): void
    {
        if (empty($companyIds) || empty($reportYear))
            return;

        $items = self::_getAmortizationItems($companyIds, BalanceArticle::TYPE_FIXED_ASSERTS);

        foreach (BalanceArticlesCategories::FIXED_ASSETS_CATEGORIES_MAP as $catId => $catName) {
            self::$osStat[$catId] = self::EMPTY_YEAR + self::EMPTY_QUARTERS;
        }

        /** @var BalanceArticle $item */
        foreach ($items as $item) {
            $catId = $item->category;
            // amortization
            $_amortization = 0;
            foreach ($item->getAmortizationScheme() as $ym => $amount) {

                if (!isset(self::$osStat[$catId]))
                    continue;

                $y = substr($ym, 0, 4);
                $m = (int)substr($ym, 4, 2);
                $_amortization += $amount;

                if ($y == $reportYear) {
                    self::$osStat[$catId][$m] += $item->amount - $_amortization;
                    if (is_int($m / 3))
                        self::$osStat[$catId]["quarter-".($m/3)] = self::$osStat[$catId][$m];
                }
            }
            // main summ
            $purchaseYm = Carbon::parse($item->purchased_at)->format('Ym');
            $pY = substr($purchaseYm, 0, 4);
            $pM = (int)substr($purchaseYm, 4, 2);
            if ($pY == $reportYear) {
                self::$osStat[$catId][$pM] += (int)$item->amount;
                if (is_int($pM / 3))
                    self::$osStat[$catId]["quarter-".($pM/3)] = self::$osStat[$catId][$pM];
            }
        }
    }

    private static function loadNmaStat(array $companyIds, int $reportYear): void
    {
        if (empty($companyIds) || empty($reportYear))
            return;

        $items = self::_getAmortizationItems($companyIds, BalanceArticle::TYPE_INTANGIBLE_ASSETS);

        self::$nmaStat = self::EMPTY_YEAR + self::EMPTY_QUARTERS;

        /** @var BalanceArticle $item */
        foreach ($items as $item) {
            // amortization
            $_amortization = 0;
            foreach ($item->getAmortizationScheme() as $ym => $amount) {
                $y = substr($ym, 0, 4);
                $m = (int)substr($ym, 4, 2);
                $_amortization += $amount;

                if ($y == $reportYear) {
                    self::$nmaStat[$m] += $item->amount - $_amortization;
                    if (is_int($m / 3))
                        self::$nmaStat["quarter-".($m/3)] = self::$nmaStat[$m];
                }
            }
            // main summ
            $purchaseYm = Carbon::parse($item->purchased_at)->format('Ym');
            $pY = substr($purchaseYm, 0, 4);
            $pM = (int)substr($purchaseYm, 4, 2);
            if ($pY == $reportYear) {
                self::$nmaStat[$pM] += (int)$item->amount;
                if (is_int($pM / 3))
                    self::$nmaStat["quarter-".($pM/3)] = self::$nmaStat[$pM];
            }
        }
    }

    private static function _getAmortizationItems($companyIds, int $type): array
    {
        return BalanceArticle::find()
            ->andWhere(['company_id' => $companyIds])
            ->andWhere(['type' => $type])
            ->all();
    }

    public function loadInitial(): BalanceFixedActivesRows
    {
        $balanceInitial = BalanceInitial::getInitialModel();
        self::$initialDate = $balanceInitial->date;
        self::$initialOtherAmount = (string)$balanceInitial->fixed_assets_other;

        if (empty(self::$nmaInitial))
            self::loadNmaInitial($this->companyIds);
        if (empty(self::$osInitial))
            self::loadOsInitial($this->companyIds);

        return $this;
    }

    public function setCompany(array $companyIds): BalanceFixedActivesRows
    {
        $this->companyIds = $companyIds;
        return $this;
    }

    public function setYear(int $year): BalanceFixedActivesRows
    {
        $this->year = $year;
        return $this;
    }

    public function getItems(): array
    {
        $items = [];
        foreach (BalanceArticlesCategories::FIXED_ASSETS_CATEGORIES_MAP as $catId => $catName) {
            $items[] = [
                'label' => $catName,
                'options' => ['class' => 'hidden row-fixed-assets-' . $catId],
                'data' => self::$osStat[$catId] ?? [],
                'dataInitial' => self::$osInitial[$catId] ?? [],
                'jsActions' => [
                    'addInitialAmountItem' => 'balance-article-modal-link',
                    'urlAddInitialAmountItem' => Url::to(['/analytics/balance-articles/create',
                        'type' => BalanceArticle::TYPE_FIXED_ASSERTS,
                        'category' => $catId
                    ]),
                    'cellClickClass' => 'balance-article-grid-link',
                    'cellClickUrl' => Url::to(['/analytics/finance-ajax/get-balance-article-grid',
                        'type' => BalanceArticle::TYPE_FIXED_ASSERTS,
                        'category' => $catId])
                ]
            ];
        }

        return [
            [
                'label' => 'Основные средства',
                'addCheckboxX' => true,
                'options' => ['class' => 'hidden row-fixed-assets'],
                'data' => [],
                'dataInitial' => [],
                'items' => $items
            ],
            [
                'label' => 'Нематериальные активы',
                'options' => ['class' => 'hidden row-fixed-assets-non-material'],
                'data' => self::$nmaStat ?? [],
                'dataInitial' => self::$nmaInitial ?? [],
                'jsActions' => [
                    'addInitialAmountItem' => 'balance-article-modal-link',
                    'urlAddInitialAmountItem' => Url::to(['/analytics/balance-articles/create',
                        'type' => BalanceArticle::TYPE_INTANGIBLE_ASSETS
                    ]),
                    'cellClickClass' => 'balance-article-grid-link',
                    'cellClickUrl' => Url::to(['/analytics/finance-ajax/get-balance-article-grid',
                        'type' => BalanceArticle::TYPE_INTANGIBLE_ASSETS])
                ]
            ],
            [
                'label' => 'Прочие внеоборотные активы',
                'options' => ['class' => 'hidden row-fixed-assets-other'],
                'data' => [],
                'dataInitial' => [
                    'date' => self::$initialDate,
                    'amount' => self::$initialOtherAmount
                ],
                'jsActions' => [
                    'editInitialAmount' => 'js-change-balance-initial-fixed-assets-other',
                    'urlEditInitialAmount' => 'javascript:void(0)'
                ]
            ]
        ];
    }

    private static function loadNmaInitial(array $companyIds): void
    {
        /** @var BalanceArticle[] $nonMaterial */
        $nonMaterial = self::_getAmortizationItems($companyIds, BalanceArticle::TYPE_INTANGIBLE_ASSETS);

        $remaining = 0;
        foreach ($nonMaterial as $nm) {
            $remaining += $nm->getRemainingAmountOnDate(self::$initialDate);
        }

        self::$nmaInitial = [
            'date' => self::$initialDate,
            'amount' => $remaining
        ];
    }

    private static function loadOsInitial($companyIds)
    {
        /** @var BalanceArticle[] $material */
        $material = self::_getAmortizationItems($companyIds, BalanceArticle::TYPE_FIXED_ASSERTS);

        foreach (BalanceArticlesCategories::FIXED_ASSETS_CATEGORIES_MAP as $catId => $catName) {
            $remainingSum[$catId] = [
                'date' => self::$initialDate,
                'amount' => 0
            ];
        }

        if (empty($remainingSum))
            return;

        foreach ($material as $m) {
            $catId = $m->category;
            if (isset($remainingSum[$catId]))
                $remainingSum[$catId]['amount'] += $m->getRemainingAmountOnDate(self::$initialDate);
        }

        self::$osInitial = $remainingSum;
    }

    private static function loadOtherInitial($companyIds)
    {
        /** @var BalanceArticle[] $material */
        $material = BalanceArticle::find()
            ->andWhere(['type' => BalanceArticle::TYPE_FIXED_ASSERTS])
            ->andWhere(['company_id' => $companyIds])
            ->all();

        foreach (BalanceArticlesCategories::FIXED_ASSETS_CATEGORIES_MAP as $catId => $catName) {
            $remainingSum[$catId] = [
                'date' => self::$initialDate,
                'amount' => 0
            ];
        }

        if (empty($remainingSum))
            return;

        foreach ($material as $m) {
            $catId = $m->category;
            if (isset($remainingSum[$catId]))
                $remainingSum[$catId]['amount'] += $m->getRemainingAmountOnDate(self::$initialDate);
        }

        self::$osInitial = $remainingSum;
    }
}