<?php namespace frontend\modules\analytics\models\balance\actives\current\cash\helper;

use common\models\company\CheckingAccountant;
use frontend\modules\cash\models\CashContractorType;
use yii\helpers\Url;
use common\models\cash\CashFlowsBase as Cash;

class BalanceCashRowsUrlHelper {

    public static function getLinkEditInitialBalance(int $wallet, ?int $accountId, ?int $flowId): string
    {
        $url = 'javascript:void(0);';
        $action = (empty($flowId)) 
            ? 'create' 
            : 'update';

        switch ($wallet) {

            // RUB
            case Cash::WALLET_BANK:
                $rs = ($account = CheckingAccountant::findOne($accountId)) ? $account->rs : null;
                $url = Url::to(["/cash/multi-wallet/{$action}-bank-flow", 
                    'rs' => $rs,
                    'type' => Cash::FLOW_TYPE_INCOME,
                    'contractorId' => CashContractorType::BALANCE_TEXT,
                    'originWallet' => Cash::WALLET_BANK, 
                    'originFlow' => $flowId]);
                break;
            case Cash::WALLET_CASHBOX:
                $url = Url::to(["/cash/multi-wallet/{$action}-order-flow", 
                    'id' => $accountId,
                    'type' => Cash::FLOW_TYPE_INCOME,
                    'contractorId' => CashContractorType::BALANCE_TEXT,
                    'originWallet' => Cash::WALLET_CASHBOX, 
                    'originFlow' => $flowId]);
                break;
            case Cash::WALLET_EMONEY:
                $url = Url::to(["/cash/multi-wallet/{$action}-emoney-flow", 
                    'id' => $accountId,
                    'type' => Cash::FLOW_TYPE_INCOME,
                    'contractorId' => CashContractorType::BALANCE_TEXT,
                    'originWallet' => Cash::WALLET_EMONEY, 
                    'originFlow' => $flowId]);
                break;
            case Cash::WALLET_ACQUIRING:
                $url = Url::to(["/acquiring/acquiring/{$action}",
                    'type' => Cash::FLOW_TYPE_INCOME,
                    'contractorId' => CashContractorType::BALANCE_TEXT,
                    'accountId' => $accountId,
                    'flowId' => $flowId
                ]);
                break;
            case Cash::WALLET_CARD:
                $url = Url::to(["/cards/operation/{$action}",
                    'accountId' => $accountId,
                    'flowId' => $flowId,
                    'redirectUrl' => Url::current()]);
                break;

            // FOREIGN
            case Cash::WALLET_FOREIGN_BANK:
                $url = Url::to(["/cash/multi-wallet/{$action}-bank-flow", 'rs' => $accountId, 'originWallet' => Cash::WALLET_FOREIGN_BANK, 'originFlow' => $flowId, 'isForeign' => 1]);
                break;
            case Cash::WALLET_FOREIGN_CASHBOX:
                $url = Url::to(["/cash/multi-wallet/{$action}-order-flow", 'id' => $accountId, 'originWallet' => Cash::WALLET_FOREIGN_CASHBOX, 'originFlow' => $flowId, 'isForeign' => 1]);
                break;
            case Cash::WALLET_FOREIGN_EMONEY:
                $url = Url::to(["/cash/multi-wallet/{$action}-emoney-flow", 'id' => $accountId, 'originWallet' => Cash::WALLET_FOREIGN_EMONEY, 'originFlow' => $flowId, 'isForeign' => 1]);
                break;
        }

        return $url;
    }
    
}