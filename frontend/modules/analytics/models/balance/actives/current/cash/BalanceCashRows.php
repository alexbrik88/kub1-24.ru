<?php namespace frontend\modules\analytics\models\balance\actives\current\cash;

use common\components\helpers\ArrayHelper;
use common\models\cash\CashFlowsBase;
use common\models\Company;
use common\models\currency\Currency;
use common\modules\acquiring\models\Acquiring;
use common\modules\cards\models\CardAccount;
use frontend\modules\analytics\models\balance\actives\current\cash\helper\BalanceCashRowsUrlHelper as UrlHelper;
use frontend\modules\analytics\models\balance\BalanceItemsInterface;
use frontend\modules\analytics\models\balance\BalanceRow;
use frontend\modules\analytics\models\OLAP;
use frontend\modules\cash\models\CashContractorType;
use yii\db\Expression;
use yii\db\Query;
use Yii;

class BalanceCashRows implements BalanceItemsInterface {

    const T_BANK = 1;
    const T_ORDER = 2;
    const T_EMONEY = 3;
    const T_ACQUIRING = 4;
    const T_CARD = 5;

    const EMPTY_WALLETS = [
        self::T_BANK => [],
        self::T_ORDER => [],
        self::T_EMONEY => [],
        self::T_ACQUIRING => [],
        self::T_CARD => [],
    ];

    const EMPTY_INITIAL_DATA = [
        'wallet' => null,
        'account' => null,
        'foreign' => null,
        'flow_id' => null,
        'date' => null,
        'amount' => null,
        'original_amount' => null
    ];

    public static string $labelBank = 'Счета в банках';
    public static string $labelOrder = 'Касса';
    public static string $labelEmoney = 'E-money';
    public static string $labelAcquiring = 'Эквайринг';
    public static string $labelCard = 'Карты';

    /**
     * cached info by wallets
     * @var array
     */
    private static array $walletStat = self::EMPTY_WALLETS;
    private static array $walletNames = self::EMPTY_WALLETS;
    private static array $walletInitial = self::EMPTY_WALLETS;

    private array $companyIds;
    private int $year;

    public function load(): BalanceCashRows
    {
        if (self::$walletStat === self::EMPTY_WALLETS) {
            self::loadWalletStat($this->companyIds, $this->year);
        }

        return $this;
    }

    public function loadInitial(): BalanceCashRows
    {
        if (self::$walletInitial === self::EMPTY_WALLETS) {
            self::loadWalletInitial($this->companyIds, $this->year);
        }

        return $this;
    }

    public function setCompany(array $companyIds): BalanceCashRows
    {
        $this->companyIds = $companyIds;
        return $this;
    }

    public function setYear(int $year): BalanceCashRows
    {
        $this->year = $year;
        return $this;
    }

    public function getItems(): array
    {
        if (self::$walletNames === self::EMPTY_WALLETS) {
            self::loadWalletNames($this->companyIds);
        }

        return [
            [
                'label' => self::$labelBank,
                'options' => ['class' => 'hidden row-cash-bank'],
                'addCheckboxX' => self::hasWalletItems(self::T_BANK),
                'data' => [],
                'dataInitial' => [],
                'items' => self::getWalletItems(self::T_BANK),
                'jsActions' => [
                    'addInitialAmountItem' => 'js-action-cash-add-bank'
                ]
            ],
            [
                'label' => self::$labelOrder,
                'options' => ['class' => 'hidden row-cash-order'],
                'addCheckboxX' => self::hasWalletItems(self::T_ORDER),
                'data' => [],
                'dataInitial' => [],
                'items' => self::getWalletItems(self::T_ORDER),
                'jsActions' => [
                    'addInitialAmountItem' => 'js-action-cash-add-cashbox'
                ]
            ],
            [
                'label' => self::$labelEmoney,
                'options' => ['class' => 'hidden row-cash-emoney'],
                'addCheckboxX' => self::hasWalletItems(self::T_EMONEY),
                'data' => [],
                'dataInitial' => [],
                'items' => self::getWalletItems(self::T_EMONEY),
                'jsActions' => [
                    'addInitialAmountItem' => 'js-action-cash-add-emoney'
                ]
            ],
            [
                'label' => self::$labelAcquiring,
                'options' => ['class' => 'hidden row-cash-acquiring'],
                'addCheckboxX' => self::hasWalletItems(self::T_ACQUIRING),
                'data' => [],
                'dataInitial' => [],
                'items' => self::getWalletItems(self::T_ACQUIRING),
            ],
            [
                'label' => self::$labelCard,
                'options' => ['class' => 'hidden row-cash-card'],
                'addCheckboxX' => self::hasWalletItems(self::T_CARD),
                'data' => [],
                'dataInitial' => [],
                'items' => self::getWalletItems(self::T_CARD),
            ]
        ];
    }

    private static function getWalletItems(int $wallet): array
    {
        $ret = [];

        foreach ((self::$walletNames[$wallet] ?? []) as $accountId => $label) {
            $data = ArrayHelper::getValue(self::$walletStat, "$wallet.$accountId", self::EMPTY_YEAR);
            $dataInitial = ArrayHelper::getValue(self::$walletInitial, "$wallet.$accountId", self::EMPTY_INITIAL_DATA);
            $hasInitialBalance = !!$dataInitial['flow_id'];
            $classAjaxModalBtn = ($hasInitialBalance) ? '' : ' ajax-modal-btn';
            $ret[] = [
                'label' => $label,
                'options' => ['class' => "hidden row-cash-{$wallet}-{$accountId}"],
                'data' => $data,
                'dataInitial' => $dataInitial,
                'jsActions' => [
                    'editInitialAmount' => ($hasInitialBalance)
                        ? 'js-action-cash-edit'
                        : 'js-action-cash-add' . $classAjaxModalBtn,
                    'urlEditInitialAmount' =>
                        UrlHelper::getLinkEditInitialBalance($wallet, $accountId, $dataInitial['flow_id']),
                ]
            ];
        }

        return $ret;
    }

    private static function hasWalletItems(int $wallet): bool
    {
        return true;
    }

    private static function loadWalletStat(array $companyIds, int $Y): void
    {
        if (empty($companyIds) || empty($Y))
            return;

        $startSum = (new Query)
            ->select(new Expression('
                t.wallet, 
                t.account_id, 
                SUM(IF(t.type = 1, t.amount, -1 * t.amount)) AS amount
            '))
            ->from(['t' => OLAP::TABLE_OLAP_FLOWS])
            ->where(['t.company_id' => $companyIds])
            ->andWhere(['<', 't.date', "{$Y}-01-01"])
            ->groupBy(['t.wallet', 't.account_id'])
            ->all(Yii::$app->db2);

        $startSumArray = self::EMPTY_WALLETS;

        foreach ($startSum as $data) {
            $wallet = $data['wallet'];
            $accountId = $data['account_id'];
            $sum = $data['amount'];
            $startSumArray[$wallet][$accountId] = $sum;

            if (!isset(self::$walletStat[$wallet][$accountId])) {
                self::$walletStat[$wallet][$accountId] = [];
                foreach (self::EMPTY_YEAR as $key => $zeroes) {
                    if ($Y == date('Y') && $key > date('n')) {
                        self::$walletStat[$wallet][$accountId][$key] = 0;
                    } else {
                        self::$walletStat[$wallet][$accountId][$key] = $sum;
                    }
                }
            }
        }

        $monthSum = (new Query)
            ->select(new Expression(' 
                t.wallet, 
                t.account_id,
                SUM(IF(t.type = 1, t.amount, -1 * t.amount)) AS amount, 
                MONTH(t.date) AS month,
                YEAR(t.date) AS year
            '))
            ->from(['t' => OLAP::TABLE_OLAP_FLOWS])
            ->groupBy(['wallet', 'account_id', 'year', 'month'])
            ->where(['t.company_id' => $companyIds])
            ->andWhere(['between', 't.date', "{$Y}-01-01", "{$Y}-12-31"])
            ->all(Yii::$app->db2);

        // growing
        foreach ($monthSum as $data) {
            $wallet = $data['wallet'];
            $accountId = $data['account_id'];
            $month = $data['month'];
            $sum = ($startSumArray[$wallet][$accountId] ?? 0) + $data['amount'];
            $startSumArray[$wallet][$accountId] = $sum;

            if (!isset(self::$walletStat[$wallet][$accountId]))
                self::$walletStat[$wallet][$accountId] = [];

            self::$walletStat[$wallet][$accountId][$month] = $sum;
        }

       // quarters
       foreach (self::$walletStat as $wallet => $walletData)
           foreach ($walletData as $accountId => $data)
               foreach (self::EMPTY_YEAR as $key => $zeroes) {
                   $amount = self::$walletStat[$wallet][$accountId][$key] ?? 0;
                   if ($Y == date('Y') && $key > date('n') && ceil($key / 3) == ceil(date('n') / 3)) {
                       $quarterAmount = self::$walletStat[$wallet][$accountId][date('n')] ?? 0;
                   } else {
                       $quarterAmount = $amount;
                   }
                   if (is_int($key / 3)) {
                       self::$walletStat[$wallet][$accountId]['quarter-' . ($key / 3)] = $quarterAmount;
                   }
               }
    }

    private static function loadWalletInitial(array $companyIds, int $Y): void
    {
        if (empty($companyIds) || empty($Y))
            return;

        $raw = (new Query)
            ->select(new Expression('
                t.wallet, 
                t.account_id,
                t.id AS flow_id,
                t.currency_name,
                t.date, 
                SUM(t.amount) AS amount,
                SUM(t.original_amount) AS original_amount
            '))
            ->from(['t' => OLAP::TABLE_OLAP_FLOWS])
            ->where([
                't.company_id' => $companyIds,
                't.type' => CashFlowsBase::FLOW_TYPE_INCOME,
                't.contractor_id' => CashContractorType::BALANCE_TEXT
            ])
            ->groupBy(['t.wallet', 't.account_id'])
            ->all(Yii::$app->db2);

        foreach ($raw as $r) {
            $wallet = $r['wallet'];
            $accountId = $r['account_id'];
            self::$walletInitial[$wallet][$accountId] = [
                'wallet' => $r['wallet'],
                'account' => $r['account_id'],
                'flow_id' => $r['flow_id'],
                'foreign' => $r['currency_name'] !== Currency::DEFAULT_NAME,
                'date' => $r['date'],
                'amount' => $r['amount'],
                'original_amount' => $r['original_amount']
            ];
        }
    }

    private static function loadWalletNames(array $companyIds): void
    {
        if (empty($companyIds))
            return;

        foreach ($companyIds as $id) {
            if ($company = Company::findOne(['id' => $id])) {
                $_tmpBankNames = $company->getRubleAccounts()
                    ->select('name')
                    ->orderBy(['type' => SORT_ASC, 'name' => SORT_ASC])
                    ->indexBy('id')
                    ->column();
                $_tmpCashboxNames = $company->getRubleCashboxes()
                    ->select('name')
                    ->orderBy(['is_main' => SORT_DESC, 'name' => SORT_ASC])
                    ->indexBy('id')
                    ->column();
                $_tmpEmoneyNames = $company->getRubleEmoneys()
                    ->select('name')
                    ->orderBy(['is_main' => SORT_DESC, 'name' => SORT_ASC])
                    ->indexBy('id')
                    ->column();
                //
                $_tmpAcquiringNames = Acquiring::find()
                    ->where(['company_id' => $company->id])
                    ->select(new Expression('CONCAT(CASE WHEN type=0 THEN "Монета" WHEN type=1 THEN "ЮKassa" END, " (",identifier ,")")'))
                    ->orderBy('type')
                    ->indexBy('identifier')
                    ->column();
                $_tmpCardNames = CardAccount::find()
                    ->where(['company_id' => $company->id])
                    ->select(new Expression('CONCAT("Дзен мани", " (", identifier, ")")'))
                    ->orderBy('identifier')
                    ->indexBy('id')
                    ->column();

                if (count($companyIds) > 1) {
                    foreach ($_tmpBankNames as &$name) $name = ($company->getShortName()) . ' ' . $name;
                    foreach ($_tmpCashboxNames as &$name) $name = ($company->getShortName()) . ' ' . $name;
                    foreach ($_tmpEmoneyNames as &$name) $name = ($company->getShortName()) . ' ' . $name;
                    foreach ($_tmpAcquiringNames as &$name) $name = ($company->getShortName()) . ' ' . $name;
                    foreach ($_tmpCardNames as &$name) $name = ($company->getShortName()) . ' ' . $name;
                }

                self::$walletNames[self::T_BANK] += $_tmpBankNames;
                self::$walletNames[self::T_ORDER] += $_tmpCashboxNames;
                self::$walletNames[self::T_EMONEY] += $_tmpEmoneyNames;
                self::$walletNames[self::T_ACQUIRING] += $_tmpAcquiringNames;
                self::$walletNames[self::T_CARD] += $_tmpCardNames;
            }
        }
    }
}