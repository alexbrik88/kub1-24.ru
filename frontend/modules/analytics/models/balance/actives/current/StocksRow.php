<?php namespace frontend\modules\analytics\models\balance\actives\current;

use common\models\product\Product;
use frontend\modules\analytics\models\balance\BalanceRow;
use frontend\modules\analytics\models\balance\BalanceRowInterface;

class StocksRow extends BalanceRow implements BalanceRowInterface {

    public static string $label  = 'Складские запасы';
    public static string $getter = self::class;

    /**
     * @param int $Y
     * @return array
     */
    public function getYearData(int $Y): array
    {
        return $this->__getStocks($Y);
    }

    public function getInitialData(string $onDate): array
    {
        $companyId = $this->_multiCompanyIds[0];
        $productionType = Product::PRODUCTION_TYPE_GOODS;

        // todo: move to method
        $querySql = "
            SELECT 
                pib.date AS initial_date,
                SUM(ps.initial_quantity * pib.price) AS initial_amount
            FROM product p
            LEFT JOIN product_initial_balance pib ON p.id = pib.product_id
            LEFT JOIN product_store ps ON p.id = ps.product_id  
            WHERE 
                p.company_id = {$companyId} AND 
                p.production_type = {$productionType} AND
                p.is_deleted = 0 AND
                p.status > 0
            GROUP BY p.id
        ";

        $ret = \Yii::$app->db->createCommand("SELECT initial_date AS date, SUM(initial_amount) AS amount FROM ({$querySql}) z")->queryOne();

        return [
            'amount' => $ret['amount'] ?? 0,
            'date' => $ret['date'] ?? null
        ];
    }
}