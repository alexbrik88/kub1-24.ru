<?php namespace frontend\modules\analytics\models\balance\actives\current;

use frontend\modules\analytics\models\balance\BalanceRow;
use frontend\modules\analytics\models\balance\BalanceRowInterface;

class IncompleteProjectsRow extends BalanceRow implements BalanceRowInterface {

    public static string $label  = 'Незавершенные проекты';
    public static string $getter = self::class;

    /**
     * @param int $Y
     * @return array
     */
    public function getYearData(int $Y): array
    {
        return [];
    }
}