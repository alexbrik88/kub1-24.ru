<?php namespace frontend\modules\analytics\models\balance\actives\current\receviables;

use frontend\models\Documents;
use frontend\modules\analytics\models\balance\BalanceRow;
use frontend\modules\analytics\models\balance\BalanceRowInterface;

class TradeReceviablesRow extends BalanceRow implements BalanceRowInterface {

    public static string $label  = 'Задолженность покупателей';
    public static string $getter = self::class;

    /**
     * @param int $Y
     * @return array
     */
    public function getYearData(int $Y): array
    {
        return $this->__getOverdueInvoices($Y, Documents::IO_TYPE_OUT);
    }
}