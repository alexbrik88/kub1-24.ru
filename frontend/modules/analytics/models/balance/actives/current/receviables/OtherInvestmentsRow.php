<?php namespace frontend\modules\analytics\models\balance\actives\current\receviables;

use frontend\modules\analytics\models\balance\BalanceRow;
use frontend\modules\analytics\models\balance\BalanceRowInterface;

class OtherInvestmentsRow extends BalanceRow implements BalanceRowInterface {

    public static string $label  = 'Прочие финансовые вложения';
    public static string $getter = self::class;

    /**
     * @param int $Y
     * @return array
     */
    public function getYearData(int $Y): array
    {
        // todo
        return [];
    }
}