<?php namespace frontend\modules\analytics\models\balance\actives\current\receviables;

use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use frontend\modules\analytics\models\balance\BalanceRow;
use frontend\modules\analytics\models\balance\BalanceRowInterface;

class RentalDepositRow extends BalanceRow implements BalanceRowInterface {

    public static string $label  = 'Депозит по аренде';
    public static string $getter = self::class;

    /**
     * @param int $Y
     * @return array
     */
    public function getYearData(int $Y): array
    {
        return $this->__getAllWalletsByItem($Y,
            InvoiceIncomeItem::ITEM_ENSURE_PAYMENT,
            InvoiceExpenditureItem::ITEM_ENSURE_PAYMENT,
            true);
    }


}