<?php namespace frontend\modules\analytics\models\balance\actives\current\receviables;

use common\models\cash\CashFlowsBase;
use frontend\models\Documents;
use common\components\helpers\ArrayHelper;
use frontend\modules\analytics\models\balance\BalanceRow;
use frontend\modules\analytics\models\balance\BalanceRowInterface;

class PrepaymentsToSuppliersRow extends BalanceRow implements BalanceRowInterface {

    public static string $label  = 'Предоплата поставщикам (авансы)';
    public static string $getter = self::class;

    /**
     * @param int $Y
     * @return array
     */
    public function getYearData(int $Y): array
    {
        $ret = self::EMPTY_YEAR;
        $byDocs = $this->__getPrepaidByDocs($Y, Documents::IO_TYPE_IN);
        $byFlows = $this->__getPrepaidByFlows($Y, CashFlowsBase::FLOW_TYPE_EXPENSE);

        foreach ($ret as $month => $amount) {
            $ret[$month] += ArrayHelper::getValue($byDocs, $month, 0);
            $ret[$month] += ArrayHelper::getValue($byFlows, $month, 0);
        }

        return $ret;
    }
}