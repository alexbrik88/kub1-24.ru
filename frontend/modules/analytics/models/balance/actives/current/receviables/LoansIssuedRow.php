<?php namespace frontend\modules\analytics\models\balance\actives\current\receviables;

use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use frontend\modules\analytics\models\balance\BalanceRow;
use frontend\modules\analytics\models\balance\BalanceRowInterface;

class LoansIssuedRow extends BalanceRow implements BalanceRowInterface {

    public static string $label  = 'Займы выданные и проценты к получению';
    public static string $getter = self::class;

    /**
     * @param int $Y
     * @return array
     */
    public function getYearData(int $Y): array
    {
        return $this->__getAllWalletsByItem($Y,
            InvoiceIncomeItem::ITEM_BORROWING_REDEMPTION,
            InvoiceExpenditureItem::ITEM_BORROWING_EXTRADITION,
            true);
    }
}