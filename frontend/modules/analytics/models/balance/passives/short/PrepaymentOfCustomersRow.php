<?php namespace frontend\modules\analytics\models\balance\passives\short;

use common\components\helpers\ArrayHelper;
use common\models\cash\CashFlowsBase;
use frontend\models\Documents;
use frontend\modules\analytics\models\balance\BalanceRow;
use frontend\modules\analytics\models\balance\BalanceRowInterface;

class PrepaymentOfCustomersRow extends BalanceRow implements BalanceRowInterface {

    public static string $label  = 'Предоплата покупателей (авансы)';
    public static string $getter = self::class;

    /**
     * @param int $Y
     * @return array
     */
    public function getYearData(int $Y): array
    {
        $byDocs = $this->__getPrepaidByDocs($Y, Documents::IO_TYPE_OUT);
        $byFlows = $this->__getPrepaidByFlows($Y, CashFlowsBase::FLOW_TYPE_INCOME);

        $ret = [];
        foreach (self::EMPTY_YEAR as $month => $amount) {
            $ret[$month] = ArrayHelper::getValue($byDocs, $month, 0);
            $ret[$month] += ArrayHelper::getValue($byFlows, $month, 0);
        }

        return $ret;
    }
}