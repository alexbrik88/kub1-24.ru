<?php namespace frontend\modules\analytics\models\balance\passives\short;

use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceExpenditureItem;
use frontend\modules\analytics\models\balance\BalanceRow;
use frontend\modules\analytics\models\balance\BalanceRowInterface;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;
use Yii;
use yii\db\Expression;
use yii\db\Query;

class BudgetArrearsRow extends BalanceRow implements BalanceRowInterface {

    public static string $label  = 'Задолженность перед бюджетом';
    public static string $getter = self::class;

    /**
     * @param int $Y
     * @return array
     */
    public function getYearData(int $Y): array
    {
        $palModel = self::getPalModel();
        $taxYears = $palModel->getResultYears();

        $taxes = $this->__getTaxes($taxYears, $palModel);
        $paidTaxes = $this->__getPaidTaxes($taxYears, $palModel);

        $ret = [];
        $growingTax = 0;
        foreach ($taxes as $yearMonth => $taxAmount) {
            $year = substr($yearMonth, 0, 4);
            $month = substr($yearMonth, 4, 2);
            $monthTax = $taxAmount + $growingTax;
            $growingTax = $monthTax;

            // collapse paid taxes
            foreach ($paidTaxes as $paidYear => $codes)
                foreach ($codes as $code => $periods)
                    foreach ($periods as $period => $taxPile)
                        if ($taxPile['amount'] > 0 && $yearMonth >= $taxPile['fromYM']) {
                            if ($taxPile['amount'] >= $growingTax) {
                                $paidTaxes[$year][$code][$period]['amount'] -= $growingTax;
                                $growingTax = 0;
                            } else {
                                $growingTax -= $taxPile['amount'];
                                $paidTaxes[$year][$code][$period]['amount'] = 0;
                            }
                        }

            if ($Y == date('Y') && $month <= date('m'))
                if ($year == $Y)
                    $ret[intval($month)] = $growingTax;
        }

        return $ret;
    }

    protected function __getTaxes($years, ProfitAndLossSearchModel $model)
    {
        $taxes = [];

        foreach ($years as $year) {
            for ($i=1; $i<=12; $i++) {
                $monthNumber = str_pad($i, 2, "0", STR_PAD_LEFT);
                $taxes[$year.$monthNumber] = $model->getValue('tax', $year, $monthNumber, 'amount');
            }
        }

        return $taxes;
    }

    protected function __getPaidTaxes($years, ProfitAndLossSearchModel $model)
    {
        $paidTaxes = [];
        foreach ($years as $year) {
            $paidTaxes[$year] = [
                'ГД' => [
                    '00' => ['amount' => 0, 'fromYM' => "{$year}01"],
                ],
                'ПЛ' => [
                    '01' => ['amount' => 0, 'fromYM' => "{$year}01"],
                    '02' => ['amount' => 0, 'fromYM' => "{$year}07"],
                ],
                'КВ' => [
                    '01' => ['amount' => 0, 'fromYM' => "{$year}01"],
                    '02' => ['amount' => 0, 'fromYM' => "{$year}04"],
                    '03' => ['amount' => 0, 'fromYM' => "{$year}07"],
                    '04' => ['amount' => 0, 'fromYM' => "{$year}10"],
                ],
                'МС' => [
                    '01' => ['amount' => 0, 'fromYM' => "{$year}01"],
                    '02' => ['amount' => 0, 'fromYM' => "{$year}02"],
                    '03' => ['amount' => 0, 'fromYM' => "{$year}03"],
                    '04' => ['amount' => 0, 'fromYM' => "{$year}04"],
                    '05' => ['amount' => 0, 'fromYM' => "{$year}05"],
                    '06' => ['amount' => 0, 'fromYM' => "{$year}06"],
                    '07' => ['amount' => 0, 'fromYM' => "{$year}07"],
                    '08' => ['amount' => 0, 'fromYM' => "{$year}08"],
                    '09' => ['amount' => 0, 'fromYM' => "{$year}09"],
                    '10' => ['amount' => 0, 'fromYM' => "{$year}10"],
                    '11' => ['amount' => 0, 'fromYM' => "{$year}11"],
                    '12' => ['amount' => 0, 'fromYM' => "{$year}12"],
                ],
            ];
        }

        $taxFlows = (new Query)
            ->select(new Expression('
                    t.tax_period_code,
                    t.amount
                '))
            ->from(['t' => CashBankFlows::tableName()])
            ->where(['t.company_id' => $this->_multiCompanyIds])
            ->andWhere(['t.flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
            ->andWhere(['t.expenditure_item_id' => [
                InvoiceExpenditureItem::ITEM_TAX_AND_PROFIT,
                InvoiceExpenditureItem::ITEM_TAX_USN_6,
                InvoiceExpenditureItem::ITEM_TAX_USN_15
            ]])
            ->all(Yii::$app->db2);

        foreach ($taxFlows as $flow) {

            $code  = mb_substr($flow['tax_period_code'], 0, 2);
            $part = mb_substr($flow['tax_period_code'], 3, 2);
            $year  = mb_substr($flow['tax_period_code'], 6, 4);

            if ($code && $part && $year) {

                if ($code == 'ГД' && isset($paidTaxes[$year][$code]['00']))
                    $paidTaxes[$year][$code]['00']['amount'] += $flow['amount'];
                elseif ($code == 'ПЛ' && isset($paidTaxes[$year][$code][$part]))
                    $paidTaxes[$year][$code][$part]['amount'] += $flow['amount'];
                elseif ($code == 'КВ' && isset($paidTaxes[$year][$code][$part]))
                    $paidTaxes[$year][$code][$part]['amount'] += $flow['amount'];
                elseif (isset($paidTaxes[$year]['МС'][$part]))
                    $paidTaxes[$year]['МС'][$part]['amount'] += $flow['amount'];
            }
        }

        return $paidTaxes;
    }
}