<?php namespace frontend\modules\analytics\models\balance\passives\short;

use frontend\modules\analytics\models\balance\BalanceRow;
use frontend\modules\analytics\models\balance\BalanceRowInterface;

class SalaryArrearsRow extends BalanceRow implements BalanceRowInterface {

    public static string $label  = 'Задолженность по зарплате';
    public static string $getter = self::class;

    /**
     * @param int $Y
     * @return array
     */
    public function getYearData(int $Y): array
    {
        return [];
    }
}