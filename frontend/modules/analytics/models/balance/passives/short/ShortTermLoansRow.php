<?php namespace frontend\modules\analytics\models\balance\passives\short;

use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use frontend\modules\analytics\models\balance\BalanceRow;
use frontend\modules\analytics\models\balance\BalanceRowInterface;

class ShortTermLoansRow extends BalanceRow implements BalanceRowInterface {

    public static string $label  = 'Кредиты и займы сроком до 12 месяцев';
    public static string $getter = self::class;

    /**
     * @param int $Y
     * @return array
     */
    public function getYearData(int $Y): array
    {
        return $this->__getAllWalletsByItem($Y,
            [InvoiceIncomeItem::ITEM_LOAN, InvoiceIncomeItem::ITEM_CREDIT],
            [InvoiceExpenditureItem::ITEM_REPAYMENT_CREDIT, InvoiceExpenditureItem::ITEM_LOAN_REPAYMENT]);
    }

    public function getInitialData(string $onDate): array
    {
        return ['amount' => null, 'date' => $onDate];
    }
}