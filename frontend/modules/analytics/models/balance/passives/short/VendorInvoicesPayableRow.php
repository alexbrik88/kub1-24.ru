<?php namespace frontend\modules\analytics\models\balance\passives\short;

use frontend\models\Documents;
use frontend\modules\analytics\models\balance\BalanceRow;
use frontend\modules\analytics\models\balance\BalanceRowInterface;

class VendorInvoicesPayableRow extends BalanceRow implements BalanceRowInterface {

    public static string $label  = 'Задолженность перед поставщиками';
    public static string $getter = self::class;

    /**
     * @param int $Y
     * @return array
     */
    public function getYearData(int $Y): array
    {
        return $this->__getOverdueInvoices($Y, Documents::IO_TYPE_IN);
    }
}