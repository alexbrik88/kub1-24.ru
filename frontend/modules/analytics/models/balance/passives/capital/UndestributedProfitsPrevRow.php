<?php namespace frontend\modules\analytics\models\balance\passives\capital;

use frontend\modules\analytics\models\balance\BalanceInitial;
use frontend\modules\analytics\models\balance\BalanceRow;
use frontend\modules\analytics\models\balance\BalanceRowInterface;
use yii\helpers\ArrayHelper;

class UndestributedProfitsPrevRow extends BalanceRow implements BalanceRowInterface {

    public static string $label  = 'Прошлые периоды';
    public static string $getter = self::class;

    /**
     * @param int $Y
     * @return array
     */
    public function getYearData(int $Y): array
    {
        try {
            $prevProfits = $this->__calcUndestributedProfits($Y - 1);
        } catch (\Exception $e) {
            $prevProfits = self::EMPTY_YEAR;
        }

        $ret = [];
        for ($i=1; $i<=12; $i++) {
            $ret[$i] = ArrayHelper::getValue($prevProfits, 12, 0);
            if ($Y == date('Y') && $i == date('m'))
                break;
        }

        return $ret;
    }

    public function getInitialData(string $onDate): array
    {
        $ba = BalanceInitial::findOne(['company_id' => $this->_multiCompanyIds[0]]);

        return ($ba) ? [
            'amount' => $ba->undistributed_profit,
            'date' => $ba->date
        ] : self::EMPTY_INITIAL;
    }
}