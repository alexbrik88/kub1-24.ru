<?php namespace frontend\modules\analytics\models\balance\passives\capital;

use frontend\modules\analytics\models\balance\BalanceRow;
use frontend\modules\analytics\models\balance\BalanceRowInterface;
use yii\helpers\ArrayHelper;

class UndestributedProfitsRow extends BalanceRow implements BalanceRowInterface {

    public static string $label  = 'Текущий год';
    public static string $getter = self::class;

    /**
     * @param int $Y
     * @return array
     */
    public function getYearData(int $Y): array
    {
        try {
            $profits = $this->__calcUndestributedProfits($Y);
            $prevProfits = $this->__calcUndestributedProfits($Y - 1);
        } catch (\Exception $e) {
            $profits = $prevProfits = [];
        }

        $ret = [];
        foreach (self::EMPTY_YEAR as $month => $zeroes) {
            $ret[$month] =
                ArrayHelper::getValue($profits, $month, 0) -
                ArrayHelper::getValue($prevProfits, 12, 0);

            if ($Y == date('Y') && $month == date('m'))
                break;
        }

        return $ret;
    }
}