<?php namespace frontend\modules\analytics\models\balance\passives\capital;

use common\models\Company;
use frontend\modules\analytics\models\balance\BalanceInitial;
use frontend\modules\analytics\models\balance\BalanceRow;
use frontend\modules\analytics\models\balance\BalanceRowInterface;

class AuthorizedCapitalRow extends BalanceRow implements BalanceRowInterface {

    public static string $label  = 'Уставный капитал';
    public static string $getter = self::class;

    /**
     * @param int $Y
     * @return array
     */
    public function getYearData(int $Y): array
    {
        $capital = Company::find()
            ->where(['id' => $this->_multiCompanyIds])
            ->sum('capital');
        $monthsCount = ($Y == date('Y'))
            ? date('n')
            : 12;

        return array_fill(1, $monthsCount, $capital);
    }

    public function getInitialData(string $onDate): array
    {
        $capital = Company::find()
            ->where(['id' => $this->_multiCompanyIds])
            ->sum('capital');
        // always one primary company
        $date = ($ba = BalanceInitial::findOne(['company_id' => $this->_multiCompanyIds[0]]))
            ? $ba->date
            : null;

        return [
            'amount' => $capital,
            'date' => $date
        ];
    }
}