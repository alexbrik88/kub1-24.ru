<?php namespace frontend\modules\analytics\models\balance\passives\long;

use frontend\modules\analytics\models\balance\BalanceRow;
use frontend\modules\analytics\models\balance\BalanceRowInterface;

class LongTermLoansRow extends BalanceRow implements BalanceRowInterface {

    public static string $label  = 'Кредиты и займы сроком более 12 месяцев';
    public static string $getter = self::class;

    /**
     * @param int $Y
     * @return array
     */
    public function getYearData(int $Y): array
    {
        return [];
    }

    public function getInitialData(string $onDate): array
    {
        return ['amount' => null, 'date' => $onDate];
    }
}