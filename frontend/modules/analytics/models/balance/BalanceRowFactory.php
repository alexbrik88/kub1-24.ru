<?php namespace frontend\modules\analytics\models\balance;

class BalanceRowFactory {

    public static function build(string $className, array $companiesIds): BalanceRow
    {
        $className = (class_exists($className))
            ? $className
            : BalanceRow::class;

        $object = (new $className);
        $object->setCompany($companiesIds);

        return $object;
    }
}