<?php namespace frontend\modules\analytics\models\balance;

interface BalanceRowInterface {

    public function setCompany(array $multiCompanyIds): void;
    public function getYearData(int $Y): array;
    public function getInitialData(string $onDate): array;

}