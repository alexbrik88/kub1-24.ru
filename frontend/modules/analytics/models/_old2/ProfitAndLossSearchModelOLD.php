<?php
namespace frontend\modules\analytics\models\_old2;

use common\components\date\DateHelper;
use common\components\excel\Excel;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\models\Contractor;

/**
 * Class ProfitAndLossSearchModel
 * @package frontend\modules\analytics\models
 */
class ProfitAndLossSearchModelOLD extends AbstractFinance
{
    use profitAndLoss\BaseOlapTrait;
    use profitAndLoss\RevenueTrait;
    use profitAndLoss\FixedCostsTrait;
    use profitAndLoss\OperatingCostsTrait;
    use profitAndLoss\AnotherIncomeTrait;
    use profitAndLoss\AnotherCostsTrait;
    use profitAndLoss\VariableCostsTrait;
    use profitAndLoss\ReceivedPercentTrait;
    use profitAndLoss\PaymentPercentTrait;
    use profitAndLoss\PaymentDividendTrait;
    use profitAndLoss\ChartsTrait;
    use profitAndLoss\CalculatedRowsTrait;

    const MIN_REPORT_DATE = '2010-01-01';

    const TAB_ALL_OPERATIONS = 10;
    const TAB_ACCOUNTING_OPERATIONS_ONLY = 11;

    // modals params
    public $incomeItemIdManyItem;
    public $expenditureItemIdManyItem;
    public $recognitionDateManyItem;

    public static $items = [
        [
            'label' => 'Выручка',
            'addCheckboxX' => true,
            'getter' => 'getRevenue',
            'options' => [
                'class' => 'text-bold expenditure_type',
                'data-children' => 'revenue',
            ],
        ],
        [
            'label' => 'Переменные расходы',
            'addCheckboxX' => true,
            'getter' => 'getVariableCosts',
            'options' => [
                'class' => 'text-bold expenditure_type expenses-block',
                'data-block' => ProfitAndLossCompanyItem::VARIABLE_EXPENSE_TYPE,
                'data-children' => 'variable-costs',
            ]
        ],
        [
            'label' => 'Маржинальный доход',
            'getter' => 'getMarginalIncome',
            'options' => [
                'class' => 'text-bold expenditure_type',
            ],
        ],
        [
            'label' => 'Маржинальность',
            'getter' => 'getMarginality',
            'suffix' => '%',
            'options' => [
                'class' => 'expenditure_type',
            ],
        ],
        [
            'label' => 'Постоянные расходы',
            'addCheckboxX' => true,
            'getter' => 'getFixedCosts',
            'options' => [
                'class' => 'text-bold expenditure_type expenses-block',
                'data-block' => ProfitAndLossCompanyItem::CONSTANT_EXPENSE_TYPE,
                'data-children' => 'fixed-costs',
            ],
        ],
        [
            'label' => 'Валовая прибыль',
            'getter' => 'getGrossProfit',
            'options' => [
                'class' => 'text-bold expenditure_type',
            ],
        ],
        [
            'label' => 'Операционные расходы',
            'addCheckboxX' => true,
            'getter' => 'getOperatingCosts',
            'options' => [
                'class' => 'text-bold expenditure_type expenses-block',
                'data-block' => ProfitAndLossCompanyItem::OPERATING_EXPENSE_TYPE,
                'data-children' => 'operating-costs',
            ],
        ],
        [
            'label' => 'Операционная прибыль',
            'getter' => 'getOperatingProfit',
            'options' => [
                'class' => 'text-bold expenditure_type',
            ],
        ],
        [
            'label' => 'Другие доходы',
            'addCheckboxX' => true,
            'getter' => 'getAnotherIncome',
            'options' => [
                'class' => 'text-bold expenditure_type',
                'data-children' => 'another-income',
            ],
        ],
        [
            'label' => 'Другие расходы',
            'addCheckboxX' => true,
            'getter' => 'getAnotherCosts',
            'options' => [
                'class' => 'text-bold expenditure_type expenses-block',
                'data-block' => ProfitAndLossCompanyItem::OTHER_EXPENSE_TYPE,
                'data-children' => 'another-costs',
            ],
        ],
        [
            'label' => 'EBITDA',
            'getter' => 'getEbidta',
            'options' => [
                'class' => 'text-bold expenditure_type',
            ],
        ],
        [
            'label' => 'Проценты полученные',
            'getter' => 'getReceivedPercent',
            'options' => [
                'class' => 'expenditure_type',
            ],
        ],
        [
            'label' => 'Проценты уплаченные',
            'getter' => 'getPaymentPercent',
            'options' => [
                'class' => 'expenditure_type',
            ],
        ],
        [
            'label' => 'Прибыль до налогообложения',
            'getter' => 'getProfitBeforeTax',
            'options' => [
                'class' => 'expenditure_type',
            ],
        ],
        [
            'label' => 'Налог на прибыль',
            'getter' => 'getTax',
            'options' => [
                'class' => 'expenditure_type',
            ],
        ],
        [
            'label' => 'Дивиденды',
            'getter' => 'getPaymentDividend',
            'addCheckboxX' => true,
            'options' => [
                'class' => 'expenditure_type',
            ],
            'isGetterOnly' => true,
        ],
        [
            'label' => 'Чистая прибыль/убыток',
            'getter' => 'getNetIncomeLoss',
            'options' => [
                'class' => 'text-bold expenditure_type',
            ],
        ],
        [
            'label' => 'Рентабельность по чистой прибыли',
            'getter' => 'getProfitabilityByNetIncomeLoss',
            'suffix' => '%',
            'options' => [
                'class' => 'expenditure_type',
            ],
        ],
        [
            'getter' => 'getPaymentDividend',
        ],
        [
            'label' => 'Нераспределенная прибыль',
            'getter' => 'getUndestributedProfits',
            'options' => [
                'class' => 'expenditure_type',
            ],
        ],
    ];

    /**
     * @var
     */
    public $dateFrom;
    /**
     * @var
     */
    public $dateTo;

    /**
     * @var
     */
    protected $_yearFilter;
    /**
     * @var
     */
    protected $_month;
    /**
     * @var array
     */
    protected $data = [];
    /**
     * @var
     */
    protected $_minCashYearMonth;

    /**
     * @var array
     */
    protected static $notAccountingContractors = [];

    /**
     * @var array
     */
    protected $result = []; // total
    protected $resultByCompany = []; // per company

    /**
     *
     */
    public function init() {

        $this->_minCashYearMonth = date('Ym');

        return parent::init();
    }

    public static function getCachePrefix($className, $companyId)
    {
        $className = explode('\\', $className);
        $className = array_pop($className);
        if ('get' == substr($className, 0, 3))
            $className = substr($className, 3);
        if ('Trait' == substr($className, -5))
            $className = substr($className, 0, -5);

        return lcfirst($className) .'_'. $companyId . '_';
    }

    /**
     * @return array
     */
    public function handleItems()
    {
        self::$notAccountingContractors = $this->_getNotAccountingContractors();

        $years = array_reverse($this->getYearFilter());
/*
        foreach ($years as $year) {

            $this->result[$year] = [];
            foreach (static::$items as $item) {
                $getter = ArrayHelper::remove($item, 'getter');
                $item = self::_getBlankParent($item);

                if (isset($item['isGetterOnly'])) {
                    $this->$getter($item, $year);
                    continue;
                }

                $this->result[$year] = array_merge($this->result[$year], $this->$getter($item, $year));
            }

        }
*/

        foreach ($this->multiCompanyIds as $companyId) {
            $this->resultByCompany[$companyId] = [];
            foreach ($years as $year) { // cycle need for calc taxes per year
                $this->resultByCompany[$companyId][$year] = [];
                foreach (static::$items as $item) {

                    $getter = ArrayHelper::remove($item, 'getter');
                    $item = self::_getBlankParent($item);

                    $rowData = call_user_func_array(
                        [$this, $getter],
                        [$item, $year, $companyId]
                    );

                    if (isset($item['isGetterOnly'])) {
                        // add results in cache to calc some values
                        continue;
                    }

                    $this->resultByCompany[$companyId][$year] = array_merge($this->resultByCompany[$companyId][$year], $rowData);
                }
            }
        }

        // todo: move to method "sumResult()"
        foreach ($this->resultByCompany as $companyId => $level1)
        {
            foreach ($level1 as $year => $level2)
            {
                foreach ($level2 as $rowKey => $data)
                {
                    if (!isset($this->result[$year])) {
                        $this->result[$year] = [];
                    }

                    if (!isset($this->result[$year][$rowKey]))
                    {
                        // parent item
                        $this->result[$year][$rowKey] = [
                            'label' => $data['label'],
                            'addCheckboxX' => $data['addCheckboxX'] ?? false,
                            'options' => $data['options'] ?? [],
                            'suffix' => $data['suffix'] ?? null,
                            'amount' => $data['amount'],
                            'amountTax' => $data['amountTax'],
                            'items' => []
                        ];

                        // item
                        if (!$this->result[$year][$rowKey]['addCheckboxX']) {
                            $this->result[$year][$rowKey]['data-block'] = $data['data-block'] ?? null;
                            $this->result[$year][$rowKey]['data-article'] = $data['data-article'] ?? null;
                            $this->result[$year][$rowKey]['data-movement-type'] = $data['data-movement-type'] ?? null;
                        }

                        // sub items
                        if (!empty($data['items'])) {
                            $this->result[$year][$rowKey]['items'] = $data['items'];
                        }

                    } else {

                        foreach ($data['amount'] as $amountKey => $amountValue) {
                            $this->result[$year][$rowKey]['amount'][$amountKey] += $amountValue;
                        }
                        foreach ($data['amountTax'] as $amountKey => $amountValue) {
                            $this->result[$year][$rowKey]['amountTax'][$amountKey] += $amountValue;
                        }
                        if (!empty($data['items'])) {
                            // subitems unique per company
                            $this->result[$year][$rowKey]['items'] = $data['items'];
                        }
                    }
                }
            }
        }

        return $this->result[$this->year];
    }

    /**
     * For external use in BalanceSearch
     */
    public function getResultYears()
    {
        return array_keys($this->result);
    }

    /**
     * For use in Charts
     */
    public function getProfitAndLossItem($name, $year)
    {
        if (isset($this->result[$year]) && isset($this->result[$year][$name]))
            return $this->result[$year][$name];

        return [];
    }

    /**
     * For external use in BalanceSearch & charts
     */
    public function getRow($name, $year, $amountKey = 'amount')
    {
        if (isset($this->result[$year]) && isset($this->result[$year][$name]) && isset($this->result[$year][$name][$amountKey]))
            return $this->result[$year][$name][$amountKey];

        return [];
    }

    /**
     * For external use in BalanceSearch & charts
     */
    public function getValue($name, $year, $month, $amountKey = 'amount')
    {
        $month = str_pad($month, 2, "0", STR_PAD_LEFT);

        if (isset($this->result[$year]) &&
            isset($this->result[$year][$name]) &&
            isset($this->result[$year][$name][$amountKey]) &&
            isset($this->result[$year][$name][$amountKey][$month]))

                return $this->result[$year][$name][$amountKey][$month];

        return 0;
    }

    public function getFromCurrentMonthsPeriods($left, $right, $offsetYear = 0, $offsetMonth = "0") {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month")->modify("{$offsetMonth} month");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr->format('Y-m-01'),
                'to' => $curr->format('Y-m-t'),
            ];
            $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        if (empty($this->_yearFilter)) {
            $range = [];
            $cashOrderMinDate = CashOrderFlows::find()
                ->byCompany($this->multiCompanyIds)
                ->min('date');
            $cashBankMinDate = CashBankFlows::find()
                ->byCompany($this->multiCompanyIds)
                ->min('date');
            $cashEmoneyMinDate = CashEmoneyFlows::find()
                ->byCompany($this->multiCompanyIds)
                ->min('date');
            $minDates = [];

            if ($cashOrderMinDate) $minDates[] = $cashOrderMinDate;
            if ($cashBankMinDate) $minDates[] = $cashBankMinDate;
            if ($cashEmoneyMinDate) $minDates[] = $cashEmoneyMinDate;

            $minCashDate = !empty($minDates) ? max(self::MIN_REPORT_DATE, min($minDates)) : date(DateHelper::FORMAT_DATE);
            $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
            $currentYear = date('Y');
            foreach (range($registrationYear, $currentYear) as $value) {
                $range[$value] = $value;
            }
            arsort($range);
            $this->_yearFilter = $range;
            $this->_minCashYearMonth = DateHelper::format($minCashDate, 'Ym', DateHelper::FORMAT_DATE);
        }

        return $this->_yearFilter;
    }

    /**
     * @param $items
     * @return mixed
     */
    private function calculateQuarterAndTotalAmount($items)
    {
        // clear before calc
        foreach ($items as $ki => $data) {
            foreach (['amount', 'amountTax'] as $ka) {

                if (!isset($data[$ka])) {
                    continue;
                }

                foreach ($data[$ka] as $month => $oneAmount) {
                    if (in_array($month, ['quarter-1', 'quarter-2', 'quarter-3', 'quarter-4', 'total']))
                        unset($items[$ki][$ka][$month]);
                }
            }
        }

        foreach ($items as $ki => $data) {
            foreach (['amount', 'amountTax'] as $ka) {

                if (!isset($data[$ka])) {
                    continue;
                }

                foreach ($data[$ka] as $month => $oneAmount) {

                    if ((int)$month < 1 || (int)$month > 12)
                        continue;

                    if (!isset($items[$ki][$ka]['quarter-' . ceil($month / 3)])) {
                        $items[$ki][$ka]['quarter-' . ceil($month / 3)] = $oneAmount;
                    } else {
                        $items[$ki][$ka]['quarter-' . ceil($month / 3)] += $oneAmount;
                    }
                    if (!isset($items[$ki][$ka]['total'])) {
                        $items[$ki][$ka]['total'] = $oneAmount;
                    } else {
                        $items[$ki][$ka]['total'] += $oneAmount;
                    }
                }
            }
        }

        return $items;
    }

    private static function _getBlankParent($item)
    {
        foreach (self::$month as $monthNumber => $monthText) {
            $item['amount'][$monthNumber] = null;
            $item['amountTax'][$monthNumber] = null;
        }

        return $item;
    }

    private static function _getBlankItem($class, $name, $dataBlock = null, $dataArticle = null, $movementType = null)
    {
        return [
            'label' => htmlspecialchars($name),
            'options' => [
                'class' => 'hidden ' . $class,
                'style' => 'padding-left: 25px;'
            ],
            'data-block' => $dataBlock,
            'data-article' => $dataArticle,
            'data-movement-type' => $movementType,
            'amount' =>
                ['01' => 0, '02' => 0, '03' => 0, '04' => 0, '05' => 0, '06' => 0, '07' => 0, '08' => 0, '09' => 0, '10' => 0, '11' => 0, '12' => 0] +
                ['quarter-1' => 0, 'quarter-2' => 0, 'quarter-3' => 0, 'quarter-4' => 0] +
                ['total' => 0],
            'amountTax' =>
                ['01' => 0, '02' => 0, '03' => 0, '04' => 0, '05' => 0, '06' => 0, '07' => 0, '08' => 0, '09' => 0, '10' => 0, '11' => 0, '12' => 0] +
                ['quarter-1' => 0, 'quarter-2' => 0, 'quarter-3' => 0, 'quarter-4' => 0] +
                ['total' => 0],
            'items' => []
        ];
    }

    public function getMinCashYearMonth()
    {
        if (empty($this->_yearFilter))
            $this->getYearFilter();

        return $this->_minCashYearMonth;
    }

    /// XLS

    public function generateXls($type)
    {
        $data = $this->handleItems();

        // header
        $columns = [];
        $columns[] = [
            'attribute' => 'itemName',
            'header' => 'Статьи',
        ];
        foreach (AbstractFinance::$month as $monthNumber => $monthText) {
            $columns[] = [
                'attribute' => "item{$monthNumber}",
                'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                'header' => $monthText,
            ];
        }
        $columns[] = [
            'attribute' => 'dataYear',
            'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
            'header' => $this->year,
        ];

        // data
        $formattedData = [];
        foreach ($data as $row) {
            $formattedData[] = $this->_buildXlsRow($row, $type);
        }

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $formattedData,
            'title' => "ОПиУ за {$this->year} год",
            'rangeHeader' => range('A', 'N'),
            'columns' => $columns,
            'format' => 'Xlsx',
            'fileName' => "ОПиУ за {$this->year} год",
        ]);
    }

    protected function _buildXlsRow($data, $type)
    {
        $amountKey = ($type == self::TAB_ACCOUNTING_OPERATIONS_ONLY) ? 'amountTax' : 'amount';
        $row = [
            'itemName' => trim(strip_tags($data['label'] ?? '')),
            'dataYear' => ($data[$amountKey]['total'] ?? '0.00') / 100
        ];
        foreach (AbstractFinance::$month as $monthNumber => $monthText) {
            $row['item' . $monthNumber] = ($data[$amountKey][$monthNumber] ?? '0.00') / 100;
        }

        return $row;
    }
}