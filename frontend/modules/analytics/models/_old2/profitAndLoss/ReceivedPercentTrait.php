<?php

namespace frontend\modules\analytics\models\_old2\profitAndLoss;

use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceIncomeItem;
use yii\db\Expression;

trait ReceivedPercentTrait {

    /**
     * @param $parentItem
     * @param $year
     * @return mixed
     */
    public function getReceivedPercent($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__TRAIT__, $companyId);

        if (!array_key_exists($cachePrefix . $year, $this->data)) {

            $ONLY_INCOME_ITEMS = [InvoiceIncomeItem::ITEM_RECEIVED_PERCENTS];

            $flows = $this->getUndestributedFlows($companyId, CashFlowsBase::FLOW_TYPE_INCOME, $ONLY_INCOME_ITEMS);

            foreach ($this->_yearFilter as $byYear) {

                $parent = $parentItem;

                foreach ($flows as $value) {
                    $month = $value['m'];
                    if ($byYear == $value['y']) {
                        foreach (['amount', 'amountTax'] as $amountKey) {
                            $parent[$amountKey][$month] += $value[$amountKey];
                        }
                    }
                }

                $parent['data-article'] = 'income-' . InvoiceIncomeItem::ITEM_RECEIVED_PERCENTS;
                $parent['data-movement-type'] = self::MOVEMENT_TYPE_FLOWS;

                $this->data[$cachePrefix . $byYear] = $this->calculateQuarterAndTotalAmount(['receivedPercent' => $parent]);
            }
        }

        return $this->data[$cachePrefix . $year];
    }

}