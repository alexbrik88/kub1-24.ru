<?php

namespace frontend\modules\analytics\models\_old2\profitAndLoss;

use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceIncomeItem;
use common\models\product\Product;
use common\modules\acquiring\models\AcquiringOperation;
use common\modules\cards\models\CardOperation;
use frontend\models\Documents;
use frontend\modules\cash\models\CashContractorType;
use frontend\modules\analytics\models\AbstractFinance;
use common\models\Contractor;
use frontend\modules\analytics\models\PlanCashFlows;
use yii\db\Expression;

trait ChartsTrait {

    /**
     * @param $dateFrom
     * @param $dateTo
     * @param string $customAmountKey
     * @return array
     */
    public function getChartRevenueByClients($dateFrom, $dateTo, $customAmountKey = 'amount')
    {
        $result = [];

        $fact = $this->_getChartRevenueByClientsFact($dateFrom, $dateTo);
        $plan = $this->_getChartRevenueByClientsPlan($dateFrom, $dateTo);

        foreach ($fact as $c => $r) {
            $result[$c] = [
                'name' => ($contractor = Contractor::findOne($c)) ? $contractor->getTitle(true) : '--',
                'amountFact' => round ($r[$customAmountKey] / 100, 2),
                'amountPlan' => 0
            ];
        }

        foreach ($plan as $c => $r) {
            if (!isset($result[$c])) {
                $result[$c] = [
                    'name' => ($contractor = Contractor::findOne($c)) ? $contractor->getTitle(true) : '--',
                    'amountFact' => 0,
                    'amountPlan' => 0
                ];
            }

            $result[$c]['amountPlan'] += round($r[$customAmountKey] / 100, 2);
        }

        uasort($result, function ($a, $b) {
            if ($a['amountFact'] == $b['amountFact']) return 0;
            return ($a['amountFact'] > $b['amountFact']) ? -1 : 1;
        });

        foreach ($result as &$res)
            if ($res['amountFact'] == 0 && $res['amountPlan'] == 0)
                unset($res);

        return $result ?: [['name' => 'Нет данных', 'amountFact' => 0, 'amountPlan' => 0]];
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @param string $customAmountKey
     * @return array
     */
    public function getChartMarginByProducts($dateFrom, $dateTo, $customAmountKey = 'amount')
    {
        $result = [];

        $fact = $this->_getChartMarginByProducts($dateFrom, $dateTo);

        $customSellKey  = ('amount' == $customAmountKey) ? '_sell'  : '_sellTax';
        $customBuyKey  = ('amount' == $customAmountKey) ? '_buy'  : '_buyTax';
        $customQuantityKey = ('amount' == $customAmountKey) ? 'quantity' : 'quantityTax';

        foreach ($fact as $p => $r) {
            $result[$p] = [
                'name' => ($product = Product::findOne($p)) ? $product->title : '--',
                'margin' => round ($r[$customAmountKey] / 100, 2),
                'quantity' => (float)$r[$customQuantityKey],
                'marginPercent' =>  ($r[$customBuyKey]) > 0 ?
                    round (100 * ((float)$r[$customSellKey] - (float)$r[$customBuyKey]) / (float)$r[$customBuyKey], 0) : "-"
            ];
        }

        uasort($result, function ($a, $b) {
            if ($a['margin'] == $b['margin']) return 0;
            return ($a['margin'] > $b['margin']) ? -1 : 1;
        });

        foreach ($result as &$res)
            if ($res['margin'] == 0)
                unset($res);

        return $result ?: [['name' => 'Нет данных', 'margin' => 0, 'quantity' => 0]];
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @return array
     */
    private function _getChartRevenueByClientsFact($dateFrom, $dateTo)
    {
        $result = [];

        //////////// BY DOC ////////////
        /// сумма всех Актов, ТН и УПД, дата которых в нужном месяце
        foreach (self::docClassArray() as $docsAlias => $docClassName) {
            $t = $docClassName::tableName();

            /** @var  $docClassName Act */
            $docData = Invoice::find()
                ->joinWith($docsAlias, false)
                ->select(["{$t}.id", "{$t}.document_date", 'invoice.contractor_id'])
                ->andWhere(['invoice.company_id' => $this->multiCompanyIds])
                ->andWhere(['invoice.type' => Documents::IO_TYPE_OUT])
                ->andWhere(['between', "{$t}.document_date", $dateFrom, $dateTo])
                ->indexBy('id')
                ->asArray()
                ->all(\Yii::$app->db2);

            $orderTable = AbstractFinance::$docOrdersTables[$docsAlias];

            $amount = $orderTable['class']::find()
                ->leftJoin('order', "{$orderTable['name']}.order_id = `order`.id")
                ->where([$orderTable['key'] => array_column($docData, 'id')])
                ->select(new Expression("{$orderTable['key']}, SUM(`order`.selling_price_with_vat * {$orderTable['name']}.quantity) sum"))
                ->groupBy($orderTable['key'])
                ->indexBy($orderTable['key'])
                ->asArray()
                ->all(\Yii::$app->db2);

            array_walk($docData, function(&$v, $k) use ($amount) { $v['sum'] = $amount[$k]['sum'] ?? 0; });

            foreach ($docData as $doc) {

                $contractorId = $doc['contractor_id'];

                if (!isset($result[$contractorId]))
                    $result[$contractorId] = [
                        'amount' => 0,
                        'amountTax' => 0
                    ];

                $result[$contractorId]['amount'] += $doc['sum'];

                // amount taxable
                $isAccounting = !in_array($doc['contractor_id'], self::$notAccountingContractors);
                if ($isAccounting) {
                    $result[$contractorId]['amountTax'] += $doc['sum'];
                }
            }
        }

        //////////// BY NO DOC ////////////
        ///  1) есть привязка к счетам, у которых НЕТ Актов/ТН/УПД или у них стоит "Без Акта/ТН/УПД" и "Дата признания дохода" в нужном месяце +
        ///  2) нет привязки к счетам и "Дата признания дохода" нужном месяце
        foreach (self::flowClassArray() as $flowClassName) {
            $flowTableName = $flowClassName::tableName();

            $query = $flowClassName::find()
                ->andWhere(["{$flowClassName::tableName()}.company_id" => $this->multiCompanyIds])
                ->andWhere(["{$flowClassName::tableName()}.flow_type" => CashFlowsBase::FLOW_TYPE_INCOME])
                ->andWhere(['not', ['in', 'income_item_id', BaseOlapTrait::getExceptUndestributedIncomeItems()]])
                ->select([
                    new Expression("DISTINCT({$flowTableName}.id)"),
                    "{$flowTableName}.income_item_id as item_id",
                    "{$flowTableName}.amount as sum",
                    "{$flowTableName}.recognition_date",
                    "{$flowTableName}.contractor_id",
                    $flowClassName == (CashBankFlows::class || AcquiringOperation::class || CardOperation::class) ?
                        new Expression("1 AS is_accounting") : "{$flowTableName}.is_accounting"
                ])
                ->andWhere(['between', 'recognition_date', $dateFrom, $dateTo]);

            if (method_exists($flowClassName, 'getInvoices')) {

                $query->joinWith(['invoices'], false)
                    ->andWhere(['or',
                        ['invoice.id' => null],
                        ['or',
                            ['and',
                                ['not', ['invoice.id' => null]],
                                ['invoice.has_act' => 0],
                                ['invoice.has_packing_list' => 0],
                                ['invoice.has_upd' => 0],
                            ],
                            ['and',
                                ['not', ['invoice.id' => null]],
                                ['or',
                                    ['invoice.need_act' => 0],
                                    ['invoice.need_packing_list' => 0],
                                    ['invoice.need_upd' => 0],
                                ]
                            ],
                        ]
                    ]);
            }

            $flows = $query->asArray()->all(\Yii::$app->db2);

            foreach ($flows as $flow) {

                $contractorId = $flow['contractor_id'];

                if (!isset($result[$contractorId]))
                    $result[$contractorId] = [
                        'amount' => 0,
                        'amountTax' => 0
                    ];

                $result[$contractorId]['amount'] += $flow['sum'];

                // amount taxable
                if ($flow['is_accounting']) {
                    $isContractorAccounting = !in_array($flow['contractor_id'], self::$notAccountingContractors);
                    if ($isContractorAccounting || $flowClassName == CashBankFlows::class) {
                        $result[$contractorId]['amountTax'] += $flow['sum'];
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @return array
     */
    private function _getChartRevenueByClientsPlan($dateFrom, $dateTo)
    {
        $result = [];

        $exceptContractors = [
            CashContractorType::BANK_TEXT,
            CashContractorType::ORDER_TEXT,
            CashContractorType::EMONEY_TEXT,
            CashContractorType::BALANCE_TEXT,
        ];

        $flows = PlanCashFlows::find()
            ->select(['contractor_id', 'income_item_id', 'payment_type', 'amount'])
            ->andWhere(['company_id' => $this->multiCompanyIds])
            ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
            ->andWhere(['between', 'date', $dateFrom, $dateTo])
            ->andWhere(['not', ['contractor_id' => $exceptContractors]])
            ->andWhere(['not', ['income_item_id' => InvoiceIncomeItem::ITEM_OWN_FOUNDS]])
            ->asArray()
            ->all(\Yii::$app->db2);

        foreach ($flows as $flow) {
            $contractorId = $flow['contractor_id'];
            if (!isset($result[$contractorId])) {
                $result[$contractorId] = [
                    'amount' => 0,
                    'amountTax' => 0
                ];
            }
            $result[$contractorId]['amount'] += $flow['amount'];

            // amount taxable
            $isContractorAccounting = !in_array($flow['contractor_id'], self::$notAccountingContractors);
            if ($isContractorAccounting || $flow['payment_type'] == PlanCashFlows::PAYMENT_TYPE_BANK) {
                $result[$contractorId]['amountTax'] += $flow['amount'];
            }
        }

        return $result;
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @return array
     */
    private function _getChartMarginByProducts($dateFrom, $dateTo)
    {
        $result = [];

        //////////// BY DOC ////////////
        /// сумма всех Актов, ТН и УПД, дата которых в нужном месяце
        foreach (self::docClassArray() as $docsAlias => $docClassName) {
            $t = $docClassName::tableName();

            /** @var  $docClassName Act */
            $docData = Invoice::find()
                ->joinWith($docsAlias, false)
                ->select(["{$t}.id", "{$t}.document_date", 'invoice.contractor_id'])
                ->andWhere(['invoice.company_id' => $this->multiCompanyIds])
                ->andWhere(['invoice.type' => Documents::IO_TYPE_OUT])
                ->andWhere(['between', "{$t}.document_date", $dateFrom, $dateTo])
                ->indexBy('id')
                ->asArray()
                ->all(\Yii::$app->db2);

            $orderTable = AbstractFinance::$docOrdersTables[$docsAlias];

            $amount = $orderTable['class']::find()
                ->leftJoin('order', "{$orderTable['name']}.order_id = `order`.id")
                ->leftJoin('product', "{$orderTable['name']}.product_id = `product`.id")
                ->where([$orderTable['key'] => array_column($docData, 'id')])
                ->select(new Expression("
                    {$orderTable['key']},
                    GROUP_CONCAT({$orderTable['name']}.quantity SEPARATOR ',') quantity,
                    GROUP_CONCAT(`order`.selling_price_with_vat * {$orderTable['name']}.quantity SEPARATOR ',') amounts_sell,
                    GROUP_CONCAT(`product`.price_for_buy_with_nds * {$orderTable['name']}.quantity SEPARATOR ',') amounts_buy,
                    GROUP_CONCAT({$orderTable['name']}.product_id SEPARATOR ',') product_ids"))
                ->groupBy($orderTable['key'])
                ->indexBy($orderTable['key'])
                ->asArray()
                ->all(\Yii::$app->db2);

            array_walk($docData, function(&$v, $k) use ($amount) {
                $v['sum'] = $amount[$k]['sum'] ?? 0;
                $v['product_ids'] = $amount[$k]['product_ids'] ?? "";
                $v['amounts_sell'] = $amount[$k]['amounts_sell'] ?? "";
                $v['amounts_buy']  = $amount[$k]['amounts_buy']  ?? "";
                $v['quantity']  = $amount[$k]['quantity']  ?? "";
            });

            foreach ($docData as $doc) {

                $productIds = explode(',', $doc['product_ids']);
                $productAmountsSell = explode(',', $doc['amounts_sell']);
                $productAmountsBuy = explode(',', $doc['amounts_buy']);
                $quantities = explode(',', $doc['quantity']);

                foreach ($productIds as $idx => $productId) {

                    if (!isset($result[$productId]))
                        $result[$productId] = [
                            'amount' => 0,
                            'amountTax' => 0,
                            'quantity' => 0,
                            'quantityTax' => 0,
                            '_sell' => 0,
                            '_sellTax' => 0,
                            '_buy' => 0,
                            '_buyTax' => 0,
                        ];

                    $sellPrice = (int)ArrayHelper::getValue($productAmountsSell, $idx, 0);
                    $buyPrice = (int)ArrayHelper::getValue($productAmountsBuy, $idx, 0);
                    $quantity = ArrayHelper::getValue($quantities, $idx, 0);

                    $result[$productId]['amount'] += $sellPrice - $buyPrice;
                    $result[$productId]['quantity'] += $quantity;
                    $result[$productId]['_sell'] += $sellPrice;
                    $result[$productId]['_buy'] += $buyPrice;

                    // amount taxable
                    $isAccounting = !in_array($doc['contractor_id'], self::$notAccountingContractors);
                    if ($isAccounting) {
                        $result[$productId]['amountTax'] += $sellPrice - $buyPrice;
                        $result[$productId]['quantityTax'] += $quantity;
                        $result[$productId]['_sellTax'] += $sellPrice;
                        $result[$productId]['_buyTax'] += $buyPrice;
                    }
                }
            }
        }

        //////////// BY NO DOC ////////////
        ///  1) есть привязка к счетам, у которых НЕТ Актов/ТН/УПД или у них стоит "Без Акта/ТН/УПД" и "Дата признания дохода" в нужном месяце +
        foreach (self::flowClassArray() as $flowClassName) {
            $flowTableName = $flowClassName::tableName();

            if (!method_exists($flowClassName, 'getInvoices'))
                continue;

            $flows = $flowClassName::find()
                ->andWhere(["{$flowClassName::tableName()}.company_id" => $this->multiCompanyIds])
                ->andWhere(["{$flowClassName::tableName()}.flow_type" => CashFlowsBase::FLOW_TYPE_INCOME])
                ->andWhere(['not', ['in', 'income_item_id', BaseOlapTrait::getExceptUndestributedIncomeItems()]])
                ->select([
                    new Expression("DISTINCT({$flowTableName}.id)"),
                    "{$flowTableName}.contractor_id",
                    "order.product_id",
                    "(`order`.quantity) as quantity",
                    "(`order`.selling_price_with_vat * `order`.quantity) as sum_sell",
                    "(`product`.price_for_buy_with_nds * `order`.quantity) as sum_buy",
                    $flowClassName == CashBankFlows::class ?
                        new Expression("1 AS is_accounting") : "{$flowTableName}.is_accounting"
                ])
                ->joinWith([
                    'invoices.orders.product',
                ], false)
                ->andWhere(['between', 'recognition_date', $dateFrom, $dateTo])
                ->andWhere(['or',
                    ['and',
                        ['not', ['invoice.id' => null]],
                        ['invoice.has_act' => 0],
                        ['invoice.has_packing_list' => 0],
                        ['invoice.has_upd' => 0],
                    ],
                    ['and',
                        ['not', ['invoice.id' => null]],
                        ['or',
                            ['invoice.need_act' => 0],
                            ['invoice.need_packing_list' => 0],
                            ['invoice.need_upd' => 0],
                        ]
                    ],
                ])
                ->asArray()
                ->all(\Yii::$app->db2);

            foreach ($flows as $flow) {

                $productId = $flow['product_id'];

                if (!isset($result[$productId]))
                    $result[$productId] = [
                        'amount' => 0,
                        'amountTax' => 0,
                        'quantity' => 0,
                        'quantityTax' => 0,
                        '_sell' => 0,
                        '_sellTax' => 0,
                        '_buy' => 0,
                        '_buyTax' => 0,
                    ];

                $result[$productId]['amount'] += (int)$flow['sum_sell'] - (int)$flow['sum_buy'];
                $result[$productId]['quantity'] += (float)$flow['quantity'];
                $result[$productId]['_sell'] += (int)$flow['sum_sell'];
                $result[$productId]['_buy'] += (int)$flow['sum_buy'];

                // amount taxable
                if ($flow['is_accounting']) {
                    $isContractorAccounting = !in_array($flow['contractor_id'], self::$notAccountingContractors);
                    if ($isContractorAccounting || $flowClassName == CashBankFlows::class) {
                        $result[$productId]['amountTax'] += (int)$flow['sum_sell'] - (int)$flow['sum_buy'];
                        $result[$productId]['quantityTax'] += (float)$flow['quantity'];
                        $result[$productId]['_sellTax'] += (int)$flow['sum_sell'];
                        $result[$productId]['_buyTax'] += (int)$flow['sum_buy'];
                    }
                }
            }
        }

        return $result;
    }
}