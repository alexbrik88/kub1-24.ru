<?php

namespace frontend\modules\analytics\models\_old2\profitAndLoss;

use common\models\cash\CashBankFlows;
use common\models\cash\CashBankFlowToInvoice;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyFlowToInvoice;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderFlowToInvoice;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use frontend\models\Documents;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\models\ProfitAndLossCompanyItem;
use yii\db\Expression;
use yii\db\Query;

/// docs: сумма входящих Актов, ТН и УПД, у которых указанные статьи и дата которых попадает в нужный месяц
/// flows: есть привязка к счетам, у которых НЕТ Актов/ТН/УПД или у них стоит "Без Акта/ТН/УПД" и "Дата признания дохода" в нужном нам месяце +
/// flows: нет привязки к счетам и "Дата признания дохода" нужном месяце
///
/// docs2+flows2: если в "Оплата поставщику" покупка Товара, то выводим, если закуплен товар который с галочкой "Не для перепродажи"
/// docs2+flows2: если в "Оплата поставщику" покупка Услуги

trait OperatingCostsTrait {

    /**
     * @param $parentItem
     * @param $year
     * @return mixed
     */
    public function getOperatingCosts($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__TRAIT__, $companyId);

        if (!array_key_exists($cachePrefix . $year, $this->data)) {

            $companyItems = $this->getCompanyExpenditureItems($companyId, ProfitAndLossCompanyItem::OPERATING_EXPENSE_TYPE, AbstractFinance::FINANCIAL_OPERATIONS_BLOCK);

            //$ONLY_EXPENDITURE_ITEMS = array_keys($companyItems);
            $ONLY_EXPENDITURE_ITEMS = self::getArticlesWithChildren(array_keys($companyItems), CashFlowsBase::FLOW_TYPE_EXPENSE, $companyId);

            $docs = $this->getUndestributedDocs($companyId, Documents::IO_TYPE_IN, $ONLY_EXPENDITURE_ITEMS);
            $flows = $this->getUndestributedFlows($companyId, CashFlowsBase::FLOW_TYPE_EXPENSE, $ONLY_EXPENDITURE_ITEMS);

            $subArticles = self::getArticles2Parents(CashFlowsBase::FLOW_TYPE_EXPENSE, $companyId);

            if ($docs2 = $this->__getSuppliersOperatingCostsByDocs($companyId))
                $docs = array_merge($docs, $docs2);

            if ($flows2 = $this->__getSuppliersOperatingCostsByFlows($companyId))
                $flows = array_merge($flows, $flows2);

            foreach ($this->_yearFilter as $byYear) {

                $parent = $parentItem;
                $result = [];

                foreach ($docs as $value) {
                    $month = $value['m'];
                    $item = $value['item_id'];
                    $key = "expense-{$item}";

                    $subItem = $subKey = null;
                    if (isset($subArticles[$item])) {
                        $subItem = $item;
                        $subKey = $key;
                        $item = $subArticles[$item];
                        $key = "expense-{$item}";
                    }
                    
                    if ($byYear == $value['y']) {

                        if (!isset($result[$key])) {
                            $companyItem = $companyItems[$item]['item'] ?? null;
                            $itemName = $companyItem['name'] ?? ($item == InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT ? 'Оплата поставщику' : (InvoiceExpenditureItem::findOne($item)->name ?? "item_id={$item}"));
                            $result[$key] = self::_getBlankItem('operating-costs expenses-block', $itemName, null, $key, self::MOVEMENT_TYPE_DOCS_AND_FLOWS);
                            $result[$key]['canDrag'] = $companyItem['can_update'] ?? false;
                            $result[$key]['data-block'] = ProfitAndLossCompanyItem::OPERATING_EXPENSE_TYPE;
                            $result[$key]['data-id'] = $item;
                        }

                        if ($subItem) {
                            if (!isset($result[$key]['items'][$subKey])) {
                                $companyItem = $companyItems[$subItem]['item'] ?? null;
                                $subItemName = $companyItem['name'] ?? InvoiceExpenditureItem::findOne($subItem)->name ?? "item_id={$subItem}";
                                $result[$key]['items'][$subKey] = self::_getBlankItem('operating-costs expenses-block', $subItemName, null, $subKey, self::MOVEMENT_TYPE_DOCS_AND_FLOWS);
                                $result[$key]['items'][$subKey]['canDrag'] = $companyItem['can_update'] ?? false;
                                $result[$key]['items'][$subKey]['data-block'] = ProfitAndLossCompanyItem::OPERATING_EXPENSE_TYPE;
                                $result[$key]['items'][$subKey]['data-id'] = $subItem;
                            }

                            foreach (['amount', 'amountTax'] as $amountKey) {
                                $result[$key]['items'][$subKey][$amountKey][$month] += $value[$amountKey];
                            }
                        }

                        foreach (['amount', 'amountTax'] as $amountKey) {
                            $result[$key][$amountKey][$month] += $value[$amountKey];
                            $parent[$amountKey][$month] += $value[$amountKey];
                        }
                    }
                }

                foreach ($flows as $value) {
                    $month = $value['m'];
                    $item = $value['item_id'];
                    $key = "expense-{$item}";

                    $subItem = $subKey = null;
                    if (isset($subArticles[$item])) {
                        $subItem = $item;
                        $subKey = $key;
                        $item = $subArticles[$item];
                        $key = "expense-{$item}";
                    }
                    
                    if ($byYear == $value['y']) {

                        if (!isset($result[$key])) {
                            $companyItem = $companyItems[$item]['item'] ?? null;
                            $itemName = $companyItem['name'] ?? InvoiceExpenditureItem::findOne($item)->name ?? "item_id={$item}";
                            $result[$key] = self::_getBlankItem('operating-costs expenses-block', $itemName, null, $key, self::MOVEMENT_TYPE_DOCS_AND_FLOWS);
                            $result[$key]['canDrag'] = $companyItem['can_update'] ?? false;
                            $result[$key]['data-block'] = ProfitAndLossCompanyItem::OPERATING_EXPENSE_TYPE;
                            $result[$key]['data-id'] = $item;
                        }

                        if ($subItem) {
                            if (!isset($result[$key]['items'][$subKey])) {
                                $companyItem = $companyItems[$subItem]['item'] ?? null;
                                $subItemName = $companyItem['name'] ?? InvoiceExpenditureItem::findOne($subItem)->name ?? "item_id={$subItem}";
                                $result[$key]['items'][$subKey] = self::_getBlankItem('operating-costs expenses-block', $subItemName, null, $subKey, self::MOVEMENT_TYPE_DOCS_AND_FLOWS);
                                $result[$key]['items'][$subKey]['canDrag'] = $companyItem['can_update'] ?? false;
                                $result[$key]['items'][$subKey]['data-block'] = ProfitAndLossCompanyItem::OPERATING_EXPENSE_TYPE;
                                $result[$key]['items'][$subKey]['data-id'] = $subItem;
                            }

                            foreach (['amount', 'amountTax'] as $amountKey) {
                                $result[$key]['items'][$subKey][$amountKey][$month] += $value[$amountKey];
                            }
                        }

                        foreach (['amount', 'amountTax'] as $amountKey) {
                            $result[$key][$amountKey][$month] += $value[$amountKey];
                            $parent[$amountKey][$month] += $value[$amountKey];
                        }
                    }
                }

                $result = ['totalOperatingCosts' => $parent] + $result;
                $this->data[$cachePrefix . $byYear] = $this->calculateQuarterAndTotalAmount($result);
            }

        }

        return $this->data[$cachePrefix . $year];
    }
}