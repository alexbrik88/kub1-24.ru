<?php

namespace frontend\modules\analytics\models\_old2\profitAndLoss;

use common\models\cash\CashFlowsBase;
use frontend\models\Documents;

/// docs: сумма всех Актов, ТН и УПД, дата которых в нужном месяце
/// flows: есть привязка к счетам, у которых НЕТ Актов/ТН/УПД или у них стоит "Без Акта/ТН/УПД" и "Дата признания дохода" в нужном месяце +
/// flows: нет привязки к счетам и "Дата признания дохода" нужном месяце

trait RevenueTrait {

    /**
     * @param $parentItem
     * @param $year
     * @return mixed
     */
    public function getRevenue($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__TRAIT__, $companyId);

        if (!array_key_exists($cachePrefix . $year, $this->data)) {

            $docs = $this->getUndestributedDocs($companyId, Documents::IO_TYPE_OUT);
            $flows = $this->getUndestributedFlows($companyId, CashFlowsBase::FLOW_TYPE_INCOME);

            foreach ($this->_yearFilter as $byYear) {

                $parent = $parentItem;
                $result = [
                    'revenueByDocs' => self::_getBlankItem('revenue', 'Выручка по документам', null, 'income-revenue_doc', self::MOVEMENT_TYPE_DOCS),
                    'revenueByNoDocs' => self::_getBlankItem('revenue', 'Выручка без документов', null, 'income-revenue_flow', self::MOVEMENT_TYPE_FLOWS)
                ];

                foreach ($docs as $value) {
                    $month = $value['m'];
                    if ($byYear == $value['y']) {
                        foreach (['amount', 'amountTax'] as $amountKey) {
                            $result["revenueByDocs"][$amountKey][$month] += $value[$amountKey];
                            $parent[$amountKey][$month] += $value[$amountKey];
                        }
                    }
                }

                foreach ($flows as $value) {
                    $month = $value['m'];
                    if ($byYear == $value['y']) {
                        foreach (['amount', 'amountTax'] as $amountKey) {
                            $result["revenueByNoDocs"][$amountKey][$month] += $value[$amountKey];
                            $parent[$amountKey][$month] += $value[$amountKey];
                        }
                    }
                }

                $result = ['totalRevenue' => $parent] + $result;

                $this->data[$cachePrefix . $byYear] = $this->calculateQuarterAndTotalAmount($result);
            }
        }

        return $this->data[$cachePrefix . $year];
    }
}