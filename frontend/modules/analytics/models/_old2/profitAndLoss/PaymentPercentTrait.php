<?php

namespace frontend\modules\analytics\models\_old2\profitAndLoss;

use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceExpenditureItem;
use yii\db\Expression;

trait PaymentPercentTrait {

    /**
     * @param $parentItem
     * @param $year
     * @return mixed
     */
    public function getPaymentPercent($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__TRAIT__, $companyId);

        if (!array_key_exists($cachePrefix . $year, $this->data)) {

            $ONLY_EXPENDITURE_ITEMS = [InvoiceExpenditureItem::ITEM_PAYMENT_PERCENT];

            $flows = $this->getUndestributedFlows($companyId, CashFlowsBase::FLOW_TYPE_EXPENSE, $ONLY_EXPENDITURE_ITEMS);

            foreach ($this->_yearFilter as $byYear) {

                $parent = $parentItem;

                foreach ($flows as $value) {
                    $month = $value['m'];
                    if ($byYear == $value['y']) {
                        foreach (['amount', 'amountTax'] as $amountKey) {
                            $parent[$amountKey][$month] += $value[$amountKey];
                        }
                    }
                }

                $parent['data-article'] = 'expense-' . InvoiceExpenditureItem::ITEM_PAYMENT_PERCENT;
                $parent['data-movement-type'] = self::MOVEMENT_TYPE_FLOWS;

                $this->data[$cachePrefix . $byYear] = $this->calculateQuarterAndTotalAmount(['paymentPercent' => $parent]);
            }
        }

        return $this->data[$cachePrefix . $year];
    }

}