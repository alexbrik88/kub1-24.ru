<?php
namespace frontend\modules\analytics\models\_old2\profitAndLoss;

use common\models\product\ProductTurnover;
use Yii;
use common\models\Contractor;
use common\models\product\Product;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\models\ProfitAndLossCompanyItem;

/// docs: сумма входящих Актов, ТН и УПД, у которых указанные статьи и дата которых попадает в нужный месяц
/// flows: есть привязка к счетам, у которых НЕТ Актов/ТН/УПД или у них стоит "Без Акта/ТН/УПД" и "Дата признания дохода" в нужном нам месяце +
/// flows: нет привязки к счетам и "Дата признания дохода" нужном месяце

trait VariableCostsTrait {

      public function getVariableCosts($parentItem, $year, $companyId)
      {
          $cachePrefix = self::getCachePrefix(__TRAIT__, $companyId);

          if (!array_key_exists($cachePrefix . $year, $this->data)) {

              $dateFrom = $this->getOlapFromDate();
              $dateTo = $this->getOlapToDate();

              $docs = $this->__getPrimeCostsByDocs($companyId);
              $flows = $this->__getPrimeCostsByFlows($companyId);

              foreach ($this->_yearFilter as $byYear) {

                  $resultKey = 'primeCost';
                  $parent = $parentItem;
                  $result = [$resultKey => self::_getBlankItem('variable-costs expenses-block', 'Себестоимость товаров и услуг', ProfitAndLossCompanyItem::VARIABLE_EXPENSE_TYPE)];

                  foreach ($docs as $value) {
                      $month = $value['m'];
                      if ($byYear == $value['y']) {
                          foreach (['amount', 'amountTax'] as $amountKey) {
                              $result[$resultKey][$amountKey][$month] += $value[$amountKey];
                              $parent[$amountKey][$month] += $value[$amountKey];
                          }
                      }
                  }

                  foreach ($flows as $value) {
                      $month = $value['m'];
                      if ($byYear == $value['y']) {
                          foreach (['amount', 'amountTax'] as $amountKey) {
                              $result[$resultKey][$amountKey][$month] += $value[$amountKey];
                              $parent[$amountKey][$month] += $value[$amountKey];
                          }
                      }
                  }

                  $result = ['totalVariableCosts' => $parent] + $result;

                  $this->data[$cachePrefix . $byYear] = $this->calculateQuarterAndTotalAmount($result);
              }
          }

          return $this->data[$cachePrefix . $year];
    }
}
