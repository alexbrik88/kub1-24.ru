<?php

namespace frontend\modules\analytics\models\_old2\profitAndLoss;
use common\models\company\CompanyTaxationType;

trait CalculatedRowsTrait {

     /**
     * @param $parentItem
     * @return mixed
     */
    public function getUndestributedProfits($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__netIncomeLoss = self::getCachePrefix('getNetIncomeLoss', $companyId);
        $__paymentDividend = self::getCachePrefix('paymentDividend', $companyId);

        $netIncomeLoss = current($this->data[$__netIncomeLoss . $year]);
        $paymentDividend = current($this->data[$__paymentDividend . $year]);

        // clear before calc
        foreach ([&$netIncomeLoss, &$paymentDividend] as $ki => &$data) {
            foreach (['quarter-1', 'quarter-2', 'quarter-3', 'quarter-4', 'total'] as $key) {

                if (isset($data['amount'][$key])) unset($data['amount'][$key]);
                if (isset($data['amountTax'][$key])) unset($data['amountTax'][$key]);
            }
        }

        foreach ($netIncomeLoss['amount'] as $key => $amount) {
            $parentItem['amount'][$key] = $netIncomeLoss['amount'][$key] - $paymentDividend['amount'][$key];
            if ($key - 1 > 0) {
                $parentItem['amount'][$key] += $parentItem['amount'][str_pad($key - 1, 2, "0", STR_PAD_LEFT)];
            } else {
                // january current + december prev
                if (isset($this->data[$cachePrefix . ($year - 1)])) {
                    $prevUndestributedProfit = current($this->data[$cachePrefix . ($year - 1)]);
                    $parentItem['amount'][$key] += $prevUndestributedProfit['amount']["total"];
                }
            }
        }

        foreach ($netIncomeLoss['amountTax'] as $key => $amount) {
            $parentItem['amountTax'][$key] = $netIncomeLoss['amountTax'][$key] - $paymentDividend['amountTax'][$key];
            if ($key - 1 > 0) {
                $parentItem['amountTax'][$key] += $parentItem['amountTax'][str_pad($key - 1, 2, "0", STR_PAD_LEFT)];
            } else {
                // january current + december prev
                if (isset($this->data[$cachePrefix . ($year - 1)])) {
                    $prevUndestributedProfit = current($this->data[$cachePrefix . ($year - 1)]);
                    $parentItem['amountTax'][$key] += $prevUndestributedProfit['amountTax']["total"];
                }

            }
        }

        // floor quarters/total
        foreach ([1 => "03", 2 => "06", 3 => "09", 4 => "12"] as $q=>$m) {
            $parentItem['amount']['quarter-'.$q] = $parentItem['amount'][$m];
            $parentItem['amountTax']['quarter-'.$q] = $parentItem['amountTax'][$m];
        }
        $parentItem['amount']['total'] = $parentItem['amount']["12"];
        $parentItem['amountTax']['total'] = $parentItem['amountTax']["12"];

        $this->data[$cachePrefix . $year]['undestributedProfits'] = $parentItem;

        return $this->data[$cachePrefix . $year];
    }

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getOperatingProfit($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__grossProfit = self::getCachePrefix('getGrossProfit', $companyId);
        $__operatingCosts = self::getCachePrefix(OperatingCostsTrait::class, $companyId);

        $grossProfitData = current($this->data[$__grossProfit . $year]);
        $operatingCostsData = current($this->data[$__operatingCosts . $year]);
        foreach (self::$month as $monthNumber => $monthText) {
            $parentItem['amount'][$monthNumber] = $grossProfitData['amount'][$monthNumber] - $operatingCostsData['amount'][$monthNumber];
            $parentItem['amountTax'][$monthNumber] = $grossProfitData['amountTax'][$monthNumber] - $operatingCostsData['amountTax'][$monthNumber];
        }
        $this->data[$cachePrefix . $year] = $this->calculateQuarterAndTotalAmount(['operatingProfit' => $parentItem]);

        return $this->data[$cachePrefix . $year];
    }

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getProfitabilityByOperatingProfit($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__revenue = self::getCachePrefix(RevenueTrait::class, $companyId);
        $__operatingProfit = self::getCachePrefix('getOperatingProfit', $companyId);

        $revenueData = current($this->data[$__revenue . $year]);
        $operatingProfitData = current($this->data[$__operatingProfit . $year]);
        foreach ($revenueData['amount'] as $key => $amount) {
            $revenueAmount = $amount > 0 ? $amount : 1;
            $parentItem['amount'][$key] = round($operatingProfitData['amount'][$key] / $revenueAmount * 10000, 2);
        }
        foreach ($revenueData['amountTax'] as $key => $amount) {
            $revenueAmount = $amount > 0 ? $amount : 1;
            $parentItem['amountTax'][$key] = round($operatingProfitData['amountTax'][$key] / $revenueAmount * 10000, 2);
        }
        $this->data[$cachePrefix . $year]['profitabilityByOperatingProfit'] = $parentItem;

        return $this->data[$cachePrefix . $year];
    }

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getEbidta($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__operatingProfit = self::getCachePrefix('getOperatingProfit', $companyId);
        $__anotherIncome = self::getCachePrefix(AnotherIncomeTrait::class, $companyId);
        $__anotherCosts = self::getCachePrefix(AnotherCostsTrait::class, $companyId);

        $operatingProfitData = current($this->data[$__operatingProfit . $year]);
        $anotherIncomeData = current($this->data[$__anotherIncome . $year]);
        $anotherCostsData = current($this->data[$__anotherCosts . $year]);

        foreach (self::$month as $monthNumber => $monthText) {
            $parentItem['amount'][$monthNumber] = $operatingProfitData['amount'][$monthNumber] + $anotherIncomeData['amount'][$monthNumber] - $anotherCostsData['amount'][$monthNumber];
            $parentItem['amountTax'][$monthNumber] = $operatingProfitData['amountTax'][$monthNumber] + $anotherIncomeData['amountTax'][$monthNumber] - $anotherCostsData['amountTax'][$monthNumber];
        }

        $this->data[$cachePrefix . $year] = $this->calculateQuarterAndTotalAmount(['ebitda' => $parentItem]);

        return $this->data[$cachePrefix . $year];
    }

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getNetIncomeLoss($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__profitBeforeTax = self::getCachePrefix('getProfitBeforeTax', $companyId);
        $__tax = self::getCachePrefix('getTax', $companyId);

        $profitBeforeTax = current($this->data[$__profitBeforeTax . $year]);
        $taxData = current($this->data[$__tax . $year]);
        
        foreach ($profitBeforeTax['amount'] as $key => $amount) {
            $parentItem['amount'][$key] = $profitBeforeTax['amount'][$key] - $taxData['amount'][$key];
        }
        foreach ($profitBeforeTax['amount'] as $key => $amount) {
            $parentItem['amountTax'][$key] = $profitBeforeTax['amountTax'][$key] - $taxData['amountTax'][$key];
        }
        $this->data[$cachePrefix . $year]['netIncomeLoss'] = $parentItem;

        return $this->data[$cachePrefix . $year];
    }

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getProfitabilityByNetIncomeLoss($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__revenue = self::getCachePrefix(RevenueTrait::class, $companyId);
        $__netIncomeLoss = self::getCachePrefix('getNetIncomeLoss', $companyId);

        $revenueData = current($this->data[$__revenue . $year]);
        $netIncomeLossData = current($this->data[$__netIncomeLoss . $year]);
        foreach ($revenueData['amount'] as $key => $amount) {
            $revenueAmount = $amount > 0 ? $amount : 9E9;
            $parentItem['amount'][$key] = round($netIncomeLossData['amount'][$key] / $revenueAmount * 100 * 100, 2);
        }
        foreach ($revenueData['amountTax'] as $key => $amount) {
            $revenueAmount = $amount > 0 ? $amount : 9E9;
            $parentItem['amountTax'][$key] = round($netIncomeLossData['amountTax'][$key] / $revenueAmount * 100 * 100, 2);
        }
        $this->data[$cachePrefix . $year]['profitabilityByNetIncomeLoss'] = $parentItem;

        return $this->data[$cachePrefix . $year];
    }

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getGrossProfit($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__marginalIncome = self::getCachePrefix('getMarginalIncome', $companyId);
        $__fixedCosts = self::getCachePrefix(FixedCostsTrait::class, $companyId);

        $marginalData = current($this->data[$__marginalIncome . $year]);
        $fixedCostsData = current($this->data[$__fixedCosts . $year]);
        foreach (self::$month as $monthNumber => $monthText) {
            $parentItem['amount'][$monthNumber] = $marginalData['amount'][$monthNumber] - $fixedCostsData['amount'][$monthNumber];
            $parentItem['amountTax'][$monthNumber] = $marginalData['amountTax'][$monthNumber] - $fixedCostsData['amountTax'][$monthNumber];
        }
        $this->data[$cachePrefix . $year] = $this->calculateQuarterAndTotalAmount(['grossProfit' => $parentItem]);

        return $this->data[$cachePrefix . $year];
    }

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getProfitabilityByGrossProfit($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__revenue = self::getCachePrefix(RevenueTrait::class, $companyId);
        $__grossProfit = self::getCachePrefix('getGrossProfit', $companyId);

        $revenueData = current($this->data[$__revenue . $year]);
        $grossProfitData = current($this->data[$__grossProfit . $year]);
        foreach ($revenueData['amount'] as $key => $amount) {
            $revenueAmount = $amount > 0 ? $amount : 1;
            $parentItem['amount'][$key] = round($grossProfitData['amount'][$key] / $revenueAmount * 10000, 2);
        }
        foreach ($revenueData['amountTax'] as $key => $amount) {
            $revenueAmount = $amount > 0 ? $amount : 1;
            $parentItem['amountTax'][$key] = round($grossProfitData['amountTax'][$key] / $revenueAmount * 10000, 2);
        }
        $this->data[$cachePrefix . $year]['profitabilityByGrossProfit'] = $parentItem;

        return $this->data[$cachePrefix . $year];
    }

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getMarginalIncome($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__revenue = self::getCachePrefix(RevenueTrait::class, $companyId);
        $__variableCosts = self::getCachePrefix(VariableCostsTrait::class, $companyId);

        $revenueData = current($this->data[$__revenue . $year]);
        $variableCostsData = current($this->data[$__variableCosts . $year]);
        foreach (self::$month as $monthNumber => $monthText) {
            $parentItem['amount'][$monthNumber] = $revenueData['amount'][$monthNumber] - $variableCostsData['amount'][$monthNumber];
            $parentItem['amountTax'][$monthNumber] = $revenueData['amountTax'][$monthNumber] - $variableCostsData['amountTax'][$monthNumber];
        }
        $this->data[$cachePrefix . $year] = $this->calculateQuarterAndTotalAmount(['marginalIncome' => $parentItem]);

        return $this->data[$cachePrefix . $year];
    }

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getMarginality($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__revenue = self::getCachePrefix(RevenueTrait::class, $companyId);
        $__marginalIncome = self::getCachePrefix('getMarginalIncome', $companyId);

        $revenueData = current($this->data[$__revenue . $year]);
        $marginalIncomeData = current($this->data[$__marginalIncome . $year]);
        foreach ($revenueData['amount'] as $key => $amount) {
            $revenueAmount = $amount > 0 ? $amount : 9E9;
            $parentItem['amount'][$key] = round($marginalIncomeData['amount'][$key] / $revenueAmount * 10000, 2);
        }
        foreach ($revenueData['amountTax'] as $key => $amount) {
            $revenueAmount = $amount > 0 ? $amount : 9E9;
            $parentItem['amountTax'][$key] = round($marginalIncomeData['amountTax'][$key] / $revenueAmount * 10000, 2);
        }
        $this->data[$cachePrefix . $year]['marginality'] = $parentItem;

        return $this->data[$cachePrefix . $year];
    }

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getProfitBeforeTax($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__ebidta = self::getCachePrefix('getEbidta', $companyId);
        $__paymentPercent = self::getCachePrefix(PaymentPercentTrait::class, $companyId);
        $__receivedPercent = self::getCachePrefix(ReceivedPercentTrait::class, $companyId);

        //  "Прибыль до налогообложения" = EBIDTA + "Проценты полученные" - "Проценты уплаченные""
        $ebitda = current($this->data[$__ebidta . $year]);
        $paymentPercent = current($this->data[$__paymentPercent . $year]);
        $receivedPercent = current($this->data[$__receivedPercent . $year]);

        foreach (self::$month as $monthNumber => $monthText) {
            $parentItem['amount'][$monthNumber] = $ebitda['amount'][$monthNumber] + ($receivedPercent['amount'][$monthNumber] - $paymentPercent['amount'][$monthNumber]);
            $parentItem['amountTax'][$monthNumber] = $ebitda['amountTax'][$monthNumber] + ($receivedPercent['amountTax'][$monthNumber] - $paymentPercent['amountTax'][$monthNumber]);
        }
        $this->data[$cachePrefix . $year] = $this->calculateQuarterAndTotalAmount(['profitBeforeTax' => $parentItem]);

        return $this->data[$cachePrefix . $year];
    }

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getTax($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__FUNCTION__, $companyId);
        $__revenue = self::getCachePrefix(RevenueTrait::class, $companyId);
        $__ebidta = self::getCachePrefix('getEbidta', $companyId);
        $__profitBeforeTax = self::getCachePrefix('getProfitBeforeTax', $companyId);
        
        $companyTaxationType = $this->multiCompanyManager->getCompanyTaxationType($companyId);

        if ($companyTaxationType['usn']) {
            if ($companyTaxationType['usn_type'] == CompanyTaxationType::INCOME) {
                $revenueData = current($this->data[$__revenue . $year]);
                foreach ($revenueData['amountTax'] as $key => $amount) {
                    $parentItem['amount'][$key] = max(0, round($revenueData['amountTax'][$key] * $companyTaxationType['usn_percent'] / 100, 2));
                    $parentItem['amountTax'][$key] = max(0, round($revenueData['amountTax'][$key] * $companyTaxationType['usn_percent'] / 100, 2));
                }
            } else {
                $profitBeforeTax = current($this->data[$__profitBeforeTax . $year]);
                foreach ($profitBeforeTax['amountTax'] as $key => $amount) {
                    $parentItem['amount'][$key] = max(0, round($profitBeforeTax['amountTax'][$key] * $companyTaxationType['usn_percent'] / 100, 2));
                    $parentItem['amountTax'][$key] = max(0, round($profitBeforeTax['amountTax'][$key] * $companyTaxationType['usn_percent'] / 100, 2));
                }
            }
        } elseif ($companyTaxationType['osno']) {
            $ebidta = current($this->data[$__ebidta . $year]);
            foreach ($ebidta['amountTax'] as $key => $amount) {
                $parentItem['amount'][$key] = max(0, round($ebidta['amountTax'][$key] * 20 / 100, 2));
                $parentItem['amountTax'][$key] = max(0, round($ebidta['amountTax'][$key] * 20 / 100, 2));
            }
        }

        $this->data[$cachePrefix . $year] = $this->calculateQuarterAndTotalAmount(['tax' => $parentItem]);

        return $this->data[$cachePrefix . $year];
    }
}