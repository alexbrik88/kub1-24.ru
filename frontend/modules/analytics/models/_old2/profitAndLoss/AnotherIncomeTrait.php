<?php

namespace frontend\modules\analytics\models\_old2\profitAndLoss;

use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceIncomeItem;
use yii\db\Expression;

trait AnotherIncomeTrait {

    /**
     * @param $parentItem
     * @param $year
     * @return mixed
     */
    public function getAnotherIncome($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__TRAIT__, $companyId);

        if (!array_key_exists($cachePrefix . $year, $this->data)) {

            $dateFrom = $this->getOlapFromDate();
            $dateTo = $this->getOlapToDate();
            $companyItems = [
                InvoiceIncomeItem::ITEM_OTHER => ['name' => 'Прочие доходы'],
                InvoiceIncomeItem::ITEM_INPUT_MONEY => ['name' => 'Ввод денег']
            ];
            //$ONLY_INCOME_ITEMS = array_keys($companyItems);
            $ONLY_INCOME_ITEMS = self::getArticlesWithChildren(array_keys($companyItems), CashFlowsBase::FLOW_TYPE_INCOME, $companyId);

            $flows = $this->getUndestributedFlows($companyId, CashFlowsBase::FLOW_TYPE_INCOME, $ONLY_INCOME_ITEMS);

            $subArticles = self::getArticles2Parents(CashFlowsBase::FLOW_TYPE_INCOME, $companyId);

            foreach ($this->_yearFilter as $byYear) {

                $result = [];
                $parent = $parentItem;

                foreach ($flows as $value) {
                    $month = $value['m'];
                    $item = $value['item_id'];
                    $key = "income-{$item}";

                    $subItem = $subKey = null;
                    if (isset($subArticles[$item])) {
                        $subItem = $item;
                        $subKey = $key;
                        $item = $subArticles[$item];
                        $key = "income-{$item}";
                    }
                    
                    if ($byYear == $value['y']) {

                        if (!isset($result[$key])) {
                            $companyItem = $companyItems[$item];
                            $itemName = $companyItem['name'] ?? InvoiceIncomeItem::findOne($item)->name ?? "item_id={$item}";

                            $result[$key] = self::_getBlankItem('another-income', $itemName, null, $key, self::MOVEMENT_TYPE_FLOWS);
                        }

                        if ($subItem) {
                            if (!isset($result[$key]['items'][$subKey])) {
                                $companyItem = $companyItems[$subItem]['item'] ?? null;
                                $subItemName = $companyItem['name'] ?? InvoiceIncomeItem::findOne($subItem)->name ?? "item_id={$subItem}";
                                $result[$key]['items'][$subKey] = self::_getBlankItem('another-costs expenses-block', $subItemName, null, $subKey, self::MOVEMENT_TYPE_FLOWS);
                            }

                            foreach (['amount', 'amountTax'] as $amountKey) {
                                $result[$key]['items'][$subKey][$amountKey][$month] += $value[$amountKey];
                            }
                        }

                        foreach (['amount', 'amountTax'] as $amountKey) {
                            $result[$key][$amountKey][$month] += $value[$amountKey];
                            $parent[$amountKey][$month] += $value[$amountKey];
                        }
                    }
                }

                $result = ['totalAnotherIncome' => $parent] + $result;
                $this->data[$cachePrefix . $byYear] = $this->calculateQuarterAndTotalAmount($result);
            }
        }

        return $this->data[$cachePrefix . $year];
    }
}