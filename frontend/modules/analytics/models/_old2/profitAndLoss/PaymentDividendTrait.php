<?php

namespace frontend\modules\analytics\models\_old2\profitAndLoss;

use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceExpenditureItem;
use yii\db\Expression;

trait PaymentDividendTrait {

    /**
     * @param $parentItem
     * @param $year
     * @return mixed
     */
    public function getPaymentDividend($parentItem, $year, $companyId)
    {
        $cachePrefix = self::getCachePrefix(__TRAIT__, $companyId);

        if (!array_key_exists($cachePrefix . $year, $this->data)) {

            $companyItems = [
                InvoiceExpenditureItem::ITEM_DIVIDEND_PAYMENT => ['name' => 'Дивиденды'],
                InvoiceExpenditureItem::ITEM_OUTPUT_PROFIT => ['name' => 'Вывод прибыли']
            ];
            //$ONLY_EXPENDITURE_ITEMS = array_keys($companyItems);
            $ONLY_EXPENDITURE_ITEMS = self::getArticlesWithChildren(array_keys($companyItems), CashFlowsBase::FLOW_TYPE_EXPENSE, $companyId);

            $flows = $this->getUndestributedFlows($companyId, CashFlowsBase::FLOW_TYPE_EXPENSE, $ONLY_EXPENDITURE_ITEMS);

            $subArticles = self::getArticles2Parents(CashFlowsBase::FLOW_TYPE_EXPENSE, $companyId);

            foreach ($this->_yearFilter as $byYear) {

                $result = [];
                $parent = $parentItem;

                foreach ($flows as $value) {
                    $month = $value['m'];
                    $item = $value['item_id'];
                    $key = "expense-{$item}";

                    $subItem = $subKey = null;
                    if (isset($subArticles[$item])) {
                        $subItem = $item;
                        $subKey = $key;
                        $item = $subArticles[$item];
                        $key = "expense-{$item}";
                    }

                    if ($byYear == $value['y']) {

                        if (!isset($result[$key])) {
                            $companyItem = $companyItems[$item];
                            $itemName = $companyItem['name'] ?? "item_id={$item}";
                            $result[$key] = self::_getBlankItem('dividend-costs expenses-block', $itemName, null, $key, self::MOVEMENT_TYPE_FLOWS);
                            $result[$key]['canDrag'] = $companyItem['can_update'] ?? false;
                            $result[$key]['data-block'] = null;
                            $result[$key]['data-id'] = $item;
                        }

                        if ($subItem) {
                            if (!isset($result[$key]['items'][$subKey])) {
                                $companyItem = $companyItems[$subItem]['item'] ?? null;
                                $subItemName = $companyItem['name'] ?? InvoiceExpenditureItem::findOne($subItem)->name ?? "item_id={$subItem}";
                                $result[$key]['items'][$subKey] = self::_getBlankItem('dividend-costs expenses-block', $subItemName, null, $subKey, self::MOVEMENT_TYPE_FLOWS);
                                $result[$key]['items'][$subKey]['canDrag'] = $companyItem['can_update'] ?? false;
                                $result[$key]['items'][$subKey]['data-block'] = null;
                                $result[$key]['items'][$subKey]['data-id'] = $subItem;
                            }

                            foreach (['amount', 'amountTax'] as $amountKey) {
                                $result[$key]['items'][$subKey][$amountKey][$month] += $value[$amountKey];
                            }
                        }

                        foreach (['amount', 'amountTax'] as $amountKey) {
                            $result[$key][$amountKey][$month] += $value[$amountKey];
                            $parent[$amountKey][$month] += $value[$amountKey];
                        }
                    }
                }

                $result = ['totalPaymentDividend' => $parent] + $result;
                $this->data[$cachePrefix . $byYear] = $this->calculateQuarterAndTotalAmount($result);
            }


        }

        return $this->data[$cachePrefix . $year];
    }

}