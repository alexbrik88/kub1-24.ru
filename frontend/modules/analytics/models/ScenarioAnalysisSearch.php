<?php

namespace frontend\modules\analytics\models;

use common\components\date\DateHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\ExpenseItemFlowOfFunds;
use common\models\IncomeItemFlowOfFunds;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankFlowToInvoice;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderFlowToInvoice;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyFlowToInvoice;
use common\models\company\CompanyType;
use common\models\document\Invoice;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductTurnover;
use frontend\models\Documents;
use frontend\modules\analytics\models\OLAP;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class ScenarioAnalysisSearch
 */
class ScenarioAnalysisSearch extends Model
{
    public $delay_plan;
    public $price_plan;
    public $type1_a_plan;
    public $type1_a_price;
    public $type1_b_plan;
    public $type1_b_price;
    public $type1_c_plan;
    public $type1_c_price;
    public $type2_a_plan;
    public $type2_a_price;
    public $type2_b_plan;
    public $type2_b_price;
    public $type2_c_plan;
    public $type2_c_price;

    protected $_company;
    protected $_from;
    protected $_till;

    private $_data = [];
    private $incomeActivityTypes = [];
    private $expenseActivityTypes = [];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                [
                    'type1_a_plan',
                    'type1_a_price',
                    'type1_b_plan',
                    'type1_b_price',
                    'type1_c_plan',
                    'type1_c_price',
                    'type2_a_plan',
                    'type2_a_price',
                    'type2_b_plan',
                    'type2_b_price',
                    'type2_c_plan',
                    'type2_c_price',
                ],
                'number',
            ],
            [
                [
                    'delay_plan',
                    'price_plan',
                ],
                'safe',
            ],
        ];
    }

    public function __construct(Company $company, array $config = [])
    {
        $this->_company = $company;

        parent::__construct($config);
    }

    public function init()
    {
        parent::init();

        $this->_from = new \DateTimeImmutable('first day of -12 month');
        $this->_till = new \DateTimeImmutable('last day of -1 month');

        $this->incomeActivityTypes = IncomeItemFlowOfFunds::find()
            ->select('flow_of_funds_block')
            ->where(['company_id' => $this->_company->id])
            ->andWhere(['not', ['flow_of_funds_block' => null]])
            ->indexBy('income_item_id')
            ->column($this->getDb());

        $this->expenseActivityTypes = ExpenseItemFlowOfFunds::find()
            ->select('flow_of_funds_block')
            ->where(['company_id' => $this->_company->id])
            ->andWhere(['not', ['flow_of_funds_block' => null]])
            ->indexBy('expense_item_id')
            ->column($this->getDb());

        $this->createDataTable();
    }

    public function getDb()
    {
        return Yii::$app->db2;
    }

    public function search($params = [])
    {
        $this->load($params);

        $data = $this->getTableRowsData();

        return $data;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @return number
     */
    public function getAverageProceedsChange()
    {
        return $this->getTableRowsData()['type2']['total']['price_plan'] ?? 0;
    }

    /**
     * @return number
     */
    public function getAverageExpensesChange()
    {
        return $this->getTableRowsData()['type1']['total']['price_plan'] ?? 0;
    }

    /**
     * @return number
     */
    public function getAverageDebitRatioFact()
    {
        return $this->getTableRowsData()['type2']['total']['turnover_fact'] ?? 0;
    }

    /**
     * @return number
     */
    public function getAverageDebitRatioPlan()
    {
        return $this->getTableRowsData()['type2']['total']['turnover_plan'] ?? 0;
    }

    /**
     * @return number
     */
    public function getAverageCreditRatioFact()
    {
        return $this->getTableRowsData()['type1']['total']['turnover_fact'] ?? 0;
    }

    /**
     * @return number
     */
    public function getAverageCreditRatioPlan()
    {
        return $this->getTableRowsData()['type1']['total']['turnover_plan'] ?? 0;
    }

    /**
     * @return number
     */
    private function getTableRowsData()
    {
        if (!array_key_exists('getTableRowsData', $this->_data)) {
            $hasDalayPlan = false;
            $hasPricePlan = false;
            $data = [];
            foreach ($this->getTableQueryData() as $i => $val) {
                $proceeds = 0;
                $total = $val['total'];
                foreach (['a', 'b', 'c'] as $k) {
                    $data[$i]['g'][$k] = [
                        'items' => [],
                        'total' => [
                            'proceeds' => 0,
                            'proceeds_part' => 0,
                            'margin' => 0,
                            'margin_part' => 0,
                            'delay_set' => 0,
                            'delay_fact' => 0,
                            'delay_plan' => 0,
                            'delay_change' => 0,
                            'turnover_fact' => 0,
                            'turnover_plan' => 0,
                            'price_plan' => 0,
                            'count' => 0,
                        ],
                    ];
                }
                if ($total['proceeds'] == 0) {
                    $data[$i]['total'] = [
                        'proceeds' => 0,
                        'proceeds_part' => 0,
                        'margin' => 0,
                        'margin_part' => 0,
                        'delay_set' => 0,
                        'delay_fact' => 0,
                        'delay_plan' => 0,
                        'delay_change' => 0,
                        'turnover_fact' => 0,
                        'turnover_plan' => 0,
                        'price_plan' => 0,
                        'count' => 0,
                    ];

                    continue;
                }
                foreach ($val['items'] as $item) {
                    $p = $proceeds / $total['proceeds'] * 100;
                    $k = $p <= 80 ? 'a' : ($p <= 95 ? 'b' : 'c');
                    $proceeds += $item['proceeds'];
                    $item['title'] = self::contractorName($item['company_type_id'], $item['name'], $item['face_type'], $item['foreign_legal_form']);
                    $item['proceeds_part'] = $total['proceeds'] ? $item['proceeds'] / $total['proceeds'] * 100 : 0;
                    $item['margin_part'] = $item['proceeds'] ? $item['margin'] / $item['proceeds'] * 100 : 0;
                    $data[$i]['g'][$k]['items'][] = $item;
                    $data[$i]['g'][$k]['total']['proceeds'] += $item['proceeds'];
                    $data[$i]['g'][$k]['total']['margin'] += $item['margin'];
                    $data[$i]['g'][$k]['total']['count']++;
                }
                foreach ($data[$i]['g'] as $k => &$g) {
                    $g['total']['proceeds_part'] = $total['proceeds'] ? $g['total']['proceeds'] / $total['proceeds'] * 100 : 0;
                    $g['total']['margin_part'] = $g['total']['proceeds'] ? $g['total']['margin'] / $g['total']['proceeds'] * 100 : 0;
                    foreach ($g['items'] as $item) {
                        $part = $g['total']['proceeds'] ? $item['proceeds'] / $g['total']['proceeds'] * 100 : 0;
                        $g['total']['delay_set'] += $item['delay_set'] * $part / 100;
                        $g['total']['delay_fact'] += $item['delay_fact'] * $part / 100;
                    }
                    $delayPlan = $this->delay_plan[$i][$k] ?? null;
                    $pricePlan = $this->price_plan[$i][$k] ?? null;
                    if (is_numeric($delayPlan)) {
                        $g['total']['delay_plan'] = $delayPlan;
                    } else {
                        $g['total']['delay_plan'] = $this->delay_plan[$i][$k] = round($g['total']['delay_fact'], 1);
                    }
                    $hasDalayPlan = $hasDalayPlan || $g['total']['delay_plan'] != round($g['total']['delay_fact'], 1);
                    if (is_numeric($pricePlan)) {
                        $g['total']['price_plan'] = $pricePlan;
                    } else {
                        $g['total']['price_plan'] = $this->price_plan[$i][$k] = 0;
                    }
                    $hasPricePlan = $hasPricePlan || $g['total']['price_plan'] != 0;
                    $g['total']['delay_change'] = $g['total']['delay_plan'] - $g['total']['delay_fact'];
                    $g['total']['turnover_fact'] = $g['total']['delay_fact'] > 0 ? 30 / $g['total']['delay_fact'] : 30;
                    $g['total']['turnover_plan'] = $g['total']['delay_plan'] > 0 ? 30 / $g['total']['delay_plan'] : 30;

                    $total['delay_set'] += $g['total']['delay_set'] * $g['total']['proceeds_part'] / 100;
                    $total['delay_fact'] += $g['total']['delay_fact'] * $g['total']['proceeds_part'] / 100;
                    $total['delay_plan'] += $g['total']['delay_plan'] * $g['total']['proceeds_part'] / 100;
                    $total['price_plan'] += $g['total']['price_plan'] * $g['total']['proceeds_part'] / 100;
                }
                $total['delay_change'] = $total['delay_plan'] - $total['delay_fact'];
                $total['turnover_fact'] = $total['delay_fact'] > 0 ? 30 / $total['delay_fact'] : 30;
                $total['turnover_plan'] = $total['delay_plan'] > 0 ? 30 / $total['delay_plan'] : 30;
                $total['margin_part'] = $total['proceeds'] ? $total['margin'] / $total['proceeds'] * 100 : 0;
                $data[$i]['total'] = $total;
            }
            $data['hasDalayPlan'] = $hasDalayPlan;
            $data['hasPricePlan'] = $hasPricePlan;

            $this->_data['getTableRowsData'] = $data;
        }

        return $this->_data['getTableRowsData'];
    }

    /**
     * @return number
     */
    private function getTableQueryData()
    {
        $total = [
            'proceeds' => 0,
            'proceeds_part' => 100,
            'margin' => 0,
            'margin_part' => 0,
            'delay_set' => 0,
            'delay_fact' => 0,
            'delay_plan' => 0,
            'delay_change' => 0,
            'turnover_fact' => 0,
            'turnover_plan' => 0,
            'price_plan' => 0,
            'count' => 0,
        ];
        $data = [
            'type1' => [
                'items' => [],
                'total' => $total,
            ],
            'type2' => [
                'items' => [],
                'total' => $total,
            ],
        ];
        $query = (new Query)->select([
            't.type',
            't.contractor_id',
            'c.company_type_id',
            'c.foreign_legal_form',
            'c.face_type',
            'c.name',
            'delay_set' => '{{c}}.[[payment_delay]]',
            'delay_fact' => '({{p}}.[[delay_fact]])',
            'proceeds' => 'SUM({{t}}.[[proceeds]])',
            'margin' => 'SUM({{t}}.[[margin]])',
        ])->from([
            't' => '__turnover',
        ])->leftJoin([
            'c' => Contractor::tableName(),
        ], '{{c}}.[[id]] = {{t}}.[[contractor_id]]')->leftJoin([
            'p' => '__payments'
        ], '{{p}}.[[contractor_id]] = {{t}}.[[contractor_id]] AND {{p}}.[[type]] = {{t}}.[[type]]')->groupBy([
            'type',
            'contractor_id',
        ])->orderBy([
            'type' => SORT_ASC,
            'proceeds' => SORT_DESC,
        ]);

        foreach ($query->all($this->db) as $row) {
            if (in_array($row['type'], [Documents::IO_TYPE_IN, Documents::IO_TYPE_OUT])) {
                $i = 'type'.$row['type'];
                $data[$i]['items'][] = $row;
                $data[$i]['total']['proceeds'] += $row['proceeds'];
                $data[$i]['total']['margin'] += $row['margin'];
                $data[$i]['total']['count']++;
            }
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getProceedsPrognosisData() : array
    {
        $retrospectiveData = $this->getProceedsRetrospectiveData();
        $initialData = $retrospectiveData['items'];
        $A1 = $retrospectiveData['var']['A1'];
        $B1 = $retrospectiveData['var']['B1'];
        $A2 = $retrospectiveData['var']['A2'];
        $B2 = $retrospectiveData['var']['B2'];
        $averageProceedsChange = $this->getAverageProceedsChange();
        $averageExpensesChange = $this->getAverageExpensesChange();
        $data = [];
        foreach ($initialData as $k => $row) {
            $key = $k + 12;
            $proceeds_trend = $A1 + $B1 * $key;
            $proceeds = $proceeds_trend * $row['season_rate'] * (1 + $averageProceedsChange / 100);
            $variable_expenses_trend = $A2 + $B2 * $key;
            $variable_expenses = $variable_expenses_trend * $row['season_rate'] * (1 + $averageExpensesChange / 100);
            $total_expenses = $variable_expenses + $row['fixed_expenses'];
            $proceeds_fact = $proceeds_trend * $row['season_rate'];
            $variable_expenses_fact = $variable_expenses_trend * $row['season_rate'];
            $total_expenses_fact = $variable_expenses_fact + $row['fixed_expenses'];
            $data[$key] = [
                'year' => $row['year'] + 1,
                'month' => $row['month'],
                'date' => clone ($row['date']->modify('+1 year')),
                'proceeds' => $proceeds,
                'proceeds_deviation' => 0,
                'proceeds_trend' => $proceeds_trend,
                'season_rate' => $row['season_rate'],
                'variable_expenses_rate' => 0,
                'variable_expenses' => $variable_expenses,
                'variable_expenses_deviation' => 0,
                'variable_expenses_trend' => $variable_expenses_trend,
                'fixed_expenses' => $row['fixed_expenses'],
                'total_expenses' => $total_expenses,
                'operating_profit' => $proceeds - $total_expenses,
                'proceeds_fact' => $proceeds_fact,
                'operating_profit_fact' => $proceeds_fact - $total_expenses_fact,
            ];
        }

        return [
            'items' => $data,
        ];
    }

    /**
     * @return array
     */
    public function getProceedsRetrospectiveData() : array
    {
        $data = [];
        $dataKeys = [];
        $range = range(1, 12);
        $total = [
            'proceeds' => 0,
            'proceeds_deviation' => 0,
            'proceeds_trend' => 0,
            'variable_expenses' => 0,
            'variable_expenses_deviation' => 0,
            'variable_expenses_trend' => 0,
            'fixed_expenses' => $this->getFixedExpenses(),
            'total_expenses' => 0,
            'operating_profit' => 0,
        ];
        $average = [
            'key' => array_sum($range) / count($range),
            'proceeds' => 0,
            'proceeds_deviation' => 0,
            'variable_expenses' => 0,
            'variable_expenses_deviation' => 0,
        ];
        $var = [
            'A1' => 0,
            'B1' => 0,
            'A2' => 0,
            'B2' => 0,
        ];

        $fixedExpenses = $total['fixed_expenses'] / 12;
        $date = \DateTime::createFromImmutable($this->_from);
        foreach ($range as $k => $key) {
            $y = (int) $date->format('Y');
            $m = (int) $date->format('m');
            $dataKeys[$y*100+$m] = $key;
            $data[$key] = [
                'year' => $y,
                'month' => $m,
                'date' => clone $date,
                'proceeds' => 0,
                'proceeds_deviation' => 0,
                'proceeds_trend' => 0,
                'season_rate' => 0,
                'variable_expenses_rate' => 0,
                'variable_expenses' => 0,
                'variable_expenses_deviation' => 0,
                'variable_expenses_trend' => 0,
                'fixed_expenses' => $fixedExpenses,
                'total_expenses' => 0,
                'operating_profit' => 0,
            ];
            $date->modify('+1 month');
        }

        $totalVariableExpenses = 0;
        foreach ($this->getProceedsData() as $item) {
            $key = $dataKeys[$item['year']*100+$item['month']];
            $data[$key]['proceeds'] = $item['proceeds'];
            $total['proceeds'] += $item['proceeds'];
            $totalVariableExpenses += $item['variable_expenses'];
        }

        $average['proceeds'] = $total['proceeds'] / 12;
        $variableExpensesRate = ($total['proceeds'])
            ? ($totalVariableExpenses / $total['proceeds'])
            : 0;

        foreach ($data as $key => $value) {
            $variable_expenses = $data[$key]['proceeds'] * $variableExpensesRate;
            $proceeds_deviation = $key == 1 ? 0 : $data[$key]['proceeds'] - $data[$key-1]['proceeds'];
            $variable_expenses_deviation = $key == 1 ? 0 : $variable_expenses - $data[$key-1]['variable_expenses'];
            $data[$key]['proceeds_deviation'] = $proceeds_deviation;
            $data[$key]['variable_expenses_rate'] = $variableExpensesRate;
            $data[$key]['variable_expenses'] = $variable_expenses;
            $data[$key]['variable_expenses_deviation'] = $variable_expenses_deviation;
            $data[$key]['season_rate'] = ($average['proceeds'])
                ? ($data[$key]['proceeds'] / $average['proceeds'])
                : 0;
            $total['proceeds_deviation'] += $proceeds_deviation;
            $total['variable_expenses'] += $variable_expenses;
            $total['variable_expenses_deviation'] += $variable_expenses_deviation;
        }

        $average['proceeds_deviation'] = $total['proceeds_deviation'] / 12;
        $average['variable_expenses'] = $total['variable_expenses'] / 12;
        $average['variable_expenses_deviation'] = $total['variable_expenses_deviation'] / 12;
        $var['B1'] = $average['proceeds_deviation'];
        $var['A1'] = $average['proceeds'] - $average['proceeds_deviation'] * $average['key'];
        $var['B2'] = $average['variable_expenses_deviation'];
        $var['A2'] = $average['variable_expenses'] - $average['variable_expenses_deviation'] * $average['key'];

        foreach ($data as $key => $value) {
            $data[$key]['proceeds_trend'] = $var['A1'] + $var['B1'] * $key;
            $data[$key]['variable_expenses_trend'] = $var['A2'] + $var['B2'] * $key;
            $data[$key]['total_expenses'] = $value['variable_expenses'] + $value['fixed_expenses'];
            $data[$key]['operating_profit'] = $value['proceeds'] - $value['total_expenses'];
            $total['proceeds_trend'] += $data[$key]['proceeds_trend'];
            $total['variable_expenses_trend'] += $data[$key]['variable_expenses_trend'];
            $total['total_expenses'] += $data[$key]['total_expenses'];
            $total['operating_profit'] += $data[$key]['operating_profit'];
        }

        return [
            'items' => $data,
            'total' => $total,
            'average' => $average,
            'var' => $var,
        ];
    }

    public function getProceedsData() : array
    {
        if (!array_key_exists('getProceedsData', $this->_data)) {
            $query = (new Query())->select([
                'year',
                'month',
                'proceeds' => 'SUM([[proceeds]])',
                'variable_expenses' => 'SUM([[purchase]])',
            ])->from([
                't' => '__turnover',
            ])->andWhere([
                'type' => Documents::IO_TYPE_OUT,
            ])->groupBy([
                'year',
                'month',
            ]);

            $this->_data['getProceedsData'] = $query->all($this->db);
        }

        return $this->_data['getProceedsData'];
    }

    public function getCdpPrognosisData() : array
    {
        $retrospectiveData = $this->getCdpRetrospectiveData();
        $initialData = $retrospectiveData['items'];
        $A1 = $retrospectiveData['var']['A1'];
        $B1 = $retrospectiveData['var']['B1'];
        $A2 = $retrospectiveData['var']['A2'];
        $B2 = $retrospectiveData['var']['B2'];
        $proceedsChange = $this->getAverageProceedsChange();
        $expensesChange = $this->getAverageExpensesChange();
        $debitRatioFact = $this->getAverageDebitRatioFact();
        $debitRatioPlan = $this->getAverageDebitRatioPlan();
        $creditRatioFact = $this->getAverageCreditRatioFact();
        $creditRatioPlan = $this->getAverageCreditRatioPlan();
        $data = [];
        foreach ($initialData as $k => $row) {
            $key = $k + 12;
            $income_trend = $A1 + $B1 * $key;
            $income = $debitRatioFact ? ($income_trend * (1 + $proceedsChange / 100)) / $debitRatioFact * $debitRatioPlan : 0;
            $expense_trend = $A2 + $B2 * $key;
            $expense = $creditRatioFact ? ($expense_trend * (1 + $expensesChange / 100)) / $creditRatioFact * $creditRatioPlan : 0;
            $income_total = $income + $row['income_fd'] + $row['income_id'];
            $expense_total = $expense + $row['expense_fd'] + $row['expense_id'];

            $income_fact = $income_trend;
            $expense_fact = $expense_trend;
            $income_total_fact = $income_fact + $row['income_fd'] + $row['income_id'];
            $expense_total_fact = $expense_fact + $row['expense_fd'] + $row['expense_id'];

            $data[$key] = [
                'year' => $row['year'] + 1,
                'month' => $row['month'],
                'date' => clone ($row['date']->modify('+1 year')),
                'income' => $income,
                'income_trend' => $income_trend,
                'income_fd' => $row['income_fd'],
                'income_id' => $row['income_id'],
                'income_total' => $income_total,
                'expense' => $expense,
                'expense_trend' => $expense_trend,
                'expense_fd' => $row['expense_fd'],
                'expense_id' => $row['expense_id'],
                'expense_total' => $expense_total,
                'cdp' => $income_total - $expense_total,
                'cdp_fact' => $income_total_fact - $expense_total_fact,
            ];
        }

        return [
            'items' => $data,
        ];

    }

    public function getCdpRetrospectiveData() : array
    {
        $data = [];
        $dataKeys = [];
        $range = range(1, 12);
        $keyCount = count($range);
        $total = [
            'income' => 0,
            'income_deviation' => 0,
            'income_trend' => 0,
            'income_fd' => 0,
            'income_id' => 0,
            'income_total' => 0,
            'expense' => 0,
            'expense_deviation' => 0,
            'expense_trend' => 0,
            'expense_fd' => 0,
            'expense_id' => 0,
            'expense_total' => 0,
            'cdp' => 0,
        ];
        $average = [
            'key' => array_sum($range) / $keyCount,
            'income' => 0,
            'income_deviation' => 0,
            'expense' => 0,
            'expense_deviation' => 0,
            'cdp' => 0,
        ];
        $var = [
            'A1' => 0,
            'B1' => 0,
            'A2' => 0,
            'B2' => 0,
        ];
        $date = \DateTime::createFromImmutable($this->_from);
        foreach ($range as $k => $key) {
            $y = (int) $date->format('Y');
            $m = (int) $date->format('m');
            $dataKeys[$y*100+$m] = $key;
            $data[$key] = [
                'year' => $y,
                'month' => $m,
                'date' => clone $date,
                'income' => 0,
                'income_deviation' => 0,
                'income_trend' => 0,
                'income_fd' => 0,
                'income_id' => 0,
                'income_total' => 0,
                'expense' => 0,
                'expense_deviation' => 0,
                'expense_trend' => 0,
                'expense_fd' => 0,
                'expense_id' => 0,
                'expense_total' => 0,
                'cdp' => 0,
            ];
            $date->modify('+1 month');
        }
        foreach ($this->getCdpData() as $k => $value) {
            $key = $dataKeys[$k];
            $data[$key]['income'] = $value[CashFlowsBase::FLOW_TYPE_INCOME][AbstractFinance::OPERATING_ACTIVITIES_BLOCK] ?? 0;
            $data[$key]['expense'] = $value[CashFlowsBase::FLOW_TYPE_EXPENSE][AbstractFinance::OPERATING_ACTIVITIES_BLOCK] ?? 0;

            // Пока не учитываем
            // $data[$key]['income_fd'] = $value[CashFlowsBase::FLOW_TYPE_INCOME][AbstractFinance::FINANCIAL_OPERATIONS_BLOCK] ?? 0;
            // $data[$key]['income_id'] = $value[CashFlowsBase::FLOW_TYPE_INCOME][AbstractFinance::INVESTMENT_ACTIVITIES_BLOCK] ?? 0;
            // $data[$key]['expense_fd'] = $value[CashFlowsBase::FLOW_TYPE_EXPENSE][AbstractFinance::FINANCIAL_OPERATIONS_BLOCK] ?? 0;
            // $data[$key]['expense_id'] = $value[CashFlowsBase::FLOW_TYPE_EXPENSE][AbstractFinance::INVESTMENT_ACTIVITIES_BLOCK] ?? 0;

            $total['income'] += $data[$key]['income'];
            $total['income_fd'] += $data[$key]['income_fd'];
            $total['income_id'] += $data[$key]['income_id'];
            $total['expense'] += $data[$key]['expense'];
            $total['expense_fd'] += $data[$key]['expense_fd'];
            $total['expense_id'] += $data[$key]['expense_id'];
        }
        $average['income'] = $total['income'] / $keyCount;
        $average['expense'] = $total['expense'] / $keyCount;

        foreach ($data as $key => $value) {
            $income_deviation = $key == 1 ? 0 : $data[$key]['income'] - $data[$key-1]['income'];
            $expense_deviation = $key == 1 ? 0 : $data[$key]['expense'] - $data[$key-1]['expense'];
            $income_total = $value['income'] + $value['income_fd'] + $value['income_id'];
            $expense_total = $value['expense'] + $value['expense_fd'] + $value['expense_id'];
            $cdp = $income_total - $expense_total;
            $data[$key]['income_deviation'] = $income_deviation;
            $data[$key]['expense_deviation'] = $expense_deviation;
            $data[$key]['income_total'] = $income_total;
            $data[$key]['expense_total'] = $expense_deviation;
            $data[$key]['cdp'] = $cdp;
            $total['income_deviation'] += $income_deviation;
            $total['expense_deviation'] += $expense_deviation;
            $total['income_total'] += $income_total;
            $total['expense_total'] += $expense_total;
            $total['cdp'] += $cdp;
        }

        $average['income_deviation'] = $total['income_deviation'] / $keyCount;
        $average['expense_deviation'] = $total['expense_deviation'] / $keyCount;
        $average['cdp'] = $total['cdp'] / $keyCount;
        $var['B1'] = $average['income_deviation'];
        $var['A1'] = $average['income'] - $average['income_deviation'] * $average['key'];
        $var['B2'] = $average['expense_deviation'];
        $var['A2'] = $average['expense'] - $average['expense_deviation'] * $average['key'];

        foreach ($data as $key => $value) {
            $data[$key]['income_trend'] = $var['A1'] + $var['B1'] * $key;
            $data[$key]['expense_trend'] = $var['A2'] + $var['B2'] * $key;
            $total['income_trend'] += $data[$key]['income_trend'];
            $total['expense_trend'] += $data[$key]['expense_trend'];
        }

        return [
            'items' => $data,
            'total' => $total,
            'average' => $average,
            'var' => $var,
        ];
    }

    public function getCdpData() : array
    {
        if (!array_key_exists('getCdpData', $this->_data)) {
            $companyId = $this->_company->id;
            $from = $this->_from->format('Y-m-d');

            $query = (new Query)
            ->select([
                'ym' => "DATE_FORMAT([[date]], '%Y%m')",
                'type',
                'item_id',
                'amount' => 'SUM([[amount]])',
            ])
            ->from(OLAP::TABLE_OLAP_FLOWS)
            ->andWhere([
                'company_id' => $this->_company->id,
            ])
            ->andWhere([
                'not',
                ['contractor_id' => ['bank', 'order', 'emoney']],
            ])
            ->andWhere([
                'between',
                'date',
                $this->_from->format('Y-m-d'),
                $this->_till->format('Y-m-d'),
            ])
            ->groupBy("[[ym]], [[type]], [[item_id]]");

            $data = [];
            $queryResult = $query->all($this->db);
            foreach ($queryResult as $row) {
                $activityType = AbstractFinance::$blockByType[$this->getActivityType($row['type'], $row['item_id'])];
                $data[$row['ym']][$row['type']][$activityType] = $row['amount'];
            }

            $this->_data['getCdpData'] = $data;
        }

        return $this->_data['getCdpData'];
    }

    private function getActivityType($type, $itemId)
    {
        if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
            if (isset($this->incomeActivityTypes[$itemId])) {
                return AbstractFinance::$typesByBlock[$this->incomeActivityTypes[$itemId]][0];
            } elseif (in_array($itemId, AbstractFinance::$typeItem[AbstractFinance::RECEIPT_FINANCING_TYPE_FIRST])) {
                return AbstractFinance::RECEIPT_FINANCING_TYPE_FIRST;
            } else {
                return AbstractFinance::INCOME_OPERATING_ACTIVITIES;
            }
        }
        if ($type == CashFlowsBase::FLOW_TYPE_EXPENSE) {
            if (isset($this->expenseActivityTypes[$itemId])) {
                return AbstractFinance::$typesByBlock[$this->expenseActivityTypes[$itemId]][1];
            } elseif (in_array($itemId, AbstractFinance::$typeItem[AbstractFinance::RECEIPT_FINANCING_TYPE_SECOND])) {
                return AbstractFinance::RECEIPT_FINANCING_TYPE_SECOND;
            } else {
                return AbstractFinance::WITHOUT_TYPE;
            }
        }

        throw new Exception('getActivityWallet: wallet not found');
    }

    /**
     * @return array
     */
    public function getChart1Data()
    {
        if (!array_key_exists('getChart1Data', $this->_data)) {
            $months = DateHelper::$months;
            $categories = [];
            $proceeds = [];
            $proceeds_plan = [];
            $proceeds_trend = [];
            $operating_profit = [];
            $operating_profit_plan = [];
            $data = [];
            $current = date('Ym');
            $items = $this->getProceedsRetrospectiveData()['items'];
            $itemsCount = count($items);
            $i = 0;
            foreach ($items as $key => $value) {
                $i++;
                $y = $value['date']->format('Y');
                $m = $value['date']->format('m');
                $month = DateHelper::$months[$m];
                $categories[] = mb_strtoupper(mb_substr($month, 0, 3));
                $proceeds[] = round($value['proceeds']/100, 2);
                $proceeds_plan[] = $i == $itemsCount ? round($value['proceeds']/100, 2) : null;
                $proceeds_trend[] = round($value['proceeds_trend']/100, 2);
                $operating_profit[] = round($value['operating_profit']/100, 2);
                $operating_profit_plan[] = $i == $itemsCount ? round($value['operating_profit']/100, 2) : null;
                $data[] = [
                    'year' => $y,
                    'month' => (int) $m,
                    'current' => $y.$m === $current,
                    'title' => mb_convert_case($month, MB_CASE_TITLE).' '.$y,
                ];
            }
            foreach ($this->getProceedsPrognosisData()['items'] as $key => $value) {
                $y = $value['date']->format('Y');
                $m = $value['date']->format('m');
                $month = DateHelper::$months[$m];
                $categories[] = mb_strtoupper(mb_substr($month, 0, 3));
                $proceeds[] = round($value['proceeds_fact']/100, 2);
                $proceeds_plan[] = round($value['proceeds']/100, 2);
                $proceeds_trend[] = round($value['proceeds_trend']/100, 2);
                $operating_profit[] = round($value['operating_profit_fact']/100, 2);
                $operating_profit_plan[] = round($value['operating_profit']/100, 2);
                $data[] = [
                    'year' => $y,
                    'month' => (int) $m,
                    'current' => $y.$m === $current,
                    'title' => mb_convert_case($month, MB_CASE_TITLE).' '.$y,
                ];
            }

            $this->_data['getChart1Data'] = [
                'categories' => $categories,
                'series' => [
                    'proceeds' => $proceeds,
                    'proceeds_plan' => $proceeds_plan,
                    'trend' => $proceeds_trend,
                    'profit' => $operating_profit,
                    'profit_plan' => $operating_profit_plan,
                ],
                'data' => $data,
                'hasPlan' => $this->getTableRowsData()['hasPricePlan'],
            ];
        }

        return $this->_data['getChart1Data'];
    }

    /**
     * @return array
     */
    public function getChart2Data() : array
    {
        if (!array_key_exists('getChart2Data', $this->_data)) {
            $months = DateHelper::$months;
            $categories = [];
            $cdp = [];
            $cdp_plan = [];
            $data = [];
            $current = date('Ym');
            $profit = $this->getChart1Data()['series']['profit'];
            $profit_plan = $this->getChart1Data()['series']['profit_plan'];
            $items = $this->getCdpRetrospectiveData()['items'];
            $itemsCount = count($items);
            $i = 0;
            foreach ($this->getCdpRetrospectiveData()['items'] as $key => $value) {
                $i++;
                $y = $value['date']->format('Y');
                $m = $value['date']->format('m');
                $month = DateHelper::$months[$m];
                $categories[] = mb_strtoupper(mb_substr($month, 0, 3));
                $cdp[] = round($value['cdp']/100, 2);
                $cdp_plan[] = $i == $itemsCount ? round($value['cdp']/100, 2) : null;
                $data[] = [
                    'year' => $y,
                    'month' => (int) $m,
                    'current' => $y.$m === $current,
                    'title' => mb_convert_case($month, MB_CASE_TITLE).' '.$y,
                ];
            }
            foreach ($this->getCdpPrognosisData()['items'] as $key => $value) {
                $y = $value['date']->format('Y');
                $m = $value['date']->format('m');
                $month = DateHelper::$months[$m];
                $categories[] = mb_strtoupper(mb_substr($month, 0, 3));
                $cdp[] = round($value['cdp_fact']/100, 2);
                $cdp_plan[] = round($value['cdp']/100, 2);
                $data[] = [
                    'year' => $y,
                    'month' => (int) $m,
                    'current' => $y.$m === $current,
                    'title' => mb_convert_case($month, MB_CASE_TITLE).' '.$y,
                ];
            }

            $this->_data['getChart2Data'] = [
                'categories' => $categories,
                'series' => [
                    'cdp' => $cdp,
                    'cdp_plan' => $cdp_plan,
                    'profit' => $profit,
                    'profit_plan' => $profit_plan,
                ],
                'data' => $data,
                'hasPlan' => $this->getTableRowsData()['hasDalayPlan'] || $this->getTableRowsData()['hasPricePlan'],
            ];
        }

        return $this->_data['getChart2Data'];
    }

    /**
     * @return int
     */
    public function getFixedExpenses() : int
    {
        return (int) ($this->getDocsFixedExpenses() + $this->getFlowsFixedExpenses());
    }

    /**
     * @param $expenseType
     * @return array
     */
    private function getFixedCostsExpenditureItems()
    {
        if (!array_key_exists('getFixedCostsExpenditureItems', $this->_data)) {
            $this->_data['getFixedCostsExpenditureItems'] = ProfitAndLossCompanyItem::find()
                ->select(['item_id'])
                ->andWhere(['profit_and_loss_company_item.expense_type' => ProfitAndLossCompanyItem::CONSTANT_EXPENSE_TYPE])
                ->andWhere(['profit_and_loss_company_item.company_id' => $this->company->id])
                ->column($this->db);
        }

        return $this->_data['getFixedCostsExpenditureItems'];
    }

    /**
     * @return int
     */
    private function getDocsFixedExpenses() : int
    {
        if (!array_key_exists('getDocsFixedExpenses', $this->_data)) {
            $this->_data['getDocsFixedExpenses'] = (int) (new Query)->from('olap_documents')->andWhere([
                'company_id' => $this->company->id,
                'type' => Documents::IO_TYPE_IN,
                'invoice_expenditure_item_id' => $this->getFixedCostsExpenditureItems(),
            ])->andWhere([
                'between',
                'date',
                $this->_from->format('Y-m-d'),
                $this->_till->format('Y-m-d'),
            ])->sum('amount', $this->db);
        }

        return $this->_data['getDocsFixedExpenses'];
    }

    /**
     * @return integer
     */
    private function getFlowsFixedExpenses() : int
    {
        if (!array_key_exists('getDocsFixedExpenses', $this->_data)) {
            $this->_data['getDocsFixedExpenses'] = (int) (new Query)->from('olap_flows')->andWhere([
                'company_id' => $this->company->id,
                'type' => CashFlowsBase::FLOW_TYPE_EXPENSE,
                'item_id' => $this->getFixedCostsExpenditureItems(),
            ])->andWhere([
                'between',
                'recognition_date',
                $this->_from->format('Y-m-d'),
                $this->_till->format('Y-m-d'),
            ])->andWhere([
                'or',
                ['has_invoice' => false],
                ['has_doc' => false],
                ['need_doc' => false],
            ])->sum('amount', 'db2');
        }

        return $this->_data['getDocsFixedExpenses'];
    }

    private function createDataTable()
    {
        $db = $this->db;
        $select = [
            'type' => 'i.type',
            'i.contractor_id',
            'delay' => 'DATEDIFF({{f}}.[[recognition_date]], {{i}}.[[document_date]])',
            't.amount',
        ];
        $where = [
            'and',
            ['i.company_id' => $this->_company->id],
            ['i.is_deleted' => 0],
            ['between', 'f.recognition_date', $this->_from->format('Y-m-d'), $this->_till->format('Y-m-d')],
        ];

        $query1 = (new Query())->select($select)->from([
            'i' => Invoice::tableName(),
        ])->innerJoin([
            't' => CashBankFlowToInvoice::tableName(),
        ], '{{t}}.[[invoice_id]] = {{i}}.[[id]]')->leftJoin([
            'f' => CashBankFlows::tableName(),
        ], '{{t}}.[[flow_id]] = {{f}}.[[id]]')->andWhere($where);

        $query2 = (new Query())->select($select)->from([
            'i' => Invoice::tableName(),
        ])->innerJoin([
            't' => CashOrderFlowToInvoice::tableName(),
        ], '{{t}}.[[invoice_id]] = {{i}}.[[id]]')->leftJoin([
            'f' => CashOrderFlows::tableName(),
        ], '{{t}}.[[flow_id]] = {{f}}.[[id]]')->andWhere($where);

        $query3 = (new Query())->select($select)->from([
            'i' => Invoice::tableName(),
        ])->innerJoin([
            't' => CashEmoneyFlowToInvoice::tableName(),
        ], '{{t}}.[[invoice_id]] = {{i}}.[[id]]')->leftJoin([
            'f' => CashEmoneyFlows::tableName(),
        ], '{{t}}.[[flow_id]] = {{f}}.[[id]]')->andWhere($where);

        $query = (new Query)->select([
            'o.type',
            'o.contractor_id',
            'delay_fact' => 'AVG({{o}}.[[delay]])',
            'amount' => 'SUM({{o}}.[[amount]])',
        ])->from([
            'o' => $query1->union($query2, true)->union($query3, true),
        ])->groupBy([
            'type',
            'contractor_id',
        ]);

        $sql = $query->createCommand()->rawSql;
        $comand = $db->createCommand("DROP TEMPORARY TABLE IF EXISTS __payments");
        $comand->execute();
        $comand = $db->createCommand("CREATE TEMPORARY TABLE __payments {$sql}");
        $comand->execute();

        $query = (new Query())->select([
            'type',
            'year',
            'month',
            'contractor_id',
            'proceeds' => 'SUM([[total_amount]])',
            'purchase' => 'SUM([[purchase_amount]])',
            'margin' => 'SUM([[margin]])',
        ])->from([
            't' => ProductTurnover::tableName(),
        ])->andWhere([
            'company_id' => $this->_company->id,
            'is_invoice_actual' => 1,
            'is_document_actual' => 1,
        ])->andWhere([
            'between',
            'date',
            $this->_from->format('Y-m-d'),
            $this->_till->format('Y-m-d'),
        ])->groupBy([
            'type',
            'year',
            'month',
            'contractor_id',
        ])->orderBy([
            'proceeds' => SORT_DESC,
        ]);

        $sql = $query->createCommand()->rawSql;
        $comand = $db->createCommand("DROP TEMPORARY TABLE IF EXISTS __turnover");
        $comand->execute();
        $comand = $db->createCommand("CREATE TEMPORARY TABLE __turnover {$sql}");
        $comand->execute();
    }

    /**
     * @param bool $short
     * @return string
     */
    public static function contractorName($typeId, $name, $faceType, $foreign_legal_form)
    {
        if ($faceType == Contractor::TYPE_FOREIGN_LEGAL_PERSON) {
            $title = (string) $foreign_legal_form;
        } else {
            $title = (string) CompanyType::getValue($typeId, 'name_short');
        }

        if (!empty($title)) {
            $title .= ' ';
        }
        $title .= $name;

        return $title;
    }
}
