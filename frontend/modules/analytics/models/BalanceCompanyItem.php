<?php

namespace frontend\modules\analytics\models;

use Yii;
use common\models\Company;

/**
 * This is the model class for table "balance_company_item".
 *
 * @property integer $company_id
 * @property integer $item_id
 * @property integer $sort
 *
 * @property Company $company
 * @property BalanceItem $item
 */
class BalanceCompanyItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'balance_company_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'item_id', 'sort'], 'required'],
            [['company_id', 'item_id', 'sort'], 'integer'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => BalanceItem::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'item_id' => 'Item ID',
            'sort' => 'Sort',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(BalanceItem::className(), ['id' => 'item_id']);
    }

    /**
     *
     */
    public static function loadCompanyItems()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        /* @var $balanceItem BalanceItem */
        foreach (BalanceItem::find()->all() as $balanceItem) {
            if (!self::find()
                ->andWhere(['and',
                    ['company_id' => $company->id],
                    ['item_id' => $balanceItem->id],
                ])->exists()) {
                $balanceCompanyItem = new static();
                $balanceCompanyItem->company_id = $company->id;
                $balanceCompanyItem->item_id = $balanceItem->id;
                $balanceCompanyItem->sort = $balanceItem->sort;
                $balanceCompanyItem->save();
            }
        }
    }
}
