<?php
namespace frontend\modules\analytics\models;

use common\components\excel\Excel;
use common\components\TextHelper;
use common\models\document\Invoice;
use frontend\components\PageSize;
use common\models\Contractor;
use frontend\modules\analytics\models\debtor\DebtorHelper2;
use Yii;
use common\models\document\status\InvoiceStatus;
use frontend\models\Documents;
use yii\data\SqlDataProvider;
use yii\helpers\ArrayHelper;

class DebtReportSearchAsBalance extends \yii\base\Model {

    /**
     *
     */
    const REPORT_TYPE_MONEY = 1;
    const REPORT_TYPE_GUARANTY = 2;

    /**
     *
     */
    const SEARCH_BY_INVOICES = 1;
    const SEARCH_BY_DOCUMENTS = 2;
    const SEARCH_BY_DEBT_PERIODS = 3;

    /**
     * @var
     */
    private $_year = null;

    /**
     * @var
     */
    private $company_id;

    /**
     * @var array
     */
    public $totalsByYear = [];

    /**
     * @var int
     */
    private $_search_by = self::SEARCH_BY_INVOICES;

    /**
     * @var int
     */
    private $_report_type = self::REPORT_TYPE_MONEY;

    /**
     * @var
     */
    public $contractor_id;

    /**
     * @var
     */
    public $responsible_employee_id;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
            [['contractor_id', 'responsible_employee_id'], 'integer']
        ];
    }

    public function init()
    {
        $this->company_id = Yii::$app->user->identity->company->id;
        $this->year = \Yii::$app->session->get('modules.reports.finance.year', date('Y'));
    }

    public function getTradeReceivables($type, $filterByTitle = null)
    {
        $this->load(Yii::$app->request->get());
        return $this->__getOverdueInvoicesDynamic($this->year, $type, $filterByTitle);
    }

    public function getPrepaymentAmount($type, $filterByTitle = null)
    {
        $this->load(Yii::$app->request->get());
        $docType = $type;
        $flowType = ($type == 1) ? 0 : 1;
        return $this->__getPrepaid($this->year, $docType, $flowType, $filterByTitle);
    }

    public function setYear($year)
    {
        $this->_year = (int)$year ?: date('Y');
    }

    public function getYear()
    {
        return $this->_year ?: date('Y');
    }

    public function getIsCurrentYear()
    {
        return date('Y') == $this->year;
    }

    public function setReport_type($val) {

        if (in_array($val, [self::REPORT_TYPE_MONEY, self::REPORT_TYPE_GUARANTY]))
            $this->_report_type = $val;
    }

    public function setSearch_by($val) {

        if (in_array($val, [self::SEARCH_BY_INVOICES, self::SEARCH_BY_DOCUMENTS, self::SEARCH_BY_DEBT_PERIODS]))
            $this->_search_by = $val;
    }

    // FROM BALANCE QUERIES ////////////////////////////////////////////////////////////////////////////////

    private function __getOverdueInvoicesDynamic($year, $type, $filterByTitle = null)
    {
        if ($filterByTitle) {
            $contractorsIds = Contractor::find()
                ->where(['company_id' => $this->company_id])
                ->andFilterWhere(['like', 'name', $filterByTitle])
                ->andFilterWhere(['responsible_employee_id' => $this->responsible_employee_id])
                ->select('id')
                ->column(Yii::$app->db2);
            if (empty($contractorsIds))
                $contractorsIds = [-1];
        } else {
            $contractorsIds = [];
        }

        $helper = new DebtorHelper2();
        $helper::$SEARCH_BY = $this->_search_by;
        $helper::$REPORT_TYPE = $this->_report_type;

        $subQuery = ($this->_search_by == self::SEARCH_BY_INVOICES)
            ? $helper::getQueryByInvoices($this->company_id, $type, $contractorsIds, $this->responsible_employee_id)
            : $helper::getQueryByDocs($this->company_id, $type, $contractorsIds, $this->responsible_employee_id);

        $calc = [];
        for ($m = 1; $m <= 12; $m++) {
            $day = cal_days_in_month(CAL_GREGORIAN, $m, $year);
            $date = sprintf("%04d-%02d-%02d", $year, $m, $day);
            $calc[$m] = "IF(t.date <= \"{$date}\", invoice_sum - flow_sum, 0)";
        }

        $query = "
            SELECT
              contractor_id,
              GREATEST(SUM({$calc[1]}), 0)  month_1,
              GREATEST(SUM({$calc[2]}), 0)  month_2,
              GREATEST(SUM({$calc[3]}), 0)  month_3,
              GREATEST(SUM({$calc[4]}), 0)  month_4,
              GREATEST(SUM({$calc[5]}), 0)  month_5,
              GREATEST(SUM({$calc[6]}), 0)  month_6,
              GREATEST(SUM({$calc[7]}), 0)  month_7,
              GREATEST(SUM({$calc[8]}), 0)  month_8,
              GREATEST(SUM({$calc[9]}), 0)  month_9,
              GREATEST(SUM({$calc[10]}), 0) month_10,
              GREATEST(SUM({$calc[11]}), 0) month_11,
              GREATEST(SUM({$calc[12]}), 0) month_12
            FROM (" . $subQuery->createCommand()->rawSql . ") t
            GROUP BY contractor_id
            HAVING (month_1 + month_2 + month_3 + month_4 + month_5 + month_6 + month_7 + month_8 + month_9 + month_10 + month_11 + month_12) > 0
        ";

        $_tmpTotalByYear = Yii::$app->db2->createCommand($query)->queryAll();
        $this->totalsByYear = ['contractor_id' => 0, 'month_1' => 0, 'month_2' => 0, 'month_3' => 0, 'month_4' => 0, 'month_5' => 0, 'month_6' => 0, 'month_7' => 0, 'month_8' => 0, 'month_9' => 0, 'month_10' => 0, 'month_11' => 0, 'month_12' => 0];
        foreach ((array)$_tmpTotalByYear as $_tmpRow)
            foreach ($_tmpRow as $_tmpKey => $_tmpValue)
                $this->totalsByYear[$_tmpKey] += $_tmpValue;

        $totalCount = $this->__getContractorsCountWithOverdueInvoices($type, $filterByTitle);

        $provider = new SqlDataProvider([
            'db' => Yii::$app->db2,
            'sql' => $query,
            'totalCount' => $totalCount, // from DebtReportSearch2
            'pagination' => [
                'pageSize' => PageSize::get(),
            ],
            'sort' => [
                'attributes' => [
                    'month_1',
                    'month_2',
                    'month_3',
                    'month_4',
                    'month_5',
                    'month_6',
                    'month_7',
                    'month_8',
                    'month_9',
                    'month_10',
                    'month_11',
                    'month_12',
                ],
                'defaultOrder' => [
                    'month_' . date('n') => SORT_DESC
                ]
            ],
        ]);

        return $provider;
    }

    private function __getPrepaid($year, $docType, $flowType, $filterByTitle)
    {
        // SWITCH TYPE BY REPORT
        if ($this->_report_type == self::REPORT_TYPE_GUARANTY) {
            $docType = ($docType == 2) ? 1 : 2;
            $flowType = ($flowType == 1) ? 0 : 1;
        }

        $tableDocs = 'olap_invoices';
        $tableFlows = 'olap_flows';
        $companyId = $this->company_id;
        $seq = self::__getSeq($year);
        $interval = self::__getInterval($year);
        $SQL_FILTER_CONTRACTOR = $this->__getSqlContractorTitleFilter($filterByTitle);
        $SQL_FILTER_ITEMS = DebtorHelper2::getSqlFilterItems($flowType);
        $SQL_FILTER_WALLETS = DebtorHelper2::getSqlFilterWallets();
        $AND_HIDE_TIN_PARENT = $this->__getSqlHideTinParent();

        $query = "
                    SELECT
                    contractor_id,
                    SUM(month_1) month_1,
                    SUM(month_2) month_2,
                    SUM(month_3) month_3,
                    SUM(month_4) month_4,
                    SUM(month_5) month_5,
                    SUM(month_6) month_6,
                    SUM(month_7) month_7,
                    SUM(month_8) month_8,
                    SUM(month_9) month_9,
                    SUM(month_10) month_10,
                    SUM(month_11) month_11,
                    SUM(month_12) month_12
                    FROM (
                        SELECT
                        t.contractor_id,
                        SUM(IF(MONTH({$interval}) =  1, IFNULL(t.total_amount_with_nds, 0), 0)) month_1,
                        SUM(IF(MONTH({$interval}) =  2, IFNULL(t.total_amount_with_nds, 0), 0)) month_2,
                        SUM(IF(MONTH({$interval}) =  3, IFNULL(t.total_amount_with_nds, 0), 0)) month_3,
                        SUM(IF(MONTH({$interval}) =  4, IFNULL(t.total_amount_with_nds, 0), 0)) month_4,
                        SUM(IF(MONTH({$interval}) =  5, IFNULL(t.total_amount_with_nds, 0), 0)) month_5,
                        SUM(IF(MONTH({$interval}) =  6, IFNULL(t.total_amount_with_nds, 0), 0)) month_6,
                        SUM(IF(MONTH({$interval}) =  7, IFNULL(t.total_amount_with_nds, 0), 0)) month_7,
                        SUM(IF(MONTH({$interval}) =  8, IFNULL(t.total_amount_with_nds, 0), 0)) month_8,
                        SUM(IF(MONTH({$interval}) =  9, IFNULL(t.total_amount_with_nds, 0), 0)) month_9,
                        SUM(IF(MONTH({$interval}) = 10, IFNULL(t.total_amount_with_nds, 0), 0)) month_10,
                        SUM(IF(MONTH({$interval}) = 11, IFNULL(t.total_amount_with_nds, 0), 0)) month_11,
                        SUM(IF(MONTH({$interval}) = 12, IFNULL(t.total_amount_with_nds, 0), 0)) month_12
                        FROM {$seq}
                        JOIN {$tableDocs} t
                            ON  (t.company_id = {$companyId})
                            AND (FROM_UNIXTIME(t.status_updated_at, '%Y-%m-%d') <= {$interval})
                            AND (t.is_deleted = FALSE)
                            AND (t.type = {$docType})
                            AND (t.status_id = " . InvoiceStatus::STATUS_PAYED . ")
                            AND (t.has_doc = FALSE OR (t.has_doc = TRUE AND t.doc_date > {$interval}))
                            AND (t.need_all_docs = TRUE)
                            {$SQL_FILTER_CONTRACTOR}
                        GROUP BY t.contractor_id

                        UNION ALL

                        SELECT
                        contractor_id,
                        SUM(IF(MONTH({$interval}) =  1, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) month_1,
                        SUM(IF(MONTH({$interval}) =  2, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) month_2,
                        SUM(IF(MONTH({$interval}) =  3, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) month_3,
                        SUM(IF(MONTH({$interval}) =  4, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) month_4,
                        SUM(IF(MONTH({$interval}) =  5, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) month_5,
                        SUM(IF(MONTH({$interval}) =  6, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) month_6,
                        SUM(IF(MONTH({$interval}) =  7, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) month_7,
                        SUM(IF(MONTH({$interval}) =  8, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) month_8,
                        SUM(IF(MONTH({$interval}) =  9, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) month_9,
                        SUM(IF(MONTH({$interval}) = 10, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) month_10,
                        SUM(IF(MONTH({$interval}) = 11, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) month_11,
                        SUM(IF(MONTH({$interval}) = 12, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) month_12
                        FROM {$seq}
                        JOIN {$tableFlows} t
                            ON  (t.company_id = {$companyId})
                            AND (t.recognition_date <= {$interval})
                            AND (t.type = {$flowType})
                            AND (t.contractor_id > 0)
                            {$SQL_FILTER_ITEMS}
                            {$SQL_FILTER_WALLETS}
                            {$SQL_FILTER_CONTRACTOR}
                            {$AND_HIDE_TIN_PARENT}
                        GROUP BY t.contractor_id
                ) z

                GROUP BY contractor_id
        ";

        $this->totalsByYear = Yii::$app->db2->createCommand(str_replace('GROUP BY contractor_id', '', $query))->queryOne();

        $provider = new SqlDataProvider([
            'db' => Yii::$app->db2,
            'sql' => $query,
            'totalCount' => 0,
            'pagination' => [
                'pageSize' => 0
            ],
            'sort' => [
                'attributes' => [
                    'month_1',
                    'month_2',
                    'month_3',
                    'month_4',
                    'month_5',
                    'month_6',
                    'month_7',
                    'month_8',
                    'month_9',
                    'month_10',
                    'month_11',
                    'month_12',
                ],
                'defaultOrder' => [
                    'month_' . date('n') => SORT_DESC
                ]
            ],
        ]);

        return $provider;
    }

    public function getEmployersFilter()
    {
        $dataArray = Yii::$app->user->identity->company
            ->getEmployeeCompanies()
            ->orderBy(['lastname' => SORT_ASC, 'firstname_initial' => SORT_ASC, 'patronymic_initial' => SORT_ASC])
            ->all();

        return ['' => 'Все'] + ArrayHelper::map($dataArray, 'employee_id', function ($model) {
                return $model->getFio(true);
            });
    }

    /**
     * @param $year
     * @return string
     */
    private static function __getSeq($year)
    {
        return 'seq_0_to_' . ($year == date('Y') ? (date('n') - 1) : 11);
    }

    /**
     * @param $year
     * @return string
     */
    private static function __getInterval($year)
    {
        $startDate = $year . '-01-31';

        if ($year == date('Y')) {
            $lastDate = date('Y-m-d');
            $replacedDate = date('Y-m-t');

            return "IF (('{$startDate}' + INTERVAL (seq) MONTH) = '{$replacedDate}', '{$lastDate}', ('{$startDate}' + INTERVAL (seq) MONTH))";
        }

        return "('{$startDate}' + INTERVAL (seq) MONTH)";
    }

    /**
     * @param $title
     * @return string|null
     */
    private function __getSqlContractorTitleFilter($title)
    {
        if ($title || $this->responsible_employee_id) {
            $contractorsIds = Contractor::find()
                ->where(['company_id' => $this->company_id])
                ->andFilterWhere(['like', 'name', $title])
                ->andFilterWhere(['responsible_employee_id' => $this->responsible_employee_id])
                ->select('id')
                ->column(Yii::$app->db2);

            return ($contractorsIds) ? (' AND t.contractor_id IN ('.implode(',', $contractorsIds).')') : ' AND 1<>1';
        }

        return null;
    }

    /**
     * @param $type
     * @param $contractorTitleLike
     * @return int|string
     */
    private function __getContractorsCountWithOverdueInvoices($type, $contractorTitleLike)
    {
        $query = Contractor::find()
            ->distinct('id')
            ->leftJoin('invoice', 'invoice.contractor_id = contractor.id')
            ->where([
                'or',
                ['contractor.company_id' => null],
                ['contractor.company_id' => $this->company_id]
            ])
            ->andWhere(['invoice.company_id' => $this->company_id])
            ->andWhere([
                'contractor.type' => $type,
                'invoice.type' => $type,
                'invoice.invoice_status_id' => InvoiceStatus::$payAllowed,
                'invoice.is_deleted' => false,
            ]);

        // SEARCH_BY_DOCUMENTS
        if ($this->_search_by == self::SEARCH_BY_DOCUMENTS) {
            $query->andWhere(['or',
                ['or',
                    ['invoice.has_act' => true],
                    ['invoice.has_packing_list' => true],
                    ['invoice.has_upd' => true]
                ],
                ['or',
                    ['invoice.need_act' => false],
                    ['invoice.need_packing_list' => false],
                    ['invoice.need_upd' => false]
                ],
            ]);
        }

        $query->andFilterWhere(['like', 'contractor.name', $contractorTitleLike]);
        $query->andFilterWhere(['contractor.responsible_employee_id' => $this->responsible_employee_id]);

        return $query->count('id', Yii::$app->db2);
    }

    // XLS ////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @param $type
     * @throws \Exception
     */
    public function generateXls($type)
    {
        $searchBy = Yii::$app->request->get('search_by', self::SEARCH_BY_INVOICES);
        $reportType = Yii::$app->request->get('report_type', self::REPORT_TYPE_MONEY);
        $ioType = Yii::$app->request->get('io_type', 2);

        $this->_search_by = $searchBy;
        $this->_report_type = $reportType;

        if ($reportType == 1) {
            $dataProvider = $this->getTradeReceivables($ioType);
        } else {
            $dataProvider = $this->getPrepaymentAmount($ioType);
        }

        $dataProvider->setPagination(false);
        $data = $dataProvider->getModels();

        // header
        $columns = [];
        $columns[] = [
            'attribute' => 'name',
            'header' => 'Название',
        ];
        foreach (AbstractFinance::$month as $monthNumber => $monthText) {
            $monthNumber = (int)$monthNumber;
            $columns[] = [
                'attribute' => "month_" . (int)$monthNumber,
                'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                'header' => $monthText,
            ];
        }

        // data
        $formattedData = [];
        foreach ($data as $d) {
            $formattedData[] = $this->_buildXlsRow($d);
        }

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $formattedData,
            'title' => ($ioType == 2 ? "Нам должны" : "Мы должны") . " за {$this->year} год",
            'rangeHeader' => range('A', 'M'),
            'columns' => $columns,
            'format' => 'Xlsx',
            'fileName' => ($ioType == 2 ? "Нам должны" : "Мы должны") . " за {$this->year} год",
        ]);
    }

    protected function _buildXlsRow($data)
    {
        $data['name'] = ($c = Contractor::findOne($data['contractor_id'])) ? $c->getNameWithType() : '---';
        for ($m=1; $m<=12; $m++) {
            if ($this->getIsCurrentYear() && $m > date('m'))
                $data["month_{$m}"] = 0;
            else
                $data["month_{$m}"] = round(($data["month_{$m}"] ?? 0) / 100, 2);
        }
        return $data;
    }



    // PREPAYMENT CHARTS

    // Top chart #1
    public function getTotalPrepayments($type)
    {
        // SWITCH TYPE BY REPORT
        if ($this->_report_type == self::REPORT_TYPE_GUARANTY) {
            $docType = ($type == 2) ? 1 : 2;
            $flowType = ($type == 2) ? 0 : 1;
        } else {
            $docType = $type;
            $flowType = ($type == 2) ? 1 : 0;
        }

        $tableDocs = 'olap_invoices';
        $tableFlows = 'olap_flows';
        $tableContractor = 'contractor';
        $companyId = $this->company_id;
        $currDate = $this->isCurrentYear ? date('Y-m-d') : ($this->year.'-12-31');
        $SQL_FILTER_ITEMS = DebtorHelper2::getSqlFilterItems($flowType);
        $SQL_FILTER_WALLETS = DebtorHelper2::getSqlFilterWallets();
        $AND_HIDE_TIN_PARENT = DebtorHelper2::getSqlHideTinParent();
        $guarantyDelay = $docType == Documents::IO_TYPE_IN ? 'seller_guaranty_delay' : 'customer_guaranty_delay';
        $query = "
                    SELECT
                    /*contractor_id,*/
                    SUM(current_debt_sum) current_debt_sum,
                    SUM(overdue_debt_sum) overdue_debt_sum,
                    SUM(debt_0_10_sum) debt_0_10_sum,
                    SUM(debt_11_30_sum) debt_11_30_sum,
                    SUM(debt_31_60_sum) debt_31_60_sum,
                    SUM(debt_61_90_sum) debt_61_90_sum,
                    SUM(debt_more_90_sum) debt_more_90_sum,
                    SUM(debt_all_sum) debt_all_sum
                    FROM (
                        SELECT
                          /*t.contractor_id,*/
                          SUM(IF(t.date >= '$currDate' - INTERVAL c.{$guarantyDelay} DAY, t.total_amount_with_nds, 0)) AS current_debt_sum,
                          SUM(IF(t.date  < '$currDate' - INTERVAL c.{$guarantyDelay} DAY, t.total_amount_with_nds, 0)) AS overdue_debt_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL  1 DAY AND t.date >= '$currDate' - INTERVAL 10 DAY, t.total_amount_with_nds, 0)) AS debt_0_10_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 11 DAY AND t.date >= '$currDate' - INTERVAL 30 DAY, t.total_amount_with_nds, 0)) AS debt_11_30_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 31 DAY AND t.date >= '$currDate' - INTERVAL 60 DAY, t.total_amount_with_nds, 0)) AS debt_31_60_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 61 DAY AND t.date >= '$currDate' - INTERVAL 90 DAY, t.total_amount_with_nds, 0)) AS debt_61_90_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 91 DAY, t.total_amount_with_nds, 0)) AS debt_more_90_sum,
                          SUM(t.total_amount_with_nds) AS debt_all_sum
                        FROM {$tableDocs} t
                        LEFT JOIN {$tableContractor} c ON t.contractor_id = c.id
                        WHERE (t.company_id = {$companyId})
                          AND (FROM_UNIXTIME(t.status_updated_at, '%Y-%m-%d') <= '{$currDate}')
                          AND (t.is_deleted = FALSE)
                          AND (t.type = {$docType})
                          AND (t.status_id = " . InvoiceStatus::STATUS_PAYED . ")
                          AND (t.has_doc = FALSE OR (t.has_doc = TRUE AND t.doc_date > '{$currDate}'))
                          AND (t.need_all_docs = TRUE)
                        GROUP BY t.contractor_id

                        UNION ALL

                        SELECT
                          /*contractor_id,*/
                          SUM(IF(t.date >= '$currDate' - INTERVAL c.{$guarantyDelay} DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS current_debt_sum,
                          SUM(IF(t.date  < '$currDate' - INTERVAL c.{$guarantyDelay} DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS overdue_debt_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL  1 DAY AND t.date >= '$currDate' - INTERVAL 10 DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS debt_0_10_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 11 DAY AND t.date >= '$currDate' - INTERVAL 30 DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS debt_11_30_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 31 DAY AND t.date >= '$currDate' - INTERVAL 60 DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS debt_31_60_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 61 DAY AND t.date >= '$currDate' - INTERVAL 90 DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS debt_61_90_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 91 DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS debt_more_90_sum,
                          SUM(t.amount - t.invoice_amount) AS debt_all_sum
                        FROM {$tableFlows} t
                        LEFT JOIN {$tableContractor} c ON t.contractor_id = c.id
                        WHERE (t.company_id = {$companyId})
                            AND (t.recognition_date <= '{$currDate}')
                            AND (t.type = {$flowType})
                            AND (t.contractor_id > 0)
                            {$SQL_FILTER_ITEMS}
                            {$SQL_FILTER_WALLETS}
                            {$AND_HIDE_TIN_PARENT}
                        GROUP BY t.contractor_id
                ) z

                /*GROUP BY contractor_id*/
        ";

        //var_dump(Yii::$app->db->createCommand($query)->queryAll());
        //exit;

        return Yii::$app->db->createCommand($query)->queryOne();
    }

    // Top chart #2
    public function getTopPrepaymentByContractors($type, $maxRowsCount)
    {
        // SWITCH TYPE BY REPORT
        if ($this->_report_type == self::REPORT_TYPE_GUARANTY) {
            $docType = ($type == 2) ? 1 : 2;
            $flowType = ($type == 2) ? 0 : 1;
        } else {
            $docType = $type;
            $flowType = ($type == 2) ? 1 : 0;
        }

        $tableDocs = 'olap_invoices';
        $tableFlows = 'olap_flows';
        $tableContractor = 'contractor';
        $companyId = $this->company_id;
        $currDate = $this->isCurrentYear ? date('Y-m-d') : ($this->year.'-12-31');
        $SQL_FILTER_ITEMS = DebtorHelper2::getSqlFilterItems($flowType);
        $SQL_FILTER_WALLETS = DebtorHelper2::getSqlFilterWallets();
        $AND_HIDE_TIN_PARENT = DebtorHelper2::getSqlHideTinParent();
        $guarantyDelay = $docType == Documents::IO_TYPE_IN ? 'seller_guaranty_delay' : 'customer_guaranty_delay';

        $query = "
                    SELECT
                    id,
                    name,
                    company_type_id,
                    face_type,
                    foreign_legal_form,
                    SUM(current_debt_sum) current_debt_sum,
                    SUM(overdue_debt_sum) overdue_debt_sum,
                    SUM(debt_0_10_sum) debt_0_10_sum,
                    SUM(debt_11_30_sum) debt_11_30_sum,
                    SUM(debt_31_60_sum) debt_31_60_sum,
                    SUM(debt_61_90_sum) debt_61_90_sum,
                    SUM(debt_more_90_sum) debt_more_90_sum,
                    SUM(debt_all_sum) debt_all_sum
                    FROM (
                        SELECT
                          t.contractor_id AS id,
                          c.name,
                          c.company_type_id,
                          c.face_type,
                          c.foreign_legal_form,
                          SUM(IF(t.date >= '$currDate' - INTERVAL c.{$guarantyDelay} DAY, t.total_amount_with_nds, 0)) AS current_debt_sum,
                          SUM(IF(t.date  < '$currDate' - INTERVAL c.{$guarantyDelay} DAY, t.total_amount_with_nds, 0)) AS overdue_debt_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL  1 DAY AND t.date >= '$currDate' - INTERVAL 10 DAY, t.total_amount_with_nds, 0)) AS debt_0_10_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 11 DAY AND t.date >= '$currDate' - INTERVAL 30 DAY, t.total_amount_with_nds, 0)) AS debt_11_30_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 31 DAY AND t.date >= '$currDate' - INTERVAL 60 DAY, t.total_amount_with_nds, 0)) AS debt_31_60_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 61 DAY AND t.date >= '$currDate' - INTERVAL 90 DAY, t.total_amount_with_nds, 0)) AS debt_61_90_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 91 DAY, t.total_amount_with_nds, 0)) AS debt_more_90_sum,
                          SUM(t.total_amount_with_nds) AS debt_all_sum
                        FROM {$tableDocs} t
                        LEFT JOIN {$tableContractor} c ON t.contractor_id = c.id
                        WHERE (t.company_id = {$companyId})
                          AND (FROM_UNIXTIME(t.status_updated_at, '%Y-%m-%d') <= '{$currDate}')
                          AND (t.is_deleted = FALSE)
                          AND (t.type = {$docType})
                          AND (t.status_id = " . InvoiceStatus::STATUS_PAYED . ")
                          AND (t.has_doc = FALSE OR (t.has_doc = TRUE AND t.doc_date > '{$currDate}'))
                          AND (t.need_all_docs = TRUE)
                        GROUP BY t.contractor_id

                        UNION ALL

                        SELECT
                          t.contractor_id AS id,
                          c.name,
                          c.company_type_id,
                          c.face_type,
                          c.foreign_legal_form,
                          SUM(IF(t.date >= '$currDate' - INTERVAL c.{$guarantyDelay} DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS current_debt_sum,
                          SUM(IF(t.date  < '$currDate' - INTERVAL c.{$guarantyDelay} DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS overdue_debt_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL  1 DAY AND t.date >= '$currDate' - INTERVAL 10 DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS debt_0_10_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 11 DAY AND t.date >= '$currDate' - INTERVAL 30 DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS debt_11_30_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 31 DAY AND t.date >= '$currDate' - INTERVAL 60 DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS debt_31_60_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 61 DAY AND t.date >= '$currDate' - INTERVAL 90 DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS debt_61_90_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 91 DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS debt_more_90_sum,
                          SUM(IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount)) AS debt_all_sum
                        FROM {$tableFlows} t
                        LEFT JOIN {$tableContractor} c ON t.contractor_id = c.id
                        WHERE (t.company_id = {$companyId})
                            AND (t.recognition_date <= '{$currDate}')
                            AND (t.type = {$flowType})
                            AND (t.contractor_id > 0)
                            {$SQL_FILTER_ITEMS}
                            {$SQL_FILTER_WALLETS}
                            {$AND_HIDE_TIN_PARENT}
                        GROUP BY t.contractor_id
                ) z

                GROUP BY id
                ORDER BY debt_all_sum DESC
                LIMIT {$maxRowsCount}
        ";

        //var_dump(Yii::$app->db->createCommand($query)->queryAll());
        //exit;

        return Yii::$app->db->createCommand($query)->queryAll();
    }

    // Top chart #3/1 (data + tooltip data)
    public function getTopPrepaymentByEmployersArr($type, $maxRowsCount = 5, $maxTooltipRowsCount = 5)
    {
        // SWITCH TYPE BY REPORT
        if ($this->_report_type == self::REPORT_TYPE_GUARANTY) {
            $docType = ($type == 2) ? 1 : 2;
            $flowType = ($type == 2) ? 0 : 1;
        } else {
            $docType = $type;
            $flowType = ($type == 2) ? 1 : 0;
        }

        $tableDocs = 'olap_invoices';
        $tableFlows = 'olap_flows';
        $tableContractor = 'contractor';
        $companyId = $this->company_id;
        $currDate = $this->isCurrentYear ? date('Y-m-d') : ($this->year.'-12-31');
        $SQL_FILTER_ITEMS = DebtorHelper2::getSqlFilterItems($flowType);
        $SQL_FILTER_WALLETS = DebtorHelper2::getSqlFilterWallets();
        $AND_HIDE_TIN_PARENT = DebtorHelper2::getSqlHideTinParent();
        $guarantyDelay = $docType == Documents::IO_TYPE_IN ? 'seller_guaranty_delay' : 'customer_guaranty_delay';

        $query = "
                    SELECT
                    z.responsible_employee_id,
                    z.id,
                    z.name,
                    SUM(z.current_debt_sum) current_debt_sum,
                    SUM(z.overdue_debt_sum) overdue_debt_sum,
                    SUM(z.debt_0_10_sum) debt_0_10_sum,
                    SUM(z.debt_11_30_sum) debt_11_30_sum,
                    SUM(z.debt_31_60_sum) debt_31_60_sum,
                    SUM(z.debt_61_90_sum) debt_61_90_sum,
                    SUM(z.debt_more_90_sum) debt_more_90_sum,
                    SUM(z.debt_all_sum) debt_all_sum
                    FROM (
                        SELECT
                          c.responsible_employee_id,
                          t.contractor_id AS id,
                          t.contractor_id AS name,
                          SUM(IF(t.date >= '$currDate' - INTERVAL c.{$guarantyDelay} DAY, t.total_amount_with_nds, 0)) AS current_debt_sum,
                          SUM(IF(t.date  < '$currDate' - INTERVAL c.{$guarantyDelay} DAY, t.total_amount_with_nds, 0)) AS overdue_debt_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL  1 DAY AND t.date >= '$currDate' - INTERVAL 10 DAY, t.total_amount_with_nds, 0)) AS debt_0_10_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 11 DAY AND t.date >= '$currDate' - INTERVAL 30 DAY, t.total_amount_with_nds, 0)) AS debt_11_30_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 31 DAY AND t.date >= '$currDate' - INTERVAL 60 DAY, t.total_amount_with_nds, 0)) AS debt_31_60_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 61 DAY AND t.date >= '$currDate' - INTERVAL 90 DAY, t.total_amount_with_nds, 0)) AS debt_61_90_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 91 DAY, t.total_amount_with_nds, 0)) AS debt_more_90_sum,
                          SUM(t.total_amount_with_nds) AS debt_all_sum
                        FROM {$tableDocs} t
                        LEFT JOIN {$tableContractor} c ON t.contractor_id = c.id
                        WHERE (t.company_id = {$companyId})
                          AND (FROM_UNIXTIME(t.status_updated_at, '%Y-%m-%d') <= '{$currDate}')
                          AND (t.is_deleted = FALSE)
                          AND (t.type = {$docType})
                          AND (t.status_id = " . InvoiceStatus::STATUS_PAYED . ")
                          AND (t.has_doc = FALSE OR (t.has_doc = TRUE AND t.doc_date > '{$currDate}'))
                          AND (t.need_all_docs = TRUE)
                        GROUP BY t.contractor_id

                        UNION ALL

                        SELECT
                          c.responsible_employee_id,
                          t.contractor_id AS id,
                          t.contractor_id AS name,
                          SUM(IF(t.date >= '$currDate' - INTERVAL c.{$guarantyDelay} DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS current_debt_sum,
                          SUM(IF(t.date  < '$currDate' - INTERVAL c.{$guarantyDelay} DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS overdue_debt_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL  1 DAY AND t.date >= '$currDate' - INTERVAL 10 DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS debt_0_10_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 11 DAY AND t.date >= '$currDate' - INTERVAL 30 DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS debt_11_30_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 31 DAY AND t.date >= '$currDate' - INTERVAL 60 DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS debt_31_60_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 61 DAY AND t.date >= '$currDate' - INTERVAL 90 DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS debt_61_90_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 91 DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS debt_more_90_sum,
                          SUM(IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount)) AS debt_all_sum
                        FROM {$tableFlows} t
                        LEFT JOIN {$tableContractor} c ON t.contractor_id = c.id
                        WHERE (t.company_id = {$companyId})
                            AND (t.recognition_date <= '{$currDate}')
                            AND (t.type = {$flowType})
                            AND (t.contractor_id > 0)
                            {$SQL_FILTER_ITEMS}
                            {$SQL_FILTER_WALLETS}
                            {$AND_HIDE_TIN_PARENT}
                        GROUP BY t.contractor_id
                ) z

                WHERE debt_all_sum > 0

                GROUP BY id
                ORDER BY debt_all_sum DESC
                LIMIT {$maxRowsCount}
        ";

        $rawData  = Yii::$app->db->createCommand($query)->queryAll();
        $chartData = $tooltipData = [];
        $_tmpTooltipData = [];
        foreach ($rawData as $v)
        {
            if (!isset($chartData[$v['responsible_employee_id']]))
                $chartData[$v['responsible_employee_id']] = [
                    'responsible_employee_id' => $v['responsible_employee_id'],
                    'sum' => 0
                ];

            $chartData[$v['responsible_employee_id']]['sum'] += $v['debt_all_sum'];

            if (!isset($_tmpTooltipData[$v['responsible_employee_id']]))
                $_tmpTooltipData[$v['responsible_employee_id']] = [];

            $_tmpTooltipData[$v['responsible_employee_id']][] = [
                'period' => ($c = Contractor::findOne($v['id'])) ? $c->getNameWithType() : $v['name'],
                'sum'  => $v['debt_all_sum'],
                'percent' => 0
            ];
        }

        uasort($chartData, function ($a, $b) {
            return $b['sum'] <=> $a['sum'];
        });

        $chartData = array_slice($chartData, 0, $maxRowsCount, true);

        foreach ($chartData as $employeeId => $value) {
            $pos = 0;
            foreach ((array)$_tmpTooltipData[$employeeId] as $kk => $vv) {
                if (++$pos > $maxTooltipRowsCount)
                    break;
                $tooltipData[$employeeId][$kk]['period'] = $vv['period'];
                $tooltipData[$employeeId][$kk]['percent'] = 100 * min(100, $vv['sum'] / (1 / 1E9 + max(array_column($_tmpTooltipData[$employeeId], 'sum'))));
                $tooltipData[$employeeId][$kk]['sum'] = TextHelper::invoiceMoneyFormat($vv['sum'], 2);
            }
        }

        return [
            'chart' => $chartData,
            'tooltip' => array_values($tooltipData)
        ];
    }

    // Top chart #3/2
    public function getTopPrepaymentByExpenditureItem($type, $maxRowsCount)
    {
        // SWITCH TYPE BY REPORT
        if ($this->_report_type == self::REPORT_TYPE_GUARANTY) {
            $docType = ($type == 2) ? 1 : 2;
            $flowType = ($type == 2) ? 0 : 1;
        } else {
            $docType = $type;
            $flowType = ($type == 2) ? 1 : 0;
        }

        $tableDocs = 'olap_invoices';
        $tableFlows = 'olap_flows';
        $tableContractor = 'contractor';
        $companyId = $this->company_id;
        $currDate = $this->isCurrentYear ? date('Y-m-d') : ($this->year.'-12-31');
        $SQL_FILTER_ITEMS = DebtorHelper2::getSqlFilterItems($flowType);
        $SQL_FILTER_WALLETS = DebtorHelper2::getSqlFilterWallets();
        $AND_HIDE_TIN_PARENT = DebtorHelper2::getSqlHideTinParent();
        $guarantyDelay = $docType == Documents::IO_TYPE_IN ? 'seller_guaranty_delay' : 'customer_guaranty_delay';

        $query = "
                    SELECT
                    z.invoice_expenditure_item_id,
                    z.id,
                    z.name,
                    SUM(z.current_debt_sum) current_debt_sum,
                    SUM(z.overdue_debt_sum) overdue_debt_sum,
                    SUM(z.debt_0_10_sum) debt_0_10_sum,
                    SUM(z.debt_11_30_sum) debt_11_30_sum,
                    SUM(z.debt_31_60_sum) debt_31_60_sum,
                    SUM(z.debt_61_90_sum) debt_61_90_sum,
                    SUM(z.debt_more_90_sum) debt_more_90_sum,
                    SUM(z.debt_all_sum) debt_all_sum
                    FROM (
                        SELECT
                          t.expenditure_item_id AS invoice_expenditure_item_id,
                          t.contractor_id AS id,
                          t.contractor_id AS name,
                          SUM(IF(t.date >= '$currDate' - INTERVAL c.{$guarantyDelay} DAY, t.total_amount_with_nds, 0)) AS current_debt_sum,
                          SUM(IF(t.date  < '$currDate' - INTERVAL c.{$guarantyDelay} DAY, t.total_amount_with_nds, 0)) AS overdue_debt_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL  1 DAY AND t.date >= '$currDate' - INTERVAL 10 DAY, t.total_amount_with_nds, 0)) AS debt_0_10_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 11 DAY AND t.date >= '$currDate' - INTERVAL 30 DAY, t.total_amount_with_nds, 0)) AS debt_11_30_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 31 DAY AND t.date >= '$currDate' - INTERVAL 60 DAY, t.total_amount_with_nds, 0)) AS debt_31_60_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 61 DAY AND t.date >= '$currDate' - INTERVAL 90 DAY, t.total_amount_with_nds, 0)) AS debt_61_90_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 91 DAY, t.total_amount_with_nds, 0)) AS debt_more_90_sum,
                          SUM(t.total_amount_with_nds) AS debt_all_sum
                        FROM {$tableDocs} t
                        LEFT JOIN {$tableContractor} c ON t.contractor_id = c.id
                        WHERE (t.company_id = {$companyId})
                          AND (FROM_UNIXTIME(t.status_updated_at, '%Y-%m-%d') <= '{$currDate}')
                          AND (t.is_deleted = FALSE)
                          AND (t.type = {$docType})
                          AND (t.status_id = " . InvoiceStatus::STATUS_PAYED . ")
                          AND (t.has_doc = FALSE OR (t.has_doc = TRUE AND t.doc_date > '{$currDate}'))
                          AND (t.need_all_docs = TRUE)
                        GROUP BY t.expenditure_item_id

                        UNION ALL

                        SELECT
                          t.item_id AS invoice_expenditure_item_id,
                          t.contractor_id AS id,
                          t.contractor_id AS name,
                          SUM(IF(t.date >= '$currDate' - INTERVAL c.{$guarantyDelay} DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS current_debt_sum,
                          SUM(IF(t.date  < '$currDate' - INTERVAL c.{$guarantyDelay} DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS overdue_debt_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL  1 DAY AND t.date >= '$currDate' - INTERVAL 10 DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS debt_0_10_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 11 DAY AND t.date >= '$currDate' - INTERVAL 30 DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS debt_11_30_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 31 DAY AND t.date >= '$currDate' - INTERVAL 60 DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS debt_31_60_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 61 DAY AND t.date >= '$currDate' - INTERVAL 90 DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS debt_61_90_sum,
                          SUM(IF(t.date <= '$currDate' - INTERVAL 91 DAY, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS debt_more_90_sum,
                          SUM(IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount)) AS debt_all_sum
                        FROM {$tableFlows} t
                        LEFT JOIN {$tableContractor} c ON t.contractor_id = c.id
                        WHERE (t.company_id = {$companyId})
                            AND (t.recognition_date <= '{$currDate}')
                            AND (t.type = {$flowType})
                            AND (t.contractor_id > 0)
                            {$SQL_FILTER_ITEMS}
                            {$SQL_FILTER_WALLETS}
                            {$AND_HIDE_TIN_PARENT}
                        GROUP BY t.item_id
                ) z

                WHERE debt_all_sum > 0

                GROUP BY invoice_expenditure_item_id
                ORDER BY debt_all_sum DESC
                LIMIT {$maxRowsCount}
        ";

        return Yii::$app->db->createCommand($query)->queryAll();
    }

    protected function __getSqlHideTinParent()
    {
        return " AND t.has_tin_children = 0 ";
    }
}