<?php

namespace frontend\modules\analytics\models\income;

use common\models\cash\CashFlowsBase;
use common\models\Company;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\OrderAct;
use common\models\document\OrderPackingList;
use common\models\document\OrderUpd;
use common\models\document\PackingList;
use common\models\document\status\ActStatus;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\PackingListStatus;
use common\models\document\status\UpdStatus;
use common\models\document\Upd;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use common\models\product\Product;
use frontend\models\Documents;
use yii\db\Expression;
use yii\db\Query;
use common\models\document\Order;

class TopChartsQuery {

    public static function payQuery(Company $company, $dateFrom, $dateTill, $article = null)
    {
        $paySelect = ['link.amount', 'link.invoice_id', 'flow.date', 'flow.income_item_id'];
        $payWhere = [
            'and',
            ['flow.company_id' => $company->id],
            ['flow.flow_type' => CashFlowsBase::FLOW_TYPE_INCOME],
            ['between', 'flow.date', $dateFrom, $dateTill],
        ];
        if ($article) {
            $payWhere[] = ['flow.income_item_id' => (int)$article];
        }
        $payQuery = new Query;

        return $payQuery->select([
            'invoice_id',
            'flow_sum' => 'SUM([[amount]])',
            'flow_date' => 'date',
        ])->from(['t' => CashFlowsBase::getAllFlows($paySelect, $payWhere, true, 'innerJoin')])->groupBy('t.invoice_id');
    }

    public static function invoiceQuery(Company $company)
    {
        return Invoice::find()->alias('invoice')
            ->innerJoin(['user' => EmployeeCompany::tableName()], [
                'and',
                '{{invoice}}.[[company_id]] = {{user}}.[[company_id]]',
                '{{invoice}}.[[document_author_id]] = {{user}}.[[employee_id]]',
            ])
            ->innerJoin(['employee' => Employee::tableName()], '{{user}}.[[employee_id]] = {{employee}}.[[id]]')
            ->where([
                'invoice.company_id' => $company->id,
                'invoice.type' => Documents::IO_TYPE_OUT,
                'invoice.is_deleted' => false,
                'invoice.invoice_status_id' => InvoiceStatus::$validInvoices,
            ]);
    }
}