<?php

namespace frontend\modules\analytics\models\operationalEfficiency;

use common\components\date\DateHelper;
use common\components\excel\Excel;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\models\AnalyticsArticleForm;
use frontend\modules\analytics\models\BalanceSearch;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;
use InvalidArgumentException;
use Yii;

class OperationalEfficiencySearchModel extends AbstractFinance
{
    public static array $itemsForOperationalEfficiency = [
        [
            'label' => 'Выручка',
            'getter' => 'getRevenue',
        ],
        [
            'getter' => 'getPrimeCosts',
            'isGetterOnly' => true,
        ],
        [
            'label' => 'Переменные расходы',
            'getter' => 'getVariableCosts',
        ],
        [
            'label' => 'Маржинальный доход',
            'getter' => 'getMarginalIncome',
        ],
        [
            'label' => 'Постоянные расходы',
            'getter' => 'getFixedCosts',
        ],
        [
            'label' => 'Валовая прибыль',
            'getter' => 'getGrossProfit',
        ],
        [
            'label' => 'Операционные расходы',
            'getter' => 'getOperatingCosts',
        ],
        [
            'label' => 'Операционная прибыль',
            'getter' => 'getOperatingProfit',
        ],
        [
            'label' => 'Другие доходы',
            'getter' => 'getAnotherIncome',
        ],
        [
            'label' => 'Другие расходы',
            'getter' => 'getAnotherCosts',
        ],
        [
            'label' => 'EBITDA',
            'getter' => 'getEbidta',
            'options' => [
                'class' => 'text-bold expenditure_type',
            ],
        ],
        [
            'getter' => 'getAmortization',
            'isGetterOnly' => true,
        ],
        [
            'getter' => 'getEbit',
            'isGetterOnly' => true,
        ],
        [
            'label' => 'Проценты полученные',
            'getter' => 'getReceivedPercent',
        ],
        [
            'label' => 'Проценты уплаченные',
            'getter' => 'getPaymentPercent',
        ],
        [
            'label' => 'Прибыль до налогообложения',
            'getter' => 'getProfitBeforeTax',
        ],
        [
            'label' => 'Налоги',
            'getter' => 'getTax',
        ],
        [
            'label' => 'Чистая прибыль/убыток',
            'getter' => 'getNetIncomeLoss',
        ],
    ];

    public const MIN_REPORT_DATE = '2000-01-01';

    public const REPORT_ID = 17;

    protected array $_yearFilter;

    protected string $_minCashYearMonth;

    private BalanceSearch $balanceSearchModel;

    private array $data = [];

    public function init(): void {
        parent::init();
        $this->initParams();
        $this->balanceSearchModel = new BalanceSearch(['company' => Yii::$app->user->identity->company]);
    }

    public function initParams(): void {
        $this->_minCashYearMonth = date('Ym');
    }

    public function handleItems(): array {
        if (!isset($this->data[$this->year])) {
            $years = array_reverse($this->getYearFilter());
            $user = Yii::$app->user->identity;
            foreach ($years as $year) {
                $items = [];
                $this->balanceSearchModel->setYear($year);
                $palModel = new ProfitAndLossSearchModel();
                $palModel->year = $year;
                $palModel->setUserOption('skipZeroesRows', false);
                $palModel->setUserOption('calcWithoutNds', false);
                $palModel->setUserOption('revenueRuleGroup', AnalyticsArticleForm::REVENUE_RULE_GROUP_UNSET);
                $palModel->setUserOption('revenueRule', AnalyticsArticleForm::REVENUE_RULE_UNSET);
                $palModel->setUserOption('monthGroupBy', AnalyticsArticleForm::MONTH_GROUP_UNSET);
                $temporaryProfitAndLossItems = $palModel->handleItems(self::$itemsForOperationalEfficiency);
                $profitAndLossItems = [
                    'totalRevenue' => array_merge($temporaryProfitAndLossItems['totalRevenue'], ['class' => 'weight-700']),
                    'totalVariableCosts' => $temporaryProfitAndLossItems['totalVariableCosts'],
                    'marginalIncome' => $temporaryProfitAndLossItems['marginalIncome'],
                    'totalFixedCosts' => $temporaryProfitAndLossItems['totalFixedCosts'],
                    'grossProfit' => array_merge($temporaryProfitAndLossItems['grossProfit'], ['class' => 'weight-700']),
                    'totalOperatingCosts' => $temporaryProfitAndLossItems['totalOperatingCosts'],
                    'operatingProfit' => $temporaryProfitAndLossItems['operatingProfit'],
                    'totalAnotherIncome' => $temporaryProfitAndLossItems['totalAnotherIncome'],
                    'totalAnotherCosts' => $temporaryProfitAndLossItems['totalAnotherCosts'],
                    'totalReceivedPercent' => $temporaryProfitAndLossItems['totalReceivedPercent'],
                    'totalPaymentPercent' => $temporaryProfitAndLossItems['totalPaymentPercent'],
                    'profitBeforeTax' => array_merge($temporaryProfitAndLossItems['profitBeforeTax'], ['class' => 'weight-700']),
                    'tax' => array_merge($temporaryProfitAndLossItems['tax'], ['class' => 'weight-700']),
                    'netIncomeLoss' => array_merge($temporaryProfitAndLossItems['netIncomeLoss'], ['class' => 'weight-700']),
                ];
                $msfoProfitItems = [
                    ['label' => 'EBT'],
                    ['label' => 'EBIT'],
                    [
                        'label' => 'EBITDA',
                        'amount' => $temporaryProfitAndLossItems['ebitda']['amount'],
                    ],
                    [
                        'label' => 'NOPAT',
                        'amount' => $temporaryProfitAndLossItems['netIncomeLoss']['amount'],
                    ],
                ];
                $revenueProfitabilityItems = [
                    ['label' => 'Маржинальность (ROM), %'],
                    ['label' => 'Рентабельность по валовой прибыли (GPM), %'],
                    ['label' => 'Операционная рентабельность (OPM), %'],
                    ['label' => 'Рентабельность по прибыли до налогообложения (ROS), %'],
                    ['label' => 'Рентабельность по чистой прибыли (NPN), %'],
                    ['label' => 'Рентабельность персонала (ROL)'],
                ];
                $returnOnInvestment = [
                    ['label' => 'Операционная рентабельность активов (OROA), %'],
                    ['label' => 'Рентабельность активов (ROA), %'],
                    ['label' => 'Рентабельность собственного капитала (ROE), %'],
                    ['label' => 'Рентабельность совокупных активов (ROTA), %'],
                    ['label' => 'Рентабельность вложенного капитала (ROCE), %'],
                    ['label' => 'Рентабельность инвестиционного капитала (ROIC), %'],
                    ['label' => 'Рентабельность чистых активов (RONA), %'],
                    ['label' => 'Рентабельность оборотных активов (RCA), %'],
                    ['label' => 'Рентабельность основных средств (ROFA), %'],
                ];
                $employeeCount = \Yii::$app->user->identity->company->getEmployees()
                    ->andWhere(['is_working' => true])
                    ->andWhere(['is_deleted' => false])
                    ->andWhere(['is_active' => true])
                    ->count();

                foreach (self::$month as $monthNumber => $monthText) {
                    $ebtAmount = $profitAndLossItems['netIncomeLoss']['amount'][$monthNumber] + $profitAndLossItems['tax']['amount'][$monthNumber];
                    $msfoProfitItems[0]['amount'][$monthNumber] = $ebtAmount;
                    $msfoProfitItems[1]['amount'][$monthNumber] = $ebtAmount + $profitAndLossItems['totalPaymentPercent']['amount'][$monthNumber];
                    $revenue = $profitAndLossItems['totalRevenue']['amount'][$monthNumber] ? $profitAndLossItems['totalRevenue']['amount'][$monthNumber] : 1;
                    $revenueProfitabilityItems[0]['amount'][$monthNumber] = (($profitAndLossItems['marginalIncome']['amount'][$monthNumber] * 100) / $revenue) * 100;
                    $revenueProfitabilityItems[1]['amount'][$monthNumber] = (($profitAndLossItems['grossProfit']['amount'][$monthNumber] * 100) / $revenue) * 100;
                    $revenueProfitabilityItems[2]['amount'][$monthNumber] = (($profitAndLossItems['operatingProfit']['amount'][$monthNumber] * 100) / $revenue) * 100;
                    $revenueProfitabilityItems[3]['amount'][$monthNumber] = (($profitAndLossItems['profitBeforeTax']['amount'][$monthNumber] * 100) / $revenue) * 100;
                    $revenueProfitabilityItems[4]['amount'][$monthNumber] = (($profitAndLossItems['netIncomeLoss']['amount'][$monthNumber] * 100) / $revenue) * 100;
                    $revenueProfitabilityItems[5]['amount'][$monthNumber] = $profitAndLossItems['netIncomeLoss']['amount'][$monthNumber] / ($employeeCount ? $employeeCount : 1);

                    $balanceStartDate = date_create("{$year}-{$monthNumber}-01");
                    $balanceEndDate = clone $balanceStartDate;
                    $balanceEndDate->modify('last day of this month')->setTime(23, 59, 59);
                    $returnOfInvestmentParams = $this->getReturnOfInvestmentParams(
                        $balanceStartDate,
                        $balanceEndDate,
                        $monthNumber,
                        $profitAndLossItems,
                        $msfoProfitItems
                    );

                    $returnOnInvestment[0]['amount'][$monthNumber] = $returnOfInvestmentParams['oroa'];
                    $returnOnInvestment[1]['amount'][$monthNumber] = $returnOfInvestmentParams['roa'];
                    $returnOnInvestment[2]['amount'][$monthNumber] = $returnOfInvestmentParams['roe'];
                    $returnOnInvestment[3]['amount'][$monthNumber] = $returnOfInvestmentParams['rota'];
                    $returnOnInvestment[4]['amount'][$monthNumber] = $returnOfInvestmentParams['roce'];
                    $returnOnInvestment[5]['amount'][$monthNumber] = $returnOfInvestmentParams['roic'];
                    $returnOnInvestment[6]['amount'][$monthNumber] = $returnOfInvestmentParams['rona'];
                    $returnOnInvestment[7]['amount'][$monthNumber] = $returnOfInvestmentParams['rca'];
                    $returnOnInvestment[8]['amount'][$monthNumber] = $returnOfInvestmentParams['rofa'];
                }

                foreach (['quarter-1', 'quarter-2', 'quarter-3', 'quarter-4', 'total'] as $periodKey) {
                    $revenue = $profitAndLossItems['totalRevenue']['amount'][$periodKey] ? $profitAndLossItems['totalRevenue']['amount'][$periodKey] : 1;
                    $netIncomeLossAmount = $profitAndLossItems['netIncomeLoss']['amount'][$periodKey] + $profitAndLossItems['tax']['amount'][$periodKey];
                    $msfoProfitItems[0]['amount'][$periodKey] = $netIncomeLossAmount;
                    $msfoProfitItems[1]['amount'][$periodKey] = $netIncomeLossAmount + $profitAndLossItems['totalPaymentPercent']['amount'][$periodKey];
                    $revenueProfitabilityItems[0]['amount'][$periodKey] = (($profitAndLossItems['marginalIncome']['amount'][$periodKey] * 100) / $revenue) * 100;
                    $revenueProfitabilityItems[1]['amount'][$periodKey] = (($profitAndLossItems['grossProfit']['amount'][$periodKey] * 100) / $revenue) * 100;
                    $revenueProfitabilityItems[2]['amount'][$periodKey] = (($profitAndLossItems['operatingProfit']['amount'][$periodKey] * 100) / $revenue) * 100;
                    $revenueProfitabilityItems[3]['amount'][$periodKey] = (($profitAndLossItems['profitBeforeTax']['amount'][$periodKey] * 100) / $revenue) * 100;
                    $revenueProfitabilityItems[4]['amount'][$periodKey] = (($profitAndLossItems['netIncomeLoss']['amount'][$periodKey] * 100) / $revenue) * 100;
                    $revenueProfitabilityItems[5]['amount'][$periodKey] = $profitAndLossItems['netIncomeLoss']['amount'][$periodKey] / ($employeeCount ? $employeeCount : 1);

                    switch ($periodKey) {
                        case 'quarter-1':
                            $balanceStartDate = date_create("{$year}-01-01");
                            $balanceEndDate = date_create("{$year}-03-01");
                            break;
                        case 'quarter-2':
                            $balanceStartDate = date_create("{$year}-04-01");
                            $balanceEndDate = date_create("{$year}-06-01");
                            break;
                        case 'quarter-3':
                            $balanceStartDate = date_create("{$year}-07-01");
                            $balanceEndDate = date_create("{$year}-09-01");
                            break;
                        case 'quarter-4':
                            $balanceStartDate = date_create("{$year}-10-01");
                            $balanceEndDate = date_create("{$year}-12-31");
                            break;
                        case 'total':
                            $balanceStartDate = date_create("{$year}-01-01");
                            $balanceEndDate = date_create("{$year}-12-31");
                            break;
                        default:
                            throw new InvalidArgumentException('Invalid periodKey param.');
                    }

                    $balanceEndDate->modify('last day of this month')->setTime(23, 59, 59);
                    $returnOfInvestmentParams = $this->getReturnOfInvestmentParams(
                        $balanceStartDate,
                        $balanceEndDate,
                        $periodKey,
                        $profitAndLossItems,
                        $msfoProfitItems
                    );

                    $returnOnInvestment[0]['amount'][$periodKey] = $returnOfInvestmentParams['oroa'];
                    $returnOnInvestment[1]['amount'][$periodKey] = $returnOfInvestmentParams['roa'];
                    $returnOnInvestment[2]['amount'][$periodKey] = $returnOfInvestmentParams['roe'];
                    $returnOnInvestment[3]['amount'][$periodKey] = $returnOfInvestmentParams['rota'];
                    $returnOnInvestment[4]['amount'][$periodKey] = $returnOfInvestmentParams['roce'];
                    $returnOnInvestment[5]['amount'][$periodKey] = $returnOfInvestmentParams['roic'];
                    $returnOnInvestment[6]['amount'][$periodKey] = $returnOfInvestmentParams['rona'];
                    $returnOnInvestment[7]['amount'][$periodKey] = $returnOfInvestmentParams['rca'];
                    $returnOnInvestment[8]['amount'][$periodKey] = $returnOfInvestmentParams['rofa'];
                }

                if ($user->config->report_operational_efficiency_row_profit_and_loss) {
                    $items[] = [
                        'label' => 'Показатели ОПиУ',
                        'addCheckboxX' => true,
                        'options' => [
                            'class' => 'text-bold',
                        ],
                        'items' => $profitAndLossItems,
                    ];
                }

                if ($user->config->report_operational_efficiency_row_msfo_profit) {
                    $items[] = [
                        'label' => 'Показатели прибыли по МСФО',
                        'addCheckboxX' => true,
                        'options' => [
                            'class' => 'text-bold',
                        ],
                        'items' => $msfoProfitItems,
                    ];
                }

                if ($user->config->report_operational_efficiency_row_revenue_profitability) {
                    $items[] = [
                        'label' => 'Рентабельность выручки (продаж)',
                        'addCheckboxX' => true,
                        'options' => [
                            'class' => 'text-bold',
                        ],
                        'items' => $revenueProfitabilityItems,
                    ];
                }

                if ($user->config->report_operational_efficiency_row_return_on_investment) {
                    $items[] = [
                        'label' => 'Рентабельность инвестиций',
                        'addCheckboxX' => true,
                        'options' => [
                            'class' => 'text-bold',
                        ],
                        'items' => $returnOnInvestment,
                    ];
                }

                $this->data[$year] = $items;
            }
        }

        return $this->data[$this->year];
    }

    public function getYearFilter(): array {
        if (empty($this->_yearFilter)) {
            $range = [];
            $cashOrderMinDate = CashOrderFlows::find()
                ->byCompany($this->multiCompanyIds)
                ->min('date');
            $cashBankMinDate = CashBankFlows::find()
                ->byCompany($this->multiCompanyIds)
                ->min('date');
            $cashEmoneyMinDate = CashEmoneyFlows::find()
                ->byCompany($this->multiCompanyIds)
                ->min('date');
            $minDates = [];

            if ($cashOrderMinDate) $minDates[] = $cashOrderMinDate;
            if ($cashBankMinDate) $minDates[] = $cashBankMinDate;
            if ($cashEmoneyMinDate) $minDates[] = $cashEmoneyMinDate;

            $minCashDate = !empty($minDates) ? max(self::MIN_REPORT_DATE, min($minDates)) : date(DateHelper::FORMAT_DATE);
            $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
            $currentYear = date('Y');
            foreach (range($registrationYear, $currentYear) as $value) {
                $range[$value] = $value;
            }
            arsort($range);
            $this->_yearFilter = $range;
            $this->_minCashYearMonth = DateHelper::format($minCashDate, 'Ym', DateHelper::FORMAT_DATE);
        }

        return $this->_yearFilter;
    }

    public function getFromCurrentMonthsPeriods($left, $right, $offsetYear = 0, $offsetMonth = "0") {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month")->modify("{$offsetMonth} month");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr->format('Y-m-01'),
                'to' => $curr->format('Y-m-t'),
            ];
            $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }

    public function getValue($year, $month, $key, $subKey)
    {
        $month = str_pad($month, 2, "0", STR_PAD_LEFT);

        return $this->data[$year][$key]['items'][$subKey]['amount'][$month] ?? 0;
    }

    public function generateXls($type) {
        $data = $this->handleItems();
        $columns = [];
        $columns[] = [
            'attribute' => 'itemName',
            'header' => 'Показатель',
        ];
        foreach (AbstractFinance::$month as $monthNumber => $monthText) {
            $columns[] = [
                'attribute' => "item{$monthNumber}",
                'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                'header' => $monthText,
            ];
        }

        $columns[] = [
            'attribute' => 'dataYear',
            'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
            'header' => $this->year,
        ];
        // data
        $formattedData = [];
        foreach ($data as $row) {
            $formattedData[] = $this->_buildXlsRow($row);
            if (isset($row['items'])) {
                foreach ($row['items'] as $subItem) {
                    $formattedData[] = $this->_buildXlsRow($subItem);
                }
            }
        }

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $formattedData,
            'title' => "Эффективность за {$this->year} год",
            'rangeHeader' => range('A', 'N'),
            'columns' => $columns,
            'format' => 'Xlsx',
            'fileName' => "Эффективность за {$this->year} год",
        ]);
    }

    private function _buildXlsRow($data) {
        $row = [
            'itemName' => trim(strip_tags($data['label'] ?? '')),
            'dataYear' => round(($data['amount']['total'] ?? '0.00') / 100, 2),
        ];
        foreach (AbstractFinance::$month as $monthNumber => $monthText) {
            $row['item' . $monthNumber] = round(($data['amount'][$monthNumber] ?? '0.00') / 100, 2);
        }

        return $row;
    }

    private function getReturnOfInvestmentParams(
        \DateTime $startDate,
        \DateTime $endDate,
        string $period,
        array $profitAndLossItems,
        array $msfoProfitItems
    ): array {
        $startMonthAssets = $this->balanceSearchModel->getAssets(clone $startDate);
        $startCapitalAmount = $this->balanceSearchModel->getCapital(clone $startDate);
        $startShorttermLiabilities = $this->balanceSearchModel->getShorttermLiabilities(clone $startDate);
        $startLongtermDuties = $this->balanceSearchModel->getLongtermDuties(clone $startDate);
        $startFixedAssets = $this->balanceSearchModel->getFixedAssets(clone $startDate);
        $startCurrentAssets = $this->balanceSearchModel->getCurrentAssets(clone $startDate);
        $endMonthAssets = $this->balanceSearchModel->getAssets(clone $endDate);
        $endCapitalAmount = $this->balanceSearchModel->getAssets(clone $endDate);
        $endShorttermLiabilities = $this->balanceSearchModel->getShorttermLiabilities(clone $endDate);
        $endLongtermDuties = $this->balanceSearchModel->getLongtermDuties(clone $endDate);
        $endFixedAssets = $this->balanceSearchModel->getFixedAssets(clone $endDate);
        $endCurrentAssets = $this->balanceSearchModel->getCurrentAssets(clone $endDate);

        $averageMonthAssets = ($startMonthAssets + $endMonthAssets) / 2;
        $averageCapitalAmount = ($startCapitalAmount + $endCapitalAmount) / 2;
        $averageShorttermLiabilities = ($startShorttermLiabilities + $endShorttermLiabilities) / 2;
        $averageLongtermDuties = ($startLongtermDuties + $endLongtermDuties) / 2;
        $averageFixedAssets = ($startFixedAssets + $endFixedAssets) / 2;
        $averageCurrentAssets = ($startCurrentAssets + $endCurrentAssets) / 2;

        $averageMonthDivider = $averageMonthAssets == 0 ? 1 : $averageMonthAssets;
        $averageCapitalDivider = $averageCapitalAmount == 0 ? 1 : $averageCapitalAmount;
        $roceDivider = ($averageMonthAssets - $averageShorttermLiabilities) == 0 ? 1 : ($averageMonthAssets - $averageShorttermLiabilities);
        $roicDivider = ($averageCapitalAmount + $averageLongtermDuties) == 0 ? 1 : ($averageCapitalAmount + $averageLongtermDuties);
        $ronaDivider = $averageFixedAssets + ($averageCurrentAssets - ($averageShorttermLiabilities + $averageLongtermDuties));
        $ronaDivider = $ronaDivider == 0 ? 1 : $ronaDivider;
        $rcaDivider = $averageCurrentAssets == 0 ? 1 : $averageCurrentAssets;
        $rofaDivider = $averageFixedAssets == 0 ? 1 : $averageFixedAssets;

        return [
            'oroa' => round((($profitAndLossItems['operatingProfit']['amount'][$period] * 100) / $averageMonthDivider) * 100),
            'roa' => round((($profitAndLossItems['netIncomeLoss']['amount'][$period] * 100) / $averageMonthDivider) * 100),
            'roe' => round((($profitAndLossItems['netIncomeLoss']['amount'][$period] * 100) / $averageCapitalDivider) * 100),
            'rota' => round((($profitAndLossItems['operatingProfit']['amount'][$period] * 100) / $averageMonthDivider) * 100),
            'roce' => round((($profitAndLossItems['operatingProfit']['amount'][$period] * 100) / $roceDivider) * 100),
            'roic' => round((($profitAndLossItems['netIncomeLoss']['amount'][$period] * 100) / $roicDivider) * 100),
            'rona' => round((($profitAndLossItems['netIncomeLoss']['amount'][$period] * 100) / $ronaDivider) * 100),
            'rca' => round((($profitAndLossItems['netIncomeLoss']['amount'][$period] * 100) / $rcaDivider) * 100),
            'rofa' => round((($msfoProfitItems[1]['amount'][$period] * 100) / $rofaDivider) * 100),
        ];
    }
}