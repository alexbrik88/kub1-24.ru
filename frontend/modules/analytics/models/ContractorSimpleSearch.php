<?php

namespace frontend\modules\analytics\models;

use common\components\TextHelper;
use common\models\cash\CashFlowsBase;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\Autoinvoice;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use frontend\components\PageSize;
use frontend\modules\documents\components\InvoiceStatistic;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * ContractorSimpleSearch represents the model behind the search form about `common\models\Contractor`.
 */
class ContractorSimpleSearch extends Contractor
{
    const ACTIVITY_STATUS_ALL = 0;
    const ACTIVITY_STATUS_ACTIVE = 1;
    const ACTIVITY_STATUS_INACTIVE = 2;

    const NOT_PAID_INVOICE_COUNT = 'notPaidInvoiceCount';
    const NOT_PAID_INVOICE_SUM = 'notPaidInvoiceSum';
    const PAID_INVOICE_COUNT = 'paidInvoiceCount';
    const PAID_INVOICE_SUM = 'paidInvoiceSum';

    public $activityStatus = self::ACTIVITY_STATUS_ACTIVE;

    public $byIds = [];
    public $excludeIds = [];
    public $item_id;
    public $item_id_prev;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['activityStatus'], 'in', 'range' => [
                self::ACTIVITY_STATUS_ALL,
                self::ACTIVITY_STATUS_ACTIVE,
                self::ACTIVITY_STATUS_INACTIVE,
            ],],
            [
                [
                    'name',
                    'type',
                    'item_id',
                    'item_id_prev',
                ],
                'safe',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     *
     * @return ActiveDataProvider
     */
    public function search($params, $formName = null)
    {
        $this->load($params, $formName);

        $cTable = $this->tableName();
        /* @var $user Employee */
        $user = Yii::$app->user->identity;


        $query = self::find()
            ->addSelect([
                'contractor.*',
                'contact' => 'IF({{contractor}}.[[contact_is_director]] = true, {{contractor}}.[[director_name]], {{contractor}}.[[contact_name]])',
                'phone' => 'IF({{contractor}}.[[contact_is_director]] = true, {{contractor}}.[[director_phone]], {{contractor}}.[[contact_phone]])',
            ])
            ->joinWith('companyType t')
            ->byCompany($this->company_id)
            ->byIsDeleted(false)
            ->byStatus(self::ACTIVE)
            ->andWhere(['=', $cTable . '.type', (int)$this->type])
            ->groupBy('contractor.id');

        if ($user->currentEmployeeCompany->employee_role_id !== EmployeeRole::ROLE_CHIEF) {
            $query->andWhere(['or',
                [Contractor::tableName() . '.not_accounting' => false],
                ['and',
                    [Contractor::tableName() . '.not_accounting' => true],
                    [Contractor::tableName() . '.responsible_employee_id' => $user->id],
                ],
            ]);
        }

        if ($this->byIds) {
            $query->andFilterWhere([Contractor::tableName() . '.id' => $this->byIds]);
        }
        if ($this->excludeIds) {
            $query->andFilterWhere(['not', [Contractor::tableName() . '.id' => $this->excludeIds]]);
        }
        // two same modals - single filter not working by one id
        if ($this->item_id_prev) {
            if ($this->flowsType == CashFlowsBase::FLOW_TYPE_INCOME) {
                $query->andWhere([Contractor::tableName() . '.invoice_income_item_id' => $this->item_id_prev]);
            } else {
                $query->andWhere([Contractor::tableName() . '.invoice_expenditure_item_id' => $this->item_id_prev]);
            }
        } else if ($this->item_id) {
            if ($this->flowsType == CashFlowsBase::FLOW_TYPE_INCOME) {
                $query->andWhere([Contractor::tableName() . '.invoice_income_item_id' => $this->item_id]);
            } else {
                $query->andWhere([Contractor::tableName() . '.invoice_expenditure_item_id' => $this->item_id]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => PageSize::get()
            ]
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'name' => [
                    'asc' => [
                        self::tableName() . '.face_type' => SORT_ASC,
                        't.name_short' => SORT_ASC,
                        self::tableName() . '.name' => SORT_ASC,
                    ],
                    'desc' => [
                        self::tableName() . '.face_type' => SORT_DESC,
                        't.name_short' => SORT_DESC,
                        self::tableName() . '.name' => SORT_DESC,
                    ],
                    'default' => SORT_ASC,
                ],
                'payment_delay',
            ],
            'defaultOrder' => ['name' => SORT_ASC]
        ]);

        $query->andFilterWhere([
            $cTable . '.responsible_employee_id' => $this->responsible_employee_id,
        ]);

        $query->andFilterWhere(['or',
            ['like', $cTable . '.name', $this->name],
            ['like', $cTable . '.ITN', $this->name],
        ]);

        return $dataProvider;
    }

    /**
     * @param $type
     * @return array
     */
    public function getContactItemsByQuery(ActiveQuery $query)
    {
        $searchQuery = clone $query;
        $data = $searchQuery->select([
            'contact' => 'IF(contractor.contact_is_director = true, contractor.director_name, contractor.contact_name)',
        ])->column();

        $data = array_unique($data);
        natsort($data);

        return ['' => 'Все'] + array_combine($data, $data);
    }

    /**
     * @param $type
     * @return array
     */
    public function getResponsibleItemsByQuery(ActiveQuery $query)
    {
        $searchQuery = clone $query;
        $dataArray = $this->company
            ->getEmployeeCompanies()
            ->andWhere([
                'employee_id' => $searchQuery->select([
                    'contractor.responsible_employee_id',
                    'contact' => 'IF(contractor.contact_is_director = true, contractor.director_name, contractor.contact_name)',
                ])->distinct()->column(),
            ])
            ->orderBy(['lastname' => SORT_ASC, 'firstname_initial' => SORT_ASC, 'patronymic_initial' => SORT_ASC])
            ->all();

        return ['' => 'Все'] + ArrayHelper::map($dataArray, 'employee_id', function ($model) {
                return $model->getFio(true);
            });
    }

    public function getArticleFilterByQuery(ActiveQuery $query)
    {
        $searchQuery = clone $query;
        $resultIds = $searchQuery
            ->select(($this->type == Contractor::TYPE_CUSTOMER) ? 'invoice_income_item_id' : 'invoice_expenditure_item_id')
            ->distinct()
            ->column();

        if ($resultIds) {
            if ($this->type == Contractor::TYPE_CUSTOMER) {
                return ['' => 'Все'] + InvoiceIncomeItem::find()->where(['id' => $resultIds])->select('name')->indexBy('id')->column();
            } else {
                return ['' => 'Все'] + InvoiceExpenditureItem::find()->where(['id' => $resultIds])->select('name')->indexBy('id')->column();
            }
        }

        return ['' => 'Все'];
    }
}
