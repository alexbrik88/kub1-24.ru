<?php namespace frontend\modules\analytics\models\balanceInitial;

use Yii;
use common\models\product\Product;
use yii\base\Model;
use yii\data\SqlDataProvider;
use yii\db\ActiveQuery;

class BalanceStoreInitialSearch extends Model {

    private int $companyId;
    private int $productionType;

    /** @var ActiveQuery */
    private $query;

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->companyId = Yii::$app->user->identity->company->id;
        $this->productionType = Product::PRODUCTION_TYPE_GOODS;
    }

    public function rules()
    {
        return [];
    }

    public function search($params = [])
    {
        $companyId = (int)$this->companyId;
        $productionType = (int)$this->productionType;

        $querySql = "
            SELECT 
                p.id AS product_id,
                p.title AS product_name,
                SUM(ps.initial_quantity) AS initial_quantity,
                pib.date AS initial_date,
                pib.price AS initial_price,
                SUM(ps.initial_quantity * pib.price) AS initial_amount
            FROM product p
            LEFT JOIN product_initial_balance pib ON p.id = pib.product_id
            LEFT JOIN product_store ps ON p.id = ps.product_id  
            WHERE 
                p.company_id = {$companyId} AND 
                p.production_type = {$productionType} AND
                p.is_deleted = 0 AND
                p.status > 0
            GROUP BY p.id
            HAVING pib.price > 0
        ";

        $count = Yii::$app->db->createCommand("SELECT COUNT(*) FROM ({$querySql}) cnt")->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql' => $querySql,
            'totalCount' => $count,
            'sort' => [
                'attributes' => [
                    'product_name',
                    'initial_quantity',
                    'initial_date',
                    'initial_price',
                    'initial_amount',
                ],
            ],
        ]);

        $this->load($params);

        return $dataProvider;
    }
}