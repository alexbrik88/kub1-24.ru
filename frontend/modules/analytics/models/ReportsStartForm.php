<?php

namespace frontend\modules\analytics\models;

use common\models\Company;
use common\models\EmployeeCompany;
use common\models\company\CompanyInfoIndustry;
use common\models\company\CompanyInfoPlace;
use common\models\company\CompanyInfoTariff;
use common\models\company\CompanyTaxationType;
use common\models\company\RegistrationPageType;
use common\models\employee\Employee;
use common\models\employee\EmployeeInfoRole;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use console\components\sender\Findirector;
use frontend\modules\crm\models\Task;
use frontend\modules\crm\models\TaskEventList;
use frontend\modules\crm\services\Client\KubClientFromCompanyService;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class ReportsStartForm
 * @package frontend\modules\subscribe\forms
 */
class ReportsStartForm extends Model
{
    const SCENARIO_CODE = 'code';
    const SCENARIO_ACCOUNT = 'account';
    const SCENARIO_TAXATION = 'taxation';
    const SCENARIO_MODULES = 'modules';

    protected $_code;
    public $info_place_id;
    public $info_industry_id;
    public $info_role_id;
    public $info_sites_count;
    public $info_shops_count;
    public $info_storage_count;
    public $tax_osno;
    public $tax_usn;
    public $tax_envd;
    public $tax_psn;
    public $usn_type;
    public $usn_percent;
    public $modules;

    public static $modulesItems = [
        SubscribeTariffGroup::STANDART => [
            'name' => 'КУБ.Выставление счетов',
            'description' => 'Выставление счетов, Актов, ТН, УПД, Счетов-фактур. Конструктор Договоров. Удобно при наличие продавцов.',
        ],
        SubscribeTariffGroup::PRICE_LIST => [
            'name' => 'КУБ.Онлайн Прайс-листы',
            'description' => 'Позволяет менять цену, кол-во, описание товаров, добавлять и удалять товары в уже отправленном прайс-листе. Получение заказов из прайс-листа. Выставление счетов на оплату из прайс-листа.',
        ],
        /*SubscribeTariffGroup::CHECK_CONTRACTOR => [
            'name' => 'КУБ.Проверка контрагентов',
            'description' => 'Данные по вашим покупателям и поставщикам. Проверка на наличие долгов, судебных исков, уплаченных налогов.',
        ],*/
        SubscribeTariffGroup::B2B_PAYMENT => [
            'name' => 'КУБ.В2В оплаты',
            'description' => 'Выставление счетов и прием оплаты на сайте от ООО и ИП. Эквайринг для юр. лиц.',
        ],
    ];

    protected $_employee;
    protected $_afterSaveRedirectUrl;

    public function __construct(Employee $employee, $config = [])
    {
        $company = $employee->company;
        $taxation = $company->companyTaxationType ?? new CompanyTaxationType([
            'company_id' => $company->id,
        ]);
        $this->_employee = $employee;
        $this->info_role_id = $employee->currentEmployeeCompany->info_role_id;
        $this->info_place_id = $company->info_place_id;
        $this->info_industry_id = $company->info_industry_id;
        $this->info_sites_count = $company->info_sites_count;
        $this->info_shops_count = $company->info_shops_count;
        $this->info_storage_count = $company->info_storage_count;
        $this->tax_osno = $taxation->osno;
        $this->tax_usn = $taxation->usn;
        $this->tax_envd = 0; // $taxation->envd;
        $this->tax_psn = $taxation->psn;
        $this->usn_type = $taxation->usn_type;
        $this->usn_percent = $taxation->usn_percent;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_CODE => [
                'code',
            ],
            self::SCENARIO_ACCOUNT => [
                'info_place_id',
                'info_role_id',
                'info_industry_id',
                'info_sites_count',
                'info_shops_count',
                'info_storage_count',
            ],
            self::SCENARIO_TAXATION => [
                'tax_osno',
                'tax_usn',
                // 'tax_envd',
                'tax_psn',
                'usn_type',
                'usn_percent',
            ],
            self::SCENARIO_MODULES => [
                'modules',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $modules = array_keys(self::$modulesItems);

        return [
            [
                [
                    'info_place_id',
                    'info_role_id',
                    'info_industry_id',
                ], 'required',
            ],
            [
                [
                    'info_sites_count',
                    'info_shops_count',
                    'info_storage_count',
                ], 'integer',
                'min' => 0,
            ],
            ['code', 'validateCode'],
            ['info_place_id', 'exist', 'targetClass' => CompanyInfoPlace::class, 'targetAttribute' => ['info_place_id' => 'id']],
            ['info_industry_id', 'exist', 'targetClass' => CompanyInfoIndustry::class, 'targetAttribute' => ['info_industry_id' => 'id']],
            ['info_role_id', 'exist', 'targetClass' => EmployeeInfoRole::class, 'targetAttribute' => ['info_role_id' => 'id']],
            [['usn_percent'], 'integer'],
            [
                [
                    'tax_osno',
                    'tax_usn',
                    // 'tax_envd',
                    'tax_psn',
                    'usn_type',
                ], 'boolean'],
            [
                'tax_osno',
                function ($attribute, $options) {
                    if (!$this->tax_osno &&
                        !$this->tax_usn &&
                        // !$this->tax_envd &&
                        !$this->tax_psn
                    ) {
                        $this->addError($attribute, 'Необходимо выбрать систему налогообложения');
                    }
                },
                'skipOnEmpty' => false,
            ],
            ['modules', 'each', 'rule' => ['boolean']],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateCode($attribute, $params)
    {
        if (empty(trim($this->_code))) {
            $this->addError($attribute, 'Введите код подтверждения');
        } elseif (!$this->_employee->verifyEmailConfirmCode($this->_code)) {
            $this->addError($attribute, 'Не верный код подтверждения');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => 'Код подтверждения',
            'info_place_id' => 'Страна местонахождения бизнеса',
            'info_role_id' => 'Ваша роль в компании',
            'info_industry_id' => 'Тип бизнеса',
            'info_sites_count' => 'Кол-во сайтов',
            'info_shops_count' => 'Кол-во магазинов',
            'info_storage_count' => 'Кол-во складов',
            'tax_osno' => 'ОСНО',
            'tax_usn' => 'УСН',
            'tax_envd' => 'ЕНВД',
            'tax_psn' => 'Патент',
            'usn_type' => '',
            'usn_percent' => '',
        ];
    }

    /**
     * @param $value
     */
    public function setCode($value)
    {
        $this->_code = implode('', (array) $value);
    }

    /**
     * @return array
     */
    public function getCode()
    {
        $str = strval($this->_code);

        return [
            mb_substr($str, 0, 1),
            mb_substr($str, 1, 1),
            mb_substr($str, 2, 1),
            mb_substr($str, 3, 1),
        ];
    }

    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->_employee;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_employee->company;
    }

    /**
     * @return array|string
     */
    public function getRedirectUrl()
    {
        return $this->_afterSaveRedirectUrl;
    }

    /**
     * @return array|string
     */
    public function setRedirectUrl($value)
    {
        $this->_afterSaveRedirectUrl = $value;
    }

    /**
     * @return string
     */
    public function getTaxName()
    {
        return $this->tax_system ?
            ArrayHelper::getValue(self::$taxSystemItems, $this->tax_system) :
            ArrayHelper::getValue($this->company, ['companyTaxationType', 'name']);
    }

    /**
     * @return string
     */
    public function getStaffName()
    {
        return ArrayHelper::getValue($this->company, ['employeeCount', 'name']);
    }

    /**
     * @return Payment
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        switch ($this->getScenario()) {
            case self::SCENARIO_CODE:
                $this->_employee->confirmEmail();
                return true;
                break;

            case self::SCENARIO_ACCOUNT:
                return $this->saveAccount();
                break;

            case self::SCENARIO_TAXATION:
                return $this->saveTaxation();
                break;

            case self::SCENARIO_MODULES:
                return $this->saveModules();
                break;

            default:
                return false;
                break;
        }
    }

    /**
     * @return boolean
     */
    private function saveAccount()
    {
        try {
            $this->_employee->company->updateAttributes([
                'info_industry_id' => $this->info_industry_id,
                'info_place_id' => $this->info_place_id,
                'info_shops_count' => $this->info_shops_count,
                'info_storage_count' => $this->info_storage_count,
                'info_sites_count' => $this->info_sites_count,
            ]);
            $this->_employee->currentEmployeeCompany->updateAttributes([
                'info_role_id' => $this->info_role_id,
            ]);
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @return boolean
     */
    private function saveTaxation()
    {
        $company = $this->_employee->company;
        $taxation = $company->companyTaxationType ?? new CompanyTaxationType([
            'company_id' => $company->id,
        ]);

        $taxation->osno = $this->tax_osno;
        $taxation->usn = $this->tax_usn;
        $taxation->envd = false; //$this->tax_envd;
        $taxation->psn = $this->tax_psn;
        $taxation->usn_type = $this->usn_type;
        $taxation->usn_percent = $this->usn_percent;

        if ($taxation->save()) {
            $this->completedForm();

            return true;
        } else {
            \common\components\helpers\ModelHelper::logErrors($taxation, __METHOD__);

            return false;
        }
    }

    /**
     * @return boolean
     */
    private function saveModules()
    {
        $company = $this->_employee->company;
        $employeeCompany = $this->_employee->currentEmployeeCompany;

        CompanyInfoTariff::deleteAll([
            'company_id' => $company->id,
        ]);

        foreach ($this->modules as $tariffGroupId => $value) {
            if (!empty($value)) {
                $model = new CompanyInfoTariff([
                    'company_id' => $company->id,
                    'tariff_group_id' => $tariffGroupId,
                ]);
                $model->save();
            }
        }

        $this->completedForm();

        return true;
    }

    private function createKubCrmClient(Company $company, EmployeeCompany $employeeCompany)
    {
        $taskDescription = 'Позвонить, узнать сферу деятельности и закрыть на демонстрацию';
        $clientService = new KubClientFromCompanyService($company);

        if (($contractor = $clientService->create()) !== null) {
            $comment = implode("\n", [
                sprintf('Страна местонахождения бизнеса: %s', $company->infoPlace ? $company->infoPlace->name : '---'),
                sprintf('Роль в компании: %s', $employeeCompany->infoRole ? $employeeCompany->infoRole->name : '---'),
                sprintf('Тип бизнеса: %s', $company->infoIndustry ? $company->infoIndustry->name : '---'),
            ]);
            $contractor->updateAttributes([
                'comment' => implode("\n", array_filter([$contractor->comment, $comment])),
            ]);

            if ($contractor->responsible_employee_id &&
                !$contractor->getTasks()->andWhere([
                    'description' => $taskDescription,
                ])->exists()
            ) {
                $dateTime = date_create(date('Y-m-d H:i:00'));
                $min = intval($dateTime->format('i'));
                $modifyMin = ceil($min / 15) * 15 + 15 - $min;
                $dateTime->modify(sprintf('%+d minutes', $modifyMin));
                $task = new Task();
                $task->task_number = Task::getNextNumber(Yii::$app->kubCompany);
                $task->company_id = Yii::$app->kubCompany->id;
                $task->contractor_id = $contractor->id;
                $task->employee_id = $contractor->responsible_employee_id;
                $task->contact_id = $contractor->defaultContact ? $contractor->defaultContact->contact_id : null;
                $task->description = $taskDescription;
                $task->event_type = TaskEventList::EVENT_TYPE_CALL;
                $task->status = Task::STATUS_IN_PROGRESS;
                $task->datetime = $dateTime->format('Y-m-d H:i:00');
                $task->save();
            }
        }

        return $contractor;
    }

    /**
     * Activating the analytics module and creating a trial subscribe for the Company
     */
    private function completedForm() : void
    {
        $company = $this->_employee->company;
        $employeeCompany = $this->_employee->currentEmployeeCompany;

        $employeeCompany->updateAttributes([
            'can_ba_all' => true,
            'at_business_analytics_now' => true,
            'can_business_analytics' => true,
            'can_business_analytics_time' => time(),
        ]);

        if (!$company->analytics_module_activated) {
            $company->updateAttributes([
                'analytics_module_activated' => true,
            ]);
            $contractor = $this->createKubCrmClient($company, $employeeCompany);
            Findirector::sendMail2($this->_employee->email);
            if ($contractor && $contractor->responsible_employee_id) {
                $responsible = EmployeeCompany::findOne([
                    'employee_id' => $contractor->responsible_employee_id,
                    'company_id' => Yii::$app->kubCompany->id,
                ]);
            } else {
                $responsible = null;
            }

            if (true /*!in_array($company->registration_page_type_id, RegistrationPageType::$analyticsPages)*/) {
                $subject = 'Регистрация в КУБ24.ФинДиректор';
                $message = \Yii::$app->mailer->compose([
                    'html' => 'system/new-analytics-registration/html',
                    'text' => 'system/new-analytics-registration/text',
                ], [
                    'name' => $this->_employee->firstname,
                    'email' => $this->_employee->email,
                    'phone' => $this->_employee->phone,
                    'regPage' => ArrayHelper::getValue($company, ['registrationPageType', 'name']),
                    'time' => time(),
                    'source' => in_array($company->registration_page_type_id, RegistrationPageType::$analyticsPages) ? 'Лендинг' : 'КУБ',
                    'subject' => $subject,
                    'place' => $company->infoPlace ? $company->infoPlace->name : '---',
                    'role' => $employeeCompany->infoRole ? $employeeCompany->infoRole->name : '---',
                    'business' => $company->infoIndustry ? $company->infoIndustry->name : '---',
                    'responsible' => $responsible ? $responsible->fio : '---',
                ])
                ->setFrom([Yii::$app->params['emailList']['info'] => Findirector::$from])
                ->setTo(Yii::$app->params['emailList']['support'])
                ->setSubject($subject);

                if ($responsible) {
                    $message->setCc([$responsible->employee->email => $responsible->getShortFio()]);
                }

                $message->send();
            }

            if ($tariff = SubscribeTariff::findOne(SubscribeTariff::TARIFF_ANALYTICS_TRIAL)) {
                $existsQuery = Subscribe::find()->andWhere([
                    'company_id' => $company->id,
                    'tariff_group_id' => $tariff->tariff_group_id,
                ]);

                if (!$existsQuery->exists()) {
                    $subscribe = new Subscribe([
                        'company_id' => $company->id,
                        'tariff_group_id' => $tariff->tariff_group_id,
                        'tariff_limit' => $tariff->tariff_limit,
                        'tariff_id' => $tariff->id,
                        'duration_month' => $tariff->duration_month,
                        'duration_day' => $tariff->duration_day,
                        'status_id' => SubscribeStatus::STATUS_PAYED,
                    ]);

                    if ($subscribe->save()) {
                        $subscribe->activate(time());
                        $company->setActiveSubscribe($subscribe);
                    } else {
                        \common\components\helpers\ModelHelper::logErrors($subscribe, __METHOD__);
                    }
                }
            }
        }

        $this->checkRedirectUrl();
    }

    /**
     * @return string
     */
    private function checkRedirectUrl()
    {
        if (empty($this->_afterSaveRedirectUrl)) {
            switch ($this->_employee->company->registration_page_type_id) {
                case RegistrationPageType::PAGE_TYPE_LANDING_PRODUCTS:
                    $this->_afterSaveRedirectUrl = ['/analytics/product-analysis/dashboard'];
                    break;
                case RegistrationPageType::PAGE_TYPE_ANALYTICS_BREAKEVEN:
                    $this->_afterSaveRedirectUrl = ['/analytics/finance/break-even-point'];
                    break;

                default:
                    if (!trim($this->_employee->company->name_short) || !trim($this->_employee->company->inn)) {
                        $this->_employee->company->updateAttributes(['show_cash_operations_onboarding' => 1]);
                    }
                    $this->_afterSaveRedirectUrl = ['/cash/default/operations'];
                    break;
            }
        }
    }
}
