<?php

namespace frontend\modules\analytics\models;

use Yii;

/**
 * This is the model class for table "balance_item".
 *
 * @property integer $id
 * @property string $name
 * @property string $subname
 * @property integer $sort
 */
class BalanceItem extends \yii\db\ActiveRecord
{
    const ITEM_REAL_ESTATE_OBJECT = 1;
    const ITEM_EQUIPMENT_ASSETS = 2;
    const ITEM_TRANSPORT_ASSETS = 3;
    const ITEM_INTANGIBLE_ASSETS = 4;
    const ITEM_OTHER_ASSETS = 5;
    const ITEM_STOCKS = 6;
    const ITEM_TRADE_RECEIVABLES = 7;
    const ITEM_PREPAYMENTS_TO_SUPPLIERS = 8;
    const ITEM_BUDGET_OVERPAYMENT = 9;
    const ITEM_LOANS_ISSUED = 10;
    const ITEM_OTHER_INVESTMENTS = 11;
    const ITEM_BANK_ACCOUNTS = 12;
    const ITEM_CASHBOX = 13;
    const ITEM_EMONEY = 14;
    const ITEM_AUTHORIZED_CAPITAL = 15;
    const ITEM_UNDESTRIBUTED_PROFITS = 16;
    const ITEM_LONG_TERM_LOANS = 17;
    const ITEM_SHORT_TERM_BORROWINGS = 18;
    const ITEM_VENDOR_INVOICES_PAYABLE = 19;
    const ITEM_PREPAYMENT_OF_CUSTOMERS = 20;
    const ITEM_SALARY_ARREARS = 21;
    const ITEM_BUDGET_ARREARS = 22;
    const ITEM_RENTAL_DEPOSIT = 23;
    const ITEM_INCOMPLETE_PROJECTS = 24;
    const ITEM_UNDESTRIBUTED_PROFITS_PREV = 25;

    public static $itemGetter = [
        self::ITEM_REAL_ESTATE_OBJECT => 'getRealEstateObjects',
        self::ITEM_EQUIPMENT_ASSETS => 'getEquipmentAssets',
        self::ITEM_TRANSPORT_ASSETS => 'getTransportAssets',
        self::ITEM_INTANGIBLE_ASSETS => 'getIntangibleAssets',
        self::ITEM_OTHER_ASSETS => 'getOtherAssets',
        self::ITEM_STOCKS => 'getStocks',
        self::ITEM_TRADE_RECEIVABLES => 'getTradeReceivables',
        self::ITEM_PREPAYMENTS_TO_SUPPLIERS => 'getPrepaymentsToSuppliers',
        self::ITEM_BUDGET_OVERPAYMENT => 'getBudgetOverpayment',
        self::ITEM_LOANS_ISSUED => 'getLoansIssued',
        self::ITEM_OTHER_INVESTMENTS => 'getOtherInvestments',
        self::ITEM_BANK_ACCOUNTS => 'getBankAccounts',
        self::ITEM_CASHBOX => 'getCashbox',
        self::ITEM_EMONEY => 'getEmoney',
        self::ITEM_AUTHORIZED_CAPITAL => 'getAuthorizedCapital',
        self::ITEM_UNDESTRIBUTED_PROFITS => 'getUndestributedProfits',
        self::ITEM_UNDESTRIBUTED_PROFITS_PREV => 'getUndestributedProfitsPrev',
        self::ITEM_LONG_TERM_LOANS => 'getLongtermLoans',
        self::ITEM_SHORT_TERM_BORROWINGS => 'getShorttermBorrowings',
        self::ITEM_VENDOR_INVOICES_PAYABLE => 'getVendorInvoicesPayable',
        self::ITEM_PREPAYMENT_OF_CUSTOMERS => 'getPrepaymentOfCustomers',
        self::ITEM_SALARY_ARREARS => 'getSalaryArrears',
        self::ITEM_BUDGET_ARREARS => 'getBudgetArrears',
        self::ITEM_RENTAL_DEPOSIT => 'getRentalDeposit',
        self::ITEM_INCOMPLETE_PROJECTS => 'getIncompleteProjects'
    ];

    public static $flowNameByItem = [
        self::ITEM_BANK_ACCOUNTS => 'Банк',
        self::ITEM_CASHBOX => 'Касса',
        self::ITEM_EMONEY => 'E-money',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'balance_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'sort'], 'required'],
            [['sort'], 'integer'],
            [['name', 'subname'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'sort' => 'Sort',
        ];
    }
}
