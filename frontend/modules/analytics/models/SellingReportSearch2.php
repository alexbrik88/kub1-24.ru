<?php

namespace frontend\modules\analytics\models;

use common\components\excel\Excel;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankFlowToInvoice;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyFlowToInvoice;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderFlowToInvoice;
use common\models\Contractor;
use common\models\document\status\InvoiceStatus;
use Yii;
use common\components\date\DateHelper;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\product\Product;
use frontend\components\PageSize;
use frontend\models\Documents;
use yii\base\Exception;
use yii\data\SqlDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

/**
 * Class SellingReportSearch
 * @package frontend\modules\analytics\models
 *
 * @property array $periodParams
 * @property string $periodName
 *
 * @property bool $showPercent
 * @property bool $showPercentTotal
 */
class SellingReportSearch2 extends AbstractFinance
{
    /**
     *
     */
    const MIN_REPORT_DATE = '2010';

    /**
     *
     */
    const BY_INVOICES = 0;
    const BY_FLOWS = 1;

    /**
     *
     */
    const TYPE_ALL = 0;
    const TYPE_PAID = 1;
    const TYPE_NOT_PAID = 2;

    /**
     *
     */
    const PREFIX_AMOUNT = 'amount_';
    const PREFIX_QUANTITY = 'quantity_';
    const PREFIX_PERCENT = 'percent_';
    const PREFIX_PERCENT_TOTAL = 'percent_total_';

    /**
     *
     */
    public $searchBy = self::BY_INVOICES;
    public $type = self::TYPE_ALL;

    /**
     * @var int
     */
    public $project_id;
    public $contractor_id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var boolean
     */
    public $groupByContractor;

    /**
     * @var
     */
    protected $_yearFilter;

    /**
     * @var
     */
    private $_filterProductsIds;
    private $_filterContractorIds;

    /**
     * @var
     */
    private $_totals;

    /**
     * @var
     */
    private $_undistributedFlows;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'name', 'project_id', 'contractor_id'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return SqlDataProvider
     * @throws Exception
     * @throws \yii\db\Exception
     */
    public function search($params = [])
    {
        $this->load($params);

        $totalsQuery = $this->getQuery(true);
        $this->_totals = Yii::$app->db2->createCommand($totalsQuery)->queryOne();

        $query = $this->getQuery();

        $dataProvider = new SqlDataProvider([
            'db' => \Yii::$app->db2,
            'sql' => $query,
            'totalCount' => $this->getTotalCount(), // todo
            'pagination' => [
                'pageSize' => PageSize::get(),
            ],
            'sort' => [
                'attributes' => ['item_name'] + $this->getSortColumns(),
                'defaultOrder' => $this->getSortDefault(),
            ],
        ]);

        if ($this->groupByContractor) {
            Yii::$app->session->set('modules.reports.selling_report.sort', $dataProvider->sort->attributeOrders);
        }

        if ($this->searchBy == self::BY_FLOWS) {
            $undistributedQuery = $this->_getUndistributedFlowsQuery();
            $this->_undistributedFlows = Yii::$app->db2->createCommand($undistributedQuery)->queryOne();
        }

        return $dataProvider;
    }

    /**
     * @return mixed
     */
    public function getTotals()
    {
        return $this->_totals;
    }

    /**
     * @return false|string|null
     * @throws \yii\db\Exception
     */
    public function getTotalCount()
    {
        $orderTable = Order::tableName();
        $invoiceTable = Invoice::tableName();
        $year = $this->year;
        $companyId = $this->_company->id;
        $documentType = Documents::IO_TYPE_OUT;

        $SQL_PAGE_FILTER = $this->getSqlPageFilter();
        $SQL_FILTER_BY_TYPE = $this->getSqlFilterByType();
        $SQL_FILTER_BY_NAME = $this->getSqlFilterByName();

        return Yii::$app->db->createCommand("
            SELECT 
              COUNT(DISTINCT ".($this->groupByContractor ? 'i.contractor_id' : 'o.product_id').")
            FROM `{$orderTable}` o
            LEFT JOIN `{$invoiceTable}` i ON i.id = o.invoice_id
            WHERE i.company_id = {$companyId}
              AND i.type = {$documentType}
              AND i.is_deleted = 0
              AND i.document_date BETWEEN '{$year}-01-01' AND '{$year}-12-31'
              AND {$SQL_PAGE_FILTER}
              AND {$SQL_FILTER_BY_TYPE}
              AND {$SQL_FILTER_BY_NAME}
        ")->queryScalar();
    }

    public function getUndistributedFlows()
    {
        return $this->_undistributedFlows;
    }

    private function _getUndistributedFlowsQuery()
    {
        $olapTable = 'olap_flows';
        $companyId = $this->_company->id;
        $invoiceTable = Invoice::tableName();
        $year = $this->year;
        $prevYear = $year - 1;
        $documentType = Documents::IO_TYPE_OUT;
        $flowType = CashFlowsBase::FLOW_TYPE_INCOME;

        $amount = trim(self::PREFIX_AMOUNT, '_');
        $quantity = trim(self::PREFIX_QUANTITY, '_');
        $percent = trim(self::PREFIX_PERCENT, '_');
        $percentTotals = trim(self::PREFIX_PERCENT_TOTAL, '_');

        $SQL_PAGE_FILTER = $this->getSqlPageFilter();
        $SQL_FILTER_BY_TYPE = $this->getSqlFilterByType();
        $SQL_FILTER_BY_NAME = $this->getSqlFilterByName();

        if ($this->contractor_id) {
            $_contractorIds = (array)$this->contractor_id;
        } else {
            $_contractorIds = Yii::$app->db->createCommand("
                SELECT 
                  DISTINCT i.contractor_id
                FROM `{$invoiceTable}` i
                WHERE i.company_id = {$companyId}
                  AND i.type = {$documentType}
                  AND i.is_deleted = 0
                  AND i.document_date BETWEEN '{$prevYear}-01-01' AND '{$year}-12-31'
                  AND {$SQL_PAGE_FILTER}
                  AND {$SQL_FILTER_BY_TYPE}
                  AND {$SQL_FILTER_BY_NAME}
                ")->queryColumn();
        }

        $contractorIds = implode(',', $_contractorIds) ?: '-1';

        $query = "
            SELECT
              t.contractor_id,                   
              SUM(IF(YEAR(t.recognition_date) = {$year} AND MONTH(t.recognition_date) =   1, t.amount - t.invoice_amount, 0)) AS {$amount}_m01,
              SUM(IF(YEAR(t.recognition_date) = {$year} AND MONTH(t.recognition_date) =   2, t.amount - t.invoice_amount, 0)) AS {$amount}_m02,
              SUM(IF(YEAR(t.recognition_date) = {$year} AND MONTH(t.recognition_date) =   3, t.amount - t.invoice_amount, 0)) AS {$amount}_m03,
              SUM(IF(YEAR(t.recognition_date) = {$year} AND MONTH(t.recognition_date) =   4, t.amount - t.invoice_amount, 0)) AS {$amount}_m04,
              SUM(IF(YEAR(t.recognition_date) = {$year} AND MONTH(t.recognition_date) =   5, t.amount - t.invoice_amount, 0)) AS {$amount}_m05,
              SUM(IF(YEAR(t.recognition_date) = {$year} AND MONTH(t.recognition_date) =   6, t.amount - t.invoice_amount, 0)) AS {$amount}_m06,
              SUM(IF(YEAR(t.recognition_date) = {$year} AND MONTH(t.recognition_date) =   7, t.amount - t.invoice_amount, 0)) AS {$amount}_m07,
              SUM(IF(YEAR(t.recognition_date) = {$year} AND MONTH(t.recognition_date) =   8, t.amount - t.invoice_amount, 0)) AS {$amount}_m08,
              SUM(IF(YEAR(t.recognition_date) = {$year} AND MONTH(t.recognition_date) =   9, t.amount - t.invoice_amount, 0)) AS {$amount}_m09,
              SUM(IF(YEAR(t.recognition_date) = {$year} AND MONTH(t.recognition_date) =  10, t.amount - t.invoice_amount, 0)) AS {$amount}_m10,
              SUM(IF(YEAR(t.recognition_date) = {$year} AND MONTH(t.recognition_date) =  11, t.amount - t.invoice_amount, 0)) AS {$amount}_m11,
              SUM(IF(YEAR(t.recognition_date) = {$year} AND MONTH(t.recognition_date) =  12, t.amount - t.invoice_amount, 0)) AS {$amount}_m12,
              SUM(IF(YEAR(t.recognition_date) = {$year} AND QUARTER(t.recognition_date) = 1, t.amount - t.invoice_amount, 0)) AS {$amount}_q1,
              SUM(IF(YEAR(t.recognition_date) = {$year} AND QUARTER(t.recognition_date) = 2, t.amount - t.invoice_amount, 0)) AS {$amount}_q2,
              SUM(IF(YEAR(t.recognition_date) = {$year} AND QUARTER(t.recognition_date) = 3, t.amount - t.invoice_amount, 0)) AS {$amount}_q3,
              SUM(IF(YEAR(t.recognition_date) = {$year} AND QUARTER(t.recognition_date) = 4, t.amount - t.invoice_amount, 0)) AS {$amount}_q4,                   
              SUM(IF(YEAR(t.recognition_date) = {$year}, t.amount - t.invoice_amount, 0)) AS {$amount}_y
            FROM {$olapTable} t
            WHERE  t.company_id = {$companyId}
              AND (t.type = {$flowType})
              AND (t.recognition_date BETWEEN '{$year}-01-01' AND '{$year}-12-31')
              AND (t.contractor_id IN ({$contractorIds}))
            GROUP BY NULL;
        ";

        return $query;
    }

    /**
     * @param bool $calcTotals
     * @return string
     * @throws Exception
     */
    public function getQuery($calcTotals = false)
    {
        $productTable = Product::tableName();
        $contractorTable = Contractor::tableName();
        $orderTable = Order::tableName();
        $invoiceTable = Invoice::tableName();
        $companyId = $this->_company->id;
        $documentType = Documents::IO_TYPE_OUT;
        $year = $this->year;
        $prevYear = $year - 1;

        $amount = trim(self::PREFIX_AMOUNT, '_');
        $quantity = trim(self::PREFIX_QUANTITY, '_');
        $percent = trim(self::PREFIX_PERCENT, '_');
        $percentTotals = trim(self::PREFIX_PERCENT_TOTAL, '_');

        $totals = $this->_totals ?: [
            'amount_m01' => NULL,
            'amount_m02' => NULL,
            'amount_m03' => NULL,
            'amount_m04' => NULL,
            'amount_m05' => NULL,
            'amount_m06' => NULL,
            'amount_m07' => NULL,
            'amount_m08' => NULL,
            'amount_m09' => NULL,
            'amount_m10' => NULL,
            'amount_m11' => NULL,
            'amount_m12' => NULL,
            'amount_q1' => NULL,
            'amount_q2' => NULL,
            'amount_q3' => NULL,
            'amount_q4' => NULL,
            'amount_y' => NULL,
        ];

        if ($this->searchBy === self::BY_FLOWS) {
            $subQuery = $this->_getSubQueryByFlows();
        } else {
            $subQuery = $this->_getSubQueryByInvoices();
        }

        if (!$calcTotals) {
            $subQuery .= ($this->groupByContractor)
                ? " GROUP BY i.contractor_id"
                : " GROUP BY o.product_id";
            $subQuery .= ' HAVING y > 0';
        }

        $query = "
            SELECT 
              t.product_id AS product_id,
              t.contractor_id AS contractor_id,
              ".(!$calcTotals ? ($this->groupByContractor ? 'c.name AS item_name,' : 'p.title AS item_name,') : '')."
              qq_m01 AS {$quantity}_m01, 
              qq_m02 AS {$quantity}_m02,
              qq_m03 AS {$quantity}_m03, 
              qq_m04 AS {$quantity}_m04, 
              qq_m05 AS {$quantity}_m05, 
              qq_m06 AS {$quantity}_m06, 
              qq_m07 AS {$quantity}_m07, 
              qq_m08 AS {$quantity}_m08, 
              qq_m09 AS {$quantity}_m09, 
              qq_m10 AS {$quantity}_m10, 
              qq_m11 AS {$quantity}_m11, 
              qq_m12 AS {$quantity}_m12,
              qq_q1  AS {$quantity}_q1, 
              qq_q2  AS {$quantity}_q2, 
              qq_q3  AS {$quantity}_q3,
              qq_q4  AS {$quantity}_q4,
              qq_y   AS {$quantity}_y,                   
                   
              IFNULL(m01, 0) AS {$amount}_m01, 
              IFNULL(m02, 0) AS {$amount}_m02,
              IFNULL(m03, 0) AS {$amount}_m03, 
              IFNULL(m04, 0) AS {$amount}_m04, 
              IFNULL(m05, 0) AS {$amount}_m05, 
              IFNULL(m06, 0) AS {$amount}_m06, 
              IFNULL(m07, 0) AS {$amount}_m07, 
              IFNULL(m08, 0) AS {$amount}_m08, 
              IFNULL(m09, 0) AS {$amount}_m09, 
              IFNULL(m10, 0) AS {$amount}_m10, 
              IFNULL(m11, 0) AS {$amount}_m11, 
              IFNULL(m12, 0) AS {$amount}_m12,
              IFNULL(q1 , 0) AS {$amount}_q1, 
              IFNULL(q2 , 0) AS {$amount}_q2, 
              IFNULL(q3 , 0) AS {$amount}_q3,
              IFNULL(q4 , 0) AS {$amount}_q4,
              IFNULL(y  , 0) AS {$amount}_y,
              
              " . ($calcTotals ? "" : "              
              (m01 *100 / {$totals['amount_m01']}) AS {$percentTotals}_m01, 
              (m02 *100 / {$totals['amount_m02']}) AS {$percentTotals}_m02,
              (m03 *100 / {$totals['amount_m03']}) AS {$percentTotals}_m03, 
              (m04 *100 / {$totals['amount_m04']}) AS {$percentTotals}_m04, 
              (m05 *100 / {$totals['amount_m05']}) AS {$percentTotals}_m05, 
              (m06 *100 / {$totals['amount_m06']}) AS {$percentTotals}_m06, 
              (m07 *100 / {$totals['amount_m07']}) AS {$percentTotals}_m07, 
              (m08 *100 / {$totals['amount_m08']}) AS {$percentTotals}_m08, 
              (m09 *100 / {$totals['amount_m09']}) AS {$percentTotals}_m09, 
              (m10 *100 / {$totals['amount_m10']}) AS {$percentTotals}_m10, 
              (m11 *100 / {$totals['amount_m11']}) AS {$percentTotals}_m11, 
              (m12 *100 / {$totals['amount_m12']}) AS {$percentTotals}_m12,
              (q1 *100 / {$totals['amount_q1']}) AS {$percentTotals}_q1, 
              (q2 *100 / {$totals['amount_q2']}) AS {$percentTotals}_q2, 
              (q3 *100 / {$totals['amount_q3']}) AS {$percentTotals}_q3,
              (q4 *100 / {$totals['amount_q4']}) AS {$percentTotals}_q4,
              (y *100 / {$totals['amount_y']}) AS {$percentTotals}_y,              
              ") . "
                
              ((m01 - prev_m12) / prev_m12) * 100 AS {$percent}_m01, 
              ((m02 - m01) / m01) * 100 AS {$percent}_m02,
              ((m03 - m02) / m02) * 100 AS {$percent}_m03,
              ((m04 - m03) / m03) * 100 AS {$percent}_m04,
              ((m05 - m04) / m04) * 100 AS {$percent}_m05,
              ((m06 - m05) / m05) * 100 AS {$percent}_m06,
              ((m07 - m06) / m06) * 100 AS {$percent}_m07,
              ((m08 - m07) / m07) * 100 AS {$percent}_m08,
              ((m09 - m08) / m08) * 100 AS {$percent}_m09,
              ((m10 - m09) / m09) * 100 AS {$percent}_m10,
              ((m11 - m10) / m10) * 100 AS {$percent}_m11,
              ((m12 - m11) / m11) * 100 AS {$percent}_m12,
              ((q1 - prev_q4) / prev_q4) * 100 AS {$percent}_q1, 
              ((q2 - q1) / q1) * 100 AS {$percent}_q2,
              ((q3 - q2) / q2) * 100 AS {$percent}_q3,
              ((q4 - q3) / q3) * 100 AS {$percent}_q4,
              ((y - prev_y) / prev_y) * 100 AS {$percent}_y
                   
            FROM ({$subQuery}) t
        ";

        if (!$calcTotals) {

            $query .= ($this->groupByContractor)
                ? " LEFT JOIN `{$contractorTable}` c ON c.id = t.contractor_id"
                : " LEFT JOIN `{$productTable}` p ON p.id = t.product_id";

            $query .= ($this->groupByContractor)
                ? " GROUP BY t.contractor_id"
                : " GROUP BY t.product_id";
        }

        return $query;
    }

    private function _getSubQueryByInvoices()
    {
        $orderTable = Order::tableName();
        $invoiceTable = Invoice::tableName();
        $companyId = $this->_company->id;
        $documentType = Documents::IO_TYPE_OUT;
        $year = $this->year;
        $prevYear = $year - 1;

        $SQL_PAGE_FILTER = $this->getSqlPageFilter();
        $SQL_PRODUCTS_BY_CONTRACTOR = $this->getSqlProductsByContractor();
        $SQL_FILTER_BY_TYPE = $this->getSqlFilterByType();
        $SQL_FILTER_BY_NAME = $this->getSqlFilterByName();

        return "
            SELECT 
              o.product_id,  
              i.contractor_id,     

              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =   1, o.amount_sales_with_vat, 0)) AS m01,
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =   2, o.amount_sales_with_vat, 0)) AS m02,
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =   3, o.amount_sales_with_vat, 0)) AS m03,
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =   4, o.amount_sales_with_vat, 0)) AS m04,
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =   5, o.amount_sales_with_vat, 0)) AS m05,
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =   6, o.amount_sales_with_vat, 0)) AS m06,
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =   7, o.amount_sales_with_vat, 0)) AS m07,
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =   8, o.amount_sales_with_vat, 0)) AS m08,
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =   9, o.amount_sales_with_vat, 0)) AS m09,
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =  10, o.amount_sales_with_vat, 0)) AS m10,
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =  11, o.amount_sales_with_vat, 0)) AS m11,
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =  12, o.amount_sales_with_vat, 0)) AS m12,                   
              SUM(IF(YEAR(i.document_date) = {$year} AND QUARTER(i.document_date) = 1, o.amount_sales_with_vat, 0)) AS q1,
              SUM(IF(YEAR(i.document_date) = {$year} AND QUARTER(i.document_date) = 2, o.amount_sales_with_vat, 0)) AS q2,
              SUM(IF(YEAR(i.document_date) = {$year} AND QUARTER(i.document_date) = 3, o.amount_sales_with_vat, 0)) AS q3,
              SUM(IF(YEAR(i.document_date) = {$year} AND QUARTER(i.document_date) = 4, o.amount_sales_with_vat, 0)) AS q4,                   
              SUM(IF(YEAR(i.document_date) = {$year}, o.amount_sales_with_vat, 0)) AS y,
            
              SUM(IF(YEAR(i.document_date) = {$prevYear} AND MONTH(i.document_date) =   1, o.amount_sales_with_vat, 0)) AS prev_m01,
              SUM(IF(YEAR(i.document_date) = {$prevYear} AND MONTH(i.document_date) =   2, o.amount_sales_with_vat, 0)) AS prev_m02,
              SUM(IF(YEAR(i.document_date) = {$prevYear} AND MONTH(i.document_date) =   3, o.amount_sales_with_vat, 0)) AS prev_m03,
              SUM(IF(YEAR(i.document_date) = {$prevYear} AND MONTH(i.document_date) =   4, o.amount_sales_with_vat, 0)) AS prev_m04,
              SUM(IF(YEAR(i.document_date) = {$prevYear} AND MONTH(i.document_date) =   5, o.amount_sales_with_vat, 0)) AS prev_m05,
              SUM(IF(YEAR(i.document_date) = {$prevYear} AND MONTH(i.document_date) =   6, o.amount_sales_with_vat, 0)) AS prev_m06,
              SUM(IF(YEAR(i.document_date) = {$prevYear} AND MONTH(i.document_date) =   7, o.amount_sales_with_vat, 0)) AS prev_m07,
              SUM(IF(YEAR(i.document_date) = {$prevYear} AND MONTH(i.document_date) =   8, o.amount_sales_with_vat, 0)) AS prev_m08,
              SUM(IF(YEAR(i.document_date) = {$prevYear} AND MONTH(i.document_date) =   9, o.amount_sales_with_vat, 0)) AS prev_m09,
              SUM(IF(YEAR(i.document_date) = {$prevYear} AND MONTH(i.document_date) =  10, o.amount_sales_with_vat, 0)) AS prev_m10,
              SUM(IF(YEAR(i.document_date) = {$prevYear} AND MONTH(i.document_date) =  11, o.amount_sales_with_vat, 0)) AS prev_m11,
              SUM(IF(YEAR(i.document_date) = {$prevYear} AND MONTH(i.document_date) =  12, o.amount_sales_with_vat, 0)) AS prev_m12,
              SUM(IF(YEAR(i.document_date) = {$prevYear} AND QUARTER(i.document_date) = 1, o.amount_sales_with_vat, 0)) AS prev_q1,
              SUM(IF(YEAR(i.document_date) = {$prevYear} AND QUARTER(i.document_date) = 2, o.amount_sales_with_vat, 0)) AS prev_q2,
              SUM(IF(YEAR(i.document_date) = {$prevYear} AND QUARTER(i.document_date) = 3, o.amount_sales_with_vat, 0)) AS prev_q3,
              SUM(IF(YEAR(i.document_date) = {$prevYear} AND QUARTER(i.document_date) = 4, o.amount_sales_with_vat, 0)) AS prev_q4,
              SUM(IF(YEAR(i.document_date) = {$prevYear}, o.amount_sales_with_vat, 0)) AS prev_y,
                   
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =   1, o.quantity, 0)) AS qq_m01,
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =   2, o.quantity, 0)) AS qq_m02,
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =   3, o.quantity, 0)) AS qq_m03,
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =   4, o.quantity, 0)) AS qq_m04,
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =   5, o.quantity, 0)) AS qq_m05,
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =   6, o.quantity, 0)) AS qq_m06,
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =   7, o.quantity, 0)) AS qq_m07,
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =   8, o.quantity, 0)) AS qq_m08,
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =   9, o.quantity, 0)) AS qq_m09,
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =  10, o.quantity, 0)) AS qq_m10,
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =  11, o.quantity, 0)) AS qq_m11,
              SUM(IF(YEAR(i.document_date) = {$year} AND MONTH(i.document_date) =  12, o.quantity, 0)) AS qq_m12,                   
              SUM(IF(YEAR(i.document_date) = {$year} AND QUARTER(i.document_date) = 1, o.quantity, 0)) AS qq_q1,
              SUM(IF(YEAR(i.document_date) = {$year} AND QUARTER(i.document_date) = 2, o.quantity, 0)) AS qq_q2,
              SUM(IF(YEAR(i.document_date) = {$year} AND QUARTER(i.document_date) = 3, o.quantity, 0)) AS qq_q3,
              SUM(IF(YEAR(i.document_date) = {$year} AND QUARTER(i.document_date) = 4, o.quantity, 0)) AS qq_q4,                   
              SUM(IF(YEAR(i.document_date) = {$year}, o.quantity, 0)) AS qq_y
            
            FROM `{$orderTable}` o
            LEFT JOIN `{$invoiceTable}` i ON i.id = o.invoice_id
            WHERE i.company_id = {$companyId}
              AND i.type = {$documentType}
              AND i.is_deleted = 0
              AND i.document_date BETWEEN '{$prevYear}-01-01' AND '{$year}-12-31'
              AND {$SQL_PAGE_FILTER}
              AND {$SQL_PRODUCTS_BY_CONTRACTOR}
              AND {$SQL_FILTER_BY_TYPE}
              AND {$SQL_FILTER_BY_NAME}
        ";
    }

    private function _getSubQueryByFlows()
    {
        $orderTable = Order::tableName();
        $invoiceTable = Invoice::tableName();
        $companyId = $this->_company->id;
        $documentType = Documents::IO_TYPE_OUT;
        $year = $this->year;
        $prevYear = $year - 1;

        $SQL_PAGE_FILTER = $this->getSqlPageFilter();
        $SQL_PRODUCTS_BY_CONTRACTOR = $this->getSqlProductsByContractor();
        $SQL_FILTER_BY_TYPE = $this->getSqlFilterByType();
        $SQL_FILTER_BY_NAME = $this->getSqlFilterByName();

        $paymentSubQuery = self::_getPaymentSubQuery();

        return "
            SELECT 
              o.product_id,  
              i.contractor_id,     

              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =   1, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS m01,
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =   2, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS m02,
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =   3, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS m03,
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =   4, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS m04,
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =   5, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS m05,
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =   6, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS m06,
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =   7, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS m07,
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =   8, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS m08,
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =   9, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS m09,
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =  10, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS m10,
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =  11, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS m11,
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =  12, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS m12,                   
              SUM(IF(YEAR(p.payment_date) = {$year} AND QUARTER(p.payment_date) = 1, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS q1,
              SUM(IF(YEAR(p.payment_date) = {$year} AND QUARTER(p.payment_date) = 2, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS q2,
              SUM(IF(YEAR(p.payment_date) = {$year} AND QUARTER(p.payment_date) = 3, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS q3,
              SUM(IF(YEAR(p.payment_date) = {$year} AND QUARTER(p.payment_date) = 4, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS q4,                   
              SUM(IF(YEAR(p.payment_date) = {$year}, o.amount_sales_with_vat, 0)) AS y,
            
              SUM(IF(YEAR(p.payment_date) = {$prevYear} AND MONTH(p.payment_date) =   1, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS prev_m01,
              SUM(IF(YEAR(p.payment_date) = {$prevYear} AND MONTH(p.payment_date) =   2, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS prev_m02,
              SUM(IF(YEAR(p.payment_date) = {$prevYear} AND MONTH(p.payment_date) =   3, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS prev_m03,
              SUM(IF(YEAR(p.payment_date) = {$prevYear} AND MONTH(p.payment_date) =   4, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS prev_m04,
              SUM(IF(YEAR(p.payment_date) = {$prevYear} AND MONTH(p.payment_date) =   5, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS prev_m05,
              SUM(IF(YEAR(p.payment_date) = {$prevYear} AND MONTH(p.payment_date) =   6, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS prev_m06,
              SUM(IF(YEAR(p.payment_date) = {$prevYear} AND MONTH(p.payment_date) =   7, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS prev_m07,
              SUM(IF(YEAR(p.payment_date) = {$prevYear} AND MONTH(p.payment_date) =   8, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS prev_m08,
              SUM(IF(YEAR(p.payment_date) = {$prevYear} AND MONTH(p.payment_date) =   9, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS prev_m09,
              SUM(IF(YEAR(p.payment_date) = {$prevYear} AND MONTH(p.payment_date) =  10, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS prev_m10,
              SUM(IF(YEAR(p.payment_date) = {$prevYear} AND MONTH(p.payment_date) =  11, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS prev_m11,
              SUM(IF(YEAR(p.payment_date) = {$prevYear} AND MONTH(p.payment_date) =  12, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS prev_m12,
              SUM(IF(YEAR(p.payment_date) = {$prevYear} AND QUARTER(p.payment_date) = 1, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS prev_q1,
              SUM(IF(YEAR(p.payment_date) = {$prevYear} AND QUARTER(p.payment_date) = 2, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS prev_q2,
              SUM(IF(YEAR(p.payment_date) = {$prevYear} AND QUARTER(p.payment_date) = 3, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS prev_q3,
              SUM(IF(YEAR(p.payment_date) = {$prevYear} AND QUARTER(p.payment_date) = 4, (i.payment_partial_amount / i.total_amount_with_nds) * o.amount_sales_with_vat, 0)) AS prev_q4,
              SUM(IF(YEAR(p.payment_date) = {$prevYear}, o.amount_sales_with_vat, 0)) AS prev_y,
                   
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =   1, o.quantity, 0)) AS qq_m01,
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =   2, o.quantity, 0)) AS qq_m02,
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =   3, o.quantity, 0)) AS qq_m03,
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =   4, o.quantity, 0)) AS qq_m04,
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =   5, o.quantity, 0)) AS qq_m05,
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =   6, o.quantity, 0)) AS qq_m06,
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =   7, o.quantity, 0)) AS qq_m07,
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =   8, o.quantity, 0)) AS qq_m08,
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =   9, o.quantity, 0)) AS qq_m09,
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =  10, o.quantity, 0)) AS qq_m10,
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =  11, o.quantity, 0)) AS qq_m11,
              SUM(IF(YEAR(p.payment_date) = {$year} AND MONTH(p.payment_date) =  12, o.quantity, 0)) AS qq_m12,                   
              SUM(IF(YEAR(p.payment_date) = {$year} AND QUARTER(p.payment_date) = 1, o.quantity, 0)) AS qq_q1,
              SUM(IF(YEAR(p.payment_date) = {$year} AND QUARTER(p.payment_date) = 2, o.quantity, 0)) AS qq_q2,
              SUM(IF(YEAR(p.payment_date) = {$year} AND QUARTER(p.payment_date) = 3, o.quantity, 0)) AS qq_q3,
              SUM(IF(YEAR(p.payment_date) = {$year} AND QUARTER(p.payment_date) = 4, o.quantity, 0)) AS qq_q4,                   
              SUM(IF(YEAR(p.payment_date) = {$year}, o.quantity, 0)) AS qq_y
            
            FROM `{$orderTable}` o
            LEFT JOIN `{$invoiceTable}` i ON i.id = o.invoice_id
            LEFT JOIN ({$paymentSubQuery}) p ON p.invoice_id = i.id
            WHERE i.company_id = {$companyId}
              AND i.type = {$documentType}
              AND i.is_deleted = 0
              AND p.payment_date BETWEEN '{$prevYear}-01-01' AND '{$year}-12-31'
              AND {$SQL_PAGE_FILTER}
              AND {$SQL_PRODUCTS_BY_CONTRACTOR}
              AND {$SQL_FILTER_BY_TYPE}
              AND {$SQL_FILTER_BY_NAME}
        ";
    }

    private function _getPaymentSubQuery()
    {
        $flowType = CashFlowsBase::FLOW_TYPE_INCOME;
        $companyId = $this->_company->id;
        $year = $this->year;
        $prevYear = $year - 1;

        return "
            SELECT pp.invoice_id, MAX(pp.payment_date) AS payment_date FROM ( 
              SELECT r1.invoice_id, MAX(p1.date) AS payment_date FROM cash_bank_flows p1 LEFT JOIN cash_bank_flow_to_invoice r1 ON p1.id = r1.flow_id 
                WHERE p1.company_id = {$companyId} AND p1.recognition_date BETWEEN '{$prevYear}-01-01' AND '{$year}-12-31' AND p1.flow_type = {$flowType} GROUP BY r1.invoice_id
              UNION ALL
              SELECT r2.invoice_id, MAX(p2.date) AS payment_date FROM cash_order_flows p2 LEFT JOIN cash_order_flow_to_invoice r2 ON p2.id = r2.flow_id 
                WHERE p2.company_id = {$companyId} AND p2.recognition_date BETWEEN '{$prevYear}-01-01' AND '{$year}-12-31' AND p2.flow_type = {$flowType} GROUP BY r2.invoice_id
              UNION ALL
              SELECT r3.invoice_id, MAX(p3.date) AS payment_date FROM cash_emoney_flows p3 LEFT JOIN cash_emoney_flow_to_invoice r3 ON p3.id = r3.flow_id 
                WHERE p3.company_id = {$companyId} AND p3.recognition_date BETWEEN '{$prevYear}-01-01' AND '{$year}-12-31' AND p3.flow_type = {$flowType} GROUP BY r3.invoice_id
        ) pp GROUP BY pp.invoice_id
        ";
    }

    /**
     * @return array]
     */
    public function getSortColumns()
    {
        $amount = trim(self::PREFIX_AMOUNT, '_');
        $quantity = trim(self::PREFIX_QUANTITY, '_');
        $percent = trim(self::PREFIX_PERCENT, '_');
        $percentTotals = trim(self::PREFIX_PERCENT_TOTAL, '_');

        return [
            "item_name",
            "{$quantity}_m01",
            "{$quantity}_m02",
            "{$quantity}_m03",
            "{$quantity}_m04",
            "{$quantity}_m05",
            "{$quantity}_m06",
            "{$quantity}_m07",
            "{$quantity}_m08",
            "{$quantity}_m09",
            "{$quantity}_m10",
            "{$quantity}_m11",
            "{$quantity}_m12",
            "{$quantity}_q1",
            "{$quantity}_q2",
            "{$quantity}_q3",
            "{$quantity}_q4",
            "{$quantity}_y",
            "{$amount}_m01",
            "{$amount}_m02",
            "{$amount}_m03",
            "{$amount}_m04",
            "{$amount}_m05",
            "{$amount}_m06",
            "{$amount}_m07",
            "{$amount}_m08",
            "{$amount}_m09",
            "{$amount}_m10",
            "{$amount}_m11",
            "{$amount}_m12",
            "{$amount}_q1",
            "{$amount}_q2",
            "{$amount}_q3",
            "{$amount}_q4",
            "{$amount}_y",
            "{$percent}_m01",
            "{$percent}_m02",
            "{$percent}_m03",
            "{$percent}_m04",
            "{$percent}_m05",
            "{$percent}_m06",
            "{$percent}_m07",
            "{$percent}_m08",
            "{$percent}_m09",
            "{$percent}_m10",
            "{$percent}_m11",
            "{$percent}_m12",
            "{$percent}_q1",
            "{$percent}_q2",
            "{$percent}_q3",
            "{$percent}_q4",
            "{$percent}_y",
            "{$percentTotals}_m01",
            "{$percentTotals}_m02",
            "{$percentTotals}_m03",
            "{$percentTotals}_m04",
            "{$percentTotals}_m05",
            "{$percentTotals}_m06",
            "{$percentTotals}_m07",
            "{$percentTotals}_m08",
            "{$percentTotals}_m09",
            "{$percentTotals}_m10",
            "{$percentTotals}_m11",
            "{$percentTotals}_m12",
            "{$percentTotals}_q1",
            "{$percentTotals}_q2",
            "{$percentTotals}_q3",
            "{$percentTotals}_q4",
            "{$percentTotals}_y",
        ];
    }

    public function getSortDefault()
    {
        if ($this->year == date('Y'))
            return ['amount_m'.date('m') => SORT_DESC];

        return ['amount_y' => SORT_ASC];
    }

    public function getSortDefaultKey()
    {
        if ($this->year == date('Y'))
            return '-amount_m'.date('m');

        return '-amount_y';
    }

    /**
     * @return string
     */
    public function getSqlFilterByName():string
    {
        if ($this->name) {

            if ($this->groupByContractor) {

                if ($this->_filterContractorIds === null) {
                    $this->_filterContractorIds = Contractor::find()
                        ->where(['company_id' => $this->_company->id])
                        ->andWhere(['or', ['like', 'name', $this->name]])
                        ->select('id')
                        ->column();
                }

                if (empty($this->_filterContractorIds))
                    $this->_filterContractorIds = [-1];

                return 'contractor_id IN (' . implode(',', $this->_filterContractorIds) . ')';

            } else {

                if ($this->_filterProductsIds === null) {
                    $this->_filterProductsIds = Product::find()
                        ->where(['company_id' => $this->_company->id])
                        ->andWhere(['or', ['like', 'title', $this->name]])
                        ->select('id')
                        ->column();
                }

                if (empty($this->_filterProductsIds))
                    $this->_filterProductsIds = [-1];

                return 'product_id IN (' . implode(',', $this->_filterProductsIds) . ')';
            }
        }

        return '1 = 1';
    }

    public function getSqlPageFilter():string
    {
        if ($this->project_id) {

            return 'project_id = ' . ((int)$this->project_id);
        }

        return '1 = 1';
    }

    public function getSqlProductsByContractor():string
    {
        if ($this->contractor_id) {

            return 'contractor_id = ' . ((int)$this->contractor_id);
        }

        return '1 = 1';
    }

    /**
     * @return string
     */
    public function getSqlFilterByType():string
    {
        if ($this->searchBy == self::BY_INVOICES) {
            if ($this->type) {
                if ($this->type == self::TYPE_PAID) {
                    return 'invoice_status_id = ' . InvoiceStatus::STATUS_PAYED;
                } else {
                    return 'invoice_status_id NOT IN (' . implode(',', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_REJECTED, InvoiceStatus::STATUS_AUTOINVOICE]) . ')';
                }
            }
        }

        return 'invoice_status_id NOT IN ('.implode(',', [InvoiceStatus::STATUS_REJECTED, InvoiceStatus::STATUS_AUTOINVOICE]) . ')';
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        if (!isset($this->_yearFilter)) {
            $minDate = Yii::$app->db2->createCommand("SELECT MIN(`document_date`) FROM `invoice` WHERE is_deleted = 0 AND company_id = " . $this->_company->id)->queryScalar();
            $minCashDate = !empty($minDate) ? max(self::MIN_REPORT_DATE, $minDate) : date(DateHelper::FORMAT_DATE);
            $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
            $currentYear = date('Y');
            foreach (range($registrationYear, $currentYear) as $value) {
                $range[$value] = $value;
            }
            arsort($range);

            $this->_yearFilter = $range;
        }

        return $this->_yearFilter;
    }

    public function setYearFilter($yearsArray)
    {
        $this->_yearFilter = $yearsArray;
    }

    /**
     * @param $periods
     * @return array
     * @throws \yii\db\Exception
     */
    public function getChartData($periods)
    {
        $invoiceTable = Invoice::tableName();
        $companyId = $this->_company->id;
        $documentType = Documents::IO_TYPE_OUT;

        $from = $periods[0]['from'];
        $to = $periods[count($periods)-1]['to'];

        $SQL_PAGE_FILTER = $this->getSqlPageFilter();
        $SQL_FILTER_BY_TYPE = $this->getSqlFilterByType();

        if ($this->searchBy == self::BY_FLOWS) {
            // by flows
            $paymentSubQuery = self::_getPaymentSubQuery();
            $rawData = Yii::$app->db->createCommand("
                SELECT 
                  DATE_FORMAT(p.payment_date, '%Y-%m') AS ym, 
                  SUM(i.total_amount_with_nds) AS amount
                FROM `{$invoiceTable}` i
                LEFT JOIN ({$paymentSubQuery}) p ON p.invoice_id = i.id
                WHERE i.company_id = {$companyId}
                  AND i.type = {$documentType}
                  AND i.is_deleted = 0
                  AND p.payment_date BETWEEN '{$from}' AND '{$to}'
                  AND {$SQL_PAGE_FILTER}              
                  AND {$SQL_FILTER_BY_TYPE}
                GROUP BY ym
            ")->queryAll();

        } else {
            // by invoices
            $rawData = Yii::$app->db->createCommand("
                SELECT 
                  DATE_FORMAT(i.document_date, '%Y-%m') AS ym, 
                  SUM(i.total_amount_with_nds) AS amount
                FROM `{$invoiceTable}` i
                WHERE i.company_id = {$companyId}
                  AND i.type = {$documentType}
                  AND i.is_deleted = 0
                  AND i.document_date BETWEEN '{$from}' AND '{$to}'
                  AND {$SQL_PAGE_FILTER}              
                  AND {$SQL_FILTER_BY_TYPE}
                GROUP BY ym
            ")->queryAll();
        }

        $data = [];
        foreach ($rawData as $d)
            $data[$d['ym']] = $d['amount'] / 100;

        $ret = [];
        foreach ($periods as $p) {
            $ret[] = ArrayHelper::getValue($data, substr($p['from'], 0, 7), 0);
        }

        return $ret;
    }

    public function getStructureChartData($dateFrom, $dateTo, $maxRows)
    {
        $orderTable = Order::tableName();
        $invoiceTable = Invoice::tableName();
        $companyId = $this->_company->id;
        $documentType = Documents::IO_TYPE_OUT;
        $year = $this->year;

        $SQL_PAGE_FILTER = $this->getSqlPageFilter();
        $SQL_FILTER_BY_TYPE = $this->getSqlFilterByType();

        if ($this->searchBy == self::BY_FLOWS) {
            // by flows
            $paymentSubQuery = self::_getPaymentSubQuery();
            $query = "
                SELECT 
                  o.product_id AS pid,
                  i.contractor_id AS cid, 
                  SUM(o.amount_sales_with_vat) AS amount
                FROM `{$orderTable}` o
                LEFT JOIN `{$invoiceTable}` i ON i.id = o.invoice_id
                LEFT JOIN ({$paymentSubQuery}) p ON p.invoice_id = i.id
                WHERE i.company_id = {$companyId}
                  AND i.type = {$documentType}
                  AND i.is_deleted = 0
                  AND p.payment_date BETWEEN '{$dateFrom}' AND '{$dateTo}'
                  AND {$SQL_PAGE_FILTER}              
                  AND {$SQL_FILTER_BY_TYPE}            
            ";
        } else {
            $query = "
                SELECT 
                  o.product_id AS pid,
                  i.contractor_id AS cid, 
                  SUM(o.amount_sales_with_vat) AS amount
                FROM `{$orderTable}` o
                LEFT JOIN `{$invoiceTable}` i ON i.id = o.invoice_id
                WHERE i.company_id = {$companyId}
                  AND i.type = {$documentType}
                  AND i.is_deleted = 0
                  AND i.document_date BETWEEN '{$dateFrom}' AND '{$dateTo}'
                  AND {$SQL_PAGE_FILTER}              
                  AND {$SQL_FILTER_BY_TYPE}            
            ";
        }

        $query .= ($this->groupByContractor)
            ? " GROUP BY i.contractor_id"
            : " GROUP BY o.product_id";

        $query .= ' ORDER BY amount DESC';

        $data = Yii::$app->db2->createCommand($query)->queryAll();

        $n = 0;
        if ($this->groupByContractor) {
            foreach ($data as $k => $v) {
                $data[$k]['amount'] = round($v['amount'] / 100, 2);
                if (++$n <= $maxRows)
                    $data[$k]['title'] = ($c = Contractor::findOne($v['cid'])) ? $c->getShortName() : "id={$v['cid']}";
            }
        } else {
            foreach ($data as $k => $v) {
                $data[$k]['amount'] = round($v['amount'] / 100, 2);
                if (++$n <= $maxRows)
                    $data[$k]['title'] = ($p = Product::findOne($v['pid'])) ? $p->title : "id={$v['pid']}";
            }
        }

        return $data;
    }

    public function getShowPercent():bool
    {
        try {
            return (bool)Yii::$app->user->identity->config->selling_report_percent;
        } catch (\Throwable $e) {
            return false;
        }
    }

    public function getShowPercentTotal():bool
    {
        try {
            return (bool)Yii::$app->user->identity->config->selling_report_percent_total;
        } catch (\Throwable $e) {
            return false;
        }
    }

    ///////////
    /// XLS ///
    ///////////

    public function generateXls()
    {
        $dataProvider = $this->search(Yii::$app->request->get());
        $dataProvider->setPagination(false);
        $data = $dataProvider->getModels();

        // header
        $columns = [[
            'attribute' => 'itemName',
            'header' => 'Статьи',
        ]];

        foreach (AbstractFinance::$month as $monthNumber => $monthText) {
            $monthNumber = (int)$monthNumber;
            $columns[] = [
                'attribute' => "item{$monthNumber}",
                'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                'header' => $monthText,
            ];
        }
        $columns[] = [
            'attribute' => 'dataYear',
            'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
            'header' => $this->year,
        ];

        // body
        $formattedData = [];

        foreach ($data as $paymentType => $level1) {
            $formattedData[] = $this->_buildXlsRow($level1, 1);
        }

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $formattedData,
            'title' => "Продажи за {$this->year} год",
            'rangeHeader' => range('A', 'N'),
            'columns' => $columns,
            'format' => 'Xlsx',
            'fileName' => "Продажи за {$this->year} год",
        ]);
    }

    protected function _buildXlsRow($data, $level = 1)
    {
        $row = [
            'itemName' => $data['item_name'],
            'dataYear' => ($data[self::PREFIX_AMOUNT."y"] ?? '0.00') / 100
        ];
        foreach (AbstractFinance::$month as $month => $monthName)
            $row['item'.(int)$month] = ($data[self::PREFIX_AMOUNT."m".$month] ?? '0.00') / 100;

        return $row;
    }
}