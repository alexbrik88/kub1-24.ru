<?php
namespace frontend\modules\analytics\models;

use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use common\models\document\Act;
use common\models\document\GoodsCancellation;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\document\OrderAct;
use common\models\document\OrderPackingList;
use common\models\document\OrderUpd;
use common\models\document\PackingList;
use common\models\document\Upd;
use common\models\product\ProductTurnover;
use common\modules\acquiring\models\AcquiringOperation;
use common\modules\cards\models\CardOperation;
use frontend\models\Documents;
use frontend\modules\analytics\models\profitAndLoss\BaseOlapTrait;
use frontend\modules\cash\models\CashContractorType;
use common\models\Contractor;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class ProfitAndLossSearchItemsModel
 * @package frontend\modules\analytics\models
 */
class ProfitAndLossSearchItemsModel extends AbstractFinance
{
    // structure filters (items table)
    public $isRevenue;
    public $isPrimeCosts;
    public $primeCostsArticle;
    public $ioType;
    public $accountingOperationsOnly;
    public $onlyNotForSaleProducts;

    // custom filters (items table)
    public $company_id;
    public $contractor_id;
    public $reason_id;
    public $period;
    public $group;
    public $flow_type;
    public $payment_type;
    public $doc_type;
    public $pal_type_id;
    public $sale_point_id;
    public $industry_id;
    public $project_id;
    public $product_id;
    public $product_group_id;

    // page filters
    public $pageFilters = [
        'industry_id' => null,
        'sale_point_id' => null,
        'project_id' => null
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['company_id', 'contractor_id', 'reason_id', 'payment_type', 'doc_type', 'pal_type_id'], 'safe'],
            [['project_id', 'sale_point_id', 'industry_id', 'product_id', 'product_group_id'], 'safe']
        ]);
    }

    public function searchItemsByDocs($periodStart, $periodEnd)
    {
        $this->load(\Yii::$app->request->get());

        if ($this->expenditure_item_id == InvoiceExpenditureItem::ITEM_GOODS_CANCELLATION) {

            // goods cancellations
            $query = (new Query)
                ->from(['t' => $this->getGoodsCancellationQuery($this->multiCompanyIds, $periodStart, $periodEnd)]);

        } elseif ($this->isPrimeCosts) {

            if ($this->primeCostsArticle == InvoiceExpenditureItem::ITEM_PRIME_COST_PRODUCT) {

                // docs
                $tp = $this->getDocQuery(self::PACKING_LIST_BLOCK, $this->multiCompanyIds, $periodStart, $periodEnd);
                $tu = $this->getDocQuery(self::UPD_BLOCK, $this->multiCompanyIds, $periodStart, $periodEnd);
                $query = (new Query)
                    ->from(['t' => $tu->union($tp, true)]);

            } else {

                // docs
                $ta = $this->getDocQuery(self::ACT_BLOCK, $this->multiCompanyIds, $periodStart, $periodEnd);
                $tu = $this->getDocQuery(self::UPD_BLOCK, $this->multiCompanyIds, $periodStart, $periodEnd);
                $query = (new Query)
                    ->from(['t' => $tu->union($ta, true)]);
            }

        } elseif ($this->doc_type) {

            // docs filtered by type
            $query = (new Query)
                ->from(['t' => $this->getDocQuery($this->doc_type, $this->multiCompanyIds, $periodStart, $periodEnd)]);

        } else {

            // docs
            $ta = $this->getDocQuery(self::ACT_BLOCK, $this->multiCompanyIds, $periodStart, $periodEnd);
            $tp = $this->getDocQuery(self::PACKING_LIST_BLOCK, $this->multiCompanyIds, $periodStart, $periodEnd);
            $tu = $this->getDocQuery(self::UPD_BLOCK, $this->multiCompanyIds, $periodStart, $periodEnd);
            $query = (new Query)
                ->from(['t' => $ta->union($tp, true)->union($tu, true)]);

        }

        $query->addSelect([
            't.id',
            't.company_id',
            't.tb',
            't.document_date',
            't.document_number',
            't.document_additional_number',
            't.type',
            't.amount',
            't.invoice_document_number',
            't.contractor_id',
            't.invoice_income_item_id',
            't.invoice_expenditure_item_id'
        ]);
        $query->andWhere(['type' => $this->ioType]);

        // filter for multicompanies
        $query->andFilterWhere(['t.company_id' => $this->company_id]);

        if ($this->contractor_id !== null) {
            if (is_array($this->contractor_id) && in_array('0', $this->contractor_id)) {
                $query->andWhere(['or',
                    ['t.contractor_id' => $this->contractor_id],
                    ['t.contractor_id' => null],
                ]);
            } elseif ($this->contractor_id == '0') {
                $query->andWhere(['t.contractor_id' => null]);
            } else {
                $query->andWhere(['t.contractor_id' => $this->contractor_id]);
            }
        }
        if ($this->sale_point_id !== null) {
            if (is_array($this->sale_point_id) && in_array('0', $this->sale_point_id)) {
                $query->andWhere(['or',
                    ['t.sale_point_id' => $this->sale_point_id],
                    ['t.sale_point_id' => null],
                ]);
            } else {
                $query->andWhere(['t.sale_point_id' => $this->sale_point_id ?: null]);
            }
        }
        if ($this->industry_id !== null) {
            if (is_array($this->industry_id) && in_array('0', $this->industry_id)) {
                $query->andWhere(['or',
                    ['t.industry_id' => $this->industry_id],
                    ['t.industry_id' => null],
                ]);
            } else {
                $query->andWhere(['t.industry_id' => $this->industry_id ?: null]);
            }
        }
        if ($this->project_id !== null) {
            if (is_array($this->project_id) && in_array('0', $this->project_id)) {
                $query->andWhere(['or',
                    ['t.project_id' => $this->project_id],
                    ['t.project_id' => null],
                ]);
            } else {
                $query->andWhere(['t.project_id' => $this->project_id ?: null]);
            }
        }
        $query->groupBy('t.id');

        // User Options
        if ($this->isRevenue) {
            $this->income_item_id = $this->getItemsByPalOptions(AnalyticsArticle::PAL_INCOME_REVENUE);
        }

        if (!empty($this->income_item_id)) {
            $this->income_item_id =
                self::getArticlesWithChildren($this->income_item_id, CashBankFlows::FLOW_TYPE_INCOME, $this->multiCompanyIds);
            $query->andWhere(['and',
                    ['type' => Documents::IO_TYPE_OUT],
                    ['in', 'invoice_income_item_id', $this->income_item_id],
            ]);

        } elseif (!empty($this->expenditure_item_id)) {
            $this->expenditure_item_id =
                self::getArticlesWithChildren($this->expenditure_item_id, CashBankFlows::FLOW_TYPE_EXPENSE, $this->multiCompanyIds);

            $query->andWhere(['and',
                ['type' => Documents::IO_TYPE_IN],
                ['in', 'invoice_expenditure_item_id', $this->expenditure_item_id],
            ]);
        }

        if ($this->accountingOperationsOnly) {
            $notAccountingContractors = $this->_getNotAccountingContractors();
            $query->andFilterWhere(['not', ['t.contractor_id' => $notAccountingContractors]]);
        }

        $dataProvider = new ActiveDataProvider([
            'db' => \Yii::$app->db2,
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'amount',
                    'document_number' => [
                        'asc' => [
                            '`document_number` * 1' => SORT_ASC,
                            '`document_additional_number` * 1' => SORT_ASC,
                        ],
                        'desc' => [
                            '`document_number` * 1' => SORT_DESC,
                            '`document_additional_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'invoice_document_number' => [
                        'asc' => [
                            '`invoice_document_number` * 1' => SORT_ASC,
                            '`document_additional_number` * 1' => SORT_ASC,
                        ],
                        'desc' => [
                            '`invoice_document_number` * 1' => SORT_DESC,
                            '`document_additional_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'document_date' => [
                        'asc' => [
                            'document_date' => SORT_ASC,
                            'created_at' => SORT_ASC,
                        ],
                        'desc' => [
                            'document_date' => SORT_DESC,
                            'created_at' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                ],
                'defaultOrder' => [
                    'document_date' => SORT_DESC,
                ],
            ],
        ]);

        $this->_filterQuery = clone $query;

        return $dataProvider;
    }

    public function searchItemsByFlows($periodStart, $periodEnd)
    {
        $this->load(\Yii::$app->request->get());
        $this->checkCashContractor();

        // payment_type: 1-bank, 2-order, 3-emoney, 4-acquiring, 5-cards
        if ($this->payment_type) {
            if (in_array($this->payment_type, [self::CASH_ACQUIRING_BLOCK, self::CASH_CARD_BLOCK])) {
                $query = (new Query)
                    ->from(['t' => $this->getFlowWithoutInvoicesQuery($this->payment_type, $this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id')]);
            } else {
                $query = (new Query)->from(['t' =>
                    $this->getFlowQuery($this->payment_type, $this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id')
                        ->union($this->getForeignFlowQuery($this->payment_type, $this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id'))
                ]);
            }
        } else {

            $cbf = $this->getFlowQuery(self::CASH_BANK_BLOCK, $this->multiCompanyIds, $periodStart, $periodEnd)->andWhere(['t.has_tin_children' => 0])->groupBy('t.id');
            $cef = $this->getFlowQuery(self::CASH_EMONEY_BLOCK, $this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id');
            $cof = $this->getFlowQuery(self::CASH_ORDER_BLOCK, $this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id');
            $a   = $this->getFlowWithoutInvoicesQuery(self::CASH_ACQUIRING_BLOCK, $this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id');
            $c   = $this->getFlowWithoutInvoicesQuery(self::CASH_CARD_BLOCK, $this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id');

            if ($this->onlyNotForSaleProducts) {
                $query = (new Query)
                    ->from(['t' => $cbf->union($cef, true)->union($cof, true)]);
            } else {

                // foreign
                $cbf2 = $this->getForeignFlowQuery(self::CASH_BANK_BLOCK, $this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id');
                $cef2 = $this->getForeignFlowQuery(self::CASH_EMONEY_BLOCK, $this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id');
                $cof2 = $this->getForeignFlowQuery(self::CASH_ORDER_BLOCK, $this->multiCompanyIds, $periodStart, $periodEnd)->groupBy('t.id');

                $query = (new Query)
                    ->from(['t' =>
                        $cbf->union($cef, true)
                            ->union($cof, true)
                            ->union($a, true)
                            ->union($c, true)
                            ->union($cbf2, true)
                            ->union($cef2, true)
                            ->union($cof2, true)
                    ]);
            }
        }

        $query->addSelect([
            't.id',
            't.company_id',
            't.tb',
            'SUM(amountExpense) as amountExpense',
            'SUM(amountIncome) as amountIncome',
            't.rs',
            't.created_at',
            't.date',
            't.recognition_date',
            't.flow_type',
            't.amount',
            't.amount_rub',
            't.currency_name',
            't.contractor_id',
            't.description',
            't.expenditure_item_id',
            't.income_item_id',
            't.document_number',
            't.document_additional_number',
            //'t.incomeItemName',
            //'t.expenseItemName',
            't.is_accounting',
            't.tin_child_amount'
        ]);
        $query->groupBy(['t.tb', 't.id']);

        // filter for multicompanies
        $query->andFilterWhere(['t.company_id' => $this->company_id]);

        if ($this->contractor_id !== null) {
            if (is_array($this->contractor_id) && in_array('0', $this->contractor_id)) {
                $query->andWhere(['or',
                    ['t.contractor_id' => $this->contractor_id],
                    ['t.contractor_id' => null],
                ]);
            } elseif ($this->contractor_id == '0') {
                $query->andWhere(['t.contractor_id' => null]);
            } else {
                $query->andWhere(['t.contractor_id' => $this->contractor_id]);
            }
        }
        if ($this->sale_point_id !== null) {
            if (is_array($this->sale_point_id) && in_array('0', $this->sale_point_id)) {
                $query->andWhere(['or',
                    ['t.sale_point_id' => $this->sale_point_id],
                    ['t.sale_point_id' => null],
                ]);
            } else {
                $query->andWhere(['t.sale_point_id' => $this->sale_point_id ?: null]);
            }
        }
        if ($this->industry_id !== null) {
            if (is_array($this->industry_id) && in_array('0', $this->industry_id)) {
                $query->andWhere(['or',
                    ['t.industry_id' => $this->industry_id],
                    ['t.industry_id' => null],
                ]);
            } else {
                $query->andWhere(['t.industry_id' => $this->industry_id ?: null]);
            }
        }
        if ($this->project_id !== null) {
            if (is_array($this->project_id) && in_array('0', $this->project_id)) {
                $query->andWhere(['or',
                    ['t.project_id' => $this->project_id],
                    ['t.project_id' => null],
                ]);
            } else {
                $query->andWhere(['t.project_id' => $this->project_id ?: null]);
            }
        }

        if ($this->accountingOperationsOnly) {
            $notAccountingContractors = $this->_getNotAccountingContractors();
            $query->andWhere(['or',
                ['t.tb' => ['cash_bank_flows', 'acquiring_operation', 'card_operation']],
                ['and',
                    ['t.tb' => ['cash_emoney_flows', 'cash_order_flows']],
                    ['t.is_accounting' => 1],
                    ['not', ['t.contractor_id' => $notAccountingContractors]]
                ],
            ]);
        }

        if ($this->isRevenue) {

            // User Options
            $this->income_item_id = $this->getItemsByPalOptions(AnalyticsArticle::PAL_INCOME_REVENUE);

            $query
                ->andWhere(['flow_type' => CashBankFlows::FLOW_TYPE_INCOME])
                //->andWhere(['not', ['in', 'income_item_id', BaseOlapTrait::getExceptUndestributedIncomeItems()]]) // OLD!
                ->andWhere(['or',
                    ['invoice_id' => null],
                    ['or',
                        ['and',
                            ['not', ['invoice_id' => null]],
                            ['invoice_has_act' => 0],
                            ['invoice_has_packing_list' => 0],
                            ['invoice_has_upd' => 0],
                        ],
                        ['and',
                            ['not', ['invoice_id' => null]],
                            ['or',
                                ['invoice_need_act' => 0],
                                ['invoice_need_packing_list' => 0],
                                ['invoice_need_upd' => 0],
                            ]
                        ],
                    ]
                ]);
        }

        if (!empty($this->income_item_id) && !empty($this->expenditure_item_id)) {
            $query->andWhere(['or',
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_EXPENSE],
                    ['in', 'expenditure_item_id', $this->expenditure_item_id],
                ],
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_INCOME],
                    ['in', 'income_item_id', $this->income_item_id],
                ],
                $this->cash_contractor ? ['in', 'contractor_id', $this->cash_contractor] : [],
            ]);
        } elseif (!empty($this->income_item_id)) {

            $this->income_item_id =
                self::getArticlesWithChildren($this->income_item_id, CashBankFlows::FLOW_TYPE_INCOME, $this->multiCompanyIds);

            $query->andWhere(['or',
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_INCOME],
                    ['in', 'income_item_id', $this->income_item_id],
                ],
                $this->cash_contractor ? ['in', 'contractor_id', $this->cash_contractor] : [],
            ]);
        } elseif (!empty($this->expenditure_item_id)) {

            $this->expenditure_item_id =
                self::getArticlesWithChildren($this->expenditure_item_id, CashBankFlows::FLOW_TYPE_EXPENSE, $this->multiCompanyIds);

            $query->andWhere(['or',
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_EXPENSE],
                    ['in', 'expenditure_item_id', $this->expenditure_item_id],
                ],
                $this->cash_contractor ? ['in', 'contractor_id', $this->cash_contractor] : [],
            ]);
        } else {
            if ($this->cash_contractor) {
                $query->andWhere(['in', 'contractor_id', $this->cash_contractor]);
            } else {
                // $query->andWhere(['flow_type' => null]);
            }
        }

        if ($this->reason_id == 'empty') {
            $query->andWhere(['or',
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_EXPENSE],
                    ['expenditure_item_id' => null],
                ],
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_INCOME],
                    ['income_item_id' => null],
                ],
            ]);
        } elseif (!empty($this->reason_id)) {
            $this->expenditure_item_id = $this->income_item_id = null;
            if (substr($this->reason_id, 0, 2) === 'e_') {
                $this->expenditure_item_id = substr($this->reason_id, 2);
            } elseif (substr($this->reason_id, 0, 2) === 'i_') {
                $this->income_item_id = substr($this->reason_id, 2);
            }
            $query->andFilterWhere([
                'expenditure_item_id' => $this->expenditure_item_id,
                'income_item_id' => $this->income_item_id,
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'db' => \Yii::$app->db2,
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'date' => [
                        'asc' => [
                            'date' => SORT_ASC,
                            'created_at' => SORT_ASC,
                        ],
                        'desc' => [
                            'date' => SORT_DESC,
                            'created_at' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'recognition_date' => [
                        'asc' => [
                            'recognition_date' => SORT_ASC,
                            'created_at' => SORT_ASC,
                        ],
                        'desc' => [
                            'recognition_date' => SORT_DESC,
                            'created_at' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'amountIncome' => [
                        'asc' => [
                            'flow_type' => SORT_DESC,
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_DESC,
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'amountExpense' => [
                        'asc' => [
                            'flow_type' => SORT_ASC,
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_ASC,
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'contractor_id',
                    'description',
                    'created_at',
                    'billPaying' => [
                        'asc' => ['document_number' => SORT_ASC, 'document_additional_number' => SORT_ASC],
                        'desc' => ['document_number' => SORT_DESC, 'document_additional_number' => SORT_ASC],
                        'default' => SORT_ASC
                    ],
                ],
                'defaultOrder' => [
                    'recognition_date' => SORT_DESC,
                ],
            ],
        ]);

        $this->_filterQuery = clone $query;

        return $dataProvider;
    }

    private function getFlowClassNameByType($type)
    {
        switch ($type) {
            case self::CASH_BANK_BLOCK:
                $className = CashBankFlows::class;
                break;
            case self::CASH_ORDER_BLOCK:
                $className = CashOrderFlows::class;
                break;
            case self::CASH_EMONEY_BLOCK:
                $className = CashEmoneyFlows::class;
                break;
            case self::CASH_ACQUIRING_BLOCK:
                $className = AcquiringOperation::class;
                break;
            case self::CASH_CARD_BLOCK:
                $className = CardOperation::class;
                break;
            default:
                throw new \Exception('Invalid type.');
                break;
        }

        return $className;
    }

    /**
     * @param $type
     * @return string
     * @throws \Exception
     */
    private function getForeignFlowClassNameByType($type)
    {
        switch ($type) {
            case self::CASH_BANK_BLOCK:
            case self::CASH_FOREIGN_BANK_BLOCK:
                $className = CashBankForeignCurrencyFlows::class;
                break;
            case self::CASH_ORDER_BLOCK:
            case self::CASH_FOREIGN_ORDER_BLOCK:
                $className = CashOrderForeignCurrencyFlows::class;
                break;
            case self::CASH_EMONEY_BLOCK:
            case self::CASH_FOREIGN_EMONEY_BLOCK:
                $className = CashEmoneyForeignCurrencyFlows::class;
                break;
            default:
                throw new \Exception('Invalid type.');
                break;
        }

        return $className;
    }

    private function getDocClassNameByType($type)
    {
        switch ($type) {
            case self::ACT_BLOCK:
                $className = Act::class;
                break;
            case self::PACKING_LIST_BLOCK:
                $className = PackingList::class;
                break;
            case self::UPD_BLOCK:
                $className = Upd::class;
                break;
            case self::GOODS_CANCELLATION_BLOCK:
                $className = GoodsCancellation::class;
                break;
            default:
                throw new \Exception('Invalid type.');
                break;
        }

        return $className;
    }

    private function getDocOrderTableNameByType($type)
    {
        switch ($type) {
            case self::ACT_BLOCK:
                $tableName = OrderAct::tableName();
                break;
            case self::PACKING_LIST_BLOCK:
                $tableName = OrderPackingList::tableName();
                break;
            case self::UPD_BLOCK:
                $tableName = OrderUpd::tableName();
                break;
            default:
                throw new \Exception('Invalid type.');
                break;
        }

        return $tableName;
    }

    private function getFlowQuery($cashBlock, $companyID, $periodStart, $periodEnd)
    {
        /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
        $className = $this->getFlowClassNameByType($cashBlock);
        $tableName = $className::tableName();

        $query = $className::find()
            ->select([
                't.id',
                't.company_id',
                new Expression('"' . $tableName . '" as tb'),
                "IF({{t}}.[[flow_type]] = 0, SUM({{t}}.[[amount]]), 0) as amountExpense",
                "IF({{t}}.[[flow_type]] = 1, SUM({{t}}.[[amount]]), 0) as amountIncome",
                $cashBlock == self::CASH_BANK_BLOCK ? 't.rs as rs' : new Expression('null as rs'),
                $cashBlock == self::CASH_BANK_BLOCK ? new Expression("1 AS is_accounting") : "t.is_accounting",
                't.created_at',
                't.date',
                't.recognition_date',
                't.flow_type',
                new Expression(($this->isPrimeCosts)
                    ? 'SUM(IFNULL(product.price_for_buy_with_nds,0) * order.quantity) AS amount'
                    : 't.amount as amount'
                ),
                new Expression(($this->isPrimeCosts)
                    ? 'SUM(IFNULL(product.price_for_buy_with_nds,0) * order.quantity) AS amount_rub'
                    : 't.amount as amount_rub'
                ),
                new Expression('"RUB" as currency_name'),
                't.contractor_id',
                't.sale_point_id',
                't.industry_id',
                't.project_id',
                't.description',
                't.expenditure_item_id',
                't.income_item_id',
                'invoice.document_number',
                'invoice.document_additional_number',
                'invoice.id AS invoice_id',
                'invoice.has_act AS invoice_has_act',
                'invoice.has_packing_list AS invoice_has_packing_list',
                'invoice.has_upd AS invoice_has_upd',
                'invoice.need_act AS invoice_need_act',
                'invoice.need_packing_list AS invoice_need_packing_list',
                'invoice.need_upd AS invoice_need_upd',
                $cashBlock == self::CASH_BANK_BLOCK ? 't.tin_child_amount as tin_child_amount' : new Expression('null as tin_child_amount'),
            ])
            ->from(['t' => $tableName])
            ->joinWith(($this->isPrimeCosts) ? 'invoices.orders.product' : 'invoices')
            ->andWhere(['t.company_id' => $companyID])
            ->andWhere(['t.is_prepaid_expense' => 0])
            ->andWhere(['between', 't.recognition_date', $periodStart, $periodEnd])
            ->andWhere(['or',
                ['invoice.id' => null],
                ['or',
                    ['and',
                        ['not', ['invoice.id' => null]],
                        ['invoice.has_act' => 0],
                        ['invoice.has_packing_list' => 0],
                        ['invoice.has_upd' => 0],
                    ],
                    ['and',
                        ['not', ['invoice.id' => null]],
                        ['or',
                            ['invoice.need_act' => 0],
                            ['invoice.need_packing_list' => 0],
                            ['invoice.need_upd' => 0],
                        ]
                    ],
                ]
            ]);

        // pageFilters
        if (strlen($this->pageFilters['industry_id']))
            $query->andWhere(['t.industry_id' => $this->pageFilters['industry_id'] ?: null]); // 0 == without industry
        if (strlen($this->pageFilters['sale_point_id']))
            $query->andWhere(['t.sale_point_id' => $this->pageFilters['sale_point_id'] ?: null]); // 0 == without sale point
        if (strlen($this->pageFilters['project_id']))
            $query->andWhere(['t.project_id' => $this->pageFilters['project_id'] ?: null]); // 0 == without project

        if ($this->onlyNotForSaleProducts) {
            $query
                ->leftJoin('order', 'order.invoice_id = invoice.id')
                ->leftJoin('product', 'product.id = order.product_id')
                ->andWhere(['or',
                    ['product.production_type' => 0],
                    ['product.not_for_sale' => 1]
                ]);
        }

        if ($this->isPrimeCosts) {
            $primeCostProductionType = ($this->primeCostsArticle == InvoiceExpenditureItem::ITEM_PRIME_COST_PRODUCT) ? 1 : 0;
            $query->andWhere(['product.production_type' => $primeCostProductionType ?? null]);
            $query->groupBy('t.id');
            $query->having(['>', 'amount', 0]);
        }

        return $query;
    }

    private function getForeignFlowQuery($cashBlock, $companyID, $periodStart, $periodEnd)
    {
        /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
        $className = $this->getForeignFlowClassNameByType($cashBlock);
        $tableName = $className::tableName();

        $query = $className::find()
            ->select([
                't.id',
                't.company_id',
                new Expression('"' . $tableName . '" as tb'),
                "IF({{t}}.[[flow_type]] = 0, SUM({{t}}.[[amount]]), 0) as amountExpense",
                "IF({{t}}.[[flow_type]] = 1, SUM({{t}}.[[amount]]), 0) as amountIncome",
                $cashBlock == self::CASH_BANK_BLOCK ? 't.rs as rs' : new Expression('null as rs'),
                $cashBlock == self::CASH_BANK_BLOCK ? new Expression("1 AS is_accounting") : "t.is_accounting",
                't.created_at',
                't.date',
                't.recognition_date',
                't.flow_type',
                't.amount',
                't.amount_rub',
                't.currency_name',
                't.contractor_id',
                't.sale_point_id',
                't.industry_id',
                't.project_id',
                't.description',
                't.expenditure_item_id',
                't.income_item_id',
                'null AS [[document_number]]',
                'null AS [[document_additional_number]]',
                'null AS [[invoice_id]]',
                'null AS [[invoice_has_act]]',
                'null AS [[invoice_has_packing_list]]',
                'null AS [[invoice_has_upd]]',
                'null AS [[invoice_need_act]]',
                'null AS [[invoice_need_packing_list]]',
                'null AS [[invoice_need_upd]]',
                $cashBlock == self::CASH_BANK_BLOCK ? 't.tin_child_amount as tin_child_amount' : new Expression('null as tin_child_amount'),
            ])
            ->from(['t' => $tableName])
            ->andWhere(['t.company_id' => $companyID])
            ->andWhere(['between', 't.recognition_date', $periodStart, $periodEnd]);

        // pageFilters
        if (strlen($this->pageFilters['industry_id']))
            $query->andWhere(['t.industry_id' => $this->pageFilters['industry_id'] ?: null]); // 0 == without industry
        if (strlen($this->pageFilters['sale_point_id']))
            $query->andWhere(['t.sale_point_id' => $this->pageFilters['sale_point_id'] ?: null]); // 0 == without sale point
        if (strlen($this->pageFilters['project_id']))
            $query->andWhere(['t.project_id' => $this->pageFilters['project_id'] ?: null]); // 0 == without project

        if ($this->onlyNotForSaleProducts) {
            $query
                ->leftJoin('order', 'order.invoice_id = invoice.id')
                ->leftJoin('product', 'product.id = order.product_id')
                ->andWhere(['or',
                    ['product.production_type' => 0],
                    ['product.not_for_sale' => 1]
                ]);
        }

        return $query;
    }

    private function getGoodsCancellationQuery($companyID, $periodStart, $periodEnd)
    {
        $docBlock = self::GOODS_CANCELLATION_BLOCK;
        $className = $this->getDocClassNameByType($docBlock);
        $tableName = $className::tableName();

        $query = $className::find()
            ->select([
                't.id',
                't.company_id',
                new Expression('"' . $tableName . '" as tb'),
                new Expression("total_amount_with_nds amount"),
                't.created_at',
                't.document_date',
                't.document_number',
                't.document_additional_number',
                new Expression(Documents::IO_TYPE_IN . ' AS type'),
                new Expression('NULL AS invoice_document_number'),
                't.contractor_id',
                new Expression('t.expenditure_item_id AS invoice_expenditure_item_id'),
                new Expression('NULL AS invoice_income_item_id'),
                't.sale_point_id',
                't.industry_id',
                't.project_id',
            ])
            ->from(['t' => $tableName])
            ->andWhere(['t.company_id' => $companyID])
            ->andWhere(['between', 't.document_date', $periodStart, $periodEnd])
            ->andFilterWhere(['profit_loss_type' => $this->pal_type_id])
            ->groupBy('t.id');

        // pageFilters
        if (strlen($this->pageFilters['industry_id']))
            $query->andWhere(['t.industry_id' => $this->pageFilters['industry_id'] ?: null]); // 0 == without industry
        if (strlen($this->pageFilters['sale_point_id']))
            $query->andWhere(['t.sale_point_id' => $this->pageFilters['sale_point_id'] ?: null]); // 0 == without sale point
        if (strlen($this->pageFilters['project_id']))
            $query->andWhere(['t.project_id' => $this->pageFilters['project_id'] ?: null]); // 0 == without project

        return $query;
    }


    private function getDocQuery($docBlock, $companyID, $periodStart, $periodEnd)
    {
        /* @var $className PackingList|Upd|Act */
        $className = $this->getDocClassNameByType($docBlock);
        $tableName = $className::tableName();
        $orderDocTableName = $this->getDocOrderTableNameByType($docBlock);

        $attrPrice = ($this->ioType == 2) ? 'selling_price_with_vat' : 'purchase_price_with_vat';
        if ($this->isPrimeCosts) {
            $attrPrice = 'purchase_amount'; // turnover table
            $primeCostProductionType = ($this->primeCostsArticle == InvoiceExpenditureItem::ITEM_PRIME_COST_PRODUCT) ? 1 : 0;
        }

        $query = $className::find()
            ->select([
                't.id',
                'invoice.company_id',
                new Expression('"' . $tableName . '" as tb'),
                new Expression(($this->isPrimeCosts)
                    ? "SUM(turnover.{$attrPrice}) amount"
                    : "SUM(order.{$attrPrice} * orderDoc.quantity) amount"),
                't.created_at',
                't.document_date',
                't.document_number',
                't.document_additional_number',
                't.type',
                'invoice.id AS invoice_document_number',
                'invoice.contractor_id',
                'invoice.sale_point_id',
                'invoice.industry_id',
                'invoice.project_id',
                'invoice.invoice_expenditure_item_id',
                'invoice.invoice_income_item_id',
                'product.id AS product_id',
                'product.group_id AS product_group_id'
            ])
            ->from(['t' => $tableName])
            ->joinWith('invoice.orders')
            ->leftJoin(['orderDoc' => $orderDocTableName], 'order.id = orderDoc.order_id')
            ->leftJoin(['turnover' => ProductTurnover::tableName()], "turnover.order_id = orderDoc.order_id AND turnover.document_table = '{$tableName}'")
            ->leftJoin('product', 'product.id = order.product_id')
            ->andWhere(['invoice.company_id' => $companyID])
            ->andWhere(['t.status_out_id' => [NULL, 1,2,3,4]])
            ->andWhere(['invoice.invoice_status_id' => [1,2,3,4,6,7,8]])
            ->andWhere(['between', 't.document_date', $periodStart, $periodEnd])
            ->groupBy('t.id, invoice.id');

            if ($this->isPrimeCosts) {
                $query->andWhere(['turnover.production_type' => $primeCostProductionType ?? null]);
                $query->having(['>', 'amount', 0]);
            }

            // pageFilters
            if (strlen($this->pageFilters['industry_id']))
                $query->andWhere(['invoice.industry_id' => $this->pageFilters['industry_id'] ?: null]); // 0 == without industry
            if (strlen($this->pageFilters['sale_point_id']))
                $query->andWhere(['invoice.sale_point_id' => $this->pageFilters['sale_point_id'] ?: null]); // 0 == without sale point
            if (strlen($this->pageFilters['project_id']))
                $query->andWhere(['invoice.project_id' => $this->pageFilters['project_id'] ?: null]); // 0 == without project

            if ($this->onlyNotForSaleProducts) {
                $query
                    ->andWhere(['or',
                        ['product.production_type' => 0],
                        ['product.not_for_sale' => 1]
                    ]);
            }
            if ($this->product_id !== null) {
                $query->andWhere(['order.product_id' => $this->product_id ?: null]);
            }
            if ($this->product_group_id !== null) {
                $query->andWhere(['product.group_id' => $this->product_group_id ?: null]);
            }

            return $query;
    }

    private function getFlowWithoutInvoicesQuery($cashBlock, $companyID, $periodStart, $periodEnd)
    {
        /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
        $className = $this->getFlowClassNameByType($cashBlock);
        $tableName = $className::tableName();

        $query = $className::find()
            ->select([
                't.id',
                't.company_id',
                new Expression('"' . $tableName . '" as tb'),
                "IF({{t}}.[[flow_type]] = 0, SUM({{t}}.[[amount]]), 0) as amountExpense",
                "IF({{t}}.[[flow_type]] = 1, SUM({{t}}.[[amount]]), 0) as amountIncome",
                'null as [[rs]]',
                '1 AS [[is_accounting]]',
                'null AS [[created_at]]',
                't.date',
                't.recognition_date',
                't.flow_type',
                't.amount',
                new Expression('t.amount as amount_rub'),
                new Expression('"RUB" as currency_name'),
                't.contractor_id',
                't.sale_point_id',
                't.industry_id',
                't.project_id',
                't.description',
                't.expenditure_item_id',
                't.income_item_id',
                'null AS [[document_number]]',
                'null AS [[document_additional_number]]',
                'null AS [[invoice_id]]',
                'null AS [[invoice_has_act]]',
                'null AS [[invoice_has_packing_list]]',
                'null AS [[invoice_has_upd]]',
                'null AS [[invoice_need_act]]',
                'null AS [[invoice_need_packing_list]]',
                'null AS [[invoice_need_upd]]',
                'null as [[tin_child_amount]]',
            ])
            ->from(['t' => $tableName])
            ->andWhere(['t.company_id' => $companyID])
            ->andWhere(['between', 't.recognition_date', $periodStart, $periodEnd]);

        // pageFilters
        if (strlen($this->pageFilters['industry_id']))
            $query->andWhere(['t.industry_id' => $this->pageFilters['industry_id'] ?: null]); // 0 == without industry
        if (strlen($this->pageFilters['sale_point_id']))
            $query->andWhere(['t.sale_point_id' => $this->pageFilters['sale_point_id'] ?: null]); // 0 == without sale point
        if (strlen($this->pageFilters['project_id']))
            $query->andWhere(['t.project_id' => $this->pageFilters['project_id'] ?: null]); // 0 == without project

        return $query;
    }

    /// FILTERS

    /**
     * @return array
     */
    public function getContractorFilterItems()
    {
        $query = clone $this->_filterQuery;

        $tContractor = Contractor::tableName();
        $tCashContractor = CashContractorType::tableName();
        $contractorIdArray = $query
            ->distinct()
            ->select("t.contractor_id")
            ->column();
        $cashContractorArray = ArrayHelper::map(
            CashContractorType::find()
                ->andWhere(["$tCashContractor.name" => $contractorIdArray])
                ->all(),
            'name',
            'text'
        );
        $contractorArray = ArrayHelper::map(
            Contractor::getSorted()
                ->andWhere(["$tContractor.id" => $contractorIdArray])
                ->all(),
            'id',
            'shortName'
        );

        return (['' => 'Все контрагенты'] + $cashContractorArray + $contractorArray);
    }

    private static $tableItemMap = [
        'cash_bank_flows' => AbstractFinance::CASH_BANK_BLOCK,
        'cash_bank_foreign_currency_flows' => AbstractFinance::CASH_BANK_BLOCK,
        'cash_order_flows' => AbstractFinance::CASH_ORDER_BLOCK,
        'cash_order_foreign_currency_flows' => AbstractFinance::CASH_ORDER_BLOCK,
        'cash_emoney_flows' => AbstractFinance::CASH_EMONEY_BLOCK,
        'cash_emoney_foreign_currency_flows' => AbstractFinance::CASH_EMONEY_BLOCK,
        'acquiring_operation' => AbstractFinance::CASH_ACQUIRING_BLOCK,
        'card_operation' => AbstractFinance::CASH_CARD_BLOCK,
    ];
    /**
     * @return array
     */
    private function getPaymentTypeId($tableName)
    {
        return self::$tableItemMap[$tableName] ?? null;
    }
    /**
     * @return array
     */
    public function getPaymentTypeFilterItems()
    {
        $query = clone $this->_filterQuery;

        $existItems = array_map([$this, 'getPaymentTypeId'], $query->distinct()->select("t.tb")->column());

        $filter = [
            AbstractFinance::CASH_BANK_BLOCK => 'Банк',
            AbstractFinance::CASH_ORDER_BLOCK => 'Касса',
            AbstractFinance::CASH_EMONEY_BLOCK => 'E-money',
            AbstractFinance::CASH_ACQUIRING_BLOCK => 'Эквайринг',
            AbstractFinance::CASH_CARD_BLOCK => 'Карты',
        ];

        foreach ($filter as $key => $value) {
            if (!in_array($key, $existItems)) {
                unset($filter[$key]);
            }
        }

        return $filter;
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function getReasonFilterItems()
    {
        $query = clone $this->_filterQuery;
        $e_reasonArray = $query
            ->distinct()
            ->select('CONCAT("e_", `expenditure`.`id`) `reason_id`, `expenditure`.`name` `reason_name`')
            ->leftJoin(InvoiceExpenditureItem::tableName() . ' `expenditure`', '`expenditure`.`id` = `expenditure_item_id`')
            ->andWhere(['not', ['expenditure.id' => null]])
            ->createCommand()
            ->queryAll();

        $query = clone $this->_filterQuery;
        $i_reasonArray = $query
            ->distinct()
            ->select('CONCAT("i_", `income`.`id`) `reason_id`, `income`.`name` `reason_name`')
            ->leftJoin(InvoiceIncomeItem::tableName() . ' `income`', '`income`.`id` = `income_item_id`')
            ->andWhere(['not', ['income.id' => null]])
            ->createCommand()
            ->queryAll();

        $reasonArray = ArrayHelper::map(array_merge($e_reasonArray, $i_reasonArray), 'reason_id', 'reason_name');
        natsort($reasonArray);

        return $reasonArray;
    }

    /**
     *
     */
    public function checkCashContractor()
    {
        foreach ([CashContractorType::BANK_TEXT, CashContractorType::ORDER_TEXT, CashContractorType::EMONEY_TEXT] as $cashContractor) {
            if (isset($this->income_item_id[$cashContractor])) {
                $this->cash_contractor[$cashContractor] = $cashContractor;
            }
            if (isset($this->expenditure_item_id[$cashContractor])) {
                $this->cash_contractor[$cashContractor] = $cashContractor;
            }
        }
    }

    /**
     * @param $palType
     * @return array
     */
    public function getItemsByPalOptions($palType) {
        return AnalyticsArticle::find()
            ->where(['company_id' => $this->multiCompanyIds])
            ->andWhere(['profit_and_loss_type' => $palType])
            ->select('item_id')
            ->column() ?: [-1];
    }
}