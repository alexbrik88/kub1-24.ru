<?php

namespace frontend\modules\analytics\models\financeModel;

use common\components\helpers\ArrayHelper;
use Yii;
use common\models\document\InvoiceExpenditureItem;
use yii\base\Exception;

/**
 * This is the model class for table "finance_model_fixed_costs".
 *
 * @property int $shop_id
 * @property int $item_id
 * @property int $year
 * @property int $month
 * @property int|null $amount
 *
 * @property InvoiceExpenditureItem $item
 * @property FinanceModelShop $shop
 */
class FinanceModelFixedCost extends FinanceModelMonth
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_model_fixed_cost';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['shop_id', 'item_id', 'year', 'month'], 'required'],
            [['shop_id', 'item_id', 'year', 'month', 'amount'], 'integer'],
            [['shop_id', 'item_id', 'year', 'month'], 'unique', 'targetAttribute' => ['shop_id', 'item_id', 'year', 'month']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceExpenditureItem::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceModelShop::className(), 'targetAttribute' => ['shop_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'shop_id' => 'Точка продаж',
            'item_id' => 'Статья расходов',
            'year' => 'Год',
            'month' => 'Месяц',
            'amount' => 'Сумма',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(InvoiceExpenditureItem::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShop()
    {
        return $this->hasOne(FinanceModelShop::className(), ['id' => 'shop_id']);
    }

    /**
     * @param FinanceModelShop $shop
     * @param $data
     * @return bool
     * @throws Exception
     */
    public static function saveShopMonthes(FinanceModelShop $shop, $data)
    {
        $monthClass = get_called_class();
        $amountByItem = ArrayHelper::getValue($data, 'amountByItem');

        foreach ((array)$amountByItem as $itemId => $monthes)
            foreach ((array)$monthes as $ym => $amount) {
                $year = substr($ym, 0, 4);
                $month = substr($ym, 4, 2);

                /** @var FinanceModelFixedCost $model */
                $model = $monthClass::findOne([
                    'shop_id' => $shop->id,
                    'item_id' => $itemId,
                    'year' => $year,
                    'month' => $month
                ]);

                if (!$model)
                    throw new Exception("Item {$itemId} or month {$month} not found in shop {$shop->id}");

                $model->amount = round(100 * self::sanitize($amount));

                if (!$model->save())
                    return false;
            }

        return true;
    }
}
