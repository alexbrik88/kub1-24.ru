<?php

namespace frontend\modules\analytics\models\financeModel;

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use Yii;
use common\models\Company;
use common\models\employee\Employee;
use yii\base\Exception;

/**
 * This is the model class for table "finance_model".
 *
 * @property int $id
 * @property int $company_id
 * @property int $employee_id
 * @property string $name
 * @property int|null $template_id
 * @property int|null $year_from
 * @property int|null $month_from
 * @property int|null $year_till
 * @property int|null $month_till
 * @property string|null $comment
 * @property int|null $total_revenue
 * @property int|null $total_net_profit
 * @property float|null $total_marginality
 * @property float|null $total_sales_profit
 * @property float|null $total_capital_profit
 * @property int|null $total_average_check
 * @property int|null $total_margin
 * @property int $created_at
 *
 * @property Company $company
 * @property Employee $employee
 * @property FinanceModelShop[] $shops
 * @property FinanceModelOtherCost[] $otherCosts
 *
 * @property array $monthes
 * @property array $otherCostsMonthsData
 */
class FinanceModel extends \yii\db\ActiveRecord
{
    /**
     * Tabs
     */
    const TAB_ODDS = 'odds';
    const TAB_PROFIT_AND_LOSS = 'profit-and-loss';
    const TAB_ASSETS = 'assets';
    const TAB_ACTIVITIES = 'activities';
    const TAB_CAPITAL = 'capital';
    const TAB_BALANCE = 'balance';

    /**
     * Costs types
     */
    const TYPE_VARIABLE_COSTS = 1;
    const TYPE_FIXED_COSTS = 2;
    const TYPE_OTHER_COSTS = 3;

    /**
     * @var
     */
    public $textFrom;

    /**
     * @var
     */
    public $textTill;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_model';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'employee_id', 'name'], 'required'],
            [['company_id', 'employee_id', 'template_id', 'year_from', 'month_from', 'year_till', 'month_till', 'total_revenue', 'total_net_profit', 'total_average_check', 'total_margin'], 'integer'],
            [['comment'], 'string'],
            [['total_marginality', 'total_sales_profit', 'total_capital_profit'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
            [['dateFrom', 'dateTill'], 'date', 'format' => 'php:Y-m-d'],
            [['textFrom', 'textTill'], 'safe'],
            [['textTill'], function ($attribute, $params) {
                    if ((new \DateTime($this->dateTill)) < (new \DateTime($this->dateFrom))) {
                        $this->addError('textTill', 'Значение «Месяц окончания» не должно быть меньше значения «Месяц начала».');
                    }
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'employee_id' => 'Ответственный сотрудник',
            'name' => 'Название',
            'template_id' => 'Шаблон финансовой модели',
            'year_from' => 'Год начала',
            'month_from' => 'Месяц начала',
            'year_till' => 'Год окончания',
            'month_till' => 'Месяц окончания',
            'comment' => 'Комментарий',
            'total_revenue' => 'Выручка',
            'total_net_profit' => 'Чистая прибыль',
            'total_marginality' => 'Маржинальность',
            'total_sales_profit' => 'Рентабельность продаж',
            'total_capital_profit' => 'Рентабельность капитала',
            'total_average_check' => 'Средний чек',
            'total_margin' => 'Маржинальный доход'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShops()
    {
        return $this->hasMany(FinanceModelShop::className(), ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOtherCosts()
    {
        return $this->hasMany(FinanceModelOtherCost::className(), ['model_id' => 'id']);
    }

    /**
     * @return string|null
     */
    public function getDateFrom()
    {
        if ($date = date_create_from_format('Y-m-d', $this->year_from.'-'.$this->month_from.'-01')) {
            return $date->format('Y-m-d');
        }

        return null;
    }

    /**
     * @return string|null
     */
    public function getDateTill()
    {
        if ($date = date_create_from_format('Y-m-d', $this->year_till.'-'.$this->month_till.'-01')) {
            return $date->format('Y-m-t');
        }

        return null;
    }

    public function setDateFrom($value)
    {
        if ($date = date_create_from_format('Y-m-d', $value)) {
            $this->year_from = $date->format('Y');
            $this->month_from = $date->format('n');
        }
    }

    public function setDateTill($value)
    {
        if ($date = date_create_from_format('Y-m-d', $value)) {
            $this->year_till = $date->format('Y');
            $this->month_till = $date->format('n');
        }
    }

    public function getPeriod()
    {
        $dateFrom = date_create_from_format('Y-m-d', $this->year_from.'-'.$this->month_from.'-01');
        $dateTill = date_create_from_format('Y-m-d', $this->year_till.'-'.$this->month_till.'-01');
        if ($dateFrom && $dateTill) {
            $diff = date_diff($dateFrom, $dateTill);
            return 1 + $diff->format('%y') * 12 + $diff->format('%m');
        }

        return 0;
    }

    /*
     * HELPERS
     */

    public function getMonthes()
    {
        $ret = [];
        for ($y = $this->year_from; $y <= $this->year_till; $y++) {
            for ($m = 1; $m <= 12; $m++) {

                if ($y == $this->year_from && $m < $this->month_from) continue;
                if ($y == $this->year_till && $m > $this->month_till) break;

                $monthNum = str_pad($m, 2, "0", STR_PAD_LEFT);
                $ret[] = $y . $monthNum;
            }
        }

        return $ret;
    }

    public function getQuarters()
    {
        $ret = [];
        for ($y = $this->year_from; $y <= $this->year_till; $y++) {
            for ($m = 1; $m <= 12; $m++) {

                if ($y == $this->year_from && $m < $this->month_from) continue;
                if ($y == $this->year_till && $m > $this->month_till) break;

                $quarterNum = str_pad((int)ceil($m / 3), 2, "0", STR_PAD_LEFT);
                $monthNum = str_pad($m, 2, "0", STR_PAD_LEFT);

                if (!isset($ret[$y . $quarterNum]))
                    $ret[$y . $quarterNum] = [];

                $ret[$y . $quarterNum][] = $y . $monthNum;
            }
        }

        return $ret;
    }

    /**
     * @param $tab
     * @return string
     */
    public static function getTabTitle($tab) 
    {
        switch ($tab) {
            case FinanceModel::TAB_ODDS:
                return "Отчёт о Движении Денежных Средств (Финмодель)";
            case FinanceModel::TAB_ASSETS:
                return "Основные средства (Финмодель)";
            case FinanceModel::TAB_ACTIVITIES:
                return "Финансовая деятельность (Финмодель)";
            case FinanceModel::TAB_CAPITAL:
                return "Оборотный капитал (Финмодель)";
            case FinanceModel::TAB_BALANCE:
                return "Баланс (Финмодель)";
            case FinanceModel::TAB_PROFIT_AND_LOSS:
            default:
                return "Отчет о Прибылях и Убытках (Финмодель)";
        }
    }

    /**
     * @param FinanceModelShop $model
     * @param FinanceModelShop $copiedModel
     */
    public static function fillShopByShop(FinanceModelShop $model, FinanceModelShop $copiedModel)
    {
        $model->type = $copiedModel->type;
        $copiedMonths = [];
        foreach ($copiedModel->revenueMonths as $m) {
            $copiedMonths[] = new FinanceModelRevenue([
                'year' => $m->year,
                'month' => $m->month,
                'check_count' => $m->check_count,
                'visit_count' => $m->visit_count,
                'average_check' => $m->average_check,
                'conversion' => $m->conversion
            ]);
            $model->populateRelation('revenueMonths', $copiedMonths);
        }
    }

    /**
     * @return array
     */
    public function getOtherCostsMonthsData()
    {
        $ret = [];
        foreach ($this->otherCosts as $m) {
            $year = $m->year;
            $month = str_pad($m->month, 2, "0", STR_PAD_LEFT);
            foreach (array_keys($m->attributeLabels()) as $attr) {
                if (in_array($attr, ['model_id', 'year', 'month']))
                    continue;

                $ret[$attr][$year . $month] = $m->{$attr};
            }
        }

        return $ret;
    }
}
