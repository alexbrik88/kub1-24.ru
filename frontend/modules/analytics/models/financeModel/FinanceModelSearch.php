<?php

namespace frontend\modules\analytics\models\financeModel;

use common\components\helpers\ArrayHelper;
use frontend\modules\analytics\models\financeModel\FinanceModel;
use Yii;
use common\models\employee\Employee;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * FinanceModelSearch
 */
class FinanceModelSearch extends FinanceModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'employee_id'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        /* @var $employee Employee */
        $employee = Yii::$app->user->identity;
        $query = FinanceModel::find()->andWhere(['company_id' => $employee->company_id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'periodFrom' => [
                        'asc' => [
                            'year_from' => SORT_ASC,
                            'month_from' => SORT_ASC,
                        ],
                        'desc' => [
                            'year_from' => SORT_DESC,
                            'month_from' => SORT_DESC,
                        ],
                    ],
                    'periodTill' => [
                        'asc' => [
                            'year_till' => SORT_ASC,
                            'month_till' => SORT_ASC,
                        ],
                        'desc' => [
                            'year_till' => SORT_DESC,
                            'month_till' => SORT_DESC,
                        ],
                    ],
                    'created_at',
                    'total_revenue',
                    'total_net_profit',
                    'total_marginality',
                    'total_sales_profit',
                    'total_capital_profit',
                ],
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getEmployeeFilter()
    {
        /* @var $employee Employee */
        $employee = Yii::$app->user->identity;
        $query = FinanceModel::find()->andWhere(['company_id' => $employee->company_id]);

        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map(self::find()
            ->andWhere(['company_id' => $employee->company_id])
            ->all(), 'employee_id', 'employee.shortFio'));
    }
}
