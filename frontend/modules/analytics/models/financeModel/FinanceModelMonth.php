<?php

namespace frontend\modules\analytics\models\financeModel;

use common\components\helpers\ArrayHelper;
use common\models\document\InvoiceExpenditureItem;
use yii\base\Exception;

class FinanceModelMonth extends \yii\db\ActiveRecord {

    /**
     * @param $val
     * @return float
     */
    public static function sanitize($val)
    {
        return (float)str_replace([',',' '], ['.',''], $val);
    }

    /**
     * @param FinanceModel $model
     * @param $toShops
     * @param $addItems
     * @return bool
     */
    public static function addExpenditureItems(FinanceModel $model, $toShops, $addItems)
    {
        $monthClass = get_called_class();

        foreach ($model->shops as $shop) {

            if (!in_array($shop->id, $toShops) && $toShops !== ['all'])
                continue;

            $presentItemsIds = $monthClass::find()->where(['shop_id' => $shop->id])->select('item_id')->distinct()->column();

            foreach ($addItems as $itemId) {

                if (in_array($itemId, $presentItemsIds))
                    continue;

                if (!$item = InvoiceExpenditureItem::find()->where(['id' => $itemId])
                    ->andWhere(['or', ['company_id' => $model->company_id], ['company_id' => null]])->exists())
                        continue;

                foreach ($model->monthes as $ym) {
                    $year = substr($ym, 0, 4);
                    $month = substr($ym, 4, 2);
                    $m = new $monthClass([
                        'shop_id' => $shop->id,
                        'item_id' => $itemId,
                        'year' => (int)$year,
                        'month' => (int)$month
                    ]);

                    if (!$m->save())
                        return false;
                }
            }
        }

        return true;
    }

    /**
     * @param $model
     * @return mixed
     * @throws Exception
     */
    public static function updateMonthsRange($model)
    {
        $monthClass = get_called_class();
        switch ($monthClass) {
            case FinanceModelRevenue::class:
                $monthMethod = '__updateShopMonthsRange';
                $monthArray = 'revenueMonths';
                break;
            case FinanceModelOtherCost::class:
                $monthMethod = '__updateMonthsRange';
                $monthArray = 'otherCosts';
                break;
            case FinanceModelFixedCost::class:
                $monthMethod = '__updateShopMonthsRangeByItem';
                $monthArray = 'fixedCostMonths';
                break;
            case FinanceModelVariableCost::class:
                $monthMethod = '__updateShopMonthsRangeByItem';
                $monthArray = 'variableCostMonths';
                break;
            default:
                throw new Exception('$monthClass not found');
        }

        return call_user_func(['self', $monthMethod], $model, $monthClass, $monthArray);
    }

    /**
     * @param $model
     * @param $monthClass
     * @param $monthArray
     * @return bool
     * @throws Exception
     */
    protected function __updateShopMonthsRangeByItem(FinanceModel $model, $monthClass, $monthArray)
    {
        // delete old
        $limitLeft = $model->year_from . str_pad($model->month_from, 2, "0", STR_PAD_LEFT);
        $limitRight = $model->year_till . str_pad($model->month_till, 2, "0", STR_PAD_LEFT);
        foreach ($model->shops as $shop) {
            foreach ($shop->{$monthArray} as $m) {
                $year = $m->year;
                $month = str_pad($m->month, 2, "0", STR_PAD_LEFT);
                if ($year . $month < $limitLeft || $year . $month > $limitRight) {
                    if (!$m->delete())
                        return false;
                }
            }
        }

        // add new
        foreach ($model->shops as $shop) {
            $items = $monthClass::find()->where(['shop_id' => $shop->id])->select('item_id')->distinct()->column();
            foreach ($items as $itemId) {
                foreach ($model->getMonthes() as $m) {
                    $year = substr($m, 0, 4);
                    $month = substr($m, 4, 2);
                    if (!$monthClass::find()->where([
                        'shop_id' => $shop->id,
                        'item_id' => $itemId,
                        'year' => (int)$year,
                        'month' => (int)$month
                    ])->exists()) {

                        $m = new $monthClass([
                            'shop_id' => $shop->id,
                            'item_id' => $itemId,
                            'year' => (int)$year,
                            'month' => (int)$month
                        ]);

                        if (!$m->save())
                            return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * @param $model
     * @param $monthClass
     * @param $monthArray
     * @return bool
     * @throws Exception
     */
    protected function __updateShopMonthsRange(FinanceModel $model, $monthClass, $monthArray)
    {
        // delete old
        $limitLeft = $model->year_from . str_pad($model->month_from, 2, "0", STR_PAD_LEFT);
        $limitRight = $model->year_till . str_pad($model->month_till, 2, "0", STR_PAD_LEFT);
        foreach ($model->shops as $shop) {
            foreach ($shop->{$monthArray} as $m) {
                $year = $m->year;
                $month = str_pad($m->month, 2, "0", STR_PAD_LEFT);
                if ($year . $month < $limitLeft || $year . $month > $limitRight) {
                    if (!$m->delete())
                        return false;
                }
            }
        }

        // add new
        foreach ($model->shops as $shop) {
            foreach ($model->getMonthes() as $m) {
                $year = substr($m, 0, 4);
                $month = substr($m, 4, 2);
                if (!$monthClass::find()->where([
                    'shop_id' => $shop->id,
                    'year' => (int)$year,
                    'month' => (int)$month
                ])->exists()) {

                    $m = new $monthClass([
                        'shop_id' => $shop->id,
                        'year' => (int)$year,
                        'month' => (int)$month
                    ]);

                    if (!$m->save())
                        return false;
                }
            }
        }

        return true;
    }

    /**
     * @param $model
     * @param $monthClass
     * @param $monthArray
     * @return bool
     * @throws Exception
     */
    protected function __updateMonthsRange(FinanceModel $model, $monthClass, $monthArray)
    {
        // delete old
        $limitLeft = $model->year_from . str_pad($model->month_from, 2, "0", STR_PAD_LEFT);
        $limitRight = $model->year_till . str_pad($model->month_till, 2, "0", STR_PAD_LEFT);

        foreach ($model->{$monthArray} as $m) {
            $year = $m->year;
            $month = str_pad($m->month, 2, "0", STR_PAD_LEFT);
            if ($year . $month < $limitLeft || $year . $month > $limitRight) {
                if (!$m->delete())
                    return false;
            }
        }

        // add new
        foreach ($model->getMonthes() as $m) {
            $year = substr($m, 0, 4);
            $month = substr($m, 4, 2);
            if (!$monthClass::find()->where([
                'model_id' => $model->id,
                'year' => (int)$year,
                'month' => (int)$month
            ])->exists()) {

                $m = new $monthClass([
                    'model_id' => $model->id,
                    'year' => (int)$year,
                    'month' => (int)$month
                ]);

                if (!$m->save())
                    return false;
            }
        }

        return true;
    }
}