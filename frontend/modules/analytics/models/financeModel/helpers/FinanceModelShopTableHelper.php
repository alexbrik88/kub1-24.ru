<?php

namespace frontend\modules\analytics\models\financeModel\helpers;

use yii\helpers\Url;
use common\components\helpers\ArrayHelper;
use frontend\modules\analytics\models\financeModel\FinanceModel;
use frontend\modules\analytics\models\financeModel\FinanceModelOtherCost;
use frontend\modules\analytics\models\financeModel\FinanceModelRevenue;
use frontend\modules\analytics\models\financeModel\FinanceModelShop;
use frontend\modules\analytics\models\financeModel\helpers\FinanceModelCalcHelper as calc;

/**
 * Class FinanceModelTableHelper
 * @package frontend\modules\analytics\models\financeModel\helpers
 */
class FinanceModelShopTableHelper
{
    /**
     * @param FinanceModel $model
     * @param FinanceModelShop $shop
     * @return array
     */
    public static function getShopCosts(FinanceModel $model, FinanceModelShop $shop)
    {
        // structure
        $modelData = [
            calc::KEY_VARIABLE_COSTS => [
                'totalBlock' => true,
                'title' => 'ПЕРЕМЕННЫЕ РАСХОДЫ, ₽',
                'editUrl' => Url::to(['update-shop-costs', 'model_id' => $model->id, 'shop_id' => $shop->id, 'type' => FinanceModel::TYPE_VARIABLE_COSTS]),
                'dataPercent' => array_sum($shop->variableCostMonthsPercentData),
                'data' => [],
            ],
            calc::KEY_MARGIN => [
                'totalBlock' => true,
                'title' => 'МАРЖИНАЛЬНЫЙ ДОХОД, ₽',
                'data' => [],
            ],
            calc::KEY_MARGINALITY => [
                'totalBlock' => true,
                'title' => 'МАРЖИНАЛЬНОСТЬ, %',
                'units' => '%',
                'data' => [],
            ],
            calc::KEY_FIXED_COSTS => [
                'totalBlock' => true,
                'title' => 'ПОСТОЯННЫЕ РАСХОДЫ, ₽',
                'editUrl' => Url::to(['update-shop-costs', 'model_id' => $model->id, 'shop_id' => $shop->id, 'type' => FinanceModel::TYPE_FIXED_COSTS]),
                'data' => [],
            ],
            calc::KEY_OPERATING_PROFIT => [
                'totalBlock' => true,
                'title' => 'ОПЕРАЦИОННАЯ ПРИБЫЛЬ, ₽',
                'data' => [],
            ],
            calc::KEY_OTHER_INCOME => [
                'totalBlock' => true,
                'editAttrJS' => calc::KEY_OTHER_INCOME,
                'title' => 'Другие доходы, ₽',
                'data' => [],
            ],
            calc::KEY_OTHER_EXPENSE => [
                'totalBlock' => true,
                'editAttrJS' => calc::KEY_OTHER_EXPENSE,
                'title' => 'Другие расходы, ₽',
                'data' => [],
            ],
            calc::KEY_EBIDTA => [
                'totalBlock' => true,
                'title' => 'EBIDTA, ₽',
                'data' => [],
            ],
            calc::KEY_OTHER_RECEIVED_PERCENTS => [
                'totalBlock' => true,
                'editAttrJS' => calc::KEY_OTHER_RECEIVED_PERCENTS,
                'title' => 'Проценты полученные, ₽',
                'data' => [],
            ],
            calc::KEY_OTHER_PAID_PERCENTS => [
                'totalBlock' => true,
                'editAttrJS' => calc::KEY_OTHER_PAID_PERCENTS,
                'title' => 'Проценты уплаченные, ₽',
                'data' => [],
            ],
            calc::KEY_OTHER_AMORTIZATION => [
                'totalBlock' => true,
                'editAttrJS' => calc::KEY_OTHER_AMORTIZATION,
                'title' => 'Амортизация, ₽',
                'data' => [],
            ],
            calc::KEY_PROFIT_BEFORE_TAX => [
                'totalBlock' => true,
                'title' => 'ПРИБЫЛЬ ДО НАЛОГООБЛОЖЕНИЯ, ₽',
                'data' => [],
            ],
            calc::KEY_OTHER_TAX_RATE => [
                'totalBlock' => true,
                'editAttrJS' => calc::KEY_OTHER_TAX_RATE,
                'title' => 'Налог на прибыль, %',
                'units' => '%',
                'data' => [],
            ],
            calc::KEY_NET_PROFIT => [
                'totalBlock' => true,
                'title' => 'ЧИСТАЯ ПРИБЫЛЬ, ₽',
                'data' => [],
            ],
            calc::KEY_OTHER_PROFIT_DISTRIBUTION => [
                'totalBlock' => true,
                'editAttrJS' => calc::KEY_OTHER_PROFIT_DISTRIBUTION,
                'title' => 'Распределение прибыли, ₽',
                'data' => [],
            ],
            calc::KEY_UNDISTRIBUTED_PROFIT => [
                'totalBlock' => true,
                'title' => 'НЕРАСПРЕДЕЛЕННАЯ ПРИБЫЛЬ, ₽',
                'data' => [],
            ],
        ];

        // calculation
        calc::init($model, $shop);

        foreach ($modelData as $getter1 => &$level1)
        {
            if (empty($level1['data']))
                $level1['data'] = calc::calculateRow($getter1);

            if (!empty($level1['levels'])) {
                foreach ($level1['levels'] as $getter2 => &$level2) {
                    if (empty($level2['data']))
                        $level2['data'] = calc::calculateRow($getter2, $getter1);
                }
            }
        }

        self::_addAvgColumn($modelData);

        return $modelData;
    }

    /**
     * @param FinanceModel $model
     * @param FinanceModelShop $shop
     * @return array
     */
    public static function getShopRevenue(FinanceModel $model, FinanceModelShop $shop)
    {
        $modelData = self::_getEmptyShop($shop->type);
        foreach ($shop->revenueMonthsData as $attr => $data) {
           if ($attr == 'average_check')
               $data = array_map(function ($amount) { return $amount / 100; }, $data);

           $modelData[$attr]['data'] = $data;
        }

        return $modelData;
    }

    /**
     * @param $modelData
     */
    protected static function _addAvgColumn(&$modelData)
    {
        // add avg column
        foreach ($modelData as $key => $value) {
            $modelData[$key]['avg'] = (!empty($value['data'])) ?
                array_sum($value['data']) / count($value['data']) : 0;

            if (!empty($value['levels']))
                foreach ($value['levels'] as $k => $v)
                    $modelData[$key]['levels'][$k]['avg'] = (!empty($v['data'])) ?
                        array_sum($v['data']) / count($v['data']) : 0;

        }
    }

    /**
     * @param $type
     * @return array
     */
    protected static function _getEmptyShop($type)
    {
        if ($type == FinanceModelShop::TYPE_SHOP) {
            return [
                'check_count' => [
                    'title' => 'Количество чеков, шт',
                    'isInt' => true,
                    'data' => [],
                ],
                'average_check' => [
                    'title' => 'Средний чек, ₽',
                    'data' => [],
                ],
                'revenue' => [
                    'title' => 'Выручка, ₽',
                    'isBold' => true,
                    'calcJS' => true,
                    'data' => []
                ]
            ];
        }

        if ($type == FinanceModelShop::TYPE_INTERNET_SHOP) {
            return [
                'visit_count' => [
                    'title' => 'Трафик (заходы на сайт), шт',
                    'isInt' => true,
                    'data' => [],
                ],
                'conversion' => [
                    'title' => 'Конверсия в покупку, %',
                    'units' => '%',
                    'data' => [],
                ],
                'check_count' => [
                    'title' => 'Количество покупок, шт',
                    'calcJS' => true,
                    'data' => [],
                ],
                'average_check' => [
                    'title' => 'Средний чек, ₽',
                    'data' => [],
                ],
                'revenue' => [
                    'title' => 'Выручка, ₽',
                    'isBold' => true,
                    'calcJS' => true,
                    'data' => []
                ]
            ];
        }

        return [];
    }
}