<?php

namespace frontend\modules\analytics\models\financeModel\helpers;

use common\components\helpers\ArrayHelper;
use frontend\modules\analytics\models\financeModel\FinanceModel;
use frontend\modules\analytics\models\financeModel\FinanceModelShop;

class FinanceModelCalcHelper {

    // level 1
    const KEY_TOTAL_COSTS = "total_costs";
    const KEY_FIXED_COSTS = "total_costs_fixed";
    const KEY_VARIABLE_COSTS = "total_costs_variable";
    const KEY_OPERATING_PROFIT = "total_operating_profit";
    const KEY_EBIDTA = "total_ebidta";
    const KEY_PROFIT_BEFORE_TAX = "total_profit_before_tax";
    const KEY_NET_PROFIT = "total_net_profit";
    const KEY_UNDISTRIBUTED_PROFIT = "total_undistributed_profit";
    const KEY_OTHER_INCOME = 'income';
    const KEY_OTHER_EXPENSE = 'expense';
    const KEY_OTHER_PAID_PERCENTS = 'paid_percents';
    const KEY_OTHER_RECEIVED_PERCENTS = 'received_percents';
    const KEY_OTHER_AMORTIZATION = 'amortization';
    const KEY_OTHER_TAX_RATE = 'tax_rate';
    const KEY_OTHER_PROFIT_DISTRIBUTION = 'profit_distribution';
    const KEY_MARGIN = 'margin';
    const KEY_MARGINALITY = 'marginality';

    // level 2
    const KEY_SHOP = 'shop';

    // ancillary data
    public static $monthes = [];
    public static $shops = [];
    public static $revenueByShop = [];
    public static $fixedCostsByShop = [];
    public static $variableCostsByShop = [];
    public static $marginByShop = [];
    public static $marginalityByShop = [];
    public static $operatingProfitByShop = [];
    public static $otherCostsByAttr = [];
    public static $ebidta = [];
    public static $profitBeforeTax = [];
    public static $netProfit = [];
    public static $undistributedProfit = [];

    /**
     * @param FinanceModel $model
     * @param FinanceModelShop $shop
     */
    public static function init(FinanceModel $model, FinanceModelShop $shop = null)
    {
        self::$monthes = $model->monthes;
        self::$shops = ($shop) ?
            [$shop] :
            $model->getShops()->orderBy(['type' => SORT_DESC, 'name' => SORT_ASC])->all();

        $i = 0;
        foreach (self::$shops as $shop) {

            self::$revenueByShop[$shop->id] = array_map(function($a) { return $a / 100; }, $shop->revenueMonthsAmountData);
            self::$fixedCostsByShop[$shop->id] = array_map(function($a) { return $a / 100; }, $shop->fixedCostMonthsTotal);
            self::$variableCostsByShop[$shop->id] = array_map(function($a) { return $a / 100; }, $shop->variableCostMonthsTotal);

            foreach ($model->otherCostsMonthsData as $attr => $data) {
                self::$otherCostsByAttr[$attr] = (in_array($attr, [self::KEY_OTHER_TAX_RATE])) ?
                    $data :
                    array_map(function ($amount) { return $amount / 100; }, $data);
            }

            foreach (self::$monthes as $k) {
                $_revenue = ArrayHelper::getValue(self::$revenueByShop[$shop->id], $k, 0);
                $_variable = ArrayHelper::getValue(self::$variableCostsByShop[$shop->id], $k, 0);
                $_fixed = ArrayHelper::getValue(self::$fixedCostsByShop[$shop->id], $k, 0);
                $_other_income = self::$otherCostsByAttr[self::KEY_OTHER_INCOME][$k];
                $_other_expense = self::$otherCostsByAttr[self::KEY_OTHER_EXPENSE][$k];
                $_received_percents = self::$otherCostsByAttr[self::KEY_OTHER_RECEIVED_PERCENTS][$k];
                $_paid_percents = self::$otherCostsByAttr[self::KEY_OTHER_PAID_PERCENTS][$k];
                $_amortization = self::$otherCostsByAttr[self::KEY_OTHER_AMORTIZATION][$k];
                $_tax_rate = self::$otherCostsByAttr[self::KEY_OTHER_TAX_RATE][$k];
                $_profit_distribution = self::$otherCostsByAttr[self::KEY_OTHER_PROFIT_DISTRIBUTION][$k];
                $_margin = ($_revenue - $_variable);
                $_marginality = $_margin / ($_revenue <> 0 ? $_revenue : 1);
                $_operating_profit = $_margin - $_fixed;
                $_ebidta = $_operating_profit + ((0 == $i) ? ($_other_income - $_other_expense) : 0);
                $_profit_before_tax = $_ebidta + ((0 == $i) ? ($_received_percents - $_paid_percents - $_amortization) : 0);
                $_net_profit = $_profit_before_tax * (1 - $_tax_rate / 100);
                $_undistributed_profit = $_net_profit - ((0 == $i) ? $_profit_distribution : 0);

                self::sum(self::$marginByShop[$shop->id], $k, $_margin);
                self::sum(self::$marginalityByShop[$shop->id], $k, $_marginality * 100);
                self::sum(self::$operatingProfitByShop[$shop->id], $k, $_operating_profit);
                self::sum(self::$ebidta, $k, $_ebidta);
                self::sum(self::$profitBeforeTax, $k, $_profit_before_tax);
                self::sum(self::$netProfit, $k, $_net_profit);
                self::sum(self::$undistributedProfit, $k, $_undistributed_profit);
            }

            $i++;
        }
    }

    /**
     * @param $getterName
     * @param $parentGetterName
     * @return array|mixed
     */
    public static function calculateRow($getterName, $parentGetterName = null)
    {
        $getterMethod = 'get' . self::camelize($getterName);

        if (method_exists(__CLASS__, $getterMethod)) {
            return call_user_func(['self', $getterMethod], $parentGetterName);
        } elseif (is_int($getterName) && in_array($getterName, ArrayHelper::getColumn(self::$shops, 'id')))
        {
            return call_user_func(['self', 'getShop'], $getterName, $parentGetterName);
        }

        return [];
    }

    /**
     * @return array
     */
    protected static function getTotalCosts()
    {
        return [];
    }

    /**
     * @return array
     */
    protected static function getTotalCostsVariable()
    {
        $tmp = [];
        foreach (self::$variableCostsByShop as $shopId => $data) {
            foreach ($data as $k => $v) {
                self::sum($tmp, $k, $v);
            }
        }

        return $tmp;
    }

    /**
     * @return array
     */
    protected static function getTotalCostsFixed()
    {
        $tmp = [];
        foreach (self::$fixedCostsByShop as $shopId => $data) {
            foreach ($data as $k => $v) {
                self::sum($tmp, $k, $v);
            }
        }

        return $tmp;
    }

    protected static function getMargin()
    {
        $tmp = [];
        foreach (self::$marginByShop as $shopId => $data) {
            foreach ($data as $k => $v) {
                self::sum($tmp, $k, $v);
            }
        }

        return $tmp;
    }

    protected static function getMarginality()
    {
        $tmp = [];
        $avgKoef = count(self::$marginalityByShop);
        foreach (self::$marginalityByShop as $shopId => $data) {
            foreach ($data as $k => $v) {

                if (!isset($tmp[$k]))
                    $tmp[$k] = 0;

                $tmp[$k] += $v / $avgKoef;

            }
        }

        return $tmp;
    }

    protected static function getTotalOperatingProfit()
    {
        $tmp = [];
        foreach (self::$operatingProfitByShop as $shopId => $data) {
            foreach ($data as $k => $v) {
                self::sum($tmp, $k, $v);
            }
        }

        return $tmp;
    }

    protected static function getIncome()
    {
        return self::$otherCostsByAttr[self::KEY_OTHER_INCOME];
    }

    protected static function getExpense()
    {
        return self::$otherCostsByAttr[self::KEY_OTHER_EXPENSE];
    }
    
    protected static function getTotalEbidta()
    {
        return self::$ebidta;
    }

    protected static function getPaidPercents()
    {
        return self::$otherCostsByAttr[self::KEY_OTHER_PAID_PERCENTS];
    }

    protected static function getReceivedPercents()
    {
        return self::$otherCostsByAttr[self::KEY_OTHER_RECEIVED_PERCENTS];
    }

    protected static function getAmortization()
    {
        return self::$otherCostsByAttr[self::KEY_OTHER_AMORTIZATION];
    }

    protected static function getTotalProfitBeforeTax()
    {
        return self::$profitBeforeTax;
    }

    protected static function getTaxRate()
    {
        return self::$otherCostsByAttr[self::KEY_OTHER_TAX_RATE];
    }

    protected static function getTotalNetProfit()
    {
        return self::$netProfit;
    }

    protected static function getProfitDistribution()
    {
        return self::$otherCostsByAttr[self::KEY_OTHER_PROFIT_DISTRIBUTION];
    }

    protected static function getTotalUndistributedProfit()
    {
        return self::$undistributedProfit;
    }

    // Level 2

    /**
     * @param $shopId
     * @param $parent
     * @return mixed
     */
    protected static function getShop($shopId, $parent)
    {
        $tmp = [];
        switch ($parent) {
            case self::KEY_FIXED_COSTS:
                $shopData = self::$fixedCostsByShop[$shopId];
                break;
            case self::KEY_VARIABLE_COSTS:
                $shopData = self::$variableCostsByShop[$shopId];
                break;
            case self::KEY_MARGIN:
                $shopData = self::$marginByShop[$shopId];
                break;
            case self::KEY_MARGINALITY:
                $shopData = self::$marginalityByShop[$shopId];
                break;
            case self::KEY_OPERATING_PROFIT:
                $shopData = self::$operatingProfitByShop[$shopId];
                break;
            default:
                $shopData = [];
                break;
        }

        foreach ($shopData as $k => $v) {
            self::sum($tmp, $k, $v);
        }

        return $tmp;
    }

    protected static function sum(&$array, $key, $value)
    {
        if (!isset($array[$key]))
            $array[$key] = 0;

        $array[$key] += $value;
    }

    /**
     * @param $input
     * @param string $separator
     * @return mixed
     */
    protected static function camelize($input, $separator = '_')
    {
        return str_replace($separator, '', ucwords($input, $separator));
    }

    /**
     * Recalc finance model totals
     * @param $model
     * @return bool
     */
    public static function recalcTotals(FinanceModel $model)
    {
        $model->refresh();
        self::init($model);

        if (empty($model->shops)) {

            $model->total_revenue = 0;
            $model->total_net_profit = 0;
            $model->total_marginality = 0;
            $model->total_sales_profit = 0;
            $model->total_capital_profit = 0;
            $model->total_average_check = 0;
            $model->total_margin = 0;

        } else {

            $_fnTotal = function ($carry, $item) { return $carry += array_sum($item); };
            $_operatingProfit = 100 * array_reduce(self::$operatingProfitByShop, $_fnTotal, 0);

            $model->total_revenue = round(100 * array_reduce(self::$revenueByShop, $_fnTotal, 0));
            $model->total_net_profit = round(100 * array_sum(self::$netProfit));
            $model->total_sales_profit = ($model->total_revenue != 0) ? (100 * $_operatingProfit / $model->total_revenue) : 0;
            $model->total_capital_profit = ($model->total_revenue != 0) ? (100 * $model->total_net_profit / $model->total_revenue) : 0;
            $model->total_margin = round(100 * array_reduce(self::$marginByShop, $_fnTotal, 0));

            $tmp = [];
            foreach ($model->shops as $shop)
            {
                foreach ($shop->revenueMonthsData as $attr => $data) {
                    if ($attr == 'average_check') {
                        $tmp[] = array_sum($data) / count($data);
                    }
                }
            }
            $model->total_average_check = round(array_sum($tmp) / count($tmp));

            $m = $r = [];
            foreach (self::$marginByShop as $data) $m[] = array_sum($data);
            foreach (self::$revenueByShop as $data) $r[] = array_sum($data);
            if (array_sum($r) > 0) {
                $model->total_marginality = 100 * array_sum($m) / array_sum($r);
            }
        }

        return $model->save();
    }
}