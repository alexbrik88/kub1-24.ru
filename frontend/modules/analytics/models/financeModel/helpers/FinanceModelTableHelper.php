<?php

namespace frontend\modules\analytics\models\financeModel\helpers;

use common\components\helpers\ArrayHelper;
use common\models\document\InvoiceExpenditureItem;
use frontend\modules\analytics\models\financeModel\FinanceModel;
use frontend\modules\analytics\models\financeModel\FinanceModelOtherCost;
use frontend\modules\analytics\models\financeModel\FinanceModelRevenue;
use frontend\modules\analytics\models\financeModel\FinanceModelShop;
use frontend\modules\analytics\models\financeModel\helpers\FinanceModelCalcHelper as calc;
use yii\helpers\Url;

/**
 * Class FinanceModelTableHelper
 * @package frontend\modules\analytics\models\financeModel\helpers
 */
class FinanceModelTableHelper
{
    /**
     * @param FinanceModel $model
     * @return array
     */
    public static function getCosts(FinanceModel $model)
    {
        // structure
        $modelData = [
            calc::KEY_TOTAL_COSTS => [
                'totalBlock' => true,
                'title' => 'РАСХОДЫ ПЛАН, ₽',
                'data' => [],
            ],
            calc::KEY_VARIABLE_COSTS => [
                'totalBlock' => true,
                'title' => 'ПЕРЕМЕННЫЕ РАСХОДЫ, ₽',
                'isButtonable' => true,
                'data' => [],
                'levels' => self::_getEmptyShops($model, FinanceModel::TYPE_VARIABLE_COSTS),
            ],
            calc::KEY_MARGIN => [
                'totalBlock' => true,
                'title' => 'МАРЖИНАЛЬНЫЙ ДОХОД, ₽',
                'isButtonable' => true,
                'data' => [],
                'levels' => self::_getEmptyShops($model),
            ],
            calc::KEY_MARGINALITY => [
                'totalBlock' => true,
                'title' => 'МАРЖИНАЛЬНОСТЬ, %',
                'isButtonable' => true,
                'units' => '%',
                'data' => [],
                'levels' => self::_getEmptyShops($model, null, '%'),
            ],
            calc::KEY_FIXED_COSTS => [
                'totalBlock' => true,
                'title' => 'ПОСТОЯННЫЕ РАСХОДЫ, ₽',
                'isButtonable' => true,
                'data' => [],
                'levels' => self::_getEmptyShops($model, FinanceModel::TYPE_FIXED_COSTS),
            ],
            calc::KEY_OPERATING_PROFIT => [
                'totalBlock' => true,
                'isButtonable' => true,
                'title' => 'ОПЕРАЦИОННАЯ ПРИБЫЛЬ, ₽',
                'data' => [],
                'levels' => self::_getEmptyShops($model),
            ],
            calc::KEY_OTHER_INCOME => [
                'totalBlock' => true,
                'editAttrJS' => calc::KEY_OTHER_INCOME,
                'title' => 'Другие доходы, ₽',
                'data' => [],
            ],
            calc::KEY_OTHER_EXPENSE => [
                'totalBlock' => true,
                'editAttrJS' => calc::KEY_OTHER_EXPENSE,
                'title' => 'Другие расходы, ₽',
                'data' => [],
            ],
            calc::KEY_EBIDTA => [
                'totalBlock' => true,
                'title' => 'EBIDTA, ₽',
                'data' => [],
            ],
            calc::KEY_OTHER_RECEIVED_PERCENTS => [
                'totalBlock' => true,
                'editAttrJS' => calc::KEY_OTHER_RECEIVED_PERCENTS,
                'title' => 'Проценты полученные, ₽',
                'data' => [],
            ],
            calc::KEY_OTHER_PAID_PERCENTS => [
                'totalBlock' => true,
                'editAttrJS' => calc::KEY_OTHER_PAID_PERCENTS,
                'title' => 'Проценты уплаченные, ₽',
                'data' => [],
            ],
            calc::KEY_OTHER_AMORTIZATION => [
                'totalBlock' => true,
                'editAttrJS' => calc::KEY_OTHER_AMORTIZATION,
                'title' => 'Амортизация, ₽',
                'data' => [],
            ],
            calc::KEY_PROFIT_BEFORE_TAX => [
                'totalBlock' => true,
                'title' => 'ПРИБЫЛЬ ДО НАЛОГООБЛОЖЕНИЯ, ₽',
                'data' => [],
            ],
            calc::KEY_OTHER_TAX_RATE => [
                'totalBlock' => true,
                'editAttrJS' => calc::KEY_OTHER_TAX_RATE,
                'title' => 'Налог на прибыль, %',
                'units' => '%',
                'data' => [],
            ],
            calc::KEY_NET_PROFIT => [
                'totalBlock' => true,
                'title' => 'ЧИСТАЯ ПРИБЫЛЬ, ₽',
                'data' => [],
            ],
            calc::KEY_OTHER_PROFIT_DISTRIBUTION => [
                'totalBlock' => true,
                'editAttrJS' => calc::KEY_OTHER_PROFIT_DISTRIBUTION,
                'title' => 'Распределение прибыли, ₽',
                'data' => [],
            ],
            calc::KEY_UNDISTRIBUTED_PROFIT => [
                'totalBlock' => true,
                'title' => 'НЕРАСПРЕДЕЛЕННАЯ ПРИБЫЛЬ, ₽',
                'data' => [],
            ],
        ];

        // calculation
        calc::init($model);

        foreach ($modelData as $getter1 => &$level1)
        {
            if (empty($level1['data']))
                $level1['data'] = calc::calculateRow($getter1);

            if (!empty($level1['levels'])) {
                foreach ($level1['levels'] as $getter2 => &$level2) {
                    if (empty($level2['data']))
                        $level2['data'] = calc::calculateRow($getter2, $getter1);
                }
            }
        }

        self::_addAvgColumn($modelData);

        return $modelData;
    }

    /**
     * @param FinanceModel $model
     * @return array
     */
    public static function getRevenue(FinanceModel $model)
    {
        $modelData = [];

        $keyTotalTotal = "total_revenue";

        // total revenue
        $modelData[$keyTotalTotal] = [
            'totalBlock' => true,
            'title' => 'ВЫРУЧКА ПЛАН, ₽',
            'data' => [],
        ];

        foreach ([FinanceModelShop::TYPE_INTERNET_SHOP, FinanceModelShop::TYPE_SHOP] as $type) {
            $keyTotal = "total_{$type}";
            $shops = $model->getShops()->andWhere(['type' => $type])->orderBy('name')->all();
            $modelData[$keyTotal] = [
                'title' => ($type == FinanceModelShop::TYPE_SHOP) ? 'Оффлайн продажи' : 'Интернет продажи',
                'totalBlock' => true,
                'isButtonable' => true,
                'shopsCount' => count($shops),
                'data' => [],
                'levels' => self::_getEmptyShop($type)
           ];

           foreach ($shops as $shop) {
               // monthes
               $modelData[$shop->id] = [
                   'title' => $shop->name,
                   'isButtonable' => true,
                   'editUrl' => Url::to(['update-shop', 'model_id' => $model->id, 'shop_id' => $shop->id]),
                   'data' => [],
                   'levels' => self::_getEmptyShop($type)
               ];
               foreach ($shop->revenueMonthsData as $attr => $data) {
                   if ($attr == 'average_check')
                       $data = array_map(function ($amount) { return $amount / 100; }, $data);

                   $modelData[$shop->id]['levels'][$attr]['data'] = $data;

                   // monthes total
                   foreach ($data as $key => $value) {
                       $modelData[$keyTotal]['levels'][$attr]['data'][$key][] = $value;
                   }
               }

               // revenue
               $modelData[$shop->id]['levels']['revenue'] = [
                   'title' => 'Выручка, ₽',
                   'data' => []
               ];
               if ($shop->type == FinanceModelShop::TYPE_SHOP) {
                   foreach ($modelData[$shop->id]['levels']['check_count']['data'] as $key => $value) {
                       $checksCount = ArrayHelper::getValue($modelData[$shop->id]['levels'], "check_count.data.{$key}", 0);
                       $averageCheck = ArrayHelper::getValue($modelData[$shop->id]['levels'], "average_check.data.{$key}", 0);
                       $modelData[$shop->id]['levels']['revenue']['data'][$key] = $checksCount * $averageCheck;

                       // revenue total
                       $modelData[$keyTotal]['levels']['revenue']['data'][$key][] = $checksCount * $averageCheck;
                       $modelData[$keyTotalTotal]['data'][$key][] = $checksCount * $averageCheck;

                       // revenue by shop
                       if (!isset($modelData[$shop->id]['data'][$key]))
                           $modelData[$shop->id]['data'][$key] = $checksCount * $averageCheck;
                       else
                           $modelData[$shop->id]['data'][$key] += $checksCount * $averageCheck;
                        // revenue by shop type
                       if (!isset($modelData[$keyTotal]['data'][$key]))
                           $modelData[$keyTotal]['data'][$key] = $checksCount * $averageCheck;
                       else
                           $modelData[$keyTotal]['data'][$key] += $checksCount * $averageCheck;
                   }
               }
               if ($shop->type == FinanceModelShop::TYPE_INTERNET_SHOP) {
                   foreach ($modelData[$shop->id]['levels']['visit_count']['data'] as $key => $value) {
                       $visitCount = ArrayHelper::getValue($modelData[$shop->id]['levels'], "visit_count.data.{$key}", 0);
                       $conversion = ArrayHelper::getValue($modelData[$shop->id]['levels'], "conversion.data.{$key}", 0);
                       $averageCheck = ArrayHelper::getValue($modelData[$shop->id]['levels'], "average_check.data.{$key}", 0);
                       $modelData[$shop->id]['levels']['check_count']['data'][$key] = round($visitCount * ($conversion / 100));
                       $modelData[$shop->id]['levels']['revenue']['data'][$key] = $visitCount * ($conversion / 100) * $averageCheck;

                       // revenue total
                       $modelData[$keyTotal]['levels']['revenue']['data'][$key][] = $visitCount * ($conversion / 100) * $averageCheck;
                       $modelData[$keyTotalTotal]['data'][$key][] = $visitCount * ($conversion / 100) * $averageCheck;

                       // revenue by shop
                       if (!isset($modelData[$shop->id]['data'][$key]))
                           $modelData[$shop->id]['data'][$key] = $visitCount * ($conversion / 100) * $averageCheck;
                       else
                           $modelData[$shop->id]['data'][$key] += $visitCount * ($conversion / 100) * $averageCheck;
                       // revenue by shop type
                       if (!isset($modelData[$keyTotal]['data'][$key]))
                           $modelData[$keyTotal]['data'][$key] = $visitCount * ($conversion / 100) * $averageCheck;
                       else
                           $modelData[$keyTotal]['data'][$key] += $visitCount * ($conversion / 100) * $averageCheck;

                        // check count by shop type
                       $modelData[$keyTotal]['levels']['check_count']['data'][$key][] = round($visitCount * ($conversion / 100));
                   }
               }
           }
       }

       // split total data
       foreach ([FinanceModelShop::TYPE_SHOP, FinanceModelShop::TYPE_INTERNET_SHOP] as $type) {
           $keyTotal = "total_{$type}";
           foreach ($modelData[$keyTotal]['levels'] as $attr => $data) {
               foreach ($data['data'] as $k => $v) {
                   switch ($attr) {
                       case 'average_check':
                       case 'conversion':
                           $modelData[$keyTotal]['levels'][$attr]['data'][$k] = array_sum($v) / count($v);
                           break;
                       case 'check_count':
                       case 'visit_count':
                       case 'revenue':
                       default:
                           $modelData[$keyTotal]['levels'][$attr]['data'][$k] = array_sum($v);
                           break;
                   }
               }
           }
       }

       // split total revenue data
       foreach ($modelData[$keyTotalTotal]['data'] as $k => $v) {
           $modelData[$keyTotalTotal]['data'][$k] = array_sum($v);
       }

       // remove totals for shops <= 1
       foreach ([FinanceModelShop::TYPE_INTERNET_SHOP, FinanceModelShop::TYPE_SHOP] as $type) {
           $keyTotal = "total_{$type}";
           if ($modelData[$keyTotal]['shopsCount'] <= 1)
               unset($modelData[$keyTotal]);
       }

        // add avg column
        foreach ($modelData as $key => $value) {
            $modelData[$key]['avg'] = (!empty($value['data'])) ?
                array_sum($value['data']) / count($value['data']) : 0;

            if (!empty($value['levels']))
                foreach ($value['levels'] as $k => $v)
                    $modelData[$key]['levels'][$k]['avg'] = (!empty($v['data'])) ?
                        array_sum($v['data']) / count($v['data']) : 0;

        }

        return $modelData;
    }

    /**
     * @param $modelData
     */
    protected static function _addAvgColumn(&$modelData)
    {
        // add avg column
        foreach ($modelData as $key => $value) {
            $modelData[$key]['avg'] = (!empty($value['data'])) ?
                array_sum($value['data']) / count($value['data']) : 0;

            if (!empty($value['levels']))
                foreach ($value['levels'] as $k => $v)
                    $modelData[$key]['levels'][$k]['avg'] = (!empty($v['data'])) ?
                        array_sum($v['data']) / count($v['data']) : 0;

        }
    }

    /**
     * @param $model
     * @param $costsType
     * @param $units
     * @return array
     */
    protected static function _getEmptyShops(FinanceModel $model, $costsType = null, $units = null)
    {
        $data = [];
        $shops = $model->getShops()->orderBy(['type' => SORT_DESC, 'name' => SORT_ASC])->all();
        /** @var FinanceModelShop $shop */
        foreach ($shops as $shop)
        {
            $data[$shop->id] = [
                'title' => htmlspecialchars($shop->name),
                'editUrl' => ($costsType) ?
                    Url::to(['update-shop-costs', 'model_id' => $model->id, 'shop_id' => $shop->id, 'type' => $costsType]) :
                    null,
                'units' => $units,
                'data' => []
            ];
        }

        return $data;
    }

    /**
     * @param $type
     * @return array
     */
    protected static function _getEmptyShop($type)
    {
        if ($type == FinanceModelShop::TYPE_SHOP) {
            return [
                'check_count' => [
                    'title' => 'Количество чеков, шт',
                    'data' => [],
                ],
                'average_check' => [
                    'title' => 'Средний чек, ₽',
                    'data' => [],
                ],
                'revenue' => [
                    'title' => 'Выручка, ₽',
                    'data' => []
                ]
            ];
        }

        if ($type == FinanceModelShop::TYPE_INTERNET_SHOP) {
            return [
                'visit_count' => [
                    'title' => 'Количество визитов, шт',
                    'data' => [],
                ],
                'conversion' => [
                    'title' => 'Конверсия, %',
                    'units' => '%',
                    'data' => [],
                ],
                'check_count' => [
                    'title' => 'Количество покупок, шт',
                    'data' => [],
                ],
                'average_check' => [
                    'title' => 'Средний чек, ₽',
                    'data' => [],
                ],
                'revenue' => [
                    'title' => 'Выручка, ₽',
                    'data' => []
                ]
            ];
        }

        return [];
    }
}