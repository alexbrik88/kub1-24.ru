<?php

namespace frontend\modules\analytics\models\financeModel;

use common\components\helpers\ArrayHelper;
use Yii;
use yii\base\Exception;

/**
 * This is the model class for table "finance_model_other_cost".
 *
 * @property int $model_id
 * @property int $year
 * @property int $month
 * @property int $income
 * @property int $expense
 * @property int $amortization
 * @property int $paid_percents
 * @property int $received_percents
 *
 * @property float $tax_rate
 * @property int $profit_distribution
 *
 * @property FinanceModel $model
 */
class FinanceModelOtherCost extends FinanceModelMonth
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_model_other_cost';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_id', 'year', 'month'], 'required'],
            [['model_id', 'year', 'month', 'income', 'expense', 'paid_percents', 'received_percents', 'amortization', 'profit_distribution'], 'integer'],
            [['tax_rate'], 'number'],
            [['model_id', 'year', 'month'], 'unique', 'targetAttribute' => ['model_id', 'year', 'month']],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceModel::className(), 'targetAttribute' => ['model_id' => 'id']],

            [['income', 'expense', 'paid_percents', 'received_percents', 'amortization', 'profit_distribution'], 'filter', 'filter' => function ($value) {
                return (int)$value;
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'model_id' => 'Модель',
            'year' => 'Год',
            'month' => 'Месяц',
            'income' => 'Другие доходы',
            'expense' => 'Другие расходы',
            'paid_percents' => 'Проценты уплаченные',
            'received_percents' => 'Проценты полученные',
            'amortization' => 'Амортизация',
            'tax_rate' => 'Налог на прибыль',
            'profit_distribution' => 'Распределение прибыли',
        ];
    }

    public static function editableAttributes()
    {
        return [
            'income',
            'expense',
            'paid_percents',
            'received_percents',
            'amortization',
            'tax_rate',
            'profit_distribution'
        ];
    }

    /**
     * Gets query for [[Model]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(FinanceModel::className(), ['id' => 'model_id']);
    }

    /**
     * @param FinanceModel $financeModel
     * @param array $data
     * @param bool $canCreate
     * @return bool
     * @throws Exception
     */
    public static function saveMonthes(FinanceModel $financeModel, $data = [], $canCreate = false)
    {
        if ($canCreate) {
            foreach ((array)$financeModel->monthes as $ym) {
                $year = substr($ym, 0, 4);
                $month = substr($ym, 4, 2);
                $model = self::findOne(['model_id' => $financeModel->id, 'year' => $year, 'month' => $month]);
                if (!$model) {
                    $model = new self(['model_id' => $financeModel->id, 'year' => (int)$year, 'month' => (int)$month]);
                }

                if (!$model->save()) {
                    return false;
                }
            }

            return true;
        }

        foreach (self::editableAttributes() as $attr)
        {
            if (isset($data[$attr])) {
                foreach ((array)$financeModel->monthes as $ym) {
                    $year = substr($ym, 0, 4);
                    $month = substr($ym, 4, 2);
                    $value = ArrayHelper::getValue($data, "{$attr}.{$ym}", 0);

                    $model = self::findOne(['model_id' => $financeModel->id, 'year' => $year, 'month' => $month]);

                    if (!$model) {
                        throw new Exception("Month {$ym} not found in finance model");
                    }

                    $model->{$attr} = round(self::sanitize($value) * ($attr == 'tax_rate' ? 1 : 100));

                    if (!$model->save()) {
                        return false;
                    }
                }
            }
        }

        return true;
    }
}
