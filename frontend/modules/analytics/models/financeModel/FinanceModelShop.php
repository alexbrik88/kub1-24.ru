<?php

namespace frontend\modules\analytics\models\financeModel;

use Yii;

/**
 * This is the model class for table "finance_model_shop".
 *
 * @property int $id
 * @property int $model_id
 * @property int $type 1 - Оффлайн Магазин, 2 - Интернет Магазин
 * @property string $name
 *
 * @property FinanceModelRevenue[] $revenueMonths
 * @property FinanceModelFixedCost[] $fixedCostMonths
 * @property FinanceModelVariableCost[] $variableCostMonths
 * @property FinanceModel $model
 *
 * Prepared data for the table
 * @property array $fixedCostMonthsData
 * @property array $variableCostMonthsData
 * @property array $variableCostMonthsPercentData
 * @property array $revenueMonthsData
 * @property array $revenueMonthsAmountData
 * @property array $fixedCostMonthsTotal
 * @property array $variableCostMonthsTotal
 */
class FinanceModelShop extends \yii\db\ActiveRecord
{
    const TYPE_SHOP = 1;
    const TYPE_INTERNET_SHOP = 2;

    const SHOP_ATTRIBUTES = ['check_count', 'average_check'];
    const INTERNET_SHOP_ATTRIBUTES = ['visit_count', 'conversion', 'average_check'];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_model_shop';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_id', 'type', 'name'], 'required'],
            [['model_id', 'type'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceModel::className(), 'targetAttribute' => ['model_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_id' => 'Финансовая модель',
            'type' => 'Тип точки продаж',
            'name' => 'Название точки продаж',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(FinanceModel::className(), ['id' => 'model_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRevenueMonths()
    {
        return $this->hasMany(FinanceModelRevenue::className(), ['shop_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFixedCostMonths()
    {
        return $this->hasMany(FinanceModelFixedCost::className(), ['shop_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVariableCostMonths()
    {
        return $this->hasMany(FinanceModelVariableCost::className(), ['shop_id' => 'id']);
    }

    /**
     * @return array
     */
    public function getRevenueMonthsData()
    {
        $ret = [];
        foreach ($this->revenueMonths as $m) {
            $year = $m->year;
            $month = str_pad($m->month, 2, "0", STR_PAD_LEFT);
            foreach (array_keys($m->attributeLabels()) as $attr) {
                if (in_array($attr, ['shop_id', 'year', 'month', 'amount']))
                    continue;

                if ($this->type == self::TYPE_SHOP && in_array($attr, self::SHOP_ATTRIBUTES)
                ||  $this->type == self::TYPE_INTERNET_SHOP && in_array($attr, self::INTERNET_SHOP_ATTRIBUTES)) {

                    $ret[$attr][$year . $month] = $m->{$attr};
                }
            }
        }

        return $ret;
    }

    /**
     * @return array
     */
    public function getRevenueMonthsAmountData()
    {
        $ret = [];
        foreach ($this->revenueMonths as $m) {
            $year = $m->year;
            $month = str_pad($m->month, 2, "0", STR_PAD_LEFT);
            $ret[$year . $month] = $m->amount;
        }

        return $ret;
    }

    /**
     * @return array
     */
    public function getFixedCostMonthsData()
    {
        $ret = [];
        $costsByMonth = $this->getFixedCostMonths()->orderBy('item_id')->all();
        /** @var FinanceModelFixedCost $m */
        foreach ($costsByMonth as $m) {
            $attr = $m->item_id;
            $year = $m->year;
            $month = str_pad($m->month, 2, "0", STR_PAD_LEFT);
            $ret[$attr][$year . $month] = $m->amount;
        }

        return $ret;
    }

    /**
     * @return array
     */
    public function getVariableCostMonthsData()
    {
        $ret = [];
        $costsByMonth = $this->getVariableCostMonths()->orderBy('item_id')->all();
        /** @var FinanceModelVariableCost $m */
        foreach ($costsByMonth as $m) {
            $attr = $m->item_id;
            $year = $m->year;
            $month = str_pad($m->month, 2, "0", STR_PAD_LEFT);
            $ret[$attr][$year . $month] = $m->amount;
        }

        return $ret;
    }

    /**
     * @return array
     */
    public function getVariableCostMonthsPercentData()
    {
        $ret = [];
        $costsByMonth = $this->getVariableCostMonths()->orderBy('item_id')->distinct()->all();
        /** @var FinanceModelVariableCost $m */
        foreach ($costsByMonth as $m) {
            $attr = $m->item_id;
            $ret[$attr] = $m->percent;
        }

        return $ret;
    }

    /**
     * @return array
     */
    public function getFixedCostMonthsTotal()
    {
        $ret = [];
        foreach ($this->fixedCostMonths as $m) {
            $year = $m->year;
            $month = str_pad($m->month, 2, "0", STR_PAD_LEFT);
            if (!isset($ret[$year . $month])) $ret[$year . $month] = 0;
            $ret[$year . $month] += $m->amount;
        }

        return $ret;
    }

    /**
     * @return array
     */
    public function getVariableCostMonthsTotal()
    {
        $ret = [];
        foreach ($this->variableCostMonths as $m) {
            $year = $m->year;
            $month = str_pad($m->month, 2, "0", STR_PAD_LEFT);
            if (!isset($ret[$year . $month])) $ret[$year . $month] = 0;
            $ret[$year . $month] += $m->amount;
        }

        return $ret;
    }
}
