<?php

namespace frontend\modules\analytics\models\financeModel;

use common\components\helpers\ArrayHelper;
use Yii;
use yii\base\Exception;

/**
 * This is the model class for table "finance_model_revenue".
 *
 * @property int $shop_id
 * @property int $year
 * @property int $month
 * @property int|null $check_count
 * @property int|null $average_check
 * @property int|null $visit_count
 * @property float|null $conversion
 * @property int|null $amount
 *
 * @property FinanceModelShop $shop
 */
class FinanceModelRevenue extends FinanceModelMonth
{
    const INTEGER_ATTRIBUTES = ['check_count', 'visit_count'];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_model_revenue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['shop_id', 'year', 'month'], 'required'],
            [['shop_id', 'year', 'month', 'check_count', 'average_check', 'visit_count', 'amount'], 'integer'],
            [['conversion'], 'number'],
            [['shop_id', 'year', 'month'], 'unique', 'targetAttribute' => ['shop_id', 'year', 'month']],
            [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceModelShop::className(), 'targetAttribute' => ['shop_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'shop_id' => 'Точка продаж',
            'year' => 'Год',
            'month' => 'Месяц',
            'visit_count' => 'Количество визитов',
            'check_count' => 'Количество чеков',
            'conversion' => 'Конверсия, %',
            'average_check' => 'Средний чек',
            'amount' => 'Сумма',
        ];
    }

    /**
     * Gets query for [[Shop]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShop()
    {
        return $this->hasOne(FinanceModelShop::className(), ['id' => 'shop_id']);
    }

    /**
     * @param FinanceModelShop $shop
     * @param $data
     * @param bool $canCreate
     * @return bool
     * @throws Exception
     */
    public static function saveShopMonthes(FinanceModelShop $shop, $data, $canCreate = false)
    {
        $monthes = ArrayHelper::getValue($data, 'monthes');

        foreach ((array)$monthes as $ym) {
            $year = substr($ym, 0, 4);
            $month = substr($ym, 4, 2);
            $checksCount = ArrayHelper::getValue($data, "check_count.{$ym}", 0);
            $averageCheck = ArrayHelper::getValue($data, "average_check.{$ym}", 0);
            $visitsCount = ArrayHelper::getValue($data, "visit_count.{$ym}", 0);
            $conversion = ArrayHelper::getValue($data, "conversion.{$ym}", 0);

            $model = FinanceModelRevenue::findOne(['shop_id' => $shop->id, 'year' => $year, 'month' => $month]);

            if (!$model) {
                if ($canCreate)
                    $model = new FinanceModelRevenue(['shop_id' => $shop->id, 'year' => (int)$year, 'month' => (int)$month]);
                else
                    throw new Exception("Month {$ym} not found in finance model");
            }

            if ($shop->type == FinanceModelShop::TYPE_SHOP) {
                $model->check_count = round(self::sanitize($checksCount));
                $model->average_check = round(100 * self::sanitize($averageCheck));
                $model->amount = $model->check_count * $model->average_check;
            }
            if ($shop->type == FinanceModelShop::TYPE_INTERNET_SHOP) {
                $model->visit_count = round(self::sanitize($visitsCount));
                $model->average_check = round(100 * self::sanitize($averageCheck));
                $model->conversion = self::sanitize($conversion);
                $model->amount = round($model->visit_count * $model->conversion / 100 * $model->average_check);
            }

            if (!$model->save())
                return false;
        }

        return true;
    }
}
