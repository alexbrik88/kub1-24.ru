<?php

namespace frontend\modules\analytics\models;

use common\models\Company;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use DateTime;
use frontend\models\Documents;
use frontend\rbac\UserRole;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class DisciplineSearch
 */
class DisciplineSearch extends Contractor
{
    const GROUP_1 = 1;
    const GROUP_2 = 2;
    const GROUP_3 = 3;
    const GROUP_4 = 4;

    const THIS_MONTH = 1;
    const THIS_QUARTER = 2;
    const THIS_YEAR = 3;
    const PAST_MONTH = 4;
    const PAST_QUARTER = 5;
    const PAST_YEAR = 6;

    const DEFAULT_PERIOD = self::THIS_YEAR;

    const TMP_TABLE_NAME = 'discipline_tmp';

    /**
     * @var attributes
     */
    public $delay;
    public $days;
    public $overdue;
    public $group;
    public $abc;
    public $m1;
    public $m2;
    public $m3;
    public $m4;
    public $m5;
    public $m6;
    public $m7;
    public $m8;
    public $m9;
    public $m10;
    public $m11;
    public $m12;

    protected $_company = null;
    protected $_dateFrom = null;
    protected $_dateTo = null;
    protected $_averageDays = null;
    protected $_tmpRowCount = null;
    protected $_period = [];
    protected $_overallResult = [];
    protected $_monthArray = [];
    protected $_responsibleEmployee = null;

    /**
     * @var array
     */
    public static $groups = [
        self::GROUP_1 => 'Норма',
        self::GROUP_2 => 'Внимание',
        self::GROUP_3 => 'Риск',
        self::GROUP_4 => 'Опасно',
    ];

    /**
     * @var array
     */
    public static $periodArray = [
        self::THIS_MONTH => 'Этот месяц',
        self::THIS_QUARTER => 'Этот квартал',
        self::THIS_YEAR => 'Этот год',
        self::PAST_MONTH => 'Предыдущий месяц',
        self::PAST_QUARTER => 'Предыдущий квартал',
        self::PAST_YEAR => 'Предыдущий год',
    ];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'group', 'abc'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Контрагент',
            'delay' => 'Отсрочка оплаты счета (дн.)',
            'days' => 'Среднее кол-во дней на оплату',
            'overdue' => 'Средняя просрочка (дн.)',
            'group' => 'Платежная дисциплина',
            'abc' => 'Группа АВС',
            'm1' => 'янв',
            'm2' => 'фев',
            'm3' => 'мар',
            'm4' => 'апр',
            'm5' => 'май',
            'm6' => 'июн',
            'm7' => 'июл',
            'm8' => 'авг',
            'm9' => 'сен',
            'm10' => 'окт',
            'm11' => 'ноя',
            'm12' => 'дек',
        ];
    }

    /**
     * @param  array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $this->createTmpTables();

        $this->load($params);

        $abcQuery = (new Query)
            ->select([
                'contractor_id',
                'abc' => 'group',
            ])
            ->from(AnalysisSearch::TMP_TABLE_NAME);

        $query = self::find()
            ->select([
                'contractor.*',
                'calc.*',
                'abcTable.abc',
            ])
            ->innerJoin(['calc' => self::TMP_TABLE_NAME], "{{contractor}}.[[id]] = {{calc}}.[[contractor_id]]")
            ->leftJoin(['abcTable' => $abcQuery], "{{contractor}}.[[id]] = {{abcTable}}.[[contractor_id]]");

        foreach ($this->monthArray as $month) {
            $query
                ->addSelect([
                    "m{$month}" => "t{$month}.days"
                ])
                ->leftJoin([
                    "t{$month}" => $this->getPerMonthQuery($month)
                ], "{{contractor}}.[[id]] = {{t{$month}}}.[[contractor_id]]");
        }

        if ($this->getResponsibleEmployeeId() !== 0) {
            $query->andWhere(['contractor.responsible_employee_id' => $this->getResponsibleEmployeeId()]);
        }

        $query->andFilterWhere([
            'contractor.id' => $this->id,
            'calc.group' => $this->group,
            'abc' => $this->abc,
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $attributes = [
            'delay',
            'days',
            'overdue',
            'group',
            'abc',
        ];
        foreach ($this->monthArray as $month) {
            $attributes[] = "m{$month}";
        }
        $dataProvider->setSort([
            'attributes' => $attributes,
            'defaultOrder' => [
                'days' => SORT_DESC,
            ],
        ]);

        return $dataProvider;
    }

    /**
     * @param $contractorID
     * @return false|null|string
     */
    public function getContractorGroup($contractorID)
    {
        $this->createTmpTables();

        return self::find()
            ->select([
                'calc.group',
            ])
            ->innerJoin(['calc' => self::TMP_TABLE_NAME], "{{contractor}}.[[id]] = {{calc}}.[[contractor_id]]")
            ->andWhere(['contractor.id' => $contractorID])
            ->scalar();
    }

    /**
     * create temporary tables
     * save overall result
     */
    public function createTmpTables()
    {
        $analysisSearch = new AnalysisSearch([
            'company' => $this->company,
            'dateRange' => $this->dateRange,
        ]);
        $analysisSearch->createTmpAbcTable();

        $tableName = self::TMP_TABLE_NAME;
        $sql = $this->getTmpTableQuery()->createCommand()->rawSql;
        Yii::$app->db->createCommand("
            CREATE TEMPORARY TABLE IF NOT EXISTS {{{$tableName}}} AS ({$sql})
        ")->execute();

        //overall result data
        $resultQuery = (new Query())
            ->select([
                'result.group',
                'contractor_count' => new Expression("COUNT({{result}}.[[group]])"),
                'contractor_part' => new Expression($this->tmpRowCount ? "(COUNT({{result}}.[[group]]) / {$this->tmpRowCount} * 100)" : "0"),
                'days_count' => new Expression("(SUM({{result}}.[[days]]) / COUNT({{result}}.[[group]]))"),
            ])
            ->from(['result' => self::TMP_TABLE_NAME])
            ->groupBy('result.group')
            ->orderBy('result.group');

        $this->_overallResult = $resultQuery->all();
    }

    /**
     * @return \yii\db\Query
     */
    public function getTmpTableQuery()
    {
        $g1 = self::GROUP_1;
        $g2 = self::GROUP_2;
        $g3 = self::GROUP_3;
        $g4 = self::GROUP_4;

        $query = (new Query)
            ->select([
                'contractor_id',
                'delay',
                'days',
                'overdue' => new Expression("([[days]] - [[delay]])"),
                'group' => new Expression("IF(
                    [[days]] <= [[delay]],
                    {$g1},
                    IF(
                        [[days]] <= 2 * [[delay]],
                        {$g2},
                        IF([[days]] <= 3 * [[delay]], {$g3}, {$g4})
                    )
                )"),
            ])
            ->from(['t' => $this->getOverdueQuery()]);

        return $query;
    }

    /**
     * @return integer
     */
    public function getTmpRowCount()
    {
        if ($this->_tmpRowCount === null) {
            $this->_tmpRowCount = (new Query())->from(self::TMP_TABLE_NAME)->count();
        }

        return (int)$this->_tmpRowCount;
    }

    /**
     * @return integer
     */
    public function getAverageDays()
    {
        if ($this->_averageDays === null) {
            $this->_averageDays = $this->tmpRowCount ? ((new Query())->from(self::TMP_TABLE_NAME)->sum('days') / $this->tmpRowCount) : 0;
        }

        return (int)$this->_averageDays;
    }

    /**
     * @return \yii\db\Query
     */
    public function getOverdueQuery()
    {
        $query = (new Query())
            ->select([
                'overdue.contractor_id',
                //'delay' => 'contractor.payment_delay',
                'delay' => new Expression("(
                    IF({{overdue}}.[[type]]=1, {{contractor}}.[[seller_payment_delay]], {{contractor}}.[[customer_payment_delay]])
                )"),
                'days' => new Expression("(
                    SUM({{overdue}}.[[invoice_delay]]) / COUNT({{overdue}}.[[contractor_id]])
                )"),
                'overdue' => new Expression("(
                    SUM({{overdue}}.[[invoice_delay]]) / COUNT({{overdue}}.[[contractor_id]])
                    - IF({{overdue}}.[[type]]=1, {{contractor}}.[[seller_payment_delay]], {{contractor}}.[[customer_payment_delay]])
                )"),
            ])
            ->from(['overdue' => $this->getBaseQuery()])
            ->innerJoin('contractor', '{{overdue}}.[[contractor_id]] = {{contractor}}.[[id]]')
            ->groupBy('overdue.contractor_id');

        return $query;
    }

    /**
     * @return \yii\db\Query
     */
    public function getPerMonthQuery($month)
    {
        $alias = "t{$month}";
        $query = (new Query())
            ->select([
                "{$alias}.contractor_id",
                'days' => new Expression("(
                    SUM({{{$alias}}}.[[invoice_delay]]) / COUNT({{{$alias}}}.[[contractor_id]])
                )"),
                'overdue' => new Expression("(
                    SUM({{{$alias}}}.[[invoice_delay]]) / COUNT({{{$alias}}}.[[contractor_id]])
                    - IF({{{$alias}}}.[[type]]=1, {{contractor}}.[[seller_payment_delay]], {{contractor}}.[[customer_payment_delay]])
                )"),
            ])
            ->from([$alias => $this->getBaseQuery($month)])
            ->innerJoin('contractor', "{{{$alias}}}.[[contractor_id]] = {{contractor}}.[[id]]")
            ->groupBy("{$alias}.contractor_id");

        return $query;
    }

    /**
     * @return \yii\db\Query
     */
    public function getBaseQuery($month = null)
    {
        $paid = InvoiceStatus::STATUS_PAYED;
        $query = (new Query())
            ->select([
                'invoice.type',
                'invoice.contractor_id',
                'invoice_delay' => new Expression("
                    FLOOR(
                        (
                            IF(
                                {{invoice}}.[[invoice_status_id]] = {$paid},
                                {{invoice}}.[[invoice_status_updated_at]],
                                UNIX_TIMESTAMP()
                            ) - UNIX_TIMESTAMP({{invoice}}.[[document_date]])
                        ) / 86400
                    )
                "),
            ])
            ->from('invoice')
            ->andWhere([
                'invoice.company_id' => $this->company->id,
                'invoice.type' => Documents::IO_TYPE_OUT,
                'invoice.is_deleted' => false,
            ])
            ->andWhere([
                'not',
                ['invoice.invoice_status_id' => [
                    InvoiceStatus::STATUS_AUTOINVOICE,
                    InvoiceStatus::STATUS_REJECTED,
                ]]
            ]);

        if ($this->dateFrom && $this->dateTo) {
            $query->andWhere([
                'between',
                'invoice.document_date',
                $this->dateFrom->format('Y-m-d'),
                $this->dateTo->format('Y-m-d'),
            ]);
        }

        if ($month !== null) {
            $query->andWhere([
                "MONTH({{invoice}}.[[document_date]])" => $month,
            ]);
        }

        if ($this->getResponsibleEmployeeId() !== 0) {
            $query
                ->innerJoin(['contractor' => Contractor::tableName()], 'invoice.contractor_id = contractor.id')
                ->andWhere(['contractor.responsible_employee_id' => $this->getResponsibleEmployeeId()]);
        }

        return $query;
    }

    /**
     * @return array
     */
    public function getOverallResult()
    {
        return (array)$this->_overallResult;
    }

    /**
     * @return array
     */
    public function getContractorFilterItems()
    {
        $idArray = (new Query())->select('contractor_id')->distinct()->from(self::TMP_TABLE_NAME)->column();
        $contractorArray = Contractor::getSorted()->andWhere(['contractor.id' => $idArray])->all();
        return ['' => 'Все'] + ArrayHelper::map($contractorArray, 'id', 'nameWithType');
    }

    /**
     * @param integer $value
     */
    public function setPeriod($value)
    {
        $this->_period = in_array($value *= 1, array_keys(self::$periodArray)) ? $value : self::DEFAULT_PERIOD;

        switch ($this->_period) {
            case self::THIS_QUARTER:
                $quarter = ceil(date('n') / 3);
                $this->_dateFrom = DateTime::createFromFormat('n', $quarter * 3 - 2)->modify('first day of this month');
                $this->_dateTo = DateTime::createFromFormat('n', $quarter * 3)->modify('last day of this month');
                break;

            case self::THIS_YEAR:
                $this->_dateFrom = new DateTime('first day of january');
                $this->_dateTo = new DateTime('last day of december');
                break;

            case self::PAST_MONTH:
                $this->_dateFrom = new DateTime('first day of -1 month');
                $this->_dateTo = new DateTime('last day of -1 month');
                break;

            case self::PAST_QUARTER:
                $quarter = ceil(date('n') / 3);
                $this->_dateFrom = DateTime::createFromFormat('n', $quarter * 3 - 2)->modify('first day of -3 month');
                $this->_dateTo = DateTime::createFromFormat('n', $quarter * 3)->modify('last day of -3 month');
                break;

            case self::PAST_YEAR:
                $this->_dateFrom = new DateTime('-1 year first day of january');
                $this->_dateTo = new DateTime('-1 year last day of december');
                break;

            case self::THIS_MONTH:
            default:
                $this->_dateFrom = new DateTime('first day of this month');
                $this->_dateTo = new DateTime('last day of this month');
                break;
        }

        $this->_monthArray = [];
        $month = (int)$this->_dateFrom->format('n');
        $monthTo = (int)$this->_dateTo->format('n');
        while ($month <= $monthTo) {
            $this->_monthArray[] = $month++;
        }
    }

    /**
     * @return array
     */
    public function getMonthArray()
    {
        return $this->_monthArray;
    }

    /**
     * @return integer
     */
    public function getPeriod()
    {
        return $this->_period;
    }

    /**
     * @return array
     */
    public function getDateRange()
    {
        if ($this->dateFrom && $this->dateTo) {
            return [
                'from' => $this->dateFrom->format('Y-m-d'),
                'to' => $this->dateTo->format('Y-m-d'),
            ];
        }

        return null;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->_company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @return DateTime
     */
    public function getDateFrom()
    {
        return $this->_dateFrom;
    }

    /**
     * @return DateTime
     */
    public function getDateTo()
    {
        return $this->_dateTo;
    }

    /**
     * @return string
     */
    public function getPeriodName()
    {
        return self::$periodArray[$this->period];
    }

    /**
     * @return array
     */
    public function getPeriodItems()
    {
        $periodItems = [];
        foreach (self::$periodArray as $id => $name) {
            $periodItems[] = [
                'label' => $name,
                'url' => ['/reports/discipline/index', 'period' => $id],
                'options' => ($this->period == $id) ? ['class' => 'active'] : [],
                'linkOptions' => ['class' => 'discipline-pjax-link']
            ];
        }

        return $periodItems;
    }

    /**
     * @return string
     */
    public function getGroupLabel($i = null)
    {
        return isset(self::$groups[$i ?: $this->group]) ? self::$groups[$i ?: $this->group] : '';
    }

    /**
     * @return string
     */
    public function getGroupColor($i = null)
    {
        $colorArray = [
            self::GROUP_1 => '#7dde81',
            self::GROUP_2 => '#dfba49',
            self::GROUP_3 => '#e87e04',
            self::GROUP_4 => '#f3555d',
        ];

        return isset($colorArray[$i ?: $this->group]) ? $colorArray[$i ?: $this->group] : '';
    }

    /**
     * @return string
     */
    public function getAbcValue()
    {
        return isset(AnalysisSearch::$groups[$this->abc]) ? AnalysisSearch::$groups[$this->abc] : '---';
    }

    /**
     * @return string
     */
    public function getResponsibleEmployeeId()
    {
        if ($this->_responsibleEmployee === null) {
            if (Yii::$app->user->can(UserRole::ROLE_CHIEF)) {
                $this->_responsibleEmployee = 0;
            } elseif (Yii::$app->user->can(UserRole::ROLE_SUPERVISOR)) {
                $this->_responsibleEmployee = $this->company->getEmployeeCompanies()
                    ->select('employee_id')
                    ->andWhere([
                        'or',
                        ['employee_id' => Yii::$app->user->id],
                        ['employee_role_id' => UserRole::ROLE_MANAGER],
                    ])
                    ->column();
            } elseif (Yii::$app->user->can(UserRole::ROLE_MANAGER)) {
                $this->_responsibleEmployee = [Yii::$app->user->id];
            } else {
                $this->_responsibleEmployee = [];
            }
        }

        return $this->_responsibleEmployee;
    }
}
