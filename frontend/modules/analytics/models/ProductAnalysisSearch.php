<?php

namespace frontend\modules\analytics\models;

use common\components\date\DateHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use common\models\document\status\InvoiceStatus;
use frontend\models\Documents;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class ProductAnalysisSearch
 */
class ProductAnalysisSearch extends Model
{
    const MIN_REPORT_DATE = '2000-01-01';

    const GROUP_X = 1;
    const GROUP_Y = 2;
    const GROUP_Z = 3;

    const TMP_TABLE_MONTH = 'product_analysis_tmp_month';
    const TMP_TABLE_NAME = 'product_analysis_tmp';
    const TMP_TABLE_NAME_2 = 'product_analysis_tmp_2';

    protected $_company = null;
    protected $_offsetYear = 0;

    public $product_id;
    public $product_title;
    public $group_id;
    public $month_0;
    public $month_1;
    public $month_2;
    public $month_3;
    public $month_4;
    public $month_5;
    public $month_6;
    public $month_7;
    public $month_8;
    public $month_9;
    public $month_10;
    public $month_11;
    public $month_12;

    public static $months;

    public $totalProductTypes = null;
    public $totalProductQuantity = null;

    public static $groups = [
        self::GROUP_X => 'X',
        self::GROUP_Y => 'Y',
        self::GROUP_Z => 'Z',
    ];

    public static $groupsLabel = [
        self::GROUP_X => 'С самым устойчивым спросом (X)',
        self::GROUP_Y => 'С изменчивым объемом продаж (Y)',
        self::GROUP_Z => 'Прочие (Z)',
    ];

    public static $groupsColor = [
        self::GROUP_X => '#00c19b',
        self::GROUP_Y => '#fac031',
        self::GROUP_Z => '#e30611',
    ];

    protected $_query;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['product_id', 'group_id', 'year'], 'safe'],
        ];
    }

    public function setYear($year)
    {
        $this->_offsetYear = (int)$year - date('Y');
    }

    public function getYear()
    {
        return date('Y') + $this->_offsetYear;
    }

    public static function getMonthName($idx)
    {
        if (!empty(self::$months[$idx])) {
            $year = substr(self::$months[$idx], 0, 4);
            $month = substr(self::$months[$idx], -2, 2);
            if ($year && $month) {
                return (AbstractFinance::$month[$month] ?? '');
            }
        }

        return '---';
    }

    public static function getYearName($idx)
    {
        if (!empty(self::$months[$idx])) {
            return substr(self::$months[$idx], 0, 4);
        }

        return '---';
    }

    public static function getMonthValue($idx, $firstYearMonth, $value)
    {
        if (self::$months[$idx] >= $firstYearMonth)
            return round($value, $value == (int)$value ? 0 : 3);

        return '';
    }

    public static function _getFromCurrentMonthsPeriods($left, $right, $offsetYear = 0, $offsetMonth = "0")
    {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month")->modify("{$offsetMonth} month");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = $curr->format('Ym');
            $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }

    public function search($params = [])
    {
        $this->load($params);

        $months = self::$months = self::_getFromCurrentMonthsPeriods(12, 0, $this->_offsetYear);

        $sellsProductIds = (new Query())
            ->select('order.product_id')
            ->distinct()
            ->from('invoice')
            ->leftJoin('order', 'invoice.id = `order`.invoice_id')
            ->where(['invoice.company_id' => $this->company->id])
            ->andWhere([
                'invoice.type' => 2,
                'invoice.is_deleted' => 0,
                'invoice.invoice_status_id' => [6,7]
            ])
            ->andWhere(['between', 'invoice.document_date', self::_getDateFromYM($months[0]), self::_getDateFromYM($months[count($months)-1], true)])
            ->column(Yii::$app->db2);

        $_firstDateQuery1 = (new Query())
            ->select(new Expression('
                p.id AS product_id,
                DATE_FORMAT(IFNULL(pib.date, NOW()), "%Y%m") AS first_date
            '))
            ->from('product AS p')
            ->leftJoin('product_initial_balance AS pib', 'p.id = pib.product_id')
            ->andWhere(['p.id' => $sellsProductIds])
            ->groupBy(['p.id']);

        $_firstDateQuery2 = (new Query())
            ->select(new Expression('
                o.product_id,
                DATE_FORMAT(MIN(i.document_date), "%Y%m") AS first_date
            '))
            ->from('invoice AS i')
            ->leftJoin('order AS o', 'i.id = o.invoice_id')
            ->andWhere([
                'i.company_id' => $this->company->id,
                'i.type' => Documents::IO_TYPE_IN,
                'i.is_deleted' => false,
                'i.invoice_status_id' => [InvoiceStatus::STATUS_PAYED_PARTIAL, InvoiceStatus::STATUS_PAYED],
            ])
            ->groupBy(['product_id']);

        $query = (new Query())->select(new Expression("
            product_id,
            MIN(first_date) AS first_date
        "))->from(['a' => $_firstDateQuery2->union($_firstDateQuery1, true)])
        ->groupBy('product_id');

        $periodQuery = "
            select 0 AS quantity, '$months[12]' AS period UNION
            select 0 AS quantity, '$months[11]' AS period UNION
            select 0 AS quantity, '$months[10]' AS period UNION
            select 0 AS quantity, '$months[9]'  AS period UNION
            select 0 AS quantity, '$months[8]'  AS period UNION
            select 0 AS quantity, '$months[7]'  AS period UNION
            select 0 AS quantity, '$months[6]'  AS period UNION
            select 0 AS quantity, '$months[5]'  AS period UNION
            select 0 AS quantity, '$months[4]'  AS period UNION
            select 0 AS quantity, '$months[3]'  AS period UNION
            select 0 AS quantity, '$months[2]'  AS period UNION
            select 0 AS quantity, '$months[1]'  AS period UNION
            select 0 AS quantity, '$months[0]'  AS period
        ";

        $tableNameMonth = self::TMP_TABLE_MONTH;
        Yii::$app->db2->createCommand("CREATE TEMPORARY TABLE IF NOT EXISTS {{{$tableNameMonth}}} AS ({$periodQuery})")->execute();

        $query2 = (new Query())->select(new Expression("
            product_id,
            first_date,
            period,
            quantity
        "))->from($query)->join('CROSS JOIN', $tableNameMonth);

        $tableName = self::TMP_TABLE_NAME;
        Yii::$app->db2->createCommand("CREATE TEMPORARY TABLE IF NOT EXISTS {{{$tableName}}} AS ({$query2->createCommand()->rawSql})")->execute();

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $sellsQuantityQuery = (new Query())
            ->select(new Expression("
                $tableName.product_id,
                $tableName.period,
                $tableName.first_date,
                (SELECT SUM(order.quantity) FROM `order` LEFT JOIN invoice ON invoice.id = order.invoice_id
                    WHERE `order`.product_id = $tableName.product_id
                        AND invoice.company_id = ".$this->company->id."
                        AND invoice.type = 2
                        AND invoice.is_deleted = 0
                        AND invoice.invoice_status_id IN (6,7)
                        AND DATE_FORMAT(invoice.document_date, '%Y%m') = $tableName.period
                ) AS quantity
            "))
            ->from($tableName)
            ->groupBy(["$tableName.product_id", "$tableName.period"]);

        $tableName2 = self::TMP_TABLE_NAME_2;
        Yii::$app->db2->createCommand("CREATE TEMPORARY TABLE IF NOT EXISTS {{{$tableName2}}} AS ({$sellsQuantityQuery->createCommand()->rawSql})")->execute();
        Yii::$app->db2->createCommand("CREATE INDEX idx_product_analysis_tmp2_product ON {$tableName2}(product_id, period);")->execute();

        $query = (new Query())
            ->select(new Expression("
                product_id,
                /* product.title AS product_title, */
                /* product.production_type, */
                first_date,
                (SELECT quantity FROM $tableName2 m0  WHERE m0.product_id  = a.product_id AND m0.period  = '$months[0]')  month_0,
                (SELECT quantity FROM $tableName2 m1  WHERE m1.product_id  = a.product_id AND m1.period  = '$months[1]')  month_1,
                (SELECT quantity FROM $tableName2 m2  WHERE m2.product_id  = a.product_id AND m2.period  = '$months[2]')  month_2,
                (SELECT quantity FROM $tableName2 m3  WHERE m3.product_id  = a.product_id AND m3.period  = '$months[3]')  month_3,
                (SELECT quantity FROM $tableName2 m4  WHERE m4.product_id  = a.product_id AND m4.period  = '$months[4]')  month_4,
                (SELECT quantity FROM $tableName2 m5  WHERE m5.product_id  = a.product_id AND m5.period  = '$months[5]')  month_5,
                (SELECT quantity FROM $tableName2 m6  WHERE m6.product_id  = a.product_id AND m6.period  = '$months[6]')  month_6,
                (SELECT quantity FROM $tableName2 m7  WHERE m7.product_id  = a.product_id AND m7.period  = '$months[7]')  month_7,
                (SELECT quantity FROM $tableName2 m8  WHERE m8.product_id  = a.product_id AND m8.period  = '$months[8]')  month_8,
                (SELECT quantity FROM $tableName2 m9  WHERE m9.product_id  = a.product_id AND m9.period  = '$months[9]')  month_9,
                (SELECT quantity FROM $tableName2 m10 WHERE m10.product_id = a.product_id AND m10.period = '$months[10]') month_10,
                (SELECT quantity FROM $tableName2 m11 WHERE m11.product_id = a.product_id AND m11.period = '$months[11]') month_11,
                (SELECT quantity FROM $tableName2 m12 WHERE m12.product_id = a.product_id AND m12.period = '$months[12]') month_12,
                IF (first_date < '$months[12]', (SELECT 100 * STDDEV_SAMP(IFNULL(quantity,0)) / AVG(IFNULL(quantity,0)) FROM $tableName2 coef
                    WHERE coef.product_id = a.product_id AND coef.period >= coef.first_date AND coef.period < '".date('Ym')."'), 999999) variation_coefficient
            "))
            ->from(['a' => $tableName])
            /* ->leftJoin('product', 'product.id = a.product_id') */
            ->groupBy('product_id');

        $this->_query = clone $query;

        $query->andFilterWhere(['product_id' => $this->product_id]);

        if ($this->group_id) {
            switch ($this->group_id) {
                case self::GROUP_X:
                    $query->andHaving(['<=', 'variation_coefficient', 10]);
                    break;
                case self::GROUP_Y:
                    $query->andHaving(['and', ['>', 'variation_coefficient', 10], ['<=', 'variation_coefficient', 25]]);
                    break;
                case self::GROUP_Z:
                    $query->andHaving(['>', 'variation_coefficient', 25]);
                    break;
            }
        }

        $dataProvider = new ActiveDataProvider([
            'db' => \Yii::$app->db2,
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                //'product_title',
                'month_0',
                'month_1',
                'month_2',
                'month_3',
                'month_4',
                'month_5',
                'month_6',
                'month_7',
                'month_8',
                'month_9',
                'month_10',
                'month_11',
                'month_12',
                'variation_coefficient'
            ],
            'defaultOrder' => [
                'variation_coefficient' => SORT_ASC,
            ],
        ]);

        return $dataProvider;
    }

    public function getOverallResult()
    {
        $tableName = self::TMP_TABLE_NAME;

        //$subQuery = (new Query())
        //    ->select(new Expression("
        //        a.product_id,
        //        (SELECT 100 * STDDEV_SAMP(quantity) / AVG(quantity) FROM $tableName coef WHERE coef.product_id = a.product_id AND coef.period <> '".date('Ym')."') variation_coefficient
        //    "))
        //    ->from(['a' => $tableName]);

        $query = (new Query())
            ->select(new Expression("
                CASE
                    WHEN b.variation_coefficient IS NULL THEN 1
                    WHEN b.variation_coefficient <= 10 THEN 1
                    WHEN b.variation_coefficient > 25 THEN 3
                    ELSE 2
                END AS group_id,
                COUNT(DISTINCT(b.product_id)) AS product_types,
                (SELECT SUM(c.quantity) FROM product_store AS c WHERE b.product_id = c.product_id) product_quantity
            "))
            ->from(['b' => $this->_query])
            ->groupBy(['group_id']);

        $result = $query->all(Yii::$app->db2);

        $this->totalProductTypes = array_sum(array_column($result, 'product_types'));
        $this->totalProductQuantity = array_sum(array_column($result, 'product_quantity'));

        return $result;
    }

    public function getProductFilterItems()
    {
        $tableName = self::TMP_TABLE_NAME;

        $productArray = (new Query())
            ->select(new Expression('DISTINCT(product.id), product.title'))
            ->from(['a' => $tableName])
            ->leftJoin('product', 'product.id = a.product_id')
            ->orderBy(['product.title' => SORT_ASC])
            ->all(Yii::$app->db2);

        return ['' => 'Все'] + ArrayHelper::map($productArray, 'id', 'title');
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->_company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @return string
     */
    public function getGroupValue($i = null)
    {
        return isset(self::$groups[$i ?: $this->group]) ? self::$groups[$i ?: $this->group] : '';
    }

    /**
     * @return string
     */
    public function getGroupLabel($i = null)
    {
        return isset(self::$groupsLabel[$i ?: $this->group]) ? self::$groupsLabel[$i ?: $this->group] : '';
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $range = [];
        $cashOrderMinDate = CashOrderFlows::find()
            ->byCompany($company->id)
            ->min('date', Yii::$app->db2);
        $cashBankMinDate = CashBankFlows::find()
            ->byCompany($company->id)
            ->min('date', Yii::$app->db2);
        $cashEmoneyMinDate = CashEmoneyFlows::find()
            ->byCompany($company->id)
            ->min('date', Yii::$app->db2);
        $minDates = [];

        if ($cashOrderMinDate) $minDates[] = $cashOrderMinDate;
        if ($cashBankMinDate) $minDates[] = $cashBankMinDate;
        if ($cashEmoneyMinDate) $minDates[] = $cashEmoneyMinDate;

        $minCashDate = !empty($minDates) ? max(self::MIN_REPORT_DATE, min($minDates)) : date(DateHelper::FORMAT_DATE);
        $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
        $currentYear = date('Y');
        foreach (range($registrationYear, $currentYear) as $value) {
            $range[$value] = $value;
        }
        arsort($range);

        return $range;
    }

    ////////////////////////////////////////////// HELPERS ////////////////////////////////////////////////////////////

    private static function _getDateFromYM($ym, $endOfMonth = false)
    {
        if (strlen($ym) != 6 || $ym != (int)$ym)
            throw new Exception('_getDateFromYM: $ym not found');

        $year = substr($ym, 0, 4);
        $month = substr($ym, -2, 2);
        $day = ($endOfMonth) ? cal_days_in_month(CAL_GREGORIAN, $month, $year) : "01";

        return "{$year}-{$month}-{$day}";
    }
}
