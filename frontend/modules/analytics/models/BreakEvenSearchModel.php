<?php
namespace frontend\modules\analytics\models;

use Yii;
use yii\base\Model;
use common\models\Company;
use frontend\models\Documents;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\components\date\DateHelper;
use common\models\cash\CashEmoneyFlows;
use common\models\ExpenseItemFlowOfFunds;
use common\components\helpers\ArrayHelper;
use common\models\document\InvoiceIncomeItem;
use common\models\document\InvoiceExpenditureItem;
use frontend\modules\analytics\models\breakEven\BaseOlapTrait;

/**
 * Class BreakEvenSearchModel
 * @package frontend\modules\analytics\models
 */
class BreakEvenSearchModel extends Model
{
    use BaseOlapTrait;

    const TABLE_TURNOVER = 'product_turnover';
    const TABLE_OLAP_DOCS = 'olap_documents';
    const TABLE_OLAP_FLOWS = 'olap_flows';
    const TABLE_OLAP_INVOICES = 'olap_invoices';

    const MIN_REPORT_DATE = '2010-01-01';

    const BLOCK_VARIABLE_COSTS = 1;
    const BLOCK_FIXED_COSTS = 2;

    /**
     * @var Company
     */
    private $_company;

    public static $items = [
        [
            'label' => 'Выручка',
            'addCheckboxX' => true,
            'getter' => 'getRevenue',
            'options' => [
                'class' => 'revenue total incomes-block',
                'data-children' => 'revenue',
                'data-block' => 'revenue',
            ],
        ],
        [
            'label' => 'Переменные расходы',
            'addCheckboxX' => true,
            'getter' => 'getVariableCosts',
            'options' => [
                'class' => 'variable-costs total expenses-block',
                'data-block' => BreakEvenSearchModel::BLOCK_VARIABLE_COSTS,
                'data-children' => 'variable-costs',
            ]
        ],
        [
            'label' => 'Маржинальный доход',
            'addCheckboxX' => true,
            'getter' => 'getMarginalIncome',
            'options' => [
                'class' => 'marginal-income calculated',
                'data-block' => 'calculated',
            ],
        ],
        [
            'label' => 'Постоянные и операционные расходы',
            'addCheckboxX' => true,
            'getter' => 'getFixedCosts',
            'options' => [
                'class' => 'fixed-costs total expenses-block',
                'data-block' => BreakEvenSearchModel::BLOCK_FIXED_COSTS,
                'data-children' => 'fixed-costs',
            ],
        ],
        [
            'label' => 'Суммарные затраты',
            'addCheckboxX' => true,
            'getter' => 'getAllCosts',
            'options' => [
                'class' => 'all-costs calculated',
                'data-block' => 'calculated',
            ],
        ],
        [
            'label' => 'Операционная прибыль',
            'addCheckboxX' => true,
            'getter' => 'getOperatingProfit',
            'options' => [
                'class' => 'operating-profit calculated',
                'data-block' => 'calculated',
            ],
        ],
        [
            'label' => 'Рентабельность, %',
            'noCheckboxX' => true,
            'getter' => 'getEfficiency',
            'options' => [
                'class' => 'efficiency calculated',
                'data-block' => 'calculated',
            ],
        ],
    ];

    /**
     * @var
     */
    protected $_yearFilter;
    protected $_yearMonthFilter;

    /**
     * @var
     */
    public $year;
    public $month;

    /**
     * @var array
     */
    protected $data = [];

    protected $result = [];

    /**
     * @var ProfitAndLossSearchModel
     */
    protected $profitAndLossModel;

    protected $palData = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['yearMonth'], 'integer'],
        ]);
    }

    /**
     *
     */
    public function init() {

        $this->_company = \Yii::$app->user->identity->company;
        $this->year = date('Y');
        $this->month = date('m');
        $this->getYearFilter();
        $this->initProfitAndLossData();

        return parent::init();
    }

    /**
     * @return array
     */
    public function handleItems()
    {
        $this->result = [];
        foreach (static::$items as $item) {
            $getter = ArrayHelper::remove($item, 'getter');
            $item = $this->_getBlankParent($item);
            $this->result = array_merge($this->result, $this->$getter($item));
        }

        return $this->result;
    }

    private function initProfitAndLossData()
    {
        $this->profitAndLossModel = new ProfitAndLossSearchModel();
        $this->profitAndLossModel->activeTab = ProfitAndLossSearchModel::TAB_ALL_OPERATIONS;
        $this->profitAndLossModel->setYearFilter([$this->year]); // init

        $_blankParent = ProfitAndLossSearchModel::_getBlankParent([]);

        // pal revenue
        $_revenue = $this->profitAndLossModel->getRevenue($_blankParent, $this->year, $this->_company->id);
        $this->palData['totalRevenue'] = $_revenue['totalRevenue'];

        // pal variable costs
        $_variableCosts = $this->profitAndLossModel->getVariableCosts($_blankParent, $this->year, $this->_company->id);
        $this->palData['variableCosts'] = [];
        foreach ($_variableCosts as $_k => $_v) {
            if (substr($_k, 0, 7) == 'expense') {
                @list($_0, $itemId) = explode('-', $_k);
                if ($itemId) {
                    $this->palData['variableCosts'][$itemId] = $_v;
                    // subitems
                    if (!empty($_v['items'])) {
                        $this->palData['variableCosts'][$itemId]['items'] = [];
                        foreach ($_v['items'] as $_kk => $_vv) {
                            if (substr($_kk, 0, 7) == 'expense') {
                                @list($_0, $subItemId) = explode('-', $_kk);
                                if ($subItemId) {
                                    $this->palData['variableCosts'][$itemId]['items'][$subItemId] = $_vv;
                                }
                            }
                        }
                    }
                }
            } elseif ($_k === 'primeCost') {
                $this->palData['variableCosts']['primeCosts'] = $_v;
            }
        }

        // pal fixed costs + operating costs
        $_fixedCosts = $this->profitAndLossModel->getFixedCosts($_blankParent, $this->year, $this->_company->id);
        $_operatingCosts = $this->profitAndLossModel->getOperatingCosts($_blankParent, $this->year, $this->_company->id);
        $this->palData['fixedCosts'] = [];
        foreach (array_merge($_fixedCosts, $_operatingCosts) as $_k => $_v) {
            if (substr($_k, 0, 7) == 'expense') {
                @list($_0, $itemId) = explode('-', $_k);
                if ($itemId) {
                    $this->palData['fixedCosts'][$itemId] = $_v;
                    // subitems
                    if (!empty($_v['items'])) {
                        $this->palData['fixedCosts'][$itemId]['items'] = [];
                        foreach ($_v['items'] as $_kk => $_vv) {
                            if (substr($_kk, 0, 7) == 'expense') {
                                @list($_0, $subItemId) = explode('-', $_kk);
                                if ($subItemId) {
                                    $this->palData['fixedCosts'][$itemId]['items'][$subItemId] = $_vv;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// Getters
    ////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getRevenue($parentItem)
    {
        if (!array_key_exists('revenue', $this->data)) {

            $result = [
                'quantity' => $this->_getBlankItem('revenue quantity', 'Объем продаж, ед.', 'revenue', 'quantity'),
                'average_check' => $this->_getBlankItem('revenue average-check', 'Средний чек', 'revenue', 'average_check'),
            ];

            $dateFrom = $this->year.'-'.$this->month.'-01';
            $dateTo = $this->year.'-'.$this->month.'-'.cal_days_in_month(CAL_GREGORIAN, $this->month, $this->year);

            $tableTurnover = self::TABLE_TURNOVER;
            $companyId = $this->_company->id;

            //////////// BY DOC ////////////
            /// сумма всех Актов, ТН и УПД, дата которых в нужном месяце
            $docsData = \Yii::$app->db2->createCommand("
                SELECT 
                  DATE_FORMAT(t.date, '%Y%m') AS ym,
                  SUM(quantity) AS quantity,                  
                  SUM(total_amount) AS amount
                FROM {$tableTurnover} t 
                WHERE (t.company_id = {$companyId})
                    AND (t.type = 2)
                    AND (t.date BETWEEN '{$dateFrom}' AND '{$dateTo}')
                    AND (t.is_document_actual = TRUE)
                    AND (t.is_invoice_actual = TRUE)
                GROUP BY YEAR(t.date), MONTH(t.date)
            ")->queryAll();

            foreach ($docsData as $value) {
                $ym = $value['ym'];
                $parentItem[$ym] += round($value['amount'] / 100, 2);
                $result['quantity'][$ym] += round($value['quantity'], 0);
            }

            $flowsData = $this->__getRevenueQuantityByFlows($dateFrom, $dateTo);

            foreach ($flowsData as $value) {
                $ym = $value['ym'];
                $parentItem[$ym] += round($value['amount'] / 100, 2);
                $result['quantity'][$ym] += round($value['quantity'], 0);
            }

            // calc curr month average check
            $ym = $this->year . $this->month;
            if ($result['quantity'][$ym] > 0)
                $result['average_check'][$ym] = round(($parentItem[$ym] ?? 0) / $result['quantity'][$ym], 2);

            // PAL DATA ////////////////////////////////////////////////////////////////////////////////////////
            if ($palAmount = ArrayHelper::getValue($this->palData, "totalRevenue.amount.{$this->month}")) {
                $parentItem[$this->year . $this->month] = round($palAmount / 100, 2);
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////


            $this->data['revenue'] = ['totalRevenue' => $parentItem] + $result;
        }

        return $this->data['revenue'];
    }

    public function getVariableCosts($parentItem)
    {
        if (!array_key_exists('variableCosts', $this->data)) {

            // Себестоимость товаров/услуг
            $result = [
                'relativeVariableCosts' => $this->_getBlankItem('relative-variable-costs calculated text-italic', 'Переменные расходы на 1 руб. выручки', 'calculated'),
            ];

            $ym = $this->year . $this->month;
            foreach ($this->palData['variableCosts'] as $itemId => $itemPalData) {
                $key = ($itemId === 'primeCosts') ? 'primeCosts' : "expense-{$itemId}";
                $itemName = $itemPalData['label'] ?? "item_id={$itemId}";
                $result[$key] = self::_getBlankItem('variable-costs expenses-block', $itemName, null, $key);
                $result[$key]['canDrag'] = false;
                $result[$key]['data-block'] = BreakEvenSearchModel::BLOCK_VARIABLE_COSTS;
                $result[$key]['data-id'] = $itemId;

                $parentItem[$ym] += round(ArrayHelper::getValue($itemPalData, "amount.{$this->month}", 0) / 100, 2);
                $result[$key][$ym] += round(ArrayHelper::getValue($itemPalData, "amount.{$this->month}", 0) / 100, 2);

                // subitems
                if (!empty($itemPalData['items'])) {
                    $result[$key]['hasSubItems'] = 1;
                    foreach ($itemPalData['items'] as $subItemId => $subItemPalData) {
                        $key = "expense-{$subItemId}";
                        $subItemName = $subItemPalData['label'] ?? "subitem_id={$subItemId}";
                        $result[$key] = self::_getBlankItem('fixed-costs expenses-block', $subItemName, null, $key);
                        $result[$key]['hasParent'] = 1;
                        $result[$key]['canDrag'] = false;
                        $result[$key]['data-block'] = BreakEvenSearchModel::BLOCK_FIXED_COSTS;
                        $result[$key]['data-id'] = $subItemId;
                        $result[$key]['data-parent_id'] = $itemId;

                        $result[$key][$ym] += round(ArrayHelper::getValue($subItemPalData, "amount.{$this->month}", 0) / 100, 2);
                    }
                }
            }

            // Calc relative costs
            $prevYm = $this->year . $this->month;
            $ym = $this->year . $this->month;
            $revenuePrev = ArrayHelper::getValue($this->data['revenue'], "totalRevenue.{$prevYm}", 0);
            $revenue = ArrayHelper::getValue($this->data['revenue'], "totalRevenue.{$ym}", 0);
            $result['relativeVariableCosts'][$prevYm] = ($revenuePrev > 0) ? $parentItem[$prevYm] / $revenuePrev : 0;
            $result['relativeVariableCosts'][$ym] = ($revenue > 0) ? $parentItem[$ym] / $revenue : 0;

            $this->data['variableCosts'] = ['totalVariableCosts' => $parentItem] + $result;
        }

        return $this->data['variableCosts'];
    }

    public function getFixedCosts($parentItem)
    {
        if (!array_key_exists('fixedCosts', $this->data)) {

            $result = [
                'relativeFixedCosts' => $this->_getBlankItem('relative-fixed-costs calculated text-italic', 'Постоянные расходы на 1 руб. выручки', 'calculated'),
            ];

            $ym = $this->year . $this->month;
            foreach ($this->palData['fixedCosts'] as $itemId => $itemPalData) {
                $key = "expense-{$itemId}";
                $itemName = $itemPalData['label'] ?? "item_id={$itemId}";
                $result[$key] = self::_getBlankItem('fixed-costs expenses-block', $itemName, null, $key);
                $result[$key]['canDrag'] = false;
                $result[$key]['data-block'] = BreakEvenSearchModel::BLOCK_FIXED_COSTS;
                $result[$key]['data-id'] = $itemId;

                $parentItem[$ym] += round(ArrayHelper::getValue($itemPalData, "amount.{$this->month}", 0) / 100, 2);
                $result[$key][$ym] += round(ArrayHelper::getValue($itemPalData, "amount.{$this->month}", 0) / 100, 2);

                // subitems
                if (!empty($itemPalData['items'])) {
                    $result[$key]['hasSubItems'] = 1;
                    foreach ($itemPalData['items'] as $subItemId => $subItemPalData) {
                        $key = "expense-{$subItemId}";
                        $subItemName = $subItemPalData['label'] ?? "subitem_id={$subItemId}";
                        $result[$key] = self::_getBlankItem('fixed-costs expenses-block', $subItemName, null, $key);
                        $result[$key]['hasParent'] = 1;
                        $result[$key]['canDrag'] = false;
                        $result[$key]['data-block'] = BreakEvenSearchModel::BLOCK_FIXED_COSTS;
                        $result[$key]['data-id'] = $subItemId;
                        $result[$key]['data-parent_id'] = $itemId;

                        $result[$key][$ym] += round(ArrayHelper::getValue($subItemPalData, "amount.{$this->month}", 0) / 100, 2);
                    }
                }
            }

            // Calc relative costs
            $prevYm = $this->year . $this->month;
            $ym = $this->year . $this->month;
            $revenuePrev = ArrayHelper::getValue($this->data['revenue'], "totalRevenue.{$prevYm}", 0);
            $revenue = ArrayHelper::getValue($this->data['revenue'], "totalRevenue.{$ym}", 0);
            $result['relativeFixedCosts'][$prevYm] = ($revenuePrev > 0) ? $parentItem[$prevYm] / $revenuePrev : 0;
            $result['relativeFixedCosts'][$ym] = ($revenue > 0) ? $parentItem[$ym] / $revenue : 0;

            $this->data['fixedCosts'] = ['totalFixedCosts' => $parentItem] + $result;
        }

        return $this->data['fixedCosts'];
    }

    /* OLD with custom articles
    public function getVariableCosts($parentItem)
    {
        if (!array_key_exists('variableCosts', $this->data)) {

            // Себестоимость товаров/услуг
            $result = [
                'relativeVariableCosts' => $this->_getBlankItem('relative-variable-costs calculated text-italic', 'Переменные расходы на 1 руб. выручки', 'calculated'),
                'primeCosts' => $this->_getBlankItem('variable-costs expenses-block', 'Себестоимость товаров и услуг', null, 'prime-costs'),
            ];

            $dateFrom = $this->year.'-'.$this->month.'-01';
            $dateTo = $this->year.'-'.$this->month.'-'.cal_days_in_month(CAL_GREGORIAN, $this->month, $this->year);

            $docs = $this->__getPrimeCostsByDocs($dateFrom, $dateTo);
            $flows = $this->__getPrimeCostsByFlows($dateFrom, $dateTo);

            foreach ($docs as $value) {
                $ym = $value['y'].$value['m'];
                $parentItem[$ym] += round($value['amount'] / 100, 2);
                $result['primeCosts'][$ym] += round($value['amount'] / 100, 2);
            }

            foreach ($flows as $value) {
                $ym = $value['y'].$value['m'];
                $parentItem[$ym] += round($value['amount'] / 100, 2);
                $result['primeCosts'][$ym] += round($value['amount'] / 100, 2);
            }

            // Переменные расходы
            $_breakEvenCompanyItems = $this->getBreakEvenCompanyExpenditureItems(BreakEvenSearchModel::BLOCK_VARIABLE_COSTS);
            $_profitAndLossCompanyItems = $this->getCompanyExpenditureItems(ProfitAndLossCompanyItem::VARIABLE_EXPENSE_TYPE);
            $companyItems = ArrayHelper::merge($_breakEvenCompanyItems, $_profitAndLossCompanyItems);
            $ONLY_EXPENDITURE_ITEMS = array_keys($companyItems);

            $docs = $this->getUndestributedDocs($dateFrom, $dateTo, Documents::IO_TYPE_IN, $ONLY_EXPENDITURE_ITEMS);
            $flows = $this->getUndestributedFlows($dateFrom, $dateTo, CashFlowsBase::FLOW_TYPE_EXPENSE, $ONLY_EXPENDITURE_ITEMS);

            $companyItems = self::prependArticleNameWithParentName($companyItems, 'item');

            // break even custom chosed items
            foreach (array_keys($_breakEvenCompanyItems) as $itemId) {
                if ($itemId == -1)
                    continue;
                $key = "expense-{$itemId}";
                $companyItem = $companyItems[$itemId]['item'] ?? null;
                $itemName = $companyItem['name'] ?? "item_id={$itemId}";
                $result[$key] = self::_getBlankItem('variable-costs expenses-block', $itemName, null, $key);
                $result[$key]['canDrag'] = $companyItem['can_update'] ?? false;
                $result[$key]['data-block'] = BreakEvenSearchModel::BLOCK_VARIABLE_COSTS;
                $result[$key]['data-id'] = $itemId;
            }

            foreach ($docs as $value) {
                $ym = $value['y'].$value['m'];
                $key = "expense-{$value['item_id']}";
                $itemId = $value['item_id'];

                // profit and loss not empty only items
                if (!isset($result[$key])) {
                    $companyItem = $companyItems[$itemId]['item'] ?? null;
                    $itemName = $companyItem['name'] ?? "item_id={$itemId}";
                    $result[$key] = self::_getBlankItem('variable-costs expenses-block', $itemName, null, $key);
                    $result[$key]['canDrag'] = $companyItem['can_update'] ?? false;
                    $result[$key]['data-block'] = BreakEvenSearchModel::BLOCK_VARIABLE_COSTS;
                    $result[$key]['data-id'] = $itemId;
                }

                $parentItem[$ym] += round($value['amount'] / 100, 2);
                $result[$key][$ym] += round($value['amount'] / 100, 2);
            }

            foreach ($flows as $value) {
                $ym = $value['y'].$value['m'];
                $key = "expense-{$value['item_id']}";
                $itemId = $value['item_id'];

                // profit and loss not empty only items
                if (!isset($result[$key])) {
                    $companyItem = $companyItems[$itemId]['item'] ?? null;
                    $itemName = $companyItem['name'] ?? "item_id={$itemId}";
                    $result[$key] = self::_getBlankItem('variable-costs expenses-block', $itemName, null, $key);
                    $result[$key]['canDrag'] = $companyItem['can_update'] ?? false;
                    $result[$key]['data-block'] = BreakEvenSearchModel::BLOCK_VARIABLE_COSTS;
                    $result[$key]['data-id'] = $itemId;
                }

                $parentItem[$ym] += round($value['amount'] / 100, 2);
                $result[$key][$ym] += round($value['amount'] / 100, 2);
            }

            // Calc relative costs
            $prevYm = $this->year . $this->month;
            $ym = $this->year . $this->month;
            $revenuePrev = ArrayHelper::getValue($this->data['revenue'], "totalRevenue.{$prevYm}", 0);
            $revenue = ArrayHelper::getValue($this->data['revenue'], "totalRevenue.{$ym}", 0);
            $result['relativeVariableCosts'][$prevYm] = ($revenuePrev > 0) ? $parentItem[$prevYm] / $revenuePrev : 0;
            $result['relativeVariableCosts'][$ym] = ($revenue > 0) ? $parentItem[$ym] / $revenue : 0;

            // Add article btn
            $result['addToVariableCosts'] = $this->_getBlankItem('add-article-btn to-variable-costs', 'Добавить статью', 'add-article-btn');

            $this->data['variableCosts'] = ['totalVariableCosts' => $parentItem] + $result;
        }

        return $this->data['variableCosts'];
    }
    */

    /* OLD with custom articles
    public function getFixedCosts($parentItem)
    {
        if (!array_key_exists('fixedCosts', $this->data)) {

            $result = [
                'relativeFixedCosts' => $this->_getBlankItem('relative-fixed-costs calculated text-italic', 'Постоянные расходы на 1 руб. выручки', 'calculated'),
            ];

            $dateFrom = $this->year.'-'.$this->month.'-01';
            $dateTo = $this->year.'-'.$this->month.'-'.cal_days_in_month(CAL_GREGORIAN, $this->month, $this->year);

            // Постоянные расходы
            $_breakEvenCompanyItems = $this->getBreakEvenCompanyExpenditureItems(BreakEvenSearchModel::BLOCK_FIXED_COSTS);
            $_profitAndLossCompanyItems = $this->getCompanyExpenditureItems([ProfitAndLossCompanyItem::OPERATING_EXPENSE_TYPE, ProfitAndLossCompanyItem::CONSTANT_EXPENSE_TYPE]);
            $companyItems = ArrayHelper::merge($_breakEvenCompanyItems, $_profitAndLossCompanyItems);
            $ONLY_EXPENDITURE_ITEMS = array_keys($companyItems);

            $docs = $this->getUndestributedDocs($dateFrom, $dateTo, Documents::IO_TYPE_IN, $ONLY_EXPENDITURE_ITEMS);
            $flows = $this->getUndestributedFlows($dateFrom, $dateTo, CashFlowsBase::FLOW_TYPE_EXPENSE, $ONLY_EXPENDITURE_ITEMS);

            $companyItems = self::prependArticleNameWithParentName($companyItems, 'item');

            // break even custom chosed items
            foreach (array_keys($_breakEvenCompanyItems) as $itemId) {
                if ($itemId == -1)
                    continue;
                $key = "expense-{$itemId}";
                $companyItem = $companyItems[$itemId]['item'] ?? null;
                $itemName = $companyItem['name'] ?? "item_id={$itemId}";
                $result[$key] = self::_getBlankItem('fixed-costs expenses-block', $itemName, null, $key);
                $result[$key]['canDrag'] = $companyItem['can_update'] ?? false;
                $result[$key]['data-block'] = BreakEvenSearchModel::BLOCK_FIXED_COSTS;
                $result[$key]['data-id'] = $itemId;
            }

            foreach ($docs as $value) {
                $ym = $value['y'].$value['m'];
                $key = "expense-{$value['item_id']}";
                $itemId = $value['item_id'];

                // profit and loss not empty only items
                if (!isset($result[$key])) {
                    $companyItem = $companyItems[$itemId]['item'] ?? null;
                    $itemName = $companyItem['name'] ?? "item_id={$itemId}";
                    $result[$key] = self::_getBlankItem('fixed-costs expenses-block', $itemName, null, $key);
                    $result[$key]['canDrag'] = $companyItem['can_update'] ?? false;
                    $result[$key]['data-block'] = BreakEvenSearchModel::BLOCK_FIXED_COSTS;
                    $result[$key]['data-id'] = $itemId;
                }

                $parentItem[$ym] += round($value['amount'] / 100, 2);
                $result[$key][$ym] += round($value['amount'] / 100, 2);
            }

            foreach ($flows as $value) {
                $ym = $value['y'].$value['m'];
                $key = "expense-{$value['item_id']}";
                $itemId = $value['item_id'];

                // profit and loss not empty items
                if (!isset($result[$key])) {
                    $companyItem = $companyItems[$itemId]['item'] ?? null;
                    $itemName = $companyItem['name'] ?? "item_id={$itemId}";
                    $result[$key] = self::_getBlankItem('fixed-costs expenses-block', $itemName, null, $key);
                    $result[$key]['canDrag'] = $companyItem['can_update'] ?? false;
                    $result[$key]['data-block'] = BreakEvenSearchModel::BLOCK_FIXED_COSTS;
                    $result[$key]['data-id'] = $itemId;
                }

                $parentItem[$ym] += round($value['amount'] / 100, 2);
                $result[$key][$ym] += round($value['amount'] / 100, 2);
            }

            // Calc relative costs
            $prevYm = $this->year . $this->month;
            $ym = $this->year . $this->month;
            $revenuePrev = ArrayHelper::getValue($this->data['revenue'], "totalRevenue.{$prevYm}", 0);
            $revenue = ArrayHelper::getValue($this->data['revenue'], "totalRevenue.{$ym}", 0);
            $result['relativeFixedCosts'][$prevYm] = ($revenuePrev > 0) ? $parentItem[$prevYm] / $revenuePrev : 0;
            $result['relativeFixedCosts'][$ym] = ($revenue > 0) ? $parentItem[$ym] / $revenue : 0;

            // Add article btn
            $result['addToFixedCosts'] = $this->_getBlankItem('add-article-btn to-fixed-costs', 'Добавить статью', 'add-article-btn');

            $this->data['fixedCosts'] = ['totalFixedCosts' => $parentItem] + $result;
        }

        return $this->data['fixedCosts'];
    }
    */

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getMarginalIncome($parentItem)
    {
        $result = [
            'marginality' => $this->_getBlankItem('marginality calculated', 'Маржинальность, %', 'calculated'),
        ];

        $prevYm = $this->year . $this->month;
        $ym = $this->year . $this->month;

        $revenuePrev = ArrayHelper::getValue($this->data['revenue'], "totalRevenue.{$prevYm}", 0);
        $revenue = ArrayHelper::getValue($this->data['revenue'], "totalRevenue.{$ym}", 0);
        $variableCostsPrev = ArrayHelper::getValue($this->data['variableCosts'], "totalVariableCosts.{$prevYm}", 0);
        $variableCosts = ArrayHelper::getValue($this->data['variableCosts'], "totalVariableCosts.{$ym}", 0);

        $parentItem[$prevYm] = $revenuePrev - $variableCostsPrev;
        $parentItem[$ym] = $revenue - $variableCosts;
        $result['marginality'][$prevYm] = ($revenuePrev > 0) ? (100 * ($revenuePrev - $variableCostsPrev) / $revenuePrev) : 0;
        $result['marginality'][$ym] = ($revenue > 0) ? (100 * ($revenue - $variableCosts) / $revenue) : 0;

        $this->data['marginalIncome'] = ['totalMarginalIncome' => $parentItem] + $result;

        return $this->data['marginalIncome'];
    }

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getAllCosts($parentItem)
    {
        $result = [
            'relativeAllCosts' => $this->_getBlankItem('relative-all-costs calculated', 'Суммарные затраты на 1 руб. выручки', 'calculated'),
        ];

        $prevYm = $this->year . $this->month;
        $ym = $this->year . $this->month;

        $revenuePrev = ArrayHelper::getValue($this->data['revenue'], "totalRevenue.{$prevYm}", 0);
        $revenue = ArrayHelper::getValue($this->data['revenue'], "totalRevenue.{$ym}", 0);
        $variableCostsPrev = ArrayHelper::getValue($this->data['variableCosts'], "totalVariableCosts.{$prevYm}", 0);
        $variableCosts = ArrayHelper::getValue($this->data['variableCosts'], "totalVariableCosts.{$ym}", 0);
        $fixedCostsPrev = ArrayHelper::getValue($this->data['fixedCosts'], "totalFixedCosts.{$prevYm}", 0);
        $fixedCosts = ArrayHelper::getValue($this->data['fixedCosts'], "totalFixedCosts.{$ym}", 0);

        $parentItem[$prevYm] = $fixedCostsPrev + $variableCostsPrev;
        $parentItem[$ym] = $fixedCosts + $variableCosts;
        $result['relativeAllCosts'][$prevYm] = ($revenuePrev > 0) ? (( $fixedCostsPrev + $variableCostsPrev) / $revenuePrev) : 0;
        $result['relativeAllCosts'][$ym] = ($revenue > 0) ? (($fixedCosts + $variableCosts) / $revenue) : 0;

        $this->data['allCosts'] = ['totalAllCosts' => $parentItem] + $result;

        return $this->data['allCosts'];
    }

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getOperatingProfit($parentItem)
    {
        $result = [
            'relativeOperatingProfit' => $this->_getBlankItem('relative-operating-profit calculated', 'Операционная прибыль на 1 руб. выручки', 'calculated'),
        ];

        $prevYm = $this->year . $this->month;
        $ym = $this->year . $this->month;

        $revenuePrev = ArrayHelper::getValue($this->data['revenue'], "totalRevenue.{$prevYm}", 0);
        $revenue = ArrayHelper::getValue($this->data['revenue'], "totalRevenue.{$ym}", 0);
        $marginalIncomePrev = ArrayHelper::getValue($this->data['marginalIncome'], "totalMarginalIncome.{$prevYm}", 0);
        $marginalIncome = ArrayHelper::getValue($this->data['marginalIncome'], "totalMarginalIncome.{$ym}", 0);
        $fixedCostsPrev = ArrayHelper::getValue($this->data['fixedCosts'], "totalFixedCosts.{$prevYm}", 0);
        $fixedCosts = ArrayHelper::getValue($this->data['fixedCosts'], "totalFixedCosts.{$ym}", 0);

        $parentItem[$prevYm] = $marginalIncomePrev - $fixedCostsPrev;
        $parentItem[$ym] = $marginalIncome - $fixedCosts;
        $result['relativeOperatingProfit'][$prevYm] = ($revenuePrev > 0) ? (($marginalIncomePrev - $fixedCostsPrev) / $revenuePrev) : 0;
        $result['relativeOperatingProfit'][$ym] = ($revenue > 0) ? (($marginalIncome - $fixedCosts) / $revenue) : 0;

        $this->data['operatingProfit'] = ['totalOperatingProfit' => $parentItem] + $result;

        return $this->data['operatingProfit'];
    }

    /**
     * @param $parentItem
     * @return mixed
     */
    public function getEfficiency($parentItem)
    {
        $result = [];

        $prevYm = $this->year . $this->month;
        $ym = $this->year . $this->month;

        $revenuePrev = ArrayHelper::getValue($this->data['revenue'], "totalRevenue.{$prevYm}", 0);
        $revenue = ArrayHelper::getValue($this->data['revenue'], "totalRevenue.{$ym}", 0);
        $operatingProfitPrev = ArrayHelper::getValue($this->data['operatingProfit'], "totalOperatingProfit.{$prevYm}", 0);
        $operatingProfit = ArrayHelper::getValue($this->data['operatingProfit'], "totalOperatingProfit.{$ym}", 0);

        $parentItem[$prevYm] = ($revenuePrev > 0) ? (100 * $operatingProfitPrev / $revenuePrev) : 0;
        $parentItem[$ym] = ($revenue > 0) ? (100 * $operatingProfit / $revenue) : 0;

        $this->data['efficiency'] = ['totalEfficiency' => $parentItem] + $result;

        return $this->data['efficiency'];
    }


    public function getFromCurrentMonthsPeriods($left, $right, $offsetYear = 0, $offsetMonth = "0") {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month")->modify("{$offsetMonth} month");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr->format('Y-m-01'),
                'to' => $curr->format('Y-m-t'),
            ];
            $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        if (empty($this->_yearFilter)) {
            $range = [];
            $rangeYM = [];
            $cashOrderMinDate = CashOrderFlows::find()
                ->byCompany($this->company->id)
                ->min('date');
            $cashBankMinDate = CashBankFlows::find()
                ->byCompany($this->company->id)
                ->min('date');
            $cashEmoneyMinDate = CashEmoneyFlows::find()
                ->byCompany($this->company->id)
                ->min('date');
            $minDates = [];

            if ($cashOrderMinDate) $minDates[] = $cashOrderMinDate;
            if ($cashBankMinDate) $minDates[] = $cashBankMinDate;
            if ($cashEmoneyMinDate) $minDates[] = $cashEmoneyMinDate;

            $minCashDate = !empty($minDates) ? max(self::MIN_REPORT_DATE, min($minDates)) : date(DateHelper::FORMAT_DATE);
            $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
            $currentYear = date('Y');
            foreach (range($registrationYear, $currentYear) as $value) {
                $range[$value] = $value;
                foreach (AbstractFinance::$month as $v => $monthName) {
                    if (($value . $v) <= date('Ym'))
                        $rangeYM[$value . $v] = $value . ' ' . $monthName;
                }
            }
            arsort($range);
            $this->_yearFilter = $range;
            $this->_yearMonthFilter = $rangeYM;
        }

        return $this->_yearFilter;
    }

    public function getYearMonthFilter()
    {
        if (empty($this->_yearMonthFilter)) {
            $this->getYearFilter();
        }

        return $this->_yearMonthFilter;
    }

    private function _getBlankParent($item)
    {
        $item[$this->year . $this->month] = 0;

        return $item;
    }

    private function _getBlankItem($class, $name, $dataBlock = null, $dataArticle = null)
    {
        return [
            'label' => htmlspecialchars($name),
            'options' => [
                'class' => $class,
                'data-block' => $dataBlock,
                'data-article' => $dataArticle,
            ],
            $this->year . $this->month => 0,
        ];
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @return bool
     */
    public function getIsCurrentYear()
    {
        return date('Y') == $this->year;
    }


    public function setYearMonth($value)
    {
        $y = substr($value, 0, 4);
        $m = substr($value, 4, 2);
        if (checkdate($m, 1, $y)) {
            $this->year = $y;
            $this->month = $m;

            return true;
        }

        return false;
    }

    public function getYearMonth()
    {
        return $this->year . $this->month;
    }

    /**
     * @param $blockType
     * @return array|\yii\db\ActiveRecord[]
     */
    private function getBreakEvenCompanyExpenditureItems($blockType)
    {
        $result = ExpenseItemFlowOfFunds::find()
            ->joinWith('item')
            ->andWhere(['expense_item_flow_of_funds.company_id' => $this->company->id])
            ->andWhere(['expense_item_flow_of_funds.break_even_block' => $blockType])
            ->indexBy(['expense_item_id'])
            ->orderBy(InvoiceExpenditureItem::tableName() . '.name')
            ->asArray()
            ->all(\Yii::$app->db2);

        return (!empty($result)) ? $result : [-1 => ['item' => ['id' => -1, 'parent_id' => null, 'name' => 'ничего не найдено']]];
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function __getRevenueQuantityByFlows($dateFrom, $dateTo)
    {
        $flowsTable = self::TABLE_OLAP_FLOWS;
        $companyId = $this->_company->id;

        //////////// BY NO DOC ////////////
        ///  1) есть привязка к счетам, у которых НЕТ Актов/ТН/УПД или у них стоит "Без Акта/ТН/УПД" и "Дата признания дохода" в нужном месяце
        $exceptIncomeFlowsArticles = implode(',', BaseOlapTrait::getExceptUndestributedIncomeItems());

        $subQuery = "
            SELECT 
              t.recognition_date,
              r.invoice_id
            FROM {$flowsTable} t
            LEFT JOIN `cash_bank_flow_to_invoice` r ON r.flow_id = t.id              
            WHERE t.company_id = {$companyId}
              AND t.wallet = 1
              AND t.type = 1
              AND t.item_id NOT IN ({$exceptIncomeFlowsArticles})
              AND t.has_invoice = TRUE AND (t.has_doc = FALSE OR t.need_doc = FALSE)
            
            UNION
            
            SELECT 
              t.recognition_date,
              r.invoice_id
            FROM {$flowsTable} t
            LEFT JOIN `cash_order_flow_to_invoice` r ON r.flow_id = t.id             
            WHERE t.company_id = {$companyId}
              AND t.wallet = 2
              AND t.type = 1
              AND t.item_id NOT IN ({$exceptIncomeFlowsArticles})
              AND t.has_invoice = TRUE AND (t.has_doc = FALSE OR t.need_doc = FALSE)
              
            UNION
            
            SELECT 
              t.recognition_date,
              r.invoice_id
            FROM {$flowsTable} t
            LEFT JOIN `cash_emoney_flow_to_invoice` r ON r.flow_id = t.id             
            WHERE t.company_id = {$companyId}
              AND t.wallet = 3            
              AND t.type = 1
              AND t.item_id NOT IN ({$exceptIncomeFlowsArticles})
              AND t.has_invoice = TRUE AND (t.has_doc = FALSE OR t.need_doc = FALSE)
        ";

        $query = "
            SELECT 
              DATE_FORMAT(t.recognition_date, '%Y%m') ym,
              SUM(o.selling_price_with_vat * o.quantity) AS amount,     
              SUM(o.quantity) AS quantity
              FROM (
                SELECT 
                  tt.invoice_id, 
                  MAX(tt.recognition_date) recognition_date
                FROM ({$subQuery}) tt GROUP BY tt.invoice_id) t
              LEFT JOIN `order` o ON t.invoice_id = o.invoice_id
              LEFT JOIN `product` p ON p.id = o.product_id
              WHERE t.recognition_date BETWEEN '{$dateFrom}' AND '{$dateTo}'
              GROUP BY YEAR(t.recognition_date), MONTH(t.recognition_date)
        ";

        return Yii::$app->db2->createCommand($query)->queryAll();
    }

    /**
     * @param $array
     * @param $itemKey
     * @param int $type
     * @return array
     */
    private static function prependArticleNameWithParentName($array, $itemKey, $type = CashFlowsBase::FLOW_TYPE_EXPENSE)
    {
        foreach ($array as $key => $value) {
            if ($value[$itemKey]['parent_id']) {
                $class = ($type == CashFlowsBase::FLOW_TYPE_EXPENSE ? InvoiceExpenditureItem::class : InvoiceIncomeItem::class);
                if ($parentName = $class::findOne($value[$itemKey]['parent_id'])->name ?? '')
                    $array[$key][$itemKey]['name'] = $parentName .' -> '. $value[$itemKey]['name'];
            }
        }

        uasort($array, function ($a, $b) use ($itemKey) {
            return $a[$itemKey]['name'] <=> $b[$itemKey]['name'];
        });

        return $array;
    }
}