<?php
namespace frontend\modules\analytics\models\odds;

trait SortTrait {

    public function sortItems(&$data)
    {
        foreach ($data as $k1 => $d1)
            if (isset($d1['levels']))
                foreach ($d1['levels'] as $k2 => $d2)
                    if (isset($d2['levels']))
                        uasort($data[$k1]['levels'][$k2]['levels'], function ($a, $b) {
                            if ($a['title'] == $b['title']) return 0;
                            return ($a['title'] < $b['title']) ? -1 : 1;
                        });
    }

    public function sortSubItems(&$data)
    {
        foreach ($data as $k1 => $d1)
            if (isset($d1['levels']))
                foreach ($d1['levels'] as $k2 => $d2)
                    if (isset($d2['levels']))
                        foreach ($d2['levels'] as $k3 => $d3)
                            if (!empty($d3['levels']))
                                uasort($data[$k1]['levels'][$k2]['levels'][$k3]['levels'], function ($a, $b) {
                                    if ($a['title'] == $b['title']) return 0;
                                    return ($a['title'] < $b['title']) ? -1 : 1;
                                });
    }

    public function glueSubItemsByNames(&$data)
    {
        foreach ($data as $k1 => $d1)
            if (isset($d1['levels']))
                foreach ($d1['levels'] as $k2 => $d2)
                    if (isset($d2['levels']))
                        foreach ($d2['levels'] as $k3 => $d3)
                            if (!empty($d3['levels'])) {
                                $tmpK4 = null;
                                $tmpD4 = null;
                                foreach ($d3['levels'] as $k4 => $d4) {

                                    if ($tmpK4 === null) {
                                        $tmpK4 = $k4;
                                        $tmpD4 = $d4;
                                        continue;
                                    }

                                    if (trim($tmpD4['title']) === trim($d4['title'])) {
                                        if (!isset($data[$k1]['levels'][$k2]['levels'][$k3]['levels'][$k4]['consolidatedSubItemsIds'])) {
                                            $data[$k1]['levels'][$k2]['levels'][$k3]['levels'][$k4]['consolidatedSubItemsIds'] = [$k4];
                                        }
                                        $data[$k1]['levels'][$k2]['levels'][$k3]['levels'][$k4]['consolidatedSubItemsIds'][] = $tmpK4;
                                        unset($data[$k1]['levels'][$k2]['levels'][$k3]['levels'][$tmpK4]);

                                        foreach ($tmpD4['data'] as $k5 => $d5)
                                            if (isset($data[$k1]['levels'][$k2]['levels'][$k3]['levels'][$k4]['data'][$k5]))
                                                $data[$k1]['levels'][$k2]['levels'][$k3]['levels'][$k4]['data'][$k5] += $d5;
                                    }

                                    $tmpD4 = $d4;
                                    $tmpK4 = $k4;
                                }
                            }
    }
}