<?php
namespace frontend\modules\analytics\models\odds;

use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use Yii;
use yii\base\Exception;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashFlowsBase;
use common\modules\acquiring\models\AcquiringOperation;
use common\modules\cards\models\CardOperation;
use frontend\modules\analytics\models\AbstractFinance;

trait SearchByPurseDetailedTrait {

    public function searchByPurseDetailed($params = [])
    {
        $rawData = $params['rawData'] ?? $this->getByPurseRawDataDetailed($params);
        $data = self::getByPurseEmptyDataDetailed();
        $growingData = self::getByPurseEmptyGrowingDataDetailed();
        $totalData = self::getEmptyTotalData();

        $subArticles = [
            CashFlowsBase::FLOW_TYPE_INCOME => $this->getArticles2Parents(CashFlowsBase::FLOW_TYPE_INCOME),
            CashFlowsBase::FLOW_TYPE_EXPENSE => $this->getArticles2Parents(CashFlowsBase::FLOW_TYPE_EXPENSE),
        ];

        foreach ($rawData as $d) {

            $walletTypeID = $d['wallet'];
            $walletID = $d['wallet_id'];
            $type = $d['type'];
            $item = $d['item_id'];
            $m = $d['m'];
            $flowKoef = ($d['type'] == 0 ? -1 : 1);
            $amount = $d['amount'];
            $growingAmount = $d['growing_amount'];

            if (!isset($data[$walletTypeID]['children'][$walletID])) {
                // todo: skip non exists rs
                continue;
            }

            if (in_array($d['contractor_id'], ['bank', 'order', 'emoney'])) {
                $item = $d['contractor_id'];
            }

            $subItem = null;
            if (isset($subArticles[$d['type']][$d['item_id']])) {
                $item = $subArticles[$d['type']][$d['item_id']];
                $subItem = $d['item_id'];
            }

            if (!isset($data[$walletTypeID]['children'][$walletID]['levels'][$type]['levels'][$item])) {
                $data[$walletTypeID]['children'][$walletID]['levels'][$type]['levels'][$item] = [
                    'title' => $this->__getCashContractorName($d['type'], $item, $d['contractor_id']),
                    'data' => self::EMPTY_YEAR
                ];
            }

            if ($subItem) {
                if (!isset( $data[$walletTypeID]['children'][$walletID]['levels'][$type]['levels'][$item]['levels'][$subItem])) {
                    $data[$walletTypeID]['children'][$walletID]['levels'][$type]['levels'][$item]['levels'][$subItem] = [
                        'title' => $this->__getCashContractorName($d['type'], $subItem),
                        'data' => self::EMPTY_YEAR
                    ];
                }
                $data[$walletTypeID]['children'][$walletID]['levels'][$type]['levels'][$item]['levels'][$subItem]['data'][$m] += $amount;
            }

            $data[$walletTypeID]['data'][$m] += $flowKoef * $amount;
            $data[$walletTypeID]['children'][$walletID]['data'][$m] += $flowKoef * $amount;
            $data[$walletTypeID]['children'][$walletID]['levels'][$type]['data'][$m] += $amount;
            $data[$walletTypeID]['children'][$walletID]['levels'][$type]['levels'][$item]['data'][$m] += $amount;

            $growingData[$walletTypeID]['data'][$m] += $flowKoef * $growingAmount;
            $growingData[$walletTypeID]['children'][$walletID]['data'][$m] += $flowKoef * $growingAmount;

            $totalData['month']['data'][$m] += $flowKoef * $amount;
            $totalData['growing']['data'][$m] += $flowKoef * $growingAmount;
        }

        // remove empty (has growing amount, but hasn't amount in current year)
        foreach ($data as $k0 => $d0)
            foreach ($d0['children'] as $k1 => $d1)
                foreach ($d1['levels'] as $k2 => $d2)
                    foreach ($d2['levels'] as $k3 => $d3)
                        if (array_sum($d3['data']) == 0)
                            unset($data[$k0]['children'][$k1]['levels'][$k2]['levels'][$k3]);

        // sort
        foreach ($data as $k0 => $d0) {
            foreach ($d0['children'] as $k1 => $d1) {
                $this->sortItems($data[$k0]['children']);
                $this->sortSubItems($data[$k0]['children']);
            }
        }

        return [
            'data' => $data,
            'growingData' => $growingData,
            'totalData' => $totalData
        ];
    }

    public function searchByPurseDaysDetailed($params = [])
    {
        $rawDataArr = $this->getByPurseRawDataDetailedDay($params);
        $data = self::getByPurseEmptyDataDetailed(true);
        $growingData = self::getByPurseEmptyGrowingDataDetailed(true);
        $totalData = self::getEmptyTotalData(true);

        $subArticles = [
            CashFlowsBase::FLOW_TYPE_INCOME => $this->getArticles2Parents(CashFlowsBase::FLOW_TYPE_INCOME),
            CashFlowsBase::FLOW_TYPE_EXPENSE => $this->getArticles2Parents(CashFlowsBase::FLOW_TYPE_EXPENSE),
        ];

        // start year balance
        foreach ($rawDataArr['growingStart'] as $d) {

            // skip if rs was changed
            if (isset($growingData[$d['wallet']]['children'][$d['wallet_id']])) {
                foreach ($growingData[$d['wallet']]['children'][$d['wallet_id']]['data'] as $k => &$g) {
                    $g += $d['amount'];
                }
            }
            foreach ($growingData[$d['wallet']]['data'] as $k => &$g) {
                $g += $d['amount'];
            }
            foreach ($totalData['growing']['data'] as &$t) {
                $t += $d['amount'];
            }
        }

        foreach ($rawDataArr['flat'] as $d) {

            $walletTypeID = $d['wallet'];
            $walletID = $d['wallet_id'];
            $type = $d['type'];
            $item = $d['item_id'];
            $m = $d['m'].$d['d'];
            $flowKoef = ($d['type'] == 0 ? -1 : 1);
            $amount = $d['amount'];

            if (!isset($data[$walletTypeID]['children'][$walletID])) {
                // todo: skip non exists rs
                continue;
            }

            if (in_array($d['contractor_id'], ['bank', 'order', 'emoney'])) {
                $item = $d['contractor_id'];
            }

            $subItem = null;
            if (isset($subArticles[$d['type']][$d['item_id']])) {
                $item = $subArticles[$d['type']][$d['item_id']];
                $subItem = $d['item_id'];
            }

            if (!isset($data[$walletTypeID]['children'][$walletID]['levels'][$type]['levels'][$item])) {
                $data[$walletTypeID]['children'][$walletID]['levels'][$type]['levels'][$item] = [
                    'title' => $this->__getCashContractorName($d['type'], $item, $d['contractor_id']),
                    'data' => self::getEmptyYearInDays($this->year)
                ];
            }

            if ($subItem) {
                if (!isset( $data[$walletTypeID]['children'][$walletID]['levels'][$type]['levels'][$item]['levels'][$subItem])) {
                    $data[$walletTypeID]['children'][$walletID]['levels'][$type]['levels'][$item]['levels'][$subItem] = [
                        'title' => $this->__getCashContractorName($d['type'], $subItem),
                        'data' => self::getEmptyYearInDays($this->year)
                    ];
                }
                $data[$walletTypeID]['children'][$walletID]['levels'][$type]['levels'][$item]['levels'][$subItem]['data'][$m] += $amount;
            }

            $data[$walletTypeID]['data'][$m] += $flowKoef * $amount;
            $data[$walletTypeID]['children'][$walletID]['data'][$m] += $flowKoef * $amount;
            $data[$walletTypeID]['children'][$walletID]['levels'][$type]['data'][$m] += $amount;
            $data[$walletTypeID]['children'][$walletID]['levels'][$type]['levels'][$item]['data'][$m] += $amount;
            $totalData['month']['data'][$m] += $flowKoef * $amount;

            // GROWING AMOUNT
            foreach ($growingData[$walletTypeID]['children'][$walletID]['data'] as $k => &$g) {
                if ($k >= $m)
                    $g += $flowKoef * $amount;
            }
            foreach ($growingData[$walletTypeID]['data'] as $k => &$g) {
                if ($k >= $m)
                    $g += $flowKoef * $amount;
            }
            foreach ($totalData['growing']['data'] as $k => &$t) {
                if ($k >= $m)
                    $t += $flowKoef * $amount;
            }
        }

        // remove empty (has growing amount, but hasn't amount in current year)
        foreach ($data as $k0 => $d0)
            foreach ($d0['children'] as $k1 => $d1)
                foreach ($d1['levels'] as $k2 => $d2)
                    foreach ($d2['levels'] as $k3 => $d3)
                        if (array_sum($d3['data']) == 0)
                            unset($data[$k0]['children'][$k1]['levels'][$k2]['levels'][$k3]);

        // sort
        foreach ($data as $k0 => $d0) {
            foreach ($d0['children'] as $k1 => $d1) {
                $this->sortItems($data[$k0]['children']);
                $this->sortSubItems($data[$k0]['children']);
            }
        }

        return [
            'data' => $data,
            'growingData' => $growingData,
            'totalData' => $totalData
        ];
    }

    public function getByPurseRawDataDetailed($params = [])
    {
        $tableBank = CashBankFlows::tableName();
        $tableCashbox = CashOrderFlows::tableName();
        $tableEmoney = CashEmoneyFlows::tableName();
        $tableAcquiring = AcquiringOperation::tableName();
        $tableCard = CardOperation::tableName();

        $companyIds = implode(',', $this->_multiCompanyIds);
        $year = $this->year;
        $seq = 'seq_0_to_11';
        $interval = "('{$year}-01-31' + INTERVAL (seq) MONTH)";
        $AND_FILTERS = $this->getSqlFilters($params);
        $AND_HIDE_TIN_PARENT = $this->getSqlHideTinParent();

        $query = "

          SELECT
            1 AS wallet,
            t.rs AS wallet_id,
            t.flow_type AS type,
            t.contractor_id,
            IF (t.flow_type = 0, t.expenditure_item_id, t.income_item_id) AS item_id,
            DATE_FORMAT({$interval}, '%m') m,
            DATE_FORMAT({$interval}, '%d') d,
            SUM(IF(DATE_FORMAT(t.date, '%Y%m') = DATE_FORMAT({$interval}, '%Y%m'), IF(t.tin_child_amount, t.tin_child_amount, t.amount), 0)) amount,
            SUM(IF(t.tin_child_amount, t.tin_child_amount, t.amount)) growing_amount
          FROM {$seq}
          JOIN {$tableBank} t ON t.company_id IN ({$companyIds}) AND t.date <= {$interval}
          WHERE 1=1 {$AND_FILTERS} {$AND_HIDE_TIN_PARENT}
          GROUP BY wallet_id, type, item_id, m

          UNION ALL

          SELECT
            2 AS wallet,
            t.cashbox_id AS wallet_id,
            t.flow_type AS type,
            t.contractor_id,
            IF (t.flow_type = 0, t.expenditure_item_id, t.income_item_id) AS item_id,
            DATE_FORMAT({$interval}, '%m') m,
            DATE_FORMAT({$interval}, '%d') d,
            SUM(IF(DATE_FORMAT(t.date, '%Y%m') = DATE_FORMAT({$interval}, '%Y%m'), t.amount, 0)) amount,
            SUM(t.amount) growing_amount
          FROM {$seq}
          JOIN {$tableCashbox} t ON t.company_id IN ({$companyIds}) AND t.date <= {$interval}
          WHERE 1=1 {$AND_FILTERS}
          GROUP BY wallet_id, type, item_id, m

          UNION ALL

          SELECT
            3 AS wallet,
            t.emoney_id AS wallet_id,
            t.flow_type AS type,
            t.contractor_id,
            IF (t.flow_type = 0, t.expenditure_item_id, t.income_item_id) AS item_id,
            DATE_FORMAT({$interval}, '%m') m,
            DATE_FORMAT({$interval}, '%d') d,
            SUM(IF(DATE_FORMAT(t.date, '%Y%m') = DATE_FORMAT({$interval}, '%Y%m'), t.amount, 0)) amount,
            SUM(t.amount) growing_amount
          FROM {$seq}
          JOIN {$tableEmoney} t ON t.company_id IN ({$companyIds}) AND t.date <= {$interval}
          WHERE 1=1 {$AND_FILTERS}
          GROUP BY wallet_id, type, item_id, m

          UNION ALL

          SELECT
            4 AS wallet,
            t.acquiring_identifier AS wallet_id,
            t.flow_type AS type,
            t.contractor_id,
            IF (t.flow_type = 0, t.expenditure_item_id, t.income_item_id) AS item_id,
            DATE_FORMAT({$interval}, '%m') m,
            DATE_FORMAT({$interval}, '%d') d,
            SUM(IF(DATE_FORMAT(t.date, '%Y%m') = DATE_FORMAT({$interval}, '%Y%m'), t.amount, 0)) amount,
            SUM(t.amount) growing_amount
          FROM {$seq}
          JOIN {$tableAcquiring} t ON t.company_id IN ({$companyIds}) AND t.date <= {$interval}
          WHERE 1=1 {$AND_FILTERS}
          GROUP BY wallet_id, type, item_id, m

          UNION ALL

          SELECT
            5 AS wallet,
            t.account_id AS wallet_id,
            t.flow_type AS type,
            t.contractor_id,
            IF (t.flow_type = 0, t.expenditure_item_id, t.income_item_id) AS item_id,
            DATE_FORMAT({$interval}, '%m') m,
            DATE_FORMAT({$interval}, '%d') d,
            SUM(IF(DATE_FORMAT(t.date, '%Y%m') = DATE_FORMAT({$interval}, '%Y%m'), t.amount, 0)) amount,
            SUM(t.amount) growing_amount
          FROM {$seq}
          JOIN {$tableCard} t ON t.company_id IN ({$companyIds}) AND t.date <= {$interval}
          WHERE 1=1 {$AND_FILTERS}
          GROUP BY wallet_id, type, item_id, m

          ";

        return Yii::$app->db2->createCommand($query)->queryAll();
    }

    public function getByPurseRawDataDetailedDay($params = [])
    {
        $tableBank = CashBankFlows::tableName();
        $tableCashbox = CashOrderFlows::tableName();
        $tableEmoney = CashEmoneyFlows::tableName();
        $tableAcquiring = AcquiringOperation::tableName();
        $tableCard = CardOperation::tableName();

        $companyIds = implode(',', $this->_multiCompanyIds);
        $year = $this->year;
        $AND_FILTERS = $this->getSqlFilters($params);
        $AND_HIDE_TIN_PARENT = $this->getSqlHideTinParent();

        $query = "

          SELECT
            1 AS wallet,
            t.rs AS wallet_id,
            t.flow_type AS type,
            t.contractor_id,
            IF (t.flow_type = 0, t.expenditure_item_id, t.income_item_id) AS item_id,
            DATE_FORMAT(t.date, '%m') m,
            DATE_FORMAT(t.date, '%d') d,
            SUM(IF(t.tin_child_amount, t.tin_child_amount, t.amount)) amount
          FROM {$tableBank} t
          WHERE t.company_id IN ({$companyIds}) AND t.date BETWEEN '{$year}-01-01' AND '{$year}-12-31' {$AND_FILTERS} {$AND_HIDE_TIN_PARENT}
          GROUP BY wallet_id, type, item_id, m

          UNION ALL

          SELECT
            2 AS wallet,
            t.cashbox_id AS wallet_id,
            t.flow_type AS type,
            t.contractor_id,
            IF (t.flow_type = 0, t.expenditure_item_id, t.income_item_id) AS item_id,
            DATE_FORMAT(t.date, '%m') m,
            DATE_FORMAT(t.date, '%d') d,
            SUM(t.amount) amount
          FROM {$tableCashbox} t
          WHERE t.company_id IN ({$companyIds}) AND t.date BETWEEN '{$year}-01-01' AND '{$year}-12-31' {$AND_FILTERS}
          GROUP BY wallet_id, type, item_id, m

          UNION ALL

          SELECT
            3 AS wallet,
            t.emoney_id AS wallet_id,
            t.flow_type AS type,
            t.contractor_id,
            IF (t.flow_type = 0, t.expenditure_item_id, t.income_item_id) AS item_id,
            DATE_FORMAT(t.date, '%m') m,
            DATE_FORMAT(t.date, '%d') d,
            SUM(t.amount) amount
          FROM {$tableEmoney} t
          WHERE t.company_id IN ({$companyIds}) AND t.date BETWEEN '{$year}-01-01' AND '{$year}-12-31' {$AND_FILTERS}
          GROUP BY wallet_id, type, item_id, m

          UNION ALL

          SELECT
            4 AS wallet,
            t.acquiring_identifier AS wallet_id,
            t.flow_type AS type,
            t.contractor_id,
            IF (t.flow_type = 0, t.expenditure_item_id, t.income_item_id) AS item_id,
            DATE_FORMAT(t.date, '%m') m,
            DATE_FORMAT(t.date, '%d') d,
            SUM(t.amount) amount
          FROM {$tableAcquiring} t
          WHERE t.company_id IN ({$companyIds}) AND t.date BETWEEN '{$year}-01-01' AND '{$year}-12-31' {$AND_FILTERS}
          GROUP BY wallet_id, type, item_id, m

          UNION ALL

          SELECT
            5 AS wallet,
            t.account_id AS wallet_id,
            t.flow_type AS type,
            t.contractor_id,
            IF (t.flow_type = 0, t.expenditure_item_id, t.income_item_id) AS item_id,
            DATE_FORMAT(t.date, '%m') m,
            DATE_FORMAT(t.date, '%d') d,
            SUM(t.amount) amount
          FROM {$tableCard} t
          WHERE t.company_id IN ({$companyIds}) AND t.date BETWEEN '{$year}-01-01' AND '{$year}-12-31' {$AND_FILTERS}
          GROUP BY wallet_id, type, item_id, m

          ";

        $queryGrowingStart = "
          SELECT
            1 AS wallet,
            t.rs AS wallet_id,
            SUM(IF(t.flow_type = 0, -t.amount, t.amount)) AS amount
          FROM {$tableBank} t
          WHERE
            t.company_id IN ({$companyIds}) AND t.date < '{$year}-01-01' {$AND_FILTERS}
          GROUP BY wallet_id

          UNION ALL

          SELECT
            2 AS wallet,
            t.cashbox_id AS wallet_id,
            SUM(IF(t.flow_type = 0, -t.amount, t.amount)) AS amount
          FROM {$tableCashbox} t
          WHERE
            t.company_id IN ({$companyIds}) AND t.date < '{$year}-01-01' {$AND_FILTERS}
          GROUP BY wallet_id

          UNION ALL

          SELECT
            3 AS wallet,
            t.emoney_id AS wallet_id,
            SUM(IF(t.flow_type = 0, -t.amount, t.amount)) AS amount
          FROM {$tableEmoney} t
          WHERE
            t.company_id IN ({$companyIds}) AND t.date < '{$year}-01-01' {$AND_FILTERS}
          GROUP BY wallet_id

          UNION ALL

          SELECT
            4 AS wallet,
            t.acquiring_identifier AS wallet_id,
            SUM(IF(t.flow_type = 0, -t.amount, t.amount)) AS amount
          FROM {$tableAcquiring} t
          WHERE
            t.company_id IN ({$companyIds}) AND t.date < '{$year}-01-01' {$AND_FILTERS}
          GROUP BY wallet_id

          UNION ALL

          SELECT
            5 AS wallet,
            t.account_id AS wallet_id,
            SUM(IF(t.flow_type = 0, -t.amount, t.amount)) AS amount
          FROM {$tableCard} t
          WHERE
            t.company_id IN ({$companyIds}) AND t.date < '{$year}-01-01' {$AND_FILTERS}
          GROUP BY wallet_id
        ";

        return [
            'flat' => Yii::$app->db2->createCommand($query)->queryAll(),
            'growingStart' => Yii::$app->db2->createCommand($queryGrowingStart)->queryAll(),
        ];
    }

    public function getByPurseEmptyDataDetailed($byDays = false)
    {
        $ret = [
            AbstractFinance::CASH_BANK_BLOCK => [
                'title' => 'Банк',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'question' => '#tooltip_block_bank',
                'children' => [],
            ],
            AbstractFinance::CASH_ORDER_BLOCK => [
                'title' => 'Касса',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'question' => '#tooltip_block_order',
                'children' => [],
            ],
            AbstractFinance::CASH_EMONEY_BLOCK => [
                'title' => 'E-money',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'question' => '#tooltip_block_emoney',
                'children' => [],
            ],
            AbstractFinance::CASH_ACQUIRING_BLOCK => [
                'title' => 'Интернет-эквайринг',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'question' => '#tooltip_block_acquiring',
                'children' => [],
            ],
            AbstractFinance::CASH_CARD_BLOCK => [
                'title' => 'Карты',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'question' => '#tooltip_block_card',
                'children' => [],
            ],
        ];

        foreach ($this->walletName as $walletTypeID => $wallets) {
            foreach ($wallets as $walletID => $walletName) {

                if (!isset($ret[$walletTypeID])) {
                    throw new Exception('Unknown wallet type ' . $walletTypeID);
                }

                $ret[$walletTypeID]['children'][$walletID] = [
                    'title' => $walletName,
                    'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                    'levels' => [
                        CashFlowsBase::FLOW_TYPE_INCOME => [
                            'title' => 'Приход',
                            'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                            'levels' => []
                        ],
                        CashFlowsBase::FLOW_TYPE_EXPENSE => [
                            'title' => 'Расход',
                            'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                            'levels' => []
                        ],
                    ],
                ];
            }
        }

        return $ret;
    }

    public function getByPurseEmptyGrowingDataDetailed($byDays = false)
    {
        // todo: дублирует ф-цию getByPurseEmptyDataDetailed() - только title другие
        $ret = [
            AbstractFinance::CASH_BANK_BLOCK => [
                'title' => 'Остаток по Банку',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'children' => [],
            ],
            AbstractFinance::CASH_ORDER_BLOCK => [
                'title' => 'Остаток по Кассе',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'children' => [],
            ],
            AbstractFinance::CASH_EMONEY_BLOCK => [
                'title' => 'Остаток по E-money',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'children' => [],
            ],
            AbstractFinance::CASH_ACQUIRING_BLOCK => [
                'title' => 'Остаток по интернет-эквайрингу',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
            ],
            AbstractFinance::CASH_CARD_BLOCK => [
                'title' => 'Остаток по картам',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
            ],
        ];

        foreach ($this->walletName as $walletTypeID => $wallets) {
            foreach ($wallets as $walletID => $walletName) {

                if (!isset($ret[$walletTypeID])) {
                    throw new Exception('Остаток по "Unknown wallet type" ' . $walletTypeID);
                }

                if (!isset($ret[$walletTypeID]['children'][$walletID])) {
                    $ret[$walletTypeID]['children'][$walletID] = [
                        'title' => 'Остаток по ' . ArrayHelper::getValue($this->walletName, "{$walletTypeID}.{$walletID}", $walletID),
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ];
                }
            }
        }

        return $ret;
    }
}