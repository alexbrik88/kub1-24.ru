<?php

namespace frontend\modules\analytics\models\odds;

use Yii;
use common\models\cash\CashFlowsBase;
use common\models\currency\Currency;
use common\models\currency\CurrencyRate;
use frontend\modules\analytics\models\AbstractFinance;

trait SearchByActivityTrait
{
    public function searchByActivity($params = [])
    {
        $daysData = $this->searchByActivityDays($params);
        $data = [];
        $growingData = [];
        $totalData = [];
        $emptyValues = self::EMPTY_YEAR;
        $income = CashFlowsBase::FLOW_TYPE_INCOME;
        $expense = CashFlowsBase::FLOW_TYPE_EXPENSE;
        $difference = [];

        foreach ($daysData['data'] as $wallet => $walletData) {
            $data[$wallet] = [
                'title' => $walletData['title'],
                'data' => $emptyValues,
                'levels' => [],
            ];
            foreach ($walletData['data'] as $period => $amount) {
                $p = substr($period, 0, 2);
                $data[$wallet]['data'][$p] += $amount;
            }

            foreach ($walletData['levels'] as $type => $typeData) {
                $data[$wallet]['levels'][$type] = [
                    'title' => $typeData['title'],
                    'data' => $emptyValues,
                    'levels' => [],
                ];
                foreach ($typeData['data'] as $period => $amount) {
                    $p = substr($period, 0, 2);
                    $data[$wallet]['levels'][$type]['data'][$p] += $amount;
                }

                foreach ($typeData['levels'] as $item => $itemData) {
                    $data[$wallet]['levels'][$type]['levels'][$item] = [
                        'title' => $itemData['title'],
                        'data' => $emptyValues,
                        'dataHoverText' => $itemData['dataHoverText'] ?? null,
                        'levels' => [],
                    ];
                    foreach ($itemData['data'] as $period => $amount) {
                        $p = substr($period, 0, 2);
                        $data[$wallet]['levels'][$type]['levels'][$item]['data'][$p] += $amount;
                    }
                    if (in_array($item, Currency::$foreignArray)) {
                        $difference[$wallet][$item][$type] = $data[$wallet]['levels'][$type]['levels'][$item]['data'];
                    }

                    if (!empty($itemData['levels'])) {
                        foreach ($itemData['levels'] as $subitem => $subitemData) {
                            $data[$wallet]['levels'][$type]['levels'][$item]['levels'][$subitem] = [
                                'title' => $subitemData['title'],
                                'data' => $emptyValues,
                                'levels' => [],
                            ];

                            if (isset($subitemData['consolidatedSubItemsIds'])) {
                                $data[$wallet]['levels'][$type]['levels'][$item]['levels'][$subitem]['consolidatedSubItemsIds']
                                    = $subitemData['consolidatedSubItemsIds'];
                            }

                            foreach ($subitemData['data'] as $period => $amount) {
                                $p = substr($period, 0, 2);
                                $data[$wallet]['levels'][$type]['levels'][$item]['levels'][$subitem]['data'][$p] += $amount;
                            }
                        }
                    }
                }
            }
        }

        foreach ($difference as $wallet => $walletData) {
            foreach ($walletData as $currency => $currencyData) {
                @list($in, $out) = array_keys($currencyData);
                if ($in && $out)
                    foreach ($currencyData[$in] as $period => $amount) {
                        $diff = $amount - $currencyData[$out][$period];
                        $data[$wallet]['levels'][$in]['levels'][$currency]['data'][$period] = max(0, $diff);
                        $data[$wallet]['levels'][$out]['levels'][$currency]['data'][$period] = abs(min(0, $diff));
                        $min = min($amount, $currencyData[$out][$period]);
                        $data[$wallet]['levels'][$in]['data'][$period] -= $min;
                        $data[$wallet]['levels'][$out]['data'][$period] -= $min;
                    }
            }
        }

        foreach ($daysData['growingData'] as $wallet => $walletData) {
            $growingData[$wallet] = [
                'title' => $walletData['title'],
                'data' => $emptyValues,
                'question' => $walletData['question'] ?? null
            ];
            foreach ($walletData['data'] as $period => $amount) {
                $p = substr($period, 0, 2);
                $growingData[$wallet]['data'][$p] = $amount;
            }
        }

        $totalData['month'] = [
            'title' => $daysData['totalData']['month']['title'],
            'data' => $emptyValues,
        ];
        foreach ($daysData['totalData']['month']['data'] as $period => $amount) {
            $p = substr($period, 0, 2);
            $totalData['month']['data'][$p] += $amount;
        }

        $totalData['growing'] = [
            'title' => $daysData['totalData']['growing']['title'],
            'data' => $emptyValues,
        ];
        foreach ($daysData['totalData']['growing']['data'] as $period => $amount) {
            $p = substr($period, 0, 2);
            $totalData['growing']['data'][$p] = $amount;
        }

        return [
            'data' => $data,
            'growingData' => $growingData,
            'totalData' => $totalData
        ];
    }

    public function searchByActivityDays($params = [])
    {
        $rawData = $params['rawDataDays'] ?? $this->getByActivityRawDataDay($params);

        $data = self::getByActivityEmptyData(true);
        $growingData = self::getByActivityEmptyGrowingData(true);
        $totalData = self::getEmptyTotalData(true);
        $emptyValues = self::getEmptyYearInDays($this->year);
        $income = CashFlowsBase::FLOW_TYPE_INCOME;
        $expense = CashFlowsBase::FLOW_TYPE_EXPENSE;

        $subArticles = [
            CashFlowsBase::FLOW_TYPE_INCOME => $this->getArticles2Parents(CashFlowsBase::FLOW_TYPE_INCOME),
            CashFlowsBase::FLOW_TYPE_EXPENSE => $this->getArticles2Parents(CashFlowsBase::FLOW_TYPE_EXPENSE),
        ];

        $typeMap = [];
        $cashStart = [];
        $balance = [];

        // start year balance
        foreach ($rawData['cashStart'] as $d) {
            $currency = $d['currency_name'] ?: Currency::DEFAULT_NAME;
            $type = $this->__getActivityBlockType($d['type'], $d['item_id']);
            $wallet = AbstractFinance::$blockByType[$type];
            $flowKoef = ($d['type'] == 0 ? -1 : 1);
            $amount = $d['amount'];

            if (isset($cashStart[$wallet][$currency])) {
                $cashStart[$wallet][$currency] += $flowKoef * $amount;
            } else {
                $cashStart[$wallet][$currency] = $flowKoef * $amount;
            }
        }

        foreach ($cashStart as $wallet => $wData) {
            if (isset($wData[Currency::DEFAULT_NAME])) {
                foreach ($growingData[$wallet]['data'] as $k => &$g) {
                    $g += $wData[Currency::DEFAULT_NAME];
                }
                foreach ($totalData['growing']['data'] as $k => &$t) {
                    $t += $wData[Currency::DEFAULT_NAME];
                }
            }
        }

        foreach ($rawData['sashFlows'] as $d) {
            $currency = $d['currency_name'] ?: Currency::DEFAULT_NAME;
            $type = $this->__getActivityBlockType($d['type'], $d['item_id']);
            $wallet = AbstractFinance::$blockByType[$type];
            $typeMap[$type] = $d['type'];
            $item = $d['item_id'];
            $key = $d['m'].$d['d'];
            $flowKoef = ($d['type'] == 0 ? -1 : 1);
            $amount = $d['amount'];

            if ($currency != Currency::DEFAULT_NAME) {
                $date = date_create_from_format('Ymd', $this->year.$key);
                $rate = $date ? (CurrencyRate::getRateOnDate($date)[$currency] ?? null) : null;
                if ($rate !== null) {
                    $amount = (int) round($d['amount']/$rate['amount']*$rate['value']);
                }

                if (!isset($balance[$wallet][$currency])) {
                    $balance[$wallet][$currency] = $emptyValues;
                }
                $balance[$wallet][$currency][$key] += $flowKoef * $d['amount'];
            } else {
                foreach ($growingData[$wallet]['data'] as $k => &$g) {
                    if ($k >= $key)
                        $g += $flowKoef * $amount;
                }
                foreach ($totalData['growing']['data'] as $k => &$t) {
                    if ($k >= $key)
                        $t += $flowKoef * $amount;
                }
            }

            $subItem = null;
            if (isset($subArticles[$d['type']][$d['item_id']])) {
                $item = $subArticles[$d['type']][$d['item_id']];
                $subItem = $d['item_id'];
            }

            if (!isset($data[$wallet]['levels'][$type]['levels'][$item])) {
                $data[$wallet]['levels'][$type]['levels'][$item] = [
                    'title' => $this->__getCashContractorName($d['type'], $item),
                    'data' => self::getEmptyYearInDays($this->year),
                    'levels' => []
                ];
            }

            if ($subItem) {
                if (!isset($data[$wallet]['levels'][$type]['levels'][$item]['levels'][$subItem])) {
                    $data[$wallet]['levels'][$type]['levels'][$item]['levels'][$subItem] = [
                        'title' => $this->__getCashContractorName($d['type'], $subItem),
                        'data' => self::getEmptyYearInDays($this->year)
                    ];
                }
                $data[$wallet]['levels'][$type]['levels'][$item]['levels'][$subItem]['data'][$key] += $amount;
            }

            $totalData['month']['data'][$key] += $flowKoef * $amount;
            $data[$wallet]['levels'][$type]['levels'][$item]['data'][$key] += $amount;
            $data[$wallet]['levels'][$type]['data'][$key] += $amount;
            $data[$wallet]['data'][$key] += $flowKoef * $amount;
        }

        $totalBalance = [];
        $difference = [];
        foreach ($emptyValues as $key => $value) {
            $date = date_create_from_format('Ymd', $this->year.$key);
            $rateArray = $date ? (CurrencyRate::getRateOnDate($date) ?? []) : [];
            foreach ($balance as $w => $wData) {
                foreach ($wData as $c => $cData) {
                    if (!isset($difference[$w][$c])) {
                        $difference[$w][$c] = [
                            $income => [
                                'title' => $this->exchangeDifferenceName($income, $c),
                                'dataHoverText' => 'Расчетная величина',
                                'data' => $emptyValues,
                            ],
                            $expense => [
                                'title' => $this->exchangeDifferenceName($expense, $c),
                                'dataHoverText' => 'Расчетная величина',
                                'data' => $emptyValues,
                            ],
                        ];
                    }
                    if (!isset($totalBalance[$w][$c])) {
                        $startDate = date_create_from_format('Ymd', $this->year.'0101');
                        $startRate = CurrencyRate::getRateOnDate($startDate)[$c] ?? null;
                        $sum = $cashStart[$w][$c] ?? 0;
                        $totalBalance[$w][$c] = [
                            'sum' => $sum,
                            'rate' => $startRate,
                        ];
                    }
                    $lastBalance = $totalBalance[$w][$c]['sum'];
                    $lastRate = $totalBalance[$w][$c]['rate'];
                    $currentBalance = $lastBalance + $cData[$key];
                    $currentRate = $rateArray[$c] ?? null;
                    $balanceBaseCurrency = $currentRate ? (int) round($currentBalance/$currentRate['amount']*$currentRate['value']) : $currentBalance;

                    if ($lastBalance == 0 || ($currentRate['value']/$currentRate['amount'] == $lastRate['value']/$lastRate['amount'])) {
                        $diff = 0;
                    } else {
                        $diff = (int) round($lastBalance*(($currentRate['value']/$currentRate['amount']) - ($lastRate['value']/$lastRate['amount'])));
                    }

                    if ($diff > 0) {
                        $difference[$w][$c][$income]['data'][$key] += $diff;
                    } elseif ($diff < 0) {
                        $difference[$w][$c][$expense]['data'][$key] -= $diff;
                    }

                    $growingData[$w]['data'][$key] += $balanceBaseCurrency;
                    $totalData['growing']['data'][$key] += $balanceBaseCurrency;
                    $totalData['month']['data'][$key] += $diff;
                    $data[$w]['data'][$key] += $diff;

                    $totalBalance[$w][$c]['sum'] = $currentBalance;
                    $totalBalance[$w][$c]['rate'] = $currentRate;
                }
            }
        }

        foreach ($difference as $w => $wData) {
            foreach ($wData as $c => $cData) {
                foreach ($data[$w]['levels'] as $type => $value) {
                    if (isset($typeMap[$type], $cData[$typeMap[$type]])) {
                        $data[$w]['levels'][$type]['levels'][$c] = $cData[$typeMap[$type]];
                        foreach ($data[$w]['levels'][$type]['levels'][$c]['data'] as $key => $value) {
                            $data[$w]['levels'][$type]['data'][$key] += $value;
                        }
                    }
                }
            }
        }

        // sort
        $this->sortItems($data);
        $this->sortSubItems($data);

        // glue subitems
        if ($this->multiCompanyManager->getIsModeOn()) {
            $this->glueSubItemsByNames($data);
        }

        return [
            'data' => $data,
            'growingData' => $growingData,
            'totalData' => $totalData
        ];
    }

//    public function getByActivityRawData($params = [])
//    {
//        $table = self::TABLE_OLAP_FLOWS;
//        $companyIds = implode(',', $this->_multiCompanyIds);
//        $year = $this->year;
//        $seq = 'seq_0_to_11';
//        $interval = "('{$year}-01-31' + INTERVAL (seq) MONTH)";
//        $AND_FILTERS = $this->getSqlFilters($params);
//
//        $query = "
//            SELECT
//              t.item_id,
//              t.type,
//              DATE_FORMAT({$interval}, '%m') m,
//              SUM(IF(DATE_FORMAT(t.date, '%Y%m') = DATE_FORMAT({$interval}, '%Y%m'), t.amount, 0)) amount,
//              SUM(IF(t.date < '{$year}-01-01', t.amount, 0)) growing_amount
//            FROM {$seq}
//            JOIN {$table} t ON
//              t.company_id IN ({$companyIds})
//              AND t.date <= {$interval}
//              {$AND_FILTERS}
//            GROUP BY t.type, t.item_id, m
//          ";
//
//        return Yii::$app->db2->createCommand($query)->queryAll();
//    }



    public function getByActivityRawDataDay($params = [])
    {
        $table = self::TABLE_OLAP_FLOWS;
        $companyIds = implode(',', $this->_multiCompanyIds);
        $year = $this->year;
        $AND_FILTERS = $this->getSqlFilters($params);
        $AND_HIDE_TIN_PARENT = $this->getSqlHideTinParent();

        $sashFlows = "
            SELECT
                t.currency_name,
                t.item_id,
                t.type,
                DATE_FORMAT(t.date, '%m') m,
                DATE_FORMAT(t.date, '%d') d,
                SUM(IF(t.has_tin_parent, t.tin_child_amount, t.original_amount)) amount
            FROM {$table} t
            WHERE
                t.company_id IN ({$companyIds})
                AND t.date BETWEEN '{$year}-01-01' AND '{$year}-12-31'
                {$AND_FILTERS}
                {$AND_HIDE_TIN_PARENT}
            GROUP BY t.currency_name, t.type, t.item_id, m, d
        ";

        $cashStart = "
            SELECT
                t.currency_name,
                t.type,
                t.item_id,
                SUM(IF(t.has_tin_parent, t.tin_child_amount, t.original_amount)) amount
            FROM {$table} t
            WHERE
                t.company_id IN ({$companyIds})
                AND t.date < '{$year}-01-01'
                {$AND_FILTERS}
                {$AND_HIDE_TIN_PARENT}
            GROUP BY t.currency_name, t.type, t.item_id
        ";

        return [
            'sashFlows' => Yii::$app->db2->createCommand($sashFlows)->queryAll(),
            'cashStart' => Yii::$app->db2->createCommand($cashStart)->queryAll(),
        ];
    }

    public function getByActivityEmptyData($byDays = false)
    {
        return [
            AbstractFinance::OPERATING_ACTIVITIES_BLOCK => [
                'title' => 'Операционная деятельность',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'levels' => [
                    AbstractFinance::INCOME_OPERATING_ACTIVITIES => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::WITHOUT_TYPE => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ],
                'question' => '#tooltip_operation_activities_block',
            ],
            AbstractFinance::FINANCIAL_OPERATIONS_BLOCK => [
                'title' => 'Финансовая деятельность',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'levels' => [
                    AbstractFinance::RECEIPT_FINANCING_TYPE_FIRST => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::RECEIPT_FINANCING_TYPE_SECOND => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ],
                'question' => '#tooltip_financial_operations_block',
            ],
            AbstractFinance::INVESTMENT_ACTIVITIES_BLOCK => [
                'title' => 'Инвестиционная деятельность',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'levels' => [
                    AbstractFinance::INCOME_INVESTMENT_ACTIVITIES => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::EXPENSE_INVESTMENT_ACTIVITIES => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ],
                'question' => '#tooltip_investment_activities_block',
            ],
            AbstractFinance::OWN_FUNDS_BLOCK => [
                'title' => 'Перемещение денег',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'levels' => [
                    AbstractFinance::OWN_FUNDS_INCOME => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::OWN_FUNDS_EXPENSE => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ],
                'question' => '#tooltip_own_funds_block',
            ],
        ];
    }

    public function getByActivityEmptyGrowingData($byDays = false)
    {
        return [
            AbstractFinance::OPERATING_ACTIVITIES_BLOCK => [
                'title' => 'Остаток по оп. деятельности',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
            ],
            AbstractFinance::FINANCIAL_OPERATIONS_BLOCK => [
                'title' => 'Остаток по фин. деятельности',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
            ],
            AbstractFinance::INVESTMENT_ACTIVITIES_BLOCK => [
                'title' => 'Остаток по инвест. деятельности',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
            ],
            AbstractFinance::OWN_FUNDS_BLOCK => [
                'title' => 'Остаток по перемещению денег',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'hide' => true
            ]
        ];
    }
}