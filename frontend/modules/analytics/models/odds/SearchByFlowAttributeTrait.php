<?php
namespace frontend\modules\analytics\models\odds;

use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use Yii;
use yii\base\Exception;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashFlowsBase;
use common\modules\acquiring\models\AcquiringOperation;
use common\modules\cards\models\CardOperation;
use frontend\modules\analytics\models\AbstractFinance;

trait SearchByFlowAttributeTrait {

    public function searchByFlowAttribute($params = [])
    {
        $theAttr = $params['attr'] ?? 'wallet';

        $rawData = $params['rawData'] ?? $this->getByFlowAttributeRawData($params);
        $data = self::getByFlowAttributeEmptyData(false, $params);
        $growingData = self::getByFlowAttributeEmptyGrowingData(false, $params);
        $totalData = self::getEmptyTotalData(false);

        $subArticles = [
            CashFlowsBase::FLOW_TYPE_INCOME => $this->getArticles2Parents(CashFlowsBase::FLOW_TYPE_INCOME),
            CashFlowsBase::FLOW_TYPE_EXPENSE => $this->getArticles2Parents(CashFlowsBase::FLOW_TYPE_EXPENSE),
        ];

        foreach ($rawData as $d) {

            $wallet = $d[$theAttr];
            $pos = $d['type'];
            $item = $d['item_id'];
            $m = $d['m'];
            $flowKoef = ($d['type'] == 0 ? -1 : 1);
            $amount = $d['amount'];
            $growingAmount = $d['growing_amount'];

            if (in_array($d['contractor_id'], ['bank', 'order', 'emoney'])) {
                $item = $d['contractor_id'];
            }

            $subItem = null;
            if (isset($subArticles[$d['type']][$d['item_id']])) {
                $item = $subArticles[$d['type']][$d['item_id']];
                $subItem = $d['item_id'];
            }

            if (!isset($data[$wallet]['levels'][$pos]['levels'][$item])) {
                $data[$wallet]['levels'][$pos]['levels'][$item] = [
                    'title' => $this->__getCashContractorName($d['type'], $item, $d['contractor_id']),
                    'data' => self::EMPTY_YEAR
                ];
            }

            if ($subItem) {
                if (!isset($data[$wallet]['levels'][$pos]['levels'][$item]['levels'][$subItem])) {
                    $data[$wallet]['levels'][$pos]['levels'][$item]['levels'][$subItem] = [
                        'title' => $this->__getCashContractorName($d['type'], $subItem),
                        'data' => self::EMPTY_YEAR
                    ];
                }
                $data[$wallet]['levels'][$pos]['levels'][$item]['levels'][$subItem]['data'][$m] += $amount;
            }

            $data[$wallet]['levels'][$pos]['levels'][$item]['data'][$m] += $amount;

            $data[$wallet]['levels'][$pos]['data'][$m] += $amount;
            $data[$wallet]['data'][$m] += $flowKoef * $amount;
            $growingData[$wallet]['data'][$m] += $flowKoef * $growingAmount;
            $totalData['month']['data'][$m] += $flowKoef * $amount;
            $totalData['growing']['data'][$m] += $flowKoef * $growingAmount;
        }

        // remove empty (has growing amount, but hasn't amount in current year)
        foreach ($data as $k1 => $d1)
            foreach ($d1['levels'] as $k2 => $d2)
                foreach ($d2['levels'] as $k3 => $d3)
                    if (array_sum($d3['data']) == 0)
                        unset($data[$k1]['levels'][$k2]['levels'][$k3]);

        // sort
        $this->sortItems($data);
        $this->sortSubItems($data);

        return [
            'data' => $data,
            'growingData' => $growingData,
            'totalData' => $totalData
        ];
    }

    public function searchByFlowAttributeDays($params = [])
    {
        $theAttr = $params['attr'] ?? 'wallet';

        $rawDataArr = $this->getByFlowAttributeRawDataDay($params);
        $data = self::getByFlowAttributeEmptyData(true, $params);
        $growingData = self::getByFlowAttributeEmptyGrowingData(true, $params);
        $totalData = self::getEmptyTotalData(true);

        $subArticles = [
            CashFlowsBase::FLOW_TYPE_INCOME => $this->getArticles2Parents(CashFlowsBase::FLOW_TYPE_INCOME),
            CashFlowsBase::FLOW_TYPE_EXPENSE => $this->getArticles2Parents(CashFlowsBase::FLOW_TYPE_EXPENSE),
        ];

        // start year balance
        foreach ($rawDataArr['growingStart'] as $d) {
            foreach ($growingData[$d[$theAttr]]['data'] as $k => &$g) {
                    $g += $d['amount'];
            }
            foreach ($totalData['growing']['data'] as &$t) {
                $t += $d['amount'];
            }
        }

        foreach ($rawDataArr['flat'] as $d) {

            $wallet = $d[$theAttr];
            $type = $d['type'];
            $item = $d['item_id'];
            $key = $d['m'].$d['d'];
            $flowKoef = ($d['type'] == 0 ? -1 : 1);
            $amount = $d['amount'];

            if (in_array($d['contractor_id'], ['bank', 'order', 'emoney'])) {
                $item = $d['contractor_id'];
            }

            $subItem = null;
            if (isset($subArticles[$d['type']][$d['item_id']])) {
                $item = $subArticles[$d['type']][$d['item_id']];
                $subItem = $d['item_id'];
            }

            if (!isset($data[$wallet]['levels'][$type]['levels'][$item])) {
                $data[$wallet]['levels'][$type]['levels'][$item] = [
                    'title' => $this->__getCashContractorName($d['type'], $item, $d['contractor_id']),
                    'data' => self::getEmptyYearInDays($this->year)
                ];
            }

            if ($subItem) {
                if (!isset($data[$wallet]['levels'][$type]['levels'][$item]['levels'][$subItem])) {
                    $data[$wallet]['levels'][$type]['levels'][$item]['levels'][$subItem] = [
                        'title' => $this->__getCashContractorName($d['type'], $subItem),
                        'data' => self::getEmptyYearInDays($this->year)
                    ];
                }
                $data[$wallet]['levels'][$type]['levels'][$item]['levels'][$subItem]['data'][$key] += $amount;
            }

            $data[$wallet]['levels'][$type]['levels'][$item]['data'][$key] += $amount;
            $data[$wallet]['levels'][$type]['data'][$key] += $amount;
            $data[$wallet]['data'][$key] += $flowKoef * $amount;
            $totalData['month']['data'][$key] += $flowKoef * $amount;

            foreach ($growingData[$wallet]['data'] as $k => &$g) {
                if ($k >= $key)
                    $g += $flowKoef * $amount;
            }
            foreach ($totalData['growing']['data'] as $k => &$t) {
                if ($k >= $key)
                    $t += $flowKoef * $amount;
            }
        }

        // sort
        foreach ($data as &$d1)
            foreach ($d1['levels'] as &$d2)
                uasort($d2['levels'], function ($a, $b) {
                    if ($a['title'] == $b['title']) return 0;
                    return ($a['title'] < $b['title']) ? -1 : 1;
                });

        return [
            'data' => $data,
            'growingData' => $growingData,
            'totalData' => $totalData
        ];
    }

    public function getByFlowAttributeRawData($params = [])
    {
        $table = self::TABLE_OLAP_FLOWS;
        $companyIds = implode(',', $this->_multiCompanyIds);
        $year = $this->year;
        $seq = 'seq_0_to_11';
        $interval = "('{$year}-01-31' + INTERVAL (seq) MONTH)";
        $AND_FILTERS = $this->getSqlFilters($params);
        $AND_HIDE_TIN_PARENT = $this->getSqlHideTinParent();
        $GROUP_BY = $params['attr'] ?? 'wallet';

        $query = "
            SELECT
              t.wallet,
              t.type,
              t.item_id,
              t.contractor_id,
              IFNULL(t.sale_point_id, 0) sale_point_id,
              IFNULL(t.industry_id, 0) industry_id, 
              DATE_FORMAT({$interval}, '%m') m,
              DATE_FORMAT({$interval}, '%d') d,
            SUM(IF(DATE_FORMAT(t.date, '%Y%m') = DATE_FORMAT({$interval}, '%Y%m'), IF(t.tin_child_amount, t.tin_child_amount, t.amount), 0)) amount,
            SUM(IF(t.tin_child_amount, t.tin_child_amount, t.amount)) growing_amount
            FROM {$seq}
            JOIN {$table} t ON
              t.company_id IN ({$companyIds})
              AND t.date <= {$interval}
              {$AND_FILTERS}
              {$AND_HIDE_TIN_PARENT}
            GROUP BY t.{$GROUP_BY}, t.type, t.contractor_id, t.item_id, m
          ";

        return Yii::$app->db2->createCommand($query)->queryAll();
    }

    public function getByFlowAttributeRawDataDay($params = [])
    {
        $table = self::TABLE_OLAP_FLOWS;
        $companyIds = implode(',', $this->_multiCompanyIds);
        $year = $this->year;
        $AND_FILTERS = $this->getSqlFilters($params);
        $AND_HIDE_TIN_PARENT = $this->getSqlHideTinParent();
        $GROUP_BY = $params['attr'] ?? 'wallet';

        $query = "
            SELECT
              t.wallet,
              t.item_id,
              t.type,
              t.contractor_id,
              IFNULL(t.sale_point_id, 0) sale_point_id,
              IFNULL(t.industry_id, 0) industry_id,                   
              DATE_FORMAT(t.date, '%m') m,
              DATE_FORMAT(t.date, '%d') d,
              SUM(IF(t.has_tin_parent, t.tin_child_amount, t.original_amount)) amount
            FROM {$table} t
            WHERE
              t.company_id IN ({$companyIds})
              AND t.date BETWEEN '{$year}-01-01' AND '{$year}-12-31'
              {$AND_FILTERS} {$AND_HIDE_TIN_PARENT}
            GROUP BY t.{$GROUP_BY}, t.type, t.contractor_id, t.item_id, m, d
          ";

        $queryGrowingStart = "
            SELECT
              t.wallet,
              IFNULL(t.sale_point_id, 0) sale_point_id,
              IFNULL(t.industry_id, 0) industry_id,                   
              SUM(IF(t.type = 0, -1, 1) * t.amount) amount
            FROM {$table} t
            WHERE
              t.company_id IN ({$companyIds})
              AND t.date < '{$year}-01-01'
              {$AND_FILTERS}            
            GROUP BY t.{$GROUP_BY}
        ";

        return [
            'flat' => Yii::$app->db2->createCommand($query)->queryAll(),
            'growingStart' => Yii::$app->db2->createCommand($queryGrowingStart)->queryAll(),
        ];
    }

    public function getByFlowAttributeEmptyData($byDays = false, $params = [])
    {
        $attr = $params['attr'] ?? 'wallet';
        $isGrowing = $params['is_growing'] ?? false;

        switch ($attr) {
            case 'wallet':
                return $this->getByPurseEmptyData($byDays);
            case 'industry_id':
                $list = CompanyIndustry::find()
                    ->where(['company_id' => $this->_multiCompanyIds])
                    ->select(['id', 'name'])
                    ->orderBy('name')
                    ->asArray()
                    ->all();
                $list = array_merge($list, [['id' => 0, 'name' => 'Без направления']]);
                break;
            case 'sale_point_id':
                $list = SalePoint::find()
                    ->where(['company_id' => $this->_multiCompanyIds])
                    ->select(['id', 'name'])
                    ->orderBy('name')
                    ->asArray()
                    ->all();
                $list = array_merge($list, [['id' => 0, 'name' => 'Без точки продаж']]);
                break;
            default:
                $list = [];
                break;
        }

        $ret = [];
        foreach ($list as $l) {
            $itemID = $l['id'];
            $itemName = $l['name'];
            $ret[$itemID] = [
                'title' => ($isGrowing) ? "Остаток по {$itemName}" : $itemName,
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'levels' => [
                    CashFlowsBase::FLOW_TYPE_INCOME => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    CashFlowsBase::FLOW_TYPE_EXPENSE => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ],
            ];
        }

        return $ret;
    }

    public function getByFlowAttributeEmptyGrowingData($byDays = false, $params = [])
    {
        $params['is_growing'] = true;
        return $this->getByFlowAttributeEmptyData($byDays, $params);
    }
    
}