<?php
namespace frontend\modules\analytics\models\odds;

use Yii;
use common\components\debts\DebtsHelper;
use common\components\TextHelper;
use common\models\document\Invoice;
use frontend\models\Documents;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\documents\components\InvoiceStatistic;

trait WarningsTrait {

    public function generateWarnings($growingBalance, $types)
    {
        $warnings = [];
        if ($growingBalance) {
            foreach ($growingBalance as $monthNumber => $oneAmount) {
                if ($oneAmount < 0) {
                    if (isset($warnings[1])) {
                        $warnings[1]['moreThenOne'] = true;
                    } else {
                        $warnings[1] = [
                            'amount' => abs($oneAmount),
                            'month' => AbstractFinance::$monthEnd[substr($monthNumber, 0, 2)],
                            'moreThenOne' => false,
                        ];
                    }
                }
            }
        }

        $totalDebtSum = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_0_MORE_90, true, false);
        if ($totalDebtSum) {
            $incomeOperatingActivitiesAmount = 1;
            $types = (isset($types[AbstractFinance::OPERATING_ACTIVITIES_BLOCK]['levels'][AbstractFinance::INCOME_OPERATING_ACTIVITIES])) ?
                $types[AbstractFinance::OPERATING_ACTIVITIES_BLOCK]['levels'][AbstractFinance::INCOME_OPERATING_ACTIVITIES] : null;
            if (isset($types['data'][date('m')]) && $types['data'][date('m')] > 0) {
                $incomeOperatingActivitiesAmount = $types['data'][date('m')];
            }
            $incomePercent = round(($totalDebtSum / 2 / $incomeOperatingActivitiesAmount) * 100, 2);
            $warnings[2] = [
                'totalDebtSum' => $totalDebtSum,
                'incomePercent' => $incomePercent,
            ];
        }
        $inInvoiceDebtSum = Invoice::find()
            ->select([
                'SUM(' . Invoice::tableName() . '.total_amount_with_nds) - IFNULL(SUM(' . Invoice::tableName() . '.payment_partial_amount), 0) AS sum',
            ])
            ->byIOType(Documents::IO_TYPE_IN)
            ->byDeleted()
            ->byStatus(InvoiceStatistic::$invoiceStatuses[InvoiceStatistic::NOT_PAID][Documents::IO_TYPE_IN])
            ->byCompany($this->_multiCompanyIds)
            ->asArray()
            ->one();
        $inInvoiceDebtSum = $inInvoiceDebtSum ? $inInvoiceDebtSum['sum'] : 0;
        if ($inInvoiceDebtSum) {
            $warnings[3] = [
                'inInvoiceDebtSum' => $inInvoiceDebtSum,
            ];
            $growingBalanceAmount = 0;
            if (isset($growingBalance[date('m')])) {
                $growingBalanceAmount = $growingBalance[date('m')];
            }
            if ($growingBalanceAmount >= $inInvoiceDebtSum) {
                $warnings[3]['message'] = 'Текущего остатка денежных средств хватит полностью оплатить все счета поставщикам.';
            } else {
                $canPayedPercent = round(($growingBalanceAmount / $inInvoiceDebtSum) * 100, 2);
                $warnings[3]['message'] = 'Текущего остатка денежных средств НЕ хватит полностью оплатить все счета
                 поставщикам. Можно оплатить только ' . $canPayedPercent . '% задолженности перед поставщиками.';
                if ($totalDebtSum) {
                    $zRaznitsa = $inInvoiceDebtSum - $growingBalanceAmount;
                    if ($zRaznitsa <= $totalDebtSum) {
                        $warnings[4] = [
                            'message' => 'Собрав ' . round(($zRaznitsa / $totalDebtSum) * 100, 2) . '% долгов ваших клиентов, вы сможете полностью оплатить долги перед поставщиками.',
                        ];
                    } else {
                        $warnings[4] = [
                            'message' => '<span style="color: red;">Плохие новости!</span><br>
                            Даже собрав 100% задолженности покупателей, вы не сможете рассчитаться с поставщиками.<br>
                            Не хватает ' . TextHelper::invoiceMoneyFormat($zRaznitsa - $totalDebtSum, 2) . ' <i class="fa fa-rub"></i>.
                            Выход:
                            <ul style="padding-left: 17px;">
                                <li>
                                    Увеличить продажи;
                                </li>
                                <li>
                                    Внести собственные деньги в компанию (дать займ);
                                </li>
                                <li>
                                    Взять кредит.
                                </li>
                            </ul>',
                            'debtPercent' => round(($zRaznitsa / $totalDebtSum) * 100, 2),
                        ];
                    }
                }
            }
        }

        return $warnings;
    }
}