<?php
namespace frontend\modules\analytics\models\odds;

use Yii;
use common\models\cash\CashFlowsBase;
use common\models\currency\Currency;
use common\models\currency\CurrencyRate;
use frontend\modules\analytics\models\AbstractFinance;

trait SearchByCurrencyTrait {

    public function searchByCurrency($params = [])
    {
        $_data = $this->getCashFlowData($params);
        $data = [];
        $growingData = [];
        $totalData = [];
        $emptyValues = self::EMPTY_YEAR;
        $income = CashFlowsBase::FLOW_TYPE_INCOME;
        $expense = CashFlowsBase::FLOW_TYPE_EXPENSE;

        foreach ($_data['cashData'] as $c => $cData) {
            foreach ($cData as $w => $wData) {
                $wallet = $c.'_'.$w;
                $data[$wallet] = [
                    'title' => $this->getWalletTitle($w, $c),
                    'question' => $this->getWalletTooltip($w),
                    'data' => $emptyValues,
                    'levels' => [],
                ];
                foreach ($wData['data'] as $period => $amount) {
                    $p = substr($period, 0, 2);
                    $data[$wallet]['data'][$p] += $amount;
                }

                $growingData[$wallet] = [
                    'title' => $this->getWalletBalanceTitle($w, $c),
                    'data' => $emptyValues,
                ];
                foreach ($wData['balance'] as $period => $amount) {
                    if ($period === 0) continue;
                    $p = substr($period, 0, 2);
                    $growingData[$wallet]['data'][$p] = $amount;
                }

                foreach ($wData['items'] as $t => $tData) {
                    $type = $wallet.'_'.$t;
                    $data[$wallet]['levels'][$type] = [
                        'title' => $tData['title'],
                        'data' => $emptyValues,
                        'levels' => [],
                    ];

                    foreach ($tData['data'] as $period => $amount) {
                        $p = substr($period, 0, 2);
                        $data[$wallet]['levels'][$type]['data'][$p] += $amount;
                    }

                    foreach ($tData['items'] as $itemKey => $itemData) {
                        $data[$wallet]['levels'][$type]['levels'][$itemKey] = [
                            'title' => $itemData['title'],
                            'data' => $emptyValues,
                            'levels' => [],
                        ];

                        foreach ($itemData['data'] as $period => $amount) {
                            $p = substr($period, 0, 2);
                            $data[$wallet]['levels'][$type]['levels'][$itemKey]['data'][$p] += $amount;
                        }

                        foreach ($itemData['items'] as $subitemKey => $subitemData) {
                            $data[$wallet]['levels'][$type]['levels'][$itemKey]['levels'][$subitemKey] = [
                                'title' => $subitemData['title'],
                                'data' => $emptyValues,
                                'levels' => [],
                            ];

                            foreach ($subitemData['data'] as $period => $amount) {
                                $p = substr($period, 0, 2);
                                $data[$wallet]['levels'][$type]['levels'][$itemKey]['levels'][$subitemKey]['data'][$p] += $amount;
                            }
                        }
                    }
                }
            }
        }
/*
        $wallet = 'base';

        $data[$wallet] = [
            'title' => $_data['baseData']['title'],
            'data' => $emptyValues,
            'levels' => [],
        ];

        foreach ($_data['baseData']['data'] as $period => $amount) {
            $p = substr($period, 0, 2);
            $data[$wallet]['data'][$p] += $amount;
        }

        $growingData[$wallet] = [
            'title' => 'Остаток на конец периода в базовой валюте',
            'data' => $emptyValues,
        ];
        foreach ($_data['baseData']['balance'] as $period => $amount) {
            if ($period === 0) {
                continue;
            }
            $p = substr($period, 0, 2);
            $growingData[$wallet]['data'][$p] = $amount;
        }

        foreach ($_data['baseData']['items'] as $t => $tData) {
            $type = $wallet.'_'.$t;
            $data[$wallet]['levels'][$type] = [
                'title' => $tData['title'],
                'data' => $emptyValues,
                'levels' => [],
            ];

            foreach ($tData['data'] as $period => $amount) {
                $p = substr($period, 0, 2);
                $data[$wallet]['levels'][$type]['data'][$p] += $amount;
            }

            foreach ($tData['items'] as $itemKey => $itemData) {
                $data[$wallet]['levels'][$type]['levels'][$itemKey] = [
                    'title' => $itemData['title'],
                    'data' => $emptyValues,
                    'levels' => [],
                ];

                foreach ($itemData['data'] as $period => $amount) {
                    $p = substr($period, 0, 2);
                    $data[$wallet]['levels'][$type]['levels'][$itemKey]['data'][$p] += $amount;
                }

                foreach ($itemData['items'] as $subitemKey => $subitemData) {
                    $data[$wallet]['levels'][$type]['levels'][$itemKey]['levels'][$subitemKey] = [
                        'title' => $subitemData['title'],
                        'data' => $emptyValues,
                        'levels' => [],
                    ];

                    foreach ($subitemData as $period => $amount) {
                        $p = substr($period, 0, 2);
                        $data[$wallet]['levels'][$type]['levels'][$itemKey]['levels'][$subitemKey]['data'][$p] += $amount;
                    }
                }
            }
        }

        foreach ($_data['difference'] as $c => $cData) {
            foreach ($cData as $t => $itemData) {
                if ($t === 'data') continue;
                $type = $wallet.'_'.$t;
                $itemKey = $type.'_'.$c;
                $data[$wallet]['levels'][$type]['levels'][$c] = [
                    'title' => $itemData['title'],
                    'data' => $emptyValues,
                    'isSumQuarters' => $itemData['isSumQuarters'] ?? true,
                    'levels' => [],
                ];
            }
            $diffData = $emptyValues;
            foreach ($cData['data'] as $period => $amount) {
                $p = substr($period, 0, 2);
                $diffData[$p] += $amount;
            }
            foreach ($diffData as $p => $amount) {
                if ($amount > 0) {
                    $data[$wallet]['levels'][$wallet.'_'.$income]['levels'][$c]['data'][$p] = $amount;
                } elseif ($amount < 0) {
                    $data[$wallet]['levels'][$wallet.'_'.$expense]['levels'][$c]['data'][$p] = -$amount;
                }
            }
        }
*/
        $totalData['month'] = [
            'title' => $_data['baseData']['title'],
            'data' => $emptyValues,
        ];
        foreach ($_data['baseData']['data'] as $period => $amount) {
            $p = substr($period, 0, 2);
            $totalData['month']['data'][$p] += $amount;
        }

        $totalData['growing'] = [
            'title' => 'Остаток на конец периода в базовой валюте',
            'data' => $emptyValues,
        ];
        foreach ($_data['baseData']['balance'] as $period => $amount) {
            if ($period === 0) {
                continue;
            }
            $p = substr($period, 0, 2);
            $totalData['growing']['data'][$p] = $amount;
        }

        // sort
        $this->sortItems($data);
        $this->sortSubItems($data);

        return [
            'data' => $data,
            'growingData' => $growingData,
            'totalData' => $totalData,
        ];
    }

    public function searchByCurrencyDays($params = [])
    {
        $_data = $this->getCashFlowData($params);
        $data = [];
        $growingData = [];
        $totalData = [];

        foreach ($_data['cashData'] as $c => $cData) {
            foreach ($cData as $w => $wData) {
                $wallet = $c.'_'.$w;
                $data[$wallet] = [
                    'title' => $this->getWalletTitle($w, $c),
                    'question' => $this->getWalletTooltip($w),
                    'data' => $wData['data'],
                    'levels' => [],
                ];
                $balance = $wData['balance'];
                unset($balance[0]);
                $growingData[$wallet] = [
                    'title' => $this->getWalletBalanceTitle($w, $c),
                    'data' => $balance,
                ];
                foreach ($wData['items'] as $t => $tData) {
                    $type = $wallet.'_'.$t;
                    $data[$wallet]['levels'][$type] = [
                        'title' => $tData['title'],
                        'data' => $tData['data'],
                        'levels' => [],
                    ];

                    foreach ($tData['items'] as $itemKey => $itemData) {
                        $data[$wallet]['levels'][$type]['levels'][$itemKey] = [
                            'title' => $itemData['title'],
                            'data' => $itemData['data'],
                            'levels' => [],
                        ];

                        foreach ($itemData['items'] as $subitemKey => $subitemData) {
                            $data[$wallet]['levels'][$type]['levels'][$itemKey]['levels'][$subitemKey] = [
                                'title' => $subitemData['title'],
                                'data' => $subitemData['data'],
                                'levels' => [],
                            ];
                        }
                    }
                }
            }
        }

        $totalBalance = $_data['baseData']['balance'];
        unset($totalBalance[0]);
/*
        $wallet = 'base';
        $data[$wallet] = [
            'title' => $_data['baseData']['title'],
            'data' => $_data['baseData']['data'],
            'levels' => [],
        ];
        $growingData[$wallet] = [
            'title' => 'Остаток на конец периода в базовой валюте',
            'data' => $totalBalance,
        ];
        foreach ($_data['baseData']['items'] as $t => $tData) {
            $type = $wallet.'_'.$t;
            $data[$wallet]['levels'][$type] = [
                'title' => $tData['title'],
                'data' => $tData['data'],
                'levels' => [],
            ];

            foreach ($tData['items'] as $itemKey => $itemData) {
                $data[$wallet]['levels'][$type]['levels'][$itemKey] = [
                    'title' => $itemData['title'],
                    'data' => $itemData['data'],
                    'isSumQuarters' => $itemData['isSumQuarters'] ?? true,
                    'levels' => [],
                ];

                foreach ($itemData['items'] as $subitemKey => $subitemData) {
                    $data[$wallet]['levels'][$type]['levels'][$itemKey]['levels'][$subitemKey] = [
                        'title' => $subitemData['title'],
                        'data' => $subitemData['data'],
                        'levels' => [],
                    ];
                }
            }
        }

        foreach ($_data['difference'] as $c => $cData) {
            foreach ($cData as $t => $itemData) {
                if ($t === 'data') continue;
                $type = $wallet.'_'.$t;
                $data[$wallet]['levels'][$type]['levels'][$c] = [
                    'title' => $itemData['title'],
                    'data' => $itemData['data'],
                    'isSumQuarters' => $itemData['isSumQuarters'] ?? true,
                    'levels' => [],
                ];
            }
        }
*/

        $totalData['month'] = [
            'title' => $_data['baseData']['title'],
            'data' => $_data['baseData']['data'],
        ];
        $totalData['growing'] = [
            'title' => 'Остаток на конец периода в базовой валюте',
            'data' => $totalBalance,
        ];

        return [
            'data' => $data,
            'growingData' => $growingData,
            'totalData' => $totalData,
        ];
    }

    public function getCashFlowData($params = [])
    {
        $rawData = $params['rawData'] ?? $this->getByCurrencyRawDataDay($params);
        $emptyValues = self::getEmptyYearInDays($this->year);
        $subArticles = [
            CashFlowsBase::FLOW_TYPE_INCOME => $this->getArticles2Parents(CashFlowsBase::FLOW_TYPE_INCOME),
            CashFlowsBase::FLOW_TYPE_EXPENSE => $this->getArticles2Parents(CashFlowsBase::FLOW_TYPE_EXPENSE),
        ];
        $walletList = [
            AbstractFinance::CASH_BANK_BLOCK,
            AbstractFinance::CASH_ORDER_BLOCK,
            AbstractFinance::CASH_EMONEY_BLOCK,
            AbstractFinance::CASH_ACQUIRING_BLOCK,
            AbstractFinance::CASH_CARD_BLOCK,
        ];
        $income = CashFlowsBase::FLOW_TYPE_INCOME;
        $expense = CashFlowsBase::FLOW_TYPE_EXPENSE;
        $emptyIoItems = [
            $income => [
                'title' => 'Приход',
                'data' => $emptyValues,
                'items' => [],
            ],
            $expense => [
                'title' => 'Расход',
                'data' => $emptyValues,
                'items' => [],
            ],
        ];
        $cashFlow = [];

        foreach (Currency::$nameArray as $c) {
            foreach ($walletList as $w) {
                $cashFlow[$c][$w] = [];
            }
        }

        $rateData = [];
        foreach ($emptyValues as $key => $value) {
            $date = date_create_from_format('Ymd', $this->year.$key);
            $rateData[$key] = (array) ($date ? CurrencyRate::getRateOnDate($date) : []);
        }

        $baseData = [
            'title' => 'Чистый денежный поток в базовой валюте',
            'data' => $emptyValues,
            'balance' => [0 => 0] + $emptyValues,
            'items' => [
                $income => [
                    'title' => 'Приход в базовой валюте',
                    'data' => $emptyValues,
                    'items' => [],
                ],
                $expense => [
                    'title' => 'Расход в базовой валюте',
                    'data' => $emptyValues,
                    'items' => [],
                ],
            ],
        ];

        // start year balance
        foreach ($rawData['cashStart'] as $d) {
            $cashFlow[$d['c']][$d['w']] = [
                'data' => $emptyValues,
                'items' => $emptyIoItems,
                'balance' => [0 => $d['a']] + $emptyValues,
            ];
        }

        //parsing raw data
        foreach ($rawData['cashFlows'] as $d) {
            $c = $d['c'] ?: Currency::DEFAULT_NAME;
            $w = $d['w'];
            $t = $d['type'];
            $amount = $d['a'];
            $p = $d['m'].$d['d'];
            $k = ($t == $expense ? -1 : 1);

            $item = $d['item_id'];
            $subItem = null;
            if (in_array($d['contractor_id'], ['bank', 'order', 'emoney'])) {
                $item = $d['contractor_id'];
            }
            if (isset($subArticles[$t][$d['item_id']])) {
                $item = $subArticles[$t][$d['item_id']];
                $subItem = $d['item_id'];
            }

            if (empty($cashFlow[$c][$w])) {
                $cashFlow[$c][$w] = [
                    'data' => $emptyValues,
                    'items' => $emptyIoItems,
                    'balance' => [0 => 0] + $emptyValues,
                ];
            }

            $cashFlow[$c][$w]['data'][$p] += $k * $amount;
            $cashFlow[$c][$w]['items'][$t]['data'][$p] += $amount;

            if (!isset($cashFlow[$c][$w]['items'][$t]['items'][$item])) {
                $cashFlow[$c][$w]['items'][$t]['items'][$item] = [
                    'title' => $this->__getCashContractorName($t, $item, $d['contractor_id']),
                    'data' => $emptyValues,
                    'items' => [],
                ];
            }
            $cashFlow[$c][$w]['items'][$t]['items'][$item]['data'][$p] += $amount;

            if ($subItem) {
                if (!isset($cashFlow[$c][$w]['items'][$t]['items'][$item]['items'][$subItem])) {
                    $cashFlow[$c][$w]['items'][$t]['items'][$item]['items'][$subItem] = [
                        'title' => $this->__getCashContractorName($t, $subItem),
                        'data' => $emptyValues,
                    ];
                }
                $cashFlow[$c][$w]['items'][$t]['items'][$item]['items'][$subItem]['data'][$p] += $amount;
            }
        }

        $difference = [];
        $balance = [];

        // deletin empty data
        foreach ($cashFlow as $c => $cData) {
            foreach ($cData as $w => $wData) {
                if (empty($cashFlow[$c][$w])) {
                    unset($cashFlow[$c][$w]);
                }
            }

            if (empty($cashFlow[$c])) {
                unset($cashFlow[$c]);
                continue;
            }

            $balance[$c] = [0 => 0] + $emptyValues;

            if ($c != Currency::DEFAULT_NAME) {
                $difference[$c] = [
                    $income => [
                        'title' => $this->exchangeDifferenceName($income, $c),
                        'dataHoverText' => 'Расчетная величина',
                        'data' => $emptyValues,
                        'isSumQuarters' => false,
                        'items' => [],
                    ],
                    $expense => [
                        'title' => $this->exchangeDifferenceName($expense, $c),
                        'dataHoverText' => 'Расчетная величина',
                        'data' => $emptyValues,
                        'isSumQuarters' => false,
                        'items' => [],
                    ],
                ];
            }
        }

        // data calculation
        $lastP = 0;
        $emptyRate = [
            'amount' => 1,
            'value' => 1,
        ];
        foreach ($rateData as $p => $rateArray) {
            foreach ($cashFlow as $c => $cData) {
                if ($c == Currency::DEFAULT_NAME) {
                    $rate = $emptyRate;
                    $lastRate = $emptyRate;
                } else {
                    $rate = $rateArray[$c] ?? $emptyRate;
                    $lastRate = $lastP === 0 ? $rate : ($rateData[$lastP][$c] ?? $emptyRate);
                }

                foreach ($cData as $w => $wData) {
                    if ($lastP === 0) {
                        $balance[$c][0] += $cashFlow[$c][$w]['balance'][0];
                    }
                    $cashFlow[$c][$w]['balance'][$p] = $cashFlow[$c][$w]['balance'][$lastP] + $wData['data'][$p];
                    $balance[$c][$p] += $cashFlow[$c][$w]['balance'][$p];

                    foreach ($wData['items'] as $t => $tData) {
                        foreach ($tData['items'] as $itemKey => $itemData) {
                            if (!isset($baseData['items'][$t]['items'][$itemKey])) {
                                $baseData['items'][$t]['items'][$itemKey] = [
                                    'title' => $itemData['title'],
                                    'data' => $emptyValues,
                                    'items' => [],
                                ];
                            }
                            foreach ($itemData['items'] as $subitemKey => $subitemData) {
                                if (!isset($baseData['items'][$t]['items'][$itemKey]['items'][$subitemKey])) {
                                    $baseData['items'][$t]['items'][$itemKey]['items'][$subitemKey] = [
                                        'title' => $subitemData['title'],
                                        'data' => $emptyValues,
                                    ];
                                }
                                if (($subitemAmount = intval(round($subitemData['data'][$p]/$rate['amount']*$rate['value']))) > 0) {
                                    $baseData['items'][$t]['items'][$itemKey]['items'][$subitemKey]['data'][$p] += $subitemAmount;
                                }
                            }

                            if (($itemAmount = intval(round($itemData['data'][$p]/$rate['amount']*$rate['value']))) > 0) {
                                if ($t == $expense) {
                                    $baseData['data'][$p] -= $itemAmount;
                                } else {
                                    $baseData['data'][$p] += $itemAmount;
                                }
                                $baseData['items'][$t]['data'][$p] += $itemAmount;
                                $baseData['items'][$t]['items'][$itemKey]['data'][$p] += $itemAmount;
                            }

                        }
                        uasort($cashFlow[$c][$w]['items'][$t]['items'], function ($a, $b) {
                            if ($a['title'] == $b['title']) return 0;
                            return ($a['title'] < $b['title']) ? -1 : 1;
                        });
                    }
                }

                $lastBalance = $balance[$c][$lastP];
                $currentBalance = $balance[$c][$p];

                if ($c != Currency::DEFAULT_NAME) {
                    if ($lastBalance == 0 || ($rate['amount'] == $lastRate['amount'] && $rate['value'] == $lastRate['value'])) {
                        $diff = 0;
                    } else {
                        $diff = (int) round(($lastBalance/$rate['amount']*$rate['value']) - ($lastBalance/$lastRate['amount']*$lastRate['value']));
                    }

                    $difference[$c]['data'][$p] = $diff;
                    if ($diff > 0) {
                        $baseData['data'][$p] += $diff;
                        $difference[$c][$income]['data'][$p] = $diff;
                        $baseData['items'][$income]['data'][$p] += $diff;
                    } elseif ($diff < 0) {
                        $baseData['data'][$p] -= abs($diff);
                        $difference[$c][$expense]['data'][$p] = abs($diff);
                        $baseData['items'][$expense]['data'][$p] += abs($diff);
                    }
                }
            }
            if ($lastP === 0) {
                foreach ($balance as $c => $value) {
                    $rate = $rateArray[$c] ?? $emptyRate;
                    $baseData['balance'][0] += intval(round($balance[$c][0] * $rate['value'] / $rate['amount']));
                }
            }
            $baseData['balance'][$p] = $baseData['balance'][$lastP] + $baseData['data'][$p];
            $lastP = $p;
        }

        foreach ($baseData['items'] as $t => $value) {
            uasort($baseData['items'][$t]['items'], function ($a, $b) {
                if ($a['title'] == $b['title']) return 0;
                return ($a['title'] < $b['title']) ? -1 : 1;
            });
        }

        return [
            'cashData' => $cashFlow,
            'baseData' => $baseData,
            'difference' => $difference,
        ];
    }

    public function getByCurrencyRawDataDay($params = [])
    {
        $table = self::TABLE_OLAP_FLOWS;
        $companyIds = implode(',', $this->_multiCompanyIds);
        $year = $this->year;
        $AND_FILTERS = $this->getSqlFilters($params);

        $cashFlows = "
            SELECT
                t.currency_name c,
                IF(t.wallet > 10, t.wallet-10, t.wallet) w,
                t.type,
                t.item_id,
                t.contractor_id,
                DATE_FORMAT(t.date, '%m') m,
                DATE_FORMAT(t.date, '%d') d,
                SUM(t.original_amount) a
            FROM {$table} t
            WHERE
                t.company_id IN ({$companyIds})
                AND t.date BETWEEN '{$year}-01-01' AND '{$year}-12-31'
                {$AND_FILTERS}
            GROUP BY c, w, t.type, t.item_id, t.contractor_id, m, d
        ";

        $cashStart = "
            SELECT
                t.currency_name c,
                IF(t.wallet > 10, t.wallet-10, t.wallet) w,
                SUM(IF(t.type = 1, t.original_amount, -t.original_amount)) a
            FROM {$table} t
            WHERE
                t.company_id IN ({$companyIds})
                AND t.date < '{$year}-01-01'
                {$AND_FILTERS}
            GROUP BY c, w
        ";

        return [
            'cashFlows' => Yii::$app->db2->createCommand($cashFlows)->queryAll(),
            'cashStart' => Yii::$app->db2->createCommand($cashStart)->queryAll(),
        ];
    }

    public function getWalletTitle($wallet, $currencyName)
    {
        $titles = [
            AbstractFinance::CASH_BANK_BLOCK => 'Банк',
            AbstractFinance::CASH_ORDER_BLOCK => 'Касса',
            AbstractFinance::CASH_EMONEY_BLOCK => 'E-money',
            AbstractFinance::CASH_ACQUIRING_BLOCK => 'Интернет-эквайринг',
            AbstractFinance::CASH_CARD_BLOCK => 'Карты',
        ];

        return sprintf('%s %s', $titles[$wallet], $currencyName);
    }

    public function getWalletBalanceTitle($wallet, $currencyName)
    {
        $titles = [
            AbstractFinance::CASH_BANK_BLOCK => 'Остаток Банк',
            AbstractFinance::CASH_ORDER_BLOCK => 'Остаток Касса',
            AbstractFinance::CASH_EMONEY_BLOCK => 'Остаток E-money',
            AbstractFinance::CASH_ACQUIRING_BLOCK => 'Остаток Интернет-эквайринг',
            AbstractFinance::CASH_CARD_BLOCK => 'Остаток Карты',
        ];

        return sprintf('%s %s', $titles[$wallet], $currencyName);
    }

    public function getWalletTooltip($wallet)
    {
        $tooltips = [
            AbstractFinance::CASH_BANK_BLOCK => '#tooltip_block_bank',
            AbstractFinance::CASH_ORDER_BLOCK => '#tooltip_block_order',
            AbstractFinance::CASH_EMONEY_BLOCK => '#tooltip_block_emoney',
            AbstractFinance::CASH_ACQUIRING_BLOCK => '#tooltip_block_acquiring',
            AbstractFinance::CASH_CARD_BLOCK => '#tooltip_block_card',
        ];

        return $tooltips[$wallet];
    }

    public function getByCurrencyEmptyData($byDays = false)
    {
        return [
            AbstractFinance::CASH_BANK_BLOCK => [
                'title' => 'Банк',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'question' => '#tooltip_block_bank',
                'levels' => [
                    AbstractFinance::INCOME_CASH_BANK => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::EXPENSE_CASH_BANK => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ],
            ],
            AbstractFinance::CASH_ORDER_BLOCK => [
                'title' => 'Касса',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'question' => '#tooltip_block_order',
                'levels' => [
                    AbstractFinance::INCOME_CASH_ORDER => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::EXPENSE_CASH_ORDER => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ],
            ],
            AbstractFinance::CASH_EMONEY_BLOCK => [
                'title' => 'E-money',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'question' => '#tooltip_block_emoney',
                'levels' => [
                    AbstractFinance::INCOME_CASH_EMONEY => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::EXPENSE_CASH_EMONEY => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ],
            ],
            AbstractFinance::CASH_ACQUIRING_BLOCK => [
                'title' => 'Интернет-эквайринг',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'question' => '#tooltip_block_acquiring',
                'levels' => [
                    AbstractFinance::INCOME_ACQUIRING => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::EXPENSE_ACQUIRING => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ],
            ],
            AbstractFinance::CASH_CARD_BLOCK => [
                'title' => 'Карты',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'question' => '#tooltip_block_card',
                'levels' => [
                    AbstractFinance::INCOME_CARD => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::EXPENSE_CARD => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ],
            ],
        ];
    }

    public function getByCurrencyEmptyGrowingData($byDays = false)
    {
        return [
            AbstractFinance::CASH_BANK_BLOCK => [
                'title' => 'Остаток по банку',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
            ],
            AbstractFinance::CASH_ORDER_BLOCK => [
                'title' => 'Остаток по кассе',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
            ],
            AbstractFinance::CASH_EMONEY_BLOCK => [
                'title' => 'Остаток по E-money',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
            ],
            AbstractFinance::CASH_ACQUIRING_BLOCK => [
                'title' => 'Остаток по интернет-эквайрингу',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
            ],
            AbstractFinance::CASH_CARD_BLOCK => [
                'title' => 'Остаток по картам',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
            ],
        ];
    }
}