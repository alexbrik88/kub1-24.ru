<?php
namespace frontend\modules\analytics\models;

use frontend\modules\analytics\models\AnalyticsArticle;
use Yii;
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\Company;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class AnalyticsArticleForm extends Model
{
    const REVENUE_RULE_GROUP_UNSET = 0;
    const REVENUE_RULE_GROUP_CUSTOMER = 1;
    const REVENUE_RULE_GROUP_PRODUCT = 2;
    const REVENUE_RULE_GROUP_PRODUCT_GROUP = 3;
    const REVENUE_RULE_GROUP_PROJECT = 4;
    const REVENUE_RULE_GROUP_SALE_POINT = 5;
    const REVENUE_RULE_GROUP_INDUSTRY = 6;

    const REVENUE_RULE_UNSET = 0;
    const REVENUE_RULE_TOP = 1;
    const REVENUE_RULE_LAST_5 = 2;
    const REVENUE_RULE_LAST_10 = 3;
    const REVENUE_RULE_LAST_20 = 4;

    const MONTH_GROUP_UNSET = 0;
    const MONTH_GROUP_INDUSTRY = 1;
    const MONTH_GROUP_SALE_POINT = 2;
    const MONTH_GROUP_PROJECT = 3;

    private $_company;

    public $incomeItems;
    public $expenseItems;

    public $skipPrepayments;
    public $skipTaxes;
    public $skipZeroesRows;
    public $skipCalculatePrimeCosts;
    public $calcWithoutNds;
    public $showAmortization;
    public $showEbit;
    public $revenueRuleGroup;
    public $revenueRule;
    public $monthGroupBy;

    public $hasChangedOptions;

    public static $revenueRuleGroups = [
        self::REVENUE_RULE_GROUP_UNSET => 'Без аналитики',
        self::REVENUE_RULE_GROUP_INDUSTRY => 'Разбивка по направлениям',
        self::REVENUE_RULE_GROUP_SALE_POINT => 'Разбивка по точкам продаж',
        self::REVENUE_RULE_GROUP_CUSTOMER => 'Разбивка по покупателям',
        self::REVENUE_RULE_GROUP_PRODUCT => 'Разбивка по номенклатуре товаров и услуг',
        self::REVENUE_RULE_GROUP_PRODUCT_GROUP => 'Разбивка по группам товаров и услуг',
        self::REVENUE_RULE_GROUP_PROJECT => 'Разбивка по проектам',
    ];

    public static $revenueRules = [
        self::REVENUE_RULE_TOP => 'Выводить ТОП-10 записей, оставшиеся выводить как «Остальные»',
        self::REVENUE_RULE_LAST_5 => 'Объединять записи, по которым выручка меньше 5% от общей в «Остальные»',
        self::REVENUE_RULE_LAST_10 => 'Объединять записи, по которым выручка меньше 10% от общей в «Остальные»',
        self::REVENUE_RULE_LAST_20 => 'Объединять записи, по которым выручка меньше 20% от общей в «Остальные»',
    ];

    public static $monthGroups = [
        self::MONTH_GROUP_UNSET => 'Без детализации',
        self::MONTH_GROUP_INDUSTRY => 'По направлениям',
        self::MONTH_GROUP_SALE_POINT => 'По точкам продаж',
        self::MONTH_GROUP_PROJECT => 'По проектам',
    ];

    public function __construct(Company $company, $config = [])
    {
        parent::__construct($config);
        $this->_company = $company;
        $this->skipPrepayments = AnalyticsOptions::getOption($company->id, AnalyticsOptions::KEY_PAL_SKIP_PREPAYMENTS) ?: false;
        $this->skipTaxes = AnalyticsOptions::getOption($company->id, AnalyticsOptions::KEY_PAL_SKIP_TAXES) ?: false;
        $this->skipZeroesRows = AnalyticsOptions::getOption($company->id, AnalyticsOptions::KEY_PAL_SKIP_ZEROES_ROWS) ?: false;
        $this->skipCalculatePrimeCosts = AnalyticsOptions::getOption($company->id, AnalyticsOptions::KEY_PAL_SKIP_CALCULATE_PRIME_COSTS) ?: false;
        $this->showAmortization = AnalyticsOptions::getOption($company->id, AnalyticsOptions::KEY_PAL_SHOW_AMORTIZATION) ?: false;
        $this->calcWithoutNds = AnalyticsOptions::getOption($company->id, AnalyticsOptions::KEY_PAL_CALC_WITHOUT_NDS) ?: false;
        $this->showEbit = AnalyticsOptions::getOption($company->id, AnalyticsOptions::KEY_PAL_SHOW_EBIT) ?: false;
        $this->revenueRuleGroup = AnalyticsOptions::getOption($company->id, AnalyticsOptions::KEY_PAL_REVENUE_RULE_GROUP) ?: self::REVENUE_RULE_GROUP_UNSET;
        $this->revenueRule = AnalyticsOptions::getOption($company->id, AnalyticsOptions::KEY_PAL_REVENUE_RULE) ?: self::REVENUE_RULE_TOP;
        $this->monthGroupBy = AnalyticsOptions::getOption($company->id, AnalyticsOptions::KEY_PAL_MONTH_GROUP_BY) ?: self::MONTH_GROUP_UNSET;
    }

    public function rules()
    {
        return [
            [['incomeItems'], 'validateIncomeItems'],
            [['expenseItems'], 'validateExpenditureItems'],
            [['skipPrepayments', 'skipTaxes', 'skipZeroesRows', 'skipCalculatePrimeCosts', 'showAmortization', 'showEbit', 'calcWithoutNds'], 'boolean'],
            [['revenueRuleGroup'], 'in', 'range' => array_keys(self::$revenueRuleGroups)],
            [['revenueRule'], 'in', 'range' => array_keys(self::$revenueRules)],
            [['monthGroupBy'], 'in', 'range' => array_keys(self::$monthGroups)],
        ];
    }

    public function beforeValidate()
    {
        if (is_array($this->revenueRule)) {
            $this->revenueRule = ArrayHelper::getValue($this->revenueRule, $this->revenueRuleGroup, self::REVENUE_RULE_TOP);
        }

        return parent::beforeValidate();
    }

    public function validateIncomeItems($attribute)
    {
        return $this->_validateItems($attribute, $type = CashFlowsBase::FLOW_TYPE_INCOME);
    }

    public function validateExpenditureItems($attribute)
    {
        return $this->_validateItems($attribute, $type = CashFlowsBase::FLOW_TYPE_EXPENSE);
    }

    private function _validateItems($attribute, $type)
    {
        foreach ((array)$this->$attribute as $itemId => $palTypeId) {

            $errorMessage = '';

            if (!AnalyticsArticle::find([
                'company_id' => $this->_company->id,
                'type' => $type,
                'item_id' => $itemId
            ])->exists()) {
                $errorMessage = 'Неизвестная статья ' . $itemId;
            }

            if ($type === CashFlowsBase::FLOW_TYPE_INCOME) {
                if (!in_array($palTypeId, [
                    AnalyticsArticle::PAL_UNSET,
                    AnalyticsArticle::PAL_INCOME_REVENUE,
                    AnalyticsArticle::PAL_INCOME_OTHER,
                    AnalyticsArticle::PAL_INCOME_PERCENT,
                ])) {
                    $errorMessage = 'Неизвестная группа приходов ' . $palTypeId;
                }
            } elseif ($type === CashFlowsBase::FLOW_TYPE_EXPENSE) {
                if (!in_array($palTypeId, [
                    AnalyticsArticle::PAL_UNSET,
                    AnalyticsArticle::PAL_EXPENSES_VARIABLE,
                    AnalyticsArticle::PAL_EXPENSES_FIXED,
                    AnalyticsArticle::PAL_EXPENSES_OTHER,
                    AnalyticsArticle::PAL_EXPENSES_PERCENT,
                    AnalyticsArticle::PAL_EXPENSES_DIVIDEND,
                    AnalyticsArticle::PAL_EXPENSES_PRIME_COST,
                ])) {
                    $errorMessage = 'Неизвестная группа расходов ' . $palTypeId;
                }
            } else {
                $errorMessage = 'Тип движения не определен ' . $type;
            }

            if ($errorMessage) {
                $this->addError($attribute, $errorMessage);
            }
        }

        return;
    }

    public function save()
    {
        $type2Items = [
            CashFlowsBase::FLOW_TYPE_INCOME => &$this->incomeItems,
            CashFlowsBase::FLOW_TYPE_EXPENSE => &$this->expenseItems
        ];
        
        foreach ($type2Items as $type => $items) {
            if (!is_array($items)) continue;
            foreach ($items as $itemId => $palTypeId) {

                if ($model = AnalyticsArticle::findOne([
                    'company_id' => $this->_company->id,
                    'type' => $type,
                    'item_id' => $itemId
                ])) {

                    if ($model->profit_and_loss_type != $palTypeId)
                    {
                        $model->profit_and_loss_type = $palTypeId;
                        $model->save();

                        $this->hasChangedOptions = true;
                    }
                }
            }
        }

        $this->saveAdditionalOptions();

        return true;
    }

    public function saveAdditionalOptions()
    {
        try {
            AnalyticsOptions::setOption($this->_company->id, AnalyticsOptions::KEY_PAL_SKIP_PREPAYMENTS, $this->skipPrepayments);
            AnalyticsOptions::setOption($this->_company->id, AnalyticsOptions::KEY_PAL_SKIP_TAXES, $this->skipTaxes);
            AnalyticsOptions::setOption($this->_company->id, AnalyticsOptions::KEY_PAL_SKIP_ZEROES_ROWS, $this->skipZeroesRows);
            AnalyticsOptions::setOption($this->_company->id, AnalyticsOptions::KEY_PAL_SKIP_CALCULATE_PRIME_COSTS, $this->skipCalculatePrimeCosts);
            AnalyticsOptions::setOption($this->_company->id, AnalyticsOptions::KEY_PAL_SHOW_AMORTIZATION, $this->showAmortization);
            AnalyticsOptions::setOption($this->_company->id, AnalyticsOptions::KEY_PAL_CALC_WITHOUT_NDS, $this->calcWithoutNds);
            AnalyticsOptions::setOption($this->_company->id, AnalyticsOptions::KEY_PAL_SHOW_EBIT, $this->showEbit);
            AnalyticsOptions::setOption($this->_company->id, AnalyticsOptions::KEY_PAL_REVENUE_RULE_GROUP, $this->revenueRuleGroup);
            AnalyticsOptions::setOption($this->_company->id, AnalyticsOptions::KEY_PAL_REVENUE_RULE, $this->revenueRule);
            AnalyticsOptions::setOption($this->_company->id, AnalyticsOptions::KEY_PAL_MONTH_GROUP_BY, $this->monthGroupBy);
        } catch (\Exception $e) {

            return false;
        }

        return true;
    }

    public function getItemsList($type)
    {
        if (!in_array($type, [CashFlowsBase::FLOW_TYPE_INCOME, CashFlowsBase::FLOW_TYPE_EXPENSE]))
            return [];

        $companyId = $this->_company->id;
        $table = AnalyticsArticle::tableName();
        $itemsTable = ($type == CashFlowsBase::FLOW_TYPE_INCOME) ?
            InvoiceIncomeItem::tableName() :
            InvoiceExpenditureItem::tableName();

        $items = Yii::$app->db->createCommand("
            SELECT a.item_id AS id, i.parent_id, i.name, a.profit_and_loss_type
            FROM {$table} AS a
            LEFT JOIN {$itemsTable} i ON a.item_id = i.id 
            WHERE a.company_id = {$companyId} AND a.type = {$type} AND i.parent_id IS NULL AND i.id IS NOT NULL
            ORDER BY i.parent_id, i.name
        ")->queryAll();

        return array_filter($items, function($item) use ($type) {

            if (in_array($item['id'], AnalyticsArticle::UNSET_ALWAYS_ARTICLES[$type]))
                if (!isset(AnalyticsArticle::UNSET_ALWAYS_ARTICLES_HELP[$type][$item['id']]))
                    return false;

                return true;
        });
    }
}