<?php

namespace frontend\modules\analytics\models;

use function Clue\StreamFilter\fun;
use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use common\models\cash\Cashbox;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\cash\Emoney;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use common\models\document\Invoice;
use common\models\project\Project;
use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\cash\models\CashContractorType;
use Yii;
use common\models\Company;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Connection;
use yii\db\Query;

/**
 * This is the model class for table "plan_cash_flows".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $contractor_id
 * @property string $date
 * @property string $first_date
 * @property integer $flow_type
 * @property integer $payment_type
 * @property string $amount
 * @property integer $income_item_id
 * @property integer $expenditure_item_id
 * @property string $description
 * @property integer $is_repeated
 * @property integer $first_flow_id
 * @property integer $checking_accountant_id
 * @property integer $cashbox_id
 * @property integer $emoney_id
 * @property integer $plan_cash_contractor_id
 * @property integer $invoice_id
 * @property integer $is_internal_transfer
 * @property string $rs
 * @property integer $credit_id
 *
 * @property integer $project_id
 * @property integer $sale_point_id
 * @property integer $industry_id
 *
 * @property integer $by_related_document
 *
 * @property Company $company
 * @property Contractor $contractor
 * @property CashContractorType $cashContractor
 * @property InvoiceExpenditureItem $expenditureItem
 * @property InvoiceIncomeItem $incomeItem
 *
 * @property CheckingAccountant $checkingAccountant
 * @property Cashbox $cashbox
 * @property Emoney $emoney
 *
 * @property Invoice $invoice
 * @property CompanyIndustry $industry
 * @property SalePoint $salePoint
 * @property Project $project
 *
 * @property bool $isAutoPlan
 */
class PlanCashFlows extends \yii\db\ActiveRecord
{
    const PAYMENT_TYPE_BANK = 1;
    const PAYMENT_TYPE_ORDER = 2;
    const PAYMENT_TYPE_EMONEY = 3;

    const PERIOD_MONTH = 1;
    const PERIOD_QUARTER = 2;
    const PERIOD_YEAR = 3;

    const UPDATE_REPEATED_ONLY_ONE = 1;
    const UPDATE_REPEATED_NEXT_OPERATIONS = 2;
    const UPDATE_REPEATED_ALL_OPERATIONS = 3;

    /**
     * @var array
     */
    public static $flowTypes = [
        CashFlowsBase::FLOW_TYPE_INCOME => 'Приход',
        CashFlowsBase::FLOW_TYPE_EXPENSE => 'Расход',
    ];

    /**
     * @var array
     */
    public static $paymentTypes = [
        self::PAYMENT_TYPE_BANK => 'Банк',
        self::PAYMENT_TYPE_ORDER => 'Касса',
        self::PAYMENT_TYPE_EMONEY => 'Emoney',
    ];

    /**
     * @var array
     */
    public static $periods = [
        self::PERIOD_MONTH => 'Каждый месяц',
        self::PERIOD_QUARTER => 'Каждый квартал',
        self::PERIOD_YEAR => 'Каждый год',
    ];

    /**
     * @var array
     */
    public static $monthByPeriod = [
        self::PERIOD_MONTH => 1,
        self::PERIOD_QUARTER => 3,
        self::PERIOD_YEAR => 12,
    ];

    /**
     * @var array
     */
    public static $updateRepeatedOperations = [
        self::UPDATE_REPEATED_ONLY_ONE => 'Только для данной операции',
        self::UPDATE_REPEATED_NEXT_OPERATIONS => 'Для данной операции и для последующих операций',
        self::UPDATE_REPEATED_ALL_OPERATIONS => 'Для всей цепочки повторяющихся операции',
    ];

    /**
     * @var
     */
    public $planEndDate;

    public $isAutoPlan;

    /**
     * @var
     */
    public $period;

    public $updateRepeatedType = self::UPDATE_REPEATED_ONLY_ONE;

    private static array $gettersReturn = [
        'null' => [
            'ofdReceipt'
        ],
        'array' => [
            //
        ],
        'zero' => [
            //
        ]
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plan_cash_flows';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => DatePickerFormatBehavior::className(),
                'attributes' => [
                    'date' => [
                        'message' => 'Дата указана неверно.',
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'contractor_id', 'date', 'amount', 'payment_type'], 'required', 'message' => 'Необходимо заполнить.'],
            [['planEndDate', 'period',], 'required', 'when' => function (PlanCashFlows $model) {
                return $model->is_repeated && $model->isNewRecord;
            }, 'message' => 'Необходимо заполнить',],
            [['income_item_id',], 'required', 'when' => function (PlanCashFlows $model) {
                return $model->flow_type == CashFlowsBase::FLOW_TYPE_INCOME;
            }, 'message' => 'Необходимо заполнить',],
            [['expenditure_item_id',], 'required', 'when' => function (PlanCashFlows $model) {
                return $model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE;
            }, 'message' => 'Необходимо заполнить',],
            ['planEndDate', function ($attribute, $params) {
                if ($this->is_repeated && strtotime($this->planEndDate) < strtotime($this->date)) {
                    $error = 'Дата последнего ';
                    if ($this->flow_type == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $error .= 'прихода';
                    } else {
                        $error .= 'расхода';
                    }
                    $error .= ' должна быть больше даты первого.';
                    $this->addError($attribute, $error);
                }
            }
            ],
            [['company_id', 'flow_type', 'payment_type', 'income_item_id', 'expenditure_item_id',
                'period', 'updateRepeatedType', 'checking_accountant_id', 'cashbox_id', 'emoney_id'], 'integer'],
            [['date', 'planEndDate', 'created_at', 'updated_at', 'contractor_id', 'is_internal_transfer'], 'safe'],
            [['amount'],
                'number',
                'numberPattern' => Yii::$app->params['numberPattern'],
                'max' => Yii::$app->params['maxCashSum'] * 100, // with kopeck
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
            [['amount'], 'compare', 'operator' => '>', 'compareValue' => 0,
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
            [['contractor_id'], 'validateContractor'],
            [['description'], 'string'],
            [['is_repeated'], 'boolean'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['expenditure_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceExpenditureItem::className(), 'targetAttribute' => ['expenditure_item_id' => 'id']],
            [['income_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceIncomeItem::className(), 'targetAttribute' => ['income_item_id' => 'id']],
            [['first_flow_id'], 'exist', 'skipOnError' => true, 'targetClass' => self::className(), 'targetAttribute' => ['first_flow_id' => 'id']],
            [['checking_accountant_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => CheckingAccountant::className(), 'targetAttribute' => ['checking_accountant_id' => 'id', 'company_id' => 'company_id']],
            [['cashbox_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => Cashbox::class, 'targetAttribute' => ['cashbox_id' => 'id', 'company_id' => 'company_id']],
            [['emoney_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => Emoney::class, 'targetAttribute' => ['emoney_id' => 'id', 'company_id' => 'company_id']],
            [['plan_cash_contractor_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => PlanCashContractor::class, 'targetAttribute' => ['plan_cash_contractor_id' => 'id', 'company_id' => 'company_id']],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => Invoice::className(), 'targetAttribute' => ['invoice_id' => 'id']],
            [['contractorInput'], 'safe'],
            [['payment_type'], 'in', 'range' => [self::PAYMENT_TYPE_BANK, self::PAYMENT_TYPE_ORDER, self::PAYMENT_TYPE_EMONEY]],
            [['rs'], 'safe'],
            [['credit_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => Credit::class, 'targetAttribute' => ['credit_id' => 'credit_id', 'company_id' => 'company_id']],
            [['project_id', 'sale_point_id', 'industry_id'], 'filter', 'filter' => function ($value) {
                return intval($value) ?: null;
            }],
            [['sale_point_id'], 'exist', 'targetClass' => SalePoint::class, 'targetAttribute' => 'id'],
            [['project_id'], 'exist', 'targetClass' => Project::class, 'targetAttribute' => 'id'],
            [['industry_id'], 'exist', 'targetClass' => CompanyIndustry::class, 'targetAttribute' => 'id'],
            [['by_related_document'], 'safe'],
            [['by_related_document'], 'default', 'value' => 0],
            [['by_related_document'], 'filter', 'filter' => function($value) {
                return intval($value);
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'contractor_id' => 'Покупатель',
            'date' => 'Дата планового '. (
                $this->flow_type == CashFlowsBase::FLOW_TYPE_INCOME ? 'прихода' : 'расхода'
            ),
            'flow_type' => 'Тип',
            'payment_type' => 'Тип оплаты',
            'amount' => 'Сумма планового '. (
                $this->flow_type == CashFlowsBase::FLOW_TYPE_INCOME ? 'прихода' : 'расхода'
            ),
            'income_item_id' => 'Статья прихода',
            'expenditure_item_id' => 'Статья расхода',
            'description' => 'Назначение',
            'is_repeated' => 'Повторяющийся платеж',
            'planEndDate' => 'Плановая дата последнего прихода',
            'period' => 'Периодичность',
            'checking_accountant_id' => 'Расчетный счет',
            'rs' => 'Расчетный счет',
            'cashbox_id' => 'Касса',
            'emoney_id' => 'E-money',
            'invoice_id' => 'Счет',
            'payment_order_number' => 'Номер ПП',
            'number' => 'Номер',
        ];
    }

    public function __get($name)
    {
        if (in_array($name, self::$gettersReturn['null'])) {
            return null;
        }
        if (in_array($name, self::$gettersReturn['zero'])) {
            return 0;
        }
        if (in_array($name, self::$gettersReturn['array'])) {
            return [];
        }

        return parent::__get($name);
    }


    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->amount = TextHelper::parseMoneyInput($this->amount);

        if ($this->isAttributeChanged('amount')) {
            $this->amount = round($this->amount * 100);
        }

        return parent::beforeValidate();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isAutoPlan
        && ($this->payment_type == self::PAYMENT_TYPE_BANK)
        && ($date = date_create_from_format('Y-m-d', $this->date))) {
            if ($date->format('w') == 0) { // sunday
                $this->date = $date->modify("+1 day")->format('Y-m-d');
            }
            if ($date->format('w') == 6) { // saturday
                $this->date = $date->modify("+2 day")->format('Y-m-d');
            }
        }

        if ($this->isNewRecord) {
            $this->first_date = $this->date;
        }

        if (in_array($this->contractor_id, ['bank', 'order', 'emoney']))
            $this->is_internal_transfer = 1;

        if ($this->payment_type == self::PAYMENT_TYPE_BANK) {
            $this->cashbox_id = null;
            $this->emoney_id = null;
            $this->rs = ($this->checkingAccountant)
                ? $this->checkingAccountant->rs
                : null;
        } elseif ($this->payment_type == self::PAYMENT_TYPE_ORDER) {
            $this->checking_accountant_id = null;
            $this->emoney_id = null;
        } elseif ($this->payment_type == self::PAYMENT_TYPE_EMONEY) {
            $this->checking_accountant_id = null;
            $this->cashbox_id = null;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateContractor($attribute, $params)
    {
        $contractor = Contractor::findOne($this->contractor_id);
        if ($contractor === null) {
            if (!in_array($this->$attribute, ArrayHelper::getColumn(CashContractorType::find()->all(), 'name'))) {
                $this->addError($attribute, 'Контрагент не найден.');
            }
        } else {
            if (!$this->isNewRecord && $this->isAttributeChanged($attribute) && $contractor->type == Contractor::TYPE_FOUNDER) {
                $this->addError($attribute, 'Редактирование котрагента-учредителя запрещено.');
            }
        }
    }

    /**
     * @return int
     */
    public function getWalletType()
    {
        return $this->payment_type;
    }

    /**
     * @return bool
     */
    public function getIsForeign()
    {
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckingAccountant()
    {
        return $this->hasOne(CheckingAccountant::className(), ['id' => 'checking_accountant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashbox()
    {
        return $this->hasOne(Cashbox::className(), ['id' => 'cashbox_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmoney()
    {
        return $this->hasOne(Emoney::className(), ['id' => 'emoney_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashContractor()
    {
        return $this->hasOne(CashContractorType::className(), ['name' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenditureItem()
    {
        return $this->hasOne(InvoiceExpenditureItem::className(), ['id' => 'expenditure_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomeItem()
    {
        return $this->hasOne(InvoiceIncomeItem::className(), ['id' => 'income_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    public function getIndustry()
    {
        return $this->hasOne(CompanyIndustry::class, ['id' => 'industry_id']);
    }

    public function getSalePoint()
    {
        return $this->hasOne(SalePoint::class, ['id' => 'sale_point_id']);
    }

    public function getProject()
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    /**
     * @return mixed
     * @throws \Throwable
     */
    public function _save()
    {
        $model = $this;

        if (!$model->is_repeated) {
            return Yii::$app->db->transaction(function (Connection $db) use ($model) {
                if ($model->checkContractor() && $model->save()) {
                    return true;
                }
                $db->transaction->rollBack();
                return false;
            });
        }

        return Yii::$app->db->transaction(function (Connection $db) use ($model) {
            $startDate = strtotime($model->date);
            $endDate = strtotime($model->planEndDate);
            $day = date('d', $startDate);
            $i = 0;
            $firstFlowId = null;
            do {
                $startMonthDate = date('01.m.Y', $startDate);
                $planItem = clone $model;
                $planItem->id = null;
                $planItem->isNewRecord = true;
                if ($i == 0) {
                    $planItem->is_repeated = true;
                } else {
                    $planItem->is_repeated = false;
                    $planItem->first_flow_id = $firstFlowId;
                }
                $planItem->date = date(DateHelper::FORMAT_USER_DATE, $startDate);
                if (!$planItem->checkContractor() || !$planItem->save()) {
                    $db->transaction->rollBack();
                    return false;
                }
                if ($i == 0) {
                    $firstFlowId = $planItem->id;
                }
                $startDate = $this->getNextStartDate($startMonthDate, $day);
                $i++;
            } while ($startDate <= $endDate);

            return true;
        });
    }

    /**
     * @return bool|mixed
     * @throws \Throwable
     */
    public function _update() {
        if (!$this->is_repeated && !$this->first_flow_id) {
            return $this->save();
        }

        $model = $this;

        return Yii::$app->db->transaction(function (Connection $db) use ($model) {
            if (!$model->save()) {
                $db->transaction->rollBack();
                return false;
            }
            $firstFlowId = $this->is_repeated && !$this->first_flow_id ? $model->id : $this->first_flow_id;
            $planCashFlows = [];
            if ($this->updateRepeatedType == self::UPDATE_REPEATED_NEXT_OPERATIONS) {
                $planCashFlows = self::find()
                    ->andWhere(['first_flow_id' => $firstFlowId])
                    ->andWhere(['>', 'date', $model->date])
                    ->all();
            } elseif ($this->updateRepeatedType == self::UPDATE_REPEATED_ALL_OPERATIONS) {
                $planCashFlows = self::find()
                    ->andWhere(['first_flow_id' => $firstFlowId])
                    ->all();
            }
            /** @var self $planCashFlow */
            foreach ($planCashFlows as $planCashFlow) {
                $planCashFlow->payment_type = $model->payment_type;
                $planCashFlow->amount = $model->amount / 100;
                $planCashFlow->contractor_id = $model->contractor_id;
                $planCashFlow->income_item_id = $model->income_item_id;
                $planCashFlow->expenditure_item_id = $model->expenditure_item_id;
                $planCashFlow->description = $model->description;
                if (!$planCashFlow->save()) {
                    $db->transaction->rollBack();
                    return false;
                }
            }

            return true;
        });
    }

    /**
     * @param $startMonthDate
     * @param $day
     * @return false|int
     */
    public function getNextStartDate($startMonthDate, $day)
    {
        $startDate = strtotime('+' . self::$monthByPeriod[$this->period] . ' month', strtotime($startMonthDate));
        $maxDaysInMonth = cal_days_in_month(CAL_GREGORIAN, date('m', $startDate), date('Y', $startDate));
        $planDay = $day > $maxDaysInMonth ? $maxDaysInMonth : $day;
        $startDate = strtotime(date("{$planDay}.m.Y", $startDate));

        return $startDate;
    }

    /**
     * @param $type
     * @return int
     * @throws Exception
     */
    public function getPaymentTypeByCashContractor($type)
    {
        switch ($type) {
            case CashContractorType::BANK_TEXT:
                $paymentType = self::PAYMENT_TYPE_BANK;
                break;
            case CashContractorType::ORDER_TEXT:
                $paymentType = self::PAYMENT_TYPE_ORDER;
                break;
            case CashContractorType::EMONEY_TEXT:
                $paymentType = self::PAYMENT_TYPE_EMONEY;
                break;
            default:
                throw new Exception('Cash with type = "' . $type . '" not found.');
        }

        return $paymentType;
    }

    /**
     * @param $type
     * @return string|null
     * @throws Exception
     */
    public function getCashContractorByPaymentType($type)
    {
        $cashContractor = null;

        switch ($type) {
            case self::PAYMENT_TYPE_BANK:
                $cashContractor = CashContractorType::BANK_TEXT;
                break;
            case self::PAYMENT_TYPE_ORDER:
                $cashContractor = CashContractorType::ORDER_TEXT;
                break;
            case self::PAYMENT_TYPE_EMONEY:
                $cashContractor = CashContractorType::EMONEY_TEXT;
                break;
            default:
                throw new Exception('Cash with payment type = "' . $type . '" not found.');
        }

        return $cashContractor;
    }

    /**
     * @param bool $copy
     * @return bool
     * @throws Exception
     */
    public function checkContractor()
    {
        if (in_array($this->contractor_id, CashContractorType::$inverseTypes)) {

            // bank to bank
            if ($this->payment_type == $this->getPaymentTypeByCashContractor($this->contractor_id))
                return true;

            $cashContractor = CashContractorType::findOne(['name' => $this->contractor_id]);
            if ($cashContractor !== null) {
                $newCash = $this->_createInverseCash();
                if ($newCash->save(false)) {

                    return true;
                } else {

                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param $type
     * @param bool|false $copy
     * @return $this
     * @throws Exception
     */
    private function _createInverseCash()
    {
        $model = new PlanCashFlows();
        $model->setAttributes($this->getAttributes());
        $model->payment_type = $this->getPaymentTypeByCashContractor($this->contractor_id);
        $model->contractor_id = $this->getCashContractorByPaymentType($this->payment_type);
        $model->amount = bcmul($this->amount, 100);
        $model->date = date('Y-m-d', strtotime($this->date));
        if ($this->flow_type == CashFlowsBase::FLOW_TYPE_INCOME) {
            $model->income_item_id = null;
            $model->flow_type = CashFlowsBase::FLOW_TYPE_EXPENSE;
            $model->expenditure_item_id = InvoiceExpenditureItem::ITEM_OWN_FOUNDS;
        } else {
            $model->expenditure_item_id = null;
            $model->flow_type = CashFlowsBase::FLOW_TYPE_INCOME;
            $model->income_item_id = InvoiceIncomeItem::ITEM_OWN_FOUNDS;
        }

        return $model;
    }

    public function getInternalTransferAccount()
    {
        return null;
    }

    /**
     * @return PlanCashFlows
     */
    public function cloneSelf()
    {
        $model = clone $this;
        $model->isNewRecord = true;
        unset($model->id);
        $model->is_repeated = false;

        return $model;
    }    

    /** Adapt with CashFlowsBase */

    // bank
    public function getRs(){
        return ($this->checkingAccountant) ? $this->checkingAccountant->rs : null;
    }
    public function setRecognitionDateInput($value){
        $this->date = is_string($value) ? implode('-', array_reverse(explode('.', $value))) : null;
    }
    public function getRecognitionDateInput(){
        return $this->date ? implode('.', array_reverse(explode('-', $this->date))) : null;
    }
    public function getPayment_order_number() { return null; }
    public function getIs_prepaid_expense() { return null; }
    public function getTaxpayers_status_id(){ return null; }
    public function getKbk() { return null; }
    public function getOktmo_code() { return null; }
    public function getPayment_details_id() { return null; }
    public function getTax_period_code() { return null; }
    public function getDocument_number_budget_payment() { return null; }
    public function getDateBudgetPayment() { return null; }
    public function getPayment_type_id() { return null; }
    public function getUin_code() { return null; }
    public function getInvoices_list() { return []; }
    public function getInvoices() { return []; }
    public function getCredit_id() { return null; }
    public function getCredit_amount() { return 0; }
    public function getCreditList() { return []; }

    // emoney
    public function getDate_time() { return null; }
    public function getNumber() { return null; }
    public function getIs_accounting() { return null; }

    // cashbox
    public function getContractorInput() { return $this->contractor_id; }
    public function setContractorInput($value) { $this->contractor_id = (int)$value; }
    public function getReason_id()  { return null; }
    public function getApplication()  { return null; }
    public function getOther() { return null; }

    public function canChangeProject()
    {
        return true;
    }

    public function getCreditListWithOptions() {
        return [
            'items' => [0 => 'Без договора'],
            'options' => []
        ];
    }

    // todo
    public static function fact2Plan($flowModel)
    {
        $planModel = new PlanCashFlows();
        if ($flowModel instanceof CashOrderFlows) {
            $planModel->payment_type = self::PAYMENT_TYPE_ORDER;
            $planModel->company_id = $flowModel->company_id;
            $planModel->flow_type = $flowModel->flow_type;
            $planModel->cashbox_id = $flowModel->cashbox_id;
            $planModel->contractor_id = $flowModel->contractor_id;
            $planModel->date = $flowModel->date;
            $planModel->amount = $flowModel->amount;
            $planModel->income_item_id = $flowModel->income_item_id;
            $planModel->expenditure_item_id = $flowModel->expenditure_item_id;
            $planModel->description = $flowModel->description;
            $planModel->is_internal_transfer = (int)$flowModel->is_internal_transfer;
            $planModel->is_repeated = 0;

            return $planModel;
        }

        return false;
    }

    public function getFlowInvoices()
    {
        return new Query();
    }
}
