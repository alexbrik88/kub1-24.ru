<?php

namespace frontend\modules\analytics\models;

use common\components\date\DateHelper;
use common\models\Company;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductTurnover;
use frontend\models\Documents;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class WhatIfSearch
 */
class WhatIfSearch extends Model
{
    public $product_type;
    public $sales_type;
    public $sales_point;

    public $showGroups = true;
    public $quantity = 0;
    public $price = 0;
    public $variable_expenses = 0;
    public $fixed_expenses = 0;
    public $filter;

    protected $_company;
    protected $_from;
    protected $_till;
    protected $_data_1 = [];
    protected $_data_2 = [];
    protected $_data = [];
    protected $_overallMargin = 0;
    protected $_fixedExpenses = 0;
    protected $_totalCount = 0;
    protected $_totalProfit = 0;
    protected $_total = [
        'proceeds_1' => 0,
        'proceeds_2' => 0,
        'change_proceeds_abs' => 0,
        'change_proceeds_rel' => 0,
        'margin_1' => 0,
        'margin_2' => 0,
        'change_margin_abs' => 0,
        'change_margin_rel' => 0,
        'profit_1' => 0,
        'overal_profit_percent_1' => 100,
        'profit_2' => 0,
        'change_profit_abs' => 0,
        'change_profit_rel' => 0,
        'deals_1' => 0,
        'deals_2' => 0,
        'rentability_1' => 0,
        'rentability_2' => 0,
        'fixed_expenses_1' => 0,
        'fixed_expenses_2' => 0,
        'variable_expenses_1' => 0,
        'variable_expenses_2' => 0,
        'average_check_1' => 0,
        'average_check_2' => 0,
        'rentability_costs_1' => 0,
        'rentability_costs_2' => 0,
        'margin_percent_1' => 0,
        'margin_percent_2' => 0,
    ];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['filter', 'string'],
            ['filter', 'trim'],
            [
                [
                    'quantity',
                    'price',
                    'variable_expenses',
                    'fixed_expenses',
                ],
                'number',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        parent::afterValidate();

        $attributes = [
            'quantity',
            'price',
            'variable_expenses',
            'fixed_expenses',
        ];
        foreach ($attributes as $attribute) {
            if ($this->hasErrors($attribute)) {
                $this->$attribute = 0;
            }
        }
    }

    public function __construct(Company $company, array $config = [])
    {
        $this->_company = $company;

        parent::__construct($config);
    }

    public function init()
    {
        parent::init();

        $this->_from = new \DateTimeImmutable('first day of -12 month');
        $this->_till = new \DateTimeImmutable('last day of -1 month');
    }

    public function getDb()
    {
        return Yii::$app->db2;
    }

    public function attributeLabels()
    {
        return [
            'quantity' => 'Спрос',
            'price' => 'Цена',
            'variable_expenses' => 'Переменные расходы',
            'fixed_expenses' => 'Постоянные расходы',
        ];
    }

    public function getHasChangies()
    {
        return $this->quantity || $this->price || $this->variable_expenses || $this->fixed_expenses;
    }

    protected function collectData()
    {
        $this->createDataTable();
        $this->collectData1();
        $this->collectData2();
        $this->collectTotalData();
    }

    public function search($params = [])
    {
        $this->load($params);

        $this->collectData();

        $items = $this->productItems();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $this->productItems(),
            'sort' => [
                'attributes' => [
                    'title',
                    'proceeds_1',
                    'proceeds_2',
                    'change_proceeds_abs',
                    'change_proceeds_rel',
                    'margin_1',
                    'margin_2',
                    'change_margin_abs',
                    'change_margin_rel',
                    'profit_1',
                    'overal_profit_percent_1',
                    'profit_2',
                    'change_profit_abs',
                    'change_profit_rel',
                    'rentability_1',
                    'rentability_2',
                ],
                'defaultOrder' => [
                    'proceeds_1' => SORT_DESC,
                ],
            ],
            'pagination' => [
                'pageSize' => 0,
            ],
        ]);

        if ($this->showGroups) {
            $items = $dataProvider->getModels();

            $dataProvider = new ArrayDataProvider([
                'allModels' => $this->groupItems($items),
                'sort' => [
                    'attributes' => [
                        'title',
                        'proceeds_1',
                        'proceeds_2',
                        'change_proceeds_abs',
                        'change_proceeds_rel',
                        'margin_1',
                        'margin_2',
                        'change_margin_abs',
                        'change_margin_rel',
                        'profit_1',
                        'overal_profit_percent_1',
                        'profit_2',
                        'change_profit_abs',
                        'change_profit_rel',
                        'rentability_1',
                        'rentability_2',
                    ],
                    'defaultOrder' => [
                        'proceeds_1' => SORT_DESC,
                    ],
                ],
                'pagination' => [
                    'pageSize' => 0,
                ],
            ]);
        }

        return $dataProvider;
    }

    private function createDataTable()
    {
        $db = $this->db;

        $query = (new \yii\db\Query())->select([
            'year',
            'month',
            'product_id',
            'quantity' => 'SUM([[quantity]])',
            'total_amount' => 'SUM([[total_amount]])',
            'margin' => 'SUM([[margin]])',
            'deals' => 'COUNT(DISTINCT([[invoice_id]]))',
        ])->from([
            't' => ProductTurnover::tableName(),
        ])->andWhere([
            'company_id' => $this->_company->id,
            'type' => Documents::IO_TYPE_OUT,
            'production_type' => Product::PRODUCTION_TYPE_GOODS,
            'is_invoice_actual' => 1,
            'is_document_actual' => 1,
        ])->andWhere([
            'between',
            'date',
            $this->_from->format('Y-m-d'),
            $this->_till->format('Y-m-d'),
        ])->groupBy([
            'year',
            'month',
            'product_id',
        ]);
        $sql = $query->createCommand()->rawSql;
        $comand = $db->createCommand("DROP TEMPORARY TABLE IF EXISTS __what_if");
        $comand->execute();
        $comand = $db->createCommand("CREATE TEMPORARY TABLE __what_if {$sql}");
        $comand->execute();
    }

    private function collectData1()
    {
        $prodData = [];
        $groupData = [];
        $totalData = [
            'month_1' => 0,
            'month_2' => 0,
            'month_3' => 0,
            'month_4' => 0,
            'month_5' => 0,
            'month_6' => 0,
            'month_7' => 0,
            'month_8' => 0,
            'month_9' => 0,
            'month_10' => 0,
            'month_11' => 0,
            'month_12' => 0,
            'proceeds' => 0,
            'quantity' => 0,
            'margin' => 0,
            'deals' => 0,
            'fixed_expenses' => 0,
            'variable_expenses' => 0,
            'margin_percent' => 0,
            'overal_margin_percent' => 0,
            'profit' => 0,
            'rentability' => 0,
            'average_check' => 0,
        ];
        $query = (new Query)->from([
            't' => '__what_if',
        ])->leftJoin([
            'p' => Product::tableName(),
        ], '{{p}}.[[id]] = {{t}}.[[product_id]]')->leftJoin([
            'g' => ProductGroup::tableName(),
        ], '{{g}}.[[id]] = {{p}}.[[group_id]]')->select([
            't.month',
            'p.group_id',
            't.product_id',
            'p.article',
            'product_name' => 'p.title',
            'group_name' => 'g.title',
            'proceeds' => 'SUM({{t}}.[[total_amount]])',
            'quantity' => 'SUM({{t}}.[[quantity]])',
            'margin' => 'SUM({{t}}.[[margin]])',
            'deals' => 'SUM({{t}}.[[deals]])',
        ])->groupBy([
            'month',
            'product_id',
        ])->groupBy('[[month]], [[product_id]]');

        $queryResult = $query->all($this->db);

        if ($queryResult) {
            foreach ($queryResult as $row) {
                $gid = $row['group_id'];
                $pid = $row['product_id'];
                if (!isset($prodData[$pid])) {
                    $prodData[$pid] = [
                        'group_id' => $gid,
                        'product_id' => $pid,
                        'group' => $row['group_name'],
                        'title' => $row['product_name'],
                        'article' => $row['article'],
                        'month_1' => 0,
                        'month_2' => 0,
                        'month_3' => 0,
                        'month_4' => 0,
                        'month_5' => 0,
                        'month_6' => 0,
                        'month_7' => 0,
                        'month_8' => 0,
                        'month_9' => 0,
                        'month_10' => 0,
                        'month_11' => 0,
                        'month_12' => 0,
                        'proceeds' => 0,
                        'quantity' => 0,
                        'margin' => 0,
                        'deals' => 0,
                    ];
                }
                if (!isset($groupData[$gid])) {
                    $groupData[$gid] = [
                        'group_id' => $gid,
                        'product_id' => null,
                        'title' => $row['group_name'],
                        'article' => null,
                        'month_1' => 0,
                        'month_2' => 0,
                        'month_3' => 0,
                        'month_4' => 0,
                        'month_5' => 0,
                        'month_6' => 0,
                        'month_7' => 0,
                        'month_8' => 0,
                        'month_9' => 0,
                        'month_10' => 0,
                        'month_11' => 0,
                        'month_12' => 0,
                        'proceeds' => 0,
                        'quantity' => 0,
                        'margin' => 0,
                        'deals' => 0,
                        'pids' => [],
                    ];
                }
                $prodData[$pid]['month_'.$row['month']] += $row['proceeds'];
                $prodData[$pid]['proceeds'] += $row['proceeds'];
                $prodData[$pid]['quantity'] += $row['quantity'];
                $prodData[$pid]['margin'] += $row['margin'];
                $prodData[$pid]['deals'] += $row['deals'];
                // group data collect
                $groupData[$gid]['month_'.$row['month']] += $row['proceeds'];
                $groupData[$gid]['proceeds'] += $row['proceeds'];
                $groupData[$gid]['margin'] += $row['margin'];
                $groupData[$gid]['deals'] += $row['deals'];
                $groupData[$gid]['pids'][] = $pid;
                // total data collect
                $totalData['month_'.$row['month']] += $row['proceeds'];
                $totalData['proceeds'] += $row['proceeds'];
                $totalData['margin'] += $row['margin'];
                $totalData['deals'] += $row['deals'];
            }

            $this->_totalCount = count($prodData);
            $this->_overallMargin = $overallMargin = $totalData['margin'] ?: 9E9;
            $this->_fixedExpenses = $fixedExpenses = $this->getDocsFixedExpenses() + $this->getFlowsFixedExpenses();
            $this->_totalProfit = $totalProfit = $this->_overallMargin - $fixedExpenses;

            foreach ($prodData as $key => &$item) {
                $item['variable_expenses'] = $item['proceeds'] - $item['margin'];
                $item['margin_percent'] = $item['proceeds'] ? 100 * $item['margin'] / $item['proceeds'] : 0;
                $item['overal_margin_percent'] = $overallMargin ? 100 * $item['margin'] / $overallMargin : 0;
                $item['fixed_expenses'] = $overallMargin ? $fixedExpenses * $item['margin'] / $overallMargin : 0;
                $item['profit'] = $item['margin'] - $item['fixed_expenses'];
                $item['rentability'] = $item['proceeds'] ? 100 * $item['profit'] / $item['proceeds'] : 0;
                $item['price'] = $item['quantity'] ? $item['proceeds'] / $item['quantity'] : 0;
                $item['average_check'] = $item['deals'] ? $item['proceeds'] / $item['deals'] : 0;
            }

            foreach ($groupData as $key => &$item) {
                $item['variable_expenses'] = $item['proceeds'] - $item['margin'];
                $item['margin_percent'] = $item['proceeds'] ? 100 * $item['margin'] / $item['proceeds'] : 0;
                $item['overal_margin_percent'] = $overallMargin ? 100 * $item['margin'] / $overallMargin : 0;
                $item['fixed_expenses'] = $overallMargin ? $fixedExpenses * $item['margin'] / $overallMargin : 0;
                $item['profit'] = $item['margin'] - $item['fixed_expenses'];
                $item['rentability'] = $item['proceeds'] ? 100 * $item['profit'] / $item['proceeds'] : 0;
                $item['average_check'] = $item['deals'] ? $item['proceeds'] / $item['deals'] : 0;
            }

            $totalData['variable_expenses'] = $totalData['proceeds'] - $totalData['margin'];
            $totalData['margin_percent'] = $totalData['proceeds'] ? 100 * $totalData['margin'] / $totalData['proceeds'] : 0;
            $totalData['overal_margin_percent'] = 100;
            $totalData['fixed_expenses'] = $fixedExpenses;
            $totalData['profit'] = $totalData['margin'] - $totalData['fixed_expenses'];
            $totalData['rentability'] = $totalData['proceeds'] ? 100 * $totalData['profit'] / $totalData['proceeds'] : 0;
            $totalData['average_check'] = $totalData['deals'] ? $totalData['proceeds'] / $totalData['deals'] : 0;
        }

        $this->_data_1 = [
            'prodData' => $prodData,
            'groupData' => $groupData,
            'totalData' => $totalData,
        ];
    }

    private function collectData2()
    {
        $prodData = [];
        $groupData = [];
        $totalData = [
            'month_1' => 0,
            'month_2' => 0,
            'month_3' => 0,
            'month_4' => 0,
            'month_5' => 0,
            'month_6' => 0,
            'month_7' => 0,
            'month_8' => 0,
            'month_9' => 0,
            'month_10' => 0,
            'month_11' => 0,
            'month_12' => 0,
            'proceeds' => 0,
            'quantity' => 0,
            'margin' => 0,
            'deals' => 0,
            'fixed_expenses' => 0,
            'variable_expenses' => 0,
            'margin_percent' => 0,
            'overal_margin_percent' => 0,
            'profit' => 0,
            'rentability' => 0,
            'average_check' => 0,
        ];
        $quantutyRatio = $this->quantity / 100 + 1;
        $priceRatio = $this->price / 100 + 1;
        $fixedExpensesRatio = $this->fixed_expenses / 100 + 1;
        $variableExpensesRatio = $this->variable_expenses / 100 + 1;

        if ($this->_data_1['prodData']) {
            foreach ($this->_data_1['prodData'] as $row) {
                $gid = $row['group_id'];
                $pid = $row['product_id'];
                $quantity = $row['quantity'] * $quantutyRatio;
                $price = $row['price'] * $priceRatio;
                $fixed_expenses = $row['fixed_expenses'] * $fixedExpensesRatio;
                $proceeds = $quantity * $price;
                $k = $proceeds / $row['proceeds'];
                $variable_expenses = ($row['variable_expenses'] / $row['quantity'] * $variableExpensesRatio) * $quantity;
                $margin = $proceeds - $variable_expenses;
                $deals = $quantity * $row['deals'] / $row['quantity'];
                $prodData[$pid] = [
                    'group_id' => $gid,
                    'product_id' => $pid,
                    'month_1' => $row['month_1'] * $k,
                    'month_2' => $row['month_2'] * $k,
                    'month_3' => $row['month_3'] * $k,
                    'month_4' => $row['month_4'] * $k,
                    'month_5' => $row['month_5'] * $k,
                    'month_6' => $row['month_6'] * $k,
                    'month_7' => $row['month_7'] * $k,
                    'month_8' => $row['month_8'] * $k,
                    'month_9' => $row['month_9'] * $k,
                    'month_10' => $row['month_10'] * $k,
                    'month_11' => $row['month_11'] * $k,
                    'month_12' => $row['month_12'] * $k,
                    'proceeds' => $proceeds,
                    'quantity' => $quantity,
                    'margin' => $margin,
                    'deals' => $deals,
                    'variable_expenses' => $variable_expenses,
                    'fixed_expenses' => $fixed_expenses,
                    'margin_percent' => 100 * $margin / $proceeds,
                    'price' => $proceeds / $quantity,
                    'average_check' => $deals ? $proceeds / $deals : 0,
                ];
                if (!isset($groupData[$gid])) {
                    $groupData[$gid] = [
                        'group_id' => $gid,
                        'product_id' => null,
                        'month_1' => 0,
                        'month_2' => 0,
                        'month_3' => 0,
                        'month_4' => 0,
                        'month_5' => 0,
                        'month_6' => 0,
                        'month_7' => 0,
                        'month_8' => 0,
                        'month_9' => 0,
                        'month_10' => 0,
                        'month_11' => 0,
                        'month_12' => 0,
                        'proceeds' => 0,
                        'quantity' => 0,
                        'margin' => 0,
                        'deals' => 0,
                        'fixed_expenses' => 0,
                        'pids' => [],
                    ];
                }
                // group data collect
                $groupData[$gid]['month_1'] += $prodData[$pid]['month_1'];
                $groupData[$gid]['month_2'] += $prodData[$pid]['month_2'];
                $groupData[$gid]['month_3'] += $prodData[$pid]['month_3'];
                $groupData[$gid]['month_4'] += $prodData[$pid]['month_4'];
                $groupData[$gid]['month_5'] += $prodData[$pid]['month_5'];
                $groupData[$gid]['month_6'] += $prodData[$pid]['month_6'];
                $groupData[$gid]['month_7'] += $prodData[$pid]['month_7'];
                $groupData[$gid]['month_8'] += $prodData[$pid]['month_8'];
                $groupData[$gid]['month_9'] += $prodData[$pid]['month_9'];
                $groupData[$gid]['month_10'] += $prodData[$pid]['month_10'];
                $groupData[$gid]['month_11'] += $prodData[$pid]['month_11'];
                $groupData[$gid]['month_12'] += $prodData[$pid]['month_12'];
                $groupData[$gid]['proceeds'] += $proceeds;
                $groupData[$gid]['margin'] += $margin;
                $groupData[$gid]['deals'] += $deals;
                $groupData[$gid]['fixed_expenses'] += $fixed_expenses;
                $groupData[$gid]['pids'][] = $pid;
                // total data collect
                $totalData['month_1'] += $prodData[$pid]['month_1'];
                $totalData['month_2'] += $prodData[$pid]['month_2'];
                $totalData['month_3'] += $prodData[$pid]['month_3'];
                $totalData['month_4'] += $prodData[$pid]['month_4'];
                $totalData['month_5'] += $prodData[$pid]['month_5'];
                $totalData['month_6'] += $prodData[$pid]['month_6'];
                $totalData['month_7'] += $prodData[$pid]['month_7'];
                $totalData['month_8'] += $prodData[$pid]['month_8'];
                $totalData['month_9'] += $prodData[$pid]['month_9'];
                $totalData['month_10'] += $prodData[$pid]['month_10'];
                $totalData['month_11'] += $prodData[$pid]['month_11'];
                $totalData['month_12'] += $prodData[$pid]['month_12'];
                $totalData['proceeds'] += $proceeds;
                $totalData['margin'] += $margin;
                $totalData['deals'] += $deals;
                $totalData['fixed_expenses'] += $fixed_expenses;
            }

            foreach ($prodData as $key => &$item) {
                $item['margin_percent'] = $item['proceeds'] ? 100 * $item['margin'] / $item['proceeds'] : 0;
                $item['overal_margin_percent'] = $totalData['margin'] ? 100 * $item['margin'] / $totalData['margin'] : 0;
                $item['profit'] = $item['margin'] - $item['fixed_expenses'];
                $item['rentability'] = $item['proceeds'] ? 100 * $item['profit'] / $item['proceeds'] : 0;
                $item['price'] = $item['quantity'] ? $item['proceeds'] / $item['quantity'] : 0;
                $item['average_check'] = $item['deals'] ? $item['proceeds'] / $item['deals'] : 0;
            }

            foreach ($groupData as $key => &$item) {
                $item['variable_expenses'] = $item['proceeds'] - $item['margin'];
                $item['margin_percent'] = $item['proceeds'] ? 100 * $item['margin'] / $item['proceeds'] : 0;
                $item['overal_margin_percent'] = $totalData['margin'] ? 100 * $item['margin'] / $totalData['margin'] : 0;
                $item['profit'] = $item['margin'] - $item['fixed_expenses'];
                $item['rentability'] = $item['proceeds'] ? 100 * $item['profit'] / $item['proceeds'] : 0;
                $item['average_check'] = $item['deals'] ? $item['proceeds'] / $item['deals'] : 0;
            }

            $totalData['variable_expenses'] = $totalData['proceeds'] - $totalData['margin'];
            $totalData['margin_percent'] = $totalData['proceeds'] ? 100 * $totalData['margin'] / $totalData['proceeds'] : 0;
            $totalData['overal_margin_percent'] = 100;
            $totalData['profit'] = $totalData['margin'] - $totalData['fixed_expenses'];
            $totalData['rentability'] = $totalData['proceeds'] ? 100 * $totalData['profit'] / $totalData['proceeds'] : 0;
            $totalData['average_check'] = $totalData['deals'] ? $totalData['proceeds'] / $totalData['deals'] : 0;
        }

        $this->_data_2 = [
            'prodData' => $prodData,
            'groupData' => $groupData,
            'totalData' => $totalData,
        ];
    }

    private function collectTotalData()
    {
        $data1 = $this->_data_1['totalData'];
        $data2 = $this->_data_2['totalData'];
        $this->_total = [
            'proceeds_1' => $data1['proceeds'],
            'proceeds_2' => $data2['proceeds'],
            'change_proceeds_abs' => $p_abs = $data2['proceeds'] - $data1['proceeds'],
            'change_proceeds_rel' => $data1['proceeds'] ? 100 * $p_abs / $data1['proceeds'] : 0,
            'margin_1' => $data1['margin'],
            'margin_2' => $data2['margin'],
            'change_margin_abs' => $m_abs = $data2['margin'] - $data1['margin'],
            'change_margin_rel' => $data1['margin'] ? 100 * $m_abs / $data1['margin'] : 0,
            'profit_1' => $data1['profit'],
            'overal_profit_percent_1' => 100,
            'profit_2' => $data2['profit'],
            'change_profit_abs' => $pf_abs = $data2['profit'] - $data1['profit'],
            'change_profit_rel' => $data1['profit'] ? 100 * $pf_abs / $data1['profit'] : 0,
            'rentability_1' => $data1['rentability'],
            'rentability_2' => $data2['rentability'],
            'rentability_costs_1' => ($exp = $data1['variable_expenses'] + $data1['fixed_expenses']) != 0 ? $data1['profit'] / $exp : 0,
            'rentability_costs_2' => ($exp = $data2['variable_expenses'] + $data2['fixed_expenses']) != 0 ? $data2['profit'] / $exp : 0,
            'margin_percent_1' => $data1['margin_percent'],
            'margin_percent_2' => $data2['margin_percent'],
            'average_check_1' => $data1['average_check'],
            'average_check_2' => $data2['average_check'],
        ];
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    public function getOverallMargin()
    {
        return $this->_overallMargin;
    }

    public function getFixedExpenses()
    {
        return $this->_fixedExpenses;
    }

    public function getTotalCount()
    {
        return $this->_totalCount;
    }

    public function getTotalProfit()
    {
        return $this->_totalProfit;
    }

    public function getTotal()
    {
        return $this->_total;
    }

    public function productItems()
    {
        if (!array_key_exists('productItems', $this->_data)) {
            $items = [];
            $s = $this->filter ?: null;
            $profit = $this->_data_1['totalData']['profit'];

            foreach ($this->_data_1['prodData'] as $pid => $data1) {
                if (isset($s) && stripos($data1['title'], $s) === false && stripos($data1['article'], $s) === false) {
                    continue;
                }
                $data2 = $this->_data_2['prodData'][$pid];

                $items[$data1['product_id']] = [
                    'group_id' => $data1['group_id'],
                    'product_id' => $data1['product_id'],
                    'group' => $data1['group'],
                    'title' => $data1['title'],
                    'proceeds_1' => $data1['proceeds'],
                    'proceeds_2' => $data2['proceeds'],
                    'change_proceeds_abs' => $p_abs = $data2['proceeds'] - $data1['proceeds'],
                    'change_proceeds_rel' => ($data1['proceeds']) ? 100 * $p_abs / $data1['proceeds'] : 0,
                    'margin_1' => $data1['margin'],
                    'margin_2' => $data2['margin'],
                    'change_margin_abs' => $m_abs = $data2['margin'] - $data1['margin'],
                    'change_margin_rel' => ($data1['margin']) ? 100 * $m_abs / $data1['margin'] : 0,
                    'profit_1' => $data1['profit'],
                    'overal_profit_percent_1' => ($profit) ? 100 * $data1['profit'] / $profit : 0,
                    'profit_2' => $data2['profit'],
                    'change_profit_abs' => $pf_abs = $data2['profit'] - $data1['profit'],
                    'change_profit_rel' => ($data1['profit']) ? 100 * $pf_abs / $data1['profit'] : 0,
                    'rentability_1' => $data1['rentability'],
                    'rentability_2' => $data2['rentability'],
                ];
            }

            $this->_data['productItems'] = $items;
        }

        return $this->_data['productItems'];

    }

    public function groupItems(array $productItems)
    {
        if (!array_key_exists('groupItems', $this->_data)) {
            $profit = $this->_data_1['totalData']['profit'];
            $groups = [];
            foreach ($this->productItems() as $product) {
                $gid = $product['group_id'];
                if (!isset($groups['group_'.$gid])) {
                    $groups['group_' . $gid] = [
                        'group_id' => $gid,
                        'product_id' => null,
                        'title' => $product['group'],
                        'proceeds_1' => 0,
                        'proceeds_2' => 0,
                        'change_proceeds_abs' => 0,
                        'change_proceeds_rel' => 0,
                        'margin_1' => 0,
                        'margin_2' => 0,
                        'change_margin_abs' => 0,
                        'change_margin_rel' => 0,
                        'profit_1' => 0,
                        'overal_profit_percent_1' => 0,
                        'profit_2' => 0,
                        'change_profit_abs' => 0,
                        'change_profit_rel' => 0,
                        'rentability_1' => 0,
                        'rentability_2' => 0,
                        'items' => [],
                    ];
                }
                $groups['group_'.$gid]['proceeds_1'] += $product['proceeds_1'];
                $groups['group_'.$gid]['proceeds_2'] += $product['proceeds_2'];
                $groups['group_'.$gid]['margin_1'] += $product['margin_1'];
                $groups['group_'.$gid]['margin_2'] += $product['margin_2'];
                $groups['group_'.$gid]['profit_1'] += $product['profit_1'];
                $groups['group_'.$gid]['profit_2'] += $product['profit_2'];
                $groups['group_' . $gid]['items'][] = $product;
            }

            foreach ($groups as &$group) {
                $group['change_proceeds_abs'] = $group['proceeds_2'] - $group['proceeds_1'];
                $group['change_proceeds_rel'] = ($group['proceeds_1'])
                    ? $group['change_proceeds_abs'] / $group['proceeds_1'] * 100 : 0;
                $group['change_margin_abs'] = $group['margin_2'] - $group['margin_1'];
                $group['change_margin_rel'] = ($group['margin_1'])
                    ? $group['change_margin_abs'] / $group['margin_1'] * 100 : 0;
                $group['change_profit_abs'] = $group['profit_2'] - $group['profit_1'];
                $group['overal_profit_percent_1'] = ($profit)
                    ? 100 * $group['profit_1'] / $profit : 0;
                $group['change_profit_rel'] = ($group['profit_1'])
                    ? $group['change_profit_abs'] / $group['profit_1'] * 100 : 0;
                $group['rentability_1'] = ($group['proceeds_1'])
                    ? 100 * $group['profit_1'] / $group['proceeds_1'] : 0;
                $group['rentability_2'] = ($group['proceeds_2'])
                    ? 100 * $group['profit_2'] / $group['proceeds_2'] : 0;
            }

            $this->_data['groupItems'] = $groups;
        }

        return $this->_data['groupItems'];
    }

    public function getResultItems()
    {
        $total = $this->_total;

        return [
            0 => [
                'label' => 'Выручка',
                'icon' => 'proceeds',
                'value1' => $total['proceeds_1'],
                'value2' => $total['proceeds_2'],
                'formatting' => 'asMoney',
                'append' => '',
            ],
            1 => [
                'label' => 'Прибыль (EBITDA)',
                'icon' => 'profit_ebitda',
                'value1' => $total['profit_1'],
                'value2' => $total['profit_2'],
                'formatting' => 'asMoney',
                'append' => '',
            ],
            2 => [
                'label' => 'Маржинальность',
                'icon' => 'marginality',
                'value1' => round($total['margin_percent_1'], 2),
                'value2' => round($total['margin_percent_2'], 2),
                'formatting' => 'asNumber',
                'append' => '%',
            ],
            3 => [
                'label' => 'Рентабильность продаж (ROS)',
                'icon' => 'rentability',
                'value1' => round($total['rentability_1'], 2),
                'value2' => round($total['rentability_2'], 2),
                'formatting' => 'asNumber',
                'append' => '%',
            ],
            4 => [
                'label' => 'Средний чек',
                'icon' => 'average_check',
                'value1' => $total['average_check_1'],
                'value2' => $total['average_check_2'],
                'formatting' => 'asMoney',
                'append' => '',
            ],
            5 => [
                'label' => 'Рентабильность затрат (ROM)',
                'icon' => 'rentability_costs',
                'value1' => round($total['rentability_costs_1'], 2),
                'value2' => round($total['rentability_costs_2'], 2),
                'formatting' => 'asNumber',
                'append' => '%',
            ],
        ];
    }

    public function getProfitForYear()
    {
        $total = $this->_total;
        $categories = [];
        $seriesData = [];
        $otherData = [];
        $date = \DateTime::createFromImmutable($this->_from);
        $till = \DateTime::createFromImmutable($this->_till);
        $current = date('m');
        while ($date < $till) {
            $m = $date->format('m');
            $k = (int) $m;
            $month = DateHelper::$months[$m];
            $year = $date->format('Y');
            $categories[] = $month;
            $seriesData[$k] = 0;
            $otherData[$k] = [
                'year' => $year,
                'month' => $k,
                'current' => $m === $current,
                'title' => mb_convert_case($month, MB_CASE_TITLE).' '.$year,
            ];
            $date->modify('+1 month');
        }

        $total1 = $this->_data_1['totalData'];
        $total2 = $this->_data_2['totalData'];

        foreach ($seriesData as $m => $value) {
            $profit1 = intval($total1['month_'.$m] / 100 * $total1['rentability']);
            $profit2 = intval($total2['month_'.$m] / 100 * $total2['rentability']);
            $seriesData[$m] = ($profit2 - $profit1) / 100;
            $otherData[$m]['profitFact'] = $profit1 / 100;
            $otherData[$m]['profitPlan'] = $profit2 / 100;
        }

        return [
            'categories' => $categories,
            'seriesData' => array_values($seriesData),
            'otherData' => array_values($otherData),
        ];
    }

    public function getProductProfit()
    {
        $items = $this->productItems();

        uasort($items, function ($a, $b) {
            return $b['profit_1'] <=> $a['profit_1'];
        });

        $items = array_slice($items, 0, 21);
        $categories = [];
        $seriesData = [
            1 => [],
            2 => [],
        ];

        $hasChangies = $this->getHasChangies();

        foreach ($items as $item) {
            $categories[] = mb_strtoupper($item['title']);
            $seriesData[1][] = $item['profit_1'];
            $seriesData[2][] = $hasChangies ? $item['profit_2'] : null;
        }

        return [
            'items' => $items,
            'categories' => $categories,
            'seriesData' => $seriesData,
        ];
    }

    public function getProfitFactors()
    {
        $total = $this->_total;
        $total1 = $this->_data_1['totalData'];
        $total2 = $this->_data_2['totalData'];
        $categories = [
            'СПРОС',
            'СТРУКТУРА ПРОДАЖ',
            'ПЕРЕМЕННЫЕ РАСХОДЫ',
            'ПОСТОЯННЫЕ РАСХОДЫ',
            'ЦЕНЫ',
            'ОБЩЕЕ ВЛИЯНИЕ ФАКТОРОВ',
        ];

        if ($total1['proceeds']) {
            $k = $total['proceeds_2']/(1+$this->price/100);

            $d1 = ($k/$total['proceeds_1']-1)*$total['profit_1'];
            $d2 = $k-($total1['variable_expenses']*$k/$total['proceeds_1'])-$total1['fixed_expenses']-($total['profit_1']*$k/$total['proceeds_1']);
            $d3 = ($k/$total['proceeds_1']*$total1['variable_expenses'])-$total2['variable_expenses'];
            $d4 = $total1['fixed_expenses']-$total2['fixed_expenses'];
            $d5 = $total['proceeds_2']-($k);
            $d6 = $d1 + $d2 + $d3 + $d4 + $d5;

            $seriesData = [
                round($d1, 2),
                round($d2, 2),
                round($d3, 2),
                round($d4, 2),
                round($d5, 2),
                round($d6, 2),
            ];
        } else {
            $seriesData = [
                0,
                0,
                0,
                0,
                0,
                0,
            ];
        }

        return [
            'categories' => $categories,
            'seriesData' => $seriesData,
        ];
    }

    /**
     * @param $expenseType
     * @return array
     */
    private function getProdNames()
    {
        if (!array_key_exists('getProdNames', $this->_data)) {
            $this->_data['getProdNames'] = (new Query)->select([
                'p.title',
                'p.id',
            ])->distinct()->from([
                't' => '__what_if'
            ])->leftJoin([
                'p' => Product::tableName()
            ], '{{p}}.[[id]] = {{t}}.[[product_id]]')->indexBy('id')->column($this->db);
        }

        return $this->_data['getProdNames'];
    }

    /**
     * @param $expenseType
     * @return array
     */
    private function getGroupNames()
    {
        if (!array_key_exists('getGroupNames', $this->_data)) {
            $this->_data['getGroupNames'] = ProductGroup::find()->select([
                'title',
                'id',
            ])->andWhere([
                'or',
                ['id' => ProductGroup::WITHOUT],
                ['company_id' => $this->company->id],
            ])->indexBy('id')->column($this->db);
        }

        return $this->_data['getGroupNames'];
    }

    /**
     * @param $expenseType
     * @return array
     */
    private function getFixedCostsExpenditureItems()
    {
        if (!array_key_exists('getFixedCostsExpenditureItems', $this->_data)) {
            $this->_data['getFixedCostsExpenditureItems'] = ProfitAndLossCompanyItem::find()
                ->select(['item_id'])
                ->andWhere(['profit_and_loss_company_item.expense_type' => ProfitAndLossCompanyItem::CONSTANT_EXPENSE_TYPE])
                ->andWhere(['profit_and_loss_company_item.company_id' => $this->company->id])
                ->column($this->db);
        }

        return $this->_data['getFixedCostsExpenditureItems'];
    }

    /**
     * @return int
     */
    private function getDocsFixedExpenses() : int
    {
        if (!array_key_exists('getDocsFixedExpenses', $this->_data)) {
            $this->_data['getDocsFixedExpenses'] = (int) (new \yii\db\Query)->from('olap_documents')->andWhere([
                'company_id' => $this->company->id,
                'type' => Documents::IO_TYPE_IN,
                'invoice_expenditure_item_id' => $this->getFixedCostsExpenditureItems(),
            ])->andWhere([
                'between',
                'date',
                $this->_from->format('Y-m-d'),
                $this->_till->format('Y-m-d'),
            ])->sum('amount', $this->db);
        }

        return $this->_data['getDocsFixedExpenses'];
    }

    /**
     * @return integer
     */
    private function getFlowsFixedExpenses() : int
    {
        if (!array_key_exists('getDocsFixedExpenses', $this->_data)) {
            $this->_data['getDocsFixedExpenses'] = (int) (new \yii\db\Query)->from('olap_flows')->andWhere([
                'company_id' => $this->company->id,
                'type' => CashFlowsBase::FLOW_TYPE_EXPENSE,
                'item_id' => $this->getFixedCostsExpenditureItems(),
            ])->andWhere([
                'between',
                'recognition_date',
                $this->_from->format('Y-m-d'),
                $this->_till->format('Y-m-d'),
            ])->andWhere([
                'or',
                ['has_invoice' => false],
                ['has_doc' => false],
                ['need_doc' => false],
            ])->sum('amount', 'db2');
        }

        return $this->_data['getDocsFixedExpenses'];
    }
}
