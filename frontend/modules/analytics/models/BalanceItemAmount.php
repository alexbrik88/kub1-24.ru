<?php

namespace frontend\modules\analytics\models;

use common\components\TextHelper;
use Yii;
use common\models\Company;
use yii\db\Connection;

/**
 * This is the model class for table "balance_item_amount".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $item_id
 * @property string $month
 * @property string $year
 * @property string $amount
 *
 * @property Company $company
 * @property BalanceItem $item
 */
class BalanceItemAmount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'balance_item_amount';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'item_id', 'month', 'year'], 'required'],
            [['company_id', 'item_id'], 'integer'],
            [['amount'],
                'number',
                'numberPattern' => Yii::$app->params['numberPattern'],
                'max' => Yii::$app->params['maxCashSum'] * 100, // with kopeck
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
            [['month', 'year'], 'safe'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => BalanceItem::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'item_id' => 'Item ID',
            'month' => 'Month',
            'year' => 'Year',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->amount = TextHelper::parseMoneyInput($this->amount);

        if ($this->isAttributeChanged('amount')) {
            $this->amount = round($this->amount * 100);
        }

        return parent::beforeValidate();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(BalanceItem::className(), ['id' => 'item_id']);
    }

    /**
     * @param $itemAmount
     * @param $itemID
     * @param $year
     * @return mixed
     * @throws \Throwable
     */
    public static function loadItems($itemAmount, $itemID, $year)
    {
        return Yii::$app->db->transaction(function (Connection $db) use ($itemAmount, $itemID, $year) {
            foreach ($itemAmount as $month => $amount) {
                $month = $month < 10 ? "0{$month}" : $month;
                $balanceItemAmount = BalanceItemAmount::find()
                    ->andWhere(['and',
                        ['item_id' => $itemID],
                        ['month' => $month],
                        ['year' => $year],
                    ])
                    ->one();
                if (!$balanceItemAmount) {
                    $balanceItemAmount = new BalanceItemAmount();
                    $balanceItemAmount->company_id = Yii::$app->user->identity->company_id;
                    $balanceItemAmount->item_id = $itemID;
                    $balanceItemAmount->month = $month;
                    $balanceItemAmount->year = $year;
                }
                $balanceItemAmount->amount = $amount;
                if (!$balanceItemAmount->save()) {
                    $db->transaction->rollBack();
                    return false;
                }
            }

            return true;
        });
    }
}
