<?php
namespace frontend\modules\analytics\models;

class ChartHelper {

    /**
     * @param $left
     * @param $right
     * @param int $offsetYear
     * @param string $offsetDay
     * @return array
     * @throws \Exception
     */
    public static function getFromCurrentDaysPeriods($left, $right, $offsetYear = 0, $offsetDay = "0") {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} days")->modify("{$offsetDay} days");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr->format('Y-m-d'),
                'to' => $curr->format('Y-m-d'),
            ];
            $curr = $curr->modify("+1 days");
        }

        return $ret;
    }

    /**
     * @param $left
     * @param $right
     * @param int $offsetYear
     * @param string $offsetMonth
     * @return array
     * @throws \Exception
     */
    public static function getFromCurrentMonthsPeriods($left, $right, $offsetYear = 0, $offsetMonth = "0")
    {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month")->modify("{$offsetMonth} month");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr->format('Y-m-01'),
                'to' => $curr->format('Y-m-t'),
            ];
            $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }

}