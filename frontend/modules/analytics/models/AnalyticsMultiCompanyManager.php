<?php

namespace frontend\modules\analytics\models;

use common\models\company\CompanyTaxationType;
use Yii;
use common\models\Company;
use common\models\employee\Employee;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class AnalyticsMultiCompanyManager extends \yii\base\Component
{
    /** @var Employee  */
    private Employee $_employee;

    /** @var Company */
    private Company $_primaryCompany;

    /** @var array */
    private array $_selectedCompaniesIds;
    private array $_allowedCompaniesIds;
    private array $_companiesNames = [];
    private array $_companiesTaxationTypes = [];

    public function __construct()
    {
        parent::__construct();

        $this->_employee = Yii::$app->user->identity;
        $this->_primaryCompany = Yii::$app->user->identity->company;

        $companyArray = $this->_employee->companies;
        usort($companyArray, function($c1, $c2) {
            if ($c1->id == $this->_primaryCompany->id) {
                return -1;
            } elseif ($c2->id == $this->_primaryCompany->id) {
                return 1;
            } else {
                return 0;
            }
        });

        $this->_selectedCompaniesIds = [];
        foreach ($this->_employee->analyticsMultiCompanies as $analyticsMultiCompany) {
            if ($analyticsMultiCompany->primary_company_id == $this->_primaryCompany->id) {
                $this->_selectedCompaniesIds[] = $analyticsMultiCompany->company_id;
            }
        }

        $this->_allowedCompaniesIds = [];
        foreach ($companyArray as $company) {
            $this->_companiesNames[$company->id] = $company->getShortName();
            $this->_allowedCompaniesIds[] = $company->id;
        }

        $this->_companiesTaxationTypes = CompanyTaxationType::find()
            ->where(['company_id' => $this->_allowedCompaniesIds])
            ->indexBy('company_id')
            ->asArray()
            ->all();
    }

    public function getCompaniesIds()
    {
        return ($this->getIsModeOn()) ?
            $this->_selectedCompaniesIds :
            [$this->_primaryCompany->id];
    }

    public function getIsModeOn()
    {
        return ($this->_employee) ?
            (bool)$this->_employee->currentEmployeeCompany->analytics_multi_company :
            false;
    }

    public function getIsModeEnabled()
    {
        return count($this->_allowedCompaniesIds) >= AnalyticsMultiCompany::MIN_COMPANIES_COUNT;
    }
    
    public function getCompanyName($id)
    {
        return ArrayHelper::getValue($this->_companiesNames, $id, "id={$id}");
    }

    public function getAllowedCompaniesList()
    {
        return $this->_companiesNames;
    }

    public function canEdit($companyId)
    {
        if (is_array($companyId))
            return ArrayHelper::getValue($companyId, 'company_id') == $this->_primaryCompany->id;

        return $companyId == $this->_primaryCompany->id;
    }

    public function getSubTitle()
    {
        return ($this->getIsModeOn()) ? ' Консолидированный' : '';
    }

    public function getCompanyTaxationType($companyId)
    {
        return ArrayHelper::getValue($this->_companiesTaxationTypes, $companyId, []);
    }

    public function getPrimaryCompany()
    {
        return $this->_primaryCompany;
    }
}
