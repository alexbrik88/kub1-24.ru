<?php
namespace frontend\modules\analytics\models;

use common\components\date\DateHelper;
use common\components\excel\Excel;
use common\components\TextHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\status\InvoiceStatus;
use frontend\components\PageSize;
use frontend\models\Documents;
use frontend\modules\analytics\models\debtor\DebtorHelper2;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * @property $report_type
 * @property $search_by
 * @property $year
 * @property $isCurrentYear
 * Class DebtReportSearch2
 * @package frontend\modules\analytics\models
 */
class DebtReportSearch2 extends Contractor
{
    /**
     *
     */
    const REPORT_ID = 15;

    /**
     *
     */
    const MIN_REPORT_DATE = '2000-01-01';

    /**
     *
     */
    const REPORT_TYPE_MONEY = 1;
    const REPORT_TYPE_GUARANTY = 2;
    const REPORT_TYPE_NET_TOTAL = 3;

    /**
     *
     */
    const SEARCH_BY_INVOICES = 1; // other search model
    const SEARCH_BY_DOCUMENTS = 2; // other search model
    const SEARCH_BY_DEBT_PERIODS = 3;

    /**
     *
     */
    const SEARCH_BY_DEBT_PERIODS_BY_INVOICES = 1;
    const SEARCH_BY_DEBT_PERIODS_BY_DOCUMENTS = 2;

    /**
     * @var
     */
    public $title;

    /**
     * @var
     */
    private $_year = null;

    /**
     * @var int
     */
    private $_search_by = self::SEARCH_BY_INVOICES;

    /**
     * @var int
     */
    private $_report_type = self::REPORT_TYPE_MONEY;

    /**
     * @var int
     */
    private $_search_by_debt_periods_by = self::SEARCH_BY_DEBT_PERIODS_BY_DOCUMENTS;

    /**
     * @var
     */
    private $_query;
    private $_queryTotal;

    /**
     * @var array
     */
    private $_filteredTotalDebts = [];

    public $debt_0_10_sum;
    public $debt_11_30_sum;
    public $debt_31_60_sum;
    public $debt_61_90_sum;
    public $debt_more_90_sum;
    public $current_debt_sum;
    public $debt_all_sum;

    protected $company;
    protected $user;

    /**
     * @var array
     */
    public $netTotals = [];

    /**
     * @var int
     */
    public $net_contractor_type;

    /**
     * @var
     */
    public $net_debt_type;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'name', 'debtType', 'responsible_employee_id', 'net_contractor_type', 'net_debt_type'], 'integer'],
        ];
    }

    public function init()
    {
        $this->company_id = Yii::$app->user->identity->company->id;
        $this->_search_by_debt_periods_by = Yii::$app->user->identity->config->report_debtor_period_type;
        $this->year = \Yii::$app->session->get('modules.reports.finance.year', date('Y'));
    }

    /**
     * @param $params
     * @param int $documentType
     * @return ActiveDataProvider
     */
    public function findDebtor($params, $documentType = Documents::IO_TYPE_OUT, $debtsOnDate = null)
    {
        $this->load($params);
        $currDate = $debtsOnDate ?? ($this->isCurrentYear ? date('Y-m-d') : ($this->year.'-12-31'));
        $currSum = 'invoice_sum - flow_sum';

        switch ($this->_search_by) {
            case self::SEARCH_BY_INVOICES:
                $subQuery = DebtorHelper2::getQueryByInvoices($this->company_id, $documentType);
                break;
            case self::SEARCH_BY_DEBT_PERIODS:
            case self::SEARCH_BY_DOCUMENTS:
            default:
                $subQuery = DebtorHelper2::getQueryByDocs($this->company_id, $documentType);
                break;
        }

        $query = self::find()
            ->addSelect(new Expression("
                contractor.id,
                contractor.type,
                contractor.name,
                contractor.responsible_employee_id,
                contractor.company_type_id,
                contractor.face_type,
                contractor.foreign_legal_form,
                docs_flows.invoice_expenditure_item_id,
                SUM(IF(payment_limit_date >= '$currDate' AND docs_flows.date <= '$currDate', $currSum, 0)) AS current_debt_sum,
                SUM(IF(payment_limit_date <  '$currDate', $currSum, 0)) AS overdue_debt_sum,
                SUM(IF(payment_limit_date <= '$currDate' - INTERVAL  1 DAY AND payment_limit_date >= '$currDate' - INTERVAL 10 DAY, $currSum, 0)) AS debt_0_10_sum,
                SUM(IF(payment_limit_date <= '$currDate' - INTERVAL 11 DAY AND payment_limit_date >= '$currDate' - INTERVAL 30 DAY, $currSum, 0)) AS debt_11_30_sum,
                SUM(IF(payment_limit_date <= '$currDate' - INTERVAL 31 DAY AND payment_limit_date >= '$currDate' - INTERVAL 60 DAY, $currSum, 0)) AS debt_31_60_sum,
                SUM(IF(payment_limit_date <= '$currDate' - INTERVAL 61 DAY AND payment_limit_date >= '$currDate' - INTERVAL 90 DAY, $currSum, 0)) AS debt_61_90_sum,
                SUM(IF(payment_limit_date <= '$currDate' - INTERVAL 91 DAY, $currSum, 0)) AS debt_more_90_sum,
                SUM(IF(docs_flows.date <= '$currDate', $currSum, 0)) AS debt_all_sum
            "))
            ->leftJoin(['docs_flows' => $subQuery], "docs_flows.contractor_id = contractor.id AND docs_flows.date <= '{$currDate}'")
            ->where([
                'or',
                ['contractor.company_id' => null],
                ['contractor.company_id' => $this->company_id]
            ])
            ->andWhere(['or',
                ['contractor.type' => $documentType],
                ($documentType == Documents::IO_TYPE_OUT)
                    ? ['contractor.is_customer' => 1]
                    : ['contractor.is_seller' => 1]
            ]);

        $this->_query = clone $query;

        $query
            ->andFilterWhere(['contractor.id' => $this->name])
            ->andFilterWhere(['like', 'contractor.name', $this->title])
            ->andFilterWhere(['contractor.responsible_employee_id' => $this->responsible_employee_id]);

        $this->_queryTotal = clone $query;

        $query->groupBy('contractor.id');
        $query->having(['>', 'debt_all_sum', 0]);

        // var_dump($query->createCommand()->rawSql);exit;

        return new ActiveDataProvider([
            'db' => \Yii::$app->db2,
            'query' => $query,
            'pagination' => [
                'pageSize' => PageSize::get(),
            ],
            'sort' => [
                'attributes' => [
                    'current_debt_sum',
                    'debt_0_10_sum',
                    'debt_11_30_sum',
                    'debt_31_60_sum',
                    'debt_61_90_sum',
                    'debt_more_90_sum',
                    'debt_all_sum',
                ],
                'defaultOrder' => [
                    'debt_all_sum' => SORT_DESC,
                ],
            ],
        ]);
    }

    /**
     * @param int $documentType
     * @return array
     */
    public function getContractorFilter($documentType = Documents::IO_TYPE_OUT)
    {
        $query = Contractor::getSorted()
            ->select([
                'name' => "IF(
                    {{contractor}}.[[company_type_id]] = :typeIP,
                    CONCAT({{company_type}}.[[name_short]], ' ', {{contractor}}.[[name]]),
                    CONCAT({{company_type}}.[[name_short]], ' ', {{contractor}}.[[name]])
                )",
                'contractor.id',
            ])
            ->distinct()
            ->leftJoin('invoice', '{{contractor}}.[[id]] = {{invoice}}.[[contractor_id]]')
            ->andWhere([
                'contractor.company_id' => Yii::$app->user->identity->company->id,
                'contractor.type' => $documentType,
                'invoice.type' => $documentType,
                'invoice.invoice_status_id' => InvoiceStatus::$payAllowed,
                'invoice.is_deleted' => false,
            ])
            ->andFilterWhere(['like', 'contractor.name', $this->title])
            ->params([':typeIP' => CompanyType::TYPE_IP])
            ->asArray()
            ->indexBy('id');

        return ArrayHelper::merge([null => 'Все'], $query->column());
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $range = [];
        $cashOrderMinDate = CashOrderFlows::find()
            ->byCompany($company->id)
            ->min('date', Yii::$app->db2);
        $cashBankMinDate = CashBankFlows::find()
            ->byCompany($company->id)
            ->min('date', Yii::$app->db2);
        $cashEmoneyMinDate = CashEmoneyFlows::find()
            ->byCompany($company->id)
            ->min('date', Yii::$app->db2);
        $minDates = [];

        if ($cashOrderMinDate) $minDates[] = $cashOrderMinDate;
        if ($cashBankMinDate) $minDates[] = $cashBankMinDate;
        if ($cashEmoneyMinDate) $minDates[] = $cashEmoneyMinDate;

        $minCashDate = !empty($minDates) ? max(self::MIN_REPORT_DATE, min($minDates)) : date(DateHelper::FORMAT_DATE);
        $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
        $currentYear = date('Y');
        foreach (range($registrationYear, $currentYear) as $value) {
            $range[$value] = $value;
        }
        arsort($range);

        return $range;
    }

    public function getResponsibleItemsByQuery()
    {
        $dataArray = Yii::$app->user->identity->company
            ->getEmployeeCompanies()
            ->andWhere([
                'employee_id' => $this->_query->select([
                    'contractor.responsible_employee_id',
                    'contact' => 'IF(contractor.contact_is_director = true, contractor.director_name, contractor.contact_name)',
                ])->distinct()->groupBy('contractor.id')->column(),
            ])
            ->orderBy(['lastname' => SORT_ASC, 'firstname_initial' => SORT_ASC, 'patronymic_initial' => SORT_ASC])
            ->all();

        return ['' => 'Все'] + ArrayHelper::map($dataArray, 'employee_id', function ($model) {
                return $model->getFio(true);
            });
    }

    // like BALANCE SEARCH ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public function setReport_type($val) {

        if (in_array($val, [self::REPORT_TYPE_MONEY, self::REPORT_TYPE_GUARANTY]))
            $this->_report_type = $val;
    }

    public function setSearch_by($val) {

        if (in_array($val, [self::SEARCH_BY_INVOICES, self::SEARCH_BY_DOCUMENTS, self::SEARCH_BY_DEBT_PERIODS]))
            $this->_search_by = $val;
    }

    public function getReport_type() {

        return $this->_report_type;
    }

    public function getSearch_by() {

        return $this->_search_by;
    }

    public function setYear($year)
    {
        $this->_year = (int)$year ?: date('Y');
    }

    public function getYear()
    {
        return $this->_year ?: date('Y');
    }

    public function getIsCurrentYear()
    {
        return date('Y') == $this->year;
    }

    public function getIsSearchByPeriodsByInvoices()
    {
        return $this->_search_by_debt_periods_by == self::SEARCH_BY_DEBT_PERIODS_BY_INVOICES;
    }

    // CHARTS //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Main chart
    public function getFromCurrentMonthsPeriods($left, $right, $offsetYear = 0, $offsetMonth = "0") {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month")->modify("{$offsetMonth} month");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr->format('Y-m-01'),
                'to' => $curr->format('Y-m-t'),
            ];
            $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }

    public function getFilteredTotalDebts($key)
    {
        if (!$this->_filteredTotalDebts) {
            $query = (clone $this->_queryTotal)->having(['>', 'debt_all_sum', 0])->groupBy('contractor.id');
            $this->_filteredTotalDebts = $this->__getTotalDebts($query);
        }

        return ArrayHelper::getValue($this->_filteredTotalDebts, $key);
    }

    public function __getTotalDebts($query)
    {
        $tmpArr = $query->asArray()->all();

        $ret = [
            'current_debt_sum' => 0,
            'overdue_debt_sum' => 0,
            'debt_0_10_sum' => 0,
            'debt_11_30_sum' => 0,
            'debt_31_60_sum' => 0,
            'debt_61_90_sum' => 0,
            'debt_more_90_sum' => 0,
            'debt_all_sum' => 0,
        ];

        foreach ($tmpArr as $tmp) {
            $ret['current_debt_sum'] += $tmp['current_debt_sum'];
            $ret['overdue_debt_sum'] += $tmp['overdue_debt_sum'];
            $ret['debt_0_10_sum'] += $tmp['debt_0_10_sum'];
            $ret['debt_11_30_sum'] += $tmp['debt_11_30_sum'];
            $ret['debt_31_60_sum'] += $tmp['debt_31_60_sum'];
            $ret['debt_61_90_sum'] += $tmp['debt_61_90_sum'];
            $ret['debt_more_90_sum'] += $tmp['debt_more_90_sum'];
            $ret['debt_all_sum'] += $tmp['debt_all_sum'];
        }

        return $ret;
    }

    // Top chart #1
    public function getTotalDebts()
    {
        $query = (clone $this->_query)->having(['>', 'debt_all_sum', 0])->groupBy('contractor.id');

        return $this->__getTotalDebts($query);
    }

    // Top chart #2
    public function getTopByContractors($maxRowsCount)
    {
        $query = clone $this->_query;
        return $query->having(['>', 'debt_all_sum', 0])->groupBy('contractor.id')->orderBy(['debt_all_sum' => SORT_DESC])->asArray()->limit($maxRowsCount)->all();
    }

    // Top chart #3/1 (data + tooltip data)
    public function getTopByEmployersArr($maxRowsCount = 5, $maxTooltipRowsCount = 5)
    {
        $query = clone $this->_query;
        $rawData  = $query->having(['>', 'debt_all_sum', 0])->groupBy(['contractor.responsible_employee_id', 'contractor.id'])->orderBy(['debt_all_sum' => SORT_DESC])->asArray()->all();
        $chartData = $tooltipData = [];
        $_tmpTooltipData = [];
        foreach ($rawData as $v)
        {
            if (!isset($chartData[$v['responsible_employee_id']]))
                $chartData[$v['responsible_employee_id']] = [
                    'responsible_employee_id' => $v['responsible_employee_id'],
                    'sum' => 0
                ];

            $chartData[$v['responsible_employee_id']]['sum'] += $v['debt_all_sum'];

            if (!isset($_tmpTooltipData[$v['responsible_employee_id']]))
                $_tmpTooltipData[$v['responsible_employee_id']] = [];

            $_tmpTooltipData[$v['responsible_employee_id']][] = [
                'period' => Contractor::title($v),
                'sum'  => $v['debt_all_sum'],
                'percent' => 0
            ];
        }

        uasort($chartData, function ($a, $b) {
            return $b['sum'] <=> $a['sum'];
        });

        $chartData = array_slice($chartData, 0, $maxRowsCount, true);

        foreach ($chartData as $employeeId => $value) {
            $pos = 0;
            foreach ((array)$_tmpTooltipData[$employeeId] as $kk => $vv) {
                if (++$pos > $maxTooltipRowsCount)
                    break;
                $tooltipData[$employeeId][$kk]['period'] = $vv['period'];
                $tooltipData[$employeeId][$kk]['percent'] = 100 * min(100, $vv['sum'] / (1 / 1E9 + max(array_column($_tmpTooltipData[$employeeId], 'sum'))));
                $tooltipData[$employeeId][$kk]['sum'] = TextHelper::invoiceMoneyFormat($vv['sum'], 2);
            }
        }

        return [
            'chart' => $chartData,
            'tooltip' => array_values($tooltipData)
        ];
    }
    // Top chart #3/2
    public function getTopByExpenditureItem($maxRowsCount)
    {
        $query = clone $this->_query;
        return $query->having(['>', 'debt_all_sum', 0])->groupBy('invoice_expenditure_item_id')->orderBy(['debt_all_sum' => SORT_DESC])->asArray()->limit($maxRowsCount)->all();

    }

    // NET TOTAL //////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @param $params
     * @return SqlDataProvider
     */
    public function findNetTotalDebts($params)
    {
        $this->load($params);

        $tableFlows = 'olap_flows';
        $tableInvoices = 'olap_invoices';
        $tableContractors = 'contractor';
        $companyId = $this->company_id;
        $currDate = $this->isCurrentYear ? date('Y-m-d') : ($this->year.'-12-31');

        // filter prepayments
        $_FLOW_ITEMS_IN = DebtorHelper2::getSqlFilterItems(0);
        $_FLOW_ITEMS_OUT = DebtorHelper2::getSqlFilterItems(1);
        $SQL_FILTER_FLOW_ITEMS = "AND (t.type = 0 {$_FLOW_ITEMS_IN} OR t.type = 1 {$_FLOW_ITEMS_OUT})";
        $SQL_FILTER_WALLETS = DebtorHelper2::getSqlFilterWallets();
        $SQL_FILTER_RESPONSIBLE_EMPLOYEE = ($this->responsible_employee_id) ? "AND c.responsible_employee_id = " . (int)$this->responsible_employee_id : "";
        $SQL_FILTER_CONTRACTOR_TITLE = $this->title ? 'AND c.name LIKE "%'.(str_replace('"', '', htmlspecialchars($this->title))).'%"' : '';
        $SQL_FILTER_CONTRACTOR_TYPE = $this->__getNetFilterContractorType();
        $SQL_HAVING_DEBT_TYPE = $this->__getNetFilterDebtType();
        $AND_HIDE_TIN_PARENT = $this->__getSqlHideTinParent();
        
        $query = "
            SELECT 
            IF (c.type = 1 AND c.opposite_id IS NOT NULL, c.opposite_id, c.id) AS cid,
            c.responsible_employee_id, 
            c.name,
            c.company_type_id,
            c.face_type,
            c.foreign_legal_form,
            SUM(z.money_1) AS money_1,
            SUM(z.money_2) AS money_2,
            SUM(z.guarancy_1) AS guarancy_1,
            SUM(z.guarancy_2) AS guarancy_2,
            SUM(z.money_1 + z.guarancy_2) AS total_1,
            SUM(z.money_2 + z.guarancy_1) AS total_2,
            SUM(z.money_2 - z.money_1 + z.guarancy_1 - z.guarancy_2) AS total_diff
            FROM (
            
                SELECT 
                  t.contractor_id,  
                  SUM(IF(t.type = 1, t.total_amount_with_nds - t.payment_partial_amount, 0)) AS money_1,
                  SUM(IF(t.type = 2, t.total_amount_with_nds - t.payment_partial_amount, 0)) AS money_2,
                  0 AS guarancy_1,
                  0 AS guarancy_2
                FROM {$tableInvoices} t
                WHERE t.company_id = {$companyId}
                  AND t.date <= '{$currDate}' 
                  AND t.status_id NOT IN (5,7,9)
                  AND t.is_deleted = 0
                  AND (t.has_doc = 1 OR t.need_all_docs = 0)
                GROUP BY t.contractor_id            
            
                UNION ALL
            
                SELECT
                  t.contractor_id,
                  0 AS money_1,
                  0 AS money_2,
                  SUM(IF(t.type = 1, t.total_amount_with_nds, 0)) AS guarancy_1,
                  SUM(IF(t.type = 2, t.total_amount_with_nds, 0)) AS guarancy_2
                FROM {$tableInvoices} t
                WHERE t.company_id = {$companyId}
                  AND FROM_UNIXTIME(t.status_updated_at, '%Y-%m-%d') <= '{$currDate}'
                  AND t.is_deleted = FALSE
                  AND t.status_id = " . InvoiceStatus::STATUS_PAYED . "
                  AND t.has_doc = FALSE
                  AND t.need_all_docs = TRUE
                GROUP BY t.contractor_id
                
                UNION ALL
                
                SELECT
                  t.contractor_id, 
                  0 AS money_1,
                  0 AS money_2,
                  SUM(IF(t.type = 0, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS guarancy_1,
                  SUM(IF(t.type = 1, IF(t.has_tin_parent = 1, t.tin_child_amount - IFNULL(t.tin_child_invoice_amount, 0), t.amount - t.invoice_amount), 0)) AS guarancy_2
                FROM {$tableFlows} t 
                WHERE t.company_id = {$companyId}
                  AND t.recognition_date <= '{$currDate}'
                  AND (t.contractor_id > 0)
                  {$SQL_FILTER_FLOW_ITEMS}
                  {$SQL_FILTER_WALLETS}
                  {$AND_HIDE_TIN_PARENT}
                GROUP BY t.contractor_id
            ) z 
        
            LEFT JOIN {$tableContractors} c ON c.id = z.contractor_id
            
            WHERE 1=1
              {$SQL_FILTER_RESPONSIBLE_EMPLOYEE}
              {$SQL_FILTER_CONTRACTOR_TITLE}
              {$SQL_FILTER_CONTRACTOR_TYPE}
              
            GROUP BY cid
            {$SQL_HAVING_DEBT_TYPE}
        ";

        $this->netTotals = Yii::$app->db2->createCommand("
            SELECT 
              COUNT(cid) AS pages_count, 
              SUM(money_1) AS money_1,
              SUM(money_2) AS money_2,
              SUM(guarancy_1) AS guarancy_1,
              SUM(guarancy_2) AS guarancy_2,
              SUM(money_1 + guarancy_2) AS total_1,
              SUM(money_2 + guarancy_1) AS total_2,
              SUM(total_diff) AS total_diff
            FROM ({$query}) totals")->queryOne();

        return new SqlDataProvider([
            'db' => \Yii::$app->db2,
            'sql' => $query,
            'totalCount' => ArrayHelper::getValue($this->netTotals, 'pages_count', 0),
            'pagination' => [
                'pageSize' => PageSize::get(),
            ],
            'sort' => [
                'attributes' => [
                    'money_1',
                    'guarancy_1',
                    'total_1',
                    'money_2',
                    'guarancy_2',
                    'total_2',
                    'total_diff'
                ],
                'defaultOrder' => [
                    'total_diff' => SORT_DESC,
                ],
            ],
        ]);
    }
    
    private function __getNetFilterContractorType()
    {
        switch ($this->net_contractor_type) {
            case '21':
                return "AND c.opposite_id IS NOT NULL";
                break;
            case '2':
                return "AND c.type = 2 AND c.opposite_id IS NULL";
                break;
            case '1':
                return "AND c.type = 1 AND c.opposite_id IS NULL";
                break;
        }

        return null;
    }

    private function __getNetFilterDebtType()
    {
        switch ($this->net_debt_type) {
            case '21':
                return "HAVING total_2 > 0 AND total_1 > 0";
                break;
            case '2':
                return "HAVING total_2 > 0";
                break;
            case '1':
                return "HAVING total_1 > 0";
                break;
        }

        return null;
    }

    protected function __getSqlHideTinParent()
    {
        return " AND t.has_tin_children = 0 ";
    }

    // XLS ////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @param $type
     * @throws \Exception
     */
    public function generateXls($type)
    {
        $reportType = Yii::$app->request->get('report_type', self::REPORT_TYPE_MONEY);
        $searchBy = Yii::$app->request->get('search_by', self::SEARCH_BY_INVOICES);
        $ioType = Yii::$app->request->get('io_type', 2);

        $this->search_by = $searchBy;
        $this->report_type = $reportType;

        if ($reportType == self::REPORT_TYPE_NET_TOTAL) {

            return $this->_generateNetTotalXls();
        }

        return $this->_generateDebtorXls($ioType);
    }

    protected function _generateDebtorXls($ioType)
    {
        $dataProvider = $this->findDebtor([], $ioType);
        $dataProvider->setPagination(false);
        $data = $dataProvider->getModels();

        // header
        $columns = [
            ['attribute' => 'name', 'header' => 'Название'],
            ['attribute' => 'current_debt_sum', 'header' => 'Текущая', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00],
            ['attribute' => 'debt_0_10_sum', 'header' => '0-10 дней', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00],
            ['attribute' => 'debt_11_30_sum', 'header' => '11-30 дней', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00],
            ['attribute' => 'debt_31_60_sum', 'header' => '31-60 дней', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00],
            ['attribute' => 'debt_61_90_sum', 'header' => '61-90 дней', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00],
            ['attribute' => 'debt_more_90_sum', 'header' => 'Больше 90 дней', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00],
            ['attribute' => 'debt_all_sum', 'header' => 'Итого', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00],
        ];

        // data
        $formattedData = [];
        foreach ($data as $d) {
            $formattedData[] = $this->_buildXlsRow($d);
        }

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $formattedData,
            'title' => ($ioType == 2 ? "Нам должны" : "Мы должны") . " за {$this->year} год",
            'rangeHeader' => range('A', 'H'),
            'columns' => $columns,
            'format' => 'Xlsx',
            'fileName' => ($ioType == 2 ? "Нам должны" : "Мы должны") . " за {$this->year} год",
        ]);
    }

    protected function _generateNetTotalXls()
    {
        $dataProvider = $this->findNetTotalDebts([]);
        $dataProvider->setPagination(false);
        $data = $dataProvider->getModels();

        // header
        $columns = [
            ['attribute' => 'name', 'header' => 'Название'],
            ['attribute' => 'type', 'header' => 'Тип'],
            ['attribute' => 'money_2', 'header' => 'Нам должны деньги', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00],
            ['attribute' => 'guarancy_1', 'header' => 'Нам должны обязательства', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00],
            ['attribute' => 'money_1', 'header' => 'Мы должны деньги', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00],
            ['attribute' => 'guarancy_2', 'header' => 'Мы должны обязательства', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00],
            ['attribute' => 'total_diff', 'header' => 'Сальдо', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00],
        ];

        // data
        $formattedData = [];
        foreach ($data as $d) {
            $formattedData[] = $this->_buildNetTotalXlsRow($d);
        }

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $formattedData,
            'title' => "Долги за {$this->year} год",
            'rangeHeader' => range('A', 'H'),
            'columns' => $columns,
            'format' => 'Xlsx',
            'fileName' => "Долги за {$this->year} год"
        ]);
    }

    protected function _buildXlsRow($data)
    {
        $data['name'] = Contractor::title($data) ?: null;
        $data['current_debt_sum'] = round($data['current_debt_sum'] / 100, 2);
        $data['debt_0_10_sum'] = round($data['debt_0_10_sum'] / 100, 2);
        $data['debt_11_30_sum'] = round($data['debt_11_30_sum'] / 100, 2);
        $data['debt_31_60_sum'] = round($data['debt_31_60_sum'] / 100, 2);
        $data['debt_61_90_sum'] = round($data['debt_61_90_sum'] / 100, 2);
        $data['debt_more_90_sum'] = round($data['debt_more_90_sum'] / 100, 2);
        $data['debt_all_sum'] = round($data['debt_all_sum'] / 100, 2);
        return $data;
    }

    protected function _buildNetTotalXlsRow($data)
    {
        /** @var $c Contractor */
        $data['name'] = Contractor::title($data) ?: null;
        if ($c) {
            $data['type'] = (!$c->opposite_id) ? ($c->type == 2 ? 'Покупатель' : 'Поставщик') : 'Покуп. & Пост.';
        } else {
            $data['type'] = null;
        }
        $data['money_2'] = round($data['money_2'] / 100, 2);
        $data['guarancy_1'] = round($data['guarancy_1'] / 100, 2);
        $data['money_1'] = round($data['money_1'] / 100, 2);
        $data['guarancy_2'] = round($data['guarancy_2'] / 100, 2);
        $data['total_diff'] = round($data['total_diff'] / 100, 2);
        return $data;
    }
}