<?php

namespace frontend\modules\analytics\models\debtor;

use common\models\cash\CashFlowsBase;
use common\models\Company;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\employee\EmployeeRole;
use frontend\models\Documents;
use yii\base\Exception;
use yii\db\Expression;
use yii\db\Query;

class DebtorHelperOLD {

    /**
     * Periods
     */
    const PERIOD_0_10 = 1;
    const PERIOD_11_30 = 2;
    const PERIOD_31_60 = 3;
    const PERIOD_61_90 = 4;
    const PERIOD_MORE_90 = 5;
    const PERIOD_0_MORE_90 = 6;
    const PERIOD_ACTUAL = 7;
    const PERIOD_OVERDUE = 8;

    /**
     * @param $debtType
     * @param $dateFrom
     * @param $dateTill
     * @param int $documentType
     * @return Query
     * @throws Exception
     */
    public static function getDebts($debtType, $dateFrom, $dateTill, $documentType = Documents::IO_TYPE_OUT)
    {
        $company = \Yii::$app->user->identity->company;
        $period = self::getPeriodByDebtType($debtType);

        if ($dateTill == date('Y-m-t')) {
            $dateTill = date('Y-m-d');
            $isCurrMonth = true;
        } else {
            $isCurrMonth = false;
        }

        $query = Invoice::find()
            ->leftJoin(['pay' => self::payQuery($company, $documentType)], "{{invoice}}.[[id]] = {{pay}}.[[invoice_id]]")
            ->select([
                'invoice.id',
                'invoice.document_date',
                'invoice.payment_limit_date',
                'flow_date' => 'IFNULL(pay.flow_date, "'.$dateTill.'")',
                'sum' => '(IFNULL(total_amount_with_nds, 0))',
                'flow_sum' => '(IFNULL(pay.flow_sum, 0))'
            ])
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $company->id,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
            ->andWhere(['invoice.type' => $documentType])
            ->andWhere(['<=', 'invoice.document_date', $dateTill]);

        if ($isCurrMonth) {
            $query->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_PAYED]]);
        }

        $invoices = (new Query)->select([
            'sum' => new Expression('IF(flow_date < "'.$dateFrom.'", sum - flow_sum, sum)'),
            'flow_date',
            'payment_limit_date',
            'document_date'
        ])->from(['q' => $query]);

        if ($period['actual']) {
            $invoices->andWhere(['>=', 'payment_limit_date', $dateTill]);
        }
        if ($period['overdue']) {
            $invoices->andWhere(['<', 'payment_limit_date', $dateTill]);
        }
        if ($period['min']) {
            $invoices->andWhere(['<=', 'payment_limit_date', (date_create_from_format('Y-m-d', $dateTill))->modify("- {$period['min']} days")->format('Y-m-d')]);
        }
        if ($period['max']) {
            $invoices->andWhere(['>=', 'payment_limit_date', (date_create_from_format('Y-m-d', $dateTill))->modify("- {$period['max']} days")->format('Y-m-d')]);
        }

        return $invoices;
    }

    /**
     * @param $type
     * @return array
     * @throws Exception
     */
    public static function getPeriodByDebtType($type)
    {
        switch ($type) {
            case self::PERIOD_0_10:
                $min = 1;
                $max = 10;
                break;
            case self::PERIOD_11_30:
                $min = 11;
                $max = 30;
                break;
            case self::PERIOD_31_60:
                $min = 31;
                $max = 60;
                break;
            case self::PERIOD_61_90:
                $min = 61;
                $max = 90;
                break;
            case self::PERIOD_MORE_90:
                $min = 91;
                break;
            case self::PERIOD_0_MORE_90:
                $min = 1;
                break;
            case self::PERIOD_ACTUAL:
                $actual = true;
                break;
            case self::PERIOD_OVERDUE:
                $overdue = true;
                break;
            default:
                break;
        }

        return [
            'min' => $min ?? null,
            'max' => $max ?? null,
            'actual' => $actual ?? null,
            'overdue' => $overdue ?? null
        ];
    }

    public static function payQuery(Company $company, $documentType)
    {
        $flowType = ($documentType == Documents::IO_TYPE_OUT) ?
            CashFlowsBase::FLOW_TYPE_INCOME : CashFlowsBase::FLOW_TYPE_EXPENSE;

        $paySelect = ['link.amount', 'link.invoice_id', 'flow.date'];
        $payWhere = [
            'and',
            ['flow.company_id' => $company->id],
            ['flow.flow_type' => $flowType],
        ];
        $payQuery = new Query;

        return $payQuery->select([
            'invoice_id',
            'flow_sum' => 'SUM([[amount]])',
            'flow_date' => 'date',
        ])->from(['t' => CashFlowsBase::getAllFlows($paySelect, $payWhere, true, 'innerJoin')])->groupBy('t.invoice_id');
    }
}