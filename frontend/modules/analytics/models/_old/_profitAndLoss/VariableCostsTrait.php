<?php

namespace frontend\modules\analytics\models\profitAndLoss\old;

use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceIncomeItem;
use frontend\models\Documents;
use common\models\product\Product;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\models\ProfitAndLossCompanyItem;
use yii\db\Expression;

trait VariableCostsTrait {

    /**
     * @param $parentItem
     * @param $year
     * @return mixed
     */
    public function getVariableCosts($parentItem, $year)
    {
        $result = [
            'productPrimeCost' => self::_getBlankItem('variable-costs expenses-block', 'Себестоимость товаров', ProfitAndLossCompanyItem::VARIABLE_EXPENSE_TYPE),
            'servicePrimeCost' => self::_getBlankItem('variable-costs expenses-block', 'Себестоимость услуг', ProfitAndLossCompanyItem::VARIABLE_EXPENSE_TYPE)
        ];

        if (!array_key_exists('variableCosts' . $year, $this->data)) {

            $dateFrom = "{$year}-01-01";
            $dateTo = "{$year}-12-31";

            //////////// BY DOC ////////////
            /// сумма всех Актов, ТН и УПД, дата которых в нужном месяце

            foreach (self::docClassArray() as $docsAlias => $docClassName) {
                $t = $docClassName::tableName();

                /** @var  $docClassName Act */
                $docData = Invoice::find()
                    ->joinWith($docsAlias, false)
                    ->select(["{$t}.id", "{$t}.document_date", 'invoice.contractor_id'])
                    ->andWhere(['invoice.company_id' => $this->company->id])
                    ->andWhere(['invoice.type' => Documents::IO_TYPE_OUT])
                    ->andWhere(['between', "{$t}.document_date", $dateFrom, $dateTo])
                    ->indexBy('id')
                    ->asArray()
                    ->all();

                $orderTable = AbstractFinance::$docOrdersTables[$docsAlias];

                foreach (['productPrimeCost' => Product::PRODUCTION_TYPE_GOODS, 'servicePrimeCost' => Product::PRODUCTION_TYPE_SERVICE] as $RESULT_KEY => $productionType) {

                    $amount = $orderTable['class']::find()
                        ->leftJoin('product', "{$orderTable['name']}.product_id = `product`.id")
                        ->where([$orderTable['key'] => array_column($docData, 'id')])
                        ->andWhere(['product.production_type' => $productionType])
                        ->select(new Expression("{$orderTable['key']}, product_id, SUM({$orderTable['name']}.quantity * `product`.price_for_buy_with_nds) sum"))
                        ->groupBy($orderTable['key'])
                        ->indexBy($orderTable['key'])
                        ->asArray()
                        ->all();

                    array_walk($docData, function(&$v, $k) use ($amount) { $v['sum'] = $amount[$k]['sum'] ?? 0; });

                    foreach ($docData as $doc) {
                        $monthNumber = substr($doc['document_date'], 5, 2);

                        $result[$RESULT_KEY]['amount'][$monthNumber] += $doc['sum'];
                        $parentItem['amount'][$monthNumber] += $doc['sum'];

                        // amount taxable
                        $isAccounting = !in_array($doc['contractor_id'], self::$notAccountingContractors);
                        if ($isAccounting) {
                            $result[$RESULT_KEY]['amountTax'][$monthNumber] += $doc['sum'];
                            $parentItem['amountTax'][$monthNumber] += $doc['sum'];
                        }
                    }
                }
            }

            //////////// BY NO DOC ////////////
            ///  1) есть привязка к счетам, у которых НЕТ Актов/ТН/УПД или у них стоит "Без Акта/ТН/УПД" и "Дата признания дохода" в нужном месяце +
            foreach (self::flowClassArray() as $flowClassName) {
                $flowTableName = $flowClassName::tableName();

                foreach (['productPrimeCost' => Product::PRODUCTION_TYPE_GOODS, 'servicePrimeCost' => Product::PRODUCTION_TYPE_SERVICE] as $RESULT_KEY => $productionType) {
                    $flows = $flowClassName::find()
                        ->andWhere(["{$flowClassName::tableName()}.company_id" => $this->company->id])
                        ->byFlowType(CashFlowsBase::FLOW_TYPE_INCOME)
                        ->andWhere(['not', ['in', 'income_item_id', [
                            InvoiceIncomeItem::ITEM_STARTING_BALANCE,
                            InvoiceIncomeItem::ITEM_RETURN,
                            InvoiceIncomeItem::ITEM_BORROWING_REDEMPTION,
                            InvoiceIncomeItem::ITEM_BUDGET_RETURN,
                            InvoiceIncomeItem::ITEM_LOAN,
                            InvoiceIncomeItem::ITEM_CREDIT,
                            InvoiceIncomeItem::ITEM_ENSURE_PAYMENT,
                            InvoiceIncomeItem::ITEM_FROM_FOUNDER,
                            InvoiceIncomeItem::ITEM_OWN_FOUNDS,
                            InvoiceIncomeItem::ITEM_OTHER,
                            InvoiceIncomeItem::ITEM_RECEIVED_PERCENTS,
                            InvoiceIncomeItem::ITEM_RECEIVED_PERCENTS_BY_DEPOSITS
                        ]]])
                        ->andWhere(['product.production_type' => $productionType])
                        ->select([
                            new Expression("DISTINCT({$flowTableName}.id)"),
                            "(`product`.price_for_buy_with_nds * `order`.quantity) sum",
                            "{$flowClassName::tableName()}.contractor_id",
                            "{$flowClassName::tableName()}.recognition_date",
                            $flowClassName == CashBankFlows::class ?
                                new Expression("1 AS is_accounting") : "{$flowTableName}.is_accounting"
                        ])
                        ->joinWith([
                            'invoices.orders.product',
                        ], false)
                        ->andWhere(['between', 'recognition_date', $dateFrom, $dateTo])
                        ->andWhere(['or',
                            ['and',
                                ['not', ['invoice.id' => null]],
                                ['invoice.has_act' => 0],
                                ['invoice.has_packing_list' => 0],
                                ['invoice.has_upd' => 0],
                            ],
                            ['and',
                                ['not', ['invoice.id' => null]],
                                ['or',
                                    ['invoice.need_act' => 0],
                                    ['invoice.need_packing_list' => 0],
                                    ['invoice.need_upd' => 0],
                                ]
                            ],
                        ])
                        ->asArray()
                        ->all();

                    foreach ($flows as $flow) {

                        $monthNumber = substr($flow['recognition_date'], 5, 2);

                        $result[$RESULT_KEY]['amount'][$monthNumber] += (int)$flow['sum'];
                        $parentItem['amount'][$monthNumber] += (int)$flow['sum'];

                        // amount taxable
                        if ($flow['is_accounting']) {
                            $isContractorAccounting = !in_array($flow['contractor_id'], self::$notAccountingContractors);
                            if ($isContractorAccounting || $flowClassName == CashBankFlows::class) {
                                $result[$RESULT_KEY]['amountTax'][$monthNumber] += (int)$flow['sum'];
                                $parentItem['amountTax'][$monthNumber] += (int)$flow['sum'];
                            }
                        }
                    }
                }
            }

            $result = ['totalVariableCosts' => $parentItem] + $result;
            $this->data['variableCosts' . $year] = $this->calculateQuarterAndTotalAmount($result);
        }

        return $this->data['variableCosts' . $year];
    }
}