<?php

namespace frontend\modules\analytics\models\profitAndLoss\old;

use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceIncomeItem;
use frontend\models\Documents;
use frontend\modules\analytics\models\AbstractFinance;
use yii\db\Expression;

trait RevenueTrait {

    /**
     * @param $parentItem
     * @param $year
     * @return mixed
     */
    public function getRevenue($parentItem, $year)
    {
        $result = [
            'revenueByDocs' => self::_getBlankItem('revenue', 'Выручка по документам', null, 'income-revenue_doc', self::MOVEMENT_TYPE_DOCS),
            'revenueByNoDocs' => self::_getBlankItem('revenue', 'Выручка без документов', null, 'income-revenue_flow', self::MOVEMENT_TYPE_FLOWS)
        ];

        if (!array_key_exists('revenue' . $year, $this->data)) {

            $dateFrom = "{$year}-01-01";
            $dateTo = "{$year}-12-31";

            //////////// BY DOC ////////////
            /// сумма всех Актов, ТН и УПД, дата которых в нужном месяце

            foreach (self::docClassArray() as $docsAlias => $docClassName) {
                $t = $docClassName::tableName();

                /** @var  $docClassName Act */
                $docData = Invoice::find()
                    ->joinWith($docsAlias, false)
                    ->select(["{$t}.id", "{$t}.document_date", 'invoice.contractor_id'])
                    ->andWhere(['invoice.company_id' => $this->company->id])
                    ->andWhere(['invoice.type' => Documents::IO_TYPE_OUT])
                    ->andWhere(['between', "{$t}.document_date", $dateFrom, $dateTo])
                    ->indexBy('id')
                    ->asArray()
                    ->all();

                $orderTable = AbstractFinance::$docOrdersTables[$docsAlias];

                $amount = $orderTable['class']::find()
                    ->leftJoin('order', "{$orderTable['name']}.order_id = `order`.id")
                    ->where([$orderTable['key'] => array_column($docData, 'id')])
                    ->select(new Expression("{$orderTable['key']}, SUM(`order`.selling_price_with_vat * {$orderTable['name']}.quantity) sum"))
                    ->groupBy($orderTable['key'])
                    ->indexBy($orderTable['key'])
                    ->asArray()
                    ->all();

                array_walk($docData, function(&$v, $k) use ($amount) { $v['sum'] = $amount[$k]['sum'] ?? 0; });

                foreach ($docData as $doc) {
                    $monthNumber = substr($doc['document_date'], 5, 2);
                    $result["revenueByDocs"]['amount'][$monthNumber] += $doc['sum'];
                    $parentItem['amount'][$monthNumber] += $doc['sum'];

                    // amount taxable
                    $isAccounting = !in_array($doc['contractor_id'], self::$notAccountingContractors);
                    if ($isAccounting) {
                        $result["revenueByDocs"]['amountTax'][$monthNumber] += $doc['sum'];
                        $parentItem['amountTax'][$monthNumber] += $doc['sum'];
                    }

                }
            }

            //////////// BY NO DOC ////////////
            ///  1) есть привязка к счетам, у которых НЕТ Актов/ТН/УПД или у них стоит "Без Акта/ТН/УПД" и "Дата признания дохода" в нужном месяце +
            ///  2) нет привязки к счетам и "Дата признания дохода" нужном месяце

            foreach (self::flowClassArray() as $flowClassName) {
                $flowTableName = $flowClassName::tableName();

                $flows = $flowClassName::find()
                    ->andWhere(["{$flowClassName::tableName()}.company_id" => $this->company->id])
                    ->byFlowType(CashFlowsBase::FLOW_TYPE_INCOME)
                    ->andWhere(['not', ['in', 'income_item_id', [
                        InvoiceIncomeItem::ITEM_STARTING_BALANCE,
                        InvoiceIncomeItem::ITEM_RETURN,
                        InvoiceIncomeItem::ITEM_BORROWING_REDEMPTION,
                        InvoiceIncomeItem::ITEM_BUDGET_RETURN,
                        InvoiceIncomeItem::ITEM_LOAN,
                        InvoiceIncomeItem::ITEM_CREDIT,
                        InvoiceIncomeItem::ITEM_ENSURE_PAYMENT,
                        InvoiceIncomeItem::ITEM_FROM_FOUNDER,
                        InvoiceIncomeItem::ITEM_OWN_FOUNDS,
                        InvoiceIncomeItem::ITEM_OTHER,
                        InvoiceIncomeItem::ITEM_RECEIVED_PERCENTS,
                        InvoiceIncomeItem::ITEM_RECEIVED_PERCENTS_BY_DEPOSITS
                    ]]])
                    ->select([
                        new Expression("DISTINCT({$flowTableName}.id)"),
                        "{$flowTableName}.income_item_id as item_id",
                        "{$flowTableName}.amount as sum",
                        "{$flowTableName}.recognition_date",
                        "{$flowTableName}.contractor_id",
                        $flowClassName == CashBankFlows::class ?
                            new Expression("1 AS is_accounting") : "{$flowTableName}.is_accounting"
                    ])
                    ->joinWith([
                        'invoices',
                    ], false)
                    ->andWhere(['between', 'recognition_date', $dateFrom, $dateTo])
                    ->andWhere(['or',
                        ['invoice.id' => null],
                        ['or',
                            ['and',
                                ['not', ['invoice.id' => null]],
                                ['invoice.has_act' => 0],
                                ['invoice.has_packing_list' => 0],
                                ['invoice.has_upd' => 0],
                            ],
                            ['and',
                                ['not', ['invoice.id' => null]],
                                ['or',
                                    ['invoice.need_act' => 0],
                                    ['invoice.need_packing_list' => 0],
                                    ['invoice.need_upd' => 0],
                                ]
                            ],
                        ]
                    ])
                    ->asArray()
                    ->all();

                foreach ($flows as $flow) {
                    $monthNumber = substr($flow['recognition_date'], 5, 2);
                    $result["revenueByNoDocs"]['amount'][$monthNumber] += $flow['sum'];
                    $parentItem['amount'][$monthNumber] += $flow['sum'];

                    // amount taxable
                    if ($flow['is_accounting']) {
                        $isContractorAccounting = !in_array($flow['contractor_id'], self::$notAccountingContractors);
                        if ($isContractorAccounting || $flowClassName == CashBankFlows::class) {
                            $result["revenueByNoDocs"]['amountTax'][$monthNumber] += $flow['sum'];
                            $parentItem['amountTax'][$monthNumber] += $flow['sum'];
                        }
                    }
                }
            }

            $result = ['totalRevenue' => $parentItem] + $result;
            $this->data['revenue' . $year] = $this->calculateQuarterAndTotalAmount($result);
        }

        return $this->data['revenue' . $year];
    }
}