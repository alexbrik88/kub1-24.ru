<?php

namespace frontend\modules\analytics\models\profitAndLoss\old;

use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceIncomeItem;
use yii\db\Expression;

trait AnotherIncomeTrait {

    /**
     * @param $parentItem
     * @param $year
     * @return mixed
     */
    public function getAnotherIncome($parentItem, $year)
    {
        $result = [];

        if (!array_key_exists('anotherIncome' . $year, $this->data)) {

            $dateFrom = "{$year}-01-01";
            $dateTo = "{$year}-12-31";

            $profitAndLossCompanyItems = InvoiceIncomeItem::find()
                ->where(['id' => InvoiceIncomeItem::ITEM_OTHER])
                ->indexBy(['id'])
                ->asArray()
                ->all();

            $ONLY_INCOME_ITEMS = array_keys($profitAndLossCompanyItems);

            foreach (self::flowClassArray() as $flowClassName) {
                $flowTableName = $flowClassName::tableName();

                $flows = $flowClassName::find()
                    ->andWhere(["{$flowClassName::tableName()}.company_id" => $this->company->id])
                    ->byFlowType(CashFlowsBase::FLOW_TYPE_INCOME)
                    ->andWhere(['income_item_id' => $ONLY_INCOME_ITEMS])
                    ->andWhere(['between', 'recognition_date', $dateFrom, $dateTo])
                    ->select([
                        new Expression("DISTINCT({$flowTableName}.id)"),
                        "{$flowTableName}.income_item_id as item_id",
                        "{$flowTableName}.amount as sum",
                        "{$flowTableName}.recognition_date",
                        "{$flowTableName}.contractor_id",
                        $flowClassName == CashBankFlows::class ?
                            new Expression("1 AS is_accounting") : "{$flowTableName}.is_accounting"
                    ])
                    ->asArray()
                    ->all();

                foreach ($flows as $flow) {
                    $monthNumber = substr($flow['recognition_date'], 5, 2);
                    $key = "income-{$flow['item_id']}";

                    if (!isset($result[$key])) {
                        $companyItem = $profitAndLossCompanyItems[$flow['item_id']];
                        $itemName = $companyItem['name'] ?? "item_id={$flow['item_id']}";

                        $result[$key] = self::_getBlankItem('another-income', $itemName, null, $key, self::MOVEMENT_TYPE_FLOWS);
                    }

                    $result[$key]['amount'][$monthNumber] += $flow['sum'];
                    $parentItem['amount'][$monthNumber] += $flow['sum'];

                    // amount taxable
                    if ($flow['is_accounting']) {
                        $isContractorAccounting = !in_array($flow['contractor_id'], self::$notAccountingContractors);
                        if ($isContractorAccounting || $flowClassName == CashBankFlows::class) {
                            $result[$key]['amountTax'][$monthNumber] += $flow['sum'];
                            $parentItem['amountTax'][$monthNumber] += $flow['sum'];
                        }
                    }
                }
            }

            $result = ['totalAnotherIncome' => $parentItem] + $result;
            $this->data['anotherIncome' . $year] = $this->calculateQuarterAndTotalAmount($result);
        }

        return $this->data['anotherIncome' . $year];


    }
}