<?php

namespace frontend\modules\analytics\models\profitAndLoss\old;

use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceExpenditureItem;
use yii\db\Expression;

trait PaymentPercentTrait {

    /**
     * @param $parentItem
     * @param $year
     * @return mixed
     */
    public function getPaymentPercent($parentItem, $year)
    {
        if (!array_key_exists('paymentPercent' . $year, $this->data)) {

            $dateFrom = "{$year}-01-01";
            $dateTo = "{$year}-12-31";

            foreach (self::flowClassArray() as $flowClassName) {

                $flowTableName = $flowClassName::tableName();

                $flows = $flowClassName::find()
                    ->andWhere(["{$flowClassName::tableName()}.company_id" => $this->company->id])
                    ->byFlowType(CashFlowsBase::FLOW_TYPE_EXPENSE)
                    ->andWhere(['expenditure_item_id' => InvoiceExpenditureItem::ITEM_PAYMENT_PERCENT])
                    ->select([
                        new Expression("DISTINCT({$flowTableName}.id)"),
                        "{$flowTableName}.expenditure_item_id as item_id",
                        "{$flowTableName}.amount as sum",
                        "{$flowTableName}.recognition_date",
                        "{$flowTableName}.contractor_id",
                        $flowClassName == CashBankFlows::class ?
                            new Expression("1 AS is_accounting") : "{$flowTableName}.is_accounting"
                    ])
                    ->andWhere(['between', 'recognition_date', $dateFrom, $dateTo])
                    ->asArray()
                    ->all();

                foreach ($flows as $flow) {
                    $monthNumber = substr($flow['recognition_date'], 5, 2);

                    $parentItem['amount'][$monthNumber] += $flow['sum'];

                    // amount taxable
                    if ($flow['is_accounting']) {
                        $isContractorAccounting = !in_array($flow['contractor_id'], self::$notAccountingContractors);
                        if ($isContractorAccounting || $flowClassName == CashBankFlows::class) {
                            $parentItem['amountTax'][$monthNumber] += $flow['sum'];
                        }
                    }
                }
            }
            $parentItem['data-article'] = 'expense-' . InvoiceExpenditureItem::ITEM_PAYMENT_PERCENT;
            $parentItem['data-movement-type'] = self::MOVEMENT_TYPE_FLOWS;
            $this->data['paymentPercent' . $year] = $this->calculateQuarterAndTotalAmount(['paymentPercent' => $parentItem]);
        }

        return $this->data['paymentPercent' . $year];
    }

}