<?php

namespace frontend\modules\analytics\models;

use common\components\date\DateHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\product\ProductGroup;
use common\models\product\ProductRecommendationAbc;
use frontend\models\Documents;
use frontend\modules\integration\models\vkAds\VkAdsAdReportSearch;
use frontend\modules\analytics\models\productAnalysis\TurnoverColumn;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class ProductAnalysisSellingSpeed
 * @package frontend\modules\analytics\models
 */
class ProductAnalysisSellingSpeed extends Model
{
    const GROUP_A = 1;
    const GROUP_B = 2;
    const GROUP_C = 3;

    public $id;
    public $title;
    public $article;
    public $group_id;

    protected $_company;
    protected $_year;
    protected $_query;

    public $dateStart;
    public $dateEnd;

    // extra data
    public $prevPeriodData;
    public $overallData;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'title', 'group_id', 'year'], 'safe'],
        ];
    }

    public function __construct($config)
    {
        parent::__construct($config);

        $this->year = Yii::$app->session->get('productAnalysis.ABC.ym', date('Ym'));
    }

    public function __destruct()
    {
        Yii::$app->session->set('productAnalysis.ABC.ym', $this->year);
    }

    public function search($params = [])
    {
        $this->load($params);

        $select = new Expression('
            product.id AS product_id,
            product.group_id,
            product.title,
            product.article,
            orderDoc.quantity,
            order.selling_price_with_vat,
            product.price_for_buy_with_nds,
            invoice.id AS invoice_id,
            product.created_at AS first_date
        ');

        $where = [
            'and',
            ['invoice.company_id' => $this->_company->id],
            ['invoice.type' => Documents::IO_TYPE_OUT],
            ['between', 'document.document_date', $this->dateStart->format('Y-m-d'), $this->dateEnd->format('Y-m-d')],
        ];


        // OVERALL RESULT
        $this->overallData = (new Query)
            ->select(['turnover' => 'SUM(selling_price_with_vat * quantity) - SUM([[price_for_buy_with_nds]] * quantity)'])
            ->from(['t' => TurnoverColumn::query($select, $where)])
            ->groupBy('product_id')
            ->all(Yii::$app->db2);

        $overallMargin = array_sum(array_column($this->overallData, 'turnover'));
        /////////////////

        $selectMain = [
            'group_id',
            'product_id',
            'title',
            'article',
            'turnover' => 'SUM(selling_price_with_vat * quantity)',
            'selling_price' => 'SUM(selling_price_with_vat * quantity) / SUM(quantity)',
            'cost_price' => 'price_for_buy_with_nds * SUM(quantity)',
            'purchase_price' => '(price_for_buy_with_nds * SUM(quantity)) / SUM(quantity)',
            'margin' => 'SUM(selling_price_with_vat * quantity) - SUM([[price_for_buy_with_nds]] * quantity)',
            'margin_percent' => '(SUM(selling_price_with_vat * quantity) - SUM([[price_for_buy_with_nds]] * quantity)) / SUM(selling_price_with_vat * quantity)',
            'selling_quantity' => 'SUM(quantity)',
            'group_margin_percent' => '(SUM(selling_price_with_vat * quantity) - SUM([[price_for_buy_with_nds]] * quantity)) / ' . ($overallMargin > 0 ? $overallMargin : 9E9),
            'number_of_sales' => 'COUNT(DISTINCT(invoice_id))',
            'average_check' => 'SUM(selling_price_with_vat * quantity) / COUNT(DISTINCT(invoice_id))',
            'first_date' => 'first_date'
        ];

        /// ABC /////////////
        $groupA = 1;
        $groupB = 2;
        $groupC = 3;

        $tmpTableMarginABC_1 = 'tmp_analysis_margin_abc_1';
        $tmpTableMarginABC_2 = 'tmp_analysis_margin_abc_2';
        $tmpTableMarginABC_3 = 'tmp_analysis_margin_abc_3';

        $abcQuery = (new Query)
            ->select([
                'pid' => 'product_id',
                'margin' => 'SUM(selling_price_with_vat * quantity) - SUM([[price_for_buy_with_nds]] * quantity)',
                'margin_percent' => '100 * (SUM(selling_price_with_vat * quantity) - SUM([[price_for_buy_with_nds]] * quantity)) / SUM(selling_price_with_vat * quantity)',
                'selling_quantity' => 'SUM(quantity)',
            ])
            ->from(['t' => TurnoverColumn::query($select, $where)])
            ->groupBy('product_id');

        $totalCount = (clone ($abcQuery))->count('pid', Yii::$app->db2) ?: 9E9;
        $thresholdA = ceil(20/100 * $totalCount);
        $thresholdB = ceil(30/100 * $totalCount);
        $thresholdC = $totalCount - $thresholdA - $thresholdB;

        $abcQuery1 = clone $abcQuery;
        $abcQuery1->addSelect(['abc' => "IF (ROW_NUMBER() OVER ( ORDER BY margin DESC ) <= {$thresholdA}, {$groupA}, IF (ROW_NUMBER() OVER ( ORDER BY margin DESC ) <= {$thresholdB}, {$groupB}, {$groupC}))"]);
        //$abcQuery1->orderBy(['margin' => SORT_DESC]);

        $abcQuery2 = clone $abcQuery;
        $abcQuery2->addSelect(['abc' => "IF (ROW_NUMBER() OVER ( ORDER BY margin_percent DESC ) <= {$thresholdA}, {$groupA}, IF (ROW_NUMBER() OVER ( ORDER BY margin_percent DESC ) <= {$thresholdB}, {$groupB}, {$groupC}))"]);
        //$abcQuery2->orderBy(['margin_percent' => SORT_DESC]);

        $abcQuery3 = clone $abcQuery;
        $abcQuery3->addSelect(['abc' => "IF (ROW_NUMBER() OVER ( ORDER BY selling_quantity DESC ) <= {$thresholdA}, {$groupA}, IF (ROW_NUMBER() OVER ( ORDER BY selling_quantity DESC ) <= {$thresholdB}, {$groupB}, {$groupC}))"]);
        //$abcQuery3->orderBy(['selling_quantity' => SORT_DESC]);

        Yii::$app->db2->createCommand(" CREATE TEMPORARY TABLE IF NOT EXISTS {{{$tmpTableMarginABC_1}}} AS ({$abcQuery1->createCommand()->rawSql}) ")->execute();
        Yii::$app->db2->createCommand(" CREATE TEMPORARY TABLE IF NOT EXISTS {{{$tmpTableMarginABC_2}}} AS ({$abcQuery2->createCommand()->rawSql}) ")->execute();
        Yii::$app->db2->createCommand(" CREATE TEMPORARY TABLE IF NOT EXISTS {{{$tmpTableMarginABC_3}}} AS ({$abcQuery3->createCommand()->rawSql}) ")->execute();

        // ABC DEBUG
        //var_dump((new Query())->select('*')->from($tmpTableMarginABC_1)->all());
        //var_dump((new Query())->select('*')->from($tmpTableMarginABC_2)->all());
        //var_dump((new Query())->select('*')->from($tmpTableMarginABC_3)->all());
        /////////////////////

        /// XYZ ////////////
        $tmpTableXYZ = 'tmp_analysis_xyz';
        $months = self::_getFromCurrentMonthsPeriods(12, 0, $this->dateEnd->format('Y') - date('Y'));
        $dateStartXYZ = self::_getDateFromYM($months[0]);
        $dateEndXYZ = self::_getDateFromYM($months[count($months)-1], true);

        $xyzQuery = (new Query())
            ->select(new Expression('
                order.product_id AS pid,
                DATE_FORMAT(document_date, "%Y%m") AS period,
                SUM(quantity) AS q,
                "0.00000000000000000000000000000000" AS avg_q,
                0 AS cnt_q,
                0 AS total_cnt_q,
                '.count($months).' AS main_select_cnt_q
                '))
            ->from('invoice')
            ->leftJoin('order', 'invoice.id = order.invoice_id')
            ->andWhere([
                'invoice.company_id' => $this->company->id,
                'invoice.type' => Documents::IO_TYPE_OUT,
                'invoice.is_deleted' => false,
                'invoice.invoice_status_id' => [InvoiceStatus::STATUS_PAYED_PARTIAL, InvoiceStatus::STATUS_PAYED],
            ])
            ->andWhere([
                'between',
                'invoice.document_date',
                $dateStartXYZ,
                $dateEndXYZ,
            ])
            ->groupBy(['pid', 'DATE_FORMAT(document_date, "%Y%m")']);

        Yii::$app->db2->createCommand("CREATE TEMPORARY TABLE IF NOT EXISTS {{{$tmpTableXYZ}}} AS ({$xyzQuery->createCommand()->rawSql})")->execute();
        Yii::$app->db2->createCommand("UPDATE {$tmpTableXYZ} x SET
            x.avg_q = (SELECT SUM(y.q) / (1 + PERIOD_DIFF({$months[count($months)-1]}, MIN(period))) FROM {$tmpTableXYZ} y WHERE x.pid = y.pid GROUP BY y.pid),
            x.cnt_q = (SELECT (1 + PERIOD_DIFF(MAX(period), MIN(period))) FROM {$tmpTableXYZ} z WHERE x.pid = z.pid GROUP BY z.pid),
            x.total_cnt_q = (SELECT (1 + PERIOD_DIFF({$months[count($months)-1]}, MIN(period))) FROM {$tmpTableXYZ} z WHERE x.pid = z.pid GROUP BY z.pid)
        ")->execute();

        // XYZ DEBUG (продажи с середины периода с месяцами без продаж)
        //var_dump((new Query())->select('*')->from($tmpTableXYZ)->all());exit;
        //$query = (new Query())
        //    ->select(new Expression("
        //    pid,
        //    avg_q,
        //    cnt_q,
        //    total_cnt_q,
        //    100 * SQRT((total_cnt_q - cnt_q) / (main_select_cnt_q) * POW(0 - avg_q, 2) + SUM(POW(q - avg_q, 2)) / (cnt_q - 1)) / avg_q AS variation
        //    "))->from($tmpTableXYZ)->where(['<>', 'period', date('Ym')])->groupBy('pid');
        //var_dump($query->groupBy('pid')->all());
        //exit;
        /////////////////////

        $query = (new Query)
            ->select($selectMain)
            ->addSelect(['variation_coefficient' => new Expression("
                (SELECT 100 * SQRT((total_cnt_q - cnt_q) / main_select_cnt_q * POW(0 - avg_q, 2) + SUM(POW(q - avg_q, 2)) / (cnt_q - 1)) / avg_q FROM {$tmpTableXYZ} coef WHERE coef.pid = t.product_id)
            ")])
            ->addSelect(['abc' => new Expression('CONCAT(abc1.abc, abc2.abc, abc3.abc)')])
            ->from(['t' => TurnoverColumn::query($select, $where)])
            ->leftJoin(['abc1' => $tmpTableMarginABC_1], 'abc1.pid = t.product_id')
            ->leftJoin(['abc2' => $tmpTableMarginABC_2], 'abc2.pid = t.product_id')
            ->leftJoin(['abc3' => $tmpTableMarginABC_3], 'abc3.pid = t.product_id')
            ->groupBy('product_id')
            ->indexBy('product_id');

        $query->andFilterWhere(['product.id' => $this->id]);

        if ($this->title) {
            $query->andWhere(['or', ['like', 'title', $this->title], ['like', 'article', $this->title]]);
        }

        $this->_query = clone $query;

        // PREV PERIOD
        $prevDateStart = (clone $this->dateStart)->modify("-1 month")->format('Y-m-d');
        $prevDateEnd = (clone $this->dateStart)->modify("-1 month")->modify("last day of")->format('Y-m-d');
        $prevWhere = [
            'and',
            ['invoice.company_id' => $this->_company->id],
            ['invoice.type' => Documents::IO_TYPE_OUT],
            ['between', 'document.document_date', $prevDateStart, $prevDateEnd],
        ];
        $prevQuery = (new Query)
            ->select($selectMain)
            ->from(['t' => TurnoverColumn::query($select, $prevWhere)])
            ->groupBy('product_id')
            ->indexBy('product_id');
        $prevQuery->andFilterWhere(['product.id' => $this->id]);
        if ($this->title) {
            $prevQuery->andWhere(['or', ['like', 'title', $this->title], ['like', 'article', $this->title]]);
        }
        $this->prevPeriodData = $prevQuery->all(Yii::$app->db2);
        //////////////

        $dataProvider = new ActiveDataProvider([
            'db' => \Yii::$app->db2,
            'query' => $query,
            'pagination' => [
                'pageSize' => 0,
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'title',
                'turnover',
                'selling_price',
                'cost_price',
                'purchase_price',
                'margin',
                'margin_percent',
                'selling_quantity',
                'group_margin_percent',
                'number_of_sales',
                'average_check',
                'variation_coefficient',
                'abc'
            ],
            'defaultOrder' => [
                'abc' => SORT_ASC,
            ],
        ]);

        return $dataProvider;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->_company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    public function setYear($value)
    {
        if (strlen($value) != 6 || !is_numeric($value)) {
            $value = date('Ym');
        }

        $this->_year = $value;

        $year = substr($value, 0, 4);
        $month = substr($value, -2, 2);

        $this->dateStart = date_create_from_format('Y-m-d', "{$year}-{$month}-01");
        $this->dateEnd = date_create_from_format('Y-m-d', "{$year}-{$month}-" . cal_days_in_month(CAL_GREGORIAN, $month, $year));
    }

    public function getYear()
    {
        return $this->_year;
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $range = [];
        $invoiceMinDate = Invoice::find()
            ->byCompany($company->id)
            ->min('document_date');

        $minDates = [];

        if ($invoiceMinDate) $minDates[] = $invoiceMinDate;

        $minCashDate = !empty($minDates) ? min($minDates) : date('Y-m-d');
        $registrationDate = date_create_from_format('Y-m-d', $minCashDate);
        $currentDate = (clone $registrationDate)->modify('first day of this month');

        while ($currentDate->format('Ym') <= date('Ym')) {
            $range[$currentDate->format('Ym')] = ArrayHelper::getValue(AbstractFinance::$month, substr($currentDate->format('Ym'), -2, 2)) . ' ' . substr($currentDate->format('Ym'), 0, 4);
            $currentDate = $currentDate->modify('last day of this month')->setTime(23, 59, 59);
            $currentDate = $currentDate->modify("+1 second");
        }
        $range = array_reverse($range, true);

        return $range;
    }

    ////////////////////////////////////////////// HELPERS ////////////////////////////////////////////////////////////

    private static function _getFromCurrentMonthsPeriods($left, $right, $offsetYear = 0)
    {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month");

        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {

            if ($curr->format('Ym') < date('Ym'))
                $ret[] = $curr->format('Ym');

            $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }

    public static function _getDateFromYM($ym, $endOfMonth = false)
    {
        if (strlen($ym) != 6 || $ym != (int)$ym)
            throw new Exception('_getDateFromYM: $ym not found');

        $year = substr($ym, 0, 4);
        $month = substr($ym, -2, 2);
        $day = ($endOfMonth) ? cal_days_in_month(CAL_GREGORIAN, $month, $year) : "01";

        return "{$year}-{$month}-{$day}";
    }

    public static function getFluidColspan($userConfig, $columnGroup = 'cell-data')
    {
        if ($columnGroup == 'cell-data') {
            return 10 - (
                !$userConfig->report_abc_cost_price +
                !$userConfig->report_abc_group_margin +
                !$userConfig->report_abc_number_of_sales +
                !$userConfig->report_abc_average_check +
                !$userConfig->report_abc_purchase_price +
                !$userConfig->report_abc_selling_price
            );
        }

        return 0;
    }

    public function getGroupsByProducts($products, $calcCoefs = false)
    {
        // fill groups
        $groups = [];
        foreach ($products as $key => $product) {
            //if ($product['group_id'] == 1) { // Без группы
            //
            //    $groups['product_' . $product['product_id']] = $product;
            //
            //} else {
                if (!isset($groups['group_' . $product['group_id']])) {

                    $modelGroup = ProductGroup::findOne($product['group_id']);

                    $groups['group_' . $product['group_id']] = [
                        'group_id' => null,
                        'product_id' => null,
                        'title' => $modelGroup ? $modelGroup->title : '---',
                        'article' => null,
                        'turnover' => 0,
                        'selling_price' => 0,
                        'cost_price' => 0,
                        'purchase_price' => 0,
                        'margin' => 0,
                        'margin_percent' => 0,
                        'selling_quantity' => 0,
                        'group_margin_percent' => 0,
                        'number_of_sales' => 0,
                        'average_check' => 0,
                        'variation_coefficient' => 0,
                        'abc' => '',
                        'first_date' => null,
                        'items' => []
                    ];
                }

                $groups['group_' . $product['group_id']]['items'][] = $product;
            //}
        }

        // calculate totals by group: SUM()
        foreach ($groups as $key => $group) {
            if (isset($group['items'])) {
                foreach(['turnover', 'cost_price', 'margin', 'selling_quantity', 'number_of_sales', 'balance_at_start', 'balance_at_end'] as $attr) {
                    $groups[$key][$attr] = array_sum(array_column($group['items'], $attr));
                }
            }
        }

        // calculate totals by group: AVG()
        $overallMargin = array_sum(array_column($this->overallData, 'turnover'));
        foreach ($groups as $key => $group) {
            if (isset($group['items'])) {
                $groups[$key]['selling_price'] = $group['turnover'] / ($group['selling_quantity'] ?: 9E9);
                $groups[$key]['purchase_price'] = $group['cost_price'] / ($group['selling_quantity'] ?: 9E9);
                $groups[$key]['margin_percent'] = $group['margin'] / ($group['turnover'] ?: 9E9);
                $groups[$key]['group_margin_percent'] = $group['margin'] / ($overallMargin ?: 9E9);
                $groups[$key]['average_check'] = $group['turnover'] / ($group['number_of_sales'] ?: 9E9);
                $groups[$key]['variation_coefficient'] = array_sum(array_column($group['items'], 'variation_coefficient')) / count($group['items']);

            }
        }

        if (!$calcCoefs)
            return $groups;

        // calculate ABC
        $totalCount = count($groups);
        $thresholdA = ceil(20/100 * $totalCount);
        $thresholdB = ceil(30/100 * $totalCount);
        $thresholdC = $totalCount - $thresholdA - $thresholdB;

        foreach(['margin', 'margin_percent', 'selling_quantity'] as $attr) {
            $i = 0;
            uasort($groups, function ($a, $b) use ($attr) {
                return $b[$attr] <=> $a[$attr];
            });
            foreach ($groups as $key => &$group) {
                $i++;
                if (empty($group['items']))
                    continue;

                if ($i <= $thresholdA)
                    $group['abc'] .= self::GROUP_A;
                elseif ($i <= $thresholdB)
                    $group['abc'] .= self::GROUP_B;
                else
                    $group['abc'] .= self::GROUP_C;
            }
        }

        // sort
        $sort = Yii::$app->request->get('sort', 'abc');
        if (strlen($sort) != strlen(trim($sort, '-'))) {
            // desc
            $sort = trim($sort, '-');
            uasort($groups, function ($a, $b) use ($sort) {
                return $b[$sort] <=> $a[$sort];
            });
        } else {
            // asc
            uasort($groups, function ($a, $b) use ($sort) {
                return $a[$sort] <=> $b[$sort];
            });
        }

        return $groups;
    }

    public static function getRecommendations($abc, $variationCoef)
    {
        $key = str_replace([1,2,3], ['A', 'B', 'C'], $abc);
        $key .= ($variationCoef <= 10) ? 'X' : ($variationCoef > 25 ? 'Z' : 'Y');

        if (strlen($key) == 4 && strlen(str_replace(['A','B','C','X','Y','Z'], '', $key)) == 0) {

            return ProductRecommendationAbc::find()->where(['key' => $key])->asArray()->one();
        }

        return [];
    }
}
