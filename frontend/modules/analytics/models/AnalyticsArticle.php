<?php

namespace frontend\modules\analytics\models;

use common\components\helpers\ArrayHelper;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\Company;
use Yii;

/**
 * This is the model class for table "analytics_article".
 *
 * @property int $company_id
 * @property int $type
 * @property int $item_id
 * @property int $profit_and_loss_type
 *
 * @property Company $company
 * @property InvoiceIncomeItem $invoiceIncomeItem
 * @property InvoiceExpenditureItem $invoiceExpenditureItem
 * @property AnalyticsArticle[] $children
 * @property AnalyticsArticle $parent
 */
class AnalyticsArticle extends \yii\db\ActiveRecord
{
    const FLOW_TYPE_INCOME  = 1; // CashFlowsBase::FLOW_TYPE_INCOME;
    const FLOW_TYPE_EXPENSE = 0; // CashFlowsBase::FLOW_TYPE_EXPENSE;

    const PAL_UNSET = 0; // Не установлено

    const PAL_EXPENSES_VARIABLE = 10; // Переменные расходы
    const PAL_EXPENSES_FIXED = 20; // Постоянные расходы
    const PAL_EXPENSES_OPERATING = 30; // not used (Операционные расходы)
    const PAL_EXPENSES_OTHER = 40; // Другие расходы
    const PAL_EXPENSES_PERCENT = 50; // Проценты уплаченные
    const PAL_EXPENSES_DIVIDEND = 60; // Дивиденды
    const PAL_EXPENSES_PRIME_COST = 70; // Себестоимость
    const PAL_EXPENSES_AMORTIZATION = 80; // Амортизация

    const PAL_INCOME_REVENUE = 11; // Выручка
    const PAL_INCOME_OTHER = 41; // Другие доходы
    const PAL_INCOME_PERCENT = 51; // Проценты полученные

    const UNSET_ALWAYS_ARTICLES = [
        self::FLOW_TYPE_INCOME => [
            InvoiceIncomeItem::ITEM_STARTING_BALANCE, // Баланс начальный
            InvoiceIncomeItem::ITEM_INPUT_MONEY, // Ввод денег
            InvoiceIncomeItem::ITEM_CASH_PAYMENT, // Взнос наличными
            InvoiceIncomeItem::ITEM_FROM_FOUNDER,  // Взнос от учредителя
            InvoiceIncomeItem::ITEM_BORROWING_REDEMPTION, // Возврат займа
            InvoiceIncomeItem::ITEM_BORROWING_DEPOSIT, // Возврат депозита
            InvoiceIncomeItem::ITEM_LOAN, // Займ
            InvoiceIncomeItem::ITEM_CREDIT, // Кредит
            InvoiceIncomeItem::ITEM_CURRENCY_CONVERSION, // Конвертация валюты
            InvoiceIncomeItem::ITEM_ENSURE_PAYMENT, // Обеспечительный платеж
            InvoiceIncomeItem::ITEM_CARD_REPLENISHMENT, // Пополнение карты
            InvoiceIncomeItem::ITEM_OWN_FOUNDS, // Перевод между счетами
        ],
        self::FLOW_TYPE_EXPENSE => [
            InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT, // Оплата поставщику
            InvoiceExpenditureItem::ITEM_RETURN_FOUNDER, // Возврат учредителю
            InvoiceExpenditureItem::ITEM_BORROWING_EXTRADITION, // Выдача займа
            InvoiceExpenditureItem::ITEM_DEPOSIT, // Депозит
            InvoiceExpenditureItem::ITEM_CURRENCY_CONVERSION, // Конвертация валюты
            InvoiceExpenditureItem::ITEM_ENSURE_PAYMENT, // Обеспечительный платеж
            InvoiceExpenditureItem::ITEM_OWN_FOUNDS, // Перевод между счетами
            InvoiceExpenditureItem::ITEM_LOAN_REPAYMENT, // Погашение займа
            InvoiceExpenditureItem::ITEM_REPAYMENT_CREDIT, // Погашение кредита
            InvoiceExpenditureItem::ITEM_PRIME_COST_PRODUCT, // Себестоимость товара
            InvoiceExpenditureItem::ITEM_PRIME_COST_SERVICE, // Себестоимость услуг
        ]
    ];

    const UNSET_ALWAYS_ARTICLES_HELP = [
        self::FLOW_TYPE_INCOME => [],
        self::FLOW_TYPE_EXPENSE => [
            InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT => 'Считаются только товары "Не для продажи"<br/> и Услуги от Поставщика'
        ]
    ];

    protected static $_data = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'analytics_article';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'type', 'item_id'], 'required'],
            [['company_id', 'type', 'item_id', 'profit_and_loss_type'], 'integer'],
            [['company_id', 'type', 'item_id'], 'unique', 'targetAttribute' => ['company_id', 'type', 'item_id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['profit_and_loss_type'], 'filter', 'filter' => function ($value) {
                return (int)$value;
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'type' => 'Type',
            'item_id' => 'Item ID',
            'profit_and_loss_type' => 'Profit And Loss Type',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {

            if ($this->parent) {
                $this->updateAttributes([
                    'profit_and_loss_type' => $this->parent->profit_and_loss_type
                ]);
            }

        } else {

            foreach ($this->children as $child) {
                $child->updateAttributes([
                    'profit_and_loss_type' => $this->profit_and_loss_type
                ]);
            }
        }
    }

    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    public function getInvoiceIncomeItem()
    {
        return $this->hasOne(InvoiceIncomeItem::class, ['id' => 'item_id']);
    }

    public function getInvoiceExpenditureItem()
    {
        return $this->hasOne(InvoiceExpenditureItem::class, ['id' => 'item_id']);
    }

    public function getChildren()
    {
        if ($this->type == self::FLOW_TYPE_INCOME) {
            $childrenIds = ($this->invoiceIncomeItem) ?
                ArrayHelper::getColumn($this->invoiceIncomeItem->children, 'id') : [];
        } elseif (($this->type == self::FLOW_TYPE_EXPENSE)) {
            $childrenIds = ($this->invoiceExpenditureItem) ?
                ArrayHelper::getColumn($this->invoiceExpenditureItem->children, 'id') : [];
        } else {
            $childrenIds = [];
        }

        if ($childrenIds) {
            return self::find()->where([
                'company_id' => $this->company_id,
                'type' => $this->type,
                'item_id' => $childrenIds
            ])->all();
        }

        return [];
    }

    public function getParent()
    {
        if ($this->type == self::FLOW_TYPE_INCOME) {
            $parentId = ($this->invoiceIncomeItem) ?
                $this->invoiceIncomeItem->parent_id : null;
        } elseif (($this->type == self::FLOW_TYPE_EXPENSE)) {
            $parentId = ($this->invoiceExpenditureItem) ?
                $this->invoiceExpenditureItem->parent_id : null;
        } else {
            $parentId = null;
        }

        if ($parentId) {
            return self::find()->where([
                'company_id' => $this->company_id,
                'type' => $this->type,
                'item_id' => $parentId
            ])->one();
        }

        return null;
    }

    public static function getArticlesBySection($companyId, $type, $palSection)
    {
        $key = \yii\helpers\Json::encode([$companyId, $type, $palSection]);
        if (!isset(self::$_data['getArticlesBySection'][$key])) {
            self::$_data['getArticlesBySection'][$key] = self::find()
                ->select('item_id')
                ->distinct()
                ->where([
                    'type' => $type,
                    'company_id' => $companyId,
                    'profit_and_loss_type' => $palSection
                ])
                ->column();
            }

        return self::$_data['getArticlesBySection'][$key];
    }
}
