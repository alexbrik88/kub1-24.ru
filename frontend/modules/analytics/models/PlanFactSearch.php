<?php

namespace frontend\modules\analytics\models;

use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use frontend\modules\cash\models\CashContractorType;
use Yii;
use common\components\excel\Excel;
use common\components\helpers\ArrayHelper;
use common\models\company\CheckingAccountant;
use yii\db\Expression;

/**
 * Class PlanFactSearch
 * @package frontend\modules\analytics\models
 */
class PlanFactSearch extends OddsSearch
{
    /**
     *
     */
    const TAB_BY_ACTIVITY = 5;
    const TAB_BY_PURSE = 6;
    const TAB_BY_PURSE_DETAILED = "6d";
    const TAB_BY_INDUSTRY = "6i";
    const TAB_BY_SALE_POINT = "6s";

    /**
     * @param $type
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function search($type, $params = [])
    {
        if ($type == self::TAB_BY_INDUSTRY) {
            return $this->searchByFlowAttribute($params + ['attr' => 'industry_id']);
        } elseif ($type == self::TAB_BY_SALE_POINT) {
            return $this->searchByFlowAttribute($params + ['attr' => 'sale_point_id']);
        } elseif ($type == self::TAB_BY_PURSE_DETAILED) {
            return $this->searchByPurseDetailed($params);
        } elseif ($type == self::TAB_BY_PURSE) {
            return $this->searchByPurse($params);
        } elseif ($type == self::TAB_BY_ACTIVITY) {
            return $this->searchByActivity($params);
        }

        return $this->searchByActivity($params);
    }

    public function searchPlan($type, $params = [])
    {
        if ($type == self::TAB_BY_INDUSTRY) {
            return $this->searchByFlowAttributePlan($params + ['attr' => 'industry_id']);
        } elseif ($type == self::TAB_BY_SALE_POINT) {
            return $this->searchByFlowAttributePlan($params + ['attr' => 'sale_point_id']);
        } elseif ($type == self::TAB_BY_PURSE_DETAILED) {
            return $this->searchByPurseDetailedPlan($params);
        } elseif ($type == self::TAB_BY_PURSE) {
            return $this->searchByPursePlan($params);
        } elseif ($type == self::TAB_BY_ACTIVITY) {
            return $this->searchByActivityPlan($params);
        }

        return $this->searchByActivityPlan($params);
    }

    public function searchByActivityPlan($params = [])
    {
        return $this->searchByActivity($params + [
            'rawDataDays' => $this->getByActivityRawDataPlan($params)
        ]);
    }

    public function searchByPursePlan($params = [])
    {
        return $this->searchByPurse($params + [
            'rawDataDays' => $this->getByPurseRawDataPlan($params)
        ]);
    }

    public function searchByPurseDetailedPlan($params = [])
    {
        return $this->searchByPurseDetailed($params + [
            'rawData' => $this->getByPurseRawDataDetailedPlan(),
        ]);
    }

    public function searchByFlowAttributePlan($params)
    {
        return $this->searchByFlowAttribute($params + [
            'rawData' => $this->getByFlowAttributeRawDataPlan($params),
        ]);
    }

    public function getByFlowAttributeRawDataPlan($params)
    {
        $table = PlanCashFlows::tableName();
        $companyIds = implode(',', $this->_multiCompanyIds);
        $year = $this->year;
        $seq = 'seq_0_to_11';
        $interval = "('{$year}-01-31' + INTERVAL (seq) MONTH)";
        $AND_FILTERS = $this->getSqlFilters($params);
        $AND_HIDE_TIN_PARENT = '';
        $GROUP_BY = $params['attr'] ?? 'wallet';

        $query = "
            SELECT
              t.payment_type as wallet,
              IF(t.flow_type = 1, t.income_item_id, t.expenditure_item_id) AS item_id,
              t.flow_type AS type,
              t.contractor_id,
              IFNULL(t.sale_point_id, 0) sale_point_id,
              IFNULL(t.industry_id, 0) industry_id, 
              DATE_FORMAT({$interval}, '%m') m,
              DATE_FORMAT({$interval}, '%d') d,
            SUM(IF(DATE_FORMAT(t.date, '%Y%m') = DATE_FORMAT({$interval}, '%Y%m'), t.amount, 0)) amount,
            SUM(t.amount) growing_amount
            FROM {$seq}
            JOIN {$table} t ON
              t.company_id IN ({$companyIds})
              AND t.date <= {$interval}
              {$AND_FILTERS}
              {$AND_HIDE_TIN_PARENT}
            GROUP BY {$GROUP_BY}, type, contractor_id, item_id, m
          ";

        return Yii::$app->db2->createCommand($query)->queryAll();
    }

    public function getByActivityRawDataPlan($params = [])
    {
        $table = PlanCashFlows::tableName();
        $companyIds = implode(',', $this->_multiCompanyIds);
        $year = $this->year;
        $AND_FILTERS = $this->getSqlFilters($params);
        $AND_HIDE_TIN_PARENT = '';

        $sashFlows = "
            SELECT
                'RUB' AS currency_name,
                IF(t.flow_type = 1, t.income_item_id, t.expenditure_item_id) AS item_id,
                t.flow_type AS type,
                DATE_FORMAT(t.date, '%m') m,
                DATE_FORMAT(t.date, '%d') d,
                SUM(amount) amount
            FROM {$table} t
            WHERE
                t.company_id IN ({$companyIds})
                AND t.date BETWEEN '{$year}-01-01' AND '{$year}-12-31'
                {$AND_FILTERS}
                {$AND_HIDE_TIN_PARENT}
            GROUP BY currency_name, type, item_id, m, d
        ";

        $cashStart = "
            SELECT
                'RUB' AS currency_name,
                IF(t.flow_type = 1, t.income_item_id, t.expenditure_item_id) AS item_id,
                t.flow_type AS type,
                SUM(IF(t.flow_type = 1, t.amount, -t.amount)) amount
            FROM {$table} t
            WHERE
                t.company_id IN ({$companyIds})
                AND t.date < '{$year}-01-01'
                {$AND_FILTERS}
            GROUP BY currency_name, type, item_id
        ";

        return [
            'sashFlows' => Yii::$app->db2->createCommand($sashFlows)->queryAll(),
            'cashStart' => Yii::$app->db2->createCommand($cashStart)->queryAll(),
        ];
    }

    public function getByPurseRawDataPlan($params = [])
    {
        $table = PlanCashFlows::tableName();
        $companyIds = implode(',', $this->_multiCompanyIds);
        $year = $this->year;
        $AND_FILTERS = $this->getSqlFilters($params);
        $AND_HIDE_TIN_PARENT = '';

        $sashFlows = "
            SELECT
              t.payment_type as w,
              'RUB' AS currency_name,
              IF(t.flow_type = 1, t.income_item_id, t.expenditure_item_id) AS item_id,
              t.flow_type AS type,
              t.contractor_id,
              DATE_FORMAT(t.date, '%m') m,
              DATE_FORMAT(t.date, '%d') d,
              SUM(amount) amount
            FROM {$table} t
            WHERE
              t.company_id IN ({$companyIds})
              AND t.date BETWEEN '{$year}-01-01' AND '{$year}-12-31'
              {$AND_FILTERS}
              {$AND_HIDE_TIN_PARENT}
            GROUP BY w, currency_name, type, contractor_id, item_id, m, d
          ";

        $cashStart = "
            SELECT
              t.payment_type as w,
              'RUB' AS currency_name,
              SUM(IF(t.flow_type = 1, t.amount, -t.amount)) amount            
            FROM {$table} t
            WHERE
              t.company_id IN ({$companyIds})
              AND t.date < '{$year}-01-01'
              {$AND_FILTERS}
            GROUP BY w, currency_name
        ";

        return [
            'sashFlows' => Yii::$app->db2->createCommand($sashFlows)->queryAll(),
            'cashStart' => Yii::$app->db2->createCommand($cashStart)->queryAll(),
        ];

//        $seq = 'seq_0_to_11';
//        $interval = "('{$year}-01-31' + INTERVAL (seq) MONTH)";
//        $query = "
//            SELECT
//              t.payment_type AS wallet,
//              t.flow_type AS type,
//              IF (t.flow_type = 0, t.expenditure_item_id, t.income_item_id) AS item_id,
//              t.contractor_id,
//              DATE_FORMAT({$interval}, '%m') m,
//              DATE_FORMAT({$interval}, '%d') d,
//              SUM(IF(DATE_FORMAT(t.date, '%Y%m') = DATE_FORMAT({$interval}, '%Y%m'), t.amount, 0)) amount,
//              SUM(t.amount) growing_amount
//            FROM {$seq}
//            JOIN {$table} t ON
//              t.company_id IN ({$companyIds})
//              AND t.date <= {$interval}
//            GROUP BY `wallet`, `type`, `contractor_id`, `item_id`, `m`
//          ";
//
//        return Yii::$app->db2->createCommand($query)->queryAll();
    }

    public function getByPurseRawDataDetailedPlan()
    {
        $table = PlanCashFlows::tableName();
        $rsTable = CheckingAccountant::tableName();
        $companyIds = implode(',', $this->_multiCompanyIds);
        $year = $this->year;
        $seq = 'seq_0_to_11';
        $interval = "('{$year}-01-31' + INTERVAL (seq) MONTH)";
        $query = "
            SELECT
              t.payment_type AS wallet,
              CASE 
                WHEN t.payment_type = 1 THEN rs.rs
                WHEN t.payment_type = 2 THEN t.cashbox_id
                WHEN t.payment_type = 3 THEN t.emoney_id
              END AS wallet_id,  
              t.flow_type AS type,
              IF (t.flow_type = 0, t.expenditure_item_id, t.income_item_id) AS item_id,
              t.contractor_id,
              DATE_FORMAT({$interval}, '%m') m,
              DATE_FORMAT({$interval}, '%d') d,
              SUM(IF(DATE_FORMAT(t.date, '%Y%m') = DATE_FORMAT({$interval}, '%Y%m'), t.amount, 0)) amount,
              SUM(t.amount) growing_amount
            FROM {$seq}
            JOIN {$table} t ON 
              t.company_id IN ({$companyIds})
              AND t.date <= {$interval}
            LEFT JOIN {$rsTable} rs ON rs.id = t.checking_accountant_id 
            GROUP BY `wallet_id`, `type`, `contractor_id`, `item_id`, `m`
          ";

        return Yii::$app->db2->createCommand($query)->queryAll();
    }

    // Populate articles /////////////////////////////////////////////////////////////////////////////////////

    public static function populateArticles($activeTab, array $source, array &$dest): void
    {
        if ($activeTab == self::TAB_BY_PURSE_DETAILED) {

            self::_populateArticlesDetailed($source, $dest);

        } else {

            self::_populateArticles($source, $dest);
        }
    }

    private static function _populateArticles($source, &$dest)
    {
        foreach ($source as $paymentType => $l1) {

            foreach ($l1['levels'] as $flowType => $l2) {

                foreach ($l2['levels'] as $itemId => $l3) {

                    if (!isset($dest[$paymentType]['levels'][$flowType]['levels'][$itemId])) {
                        $dest[$paymentType]['levels'][$flowType]['levels'][$itemId] = [
                            'title' => $l3['title'],
                            'data' => self::EMPTY_YEAR
                        ];
                    }

                    if (empty($l3['levels']))
                        continue 1;

                    foreach ($l3['levels'] as $subItemId => $l4) {
                        if (!isset($dest[$paymentType]['levels'][$flowType]['levels'][$itemId]['levels'][$subItemId])) {
                            $dest[$paymentType]['levels'][$flowType]['levels'][$itemId]['levels'][$subItemId] = [
                                'title' => $l4['title'],
                                'data' => self::EMPTY_YEAR
                            ];
                        }
                    }
                }
            }
        }
    }

    private static function _populateArticlesDetailed($source, &$dest)
    {
        foreach ($source as $paymentType => $l0) {

            foreach ($l0['children'] as $walletId => $l1) {

                foreach ($l1['levels'] as $flowType => $l2) {

                    foreach ($l2['levels'] as $itemId => $l3) {

                        if (!isset($dest[$paymentType]['children'][$walletId]['levels'][$flowType]['levels'][$itemId])) {
                            $dest[$paymentType]['children'][$walletId]['levels'][$flowType]['levels'][$itemId] = [
                                'title' => $l3['title'],
                                'data' => self::EMPTY_YEAR
                            ];
                        }

                        if (empty($l3['levels']))
                            continue 1;

                        foreach ($l3['levels'] as $subItemId => $l4) {
                            if (!isset($dest[$paymentType]['children'][$walletId]['levels'][$flowType]['levels'][$itemId]['levels'][$subItemId])) {
                                $dest[$paymentType]['children'][$walletId]['levels'][$flowType]['levels'][$itemId]['levels'][$subItemId] = [
                                    'title' => $l4['title'],
                                    'data' => self::EMPTY_YEAR
                                ];
                            }
                        }
                    }
                }
            }
        }
    }


    // CHARTS /////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param $showDeviation
     * @param $isIncome
     * @param $factAmount
     * @param $planAmount
     * @return array
     */
    public static function getTooltipData($showDeviation, $isIncome, $factAmount, $planAmount)
    {
        $diff = ($factAmount - $planAmount) * ($isIncome ? 1 : -1);
        $deviation = ($planAmount > 0) ? round($diff / $planAmount * 100, 2) : ($factAmount > 0 ? ($isIncome ? 1 : -1) * 100 : 0);

        return [
            'fact' => $factAmount,
            'plan' => $planAmount,
            'diff' => $diff,
            'deviation' => $deviation,
            'tdClass' => ($showDeviation) ? ' tooltip2-deviation-td ' : null,
            'color' => ($showDeviation) ? self::getItemColor($deviation, true) : 'transparent'
        ];
    }

    /**
     * @param $deviation
     * @param bool $isNewTheme
     * @return mixed|string
     */
    public static function getItemColor($deviation, $isNewTheme = false)
    {
        $deviation = floatval(str_replace([' ', ','], ['', '.'],$deviation));

        $colors = [
            [
                'from' => 1,
                'to' => 10,
                'colorMinus' => ($isNewTheme) ? 'rgba(227, 6, 17, 0.05)' : '#f3565d29',
                'colorPlus' => ($isNewTheme) ? 'rgba(38, 205, 88, 0.05)' : '#45b6af1c',
            ],
            [
                'from' => 11,
                'to' => 25,
                'colorMinus' => ($isNewTheme) ? 'rgba(227, 6, 17, 0.1)' : '#f3565d4d',
                'colorPlus' => ($isNewTheme) ? 'rgba(38, 205, 88, 0.1)' : '#45b6af54',
            ],
            [
                'from' => 26,
                'to' => 50,
                'colorMinus' => ($isNewTheme) ? 'rgba(227, 6, 17, 0.15)' : '#f3565d85',
                'colorPlus' => ($isNewTheme) ? 'rgba(38, 205, 88, 0.15)' : '#45b6af8c',
            ],
            [
                'from' => 51,
                'to' => 75,
                'colorMinus' => ($isNewTheme) ? 'rgba(227, 6, 17, 0.2)' : '#f3565db8',
                'colorPlus' => ($isNewTheme) ? 'rgba(38, 205, 88, 0.2)' : '#45b6afbd',
            ],
            [
                'from' => 76,
                'colorMinus' => ($isNewTheme) ? 'rgba(227, 6, 17, 0.25)' : '#f3565d',
                'colorPlus' => ($isNewTheme) ? 'rgba(38, 205, 88, 0.25)' : '#45b6af',
            ],
        ];
        foreach ($colors as $color) {
            if (isset($color['to'])) {
                if ($deviation > 0) {
                    if ($deviation >= $color['from'] && $deviation <= $color['to']) {
                        return $color['colorPlus'];
                    }
                } else {
                    if ($deviation <= -$color['from'] && $deviation >= -$color['to']) {
                        return $color['colorMinus'];
                    }
                }
            } else {
                if ($deviation > 0 && $deviation > $color['from']) {
                    return $color['colorPlus'];
                } elseif ($deviation < 0 && $deviation < -$color['from']) {
                    return $color['colorMinus'];
                }
            }
        }

        return 'transparent';
    }

    /**
     * @param $dates
     * @param string $period
     * @param array $filters
     * @return array
     */
    public function getPlanFactSeriesData($dates, $period = "days", $filters = [])
    {
        $companyIds = implode(',', $this->_multiCompanyIds);

        $purse = $filters['purse'] ?? null;
        $activity = $filters['activity'] ?? null;
        $article = $filters['article'] ?? [];

        $PLAN = 'PLAN';
        $FACT = 'FACT';
        $INCOME = CashFlowsBase::FLOW_TYPE_INCOME;
        $OUTCOME = CashFlowsBase::FLOW_TYPE_EXPENSE;

        $_amountIncome = [
            $PLAN => [],
            $FACT => []
        ];
        $_amountOutcome = [
            $PLAN => [],
            $FACT => []
        ];
        $_diff = [
            $INCOME => [],
            $OUTCOME => []
        ];
        $_balance = [];

        $sqlFilterByArticlesPlan = '1 = 1';
        if ($article) {
            $incomeItems = AbstractFinance::getArticlesWithChildren(
                array_filter($article[CashFlowsBase::FLOW_TYPE_INCOME] ?? [], function($v) { return (int)$v; }),
                CashBankFlows::FLOW_TYPE_INCOME,
                $this->_multiCompanyIds
            );
            $expenseItems = AbstractFinance::getArticlesWithChildren(
                array_filter($article[CashFlowsBase::FLOW_TYPE_EXPENSE] ?? [], function($v) { return (int)$v; }),
                CashBankFlows::FLOW_TYPE_EXPENSE,
                $this->_multiCompanyIds
            );
            if ($incomeItems && $expenseItems) {
                $sqlFilterByArticlesPlan = 'income_item_id IN ('.implode(',', $incomeItems).' OR expenditure_item_id IN ('.implode(',', $expenseItems).')';
            }
            if ($incomeItems) {
                $sqlFilterByArticlesPlan = 'income_item_id IN ('.implode(',', $incomeItems).')';
            }
            if ($expenseItems) {
                $sqlFilterByArticlesPlan = 'expenditure_item_id IN ('.implode(',', $expenseItems).')';
            }
        }

        $sqlFilterByArticlesFact = '';
        if ($article) {
            $incomeItems = AbstractFinance::getArticlesWithChildren(
                array_filter($article[CashFlowsBase::FLOW_TYPE_INCOME] ?? [], function($v) { return (int)$v; }),
                CashBankFlows::FLOW_TYPE_INCOME,
                $this->_multiCompanyIds
            );
            $expenseItems = AbstractFinance::getArticlesWithChildren(
                array_filter($article[CashFlowsBase::FLOW_TYPE_EXPENSE] ?? [], function($v) { return (int)$v; }),
                CashBankFlows::FLOW_TYPE_EXPENSE,
                $this->_multiCompanyIds
            );
            if ($incomeItems && $expenseItems) {
                $sqlFilterByArticlesFact = '((t.type = 1 AND t.item_id IN ('.implode(',', $incomeItems).')) OR (t.type = 0 AND t.item_id IN ('.implode(',', $expenseItems).')))';
            }
            elseif ($incomeItems) {
                $sqlFilterByArticlesFact = 't.type = 1 AND item_id IN ('.implode(',', $incomeItems).')';
            }
            elseif ($expenseItems) {
                $sqlFilterByArticlesFact = '(t.type = 0 AND item_id IN ('.implode(',', $expenseItems).'))';
            }
        }

        $dateStart = $dates[0]['from'];
        $dateEnd = $dates[count($dates)-1]['to'];
        $dateKeyLength = ($period == "months") ? 7 : 10;

        // 0. Zeroes
        foreach ($dates as $date) {
            $pos = substr($date['from'], 0, $dateKeyLength);
            $_amountIncome[$PLAN][$pos] = 0;
            $_amountIncome[$FACT][$pos] = 0;
            $_amountOutcome[$PLAN][$pos] = 0;
            $_amountOutcome[$FACT][$pos] = 0;
            $_diff[$INCOME][$pos] = 0;
            $_diff[$OUTCOME][$pos] = 0;
            $_balance[$pos] = 0;
        }

        // 1. Fact
        $query = "
            SELECT
              t.amount, 
              t.type AS flow_type,
              t.item_id,
              t.date
            FROM `olap_flows` t
            WHERE t.company_id IN ({$companyIds})
              AND t.date BETWEEN '{$dateStart}' AND '{$dateEnd}'
          ";

        if ($purse) {
            $query .= ' AND t.wallet = ' . (int)$purse;
        }
        if ($sqlFilterByArticlesFact) {
            $query .= ' AND ' . $sqlFilterByArticlesFact;
        }

        $flows = Yii::$app->db2->createCommand($query)->queryAll();

        foreach ($flows as $f) {

            if ($activity && $activity != AbstractFinance::$blockByType[self::__getActivityBlockType($f['flow_type'], $f['item_id'])])
                continue;

            $pos = substr($f['date'], 0, $dateKeyLength);
            $k = ($f['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME ? 1 : -1);

            $_diff[$f['flow_type']][$pos] += $k * $f['amount'] / 100;
            if ($k == 1)
                $_amountIncome[$FACT][$pos] += $f['amount'] / 100;
            else
                $_amountOutcome[$FACT][$pos] += $f['amount'] / 100;
        }

        // 2. Plan
        $flows = PlanCashFlows::find()
            ->andWhere(['company_id' => $this->_multiCompanyIds])
            //->andWhere(['not', ['contractor_id' => $EXCLUDE_CONTRACTORS]])
            ->andWhere(['between', 'date', $dateStart, $dateEnd])
            ->andWhere(new Expression($sqlFilterByArticlesPlan))
            ->andFilterWhere(['payment_type' => $purse]) // !
            ->select(['amount', 'flow_type', 'date'])
            ->addSelect(new Expression('IF (flow_type = 1, income_item_id, expenditure_item_id) AS item_id'))
            ->asArray()
            ->all(\Yii::$app->db2);

        foreach ($flows as $f) {

            if ($activity && $activity != AbstractFinance::$blockByType[self::__getActivityBlockType($f['flow_type'], $f['item_id'])])
                continue;

            $pos = substr($f['date'], 0, $dateKeyLength);
            $k = ($f['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME ? 1 : -1);

            $_diff[$f['flow_type']][$pos] -= $k * $f['amount'] / 100;
            if ($k == 1)
                $_amountIncome[$PLAN][$pos] += $f['amount'] / 100;
            else
                $_amountOutcome[$PLAN][$pos] += $f['amount'] / 100;
        }

        foreach ($dates as $date) {
            $pos = substr($date['from'], 0, $dateKeyLength);
            $_balance[$pos] +=
                ($_amountIncome[$FACT][$pos] - $_amountOutcome[$FACT][$pos]) -
                ($_amountIncome[$PLAN][$pos] - $_amountOutcome[$PLAN][$pos]);

        }

        return [
            'planIncome' => array_values($_amountIncome[$PLAN]),
            'planOutcome' => array_values($_amountOutcome[$PLAN]),
            'factIncome' => array_values($_amountIncome[$FACT]),
            'factOutcome' => array_values($_amountOutcome[$FACT]),
            'balance' => array_values($_balance),
            'incomeDiff' => array_values($_diff[$INCOME]),
            'outcomeDiff' => array_values($_diff[$OUTCOME]),
        ];
    }

    // XLS ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param $type
     * @param $params
     * @throws \Exception
     */
    public function generateXls($type, $params = [])
    {
        $userConfig = Yii::$app->user->identity->config;

            $data = ($userConfig->report_plan_fact_expanded) ?
                $this->_generateXlsExpanded($type) :
                $this->_generateXls($type);

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $data['formattedData'],
            'title' => "План-Факт за {$this->year} год",
            'rangeHeader' => range('A', 'AW'),
            'columns' => $data['columns'],
            'format' => 'Xlsx',
            'fileName' => "Плат-Факт за {$this->year} год",
        ]);
    }

    protected function _generateXls($type)
    {
        $dataFull = ($type == self::TAB_BY_ACTIVITY) ? $this->searchByActivity() : $this->searchByPurse();
        $data = &$dataFull['data'];
        $growingData = &$dataFull['growingData'];
        $totalData = &$dataFull['totalData'];

        // header
        $columns = [];
        $columns[] = [
            'attribute' => 'title',
            'header' => 'Статьи',
        ];
        foreach (AbstractFinance::$month as $monthNumber => $monthText) {
            $columns[] = [
                'attribute' => $monthNumber,
                'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                'header' => $monthText,
            ];
        }

        // data
        $formattedData = [];

        // LEVEL 1
        foreach ($data as $paymentType => $level1) {

            if ($paymentType == AbstractFinance::OWN_FUNDS_BLOCK)
                continue;

            $formattedData[] = $this->_buildXlsRow(1, $level1['title'], $level1['data']);

            // LEVEL 2
            foreach ($level1['levels'] as $paymentFlowType => $level2) {

                $formattedData[] = $this->_buildXlsRow(2, $level2['title'], $level2['data']);

                // LEVEL 3
                foreach ($level2['levels'] as $itemId => $level3) {

                    $formattedData[] = $this->_buildXlsRow(3, $level3['title'], $level3['data']);
                }
            }

            if (isset($growingData[$paymentType]['hide']))
                continue;

            // GROWING
            $formattedData[] = $this->_buildXlsRow(1, $growingData[$paymentType]['title'], $growingData[$paymentType]['data']);
        }

        // TOTAL BY MONTH
        $formattedData[] = $this->_buildXlsRow(1, $totalData['month']['title'], $totalData['month']['data']);

        // TOTAL BY YEAR
        $formattedData[] = $this->_buildXlsRow(1, $totalData['growing']['title'], $totalData['growing']['data']);

        return ['columns' => $columns, 'formattedData' => $formattedData];
    }

    protected function _generateXlsExpanded($type)
    {
        $dataFull = ($type == self::TAB_BY_ACTIVITY) ? $this->searchByActivity() : $this->searchByPurse();
        $planDataFull = ($type == self::TAB_BY_ACTIVITY) ? $this->searchByActivityPlan() : $this->searchByPursePlan();
        $data = &$dataFull['data'];
        $growingData = &$dataFull['growingData'];
        $totalData = &$dataFull['totalData'];
        $plan = &$planDataFull['data'];
        $totalPlan = &$planDataFull['totalData'];

        // header
        $columns = [];
        $columns[] = [
            'attribute' => 'title',
            'header' => 'Статьи',
        ];
        // data
        $formattedData = [
            0 => ['title' => '']
        ];
        foreach (AbstractFinance::$month as $monthNumber => $monthText) {
            $colPlan = $monthNumber . '_plan';
            $colFact = $monthNumber . '_fact';
            $colDiff = $monthNumber . '_diff';
            $colDeviation = $monthNumber . '_deviation';
            $columns[] = [
                'attribute' => $colPlan,
                'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                'header' => $monthText,
            ];
            $columns[] = [
                'attribute' => $colFact,
                'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                'header' => '',
            ];
            $columns[] = [
                'attribute' => $colDiff,
                'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                'header' => '',
            ];
            $columns[] = [
                'attribute' => $colDeviation,
                'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                'header' => '',
            ];
            $formattedData[0][$colPlan] = 'План';
            $formattedData[0][$colFact] = 'Факт';
            $formattedData[0][$colDiff] = 'Разница';
            $formattedData[0][$colDeviation] = '%';
        }

        // LEVEL 1
        foreach ($data as $paymentType => $level1) {

            if ($paymentType == AbstractFinance::OWN_FUNDS_BLOCK)
                continue;

            $planLevel1 = ArrayHelper::getValue($plan, $paymentType);
            $formattedData[] = $this->_buildXlsRowExpanded(1, $level1['title'], $level1['data'], $planLevel1['data'] ?? [], true);

            // LEVEL 2
            foreach ($level1['levels'] as $paymentFlowType => $level2) {
                $isIncome = in_array($paymentFlowType, ($type == self::TAB_BY_ACTIVITY) ? [1,3,4,6] : [1,3,5]);
                $planLevel2 = ArrayHelper::getValue($planLevel1['levels'] ?? [], $paymentFlowType);
                $formattedData[] = $this->_buildXlsRowExpanded(2, $level2['title'], $level2['data'], $planLevel2['data'] ?? [], $isIncome);

                // LEVEL 3
                foreach ($level2['levels'] as $itemId => $level3) {

                    $planLevel3 = ArrayHelper::getValue($planLevel2['levels'] ?? [], $itemId);
                    $formattedData[] = $this->_buildXlsRowExpanded(3, $level3['title'], $level3['data'], $planLevel3['data'] ?? [], $isIncome);
                }
            }

            if (isset($growingData[$paymentType]['hide']))
                continue;

            // GROWING
            $formattedData[] = $this->_buildXlsRowExpanded(1, $growingData[$paymentType]['title'], $growingData[$paymentType]['data']);
        }

        // TOTAL BY MONTH
        $formattedData[] = $this->_buildXlsRowExpanded(1, $totalData['month']['title'], $totalData['month']['data']);

        // TOTAL BY YEAR
        $formattedData[] = $this->_buildXlsRowExpanded(1, $totalData['growing']['title'], $totalData['growing']['data']);

        return ['columns' => $columns, 'formattedData' => $formattedData];
    }

    protected function _buildXlsRow($level, $title, $d)
    {
        return [
            'title' => str_repeat(' ', 5*($level-1)) . $title,
            '01' => round($d['01'] / 100, 2),
            '02' => round($d['02'] / 100, 2),
            '03' => round($d['03'] / 100, 2),
            '04' => round($d['04'] / 100, 2),
            '05' => round($d['05'] / 100, 2),
            '06' => round($d['06'] / 100, 2),
            '07' => round($d['07'] / 100, 2),
            '08' => round($d['08'] / 100, 2),
            '09' => round($d['09'] / 100, 2),
            '10' => round($d['10'] / 100, 2),
            '11' => round($d['11'] / 100, 2),
            '12' => round($d['12'] / 100, 2)
        ];
    }

    protected function _buildXlsRowExpanded($level, $title, $data, $plan = null, $isIncome = null)
    {
        $ret = [
            'title' => str_repeat(' ', 5*($level-1)) . $title,
        ];

        foreach (AbstractFinance::$month as $month => $monthText) {
            $amount = isset($data[$month]) ? $data[$month] : 0;
            $planAmount = ArrayHelper::getValue($plan, $month, 0);
            $diff = ($amount - $planAmount) * ($isIncome ? 1 : -1);
            $deviation = ($amount > 0) ? round($diff / $amount * 100, 2) : ($planAmount > 0 ? ($isIncome ? -1 : 1) * 100 : 0);
            $ret[$month.'_plan'] = ($plan === null) ? '' : round($planAmount / 100, 2);
            $ret[$month.'_fact'] = round($amount / 100, 2);
            $ret[$month.'_diff'] = ($plan === null) ? '' : round($diff / 100, 2);
            $ret[$month.'_deviation'] = ($plan === null) ? '' : round($deviation, 2);
        }

        return $ret;
    }
}