<?php
namespace frontend\modules\analytics\models;

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\components\helpers\Month;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\Employee;
use common\models\project\Project;
use common\modules\acquiring\models\AcquiringOperation;
use common\modules\cards\models\CardOperation;
use frontend\modules\analytics\models\AbstractFinanceHelper as AFH;
use frontend\modules\cash\models\CashContractorType;
use yii\base\Exception;
use yii\base\InvalidParamException;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;
use common\models\IncomeItemFlowOfFunds;
use common\models\ExpenseItemFlowOfFunds;
use yii\db\ActiveQuery;
use common\components\excel\Excel;

/**
 * Class PaymentCalendarSearch
 * @package frontend\modules\analytics\models
 *
 * @property Company $company
 */
class PaymentCalendarSearch extends AbstractFinance
{
    const TAB_BY_ACTIVITY = 3;
    const TAB_BY_PURSE = 4;
    const TAB_BY_PURSE_DETAILED = "4d";
    const TAB_CALENDAR = "payment_calendar";
    const TAB_BY_INDUSTRY = 14;
    const TAB_BY_SALE_POINT = 15;

    /**
     * Min flows count to add contractors flows to auto-plan list
     */
    const AUTO_PLAN_CONTRACTOR_FLOWS_COUNT = 3;

    const STATUS_PLAN = 'plan';
    const STATUS_OVERDUE = 'overdue';
    const STATUS_MOVED = 'moved';

    /**
     * @var
     */
    public $period;
    public $fullPeriod;
    public $futurePeriod;

    /**
     * @var
     */
    public $group;

    /**
     * @var
     */
    public $payment_type;

    /**
     * @var
     */
    public $flow_type;

    /**
     * @var
     */
    public $status_type;

    /**
     * @var
     */
    public $priority_type;

    /**
     * @var
     */
    public $contractor_id;

    /**
     * @var
     */
    public $company_id;

    /**
     * @var
     */
    public $contractor_name;

    /**
     * @var
     */
    public $reason_id;

    /**
     * @var (not used)
     */
    public $wallet_id;

    /**
     * @var
     */
    public $project_id;

    /**
     * @var
     */
    public $sale_point_id;

    /**
     * @var
     */
    public $industry_id;

    /**
     * @var
     */
    public $byIds;

    public $exportPlanFlowsRegister = false;

    /**
     * @vars
     */
    protected $_bankArray;
    protected $_cashboxArray;
    protected $_emoneyArray;

    /**
     * @var array
     */
    public static $cashContractorTypeByTypes = [
        self::INCOME_CASH_BANK => [
            CashContractorType::ORDER_TEXT,
            CashContractorType::EMONEY_TEXT,
        ],
        self::EXPENSE_CASH_BANK => [
            CashContractorType::ORDER_TEXT,
            CashContractorType::EMONEY_TEXT,
        ],
        self::INCOME_CASH_ORDER => [
            CashContractorType::BANK_TEXT,
            CashContractorType::EMONEY_TEXT,
        ],
        self::EXPENSE_CASH_ORDER => [
            CashContractorType::BANK_TEXT,
            CashContractorType::EMONEY_TEXT,
        ],
        self::INCOME_CASH_EMONEY => [
            CashContractorType::ORDER_TEXT,
            CashContractorType::BANK_TEXT,
        ],
        self::EXPENSE_CASH_EMONEY => [
            CashContractorType::ORDER_TEXT,
            CashContractorType::BANK_TEXT,
        ],
    ];

    /**
     * @var array
     */
    public static $paymentTypeByType = [
        self::INCOME_CASH_BANK => PlanCashFlows::PAYMENT_TYPE_BANK,
        self::EXPENSE_CASH_BANK => PlanCashFlows::PAYMENT_TYPE_BANK,
        self::INCOME_CASH_ORDER => PlanCashFlows::PAYMENT_TYPE_ORDER,
        self::EXPENSE_CASH_ORDER => PlanCashFlows::PAYMENT_TYPE_ORDER,
        self::INCOME_CASH_EMONEY => PlanCashFlows::PAYMENT_TYPE_EMONEY,
        self::EXPENSE_CASH_EMONEY => PlanCashFlows::PAYMENT_TYPE_EMONEY,
    ];

    public $incomeItemIdManyItem;
    public $expenditureItemIdManyItem;
    public $incomeManyDate;
    public $expenditureManyDate;

    /**
     * @var array
     */
    public static $paymentTypeTextByClassName = [
        CashBankFlows::class => 'Банк',
        CashOrderFlows::class => 'Касса',
        CashEmoneyFlows::class => 'E-money',
    ];

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['company_id', 'contractor_id', 'reason_id', 'payment_type', 'status_type', 'priority_type', 'flow_type'], 'integer'],
            [['project_id', 'sale_point_id', 'industry_id'], 'integer'],
            [['contractor_name', 'wallet_id'], 'safe'],
            [['status_type'], 'in', 'range' => [self::STATUS_PLAN, self::STATUS_OVERDUE, self::STATUS_MOVED]],
            [['priority_type'], 'in', 'range' => [Contractor::PAYMENT_PRIORITY_HIGH, Contractor::PAYMENT_PRIORITY_MEDIUM, Contractor::PAYMENT_PRIORITY_LOW]],
            [['flow_type'], 'in', 'range' => [CashFlowsBase::FLOW_TYPE_INCOME, CashFlowsBase::FLOW_TYPE_EXPENSE]],
        ]);
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        AFH::init($this->multiCompanyIds);

        $this->year = min(
            \Yii::$app->session->get('modules.reports.finance.year', date('Y')),
            date('Y') + 1);
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     * @throws \Exception
     */
    public function searchItems($params = [])
    {
        $this->_load($params);
        $this->checkCashContractor();
        if ($this->futurePeriod) {
            $periodStart = '1999-01-01';
            $periodEnd = '2999-12-31';
        }
        elseif ($this->fullPeriod) {
            $periodStart = $this->fullPeriod['start'];
            $periodEnd = $this->fullPeriod['end'];
        }
        elseif (!$this->period) {
            $this->period['start'] = '01';
            $this->period['end'] = '12';
            $periodStart = $this->year . '-' . $this->period['start'] . '-01';
            $periodEnd = $this->year . '-' . $this->period['end'] . '-' . cal_days_in_month(CAL_GREGORIAN, $this->period['end'], $this->year);
        } else {
            if (isset($this->period['day'])) {
                $day = ($this->period['day'] > 9) ? $this->period['day'] : ('0' . $this->period['day']);
                $periodStart = $this->year . '-' . $this->period['start'] . '-' . $day;
                $periodEnd = $this->year . '-' . $this->period['end'] . '-' . $day;
            } else {
                $periodStart = $this->year . '-' . $this->period['start'] . '-01';
                $periodEnd = $this->year . '-' . $this->period['end'] . '-' . cal_days_in_month(CAL_GREGORIAN, $this->period['end'], $this->year);
            }
        }

        $query = PlanCashFlows::find()
            ->select([
                't.id',
                't.company_id',
                't.payment_type',
                "IF({{t}}.[[flow_type]] = 0, SUM({{t}}.[[amount]]), 0) as amountExpense",
                "IF({{t}}.[[flow_type]] = 1, SUM({{t}}.[[amount]]), 0) as amountIncome",
                't.created_at',
                't.date',
                't.first_date',
                't.flow_type',
                't.amount',
                't.contractor_id',
                't.description',
                't.expenditure_item_id',
                't.income_item_id',
                't.checking_accountant_id',
                't.cashbox_id',
                't.emoney_id',
                't.project_id',
                't.sale_point_id',
                't.industry_id',
                't.invoice_id',
                //InvoiceIncomeItem::tableName() . '.name incomeItemName',
                //InvoiceExpenditureItem::tableName() . '.name expenseItemName',
            ])
            ->addSelect(new Expression('
              CASE 
                WHEN t.payment_type = 1 THEN t.checking_accountant_id
                WHEN t.payment_type = 2 THEN t.cashbox_id
                WHEN t.payment_type = 3 THEN t.emoney_id
              END AS wallet_id
            '))
            ->from(['t' => PlanCashFlows::tableName()])
            //->joinWith('expenditureItem')
            //->joinWith('incomeItem')
            ->joinWith('contractor')
            ->andWhere(['t.company_id' => $this->multiCompanyIds])
            ->andWhere(['between', 't.date', $periodStart, $periodEnd]);

        switch ($this->status_type) {
            case self::STATUS_PLAN:
                $query->andWhere(['>=', 't.date', date('Y-m-d')]);
                $query->andWhere(['>=', 't.first_date', date('Y-m-d')]);
                break;
            case self::STATUS_OVERDUE:
                $query->andWhere(['<', 't.first_date', date('Y-m-d')]);
                $query->andWhere(['<', 't.date', date('Y-m-d')]);
                break;
            case self::STATUS_MOVED:
                $query->andWhere(['<', 't.first_date', date('Y-m-d')]);
                $query->andWhere(['>=', 't.date', date('Y-m-d')]);
                break;
        }

        if ($this->priority_type) {
            $query->andWhere([
                'contractor.is_seller' => 1,
                'contractor.payment_priority' => $this->priority_type
            ]);
        }

        if ($this->flow_type) {
            $query->andWhere([
                't.flow_type' => $this->flow_type,
            ]);
        }

        if ($this->byIds) {
            $query->andWhere([
                't.id' => $this->byIds
            ]);
        }

        $query->groupBy('t.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query->asArray(),
            'sort' => [
                'attributes' => [
                    'date' => [
                        'asc' => [
                            'date' => SORT_ASC,
                            'created_at' => SORT_ASC,
                        ],
                        'desc' => [
                            'date' => SORT_DESC,
                            'created_at' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'amountIncome' => [
                        'asc' => [
                            'flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'amountExpense' => [
                        'asc' => [
                            'flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'contractor_id',
                    'description',
                    'created_at',
                ],
                'defaultOrder' => [
                    'date' => SORT_DESC,
                ],
            ],
        ]);
        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        $query->andFilterWhere(['t.contractor_id' => $this->contractor_id]);
        $query->andFilterWhere(['t.company_id' => $this->company_id]);

        if (is_array($this->income_item_id) || is_array($this->expenditure_item_id)) {
            if (!empty($this->income_item_id) && !empty($this->expenditure_item_id)) {
                $query->andWhere(['or',
                    ['and',
                        ['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE],
                        ['in', 'expenditure_item_id', $this->expenditure_item_id],
                    ],
                    ['and',
                        ['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME],
                        ['in', 'income_item_id', $this->income_item_id],
                    ],
                    $this->cash_contractor ? ['in', 'contractor_id', $this->cash_contractor] : [],
                ]);
            } elseif (!empty($this->income_item_id)) {

                $this->income_item_id =
                    self::getArticlesWithChildren($this->income_item_id, CashBankFlows::FLOW_TYPE_INCOME, $this->multiCompanyIds);

                $query->andWhere(['or',
                    ['and',
                        ['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME],
                        ['in', 'income_item_id', $this->income_item_id],
                    ],
                    $this->cash_contractor ? ['in', 'contractor_id', $this->cash_contractor] : [],
                ]);
            } elseif (!empty($this->expenditure_item_id)) {

                $this->expenditure_item_id =
                    self::getArticlesWithChildren($this->expenditure_item_id, CashBankFlows::FLOW_TYPE_EXPENSE, $this->multiCompanyIds);

                $query->andWhere(['or',
                    ['and',
                        ['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE],
                        ['in', 'expenditure_item_id', $this->expenditure_item_id],
                    ],
                    $this->cash_contractor ? ['in', 'contractor_id', $this->cash_contractor] : [],
                ]);
            } else {
                if ($this->cash_contractor) {
                    $query->andWhere(['in', 'contractor_id', $this->cash_contractor]);
                } else {
                    $query->andWhere(['flow_type' => null]);
                }
            }
        }

        $query->andFilterWhere(['t.payment_type' => $this->payment_type]);

        if ($this->project_id > 0) {
            $query->andFilterWhere(['t.project_id' => $this->project_id]);
        }
        if ($this->sale_point_id > 0) {
            $query->andFilterWhere(['t.sale_point_id' => $this->sale_point_id]);
        }
        if ($this->industry_id > 0) {
            $query->andFilterWhere(['t.industry_id' => $this->industry_id]);
        }

        if ($this->contractor_name) {
            $query->andFilterWhere(['or',
                ['like', 'contractor.name', $this->contractor_name],
                ['like', 'contractor.ITN', $this->contractor_name],
            ]);
        }

        if ($this->reason_id == 'empty') {
            $query->andWhere(['or',
                ['and',
                    ['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE],
                    ['expenditure_item_id' => null],
                ],
                ['and',
                    ['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME],
                    ['income_item_id' => null],
                ],
            ]);
        } elseif (!empty($this->reason_id)) {
            $this->expenditure_item_id = $this->income_item_id = null;
            if (substr($this->reason_id, 0, 2) === 'e_') {
                $this->expenditure_item_id = substr($this->reason_id, 2);
            } elseif (substr($this->reason_id, 0, 2) === 'i_') {
                $this->income_item_id = substr($this->reason_id, 2);
            }
            $query->andFilterWhere([
                'expenditure_item_id' => $this->expenditure_item_id,
                'income_item_id' => $this->income_item_id,
            ]);
        }

        if ($this->wallet_id) {
            if ($this->payment_type == CashFlowsBase::WALLET_BANK) {
                $_tmp = CheckingAccountant::find()->where(['company_id' => $this->multiCompanyIds, 'rs' => $this->wallet_id])->one();
                $this->wallet_id = $_tmp ? $_tmp->id : null;
            }
            $query->andHaving([
                'wallet_id' => $this->wallet_id,
            ]);
        }

        $this->_filterQuery = clone $query;



        return $dataProvider;
    }

    /**
     * @param array $selectedContractorsFlows
     * @return mixed
     */
    public function searchAutoPlanItems($selectedContractorsFlows = null, $skipByCashFlowAutoPlan = true)
    {
        $result['years'] = [];
        $contractorItems = [];
        foreach ([CashFlowsBase::FLOW_TYPE_EXPENSE, CashFlowsBase::FLOW_TYPE_INCOME] as $flowType) {
            $params = $this->getParamsByFlowType($flowType);
            $contractors = ($selectedContractorsFlows !== null) ?
                $selectedContractorsFlows : $this->getAutoPlanContractors($flowType);

            foreach ($this->getAutoPlanYears() as $year) {
                $dateFrom = $year . '-01-01';
                $dateTo = $year . '-12-31';
                /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */

                $autoPlanWallets = [
                    CashBankFlows::class,
                    CashOrderFlows::class,
                    CashEmoneyFlows::class
                ];

                foreach ($autoPlanWallets as $className) {
                    foreach ($contractors[$className] as $itemID => $contractorIDs) {
                        $items = $this->getDefaultCashFlowsQuery($className, $params, $dateFrom, $dateTo)
                            ->select([
                                $className::tableName() . ".{$params['itemAttrName']} as itemID",
                                'invoiceItem.name as itemName',
                                $className::tableName() . '.amount as flowSum',
                                $className::tableName() . '.contractor_id as contractorID',
                                new Expression('"' . $className::tableName() . '" as paymentType'),
                                'MONTH(' . $className::tableName() . '.date) as monthNumber',
                            ])
                            ->andWhere(['flow_type' => $flowType])
                            ->andWhere(['in', $className::tableName() . '.contractor_id', $contractorIDs])
                            ->andWhere(['invoiceItem.id' => $itemID])
                            ->orderBy(['invoiceItem.name' => SORT_ASC])
                            ->asArray()
                            ->all(\Yii::$app->db2);
                        foreach ($items as $item) {
                            $cashFlowsAutoPlanItem = $this->findCashFlowAutoPlan(
                                $flowType,
                                $this->getPaymentTypeByTableName($item['paymentType']),
                                $item['itemID'],
                                $item['contractorID']
                            );
                            if ($cashFlowsAutoPlanItem && ($cashFlowsAutoPlanItem->is_deleted || (!$cashFlowsAutoPlanItem->is_deleted && $cashFlowsAutoPlanItem->end_plan_date > date(DateHelper::FORMAT_DATE)))) {
                                if ($skipByCashFlowAutoPlan)
                                    continue;
                            }
                            $flowSum = $item['flowSum'];
                            $contractorID = $item['contractorID'];
                            $contractor = null;
                            if (in_array($contractorID, [
                                CashContractorType::BANK_TEXT,
                                CashContractorType::ORDER_TEXT,
                                CashContractorType::EMONEY_TEXT
                            ])) {

                                /* @var $cashFlowsAutoPlanItem CashFlowsAutoPlan */
                                $cashFlowsAutoPlanItem = CashFlowsAutoPlan::find()
                                    ->andWhere(['and',
                                        ['company_id' => $this->company->id],
                                        ['flow_type' => $flowType],
                                        ['payment_type' => $this->getPaymentTypeByTableName($item['paymentType'])],
                                        ['contractor_id' => $item['contractorID']],
                                    ])->one();
                                if (
                                    ($className == CashBankFlows::className() && $contractorID == CashContractorType::BANK_TEXT) ||
                                    ($className == CashOrderFlows::className() && $contractorID == CashContractorType::ORDER_TEXT) ||
                                    ($className == CashEmoneyFlows::className() && $contractorID == CashContractorType::EMONEY_TEXT) ||
                                    ($cashFlowsAutoPlanItem && ($cashFlowsAutoPlanItem->is_deleted || (!$cashFlowsAutoPlanItem->is_deleted && $cashFlowsAutoPlanItem->end_plan_date > date(DateHelper::FORMAT_DATE))))
                                ) {
                                    continue;
                                }

                                $item['itemID'] = $contractorID;
                                $itemName = self::$cashContractorTextByType[$flowType][$contractorID];
                                $contractor = CashContractorType::findOne(['name' => $contractorID])->text;
                                if ($flowType == CashFlowsBase::FLOW_TYPE_INCOME) {
                                    $item['itemName'] = "Приход из {$itemName}";
                                } else {
                                    $item['itemName'] = "Перевод в {$itemName}";
                                }
                            }

                            $result['years'][$year] = $year;
                            if ($contractor === null) {
                                $contractor = Contractor::findOne($item['contractorID']);
                                $contractor = $contractor ? $contractor->getNameWithType() : null;
                            }

                            if (!(isset($contractorItems["{$flowType}-{$item['paymentType']}-{$item['itemID']}-{$item['contractorID']}-{$year}"]))) {
                                $contractorItems["{$flowType}-{$item['paymentType']}-{$item['itemID']}-{$item['contractorID']}-{$year}"] = [
                                    'flowType' => $flowType,
                                    'flowTypeName' => "Плановые {$params['flowTypeText']}",
                                    'paymentType' => $item['paymentType'],
                                    'paymentTypeName' => self::$paymentTypeTextByClassName[$className],
                                    'itemID' => $item['itemID'],
                                    'itemName' => $item['itemName'],
                                    'contractorID' => $item['contractorID'],
                                    'contractorName' => $contractor,
                                    'flowSum' => $flowSum,
                                    'month' => [
                                        $item['monthNumber'] => $item['monthNumber'],
                                    ],
                                    'year' => $year,
                                ];
                            } else {
                                $contractorItems["{$flowType}-{$item['paymentType']}-{$item['itemID']}-{$item['contractorID']}-{$year}"]['flowSum'] += $flowSum;
                                $contractorItems["{$flowType}-{$item['paymentType']}-{$item['itemID']}-{$item['contractorID']}-{$year}"]['month'][$item['monthNumber']] = $item['monthNumber'];
                            }
                        }
                    }
                }
            }
        }
        array_multisort(
            array_column($contractorItems, 'itemName'),
            SORT_ASC,
            array_column($contractorItems, 'contractorName'),
            SORT_ASC,
            $contractorItems
        );
        if (!isset($result['years'][date('Y')])) {
            $contractorItems = $result['years'] = [];
        }
        foreach ($contractorItems as $contractorItemData) {
            $monthCount = count($contractorItemData['month']);
            $flowSum = round($contractorItemData['flowSum'] / $monthCount, 2);
            // Сумма по типу прихода/расхода
            if (!isset($result[$contractorItemData['flowType']]['flowSum'][$contractorItemData['year']])) {
                $result[$contractorItemData['flowType']]['name'] = $contractorItemData['flowTypeName'];
                $result[$contractorItemData['flowType']]['flowSum'][$contractorItemData['year']] = $flowSum;
            } else {
                $result[$contractorItemData['flowType']]['flowSum'][$contractorItemData['year']] += $flowSum;
            }
            // Сумма по типу оплат
            if (!isset($result[$contractorItemData['flowType']][$contractorItemData['paymentType']]['flowSum'][$contractorItemData['year']])) {
                $result[$contractorItemData['flowType']][$contractorItemData['paymentType']]['name'] = $contractorItemData['paymentTypeName'];
                $result[$contractorItemData['flowType']][$contractorItemData['paymentType']]['flowSum'][$contractorItemData['year']] = $flowSum;
            } else {
                $result[$contractorItemData['flowType']][$contractorItemData['paymentType']]['flowSum'][$contractorItemData['year']] += $flowSum;
            }
            // Сумма по статьям
            if (!isset($result[$contractorItemData['flowType']][$contractorItemData['paymentType']][$contractorItemData['itemID']]['flowSum'][$contractorItemData['year']])) {
                $result[$contractorItemData['flowType']][$contractorItemData['paymentType']][$contractorItemData['itemID']]['name'] = $contractorItemData['itemName'];
                $result[$contractorItemData['flowType']][$contractorItemData['paymentType']][$contractorItemData['itemID']]['flowSum'][$contractorItemData['year']] = $flowSum;
            } else {
                $result[$contractorItemData['flowType']][$contractorItemData['paymentType']][$contractorItemData['itemID']]['flowSum'][$contractorItemData['year']] += $flowSum;
            }
            // Сумма по контрагентам
            if (!isset($result[$contractorItemData['flowType']][$contractorItemData['paymentType']][$contractorItemData['itemID']][$contractorItemData['contractorID']]['flowSum'][$contractorItemData['year']])) {
                $result[$contractorItemData['flowType']][$contractorItemData['paymentType']][$contractorItemData['itemID']][$contractorItemData['contractorID']]['name'] = $contractorItemData['contractorName'];
                $result[$contractorItemData['flowType']][$contractorItemData['paymentType']][$contractorItemData['itemID']][$contractorItemData['contractorID']]['flowSum'][$contractorItemData['year']] = $flowSum;
            } else {
                $result[$contractorItemData['flowType']][$contractorItemData['paymentType']][$contractorItemData['itemID']][$contractorItemData['contractorID']]['flowSum'][$contractorItemData['year']] += $flowSum;
            }
        }

        return $result;
    }

    /**
     * @param $flowType
     * @return array
     */
    public function getAutoPlanContractors($flowType, $minFlowsCount = null)
    {
        $minusMonthArray = range(0, 3);
        $result = [];
        $params = $this->getParamsByFlowType($flowType);
        /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
        foreach (self::flowClassArray() as $className) {
            $result[$className] = [];
            $existsContractors = [];
            foreach ($minusMonthArray as $monthCount) {
                $prevDate = strtotime("-{$monthCount} month");
                $dateFrom = date('Y-m-01', $prevDate);
                $dateTo = date('Y-m-', $prevDate) . cal_days_in_month(CAL_GREGORIAN, date('m', $prevDate), date('Y', $prevDate));
                $contractors = $this->getDefaultCashFlowsQuery($className, $params, $dateFrom, $dateTo)
                    ->select([
                        $className::tableName() . '.contractor_id as contractorID',
                        "invoiceItem.id as itemID",
                    ])
                    ->andWhere(['flow_type' => $flowType])
                    ->andWhere(['or',
                        ['invoiceItem.company_id' => null],
                        ['invoiceItem.company_id' => $this->company->id],
                    ])
                    ->groupBy($className::tableName() . '.contractor_id')
                    ->asArray()
                    ->all(\Yii::$app->db2);

                //var_dump($contractors);

                foreach ($contractors as $contractor) {
                    if (!isset($existsContractors[$contractor['itemID']][$contractor['contractorID']])) {
                        $existsContractors[$contractor['itemID']][$contractor['contractorID']] = 1;
                    } else {
                        $existsContractors[$contractor['itemID']][$contractor['contractorID']] += 1;
                    }
                }
            }

            //var_dump($existsContractors);die;
            if ($minFlowsCount === null)
                $minFlowsCount = self::AUTO_PLAN_CONTRACTOR_FLOWS_COUNT;

            foreach ($existsContractors as $itemID => $contractors) {
                foreach ($contractors as $contractorID => $cashFlowsCount) {
                    if ($cashFlowsCount >= $minFlowsCount) {
                        $result[$className][$itemID][] = $contractorID;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param $flowType
     * @param null $paymentType
     * @param null $itemID
     * @param null $contractorID
     * @return float
     */
    public function calculatePlanMonthAmount($flowType, $paymentType = null, $itemID = null, $contractorID = null)
    {
        $flowArray = $this->getFlowArrayByPaymentType($paymentType);
        $params = $this->getParamsByFlowType($flowType);
        $contractors = $this->getAutoPlanContractors($flowType);
        $prevDate = strtotime("-2 month");
        $dateFrom = date('Y-m-01', $prevDate);
        $dateTo = date('Y-m-') . cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
        $amount = null;
        $cashFlowsAutoPlanItemAmount = null;
        /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
        foreach ($flowArray as $className) {
            foreach ($contractors[$className] as $itemID => $contractorIDs) {
                $query = $this->getDefaultCashFlowsQuery($className, $params, $dateFrom, $dateTo)
                    ->andWhere(['flow_type' => $flowType])
                    ->andWhere(['in', $className::tableName() . '.contractor_id', $contractorIDs])
                    ->andWhere(['invoiceItem.id' => $itemID]);
                if ($contractorID) {
                    $query->andWhere([$className::tableName() . '.contractor_id' => $contractorID]);
                }
                if ($itemID) {
                    $cashContractors = array_intersect($contractorIDs, [CashContractorType::BANK_TEXT, CashContractorType::ORDER_TEXT, CashContractorType::EMONEY_TEXT]);
                    $query->andWhere(['or',
                        ["itemFlowOfFunds.{$params['flowOfFundItemName']}" => $itemID],
                        $cashContractors ?
                            ['in', $className::tableName() . '.contractor_id', $cashContractors] :
                            [],
                    ]);
                } else {
                    $query->andWhere(['or',
                        ['invoiceItem.company_id' => null],
                        ['invoiceItem.company_id' => $this->company->id],
                    ]);
                }
                $amount += $query->sum('amount');
            }
        }

        return round($amount / 3, 2) + $cashFlowsAutoPlanItemAmount;
    }

    /**
     * @param $flowType
     * @param null $paymentType
     * @param null $itemID
     * @param null $contractorID
     * @return false|string
     */
    public function calculatePlanMonthDate($flowType, $paymentType = null, $itemID = null, $contractorID = null)
    {
        $flowArray = $this->getFlowArrayByPaymentType($paymentType);
        $params = $this->getParamsByFlowType($flowType);
        $contractors = $this->getAutoPlanContractors($flowType);
        $prevDate = strtotime("-2 month");
        $dateFrom = date('Y-m-01', $prevDate);
        $dateTo = date('Y-m-') . cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
        $dayCount = $daySum = $planDate = null;
        /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
        foreach ($flowArray as $className) {
            foreach ($contractors[$className] as $itemID => $contractorIDs) {
                $query = $this->getDefaultCashFlowsQuery($className, $params, $dateFrom, $dateTo)
                    ->select([
                        'DAY(' . $className::tableName() . '.date) as day',
                    ])
                    ->andWhere(['flow_type' => $flowType])
                    ->andWhere(['in', $className::tableName() . '.contractor_id', $contractorIDs])
                    ->andWhere(['invoiceItem.id' => $itemID]);
                if ($contractorID) {
                    $query->andWhere([$className::tableName() . '.contractor_id' => $contractorID]);
                }
                if ($itemID) {
                    $query->andWhere(['or',
                        ["itemFlowOfFunds.{$params['flowOfFundItemName']}" => $itemID],
                        ['in', $className::tableName() . '.contractor_id', [CashContractorType::BANK_TEXT, CashContractorType::ORDER_TEXT, CashContractorType::EMONEY_TEXT]]
                    ]);
                } else {
                    $query->andWhere(['or',
                        ['invoiceItem.company_id' => null],
                        ['invoiceItem.company_id' => $this->company->id],
                    ]);
                }
                foreach ($query->column(\Yii::$app->db2) as $day) {
                    $dayCount++;
                    $daySum += $day;
                }
            }
        }
        if ($dayCount) {
            $planDay = floor($daySum / $dayCount);
            $planDay = $planDay < 10 ? "0{$planDay}" : $planDay;
            $planDate = date("{$planDay}.m.Y");
            if ($planDate <= date(DateHelper::FORMAT_USER_DATE)) {
                $planDate = date(DateHelper::FORMAT_USER_DATE, strtotime('+1 month', strtotime($planDate)));
            }
        }

        return $planDate;
    }

    /**
     * @param null $paymentType
     * @return array
     */
    public function getFlowArrayByPaymentType($paymentType = null)
    {
        if ($paymentType === null) {
            $flowArray = self::flowClassArray();
        } else {
            switch ($paymentType) {
                case CashBankFlows::tableName():
                    $flowArray[] = CashBankFlows::className();
                    break;
                case CashOrderFlows::tableName():
                    $flowArray[] = CashOrderFlows::className();
                    break;
                case CashEmoneyFlows::tableName():
                    $flowArray[] = CashEmoneyFlows::className();
                    break;
                default:
                    throw new InvalidParamException('Invalid payment type param.');
            }
        }

        return $flowArray;
    }

    /**
     * @param $tableName
     * @return array|int
     */
    public function getPaymentTypeByTableName($tableName)
    {
        if ($tableName === null) {
            return [
                PlanCashFlows::PAYMENT_TYPE_BANK,
                PlanCashFlows::PAYMENT_TYPE_ORDER,
                PlanCashFlows::PAYMENT_TYPE_EMONEY,
            ];
        } else {
            switch ($tableName) {
                case CashBankFlows::tableName():
                    return PlanCashFlows::PAYMENT_TYPE_BANK;
                case CashOrderFlows::tableName():
                    return PlanCashFlows::PAYMENT_TYPE_ORDER;
                case CashEmoneyFlows::tableName():
                    return PlanCashFlows::PAYMENT_TYPE_EMONEY;
                default:
                    throw new InvalidParamException('Invalid table name param.');
            }
        }
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        $minDate = PlanCashFlows::find()
            ->andWhere(['company_id' => $this->multiCompanyIds])
            ->min('date');
        $maxDate = PlanCashFlows::find()
            ->andWhere(['company_id' => $this->multiCompanyIds])
            ->max('date');
        if (!$minDate) {
            return [
                date('Y') => date('Y'),
            ];
        }
        $minYear = DateHelper::format($minDate, 'Y', DateHelper::FORMAT_DATE);
        $maxYear = DateHelper::format($maxDate, 'Y', DateHelper::FORMAT_DATE);
        foreach (range($minYear, $maxYear) as $value) {
            $range[$value] = $value;
        }
        arsort($range);

        return $range;
    }

    /**
     * @return array
     */
    public function getRealFlowsYearFilter()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $range = [];
        $cashOrderMinDate = CashOrderFlows::find()
            ->byCompany($company->id)
            ->min('date');
        $cashBankMinDate = CashBankFlows::find()
            ->byCompany($company->id)
            ->min('date');
        $cashEmoneyMinDate = CashEmoneyFlows::find()
            ->byCompany($company->id)
            ->min('date');
        $minDates = [];

        if ($cashOrderMinDate) $minDates[] = $cashOrderMinDate;
        if ($cashBankMinDate) $minDates[] = $cashBankMinDate;
        if ($cashEmoneyMinDate) $minDates[] = $cashEmoneyMinDate;

        $minCashDate = !empty($minDates) ? min($minDates) : date(DateHelper::FORMAT_DATE);
        $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
        $currentYear = date('Y');
        foreach (range($registrationYear, $currentYear) as $value) {
            $range[$value] = $value;
        }
        arsort($range);

        return $range;
    }

    /**
     * @return mixed
     */
    public function getAutoPlanYears()
    {
        $range = [];
        foreach (range(date('Y') - 2, date('Y')) as $value) {
            $range[$value] = $value;
        }

        return $range;
    }

    /**
     * @param $flowType
     * @param $paymentType
     * @param $item
     * @param $contractorID
     * @return CashFlowsAutoPlan|null|\yii\db\ActiveRecord
     */
    public function findCashFlowAutoPlan($flowType, $paymentType, $item, $contractorID)
    {
        return CashFlowsAutoPlan::find()
            ->andWhere(['and',
                ['company_id' => $this->company->id],
                ['flow_type' => $flowType],
                ['payment_type' => $paymentType],
                ['item' => $item],
                ['contractor_id' => $contractorID],
            ])->one();
    }

    /**
     * @param $data
     * @param null $formName
     */
    public function _load($data, $formName = null)
    {
        if ($this->load($data, $formName)) {
            if ($this->year < 1999 || $this->year > 2999) {
                $this->year = date('Y');
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// FILTERS
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return array
     */
    public function getContractorFilterItems()
    {
        $query = clone $this->_filterQuery;

        $tContractor = Contractor::tableName();
        $tCashContractor = CashContractorType::tableName();
        $contractorIdArray = $query
            ->distinct()
            ->select("t.contractor_id")
            ->addSelect(new Expression('
              CASE 
                WHEN t.payment_type = 1 THEN t.checking_accountant_id
                WHEN t.payment_type = 2 THEN t.cashbox_id
                WHEN t.payment_type = 3 THEN t.emoney_id
              END AS wallet_id
            '))
            ->column();
        $cashContractorArray = ArrayHelper::map(
            CashContractorType::find()
                ->andWhere(["$tCashContractor.name" => $contractorIdArray])
                ->all(),
            'name',
            'text'
        );
        $contractorArray = ArrayHelper::map(
            Contractor::getSorted()
                ->andWhere(["$tContractor.id" => $contractorIdArray])
                ->all(),
            'id',
            'shortName'
        );

        return (['' => 'Все контрагенты'] + $cashContractorArray + $contractorArray);
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function getReasonFilterItems()
    {
        $query = clone $this->_filterQuery;
        $e_reasonArray = $query
            ->distinct()
            ->select(new Expression('
                CONCAT("e_", `expenditure`.`id`) `reason_id`, 
                IF (LENGTH(`expenditureParent`.`name`) > 0, CONCAT(`expenditureParent`.`name`, " - ", `expenditure`.`name`), `expenditure`.`name`) `reason_name`
            '))
            ->addSelect(new Expression('
              CASE 
                WHEN t.payment_type = 1 THEN t.checking_accountant_id
                WHEN t.payment_type = 2 THEN t.cashbox_id
                WHEN t.payment_type = 3 THEN t.emoney_id
              END AS wallet_id
            '))
            ->leftJoin(InvoiceExpenditureItem::tableName() . ' `expenditure`', '`expenditure`.`id` = `expenditure_item_id`')
            ->leftJoin(InvoiceExpenditureItem::tableName() . ' `expenditureParent`', '`expenditureParent`.`id` = `expenditure`.`parent_id`')
            ->andWhere(['not', ['expenditure.id' => null]])
            ->createCommand()
            ->queryAll();

        $query = clone $this->_filterQuery;
        $i_reasonArray = $query
            ->distinct()
            ->select(new Expression('
                CONCAT("i_", `income`.`id`) `reason_id`, 
                IF (LENGTH(`incomeParent`.`name`) > 0, CONCAT(`incomeParent`.`name`, " - ", `income`.`name`), `income`.`name`) `reason_name`
            '))
            ->addSelect(new Expression('
              CASE 
                WHEN t.payment_type = 1 THEN t.checking_accountant_id
                WHEN t.payment_type = 2 THEN t.cashbox_id
                WHEN t.payment_type = 3 THEN t.emoney_id
              END AS wallet_id
            '))
            ->leftJoin(InvoiceIncomeItem::tableName() . ' `income`', '`income`.`id` = `income_item_id`')
            ->leftJoin(InvoiceIncomeItem::tableName() . ' `incomeParent`', '`incomeParent`.`id` = `income`.`parent_id`')
            ->andWhere(['not', ['income.id' => null]])
            ->createCommand()
            ->queryAll();

        $reasonArray = ArrayHelper::map(array_merge($e_reasonArray, $i_reasonArray), 'reason_id', 'reason_name');
        natsort($reasonArray);

        return $reasonArray;
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function getProjectFilterItems()
    {
        $query = clone ($this->_filterQuery);
        $projectArray = $query
            ->distinct()
            ->select('`project_id`, `project`.`name` `project_name`')
            ->leftJoin(Project::tableName() . ' `project`', '`project`.`id` = `project_id`')
            ->orderBy('project_name')
            ->andWhere(['not', ['project_id' => null]])
            ->createCommand()
            ->queryAll();

        return ArrayHelper::map($projectArray, 'project_id', 'project_name');
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function getSalePointFilterItems()
    {
        $query = clone ($this->_filterQuery);
        $salePointArray = $query
            ->distinct()
            ->select('`sale_point_id`, `sale_point`.`name` `sale_point_name`')
            ->leftJoin(SalePoint::tableName() . ' `sale_point`', '`sale_point`.`id` = `sale_point_id`')
            ->orderBy('sale_point_name')
            ->andWhere(['not', ['sale_point_id' => null]])
            ->createCommand()
            ->queryAll();

        return ArrayHelper::map($salePointArray, 'sale_point_id', 'sale_point_name');
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function getCompanyIndustryFilterItems()
    {
        $query = clone ($this->_filterQuery);
        $salePointArray = $query
            ->distinct()
            ->select('`industry_id`, `company_industry`.`name` `company_industry_name`')
            ->leftJoin(CompanyIndustry::tableName() . ' `company_industry`', '`company_industry`.`id` = `industry_id`')
            ->orderBy('company_industry_name')
            ->createCommand()
            ->queryAll();

        return ArrayHelper::map($salePointArray, 'industry_id', 'company_industry_name');
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// CHARTS
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param $flowType
     * @param bool $plan
     * @param null $year
     * @return array
     */
    public function getChartAmount($flowType, $plan = false, $year = null)
    {
        $result = [];
        $flowArray = [];
        $year = $year ?: $this->year;
        if ($plan) {
            $flowArray[] = PlanCashFlows::className();
        } else {
            $flowArray = self::flowClassArray();
        }
        foreach ($flowArray as $flowClassName) {
            foreach (Month::$monthShort as $monthNumber => $monthName) {
                $formattedMonthNumber = $monthNumber > 9 ? $monthNumber : "0{$monthNumber}";
                if (!$plan && $this->year == $year && $monthNumber > date('m')) {
                    $amount = $flowClassName == CashEmoneyFlows::className() ? (float)PlanCashFlows::find()
                            ->andWhere(['company_id' => $this->multiCompanyIds])
                            ->andWhere(['not', ['contractor_id' => [
                                CashContractorType::BANK_TEXT,
                                CashContractorType::ORDER_TEXT,
                                CashContractorType::EMONEY_TEXT,
                                CashContractorType::BALANCE_TEXT,
                            ]]])
                            ->andWhere(['flow_type' => $flowType])
                            ->andWhere(['between', 'date', "{$year}-$formattedMonthNumber-01", "{$year}-$formattedMonthNumber-" . cal_days_in_month(CAL_GREGORIAN, $formattedMonthNumber, $year)])
                            ->sum('amount') / 100 : 0;
                } else {
                    $amount = (float)$flowClassName::find()
                            ->andWhere(['company_id' => $this->multiCompanyIds])
                            ->andWhere(['not', ['contractor_id' => [
                                CashContractorType::BANK_TEXT,
                                CashContractorType::ORDER_TEXT,
                                CashContractorType::EMONEY_TEXT,
                                CashContractorType::BALANCE_TEXT,
                            ]]])
                            ->andWhere(['flow_type' => $flowType])
                            ->andWhere(['between', 'date', "{$year}-$formattedMonthNumber-01", "{$year}-$formattedMonthNumber-" . cal_days_in_month(CAL_GREGORIAN, $formattedMonthNumber, $year)])
                            ->sum('amount') / 100;
                }
                if (isset($result[$monthNumber])) {
                    $result[$monthNumber] += $amount;
                } else {
                    $result[$monthNumber] = $amount;
                }
            }
        }
        $result = array_values($result);
        return $result;
    }

    /**
     * @param null $year
     * @return array
     */
    public function getChartReminderAmount($year = null)
    {
        $year = $year ?: $this->year;
        $oddsSearch = new FlowOfFundsReportSearch();
        $data = $oddsSearch->searchByActivity(['FlowOfFundsReportSearch' => ['year' => $year]]);

        $ret = array_map(function ($amount, $monthNumber) use ($year) {
            if ($year == date('Y') && $monthNumber > date('m')) {
                return '';
            }

            return $amount / 100;
        }, array_values($data['growingBalance']), array_keys($data['growingBalance']));

        return $ret;
    }

    public function getFromCurrentDaysPeriods($left, $right, $offsetYear = 0, $offsetDay = "0") {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} days")->modify("{$offsetDay} days");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr->format('Y-m-d'),
                'to' => $curr->format('Y-m-d'),
            ];
            $curr = $curr->modify("+1 days");
        }

        return $ret;
    }

    public function getFromCurrentMonthsPeriods($left, $right, $offsetYear = 0, $offsetMonth = "0") {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month")->modify("{$offsetMonth} month");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr->format('Y-m-01'),
                'to' => $curr->format('Y-m-t'),
            ];
            $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }

    function getChartYearPeriods($year = null) {
        $left = 1;
        $right = 12;
        $start = (new \DateTime())->modify("first day of January " . $year ?: date('Y'));
        $curr = $start->modify("-{$left} months");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr->format('Y-m-d'),
                'to' => $curr->format('Y-m-t')
            ];

            $curr = $curr->modify("+1 month");
        }

        return $ret;
    }

    function getChartPlanBalanceFuturePoint($toDate, $purse = null, $pageFilter = [])
    {
        $companyIds = implode(',', $this->multiCompanyIds);

        /** @var $planCashFlow $planCashFlow */
        $t = PlanCashFlows::tableName();
        $i = CashFlowsBase::FLOW_TYPE_INCOME;
        $select = "IF({{{$t}}}.[[flow_type]] = {$i}, {{{$t}}}.[[amount]], - {{{$t}}}.[[amount]])";
        $query = PlanCashFlows::find()
            ->select(['flow_sum' => $select])
            ->where(['company_id' => $companyIds])
            ->andFilterWhere(['payment_type' => $purse]) // !
            ->andWhere(['>=', $t.'.date', date('Y-m-d')])
            ->andWhere(['<=', $t.'.date', $toDate]);

        // pageFilters
        if (strlen($this->industry_id))
            $query->andWhere([$t.'.industry_id' => $this->industry_id ?: null]);
        if (strlen($this->sale_point_id))
            $query->andWhere([$t.'.sale_point_id' => $this->sale_point_id ?: null]);
        if (strlen($this->project_id))
            $query->andWhere([$t.'.project_id' => $this->project_id ?: null]);

        $planAmount = (new Query)->from(['flow' => $query])->sum('flow_sum');
        $factAmount = $this->getBalanceBeforeDate((new \DateTime)->format('Y-m-d'), $purse);

        return (float)bcdiv(bcadd($factAmount, $planAmount), 100, 2);
    }

    /**
     * @param array $dates
     * @param int $purse
     * @param string $period
     * @return array
     * @throws \yii\base\ErrorException
     */
    public function getPlanFactSeriesData($dates, $purse = null, $period = "days")
    {
        $resultFact = [];
        $resultPlan = [];
        $resultBalance = [];
        $resultCurrMonthPlanBalance = 0;

        $INCOME = CashFlowsBase::FLOW_TYPE_INCOME;
        $OUTCOME = CashFlowsBase::FLOW_TYPE_EXPENSE;
        $EXCLUDE_CONTRACTORS = [
            CashContractorType::BANK_TEXT,
            CashContractorType::ORDER_TEXT,
            CashContractorType::EMONEY_TEXT,
            CashContractorType::BALANCE_TEXT
        ];

        switch ($purse) {
            case self::CASH_BANK_BLOCK:
                $flowArray = [CashBankFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case self::CASH_ORDER_BLOCK:
                $flowArray = [CashOrderFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case self::CASH_EMONEY_BLOCK:
                $flowArray = [CashEmoneyFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case self::CASH_ACQUIRING_BLOCK:
                $flowArray = [AcquiringOperation::class];
                $planFlowArray = [];
                break;
            case self::CASH_CARD_BLOCK:
                $flowArray = [CardOperation::class];
                $planFlowArray = [];
                break;
            default:
                $flowArray = self::flowClassArray();
                $planFlowArray = [PlanCashFlows::class];
                break;
        }

        $dateStart = $dates[0]['from'];
        $dateEnd = $dates[count($dates)-1]['to'];

        // 0. Zeroes
        $dateKeyLength = ($period == "months") ? 7 : 10;
        foreach ($dates as $date) {

            $pos = substr($date['from'], 0, $dateKeyLength);

            $resultFact[$INCOME][$pos] = 0;
            $resultFact[$OUTCOME][$pos] = 0;
            $resultPlan[$INCOME][$pos] = 0;
            $resultPlan[$OUTCOME][$pos] = 0;
            $resultBalance[$pos] = 0;
        }

        // 1. Fact
        foreach ($flowArray as $flowClassName) {
            /** @var CashBankFlows $flowClassName */
            $query = $flowClassName::find()
                ->andWhere(['company_id' => $this->multiCompanyIds])
                ->andWhere(['between', 'date', $dateStart, $dateEnd])
                ->select(($flowClassName == CashBankFlows::class)
                    ? new Expression('IF(parent_id > 0, tin_child_amount, amount) AS amount, flow_type, date, contractor_id')
                    : ['amount', 'flow_type', 'date', 'contractor_id'])
                ->asArray();

            if ($flowClassName == CashBankFlows::class) {
                $query->andWhere(['has_tin_children' => 0]);
            }

            // pageFilters
            if (strlen($this->industry_id))
                $query->andWhere(['industry_id' => $this->industry_id ?: null]);
            if (strlen($this->sale_point_id))
                $query->andWhere(['sale_point_id' => $this->sale_point_id ?: null]);
            if (strlen($this->project_id))
                $query->andWhere(['project_id' => $this->project_id ?: null]);

            $flows = $query->all(\Yii::$app->db2);

            foreach ($flows as $f) {
                $pos = substr($f['date'], 0, $dateKeyLength);
                if (in_array($f['contractor_id'], $EXCLUDE_CONTRACTORS)) {
                    $resultBalance[$pos] += (float)bcdiv(($f['flow_type'] == $INCOME ? '' : '-') . $f['amount'], 100, 2);
                    continue;
                }

                $resultFact[$f['flow_type']][$pos] += (float)bcdiv($f['amount'], 100, 2);
            }
        }

        // 2. Plan
        foreach ($planFlowArray as $flowClassName) {
            /** @var PlanCashFlows $flowClassName */
            $query = $flowClassName::find()
                ->andWhere(['company_id' => $this->multiCompanyIds])
                ->andWhere(['not', ['contractor_id' => $EXCLUDE_CONTRACTORS]])
                ->andWhere(['between', 'date', $dateStart, $dateEnd])
                ->andFilterWhere(['payment_type' => $purse]) // !
                ->select(['amount', 'flow_type', 'date'])
                ->asArray();

            // pageFilters
            if (strlen($this->industry_id))
                $query->andWhere(['industry_id' => $this->industry_id ?: null]);
            if (strlen($this->sale_point_id))
                $query->andWhere(['sale_point_id' => $this->sale_point_id ?: null]);
            if (strlen($this->project_id))
                $query->andWhere(['project_id' => $this->project_id ?: null]);

            $flows = $query->all(\Yii::$app->db2);

            foreach ($flows as $f) {
                $pos = substr($f['date'], 0, $dateKeyLength);
                $resultPlan[$f['flow_type']][$pos] += (float)bcdiv($f['amount'], 100, 2);

                // curr month (future days) plan sum
                if ("months" == $period
                && date('Ym', strtotime($f['date'])) == date('Ym')
                && date('d', strtotime($f['date'])) >= date('d')
                ) {
                    $resultCurrMonthPlanBalance += (float)bcdiv(($f['flow_type'] == $INCOME ? '' : '-') . $f['amount'], 100, 2);
                }
            }
        }

        // 3. Balance
        $factStartAmount = (float)bcdiv($this->getBalanceBeforeDate($dateStart, $purse), 100, 2);

        $keys = array_keys($resultBalance);
        foreach ($dates as $date) {
            $pos = substr($date['from'], 0, $dateKeyLength);
            $resultBalance[$pos] += ($resultFact[$INCOME][$pos] - $resultFact[$OUTCOME][$pos]);
            if (array_search($pos, $keys) == 0)
                $resultBalance[$pos] += $factStartAmount;
            else
                $resultBalance[$pos] += $resultBalance[$keys[array_search($pos, $keys)-1]];
        }

        return [
            'incomeFlowsFact' => array_values($resultFact[$INCOME]),
            'outcomeFlowsFact' => array_values($resultFact[$OUTCOME]),
            'incomeFlowsPlan' => array_values($resultPlan[$INCOME]),
            'outcomeFlowsPlan' => array_values($resultPlan[$OUTCOME]),
            'balanceFact' => array_values($resultBalance),
            'balancePlanCurrMonth' => $resultCurrMonthPlanBalance
        ];
    }

    /**
     * @param $flowType
     * @param bool $plan
     * @param null $year
     * @return array
     */
    public function getChartAmountByDate($flowType, $dates, $plan = false, $purse = null)
    {
        $result = [];
        $flowArray = [];
        if ($plan) {
            $flowArray[] = PlanCashFlows::className();
            $purseClassName = CashEmoneyFlows::className(); // any
        } else {
            switch ($purse) {
                case self::CASH_BANK_BLOCK:
                    $purseClassName = CashBankFlows::className();
                    $flowArray = [$purseClassName];
                    break;
                case self::CASH_ORDER_BLOCK:
                    $purseClassName = CashOrderFlows::className();
                    $flowArray = [$purseClassName];
                    break;
                case self::CASH_EMONEY_BLOCK:
                    $purseClassName = CashEmoneyFlows::className();
                    $flowArray = [$purseClassName];
                    break;
                case self::CASH_ACQUIRING_BLOCK:
                    $purseClassName = AcquiringOperation::class;
                    $flowArray = [$purseClassName];
                    break;
                case self::CASH_CARD_BLOCK:
                    $purseClassName = CardOperation::class;
                    $flowArray = [$purseClassName];
                    break;
                default:
                    $purseClassName = CashEmoneyFlows::className(); // any
                    $flowArray = self::flowClassArray();
                    break;
            }
        }



        foreach ($flowArray as $flowClassName) {
            foreach ($dates as $date) {

                $dateStart = $date['from'];
                $dateEnd = $date['to'];
                $isFuturePeriod = str_replace('-', '', $dateStart) > str_replace('-', '', date('Y-m-d'));

                if (!$plan && $isFuturePeriod) {
                    $amount = $flowClassName == $purseClassName ? (float)PlanCashFlows::find()
                            ->andWhere(['company_id' => $this->multiCompanyIds])
                            ->andWhere(['not', ['contractor_id' => [
                                CashContractorType::BANK_TEXT,
                                CashContractorType::ORDER_TEXT,
                                CashContractorType::EMONEY_TEXT,
                                CashContractorType::BALANCE_TEXT,
                            ]]])
                            ->andWhere(['flow_type' => $flowType])
                            ->andWhere(['between', 'date', $dateStart, $dateEnd])
                            ->andFilterWhere(['payment_type' => $purse])
                            ->sum('amount') / 100 : 0;
                } elseif ($plan && $isFuturePeriod) {
                    $result[$dateStart] = "";
                    continue;
                } elseif ($plan) {

                    $amount = (float)$flowClassName::find()
                            ->andWhere(['company_id' => $this->multiCompanyIds])
                            ->andWhere(['not', ['contractor_id' => [
                                CashContractorType::BANK_TEXT,
                                CashContractorType::ORDER_TEXT,
                                CashContractorType::EMONEY_TEXT,
                                CashContractorType::BALANCE_TEXT,
                            ]]])
                            ->andWhere(['flow_type' => $flowType])
                            ->andWhere(['between', 'date', $dateStart, $dateEnd])
                            ->andFilterWhere(['payment_type' => $purse])
                            ->sum('amount') / 100;

                } else {
                    $amount = (float)$flowClassName::find()
                            ->andWhere(['company_id' => $this->multiCompanyIds])
                            ->andWhere(['not', ['contractor_id' => [
                                CashContractorType::BANK_TEXT,
                                CashContractorType::ORDER_TEXT,
                                CashContractorType::EMONEY_TEXT,
                                CashContractorType::BALANCE_TEXT,
                            ]]])
                            ->andWhere(['flow_type' => $flowType])
                            ->andWhere(['between', 'date', $dateStart, $dateEnd])
                            ->sum('amount') / 100;
                }
                if (isset($result[$dateStart])) {
                    $result[$dateStart] += $amount;
                } else {
                    $result[$dateStart] = $amount;
                }
            }
        }
        $result = array_values($result);

        return $result;
    }

    /**
     * С учетом области видимости кассы: руководитель/бухгалтер/...
     * @param $dates
     * @return array
     * @throws \yii\base\ErrorException
     */
    public function getChartBalanceByDateFact($dates, $prevPeriods = false, $purse = null)
    {
        $result = [];

        if ($prevPeriods) {
            $factAmount = (float)bcdiv(array_sum(
                array_column($this->getPeriodChartInfo([
                    'from' => new \DateTime(),
                    'to' => new \DateTime(),
                ], $purse), 'endBalance')), 100, 2);
        }

        foreach ($dates as $date) {

            $dateStart = $date['from'];
            $dateEnd = $date['to'];
            $isPlanDate = str_replace('-', '', $dateStart) > str_replace('-', '', date('Y-m-d'));

            if ($isPlanDate) {
                $result[] = ($prevPeriods) ? $factAmount : 0;
                continue;
            } else {
                $amount = 0;
                $stat = $this->getPeriodChartInfo([
                    'from' => date_create_from_format('Y-m-d', $dateStart),
                    'to' => date_create_from_format('Y-m-d', $dateEnd),
                ], $purse);
                foreach ($stat as $v) {
                    $amount += $v['endBalance'];
                }
                $result[] = $amount / 100;
            }
        }

        return $result;
    }

    /**
     * Плановые операции
     * @param $dates
     * @param bool $plan
     * @param bool $hideFuturePeriods
     * @return array
     */
    public function getChartBalanceByDatePlan($dates, $plan = false, $hideFuturePeriods = false)
    {
        $oddsSearch = new FlowOfFundsReportSearch();
        $data = $oddsSearch->searchByActivityByDate($dates, $plan);

        $ret = [];
        foreach ($data['growingBalance'] as $date => $amount) {
            $isMoreThenCurrent = str_replace('-', '', $date) > str_replace('-', '', date('Y-m-d'));
            if (!$plan && $isMoreThenCurrent && $hideFuturePeriods) {
                $ret[] = '';
            } else {
                $ret[] = round($amount / 100);
            }
        }

        return $ret;
    }

    // todo: remove this method! 28-02-2021
    public function getPeriodChartInfo($period = [], $purse = null)
    {
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $periodFrom = \yii\helpers\ArrayHelper::getValue($period, 'from');
        $periodTo = ArrayHelper::getValue($period, 'to');

        if (!$purse || $purse == 1)
            $data[] = [
                'beginBalance' => (new CashBankFlows())->getBalanceAtBegin($periodFrom, $company),
                'endBalance' => (new CashBankFlows())->getBalanceAtEnd($periodTo, $company)
            ];
        if (!$purse || $purse == 2)
            $data[] = [
                'beginBalance' => (new CashOrderFlows())->getBalanceAtBegin($periodFrom, $company),
                'endBalance' => (new CashOrderFlows())->getBalanceAtEnd($periodTo, $company)
            ];
        if (!$purse || $purse == 3)
            $data[] = [
                'beginBalance' => (new CashEmoneyFlows())->getBalanceAtBegin($periodFrom, $company),
                'endBalance' => (new CashEmoneyFlows())->getBalanceAtEnd($periodTo, $company)
            ];

        return $data;

    }

    private static function _balanceBeforeDateQuery($table, $companyIds, $date, $pageFilters) {

        foreach (['industry_id', 'sale_point_id', 'project_id'] as $attr) {
            if ($pageFilters[$attr] === 0)
                $sqlPageFilter = "t.{$attr} IS NULL";
            elseif ($pageFilters[$attr] > 0)
                $sqlPageFilter = "t.{$attr} = {$pageFilters[$attr]}";
        }

        $sqlPageFilter = $sqlPageFilter ?? '1=1';

        return Yii::$app->db2->createCommand("
                        SELECT SUM(IF(t.flow_type = 0, -t.amount, t.amount)) net_amount
                        FROM `{$table}` t
                        WHERE t.company_id IN ({$companyIds}) AND t.date < '{$date}' AND {$sqlPageFilter}
                    ")->queryScalar() ?: 0;
    }

    public function getBalanceBeforeDate($date, $purse)
    {
        $companyIds = implode(',', $this->multiCompanyIds);

        $pageFilters = [
            'industry_id' => (strlen($this->industry_id)) ? (int)$this->industry_id : -1,
            'sale_point_id' => (strlen($this->sale_point_id)) ? (int)$this->sale_point_id : -1,
            'project_id' => (strlen($this->project_id)) ? (int)$this->project_id : -1
        ];

        $sum = [];
        if (!$purse || $purse == self::CASH_BANK_BLOCK)
            $sum[] = self::_balanceBeforeDateQuery(CashBankFlows::tableName(), $companyIds, $date, $pageFilters);
        if (!$purse || $purse == self::CASH_ORDER_BLOCK)
            $sum[] = self::_balanceBeforeDateQuery(CashOrderFlows::tableName(), $companyIds, $date, $pageFilters);
        if (!$purse || $purse == self::CASH_EMONEY_BLOCK)
            $sum[] = self::_balanceBeforeDateQuery(CashEmoneyFlows::tableName(), $companyIds, $date, $pageFilters);

        return array_sum($sum);
    }

    /**
     * @param bool $name
     * @return array
     */
    public function getTooltipStructureByContractors($dateFrom, $dateTo, $purse = null, $flowType = 0)
    {
        $amountsFact = [];
        $amountsPlan = [];
        $countsFact = [];
        $countsPlan = [];

        $exceptContractors = [
            CashContractorType::BANK_TEXT,
            CashContractorType::ORDER_TEXT,
            CashContractorType::EMONEY_TEXT,
            CashContractorType::BALANCE_TEXT,
            CashContractorType::COMPANY_TEXT,
        ];

        $articleAttr = ($flowType == 0) ? 'expenditure_item_id' : 'income_item_id';

        switch ($purse) {
            case self::CASH_BANK_BLOCK:
                $flowArray = [CashBankFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case self::CASH_ORDER_BLOCK:
                $flowArray = [CashOrderFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case self::CASH_EMONEY_BLOCK:
                $flowArray = [CashEmoneyFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case self::CASH_ACQUIRING_BLOCK:
                $flowArray = [AcquiringOperation::class];
                $planFlowArray = [];
                break;
            case self::CASH_CARD_BLOCK:
                $flowArray = [CardOperation::class];
                $planFlowArray = [];
                break;
            default:
                $flowArray = self::flowClassArray();
                $planFlowArray = [PlanCashFlows::class];
                break;
        }

        // 1. Fact
        foreach ($flowArray as $flowClassName) {
            $flows = $flowClassName::find()
                ->select(['contractor_id', 'amount', 'date'])
                ->andWhere(['company_id' => $this->multiCompanyIds])
                ->andWhere(['between', 'date', $dateFrom, $dateTo])
                ->andWhere(['not', ['contractor_id' => $exceptContractors]])
                ->andWhere(['flow_type' => $flowType])
                ->andWhere(['not', [$articleAttr => ($flowType == 0) ? InvoiceExpenditureItem::ITEM_OWN_FOUNDS : InvoiceIncomeItem::ITEM_OWN_FOUNDS]])
                ->asArray()
                ->all(\Yii::$app->db2);

            foreach ($flows as $f) {
                $DATE = $f['date'];
                if (!isset($amountsFact[$DATE])) {
                    $amountsFact[$DATE] = [];
                    $countsFact[$DATE] = [];
                }
                if (!isset($amountsFact[$DATE][$f['contractor_id']])) {
                    $amountsFact[$DATE][$f['contractor_id']] = 0;
                    $countsFact[$DATE][$f['contractor_id']] = 0;
                }
                $amountsFact[$DATE][$f['contractor_id']] += $f['amount'];
                $countsFact[$DATE][$f['contractor_id']] += 1;
            }
        }

        // 2. Plan
        foreach ($planFlowArray as $flowClassName) {
            $flows = $flowClassName::find()
                ->select(['contractor_id', 'amount', 'date'])
                ->andWhere(['company_id' => $this->multiCompanyIds])
                ->andWhere(['flow_type' => $flowType])
                ->andWhere(['between', 'date', $dateFrom, $dateTo])
                ->andWhere(['not', ['contractor_id' => $exceptContractors]])
                ->andWhere(['not', [$articleAttr => ($flowType == 0) ? InvoiceExpenditureItem::ITEM_OWN_FOUNDS : InvoiceIncomeItem::ITEM_OWN_FOUNDS]])
                ->andFilterWhere(['payment_type' => $purse]) // !
                ->asArray()
                ->all(\Yii::$app->db2);

            foreach ($flows as $f) {
                $DATE = $f['date'];
                if (!isset($amountsPlan[$DATE])) {
                    $amountsPlan[$DATE] = [];
                    $countsPlan[$DATE] = [];
                }
                if (!isset($amountsPlan[$DATE][$f['contractor_id']])) {
                    $amountsPlan[$DATE][$f['contractor_id']] = 0;
                    $countsPlan[$DATE][$f['contractor_id']] = 0;
                }
                $amountsPlan[$DATE][$f['contractor_id']] += $f['amount'];
                $countsPlan[$DATE][$f['contractor_id']] += 1;
            }
        }

        // 3. Names + merge amounts
        $items = [];
        foreach ($amountsFact as $date => $itemsAmounts) {
            $items[$date] = [];
            foreach ($itemsAmounts as $itemId => $amount) {
                $items[$date][$itemId] = [
                    'date' => $date,
                    'name' => ($itemModel = Contractor::findOne($itemId)) ? $itemModel->getTitle(true) : '---',
                    'amountFact' => round($amount / 100, 2),
                    'amountPlan' => 0,
                    'countFact' => $countsFact[$date][$itemId],
                    'countPlan' => 0
                ];
            }
        }
        foreach ($amountsPlan as $date => $itemsAmounts) {
            if (!isset($items[$date]))
                $items[$date] = [];
            foreach ($itemsAmounts as $itemId => $amount) {
                if (!isset($items[$date][$itemId])) {
                    $items[$date][$itemId] = [
                        'date' => $date,
                        'name' => ($itemModel = Contractor::findOne($itemId)) ? $itemModel->getTitle(true) : '---',
                        'amountFact' => 0,
                        'amountPlan' => 0,
                        'countFact' => 0,
                        'countPlan' => 0
                    ];
                }
                $items[$date][$itemId]['amountPlan'] = round($amount / 100, 2);
                $items[$date][$itemId]['countPlan'] = $countsPlan[$date][$itemId];
            }
        }

        foreach ($items as $date => &$itemsByDay) {
            uasort($itemsByDay, function ($a, $b) {
                if ($a['amountFact'] == $b['amountFact'])
                    return $a['amountPlan'] > $b['amountPlan'] ? -1 : 1;

                return ($a['amountFact'] > $b['amountFact']) ? -1 : 1;
            });
        }

        return $items;
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// NEW METHODS 2020
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function search2($type, $params = [], $byDays = false)
    {
        if ($type == self::TAB_BY_PURSE_DETAILED) {
            return $this->searchByPurseDetailed2($byDays);
        } elseif ($type == self::TAB_BY_INDUSTRY) {
            return $this->searchByFlowAttribute2($byDays, ['attr' => 'industry_id']);
        } elseif ($type == self::TAB_BY_SALE_POINT) {
            return $this->searchByFlowAttribute2($byDays, ['attr' => 'sale_point_id']);
        } elseif ($type == self::TAB_BY_PURSE) {
            return $this->searchByPurse2($byDays);
        }  elseif ($type == self::TAB_BY_ACTIVITY) {
            return $this->searchByActivity2($byDays);
        } elseif ($type == self::TAB_CALENDAR) {
            return $this->searchByPaymentCalendar();
        }

        throw new Exception('Invalid type param.');
    }

    public function searchByActivity2($byDays)
    {
        $rawData = $this->__getByActivityPlanData();
        $data = AFH::getByActivityEmptyData($this->year, $byDays);
        $totalData = AFH::getEmptyTotalData($this->year, $byDays);
        //
        $growingData = AFH::getByActivityEmptyGrowingData($this->year, $byDays);
        $currMonthFactPart = $currMonthPlanPart = array_fill_keys(array_keys($growingData), 0);
        $currMonth = (int)date('m');
        $currDay = (int)date('d');

        $subArticles = [
            CashFlowsBase::FLOW_TYPE_INCOME => AbstractFinance::getArticles2Parents(CashFlowsBase::FLOW_TYPE_INCOME, $this->company->id),
            CashFlowsBase::FLOW_TYPE_EXPENSE => AbstractFinance::getArticles2Parents(CashFlowsBase::FLOW_TYPE_EXPENSE, $this->company->id),
        ];

        // months
        foreach ($rawData as $d) {

            $type = AFH::getActivityBlockType($d['type'], $d['item_id']);
            $wallet = AbstractFinance::$blockByType[$type];
            $item = $d['item_id'];
            $month = substr($d['date'], 5, 2);
            $day = substr($d['date'], 8, 2);
            $x = ($byDays) ? ($month.$day) : ($month);
            $flowKoef = ($d['type'] == 0 ? -1 : 1);
            $amount = $d['amount'];

            $subItem = null;
            if (isset($subArticles[$d['type']][$d['item_id']])) {
                $item = $subArticles[$d['type']][$d['item_id']];
                $subItem = $d['item_id'];
            }

            if (!isset($data[$wallet]['levels'][$type]['levels'][$item])) {
                $data[$wallet]['levels'][$type]['levels'][$item] = [
                    'title' => AFH::getCashContractorName($d['type'], $item),
                    'data' => ($byDays) ? AFH::getEmptyYearInDays($this->year) : AFH::EMPTY_YEAR,
                    'levels' => []
                ];
            }

            if ($subItem) {
                if (!isset($data[$wallet]['levels'][$type]['levels'][$item]['levels'][$subItem])) {
                    $data[$wallet]['levels'][$type]['levels'][$item]['levels'][$subItem] = [
                        'title' => AFH::getCashContractorName($d['type'], $subItem),
                        'data' => ($byDays) ? AFH::getEmptyYearInDays($this->year) : AFH::EMPTY_YEAR
                    ];
                }
                $data[$wallet]['levels'][$type]['levels'][$item]['levels'][$subItem]['data'][$x] += $amount;
            }

            $data[$wallet]['levels'][$type]['levels'][$item]['data'][$x] += $amount;
            $data[$wallet]['levels'][$type]['data'][$x] += $amount;
            $data[$wallet]['data'][$x] += $flowKoef * $amount;
            $totalData['month']['data'][$x] += $flowKoef * $amount;

            if ($this->isCurrentYear && $x == $currMonth && $day > $currDay) {
                $currMonthPlanPart[$wallet] += $flowKoef * $amount;
            }
        }

        // growing
        if ($this->isCurrentYear) {
            $todayGrowingFactRawData = $this->__getByActivityTodayGrowingFact();
            $currMonth = date('m');
            // fact
            foreach ($todayGrowingFactRawData as $d) {
                $type = AFH::getActivityBlockType($d['type'], $d['item_id']);
                $wallet = AbstractFinance::$blockByType[$type];
                $flowKoef = ($d['type'] == 0 ? -1 : 1);
                $currMonthFactPart[$wallet] += $flowKoef * $d['growing_amount'];
            }
            // plan
            foreach ($data as $wallet => $d) {
                $prevAmount = 0;
                $_planPartCalculated = 0;
                foreach ($d['data'] as $x => $amount) {
                    $month = ($byDays) ? substr($x, 0, 2) : $x;
                    $day = ($byDays) ? substr($x, 2, 2) : null;
                    if ($month == $currMonth && (!$byDays || $day == $currDay)) {
                        $growingData[$wallet]['data'][$x] += $currMonthFactPart[$wallet] ?? 0;
                        $totalData['growing']['data'][$x] += $currMonthFactPart[$wallet] ?? 0;
                    } else {
                        if (!$_planPartCalculated && (($byDays && $x > ($currMonth.$currDay)) || (!$byDays && $month > $currMonth))) {
                            $growingData[$wallet]['data'][$x] += $currMonthPlanPart[$wallet] ?? 0;
                            $totalData['growing']['data'][$x] += $currMonthPlanPart[$wallet] ?? 0;
                            $_planPartCalculated = true;
                        }
                        $growingData[$wallet]['data'][$x] += $amount;
                        $totalData['growing']['data'][$x] += $amount;
                        $growingData[$wallet]['data'][$x] += $prevAmount;
                        $totalData['growing']['data'][$x] += $prevAmount;
                    }

                    $prevAmount = $growingData[$wallet]['data'][$x];
                }
            }
        }

        // sort
        foreach ($data as &$d1)
            foreach ($d1['levels'] as &$d2)
                uasort($d2['levels'], function ($a, $b) {
                    if ($a['title'] == $b['title']) return 0;
                    return ($a['title'] < $b['title']) ? -1 : 1;
                });

        return [
            'data' => $data,
            'totalData' => $totalData,
            'growingData' => $growingData
        ];
    }

    public function searchByPurse2($byDays)
    {
        $rawData = $this->__getByPursePlanData();
        $data = AFH::getByPurseEmptyData($this->year, $byDays);
        $totalData = AFH::getEmptyTotalData($this->year, $byDays);
        //
        $growingData = AFH::getByPurseEmptyGrowingData($this->year, $byDays);
        $currMonthFactPart = $currMonthPlanPart = array_fill_keys(array_keys($growingData), 0);
        $currMonth = (int)date('m');
        $currDay = (int)date('d');

        $subArticles = [
            CashFlowsBase::FLOW_TYPE_INCOME => AbstractFinance::getArticles2Parents(CashFlowsBase::FLOW_TYPE_INCOME, $this->company->id),
            CashFlowsBase::FLOW_TYPE_EXPENSE => AbstractFinance::getArticles2Parents(CashFlowsBase::FLOW_TYPE_EXPENSE, $this->company->id),
        ];

        // months
        foreach ($rawData as $d) {

            $wallet = $d['wallet'];
            $pos = ($d['type'] == 0 ? 1 : 0) + (($d['wallet'] - 1) * 2) + 1;
            $item = $d['item_id'];
            $month = substr($d['date'], 5, 2);
            $day = substr($d['date'], 8, 2);
            $x = ($byDays) ? ($month.$day) : ($month);
            $flowKoef = ($d['type'] == 0 ? -1 : 1);
            $amount = $d['amount'];

            $subItem = null;
            if (isset($subArticles[$d['type']][$d['item_id']])) {
                $item = $subArticles[$d['type']][$d['item_id']];
                $subItem = $d['item_id'];
            }

            if (in_array($d['contractor_id'], ['bank', 'order', 'emoney'])) {
                $item = $d['contractor_id'];
            }

            if (!isset($data[$wallet]['levels'][$pos]['levels'][$item])) {
                $data[$wallet]['levels'][$pos]['levels'][$item] = [
                    'title' => AFH::getCashContractorName($d['type'], $item, $d['contractor_id']),
                    'data' => ($byDays) ? AFH::getEmptyYearInDays($this->year) : AFH::EMPTY_YEAR
                ];
            }

            if ($subItem) {
                if (!isset($data[$wallet]['levels'][$pos]['levels'][$item]['levels'][$subItem])) {
                    $data[$wallet]['levels'][$pos]['levels'][$item]['levels'][$subItem] = [
                        'title' => AFH::getCashContractorName($d['type'], $subItem),
                        'data' => ($byDays) ? AFH::getEmptyYearInDays($this->year) : AFH::EMPTY_YEAR
                    ];
                }
                $data[$wallet]['levels'][$pos]['levels'][$item]['levels'][$subItem]['data'][$x] += $amount;
            }

            $data[$wallet]['levels'][$pos]['levels'][$item]['data'][$x] += $amount;
            $data[$wallet]['levels'][$pos]['data'][$x] += $amount;
            $data[$wallet]['data'][$x] += $flowKoef * $amount;
            $totalData['month']['data'][$x] += $flowKoef * $amount;

            if ($this->isCurrentYear && $month == $currMonth && $day > $currDay) {
                $currMonthPlanPart[$wallet] += $flowKoef * $amount;
            }
        }

        // growing
        if ($this->isCurrentYear) {
            $todayGrowingFactRawData = $this->__getByPurseTodayGrowingFact();
            // fact
            foreach ($todayGrowingFactRawData as $d) {
                $wallet = $d['wallet'];
                $flowKoef = ($d['type'] == 0 ? -1 : 1);
                $currMonthFactPart[$wallet] += $flowKoef * $d['growing_amount'];
            }
            // plan
            foreach ($data as $wallet => $d) {
                $prevAmount = 0;
                $_planPartCalculated = 0;
                foreach ($d['data'] as $x => $amount) {
                    $month = ($byDays) ? substr($x, 0, 2) : $x;
                    $day = ($byDays) ? substr($x, 2, 2) : null;
                    if ($month == $currMonth && (!$byDays || $day == $currDay)) {
                        $growingData[$wallet]['data'][$x] += $currMonthFactPart[$wallet] ?? 0;
                        $totalData['growing']['data'][$x] += $currMonthFactPart[$wallet] ?? 0;
                    } else {
                        if (!$_planPartCalculated && (($byDays && $x > ($currMonth.$currDay)) || (!$byDays && $month > $currMonth))) {
                            $growingData[$wallet]['data'][$x] += $currMonthPlanPart[$wallet] ?? 0;
                            $totalData['growing']['data'][$x] += $currMonthPlanPart[$wallet] ?? 0;
                            $_planPartCalculated = true;
                        }
                        $growingData[$wallet]['data'][$x] += $amount;
                        $totalData['growing']['data'][$x] += $amount;
                        $growingData[$wallet]['data'][$x] += $prevAmount;
                        $totalData['growing']['data'][$x] += $prevAmount;
                    }

                    $prevAmount = $growingData[$wallet]['data'][$x];
                }
            }
        }

        // sort
        foreach ($data as &$d1)
            foreach ($d1['levels'] as &$d2)
                uasort($d2['levels'], function ($a, $b) {
                    if ($a['title'] == $b['title']) return 0;
                    return ($a['title'] < $b['title']) ? -1 : 1;
                });

        return [
            'data' => $data,
            'totalData' => $totalData,
            'growingData' => $growingData
        ];
    }

    public function searchByPurseDetailed2($byDays)
    {
        $rawData = $this->__getByPursePlanDataDetailed();
        $data = AFH::getByPurseEmptyDataDetailed($rawData, $this->year, $byDays);
        $totalData = AFH::getEmptyTotalData($this->year, $byDays);
        //
        $growingData = AFH::getByPurseEmptyGrowingDataDetailed($rawData, $this->year, $byDays);
        $currMonthFactPart = $currMonthPlanPart = [];
        $currMonth = (int)date('m');
        $currDay = (int)date('d');

        $subArticles = [
            CashFlowsBase::FLOW_TYPE_INCOME => AbstractFinance::getArticles2Parents(CashFlowsBase::FLOW_TYPE_INCOME, $this->company->id),
            CashFlowsBase::FLOW_TYPE_EXPENSE => AbstractFinance::getArticles2Parents(CashFlowsBase::FLOW_TYPE_EXPENSE, $this->company->id),
        ];

        foreach ($rawData as $d) {

            $walletTypeID = $d['wallet'];
            $walletID = $d['wallet_id'];
            $type = $d['type'];
            $item = $d['item_id'];
            $month = $d['m'];
            $day = $d['d'];
            $x = ($byDays) ? ($month.$day) : ($month);
            $flowKoef = ($d['type'] == 0 ? -1 : 1);
            $amount = $d['amount'];
            $growingAmount = $d['growing_amount'];

            if (in_array($d['contractor_id'], ['bank', 'order', 'emoney'])) {
                $item = $d['contractor_id'];
            }

            $subItem = null;
            if (isset($subArticles[$d['type']][$d['item_id']])) {
                $item = $subArticles[$d['type']][$d['item_id']];
                $subItem = $d['item_id'];
            }

            if (!isset($data[$walletTypeID]['children'][$walletID]['levels'][$type]['levels'][$item])) {
                $data[$walletTypeID]['children'][$walletID]['levels'][$type]['levels'][$item] = [
                    'title' => AFH::getCashContractorName($d['type'], $item, $d['contractor_id']),
                    'data' => ($byDays) ? AFH::getEmptyYearInDays($this->year) : AFH::EMPTY_YEAR
                ];
            }

            if ($subItem) {
                if (!isset( $data[$walletTypeID]['children'][$walletID]['levels'][$type]['levels'][$item]['levels'][$subItem])) {
                    $data[$walletTypeID]['children'][$walletID]['levels'][$type]['levels'][$item]['levels'][$subItem] = [
                        'title' => AFH::getCashContractorName($d['type'], $subItem),
                        'data' => ($byDays) ? AFH::getEmptyYearInDays($this->year) : AFH::EMPTY_YEAR
                    ];
                }
                $data[$walletTypeID]['children'][$walletID]['levels'][$type]['levels'][$item]['levels'][$subItem]['data'][$x] += $amount;
            }

            $data[$walletTypeID]['data'][$x] += $flowKoef * $amount;
            $data[$walletTypeID]['children'][$walletID]['data'][$x] += $flowKoef * $amount;
            $data[$walletTypeID]['children'][$walletID]['levels'][$type]['data'][$x] += $amount;
            $data[$walletTypeID]['children'][$walletID]['levels'][$type]['levels'][$item]['data'][$x] += $amount;

            //$growingData[$walletTypeID]['data'][$x] += $flowKoef * $growingAmount;
            //$growingData[$walletTypeID]['children'][$walletID]['data'][$x] += $flowKoef * $growingAmount;

            $totalData['month']['data'][$x] += $flowKoef * $amount;
            //$totalData['growing']['data'][$x] += $flowKoef * $growingAmount;

            if ($this->isCurrentYear && $month == $currMonth && $day > $currDay) {
                if (!isset($currMonthPlanPart[$walletTypeID][$walletID])) {
                    $currMonthPlanPart[$walletTypeID][$walletID] = 0;
                }
                $currMonthPlanPart[$walletTypeID][$walletID] += $flowKoef * $amount;
            }
        }

        // todo: growing
        if ($this->isCurrentYear) {
            $todayGrowingFactRawData = $this->__getByPurseTodayGrowingFactDetailed();
            // fact
            foreach ($todayGrowingFactRawData as $d) {
                $walletTypeID = $d['wallet'];
                $walletID = $d['wallet_id'];
                if (!isset($currMonthFactPart[$walletTypeID])) {
                    $currMonthFactPart[$walletTypeID] = [];
                }
                if (!isset($currMonthFactPart[$walletTypeID][$walletID])) {
                    $currMonthFactPart[$walletTypeID][$walletID] = 0;
                }

                $currMonthFactPart[$walletTypeID][$walletID] += $d['amount'];
            }

            // plan
            foreach ($data as $walletTypeID => $da) {
                foreach ($da['children'] as $walletID => $d) {
                    $prevAmount = 0;
                    $_planPartCalculated = false;
                    foreach ($d['data'] as $x => $amount) {
                        $month = ($byDays) ? substr($x, 0, 2) : $x;
                        $day = ($byDays) ? substr($x, 2, 2) : null;
                        $_currAmountFact = $currMonthFactPart[$walletTypeID][$walletID] ?? 0;
                        $_currAmountPlan = $currMonthPlanPart[$walletTypeID][$walletID] ?? 0;
                        
                        if ($month == $currMonth && (!$byDays || $day == $currDay)) {
                            $growingData[$walletTypeID]['children'][$walletID]['data'][$x] += $_currAmountFact;
                            $growingData[$walletTypeID]['data'][$x] += $_currAmountFact;
                            $totalData['growing']['data'][$x] += $_currAmountFact;
                        } else {
                            if (!$_planPartCalculated && (($byDays && $x > ($currMonth . $currDay)) || (!$byDays && $month > $currMonth))) {
                                $growingData[$walletTypeID]['children'][$walletID]['data'][$x] += $_currAmountPlan;
                                $totalData['growing']['data'][$x] += $_currAmountPlan;
                                $_planPartCalculated = true;
                            }
                            $growingData[$walletTypeID]['children'][$walletID]['data'][$x] += $amount;
                            $totalData['growing']['data'][$x] += $amount;
                            $growingData[$walletTypeID]['children'][$walletID]['data'][$x] += $prevAmount;
                            $totalData['growing']['data'][$x] += $prevAmount;
                        }

                        $prevAmount = $growingData[$walletTypeID]['children'][$walletID]['data'][$x];
                    }
                }
            }
        }

        // sort
        foreach ($data as $k0 => $d0)
            foreach ($d0['children'] as $k1 => $d1)
                foreach ($d1['levels'] as $k2 => $d2)
                    uasort($data[$k0]['children'][$k1]['levels'][$k2]['levels'], function ($a, $b) {
                        if ($a['title'] == $b['title']) return 0;
                        return ($a['title'] < $b['title']) ? -1 : 1;
                    });

        return [
            'data' => $data,
            'totalData' => $totalData,
            'growingData' => $growingData
        ];
    }

    public function searchByFlowAttribute2($byDays, $params)
    {
        $theAttr = $params['attr'] ?? 'wallet';

        $rawData = $this->__getByFlowAttributePlanData($params);
        $data = AFH::getByFlowAttributeEmptyData($this->year, $byDays, $params + ['company_id' => $this->company->id]);
        $totalData = AFH::getEmptyTotalData($this->year, $byDays);
        //
        $growingData = AFH::getByFlowAttributeEmptyData($this->year, $byDays, $params + ['is_growing' => true, 'company_id' => $this->company->id]);
        $currMonthFactPart = $currMonthPlanPart = array_fill_keys(array_keys($growingData), 0);
        $currMonth = (int)date('m');
        $currDay = (int)date('d');

        $subArticles = [
            CashFlowsBase::FLOW_TYPE_INCOME => AbstractFinance::getArticles2Parents(CashFlowsBase::FLOW_TYPE_INCOME, $this->company->id),
            CashFlowsBase::FLOW_TYPE_EXPENSE => AbstractFinance::getArticles2Parents(CashFlowsBase::FLOW_TYPE_EXPENSE, $this->company->id),
        ];

        // months
        foreach ($rawData as $d) {

            $wallet = $d[$theAttr];
            $pos = $d['type'];
            $item = $d['item_id'];
            $month = substr($d['date'], 5, 2);
            $day = substr($d['date'], 8, 2);
            $x = ($byDays) ? ($month.$day) : ($month);
            $flowKoef = ($d['type'] == 0 ? -1 : 1);
            $amount = $d['amount'];

            $subItem = null;
            if (isset($subArticles[$d['type']][$d['item_id']])) {
                $item = $subArticles[$d['type']][$d['item_id']];
                $subItem = $d['item_id'];
            }

            if (in_array($d['contractor_id'], ['bank', 'order', 'emoney'])) {
                $item = $d['contractor_id'];
            }

            if (!isset($data[$wallet]['levels'][$pos]['levels'][$item])) {
                $data[$wallet]['levels'][$pos]['levels'][$item] = [
                    'title' => AFH::getCashContractorName($d['type'], $item, $d['contractor_id']),
                    'data' => ($byDays) ? AFH::getEmptyYearInDays($this->year) : AFH::EMPTY_YEAR
                ];
            }

            if ($subItem) {
                if (!isset($data[$wallet]['levels'][$pos]['levels'][$item]['levels'][$subItem])) {
                    $data[$wallet]['levels'][$pos]['levels'][$item]['levels'][$subItem] = [
                        'title' => AFH::getCashContractorName($d['type'], $subItem),
                        'data' => ($byDays) ? AFH::getEmptyYearInDays($this->year) : AFH::EMPTY_YEAR
                    ];
                }
                $data[$wallet]['levels'][$pos]['levels'][$item]['levels'][$subItem]['data'][$x] += $amount;
            }

            $data[$wallet]['levels'][$pos]['levels'][$item]['data'][$x] += $amount;
            $data[$wallet]['levels'][$pos]['data'][$x] += $amount;
            $data[$wallet]['data'][$x] += $flowKoef * $amount;
            $totalData['month']['data'][$x] += $flowKoef * $amount;

            if ($this->isCurrentYear && $month == $currMonth && $day > $currDay) {
                $currMonthPlanPart[$wallet] += $flowKoef * $amount;
            }
        }

        // growing
        if ($this->isCurrentYear) {
            $todayGrowingFactRawData = $this->__getByFlowAttributeTodayGrowingFact($params);
            // fact
            foreach ($todayGrowingFactRawData as $d) {
                $wallet = $d[$theAttr];
                $flowKoef = ($d['type'] == 0 ? -1 : 1);
                $currMonthFactPart[$wallet] += $flowKoef * $d['growing_amount'];
            }
            // plan
            foreach ($data as $wallet => $d) {
                $prevAmount = 0;
                $_planPartCalculated = 0;
                foreach ($d['data'] as $x => $amount) {
                    $month = ($byDays) ? substr($x, 0, 2) : $x;
                    $day = ($byDays) ? substr($x, 2, 2) : null;
                    if ($month == $currMonth && (!$byDays || $day == $currDay)) {
                        $growingData[$wallet]['data'][$x] += $currMonthFactPart[$wallet] ?? 0;
                        $totalData['growing']['data'][$x] += $currMonthFactPart[$wallet] ?? 0;
                    } else {
                        if (!$_planPartCalculated && (($byDays && $x > ($currMonth.$currDay)) || (!$byDays && $month > $currMonth))) {
                            $growingData[$wallet]['data'][$x] += $currMonthPlanPart[$wallet] ?? 0;
                            $totalData['growing']['data'][$x] += $currMonthPlanPart[$wallet] ?? 0;
                            $_planPartCalculated = true;
                        }
                        $growingData[$wallet]['data'][$x] += $amount;
                        $totalData['growing']['data'][$x] += $amount;
                        $growingData[$wallet]['data'][$x] += $prevAmount;
                        $totalData['growing']['data'][$x] += $prevAmount;
                    }

                    $prevAmount = $growingData[$wallet]['data'][$x];
                }
            }
        }

        // sort
        foreach ($data as &$d1)
            foreach ($d1['levels'] as &$d2)
                uasort($d2['levels'], function ($a, $b) {
                    if ($a['title'] == $b['title']) return 0;
                    return ($a['title'] < $b['title']) ? -1 : 1;
                });

        return [
            'data' => $data,
            'totalData' => $totalData,
            'growingData' => $growingData
        ];
    }

    public function searchByPaymentCalendar($fromDate = null, $toDate = null)
    {
        if (!$fromDate || !$toDate)
            return [];

        $table = PlanCashFlows::tableName();
        //$companyIds = implode(',', $this->multiCompanyIds);
        $companyId = $this->company->id; // todo: change after 20-281

        $data = Yii::$app->db2->createCommand("
            SELECT
              t.id, 
              t.company_id, 
              t.flow_type AS type,
              t.contractor_id,
              CONCAT_WS(' ', ct.name_short, c.name) AS contractor_name, 
              DATE_FORMAT(t.date, '%Y%m%d') AS `ymd`,
              SUM(t.amount) amount
            FROM {$table} t
            LEFT JOIN `contractor` c ON t.contractor_id = c.id
            LEFT JOIN `company_type` ct ON c.company_type_id = ct.id
            WHERE
              t.company_id IN ({$companyId})
              AND t.date BETWEEN '{$fromDate}' AND '{$toDate}'
            GROUP BY t.`id`
          ")->queryAll();

        $ret = [];
        foreach ($data as $d) {
            if (!isset($ret[$d['ymd']])) { $ret[$d['ymd']] = []; }

            $ret[$d['ymd']][] = [
                'id' => $d['id'],
                'company_id' => $d['company_id'],
                'type' => $d['type'],
                'contractor_id' => $d['contractor_id'],
                'contractor_name' => $d['contractor_name'],
                'amount' => $d['amount']
            ];
        }

        return $ret;
    }

    public function __getByActivityPlanData()
    {
        $table = PlanCashFlows::tableName();
        $companyIds = implode(',', $this->multiCompanyIds);
        $year = (int)$this->year;

        $query = "
            SELECT
              IF (t.flow_type = 0, t.expenditure_item_id, t.income_item_id) AS item_id,
              t.flow_type AS type,
              t.date AS `date`,
              SUM(t.amount) amount
            FROM {$table} t
            WHERE
              t.company_id IN ({$companyIds})
              AND t.date BETWEEN '{$year}-01-01' AND '{$year}-12-31'
            GROUP BY `type`, `item_id`, `date`
          ";

        return Yii::$app->db2->createCommand($query)->queryAll();
    }

    public function __getByActivityTodayGrowingFact()
    {
        $table = self::TABLE_OLAP_FLOWS;
        $companyIds = implode(',', $this->multiCompanyIds);
        $currDate = date('Y-m-d');

        $query = "
            SELECT
              t.item_id,
              t.type,
              SUM(t.amount) growing_amount
            FROM {$table} t
            WHERE t.company_id IN ({$companyIds})
              AND t.date <= '{$currDate}'
            GROUP BY t.type, t.item_id
          ";

        return Yii::$app->db2->createCommand($query)->queryAll();
    }

    public function __getByPursePlanData()
    {
        $table = PlanCashFlows::tableName();
        $companyIds = implode(',', $this->multiCompanyIds);
        $year = (int)$this->year;

        $query = "
            SELECT
              t.payment_type AS wallet,
              t.date AS `date`,
              t.flow_type AS type,
              IF (t.flow_type = 0, t.expenditure_item_id, t.income_item_id) AS item_id,
              t.contractor_id,
              SUM(t.amount) amount
            FROM {$table} t
            WHERE
              t.company_id IN ({$companyIds})
              AND t.date BETWEEN '{$year}-01-01' AND '{$year}-12-31'
            GROUP BY `wallet`, `type`, `date`, `contractor_id`
          ";

        return Yii::$app->db2->createCommand($query)->queryAll();
    }

    public function __getByFlowAttributePlanData($params)
    {
        $table = PlanCashFlows::tableName();
        $companyIds = implode(',', $this->multiCompanyIds);
        $year = (int)$this->year;
        $groupBy = $params['attr'] ?? 'wallet';

        $query = "
            SELECT
              t.payment_type AS wallet,
              IFNULL(t.sale_point_id, 0) sale_point_id,
              IFNULL(t.industry_id, 0) industry_id,                   
              t.date AS `date`,
              t.flow_type AS type,
              IF (t.flow_type = 0, t.expenditure_item_id, t.income_item_id) AS item_id,
              t.contractor_id,
              SUM(t.amount) amount
            FROM {$table} t
            WHERE
              t.company_id IN ({$companyIds})
              AND t.date BETWEEN '{$year}-01-01' AND '{$year}-12-31'
            GROUP BY `{$groupBy}`, `type`, `date`, `contractor_id`
          ";

        return Yii::$app->db2->createCommand($query)->queryAll();
    }

    public function __getByPursePlanDataDetailed()
    {
        $table = PlanCashFlows::tableName();
        $rsTable = CheckingAccountant::tableName();
        $companyIds = implode(',', $this->multiCompanyIds);
        $year = $this->year;
        $seq = 'seq_0_to_11';
        $interval = "('{$year}-01-31' + INTERVAL (seq) MONTH)";
        $query = "
            SELECT
              t.payment_type AS wallet,
              CASE 
                WHEN t.payment_type = 1 THEN rs.rs
                WHEN t.payment_type = 2 THEN t.cashbox_id
                WHEN t.payment_type = 3 THEN t.emoney_id
              END AS wallet_id,  
              t.flow_type AS type,
              IF (t.flow_type = 0, t.expenditure_item_id, t.income_item_id) AS item_id,
              t.contractor_id,
              DATE_FORMAT({$interval}, '%m') m,
              DATE_FORMAT({$interval}, '%d') d,
              SUM(IF(DATE_FORMAT(t.date, '%Y%m') = DATE_FORMAT({$interval}, '%Y%m'), t.amount, 0)) amount,
              SUM(t.amount) growing_amount
            FROM {$seq}
            JOIN {$table} t ON 
              t.company_id IN ({$companyIds})
              AND t.date <= {$interval}
            LEFT JOIN {$rsTable} rs ON rs.id = t.checking_accountant_id 
            GROUP BY `wallet_id`, `type`, `contractor_id`, `item_id`, `m`
          ";

        return Yii::$app->db2->createCommand($query)->queryAll();
    }

    public function __getByPurseTodayGrowingFact()
    {
        $table = self::TABLE_OLAP_FLOWS;
        $companyIds = implode(',', $this->multiCompanyIds);
        $currDate = date('Y-m-d');

        $query = "
            SELECT
              IF(t.wallet > 10, t.wallet-10, t.wallet) wallet,
              t.type,
              SUM(t.amount) growing_amount
            FROM {$table} t
            WHERE
              t.company_id IN ({$companyIds})
              AND t.date <= '{$currDate}'
            GROUP BY wallet, t.type
          ";

        return Yii::$app->db2->createCommand($query)->queryAll();
    }

    public function __getByFlowAttributeTodayGrowingFact($params)
    {
        $theAttr = $params['attr'] ?? 'wallet';

        $table = self::TABLE_OLAP_FLOWS;
        $companyIds = implode(',', $this->multiCompanyIds);
        $currDate = date('Y-m-d');

        $query = "
            SELECT
              IF(t.wallet > 10, t.wallet-10, t.wallet) wallet,
              IFNULL(t.sale_point_id, 0) sale_point_id,
              IFNULL(t.industry_id, 0) industry_id,
              t.type,
              SUM(t.amount) growing_amount
            FROM {$table} t
            WHERE
              t.company_id IN ({$companyIds})
              AND t.date <= '{$currDate}'
            GROUP BY t.{$theAttr}, t.type
          ";

        return Yii::$app->db2->createCommand($query)->queryAll();
    }

    public function __getByPurseTodayGrowingFactDetailed()
    {
        $tableBank = CashBankFlows::tableName();
        $tableCashbox = CashOrderFlows::tableName();
        $tableEmoney = CashEmoneyFlows::tableName();
        $companyIds = implode(',', $this->_multiCompanyIds);
        $currDate = date('Y-m-d');

        $query = "
          SELECT
            1 AS wallet, 
            t.rs AS wallet_id,
            SUM(IF(t.flow_type = 0, -t.amount, t.amount)) AS amount
          FROM {$tableBank} t
          WHERE
            t.company_id IN ({$companyIds}) AND t.date <= '$currDate'
          GROUP BY wallet_id

          UNION ALL

          SELECT
            2 AS wallet, 
            t.cashbox_id AS wallet_id,
            SUM(IF(t.flow_type = 0, -t.amount, t.amount)) AS amount
          FROM {$tableCashbox} t
          WHERE
            t.company_id IN ({$companyIds}) AND t.date <= '$currDate'
          GROUP BY wallet_id          

          UNION ALL

          SELECT
            3 AS wallet, 
            t.emoney_id AS wallet_id,
            SUM(IF(t.flow_type = 0, -t.amount, t.amount)) AS amount
          FROM {$tableEmoney} t
          WHERE
            t.company_id IN ({$companyIds}) AND t.date <= '$currDate'
          GROUP BY wallet_id
        ";

        return  Yii::$app->db2->createCommand($query)->queryAll();
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// OLD METHODS 2019 (from AbstractFinance) todo: need update
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param $flowType
     * @return mixed
     */
    public function getParamsByFlowType($flowType)
    {
        $result['flowType'] = $flowType;
        if ($flowType == CashFlowsBase::FLOW_TYPE_INCOME) {
            $result['itemFlowOfFundsTableName'] = IncomeItemFlowOfFunds::tableName();
            $result['invoiceItemTableName'] = InvoiceIncomeItem::tableName();
            $result['itemAttrName'] = $result['flowOfFundItemName'] = 'income_item_id';
            $result['withoutItem'] = InvoiceIncomeItem::ITEM_OWN_FOUNDS;
            $result['flowTypeText'] = 'ПРИХОДЫ';
            $result['prevSumAttrName'] = 'incomePrevSum';
        } elseif ($flowType == CashFlowsBase::FLOW_TYPE_EXPENSE) {
            $result['itemFlowOfFundsTableName'] = ExpenseItemFlowOfFunds::tableName();
            $result['invoiceItemTableName'] = InvoiceExpenditureItem::tableName();
            $result['itemAttrName'] = 'expenditure_item_id';
            $result['flowOfFundItemName'] = 'expense_item_id';
            $result['withoutItem'] = InvoiceExpenditureItem::ITEM_OWN_FOUNDS;
            $result['flowTypeText'] = 'РАСХОДЫ';
            $result['prevSumAttrName'] = 'expenditurePrevSum';
        } else {
            throw new InvalidParamException('Invalid flow type param.');
        }

        return $result;
    }

    /**
     * @param $flowType
     * @return array
     */
    public function getItemsByFlowType($flowType)
    {
        if ($flowType == CashFlowsBase::FLOW_TYPE_INCOME) {
            $items = InvoiceIncomeItem::find()
                ->andWhere(['or',
                    ['company_id' => null],
                    ['company_id' => $this->multiCompanyIds],
                ])
                ->andWhere(['not', ['id' => InvoiceIncomeItem::ITEM_OWN_FOUNDS]])
                ->column();
        } elseif ($flowType == CashFlowsBase::FLOW_TYPE_EXPENSE) {
            $items = InvoiceExpenditureItem::find()
                ->andWhere(['or',
                    ['company_id' => null],
                    ['company_id' => $this->multiCompanyIds],
                ])
                ->andWhere(['not', ['id' => InvoiceExpenditureItem::ITEM_OWN_FOUNDS]])
                ->column();
        } else {
            throw new InvalidParamException('Invalid flow type param.');
        }

        return $items;
    }

    /**
     * @param $className CashBankFlows|CashOrderFlows|CashEmoneyFlows|PlanCashFlows
     * @param $flowTypeParams
     * @param $dateFrom
     * @param $dateTo
     * @return ActiveQuery
     */
    public function getDefaultCashFlowsQuery($className, $flowTypeParams, $dateFrom, $dateTo)
    {
        return $className::find()
            ->leftJoin(['itemFlowOfFunds' => $flowTypeParams['itemFlowOfFundsTableName']],
                "itemFlowOfFunds.{$flowTypeParams['flowOfFundItemName']} = " . $className::tableName() . ".{$flowTypeParams['itemAttrName']} AND
                itemFlowOfFunds.company_id = " . $this->company->id)
            ->leftJoin(['invoiceItem' => $flowTypeParams['invoiceItemTableName']],
                "invoiceItem.id = " . $className::tableName() . ".{$flowTypeParams['itemAttrName']}")
            ->andWhere(['not', [$className::tableName() . ".{$flowTypeParams['itemAttrName']}" => $flowTypeParams['withoutItem']]])
            ->andWhere(['between', $className::tableName() . '.date', $dateFrom, $dateTo])
            ->andWhere([$className::tableName() . '.company_id' => $this->company->id]);
    }

    ///////////
    /// XLS ///
    ///////////

    public function generateXls($type) {
        if ($this->exportPlanFlowsRegister)
            return $this->_generateRegisterXls();

        return $this->_generateXls($type);
    }

    public function _generateRegisterXls()
    {
        $dataProvider = $this->searchItems(Yii::$app->request->get());
        $dataProvider->pagination = false;
        $data = $dataProvider->getModels();

        // header
        $columns = [
            ['attribute' => 'date', 'header' => 'Дата', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_GENERAL],
            ['attribute' => 'amountIncome', 'header' => 'Приход', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00],
            ['attribute' => 'amountExpense', 'header' => 'Расход', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00],
            ['attribute' => 'paymentType', 'header' => 'Тип оплаты', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_GENERAL],
            ['attribute' => 'priorityType', 'header' => 'Приоритет в оплате', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_GENERAL],
            ['attribute' => 'status', 'header' => 'Статус', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_GENERAL],
            ['attribute' => 'contractor', 'header' => 'Контрагент', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_GENERAL],
            ['attribute' => 'industry', 'header' => 'Направление', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_GENERAL],
            ['attribute' => 'sale_point', 'header' => 'Точка продаж', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_GENERAL],
            ['attribute' => 'project', 'header' => 'Проект', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_GENERAL],
            ['attribute' => 'description', 'header' => 'Назначение', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_GENERAL],
            ['attribute' => 'reason', 'header' => 'Статья', 'type' => \PHPExcel_Style_NumberFormat::FORMAT_GENERAL],
        ];

        // data
        $formattedData = [];
        foreach ($data as $d) {
            $formattedData[] = $this->_buildRegisterXlsRow($d);
        }

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $formattedData,
            'title' => "ПК за {$this->year} год",
            'rangeHeader' => range('A', 'L'),
            'columns' => $columns,
            'format' => 'Xlsx',
            'fileName' => "Реестр плановых платежей за {$this->year} год",
        ]);
    }

    public function _generateXls($type)
    {
        $data = $this->search2($type, Yii::$app->request->get());

        // header
        $columns = [[
            'attribute' => 'itemName',
            'header' => 'Статьи',
        ]];

        foreach (AbstractFinance::$month as $monthNumber => $monthText) {
            $monthNumber = (int)$monthNumber;
            $columns[] = [
                'attribute' => "item{$monthNumber}",
                'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                'header' => $monthText,
            ];
        }
        $columns[] = [
            'attribute' => 'dataYear',
            'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
            'header' => $this->year,
        ];

        // data
        $formattedData = [];

        foreach ($data['data'] as $paymentType => $level1) {
            $formattedData[] = $this->_buildXlsRow2($level1, 1);
            if (!empty($level1['levels'])) foreach ($level1['levels'] as $level2) {
                $formattedData[] = $this->_buildXlsRow2($level2, 2);
                if (!empty($level2['levels'])) foreach ($level2['levels'] as $level3) {
                    $formattedData[] = $this->_buildXlsRow2($level3, 3);
                    if (!empty($level3['levels'])) foreach ($level3['levels'] as $level4) {
                        $formattedData[] = $this->_buildXlsRow2($level4, 4);
                    }
                }
            }
            if (!empty($data['growingData'][$paymentType]))
                $formattedData[] = $this->_buildXlsRow2($data['growingData'][$paymentType], 1, true);
        }

        if (!empty($data['totalData'])) {
            $formattedData[] = $this->_buildXlsRow2($data['totalData']['month'], 1);
            $formattedData[] = $this->_buildXlsRow2($data['totalData']['growing'], 1, true);
        }

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $formattedData,
            'title' => "ПК за {$this->year} год",
            'rangeHeader' => range('A', 'N'),
            'columns' => $columns,
            'format' => 'Xlsx',
            'fileName' => "ПК за {$this->year} год",
        ]);
    }

    protected function _buildXlsRow2($data, $level = 1, $isGrowing = false)
    {
        $row = [
            'itemName' => str_repeat(' ', ($level-1) * 5) . trim(strip_tags($data['title'] ?? '')),
            'dataYear' => (!$isGrowing ? array_sum($data['data']) : $data['data'][array_key_last($data['data'])]) / 100
        ];
        foreach (AbstractFinance::$month as $month => $monthName)
            $row['item'.(int)$month] = ($data['data'][$month] ?? '0.00') / 100;

        return $row;
    }

    protected function _buildRegisterXlsRow($d)
    {
        $paymentType = '';
        $priorityType = '';
        $status = '';
        $contractorName = '';
        $industryName = '';
        $salePointName = '';
        $projectName = '';
        $reasonName = '';

        $contractor = Contractor::findOne($d['contractor_id']);

        // payment type
        switch ($d['payment_type']) {
            case PaymentCalendarSearch::CASH_BANK_BLOCK:
                $paymentType = 'Банк';
                break;
            case PaymentCalendarSearch::CASH_ORDER_BLOCK:
                $paymentType = 'Касса';
                break;
            case PaymentCalendarSearch::CASH_EMONEY_BLOCK :
                $paymentType = 'E-money';
                break;
        }

        // priority
        if ($contractor && $contractor->type == Contractor::TYPE_SELLER) {
            switch ($contractor->payment_priority) {
                case Contractor::PAYMENT_PRIORITY_HIGH:
                    $priorityType = '1 - cамые приоритетные';
                    break;
                case Contractor::PAYMENT_PRIORITY_MEDIUM:
                    $priorityType = '2 - менее приоритетные';
                    break;
                case Contractor::PAYMENT_PRIORITY_LOW:
                    $priorityType = '3 - наименьший приоритет';
                    break;
            }
        }

        // status
        $currDate = date('Y-m-d');
        if ($d['first_date'] < $currDate && $d['date'] != $d['first_date'])
            $status = 'Перенос';
        if ($d['date'] >= $currDate)
            $status = 'План';
        if ($d['date'] < $currDate)
            $status = 'Просрочен';

        // contractor
        if ($contractor) {
            $contractorName = $contractor->nameWithType;
        } elseif ($contractor == CashContractorType::BANK_TEXT) {
            $contractorName = 'Банк';
        }
        elseif ($d['contractor_id'] == CashContractorType::ORDER_TEXT) {
            $contractorName = 'Касса';
        }
        elseif ($d['contractor_id'] == CashContractorType::EMONEY_TEXT) {
            $contractorName = 'E-money';
        }
        elseif ($d['contractor_id'] == CashContractorType::BALANCE_TEXT) {
            $contractorName = 'Баланс начальный';
        }
        elseif ($d['contractor_id'] == CashContractorType::COMPANY_TEXT) {
            $contractorName = 'Компания';
        }
        elseif ($d['contractor_id'] == CashContractorType::CUSTOMERS_TEXT) {
            $contractorName = 'Физ. лица';
        }

        // project / industry / sale_point
        if ($d['industry_id'] && ($companyIndustry = CompanyIndustry::findOne(['id' => $d['industry_id']]))) {
            $industryName = $companyIndustry->name;
        }
        if ($d['sale_point_id'] && ($salePoint = CompanyIndustry::findOne(['id' => $d['sale_point_id']]))) {
            $salePointName = $salePoint->name;
        }
        if ($d['project_id'] && ($project = Project::findOne(['id' => $d['project_id']]))) {
            $projectName = $project->name;
        }

        // reason
        if ($d['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
            $reasonName = ($reason = InvoiceIncomeItem::findOne($d['income_item_id'])) ? $reason->fullName : null;
        } elseif ($d['flow_type'] == CashFlowsBase::FLOW_TYPE_EXPENSE) {
            $reasonName = ($reason = InvoiceExpenditureItem::findOne($d['expenditure_item_id'])) ? $reason->fullName : null;
        } else {
            $reasonName = null;
        }

        return [
            'date' => DateHelper::format($d['date'], 'd.m.Y', 'Y-m-d'),
            'amountIncome' => round($d['amountIncome'] / 100, 2),
            'amountExpense' => round($d['amountExpense'] / 100, 2),
            'paymentType' => $paymentType,
            'priorityType' => $priorityType,
            'status' => $status,
            'contractor' => $contractorName,
            'industry' => $industryName,
            'sale_point' => $salePointName,
            'project' => $projectName,
            'description' => $d['description'],
            'reason' => $reasonName
        ];
    }
}