<?php

namespace frontend\modules\analytics\models;

use frontend\components\StatisticPeriod;
use Yii;
use yii\base\Exception;
use yii\db\Expression;
use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\ExpenseItemFlowOfFunds;
use common\models\IncomeItemFlowOfFunds;
use common\components\excel\Excel;
use common\models\Company;
use common\modules\acquiring\models\Acquiring;
use common\modules\cards\models\CardAccount;

/**
 * Class OddsSearch
 * @package frontend\modules\analytics\models
 */
class OddsSearch extends OLAP
{
    use odds\SearchByActivityTrait;
    use odds\SearchByCurrencyTrait;
    use odds\SearchByPurseTrait;
    use odds\SearchByPurseDetailedTrait;
    use odds\SearchByFlowAttributeTrait;
    use odds\WarningsTrait;
    use odds\SortTrait;

    const TAB_ODDS = 1;
    const TAB_ODDS_BY_PURSE = 2;
    const TAB_ODDS_BY_PURSE_DETAILED = "2d";
    const TAB_ODDS_BY_INDUSTRY = 3;
    const TAB_ODDS_BY_SALE_POINT = 4;
    const TAB_ODDS_BY_CURRENCY = 5;

    const MIN_REPORT_DATE = '2010';

    public $year;
    public $contractor_id;
    public $reason_id;
    public $period;
    public $group;
    public $flow_type;
    public $payment_type;

    protected $_company;

    public $incomeItemIdManyItem;
    public $expenditureItemIdManyItem;
    public $recognitionDateManyItem;

    public $incomeItemName = [];
    public $expenditureItemName = [];

    public $incomeItemSort = [];
    public $expenditureItemSort = [];

    public $walletName = [
        CashFlowsBase::WALLET_BANK => [],
        CashFlowsBase::WALLET_CASHBOX => [],
        CashFlowsBase::WALLET_EMONEY => [],
        CashFlowsBase::WALLET_ACQUIRING => [],
        CashFlowsBase::WALLET_CARD => []
    ];
    public $_bankRs2Bik = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['contractor_id', 'reason_id', 'payment_type'], 'integer'],
        ]);
    }

    public function init()
    {
        parent::init();

        $this->incomeItemName = InvoiceIncomeItem::find()
            ->select('name')
            ->where(['or', ['company_id' => null], ['company_id' => $this->_multiCompanyIds]])
            ->indexBy('id')
            ->column(Yii::$app->db2);
        $this->expenditureItemName = InvoiceExpenditureItem::find()
            ->select('name')
            ->where(['or', ['company_id' => null], ['company_id' => $this->_multiCompanyIds]])
            ->indexBy('id')
            ->column(Yii::$app->db2);
        $this->incomeItemSort = IncomeItemFlowOfFunds::find()
            ->select('flow_of_funds_block')
            ->where(['company_id' => $this->_multiCompanyIds])
            ->andWhere(['not', ['flow_of_funds_block' => null]])
            ->indexBy('income_item_id')
            ->column(Yii::$app->db2);
        $this->expenditureItemSort = ExpenseItemFlowOfFunds::find()
            ->select('flow_of_funds_block')
            ->where(['company_id' => $this->_multiCompanyIds])
            ->andWhere(['not', ['flow_of_funds_block' => null]])
            ->indexBy('expense_item_id')
            ->column(Yii::$app->db2);

        foreach ($this->_multiCompanyIds as $companyIds) {
            if ($company = Company::findOne(['id' => $companyIds])) {

                $_tmpBankNames = $company->getRubleAccounts()
                    ->select('name')
                    ->orderBy(['type' => SORT_ASC, 'name' => SORT_ASC])
                    ->indexBy('rs')
                    ->column();
                $_tmpCashboxNames = $company->getRubleCashboxes()
                    ->select('name')
                    ->orderBy(['is_main' => SORT_DESC, 'name' => SORT_ASC])
                    ->indexBy('id')
                    ->column();
                $_tmpEmoneyNames = $company->getRubleEmoneys()
                    ->select('name')
                    ->orderBy(['is_main' => SORT_DESC, 'name' => SORT_ASC])
                    ->indexBy('id')
                    ->column();
                //
                $_tmpAcquiringNames = Acquiring::find()
                    ->where(['company_id' => $company->id])
                    ->select(new Expression('CONCAT(CASE WHEN type=0 THEN "Монета" WHEN type=1 THEN "ЮKassa" END, " (",identifier ,")")'))
                    ->orderBy('type')
                    ->indexBy('identifier')
                    ->column();
                $_tmpCardNames = CardAccount::find()
                    ->where(['company_id' => $company->id])
                    ->select(new Expression('CONCAT("Дзен мани", " (", identifier, ")")'))
                    ->orderBy('identifier')
                    ->indexBy('id')
                    ->column();

                if (count($this->_multiCompanyIds) > 1) {
                    foreach ($_tmpBankNames as &$name) $name = ($company->getShortName()) . ' ' . $name;
                    foreach ($_tmpCashboxNames as &$name) $name = ($company->getShortName()) . ' ' . $name;
                    foreach ($_tmpEmoneyNames as &$name) $name = ($company->getShortName()) . ' ' . $name;
                    foreach ($_tmpAcquiringNames as &$name) $name = ($company->getShortName()) . ' ' . $name;
                    foreach ($_tmpCardNames as &$name) $name = ($company->getShortName()) . ' ' . $name;
                }

                $this->_bankRs2Bik = array_merge($this->_bankRs2Bik, $company->getCheckingAccountants()->select('bik')->orderBy(['type' => SORT_ASC, 'bank_name' => SORT_ASC])->indexBy('rs')->column());
                $this->walletName[CashFlowsBase::WALLET_BANK] += $_tmpBankNames;
                $this->walletName[CashFlowsBase::WALLET_CASHBOX] += $_tmpCashboxNames;
                $this->walletName[CashFlowsBase::WALLET_EMONEY] += $_tmpEmoneyNames;
                $this->walletName[CashFlowsBase::WALLET_ACQUIRING] += $_tmpAcquiringNames;
                $this->walletName[CashFlowsBase::WALLET_CARD] += $_tmpCardNames;
            }
        }
    }

    /**
     * @param $type
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function search($type, $params = [])
    {
        if ($type == self::TAB_ODDS_BY_INDUSTRY) {
            return $this->searchByFlowAttribute($params + ['attr' => 'industry_id']);
        } elseif ($type == self::TAB_ODDS_BY_SALE_POINT) {
            return $this->searchByFlowAttribute($params + ['attr' => 'sale_point_id']);
        } elseif ($type == self::TAB_ODDS_BY_PURSE_DETAILED) {
            return $this->searchByPurseDetailed($params);
        } elseif ($type == self::TAB_ODDS_BY_PURSE) {
            return $this->searchByPurse($params);
        } elseif ($type == self::TAB_ODDS) {
            return $this->searchByActivity($params);
        } elseif ($type == self::TAB_ODDS_BY_CURRENCY) {
            return $this->searchByCurrency($params);
        }

        return $this->searchByActivity($params);
    }

    /**
     * @param $type
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function searchByDays($type, $params = [])
    {
        if ($type == self::TAB_ODDS_BY_INDUSTRY) {
            return $this->searchByFlowAttributeDays($params + ['attr' => 'industry_id']);
        } elseif ($type == self::TAB_ODDS_BY_SALE_POINT) {
            return $this->searchByFlowAttributeDays($params + ['attr' => 'sale_point_id']);
        } elseif ($type == self::TAB_ODDS_BY_PURSE_DETAILED) {
            return $this->searchByPurseDaysDetailed($params);
        } elseif ($type == self::TAB_ODDS_BY_PURSE) {
            return $this->searchByPurseDays($params);
        } elseif ($type == self::TAB_ODDS) {
            return $this->searchByActivityDays($params);
        } elseif ($type == self::TAB_ODDS_BY_CURRENCY) {
            return $this->searchByCurrencyDays($params);
        }

        return $this->searchByActivityDays($params);
    }

    protected function __getActivityBlockType($type, $itemId)
    {
        if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
            if ($itemId == InvoiceIncomeItem::ITEM_OWN_FOUNDS) {
                return AbstractFinance::OWN_FUNDS_INCOME;
            } elseif (isset($this->incomeItemSort[$itemId])) {
                return AbstractFinance::$typesByBlock[$this->incomeItemSort[$itemId]][0];
            } elseif (in_array($itemId, AbstractFinance::$typeItem[AbstractFinance::RECEIPT_FINANCING_TYPE_FIRST])) {
                return AbstractFinance::RECEIPT_FINANCING_TYPE_FIRST;
            } else {
                return AbstractFinance::INCOME_OPERATING_ACTIVITIES;
            }
        }
        if ($type == CashFlowsBase::FLOW_TYPE_EXPENSE) {
            if ($itemId == InvoiceExpenditureItem::ITEM_OWN_FOUNDS) {
                return AbstractFinance::OWN_FUNDS_EXPENSE;
            } elseif (isset($this->expenditureItemSort[$itemId])) {
                return AbstractFinance::$typesByBlock[$this->expenditureItemSort[$itemId]][1];
            } elseif (in_array($itemId, AbstractFinance::$typeItem[AbstractFinance::RECEIPT_FINANCING_TYPE_SECOND])) {
                return AbstractFinance::RECEIPT_FINANCING_TYPE_SECOND;

            } else {
                return AbstractFinance::WITHOUT_TYPE;
            }
        }

        throw new Exception('wallet not found');
    }

    protected function __getCashContractorName($type, $itemId, $contractorId = null)
    {
        if (in_array($contractorId, ['bank', 'order', 'emoney'])) {
            return ($type == 0 ? 'Перевод в ' : 'Приход из ') .
                ArrayHelper::getValue(AbstractFinance::$cashContractorTextByType, "{$type}.{$contractorId}");
        }

        return ArrayHelper::getValue(($type == 0) ? $this->expenditureItemName : $this->incomeItemName, $itemId);
    }

    protected function exchangeDifferenceName($type, $code)
    {
        return sprintf('Курсовая разница %s %s', $code, $type == 0 ? 'отрицательная' : 'положительная');
    }

    public function getEmptyTotalData($byDays = false)
    {
        return [
            'month' => [
                'title' => 'Чистый денежный поток',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'question' => '#tooltip_net_cash_flow',
            ],
            'growing' => [
                'title' => 'Остаток на конец месяца',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
            ]
        ];
    }

    /**
     * @var $params
     * @return array
     */
    public function getYearFilter($params = [])
    {
        $tableFlows = self::TABLE_OLAP_FLOWS;
        $companyIds = implode(',', $this->_multiCompanyIds);
        $AND_FILTERS = $this->getSqlFilters($params);

        $minDate = Yii::$app->db2->createCommand("SELECT MIN(t.date) FROM {$tableFlows} t WHERE t.company_id IN ({$companyIds}) {$AND_FILTERS}")->queryScalar();
        $minCashDate = !empty($minDate) ? max(self::MIN_REPORT_DATE, $minDate) : date(DateHelper::FORMAT_DATE);
        $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
        $currentYear = date('Y');
        foreach (range($registrationYear, $currentYear) as $value) {
            $range[$value] = $value;
        }
        arsort($range);

        return $range;
    }

    /**
     * @return bool
     */
    public function getIsCurrentYear()
    {
        return date('Y') == $this->year;
    }

    public function getCompany()
    {
        return $this->_company;
    }

    /** ADDITIONAL METHODS */

    public function getDaysInMonth($month)
    {
        if (strlen($month) == 1) { $month = '0'.$month; }
        $start = (new \DateTime())->createFromFormat('Y-m-d', $this->year.'-'.$month.'-'.'01');
        $curr = $start;
        $endDate = $start->format('mt');
        $ret = [];
        $err = 1;
        while(true) {
            $date = $curr->format('md');
            $ret[$date] = $curr->format('d');
            $curr = $curr->modify("+1 day");

            if ($date >= $endDate)
                break;

            if (++$err > 31) {
                echo 'getDaysInMonth: error';
                exit;
            }
        }

        return $ret;
    }

    /**
     * @param $flowType
     * @return array
     */
    protected function getArticles2Parents($flowType)
    {
        if ($flowType == CashFlowsBase::FLOW_TYPE_INCOME)
            return InvoiceIncomeItem::find()
                ->select(['parent_id', 'id'])
                ->where(['company_id' => $this->_multiCompanyIds])
                ->andWhere(['not', ['parent_id' => null]])
                ->indexBy('id')
                ->column();

        if ($flowType == CashFlowsBase::FLOW_TYPE_EXPENSE)
            return InvoiceExpenditureItem::find()
                ->select(['parent_id', 'id'])
                ->where(['company_id' => $this->_multiCompanyIds])
                ->andWhere(['not', ['parent_id' => null]])
                ->indexBy('id')
                ->column();

        return [];
    }

    protected function getSqlFilters($params)
    {
        $ret = '';
        if (isset($params['filters']) && is_array($params['filters'])) {
            foreach ($params['filters'] as $attr => $value) {
                if (in_array($attr, ['project_id', 'industry_id', 'sale_point_id'])) {
                    if (strlen($value)) {
                        $value = (int)$value;
                        if ($value > 0)
                            $ret .= " AND t.{$attr} = '{$value}'";
                        elseif ($value === 0)
                            $ret .= " AND t.{$attr} IS NULL";
                    }
                }
            }
        }

        return $ret;
    }

    protected function getSqlHideTinParent()
    {
        return " AND t.has_tin_children = 0 ";
    }

    ///////////
    /// XLS ///
    ///////////

    public function generateXls($type, $params)
    {
        $data = $this->search($type, $params);

        // header
        $columns = [[
            'attribute' => 'itemName',
            'header' => 'Статьи',
        ]];

        foreach (AbstractFinance::$month as $monthNumber => $monthText) {
            $monthNumber = (int)$monthNumber;
            $columns[] = [
                'attribute' => "item{$monthNumber}",
                'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                'header' => $monthText,
            ];
        }
        $columns[] = [
            'attribute' => 'dataYear',
            'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
            'header' => $this->year,
        ];

        // data
        $formattedData = [];
        if ($type == self::TAB_ODDS_BY_PURSE_DETAILED) {
            foreach ($data['data'] as $paymentType => $level0) {

                if (count($level0['children']) > 1)
                    $formattedData[] = $this->_buildXlsRow2($level0, 1);

                foreach ($level0['children'] as $walletID => $level1) {
                    $formattedData[] = $this->_buildXlsRow2($level1, 1);
                    if (!empty($level1['levels'])) foreach ($level1['levels'] as $level2) {
                        $formattedData[] = $this->_buildXlsRow2($level2, 2);
                        if (!empty($level2['levels'])) foreach ($level2['levels'] as $level3) {
                            $formattedData[] = $this->_buildXlsRow2($level3, 3);
                            if (!empty($level3['levels'])) foreach ($level3['levels'] as $level4) {
                                $formattedData[] = $this->_buildXlsRow2($level4, 4);
                            }
                        }
                    }

                    $formattedData[] = $this->_buildXlsRow2($data['growingData'][$paymentType]['children'][$walletID], 1, true);
                }

                if (count($level0['children']) > 1)
                    $formattedData[] = $this->_buildXlsRow2($data['growingData'][$paymentType], 1, true);
            }

        } else {
            foreach ($data['data'] as $paymentType => $level1) {
                $formattedData[] = $this->_buildXlsRow2($level1, 1);
                if (!empty($level1['levels'])) foreach ($level1['levels'] as $level2) {
                    $formattedData[] = $this->_buildXlsRow2($level2, 2);
                    if (!empty($level2['levels'])) foreach ($level2['levels'] as $level3) {
                        $formattedData[] = $this->_buildXlsRow2($level3, 3);
                        if (!empty($level3['levels'])) foreach ($level3['levels'] as $level4) {
                            $formattedData[] = $this->_buildXlsRow2($level4, 4);
                        }
                    }
                }
                if (!empty($data['growingData'][$paymentType]))
                    $formattedData[] = $this->_buildXlsRow2($data['growingData'][$paymentType], 1, true);
            }
        }
        if (!empty($data['totalData'])) {
            $formattedData[] = $this->_buildXlsRow2($data['totalData']['month'], 1);
            $formattedData[] = $this->_buildXlsRow2($data['totalData']['growing'], 1, true);
        }

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $formattedData,
            'title' => "ОДДС за {$this->year} год",
            'rangeHeader' => range('A', 'N'),
            'columns' => $columns,
            'format' => 'Xlsx',
            'fileName' => "ОДДС за {$this->year} год",
        ]);
    }

    protected function _buildXlsRow2($data, $level = 1, $isGrowing = false)
    {
        $row = [
            'itemName' => str_repeat(' ', ($level-1) * 5) . trim(strip_tags($data['title'] ?? '')),
            'dataYear' => (!$isGrowing ? array_sum($data['data']) : $data['data'][array_key_last($data['data'])]) / 100
        ];
        foreach (AbstractFinance::$month as $month => $monthName)
            $row['item'.(int)$month] = ($data['data'][$month] ?? '0.00') / 100;

        return $row;
    }

    /**
     * @param integer $oddsReportType
     * @param integer $oddsBlock
     * @param \common\models\employee\Config $userConfig
     * @return bool
     */
    public static function hideBlockByUser($oddsReportType, $oddsBlock, $userConfig)
    {
        if ($oddsReportType == self::TAB_ODDS) {
            return ($oddsBlock == AbstractFinance::OWN_FUNDS_BLOCK && !$userConfig->report_odds_own_funds) ||
                ($oddsBlock == AbstractFinance::FINANCIAL_OPERATIONS_BLOCK && !$userConfig->report_odds_row_finance) ||
                ($oddsBlock == AbstractFinance::INVESTMENT_ACTIVITIES_BLOCK && !$userConfig->report_odds_row_investments);
        }
        if ($oddsReportType == self::TAB_ODDS_BY_PURSE || $oddsReportType == self::TAB_ODDS_BY_PURSE_DETAILED) {
            return ($oddsBlock == AbstractFinance::CASH_BANK_BLOCK && !$userConfig->report_odds_row_bank) ||
                ($oddsBlock == AbstractFinance::CASH_ORDER_BLOCK && !$userConfig->report_odds_row_order) ||
                ($oddsBlock == AbstractFinance::CASH_EMONEY_BLOCK && !$userConfig->report_odds_row_emoney) ||
                ($oddsBlock == AbstractFinance::CASH_ACQUIRING_BLOCK && !$userConfig->report_odds_row_acquiring) ||
                ($oddsBlock == AbstractFinance::CASH_CARD_BLOCK && !$userConfig->report_odds_row_card);
        }

        return false;
    }

    // HELPERS

    public function getFlowsPeriodSumByContractors(int $type, string $dateFrom, string $dateTo, array $articlesIds = []): array
    {
        $table = self::TABLE_OLAP_FLOWS;
        $companyIds = implode(',', $this->_multiCompanyIds);
        $articlesIds = implode(',', $articlesIds);
        $AND_FILTERS = ''; // todo

        $query = "
            SELECT contractor_id, SUM(t.original_amount) amount
            FROM {$table} t
            WHERE
              t.company_id IN ({$companyIds})
              AND t.type = {$type}
              AND t.date BETWEEN '{$dateFrom}' AND '{$dateTo}'
              ".($articlesIds ? "AND t.item_id IN ({$articlesIds})":"")."
              {$AND_FILTERS}
            GROUP BY contractor_id ORDER BY amount DESC";

        return Yii::$app->db2->createCommand($query)->queryAll();
    }

    public static function getSubtitlePeriod(?string $from = null, ?string $to = null): string
    {
        if (empty($from) || empty($to)) {
            $_period = StatisticPeriod::getSessionPeriod();
            $from = $_period['from'];
            $to = $_period['to'];
        }

        $period = null;
        $defaultPeriod = 'за период';

        $isMonth = substr($from, 0, 7) === substr($to, 0, 7);
        $isQuarter = substr($from, 0, 4) === substr($to, 0, 4)
            && ceil(substr($from, 5, 2) / 3) === ceil(substr($to, 5, 2) / 3);
        $isYear = substr($from, 0, 4) === substr($to, 0, 4)
            && substr($from, 5, 2) === '01' && substr($to, 5, 2) === '12';

        if ($isMonth)
            $period = mb_strtolower(ArrayHelper::getValue(AbstractFinance::$month, substr($to, 5, 2)) . ' ' . substr($to, 0, 4));
        elseif ($isQuarter)
            $period = ceil(substr($to, 5, 2) / 3) . ' кв. ' . substr($to, 0, 4);
        elseif ($isYear)
            $period = substr($to, 0, 4);

        return ($period) ? " за {$period} г." : " {$defaultPeriod}";
    }

}