<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 22.11.2019
 * Time: 0:44
 */

namespace frontend\modules\analytics\models\googleAds;

use common\models\Company;
use common\models\employee\Employee;
use yii\base\Model;

class GoogleAdsSettingForm extends Model
{
    public $customerId;

    public function rules(): array
    {
        return [
            [['customerId'], 'required'],
            [['customerId'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'customerId' => 'Идентификатор клиента',
        ];
    }

    public function storeToCompany(Company $company)
    {
        if (!$this->validate()) {
            return false;
        }

        $googleAdsParams = $company->integration(Employee::INTEGRATION_GOOGLE_ADS);
        $googleAdsParams['customerId'] = $this->customerId;

        return $company->saveIntegration(Employee::INTEGRATION_GOOGLE_ADS, $googleAdsParams);
    }
}