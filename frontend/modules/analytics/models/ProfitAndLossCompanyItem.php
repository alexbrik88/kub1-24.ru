<?php

namespace frontend\modules\analytics\models;

use common\models\cash\CashFlowsBase;
use Yii;
use common\models\Company;
use common\models\document\InvoiceExpenditureItem;

/**
 * This is the model class for table "profit_and_loss_company_item".
 *
 * @property integer $company_id
 * @property integer $item_id
 * @property integer $expense_type
 * @property integer $can_update
 *
 * @property Company $company
 * @property InvoiceExpenditureItem $item
 */
class ProfitAndLossCompanyItem extends \yii\db\ActiveRecord
{
    const VARIABLE_EXPENSE_TYPE = 1;
    const CONSTANT_EXPENSE_TYPE = 2;
    const OPERATING_EXPENSE_TYPE = 3; // not used
    const OTHER_EXPENSE_TYPE = 4;
    const PRIME_COST_EXPENSE_TYPE = 5;

    /**
     * @var array
     */
    public static $expenseTypes = [
        self::VARIABLE_EXPENSE_TYPE => [
            InvoiceExpenditureItem::ITEM_DELIVERY,
        ],
        self::OPERATING_EXPENSE_TYPE => [
            InvoiceExpenditureItem::ITEM_BOOKKEEPING,
            InvoiceExpenditureItem::ITEM_INTERNET,
            InvoiceExpenditureItem::ITEM_BANK_COMMISSION,
            InvoiceExpenditureItem::ITEM_FURNITURE,
            InvoiceExpenditureItem::ITEM_OFFICE_EQUIPMENT,
            InvoiceExpenditureItem::ITEM_PROGRAMS,
            InvoiceExpenditureItem::ITEM_TELEPHONY,
            InvoiceExpenditureItem::ITEM_SERVICE,
            InvoiceExpenditureItem::ITEM_LAWYER_SERVICE,
            InvoiceExpenditureItem::ITEM_HOUSEHOLD_EXPENSES,
        ],
        self::OTHER_EXPENSE_TYPE => [
            InvoiceExpenditureItem::ITEM_OTHER,
            InvoiceExpenditureItem::ITEM_OUTPUT_MONEY
        ],
        self::CONSTANT_EXPENSE_TYPE => [
            InvoiceExpenditureItem::ITEM_IT,
            InvoiceExpenditureItem::ITEM_LEASE,
            InvoiceExpenditureItem::ITEM_SALARY,
            InvoiceExpenditureItem::ITEM_SALARY_TAX,
            InvoiceExpenditureItem::ITEM_ADVERTISING,
            InvoiceExpenditureItem::ITEM_PAYMENT_ACCOUNTABLE_PERSONS,
            InvoiceExpenditureItem::ITEM_PROJECT_1,
            InvoiceExpenditureItem::ITEM_PROJECT_2,
            InvoiceExpenditureItem::ITEM_PROJECT_3,
        ],
    ];

    /**
     * @var array
     */
    public static $canUpdate = [
        InvoiceExpenditureItem::ITEM_PAYMENT_ACCOUNTABLE_PERSONS,
        InvoiceExpenditureItem::ITEM_PROJECT_1,
        InvoiceExpenditureItem::ITEM_PROJECT_2,
        InvoiceExpenditureItem::ITEM_PROJECT_3,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profit_and_loss_company_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'item_id', 'expense_type'], 'required'],
            [['company_id', 'item_id', 'expense_type'], 'integer'],
            [['can_update'], 'safe'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceExpenditureItem::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'item_id' => 'Item ID',
            'expense_type' => 'Expense Type',
            'can_update' => 'Can Update',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(InvoiceExpenditureItem::className(), ['id' => 'item_id']);
    }

    /**
     *
     */
    public static function loadCompanyItems()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        foreach (static::$expenseTypes as $expenseType => $expenditureItems) {

            $expenditureItemsWithChildren = AbstractFinance::getArticlesWithChildren($expenditureItems, CashFlowsBase::FLOW_TYPE_EXPENSE, $company->id);

            $items = InvoiceExpenditureItem::find()
                ->select([
                    'id',
                    'company_id',
                ])
                ->andWhere(['in', 'id', $expenditureItemsWithChildren]);
            if ($expenseType == static::CONSTANT_EXPENSE_TYPE) {
                $items->orWhere(['company_id' => $company->id]);
            }
            foreach ($items->asArray()->all() as $item) {
                if (!self::find()
                    ->andWhere(['and',
                        ['company_id' => $company->id],
                        ['item_id' => $item['id']],
                    ])->exists()) {
                    $balanceCompanyItem = new ProfitAndLossCompanyItem();
                    $balanceCompanyItem->company_id = $company->id;
                    $balanceCompanyItem->item_id = $item['id'];
                    $balanceCompanyItem->expense_type = $expenseType;
                    $balanceCompanyItem->can_update = in_array($item['id'], static::$canUpdate) || $item['company_id'] == $company->id;
                    $balanceCompanyItem->save();
                }
            }
        }
    }
}