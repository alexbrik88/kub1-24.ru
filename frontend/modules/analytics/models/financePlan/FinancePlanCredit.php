<?php

namespace frontend\modules\analytics\models\financePlan;

use common\components\date\DateHelper;
use frontend\modules\analytics\models\credits\Date;
use Yii;
use common\models\Contractor;
use yii\validators\InlineValidator;

/**
 * This is the model class for table "finance_plan_credit".
 *
 * @property int $id
 * @property int $model_id
 * @property int $contractor_id
 * @property int $credit_type
 * @property int $credit_status
 * @property float $credit_amount
 * @property string $credit_first_date
 * @property string $credit_last_date
 * @property int $credit_day_count
 * @property float $credit_percent_rate
 * @property float $credit_commission_rate
 * @property float $credit_expiration_rate
 * @property int $credit_year_length
 * @property int|null $credit_tranche_depth
 * @property int $payment_type
 * @property int $payment_mode
 * @property int $payment_day
 * @property int $payment_first
 * @property float $payment_amount
 * @property string $agreement_date
 * @property float $debt_amount
 * @property float $debt_repaid
 * @property float $debt_diff
 * @property float $interest_amount
 * @property float $interest_repaid
 * @property float $interest_diff
 *
 * @property Contractor $contractor
 * @property FinancePlan $model
 */
class FinancePlanCredit extends \yii\db\ActiveRecord
{
    /** @var mixed[] */
    public const DECIMAL_FORMAT = ['decimal', 2];

    /** @var mixed[] */
    public const PERCENT_FORMAT = ['decimal', 2];

    /** @var float */
    public const EMPTY_RATE = 0.0;

    /** @var int Кредит */
    public const TYPE_CREDIT = 1;

    /** @var int Займ */
    public const TYPE_LOAN = 2;

    /** @var int Овердрафт */
    public const TYPE_OVERDRAFT = 3;

    /** @var int Действует */
    public const STATUS_ACTIVE = 1;

    /** @var int Просрочен */
    public const STATUS_EXPIRED = 2;

    /** @var int Погашен */
    public const STATUS_ARCHIVE = 0;

    /** @var int В конце срока */
    public const PAYMENT_TYPE_AT_END = 1;

    /** @var int Дифференцированный */
    public const PAYMENT_TYPE_DIFFERENTIATED = 2;

    /** @var int Аннуитетный */
    public const PAYMENT_TYPE_ANNUITY = 3;

    /** @var int Первый платеж стандартный */
    public const PAYMENT_FIRST_DEFAULT = 0;

    /** @var int Первый платеж только % */
    public const PAYMENT_FIRST_ONLY_PERCENT = 1;

    /** @var int Платеж со второго месяца */
    public const PAYMENT_FIRST_SKIP = 2;

    /** @var int Последний день месяца */
    public const PAYMENT_DAY_LAST = 0;

    /** @var int Последний день месяца */
    public const PAYMENT_MODE_DEFAULT = 1;

    /** @var int Число месяца получения кредита */
    public const PAYMENT_MODE_CREDIT = 2;

    /** @var int Число месяца определено по договору */
    public const PAYMENT_MODE_AGREEMENT = 3;

    /** @var int */
    public const YEAR_LENGTH_DEFAULT = 0;

    /** @var int */
    public const YEAR_LENGTH_365 = 365;

    /** @var int */
    public const YEAR_LENGTH_360 = 360;

    /** @var string[] Виды кредита */
    public const TYPE_LIST = [
        self::TYPE_CREDIT => 'Кредит',
        self::TYPE_LOAN => 'Займ',
        //self::TYPE_OVERDRAFT => 'Овердрафт',
    ];

    /** @var string[] Статусы кредита */
    public const STATUS_LIST = [
        self::STATUS_ACTIVE => 'Действует',
        self::STATUS_EXPIRED => 'Просрочен',
        self::STATUS_ARCHIVE => 'Погашен',
    ];

    /** @var string[] */
    public const YEAR_LENGTH_LIST = [
        self::YEAR_LENGTH_DEFAULT => 'По календарю',
        self::YEAR_LENGTH_365 => '365 дней',
        self::YEAR_LENGTH_360 => '360 дней',
    ];

    /** @var string[] Виды платежа */
    public const PAYMENT_TYPE_LIST = [
        self::PAYMENT_TYPE_AT_END => 'В конце срока',
        self::PAYMENT_TYPE_DIFFERENTIATED => 'Дифференцированный',
        self::PAYMENT_TYPE_ANNUITY => 'Аннуитетный',
    ];

    /** @var string[] Виды первого ежемесячного платежа */
    public const PAYMENT_FIRST_LIST = [
        self::PAYMENT_FIRST_DEFAULT => 'Стандартно',
        self::PAYMENT_FIRST_ONLY_PERCENT => 'Первый платеж только %',
        self::PAYMENT_FIRST_SKIP => 'Платеж со второго месяца',
    ];

    /** @var string[] Виды ежемесячного платежа */
    public const PAYMENT_MODE_LIST = [
        self::PAYMENT_MODE_DEFAULT => 'Последний день месяца',
        self::PAYMENT_MODE_CREDIT => 'Число месяца получения кредита',
        self::PAYMENT_MODE_AGREEMENT => 'Число месяца определено по договору',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_plan_credit';
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        $when = function (): bool {
            return ($this->payment_mode == self::PAYMENT_MODE_AGREEMENT);
        };

        return [
            [['model_id'], 'required'],
            [['contractor_id'], 'required'],
            [['credit_type'], 'required'],
            [['credit_amount'], 'required'],
            [['credit_first_date'], 'required'],
            [['credit_last_date'], 'required'],
            [['credit_percent_rate'], 'required'],
            [['credit_commission_rate'], 'required'],
            [['credit_expiration_rate'], 'required'],
            [['credit_year_length'], 'required'],
            [['credit_tranche_depth'], 'required'],
            [['payment_type'], 'required'],
            [['payment_day'], 'required', 'when' => $when],
            [['payment_first'], 'required'],
            //[['wallet'], 'required'],
            //[['agreement_number'], 'required'],
            [['agreement_date'], 'required'],
            //[['contractor_id'], 'required'],
            //[['currency_id'], 'required'],
            [['model_id', 'contractor_id'], 'integer'],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinancePlan::class, 'targetAttribute' => ['model_id' => 'id']],
            [['contractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::class, 'targetAttribute' => ['contractor_id' => 'id']],
            [['credit_type'], 'in', 'range' => array_keys(self::TYPE_LIST)],
            [['credit_amount'], 'double', 'min' => 0],
            [['credit_first_date'], 'date', 'format' => 'php:d.m.Y'],
            [['credit_last_date'], 'date', 'format' => 'php:d.m.Y'],
            [['credit_percent_rate'], 'double', 'min' => 0],
            [['credit_commission_rate'], 'double', 'min' => 0],
            [['credit_expiration_rate'], 'double', 'min' => 0],
            [['credit_year_length'], 'in', 'range' => array_keys(self::YEAR_LENGTH_LIST)],
            [['credit_tranche_depth'], 'integer', 'min' => 0],
            [['payment_type'], 'in', 'range' => array_keys(self::PAYMENT_TYPE_LIST)],
            [['payment_mode'], 'in', 'range' => array_keys(self::PAYMENT_MODE_LIST)],
            [['payment_day'], 'integer', 'min' => 1, 'max' => 31, 'when' => $when, 'enableClientValidation' => false],
            [['payment_first'], 'in', 'range' => array_keys(self::PAYMENT_FIRST_LIST)],
            [['payment_mode'], 'in', 'range' => array_keys(self::PAYMENT_MODE_LIST)],
            //[['wallet_value'], 'match', 'pattern' => '/^([0-9]+)[_]([0-9]+)$/'],
            //[['wallet_value'], 'validateWalletValue'],
            //[['agreement_number'], 'integer', 'min' => 1],
            //[['agreement_name'], 'string', 'max' => 64],
            //[['agreement_name'], 'trim'],
            //[['agreement_name'], 'match', 'pattern' => '#^([А-ЯЁ0-9_/-]+)$#ui'],
            [['agreement_date'], 'date', 'format' => 'php:d.m.Y'],
            //[['contractor_id'], 'integer'],
            //[['contractor_id'], 'in', 'range' => array_keys($this->getItems('contractor_id')), 'enableClientValidation' => false],
            //[['currency_id'], 'in', 'range' => array_keys($this->getItems('currency_id'))],
            [['credit_last_date'], 'validateCreditLastDate'],
        ];
    }

    public function beforeValidate()
    {
        $this->payment_day = $this->getPaymentDay();
        $this->credit_day_count = $this->getCreditDayCount();

        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {
        $dates = ['credit_first_date', 'credit_last_date', 'agreement_date'];
        array_walk($dates, [$this, 'saveDate']);

        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        //$dates = ['credit_first_date', 'credit_last_date', 'agreement_date'];
        //array_walk($dates, [$this, 'loadDate']);

        return parent::afterFind();
    }

    public function loadDate(string $attribute): void
    {
        $value = $this->getAttribute($attribute);

        if (!empty($value)) {
            $this->$attribute = DateHelper::format($value, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        }
    }

    public function saveDate(string $attribute): void
    {
        $value = $this->$attribute;

        if (!empty($value)) {
            $this->setAttribute(
                $attribute,
                DateHelper::format($this->$attribute, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE)
            );
        }
    }

    /**
     * @param string $attribute
     * @param mixed[]|null $params
     * @param InlineValidator $validator
     * @return void
     */
    public function validateCreditLastDate(string $attribute, ?array $params, InlineValidator $validator): void
    {
        if (!$this->hasErrors('credit_first_date') && !$this->hasErrors('credit_last_date')) {
            $min = DateHelper::format($this->credit_first_date, 'U', DateHelper::FORMAT_USER_DATE);
            $max = DateHelper::format($this->credit_last_date, 'U', DateHelper::FORMAT_USER_DATE);

            if ($min >= $max) {
                $message = Yii::t(
                    'yii',
                    '{attribute} must be greater than "{compareValueOrAttribute}".',
                    ['compareValueOrAttribute' => $this->getAttributeLabel('credit_first_date')]
                );
                $validator->addError($this, $attribute, $message);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_id' => 'Финансовый план',
            'contractor_id' => 'Кредитор',
            'credit_day_count' => 'Количество дней',
            'payment_amount' => 'Выплаты по основному долгу на дату уплаты процента',
            'agreement_date' => 'Дата договора',
            'debt_amount' => 'Сумма долга',
            'debt_repaid' => 'Выплаты основного долга',
            'debt_diff' => 'Остаток долга',
            'interest_amount' => 'Сумма процентов по долгу',
            'interest_repaid' => 'Сумма выплаченные процентов по долгу',
            'interest_diff' => 'Остаток процентов по долгу',
            'credit_type' => 'Вид кредита',
            'credit_status' => 'Статус кредита',
            'credit_amount' => 'Сумма кредита',
            'credit_first_date' => 'Дата получения',
            'credit_last_date' => 'Дата погашения',
            'credit_percent_rate' => 'Ставка, %-годовых',
            'credit_commission_rate' => 'Комиссия/Дисконт',
            'credit_expiration_rate' => 'Ставка при просрочке',
            'credit_year_length' => 'Число дней в году',
            'credit_tranche_depth' => 'Глубина транша (дней)',
            'payment_type' => 'Вид платежа по кредиту',
            'payment_day' => 'День платежа',
            'payment_mode' => 'Ежемесячные платежи',
            'payment_first' => 'Первый ежемесячный платеж',
            //'wallet_value' => 'Счет для зачисления',
            //'agreement_number' => 'Номер договора',
            //'agreement_name' => 'Доп. поле',
            //'checking_accountant_id' => 'Расчётные счёта',
            //'cashbox_id' => 'Кассы',
            //'emoney_id' => 'E-money',
            //'currency_id' => 'Валюта',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::class, ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(FinancePlan::class, ['id' => 'model_id']);
    }

    /**
     * @return int
     * @throws
     */
    private function getCreditDayCount()
    {
        if ($this->credit_first_date)
            $first = Date::createFromFormat(DateHelper::FORMAT_USER_DATE, $this->credit_first_date);
        if ($this->credit_last_date)
            $last = Date::createFromFormat(DateHelper::FORMAT_USER_DATE, $this->credit_last_date);

        if (isset($last) && isset($first))
            return $last->diff($first)->days;

        return 0;
    }

    /**
     * Получить день платежа
     *
     * @return int
     */
    private function getPaymentDay(): int
    {
        if ($this->payment_mode == self::PAYMENT_MODE_AGREEMENT) {
            return $this->payment_day;
        }

        if ($this->payment_mode == self::PAYMENT_MODE_CREDIT) {
            return explode('.', $this->credit_first_date)[0];
        }

        return self::PAYMENT_DAY_LAST;
    }
}
