<?php

namespace frontend\modules\analytics\models\financePlan;

use Yii;

/**
 * This is the model class for table "finance_plan_prime_cost_item".
 *
 * @property int $id
 * @property int $model_id
 * @property int $prime_cost_item_id
 * @property int $relation_type
 * @property int $action
 * @property string|null $data
 *
 * @property FinancePlan $model
 * @property FinancePlan[] $models
 * @property FinancePlanPrimeCosts $primeCostItem
 */
class FinancePlanPrimeCostItem extends FinancePlanItem
{
    const PRIME_COST_ITEM_GOODS = 1;
    const PRIME_COST_ITEM_SERVICE = 2;
    const PRIME_COST_ITEM_DELIVERY = 3;
    const PRIME_COST_ITEM_GOODS_CANCELLATION = 4;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_plan_prime_cost_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_id', 'prime_cost_item_id', 'relation_type'], 'required'],
            [['action'], 'required', 'when' => function(FinancePlanPrimeCostItem $model) {
                return $model->relation_type != self::RELATION_TYPE_UNSET;
            }],
            [['model_id', 'prime_cost_item_id', 'relation_type'], 'integer'],
            [['action'], 'filter', 'filter' => function($v) { return ((float)$v) ? self::sanitize($v) : $v; }],
            [['action'], 'number'],
            [['data'], 'string'],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinancePlan::class, 'targetAttribute' => ['model_id' => 'id']],
            [['prime_cost_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinancePlanPrimeCosts::class, 'targetAttribute' => ['prime_cost_item_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_id' => 'Model ID',
            'prime_cost_item_id' => 'Строка',
            'relation_type' => 'Зависит от строки',
            'action' => 'Умножить на',
            'data' => 'Data',
        ];
    }

    public function beforeValidate()
    {
        if (is_array($this->data))
            $this->data = json_encode($this->data);

        return parent::beforeValidate();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(FinancePlan::class, ['id' => 'model_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrimeCostItem()
    {
        return $this->hasOne(FinancePlanPrimeCosts::class, ['id' => 'prime_cost_item_id']);
    }

    /**
     * @return bool
     */
    public function isEditable()
    {
        return $this->relation_type == self::RELATION_TYPE_UNSET;
    }
}
