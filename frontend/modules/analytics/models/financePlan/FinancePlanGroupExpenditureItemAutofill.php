<?php

namespace frontend\modules\analytics\models\financePlan;

use Yii;

/**
 * This is the model class for table "finance_plan_group_expenditure_item_autofill".
 *
 * @property int $model_group_id
 * @property int $row_id
 * @property int $start_month
 * @property int|null $start_amount
 * @property int|null $amount
 * @property int|null $limit_amount
 * @property int $action
 *
 * @property FinancePlanGroup $modelGroup
 * @property FinancePlanGroupExpenditureItem $row
 */
class FinancePlanGroupExpenditureItemAutofill extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_plan_group_expenditure_item_autofill';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_group_id', 'row_id', 'start_month', 'action'], 'required'],
            [['model_group_id', 'row_id', 'start_month', 'start_amount', 'amount', 'limit_amount', 'action'], 'integer'],
            [['model_group_id', 'row_id'], 'unique', 'targetAttribute' => ['model_group_id', 'row_id']],
            [['row_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinancePlanGroupExpenditureItem::class, 'targetAttribute' => ['row_id' => 'id']],
            [['model_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinancePlanGroup::class, 'targetAttribute' => ['model_group_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'model_group_id' => 'Model Group ID',
            'row_id' => 'Row ID',
            'start_month' => 'Start Month',
            'start_amount' => 'Start Amount',
            'amount' => 'Amount',
            'limit_amount' => 'Limit Amount',
            'action' => 'Действие',
        ];
    }

    /**
     * Gets query for [[ModelGroup]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModelGroup()
    {
        return $this->hasOne(FinancePlanGroup::class, ['id' => 'model_group_id']);
    }

    /**
     * Gets query for [[Row]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRow()
    {
        return $this->hasOne(FinancePlanGroupExpenditureItem::class, ['id' => 'row_id']);
    }
}
