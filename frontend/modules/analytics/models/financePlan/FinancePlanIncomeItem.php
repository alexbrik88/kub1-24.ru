<?php

namespace frontend\modules\analytics\models\financePlan;

use common\models\document\InvoiceIncomeItem;
use Yii;

/**
 * This is the model class for table "finance_plan_income_item".
 *
 * @property int $id
 * @property int $model_id
 * @property int $income_item_id
 * @property int $type 1 - новые продажи, 2 - повторные продажи, 3 - средний чек
 * @property string|null $data
 *
 * @property InvoiceIncomeItem $incomeItem
 * @property FinancePlanIncomeItemAutofill $autofill
 * @property FinancePlan $model
 */
class FinancePlanIncomeItem extends FinancePlanItem
{
    public $relation_type; // not used
    public $action; // not used

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_plan_income_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_id', 'type'], 'required'],
            [['model_id', 'type'], 'integer'],
            [['data'], 'string'],
            [['income_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceIncomeItem::class, 'targetAttribute' => ['income_item_id' => 'id']],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinancePlan::class, 'targetAttribute' => ['model_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_id' => 'Model ID',
            'type' => 'Type',
            'data' => 'Data',
        ];
    }

    public function beforeValidate()
    {
        if (is_array($this->data))
            $this->data = json_encode($this->data);

        return parent::beforeValidate();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutofill()
    {
        return $this->hasOne(FinancePlanIncomeItemAutofill::class, ['row_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(FinancePlan::class, ['id' => 'model_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomeItem()
    {
        return $this->hasOne(InvoiceIncomeItem::class, ['id' => 'income_item_id']);
    }

}
