<?php

namespace frontend\modules\analytics\models\financePlan;

use Yii;

/**
 * This is the model class for table "finance_plan_prime_costs".
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $sort
 *
 * @property FinancePlanPrimeCostItem[] $financePlanPrimeCostItems
 */
class FinancePlanPrimeCosts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_plan_prime_costs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sort'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'sort' => 'Sort',
        ];
    }

    /**
     * Gets query for [[FinancePlanPrimeCostItems]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFinancePlanPrimeCostItems()
    {
        return $this->hasMany(FinancePlanPrimeCostItem::class, ['prime_cost_item_id' => 'id']);
    }
}
