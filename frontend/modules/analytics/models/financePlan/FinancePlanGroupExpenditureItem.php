<?php

namespace frontend\modules\analytics\models\financePlan;

use Yii;
use common\models\document\InvoiceExpenditureItem;

/**
 * This is the model class for table "finance_plan_group_expenditure_item".
 *
 * @property int $id
 * @property int $model_group_id
 * @property int $expenditure_item_id
 * @property int|null $fill_type 1 - автозаполнение, 2 - зависит от строки
 * @property int $relation_type 0 - нет, 1 - выручка, 2 - кол-во продаж, 3 - ebidta
 * @property float|null $action
 * @property string|null $data
 *
 * @property InvoiceExpenditureItem $expenditureItem
 * @property FinancePlanGroup $modelGroup
 */
class FinancePlanGroupExpenditureItem extends FinancePlanItem
{
    const FILL_TYPE_AUTOFILL = 1;
    const FILL_TYPE_FORMULA = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_plan_group_expenditure_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_group_id', 'expenditure_item_id'], 'required'],
            [['model_group_id', 'expenditure_item_id', 'fill_type', 'relation_type'], 'integer'],
            [['action'], 'filter', 'filter' => function($v) { return ((float)$v) ? self::sanitize($v) : $v; }],
            [['action'], 'number'],
            [['data'], 'string'],
            [['expenditure_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceExpenditureItem::class, 'targetAttribute' => ['expenditure_item_id' => 'id']],
            [['model_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinancePlanGroup::class, 'targetAttribute' => ['model_group_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_group_id' => 'Model Group ID',
            'expenditure_item_id' => 'Добавить строку',
            'fill_type' => 'Fill Type',
            'relation_type' => 'Зависит от строки',
            'action' => 'Действие',
            'data' => 'Data',
        ];
    }

    public function beforeValidate()
    {
        if (is_array($this->data))
            $this->data = json_encode($this->data);

        return parent::beforeValidate();
    }

    /**
     * Gets query for [[ExpenditureItem]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExpenditureItem()
    {
        return $this->hasOne(InvoiceExpenditureItem::class, ['id' => 'expenditure_item_id']);
    }

    /**
     * Gets query for [[ModelGroup]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModelGroup()
    {
        return $this->hasOne(FinancePlanGroup::class, ['id' => 'model_group_id']);
    }

    public static function sanitize($val)
    {
        return (float)str_replace([',',' '], ['.',''], $val);
    }
}
