<?php

namespace frontend\modules\analytics\models\financePlan\odds;

use Yii;
use frontend\modules\analytics\models\financePlan\FinancePlan;

/**
 * This is the model class for table "finance_plan_odds_income_item_autofill".
 *
 * @property int $model_id
 * @property int $row_id
 * @property int $start_month
 * @property int|null $start_amount
 * @property int|null $amount
 * @property int|null $limit_amount
 * @property int $action
 *
 * @property FinancePlan $model
 * @property FinancePlanOddsIncomeItem $row
 */
class FinancePlanOddsIncomeItemAutofill extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_plan_odds_income_item_autofill';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_id', 'row_id', 'start_month', 'action'], 'required'],
            [['model_id', 'row_id', 'start_month', 'start_amount', 'amount', 'limit_amount', 'action'], 'integer'],
            [['model_id', 'row_id'], 'unique', 'targetAttribute' => ['model_id', 'row_id']],
            [['row_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinancePlanOddsIncomeItem::class, 'targetAttribute' => ['row_id' => 'id']],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinancePlan::class, 'targetAttribute' => ['model_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'model_id' => 'Model ID',
            'row_id' => 'Row ID',
            'start_month' => 'Start Month',
            'start_amount' => 'Start Amount',
            'amount' => 'Amount',
            'limit_amount' => 'Limit Amount',
            'action' => 'Действие',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(FinancePlan::class, ['id' => 'model_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRow()
    {
        return $this->hasOne(FinancePlanOddsIncomeItem::class, ['id' => 'row_id']);
    }
}
