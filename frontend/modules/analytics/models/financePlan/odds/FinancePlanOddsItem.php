<?php

namespace frontend\modules\analytics\models\financePlan\odds;

use Yii;
use frontend\modules\analytics\models\financePlan\FinancePlan;

/**
 * @property FinancePlan $model
 */
class FinancePlanOddsItem extends \yii\db\ActiveRecord
{
    const PAYMENT_TYPE_PREPAYMENT = 1;
    const PAYMENT_TYPE_SURCHARGE = 2;

    const RELATION_TYPE_UNSET = 0;
    const RELATION_TYPE_REVENUE = 1;
    const RELATION_TYPE_SALES_COUNT = 2;
    const RELATION_TYPE_EBIDTA = 3;
    const RELATION_TYPE_PRIME_COST = 4;

    public static $paymentDelayList = [
        0 => 'До 30 дней',
        1 => '31-60 дней',
        2 => '61-90 дней',
        3 => '91-120 дней',
        4 => '121-150 дней',
        5 => '151-180 дней',
        6 => '181-210 дней',
        7 => '211-240 дней',
    ];

    public function getRowData()
    {
        $data = (array)json_decode($this->data, true);
        return array_map(function($v) { return $this->div100($v); }, $data);
    }

    public function setRowData($array)
    {
        $_data = [];
        foreach ($this->getRowData() as $k => $v) {
            $_data[$k] = $this->mul100(isset($array[$k])
                ? $this->sanitize($array[$k])
                : $v);
        }

        $this->data = json_encode($_data);
    }

    public function mul100($val)
    {
        return round(100 * $val);
    }

    public function div100($val)
    {
        return round((int)$val / 100, 2);
    }

    public static function sanitize($val)
    {
        return (float)str_replace([',',' '], ['.',''], $val);
    }
}
