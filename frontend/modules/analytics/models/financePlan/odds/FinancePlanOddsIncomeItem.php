<?php

namespace frontend\modules\analytics\models\financePlan\odds;

use frontend\modules\analytics\models\financePlan\FinancePlanBalanceArticle;
use frontend\modules\analytics\models\financePlan\FinancePlanCredit;
use Yii;
use frontend\modules\analytics\models\financePlan\FinancePlan;
use common\models\document\InvoiceIncomeItem;

/**
 * This is the model class for table "finance_plan_odds_income_item".
 *
 * @property int $id
 * @property int $model_id
 * @property int $income_item_id
 * @property int $credit_id
 * @property int $balance_article_id
 * @property int $type 1 - операционная деятельность, 2 - финансовая, 3 - инвестиционная
 * @property int $payment_type 1 - предоплата, 2 - доплата
 * @property int $payment_delay
 * @property int $relation_type 0 - нет, 1 - выручка, 2 - кол-во продаж, 3 - ebidta, 4 - себестоимость
 * @property float|null $action
 * @property string|null $data
 * @property int $can_edit
 *
 * @property InvoiceIncomeItem $incomeItem
 * @property FinancePlan $model
 * @property FinancePlanCredit $credit
 * @property FinancePlanBalanceArticle $balanceArticle
 */
class FinancePlanOddsIncomeItem extends FinancePlanOddsItem
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_plan_odds_income_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_id', 'income_item_id', 'type'], 'required'],
            [['model_id', 'income_item_id', 'type', 'payment_type', 'relation_type'], 'integer'],
            [['action'], 'filter', 'filter' => function($v) { return ((float)$v) ? self::sanitize($v) : $v; }],
            [['action'], 'number', 'min' => 0, 'max' => 100],
            [['payment_delay'], 'number', 'min' => 0, 'max' => 7],
            [['data'], 'string'],
            [['income_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceIncomeItem::class, 'targetAttribute' => ['income_item_id' => 'id']],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinancePlan::class, 'targetAttribute' => ['model_id' => 'id']],
            [['credit_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinancePlanCredit::class, 'targetAttribute' => ['credit_id' => 'id']],
            [['balance_article_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinancePlanBalanceArticle::class, 'targetAttribute' => ['balance_article_id' => 'id']],
            [['can_edit'], 'boolean']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_id' => 'Model ID',
            'income_item_id' => 'Добавить строку',
            'type' => 'Вид деятельности',
            'payment_type' => 'Предоплата / доплата',
            'payment_delay' => 'Отсрочка на',
            'relation_type' => 'Зависит от строки',
            'action' => '%',
            'data' => 'Data',
        ];
    }

    public function beforeValidate()
    {
        if (is_array($this->data))
            $this->data = json_encode($this->data);

        return parent::beforeValidate();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomeItem()
    {
        return $this->hasOne(InvoiceIncomeItem::class, ['id' => 'income_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(FinancePlan::class, ['id' => 'model_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCredit()
    {
        return $this->hasOne(FinancePlanCredit::class, ['id' => 'credit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBalanceArticle()
    {
        return $this->hasOne(FinancePlanBalanceArticle::class, ['id' => 'balance_article_id']);
    }

    /**
     * @return int|string
     */
    public function getTitle()
    {
        $title = ($this->incomeItem) ? $this->incomeItem->name : $this->income_item_id;

        if ($this->type == FinancePlan::ODDS_ROW_TYPE_OPERATIONS) {
            if ($this->payment_type == self::PAYMENT_TYPE_PREPAYMENT)
                $title .= ' (предоплата)';
            if ($this->payment_type == self::PAYMENT_TYPE_SURCHARGE)
                $title .= ' (отсрочка)';
        }

        if ($this->credit_id && $this->credit && $this->credit->contractor) {
            $title = $this->credit->contractor->getShortName();
        }
        if ($this->balance_article_id && $this->balanceArticle) {
            $title = $this->balanceArticle->name;
        }

        return $title;
    }
}
