<?php

namespace frontend\modules\analytics\models\financePlan;

use Yii;
use yii\data\ActiveDataProvider;
use common\models\employee\Employee;
use common\components\helpers\ArrayHelper;

/**
 * FinanceModelSearch
 */
class FinancePlanGroupSearch extends FinancePlanGroup
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'employee_id'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        /* @var $employee Employee */
        $employee = Yii::$app->user->identity;
        $query = FinancePlanGroup::find()->andWhere(['company_id' => $employee->company_id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'created_at',
                    'periodFrom' => [
                        'asc' => [
                            'year_from' => SORT_ASC,
                            'month_from' => SORT_ASC,
                        ],
                        'desc' => [
                            'year_from' => SORT_DESC,
                            'month_from' => SORT_DESC,
                        ],
                    ],
                    'periodTill' => [
                        'asc' => [
                            'year_till' => SORT_ASC,
                            'month_till' => SORT_ASC,
                        ],
                        'desc' => [
                            'year_till' => SORT_DESC,
                            'month_till' => SORT_DESC,
                        ],
                    ],
                ],
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ],
            'pagination' => [
                'pageSize' => \frontend\components\PageSize::get()
            ],
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getEmployeeFilter()
    {
        /* @var $employee Employee */
        $employee = Yii::$app->user->identity;

        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map(self::find()
            ->andWhere(['company_id' => $employee->company_id])
            ->all(), 'employee_id', 'employee.shortFio'));
    }
}
