<?php

namespace frontend\modules\analytics\models\financePlan\helpers;

use common\models\document\InvoiceExpenditureItem;
use frontend\modules\analytics\models\financePlan\FinancePlan;
use frontend\modules\analytics\models\financePlan\helpers\FinancePlanOddsCalculatorHelper as oddsCalc;

/**
 * Class FinancePlanCalculatorHelper
 */
class FinancePlanCalculatorHelper
{
    public static function getNewSales(FinancePlan $model)
    {
        $sales = $model->getEmptyRow();
        foreach ($model->incomeItems as $item) {
            if ($item->type == FinancePlan::ROW_TYPE_INCOME_NEW_SALES) {
                foreach ($item->getRowData() as $ym => $amount) {
                    $sales[$ym] += $amount;
                }
            }
        }

        return $sales;
    }

    public static function getRepeatedSales(FinancePlan $model)
    {
        $sales = $model->getEmptyRow();
        foreach ($model->incomeItems as $item) {
            if ($item->type == FinancePlan::ROW_TYPE_INCOME_SALES) {
                foreach ($item->getRowData() as $ym => $amount) {
                    $sales[$ym] += $amount;
                }
            }
        }

        return $sales;
    }

    public static function getAvgCheck(FinancePlan $model)
    {
        $avgCheck = $model->getEmptyRow();
        foreach ($model->incomeItems as $item) {
            if ($item->type == FinancePlan::ROW_TYPE_INCOME_AVG_CHECK) {
                foreach ($item->getRowData() as $ym => $amount) {
                    $avgCheck[$ym] += $amount;
                }
            }
        }

        return $avgCheck;
    }

    public static function getSalesTotal(FinancePlan $model)
    {
        $newSales = self::getNewSales($model);
        $repeatedSales = self::getRepeatedSales($model);
        $salesTotal = $model->getEmptyRow();

        foreach (array_keys($salesTotal) as $ym)
            $salesTotal[$ym] += ($newSales[$ym] + $repeatedSales[$ym]);

        return $salesTotal;
    }

    public static function getRevenue(FinancePlan $model)
    {
        $salesTotal = self::getSalesTotal($model);
        $revenue = $model->getEmptyRow();

        foreach ($model->incomeItems as $item) {
            if ($item->type == FinancePlan::ROW_TYPE_INCOME_AVG_CHECK) {
                foreach ($item->getRowData() as $ym => $amount) {
                    $revenue[$ym] += ($salesTotal[$ym] * $amount);
                }
            }
        }

        return $revenue;
    }

    public static function getPrimeCosts(FinancePlan $model, $onlyRowID = null)
    {
        $revenue = self::getRevenue($model);
        $salesTotal = self::getSalesTotal($model);
        $primeCosts = $model->getEmptyRow();

        foreach ($model->primeCostItems as $item) {

            if ($onlyRowID && $item->id != $onlyRowID)
                continue;

            foreach ($item->getRowData() as $ym => $amount) {
                switch ($item->relation_type) {
                    case $item::RELATION_TYPE_REVENUE:
                        $primeCosts[$ym] += $revenue[$ym] * (float)$item->action / 100;
                        break;
                    case $item::RELATION_TYPE_SALES_COUNT:
                        $primeCosts[$ym] += $salesTotal[$ym] * (float)$item->action;
                        break;
                    case $item::RELATION_TYPE_UNSET:
                    default:
                        $primeCosts[$ym] += $amount;
                        break;
                }
            }
        }

        return $primeCosts;
    }

    public static function getPrimeCostLevel(FinancePlan $model, $rowID)
    {
        return self::getPrimeCosts($model, $rowID);
    }

    public static function getMarginIncome(FinancePlan $model)
    {
        $revenue = self::getRevenue($model);
        $primeCosts = self::getPrimeCosts($model);
        $marginIncome = $model->getEmptyRow();

        foreach (array_keys($marginIncome) as $ym)
            $marginIncome[$ym] += ($revenue[$ym] - $primeCosts[$ym]);

        return $marginIncome;
    }

    public static function getMarginality(FinancePlan $model)
    {
        $revenue = self::getRevenue($model);
        $marginIncome = self::getMarginIncome($model);
        $marginality = $model->getEmptyRow();

        foreach (array_keys($marginIncome) as $ym)
            $marginality[$ym] = 100 * $marginIncome[$ym] / (($revenue[$ym] > 0) ? $revenue[$ym] : 9E9);

        return $marginality;
    }

    public static function getVariableCosts(FinancePlan $model, $onlyRowID = null)
    {
        $revenue = self::getRevenue($model);
        $salesTotal = self::getSalesTotal($model);
        $variableCosts = $model->getEmptyRow();

        foreach ($model->expenditureItems as $item) {

            if ($onlyRowID && $item->id != $onlyRowID)
                continue;

            if ($item->type != FinancePlan::ROW_TYPE_EXPENSE_VARIABLE)
                continue;

            foreach ($item->getRowData() as $ym => $amount) {
                switch ($item->relation_type) {
                    case $item::RELATION_TYPE_REVENUE:
                        $variableCosts[$ym] += $revenue[$ym] * (float)$item->action / 100;
                        break;
                    case $item::RELATION_TYPE_SALES_COUNT:
                        $variableCosts[$ym] += $salesTotal[$ym] * (float)$item->action;
                        break;
                    case $item::RELATION_TYPE_UNSET:
                    default:
                    $variableCosts[$ym] += $amount;
                        break;
                }
            }
        }

        return $variableCosts;
    }

    public static function getVariableCostLevel(FinancePlan $model, $rowID)
    {
        return self::getVariableCosts($model, $rowID);
    }

    public static function getGrossProfit(FinancePlan $model)
    {
        $marginIncome = self::getMarginIncome($model);
        $variableCosts = self::getVariableCosts($model);
        $grossProfit = $model->getEmptyRow();

        foreach (array_keys($grossProfit) as $ym)
            $grossProfit[$ym] += ($marginIncome[$ym] - $variableCosts[$ym]);

        return $grossProfit;
    }

    public static function getFixedCostLevel(FinancePlan $model, $rowID)
    {
        return self::getFixedCosts($model, $rowID);
    }

    public static function getFixedCosts(FinancePlan $model, $onlyRowID = false)
    {
        $fixedCosts = $model->getEmptyRow();
        foreach ($model->expenditureItems as $item) {
            if ($item->type == FinancePlan::ROW_TYPE_EXPENSE_FIXED) {

                if ($onlyRowID && $item->id != $onlyRowID)
                    continue;

                foreach ($item->getRowData() as $ym => $amount) {
                    $fixedCosts[$ym] += $amount;
                }
            }
        }

        return $fixedCosts;
    }

    public static function getOtherIncome(FinancePlan $model, $onlyRowID = false)
    {
        $otherIncome = $model->getEmptyRow();
        foreach ($model->incomeItems as $item) {
            if ($item->type == FinancePlan::ROW_TYPE_INCOME_OTHER) {

                if ($onlyRowID && $item->id != $onlyRowID)
                    continue;

                foreach ($item->getRowData() as $ym => $amount) {
                    $otherIncome[$ym] += $amount;
                }
            }
        }

        return $otherIncome;
    }

    public static function getOtherExpense(FinancePlan $model, $onlyRowID = false)
    {
        $otherExpense = $model->getEmptyRow();
        foreach ($model->expenditureItems as $item) {
            if ($item->type == FinancePlan::ROW_TYPE_EXPENSE_OTHER) {

                if ($onlyRowID && $item->id != $onlyRowID)
                    continue;

                foreach ($item->getRowData() as $ym => $amount) {
                    $otherExpense[$ym] += $amount;
                }
            }
        }

        return $otherExpense;
    }

    public static function getOtherIncomeLevel(FinancePlan $model, $rowID)
    {
        return self::getOtherIncome($model, $rowID);
    }

    public static function getOtherExpenseLevel(FinancePlan $model, $rowID)
    {
        return self::getOtherExpense($model, $rowID);
    }

    public static function getTax(FinancePlan $model)
    {
        $revenue = self::getRevenue($model);
        $ebidta = self::getEbidta($model);
        $tax = $model->getEmptyRow();

        foreach ($model->expenditureItems as $item) {

            if ($item->type != FinancePlan::ROW_TYPE_EXPENSE_TAX)
                continue;

            foreach ($item->getRowData() as $ym => $amount) {
                switch ($item->relation_type) {
                    case $item::RELATION_TYPE_REVENUE:
                        $tax[$ym] += $revenue[$ym] * (float)$item->action / 100;
                        break;
                    case $item::RELATION_TYPE_EBIDTA:
                        $tax[$ym] += $ebidta[$ym] * (float)$item->action / 100;
                        break;
                    case $item::RELATION_TYPE_UNSET:
                    default:
                        $tax[$ym] += $amount;
                        break;
                }
            }
        }

        return $tax;
    }

    public static function getEbidta(FinancePlan $model)
    {
        $grossProfit = self::getGrossProfit($model);
        $fixedCosts = self::getFixedCosts($model);
        $otherIncome = self::getOtherIncome($model);
        $otherExpense = self::getOtherExpense($model);
        $ebidta = $model->getEmptyRow();

        foreach (array_keys($ebidta) as $ym)
            $ebidta[$ym] += ($grossProfit[$ym] - $fixedCosts[$ym])
                + ($otherIncome[$ym] - $otherExpense[$ym]);

        return $ebidta;
    }

    public static function getAmortization(FinancePlan $model)
    {
        $amortization = $model->emptyRow;
        $amortizationFromOdds = oddsCalc::getAmortization($model);

        foreach (array_keys($amortization) as $ym)
            $amortization[$ym] += $amortizationFromOdds[$ym] ?? 0;

        return $amortization;
    }

    public static function getEbit(FinancePlan $model)
    {
        $ebidta = self::getEbidta($model);
        $amortization = oddsCalc::getAmortization($model);
        $ebit = $model->getEmptyRow();

        foreach (array_keys($ebidta) as $ym)
            $ebit[$ym] += ($ebidta[$ym] - $amortization[$ym]);

        return $ebit;
    }

    public static function getReceivedPercent(FinancePlan $model)
    {
        $percents = $model->emptyRow;

        return $percents;
    }

    public static function getPaymentPercent(FinancePlan $model)
    {
        $percents = $model->emptyRow;
        $percentsFromOdds = oddsCalc::getLevelByExpenditureItem($model, InvoiceExpenditureItem::ITEM_PAYMENT_PERCENT, 'credit_id');

        foreach (array_keys($percents) as $ym)
            $percents[$ym] += $percentsFromOdds[$ym] ?? 0;

        return $percents;
    }

    public static function getProfitBeforeTax(FinancePlan $model)
    {
        $ebit = self::getEbit($model);
        $receivedPercent = self::getReceivedPercent($model);
        $paymentPercent = self::getPaymentPercent($model);
        $profitBeforeTax = $model->getEmptyRow();

        foreach (array_keys($profitBeforeTax) as $ym)
            $profitBeforeTax[$ym] += ($ebit[$ym] - $paymentPercent[$ym] + $receivedPercent[$ym]);

        return $profitBeforeTax;
    }

    public static function getNetProfit(FinancePlan $model)
    {
        $ebit = self::getProfitBeforeTax($model);
        $tax = self::getTax($model);
        $netProfit = $model->getEmptyRow();

        foreach (array_keys($netProfit) as $ym)
            $netProfit[$ym] += ($ebit[$ym] - $tax[$ym]);

        return $netProfit;
    }

    public static function getNetProfitMargin(FinancePlan $model)
    {
        $revenue = self::getRevenue($model);
        $netProfit = self::getNetProfit($model);
        $netProfitMargin = $model->getEmptyRow();

        foreach (array_keys($netProfitMargin) as $ym)
            $netProfitMargin[$ym] = ($netProfit[$ym] > 0 && $revenue[$ym] > 0) ? (100 * $netProfit[$ym] / $revenue[$ym]) : 0;

        return $netProfitMargin;
    }

    public static function getNetProfitMarginTotal(FinancePlan $model)
    {
        $revenue = (array)self::getRevenue($model);
        $netProfit = (array)self::getNetProfit($model);

        $revenueTotal = array_sum($revenue);
        $netProfitTotal = array_sum($netProfit);

        if ($revenueTotal != 0)
            return $netProfitTotal / $revenueTotal * 100;

        return 0;
    }
}