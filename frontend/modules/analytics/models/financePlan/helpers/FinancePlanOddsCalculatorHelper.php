<?php

namespace frontend\modules\analytics\models\financePlan\helpers;

use frontend\modules\analytics\models\financePlan\FinancePlan;
use frontend\modules\analytics\models\financePlan\odds\FinancePlanOddsItem;


/**
 * Class FinancePlanOddsCalculatorHelper
 */
class FinancePlanOddsCalculatorHelper extends FinancePlanCalculatorHelper
{
    // OPERATING
    
    public static function getOperatingIncomes(FinancePlan $model, $onlyRowID = null)
    {
        $revenue = self::getRevenue($model);
        $ret = $model->getEmptyRow();

        foreach ($model->oddsIncomeItems as $item) {

            if ($item->type != FinancePlan::ODDS_ROW_TYPE_OPERATIONS)
                continue;
            
            if ($onlyRowID && $item->id != $onlyRowID)
                continue;

            $rowData = $item->getRowData();
            foreach ($rowData as $ym => $amount) {
                switch ($item->relation_type) {
                    case $item::RELATION_TYPE_REVENUE:
                        if ($item->payment_type == FinancePlanOddsItem::PAYMENT_TYPE_SURCHARGE) {
                            if ($prevYm = self::_getPrevYM($ym, $rowData, $item->payment_delay)) {
                                $ret[$ym] += $revenue[$prevYm] * (float)$item->action / 100;
                            } else {
                                $ret[$ym] += 0;
                            }
                        } else {
                            $ret[$ym] += $revenue[$ym] * (float)$item->action / 100;
                        }
                        break;
                    case $item::RELATION_TYPE_UNSET:
                    default:
                        $ret[$ym] += $amount;
                        break;
                }
            }
        }

        return $ret;
    }

    public static function getOperatingIncomesLevel(FinancePlan $model, $rowID)
    {
        return self::getOperatingIncomes($model, $rowID);
    }

    public static function getOperatingExpenses(FinancePlan $model, $onlyRowID = null)
    {
        $primeCosts = self::getPrimeCosts($model);
        $ret = $model->getEmptyRow();

        foreach ($model->oddsExpenditureItems as $item) {

            if ($item->type != FinancePlan::ODDS_ROW_TYPE_OPERATIONS)
                continue;

            if ($onlyRowID && $item->id != $onlyRowID)
                continue;

            $rowData = $item->getRowData();
            foreach ($rowData as $ym => $amount) {
                switch ($item->relation_type) {
                    case $item::RELATION_TYPE_PRIME_COST:
                        if ($item->payment_type == FinancePlanOddsItem::PAYMENT_TYPE_SURCHARGE) {
                            if ($prevYm = self::_getPrevYM($ym, $rowData, $item->payment_delay)) {
                                $ret[$ym] += $primeCosts[$prevYm] * (float)$item->action / 100;
                            } else {
                                $ret[$ym] += 0;
                            }
                        } else {
                            $ret[$ym] += $primeCosts[$ym] * (float)$item->action / 100;
                        }
                        break;
                    case $item::RELATION_TYPE_UNSET:
                    default:
                        $ret[$ym] += $amount;
                        break;
                }
            }
        }

        return $ret;
    }

    public static function getOperatingExpensesLevel(FinancePlan $model, $rowID)
    {
        return self::getOperatingExpenses($model, $rowID);
    }

    public static function getOperatingTotal(FinancePlan $model)
    {
        $incomes = self::getOperatingIncomes($model);
        $expenses = self::getOperatingExpenses($model);
        $fixedCosts = self::getFixedCosts($model);
        $oddsTax = self::getOddsTax($model);
        $ret = $model->getEmptyRow();

        foreach ($ret as $ym => $amount) {
            $ret[$ym] += $incomes[$ym];
            $ret[$ym] -= $expenses[$ym];
            $ret[$ym] -= $fixedCosts[$ym];
            $ret[$ym] -= $oddsTax[$ym];
        }

        return $ret;
    }

    public static function getOddsTax(FinancePlan $model)
    {
        $tax = self::getTax($model);
        $oddsTax = $model->emptyRow;

        $_sum = 0;
        foreach ($tax as $ym => $amount) {
            $y = substr($ym, 0, 4);
            $m = substr($ym, 4, 2);

            if (in_array($m, ['01', '04', '07', '10'])) {
                $oddsTax[$ym] += $_sum;
                $_sum = 0;
            }

            $_sum += $amount;
        }

        return $oddsTax;
    }

    // FINANCES
    
    public static function getFinancesIncomes(FinancePlan $model, $onlyRowID = null)
    {
        $ret = $model->getEmptyRow();

        foreach ($model->oddsIncomeItems as $item) {
            if ($item->type == FinancePlan::ODDS_ROW_TYPE_FINANCES) {

                if ($onlyRowID && $item->id != $onlyRowID)
                    continue;

                foreach ($item->getRowData() as $ym => $amount) {
                    $ret[$ym] += $amount;
                }
            }
        }

        return $ret;
    }

    public static function getFinancesIncomesLevel(FinancePlan $model, $rowID)
    {
        return self::getFinancesIncomes($model, $rowID);
    }

    public static function getFinancesExpenses(FinancePlan $model, $onlyRowID = null)
    {
        $ret = $model->getEmptyRow();

        foreach ($model->oddsExpenditureItems as $item) {
            if ($item->type == FinancePlan::ODDS_ROW_TYPE_FINANCES) {

                if ($onlyRowID && $item->id != $onlyRowID)
                    continue;

                foreach ($item->getRowData() as $ym => $amount) {
                    $ret[$ym] += $amount;
                }
            }
        }

        return $ret;
    }

    public static function getFinancesExpensesLevel(FinancePlan $model, $rowID)
    {
        return self::getFinancesExpenses($model, $rowID);
    }

    public static function getFinancesTotal(FinancePlan $model)
    {
        $incomes = self::getFinancesIncomes($model);
        $expenses = self::getFinancesExpenses($model);
        $ret = $model->getEmptyRow();

        foreach ($ret as $ym => $amount) {
            $ret[$ym] = $incomes[$ym] - $expenses[$ym];
        }

        return $ret;
    }

    // INVESTMENTS

    public static function getInvestmentsIncomes(FinancePlan $model, $onlyRowID = null)
    {
        $ret = $model->getEmptyRow();

        foreach ($model->oddsIncomeItems as $item) {
            if ($item->type == FinancePlan::ODDS_ROW_TYPE_INVESTMENTS) {

                if ($onlyRowID && $item->id != $onlyRowID)
                    continue;

                foreach ($item->getRowData() as $ym => $amount) {
                    $ret[$ym] += $amount;
                }
            }
        }

        return $ret;
    }

    public static function getInvestmentsIncomesLevel(FinancePlan $model, $rowID)
    {
        return self::getInvestmentsIncomes($model, $rowID);
    }

    public static function getInvestmentsExpenses(FinancePlan $model, $onlyRowID = null)
    {
        $ret = $model->getEmptyRow();

        foreach ($model->oddsExpenditureItems as $item) {
            if ($item->type == FinancePlan::ODDS_ROW_TYPE_INVESTMENTS) {

                if ($onlyRowID && $item->id != $onlyRowID)
                    continue;

                foreach ($item->getRowData() as $ym => $amount) {
                    $ret[$ym] += $amount;
                }
            }
        }

        return $ret;
    }

    public static function getInvestmentsExpensesLevel(FinancePlan $model, $rowID)
    {
        return self::getInvestmentsExpenses($model, $rowID);
    }

    public static function getInvestmentsTotal(FinancePlan $model)
    {
        $incomes = self::getInvestmentsIncomes($model);
        $expenses = self::getInvestmentsExpenses($model);
        $ret = $model->getEmptyRow();

        foreach ($ret as $ym => $amount) {
            $ret[$ym] = $incomes[$ym] - $expenses[$ym];
        }

        return $ret;
    }

    public static function getLevelByIncomeItem(FinancePlan $model, $incomeItemIds, $withAttr = null)
    {
        $ret = $model->getEmptyRow();
        $incomeItemIds = (array)$incomeItemIds;

        foreach ($model->oddsIncomeItems as $item) {
            if (in_array($item->income_item_id, $incomeItemIds) && (!$withAttr || $item->{$withAttr})) {
                foreach ($item->getRowData() as $ym => $amount) {
                    $ret[$ym] += $amount;
                }
            }
        }

        return $ret;
    }

    public static function getLevelByExpenditureItem(FinancePlan $model, $expenditureItemIds, $withAttr = null)
    {
        $ret = $model->getEmptyRow();
        $expenditureItemIds = array($expenditureItemIds);

        foreach ($model->oddsExpenditureItems as $item) {
            if (in_array($item->expenditure_item_id, $expenditureItemIds) && (!$withAttr || $item->{$withAttr})) {
                foreach ($item->getRowData() as $ym => $amount) {
                    $ret[$ym] += $amount;
                }
            }
        }

        return $ret;
    }

    public static function getAmortization(FinancePlan $model)
    {
        $ret = $model->getEmptyRow();

        foreach ($model->oddsExpenditureItems as $item) {
            if ($item->balance_article_id) {
                $balanceArticle = $item->balanceArticle;
                $date = date_create_from_format('Y-m-d', $balanceArticle->purchased_at);
                $months = $balanceArticle->useful_life_in_month ?: 1;
                $rawAmortization = [];
                for ($i = 0; $i <= $months; $i++) {
                    $date = $date->modify('last day of this month')->setTime(23, 59, 59);
                    $date = $date->modify("+1 second");
                    $rawAmortization[$date->format('Ym')] = $balanceArticle->amount / $months / 100;
                }
                foreach ($ret as $ym => $amount) {
                    $ret[$ym] += $rawAmortization[$ym] ?? 0;
                }
            }
        }

        return $ret;
    }

    // TOTALS

    public static function getOddsNetProfit(FinancePlan $model)
    {
        $operations = self::getOperatingTotal($model);
        $finances = self::getFinancesTotal($model);
        $investments = self::getInvestmentsTotal($model);
        $ret = $model->getEmptyRow();

        foreach ($ret as $ym => $amount) {
            $ret[$ym] = $operations[$ym] + $finances[$ym] + $investments[$ym];
        }

        return $ret;
    }

    public static function getOddsBalance(FinancePlan $model)
    {
        $netProfit = self::getOddsNetProfit($model);
        $ret = $model->getEmptyRow();

        $prevMonthProfit = 0;
        foreach ($ret as $ym => $amount) {
            $ret[$ym] = $netProfit[$ym] + $prevMonthProfit;
            $prevMonthProfit += $netProfit[$ym];
        }

        return $ret;
    }

    // HELPERS

    private static function _getPrevYM($key, $hash = array(), $offsetMonths = 0)
    {
        $keys = array_keys($hash);
        $found_index = array_search($key, $keys);
        if ($found_index === false || $found_index < $offsetMonths)
            return false;

        return $keys[$found_index - $offsetMonths];
    }
}