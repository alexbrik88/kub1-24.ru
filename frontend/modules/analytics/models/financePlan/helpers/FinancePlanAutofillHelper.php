<?php

namespace frontend\modules\analytics\models\financePlan\helpers;


use frontend\modules\analytics\models\financePlan\FinancePlanExpenditureItem;
use frontend\modules\analytics\models\financePlan\FinancePlanExpenditureItemAutofill;
use frontend\modules\analytics\models\financePlan\FinancePlanGroup;
use frontend\modules\analytics\models\financePlan\FinancePlanGroupExpenditureItem;
use frontend\modules\analytics\models\financePlan\FinancePlanGroupExpenditureItemAutofill;
use frontend\modules\analytics\models\financePlan\FinancePlanIncomeItem;
use frontend\modules\analytics\models\financePlan\FinancePlanIncomeItemAutofill;
use frontend\modules\analytics\models\financePlan\odds\FinancePlanOddsExpenditureItem;
use frontend\modules\analytics\models\financePlan\odds\FinancePlanOddsExpenditureItemAutofill;
use frontend\modules\analytics\models\financePlan\odds\FinancePlanOddsIncomeItem;
use frontend\modules\analytics\models\financePlan\odds\FinancePlanOddsIncomeItemAutofill;
use yii\helpers\Url;
use common\components\helpers\ArrayHelper;
use frontend\modules\analytics\models\financePlan\FinancePlan;


/**
 * Class FinancePlanAutofillHelper
 */
class FinancePlanAutofillHelper {

    public const AUTO_PLANNING_ACTION_START_AMOUNT = 0;
    public const AUTO_PLANNING_ACTION_PLUS = 1;
    public const AUTO_PLANNING_ACTION_MINUS = 2;
    public const AUTO_PLANNING_ACTION_MULTIPLY = 3;

    public static array $autoPlanningActionsMap = [
        self::AUTO_PLANNING_ACTION_START_AMOUNT => '=Нач. значение',
        self::AUTO_PLANNING_ACTION_PLUS => '+',
        self::AUTO_PLANNING_ACTION_MINUS => '-',
        self::AUTO_PLANNING_ACTION_MULTIPLY => '*',
    ];

    public static function fillIncome(FinancePlanIncomeItemAutofill $autofill, FinancePlanIncomeItem $item)
    {
        return self::fill($autofill, $item);
    }

    public static function fillExpense(FinancePlanExpenditureItemAutofill $autofill, FinancePlanExpenditureItem $item)
    {
        return self::fill($autofill, $item);
    }

    public static function fillOddsIncome(FinancePlanOddsIncomeItemAutofill $autofill, FinancePlanOddsIncomeItem $item)
    {
        return self::fill($autofill, $item);
    }

    public static function fillOddsExpense(FinancePlanOddsExpenditureItemAutofill $autofill, FinancePlanOddsExpenditureItem $item)
    {
        return self::fill($autofill, $item);
    }

    public static function fillGroupExpense(FinancePlanGroupExpenditureItemAutofill $autofill, FinancePlanGroupExpenditureItem $item)
    {
        return self::fill($autofill, $item);
    }

    private static function fill($autofill, $item)
    {
        $rowData = [];

        $action = $autofill->action;
        $startAmount = $autofill->start_amount;
        $startMonth = $autofill->start_month;
        $amount = $autofill->amount ?: null;
        $limitAmount = $autofill->limit_amount ?: 0;

        $calculatedAmount = $startAmount;
        foreach ($item->getRowData() as $ym => $oldAmount) {
            switch ($action) {
                case self::AUTO_PLANNING_ACTION_START_AMOUNT:
                    $calculatedAmount = $startAmount;
                    break;
                case self::AUTO_PLANNING_ACTION_PLUS:
                    if ($ym > $startMonth) {
                        $calculatedAmount += $amount;
                        if ($limitAmount > 0 && $calculatedAmount > $limitAmount)
                            $calculatedAmount = $limitAmount;
                    }
                    break;
                case self::AUTO_PLANNING_ACTION_MULTIPLY:
                    if ($ym > $startMonth) {
                        $calculatedAmount *= $amount / 100;
                        if ($limitAmount > 0 && $calculatedAmount > $limitAmount)
                            $calculatedAmount = $limitAmount;
                    }
                    break;
                case self::AUTO_PLANNING_ACTION_MINUS:
                    if ($ym > $startMonth) {
                        $calculatedAmount -= $amount;
                        if ($limitAmount > 0 && $calculatedAmount < $limitAmount)
                            $calculatedAmount = $limitAmount;
                    }
                    break;
            }

            $rowData[$ym] = ($ym >= $startMonth)
                ? $calculatedAmount / 100
                : $oldAmount;
        }

        return $rowData;
    }

    /**
     * @param FinancePlan $activePlan
     * @param $isEditMode bool
     * @return array
     */
    public static function getPanelDataJS(FinancePlan $activePlan, $isEditMode)
    {
        $panelDataJS = [
            'incomeAutofill' => [],
            'expenseAutofill' => [],
            'primeCost' => [],
            'variableCost' => [],
            'fixedCost' => [],
            'taxCost' => [],
            'oddsIncomeAutofill' => [],
            'oddsExpenseAutofill' => [],
            'oddsSystemIncome' => [],
            'oddsSystemExpense' => [],
            'otherIncome' => [],
            'otherExpense' => []
        ];

        if ($isEditMode) {

            foreach ($activePlan->incomeAutofills as $autofill) {
                $panelDataJS['incomeAutofill'][$autofill->row_id] = [
                    'start_month' => $autofill->start_month,
                    'start_amount' => $autofill->start_amount,
                    'amount' => $autofill->amount,
                    'limit_amount' => $autofill->limit_amount,
                    'action' => $autofill->action
                ];
            }
            foreach ($activePlan->expenditureAutofills as $autofill) {
                $panelDataJS['expenseAutofill'][$autofill->row_id] = [
                    'start_month' => $autofill->start_month,
                    'start_amount' => $autofill->start_amount,
                    'amount' => $autofill->amount,
                    'limit_amount' => $autofill->limit_amount,
                    'action' => $autofill->action
                ];
            }
            foreach ($activePlan->primeCostItems as $item) {
                $panelDataJS['primeCost'][$item->id] = [
                    'name' => $item->primeCostItem->name,
                    'item' => $item->prime_cost_item_id,
                    'relation' => $item->relation_type,
                    'action' => $item->action
                ];
            }
            foreach ($activePlan->incomeItems as $item) {
                if ($item->type == FinancePlan::ROW_TYPE_INCOME_OTHER)
                    $panelDataJS['otherIncome'][$item->id] = [
                        'name' => $item->incomeItem->name,
                        'item' => $item->incomeItem,
                        'relation' => $item->relation_type,
                        'action' => $item->action
                    ];
            }
            foreach ($activePlan->expenditureItems as $item) {
                if ($item->type == FinancePlan::ROW_TYPE_EXPENSE_VARIABLE)
                    $panelDataJS['variableCost'][$item->id] = [
                        'name' => $item->expenditureItem->name,
                        'item' => $item->expenditure_item_id,
                        'relation' => $item->relation_type,
                        'action' => $item->action
                    ];
                elseif ($item->type == FinancePlan::ROW_TYPE_EXPENSE_FIXED)
                    $panelDataJS['fixedCost'][$item->id] = [
                        'name' => $item->expenditureItem->name,
                        'item' => $item->expenditure_item_id,
                        'relation' => $item->relation_type,
                        'action' => $item->action
                    ];
                elseif ($item->type == FinancePlan::ROW_TYPE_EXPENSE_TAX)
                    $panelDataJS['taxCost']['tax'] = [
                        'name' => 'Налог',
                        'item' => $item->expenditure_item_id,
                        'relation' => $item->relation_type,
                        'action' => $item->action
                    ];
                elseif ($item->type == FinancePlan::ROW_TYPE_EXPENSE_OTHER)
                    $panelDataJS['otherExpense'][$item->id] = [
                        'name' => $item->expenditureItem->name,
                        'item' => $item->expenditure_item_id,
                        'relation' => $item->relation_type,
                        'action' => $item->action
                    ];
            }
            foreach ($activePlan->oddsIncomeAutofills as $autofill) {
                $panelDataJS['oddsIncomeAutofill'][$autofill->row_id] = [
                    'start_month' => $autofill->start_month,
                    'start_amount' => $autofill->start_amount,
                    'amount' => $autofill->amount,
                    'limit_amount' => $autofill->limit_amount,
                    'action' => $autofill->action
                ];
            }
            foreach ($activePlan->oddsExpenditureAutofills as $autofill) {
                $panelDataJS['oddsExpenseAutofill'][$autofill->row_id] = [
                    'start_month' => $autofill->start_month,
                    'start_amount' => $autofill->start_amount,
                    'amount' => $autofill->amount,
                    'limit_amount' => $autofill->limit_amount,
                    'action' => $autofill->action
                ];
            }
            foreach ($activePlan->oddsIncomeItems as $item) {
                $panelDataJS['oddsSystemIncome'][$item->id] = [
                    'action' => $item->action,
                    'payment_delay' => $item->payment_delay
                ];
            }
            foreach ($activePlan->oddsExpenditureItems as $item) {
                $panelDataJS['oddsSystemExpense'][$item->id] = [
                    'action' => $item->action,
                    'payment_delay' => $item->payment_delay
                ];
            }
        }

        $panelDataJS['incomeAutofill'] = json_encode($panelDataJS['incomeAutofill']);
        $panelDataJS['expenseAutofill'] = json_encode($panelDataJS['expenseAutofill']);
        $panelDataJS['primeCost'] = json_encode($panelDataJS['primeCost']);
        $panelDataJS['variableCost'] = json_encode($panelDataJS['variableCost']);
        $panelDataJS['fixedCost'] = json_encode($panelDataJS['fixedCost']);
        $panelDataJS['taxCost'] = json_encode($panelDataJS['taxCost']);
        $panelDataJS['oddsIncomeAutofill'] = json_encode($panelDataJS['oddsIncomeAutofill']);
        $panelDataJS['oddsExpenseAutofill'] = json_encode($panelDataJS['oddsExpenseAutofill']);
        $panelDataJS['oddsSystemIncome'] = json_encode($panelDataJS['oddsSystemIncome']);
        $panelDataJS['oddsSystemExpense'] = json_encode($panelDataJS['oddsSystemExpense']);
        $panelDataJS['otherIncome'] = json_encode($panelDataJS['otherIncome']);
        $panelDataJS['otherExpense'] = json_encode($panelDataJS['otherExpense']);

        return $panelDataJS;
    }

    /**
     * @param FinancePlanGroup $activePlanGroup
     * @param $isEditMode bool
     * @return array
     */
    public static function getGroupPanelDataJS(FinancePlanGroup $activePlanGroup, $isEditMode)
    {
        $panelDataJS = [
            'groupExpenseAutofill' => [],
        ];

        if ($isEditMode) {

            foreach ($activePlanGroup->expenditureAutofills as $autofill) {
                $panelDataJS['groupExpenseAutofill'][$autofill->row_id] = [
                    'start_month' => $autofill->start_month,
                    'start_amount' => $autofill->start_amount,
                    'amount' => $autofill->amount,
                    'limit_amount' => $autofill->limit_amount,
                    'action' => $autofill->action
                ];
            }
        }

        $panelDataJS['groupExpenseAutofill'] = json_encode($panelDataJS['groupExpenseAutofill']);

        return $panelDataJS;
    }

    public static function getTooltipText()
    {
        return [
            'startAmount' => '<span>Начальное значение - значение в ячейке месяца, который выбран в поле "С месяца". В следующих полях вы задаете формулу, как в ячейках от месяца указанного в поле слева должны меняться значения.</span>',
            'action' => '<span>"= Нач. значение" - значение у всех ячеек правее будут равно цифре в поле "Нач. значение"</span>',
            'amount' => '<span>Укажите число, которое нужно прибавить, или вычесть или на которое умножить. Если вам нужно увеличить последующие цифры например на 10%, то вам нужно выбрать действие умножение "*" и в поле "число" поставить "1,1"</span>',
            'limitAmount' => '<span>В данном поле вы можете указать значение выше которого не может быть значение в ячейке</span>',
        ];
    }

}