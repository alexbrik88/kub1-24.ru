<?php

namespace frontend\modules\analytics\models\financePlan\helpers;

use frontend\modules\analytics\models\financePlan\FinancePlan;
use frontend\modules\analytics\models\financePlan\FinancePlanGroup;


/**
 * Class FinancePlanGroupCalculatorHelper
 */
class FinancePlanGroupCalculatorHelper
{
    public static function getExpensesLevel(FinancePlanGroup $model, $rowID)
    {
        return self::getExpenses($model, $rowID);
    }

    public static function getExpenses(FinancePlanGroup $model, $onlyRowID = false)
    {
        $expenses = $model->getEmptyRow();
        foreach ($model->expenditureItems as $item) {

                if ($onlyRowID && $item->id != $onlyRowID)
                    continue;

                foreach ($item->getRowData() as $ym => $amount) {
                    $expenses[$ym] += $amount;
                }
        }

        return $expenses;
    }
}