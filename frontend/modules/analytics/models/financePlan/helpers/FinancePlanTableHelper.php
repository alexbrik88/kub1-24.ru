<?php

namespace frontend\modules\analytics\models\financePlan\helpers;

use frontend\modules\reference\models\ArticlesSearch;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\analytics\models\financePlan\FinancePlan;
use frontend\modules\analytics\models\financePlan\helpers\FinancePlanCalculatorHelper as calc;
use frontend\modules\analytics\models\financePlan\helpers\FinancePlanOddsCalculatorHelper as oddsCalc;
use common\models\document\InvoiceExpenditureItem as ExpenditureItem;
use common\models\document\InvoiceIncomeItem as IncomeItem;

/**
 * Class FinancePlanTableHelper
 */
class FinancePlanTableHelper
{
    // Table 1
    const SALES_FUNNEL = 'sales_funnel';
    const SALES_FUNNEL_TOTAL = 'sales_funnel_total';

    // Table 2
    const PROFIT_AND_LOSS = 'profit_and_loss';
    const REVENUE = 'revenue';
    const SALES_TOTAL = 'sales_total';
    const PRIME_COST = 'prime_cost';
    const MARGIN_INCOME = 'margin_income';
    const VARIABLE_COST = 'variable_cost';
    const GROSS_PROFIT = 'gross_profit';
    const FIXED_COST = 'fixed_cost';
    const OTHER_INCOME = 'other_income';
    const OTHER_EXPENSE = 'other_expense';
    const EBIDTA = 'ebidta';
    const AMORTIZATION = 'amortization';
    const EBIT = 'ebit';
    const RECEIVED_PERCENT = 'received_percent';
    const PAID_PERCENT = 'paid_percent';
    const PROFIT_BEFORE_TAX = 'profit_before_tax';
    const TAX = 'tax';
    const NET_PROFIT = 'net_profit';
    const NET_PROFIT_MARGIN = 'net_profit_margin';

    // Table 3
    const ODDS = 'odds';
    const ODDS_OPERATIONS = 'odds_operations';
    const ODDS_FINANCES = 'odds_finances';
    const ODDS_INVESTMENTS = 'odds_investments';
    const ODDS_OPERATIONS_INCOME = 'oddsIncome';
    const ODDS_OPERATIONS_EXPENSE = 'oddsExpense';
    const ODDS_OPERATIONS_FIXED_COSTS = 'odds_operations_fixed_costs';
    const ODDS_OPERATIONS_GROUP_EXPENSES = 'odds_operations_group_expenses';
    const ODDS_OPERATIONS_TAX = 'odds_operations_tax';
    const ODDS_FINANCES_INCOME = 'oddsIncome';
    const ODDS_FINANCES_EXPENSE = 'oddsExpense';
    const ODDS_INVESTMENTS_INCOME = 'oddsIncome';
    const ODDS_INVESTMENTS_EXPENSE = 'oddsExpense';
    const ODDS_NET_PROFIT = 'odds_net_profit';
    const ODDS_BALANCE = 'odds_balance';

    // credit
    const ODDS_ROW_CREDIT = 'odds_credit';
    const ODDS_ROW_LOAN = 'odds_loan';
    const ODDS_ROW_CREDIT_PERCENT = 'odds_credit_loan_percent';
    const ODDS_ROW_LOAN_PERCENT = 'odds_credit_loan_percent';

    // balanceArticle
    const ODDS_ROW_BALANCE_ARTICLE_FIXED = 'odds_balance_article_fixed';
    const ODDS_ROW_BALANCE_ARTICLE_INTANGIBLE = 'odds_balance_article_intangible';

    // Table GroupExpenses
    const GROUP_EXPENSE = 'group_expense';

    // Panels
    const TRIGGER_AUTOFILL_CLASS = 'edit-autofill-js';
    const TRIGGER_PRIME_COSTS_CLASS = 'edit-prime-cost-js';
    const TRIGGER_VARIABLE_COSTS_CLASS = 'edit-variable-cost-js';
    const TRIGGER_FIXED_COSTS_CLASS = 'edit-fixed-cost-js';
    const TRIGGER_OTHER_INCOME_CLASS = 'edit-other-income-js';
    const TRIGGER_OTHER_EXPENSE_CLASS = 'edit-other-expense-js';
    const TRIGGER_TAX_CLASS = 'edit-tax-js';
    const TRIGGER_ODDS_INCOME_CLASS = 'edit-odds-income-js';
    const TRIGGER_ODDS_EXPENSE_CLASS = 'edit-odds-expense-js';
    const TRIGGER_GROUP_EXPENSE_CLASS = 'edit-group-expense-js';
    const TRIGGER_ODDS_SYSTEM_INCOME_CLASS = 'edit-odds-system-income-js';
    const TRIGGER_ODDS_SYSTEM_EXPENSE_CLASS = 'edit-odds-system-expense-js';

    // Buttons
    const TRIGGER_ODDS_DELETE_CREDIT = 'delete-odds-credit-js';
    const TRIGGER_ODDS_UPDATE_CREDIT = 'update-odds-credit-js';
    const TRIGGER_ODDS_DELETE_BALANCE_ARTICLE = 'delete-odds-balance-article-js';
    const TRIGGER_ODDS_UPDATE_BALANCE_ARTICLE = 'update-odds-balance-article-js';

    /**
     * @param FinancePlan $model
     * @return array
     */
    public static function getSalesFunnel(FinancePlan $model)
    {
        $modelData = [];

        $modelData[self::SALES_FUNNEL] = [
            'title' => 'Воронка продаж',
            'totalBlock' => true,
            'isButtonable' => true,
            'data' => null,
            'levels' => [
                'systemIncome_' . FinancePlan::ROW_TYPE_INCOME_NEW_SALES => [
                    'title' => 'Новые продажи, шт',
                    'data' => calc::getNewSales($model),
                    'isEditable' => true,
                    'editAttrJS' => self::TRIGGER_AUTOFILL_CLASS,
                    'isInt' => true,
                    'rowID' => null
                ],
                'systemIncome_' . FinancePlan::ROW_TYPE_INCOME_SALES => [
                    'title' => 'Повторные продажи, шт',
                    'data' => calc::getRepeatedSales($model),
                    'isEditable' => true,
                    'editAttrJS' => self::TRIGGER_AUTOFILL_CLASS,
                    'isInt' => true,
                    'rowID' => null
                ],
                self::SALES_FUNNEL_TOTAL => [
                    'title' => 'Итого продажи, шт',
                    'data' => calc::getSalesTotal($model),
                    'isInt' => true
                ]
            ]
        ];

        foreach ($model->incomeItems as $item) {
            if ($item->type == FinancePlan::ROW_TYPE_INCOME_SALES || $item->type == FinancePlan::ROW_TYPE_INCOME_NEW_SALES) {
                $key = 'systemIncome_' . $item->type;
                $modelData[self::SALES_FUNNEL]['levels'][$key]['rowID'] = $item->id;
            }
        }

        return $modelData;
    }

    /**
     * @param FinancePlan $model
     * @return array
     */
    public static function getProfitAndLoss(FinancePlan $model)
    {
        $modelData = [];

        // Выручка //////////////////////////////////////////////////////////////////////////////////////////

        $modelData[self::PROFIT_AND_LOSS] = [
            'title' => 'ОТЧЕТ о ПРИБЫЛЯХ и УБЫТКАХ',
            'totalBlock' => true,
            'isButtonable' => false,
            'data' => null,
            'levels' => []
        ];

        $modelData[self::REVENUE] = [
            'title' => 'Выручка, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'isButtonable' => true,
            'data' => calc::getRevenue($model),
            'levels' => [
                self::SALES_TOTAL => [
                    'title' => 'Кол-во продаж, шт',
                    'data' => calc::getSalesTotal($model),
                    'isInt' => true
                ],
                'systemIncome_' . FinancePlan::ROW_TYPE_INCOME_AVG_CHECK => [
                    'title' => 'Средний чек, ' . $model->getCurrencySymbol(),
                    'data' => calc::getAvgCheck($model),
                    'isEditable' => true,
                    'isAvgRow' => true,
                    'editAttrJS' => self::TRIGGER_AUTOFILL_CLASS,
                    'rowID' => null
                ],
            ]
        ];

        foreach ($model->incomeItems as $item) {
            if ($item->type == FinancePlan::ROW_TYPE_INCOME_AVG_CHECK) {
                $key = 'systemIncome_' . $item->type;
                $modelData[self::REVENUE]['levels'][$key]['rowID'] = $item->id;
            }
        }

        // Себестоимость //////////////////////////////////////////////////////////////////////////////////////////

        $modelData[self::PRIME_COST] = [
            'title' => 'Себестоимость, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'isButtonable' => true,
            'isExpandable' => true,
            'editAttrJS' => self::TRIGGER_PRIME_COSTS_CLASS,
            'data' => calc::getPrimeCosts($model),
            'levels' => []
        ];

        // todo: move into method self::_getSubrows()
        foreach ($model->primeCostItems as $item) {
            $key = 'prime_cost_' . $item->id;
            $modelData[self::PRIME_COST]['levels'][$key] = [
                'title' => ($item->primeCostItem) ? $item->primeCostItem->name : $key,
                'data' => calc::getPrimeCostLevel($model, $item->id),
                'isEditable' => $item->isEditable(),
                'editAttrJS' => self::TRIGGER_PRIME_COSTS_CLASS,
                'rowID' => $item->id
            ];
        }

        // Маржинальный доход ///////////////////////////////////////////////////////////////////////////////////

        $modelData[self::MARGIN_INCOME] = [
            'title' => 'МАРЖИНАЛЬНЫЙ ДОХОД, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'data' => calc::getMarginIncome($model),
            'levels' => []
        ];

        // Переменные расходы ///////////////////////////////////////////////////////////////////////////////////

        $modelData[self::VARIABLE_COST] = [
            'title' => 'Переменные расходы, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'isButtonable' => true,
            'isExpandable' => true,
            'editAttrJS' => self::TRIGGER_VARIABLE_COSTS_CLASS,
            'data' => calc::getVariableCosts($model),
            'levels' => []
        ];

        // todo: move into method self::_getSubrows()
        foreach ($model->expenditureItems as $item) {
            if ($item->type == FinancePlan::ROW_TYPE_EXPENSE_VARIABLE) {
                $key = 'expense_' . $item->id;
                $modelData[self::VARIABLE_COST]['levels'][$key] = [
                    'title' => ($item->expenditureItem) ? $item->expenditureItem->name : $key,
                    'data' => calc::getVariableCostLevel($model, $item->id),
                    'isEditable' => $item->isEditable(),
                    'editAttrJS' => self::TRIGGER_VARIABLE_COSTS_CLASS,
                    'rowID' => $item->id
                ];
            }
        }

        // Валовая прибыль ///////////////////////////////////////////////////////////////////////////////////

        $modelData[self::GROSS_PROFIT] = [
            'title' => 'ВАЛОВАЯ ПРИБЫЛЬ, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'data' => calc::getGrossProfit($model),
            'levels' => []
        ];

        // Постоянные расходы ///////////////////////////////////////////////////////////////////////////////////

        $modelData[self::FIXED_COST] = [
            'title' => 'Постоянные расходы, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'isButtonable' => true,
            'isExpandable' => true,
            'editAttrJS' => self::TRIGGER_FIXED_COSTS_CLASS,
            'data' => calc::getFixedCosts($model),
            'levels' => []
        ];

        // todo: move into method self::_getSubrows()
        foreach ($model->expenditureItems as $item) {
            if ($item->type == FinancePlan::ROW_TYPE_EXPENSE_FIXED) {
                $key = 'expense_' . $item->id;
                $modelData[self::FIXED_COST]['levels'][$key] = [
                    'title' => ($item->expenditureItem) ? $item->expenditureItem->name : $key,
                    'data' => calc::getFixedCostLevel($model, $item->id),
                    'isEditable' => true,
                    'editAttrJS' => self::TRIGGER_AUTOFILL_CLASS,
                    'rowID' => $item->id
                ];
            }
        }

        // Другие доходы ///////////////////////////////////////////////////////////////////////////////////

        $modelData[self::OTHER_INCOME] = [
            'title' => 'Другие доходы, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'isButtonable' => true,
            'isExpandable' => true,
            'editAttrJS' => self::TRIGGER_OTHER_INCOME_CLASS,
            'data' => calc::getOtherIncome($model),
            'visibilityAttrJS' => 'finance_model_other_income',
            'levels' => []
        ];

        // todo: move into method self::_getSubrows()
        foreach ($model->incomeItems as $item) {
            if ($item->type == FinancePlan::ROW_TYPE_INCOME_OTHER) {
                $key = 'income_' . $item->id;
                $modelData[self::OTHER_INCOME]['levels'][$key] = [
                    'title' => ($item->incomeItem) ? $item->incomeItem->name : $key,
                    'data' => calc::getOtherIncome($model, $item->id),
                    'isEditable' => true,
                    'editAttrJS' => self::TRIGGER_AUTOFILL_CLASS,
                    'visibilityAttrJS' => 'finance_model_other_income',
                    'rowID' => $item->id,
                ];
            }
        }

        // Другие расходы ///////////////////////////////////////////////////////////////////////////////////

        $modelData[self::OTHER_EXPENSE] = [
            'title' => 'Другие расходы, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'isButtonable' => true,
            'isExpandable' => true,
            'editAttrJS' => self::TRIGGER_OTHER_EXPENSE_CLASS,
            'data' => calc::getOtherExpense($model),
            'visibilityAttrJS' => 'finance_model_other_expense',
            'levels' => []
        ];

        // todo: move into method self::_getSubrows()
        foreach ($model->expenditureItems as $item) {
            if ($item->type == FinancePlan::ROW_TYPE_EXPENSE_OTHER) {
                $key = 'expense_' . $item->id;
                $modelData[self::OTHER_EXPENSE]['levels'][$key] = [
                    'title' => ($item->expenditureItem) ? $item->expenditureItem->name : $key,
                    'data' => calc::getOtherExpenseLevel($model, $item->id),
                    'isEditable' => true,
                    'editAttrJS' => self::TRIGGER_AUTOFILL_CLASS,
                    'visibilityAttrJS' => 'finance_model_other_expense',
                    'rowID' => $item->id
                ];
            }
        }

        // Ebidta //////////////////////////////////////////////////////////////////////////////////////////////

        $modelData[self::EBIDTA] = [
            'title' => 'ПРИБЫЛЬ (EBIDTA), ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'data' => calc::getEbidta($model),
            'levels' => []
        ];

        // Амортизация //////////////////////////////////////////////////////////////////////////////////////////////

        $modelData[self::AMORTIZATION] = [
            'title' => 'Амортизация, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'data' => calc::getAmortization($model),
            'levels' => [],
            'visibilityAttrJS' => 'finance_model_amortization'
        ];

        // EBIT //////////////////////////////////////////////////////////////////////////////////////////////

        $modelData[self::EBIT] = [
            'title' => 'EBIT, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'data' => calc::getEbit($model),
            'levels' => [],
            'visibilityAttrJS' => 'finance_model_ebit'
        ];

        // Проценты полученные ///////////////////////////////////////////////////////////////////////////////

        $modelData[self::RECEIVED_PERCENT] = [
            'title' => 'Проценты полученные, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'data' => oddsCalc::getReceivedPercent($model),
            'levels' => [],
            'visibilityAttrJS' => 'finance_model_percent_received'
        ];

        // Проценты уплаченные ////////////////////////////////////////////////////////////////////////////////

        $modelData[self::PAID_PERCENT] = [
            'title' => 'Проценты уплаченные, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'data' => oddsCalc::getPaymentPercent($model),
            'levels' => [],
            'visibilityAttrJS' => 'finance_model_percent_paid'
        ];

        // Прибыль до налогообложения ////////////////////////////////////////////////////////////////////////

        $modelData[self::PROFIT_BEFORE_TAX] = [
            'title' => 'Прибыль до налогообложения, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'data' => calc::getProfitBeforeTax($model),
            'levels' => [],
            'visibilityRelatedFrom' => [
                'finance_model_amortization',
                'finance_model_ebit',
                'finance_model_percent_received',
                'finance_model_percent_paid',
            ]
        ];

        // Налог //////////////////////////////////////////////////////////////////////////////////////////////
        foreach ($model->expenditureItems as $item) {
            if ($item->type == FinancePlan::ROW_TYPE_EXPENSE_TAX) {
                $modelData[self::TAX] = [
                    'title' => 'Налог, ' . $model->getCurrencySymbol(),
                    'totalBlock' => true,
                    'data' => calc::getTax($model),
                    'isEditable' => $item->isEditable(),
                    'editAttrJS' => self::TRIGGER_TAX_CLASS,
                    'rowID' => $item->id
                ];
            }
        }

        // Чистая прибыль //////////////////////////////////////////////////////////////////////////////////////

        $modelData[self::NET_PROFIT] = [
            'title' => 'Чистая прибыль, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'data' => calc::getNetProfit($model),
            'levels' => []
        ];

        // Рентабельнось по чистой прибыли //////////////////////////////////////////////////////////////////////

        $modelData[self::NET_PROFIT_MARGIN] = [
            'title' => 'Рентабельнось по чистой прибыли',
            'totalBlock' => true,
            'isBold' => false,
            'data' => calc::getNetProfitMargin($model),
            'calculatedTotalSum' => calc::getNetProfitMarginTotal($model),
            'units' => '%',
            'levels' => []
        ];

        return $modelData;
    }


    /********
     * ODDS *
     ********/

    /**
     * @param FinancePlan $model
     * @return array
     */
    public static function getOdds(FinancePlan $model)
    {
        $modelData = [];

        $modelData[self::ODDS] = [
            'title' => 'ОТЧЕТ о ДВИЖЕНИИ ДЕНЕЖНЫХ СРЕДСТВ',
            'totalBlock' => true,
            'isButtonable' => false,
            'data' => null,
            'levels' => []
        ];

        // Операционная деятельность //////////////////////////////////////////////////////////////////////////////////////////

        $modelData[self::ODDS_OPERATIONS] = [
            'title' => 'Операционная деятельность, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'isButtonable' => false,
            'data' => oddsCalc::getOperatingTotal($model),
            'levels' => [
                self::ODDS_OPERATIONS_INCOME => [
                    'title' => 'Приход',
                    'data' => oddsCalc::getOperatingIncomes($model),
                    'isButtonable' => true,
                    'isExpandable' => true,
                    'editAttrJS' => self::TRIGGER_ODDS_INCOME_CLASS,
                    'rowType' => FinancePlan::ODDS_ROW_TYPE_OPERATIONS,
                    'isBold' => true,
                    'levels' => []
                ],
                self::ODDS_OPERATIONS_EXPENSE => [
                    'title' => 'Расход',
                    'data' => oddsCalc::getOperatingExpenses($model),
                    'isButtonable' => true,
                    'isExpandable' => true,
                    'editAttrJS' => self::TRIGGER_ODDS_EXPENSE_CLASS,
                    'rowType' => FinancePlan::ODDS_ROW_TYPE_OPERATIONS,
                    'isBold' => true,
                    'levels' => []
                ],
            ]
        ];

        $modelDataLevels = &$modelData[self::ODDS_OPERATIONS]['levels'];

        foreach ($model->oddsIncomeItems as $item) {
            $key = 'oddsIncome_' . $item->id;

            if ($item->type !== FinancePlan::ODDS_ROW_TYPE_OPERATIONS)
                continue;

            if ($item->income_item_id === IncomeItem::ITEM_PAYMENT_FROM_BUYER) {
                // system rows
                $modelDataLevels[self::ODDS_OPERATIONS_INCOME]['levels'][$key] = [
                    'title' => $item->getTitle(),
                    'data' => oddsCalc::getOperatingIncomesLevel($model, $item->id),
                    'isEditable' => false,
                    'editAttrJS' => self::TRIGGER_ODDS_SYSTEM_INCOME_CLASS,
                    'rowID' => $item->id,
                    'rowPaymentType' => $item->payment_type
                ];
            } else {
                // customer rows
                $modelDataLevels[self::ODDS_OPERATIONS_INCOME]['levels'][$key] = [
                    'title' => $item->getTitle(),
                    'data' => oddsCalc::getOperatingIncomesLevel($model, $item->id),
                    'isEditable' => true,
                    'editAttrJS' => self::TRIGGER_AUTOFILL_CLASS,
                    'rowID' => $item->id
                ];
            }
        }

        foreach ($model->oddsExpenditureItems as $item) {
            $key = 'oddsExpense_' . $item->id;

            if ($item->type !== FinancePlan::ODDS_ROW_TYPE_OPERATIONS)
                continue;

            if ($item->expenditure_item_id === ExpenditureItem::ITEM_CONTRACTOR_PAYMENT) {
                // system rows
                $modelDataLevels[self::ODDS_OPERATIONS_EXPENSE]['levels'][$key] = [
                    'title' => $item->getTitle(),
                    'data' => oddsCalc::getOperatingExpensesLevel($model, $item->id),
                    'isEditable' => false,
                    'editAttrJS' => self::TRIGGER_ODDS_SYSTEM_EXPENSE_CLASS,
                    'rowID' => $item->id,
                    'rowPaymentType' => $item->payment_type
                ];
            } else {
                // customer rows
                $modelDataLevels[self::ODDS_OPERATIONS_EXPENSE]['levels'][$key] = [
                    'title' => $item->getTitle(),
                    'data' => oddsCalc::getOperatingExpensesLevel($model, $item->id),
                    'isEditable' => true,
                    'editAttrJS' => self::TRIGGER_AUTOFILL_CLASS,
                    'rowID' => $item->id
                ];
            }
        }

        // постоянные расходы

        $modelDataLevels[self::ODDS_OPERATIONS_EXPENSE]['levels'][self::ODDS_OPERATIONS_FIXED_COSTS] = [
            'title' => 'Постоянные расходы',
            'data' => calc::getFixedCosts($model)
        ];

        foreach (calc::getFixedCosts($model) as $ym => $amount) {
            $modelData[self::ODDS_OPERATIONS]['levels'][self::ODDS_OPERATIONS_EXPENSE]['data'][$ym] += $amount;
        }

        // налог

        $modelDataLevels[self::ODDS_OPERATIONS_EXPENSE]['levels'][self::ODDS_OPERATIONS_TAX] = [
            'title' => 'Налог',
            'data' => oddsCalc::getOddsTax($model)
        ];

        foreach (oddsCalc::getOddsTax($model) as $ym => $amount) {
            $modelData[self::ODDS_OPERATIONS]['levels'][self::ODDS_OPERATIONS_EXPENSE]['data'][$ym] += $amount;
        }

        // Финансовая деятельность ///////////////////////////////////////////////////////////////////////////////////

        $modelData[self::ODDS_FINANCES] = [
            'title' => 'Финансовая деятельность, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'isButtonable' => false,
            'data' => oddsCalc::getFinancesTotal($model),
            'levels' => [
                self::ODDS_FINANCES_INCOME => [
                    'title' => 'Приход',
                    'data' => oddsCalc::getFinancesIncomes($model),
                    'isButtonable' => true,
                    'isExpandable' => true,
                    'editAttrJS' => self::TRIGGER_ODDS_INCOME_CLASS,
                    'rowType' => FinancePlan::ODDS_ROW_TYPE_FINANCES,
                    'isBold' => true,
                    'levels' => []
                ],
                self::ODDS_FINANCES_EXPENSE => [
                    'title' => 'Расход',
                    'data' => oddsCalc::getFinancesExpenses($model),
                    'isButtonable' => true,
                    'isExpandable' => true,
                    'editAttrJS' => self::TRIGGER_ODDS_EXPENSE_CLASS,
                    'rowType' => FinancePlan::ODDS_ROW_TYPE_FINANCES,
                    'isBold' => true,
                    'levels' => []
                ],
            ]
        ];

        $modelDataLevels = &$modelData[self::ODDS_FINANCES]['levels'];

        // CREDIT
        if ($model->hasOddsCreditRows(IncomeItem::ITEM_CREDIT)) {
            // credits wrapper
            $modelDataLevels[self::ODDS_FINANCES_INCOME]['levels'][self::ODDS_ROW_CREDIT] = [
                'title' => 'Кредит',
                'data' => oddsCalc::getLevelByIncomeItem($model, IncomeItem::ITEM_CREDIT, 'credit_id'),
                'isEditable' => false,
                'isButtonable' => true,
                'levels' => []
            ];
            $modelDataLevels[self::ODDS_FINANCES_EXPENSE]['levels'][self::ODDS_ROW_CREDIT] = [
                'title' => 'Погашение кредита',
                'data' => oddsCalc::getLevelByExpenditureItem($model, ExpenditureItem::ITEM_REPAYMENT_CREDIT, 'credit_id'),
                'isEditable' => false,
                'isButtonable' => true,
                'levels' => []
            ];
            $modelDataLevels[self::ODDS_FINANCES_EXPENSE]['levels'][self::ODDS_ROW_CREDIT_PERCENT] = [
                'title' => 'Проценты уплаченные',
                'data' => oddsCalc::getLevelByExpenditureItem($model, ExpenditureItem::ITEM_PAYMENT_PERCENT, 'credit_id'),
                'isEditable' => false,
                'isButtonable' => true,
                'levels' => []
            ];
        }
        // LOAN
        if ($model->hasOddsCreditRows(IncomeItem::ITEM_LOAN)) {
            // loans wrapper
            $modelDataLevels[self::ODDS_FINANCES_INCOME]['levels'][self::ODDS_ROW_LOAN] = [
                'title' => 'Займ',
                'data' => oddsCalc::getLevelByIncomeItem($model, IncomeItem::ITEM_LOAN, 'credit_id'),
                'isEditable' => false,
                'isButtonable' => true,
                'levels' => []
            ];
            $modelDataLevels[self::ODDS_FINANCES_EXPENSE]['levels'][self::ODDS_ROW_LOAN] = [
                'title' => 'Погашение займа',
                'data' => oddsCalc::getLevelByExpenditureItem($model, ExpenditureItem::ITEM_LOAN_REPAYMENT, 'credit_id'),
                'isEditable' => false,
                'isButtonable' => true,
                'levels' => []
            ];
            $modelDataLevels[self::ODDS_FINANCES_EXPENSE]['levels'][self::ODDS_ROW_LOAN_PERCENT] = [
                'title' => 'Проценты уплаченные',
                'data' => oddsCalc::getLevelByExpenditureItem($model, ExpenditureItem::ITEM_PAYMENT_PERCENT, 'credit_id'),
                'isEditable' => false,
                'isButtonable' => true,
                'levels' => []
            ];
        }

        foreach ($model->oddsIncomeItems as $item) {
            $key = 'oddsIncome_' . $item->id;
            if ($item->type == FinancePlan::ODDS_ROW_TYPE_FINANCES) {
                if ($item->credit_id) {
                    // system rows
                    $wrapperKey = ($item->income_item_id == IncomeItem::ITEM_CREDIT) ? self::ODDS_ROW_CREDIT : self::ODDS_ROW_LOAN;
                    $modelDataLevels[self::ODDS_FINANCES_INCOME]['levels'][$wrapperKey]['levels'][$key] = [
                        'title' => $item->getTitle(),
                        'data' => oddsCalc::getFinancesIncomesLevel($model, $item->id),
                        'isEditable' => false,
                        'updateAttrJS' => self::TRIGGER_ODDS_UPDATE_CREDIT,
                        'deleteAttrJS' => self::TRIGGER_ODDS_DELETE_CREDIT,
                        'rowID' => $item->id,
                        'creditID' => $item->credit_id
                    ];
                } else {
                    // customer rows
                    $modelDataLevels[self::ODDS_FINANCES_INCOME]['levels'][$key] = [
                        'title' => $item->getTitle(),
                        'data' => oddsCalc::getFinancesIncomesLevel($model, $item->id),
                        'isEditable' => true,
                        'editAttrJS' => self::TRIGGER_AUTOFILL_CLASS,
                        'rowID' => $item->id
                    ];
                }
            }
        }

        foreach ($model->oddsExpenditureItems as $item) {
            $key = 'oddsExpense_' . $item->id;
            if ($item->type == FinancePlan::ODDS_ROW_TYPE_FINANCES) {
                if ($item->credit_id) {
                    // system rows
                    $wrapperKey = ($item->expenditure_item_id == ExpenditureItem::ITEM_PAYMENT_PERCENT)
                        ? self::ODDS_ROW_CREDIT_PERCENT
                        : ($item->expenditure_item_id == ExpenditureItem::ITEM_REPAYMENT_CREDIT ? self::ODDS_ROW_CREDIT : self::ODDS_ROW_LOAN);

                    $modelDataLevels[self::ODDS_FINANCES_EXPENSE]['levels'][$wrapperKey]['levels'][$key] = [
                        'title' => $item->getTitle(),
                        'data' => oddsCalc::getFinancesExpensesLevel($model, $item->id),
                        'isEditable' => false,
                        'rowID' => $item->id,
                        'creditID' => $item->credit_id
                    ];
                } else {
                    $modelDataLevels[self::ODDS_FINANCES_EXPENSE]['levels'][$key] = [
                        'title' => $item->getTitle(),
                        'data' => oddsCalc::getFinancesExpensesLevel($model, $item->id),
                        'isEditable' => true,
                        'editAttrJS' => self::TRIGGER_AUTOFILL_CLASS,
                        'rowID' => $item->id
                    ];
                }
            }
        }
        
        // Инвестиционная деятельность ///////////////////////////////////////////////////////////////////////////////

        $modelData[self::ODDS_INVESTMENTS] = [
            'title' => 'Инвестиционная деятельность, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'isButtonable' => false,
            'data' => oddsCalc::getInvestmentsTotal($model),
            'levels' => [
                self::ODDS_INVESTMENTS_INCOME => [
                    'title' => 'Приход',
                    'data' => oddsCalc::getInvestmentsIncomes($model),
                    'isButtonable' => true,
                    'isExpandable' => true,
                    'editAttrJS' => self::TRIGGER_ODDS_INCOME_CLASS,
                    'rowType' => FinancePlan::ODDS_ROW_TYPE_INVESTMENTS,
                    'isBold' => true,
                    'levels' => []
                ],
                self::ODDS_INVESTMENTS_EXPENSE => [
                    'title' => 'Расход',
                    'data' => oddsCalc::getInvestmentsExpenses($model),
                    'isButtonable' => true,
                    'isExpandable' => true,
                    'editAttrJS' => self::TRIGGER_ODDS_EXPENSE_CLASS,
                    'rowType' => FinancePlan::ODDS_ROW_TYPE_INVESTMENTS,
                    'isBold' => true,
                    'levels' => []
                ],
            ]
        ];

        $modelDataLevels = &$modelData[self::ODDS_INVESTMENTS]['levels'];

        // BALANCE ARTICLE
        if ($model->hasOddsBalanceArticleRows(ExpenditureItem::ITEM_BALANCE_ARTICLE_FIXED)) {
            $modelDataLevels[self::ODDS_INVESTMENTS_EXPENSE]['levels'][self::ODDS_ROW_BALANCE_ARTICLE_FIXED] = [
                'title' => 'Приобретение Основных средств',
                'data' => oddsCalc::getLevelByExpenditureItem($model, ExpenditureItem::ITEM_BALANCE_ARTICLE_FIXED, 'balance_article_id'),
                'isButtonable' => true,
                'isEditable' => false,
                'levels' => []
            ];
        }
        if ($model->hasOddsBalanceArticleRows(ExpenditureItem::ITEM_BALANCE_ARTICLE_INTANGIBLE)) {
            $modelDataLevels[self::ODDS_INVESTMENTS_EXPENSE]['levels'][self::ODDS_ROW_BALANCE_ARTICLE_INTANGIBLE] = [
                'title' => 'Приобретение НМА',
                'data' => oddsCalc::getLevelByExpenditureItem($model, ExpenditureItem::ITEM_BALANCE_ARTICLE_INTANGIBLE, 'balance_article_id'),
                'isButtonable' => true,
                'isEditable' => false,
                'levels' => []
            ];
        }

        foreach ($model->oddsIncomeItems as $item) {
            $key = 'oddsIncome_' . $item->id;
            if ($item->type == FinancePlan::ODDS_ROW_TYPE_INVESTMENTS) {
                $modelDataLevels[self::ODDS_INVESTMENTS_INCOME]['levels'][$key] = [
                    'title' => $item->getTitle(),
                    'data' => oddsCalc::getInvestmentsIncomesLevel($model, $item->id),
                    'isEditable' => true,
                    'editAttrJS' => self::TRIGGER_AUTOFILL_CLASS,
                    'rowID' => $item->id
                ];
            }
        }

        foreach ($model->oddsExpenditureItems as $item) {
            $key = 'oddsExpense_' . $item->id;
            if ($item->type == FinancePlan::ODDS_ROW_TYPE_INVESTMENTS) {
                if ($item->balance_article_id) {
                    // system rows
                    $wrapperKey = ($item->expenditure_item_id == ExpenditureItem::ITEM_BALANCE_ARTICLE_INTANGIBLE)
                        ? self::ODDS_ROW_BALANCE_ARTICLE_INTANGIBLE
                        : self::ODDS_ROW_BALANCE_ARTICLE_FIXED;

                    $modelDataLevels[self::ODDS_INVESTMENTS_EXPENSE]['levels'][$wrapperKey]['levels'][$key] = [
                        'title' => $item->getTitle(),
                        'data' => oddsCalc::getInvestmentsExpensesLevel($model, $item->id),
                        'isEditable' => false,
                        'updateAttrJS' => self::TRIGGER_ODDS_UPDATE_BALANCE_ARTICLE,
                        'deleteAttrJS' => self::TRIGGER_ODDS_DELETE_BALANCE_ARTICLE,
                        'rowID' => $item->id,
                        'balanceArticleID' => $item->balance_article_id
                    ];
                } else {
                    // customer rows
                    $modelDataLevels[self::ODDS_INVESTMENTS_EXPENSE]['levels'][$key] = [
                        'title' => $item->getTitle(),
                        'data' => oddsCalc::getInvestmentsExpensesLevel($model, $item->id),
                        'isEditable' => true,
                        'editAttrJS' => self::TRIGGER_AUTOFILL_CLASS,
                        'rowID' => $item->id
                    ];
                }
            }
        }

        // Чистый денежный поток ////////////////////////////////////////////////////////////////////////

        $modelData[self::ODDS_NET_PROFIT] = [
            'title' => 'Чистый денежный поток, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'data' => oddsCalc::getOddsNetProfit($model),
            'levels' => []
        ];

        // Остаток на конец месяца ////////////////////////////////////////////////////////////////////////

        $modelData[self::ODDS_BALANCE] = [
            'title' => 'Остаток на конец месяца, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'isBalanceRow' => true,
            'data' => oddsCalc::getOddsBalance($model),
            'levels' => []
        ];

        return $modelData;
    }

    /**
     * @return string
     */
    public static function getItemsByFlowOfFundsTypeJS()
    {
        $ret = [
            FinancePlan::ODDS_ROW_TYPE_FINANCES => [
                self::ODDS_FINANCES_INCOME => [],
                self::ODDS_FINANCES_EXPENSE => [],
            ],
            FinancePlan::ODDS_ROW_TYPE_OPERATIONS => [
                self::ODDS_OPERATIONS_INCOME => [],
                self::ODDS_OPERATIONS_EXPENSE => [],
            ],
            FinancePlan::ODDS_ROW_TYPE_INVESTMENTS => [
                self::ODDS_INVESTMENTS_INCOME => [],
                self::ODDS_INVESTMENTS_EXPENSE => [],
            ]
        ];

        $_tr = [
            AbstractFinance::FINANCIAL_OPERATIONS_BLOCK => FinancePlan::ODDS_ROW_TYPE_FINANCES,
            AbstractFinance::OPERATING_ACTIVITIES_BLOCK => FinancePlan::ODDS_ROW_TYPE_OPERATIONS,
            AbstractFinance::INVESTMENT_ACTIVITIES_BLOCK => FinancePlan::ODDS_ROW_TYPE_INVESTMENTS
        ];

        try {
            ////////////////////////////////////////////
            $articlesSearchModel = new ArticlesSearch();
            $articlesSearchModel->withContractors = false;
            $articlesItems = $articlesSearchModel->search();
            unset($articlesItems['block-' . $articlesSearchModel::BLOCK_FREE_CONTRACTORS]);
            ////////////////////////////////////////////

            foreach ($articlesItems as $blockKey => $blockData) {
                @list($bName, $bId) = explode('-', $blockKey);
                if (isset($_tr[$bId])) {
                    $blockType = $_tr[$bId];
                    foreach ($blockData['items'] as $flowKey => $flowData) {
                        // item
                        foreach ($flowData['items'] as $item) {
                            $flowType = ($item['flowType'] == ArticlesSearch::TYPE_INCOME)
                                ? 'oddsIncome'
                                : 'oddsExpense';

                            if (isset($ret[$blockType][$flowType])) {
                                $ret[$blockType][$flowType][] = $item['id'];
                            }
                            // subitem
                            if (isset($item['children'])) {
                                foreach ($item['children'] as $subitem) {
                                    $flowType = ($subitem['flowType'] == ArticlesSearch::TYPE_INCOME)
                                        ? 'oddsIncome'
                                        : 'oddsExpense';

                                    if (isset($ret[$blockType][$flowType])) {
                                        $ret[$blockType][$flowType][] = $subitem['id'];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (\Exception $e) {

            return '[]';
        }

        return json_encode($ret) ?: '[]';
    }
}