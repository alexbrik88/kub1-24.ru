<?php

namespace frontend\modules\analytics\models\financePlan\helpers;

use common\components\helpers\Html;
use frontend\modules\analytics\models\financePlan\FinancePlan;
use frontend\modules\analytics\models\financePlan\FinancePlanGroup;
use frontend\modules\analytics\models\financePlan\helpers\FinancePlanCalculatorHelper as calc;
use frontend\modules\analytics\models\financePlan\helpers\FinancePlanOddsCalculatorHelper as oddsCalc;
use frontend\modules\analytics\models\financePlan\helpers\FinancePlanGroupCalculatorHelper as groupCalc;
use common\models\document\InvoiceExpenditureItem as ExpenditureItem;
use common\models\document\InvoiceIncomeItem as IncomeItem;

/**
 * Class FinancePlanGroupTableHelper
 */
class FinancePlanGroupTableHelper extends FinancePlanTableHelper
{
    /**
     * @param FinancePlan $model
     * @return array
     */
    public static function getSalesFunnel(FinancePlan $model)
    {
        $modelGroup = $model->modelGroup;
        $modelData = [];

        $modelData[self::SALES_FUNNEL] = [
            'title' => 'Воронка продаж',
            'totalBlock' => true,
            'isButtonable' => true,
            'data' => null,
            'levels' => [
                'income_' . FinancePlan::ROW_TYPE_INCOME_NEW_SALES => [
                    'title' => 'Новые продажи, шт',
                    'data' => $model->emptyRow,
                    //'isEditable' => true,
                    //'editAttrJS' => self::TRIGGER_AUTOFILL_CLASS,
                    'isInt' => true,
                    //'rowID' => null
                ],
                'income_' . FinancePlan::ROW_TYPE_INCOME_SALES => [
                    'title' => 'Повторные продажи, шт',
                    'data' => $model->emptyRow,
                    //'isEditable' => true,
                    //'editAttrJS' => self::TRIGGER_AUTOFILL_CLASS,
                    'isInt' => true,
                    //'rowID' => null
                ],
                self::SALES_FUNNEL_TOTAL => [
                    'title' => 'Итого продажи, шт',
                    'data' => $model->emptyRow,
                    'isInt' => true
                ]
            ]
        ];

        foreach ($modelGroup->financePlans as $plan)
            foreach ($plan->incomeItems as $item) {
                $key = 'income_' . $item->type;
                if ($item->type == FinancePlan::ROW_TYPE_INCOME_SALES || $item->type == FinancePlan::ROW_TYPE_INCOME_NEW_SALES) {
                    self::_sumRow($modelData[self::SALES_FUNNEL]['levels'][$key]['data'], $item->getRowData());
                    self::_sumRow($modelData[self::SALES_FUNNEL]['levels'][self::SALES_FUNNEL_TOTAL]['data'], $item->getRowData());
                }
            }

        return $modelData;
    }

    /**
     * @param FinancePlan $model
     * @return array
     */
    public static function getProfitAndLoss(FinancePlan $model)
    {
        $modelGroup = $model->modelGroup;
        $modelData = [];

        // Выручка //////////////////////////////////////////////////////////////////////////////////////////

        $modelData[self::PROFIT_AND_LOSS] = [
            'title' => 'ОТЧЕТ о ПРИБЫЛЯХ и УБЫТКАХ',
            'totalBlock' => true,
            'isButtonable' => false,
            'data' => null,
            'levels' => []
        ];

        $modelData[self::REVENUE] = [
            'title' => 'Выручка, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'isButtonable' => true,
            'data' => $model->emptyRow,
            'levels' => []
        ];

        foreach ($modelGroup->financePlans as $plan) {

            $modelData[self::REVENUE]['levels'][$plan->id] = [
                'title' => Html::encode($plan->name),
                'totalBlock' => true,
                'isButtonable' => true,
                'data' => calc::getRevenue($plan),
                'levels' => [
                    self::SALES_TOTAL => [
                        'title' => 'Кол-во продаж, шт',
                        'data' => calc::getSalesTotal($plan),
                        'isInt' => true
                    ],
                    'income_' . FinancePlan::ROW_TYPE_INCOME_AVG_CHECK => [
                        'title' => 'Средний чек, ' . $plan->getCurrencySymbol(),
                        'data' => calc::getAvgCheck($plan),
                        'isAvgRow' => true
                        //'isEditable' => true,
                        //'editAttrJS' => self::TRIGGER_AUTOFILL_CLASS,
                        //'rowID' => null
                    ],
                ]
            ];

            self::_sumPlanRow($modelData, self::REVENUE, $plan->id);
        }

        // Себестоимость //////////////////////////////////////////////////////////////////////////////////////////

        $modelData[self::PRIME_COST] = [
            'title' => 'Себестоимость, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'isButtonable' => true,
            'isExpandable' => true,
            'editAttrJS' => self::TRIGGER_PRIME_COSTS_CLASS,
            'data' => $model->emptyRow,
            'levels' => []
        ];

        foreach ($modelGroup->financePlans as $plan) {

            $modelData[self::PRIME_COST]['levels'][$plan->id] = [
                'title' => Html::encode($plan->name),
                'totalBlock' => true,
                'isButtonable' => true,
                'data' => calc::getPrimeCosts($plan),
                'levels' => []
            ];

            foreach ($plan->primeCostItems as $item) {
                $key = 'prime_cost_' . $item->id;
                $modelData[self::PRIME_COST]['levels'][$plan->id]['levels'][$key] = [
                    'title' => ($item->primeCostItem) ? $item->primeCostItem->name : $key,
                    'data' => calc::getPrimeCostLevel($plan, $item->id),
                    //'isEditable' => $item->isEditable(),
                    //'editAttrJS' => self::TRIGGER_PRIME_COSTS_CLASS,
                    //'rowID' => $item->id
                ];
            }

            self::_sumPlanRow($modelData, self::PRIME_COST, $plan->id);
        }

        // Маржинальный доход ///////////////////////////////////////////////////////////////////////////////////

        $modelData[self::MARGIN_INCOME] = [
            'title' => 'МАРЖИНАЛЬНЫЙ ДОХОД, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'data' => $model->emptyRow,
            'levels' => []
        ];

        foreach ($modelGroup->financePlans as $plan) {
            self::_sumRow($modelData[self::MARGIN_INCOME]['data'], calc::getMarginIncome($plan));
        }

        // Переменные расходы ///////////////////////////////////////////////////////////////////////////////////

        $modelData[self::VARIABLE_COST] = [
            'title' => 'Переменные расходы, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'isButtonable' => true,
            'data' => $model->emptyRow,
            //'isExpandable' => true,
            //'editAttrJS' => self::TRIGGER_VARIABLE_COSTS_CLASS,
            'levels' => []
        ];

        foreach ($modelGroup->financePlans as $plan) {

            $modelData[self::VARIABLE_COST]['levels'][$plan->id] = [
                'title' => Html::encode($plan->name),
                'totalBlock' => true,
                'isButtonable' => true,
                'data' => calc::getVariableCosts($plan),
                'levels' => []
            ];

            foreach ($plan->expenditureItems as $item) {
                if ($item->type == FinancePlan::ROW_TYPE_EXPENSE_VARIABLE) {
                    $key = 'expense_' . $item->id;
                    $modelData[self::VARIABLE_COST]['levels'][$plan->id]['levels'][$key] = [
                        'title' => ($item->expenditureItem) ? $item->expenditureItem->name : $key,
                        'data' => calc::getVariableCostLevel($plan, $item->id),
                        //'isEditable' => $item->isEditable(),
                        //'editAttrJS' => self::TRIGGER_VARIABLE_COSTS_CLASS,
                        //'rowID' => $item->id
                    ];
                }
            }

            self::_sumPlanRow($modelData, self::VARIABLE_COST, $plan->id);
        }


        // Валовая прибыль ///////////////////////////////////////////////////////////////////////////////////

        $modelData[self::GROSS_PROFIT] = [
            'title' => 'ВАЛОВАЯ ПРИБЫЛЬ, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'data' => $model->emptyRow,
            'levels' => []
        ];

        foreach ($modelGroup->financePlans as $plan) {
            self::_sumRow($modelData[self::GROSS_PROFIT]['data'], calc::getGrossProfit($plan));
        }

        // Постоянные расходы ///////////////////////////////////////////////////////////////////////////////////

        $modelData[self::FIXED_COST] = [
            'title' => 'Постоянные расходы, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'isButtonable' => true,
            'data' => $model->emptyRow,
            //'isExpandable' => true,
            //'editAttrJS' => self::TRIGGER_FIXED_COSTS_CLASS,
            'levels' => []
        ];

        foreach ($modelGroup->financePlans as $plan) {

            $modelData[self::FIXED_COST]['levels'][$plan->id] = [
                'title' => Html::encode($plan->name),
                'totalBlock' => true,
                'isButtonable' => true,
                'data' => calc::getFixedCosts($plan),
                'levels' => []
            ];

            foreach ($plan->expenditureItems as $item) {
                if ($item->type == FinancePlan::ROW_TYPE_EXPENSE_FIXED) {
                    $key = 'expense_' . $item->id;
                    $modelData[self::FIXED_COST]['levels'][$plan->id]['levels'][$key] = [
                        'title' => ($item->expenditureItem) ? $item->expenditureItem->name : $key,
                        'data' => calc::getFixedCostLevel($plan, $item->id),
                        //'isEditable' => $item->isEditable(),
                        //'editAttrJS' => self::TRIGGER_AUTOFILL_CLASS,
                        //'rowID' => $item->id
                    ];
                }
            }

            self::_sumPlanRow($modelData, self::FIXED_COST, $plan->id);
        }
        
        if (count($modelGroup->expenditureItems))
        {
            $modelData[self::GROUP_EXPENSE] = [
                'title' => 'Общие расходы, ' . $model->getCurrencySymbol(),
                'totalBlock' => true,
                'isButtonable' => true,
                //'isExpandable' => true,
                //'editAttrJS' => self::TRIGGER_GROUP_EXPENSE_CLASS,
                'data' => groupCalc::getExpenses($modelGroup),
                'levels' => []
            ];

            foreach ($modelGroup->expenditureItems as $item) {
                $key = 'group_expense_' . $item->id;
                $modelData[self::GROUP_EXPENSE]['levels'][$key] = [
                    'title' => ($item->expenditureItem) ? $item->expenditureItem->name : $key,
                    'data' =>  groupCalc::getExpensesLevel($modelGroup, $item->id),
                    //'isEditable' => true,
                    //'editAttrJS' => self::TRIGGER_AUTOFILL_CLASS,
                    //'rowID' => $item->id
                ];
            }
        }

        // Другие доходы ///////////////////////////////////////////////////////////////////////////////////

        $modelData[self::OTHER_INCOME] = [
            'title' => 'Другие доходы, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'isButtonable' => true,
            'data' => $model->emptyRow,
            'visibilityAttrJS' => 'finance_model_other_income',
            'levels' => []
        ];

        foreach ($modelGroup->financePlans as $plan) {

            $modelData[self::OTHER_INCOME]['levels'][$plan->id] = [
                'title' => Html::encode($plan->name),
                'totalBlock' => true,
                'isButtonable' => true,
                'data' => calc::getOtherIncome($plan),
                'visibilityAttrJS' => 'finance_model_other_income',
                'levels' => []
            ];

            foreach ($plan->incomeItems as $item) {
                if ($item->type == FinancePlan::ROW_TYPE_INCOME_OTHER) {
                    $key = 'income_' . $item->id;
                    $modelData[self::OTHER_INCOME]['levels'][$plan->id]['levels'][$key] = [
                        'title' => ($item->incomeItem) ? $item->incomeItem->name : $key,
                        'data' => calc::getOtherIncomeLevel($plan, $item->id),
                        'visibilityAttrJS' => 'finance_model_other_income',
                    ];
                }
            }

            self::_sumPlanRow($modelData, self::OTHER_INCOME, $plan->id);
        }        
        
       // Другие расходы ///////////////////////////////////////////////////////////////////////////////////

        $modelData[self::OTHER_EXPENSE] = [
            'title' => 'Другие расходы, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'isButtonable' => true,
            'data' => $model->emptyRow,
            'visibilityAttrJS' => 'finance_model_other_expense',
            'levels' => []
        ];

        foreach ($modelGroup->financePlans as $plan) {

            $modelData[self::OTHER_EXPENSE]['levels'][$plan->id] = [
                'title' => Html::encode($plan->name),
                'totalBlock' => true,
                'isButtonable' => true,
                'data' => calc::getOtherExpense($plan),
                'visibilityAttrJS' => 'finance_model_other_expense',
                'levels' => []
            ];

            foreach ($plan->expenditureItems as $item) {
                if ($item->type == FinancePlan::ROW_TYPE_EXPENSE_OTHER) {
                    $key = 'expense_' . $item->id;
                    $modelData[self::OTHER_EXPENSE]['levels'][$plan->id]['levels'][$key] = [
                        'title' => ($item->expenditureItem) ? $item->expenditureItem->name : $key,
                        'data' => calc::getOtherExpenseLevel($plan, $item->id),
                        'visibilityAttrJS' => 'finance_model_other_expense',
                    ];
                }
            }

            self::_sumPlanRow($modelData, self::OTHER_EXPENSE, $plan->id);
        }

        // Ebidta //////////////////////////////////////////////////////////////////////////////////////////////

        $modelData[self::EBIDTA] = [
            'title' => 'ПРИБЫЛЬ (EBIDTA), ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'data' => $model->emptyRow,
            'levels' => []
        ];

        foreach ($modelGroup->financePlans as $plan) {
            self::_sumRow($modelData[self::EBIDTA]['data'], calc::getEbidta($plan));
        }
        self::_subRow($modelData[self::EBIDTA]['data'], groupCalc::getExpenses($modelGroup)); // SUB GROUP EXPENSES

        // Амортизация //////////////////////////////////////////////////////////////////////////////////////////////

        $modelData[self::AMORTIZATION] = [
            'title' => 'Амортизация, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'data' => $model->emptyRow,
            'levels' => [],
            'visibilityAttrJS' => 'finance_model_amortization'
        ];

        foreach ($modelGroup->financePlans as $plan) {
            self::_sumRow($modelData[self::AMORTIZATION]['data'], calc::getAmortization($plan));
        }

        // EBIT //////////////////////////////////////////////////////////////////////////////////////////////

        $modelData[self::EBIT] = [
            'title' => 'EBIT, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'data' => $model->emptyRow,
            'levels' => [],
            'visibilityAttrJS' => 'finance_model_ebit'
        ];

        foreach ($modelGroup->financePlans as $plan) {
            self::_sumRow($modelData[self::EBIT]['data'], calc::getEbit($plan));
        }

        // Проценты полученные ///////////////////////////////////////////////////////////////////////////////

        $modelData[self::RECEIVED_PERCENT] = [
            'title' => 'Проценты полученные, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'data' => $model->emptyRow,
            'visibilityAttrJS' => 'finance_model_percent_received'
        ];

        foreach ($modelGroup->financePlans as $plan) {
            self::_sumRow($modelData[self::RECEIVED_PERCENT]['data'], calc::getReceivedPercent($plan));
        }

        // Проценты уплаченные ////////////////////////////////////////////////////////////////////////////////

        $modelData[self::PAID_PERCENT] = [
            'title' => 'Проценты уплаченные, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'data' => $model->emptyRow,
            'visibilityAttrJS' => 'finance_model_percent_paid'
        ];

        foreach ($modelGroup->financePlans as $plan) {
            self::_sumRow($modelData[self::PAID_PERCENT]['data'], calc::getPaymentPercent($plan));
        }

        // Налог //////////////////////////////////////////////////////////////////////////////////////////////

        $modelData[self::TAX] = [
            'title' => 'Налог, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'data' => $model->emptyRow,
        ];

        foreach ($modelGroup->financePlans as $plan) {
            self::_sumRow($modelData[self::TAX]['data'], calc::getTax($plan));
        }


        // Чистая прибыль //////////////////////////////////////////////////////////////////////////////////////

        $modelData[self::NET_PROFIT] = [
            'title' => 'Чистая прибыль, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'data' => $model->emptyRow,
            'levels' => []
        ];

        foreach ($modelGroup->financePlans as $plan) {
            self::_sumRow($modelData[self::NET_PROFIT]['data'], calc::getNetProfit($plan));
        }

        // Рентабельнось по чистой прибыли //////////////////////////////////////////////////////////////////////

        $modelData[self::NET_PROFIT_MARGIN] = [
            'title' => 'Рентабельнось по чистой прибыли',
            'totalBlock' => true,
            'isBold' => false,
            'data' => $model->emptyRow,
            'calculatedTotalSum' => 0,
            'units' => '%',
            'levels' => []
        ];

        $_revenueTotal = 0;
        $_netProfitTotal = 0;
        foreach ($modelGroup->financePlans as $plan) {
            self::_avgRow($modelData[self::NET_PROFIT_MARGIN]['data'], calc::getNetProfitMargin($plan), count($modelGroup->financePlans));
            $_revenue = (array)calc::getRevenue($plan);
            $_netProfit = (array)calc::getNetProfit($plan);
            $_revenueTotal += array_sum($_revenue);
            $_netProfitTotal += array_sum($_netProfit);
        }

        if ($_revenueTotal != 0)
            $modelData[self::NET_PROFIT_MARGIN]['calculatedTotalSum'] = $_netProfitTotal / $_revenueTotal * 100;

        return $modelData;
    }

    /**
     * @param FinancePlan $model
     * @return array
     */
    public static function getOdds(FinancePlan $model)
    {
        $modelGroup = $model->modelGroup;
        $modelData = [];

        $modelData[self::ODDS] = [
            'title' => 'ОТЧЕТ о ДВИЖЕНИИ ДЕНЕЖНЫХ СРЕДСТВ',
            'totalBlock' => true,
            'isButtonable' => false,
            'data' => null,
            'levels' => []
        ];

        // Операционная деятельность //////////////////////////////////////////////////////////////////////////////////////////

        $modelData[self::ODDS_OPERATIONS] = [
            'title' => 'Операционная деятельность, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'isButtonable' => false,
            'data' => $model->emptyRow,
            'levels' => [
                self::ODDS_OPERATIONS_INCOME => [
                    'title' => 'Приход',
                    'data' => $model->emptyRow,
                    'isButtonable' => true,
                    'isBold' => true,
                    'rowType' => FinancePlan::ODDS_ROW_TYPE_OPERATIONS,
                    'levels' => []
                ],
                self::ODDS_OPERATIONS_EXPENSE => [
                    'title' => 'Расход',
                    'data' => $model->emptyRow,
                    'isButtonable' => true,
                    'isBold' => true,
                    'rowType' => FinancePlan::ODDS_ROW_TYPE_OPERATIONS,
                    'levels' => [
                        self::ODDS_OPERATIONS_FIXED_COSTS => [
                            'title' => 'Постоянные расходы',
                            'data' => $model->emptyRow
                        ],
                        self::ODDS_OPERATIONS_GROUP_EXPENSES => [
                            'title' => 'Общие расходы',
                            'data' => $model->emptyRow
                        ]
                    ]
                ],
            ]
        ];

        $modelDataLevels = &$modelData[self::ODDS_OPERATIONS]['levels'];

        foreach ($modelGroup->financePlans as $plan) {
            foreach ($plan->oddsIncomeItems as $item) {
                $key = 'oddsIncomeType_' . $item->payment_type;
                $data = oddsCalc::getOperatingIncomesLevel($plan, $item->id);
                if ($item->type == FinancePlan::ODDS_ROW_TYPE_OPERATIONS) {
                    if (isset($modelDataLevels[self::ODDS_OPERATIONS_INCOME]['levels'][$key])) {
                        self::_sumRow($modelDataLevels[self::ODDS_OPERATIONS_INCOME]['levels'][$key]['data'], $data);
                    } else {
                        $modelDataLevels[self::ODDS_OPERATIONS_INCOME]['levels'][$key] = [
                            'title' => $item->getTitle(),
                            'data' => $data,
                        ];
                    }

                    self::_sumRow($modelData[self::ODDS_OPERATIONS]['data'], $data);
                    self::_sumRow($modelData[self::ODDS_OPERATIONS]['levels'][self::ODDS_OPERATIONS_INCOME]['data'], $data);
                }
            }
        }

        foreach ($modelGroup->financePlans as $plan) {
            foreach ($plan->oddsExpenditureItems as $item) {
                $key = 'oddsExpenseType_' . $item->payment_type;
                $data = oddsCalc::getOperatingExpensesLevel($plan, $item->id);
                if ($item->type == FinancePlan::ODDS_ROW_TYPE_OPERATIONS) {
                    if (isset($modelDataLevels[self::ODDS_OPERATIONS_EXPENSE]['levels'][$key])) {
                        self::_sumRow($modelDataLevels[self::ODDS_OPERATIONS_EXPENSE]['levels'][$key]['data'], $data);
                    } else {
                        $modelDataLevels[self::ODDS_OPERATIONS_EXPENSE]['levels'][$key] = [
                            'title' => $item->getTitle(),
                            'data' => $data,
                        ];
                    }

                    self::_subRow($modelData[self::ODDS_OPERATIONS]['data'], $data);
                    self::_sumRow($modelData[self::ODDS_OPERATIONS]['levels'][self::ODDS_OPERATIONS_EXPENSE]['data'], $data);
                }
            }
        }

        foreach ($modelGroup->financePlans as $plan) {
            $key = self::ODDS_OPERATIONS_FIXED_COSTS;
            $data = calc::getFixedCosts($plan);
            self::_subRow($modelData[self::ODDS_OPERATIONS]['data'], $data);
            self::_sumRow($modelData[self::ODDS_OPERATIONS]['levels'][self::ODDS_OPERATIONS_EXPENSE]['data'], $data);
            self::_sumRow($modelData[self::ODDS_OPERATIONS]['levels'][self::ODDS_OPERATIONS_EXPENSE]['levels'][$key]['data'], $data);
        }

        // SUB GROUP EXPENSES
        $key = self::ODDS_OPERATIONS_GROUP_EXPENSES;
        $data = groupCalc::getExpenses($modelGroup);
        self::_subRow($modelData[self::ODDS_OPERATIONS]['data'], $data);
        self::_sumRow($modelData[self::ODDS_OPERATIONS]['levels'][self::ODDS_OPERATIONS_EXPENSE]['data'], $data);
        self::_sumRow($modelData[self::ODDS_OPERATIONS]['levels'][self::ODDS_OPERATIONS_EXPENSE]['levels'][$key]['data'], $data);

        // Финансовая деятельность ///////////////////////////////////////////////////////////////////////////////////

        $modelData[self::ODDS_FINANCES] = [
            'title' => 'Финансовая деятельность, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'isButtonable' => false,
            'data' => $model->emptyRow,
            'levels' => [
                self::ODDS_FINANCES_INCOME => [
                    'title' => 'Приход',
                    'data' => $model->emptyRow,
                    'isButtonable' => true,
                    'isExpandable' => true,
                    'editAttrJS' => self::TRIGGER_ODDS_INCOME_CLASS,
                    'rowType' => FinancePlan::ODDS_ROW_TYPE_FINANCES,
                    'isBold' => true,
                    'levels' => []
                ],
                self::ODDS_FINANCES_EXPENSE => [
                    'title' => 'Расход',
                    'data' => $model->emptyRow,
                    'isButtonable' => true,
                    'isExpandable' => true,
                    'editAttrJS' => self::TRIGGER_ODDS_EXPENSE_CLASS,
                    'rowType' => FinancePlan::ODDS_ROW_TYPE_FINANCES,
                    'isBold' => true,
                    'levels' => []
                ],
            ]
        ];

        $modelDataLevels = &$modelData[self::ODDS_FINANCES]['levels'];

        // CREDIT
        foreach ($modelGroup->financePlans as $plan) {
            if ($plan->hasOddsCreditRows(IncomeItem::ITEM_CREDIT)) {
                // credits wrapper
                $modelDataLevels[self::ODDS_FINANCES_INCOME]['levels'][self::ODDS_ROW_CREDIT] = [
                    'title' => 'Кредит',
                    'data' => $plan->emptyRow, // oddsCalc::getLevelByIncomeItem($plan, IncomeItem::ITEM_CREDIT),
                    'isEditable' => false,
                    'levels' => []
                ];
                $modelDataLevels[self::ODDS_FINANCES_EXPENSE]['levels'][self::ODDS_ROW_CREDIT] = [
                    'title' => 'Погашение кредита',
                    'data' => $plan->emptyRow, //oddsCalc::getLevelByExpenditureItem($plan, ExpenditureItem::ITEM_REPAYMENT_CREDIT),
                    'isEditable' => false,
                    'levels' => []
                ];
                $modelDataLevels[self::ODDS_FINANCES_EXPENSE]['levels'][self::ODDS_ROW_CREDIT_PERCENT] = [
                    'title' => 'Проценты уплаченные',
                    'data' => $plan->emptyRow, // oddsCalc::getLevelByExpenditureItem($plan, ExpenditureItem::ITEM_PAYMENT_PERCENT),
                    'isEditable' => false,
                    'levels' => []
                ];
            }
            // LOAN
            if ($plan->hasOddsCreditRows(IncomeItem::ITEM_LOAN)) {
                // loans wrapper
                $modelDataLevels[self::ODDS_FINANCES_INCOME]['levels'][self::ODDS_ROW_LOAN] = [
                    'title' => 'Займ',
                    'data' => $plan->emptyRow, //oddsCalc::getLevelByIncomeItem($plan, IncomeItem::ITEM_LOAN),
                    'isEditable' => false,
                    'levels' => []
                ];
                $modelDataLevels[self::ODDS_FINANCES_EXPENSE]['levels'][self::ODDS_ROW_LOAN] = [
                    'title' => 'Погашение займа',
                    'data' => $plan->emptyRow, // oddsCalc::getLevelByExpenditureItem($plan, ExpenditureItem::ITEM_LOAN_REPAYMENT),
                    'isEditable' => false,
                    'levels' => []
                ];
                $modelDataLevels[self::ODDS_FINANCES_EXPENSE]['levels'][self::ODDS_ROW_LOAN_PERCENT] = [
                    'title' => 'Проценты уплаченные',
                    'data' => $plan->emptyRow, // oddsCalc::getLevelByExpenditureItem($plan, ExpenditureItem::ITEM_PAYMENT_PERCENT),
                    'isEditable' => false,
                    'levels' => []
                ];
            }
        }

        foreach ($modelGroup->financePlans as $plan) {
            foreach ($plan->oddsIncomeItems as $item) {
                $key = 'oddsIncomeItemId_' . $item->income_item_id;
                $data = oddsCalc::getFinancesIncomesLevel($plan, $item->id);
                if ($item->type == FinancePlan::ODDS_ROW_TYPE_FINANCES) {
                    if ($item->credit_id) {
                        // system rows
                        $wrapperKey = ($item->income_item_id == IncomeItem::ITEM_CREDIT) ? self::ODDS_ROW_CREDIT : self::ODDS_ROW_LOAN;
                        self::_sumRow($modelDataLevels[self::ODDS_FINANCES_INCOME]['levels'][$wrapperKey]['data'], $data);
                        //$modelDataLevels[self::ODDS_FINANCES_INCOME]['levels'][$wrapperKey]['levels'][$key] = [
                        //    'title' => $item->getTitle(),
                        //    'data' => $data,
                        //];
                    } else {
                        // customer rows
                        if (isset($modelDataLevels[self::ODDS_FINANCES_INCOME]['levels'][$key])) {
                            self::_sumRow($modelDataLevels[self::ODDS_FINANCES_INCOME]['levels'][$key]['data'], $data);
                        } else {
                            $modelDataLevels[self::ODDS_FINANCES_INCOME]['levels'][$key] = [
                                'title' => $item->getTitle(),
                                'data' => $data,
                            ];
                        }
                    }

                    self::_sumRow($modelData[self::ODDS_FINANCES]['data'], $data);
                    self::_sumRow($modelData[self::ODDS_FINANCES]['levels'][self::ODDS_FINANCES_INCOME]['data'], $data);
                }
            }
        }

        foreach ($modelGroup->financePlans as $plan) {
            foreach ($plan->oddsExpenditureItems as $item) {
                $key = 'oddsExpenseItemId_' . $item->expenditure_item_id;
                $data = oddsCalc::getFinancesExpensesLevel($plan, $item->id);
                if ($item->type == FinancePlan::ODDS_ROW_TYPE_FINANCES) {
                    if ($item->credit_id) {
                        // system rows
                        $wrapperKey = ($item->expenditure_item_id == ExpenditureItem::ITEM_PAYMENT_PERCENT)
                            ? self::ODDS_ROW_CREDIT_PERCENT
                            : ($item->expenditure_item_id == ExpenditureItem::ITEM_REPAYMENT_CREDIT ? self::ODDS_ROW_CREDIT : self::ODDS_ROW_LOAN);
                        self::_sumRow($modelDataLevels[self::ODDS_FINANCES_EXPENSE]['levels'][$wrapperKey]['data'], $data);
                        //$modelDataLevels[self::ODDS_FINANCES_EXPENSE]['levels'][$wrapperKey]['levels'][$key] = [
                        //    'title' => $item->getTitle(),
                        //    'data' => $data,
                        //];
                    } else {
                        // customer rows
                        if (isset($modelDataLevels[self::ODDS_FINANCES_EXPENSE]['levels'][$key])) {
                            self::_sumRow($modelDataLevels[self::ODDS_FINANCES_EXPENSE]['levels'][$key]['data'], $data);
                        } else {
                            $modelDataLevels[self::ODDS_FINANCES_EXPENSE]['levels'][$key] = [
                                'title' => $item->getTitle(),
                                'data' => $data,
                            ];
                        }
                    }

                    self::_subRow($modelData[self::ODDS_FINANCES]['data'], $data);
                    self::_sumRow($modelData[self::ODDS_FINANCES]['levels'][self::ODDS_FINANCES_EXPENSE]['data'], $data);
                }
            }
        }        

        // Инвестиционная деятельность ///////////////////////////////////////////////////////////////////////////////

        $modelData[self::ODDS_INVESTMENTS] = [
            'title' => 'Инвестиционная деятельность, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'isButtonable' => false,
            'data' => $model->emptyRow,
            'levels' => [
                self::ODDS_INVESTMENTS_INCOME => [
                    'title' => 'Приход',
                    'data' => $model->emptyRow,
                    'isButtonable' => true,
                    'isExpandable' => true,
                    'editAttrJS' => self::TRIGGER_ODDS_INCOME_CLASS,
                    'rowType' => FinancePlan::ODDS_ROW_TYPE_INVESTMENTS,
                    'isBold' => true,
                    'levels' => []
                ],
                self::ODDS_INVESTMENTS_EXPENSE => [
                    'title' => 'Расход',
                    'data' => $model->emptyRow,
                    'isButtonable' => true,
                    'isExpandable' => true,
                    'editAttrJS' => self::TRIGGER_ODDS_EXPENSE_CLASS,
                    'rowType' => FinancePlan::ODDS_ROW_TYPE_INVESTMENTS,
                    'isBold' => true,
                    'levels' => []
                ],
            ]
        ];

        $modelDataLevels = &$modelData[self::ODDS_INVESTMENTS]['levels'];

        // BALANCE ARTICLE
        foreach ($modelGroup->financePlans as $plan) {
            if ($plan->hasOddsBalanceArticleRows(ExpenditureItem::ITEM_BALANCE_ARTICLE_FIXED)) {
                $modelDataLevels[self::ODDS_INVESTMENTS_EXPENSE]['levels'][self::ODDS_ROW_BALANCE_ARTICLE_FIXED] = [
                    'title' => 'Приобретение Основных средств',
                    'data' => $plan->emptyRow, //oddsCalc::getLevelByExpenditureItem($model, ExpenditureItem::ITEM_BALANCE_ARTICLE_FIXED),
                    'isEditable' => false,
                    'levels' => []
                ];
            }
            if ($plan->hasOddsBalanceArticleRows(ExpenditureItem::ITEM_BALANCE_ARTICLE_INTANGIBLE)) {
                $modelDataLevels[self::ODDS_INVESTMENTS_EXPENSE]['levels'][self::ODDS_ROW_BALANCE_ARTICLE_INTANGIBLE] = [
                    'title' => 'Приобретение НМА',
                    'data' => $plan->emptyRow, //oddsCalc::getLevelByExpenditureItem($model, ExpenditureItem::ITEM_BALANCE_ARTICLE_INTANGIBLE),
                    'isEditable' => false,
                    'levels' => []
                ];
            }
        }

        foreach ($modelGroup->financePlans as $plan) {
            foreach ($plan->oddsIncomeItems as $item) {
                $key = 'oddsIncomeItemId_' . $item->income_item_id;
                $data = oddsCalc::getInvestmentsIncomesLevel($plan, $item->id);
                if ($item->type == FinancePlan::ODDS_ROW_TYPE_INVESTMENTS) {
                    if (isset($modelDataLevels[self::ODDS_INVESTMENTS_INCOME]['levels'][$key])) {
                        self::_sumRow($modelDataLevels[self::ODDS_INVESTMENTS_INCOME]['levels'][$key]['data'], $data);
                    } else {
                        $modelDataLevels[self::ODDS_INVESTMENTS_INCOME]['levels'][$key] = [
                            'title' => $item->getTitle(),
                            'data' => $data,
                        ];
                    }

                    self::_sumRow($modelData[self::ODDS_INVESTMENTS]['data'], $data);
                    self::_sumRow($modelData[self::ODDS_INVESTMENTS]['levels'][self::ODDS_INVESTMENTS_INCOME]['data'], $data);
                }
            }
        }

        foreach ($modelGroup->financePlans as $plan) {
            foreach ($plan->oddsExpenditureItems as $item) {
                $key = 'oddsExpenseItemId_' . $item->expenditure_item_id;
                $data = oddsCalc::getInvestmentsExpensesLevel($plan, $item->id);
                if ($item->type == FinancePlan::ODDS_ROW_TYPE_INVESTMENTS) {
                    if ($item->balance_article_id) {
                        // system rows
                        $wrapperKey = ($item->expenditure_item_id == ExpenditureItem::ITEM_BALANCE_ARTICLE_INTANGIBLE)
                            ? self::ODDS_ROW_BALANCE_ARTICLE_INTANGIBLE
                            : self::ODDS_ROW_BALANCE_ARTICLE_FIXED;
                        self::_sumRow($modelDataLevels[self::ODDS_INVESTMENTS_EXPENSE]['levels'][$wrapperKey]['data'], $data);
                        //$modelDataLevels[self::ODDS_INVESTMENTS_EXPENSE]['levels'][$wrapperKey]['levels'][$key] = [
                        //    'title' => $item->getTitle(),
                        //    'data' => $data,
                        //];
                    } else {
                        // customer rows
                        if (isset($modelDataLevels[self::ODDS_INVESTMENTS_EXPENSE]['levels'][$key])) {
                            self::_sumRow($modelDataLevels[self::ODDS_INVESTMENTS_EXPENSE]['levels'][$key]['data'], $data);
                        } else {
                            $modelDataLevels[self::ODDS_INVESTMENTS_EXPENSE]['levels'][$key] = [
                                'title' => $item->getTitle(),
                                'data' => $data,
                            ];
                        }
                    }

                    self::_subRow($modelData[self::ODDS_INVESTMENTS]['data'], $data);
                    self::_sumRow($modelData[self::ODDS_INVESTMENTS]['levels'][self::ODDS_INVESTMENTS_EXPENSE]['data'], $data);
                }
            }
        }

        // Чистый денежный поток ////////////////////////////////////////////////////////////////////////

        $modelData[self::ODDS_NET_PROFIT] = [
            'title' => 'Чистый денежный поток, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'data' => $model->emptyRow,
            'levels' => []
        ];

        foreach ($modelGroup->financePlans as $plan) {
            $data = oddsCalc::getOddsNetProfit($plan);
            self::_sumRow($modelData[self::ODDS_NET_PROFIT]['data'], $data);
        }

        // SUB GROUP EXPENSES
        self::_subRow($modelData[self::ODDS_NET_PROFIT]['data'], groupCalc::getExpenses($modelGroup));

        // Остаток на конец месяца ////////////////////////////////////////////////////////////////////////

        $modelData[self::ODDS_BALANCE] = [
            'title' => 'Остаток на конец месяца, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'isBalanceRow' => true,
            'data' => $model->emptyRow,
            'levels' => []
        ];

        foreach ($modelGroup->financePlans as $plan) {
            $data = oddsCalc::getOddsBalance($plan);
            self::_sumRow($modelData[self::ODDS_BALANCE]['data'], $data);
        }

        // SUB GROUP EXPENSES
        self::_subBalanceRow($modelData[self::ODDS_BALANCE]['data'], groupCalc::getExpenses($modelGroup));

        return $modelData;
    }

    public static function getGroupExpenses(FinancePlanGroup $model)
    {
        // Общие расходы ///////////////////////////////////////////////////////////////////////////////////

        $modelData[self::GROUP_EXPENSE] = [
            'title' => 'Общие расходы, ' . $model->getCurrencySymbol(),
            'totalBlock' => true,
            'isButtonable' => true,
            'isExpandable' => true,
            'editAttrJS' => self::TRIGGER_GROUP_EXPENSE_CLASS,
            'data' => groupCalc::getExpenses($model),
            'levels' => []
        ];

        foreach ($model->expenditureItems as $item) {
            $key = 'expense_' . $item->id;
            $modelData[self::GROUP_EXPENSE]['levels'][$key] = [
                'title' => ($item->expenditureItem) ? $item->expenditureItem->name : $key,
                'data' =>  groupCalc::getExpensesLevel($model, $item->id),
                'isEditable' => true,
                'editAttrJS' => self::TRIGGER_AUTOFILL_CLASS,
                'rowID' => $item->id
            ];
        }

        return $modelData;
    }

    private static function _avgRow(&$dest, $source, $divider)
    {
        foreach ($source as $ym => $amount)
            $dest[$ym] += $amount / $divider;
    }

    private static function _sumRow(&$dest, $source)
    {
        foreach ($source as $ym => $amount)
            $dest[$ym] += $amount;
    }

    private static function _subRow(&$dest, $source)
    {
        foreach ($source as $ym => $amount)
            $dest[$ym] -= $amount;
    }

    private static function _subBalanceRow(&$dest, $source)
    {
        $prevAmount = 0;
        foreach ($source as $ym => $amount) {
            $dest[$ym] -= ($amount + $prevAmount);
            $prevAmount += $amount;
        }
    }

    private static function _sumPlanRow(&$data, $key, $planId)
    {
        self::_sumRow($data[$key]['data'], $data[$key]['levels'][$planId]['data']);
    }
}