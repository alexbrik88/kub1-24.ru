<?php

namespace frontend\modules\analytics\models\financePlan;
use frontend\modules\analytics\models\financePlan\helpers\FinancePlanCalculatorHelper as calc;

use common\components\helpers\ArrayHelper;
use common\models\company\CompanyInfoIndustry;
use common\models\currency\Currency;
use frontend\modules\analytics\models\financePlan\traits\FinancePlanGroupSessionTrait;
use Yii;
use common\models\Company;
use common\models\employee\Employee;
use frontend\modules\analytics\models\financePlan\helpers\FinancePlanAutofillHelper as AutofillHelper;

/**
 * This is the model class for table "finance_plan_group".
 *
 * @property int $id
 * @property int $company_id
 * @property int $employee_id
 * @property string $name
 * @property int|null $year_from
 * @property int|null $month_from
 * @property int|null $year_till
 * @property int|null $month_till
 * @property string $currency_id
 * @property string|null $comment
 * @property int $created_at
 *
 * @property array $monthes
 * @property array $quarters
 * @property array $emptyRow
 *
 * @property Company $company
 * @property Employee $employee
 * @property FinancePlan[] $financePlans
 * @property FinancePlan $mainFinancePlan
 * @property FinancePlanGroupExpenditureItem[] $expenditureItems
 * @property FinancePlanGroupExpenditureItemAutofill[] $expenditureAutofills
 * @property string $dateFrom
 * @property string $dateTill
 * @property int $mainIndustryId
 * @property string $mainIndustryName
 */
class FinancePlanGroup extends \yii\db\ActiveRecord
{
    use FinancePlanGroupSessionTrait;

    const SESS_KEY = 'FinancePlanGroup.sessionPlans';
    const SESS_INCOME_ITEMS = 'incomeItems';
    const SESS_EXPENDITURE_ITEMS = 'expenditureItems';
    const SESS_PRIME_COST_ITEMS = 'primeCostItems';
    const SESS_ODDS_INCOME_ITEMS = 'oddsIncomeItems';
    const SESS_ODDS_EXPENDITURE_ITEMS = 'oddsExpenditureItems';

    const SESS_KEY_EXPENSES = 'FinancePlanGroup.sessionExpenses';

    const TAB_PLAN = 'plan';
    const TAB_EXPENSES = 'expenses';

    const PLAN_TOTAL_ID = 'total';

    private $_mainIndustryID;
    private $_mainIndustryName;
    
    /**
     * @var
     */
    public $textFrom;
    public $textTill;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_plan_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'employee_id', 'name', 'dateFrom', 'dateTill', 'mainIndustryId', 'mainIndustryName'], 'required'],
            [['company_id', 'employee_id', 'year_from', 'month_from', 'year_till', 'month_till', 'mainIndustryId'], 'integer'],
            [['comment'], 'string'],
            [['name', 'mainIndustryName'], 'string', 'max' => 64],
            [['currency_id'], 'string', 'max' => 3],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::class, 'targetAttribute' => ['employee_id' => 'id']],
            [['dateFrom', 'dateTill'], 'date', 'format' => 'php:Y-m-d'],
            [['textFrom', 'textTill'], 'safe'],
            [['textTill'], function ($attribute, $params) {
                if ((new \DateTime($this->dateTill)) < (new \DateTime($this->dateFrom))) {
                    $this->addError('textTill', 'Значение «Месяц окончания» не должно быть меньше значения «Месяц начала».');
                }
            },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'employee_id' => 'Employee ID',
            'created_at' => 'Дата',
            'name' => 'Название финансовой модели',
            'year_from' => 'Year From',
            'month_from' => 'Month From',
            'year_till' => 'Year Till',
            'month_till' => 'Month Till',
            'currency_id' => 'Валюта',
            'comment' => 'Комментарий',
            'mainIndustryId' => 'Тип основного направления бизнеса',
            'mainIndustryName' => 'Название основного направления бизнеса',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (!$insert && $this->_mainIndustryName) {
            $this->mainFinancePlan->name = $this->_mainIndustryName;
            $this->mainFinancePlan->save();
        }
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Gets query for [[Employee]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::class, ['id' => 'employee_id']);
    }

    /**
     * Gets query for [[FinancePlans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFinancePlans()
    {
        return $this->hasMany(FinancePlan::class, ['model_group_id' => 'id'])
            ->orderBy('finance_plan.sort');
    }

    public function getMainFinancePlan()
    {
        return $this->hasOne(FinancePlan::class, ['model_group_id' => 'id'])
            ->andWhere(['is_main' => 1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenditureItems()
    {
        return $this->hasMany(FinancePlanGroupExpenditureItem::class, ['model_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenditureAutofills()
    {
        return $this->hasMany(FinancePlanGroupExpenditureItemAutofill::class, ['model_group_id' => 'id']);
    }

    /**
     * @return string|null
     */
    public function getDateFrom()
    {
        if ($date = date_create_from_format('Y-m-d', $this->year_from.'-'.$this->month_from.'-01')) {
            return $date->format('Y-m-d');
        }

        return null;
    }

    /**
     * @return string|null
     */
    public function getDateTill()
    {
        if ($date = date_create_from_format('Y-m-d', $this->year_till.'-'.$this->month_till.'-01')) {
            return $date->format('Y-m-t');
        }

        return null;
    }

    public function setDateFrom($value)
    {
        if ($date = date_create_from_format('Y-m-d', $value)) {
            $this->year_from = $date->format('Y');
            $this->month_from = $date->format('n');
        }
    }

    public function setDateTill($value)
    {
        if ($date = date_create_from_format('Y-m-d', $value)) {
            $this->year_till = $date->format('Y');
            $this->month_till = $date->format('n');
        }
    }

    public function getMainIndustryId()
    {
        return $this->_mainIndustryID;
    }

    public function setMainIndustryId($value)
    {
        $this->_mainIndustryID = (int)$value;
    }

    public function getMainIndustryName()
    {
        return $this->_mainIndustryName;
    }

    public function setMainIndustryName($value)
    {
        $this->_mainIndustryName = (string)$value;
    }

    public function getPeriod()
    {
        $dateFrom = date_create_from_format('Y-m-d', $this->year_from.'-'.$this->month_from.'-01');
        $dateTill = date_create_from_format('Y-m-d', $this->year_till.'-'.$this->month_till.'-01');
        if ($dateFrom && $dateTill) {
            $diff = date_diff($dateFrom, $dateTill);
            return 1 + $diff->format('%y') * 12 + $diff->format('%m');
        }

        return 0;
    }

    /*
     * HELPERS
     */

    public function getMonthes()
    {
        $ret = [];
        for ($y = $this->year_from; $y <= $this->year_till; $y++) {
            for ($m = 1; $m <= 12; $m++) {

                if ($y == $this->year_from && $m < $this->month_from) continue;
                if ($y == $this->year_till && $m > $this->month_till) break;

                $monthNum = str_pad($m, 2, "0", STR_PAD_LEFT);
                $ret[] = $y . $monthNum;
            }
        }

        return $ret;
    }

    public function getQuarters()
    {
        $ret = [];
        for ($y = $this->year_from; $y <= $this->year_till; $y++) {
            for ($m = 1; $m <= 12; $m++) {

                if ($y == $this->year_from && $m < $this->month_from) continue;
                if ($y == $this->year_till && $m > $this->month_till) break;

                $quarterNum = str_pad((int)ceil($m / 3), 2, "0", STR_PAD_LEFT);
                $monthNum = str_pad($m, 2, "0", STR_PAD_LEFT);

                if (!isset($ret[$y . $quarterNum]))
                    $ret[$y . $quarterNum] = [];

                $ret[$y . $quarterNum][] = $y . $monthNum;
            }
        }

        return $ret;
    }

    public function createMainPlan()
    {
        $model = new FinancePlan([
            'model_group_id' => $this->id,
            'industry_id' => $this->_mainIndustryID,
            'name' => $this->_mainIndustryName,
            'is_main' => 1,
            'sort' => 1
        ]);

        return ($model->save()) ? $model : null;
    }

    public function getActivePlan($planId)
    {
        foreach ($this->financePlans as $p) {
            if ($p->id == $planId || $planId === null) {
                return $p;
            }
        }
        
        if ($planId === self::PLAN_TOTAL_ID) {
            return new FinancePlan([
                'id' => self::PLAN_TOTAL_ID,
                'model_group_id' => $this->id,
            ]);
        }

        return null;
    }

    public function isEditMode()
    {
        return !empty(Yii::$app->session->get('FinancePlanGroup.updatedPlans'));
    }

    public function updateMonthsRange()
    {
        foreach ($this->financePlans as $plan) {

            $relatedTables = array_merge(
                $plan->incomeItems,
                $plan->expenditureItems,
                $plan->primeCostItems,
                $plan->oddsIncomeItems,
                $plan->oddsExpenditureItems
            );

            foreach ($relatedTables as $item)
            {
                $newData = [];
                $oldData = $item->getRowData();
                foreach ($this->getMonthes() as $m) {
                    $newData[$m] = 100 * ArrayHelper::getValue($oldData, $m, 0);
                }

                $item->data = $newData;

                if (!$item->save()) {
                    return false;
                }
            }
        }

        $relatedGroupTables = $this->expenditureItems;

        foreach ($relatedGroupTables as $item)
        {
            $newData = [];
            $oldData = $item->getRowData();
            foreach ($this->getMonthes() as $m) {
                $newData[$m] = 100 * ArrayHelper::getValue($oldData, $m, 0);
            }

            $item->data = $newData;

            if (!$item->save()) {
                return false;
            }
        }

        return true;
    }

    public function getPlanNextSortNumber()
    {
        return $this->getFinancePlans()->max('sort') + 1;
    }

    public function getCurrencySymbol()
    {
        return ArrayHelper::getValue(Currency::$currencySymbolsById, $this->currency_id);
    }

    public function getCurrencyName()
    {
        return ArrayHelper::getValue(Currency::$currencyNameById, $this->currency_id);
    }

    public function updateTotals($plans = [])
    {
        $updatedPlans = $plans ?: $this->financePlans;

        $totalRevenue = 0;
        $totalMarginIncome = 0;
        $totalNetProfit = 0;
        foreach ($updatedPlans as $p) {
            $totalRevenue += array_sum(calc::getRevenue($p));
            $totalNetProfit += array_sum(calc::getNetProfit($p));
            $totalMarginIncome += array_sum(calc::getMarginIncome($p));
        }

        $this->updateAttributes([
            'total_revenue' => (int)$totalRevenue * 100,
            'total_net_profit' => (int)$totalNetProfit * 100,
            'total_marginality' => (float)$totalMarginIncome / (($totalRevenue > 0) ? $totalRevenue : 9E9) * 100,
        ]);

    }

    public function getEmptyRow()
    {
        return array_combine($this->getMonthes(), array_fill(0, count($this->getMonthes()), 0));
    }

    /**
     * @param $userData
     */
    public function loadUserData($userData)
    {
        foreach ($this->expenditureItems as $expenditureItem) {
            if ($data = ArrayHelper::getValue($userData, 'expense_' . $expenditureItem->id))
                $expenditureItem->setRowData($data);
        }
    }

    /**
     * @param $autofill
     */
    public function loadAutofillData($autofill)
    {
        if ($autofill) {
            $attr = $autofill['attr'] ?? "";
            $rowID = $autofill['row_id'] ?? "";
            @list($ioType, $rowType) = explode('_', $attr);

            if (!in_array($ioType, ['expense']))
                return;

            if ($ioType == 'expense') {

                foreach ($this->expenditureItems as $item)
                    if ($item->id == $rowID)
                        $activeRow = $item;

                if (isset($activeRow)) {
                    $autofillModel =
                        FinancePlanGroupExpenditureItemAutofill::findOne(['model_group_id' => $this->id, 'row_id' => $activeRow->id]) ?:
                            new FinancePlanGroupExpenditureItemAutofill(['model_group_id' => $this->id, 'row_id' => $activeRow->id]);
                }
            }
            else {
                $autofillModel = null;
            }

            if (isset($activeRow) && isset($autofillModel)) {
                $autofillModel->start_month = self::sanitize(ArrayHelper::getValue($autofill, 'start_month'));
                $autofillModel->start_amount = round(100 * self::sanitize(ArrayHelper::getValue($autofill, 'start_amount')));
                $autofillModel->amount = round(100 * self::sanitize(ArrayHelper::getValue($autofill, 'amount')));
                $autofillModel->limit_amount = round(100 * self::sanitize(ArrayHelper::getValue($autofill, 'limit_amount')));
                $autofillModel->action = self::sanitize(ArrayHelper::getValue($autofill, 'action'));

                if ($autofillModel->save()) {
                    if ($ioType == 'expense') {
                        if ($filledRowData = AutofillHelper::fillGroupExpense($autofillModel, $activeRow)) {
                            $activeRow->setRowData($filledRowData);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $val
     * @return float
     */
    public static function sanitize($val)
    {
        return (float)str_replace([',',' '], ['.',''], $val);
    }
}
