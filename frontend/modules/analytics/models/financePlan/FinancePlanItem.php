<?php

namespace frontend\modules\analytics\models\financePlan;

use Yii;

/**
 * @property FinancePlan $model
 */
class FinancePlanItem extends \yii\db\ActiveRecord
{
    const RELATION_TYPE_UNSET = 0;
    const RELATION_TYPE_REVENUE = 1;
    const RELATION_TYPE_SALES_COUNT = 2;
    const RELATION_TYPE_EBIDTA = 3;

    public function getRowData()
    {
        $data = (is_string($this->data))
            ? (array)json_decode($this->data, true)
            : $this->data;

        return array_map(function($v) { return $this->div100($v); }, $data);
    }

    public function setRowData($array)
    {
        $_data = [];
        foreach ($this->getRowData() as $k => $v) {
            $_data[$k] = $this->mul100(isset($array[$k])
                ? $this->sanitize($array[$k])
                : $v);
        }

        $this->data = json_encode($_data);
    }

    public function mul100($val)
    {
        return round(100 * $val);
    }

    public function div100($val)
    {
        return round((int)$val / 100, 2);
    }

    public static function sanitize($val)
    {
        return (float)str_replace([',',' '], ['.',''], $val);
    }
}
