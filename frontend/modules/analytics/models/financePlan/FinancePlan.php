<?php

namespace frontend\modules\analytics\models\financePlan;

use common\models\company\CompanyInfoIndustry;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use frontend\modules\analytics\models\financePlan\helpers\FinancePlanAutofillHelper as AutofillHelper;
use frontend\modules\analytics\models\financePlan\odds\FinancePlanOddsExpenditureItem;
use frontend\modules\analytics\models\financePlan\odds\FinancePlanOddsExpenditureItemAutofill;
use frontend\modules\analytics\models\financePlan\odds\FinancePlanOddsIncomeItem;
use frontend\modules\analytics\models\financePlan\odds\FinancePlanOddsIncomeItemAutofill;
use frontend\modules\analytics\models\financePlan\odds\FinancePlanOddsItem;
use Yii;
use common\components\helpers\ArrayHelper;
use yii\web\Response;
use yii\widgets\ActiveForm;


/**
 * This is the model class for table "finance_plan".
 *
 * @property int $id
 * @property int $model_group_id
 * @property int $industry_id
 * @property string $name
 * @property int $is_main
 * @property int $sort
 *
 * @property bool $isTotal
 * @property array $emptyRow
 *
 * @property FinancePlanGroup $modelGroup
 * @property CompanyInfoIndustry $industryType
 *
 * PAL
 * @property FinancePlanPrimeCostItem[] $primeCostItems
 * @property FinancePlanExpenditureItem[] $expenditureItems
 * @property FinancePlanIncomeItem[] $incomeItems
 * @property FinancePlanExpenditureItemAutofill[] $expenditureAutofills
 * @property FinancePlanIncomeItemAutofill[] $incomeAutofills
 *
 * ODDS
 * @property FinancePlanOddsExpenditureItem[] $oddsExpenditureItems
 * @property FinancePlanOddsIncomeItem[] $oddsIncomeItems
 * @property FinancePlanOddsExpenditureItemAutofill[] $oddsExpenditureAutofills
 * @property FinancePlanOddsIncomeItemAutofill[] $oddsIncomeAutofills
 */
class FinancePlan extends \yii\db\ActiveRecord
{
    const ROW_TYPE_INCOME_NEW_SALES = 1;
    const ROW_TYPE_INCOME_SALES = 2;
    const ROW_TYPE_INCOME_AVG_CHECK = 3;
    const ROW_TYPE_INCOME_OTHER = 5;

    const ROW_TYPE_EXPENSE_VARIABLE = 1;
    const ROW_TYPE_EXPENSE_FIXED = 2;
    const ROW_TYPE_EXPENSE_PRIME_COST = 3;
    const ROW_TYPE_EXPENSE_TAX = 4;
    const ROW_TYPE_EXPENSE_OTHER = 5;

    const ODDS_ROW_TYPE_OPERATIONS = 1;
    const ODDS_ROW_TYPE_FINANCES = 2;
    const ODDS_ROW_TYPE_INVESTMENTS = 3;

    const ODDS_IO_TYPE_EXPENSE = 1;
    const ODDS_IO_TYPE_INCOME = 2;

    const GROUP_IO_TYPE_EXPENSE = 1;
    const GROUP_IO_TYPE_INCOME = 2;

    const SYSTEM_INCOME_ITEMS_IDS = [
        InvoiceIncomeItem::ITEM_CREDIT,
        InvoiceIncomeItem::ITEM_LOAN,
    ];
    const SYSTEM_EXPENDITURE_ITEMS_IDS = [
        InvoiceExpenditureItem::ITEM_REPAYMENT_CREDIT,
        InvoiceExpenditureItem::ITEM_LOAN_REPAYMENT,
        InvoiceExpenditureItem::ITEM_PAYMENT_PERCENT,
        InvoiceExpenditureItem::ITEM_BALANCE_ARTICLE_FIXED,
        InvoiceExpenditureItem::ITEM_BALANCE_ARTICLE_INTANGIBLE,
    ];

    public $newRelatedModel;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_plan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_group_id', 'industry_id', 'name'], 'required'],
            [['model_group_id', 'industry_id', 'is_main', 'sort'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['model_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinancePlanGroup::class, 'targetAttribute' => ['model_group_id' => 'id']],
            [['industry_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompanyInfoIndustry::class, 'targetAttribute' => ['industry_id' => 'id']],
            [['is_main'], 'default', 'value' => 0],
            [['sort'], 'default', 'value' => 2],
            [['is_main', 'sort'], 'filter', 'filter' => function ($value) {
                return intval($value);
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_group_id' => 'Model Group ID',
            'industry_id' => 'Направление',
            'name' => 'Название',
        ];
    }

    /**
     * @inheritdoc
     */
    public function formName()
    {
        return parent::formName() . ($this->isNewRecord ? '' : $this->id);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrimeCostItems()
    {
        return $this->hasMany(FinancePlanPrimeCostItem::class, ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenditureItems()
    {
        return $this->hasMany(FinancePlanExpenditureItem::class, ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomeItems()
    {
        return $this->hasMany(FinancePlanIncomeItem::class, ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOddsExpenditureItems()
    {
        return $this->hasMany(FinancePlanOddsExpenditureItem::class, ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOddsIncomeItems()
    {
        return $this->hasMany(FinancePlanOddsIncomeItem::class, ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenditureAutofills()
    {
        return $this->hasMany(FinancePlanExpenditureItemAutofill::class, ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomeAutofills()
    {
        return $this->hasMany(FinancePlanIncomeItemAutofill::class, ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOddsExpenditureAutofills()
    {
        return $this->hasMany(FinancePlanOddsExpenditureItemAutofill::class, ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOddsIncomeAutofills()
    {
        return $this->hasMany(FinancePlanOddsIncomeItemAutofill::class, ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModelGroup()
    {
        return $this->hasOne(FinancePlanGroup::class, ['id' => 'model_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndustryType()
    {
        return $this->hasOne(CompanyInfoIndustry::class, ['id' => 'industry_id']);
    }

    public function getEmptyRow()
    {
        return array_combine($this->modelGroup->getMonthes(), array_fill(0, count($this->modelGroup->getMonthes()), 0));
    }

    public function getFilledRow($value)
    {
        return array_combine($this->modelGroup->getMonthes(), array_fill(0, count($this->modelGroup->getMonthes()), $value));
    }

    public function createEmptyIncomeItems()
    {
        $requiredTypes = [
            self::ROW_TYPE_INCOME_NEW_SALES,
            self::ROW_TYPE_INCOME_SALES,
            self::ROW_TYPE_INCOME_AVG_CHECK
        ];

        foreach ($requiredTypes as $type) {
            $modelItem = new FinancePlanIncomeItem([
                'model_id' => $this->id,
                'type' => $type,
                'data' => $this->getEmptyRow()
            ]);
            if (!$modelItem->save()) {
                return false;
            }
        }

        return true;
    }

    public function createEmptyExpenseItems()
    {
        $requiredTypes = [
            self::ROW_TYPE_EXPENSE_TAX,
        ];

        foreach ($requiredTypes as $type) {
            $modelItem = new FinancePlanExpenditureItem([
                'model_id' => $this->id,
                'expenditure_item_id' => InvoiceExpenditureItem::ITEM_TAX_AND_PROFIT,
                'relation_type' => FinancePlanItem::RELATION_TYPE_REVENUE,
                'action' => 0,
                'type' => $type,
                'data' => $this->getEmptyRow()
            ]);
            if (!$modelItem->save()) {
                return false;
            }
        }

        return true;
    }

    public function createOddsEmptyIncomeItems()
    {
        $requiredPaymentTypes = [
            FinancePlanOddsIncomeItem::PAYMENT_TYPE_PREPAYMENT,
            FinancePlanOddsIncomeItem::PAYMENT_TYPE_SURCHARGE,
        ];

        $requiredType = self::ODDS_ROW_TYPE_OPERATIONS;

        foreach ($requiredPaymentTypes as $paymentType) {
            $modelItem = new FinancePlanOddsIncomeItem([
                'model_id' => $this->id,
                'income_item_id' => InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER,
                'type' => $requiredType,
                'payment_type' => $paymentType,
                'payment_delay' => 0,
                'relation_type' => FinancePlanOddsItem::RELATION_TYPE_REVENUE,
                'action' => ($paymentType == FinancePlanOddsItem::PAYMENT_TYPE_PREPAYMENT) ? 0 : 100,
                'data' => $this->getEmptyRow()
            ]);

            if (!$modelItem->save()) {
                return false;
            }
        }

        return true;
    }

    public function createOddsEmptyExpenseItems()
    {
        $requiredPaymentTypes = [
            FinancePlanOddsIncomeItem::PAYMENT_TYPE_PREPAYMENT,
            FinancePlanOddsIncomeItem::PAYMENT_TYPE_SURCHARGE,
        ];

        $requiredType = self::ODDS_ROW_TYPE_OPERATIONS;

        foreach ($requiredPaymentTypes as $paymentType) {
            $modelItem = new FinancePlanOddsExpenditureItem([
                'model_id' => $this->id,
                'expenditure_item_id' => InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT,
                'type' => $requiredType,
                'payment_type' => $paymentType,
                'payment_delay' => 0,
                'relation_type' => FinancePlanOddsItem::RELATION_TYPE_PRIME_COST,
                'action' => ($paymentType == FinancePlanOddsItem::PAYMENT_TYPE_PREPAYMENT) ? 0 : 100,
                'data' => $this->getEmptyRow()
            ]);

            if (!$modelItem->save()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param $userData
     */
    public function loadUserData($userData)
    {
        foreach ($this->incomeItems as $incomeItem) {
            if ($data = ArrayHelper::getValue($userData, 'systemIncome_' . $incomeItem->type))
                $incomeItem->setRowData($data);
        }
        foreach ($this->incomeItems as $incomeItem) {
            if ($data = ArrayHelper::getValue($userData, 'income_' . $incomeItem->id))
                $incomeItem->setRowData($data);
        }
        foreach ($this->primeCostItems as $primeCostItem) {
            if ($data = ArrayHelper::getValue($userData, 'prime_cost_' . $primeCostItem->id))
                $primeCostItem->setRowData($data);
        }
        foreach ($this->expenditureItems as $expenditureItem) {
            if ($data = ArrayHelper::getValue($userData, 'expense_' . $expenditureItem->id))
                $expenditureItem->setRowData($data);
        }
        foreach ($this->expenditureItems as $expenditureItem) {
            if ($expenditureItem->type == self::ROW_TYPE_EXPENSE_TAX)
                if ($data = ArrayHelper::getValue($userData, 'tax'))
                    $expenditureItem->setRowData($data);
        }
        foreach ($this->oddsIncomeItems as $oddsIncomeItem) {
            if ($data = ArrayHelper::getValue($userData, 'oddsIncome_' . $oddsIncomeItem->id))
                $oddsIncomeItem->setRowData($data);
        }
        foreach ($this->oddsExpenditureItems as $oddsExpenditureItem) {
            if ($data = ArrayHelper::getValue($userData, 'oddsExpense_' . $oddsExpenditureItem->id))
                $oddsExpenditureItem->setRowData($data);
        }
    }

    /**
     * @param $autofill
     */
    public function loadAutofillData($autofill)
    {
        if ($autofill) {
            $attr = $autofill['attr'] ?? "";
            $rowID = $autofill['row_id'] ?? "";
            @list($ioType, $rowType) = explode('_', $attr);

            if (!in_array($ioType, ['systemIncome', 'income', 'expense', 'oddsIncome', 'oddsExpense']))
                return;

            if ($ioType == 'systemIncome' || $ioType == 'income') {

                foreach ($this->incomeItems as $item)
                    if ($item->id == $rowID)
                        $activeRow = $item;

                if (isset($activeRow)) {
                    $autofillModel =
                        FinancePlanIncomeItemAutofill::findOne(['model_id' => $this->id, 'row_id' => $activeRow->id]) ?:
                        new FinancePlanIncomeItemAutofill(['model_id' => $this->id, 'row_id' => $activeRow->id]);
                }
            }
            elseif ($ioType == 'expense') {

                foreach ($this->expenditureItems as $item)
                    if ($item->id == $rowID)
                        $activeRow = $item;

                if (isset($activeRow)) {
                    $autofillModel =
                        FinancePlanExpenditureItemAutofill::findOne(['model_id' => $this->id, 'row_id' => $activeRow->id]) ?:
                        new FinancePlanExpenditureItemAutofill(['model_id' => $this->id, 'row_id' => $activeRow->id]);
                }
            }
            elseif ($ioType == 'oddsIncome') {

                foreach ($this->oddsIncomeItems as $item)
                    if ($item->id == $rowID)
                        $activeRow = $item;

                if (isset($activeRow)) {
                    $autofillModel =
                        FinancePlanOddsIncomeItemAutofill::findOne(['model_id' => $this->id, 'row_id' => $activeRow->id]) ?:
                        new FinancePlanOddsIncomeItemAutofill(['model_id' => $this->id, 'row_id' => $activeRow->id]);
                }
            }
            elseif ($ioType == 'oddsExpense') {

                foreach ($this->oddsExpenditureItems as $item)
                    if ($item->id == $rowID)
                        $activeRow = $item;

                if (isset($activeRow)) {
                    $autofillModel =
                        FinancePlanOddsExpenditureItemAutofill::findOne(['model_id' => $this->id, 'row_id' => $activeRow->id]) ?:
                        new FinancePlanOddsExpenditureItemAutofill(['model_id' => $this->id, 'row_id' => $activeRow->id]);
                }
            }
            else {
                $autofillModel = null;
            }

            if (isset($activeRow) && isset($autofillModel)) {
                $autofillModel->start_month = self::sanitize(ArrayHelper::getValue($autofill, 'start_month'));
                $autofillModel->start_amount = round(100 * self::sanitize(ArrayHelper::getValue($autofill, 'start_amount')));
                $autofillModel->amount = round(100 * self::sanitize(ArrayHelper::getValue($autofill, 'amount')));
                $autofillModel->limit_amount = round(100 * self::sanitize(ArrayHelper::getValue($autofill, 'limit_amount')));
                $autofillModel->action = self::sanitize(ArrayHelper::getValue($autofill, 'action'));

                if ($autofillModel->save()) {
                    if ($ioType == 'systemIncome' || $ioType == 'income') {
                        if ($filledRowData = AutofillHelper::fillIncome($autofillModel, $activeRow)) {
                            $activeRow->setRowData($filledRowData);
                        }
                    }
                    if ($ioType == 'expense') {
                        if ($filledRowData = AutofillHelper::fillExpense($autofillModel, $activeRow)) {
                            $activeRow->setRowData($filledRowData);
                        }
                    }
                    if ($ioType == 'oddsIncome') {
                        if ($filledRowData = AutofillHelper::fillOddsIncome($autofillModel, $activeRow)) {
                            $activeRow->setRowData($filledRowData);
                        }
                    }
                    if ($ioType == 'oddsExpense') {
                        if ($filledRowData = AutofillHelper::fillOddsExpense($autofillModel, $activeRow)) {
                            $activeRow->setRowData($filledRowData);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $val
     * @return float
     */
    public static function sanitize($val)
    {
        return (float)str_replace([',',' '], ['.',''], $val);
    }

    /**
     * @param $relatedModel
     * @param $relatedModelType
     */
    public function setNewSessionRelatedModel($relatedModel, $relatedModelType)
    {
        switch ($relatedModelType) {
            case FinancePlanGroup::SESS_INCOME_ITEMS:
                $this->newRelatedModel[$relatedModelType][$relatedModel->id]['data'] = $relatedModel->data;
                break;
            case FinancePlanGroup::SESS_EXPENDITURE_ITEMS:
            case FinancePlanGroup::SESS_PRIME_COST_ITEMS:
                $this->newRelatedModel[$relatedModelType][$relatedModel->id]['data'] = $relatedModel->data;
                $this->newRelatedModel[$relatedModelType][$relatedModel->id]['action'] = $relatedModel->action;
                $this->newRelatedModel[$relatedModelType][$relatedModel->id]['relation_type'] = $relatedModel->relation_type;
                break;
            case FinancePlanGroup::SESS_ODDS_INCOME_ITEMS:
            case FinancePlanGroup::SESS_ODDS_EXPENDITURE_ITEMS:
                $this->newRelatedModel[$relatedModelType][$relatedModel->id]['data'] = $relatedModel->data;
                break;
        }
    }

    public function getCurrencySymbol()
    {
        return $this->modelGroup->getCurrencySymbol();
    }

    public function getIsTotal()
    {
        return $this->id === FinancePlanGroup::PLAN_TOTAL_ID;
    }

    public function hasOddsCreditRows($incomeItemId = InvoiceIncomeItem::ITEM_CREDIT)
    {
        foreach ($this->oddsIncomeItems as $item) {
            if ($item->type == FinancePlan::ODDS_ROW_TYPE_FINANCES)
                if ($item->credit_id && $item->income_item_id == $incomeItemId)
                    return true;
            }

        return false;
    }

    public function hasOddsBalanceArticleRows($expenditureItemId = InvoiceExpenditureItem::ITEM_BALANCE_ARTICLE_FIXED)
    {
        foreach ($this->oddsExpenditureItems as $item) {
            if ($item->type == FinancePlan::ODDS_ROW_TYPE_INVESTMENTS)
                if ($item->balance_article_id && $item->expenditure_item_id == $expenditureItemId)
                    return true;
        }

        return false;
    }
}
