<?php

namespace frontend\modules\analytics\models\financePlan\traits;

use Yii;
use yii\helpers\ArrayHelper;

trait FinancePlanGroupSessionTrait {

    public $sessionRelatedModels = [
        self::SESS_INCOME_ITEMS,
        self::SESS_EXPENDITURE_ITEMS,
        self::SESS_PRIME_COST_ITEMS,
        self::SESS_ODDS_INCOME_ITEMS,
        self::SESS_ODDS_EXPENDITURE_ITEMS,
    ];

    public $emptySessionPlan = [
        self::SESS_INCOME_ITEMS => [
            'data' => []
        ],
        self::SESS_EXPENDITURE_ITEMS => [
            'data' => [],
            'action' => null,
            'relation_type' => null
        ],
        self::SESS_PRIME_COST_ITEMS => [
            'data' => [],
            'action' => null,
            'relation_type' => null
        ],
        self::SESS_ODDS_INCOME_ITEMS => [
            'data' => []
        ],
        self::SESS_ODDS_EXPENDITURE_ITEMS => [
            'data' => []
        ],
    ];

    public function setSessionData()
    {
        $sessionPlans = [];
        foreach ($this->financePlans as $plan) {
            $sessionPlans[$plan->id] = [];
            foreach ($this->sessionRelatedModels as $relatedModels) {
                $sessionPlans[$plan->id][$relatedModels] = [];

                // new
                $newRelatedModels = $plan->newRelatedModel[$relatedModels] ?? null;
                if ($newRelatedModels) {
                    foreach ($newRelatedModels as $newRelatedModelId => $newRelatedModelFields)
                        foreach ($newRelatedModelFields as $field => $value)
                            $sessionPlans[$plan->id][$relatedModels][$newRelatedModelId][$field]
                                = $value;
                }

                foreach ($plan->{$relatedModels} as $relatedModel) {
                    $sessionPlans[$plan->id][$relatedModels][$relatedModel->id] = [];
                    foreach ($this->emptySessionPlan[$relatedModels] as $field => $value)
                        $sessionPlans[$plan->id][$relatedModels][$relatedModel->id][$field]
                            = $relatedModel->{$field};
                }
            }
        }

        // group expenses
        $sessionExpenses = [];
        foreach ($this->expenditureItems as $item) {
            $sessionExpenses[$item->id] = $item['data'];
        }

        Yii::$app->session->set(self::SESS_KEY, $sessionPlans);
        Yii::$app->session->set(self::SESS_KEY_EXPENSES, $sessionExpenses);
    }

    public function getSessionData($remove = false)
    {
        $sessionPlans = ($remove)
            ? Yii::$app->session->remove(self::SESS_KEY)
            : Yii::$app->session->get(self::SESS_KEY);

        foreach ($this->financePlans as $plan) {
            $planID = $plan->id;
            foreach ($this->sessionRelatedModels as $relatedModels) {
                foreach ($plan->{$relatedModels} as $relatedModel) {
                    $relatedModelID = $relatedModel->id;
                    foreach ($this->emptySessionPlan[$relatedModels] as $field => $value) {
                        $defaultValue = ($field === 'data') ? json_encode($this->emptyRow) : 0;
                        $relatedModel->{$field} = ArrayHelper::getValue($sessionPlans, "{$planID}.{$relatedModels}.{$relatedModelID}.{$field}", $defaultValue);
                    }
                }
            }
        }

        // group expenses
        $sessionExpenses = ($remove)
            ? Yii::$app->session->remove(self::SESS_KEY_EXPENSES)
            : Yii::$app->session->get(self::SESS_KEY_EXPENSES);

        foreach ($this->expenditureItems as $item)
            $item->data =  ArrayHelper::getValue($sessionExpenses, $item->id, $this->emptyRow);

        return $this->financePlans;
    }

    public function removeSessionData()
    {
        return $this->getSessionData(true);
    }
}