<?php

namespace frontend\modules\analytics\models\financePlan;

use Yii;
use common\models\document\InvoiceExpenditureItem;

/**
 * This is the model class for table "finance_plan_expenditure_item".
 *
 * @property int $id
 * @property int $model_id
 * @property int $expenditure_item_id
 * @property int $relation_type
 * @property int $action
 * @property int $type 1 - переменные расходы, 2 - постоянные расходы
 * @property string|null $data
 *
 * @property InvoiceExpenditureItem $expenditureItem
 * @property FinancePlanExpenditureItemAutofill $autofill
 * @property FinancePlan $model
 */
class FinancePlanExpenditureItem extends FinancePlanItem
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_plan_expenditure_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_id', 'expenditure_item_id', 'type', 'relation_type'], 'required'],
            [['action'], 'required', 'when' => function(FinancePlanExpenditureItem $model) {
                return $model->relation_type != self::RELATION_TYPE_UNSET;
            }],
            [['model_id', 'expenditure_item_id', 'type', 'relation_type'], 'integer'],
            [['action'], 'filter', 'filter' => function($v) { return ((float)$v) ? self::sanitize($v) : $v; }],
            [['action'], 'number'],
            [['data'], 'string'],
            [['expenditure_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceExpenditureItem::class, 'targetAttribute' => ['expenditure_item_id' => 'id']],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinancePlan::class, 'targetAttribute' => ['model_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_id' => 'Model ID',
            'expenditure_item_id' => 'Строка',
            'relation_type' => 'Зависит от строки',
            'action' => 'Умножить на',
            'type' => 'Тип расходов (Постоянный / Переменный)',
            'data' => 'Data',
        ];
    }

    public function beforeValidate()
    {
        if (is_array($this->data))
            $this->data = json_encode($this->data);

        return parent::beforeValidate();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenditureItem()
    {
        return $this->hasOne(InvoiceExpenditureItem::class, ['id' => 'expenditure_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutofill()
    {
        return $this->hasOne(FinancePlanExpenditureItemAutofill::class, ['row_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(FinancePlan::class, ['id' => 'model_id']);
    }

    /**
     * @return bool
     */
    public function isEditable()
    {
        return $this->relation_type == self::RELATION_TYPE_UNSET;
    }
}
