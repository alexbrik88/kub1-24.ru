<?php

namespace frontend\modules\analytics\models\financePlan;

use Carbon\Carbon;
use common\components\date\DateHelper;
use common\components\TextHelper;
use frontend\modules\reference\models\BalanceArticlesCategories;
use frontend\modules\reference\models\BalanceArticlesSubcategories;
use Yii;

/**
 * This is the model class for table "finance_plan_balance_article".
 *
 * @property int $id
 * @property int $model_id
 * @property int $type
 * @property int $status
 * @property string $name
 * @property int $category
 * @property int $subcategory
 * @property int|null $useful_life_in_month
 * @property int $count
 * @property string $purchased_at
 * @property string|null $sold_at
 * @property string|null $written_off_at
 * @property int $amount
 * @property string|null $description
 *
 * @property FinancePlan $model
 * @property string $typeAsText
 */
class FinancePlanBalanceArticle extends \yii\db\ActiveRecord
{
    const TYPE_FIXED_ASSERTS = 0;
    const TYPE_INTANGIBLE_ASSETS = 1;

    const STATUS_ON_BALANCE = 0;
    const STATUS_WRITTEN_OFF = 1;
    const STATUS_SOLD = 2;

    const STATUS_MAP = [
        self::STATUS_ON_BALANCE => 'На балансе',
        self::STATUS_WRITTEN_OFF => 'Списано',
        self::STATUS_SOLD => 'Продано',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_plan_balance_article';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                ['name', 'category', 'subcategory', 'count', 'purchased_at', 'amount'],
                'required',
                'message' => 'Необходимо заполнить.',
            ],
            [['name'], 'string', 'max' => 255],
            [['description'], 'string'],
            [['category', 'subcategory', 'count', 'useful_life_in_month'], 'integer'],
            [['category'], 'in', 'range' => array_keys(BalanceArticlesCategories::MAP)],
            [['subcategory'], 'in', 'range' => array_keys(BalanceArticlesSubcategories::MAP)],
            [
                ['amount'],
                'number',
                'numberPattern' => Yii::$app->params['numberPattern'],
                'max' => Yii::$app->params['maxCashSum'] * 100,
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
                'whenClient' => 'function(){}',
            ],
            [['amount'], 'compare', 'operator' => '>', 'compareValue' => 0, 'whenClient' => 'function(){}'],
            [['purchased_at'], 'safe'],
            [['purchased_at'], 'validatePurchasedAt'],
        ];
    }

    public function validatePurchasedAt($attribute)
    {
        $purchasedAt = Carbon::parse($this->$attribute);

        if ($purchasedAt->isFuture()) {
            $this->addError($attribute, 'Дата покупки не может быть больше текущей даты.');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_id' => 'Model ID',
            'type' => 'Категория',
            'name' => 'Наименование',
            'category' => 'Вид',
            'subcategory' => 'Подвид',
            'useful_life_in_month' => 'Срок полезного использования (мес.)',
            'count' => 'Количество',
            'purchased_at' => 'Дата приобретения',
            'amount' => 'Стоимость приобретения (руб.)',
            'description' => 'Описание',
            'status' => 'Status',
            'sold_at' => 'Sold At',
            'written_off_at' => 'Written Off At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(FinancePlan::class, ['id' => 'model_id']);
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->amount = round(TextHelper::parseMoneyInput($this->amount) * 100);

        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {
        if (strpos($this->purchased_at, '.') !== false) {
            $this->purchased_at = DateHelper::format($this->purchased_at, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE);
        }
        return parent::beforeSave($insert);
    }

    public function typeAsText()
    {
        return ($this->type == self::TYPE_INTANGIBLE_ASSETS) ? 'НМА' : 'ОС';
    }
}
