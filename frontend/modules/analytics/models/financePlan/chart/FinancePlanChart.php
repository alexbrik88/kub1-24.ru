<?php

namespace frontend\modules\analytics\models\financePlan\chart;


use frontend\modules\analytics\models\financePlan\FinancePlanGroup;
use frontend\modules\analytics\models\financePlan\FinancePlan;
use frontend\modules\analytics\models\financePlan\helpers\FinancePlanCalculatorHelper as calc;
use frontend\modules\analytics\models\financePlan\helpers\FinancePlanOddsCalculatorHelper as oddsCalc;
use frontend\modules\analytics\models\financePlan\helpers\FinancePlanGroupCalculatorHelper as groupCalc;

/**
 * Class FinancePlanChart
 */
class FinancePlanChart {
    
    const PLANS_TOTAL = 'total';
    const PLANS_ALL = 'all';

    private $_plansGroup;

    /**
     * @param FinancePlanGroup $planGroup
     */
    public function __construct(FinancePlanGroup $planGroup)
    {
        $this->_plansGroup = $planGroup;
    }

    /**
     * @return int
     */
    public function getGroupID()
    {
        return $this->_plansGroup->id;
    }

    /**
     * @return FinancePlan[]
     */
    public function getPlans()
    {
        return $this->_plansGroup->financePlans;
    }

    /**
     * @param $left
     * @param $right
     * @param int $offsetYear
     * @param string $offsetMonth
     * @return array
     * @throws \Exception
     */
    public function getPeriods($left, $right, $offsetYear = 0, $offsetMonth = "0")
    {
        $start = date_create_from_format('Y-m-d H:i:s', $this->_plansGroup->year_from.'-'.$this->_plansGroup->month_from.'-01'.'00:00:00');
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month")->modify("{$offsetMonth} month")->modify("-2 month");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = $curr->format('Ym');
            $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }

    public function getSingleChartData($periods, $customPlan) {

        $ret = [
            'revenue' => [],
            'net_total' => [],
            'balance' => []
        ];

        $revenueGroup = [];
        $netTotalGroup = [];
        $balanceGroup = [];

        $filterByPlan = ($customPlan > 0) ? $customPlan : null;

        foreach ($this->_plansGroup->financePlans as $plan) {

            if ($filterByPlan && $filterByPlan != $plan->id)
                continue;

            $revenueGroup[$plan->id] = calc::getRevenue($plan);
            $netTotalGroup[$plan->id] = calc::getNetProfit($plan);
            $balanceGroup[$plan->id] = oddsCalc::getOddsBalance($plan);
        }

        $groupExpenses = groupCalc::getExpenses($this->_plansGroup); // SUB GROUP EXPENSES
        $endYm = substr($this->_plansGroup->dateTill, 0, 4) . substr($this->_plansGroup->dateTill, 5, 2);
        $prevGroupExpenses = 0;

        foreach ($periods as $periodYm) {

            // revenue
            $sum = 0;
            foreach ($revenueGroup as $pid => $revenue) {
                $sum += $revenue[$periodYm] ?? 0;
            }
            $ret['revenue'][] = $sum;

            // net total
            $sum = 0;
            foreach ($netTotalGroup as $pid => $netTotal) {
                $sum += $netTotal[$periodYm] ?? 0;
            }
            $ret['net_total'][] = $sum;

            // balance
            $sum = 0;
            foreach ($balanceGroup as $pid => $balance) {
                $sum += $balance[$periodYm] ?? 0;
            }

            // SUB GROUP EXPENSES
            if (!$filterByPlan) {
                $sum -= ($groupExpenses[$periodYm] ?? 0) + $prevGroupExpenses;
                if ($periodYm >= $endYm) {
                    $prevGroupExpenses = 0;
                } else {
                    $prevGroupExpenses += $groupExpenses[$periodYm] ?? 0;
                }
            }

            $ret['balance'][] = $sum;
        }

        return $ret;
    }

    /**
     * @param $periods
     * @return array
     */
    public function getMultiChartSeries($periods)
    {
        $ret = [];
        foreach ($this->_plansGroup->financePlans as $idx => $plan) {
            $ret[$plan->id] = $this->getSingleChartData($periods, $plan->id);
            $ret[$plan->id]['name'] = $plan->name;
            $ret[$plan->id]['color'] = self::getMultiChartColor($idx);
        }

        return $ret;
    }

    /**
     * @param $idx
     * @return string
     */
    public static function getMultiChartColor($idx)
    {
        switch ($idx) {
            case 0:
                return '#0099FF';
            case 1:
                return '#f3b73b';
            case 2:
                return '#848dc9';
            case 3:
                return '#84c5c9';
            case 4:
                return '#c98484';
            case 5:
                return '#660033';
            case 6:
                return '#996633';
            default:
                return '#0000FF';
        }
    }
}