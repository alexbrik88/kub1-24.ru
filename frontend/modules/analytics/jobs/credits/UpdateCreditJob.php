<?php

namespace frontend\modules\analytics\jobs\credits;

use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\analytics\models\credits\UpdateCreditRepository;
use frontend\modules\analytics\models\credits\RepositoryFactory;
use Yii;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class UpdateCreditJob extends BaseObject implements JobInterface
{
    /**
     * @return int
     */
    public static function dispatchJob(): int
    {
        return Yii::$app->queue->push(new self());
    }

    /**
     * @inheritDoc
     */
    public function execute($queue)
    {
        /** @var Credit[] $credits */
        $credits = (new RepositoryFactory)
            ->createRepository(UpdateCreditRepository::class)
            ->getProvider()
            ->getModels();

        foreach ($credits as $credit) {
            $credit->save(false);
        }
    }
}
