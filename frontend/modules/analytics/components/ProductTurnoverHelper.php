<?php

namespace frontend\modules\analytics\components;

use Yii;
use common\models\Company;
use common\models\product\Product;
use common\models\product\ProductInitialBalance;
use common\models\product\ProductStore;
use common\models\product\ProductTurnover;
use frontend\models\Documents;
use yii\db\Query;

class ProductTurnoverHelper
{
    protected static $_data = [];

    public static $analyticsConnection = 'db2';

    public static $topProductLimit = 20;

    /**
     * {@inheritdoc}
     */
    public static function db()
    {
        return Yii::$app->{self::$analyticsConnection};
    }

    /**
     * {@inheritdoc}
     */
    public static function data($companyId, array $filter = null)
    {
        $group_id = $filter['group_id'] ?? null;
        $key = md5(json_encode($group_id));

        if (!isset(self::$_data['getData'][$companyId][$key])) {
            $query = (new \yii\db\Query())
            ->select([
                'turnover.type',
                'turnover.production_type',
                'turnover.year',
                'turnover.month',
                'turnover.date',
                'total' => 'SUM({{turnover}}.[[total_amount]])',
                'margin' => 'SUM({{turnover}}.[[margin]])',
            ])
            ->from(['turnover' => ProductTurnover::tableName()])->andWhere([
                'turnover.company_id' => $companyId,
                'turnover.is_invoice_actual' => 1,
                'turnover.is_document_actual' => 1,
            ])
            ->leftJoin(Product::tableName(), '{{product}}.[[id]] = {{turnover}}.[[product_id]]')
            ->andFilterWhere(['turnover.group_id' => $group_id])
            ->groupBy('[[type]], [[production_type]], [[year]], [[month]], [[date]] WITH ROLLUP');

            $data = [];

            foreach ($query->all(self::db()) as $row) {
                if ($row['type'] === null) {
                    $data['total'] = $row['total'];
                    $data['margin'] = 0;
                    continue;
                }
                $margin = $row['type'] == Documents::IO_TYPE_OUT &&
                          $row['production_type'] == Product::PRODUCTION_TYPE_GOODS ?
                          $row['margin'] :
                          0;
                if ($row['production_type'] === null) {
                    $data[$row['type']]['total'] = $row['total'];
                    $data[$row['type']]['margin'] = $margin;
                    continue;
                }
                if ($row['year'] === null) {
                    $data[$row['type']][$row['production_type']]['total'] = $row['total'];
                    $data[$row['type']][$row['production_type']]['margin'] = $margin;
                    continue;
                }
                if ($row['month'] === null) {
                    $data[$row['type']][$row['production_type']][$row['year']]['total'] = $row['total'];
                    $data[$row['type']][$row['production_type']][$row['year']]['margin'] = $margin;
                    continue;
                }
                if ($row['date'] === null) {
                    $data[$row['type']][$row['production_type']][$row['year']][$row['month']]['total'] = $row['total'];
                    $data[$row['type']][$row['production_type']][$row['year']][$row['month']]['margin'] = $margin;
                    continue;
                }
                $data[$row['type']][$row['production_type']][$row['year']][$row['month']][$row['date']]['total'] = $row['total'];
                $data[$row['type']][$row['production_type']][$row['year']][$row['month']][$row['date']]['margin'] = $margin;
            }

            self::$_data['getData'][$companyId][$key] = $data;
        }

        return self::$_data['getData'][$companyId][$key];
    }

    /**
     * {@inheritdoc}
     */
    public static function productData($companyId, array $filter = null)
    {
        $group_id = $filter['group_id'] ?? null;
        $key = md5(json_encode($group_id));

        if (!isset(self::$_data['getData'][$companyId][$key])) {
            $query = (new \yii\db\Query())
            ->select([
                'turnover.type',
                'turnover.year',
                'turnover.month',
                'turnover.date',
                'total' => 'SUM({{turnover}}.[[total_amount]])',
                'margin' => 'SUM({{turnover}}.[[margin]])',
            ])
            ->from(['turnover' => ProductTurnover::tableName()])
            ->andWhere([
                'turnover.company_id' => $companyId,
                'turnover.production_type' => Product::PRODUCTION_TYPE_GOODS,
                'turnover.is_invoice_actual' => 1,
                'turnover.is_document_actual' => 1,
            ])
            ->andWhere([
                'not',
                ['turnover.document_table' => 'initial_balance'],
            ])
            ->leftJoin(Product::tableName(), '{{product}}.[[id]] = {{turnover}}.[[product_id]]')
            ->andFilterWhere(['turnover.group_id' => $group_id])
            ->groupBy('[[type]], [[year]], [[month]], [[date]] WITH ROLLUP');

            $data = [];

            foreach ($query->all(self::db()) as $row) {
                if ($row['type'] === null) {
                    $data['total'] = $row['total'];
                    $data['margin'] = 0;
                    continue;
                }
                $margin = $row['type'] == Documents::IO_TYPE_OUT?
                          $row['margin'] :
                          0;
                if ($row['year'] === null) {
                    $data[$row['type']]['total'] = $row['total'];
                    $data[$row['type']]['margin'] = $margin;
                    continue;
                }
                if ($row['month'] === null) {
                    $data[$row['type']][$row['year']]['total'] = $row['total'];
                    $data[$row['type']][$row['year']]['margin'] = $margin;
                    continue;
                }
                if ($row['date'] === null) {
                    $data[$row['type']][$row['year']][$row['month']]['total'] = $row['total'];
                    $data[$row['type']][$row['year']][$row['month']]['margin'] = $margin;
                    continue;
                }
                $data[$row['type']][$row['year']][$row['month']][$row['date']]['total'] = $row['total'];
                $data[$row['type']][$row['year']][$row['month']][$row['date']]['margin'] = $margin;
            }

            self::$_data['getData'][$companyId][$key] = $data;
        }

        return self::$_data['getData'][$companyId][$key];
    }

    /**
     * {@inheritdoc}
     */
    public static function balanceData($companyId, array $filter = null)
    {
        $group_id = $filter['group_id'] ?? null;
        $key = md5(json_encode($group_id));

        if (!isset(self::$_data['balanceData'][$companyId][$key])) {
            $inOutQuery = (new \yii\db\Query)
                ->select([
                    'turnover.date',
                    'turnover.product_id',
                    'quantity' => 'SUM(IF({{turnover}}.[[type]] = 2, -{{turnover}}.[[quantity]], {{turnover}}.[[quantity]]))',
                ])
                ->from(['turnover' => ProductTurnover::tableName()])
                ->leftJoin(Product::tableName(), '{{product}}.[[id]] = {{turnover}}.[[product_id]]')
                ->andWhere([
                    'turnover.company_id' => $companyId,
                    'turnover.production_type' => Product::PRODUCTION_TYPE_GOODS,
                ])
                ->andWhere([
                    '>',
                    'product.price_for_buy_with_nds',
                    0,
                ])
                ->andWhere([
                    'or',
                    [
                        'and',
                        ['turnover.is_invoice_actual' => 1],
                        ['turnover.is_document_actual' => 1],
                        ['not', ['turnover.document_table' => 'initial_balance']],
                    ],
                    ['turnover.document_table' => 'initial_balance'],
                ])
                ->andFilterWhere(['product.group_id' => $group_id])
                ->groupBy('[[date]], [[product_id]]');

            $query = (new \yii\db\Query)
                ->select([
                    'date',
                    'balance' => 'SUM({{t}}.[[quantity]] * {{product}}.[[price_for_buy_with_nds]])',
                ])
                ->from(['t' => $inOutQuery])
                ->leftJoin(Product::tableName(), '{{product}}.[[id]] = {{t}}.[[product_id]]')
                ->groupBy('[[date]] ASC');

            $data = [];
            foreach ($query->all(self::db()) as $row) {
                $data[$row['date']] = round($row['balance']);
            }

            self::$_data['balanceData'][$companyId][$key] = $data;
        }

        return self::$_data['balanceData'][$companyId][$key];

    }

    /**
     * {@inheritdoc}
     */
    public static function analyticsProductDashboard4blocks(int $companyId, array $filter = null) : array
    {
        $thisDay = date_create();
        $lastDay = date_create('-1 day');
        $thisMonth = date_create('first day of');
        $lastMonth = date_create('first day of -1 month');

        $sellsToday = self::sellsPerDate($companyId, $thisDay, $filter);
        $buysToday = self::buysPerDate($companyId, $thisDay, $filter);
        $marginToday = self::marginPerDate($companyId, $thisDay, $filter);
        $balanceToday = self::balanceAtDate($companyId, $thisDay, $filter);

        $sellsYesterday = self::sellsPerDate($companyId, $lastDay, $filter);
        $buysYesterday = self::buysPerDate($companyId, $lastDay, $filter);
        $marginYesterday = self::marginPerDate($companyId, $lastDay, $filter);
        $balanceYesterday = self::balanceAtDate($companyId, $lastDay, $filter);

        $sellsThisMonth = self::sellsPerMonth($companyId, $thisMonth, $filter);
        $buysThisMonth = self::buysPerMonth($companyId, $thisMonth, $filter);
        $marginThisMonth = self::marginPerMonth($companyId, $thisMonth, $filter);
        $balanceThisMonth = self::balanceAtDate($companyId, $thisMonth, $filter);

        $sellsLastMonth = self::sellsPerMonth($companyId, $lastMonth, $filter);
        $buysLastMonth = self::buysPerMonth($companyId, $lastMonth, $filter);
        $marginLastMonth = self::marginPerMonth($companyId, $lastMonth, $filter);
        $balanceLastMonth = self::balanceAtDate($companyId, $lastMonth, $filter);

        return [
            'sellsToday' => $sellsToday,
            'buysToday' => $buysToday,
            'marginToday' => $marginToday,
            'balanceToday' => $balanceToday,
            'sellsYesterday' => $sellsYesterday,
            'buysYesterday' => $buysYesterday,
            'marginYesterday' => $marginYesterday,
            'balanceYesterday' => $balanceYesterday,
            'sellsThisMonth' => $sellsThisMonth,
            'buysThisMonth' => $buysThisMonth,
            'marginThisMonth' => $marginThisMonth,
            'balanceThisMonth' => $balanceThisMonth,
            'sellsLastMonth' => $sellsLastMonth,
            'buysLastMonth' => $buysLastMonth,
            'marginLastMonth' => $marginLastMonth,
            'balanceLastMonth' => $balanceLastMonth,
        ];
    }

    /**
     * @param array $dates
     * @param int $groupId
     * @param string $period
     * @return array
     * @throws \yii\base\ErrorException
     */
    public static function incomeOutcomeMarginSeriesData($companyId, $dateArray, array $filter = null, $period = "months")
    {
        $perMonth = $period == "months";
        $incomeFact = [];
        $outcomeFact = [];
        $marginFact = [];

        $turnoverData = self::productData($companyId, $filter);
        $inData = $turnoverData[Documents::IO_TYPE_IN] ?? [];
        $outData = $turnoverData[Documents::IO_TYPE_OUT] ?? [];

        foreach ($dateArray as $date) {
            $d = date_create($date['from']);
            $year = (int) $d->format('Y');
            $month = (int) $d->format('m');

            $income = $perMonth ? ($inData[$year][$month]['total'] ?? 0) : ($inData[$year][$month][$date['from']]['total'] ?? 0);
            $outcome = $perMonth ? ($outData[$year][$month]['total'] ?? 0) : ($outData[$year][$month][$date['from']]['total'] ?? 0);
            $margin = $perMonth ? ($outData[$year][$month]['margin'] ?? 0) : ($outData[$year][$month][$date['from']]['margin'] ?? 0);

            $incomeFact[] = $income / 100;
            $outcomeFact[] = $outcome / 100;
            $marginFact[] = $margin / 100;
        }

        return [
            'incomeFact' => $incomeFact,
            'outcomeFact' => $outcomeFact,
            'marginFact' => $marginFact,
        ];
    }

    /**
     * @param array $dates
     * @param int $groupId
     * @param string $period
     * @return array
     * @throws \yii\base\ErrorException
     */
    public static function balanceSeriesData($companyId, $dateArray, array $filter = null)
    {
        $result = [];

        foreach ($dateArray as $date) {
            $d = date_create($date['from']);
            $result[] = self::balanceAtDate($companyId, $d, $filter) / 100;
        }

        return [
            'balanceFact' => $result,
        ];
    }

    public static function topProductSeriesData(int $companyId, $yearMonth, array $filter = null)
    {
        $group_id = $filter['group_id'] ?? null;
        $key = md5(json_encode([$yearMonth, $group_id]));

        if (!isset(self::$_data['topProductSeriesData'][$companyId][$key])) {
            $db = self::db();
            $db->open();
            $result = [
                'totalAmount' => 0,
                'topRevenue' => [],
                'topMargin' => []
            ];

            if (strlen($yearMonth) === 6) {
                $dateStart = date_create_from_format('Ymd', $yearMonth . '01');
                $dateEnd = (clone $dateStart)->modify('last day of');
            } elseif (strlen($yearMonth) === 4) {
                $dateStart =  date_create_from_format('Ymd', $yearMonth . '0101');
                $dateEnd   =  date_create_from_format('Ymd', $yearMonth . '1231');
            } else {
                $dateStart = $dateEnd = new DateTime;
            }

            $query = (new \yii\db\Query())
                ->select([
                    'turnover.product_id',
                    'quantity' => 'SUM({{turnover}}.[[quantity]])',
                    'total' => 'SUM({{turnover}}.[[total_amount]])',
                    'margin' => 'SUM({{turnover}}.[[margin]])',
                ])
                ->from(['turnover' => ProductTurnover::tableName()])
                ->leftJoin(Product::tableName(), '{{product}}.[[id]] = {{turnover}}.[[product_id]]')
                ->andWhere([
                    'turnover.company_id' => $companyId,
                    'turnover.type' => 2,
                    'turnover.production_type' => 1,
                    'turnover.is_invoice_actual' => 1,
                    'turnover.is_document_actual' => 1,
                ])
                ->andWhere([
                    'not',
                    ['turnover.document_table' => 'initial_balance'],
                ])
                ->andWhere([
                    'between',
                    'turnover.date',
                    $dateStart->format('Y-m-d'),
                    $dateEnd->format('Y-m-d'),
                ])
                ->andFilterWhere(['turnover.group_id' => $group_id])
                ->groupBy('{{turnover}}.[[product_id]]');

            $sql = $query->createCommand()->rawSql;

            $comand = $db->createCommand("DROP TEMPORARY TABLE IF EXISTS __turnover");
            $comand->execute();
            $comand = $db->createCommand("CREATE TEMPORARY TABLE __turnover {$sql}");
            $comand->execute();

            $result['totalAmount'] = ((int) (new Query)
                ->select('SUM([[total]])')
                ->from('__turnover')
                ->scalar($db)) / 100;

            $topRevenue = (new Query)
                ->select([
                    'product.id',
                    'product.title',
                    'turnover.quantity',
                    'turnover.total',
                    'turnover.margin',
                ])
                ->from(['turnover' => '__turnover'])
                ->leftJoin(Product::tableName(), '{{product}}.[[id]] = {{turnover}}.[[product_id]]')
                ->orderBy(['total'=> SORT_DESC])
                ->limit(self::$topProductLimit)
                ->all($db);

            $topMargin = (new Query)
                ->select([
                    'product.id',
                    'product.title',
                    'turnover.quantity',
                    'turnover.total',
                    'turnover.margin',
                ])
                ->from(['turnover' => '__turnover'])
                ->leftJoin(Product::tableName(), '{{product}}.[[id]] = {{turnover}}.[[product_id]]')
                ->orderBy(['margin'=> SORT_DESC])
                ->limit(self::$topProductLimit)
                ->all($db);

            $db->createCommand()->execute("DROP TEMPORARY TABLE IF EXISTS __turnover");

            foreach ($topRevenue as $key => $p) {
                $result['topRevenue'][$p['id']] = [
                    'name' => $p['title'],
                    'revenue' => $p['total'] / 100,
                    'margin' => $p['margin'] / 100,
                    'quantity' => $p['quantity'],
                    'marginPercent' => $p['total'] ? 100 * $p['margin'] / $p['total'] : 0,
                ];
            }

            foreach ($topMargin as $key => $p) {
                $result['topMargin'][$p['id']] = [
                    'name' => $p['title'],
                    'revenue' => $p['total'] / 100,
                    'margin' => $p['margin'] / 100,
                    'quantity' => $p['quantity'],
                    'marginPercent' => $p['total'] ? 100 * $p['margin'] / $p['total'] : 0,
                ];
            }

            self::$_data['topProductSeriesData'][$companyId][$key] = $result;
        }

        return self::$_data['topProductSeriesData'][$companyId][$key];
    }

    public static function buysPerDate($companyId, \DateTime $d, $filter = null) : int
    {
        $data = self::productData($companyId, $filter);

        return $data[Documents::IO_TYPE_IN][$d->format('Y')][$d->format('n')][$d->format('Y-m-d')]['total'] ?? 0;
    }

    public static function sellsPerDate($companyId, \DateTime $d, $filter = null) : int
    {
        $data = self::productData($companyId, $filter);

        return $data[Documents::IO_TYPE_OUT][$d->format('Y')][$d->format('n')][$d->format('Y-m-d')]['total'] ?? 0;
    }

    public static function marginPerDate($companyId, \DateTime $d, $filter = null) : int
    {
        $data = self::productData($companyId, $filter);

        return $data[Documents::IO_TYPE_OUT][$d->format('Y')][$d->format('n')][$d->format('Y-m-d')]['margin'] ?? 0;
    }

    public static function buysPerMonth($companyId, \DateTime $d, $filter = null) : int
    {
        $data = self::productData($companyId, $filter);

        return $data[Documents::IO_TYPE_IN][$d->format('Y')][$d->format('n')]['total'] ?? 0;
    }

    public static function sellsPerMonth($companyId, \DateTime $d, $filter = null) : int
    {
        $data = self::productData($companyId, $filter);

        return $data[Documents::IO_TYPE_OUT][$d->format('Y')][$d->format('n')]['total'] ?? 0;
    }

    public static function marginPerMonth($companyId, \DateTime $d, $filter = null) : int
    {
        $data = self::productData($companyId, $filter);

        return $data[Documents::IO_TYPE_OUT][$d->format('Y')][$d->format('n')]['margin'] ?? 0;
    }

    public static function buysAtDate($companyId, \DateTime $date, $filter = null) : int
    {
        $data = self::productData($companyId, $filter)[Documents::IO_TYPE_IN];
        $Y = (int) $date->format('Y');
        $M = (int) $date->format('n');
        $D = (int) $date->format('j');
        $result = 0;

        foreach ($data as $y => $yearData) {
            if ($y == 'total') continue;
            if ($y < $Y) {
                $result += $yearData['total'] ?? 0;
            } elseif ($y == $Y) {
                foreach ($yearData as $m => $monthData) {
                    if ($m == 'total') continue;
                    if ($m < $M) {
                        $result += $monthData['total'] ?? 0;
                    } elseif ($m == $M) {
                        foreach ($monthData as $d => $dayData) {
                            if ($d == 'total') continue;
                            $d = (int) substr($d, 8);
                            if ($d < $D) {
                                $result += $dayData['total'] ?? 0;
                            }
                        }
                    }
                }
            }
        }

        return (int) round($result);
    }

    public static function sellsAtDate($companyId, \DateTime $date, $filter = null) : int
    {
        $data = self::productData($companyId, $filter)[Documents::IO_TYPE_OUT];
        $Y = (int) $date->format('Y');
        $M = (int) $date->format('n');
        $D = (int) $date->format('j');
        $result = 0;

        foreach ($data as $y => $yearData) {
            if ($y == 'total') continue;
            if ($y < $Y) {
                $result += $yearData['total'] ?? 0;
            } elseif ($y == $Y) {
                foreach ($yearData as $m => $monthData) {
                    if ($m == 'total') continue;
                    if ($m < $M) {
                        $result += $monthData['total'] ?? 0;
                    } elseif ($m == $M) {
                        foreach ($monthData as $d => $dayData) {
                            if ($d == 'total') continue;
                            $d = (int) substr($d, 8);
                            if ($d < $D) {
                                $result += $dayData['total'] ?? 0;
                            }
                        }
                    }
                }
            }
        }

        return (int) round($result);
    }

    public static function balanceAtDate($companyId, \DateTime $d, array $filter = null) : int
    {
        $data = self::balanceData($companyId, $filter);

        $dataAtDate = array_filter($data, function ($val, $key) use ($d) {
            $date = date_create_from_format('Y-m-d|', $key);

            return $date && $date <= $d;
        }, ARRAY_FILTER_USE_BOTH);

        return (int) round(array_sum($dataAtDate));
    }
}
