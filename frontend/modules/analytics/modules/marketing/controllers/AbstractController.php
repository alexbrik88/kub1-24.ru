<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\controllers\WebTrait;
use frontend\rbac\permissions\BusinessAnalytics;
use yii\filters\AccessControl;
use yii\filters\AjaxFilter;

abstract class AbstractController extends FrontendController
{
    use WebTrait;

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                            'job-status',
                            'test',
                            'xls',
                        ],
                        'roles' => [BusinessAnalytics::INDEX],
                        'matchCallback' => function() {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_MARKETING);
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'import',
                            'connect',
                            'set-auto-import',
                            'disconnect',
                            'token',
                            'auth',
                        ],
                        'roles' => [BusinessAnalytics::ADMIN],
                        'matchCallback' => function() {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_MARKETING);
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'credentials',
                        ],
                        'roles' => [BusinessAnalytics::ADMIN],
                        'matchCallback' => function() {
                            if (!YII_DEBUG) {
                                return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_MARKETING);
                            }

                            return true;
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'group-create',
                            'group-update',
                            'group-delete',
                            'group-get-campaigns-list',
                            'index-items'
                        ],
                        'roles' => [BusinessAnalytics::INDEX],
                        'matchCallback' => function() {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_MARKETING);
                        },
                    ],
                ],
            ],
            'ajax' => [
                'class' => AjaxFilter::class,
                'only' => [
                    'connect',
                    'import',
                    'set-auto-import',
                    'job-status',
                ],
            ],
        ];
    }
}
