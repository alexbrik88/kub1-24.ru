<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use common\models\employee\Employee;
use common\modules\marketing\components\YandexDirectComponent;
use common\modules\marketing\models\MarketingChannel;
use common\modules\marketing\commands\YandexDirectImportCommand;
use frontend\modules\analytics\modules\marketing\models\YandexDirectTokenForm;
use yii\helpers\Url;

class YandexDirectController extends AbstractController
{
    /** @var string */
    private const DEFAULT_SORTING_KEY = 'yandexDirectDefaultSorting';

    /** @var string */
    private const REDIRECT_URL = '/analytics/marketing/yandex-direct';

    /**
     * @inheritDoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'sortingKey' => self::DEFAULT_SORTING_KEY,
                'channelType' => MarketingChannel::YANDEX_AD,
                'commandClass' => YandexDirectImportCommand::class,
            ],
            'xls' => [
                'class' => XlsAction::class,
                'sortingKey' => self::DEFAULT_SORTING_KEY,
                'channelType' => MarketingChannel::YANDEX_AD,
            ],
            'import' => [
                'class' => ImportAction::class,
                'redirectUrl' => Url::to([self::REDIRECT_URL]),
                'commandClass' => YandexDirectImportCommand::class,
            ],
            'token' => [
                'class' => TokenAction::class,
                'redirectUrl' => Url::to([self::REDIRECT_URL]),
                'formClass' => YandexDirectTokenForm::class,
            ],
            'disconnect' => [
                'class' => DisconnectAction::class,
                'commandClass' => YandexDirectImportCommand::class,
                'redirectUrl' => Url::to([self::REDIRECT_URL]),
                'integrationName' => Employee::INTEGRATION_YANDEX_DIRECT,
                'successMessage' => 'Интеграция с Яндекс.Директ отключена.',
                'errorMessage' => 'Произошла ошибка при отключении интеграции с Яндекс.Директ.',
            ],
            'job-status' => [
                'class' => JobStatusAction::class,
                'commandClass' => YandexDirectImportCommand::class,
            ],
            'set-auto-import' => [
                'class' => SetAutoImportAction::class,
                'commandClass' => YandexDirectImportCommand::class,
            ],
            'credentials' => [
                'class' => CredentialsAction::class,
                'integrationName' => Employee::INTEGRATION_YANDEX_DIRECT,
            ],
            'auth' => [
                'class' => AuthAction::class,
                'componentClass' => YandexDirectComponent::class,
            ],
            // Groups
            'group-create' => [
                'class' => GroupCreateAction::class,
                'channelType' => MarketingChannel::YANDEX_AD,
            ],
            'group-update' => [
                'class' => GroupUpdateAction::class,
                'channelType' => MarketingChannel::YANDEX_AD,
            ],
            'group-delete' => [
                'class' => GroupDeleteAction::class,
                'channelType' => MarketingChannel::YANDEX_AD,
            ],
            'group-get-campaigns-list' => [
                'class' => GroupGetCampaignsListAction::class,
                'channelType' => MarketingChannel::YANDEX_AD,
            ],
            'index-items' => [
                'class' => IndexItemsAction::class,
                'channelType' => MarketingChannel::YANDEX_AD,
                'sortingKey' => self::DEFAULT_SORTING_KEY,
            ],
        ];
    }
}
