<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use common\modules\import\components\AuthComponentInterface;
use frontend\controllers\ActionInterface;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\web\Response;

class AuthAction extends Action implements ActionInterface
{
    /**
     * @var AuthComponentInterface
     */
    public $componentClass;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!is_a($this->componentClass, AuthComponentInterface::class, true)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function run(): Response
    {
        /** @var AuthComponentInterface $component */
        $component = call_user_func([$this->componentClass, 'getInstance']); // TODO

        return $this->controller->redirect($component->getAuthorizationUrl());
    }
}
