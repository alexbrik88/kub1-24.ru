<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use common\models\employee\Employee;
use Yii;

trait DefaultSortingTrait
{
    /**
     * @param string $key
     * @return string
     */
    public function getDefaultSorting(string $key): string
    {
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $profile = $user->currentEmployeeCompany->company->googleAnalyticsProfile;
        $defaultSorting = Yii::$app->session->get($key);

        if (isset(Yii::$app->request->get()['defaultSorting'])) {
            $defaultSorting = Yii::$app->request->get()['defaultSorting'];
            Yii::$app->session->set($key, $defaultSorting);
        }

        if (!$defaultSorting) {
            if (!$profile || count($profile->goals) === 0) {
                $defaultSorting = 'clicks';
            } else {
                $defaultSorting = "goal_{$profile->goals[0]->external_id}_total";
            }
        } else {
            if (strpos($defaultSorting, 'goal_') !== false) {
                $defaultSortingGoalExists = false;
                $defaultSortingArr = explode('_', $defaultSorting);

                if ($profile) {
                    $goals = $profile->goals;

                    foreach ($goals as $goal) {
                        if ($goal->external_id == $defaultSortingArr[1]) {
                            $defaultSortingGoalExists = true;
                            break;
                        }
                    }
                }

                if (!$defaultSortingGoalExists) {
                    $defaultSorting = 'clicks';
                }
            }
        }
        
        return $defaultSorting;
    }
}
