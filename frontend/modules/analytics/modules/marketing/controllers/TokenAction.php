<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use common\modules\import\models\TokenFormInterface;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\web\Response;

class TokenAction extends AbstractAction
{
    /**
     * @var string|TokenFormInterface
     */
    public $formClass;

    /**
     * @var string
     */
    public $redirectUrl;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!isset($this->redirectUrl, $this->formClass)) {
            throw new InvalidConfigException();
        }

        if (!is_a($this->formClass, TokenFormInterface::class, true)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @return Response
     */
    public function run(): Response
    {
        /** @var TokenFormInterface|Model $form */
        $form = $this->formClass::createTokenForm($this->user->identity->company);
        $form->load($this->request->get(), '');

        if ($form->validate() && $form->updateToken()) {
            $this->session->setFlash('success', 'Интеграция подключена.');
        } elseif ($form->hasErrors('code')) {
            $this->session->setFlash('error', $form->getFirstError('code'));
        }

        return $this->controller->redirect($this->redirectUrl);
    }
}
