<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use common\models\employee\Employee;
use common\modules\marketing\components\VkAdsComponent;
use common\modules\marketing\models\MarketingChannel;
use common\modules\marketing\commands\VkAdsImportCommand;
use frontend\modules\analytics\modules\marketing\models\VkAdsSettingsForm;
use frontend\modules\analytics\modules\marketing\models\VkAdsTokenForm;
use yii\helpers\Url;

class VkAdsController extends AbstractController
{
    /** @var string */
    private const DEFAULT_SORTING_KEY = 'VkAdsDefaultSorting';

    /** @var string */
    private const REDIRECT_URL = '/analytics/marketing/vk-ads';

    /**
     * @inheritDoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'sortingKey' => self::DEFAULT_SORTING_KEY,
                'channelType' => MarketingChannel::VKONTAKTE,
                'commandClass' => VkAdsImportCommand::class,
            ],
            'xls' => [
                'class' => XlsAction::class,
                'sortingKey' => self::DEFAULT_SORTING_KEY,
                'channelType' => MarketingChannel::VKONTAKTE,
            ],
            'import' => [
                'class' => ImportAction::class,
                'redirectUrl' => Url::to([self::REDIRECT_URL]),
                'commandClass' => VkAdsImportCommand::class,
            ],
            'token' => [
                'class' => TokenAction::class,
                'redirectUrl' => Url::to([self::REDIRECT_URL]),
                'formClass' => VkAdsTokenForm::class,
            ],
            'connect' => [
                'class' => ConnectAction::class,
                'formClass' => VkAdsSettingsForm::class,
            ],
            'disconnect' => [
                'class' => DisconnectAction::class,
                'commandClass' => VkAdsImportCommand::class,
                'redirectUrl' => Url::to([self::REDIRECT_URL]),
                'integrationName' => Employee::INTEGRATION_VK,
                'successMessage' => 'Интеграция с ВКонтакте отключена.',
                'errorMessage' => 'Произошла ошибка при отключении интеграции с ВКонтакте.',
            ],
            'job-status' => [
                'class' => JobStatusAction::class,
                'commandClass' => VkAdsImportCommand::class,
            ],
            'set-auto-import' => [
                'class' => SetAutoImportAction::class,
                'commandClass' => VkAdsImportCommand::class,
            ],
            'credentials' => [
                'class' => CredentialsAction::class,
                'integrationName' => Employee::INTEGRATION_VK,
            ],
            'auth' => [
                'class' => AuthAction::class,
                'componentClass' => VkAdsComponent::class,
            ],
            // Groups
            'group-create' => [
                'class' => GroupCreateAction::class,
                'channelType' => MarketingChannel::VKONTAKTE,
            ],
            'group-update' => [
                'class' => GroupUpdateAction::class,
                'channelType' => MarketingChannel::VKONTAKTE,
            ],
            'group-delete' => [
                'class' => GroupDeleteAction::class,
                'channelType' => MarketingChannel::VKONTAKTE,
            ],
            'group-get-campaigns-list' => [
                'class' => GroupGetCampaignsListAction::class,
                'channelType' => MarketingChannel::VKONTAKTE,
            ],
            'index-items' => [
                'class' => IndexItemsAction::class,
                'channelType' => MarketingChannel::VKONTAKTE,
                'sortingKey' => self::DEFAULT_SORTING_KEY,
            ],
        ];
    }
}
