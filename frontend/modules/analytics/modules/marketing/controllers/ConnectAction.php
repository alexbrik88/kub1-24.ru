<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use frontend\modules\analytics\modules\marketing\models\SettingsFormInterface;
use yii\base\InvalidConfigException;
use yii\web\Response;

class ConnectAction extends AbstractAction
{
    /**
     * @var string|SettingsFormInterface
     */
    public $formClass;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!isset($this->formClass) || !is_a($this->formClass, SettingsFormInterface::class, true)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function run(): Response
    {
        $form = $this->formClass::createSettingsForm($this->user->identity->company);

        if ($form->load($this->request->post()) && $form->validate()) {
            if ($form->updateSettings()) {
                return $this->controller->runAction('import');
            }
        }

        $this->response->content = $this->controller->renderAjax('connect_modal', compact('form'));

        return $this->response;
    }
}
