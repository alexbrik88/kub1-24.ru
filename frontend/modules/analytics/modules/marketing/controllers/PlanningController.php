<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use common\components\filters\AccessControl;
use common\models\employee\Employee;
use common\models\marketing\MarketingPlan;
use common\models\marketing\MarketingPlanItem;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\controllers\WebTrait;
use frontend\modules\analytics\modules\marketing\models\MarketingPlanningForm;
use frontend\modules\analytics\modules\marketing\models\UpdatePlanningMarketingForm;
use frontend\modules\analytics\modules\marketing\models\PlanningMarketingSearch;
use frontend\rbac\permissions\BusinessAnalytics;
use InvalidArgumentException;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class PlanningController extends FrontendController
{
    use WebTrait;

    public $layout = 'planning';

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function () {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_MARKETING);
                        },
                    ],
                ],
            ],
        ];
    }

    public function actionIndex(): string {
        $user = Yii::$app->user->identity;
        $searchModel = new PlanningMarketingSearch($user);
        $dataProvider = $searchModel->search($this->request->get());
        $form = new MarketingPlanningForm(new MarketingPlan(), $user);

        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 139, true);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'model' => $form,
        ]);
    }

    public function actionView(int $id, int $activeTab = null): string
    {
        $user = Yii::$app->user->identity;
        $model = $this->findModel($id);
        $form = new MarketingPlanningForm($model, $user);
        $data = [];
        if (count($form->channels) === 1) {
            $activeTab = $form->channels[0];
        }

        if (count($form->channels) > 1) {
            $data = (new PlanningMarketingSearch($user))->searchTotal($model->id);
        }

        return $this->render('view', [
            'model' => $model,
            'updateForm' => $form,
            'user' => $user,
            'activeTab' => $activeTab,
            'data' => $data,
        ]);
    }

    public function actionCreate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* @var $employee Employee */
        $employee = Yii::$app->user->identity;
        $model = new MarketingPlanningForm(new MarketingPlan(), $employee);
        $model->load($this->request->post());
        if ($this->request->post('ajax')) {
            return ActiveForm::validate($model);
        }

        if ($model->save()) {
            \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 140, true);

            return $this->redirect(['view', 'id' => $model->getMarketingPlan()->id]);
        }

        Yii::$app->session->setFlash('success', 'Не удалось сохранить прогноз по маркетингу');

        return $this->redirect(['index']);
    }

    public function actionUpdate(int $id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);
        /* @var $employee Employee */
        $employee = Yii::$app->user->identity;
        $model = new MarketingPlanningForm($model, $employee);
        $model->load($this->request->post());
        if ($this->request->post('ajax')) {
            return ActiveForm::validate($model);
        }

        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'Прогноз по маркетингу сохранен');
            return $this->redirect(['view', 'id' => $id]);
        }

        Yii::$app->session->setFlash('success', 'Не удалось сохранить прогноз по маркетингу');

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionUpdateChannelPlanning(int $id, int $activeChannel)
    {
        $activeChannel = $this->request->post('activeChannel', $activeChannel);
        $model = $this->findModel($id);
        $items = $this->request->post('UpdatePlanningMarketingForm')['items'] ?? null;
        $averageCheck = $this->request->post('UpdatePlanningMarketingForm')['average_check'] ?? null;
        $churnRate = $this->request->post('UpdatePlanningMarketingForm')['churn_rate'] ?? null;
        $repeatedPurchaseCount = $this->request->post('UpdatePlanningMarketingForm')['repeated_purchases_count'] ?? null;
        if ($this->request->isGet) {
            return $this->render('update-channel-planning', [
                'marketingPlan' => $model,
                'user' => Yii::$app->user->identity,
                'channel' => $activeChannel,
            ]);
        }

        $transaction = Yii::$app->db->beginTransaction();
        foreach ($items as $channel => $data) {
            $form = new UpdatePlanningMarketingForm($model, $channel);
            $form->items = $data;
            $form->averageCheck = $averageCheck;
            $form->churnRate = $churnRate;
            $form->repeatedPurchasesCount = $repeatedPurchaseCount;
            if (!$form->update()) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', 'Произошла ошибка при обновлении прогноза по маркетингу.');

                return $this->redirect(['update-channel-planning', 'id' => $model->id, 'activeChannel' => $activeChannel]);
            }
        }

        $transaction->commit();

        return $this->redirect(['view', 'id' => $model->id, 'activeTab' => $activeChannel]);
    }

    public function actionReloadChannelPlanningTable(int $id, int $channel): ?string {
        $result = [];
        $model = $this->findModel($id);
        $autofill = Yii::$app->request->post('autofill');
        if ($channel === MarketingPlan::REPEATED_PURCHASES) {
            $items = Yii::$app->request->post('UpdatePlanningMarketingForm')['items'] ?? null;
        } else {
            $items = Yii::$app->request->post('UpdatePlanningMarketingForm')['items'][$channel] ?? null;
        }

        $averageCheck = $this->request->post('UpdatePlanningMarketingForm')['average_check'] ?? null;
        $churnRate = $this->request->post('UpdatePlanningMarketingForm')['churn_rate'] ?? null;
        $repeatedPurchaseCount = $this->request->post('UpdatePlanningMarketingForm')['repeated_purchases_count'] ?? null;
        $form = new UpdatePlanningMarketingForm($model, $channel);
        $form->averageCheck = $averageCheck;
        $form->churnRate = $churnRate;
        $form->repeatedPurchasesCount = $repeatedPurchaseCount;
        if ($autofill !== null) {
            $form->items = $items;
            $form->calculateAutoFill($autofill, $channel);
        } else {
            if ($items === null) {
                return null;
            }

            $form->items = $items;
        }

        if ($channel === MarketingPlan::REPEATED_PURCHASES) {
            $channelsCount = count($items);
            foreach ($form->items as $itemsByChannel) {
                $result[] = $this->calculateItemsByChannel($itemsByChannel, $form);
            }

            $totalResult = [];
            foreach ($result as $itemsByChannel) {
                foreach ($itemsByChannel as $items) {
                    if (!isset($totalResult["{$items['month']}{$items['year']}"])) {
                        $totalResult["{$items['month']}{$items['year']}"] = [
                            'month' => $items['month'],
                            'year' => $items['year'],
                            'budget_amount' => $items['budget_amount'],
                            'marketing_service_amount' => $items['marketing_service_amount'],
                            'additional_expenses_amount' => $items['additional_expenses_amount'],
                            'total_additional_expenses_amount' => $items['total_additional_expenses_amount'],
                            'additional_expenses_amount_for_one_sale' => $items['additional_expenses_amount_for_one_sale'],
                            'clicks_count' => $items['clicks_count'],
                            'registration_count' => $items['registration_count'],
                            'active_users_count' => $items['active_users_count'],
                            'purchases_count' => $items['purchases_count'],
                            'sale_amount' => $items['sale_amount'],
                            'average_check' => $items['average_check'],
                            'proceeds_from_new_amount' => $items['proceeds_from_new_amount'],
                            'churn_rate' => $items['churn_rate'],
                            'repeated_purchases_amount' => $items['repeated_purchases_amount'],
                            'total_proceeds_amount' => $items['total_proceeds_amount'],
                            'cumulative_financial_amount' => $items['cumulative_financial_amount'],
                            'total_purchases_count' => $items['total_purchases_count'],
                            'repeated_purchases_count' => $items['repeated_purchases_count'],
                            'salesperson_salary_amount' => $items['salesperson_salary_amount'],
                        ];
                    } else {
                        $totalResult["{$items['month']}{$items['year']}"]['budget_amount'] += $items['budget_amount'];
                        $totalResult["{$items['month']}{$items['year']}"]['marketing_service_amount'] += $items['marketing_service_amount'];
                        $totalResult["{$items['month']}{$items['year']}"]['additional_expenses_amount'] += $items['additional_expenses_amount'];
                        $totalResult["{$items['month']}{$items['year']}"]['total_additional_expenses_amount'] += $items['total_additional_expenses_amount'];
                        $totalResult["{$items['month']}{$items['year']}"]['additional_expenses_amount_for_one_sale'] += $items['additional_expenses_amount_for_one_sale'];
                        $totalResult["{$items['month']}{$items['year']}"]['clicks_count'] += $items['clicks_count'];
                        $totalResult["{$items['month']}{$items['year']}"]['registration_count'] += $items['registration_count'];
                        $totalResult["{$items['month']}{$items['year']}"]['active_users_count'] += $items['active_users_count'];
                        $totalResult["{$items['month']}{$items['year']}"]['purchases_count'] += $items['purchases_count'];
                        $totalResult["{$items['month']}{$items['year']}"]['sale_amount'] += $items['sale_amount'];
                        $totalResult["{$items['month']}{$items['year']}"]['average_check'] += $items['average_check'];
                        $totalResult["{$items['month']}{$items['year']}"]['proceeds_from_new_amount'] += $items['proceeds_from_new_amount'];
                        $totalResult["{$items['month']}{$items['year']}"]['churn_rate'] += $items['churn_rate'];
                        $totalResult["{$items['month']}{$items['year']}"]['repeated_purchases_amount'] += $items['repeated_purchases_amount'];
                        $totalResult["{$items['month']}{$items['year']}"]['total_proceeds_amount'] += $items['total_proceeds_amount'];
                        $totalResult["{$items['month']}{$items['year']}"]['cumulative_financial_amount'] += $items['cumulative_financial_amount'];
                        $totalResult["{$items['month']}{$items['year']}"]['total_purchases_count'] += $items['total_purchases_count'];
                        $totalResult["{$items['month']}{$items['year']}"]['repeated_purchases_count'] += $items['repeated_purchases_count'];
                        $totalResult["{$items['month']}{$items['year']}"]['salesperson_salary_amount'] += $items['salesperson_salary_amount'];
                    }
                }
            }

            $i = 0;
            $prevCumulativeFinancialAmount = 0;
            foreach ($totalResult as $key => $totalResultItem) {
                $totalResult[$key]['average_check'] = $totalResultItem['average_check'] / $channelsCount;
                $totalResult[$key]['churn_rate'] = $totalResultItem['churn_rate'] / $channelsCount;
                $totalResult[$key]['financial_result'] = $totalResultItem['total_proceeds_amount'] - $totalResultItem['budget_amount'] - $totalResultItem['total_additional_expenses_amount'];
                $totalResult[$key]['cumulative_financial_amount'] = $prevCumulativeFinancialAmount + $totalResult[$key]['financial_result'];
                $prevCumulativeFinancialAmount = $totalResult[$key]['cumulative_financial_amount'];
                if ($i === 0) {
                    $totalResult[$key]['repeated_purchases_count'] = $totalResultItem['repeated_purchases_count'] / $channelsCount;
                }

                $i++;
            }

            $form->items = $totalResult;

            return $this->renderAjax('partial/repeated_purchases_update_table', [
                'model' => $form,
                'user' => Yii::$app->user->identity,
                'reloadItems' => false,
            ]);
        }

        $form->items = $this->calculateItemsByChannel($form->items, $form);
        $updateTableView = null;
        switch ($model->template) {
            case MarketingPlan::TEMPLATE_LEAD_GENERATION:
                $updateTableView = 'partial/lead_generation_update_table';
                break;
            case MarketingPlan::TEMPLATE_ONLINE_SERVICE:
                $updateTableView = 'partial/online_service_update_table';
                break;
            case MarketingPlan::TEMPLATE_ONLINE_STORE:
                $updateTableView = 'partial/online_store_update_table';
                break;
        }

        return $this->renderAjax($updateTableView, [
            'model' => $form,
            'user' => Yii::$app->user->identity,
            'channel' => $channel,
        ]);
    }

    public function actionManyDelete(): Response {
        $financePlanningModels = Yii::$app->request->post('FinanceModel', []);
        if (is_array($financePlanningModels)) {
            foreach ($financePlanningModels as $id => $financePlanningModel) {
                if ($financePlanningModel['checked']) {
                    $this->findModel($id)->delete();
                }
            }
        }

        return $this->redirect(['index']);
    }

    public function actionManyCopy(): Response {
        $financePlanningModels = Yii::$app->request->post('FinanceModel', []);
        if (is_array($financePlanningModels)) {
            $transaction = Yii::$app->db->beginTransaction();
            foreach ($financePlanningModels as $id => $financePlanningModel) {
                if ($financePlanningModel['checked']) {
                    $model = $this->findModel($id);
                    $cloneMarketingPlan = clone $model;
                    unset($cloneMarketingPlan->id);
                    $cloneMarketingPlan->isNewRecord = true;
                    if (!$cloneMarketingPlan->save()) {
                        $transaction->rollBack();
                        Yii::$app->session->setFlash('error', 'Произошла ошибка при копировании прогнозов по маркетингу.');

                        return $this->redirect(['index']);
                    }

                    foreach ($model->marketingPlanItems as $marketingPlanItem) {
                        $cloneMarketingPlanItem = clone $marketingPlanItem;
                        unset($cloneMarketingPlanItem->id);
                        $cloneMarketingPlanItem->isNewRecord = true;
                        $cloneMarketingPlanItem->marketing_plan_id = $cloneMarketingPlan->id;
                        if (!$cloneMarketingPlanItem->save()) {
                            $transaction->rollBack();
                            Yii::$app->session->setFlash('error', 'Произошла ошибка при копировании прогнозов по маркетингу.');

                            return $this->redirect(['index']);
                        }
                    }
                }
            }

            $transaction->commit();
        }

        Yii::$app->session->setFlash('success', 'Прогнозы по маркетингу успешно скопированы.');

        return $this->redirect(['index']);
    }

    private function calculateItemsByChannel(array $items, UpdatePlanningMarketingForm $form): array {
        $result = [];
        $prevCumulativeFinancialAmount = 0;
        $prevMarketingPlanningItem = null;
        $firstRepeatedPurchaseCount = null;
        foreach ($items as $marketingPlanItemId => $marketingPlanItemData) {
            /** @var MarketingPlanItem $marketingPlanItem */
            $marketingPlanItem = MarketingPlanItem::findOne(['id' => $marketingPlanItemId]);
            if ($prevMarketingPlanningItem === null) {
                $firstRepeatedPurchaseCount = (int)($form->repeatedPurchasesCount[$marketingPlanItem->month . $marketingPlanItem->year] ?? 0);
            } else {
                $firstRepeatedPurchaseCount = 0;
            }

            $calculatedData = $form->calculateItems(
                $marketingPlanItemData,
                $form->averageCheck[$marketingPlanItem->month . $marketingPlanItem->year] ?? 0,
                $form->churnRate[$marketingPlanItem->month . $marketingPlanItem->year] ?? 0,
                $firstRepeatedPurchaseCount,
                $prevCumulativeFinancialAmount,
                $prevMarketingPlanningItem,
            );

            $calculatedData['month'] = $marketingPlanItem->month;
            $calculatedData['year'] = $marketingPlanItem->year;
            $result[$marketingPlanItemId] = $calculatedData;
            $prevCumulativeFinancialAmount = $calculatedData['cumulative_financial_amount'];
            $prevMarketingPlanningItem = $calculatedData;
        }

        return $result;
    }

    private function findModel($id): MarketingPlan
    {
        /** @var MarketingPlan $model */
        $model = MarketingPlan::find()
            ->andWhere(['id' => $id])
            ->andWhere(['company_id' => Yii::$app->user->identity->company->id])
            ->one();
        if ($model === null) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }

        return $model;
    }
}
