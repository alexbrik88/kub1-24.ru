<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use common\modules\marketing\models\MarketingReport;
use common\modules\marketing\models\MarketingReportGroup;
use common\modules\marketing\models\MarketingReportGroupItem;
use Yii;
use yii\base\Action;
use common\models\Company;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class AbstractGroupAction extends Action
{
    /**
     *
     */
    const VIEW_PATH = '@frontend/modules/analytics/modules/marketing/views/widgets/group';

    /**
     * @var Company
     */
    protected $company;

    /**
     * @var string
     */
    public $channelType;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        $this->company = Yii::$app->user->identity->currentEmployeeCompany->company ?? null;

        if (!isset($this->channelType) || !isset($this->company)) {
            throw new InvalidConfigException();
        }
    }

    public function ajaxValidate(MarketingReportGroup $model, $items)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model->load(Yii::$app->request->post());

        $validationErrors = ActiveForm::validate($model);

        if (empty($validationErrors)) {

            if (empty($items)) {
                $model->addError('itemsErrors', 'Группа не может быть пустой');
            }

            foreach ($items as $item) {
                $relationModel = new MarketingReportGroupItem([
                    'company_id' => $model->company_id,
                    'group_id' => null,
                    'utm_campaign' => $item
                ]);

                $relationModel->campaign_name = $this->_getCampaignNameByUtm($relationModel) ?: $item;

                if (!$relationModel->validate(['utm_campaign'])) {
                    $model->addError('itemsErrors', $relationModel->getFirstError('utm_campaign'));
                }
            }
        }

        foreach ($model->getErrors() as $attribute => $errors) {
            $validationErrors[Html::getInputId($model, $attribute)] = $errors;
        }

        return $validationErrors;
    }

    /**
     * @param $fileName
     * @param $params
     * @return string
     */
    public function render($fileName, $params)
    {
        return $this->controller->renderAjax(self::VIEW_PATH . DIRECTORY_SEPARATOR . $fileName, $params);
    }

    /**
     * @param MarketingReportGroupItem $item
     * @return false|string|null
     */
    protected function _getCampaignNameByUtm(MarketingReportGroupItem $item)
    {
        return MarketingReport::find()
            ->where(['company_id' => $item->company_id])
            ->andWhere(['utm_campaign' => $item->utm_campaign])
            ->andWhere(['not', ['campaign_name' => null]])
            ->select('campaign_name')
            ->scalar();
    }

    /**
     * @param int $id
     * @return MarketingReportGroup
     * @throws NotFoundHttpException
     */
    protected function _findModel($id)
    {
        /* @var MarketingReportGroup $model */
        $model = MarketingReportGroup::findOne([
            'company_id' => $this->company->id,
            'id' => (int)$id
        ]);

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested marketing group does not exist.');
        }
    }
}
