<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use common\components\filters\AccessControl;
use common\modules\marketing\models\MarketingUserConfig;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\rbac\permissions\BusinessAnalytics;
use Yii;
use yii\base\Exception;
use yii\filters\AjaxFilter;
use yii\web\Response;

class ChartAjaxController extends FrontendController
{
    protected static $viewPath = '@frontend/modules/analytics/modules/marketing/views/widgets/partial/chart';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'get-data',
                            'set-config'
                        ],
                        'roles' => [
                            BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function () {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_MARKETING);
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     * @throws Exception
     */
    public function actionGetData()
    {
        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        $channelType = Yii::$app->request->post('channel');
        $chartGroup = Yii::$app->request->post('chart-group');
        $chartPosition = Yii::$app->request->post('chart-position');
        $customOffset = Yii::$app->request->post('offset') ?: 0;
        $customCampaign = Yii::$app->request->post('campaign');
        $customPeriod = Yii::$app->request->post('period');

        switch ($chartPosition) {
            case 'top':
                $viewFile = 'chart_1';
                break;
            case 'bottom':
                $viewFile = 'chart_21';
                break;
            case 'bottom-table':
                $viewFile = 'chart_22';
                break;
            default:
                throw new Exception('Chart position not found: ' . $chartPosition);
        }

        return $this->renderPartial(self::$viewPath .'/'. $viewFile, [
            'marketingUserConfig' => MarketingUserConfig::findOne($employee->id),
            'channelType' => $channelType,
            'customOffset' => $customOffset,
            'customCampaign' => $customCampaign,
            'customPeriod' => $customPeriod,
            'company' => $company,
            'isAjax' => true
        ]);
    }

    /**
     * @return array
     */
    public function actionSetConfig()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $employee = Yii::$app->user->identity;
        $config = MarketingUserConfig::findOne(['employee_id' => $employee->id]);

        if (!$config) {
            $config = new MarketingUserConfig(['employee_id' => $employee->id]);
        }

        if ($config->load(Yii::$app->request->post()) && $config->save()) {
            return ['success' => 1, 'message' => 'Настройки сохранены'];
        } else {
            return ['success' => 0, 'message' => 'Не удалось сохранить настройки', 'errors' => json_encode($config->getErrors())];
        }
    }
}