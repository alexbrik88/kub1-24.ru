<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use common\components\filters\AccessControl;
use common\models\employee\Employee;
use common\modules\import\models\ImportJobDataRepository2;
use common\modules\marketing\components\GoogleAnalyticsComponent;
use common\modules\marketing\models\channel\YandexNotificationHelper;
use common\modules\marketing\models\MarketingChannel;
use common\modules\marketing\models\MarketingNotification;
use common\modules\marketing\models\MarketingNotificationGroup;
use common\modules\marketing\models\MarketingReportSearch;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\components\PageSize;
use frontend\controllers\WebTrait;
use frontend\modules\analytics\modules\marketing\models\AggregateImportForm;
use frontend\modules\analytics\modules\marketing\models\GoogleAnalyticsDisconnectForm;
use common\modules\marketing\models\GoogleAnalyticsProfile;
use frontend\modules\analytics\modules\marketing\models\GoogleAnalyticsSettingsForm;
use frontend\modules\analytics\modules\marketing\models\GoogleAnalyticsTokenForm;
use frontend\modules\analytics\modules\marketing\models\IntegrationRepository;
use frontend\modules\analytics\modules\marketing\models\UploadDataSearch;
use frontend\modules\analytics\modules\marketing\models\UploadType;
use frontend\rbac\permissions\BusinessAnalytics;
use Yii;
use yii\filters\AjaxFilter;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class DefaultController extends FrontendController
{
    use WebTrait;

    /** @var array */
    private const REDIRECT_URL = ['upload-data', 'type' => 2];

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                        ],
                        'roles' => [
                            BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function() {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_MARKETING);
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'upload-data',
                            'notifications',
                            'google-analytics-settings',
                            'analytics-token',
                            'analytics-auth',
                            'disconnect-google-analytics',
                            'aggregate-import',
                        ],
                        'roles' => [
                            BusinessAnalytics::ADMIN,
                        ],
                        'matchCallback' => function() {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_MARKETING);
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'credentials',
                        ],
                        'roles' => [
                            BusinessAnalytics::ADMIN,
                        ],
                        'matchCallback' => function() {
                            if (YII_DEBUG) {
                                return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_MARKETING);
                            }

                            return false;
                        },
                    ],
                ],
            ],
            'ajaxFilter' => [
                'class' => AjaxFilter::class,
                'only' => ['aggregate-import'],
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function actions()
    {
        return [
            'analytics-token' => [
                'class' => TokenAction::class,
                'redirectUrl' => Url::to(self::REDIRECT_URL + ['googleAnalyticsModal' => 1]),
                'formClass' => GoogleAnalyticsTokenForm::class,
            ],
            'analytics-auth' => [
                'class' => AuthAction::class,
                'componentClass' => GoogleAnalyticsComponent::class,
            ],
        ];
    }

    /**
     * @return string
     * @throws
     */
    public function actionIndex()
    {
        $get = $this->request->get();
        $company = $this->user->identity->company;

        $integrationsRepository = new IntegrationRepository($this->user->identity->company);
        $integrationsForm = new UploadDataSearch($this->user->identity->company);
        $integrationsDefaultSorting = '-clicks';
        $integrationsSearchModel = new MarketingReportSearch(['utm_campaign'], $integrationsDefaultSorting);
        $integrationsSearchModel::$GROUP_BY_CHANNELS = 1;
        $integrationsSearchModel->search(MarketingChannel::ALL, $company, $get);

        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 133, true);

        return $this->render('index', [
            'company' => $company,
            'repository' => $integrationsRepository,
            'integrations' => $integrationsForm->search(),
            'searchModel' => $integrationsSearchModel,
            'defaultSorting' => $integrationsDefaultSorting,
            'jobData' => (new ImportJobDataRepository2($this->user->identity->company))->findFinishedJobData(),
        ]);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionUploadData(): Response
    {
        $form = new UploadDataSearch($this->user->identity->company);
        $type = $this->request->get('type', UploadType::TYPE_ADS);

        if (!is_scalar($type) || !in_array($type, UploadType::TYPE_LIST)) {
            throw new NotFoundHttpException();
        }

        \common\models\company\CompanyFirstEvent::checkEvent(\Yii::$app->user->identity->company, 138, true);

        $this->response->content = $this->render('upload-data', [
            'type' => $type,
            'integrations' => $form->search(),
            'showGoogleAnalyticsModal' => $this->request->get('googleAnalyticsModal') ? 1 : 0,
        ]);

        return $this->response;
    }

    public function actionNotifications()
    {
        if ($this->request->isPost) {
            if (MarketingNotification::_save($this->request->post('rule', []), $this->user->identity->company)) {
                $this->session->setFlash('success', 'Настройки обновлены');
            } else {
                $this->session->setFlash('error', 'Ошибка обновления');
            }

            return $this->redirect('/analytics/marketing/default/notifications');
        }

        $channels = MarketingChannel::find()->asArray()->all();
        $groups = MarketingNotificationGroup::find()->asArray()->all();
        $items = MarketingNotification::getItems();

        /////////////// TEST! ////////////////////////////////////
        // notify client
        //$balance = 5;
        //$notification = new MarketingNotificationSender();
        //$notification->setCompany($company);
        //$notification->setChannel(MarketingChannel::YANDEX_AD);
        //$notification->triggerBalanceEvent($balance);
        //$notification->triggerUpdateEvent();
        //////////////////////////////////////////////////////////

        return $this->render('notifications', [
            'channels' => $channels,
            'groups' => $groups,
            'items' => $items,
            'isEditMode' => $this->request->get('edit'), // TODO: ? 1 : 0
        ]);
    }

    /**
     * @return Response
     */
    public function actionGoogleAnalyticsSettings(): Response
    {
        $model = new GoogleAnalyticsSettingsForm($this->user->identity->company);

        if ($model->profile->isNewRecord) {
            $this->session->setFlash('error', 'Необходимо авторизироваться для дальнейшей работы');

            return $this->redirect(self::REDIRECT_URL);
        }

        if ($model->load($this->request->post()) && $model->validate() && $model->updateSettings()) {
            $this->session->setFlash('success', 'Настройки сохранены.');

            return $this->redirect(self::REDIRECT_URL);
        }

        $this->response->content = $this->renderAjax('partial/google-analytics-settings', compact('model'));

        return $this->response;
    }

    /**
     * @return Response
     */
    public function actionDisconnectGoogleAnalytics(): Response
    {
        $form = new GoogleAnalyticsDisconnectForm($this->user->identity->company);

        if ($form->removeIntegration()) {
            $this->session->setFlash('success', 'Интеграция с Google Analytics отключена.');
        } else {
            $this->session->setFlash('error', 'Произошла ошибка при отключении интеграции с Google Analytics.');
        }

        return $this->redirect(self::REDIRECT_URL);
    }

    /**
     * @return Response
     */
    public function actionCredentials(): Response
    {
        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;
        $this->response->format = Response::FORMAT_JSON;
        $this->response->data = GoogleAnalyticsProfile::findByCompany($employee->company)->toArray();

        return $this->response;
    }

    /**
     * @return Response
     */
    public function actionAggregateImport(): Response
    {
        $form = new AggregateImportForm($this->user->identity);

        if ($form->load($this->request->post()) && $form->validate()) {
            if (!$form->createJob()) {
                $this->session->setFlash('error', 'Произошла ошибка при создании задачи на выгрузку операций.');
            } else {
                $this->session->setFlash('success', 'Задачи на выгрузку операций созданы.');
            }

            return $this->redirect(['/analytics/marketing']);
        }

        $this->response->content = $this->renderAjax('../widgets/aggregate-import-form', [
            'form' => $form,
        ]);

        return $this->response;
    }
}
