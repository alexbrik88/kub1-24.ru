<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use frontend\modules\analytics\modules\marketing\models\CredentialsForm;
use yii\base\InvalidConfigException;
use yii\web\Response;

class CredentialsAction extends AbstractAction
{
    /**
     * @var string
     */
    public $integrationName;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!isset($this->integrationName)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @return Response
     */
    public function run(): Response
    {
        $form = new CredentialsForm($this->user->identity->company, $this->integrationName);
        $this->response->format = Response::FORMAT_JSON;
        $this->response->data = $form->getData();

        return $this->response;
    }
}
