<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use common\modules\import\components\ImportCommandManager;
use common\modules\import\models\ImportJobDataRepository;
use frontend\components\PageSize;
use common\modules\marketing\models\MarketingReportSearch;
use yii\base\InvalidConfigException;
use yii\web\Response;

class IndexAction extends AbstractAction
{
    use DefaultSortingTrait;

    /**
     * @var string
     */
    public $sortingKey;

    /**
     * @var string
     */
    public $channelType;

    /**
     * @var
     */
    public $commandClass;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!isset($this->sortingKey, $this->channelType, $this->commandClass)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function run(): Response
    {
        $get = $this->request->get();
        $defaultSorting = $this->getDefaultSorting($this->sortingKey);
        $searchModel = new MarketingReportSearch(['utm_campaign'], $defaultSorting);

        $dataProvider = $searchModel->search($this->channelType, $this->user->identity->company, $get);
        $dataProvider->pagination->pageSize = PageSize::get();

        $totalSearchModel = new MarketingReportSearch(['utm_campaign'], $defaultSorting);
        $totalDataProvider = $totalSearchModel->search($this->channelType, $this->user->identity->company, $get);
        $totalDataProvider->pagination = false;

        $commandType = ImportCommandManager::getInstance()->getTypeByClass($this->commandClass);
        $jobDataRepository = new ImportJobDataRepository($this->user->identity->company);
        $lastJobData = $jobDataRepository->getLastJobData($commandType);

        $this->response->content = $this->controller->render('index', [
            'channelType' => $this->channelType,
            'defaultSorting' => $defaultSorting,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'totalDataProvider' => $totalDataProvider,
            'lastJobData' => $lastJobData
        ]);

        return $this->response;
    }
}
