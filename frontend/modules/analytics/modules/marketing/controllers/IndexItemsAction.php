<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use Yii;
use frontend\components\PageSize;
use common\modules\marketing\models\MarketingReportSearch;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\web\Response;

class IndexItemsAction extends Action
{
    use DefaultSortingTrait;

    public $user;

    /**
     * @var string
     */
    public $sortingKey;

    /**
     * @var string
     */
    public $channelType;

    /**
     * @var
     */
    public $commandClass;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!isset($this->sortingKey, $this->channelType)) {
            throw new InvalidConfigException();
        }

        $this->user = Yii::$app->user;
    }

    /**
     * @inheritDoc
     */
    public function run($group_id)
    {
        $get = Yii::$app->request->get();
        $defaultSorting = $this->getDefaultSorting($this->sortingKey);
        $searchModel = new MarketingReportSearch(['utm_campaign'], $defaultSorting);

        ////////////////////////////////////////
        $searchModel::$SHOW_GROUPS = 0;
        $searchModel->group_id = (int)$group_id;
        ////////////////////////////////////////

        $dataProvider = $searchModel->search($this->channelType, $this->user->identity->company, $get);
        $dataProvider->pagination->pageSize = PageSize::get();

        return $this->controller->renderAjax('@frontend/modules/analytics/modules/marketing/views/widgets/reports-grid-items', [
            'channelType' => $this->channelType,
            'defaultSorting' => $defaultSorting,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
