<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use common\models\employee\Employee;
use common\modules\marketing\components\FacebookComponent;
use common\modules\marketing\models\MarketingChannel;
use common\modules\marketing\commands\FacebookImportCommand;
use frontend\modules\analytics\modules\marketing\models\FacebookSettingsForm;
use frontend\modules\analytics\modules\marketing\models\FacebookTokenForm;
use yii\helpers\Url;

class FacebookController extends AbstractController
{
    /** @var string */
    private const DEFAULT_SORTING_KEY = 'FacebookDefaultSorting';

    /** @var string */
    private const REDIRECT_URL = '/analytics/marketing/facebook';

    /**
     * @inheritDoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'sortingKey' => self::DEFAULT_SORTING_KEY,
                'channelType' => MarketingChannel::FACEBOOK,
                'commandClass' => FacebookImportCommand::class,
            ],
            'xls' => [
                'class' => XlsAction::class,
                'sortingKey' => self::DEFAULT_SORTING_KEY,
                'channelType' => MarketingChannel::FACEBOOK,
            ],
            'import' => [
                'class' => ImportAction::class,
                'redirectUrl' => Url::to([self::REDIRECT_URL]),
                'commandClass' => FacebookImportCommand::class,
            ],
            'token' => [
                'class' => TokenAction::class,
                'redirectUrl' => Url::to([self::REDIRECT_URL]),
                'formClass' => FacebookTokenForm::class,
            ],
            'connect' => [
                'class' => ConnectAction::class,
                'formClass' => FacebookSettingsForm::class,
            ],
            'disconnect' => [
                'class' => DisconnectAction::class,
                'commandClass' => FacebookImportCommand::class,
                'redirectUrl' => Url::to([self::REDIRECT_URL]),
                'integrationName' => Employee::INTEGRATION_FACEBOOK,
                'successMessage' => 'Интеграция с Facebook отключена.',
                'errorMessage' => 'Произошла ошибка при отключении интеграции с Facebook.',
            ],
            'job-status' => [
                'class' => JobStatusAction::class,
                'commandClass' => FacebookImportCommand::class,
            ],
            'set-auto-import' => [
                'class' => SetAutoImportAction::class,
                'commandClass' => FacebookImportCommand::class,
            ],
            'credentials' => [
                'class' => CredentialsAction::class,
                'integrationName' => Employee::INTEGRATION_FACEBOOK,
            ],
            'auth' => [
                'class' => AuthAction::class,
                'componentClass' => FacebookComponent::class,
            ],
            // Groups
            'group-create' => [
                'class' => GroupCreateAction::class,
                'channelType' => MarketingChannel::FACEBOOK,
            ],
            'group-update' => [
                'class' => GroupUpdateAction::class,
                'channelType' => MarketingChannel::FACEBOOK,
            ],
            'group-delete' => [
                'class' => GroupDeleteAction::class,
                'channelType' => MarketingChannel::FACEBOOK,
            ],
            'group-get-campaigns-list' => [
                'class' => GroupGetCampaignsListAction::class,
                'channelType' => MarketingChannel::FACEBOOK,
            ],
            'index-items' => [
                'class' => IndexItemsAction::class,
                'channelType' => MarketingChannel::FACEBOOK,
                'sortingKey' => self::DEFAULT_SORTING_KEY,
            ],
        ];
    }
}
