<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use common\models\employee\Employee;
use common\modules\marketing\components\GoogleAdsComponent;
use common\modules\marketing\models\MarketingChannel;
use common\modules\marketing\commands\GoogleAdsImportCommand;
use frontend\modules\analytics\modules\marketing\models\GoogleAdsSettingForm;
use frontend\modules\analytics\modules\marketing\models\GoogleAdsTokenForm;
use yii\helpers\Url;

class GoogleWordsController extends AbstractController
{
    /** @var string */
    private const DEFAULT_SORTING_KEY = 'googleAdsDefaultSorting';

    /** @var string */
    private const REDIRECT_URL = '/analytics/marketing/google-words';

    /**
     * @inheritDoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'sortingKey' => self::DEFAULT_SORTING_KEY,
                'channelType' => MarketingChannel::GOOGLE_AD,
                'commandClass' => GoogleAdsImportCommand::class,
            ],
            'xls' => [
                'class' => XlsAction::class,
                'sortingKey' => self::DEFAULT_SORTING_KEY,
                'channelType' => MarketingChannel::GOOGLE_AD,
            ],
            'import' => [
                'class' => ImportAction::class,
                'redirectUrl' => Url::to([self::REDIRECT_URL]),
                'commandClass' => GoogleAdsImportCommand::class,
            ],
            'token' => [
                'class' => TokenAction::class,
                'redirectUrl' => Url::to([self::REDIRECT_URL]),
                'formClass' => GoogleAdsTokenForm::class,
            ],
            'disconnect' => [
                'class' => DisconnectAction::class,
                'commandClass' => GoogleAdsImportCommand::class,
                'redirectUrl' => Url::to([self::REDIRECT_URL]),
                'integrationName' => Employee::INTEGRATION_GOOGLE_ADS,
                'successMessage' => 'Интеграция с GoogleAds отключена.',
                'errorMessage' => 'Произошла ошибка при отключении интеграции с GoogleAds.',
            ],
            'connect' => [
                'class' => ConnectAction::class,
                'formClass' => GoogleAdsSettingForm::class,
            ],
            'job-status' => [
                'class' => JobStatusAction::class,
                'commandClass' => GoogleAdsImportCommand::class,
            ],
            'set-auto-import' => [
                'class' => SetAutoImportAction::class,
                'commandClass' => GoogleAdsImportCommand::class,
            ],
            'credentials' => [
                'class' => CredentialsAction::class,
                'integrationName' => Employee::INTEGRATION_GOOGLE_ADS,
            ],
            'auth' => [
                'class' => AuthAction::class,
                'componentClass' => GoogleAdsComponent::class,
            ],
            // Groups
            'group-create' => [
                'class' => GroupCreateAction::class,
                'channelType' => MarketingChannel::GOOGLE_AD,
            ],
            'group-update' => [
                'class' => GroupUpdateAction::class,
                'channelType' => MarketingChannel::GOOGLE_AD,
            ],
            'group-delete' => [
                'class' => GroupDeleteAction::class,
                'channelType' => MarketingChannel::GOOGLE_AD,
            ],
            'group-get-campaigns-list' => [
                'class' => GroupGetCampaignsListAction::class,
                'channelType' => MarketingChannel::GOOGLE_AD,
            ],
            'index-items' => [
                'class' => IndexItemsAction::class,
                'channelType' => MarketingChannel::GOOGLE_AD,
                'sortingKey' => self::DEFAULT_SORTING_KEY,
            ],
        ];
    }
}
