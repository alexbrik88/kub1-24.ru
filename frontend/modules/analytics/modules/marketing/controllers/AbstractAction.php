<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use frontend\controllers\WebTrait;
use yii\base\Action;
use yii\web\Response;

abstract class AbstractAction extends Action
{
    use WebTrait;

    /**
     * @return Response
     * @throws
     */
    abstract public function run(): Response;
}
