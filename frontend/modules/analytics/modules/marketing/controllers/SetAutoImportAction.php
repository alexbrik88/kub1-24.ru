<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use common\modules\import\components\ImportCommandManager;
use frontend\modules\analytics\modules\marketing\models\AutoImportForm;
use yii\base\InvalidConfigException;
use yii\web\Response;

class SetAutoImportAction extends AbstractAction
{
    /**
     * @var string
     */
    public $commandClass;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->commandClass)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function run(): Response
    {
        $form = new AutoImportForm(
            $this->user->identity->company,
            ImportCommandManager::getInstance()->getTypeByClass($this->commandClass)
        );

        $this->response->format = Response::FORMAT_JSON;
        $this->response->data = ($form->load($this->request->post()) && $form->validate() && $form->save());

        return $this->response;
    }
}
