<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use common\modules\import\components\ImportCommandManager;
use frontend\modules\analytics\modules\marketing\models\DisconnectForm;
use yii\base\InvalidConfigException;
use yii\web\Response;

class DisconnectAction extends AbstractAction
{
    /**
     * @var string
     */
    public $integrationName;

    /**
     * @var string
     */
    public $commandClass;

    /**
     * @var string
     */
    public $successMessage;

    /**
     * @var string
     */
    public $errorMessage;

    /**
     * @var string
     */
    public $redirectUrl;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!isset($this->redirectUrl, $this->commandClass, $this->integrationName)) {
            throw new InvalidConfigException();
        }

        if (!isset($this->successMessage, $this->errorMessage)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @return Response
     */
    public function run(): Response
    {
        $form = new DisconnectForm(
            $this->user->identity->company,
            ImportCommandManager::getInstance()->getTypeByClass($this->commandClass),
            $this->integrationName
        );

        if ($form->removeIntegration()) {
            $this->session->setFlash('success', $this->successMessage);
        } else {
            $this->session->setFlash('error', $this->errorMessage);
        }

        return $this->controller->redirect($this->redirectUrl);
    }
}
