<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use Yii;
use yii\db\Transaction;
use common\components\helpers\ModelHelper;
use common\modules\marketing\models\MarketingReportGroup;
use common\modules\marketing\models\MarketingReportGroupItem;

class GroupCreateAction extends AbstractGroupAction
{
    /**
     * @inheritDoc
     */
    public function run()
    {
        $model = new MarketingReportGroup([
            'company_id' => $this->company->id,
            'source_type' => $this->channelType,
        ]);

        $items = array_filter(Yii::$app->request->post('marketingGroupItems', []));

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model, $items);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($this->_create($model, $items)) {
                Yii::$app->session->setFlash('success', 'Группа успешно добавлена');
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось добавить группу');
            }

            return $this->controller->redirect(Yii::$app->request->referrer ?: 'index');
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }

    /**
     * @param MarketingReportGroup $model
     * @param array $items
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\NotSupportedException
     * @throws \yii\db\Exception
     */
    private function _create(MarketingReportGroup $model, array $items)
    {
        $transaction = new Transaction(['db' => Yii::$app->db]);
        $transaction->begin();

        if ($model->save()) {

            foreach (array_filter($items) as $item) {

                $relationModel = new MarketingReportGroupItem([
                    'company_id' => $model->company_id,
                    'group_id' => $model->id,
                    'source_type' => $model->source_type,
                    'utm_campaign' => $item
                ]);

                $relationModel->campaign_name = $this->_getCampaignNameByUtm($relationModel) ?: $item;

                if (!$relationModel->save()) {
                    ModelHelper::logErrors($relationModel, __METHOD__);
                    $transaction->rollBack();
                    return false;
                }
            }

        } else {

            ModelHelper::logErrors($model, __METHOD__);
            $transaction->rollBack();
            return false;
        }

        $transaction->commit();
        return true;
    }
}
