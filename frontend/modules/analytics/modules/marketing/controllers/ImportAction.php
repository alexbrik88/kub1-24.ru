<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use common\modules\import\components\ImportCommandManager;
use common\modules\import\models\ImportJobDataRepository;
use frontend\modules\analytics\modules\marketing\models\AutoImportForm;
use frontend\modules\analytics\modules\marketing\models\ImportForm;
use yii\base\InvalidConfigException;
use yii\web\Response;

class ImportAction extends AbstractAction
{
    /**
     * @var string
     */
    public $commandClass;

    /**
     * @var string
     */
    public $disconnectUrl = ['disconnect'];

    /**
     * @var string
     */
    public $redirectUrl;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!isset($this->redirectUrl, $this->commandClass, $this->disconnectUrl)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @return Response
     */
    public function run(): Response
    {
        $commandType = ImportCommandManager::getInstance()->getTypeByClass($this->commandClass);
        $form = new ImportForm($this->user->identity, $commandType);

        if ($form->load($this->request->post()) && $form->validate()) {
            if (!$form->createJob()) {
                $this->session->setFlash('error', 'Произошла ошибка при создании задачи на выгрузку операций.');
            } else {
                $this->session->setFlash('success', 'Задача на загрузку операций создана.');
            }

            return $this->controller->redirect($this->redirectUrl);
        }

        $disconnectUrl = $this->disconnectUrl;
        $autoImportForm = new AutoImportForm($this->user->identity->company, $commandType);
        $marketingSettings = $autoImportForm->settings;
        $jobDataRepository = new ImportJobDataRepository($this->user->identity->company);
        $lastAutoJobs = $jobDataRepository->getAutoJobsData($commandType);

        $this->response->content = $this->controller->renderAjax(
            '../widgets/import-modal',
            compact('form', 'lastAutoJobs', 'marketingSettings', 'disconnectUrl')
        );

        return $this->response;
    }
}
