<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use Yii;

class GroupDeleteAction extends AbstractGroupAction
{
    /**
     * @inheritDoc
     */
    public function run($id)
    {
        if ($this->_remove($id)) {
            Yii::$app->session->setFlash('success', 'Группа удалена');
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось удалить группу');
        }

        return $this->controller->redirect(Yii::$app->request->referrer ?: 'index');
    }

    private function _remove($id)
    {
        $model = $this->_findModel($id);
        return $model->delete();
    }
}
