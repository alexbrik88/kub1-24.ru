<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use common\modules\import\components\ImportCommandManager;
use common\modules\import\models\ImportJobDataRepository;
use yii\base\InvalidConfigException;
use yii\web\Response;

class JobStatusAction extends AbstractAction
{
    /**
     * @var string
     */
    public $commandClass;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->commandClass)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function run(): Response
    {
        $form = new ImportJobDataRepository($this->user->identity->company);
        $jobData = $form->getLastJobData(ImportCommandManager::getInstance()->getTypeByClass($this->commandClass));
        $this->response->format = Response::FORMAT_JSON;
        $this->response->data = ['status' => ($jobData && $jobData->finished_at)];

        return $this->response;
    }
}
