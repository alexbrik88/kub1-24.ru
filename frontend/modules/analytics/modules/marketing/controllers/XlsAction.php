<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use common\modules\marketing\models\MarketingChannel;
use frontend\controllers\ActionInterface;
use frontend\controllers\WebTrait;
use frontend\modules\analytics\modules\marketing\commands\XlsCommand;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\web\Response;

class XlsAction extends Action implements ActionInterface
{
    use WebTrait;
    use DefaultSortingTrait;

    /**
     * @var string|null
     */
    public ?string $sortingKey = null;

    /**
     * @var int|null
     */
    public ?int $channelType = null;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->sortingKey) || empty($this->channelType)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function run(): Response
    {
        $defaultSorting = $this->getDefaultSorting($this->sortingKey);
        $channel = MarketingChannel::findOne(['id' => $this->channelType]);
        $command = new XlsCommand($this->user->identity->company, $channel, $defaultSorting);
        $command->run();

        return $this->response; // Не исполняется
    }
}
