<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use common\modules\marketing\models\MarketingReportGroupItem;
use common\modules\marketing\models\MarketingReport;
use frontend\components\PageSize;
use yii\data\SqlDataProvider;
use yii\db\Query;
use Yii;

class GroupGetCampaignsListAction extends AbstractGroupAction
{
    /**
     * @inheritDoc
     */
    public function run()
    {
        $filterByCampaign = Yii::$app->request->get('filter_by_campaign_name');
        $excludeIds = array_filter((array)Yii::$app->request->post('exclude'));

        $query = (new Query)
            ->from(['m' => MarketingReport::tableName()])
            ->leftJoin(['i' => MarketingReportGroupItem::tableName()], 'm.company_id = i.company_id AND m.source_type = i.source_type AND m.utm_campaign = i.utm_campaign')
            ->andWhere(['m.company_id' => $this->company->id])
            ->andWhere(['m.source_type' => $this->channelType])
            ->andWhere(['i.group_id' => null])
            ->andFilterWhere(['like', 'm.campaign_name', $filterByCampaign])
            ->select(['m.utm_campaign', 'm.campaign_name'])
            ->groupBy(['m.utm_campaign']);

        if ($excludeIds)
            $query->andWhere(['not', ['m.utm_campaign' => $excludeIds]]);

        $dataProvider = new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'totalCount' => $query->count(),
            'sort' => [
                'attributes' => [
                    'campaign_name'
                ],
                'defaultOrder' => [
                    'campaign_name' => SORT_ASC
                ]
            ],
        ]);

        $dataProvider->pagination->pageSize = PageSize::get();

        return $this->controller->renderAjax(self::VIEW_PATH . DIRECTORY_SEPARATOR . 'campaigns-list', [
            'dataProvider' => $dataProvider,
            'filterByCampaign' => $filterByCampaign
        ]);
    }
}
