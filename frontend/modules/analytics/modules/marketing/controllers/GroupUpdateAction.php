<?php

namespace frontend\modules\analytics\modules\marketing\controllers;

use Yii;
use yii\db\Transaction;
use common\components\helpers\ModelHelper;
use common\modules\marketing\models\MarketingReportGroup;
use common\modules\marketing\models\MarketingReportGroupItem;
use yii\web\NotFoundHttpException;

class GroupUpdateAction extends AbstractGroupAction
{
    /**
     * @inheritDoc
     */
    public function run($id)
    {
        $model = $this->_findModel($id);

        if (Yii::$app->request->post('ajax') !== null) {
            $items = array_filter(Yii::$app->request->post('marketingGroupItems', []));

            if (empty($items)) {
                if ($this->_remove($id)) {
                    Yii::$app->session->setFlash('success', 'Группа удалена');
                } else {
                    Yii::$app->session->setFlash('error', 'Не удалось удалить группу');
                }

                return $this->controller->redirect(Yii::$app->request->referrer ?: 'index');
            }

            return $this->ajaxValidate($model, $items);
        }

        if ($model->load(Yii::$app->request->post())) {
            $items = array_filter(Yii::$app->request->post('marketingGroupItems', []));
            if ($this->_update($model, $items)) {
                Yii::$app->session->setFlash('success', 'Группа изменена');
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось изменить группу');
            }

            return $this->controller->redirect(Yii::$app->request->referrer ?: 'index');
        }

        return $this->render('update', [
            'model' => $model
        ]);
    }

    private function _remove($id)
    {
        $model = $this->_findModel($id);
        return $model->delete();
    }

    /**
     * @param MarketingReportGroup $model
     * @param array $items
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\NotSupportedException
     * @throws \yii\db\Exception
     */
    private function _update(MarketingReportGroup $model, array $items)
    {
        $transaction = new Transaction(['db' => Yii::$app->db]);
        $transaction->begin();

        $dbItems = $model->items;
        $clientItems = [];
        
        if ($model->save()) {

            foreach (array_filter($items) as $item) {

                /** @var MarketingReportGroupItem $relationModel */
                if ($relationModel = MarketingReportGroupItem::findOne([
                    'company_id' => $model->company_id,
                    'group_id' => $model->id,
                    'source_type' => $model->source_type,
                    'utm_campaign' => $item
                ])) {

                    $clientItems[] = $relationModel->utm_campaign;

                } else {

                    $relationModel = new MarketingReportGroupItem([
                        'company_id' => $model->company_id,
                        'group_id' => $model->id,
                        'source_type' => $model->source_type,
                        'utm_campaign' => $item
                    ]);
                }

                $relationModel->campaign_name = $this->_getCampaignNameByUtm($relationModel) ?: $item;

                if (!$relationModel->save()) {
                    ModelHelper::logErrors($relationModel, __METHOD__);
                    $transaction->rollBack();
                    return false;
                }
            }
            
            // delete
            foreach ($dbItems as $item) {
                if (!in_array($item->utm_campaign, $clientItems))
                    $item->delete();
            }

        } else {

            ModelHelper::logErrors($model, __METHOD__);
            $transaction->rollBack();
            return false;
        }

        $transaction->commit();
        return true;
    }
}
