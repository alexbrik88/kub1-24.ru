<?php

namespace frontend\modules\analytics\modules\marketing;

use yii\base\Module as AbstractModule;

class Module extends AbstractModule
{
    /**
     * @inheritDoc
     */
    public $controllerNamespace = 'frontend\modules\analytics\modules\marketing\controllers';
}
