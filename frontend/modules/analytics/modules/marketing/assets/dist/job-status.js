$(document).ready(function() {
    var timer = {
        interval: null,

        create: function(callback) {
            this.destroy();
            this.interval = window.setInterval(callback, 5 * 1000);
        },

        destroy: function() {
            if (this.interval !== null) {
                window.clearInterval(this.interval);
                this.interval = null;
            }
        }
    };

    var jobStatus = $('#jobStatus');

    if (jobStatus.length) {
        timer.create(function () {
            var url = jobStatus.data('url');
            var ajax = $.ajax(url);

            ajax.done(function(data) {
                if (data.status && data.status === true) {
                    timer.destroy();
                    window.location.reload();
                }
            });

            ajax.fail(function () {
                timer.destroy();
            })
        });
    }
});
