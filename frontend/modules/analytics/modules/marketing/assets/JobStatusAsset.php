<?php

namespace frontend\modules\analytics\modules\marketing\assets;
use yii\web\AssetBundle;

class JobStatusAsset extends AssetBundle
{
    /**
     * @inheritDoc
     */
    public $js = [
        'job-status.js',
    ];

    /**
     * @inheritDoc
     */
    public $sourcePath = __DIR__ . '/dist';

    /**
     * @inheritDoc
     */
    public $depends = [
        'yii\bootstrap4\BootstrapPluginAsset',
    ];
}
