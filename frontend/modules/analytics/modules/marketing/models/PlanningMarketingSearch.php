<?php

namespace frontend\modules\analytics\modules\marketing\models;

use common\components\helpers\ArrayHelper;
use common\models\employee\Employee;
use common\models\marketing\MarketingPlan;
use common\models\marketing\MarketingPlanItem;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\db\ActiveQuery;

class PlanningMarketingSearch extends Model
{
    public $name;

    public $responsible_employee_id;

    private Employee $employee;

    public function __construct(Employee $employee, $config = []) {
        $this->employee = $employee;
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['name'], 'string'],
            [['responsible_employee_id'], 'integer'],
        ];
    }

    public function search(array $params = []): DataProviderInterface {
        $query = $this->getBaseQuery();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
                'attributes' => [
                    'created_at',
                    'name',
                    'start_date',
                    'end_date',
                    'total_revenue',
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['LIKE', 'mp.name', $this->name]);

        return $dataProvider;
    }

    public function searchTotal(int $marketingPlanId): array {
        $data = MarketingPlanItem::find()
            ->select([
                'month',
                'year',
                'SUM(budget_amount) as budget_amount',
                'SUM(marketing_service_amount) as marketing_service_amount',
                'SUM(additional_expenses_amount) as additional_expenses_amount',
                'SUM(total_additional_expenses_amount) as total_additional_expenses_amount',
                'SUM(additional_expenses_amount_for_one_sale) as additional_expenses_amount_for_one_sale',
                'SUM(clicks_count) as clicks_count',
                'SUM(registration_count) as registration_count',
                'SUM(active_users_count) as active_users_count',
                'SUM(purchases_count) as purchases_count',
                'SUM(sale_amount) as sale_amount',
                'AVG(average_check) as average_check',
                'SUM(proceeds_from_new_amount) as proceeds_from_new_amount',
                'AVG(churn_rate) as churn_rate',
                'SUM(repeated_purchases_amount) as repeated_purchases_amount',
                'SUM(total_proceeds_amount) as total_proceeds_amount',
                'SUM(cumulative_financial_amount) as cumulative_financial_amount',
                'SUM(total_purchases_count) as total_purchases_count',
                'SUM(repeated_purchases_count) as repeated_purchases_count',
                'SUM(salesperson_salary_amount) as salesperson_salary_amount',
            ])
            ->andWhere(['marketing_plan_id' => $marketingPlanId])
            ->andWhere(['company_id' => $this->employee->company_id])
            ->groupBy(['year', 'month'])
            ->asArray()
            ->indexBy(fn(array $marketingPlanItem): string => "{$marketingPlanItem['year']}{$marketingPlanItem['month']}")
            ->all();

        $i = 0;
        $channelsCount = MarketingPlanItem::find()
            ->andWhere(['company_id' => $this->employee->company_id])
            ->andWhere(['marketing_plan_id' => $marketingPlanId])
            ->groupBy('channel')
            ->count();
        $prevCumulativeFinancialAmount = 0;
        foreach ($data as $date => $dataByDate) {
            $clickAmount = $data[$date]['clicks_count'] ? round($data[$date]['budget_amount'] / $data[$date]['clicks_count']) : 0;
            $registrationConversion = $data[$date]['clicks_count'] ? round(($data[$date]['registration_count'] / $data[$date]['clicks_count']) * 100, 2) : 0;
            $registrationAmount = $data[$date]['registration_count'] ? round($data[$date]['budget_amount'] / $data[$date]['registration_count'], 2) : 0;
            $activeUserConversion = $data[$date]['registration_count'] ? round(($data[$date]['active_users_count'] / $data[$date]['registration_count']) * 100, 2) : 0;
            $purchaseConversion = $data[$date]['active_users_count'] ? round(($data[$date]['purchases_count'] / $data[$date]['active_users_count']) * 100, 2) : 0;
            $activeUserAmount = $data[$date]['active_users_count'] ? round($data[$date]['budget_amount'] / $data[$date]['active_users_count'], 2) : 0;
            $data[$date]['click_amount'] = $clickAmount;
            $data[$date]['registration_conversion'] = $registrationConversion;
            $data[$date]['registration_amount'] = $registrationAmount;
            $data[$date]['active_user_conversion'] = $activeUserConversion;
            $data[$date]['purchase_conversion'] = $purchaseConversion;
            $data[$date]['active_user_amount'] = $activeUserAmount;
            $data[$date]['financial_result'] = $data[$date]['total_proceeds_amount'] - $data[$date]['budget_amount'] - $data[$date]['total_additional_expenses_amount'];
            $data[$date]['cumulative_financial_amount'] = $prevCumulativeFinancialAmount + $data[$date]['financial_result'];
            $prevCumulativeFinancialAmount = $data[$date]['cumulative_financial_amount'];

            // ???
            /*if ($i === 0) {
                $data[$date]['repeated_purchases_count'] = $data[$date]['repeated_purchases_count'] / $channelsCount;
            }*/

            $i++;
        }

        $purchasesCountByChannels = [];
        foreach (MarketingPlanItem::find()
                     ->select('channel')
                     ->andWhere(['marketing_plan_id' => $marketingPlanId])
                     ->groupBy('channel')
                     ->column() as $channel) {
            $totalPurchasesCountByChannel = 0;
            foreach (MarketingPlanItem::find()
                ->select([
                    'month',
                    'year',
                    'SUM(purchases_count) as purchases_count',
                ])
                ->andWhere(['marketing_plan_id' => $marketingPlanId])
                ->andWhere(['company_id' => $this->employee->company_id])
                ->andWhere(['channel' => $channel])
                ->groupBy(['year', 'month'])
                ->asArray()
                ->indexBy(fn(array $marketingPlanItem): string => "{$marketingPlanItem['year']}{$marketingPlanItem['month']}")
                ->all() as $date => $purchasesCount) {
                $totalPurchasesCountByChannel += $purchasesCount['purchases_count'];
                $purchasesCountByChannels[$channel][$date] = $purchasesCount['purchases_count'];
            }

            $purchasesCountByChannels[$channel]['total'] = $totalPurchasesCountByChannel;
        }

        uasort($purchasesCountByChannels, function($a, $b): int {
            return $b['total'] <=> $a['total'];
        });

        $data['purchases_count_by_channels'] = $purchasesCountByChannels;

        return $data;
    }

    public function getResponsibleEmployeeFilter(): array
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map($this->getBaseQuery()
            ->joinWith('responsibleEmployee')
            ->groupBy('responsible_employee_id')
            ->all(), 'responsible_employee_id', 'responsibleEmployee.shortFio'));
    }

    private function getBaseQuery(): ActiveQuery
    {
        return MarketingPlan::find()
            ->alias('mp')
            ->andWhere(['mp.company_id' => $this->employee->currentEmployeeCompany->company_id]);
    }
}