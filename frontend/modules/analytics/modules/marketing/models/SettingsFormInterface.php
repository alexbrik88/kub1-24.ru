<?php

namespace frontend\modules\analytics\modules\marketing\models;

use common\models\Company;
use common\models\FormInterface;

interface SettingsFormInterface extends FormInterface
{
    /**
     * @return bool
     */
    public function updateSettings(): bool;

    /**
     * @param Company $company
     * @return static
     */
    public static function createSettingsForm(Company $company): self;
}
