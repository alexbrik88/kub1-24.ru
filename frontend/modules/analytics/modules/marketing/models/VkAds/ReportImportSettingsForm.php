<?php

namespace frontend\modules\analytics\modules\marketing\models\VkAds;


use Carbon\Carbon;
use common\components\date\DateHelper;
use common\models\employee\Employee;
use common\models\integration\LastOperationInIntegration;
use common\models\jobs\Jobs;
use Yii;
use yii\base\Model;

class ReportImportSettingsForm extends Model
{
    public $dateFrom;
    public $dateTo;

    /** @var Employee */
    private $user;

    public function __construct(Employee $user, array $config = [])
    {
        $this->user = $user;
        parent::__construct($config);
    }

    public function init()
    {
        parent::init();

        $this->dateFrom = date('d.m.Y');
        $this->dateTo = date('d.m.Y');
    }

    public function attributeLabels()
    {
        return [
            'dateFrom' => 'Начало периода',
            'dateTo' => 'Конец периода',
        ];
    }

    public function rules()
    {
        return [
            [['dateFrom', 'dateTo'], 'required', 'message' => 'Необходимо заполнить.'],
            [['dateFrom'], 'validateDateFrom'],
            [['dateTo'], 'validateDateTo'],
        ];
    }

    public function validateDateTo($attribute)
    {
        if ($this->dateFrom > $this->dateTo) {
            $this->addError($attribute, 'Дата окончания должна быть больше даты начала.');
        }
    }

    public function validateDateFrom($attribute)
    {
        if ($this->dateFrom > date(DateHelper::FORMAT_DATE)) {
            $this->addError($attribute, 'Дата не должна быть больше текущей даты.');
        }
    }

    public function getUser()
    {
        return $this->user;
    }

//    public function getAccessToken()
//    {
//        return $this->user->company->integration(Employee::INTEGRATION_VK)['access_token'] ?? null;
//    }

    public function createJob()
    {
//        if ($this->getAccessToken() === null) {
//            $this->addError('access_token', 'Необходимо авторизироваться');
//
//            return null;
//        }

        $transaction = Yii::$app->db->beginTransaction();

        $job = new Jobs();

        $job->company_id = $this->user->company_id;
        $job->employee_id = $this->user->id;
        $job->type = Jobs::TYPE_IMPORT_REPORT_FROM_VK_ADS;
        $job->params = [
            'from' => Carbon::parse($this->dateFrom)->startOfDay()->format(DateHelper::FORMAT_DATETIME),
            'to' => Carbon::parse($this->dateTo)->endOfDay()->format(DateHelper::FORMAT_DATETIME),
        ];
        $job->result = 'Задача выполняется...';

        if (!$job->save()) {
            $transaction->rollBack();
            return null;
        }

//        $this->updateBalanceAmount();
//        LastOperationInIntegration::updateIntegrationDate(
//            $job->company,
//            LastOperationInIntegration::YANDEX_DIRECT_TYPE,
//            $job->created_at,
//            Carbon::parse($this->dateFrom)->timestamp,
//            Carbon::parse($this->dateTo)->timestamp
//        );

        $transaction->commit();

        return $job;
    }
}