<?php

namespace frontend\modules\analytics\modules\marketing\models;

use common\models\Company;
use common\models\company\IntegrationData;
use common\modules\import\models\TokenFormInterface;
use yii\base\Model;
use Throwable;

abstract class AbstractTokenForm extends Model implements TokenFormInterface
{
    /**
     * @var string|null
     */
    public $code;

    /**
     * @var Company
     */
    protected $company;

    /**
     * @param Company $company
     * @param array $config
     */
    public function __construct(Company $company, array $config = [])
    {
        $this->company = $company;

        parent::__construct($config);
    }

    /**
     * @inheritDoc
     */
    public static function createTokenForm(Company $company): TokenFormInterface
    {
        return new static($company);
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['code'], 'string'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function updateToken(): bool
    {
        try {
            $module = $this->getIntegrationModule();
            $accessToken = $this->getAccessToken();

            if ($accessToken) {
                $integrationData = $this->company->getIntegrationData($module);
                $this->afterFetchToken($integrationData);
                $integrationData->setAccessToken($accessToken);

                return $integrationData->saveConfig();
            }
        } catch (Throwable $exception) {
            $this->addError('code', $exception->getMessage());
        }

        return false;
    }

    /**
     * @param IntegrationData $integrationData
     * @return void
     */
    protected function afterFetchToken(IntegrationData $integrationData): void
    {
    }

    /**
     * @return string
     * @throws Throwable
     */
    abstract protected function getAccessToken(): string;

    /**
     * @return string
     */
    abstract protected function getIntegrationModule(): string;
}
