<?php

namespace frontend\modules\analytics\modules\marketing\models;

final class UploadType
{
    /** @var int */
    public const TYPE_ADS = 1;

    /** @var int */
    public const TYPE_ADS_LEADS = 2;

    /** @var int */
    public const TYPE_ADS_LEADS_SELLS = 3;

    /** @var int[] */
    public const TYPE_LIST = [
        self::TYPE_ADS,
        self::TYPE_ADS_LEADS,
        self::TYPE_ADS_LEADS_SELLS
    ];
}
