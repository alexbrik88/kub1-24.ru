<?php

namespace frontend\modules\analytics\modules\marketing\models;

use common\models\Company;
use common\models\company\IntegrationData;
use common\modules\marketing\models\MarketingChannel;
use common\modules\marketing\models\MarketingReport;
use common\modules\marketing\models\MarketingReportSearch;
use yii\base\BaseObject;
use yii\data\DataProviderInterface;

class Integration extends BaseObject
{
    /**
     * @var Company
     */
    private Company $company;

    /**
     * @var IntegrationData
     */
    private IntegrationData $integrationData;

    /**
     * @var MarketingChannel|null
     */
    private ?MarketingChannel $channel;

    /**
     * @var DataProviderInterface
     */
    private $_totalProvider;

    /**
     * @param Company $company
     * @param string $integrationName
     * @param int $channelId
     */
    public function __construct(Company $company, string $integrationName, int $channelId)
    {
        $this->company = $company;
        $this->integrationData = $company->getIntegrationData($integrationName);
        $this->channel = MarketingChannel::findOne(['id' => $channelId]);

        parent::__construct();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->channel->name;
    }

    /**
     * @return MarketingChannel
     */
    public function getChannel(): MarketingChannel
    {
        return $this->channel;
    }

    /**
     * @return float
     */
    public function getBalance(): float
    {
        return $this->integrationData->getBalance();
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return ($this->integrationData->hasAccessToken() && $this->integrationData->hasAccountId());
    }

    /**
     * @return bool
     */
    public function getIsActive(): bool
    {
        return $this->isActive();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->channel->sort;
    }

    /**
     * @return float
     */
    public function getTotalCost(): float
    {
        return MarketingReport::getTotal($this->getTotalProvider()->getModels(), 'cost');
    }

    /**
     * @return int
     */
    public function getTotalImpressions(): int
    {
        return MarketingReport::getTotal($this->getTotalProvider()->getModels(), 'impressions');
    }

    /**
     * @return int
     */
    public function getTotalClicks(): int
    {
        return MarketingReport::getTotal($this->getTotalProvider()->getModels(), 'clicks');
    }

    /**
     * @return float
     */
    public function getTotalCtr(): float
    {
        return MarketingReport::getTotalCtr($this->getTotalProvider()->getModels(), 'clicks', 'impressions');
    }

    /**
     * @return DataProviderInterface
     */
    private function getTotalProvider(): DataProviderInterface
    {
        if ($this->_totalProvider === null) {
            $this->_totalProvider = (new MarketingReportSearch(['utm_campaign']))->search(
                $this->channel->id,
                $this->company
            );
            $this->_totalProvider->pagination = false;
            $this->_totalProvider->prepare(true);
        }

        return $this->_totalProvider;
    }
}
