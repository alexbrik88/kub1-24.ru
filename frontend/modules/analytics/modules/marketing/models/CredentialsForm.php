<?php

namespace frontend\modules\analytics\modules\marketing\models;

use common\models\Company;
use common\models\FormInterface;
use yii\base\Model;

class CredentialsForm extends Model implements FormInterface
{
    /**
     * @var Company
     */
    private $company;

    /**
     * @var string
     */
    private $integrationName;

    /**
     * @param Company $company
     * @param string $integrationName
     * @param array $config
     */
    public function __construct(Company $company, string $integrationName, array $config = [])
    {
        $this->company = $company;
        $this->integrationName = $integrationName;

        parent::__construct($config);
    }

    /**
     * @return string[]
     */
    public function getData(): array
    {
        return $this->company->getIntegrationData($this->integrationName)->toArray();
    }
}
