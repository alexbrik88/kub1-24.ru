<?php

namespace frontend\modules\analytics\modules\marketing\models;

use common\models\employee\Employee;

class GoogleAdsSettingForm extends AbstractSettingsForm
{
    /**
     * @var string|null
     */
    public $customerId;

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'customerId' => 'Идентификатор клиента',
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['customerId'], 'required'],
            [['customerId'], 'string'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getAccountId(): ?string
    {
        return $this->customerId;
    }

    /**
     * @inheritDoc
     */
    protected function getIntegrationName(): string
    {
        return Employee::INTEGRATION_GOOGLE_ADS;
    }
}
