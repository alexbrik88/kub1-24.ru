<?php

namespace frontend\modules\analytics\modules\marketing\models;

use common\modules\marketing\components\FacebookComponent;
use common\models\employee\Employee;

class FacebookTokenForm extends AbstractTokenForm
{
    /**
     * @inheritDoc
     */
    protected function getAccessToken(): string
    {
        return FacebookComponent::getInstance()->getAccessToken($this->code);
    }

    /**
     * @inheritDoc
     */
    protected function getIntegrationModule(): string
    {
        return Employee::INTEGRATION_FACEBOOK;
    }
}
