<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 23.01.2020
 * Time: 1:30
 */

namespace frontend\modules\marketing\modules\reports\models\YandexDirect;

use common\models\employee\Employee;
use common\models\jobs\AutoloadJob;
use common\models\marketing\MarketingChannel;
use frontend\modules\marketing\modules\reports\models\MarketingSettings;
use yii\base\Model;

class SetAutoImportForm extends Model
{
    public $periodType;

    /** @var Employee */
    private $user;

    public function __construct(Employee $user, array $config = [])
    {
        $this->user = $user;
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['periodType'], 'required'],
            [['periodType'], 'in', 'range' => array_keys(MarketingSettings::AUTO_IMPORT_TYPE_MAP)],
        ];
    }

    public function save()
    {
        $settings = $this->user->currentEmployeeCompany->company->yandexDirectMarketingSettings;

        if ($settings === null) {
            $settings = new MarketingSettings();
            $settings->company_id = $this->user->currentEmployeeCompany->company_id;
            $settings->channel_id = MarketingChannel::YANDEX_AD;
        }

        $settings->auto_import_type = $this->periodType;

        return $settings->save();
    }
}
