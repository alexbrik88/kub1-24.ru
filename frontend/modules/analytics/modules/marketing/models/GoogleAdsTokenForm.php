<?php

namespace frontend\modules\analytics\modules\marketing\models;

use common\modules\marketing\components\GoogleAdsComponent;
use common\models\employee\Employee;

class GoogleAdsTokenForm extends AbstractTokenForm
{
    /**
     * @inheritDoc
     */
    protected function getAccessToken(): string
    {
        $credentials = GoogleAdsComponent::getInstance()->getCredentials($this->code);

        return $credentials['refresh_token'];
    }

    /**
     * @inheritDoc
     */
    protected function getIntegrationModule(): string
    {
        return Employee::INTEGRATION_GOOGLE_ADS;
    }
}
