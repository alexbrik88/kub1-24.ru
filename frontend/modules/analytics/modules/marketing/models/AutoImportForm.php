<?php

namespace frontend\modules\analytics\modules\marketing\models;

use common\models\Company;
use common\models\FormInterface;
use common\modules\import\components\ImportCommandType;
use common\modules\marketing\models\MarketingSettings;
use yii\base\Model;

/**
 * @property-read MarketingSettings $settings
 */
class AutoImportForm extends Model implements FormInterface
{
    /**
     * @var int
     */
    public $auto_import_type = MarketingSettings::AUTO_IMPORT_TYPE_DISABLED;

    /**
     * @var Company
     */
    private $company;

    /**
     * @var ImportCommandType
     */
    private $commandType;

    /**
     * @var MarketingSettings
     */
    private $_settings;

    /**
     * @param Company $company
     * @param ImportCommandType $commandType
     */
    public function __construct(Company $company, ImportCommandType $commandType)
    {
        $this->company = $company;
        $this->commandType = $commandType;

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['auto_import_type'], 'required'],
            [['auto_import_type'], 'in', 'range' => array_keys(MarketingSettings::AUTO_IMPORT_TYPE_MAP)],
        ];
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        $this->settings->auto_import_type = $this->auto_import_type;

        return $this->settings->save(false);
    }

    /**
     * @return MarketingSettings
     */
    public function getSettings(): MarketingSettings
    {
        if ($this->_settings === null) {
            $condition = [
                'company_id' => $this->company->id,
                'command_type' => $this->commandType->getCommandId(),
            ];
            $this->_settings = MarketingSettings::findOne($condition);

            if (!$this->_settings) {
                $this->_settings = new MarketingSettings($condition);
                $this->_settings->auto_import_type = MarketingSettings::AUTO_IMPORT_TYPE_DISABLED;
            }
        }

        return $this->_settings;
    }
}
