<?php

namespace frontend\modules\analytics\modules\marketing\models;

use common\modules\marketing\components\GoogleAnalyticsComponent;
use common\models\Company;
use common\modules\marketing\models\GoogleAnalyticsProfile;
use common\modules\marketing\models\GoogleAnalyticsProfileGoal;
use Google_Service_Analytics;
use yii\base\Model;

/**
 * @property-read GoogleAnalyticsProfile $profile
 */
class GoogleAnalyticsSettingsForm extends Model implements SettingsFormInterface
{
    public $name;

    public $avatar;

    public $analyticsAccount;

    public $analyticsProperty;

    public $analyticsView;

    /**
     * @var int
     */
    public $mainAnalyticsGoal;

    /**
     * @var array
     */
    public $additionalAnalyticsGoals = [];

    private $analyticsAccounts;

    private $analyticsProperties = [];

    private $analyticsViews = [];

    private $analyticsGoals = [];

    /**
     * @var Company
     */
    private $company;

    /**
     * @var GoogleAnalyticsProfile
     */
    private $_profile;

    /**
     * @param Company $company
     * @param array $config
     */
    public function __construct(Company $company, array $config = [])
    {
        $this->company = $company;

        parent::__construct($config);
    }

    /**
     * @inheritDoc
     */
    public static function createSettingsForm(Company $company): SettingsFormInterface {
        return new static($company);
    }

    /**
     * @return string
     */
    public function formName()
    {
        return 'SettingsForm';
    }

    /**
     * @inheritDoc
     * @throws
     */
    public function init()
    {
        $this->name = $this->profile->user_name;
        $this->avatar = $this->profile->avatar;
        $this->analyticsAccount = $this->profile->analytics_account_id;
        $this->analyticsProperty = $this->profile->analytics_property_id;
        $this->analyticsView = $this->profile->analytics_view_id;

        $googleAnalyticsProfileGoals = $this->profile->goals;

        foreach ($googleAnalyticsProfileGoals as $index => $goal) {
            if ($index === 0) {
                $this->mainAnalyticsGoal = $goal->external_id;
            } else {
                $this->additionalAnalyticsGoals[$index] = $goal->external_id;
            }
        }

        $this->analyticsAccounts = [];
        $this->analyticsProperties = [];
        $this->analyticsViews = [];
        $this->analyticsGoals = [];

        $client = GoogleAnalyticsComponent::getInstance()->createClient($this->profile->refresh_token);
        $analytics = new Google_Service_Analytics($client);
        $accounts = $analytics->management_accounts->listManagementAccounts();
        $accountItems = $accounts->getItems();

        /** @var \Google_Service_Analytics_Account $accountItem */
        foreach ($accountItems as $accountItem) {
            $accountId = $accountItem->getId();
            $this->analyticsAccounts[$accountId] = $accountItem->getName();
            $properties = $analytics->management_webproperties->listManagementWebproperties($accountId);
            /** @var \Google_Service_Analytics_Webproperty $property */
            foreach ($properties as $property) {
                $propertyId = $property->id;
                $this->analyticsProperties[$accountId][$propertyId] = "{$propertyId}, {$property->name}";
                $profiles = $analytics->management_profiles->listManagementProfiles($accountId, $propertyId);
                /** @var \Google_Service_Analytics_Profile $profile */
                foreach ($profiles->getItems() as $profile) {
                    $profileId = $profile->id;
                    $this->analyticsViews[$propertyId][$profileId] = $profile->name;
                    $goals = $analytics->management_goals->listManagementGoals(
                        $accountId,
                        $propertyId,
                        $profileId
                    );
                    /** @var \Google_Service_Analytics_Goal $goal */
                    foreach ($goals->getItems() as $goal) {
                        $this->analyticsGoals[$profileId][$goal->id] = $goal->name;
                    }
                }
            }
        }
    }

    public function rules(): array
    {
        return [
            [['analyticsAccount', 'analyticsProperty', 'analyticsView'], 'required'],
            [['analyticsAccount', 'analyticsProperty', 'analyticsView'], 'string'],
            [['mainAnalyticsGoal', 'additionalAnalyticsGoals'], 'safe'],
            [['mainAnalyticsGoal'], 'required', 'message' => 'Нужно добавить цель'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'analyticsAccount' => 'Аккаунт Google Analytics',
            'analyticsProperty' => 'Ресурс',
            'analyticsView' => 'Представление',
            'mainAnalyticsGoal' => 'Цель',
            'additionalAnalyticsGoals' => 'Цель',
        ];
    }

    public function getAnalyticsAccounts(): array
    {
        return $this->analyticsAccounts;
    }

    public function getAnalyticsProperties(): array
    {
        return $this->analyticsProperties;
    }

    public function getAnalyticsViews(): array
    {
        return $this->analyticsViews;
    }

    public function getAnalyticsGoals(): array
    {
        return $this->analyticsGoals;
    }

    public function isIntegrationConnected(): bool
    {
        return $this->company->googleAnalyticsProfile->analytics_account_id !== null;
    }

    public function updateSettings(): bool
    {
        $this->profile->analytics_account_id = $this->analyticsAccount;
        $this->profile->analytics_account_name = $this->analyticsAccounts[$this->analyticsAccount];
        $this->profile->analytics_property_id = $this->analyticsProperty;
        $this->profile->analytics_property_name = $this->analyticsProperties[$this->analyticsAccount][$this->analyticsProperty];
        $this->profile->analytics_view_id = $this->analyticsView;
        $this->profile->analytics_view_name = $this->analyticsViews[$this->analyticsProperty][$this->analyticsView];

        $allFormAnalyticsGoals = array_merge([$this->mainAnalyticsGoal], array_values($this->additionalAnalyticsGoals));
        $allFormAnalyticsGoals = array_unique($allFormAnalyticsGoals);

        // Удаление  целей перед сохранением
        $profileGoals = $this->profile->goals;
        foreach ($profileGoals as $profileGoal) {
            $profileGoal->delete();
        }

        foreach ($allFormAnalyticsGoals as $goal) {

            if ($goal === '') {
                continue;
            }

            // Сохранение новой цели
            $googleAnalyticsProfileGoal = new GoogleAnalyticsProfileGoal();
            $googleAnalyticsProfileGoal->company_id = $this->company->id;
            $googleAnalyticsProfileGoal->profile_id = $this->profile->id;
            $googleAnalyticsProfileGoal->external_id = $goal;
            $googleAnalyticsProfileGoal->name = $this->analyticsGoals[$this->analyticsView][$goal];
            $googleAnalyticsProfileGoal->save();
        }

        if ($this->profile->save()) {
            return true;
        }

        $this->addError('analyticsAccount', 'Произошла ошибка при сохранении настроек.');

        return false;
    }

    /**
     * @return GoogleAnalyticsProfile
     */
    public function getProfile(): GoogleAnalyticsProfile
    {
        if ($this->_profile === null) {
            $this->_profile = GoogleAnalyticsProfile::findByCompany($this->company) ?: new GoogleAnalyticsProfile();
        }

        return $this->_profile;
    }
}
