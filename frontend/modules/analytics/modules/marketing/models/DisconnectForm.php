<?php

namespace frontend\modules\analytics\modules\marketing\models;

use common\models\Company;
use common\modules\import\components\ImportCommandType;
use common\modules\marketing\models\MarketingSettings;
use yii\base\Model;

class DisconnectForm extends Model
{
    /**
     * @var Company
     */
    private $company;

    /**
     * @var string
     */
    private $integrationName;

    /**
     * @var ImportCommandType
     */
    private $commandType;

    /**
     * @param Company $company
     * @param ImportCommandType $commandType
     * @param string $integrationName
     */
    public function __construct(Company $company, ImportCommandType $commandType, string $integrationName)
    {
        $this->company = $company;
        $this->commandType = $commandType;
        $this->integrationName = $integrationName;

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function removeIntegration(): bool
    {
        MarketingSettings::deleteAll([
            'company_id' => $this->company->id,
            'command_type' => $this->commandType->getCommandId(),
        ]);

        $integrationData = $this->company->getIntegrationData($this->integrationName);
        $integrationData->cleanConfig();

        return $integrationData->saveConfig();
    }
}
