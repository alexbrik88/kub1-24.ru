<?php

namespace frontend\modules\analytics\modules\marketing\models;

use common\models\Company;
use yii\base\Model;

abstract class AbstractSettingsForm extends Model implements SettingsFormInterface
{
    /**
     * @var Company
     */
    protected $company;

    /**
     * @param Company $company
     * @param array $config
     */
    public function __construct(Company $company, array $config = [])
    {
        $this->company = $company;

        parent::__construct($config);
    }

    /**
     * @param Company $company
     * @return SettingsFormInterface
     */
    public static function createSettingsForm(Company $company): SettingsFormInterface
    {
        return new static($company);
    }

    /**
     * @inheritDoc
     */
    public function updateSettings(): bool
    {
        $accountId = $this->getAccountId();

        if ($accountId) {
            $integrationData = $this->company->getIntegrationData($this->getIntegrationName());
            $integrationData->setAccountId($accountId);

            return $integrationData->saveConfig();
        }

        return false;
    }

    /**
     * @return string
     */
    abstract protected function getIntegrationName(): string;

    /**
     * @return string|null
     */
    abstract public function getAccountId(): ?string;
}
