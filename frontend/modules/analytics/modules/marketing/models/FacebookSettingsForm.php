<?php

namespace frontend\modules\analytics\modules\marketing\models;

use common\models\employee\Employee;
use common\modules\marketing\components\FacebookIntegration;
use yii\helpers\ArrayHelper;

class FacebookSettingsForm extends AbstractSettingsForm
{
    /**
     * @var string|null
     */
    public $account_id;

    /**
     * @var string[]
     */
    private $_accountList;

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'account_id' => 'Рекламный аккаунт',
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['account_id'], 'required'],
            [['account_id'], 'in', 'range' => array_keys($this->getAccountList())],
        ];
    }

    /**
     * @return string[]
     */
    public function getAccountList(): array
    {
        if ($this->_accountList === null) {
            $token = $this->company->getIntegrationData($this->getIntegrationName())->getAccessToken();

            if ($token) {
                $integration = new FacebookIntegration($token);
                $accounts = $integration->getAdAccounts();
                $accounts = ArrayHelper::index($accounts, 'account_id');
                $this->_accountList = ArrayHelper::getColumn($accounts, 'name');
            }
        }

        return $this->_accountList;
    }

    /**
     * @return string|null
     */
    public function getAccountId(): ?string
    {
        return $this->account_id;
    }

    /**
     * @return string
     */
    protected function getIntegrationName(): string
    {
        return Employee::INTEGRATION_FACEBOOK;
    }
}
