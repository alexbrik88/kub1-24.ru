<?php

namespace frontend\modules\analytics\modules\marketing\models;

use Carbon\Carbon;
use common\models\employee\Employee;
use common\models\marketing\MarketingPlan;
use common\models\marketing\MarketingPlanItem;
use DateTimeInterface;
use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;

class MarketingPlanningForm extends Model {

    public $name;

    public $startDate;

    public $endDate;

    public $template;

    public $comment;

    public $currency;

    /**
     * @var array
     */
    public $channels;

    private Employee $employee;

    private MarketingPlan $model;

    public function __construct(MarketingPlan $marketingPlan, Employee $employee, $config = [])
    {
        $this->employee = $employee;
        $this->model = $marketingPlan;
        $this->name = $marketingPlan->name;
        $this->startDate = $marketingPlan->start_date;
        $this->endDate = $marketingPlan->end_date;
        $this->template = $marketingPlan->template;
        $this->comment = $marketingPlan->comment;
        $this->currency = $marketingPlan->currency;
        $this->channels = MarketingPlanItem::find()
            ->select('channel')
            ->andWhere(['marketing_plan_id' => $marketingPlan->id])
            ->groupBy('channel')
            ->column();
        parent::__construct($config);
    }

    public function rules(): array {
        return [
            [['name', 'startDate', 'endDate', 'channels', 'template', 'currency'], 'required'],
            [['name', 'comment'], 'string'],
            [['name'], 'unique',
                'targetClass' => MarketingPlan::class,
                'filter' => fn(ActiveQuery $query) => $query
                    ->andWhere(['NOT', ['id' => $this->model->id]])
                    ->andWhere(['name' => $this->name])
                    ->andWhere(['company_id' => $this->employee->company_id]),
                'message' => 'Такое название уже у вас есть. Измените название.',
            ],
            [['template'], 'in', 'range' => array_keys(MarketingPlan::$templateMap)],
            [['channels'], 'validateChannels'],
            [['currency'], 'in', 'range' => array_keys(MarketingPlan::$currencyMap)],
            [['startDate', 'endDate'], 'date', 'format' => 'php:Y-m-d'],
            [['startDate'], function (): void {
                $startDate = Carbon::createFromFormat('Y-m-d', $this->startDate);
                $endDate = Carbon::createFromFormat('Y-m-d', $this->endDate);
                if ($startDate->greaterThan($endDate)) {
                    $this->addError('startDate', 'Значение «Месяц окончания» не должно быть меньше значения «Месяц начала».');
                }
            }],
        ];
    }

    public function validateChannels(string $attribute): void {
        if (!is_array($this->$attribute)) {
            $this->addError($attribute, 'Неверное значение.');
            return;
        }

        if (!empty(array_diff($this->$attribute, [
            MarketingPlan::CHANNEL_YANDEX_DIRECT,
            MarketingPlan::CHANNEL_GOOGLE_ADS,
            MarketingPlan::CHANNEL_VK,
            MarketingPlan::CHANNEL_FACEBOOK,
        ]))) {
            $this->addError($attribute, 'Неверное значение.');
        }
    }

    public function attributeLabels(): array
    {
        return [
            'name' => 'Название',
            'template' => 'Шаблон воронки продаж',
            'comment' => 'Комментарий',
            'channels' => 'Рекламные каналы',
            'currency' => 'Валюта отчета',
        ];
    }

    public function getMarketingPlan(): MarketingPlan
    {
        return $this->model;
    }

    public function save(): bool {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();
        $startDate = Carbon::createFromFormat('Y-m-d', $this->startDate);
        $endDate = Carbon::createFromFormat('Y-m-d', $this->endDate);
        $companyId = $this->employee->currentEmployeeCompany->company_id;
        $model = $this->model;
        if ($this->model->isNewRecord) {
            $model->company_id = $companyId;
            $model->name = $this->name;
            $model->start_date = $startDate->format('m.Y');
            $model->end_date = $endDate->format('m.Y');
            $model->comment = $this->comment;
            $model->responsible_employee_id = $this->employee->id;
            $model->currency = $this->currency;
            $model->template = $this->template;
            if (!$model->save()) {
                $transaction->rollBack();
                return false;
            }

            if (!$this->createMarketingPlanItems($model, $companyId, $startDate, $endDate)) {
                $transaction->rollBack();
                return false;
            }
        } else {
            $model = $this->model;
            $model->name = $this->name;
            $model->start_date = $startDate->format('m.Y');
            $model->end_date = $endDate->format('m.Y');
            $model->comment = $this->comment;
            $model->currency = $this->currency;

            MarketingPlanItem::deleteAll([
                'AND',
                ['marketing_plan_id' => $model->id],
                ['NOT', ['channel' => $this->channels]],
            ]);

            foreach ($model->marketingPlanItems as $marketingPlanItem) {
                $itemDate = Carbon::createFromFormat('Y-m-d', "{$marketingPlanItem->year}-{$marketingPlanItem->month}-01");
                if ($itemDate->lessThan($startDate) || $itemDate->greaterThan($endDate)) {
                    $marketingPlanItem->delete();
                }
            }

            if (!$this->createMarketingPlanItems($model, $companyId, $startDate, $endDate)) {
                $transaction->rollBack();
                return false;
            }

            $model->financial_result = (int)$model->getMarketingPlanItems()->sum('financial_result');
            if (!$model->save()) {
                $transaction->rollBack();
                return false;
            }
        }

        $transaction->commit();

        return true;
    }

    private function createMarketingPlanItems(
        MarketingPlan $model,
        int $companyId,
        DateTimeInterface $startDate,
        DateTimeInterface $endDate
    ): bool {
        $startMonth = $startDate->format('m');
        $startYear = $startDate->format('Y');
        do {
            foreach ($this->channels as $channel) {
                if (!$model->getMarketingPlanItems()
                    ->andWhere(['channel' => $channel])
                    ->andWhere(['month' => $startDate->format('m')])
                    ->andWhere(['year' => $startDate->format('Y')])
                    ->exists()) {
                    /** @var MarketingPlanItem $anotherChannelPlanItem */
                    $anotherChannelPlanItem = $model->getMarketingPlanItems()
                        ->andWhere(['NOT', ['channel' => $channel]])
                        ->andWhere(['month' => $startDate->format('m')])
                        ->andWhere(['year' => $startDate->format('Y')])
                        ->one();

                    $planItem = new MarketingPlanItem();
                    $planItem->company_id = $companyId;
                    $planItem->marketing_plan_id = $model->id;
                    $planItem->channel = $channel;
                    $planItem->month = $startDate->format('m');
                    $planItem->year = $startDate->format('Y');
                    if ($anotherChannelPlanItem !== null) {
                        $planItem->average_check = $anotherChannelPlanItem->average_check;
                        $planItem->churn_rate = $anotherChannelPlanItem->churn_rate;
                        if ($startMonth === $startDate->format('m') && $startYear === $startDate->format('Y')) {
                            $planItem->repeated_purchases_count = $anotherChannelPlanItem->repeated_purchases_count;
                        }
                    }

                    if (!$planItem->save()) {
                        return false;
                    }
                }
            }

            $startDate->addMonth();
        } while ($startDate->lessThanOrEqualTo($endDate));

        return true;
    }
}
