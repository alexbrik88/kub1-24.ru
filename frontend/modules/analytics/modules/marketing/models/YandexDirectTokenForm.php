<?php

namespace frontend\modules\analytics\modules\marketing\models;

use common\models\company\IntegrationData;
use common\models\employee\Employee;
use common\modules\import\factories\DateTimeFactory;
use common\modules\marketing\components\YandexDirectComponent;

class YandexDirectTokenForm extends AbstractTokenForm
{
    /**
     * @inheritDoc
     */
    protected function afterFetchToken(IntegrationData $integrationData): void
    {
        $integrationData->setAccountId('0'); // TODO: Это костыль
    }

    /**
     * @inheritDoc
     */
    protected function getAccessToken(): string
    {
        $factory = new DateTimeFactory();
        $component = YandexDirectComponent::getInstance();
        $accessToken = $component->getAccessToken($this->code);
        $component->setAccessToken($accessToken);
        $component->getCampaignPerformanceReport($factory->createDateFrom(), $factory->createDateTo());

        return $accessToken;
    }

    /**
     * @inheritDoc
     */
    protected function getIntegrationModule(): string
    {
        return Employee::INTEGRATION_YANDEX_DIRECT;
    }
}
