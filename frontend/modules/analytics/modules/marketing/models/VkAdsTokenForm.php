<?php

namespace frontend\modules\analytics\modules\marketing\models;

use common\modules\marketing\components\VkAdsComponent;
use common\models\employee\Employee;

class VkAdsTokenForm extends AbstractTokenForm
{
    /**
     * @inheritDoc
     */
    protected function getAccessToken(): string
    {
        return VkAdsComponent::getInstance()->getAccessToken($this->code);
    }

    /**
     * @inheritDoc
     */
    protected function getIntegrationModule(): string
    {
        return Employee::INTEGRATION_VK;
    }
}
