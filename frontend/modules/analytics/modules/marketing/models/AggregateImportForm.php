<?php

namespace frontend\modules\analytics\modules\marketing\models;

use common\models\employee\Employee;
use common\models\FormInterface;
use common\modules\import\commands\ImportCommandInterface;
use common\modules\import\components\ImportCommandManager;
use common\modules\import\models\AbstractPeriodForm;
use common\modules\marketing\commands\FacebookImportCommand;
use common\modules\marketing\commands\GoogleAdsImportCommand;
use common\modules\marketing\commands\VkAdsImportCommand;
use common\modules\marketing\commands\YandexDirectImportCommand;

final class AggregateImportForm extends AbstractPeriodForm implements FormInterface
{
    /** @var string[]|ImportCommandInterface[] */
    private const COMMAND_LIST = [
        Employee::INTEGRATION_YANDEX_DIRECT => YandexDirectImportCommand::class,
        Employee::INTEGRATION_GOOGLE_ADS => GoogleAdsImportCommand::class,
        Employee::INTEGRATION_FACEBOOK => FacebookImportCommand::class,
        Employee::INTEGRATION_VK => VkAdsImportCommand::class,
    ];

    /**
     * @var Employee
     */
    private Employee $employee;

    /**
     * @param Employee $employee
     */
    public function __construct(Employee $employee)
    {
        $this->employee = $employee;

        parent::__construct();
    }

    /**
     * @return bool
     */
    public function createJob(): bool
    {
        $manager = ImportCommandManager::getInstance();

        foreach (self::COMMAND_LIST as $integrationName => $commandClass) {
            $integrationData = $this->employee->company->getIntegrationData($integrationName);

            if ($integrationData->hasAccessToken() && $integrationData->hasAccountId()) {
                $form = new ImportForm($this->employee, $manager->getTypeByClass($commandClass));
                $form->dateFrom = $this->dateFrom;
                $form->dateTo = $this->dateTo;

                if (!$form->createJob()) {
                    return false;
                }
            }
        }

        return true;
    }
}
