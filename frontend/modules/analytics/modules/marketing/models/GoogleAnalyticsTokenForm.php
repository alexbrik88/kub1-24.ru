<?php

namespace frontend\modules\analytics\modules\marketing\models;

use common\modules\import\models\TokenFormInterface;
use common\modules\marketing\components\GoogleAnalyticsComponent;
use common\models\Company;
use common\modules\marketing\models\GoogleAnalyticsProfile;
use GuzzleHttp\Exception\BadResponseException;
use yii\base\Model;

/**
 * @property-read GoogleAnalyticsProfile $profile
 */
class GoogleAnalyticsTokenForm extends Model implements TokenFormInterface
{
    /**
     * @var string|null
     */
    public $code;

    /**
     * @var Company
     */
    private $company;

    /**
     * @var GoogleAnalyticsProfile
     */
    private $_profile;

    /**
     * @param Company $company
     * @param array $config
     */
    public function __construct(Company $company, array $config = [])
    {
        $this->company = $company;

        parent::__construct($config);
    }

    /**
     * @inheritDoc
     */
    public static function createTokenForm(Company $company): TokenFormInterface
    {
        return new static($company);
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['code'], 'string'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function updateToken(): bool
    {
        try {
            $component = GoogleAnalyticsComponent::getInstance();
            $credentials = $component->getCredentials($this->code);
            $userInfo = $component->getUserInfo($credentials['access_token']);

            $this->profile->setAttributes([
                'company_id' => $this->company->id,
                'user_name' => $userInfo->getName(),
                'avatar' => $userInfo->getPicture(),
                'access_token' => $credentials['access_token'],
                'expires_at' => time() + $credentials['expires_in'],
                'refresh_token' => $credentials['refresh_token'],
            ], false);

            return $this->profile->save();
        } catch (BadResponseException $exception) {
            return false;
        }
    }

    /**
     * @return GoogleAnalyticsProfile
     */
    public function getProfile(): GoogleAnalyticsProfile
    {
        if ($this->_profile === null) {
            $this->_profile = GoogleAnalyticsProfile::findByCompany($this->company) ?: new GoogleAnalyticsProfile();
        }

        return $this->_profile;
    }
}
