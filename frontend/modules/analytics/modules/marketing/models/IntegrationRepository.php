<?php

namespace frontend\modules\analytics\modules\marketing\models;

use common\data\SortBuilder;
use common\models\Company;
use common\models\employee\Employee;
use common\models\RepositoryInterface;
use common\modules\marketing\models\MarketingChannel;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\data\DataProviderInterface;
use yii\data\Sort;

class IntegrationRepository extends Model implements RepositoryInterface
{
    /** @var string[] */
    private const INTEGRATION_LIST = [
        MarketingChannel::FACEBOOK => Employee::INTEGRATION_FACEBOOK,
        MarketingChannel::GOOGLE_AD => Employee::INTEGRATION_GOOGLE_ADS,
        MarketingChannel::YANDEX_AD => Employee::INTEGRATION_YANDEX_DIRECT,
        MarketingChannel::VKONTAKTE => Employee::INTEGRATION_VK,
    ];

    /**
     * @var Company
     */
    private Company $company;

    /**
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;

        parent::__construct([]);
    }

    /**
     * @inheritDoc
     */
    public function getDataProvider(): DataProviderInterface
    {
        return new ArrayDataProvider([
            'allModels' => $this->createModels(),
            'sort' => $this->getSort(),
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        $attributes = ['name', 'isActive', 'balance', 'order', 'totalCost', 'totalImpressions', 'totalClicks', 'totalCtr'];
        $builder = new SortBuilder();
        $builder->setAttributes(array_combine($attributes, $attributes), 'order');
        $builder->setDefaultOrder('isActive', 'isActive', SORT_ASC, 'order');

        return $builder->buildSort();
    }

    /**
     * @return Integration[]
     */
    private function createModels(): array
    {
        $models = [];

        foreach (self::INTEGRATION_LIST as $channelId => $integrationName) {
            $models[] = new Integration($this->company, $integrationName, $channelId);
        }

        return $models;
    }
}
