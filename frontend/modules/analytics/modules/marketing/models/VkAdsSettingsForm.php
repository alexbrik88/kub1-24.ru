<?php

namespace frontend\modules\analytics\modules\marketing\models;

use common\modules\marketing\components\VkAdsComponent;
use common\models\employee\Employee;
use yii\helpers\ArrayHelper;

class VkAdsSettingsForm extends AbstractSettingsForm
{
    /**
     * @var string|null
     */
    public $account_id;

    /**
     * @var string[]|null
     */
    private $_accountList;

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'account_id' => 'Название кабинета',
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['account_id'], 'required'],
            [['account_id'], 'string'],
            [['account_id'], 'in', 'range' => array_keys($this->getAccountList())],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getAccountId(): ?string
    {
        return $this->account_id;
    }

    /**
     * @return array
     * @throws
     */
    public function getAccountList(): array
    {
        if ($this->_accountList === null) {
            $accessToken = $this->company->getIntegrationData(Employee::INTEGRATION_VK)->getAccessToken();
            $accounts = VkAdsComponent::getInstance()->getAccounts($accessToken);
            $accounts = ArrayHelper::index($accounts, 'account_id');
            $this->_accountList = ArrayHelper::getColumn($accounts, 'account_name', true);
        }

        return $this->_accountList;
    }

    /**
     * @inheritDoc
     */
    protected function getIntegrationName(): string
    {
        return Employee::INTEGRATION_VK;
    }
}
