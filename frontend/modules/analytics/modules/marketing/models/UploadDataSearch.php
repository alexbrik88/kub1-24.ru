<?php

namespace frontend\modules\analytics\modules\marketing\models;

use common\models\Company;
use common\models\employee\Employee;
use common\models\FormInterface;
use common\modules\marketing\models\GoogleAnalyticsProfile;
use common\modules\marketing\models\MarketingChannel;
use yii\base\Model;
use yii\helpers\Url;

class UploadDataSearch extends Model implements FormInterface
{
    /**
     * @var Company
     */
    public $company;

    /**
     * @param Company $company
     * @param array $config
     */
    public function __construct(Company $company, array $config = [])
    {
        $this->company = $company;

        parent::__construct($config);
    }

    public function search()
    {
        $profile = GoogleAnalyticsProfile::findByCompany($this->company);

        return [
            [
                'channel' => MarketingChannel::GOOGLE_ANALYTICS,
                'logo' => '/images/google-analytics-logo.png',
                'logoWidth' => 100,
                'name' => null,
                'linkClass' => $profile === null
                    ? null
                    : 'connect-google-analytics',
                'status' => ($profile !== null && $profile->analytics_account_id !== null),
                'url' => ($profile && $profile->refresh_token)
                    ? Url::to('/analytics/marketing/default/google-analytics-settings')
                    : Url::to('/analytics/marketing/default/analytics-auth'),
                'disconnectUrl' => Url::to('/analytics/marketing/default/google-analytics-settings'),
                'disconnectText' => 'Настройки',
                'disconnectLinkClass' => 'connect-google-analytics',
                'isImplemented' => true,
                'description' => 'Информация по лидам передается в КУБ-24 из целей Google Analytics вашего сайта.<br>
                    Для корректной работы на вашем сайте установите счетчик Google Analytics и настройте цель,
                    соответствующую лиду (заявке).<br>
                    При подключении Google Analytics предоставьте доступ к вашему Google аккаунту, далее:<br>
                    выберите аккаунт Google Analytics,<br>
                    Ресурс,<br>
                    представление Google Analytics,<br>
                    цель настроенную в представлении как лид',
            ],
            [
                'channel' => MarketingChannel::YANDEX_AD,
                'logo' => '/images/yandex_direct_logo.png',
                'logoWidth' => 100,
                'name' => null,
                'status' => $this->getIntegrationStatus(Employee::INTEGRATION_YANDEX_DIRECT),
                'url' => Url::to('/analytics/marketing/yandex-direct'),
                'disconnectUrl' => Url::to('/analytics/marketing/yandex-direct/disconnect'),
                'isImplemented' => true,
                'description' => 'Позволяет получить подробные данные по всем параметрам, включая расходы с детализацией до кампаний',
            ],
            [
                'channel' => MarketingChannel::GOOGLE_AD,
                'logo' => '/images/google_words_logo.png',
                'logoWidth' => 100,
                'name' => null,
                'status' => $this->getIntegrationStatus(Employee::INTEGRATION_GOOGLE_ADS),
                'url' => Url::to('/analytics/marketing/google-words'),
                'disconnectUrl' => Url::to('/analytics/marketing/google-words/disconnect'),
                'isImplemented' => true,
                'description' => 'Позволяет получить подробные данные по всем параметрам, включая расходы с детализацией до кампаний',
            ],
            [
                'channel' => MarketingChannel::FACEBOOK,
                'logo' => '/images/fb-insta-logo.png',
                'logoWidth' => 30,
                'name' => 'Facebook/Instagram',
                'status' => $this->getIntegrationStatus(Employee::INTEGRATION_FACEBOOK),
                'url' => Url::to('/analytics/marketing/facebook'),
                'disconnectUrl' => Url::to('/analytics/marketing/facebook/disconnect'),
                'isImplemented' => true,
                'description' => 'Позволяет получить подробные данные по всем параметрам, включая расходы с детализацией до кампаний',
            ],
            [
                'channel' => MarketingChannel::VKONTAKTE,
                'logo' => '/images/vk_logo.png',
                'logoWidth' => 30,
                'name' => 'Вконтакте',
                'status' => $this->getIntegrationStatus(Employee::INTEGRATION_VK),
                'url' => Url::to('/analytics/marketing/vk-ads'),
                'disconnectUrl' => Url::to('/analytics/marketing/vk-ads/disconnect'),
                'isImplemented' => true,
                'description' => 'Позволяет получить подробные данные по всем параметрам, включая расходы с детализацией до кампаний',
            ],
            [
                'channel' => MarketingChannel::YANDEX_AD,
                'logo' => '/images/yandex-market-logo.svg',
                'logoWidth' => 100,
                'name' => null,
                'status' => $this->getIntegrationStatus(Employee::INTEGRATION_YANDEX_DIRECT),
                'url' => Url::to('/analytics/marketing/yandex-direct'),
                'disconnectUrl' => Url::to('/analytics/marketing/yandex-direct/disconnect'),
                'isImplemented' => true,
                'description' => 'Позволяет получить подробные данные по всем параметрам, включая расходы с детализацией до кампаний',
            ],
            [
                'logo' => '/images/mytarget-logo.png',
                'logoWidth' => 100,
                'name' => 'MyTarget',
                'status' => false,
                'url' => null,
                'isImplemented' => false,
                'description' => 'Позволяет получить подробные данные по всем параметрам, включая расходы с детализацией до кампаний',
            ],
            [
                'logo' => '/images/criteo-logo.png',
                'logoWidth' => 100,
                'name' => null,
                'status' => false,
                'url' => null,
                'isImplemented' => false,
                'description' => 'Позволяет получить подробные данные по всем параметрам, включая расходы с детализацией до кампаний',
            ],
        ];
    }

    /**
     * @param string $module
     * @return bool
     */
    private function getIntegrationStatus(string $module): bool
    {
        $integrationData = $this->company->getIntegrationData($module);

        return ($integrationData->hasAccessToken() && $integrationData->hasAccountId());
    }
}
