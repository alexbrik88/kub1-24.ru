<?php

namespace frontend\modules\analytics\modules\marketing\models;

use common\components\TextHelper;
use common\models\employee\Employee;
use common\models\marketing\MarketingCalculatingPlanningSetting;
use common\models\marketing\MarketingPlan;
use common\models\marketing\MarketingPlanItem;
use Yii;
use yii\base\Model;

class UpdatePlanningMarketingForm extends Model
{
    public const AUTO_PLANNING_ACTION_START_AMOUNT = 0;
    public const AUTO_PLANNING_ACTION_PLUS = 1;
    public const AUTO_PLANNING_ACTION_MINUS = 2;
    public const AUTO_PLANNING_ACTION_MULTIPLY = 3;

    public static array $autoPlanningActionsMap = [
        self::AUTO_PLANNING_ACTION_START_AMOUNT => '=Нач. значение',
        self::AUTO_PLANNING_ACTION_PLUS => '+',
        self::AUTO_PLANNING_ACTION_MINUS => '-',
        self::AUTO_PLANNING_ACTION_MULTIPLY => '*',
    ];

    /**
     * @var array
     */
    public $items;

    /**
     * @var array
     */
    public $averageCheck;

    /**
     * @var array
     */
    public $churnRate;

    /**
     * @var array
     */
    public $repeatedPurchasesCount;

    private MarketingPlan $marketingPlan;

    private int $channel;

    public function __construct(MarketingPlan $marketingPlan, int $channel, $config = [])
    {
        $this->marketingPlan = $marketingPlan;
        $this->channel = $channel;
        $this->items = $marketingPlan->getMarketingPlanItems()
            ->andWhere(['channel' => $this->channel])
            ->orderBy([
                'year' => SORT_ASC,
                'month' => SORT_ASC,
            ])
            ->indexBy('id')
            ->asArray()
            ->all();
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['items'], 'required'],
            [['items'], 'validateItems'],
        ];
    }

    public function validateItems(string $attributeName): void {
        $itemsIds = array_keys($this->items);
        if (!empty(array_diff($itemsIds, $this->marketingPlan->getMarketingPlanItems()
            ->select('id')
            ->andWhere(['channel' => $this->channel])
            ->column()))) {
            $this->addError($attributeName, 'Неверный формат данных в таблице.');
        }
    }

    public function getChannel(): int {
        return $this->channel;
    }

    public function getMarketingPlan(): MarketingPlan {
        return $this->marketingPlan;
    }

    public function update(): bool {
        if (!$this->validate()) {
            \common\components\helpers\ModelHelper::logErrors($this, __METHOD__);
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        $prevCumulativeFinancialAmount = 0;
        $prevMarketingPlanningItem = null;
        $firstRepeatedPurchaseCount = null;
        foreach ($this->items as $marketingPlanItemId => $marketingPlanItemData) {
            /** @var MarketingPlanItem $marketingPlanningItem */
            $marketingPlanningItem = $this->marketingPlan->getMarketingPlanItems()
                ->andWhere(['channel' => $this->channel])
                ->andWhere(['id' => $marketingPlanItemId])
                ->one();
            if ($prevMarketingPlanningItem === null) {
                $firstRepeatedPurchaseCount = (int)($this->repeatedPurchasesCount[$marketingPlanningItem->month . $marketingPlanningItem->year] ?? 0);
            } else {
                $firstRepeatedPurchaseCount = 0;
            }

            $calculatedData = $this->calculateItems(
                $marketingPlanItemData,
                $this->averageCheck[$marketingPlanningItem->month . $marketingPlanningItem->year] ?? 0,
                $this->churnRate[$marketingPlanningItem->month . $marketingPlanningItem->year] ?? 0,
                $firstRepeatedPurchaseCount,
                $prevCumulativeFinancialAmount,
                $prevMarketingPlanningItem,
            );
            $marketingPlanningItem->budget_amount = $calculatedData['budget_amount'];
            $marketingPlanningItem->click_amount = $calculatedData['click_amount'];
            $marketingPlanningItem->clicks_count = $calculatedData['clicks_count'];
            $marketingPlanningItem->registration_conversion = $calculatedData['registration_conversion'];
            $marketingPlanningItem->registration_count = $calculatedData['registration_count'];
            $marketingPlanningItem->registration_amount = $calculatedData['registration_amount'];
            $marketingPlanningItem->active_user_conversion = $calculatedData['active_user_conversion'];
            $marketingPlanningItem->active_users_count = $calculatedData['active_users_count'];
            $marketingPlanningItem->active_user_amount = $calculatedData['active_user_amount'];
            $marketingPlanningItem->purchase_conversion = $calculatedData['purchase_conversion'];
            $marketingPlanningItem->purchases_count = $calculatedData['purchases_count'];
            $marketingPlanningItem->sale_amount = $calculatedData['sale_amount'];
            $marketingPlanningItem->marketing_service_amount = $calculatedData['marketing_service_amount'];
            $marketingPlanningItem->additional_expenses_amount = $calculatedData['additional_expenses_amount'];
            $marketingPlanningItem->total_additional_expenses_amount = $calculatedData['total_additional_expenses_amount'];
            $marketingPlanningItem->additional_expenses_amount_for_one_sale = $calculatedData['additional_expenses_amount_for_one_sale'];
            $marketingPlanningItem->average_check = $calculatedData['average_check'];
            $marketingPlanningItem->proceeds_from_new_amount = $calculatedData['proceeds_from_new_amount'];
            $marketingPlanningItem->churn_rate = $calculatedData['churn_rate'];
            $marketingPlanningItem->repeated_purchases_amount = $calculatedData['repeated_purchases_amount'];
            $marketingPlanningItem->total_proceeds_amount = $calculatedData['total_proceeds_amount'];
            $marketingPlanningItem->financial_result = $calculatedData['financial_result'];
            $marketingPlanningItem->cumulative_financial_amount = $calculatedData['cumulative_financial_amount'];
            $marketingPlanningItem->total_purchases_count = $calculatedData['total_purchases_count'];
            $marketingPlanningItem->repeated_purchases_count = $calculatedData['repeated_purchases_count'];
            $marketingPlanningItem->salesperson_salary_amount = $calculatedData['salesperson_salary_amount'];
            $prevCumulativeFinancialAmount = $marketingPlanningItem->cumulative_financial_amount;
            if (!$marketingPlanningItem->save()) {
                $transaction->rollBack();
                \common\components\helpers\ModelHelper::logErrors($marketingPlanningItem, __METHOD__);
                return false;
            }

            $prevMarketingPlanningItem = $calculatedData;
        }

        $marketingPlan = $this->getMarketingPlan();
        $marketingPlan->financial_result = $marketingPlan->getMarketingPlanItems()->sum('financial_result');
        if (!$marketingPlan->save()) {
            \common\components\helpers\ModelHelper::logErrors($marketingPlan, __METHOD__);
            $transaction->rollBack();
            return false;
        }

        $transaction->commit();

        return true;
    }

    public function calculateItems(
        array $marketingPlanItemData,
        string $averageCheck,
        string $churnRate,
        int $repeatedPurchaseCount,
        int $prevCumulativeFinancialAmount,
        array $prevMarketingPlanningItem = null
    ): array {
        $budgetAmount = (int)$this->sanitize($marketingPlanItemData['budget_amount']);
        $marketingServiceAmount = (int)$this->sanitize($marketingPlanItemData['marketing_service_amount']);
        $additionalExpensesAmount = (int)$this->sanitize($marketingPlanItemData['additional_expenses_amount']);
        $salespersonSalaryAmount = (int)$this->sanitize($marketingPlanItemData['salesperson_salary_amount']);
        $totalAdditionalExpensesAmount = $marketingServiceAmount + $additionalExpensesAmount + $salespersonSalaryAmount;
        $clickAmount = (int)$this->sanitize($marketingPlanItemData['click_amount'] ?? 0);
        $clicksCount = $clickAmount ? (int)round($budgetAmount / $clickAmount) : 0;
        $registrationConversion = (float)($this->sanitize($marketingPlanItemData['registration_conversion']) / 100);
        $registrationCount = (int)round($clicksCount * ($registrationConversion / 100));
        $activeUserConversion = isset($marketingPlanItemData['active_user_conversion'])
            ? (float)($this->sanitize($marketingPlanItemData['active_user_conversion']) / 100)
            : 0;
        $activeUsersCount = (int)round($registrationCount * ($activeUserConversion / 100));
        $purchaseConversion = (float)($this->sanitize($marketingPlanItemData['purchase_conversion']??0) / 100);
        $purchasesCount = 0;
        switch($this->marketingPlan->template) {
            case MarketingPlan::TEMPLATE_ONLINE_SERVICE:
                $purchasesCount = (int)round($activeUsersCount * ($purchaseConversion / 100));
                break;
            case MarketingPlan::TEMPLATE_ONLINE_STORE:
                $purchasesCount = (int)round($registrationCount * ($purchaseConversion / 100));
                break;
        }

        $averageCheck = (int)$this->sanitize($averageCheck);
        $proceedsFromNewAmount = $averageCheck * $purchasesCount;
        $churnRate = (float)($this->sanitize($churnRate) / 100);
        if ($prevMarketingPlanningItem !== null) {
            $repeatedPurchasesCount = $churnRate > 0 ? (int)round($prevMarketingPlanningItem['total_purchases_count'] - ($prevMarketingPlanningItem['total_purchases_count'] / 100 * $churnRate)) : 0;
        } else {
            $repeatedPurchasesCount = $repeatedPurchaseCount;
        }

        $repeatedPurchasesAmount = $repeatedPurchasesCount * $averageCheck;
        $totalPurchasesCount = $purchasesCount + $repeatedPurchasesCount;
        $totalProceedsAmount = $proceedsFromNewAmount + $repeatedPurchasesAmount;
        $financialResult = $proceedsFromNewAmount - $budgetAmount - $totalAdditionalExpensesAmount;

        return [
            'budget_amount' => $budgetAmount,
            'click_amount' => $clickAmount,
            'clicks_count' => $clicksCount,
            'registration_conversion' => $registrationConversion,
            'registration_count' => $registrationCount,
            'registration_amount' => $registrationCount ? (int)round($budgetAmount / $registrationCount) : 0,
            'active_user_conversion' => $activeUserConversion,
            'active_users_count' => $activeUsersCount,
            'active_user_amount' => $activeUsersCount ? (int)round($budgetAmount / $activeUsersCount) : 0,
            'purchase_conversion' => $purchaseConversion,
            'purchases_count' => $purchasesCount,
            'sale_amount' => $purchasesCount ? (int)round($budgetAmount / $purchasesCount) : 0,
            'marketing_service_amount' => $marketingServiceAmount,
            'additional_expenses_amount' => $additionalExpensesAmount,
            'salesperson_salary_amount' => $salespersonSalaryAmount,
            'total_additional_expenses_amount' => $totalAdditionalExpensesAmount,
            'additional_expenses_amount_for_one_sale' => $purchasesCount ? (int)round($totalAdditionalExpensesAmount / $purchasesCount) : 0,
            'average_check' => $averageCheck,
            'proceeds_from_new_amount' => $proceedsFromNewAmount,
            'churn_rate' => $churnRate,
            'repeated_purchases_amount' => $repeatedPurchasesAmount,
            'total_proceeds_amount' => $totalProceedsAmount,
            'financial_result' => $financialResult,
            'cumulative_financial_amount' => $prevCumulativeFinancialAmount + $financialResult,
            'total_purchases_count' => $totalPurchasesCount,
            'repeated_purchases_count' => $repeatedPurchasesCount,
        ];
    }

    public function calculateAutoFill(array $autoFillData, int $channel): bool {
        $attribute = $autoFillData['attr'];
        $startYear = substr($autoFillData['month_start'], 0, 4);
        $startMonth = substr($autoFillData['month_start'], 4, 6);
        $action = $autoFillData['action'];
        $startAmount = (int)$this->sanitize($autoFillData['start_amount']);
        $amount = isset($autoFillData['amount']) ? (int)$this->sanitize($autoFillData['amount']) : null;
        $limitAmount = isset($autoFillData['limit_amount']) && $autoFillData['limit_amount'] !== ''
            ? (int)$this->sanitize($autoFillData['limit_amount'])
            : 0;
        $marketingCalculatingPlanningSetting = MarketingCalculatingPlanningSetting::find()
            ->andWhere(['marketing_plan_id' => $this->marketingPlan->id])
            ->andWhere(['channel' => $channel])
            ->andWhere(['attribute' => $attribute])
            ->one();
        if ($marketingCalculatingPlanningSetting === null) {
            $marketingCalculatingPlanningSetting = new MarketingCalculatingPlanningSetting();
            $marketingCalculatingPlanningSetting->marketing_plan_id = $this->marketingPlan->id;
            $marketingCalculatingPlanningSetting->channel = $channel;
            $marketingCalculatingPlanningSetting->attribute = $attribute;
        }

        $marketingCalculatingPlanningSetting->start_amount = $startAmount;
        $marketingCalculatingPlanningSetting->action = $action;
        $marketingCalculatingPlanningSetting->start_month = $startMonth;
        $marketingCalculatingPlanningSetting->start_year = $startYear;
        $marketingCalculatingPlanningSetting->amount = $amount;
        $marketingCalculatingPlanningSetting->limit_amount = $limitAmount;
        if (!$marketingCalculatingPlanningSetting->save()) {
            return false;
        }

        /** @var MarketingPlanItem[] $marketingPlanItems */
        $marketingPlanItemsQuery = $this->marketingPlan->getMarketingPlanItems()
            ->andWhere(['>=', 'month', $startMonth])
            ->andWhere(['>=', 'year', $startYear]);
        if ($this->channel !== MarketingPlan::REPEATED_PURCHASES) {
            $marketingPlanItemsQuery->andWhere(['channel' => $channel]);
        }

        $marketingPlanItems = $marketingPlanItemsQuery->groupBy(['year', 'month'])->orderBy(['id' => SORT_ASC])->all();
        switch ($action) {
            case self::AUTO_PLANNING_ACTION_START_AMOUNT:
                foreach ($marketingPlanItems as $marketingPlanItem) {
                    $this->updateItemsByAutoPlanning($attribute, $marketingPlanItem, $startAmount / 100);
                }

                break;
            case self::AUTO_PLANNING_ACTION_PLUS:
                $i = 0;
                $calculatedAmount = $startAmount;
                foreach ($marketingPlanItems as $marketingPlanItem) {
                    $oldCalculatedAmount = $calculatedAmount;
                    if ($i !== 0) {
                        $calculatedAmount += $amount;
                    }

                    if ($limitAmount === 0 || $calculatedAmount <= $limitAmount) {
                        $resultAmount = $calculatedAmount / 100;
                    } else {
                        $resultAmount = $oldCalculatedAmount / 100;
                        $calculatedAmount = $oldCalculatedAmount;
                    }
                    $this->updateItemsByAutoPlanning($attribute, $marketingPlanItem, $resultAmount);

                    $i++;
                }
                break;
            case self::AUTO_PLANNING_ACTION_MINUS:
                $i = 0;
                $calculatedAmount = $startAmount;
                foreach ($marketingPlanItems as $marketingPlanItem) {
                    $oldCalculatedAmount = $calculatedAmount;
                    if ($i !== 0) {
                        $calculatedAmount = $calculatedAmount - $amount;
                    }

                    if ($limitAmount === 0 || $calculatedAmount >= $limitAmount) {
                        $resultAmount = $calculatedAmount / 100;
                    } else {
                        $resultAmount = $oldCalculatedAmount / 100;
                        $calculatedAmount = $oldCalculatedAmount;
                    }

                    $this->updateItemsByAutoPlanning($attribute, $marketingPlanItem, $resultAmount);

                    $i++;
                }
                break;
            case self::AUTO_PLANNING_ACTION_MULTIPLY:
                $amount = $amount / 100;
                $i = 0;
                $calculatedAmount = $startAmount;
                foreach ($marketingPlanItems as $marketingPlanItem) {
                    $oldCalculatedAmount = $calculatedAmount;
                    if ($i !== 0) {
                        $calculatedAmount = $calculatedAmount * $amount;
                    }

                    if ($limitAmount === 0 || $calculatedAmount <= $limitAmount) {
                        $resultAmount = $calculatedAmount / 100;
                    } else {
                        $resultAmount = $oldCalculatedAmount / 100;
                        $calculatedAmount = $oldCalculatedAmount;
                    }

                    $this->updateItemsByAutoPlanning($attribute, $marketingPlanItem, $resultAmount);

                    $i++;
                }
                break;
        }

        return true;
    }

    public function getRepeatedPurchasesItems(Employee $user): array {
        $this->items = (new PlanningMarketingSearch($user))->searchTotal($this->marketingPlan->id);
        if (isset($this->items['purchases_count_by_channels'])) {
            unset($this->items['purchases_count_by_channels']);
        }

        return $this->items;
    }

    public function sanitize(string $val)
    {
        return str_replace([',',' '], ['.',''], $val) * 100;
    }

    private function updateItemsByAutoPlanning(string $attribute, MarketingPlanItem $marketingPlanItem, $amount): void {
        switch($attribute) {
            case 'average_check':
                $this->averageCheck["{$marketingPlanItem->month}{$marketingPlanItem->year}"] = $amount;
                break;
            case 'churn_rate':
                $this->churnRate["{$marketingPlanItem->month}{$marketingPlanItem->year}"] = $amount;
                break;
            default:
                $this->items[$marketingPlanItem->id][$attribute] = $amount;
                break;
        }
    }
}
