<?php

namespace frontend\modules\analytics\modules\marketing\models;

use common\models\Company;
use common\modules\marketing\models\GoogleAnalyticsProfile;
use yii\base\Model;

class GoogleAnalyticsDisconnectForm extends Model
{
    /**
     * @var Company
     */
    private $company;

    /**
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;

        parent::__construct();
    }

    /**
     * @return bool
     * @throws
     */
    public function removeIntegration(): bool
    {
        $profile = GoogleAnalyticsProfile::findByCompany($this->company);

        if ($profile) {
            return $profile->delete();
        }

        return true;
    }
}
