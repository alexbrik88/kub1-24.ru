<?php

namespace frontend\modules\analytics\modules\marketing\commands;

use common\commands\CommandInterface;
use common\components\date\DateHelper;
use common\components\excel\Excel;
use common\models\Company;
use common\modules\marketing\models\GoogleAnalyticsProfileGoal;
use common\modules\marketing\models\MarketingChannel;
use common\modules\marketing\models\MarketingReportSearch;
use frontend\components\StatisticPeriod;

class XlsCommand implements CommandInterface
{
    /**
     * @var Company
     */
    private Company $company;

    /**
     * @var MarketingChannel
     */
    private MarketingChannel $channel;

    /**
     * @var string
     */
    private string $defaultSorting;

    /**
     * @var GoogleAnalyticsProfileGoal[]
     */
    private array $goals;

    /**
     * @param Company $company
     * @param MarketingChannel $channel
     * @param string $defaultSorting
     */
    public function __construct(Company $company, MarketingChannel $channel, string $defaultSorting)
    {
        $this->company = $company;
        $this->channel = $channel;
        $this->defaultSorting = $defaultSorting;
        $this->goals = $company->googleAnalyticsProfile->goals ?? [];
    }

    /**
     * @inheritDoc
     */
    public function run(): void
    {
        $searchModel = new MarketingReportSearch(['utm_campaign'], $this->defaultSorting);
        $dataProvider = $searchModel->search($this->channel->id, $this->company);
        $dataProvider->setPagination(false);

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $dataProvider->getModels(),
            'title' => $this->channel->name,
            'columns' => $this->getColumns(),
            'format' => 'Xlsx',
            'fileName' => $this->getFileName($this->channel->name),
        ]);
    }

    /**
     * @return array
     */
    private function getColumns(): array
    {
        $format = function ($value): string {
            return number_format($value, is_int($value) ? 0 : 2, ',', ' ');
        };

        $columns = [
            [
                'attribute' => 'campaign_name',
                'header' => 'Кампания',
            ],
            [
                'attribute' => 'utm_campaign',
                'header' => 'UTM Campaign'
            ],
            [
                'attribute' => 'cost',
                'header' => 'Расход, ₽',
                'format' => $format,
            ],
            [
                'attribute' => 'impressions',
                'header' => 'Показы',
                'format' => $format,
            ],
            [
                'attribute' => 'clicks',
                'header' => 'Клики',
                'format' => $format,
            ],
            [
                'attribute' => 'ctr',
                'header' => 'CTR, %',
                'format' => $format,
            ],
            [
                'attribute' => 'avg_cpc',
                'header' => 'Ср. цена клика, ₽',
                'format' => $format,
            ],
        ];

        foreach ($this->goals as $goal) {
            $columns = array_merge($columns, [
                [
                    'attribute' => "goal_{$goal->external_id}_total",
                    'header' => "{$goal->name}\rКол-во",
                    'format' => function ($value): string {
                        return number_format($value, 0, ',', ' ');
                    },
                ],
                [
                    'attribute' => "goal_{$goal->external_id}_conversion_rate",
                    'header' => "{$goal->name}\rКонверсия, %",
                    'format' => $format,
                ],
                [
                    'attribute' => "goal_{$goal->external_id}_roi",
                    'header' => "{$goal->name}\rЦена цели, ₽",
                    'format' => $format,
                ],
            ]);
        }

        return $columns;
    }

    /**
     * @param string $prefix
     * @return string
     */
    private function getFileName(string $prefix): string
    {
        return sprintf(
            '%s_%s-%s',
            str_replace(' ', '_', $prefix),
            StatisticPeriod::getDateFrom()->format(DateHelper::FORMAT_USER_DATE),
            StatisticPeriod::getDateTo()->format(DateHelper::FORMAT_USER_DATE)
        );
    }
}
