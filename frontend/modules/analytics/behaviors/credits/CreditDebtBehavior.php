<?php

namespace frontend\modules\analytics\behaviors\credits;

use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\analytics\models\credits\DebtCalculatorFactory;
use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;

/**
 * @property-read Credit $owner
 */
class CreditDebtBehavior extends Behavior
{
    /**
     * @var Credit
     */
    public $credit;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!is_a($this->credit, Credit::class, false)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
        ];
    }

    /**
     * @return void
     */
    public function beforeSave(): void
    {
        $calculator = (new DebtCalculatorFactory)->createCalculator($this->credit);
        $calculator->updateCredit();
    }
}
