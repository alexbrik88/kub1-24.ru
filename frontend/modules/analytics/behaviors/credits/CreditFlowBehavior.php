<?php

namespace frontend\modules\analytics\behaviors\credits;

use common\components\date\DateHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\Employee;
use frontend\components\BusinessAnalyticsAccess;
use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\analytics\models\credits\CreditFlow;
use frontend\modules\analytics\models\credits\CreditRepository;
use Yii;
use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

/**
 * @property CashBankFlows|CashOrderFlows|CashEmoneyFlows $owner
 * @property-read string[] $creditList
 */
class CreditFlowBehavior extends Behavior
{
    /**
     * @var int[]
     */
    public const INCOME_ITEM_ID = [
        InvoiceIncomeItem::ITEM_LOAN,
        InvoiceIncomeItem::ITEM_CREDIT,
    ];

    /**
     * @var int[]
     */
    public const EXPENDITURE_ITEM_ID = [
        InvoiceExpenditureItem::ITEM_LOAN_REPAYMENT,
        InvoiceExpenditureItem::ITEM_REPAYMENT_CREDIT,
        InvoiceExpenditureItem::ITEM_PAYMENT_PERCENT,
    ];

    /**
     * @var string
     */
    public $attribute;

    /**
     * @var int
     */
    public $wallet_id;

    /**
     * @var CreditFlow|null
     */
    private $_creditFlow;

    /**
     * @var string[]
     */
    private $_creditList;

    /**
     * @inheritDoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => [$this, 'afterFind'],
            ActiveRecord::EVENT_AFTER_INSERT => [$this, 'afterSave'],
            ActiveRecord::EVENT_AFTER_UPDATE => [$this, 'afterSave'],
            ActiveRecord::EVENT_AFTER_DELETE => [$this, 'afterDelete'],
        ];
    }

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->attribute) || empty($this->wallet_id)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     * @throws
     */
    public function afterSave(): void
    {
        if (self::flowCanBeCredit($this->owner)) {
            if ($this->owner->credit_id) {
                $this->_creditFlow = $this->_creditFlow ?? self::newCreditFlow($this);
                $this->_creditFlow->save(false);
                $this->_creditFlow->credit->save(false);
            } elseif ($this->_creditFlow) {
                $this->_creditFlow->delete();
                $this->_creditFlow->credit->save(false);
            }
        } else if ($this->_creditFlow) {
            $this->_creditFlow->delete();
            $this->_creditFlow->credit->save(false);
        }
    }

    /**
     * @inheritDoc
     */
    public function afterFind(): void
    {
        if (self::flowCanBeCredit($this->owner)) {
            $this->_creditFlow = CreditFlow::findOne([$this->attribute => $this->owner->id]);

            if ($this->_creditFlow) {
                $this->owner->credit_id = $this->_creditFlow->credit_id;
                $this->owner->credit_amount = $this->_creditFlow->credit->amountWithCurrency;
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function afterDelete(): void
    {
        if ($this->_creditFlow) {
            $this->_creditFlow->credit->save(false);
        }
    }

    /**
     * @param ActiveRecord|CashBankFlows|CashOrderFlows|CashEmoneyFlows $record
     * @return bool
     */
    public static function flowCanBeCredit(ActiveRecord $record): bool
    {
        if ($record->flow_type == CashFlowsBase::FLOW_TYPE_INCOME && $record->income_item_id) {
            return in_array($record->income_item_id, self::INCOME_ITEM_ID);
        }

        if ($record->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE && $record->expenditure_item_id) {
            return in_array($record->expenditure_item_id, self::EXPENDITURE_ITEM_ID);
        }

        return false;
    }

    /**
     * @param CreditFlowBehavior $self
     * @return CreditFlow
     */
    public static function newCreditFlow(CreditFlowBehavior $self)
    {
        return new CreditFlow([
            'credit_id' => $self->owner->credit_id,
            'olap_flow_id' => $self->owner->id,
            'wallet_id' => $self->wallet_id,
            $self->attribute => $self->owner->id
        ]);
    }

    /**
     * @return bool
     */
    public static function canAccess(): bool
    {
        return BusinessAnalyticsAccess::matchEmployee(BusinessAnalyticsAccess::SECTION_FINANCE);
    }

    /**
     * @return string[]
     */
    public function getCreditList(): array
    {
        if ($this->_creditList === null) {
            $repository = $this->createRepository();
            $this->_creditList = $repository ? $repository->getItems() : [];
        }

        return $this->_creditList;
    }

    /**
     * @return string[]
     */
    public function getCreditListWithOptions(): array
    {
        $items = [];
        $options = [];

        if ($repository = $this->createRepository()) {
            $provider = new ActiveDataProvider([
                'query' => $repository->getQuery()->andWhere(['!=', 'credit_status', Credit::STATUS_ARCHIVE]),
                'pagination' => false,
            ]);
            /** @var Credit $model */
            foreach ($provider->getModels() as $model) {
                $items[$model->credit_id] = sprintf(
                    'Договор № %s от %s',
                    $model->agreement_number,
                    DateHelper::format($model->agreement_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)
                );
                $options[$model->credit_id] = ['data-contractor' => $model->contractor_id];
            }
        }

        return [
            'items' => [0 => 'Без договора'] + $items,
            'options' => $options
        ];
    }

    /**
     * @return CreditRepository|null
     */
    private function createRepository(): ?CreditRepository
    {
        if ($this->owner->company_id) {
            return new CreditRepository(['company_id' => $this->owner->company_id]);
        }

        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;

        if ($employee && $employee instanceof Employee) {
            return new CreditRepository(['company_id' => $employee->company->id]);
        }

        return null;
    }
}
