<?php

namespace frontend\modules\analytics\behaviors\leasing;

use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\document\InvoiceExpenditureItem;
use frontend\components\BusinessAnalyticsAccess;
use frontend\modules\analytics\models\leasing\DebtCalculator;
use frontend\modules\analytics\models\leasing\LeasingFlow;
use frontend\modules\analytics\models\leasing\Schedule;
use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;

/**
 * @property CashBankFlows|CashOrderFlows|CashEmoneyFlows $owner
 */
final class LeasingFlowBehavior extends Behavior
{
    /**
     * @var string
     */
    public $attribute;

    /**
     * @var int
     */
    public $wallet_id;

    /**
     * @var LeasingFlow|null
     */
    private $_leasingFlow;

    /**
     * @inheritDoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => [$this, 'afterFind'],
            ActiveRecord::EVENT_AFTER_INSERT => [$this, 'afterSave'],
            ActiveRecord::EVENT_AFTER_UPDATE => [$this, 'afterSave'],
            ActiveRecord::EVENT_AFTER_DELETE => [$this, 'afterDelete'],
        ];
    }

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->attribute) || empty($this->wallet_id)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @return void
     * @throws
     */
    public function afterSave(): void
    {
        if ($this->isLeasingFlow()) {
            if ($this->owner->leasing_id) {
                if ($this->_leasingFlow === null) {
                    $this->_leasingFlow = new LeasingFlow([
                        'leasing_id' => $this->owner->leasing_id,
                        'olap_flow_id' => $this->owner->id,
                        'wallet_id' => $this->wallet_id,
                        $this->attribute => $this->owner->id
                    ]);
                }

                $this->_leasingFlow->save(false);
                $this->_leasingFlow->leasing->save(false);
            } elseif ($this->_leasingFlow) {
                $this->_leasingFlow->delete();
            }

            $this->updateLeasing();
        } else if ($this->_leasingFlow) {
            $this->_leasingFlow->delete();
            $this->_leasingFlow->leasing->save(false);
        }
    }

    /**
     * @return void
     */
    public function afterFind(): void
    {
        if ($this->isLeasingFlow()) {
            $this->_leasingFlow = LeasingFlow::findOne([$this->attribute => $this->owner->id]);

            if ($this->_leasingFlow) {
                $this->owner->leasing_id = $this->_leasingFlow->leasing_id;
                $this->owner->leasing_amount = $this->_leasingFlow->leasing->amountWithCurrency;
            }
        }
    }

    /**
     * @return void
     */
    public function afterDelete(): void
    {
        if ($this->_leasingFlow) {
            $this->updateLeasing();
        }
    }

    /**
     * @return bool
     */
    public function isLeasingFlow(): bool
    {
        if ($this->owner->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE) {
            if (in_array($this->owner->expenditure_item_id, InvoiceExpenditureItem::LEASING_ITEMS)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public static function canAccess(): bool
    {
        return BusinessAnalyticsAccess::matchEmployee(BusinessAnalyticsAccess::SECTION_FINANCE);
    }

    /**
     * @return void
     */
    private function updateLeasing(): void
    {
        if ($this->_leasingFlow) {
            $schedule = new Schedule($this->_leasingFlow->leasing);
            $calculator = new DebtCalculator($this->_leasingFlow->leasing, $schedule);
            $calculator->updateLeasing();
            $this->_leasingFlow->leasing->save(false);
        }
    }
}
