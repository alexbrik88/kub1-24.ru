<?php

namespace frontend\modules\analytics;

/**
 * analytics module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\analytics\controllers';

    /**
     * @inheritdoc
     */
    public $layout = 'invoice-report';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        //
    }
}
