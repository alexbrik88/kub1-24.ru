<?php

namespace frontend\modules\analytics\views\finance;

use frontend\modules\analytics\models\credits\CreditRepository;
use frontend\modules\crm\widgets\PjaxWidget;
use frontend\themes\kub\widgets\SpriteIconWidget;
use frontend\widgets\RangeButtonWidget;
use Yii;
use yii\bootstrap4\Html;
use yii\web\View;

/**
 * @var View $this
 * @var CreditRepository $repository
 */

$this->title = 'Кредиты полученные';
$placeholder = 'Поиск по номеру договора...';
$tableAttribute = 'credits_credit_table';
$summary = $repository->getSummary();
$formatter = Yii::$app->formatter;
$configs = [
    ['attribute' => 'credits_credit_amount', 'label' => 'Сумма по договору'],
    ['attribute' => 'credits_currency_id', 'label' => 'Валюта'],
    ['attribute' => 'credits_credit_first_date', 'label' => 'Дата получения'],
    ['attribute' => 'credits_contractor_id', 'label' => 'Кредитор'],
    ['attribute' => 'credits_credit_type', 'label' => 'Вид кредита'],
    ['attribute' => 'credits_debt_amount', 'label' => 'Получено'],
    ['attribute' => 'credits_interest_repaid', 'label' => 'Уплаченные %%'],
    ['attribute' => 'credits_interest_amount', 'label' => 'Сумма %% всего'],
];

?>

<div class="page-head d-flex flex-wrap align-items-center mt-1">
    <h4><?= Html::encode($this->title); ?></h4>

    <?= Html::a(
        SpriteIconWidget::widget(['icon' => 'add-icon']) . ' ' . Html::tag('span', 'Добавить', ['class' => 'ml-1']),
        ['credit/create'],
        ['class' => 'button-regular button-regular_red pl-3 pr-3 ml-auto']
    ) ?>
</div>

<?php PjaxWidget::begin(['id' => 'pjaxContent']) ?>

<div class="wrap wrap_count">
    <div class="row">
        <div class="col-6 col-xl-3">
            <a href="/" style="text-decoration: none;" class="count-card wrap">
                <div class="count-card-main"><?= $formatter->format($summary['debt_amount'], 'currency') ?></div>
                <div class="count-card-title">Получено всего</div>
                <hr>
                <div class="count-card-foot">Количество кредитов: <?= $summary['count'] ?></div>
            </a>
        </div>
        <div class="col-6 col-xl-3">
            <a href="/" style="text-decoration: none;" class="count-card count-card_green wrap">
                <div class="count-card-main"><?= $formatter->format($summary['payment_amount'], 'currency') ?></div>
                <div class="count-card-title">Погашено всего</div>
                <hr>
                <div class="count-card-foot">Количество кредитов: <?= $summary['count'] ?></div>
            </a>
        </div>
        <div class="col-6 col-xl-3">
            <a href="/" style="text-decoration: none;" class="count-card count-card_red wrap">
                <div class="count-card-main"><?= $formatter->format($summary['payment_debt'], 'currency') ?></div>
                <div class="count-card-title">Текущая задолжность</div>
                <hr>
                <div class="count-card-foot">Количество кредитов: <?= $summary['count'] ?></div>
            </a>
        </div>
        <div class="col-6 col-xl-3 d-flex flex-column justify-content-between">
            <?= RangeButtonWidget::widget() ?>
        </div>
    </div>
</div>

<?= $this->render('../credit/widgets/search-filter', compact('repository', 'placeholder', 'configs', 'tableAttribute')) ?>

<?= $this->render('../credit/widgets/credit-grid', compact('repository')) ?>

<?php $this->registerJs(<<<JS
    $(document).ready(function() {
        initMain();
    });
JS); ?>

<?php PjaxWidget::end() ?>
