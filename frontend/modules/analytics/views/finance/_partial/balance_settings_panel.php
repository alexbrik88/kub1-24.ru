<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 18.04.2019
 * Time: 15:04
 */

use frontend\modules\analytics\models\BalanceItem;
use common\components\helpers\Html;

?>
<div class="balance-settings-panel panel-block">
    <div class="main-block" style="width: 100%;">
        <p class="bold" style="font-size:19px;">
            Инструкция по работе с Балансом
        </p>
        <p class="bold">
            АКТИВЫ
        </p>
        <p class="bold">
            Основные средства
        </p>
        <?php foreach ([
                           BalanceItem::ITEM_REAL_ESTATE_OBJECT,
                           BalanceItem::ITEM_EQUIPMENT_ASSETS,
                           BalanceItem::ITEM_TRANSPORT_ASSETS,
                           BalanceItem::ITEM_INTANGIBLE_ASSETS,
                           BalanceItem::ITEM_OTHER_ASSETS
                       ] as $itemID): ?>
            <p>
                <span class="underline toggle-text" style="display: block;cursor: pointer;">
                    <?= BalanceItem::findOne($itemID)->name; ?>
                </span>
                <span class="item-text hidden">
                    Суммы заполняются <b><i>вручную</i></b>. Нужно нажать на иконку «карандаш» в конце названия строки.<br>
                    Внесенные данные в таблице не отображаются в будущих месяцах.
                </span>
            </p>
        <?php endforeach; ?>
        <p class="bold">
            Оборотные активы
        </p>
        <p>
            <span class="underline toggle-text" style="display: block;cursor: pointer;">
                <?= BalanceItem::findOne(BalanceItem::ITEM_STOCKS)->name; ?>
            </span>
            <span class="item-text hidden">
                Суммы заполняются <b><i>автоматически</i></b>. Данные берутся из склада на последний день месяц и на текущую дату.
                Количество товара умножается на Цену покупки.
            </span>
        </p>
        <p class="bold">
            Дебиторская задолженность
        </p>
        <p>
            <span class="underline toggle-text" style="display: block;cursor: pointer;">
                <?= BalanceItem::findOne(BalanceItem::ITEM_TRADE_RECEIVABLES)->name; ?>
            </span>
            <span class="item-text hidden">
                Суммы заполняются <b><i>автоматически</i></b>. Данные суммируются по текущей и просроченной задолженности покупателей
                на последний день месяц и на текущую дату.
            </span>
        </p>
        <div style="margin-bottom: 10px;">
            <span class="underline toggle-text" style="display: block;cursor: pointer;">
                <?= BalanceItem::findOne(BalanceItem::ITEM_PREPAYMENTS_TO_SUPPLIERS)->name; ?>
            </span>
            <div class="item-text hidden">
                Суммы заполняются <b><i>автоматически</i></b>. Данные суммируются по<br>
                <ul style="list-style: decimal;">
                    <li>
                        Счетам поставщиков, которые оплачены ранее даты, на которую считается Баланс, и для которых
                        выполняется одно из следующих условий:<br>
                        А) У счета нет Акта/ТН/УПД<br>
                        Б) У счета есть Акт/ТН/УПД, но с датой позже, чем дата, на которую мы считаем Балан<br>
                        В) У счета вместо Акта/ТН/УПД стоит "Не нужен"
                    </li>
                    <li>
                        Расходам в разделе Деньги, которые не привязаны к счету и у которых дата ранее даты, на которую
                        считается Баланс, и для которых выполняется одно из следующих условий:<br>
                        А) В чекбоксе "Авансовый платеж" стоит галочка<br>
                        Б) Дата признаний расхода позднее даты, на которую мы считаем Баланс
                    </li>
                </ul>
            </div>
        </div>
        <p>
            <span class="underline toggle-text" style="display: block;cursor: pointer;">
                <?= BalanceItem::findOne(BalanceItem::ITEM_BUDGET_OVERPAYMENT)->name; ?>
            </span>
            <span class="item-text hidden">
                Суммы заполняются <b><i>вручную</i></b>. Нужно нажать на иконку «карандаш» в конце названия строки. Внесенные данные
                в таблице не отображаются в будущих месяцах.
            </span>
        </p>
        <p>
            <span class="underline toggle-text" style="display: block;cursor: pointer;">
                <?= BalanceItem::findOne(BalanceItem::ITEM_LOANS_ISSUED)->name; ?>
            </span>
            <span class="item-text hidden">
                Суммы заполняются <b><i>автоматически</i></b>. Данные из раздела Банк и Касса суммируются по статье расхода
                "Выдача займа" минус сумма по статье прихода "Возврат займа" на последний день месяц и на текущую дату.
                Проценты к получению пока не учитываются.
            </span>
        </p>
        <p>
            <span class="underline toggle-text" style="display: block;cursor: pointer;">
                <?= BalanceItem::findOne(BalanceItem::ITEM_RENTAL_DEPOSIT)->name; ?>
            </span>
            <span class="item-text hidden">
                Суммы заполняются <b><i>автоматически</i></b>. Данные из раздела Банк и Касса суммируются по статье расхода
                "Обеспечительный платеж" минус сумма по статье прихода "Обеспечительный платеж" на последний день
                окончившегося месяца и на текущую дату.
            </span>
        </p>
        <p>
            <span class="underline toggle-text" style="display: block;cursor: pointer;">
                <?= BalanceItem::findOne(BalanceItem::ITEM_OTHER_INVESTMENTS)->name; ?>
            </span>
            <span class="item-text hidden">
                Суммы заполняются <b><i>вручную</i></b>. Нужно нажать на иконку «карандаш» в конце названия строки.
                Внесенные данные в таблице не отображаются в будущих месяцах.
            </span>
        </p>
        <p class="bold">
            Денежные средства
        </p>
        <?php foreach ([
                           BalanceItem::ITEM_BANK_ACCOUNTS,
                           BalanceItem::ITEM_CASHBOX,
                           BalanceItem::ITEM_EMONEY,
                       ] as $itemID): ?>
            <p>
                <span class="underline toggle-text" style="display: block;cursor: pointer;">
                    <?= BalanceItem::findOne($itemID)->name; ?>
                </span>
                <span class="item-text hidden">
                    Суммы заполняются <b><i>автоматически</i></b>. Данные берутся из раздела <?= BalanceItem::$flowNameByItem[$itemID]; ?>
                    – остаток на последний день месяц и на текущую дату.
                </span>
            </p>
        <?php endforeach; ?>
        <p class="bold">
            ПАССИВЫ
        </p>
        <p class="bold">
            Собственный капитал
        </p>
        <p>
            <span class="underline toggle-text" style="display: block;cursor: pointer;">
                <?= BalanceItem::findOne(BalanceItem::ITEM_AUTHORIZED_CAPITAL)->name; ?>
            </span>
            <span class="item-text hidden">
                Суммы заполняются <b><i>вручную</i></b>. Нужно нажать на иконку «карандаш» в конце названия строки.
                Внесенные данные в таблице не отображаются в будущих месяцах.
            </span>
        </p>
        <p>
            <span class="underline toggle-text" style="display: block;cursor: pointer;">
                <?= BalanceItem::findOne(BalanceItem::ITEM_UNDESTRIBUTED_PROFITS)->name; ?>
            </span>
            <span class="item-text hidden">
                Суммы заполняются <b><i>автоматически</i></b>. Данные берутся из Отчета о Прибылях и Убытках на
                последний день месяц и на текущую дату.
            </span>
        </p>
        <p class="bold">
            Долгосрочные обязательства
        </p>
        <p>
            <span class="underline toggle-text" style="display: block;cursor: pointer;">
                <?= BalanceItem::findOne(BalanceItem::ITEM_LONG_TERM_LOANS)->name; ?>
            </span>
            <span class="item-text hidden">
                Суммы заполняются <b><i>вручную</i></b>. Нужно нажать на иконку «карандаш» в конце названия строки.
                Внесенные данные в таблице не отображаются в будущих месяцах.
            </span>
        </p>
        <p class="bold">
            Краткосрочные обязательства
        </p>
        <p>
            <span class="underline toggle-text" style="display: block;cursor: pointer;">
                <?= BalanceItem::findOne(BalanceItem::ITEM_SHORT_TERM_BORROWINGS)->name; ?>
            </span>
            <span class="item-text hidden">
                Суммы заполняются <b><i>автоматически</i></b>. Данные из раздела Банк и Касса суммируются по
                статьям прихода "Займ" и "Кредит" и вычитается сумма по статям расхода "Погашение займа" и
                "Погашение кредита" на последний день месяц и на текущую дату.
            </span>
        </p>
        <p class="bold">
            Кредиторская задолженность
        </p>
        <p>
            <span class="underline toggle-text" style="display: block;cursor: pointer;">
                <?= BalanceItem::findOne(BalanceItem::ITEM_VENDOR_INVOICES_PAYABLE)->name; ?>
            </span>
            <span class="item-text hidden">
                Суммы заполняются <b><i>автоматически</i></b>. Данные суммируются по текущей и просроченной
                задолженности перед поставщиками на последний день месяц и на текущую дату.
            </span>
        </p>
        <div style="margin-bottom: 10px;">
            <span class="underline toggle-text" style="display: block;cursor: pointer;">
                <?= BalanceItem::findOne(BalanceItem::ITEM_PREPAYMENT_OF_CUSTOMERS)->name; ?>
            </span>
            <div class="item-text hidden">
                Суммы заполняются <b><i>автоматически</i></b>. Данные суммируются по<br>
                <ul style="list-style: decimal;">
                    <li>
                        Счетам поставщиков, которые оплачены ранее даты, на которую считается Баланс, и для которых
                        выполняется одно из следующих условий:<br>
                        А) У счета нет Акта/ТН/УПД<br>
                        Б) У счета есть Акт/ТН/УПД, но с датой позже, чем дата, на которую мы считаем Балан<br>
                        В) У счета вместо Акта/ТН/УПД стоит "Не нужен"
                    </li>
                    <li>
                        Расходам в разделе Деньги, которые не привязаны к счету и у которых дата ранее даты, на которую
                        считается Баланс, и для которых выполняется одно из следующих условий:<br>
                        А) В чекбоксе "Авансовый платеж" стоит галочка<br>
                        Б) Дата признаний расхода позднее даты, на которую мы считаем Баланс
                    </li>
                </ul>
            </div>
        </div>
        <p>
            <span class="underline toggle-text" style="display: block;cursor: pointer;">
                <?= BalanceItem::findOne(BalanceItem::ITEM_SALARY_ARREARS)->name; ?>
            </span>
            <span class="item-text hidden">
                Суммы заполняются <b><i>вручную</i></b>. Нужно нажать на иконку «карандаш» в конце названия строки.
                Внесенные данные в таблице не отображаются в будущих месяцах.
            </span>
        </p>
        <p>
            <span class="underline toggle-text" style="display: block;cursor: pointer;">
                <?= BalanceItem::findOne(BalanceItem::ITEM_BUDGET_ARREARS)->name; ?>
            </span>
            <span class="item-text hidden">
               Суммы заполняются <b><i>вручную</i></b>. Нужно нажать на иконку «карандаш» в конце названия строки.
                Внесенные данные в таблице не отображаются в будущих месяцах.
            </span>
        </p>
        <p class="bold" style="margin-bottom: 0;margin-top: 25px;">
            ОСТАЛИСЬ ВОПРОСЫ?
        </p>
        <p>
            Напишите нам на почту <?= Html::a('support@kub-24.ru', 'mailto:support@kub-24.ru'); ?>
        </p>
    </div>
    <span class="side-panel-close" title="Закрыть">
        <span class="side-panel-close-inner"></span>
    </span>
</div>
<?= $this->registerJs('
$(document).on("click", ".toggle-text", function (e) {
   $(this).next(".item-text").toggleClass("hidden");
});
'); ?>
