<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 25.02.2019
 * Time: 19:36
 */

use frontend\modules\analytics\models\PaymentCalendarSearch;
use common\components\date\DateHelper;
use common\models\Contractor;
use common\components\grid\DropDownSearchDataColumn;
use common\components\helpers\Html;
use common\models\cash\CashBankFlows;
use common\components\grid\GridView;
use yii\widgets\Pjax;
use frontend\modules\analytics\models\FlowOfFundsReportSearch;
use common\components\TextHelper;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\widgets\ConfirmModalWidget;
use yii\grid\ActionColumn;
use yii\helpers\Url;
use common\models\cash\CashFlowsBase;

/* @var $this yii\web\View
 * @var $searchModel PaymentCalendarSearch
 * @var $itemsDataProvider \yii\data\ActiveDataProvider
 * @var $activeTab integer
 */
?>
<div class="portlet box darkblue items-table-block">
    <div class="portlet-title">
        <div class="caption" style="padding-top: 10px;">
            Реестр плановых операций
        </div>
        <div class="filter hidden">
            <?= Html::a('Дата', 'javascript:;', [
                'class' => 'btn',
                'data-attr' => FlowOfFundsReportSearch::GROUP_DATE,
            ]); ?>
            <?= Html::a('Контрагент', 'javascript:;', [
                'class' => 'btn',
                'data-attr' => FlowOfFundsReportSearch::GROUP_CONTRACTOR,
            ]); ?>
            <?= Html::a('Тип оплат', 'javascript:;', [
                'class' => 'btn',
                'data-attr' => FlowOfFundsReportSearch::GROUP_PAYMENT_TYPE,
            ]); ?>
            <?= Html::a('Заркыть', 'javascript:;', [
                'class' => 'btn close-odds',
            ]); ?>
        </div>
        <div class="actions joint-operations pull-left"
             style="display:none; width: 220px;padding: 7px 0 0 0!important;">
            <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                'class' => 'btn btn-default btn-sm',
                'data-toggle' => 'modal',
            ]); ?>
            <div id="many-delete" class="confirm-modal fade modal"
                 role="dialog" tabindex="-1" aria-hidden="true"
                 style="display: none; margin-top: -51.5px;">
                <div class="modal-dialog ">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="form-body">
                                <div class="row">
                                    Вы уверены, что хотите удалить выбранные операции?
                                </div>
                            </div>
                            <div class="form-actions row">
                                <div class="col-xs-6">
                                    <?= Html::a('ДА', null, [
                                        'class' => 'btn darkblue pull-right modal-many-delete',
                                        'data-url' => Url::to(['many-delete']),
                                    ]); ?>
                                </div>
                                <div class="col-xs-6">
                                    <button type="button" class="btn darkblue" data-dismiss="modal">
                                        НЕТ
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?php $pjax = Pjax::begin([
                'id' => 'odds-items_pjax',
                'enablePushState' => false,
                'enableReplaceState' => false,
            ]); ?>
            <?php if (isset($itemsDataProvider)): ?>
                <?= GridView::widget([
                    'dataProvider' => $itemsDataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable documents_table fix-thead plan-item-table',
                        'aria-describedby' => 'datatable_ajax_info',
                        'role' => 'grid',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    'options' => [
                        'class' => 'dataTables_wrapper dataTables_extended_wrapper bank-scroll-table',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $itemsDataProvider->totalCount]),
                    'columns' => [
                        [
                            'header' => Html::checkbox('', false, [
                                'class' => 'joint-operation-main-checkbox',
                            ]),
                            'headerOptions' => [
                                'class' => 'text-center',
                                'width' => '5%',
                            ],
                            'contentOptions' => [
                                'class' => 'text-center joint-operation-checkbox-td',
                            ],
                            'format' => 'raw',
                            'value' => function ($flows) {
                                if ($flows['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                                    $typeCss = 'income-item';
                                } else {
                                    $typeCss = 'expense-item';
                                }

                                return Html::checkbox('flowId[]', false, [
                                    'class' => 'joint-operation-checkbox ' . $typeCss,
                                    'value' => $flows['id'],
                                ]);
                            },
                        ],
                        [
                            'attribute' => 'date',
                            'label' => 'Дата',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                            ],
                            'value' => function ($flows) use ($searchModel) {
                                return !in_array($searchModel->group, [FlowOfFundsReportSearch::GROUP_CONTRACTOR, FlowOfFundsReportSearch::GROUP_PAYMENT_TYPE]) ?
                                    DateHelper::format($flows['date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) : '';
                            },
                        ],
                        [
                            'attribute' => 'amountIncome',
                            'label' => 'Приход',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                                'style' => 'display: ' . (!empty($searchModel->income_item_id) ||
                                    $searchModel->income_item_id === null ? 'table-cell;' : 'none;'),
                            ],
                            'contentOptions' => [
                                'style' => 'display: ' . (!empty($searchModel->income_item_id) ||
                                    $searchModel->income_item_id === null ? 'table-cell;' : 'none;'),
                            ],
                            'value' => function ($flows) {
                                return $flows['amountIncome'] > 0
                                    ? TextHelper::invoiceMoneyFormat($flows['amountIncome'], 2)
                                    : '-';
                            },
                        ],
                        [
                            'attribute' => 'amountExpense',
                            'label' => 'Расход',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                                'style' => 'display: ' . (!empty($searchModel->expenditure_item_id) ||
                                    $searchModel->expenditure_item_id === null ? 'table-cell;' : 'none;'),
                            ],
                            'contentOptions' => [
                                'style' => 'display: ' . (!empty($searchModel->expenditure_item_id) ||
                                    $searchModel->expenditure_item_id === null ? 'table-cell;' : 'none;'),
                            ],
                            'value' => function ($flows) {
                                return $flows['amountExpense'] > 0
                                    ? TextHelper::invoiceMoneyFormat($flows['amountExpense'], 2)
                                    : '-';
                            },
                        ],
                        [
                            'attribute' => 'payment_type',
                            'label' => 'Тип оплаты',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'contentOptions' => [
                                'class' => 'text-center',
                            ],
                            'format' => 'raw',
                            'value' => function ($flows) use ($searchModel) {
                                if (!in_array($searchModel->group, [FlowOfFundsReportSearch::GROUP_CONTRACTOR, FlowOfFundsReportSearch::GROUP_DATE])) {
                                    switch ($flows['payment_type']) {
                                        case PlanCashFlows::PAYMENT_TYPE_BANK:
                                            return 'Банк <i class="fa fa-bank m-r-sm" style="color: #4276a4;"></i>';
                                        case PlanCashFlows::PAYMENT_TYPE_ORDER:
                                            return 'Касса <i class="fa fa-money m-r-sm" style="color: #4276a4;"></i>';
                                        case PlanCashFlows::PAYMENT_TYPE_EMONEY:
                                            return 'E-money <i class="flaticon-wallet31 m-r-sm m-l-n-xs" style="color: #4276a4;"></i>';
                                        default:
                                            return '';
                                    }
                                }
                                return '';
                            },
                        ],
                        [
                            'class' => DropDownSearchDataColumn::className(),
                            'attribute' => 'contractor_id',
                            'label' => 'Контрагент',
                            'headerOptions' => [
                                'width' => '30%',
                                'class' => 'nowrap-normal max10list',
                            ],
                            'format' => 'raw',
                            'filter' => !in_array($searchModel->group, [FlowOfFundsReportSearch::GROUP_PAYMENT_TYPE, FlowOfFundsReportSearch::GROUP_DATE]) ?
                                $searchModel->getContractorFilterItems() : ['' => 'Все контрагенты'],
                            'value' => function ($flows) use ($searchModel) {
                                if (!in_array($searchModel->group, [FlowOfFundsReportSearch::GROUP_PAYMENT_TYPE, FlowOfFundsReportSearch::GROUP_DATE])) {
                                    /* @var $contractor Contractor */
                                    $contractor = Contractor::findOne($flows['contractor_id']);
                                    $model = PlanCashFlows::findOne($flows['id']);

                                    return $contractor !== null ?
                                        ('<span title="' . htmlspecialchars($contractor->nameWithType) . '">' . $contractor->nameWithType . '</span>') :
                                        ($model->cashContractor ?
                                            ('<span title="' . htmlspecialchars($model->cashContractor->text) . '">' . $model->cashContractor->text . '</span>') : ''
                                        );
                                }

                                return '';
                            },
                        ],
                        [
                            'attribute' => 'description',
                            'label' => 'Назначение',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '30%',
                            ],
                            'format' => 'raw',
                            'value' => function ($flows) use ($searchModel) {
                                if (empty($searchModel->group) && $flows['description']) {
                                    $description = mb_substr($flows['description'], 0, 50) . '<br>' . mb_substr($flows['description'], 50, 50);

                                    return Html::label(strlen($flows['description']) > 100 ? $description . '...' : $description, null, ['title' => $flows['description']]);
                                }

                                return '';
                            },
                        ],
                        [
                            'class' => DropDownSearchDataColumn::className(),
                            'attribute' => 'reason_id',
                            'label' => 'Статья',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'filter' => empty($searchModel->group) ?
                                array_merge(['' => 'Все статьи', 'empty' => '-'], $searchModel->getReasonFilterItems()) :
                                ['' => 'Все статьи'],
                            'format' => 'raw',
                            'value' => function ($flows) use ($searchModel) {
                                if (empty($searchModel->group)) {
                                    $reason = ($flows['flow_type'] == CashBankFlows::FLOW_TYPE_INCOME) ? $flows['incomeItemName'] : $flows['expenseItemName'];

                                    return $reason ?: '-';
                                }

                                return '';
                            },
                        ],
                        [
                            'class' => ActionColumn::className(),
                            'template' => '{update}<br>{delete}',
                            'headerOptions' => [
                                'width' => '3%',
                            ],
                            'contentOptions' => [
                                'class' => 'action-line',
                            ],
                            'visible' => Yii::$app->user->can(\frontend\rbac\permissions\Cash::DELETE),
                            'buttons' => [
                                'update' => function ($url, $model, $key) use ($searchModel, $activeTab) {
                                    return Html::a("<span aria-hidden='true' class='icon-pencil'></span>", 'javascript:;', [
                                        'title' => 'Изменить',
                                        'class' => 'update-item-payment-calendar',
                                        'data-url' => Url::to(['update-plan-item', 'id' => $model['id'], 'activeTab' => $activeTab, 'year' => $searchModel->year]),
                                    ]);
                                },
                                'delete' => function ($url, $model) use ($searchModel, $activeTab) {
                                    return ConfirmModalWidget::widget([
                                        'toggleButton' => [
                                            'label' => '<span aria-hidden="true" class="icon-close"></span>',
                                            'class' => 'delete-item-payment-calendar',
                                            'tag' => 'a',
                                        ],
                                        'options' => [
                                            'id' => 'delete-plan-item-' . $model['id'],
                                        ],
                                        'confirmUrl' => Url::to(['delete-plan-item', 'id' => $model['id'], 'activeTab' => $activeTab, 'year' => $searchModel->year]),
                                        'confirmParams' => [],
                                        'message' => 'Вы уверены, что хотите удалить операцию?',
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            <?php endif; ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>