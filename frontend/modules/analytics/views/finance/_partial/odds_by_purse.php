<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 24.01.2019
 * Time: 17:15
 */

use common\components\helpers\Html;
use frontend\modules\analytics\models\FlowOfFundsReportSearch;
use kartik\checkbox\CheckboxX;
use common\components\TextHelper;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $searchModel FlowOfFundsReportSearch
 * @var $data []
 * @var $expenditureItems []
 * @var $types []
 * @var $balance []
 * @var $growingBalance []
 * @var $blocks []
 * @var $checkMonth boolean
 * @var $activeTab integer
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 */

$isCurrentYear = $searchModel->isCurrentYear;
?>
<div class="portlet box darkblue" style="margin-bottom: 5px;">
    <?= Html::beginForm(['odds', 'activeTab' => $activeTab], 'GET', [
        'validateOnChange' => true,
    ]); ?>
    <div class="search-form-default">
        <div class="col-md-12 pull-right serveces-search" style="padding-left: 5px;">
            <div class="input-group" style="display: block;">
                <div class="input-cont">
                    <div class="col-md-2 p-l-0">
                        <?= Html::activeDropDownList($searchModel, 'year', $searchModel->getYearFilter(), [
                            'class' => 'form-control',
                            'style' => 'display: inline-block;',
                            'value' => $searchModel->year,
                        ]); ?>
                    </div>
                    <div class="col-md-10" style="padding-right: 0; padding-left: 0">
                        <?php if (!$searchModel->company->isFreeTariff): ?>
                            <?= Html::a('<i class="fa fa-file-excel-o"></i> Скачать в Excel', Url::to(['/analytics/finance/get-xls', 'type' => $activeTab, 'year' => $searchModel->year]), [
                                'class' => 'get-xls-link download-odds-xls pull-right',
                            ]); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <div class="dataTables_wrapper dataTables_extended_wrapper no-footer scroll-table"
                 id="datatable_ajax_wrapper">
                <div class="scroll-table-wrapper">
                    <table class="table table-striped table-bordered table-hover flow-of-funds by_purse">
                        <thead class="not-fixed">
                        <tr class="heading quarters-flow-of-funds" role="row">
                            <th width="20%">
                                <?= CheckboxX::widget([
                                    'id' => 'main-checkbox-side',
                                    'name' => 'main-checkbox-side',
                                    'value' => true,
                                    'options' => [
                                        'class' => 'main-checkbox-side',
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]); ?>
                                <label for="main-checkbox-side">
                                    Статьи
                                </label>
                            </th>
                            <th colspan="<?= $currentMonthNumber < 4 && $isCurrentYear ? 3 : 1; ?>"
                                class="text-left quarter-th" data-quarter="1"
                                style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber < 4 && $isCurrentYear ? '180px' : '172px'; ?>;">
                                <?= CheckboxX::widget([
                                    'id' => 'first-quarter',
                                    'name' => 'quarter-month-1',
                                    'value' => !($currentQuarter == 1 && $isCurrentYear),
                                    'options' => [
                                        'class' => 'quarter-checkbox',
                                        'data' => [
                                            'month-class' => 'quarter-month-1',
                                            'total-quarter-class' => 'quarter-1',
                                        ],
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]); ?>
                                <label for="first-quarter">
                                    1 <?= $currentMonthNumber < 4 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $searchModel->year; ?>
                                </label>
                            </th>
                            <th colspan="<?= $currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear ? 3 : 1; ?>"
                                class="text-left quarter-th" data-quarter="2"
                                style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear ? '180px' : '172px'; ?>;">
                                <?= CheckboxX::widget([
                                    'id' => 'second-quarter',
                                    'name' => 'quarter-month-1',
                                    'value' => !($currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear),
                                    'options' => [
                                        'class' => 'quarter-checkbox',
                                        'data' => [
                                            'month-class' => 'quarter-month-2',
                                            'total-quarter-class' => 'quarter-2',
                                        ],
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]); ?>
                                <label for="second-quarter">
                                    2 <?= $currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $searchModel->year; ?>
                                </label>
                            </th>
                            <th colspan="<?= $currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear ? 3 : 1; ?>"
                                class="text-left quarter-th" data-quarter="3"
                                style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear ? '180px' : '172px'; ?>;">
                                <?= CheckboxX::widget([
                                    'id' => 'third-quarter',
                                    'name' => 'quarter-month-1',
                                    'value' => !($currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear),
                                    'options' => [
                                        'class' => 'quarter-checkbox',
                                        'data' => [
                                            'month-class' => 'quarter-month-3',
                                            'total-quarter-class' => 'quarter-3',
                                        ],
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]); ?>
                                <label for="third-quarter">
                                    3 <?= $currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $searchModel->year; ?>
                                </label>
                            </th>
                            <th colspan="<?= $currentMonthNumber > 9 && $isCurrentYear ? 3 : 1; ?>"
                                class="text-left quarter-th" data-quarter="4"
                                style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber > 9 && $isCurrentYear ? '180px' : '172px'; ?>;">
                                <?= CheckboxX::widget([
                                    'id' => 'fourth-quarter',
                                    'name' => 'quarter-month-1',
                                    'value' => !($currentMonthNumber > 9 && $isCurrentYear),
                                    'options' => [
                                        'class' => 'quarter-checkbox',
                                        'data' => [
                                            'month-class' => 'quarter-month-4',
                                            'total-quarter-class' => 'quarter-4',
                                        ],
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]); ?>
                                <label for="fourth-quarter">
                                    4 <?= $currentMonthNumber > 9 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $searchModel->year; ?>
                                </label>
                            </th>
                            <th width="10%" class="text-center" style="border-right: 1px solid #ddd;">
                                <?= $searchModel->year; ?>
                            </th>
                        </tr>
                        <tr class="heading" role="row">
                            <th width="20%"></th>
                            <?php foreach (FlowOfFundsReportSearch::$month as $key => $month): ?>
                                <?php $quarter = ceil($key / 3); ?>
                                <th class="quarter-month-<?= $quarter; ?> text-left" width="10%"
                                    data-month="<?= $key; ?>"
                                    style="min-width: 117px;display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                    <?= $month; ?>
                                </th>
                                <?php if ($key % 3 == 0): ?>
                                    <th class="text-left quarter-<?= $quarter; ?>" width="10%"
                                        style="min-width: 117px;display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                        Итого
                                    </th>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <th class="text-center" width="10%">Итого</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach (FlowOfFundsReportSearch::$purseTypes as $typeID => $typeName): ?>
                            <?php $class = in_array($typeID, [
                                FlowOfFundsReportSearch::INCOME_CASH_BANK,
                                FlowOfFundsReportSearch::INCOME_CASH_ORDER,
                                FlowOfFundsReportSearch::INCOME_CASH_EMONEY
                            ]) ? 'income' : 'expense'; ?>
                            <?php if ($class == 'income'): ?>
                                <tr class="main-block"
                                    data-flow_type="<?= FlowOfFundsReportSearch::$purseBlockByType[$typeID]; ?>">
                                    <td class="odd bold cash-bank-block">
                                        <?= FlowOfFundsReportSearch::$purseBlocks[FlowOfFundsReportSearch::$purseBlockByType[$typeID]]; ?>
                                    </td>
                                    <?php $key = $quarterSum = 0; ?>
                                    <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>
                                        <?php $key++;
                                        $quarter = ceil($key / 3); ?>
                                        <td class="odd text-right bold can-hover quarter-month-<?= ceil($key / 3); ?> cash-bank-block-month-<?= $key; ?>
                                        <?= ($checkMonth && $monthNumber == date('m')) || !$checkMonth ? null : 'blur'; ?>"
                                            role="row"
                                            style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                            <?= isset($blocks[FlowOfFundsReportSearch::$purseBlockByType[$typeID]][$monthNumber]) ?
                                                TextHelper::invoiceMoneyFormat($blocks[FlowOfFundsReportSearch::$purseBlockByType[$typeID]][$monthNumber]['flowSum'], 2) : 0; ?>
                                        </td>
                                        <?php $quarterSum += isset($blocks[FlowOfFundsReportSearch::$purseBlockByType[$typeID]][$monthNumber]) ?
                                            $blocks[FlowOfFundsReportSearch::$purseBlockByType[$typeID]][$monthNumber]['flowSum'] : 0; ?>
                                        <?php if ($key % 3 == 0): ?>
                                            <td class="odd text-right bold quarter-block can-hover quarter-<?= $key / 3; ?> cash-bank-block-quarter-<?= $key / 3; ?>
                                            <?= $checkMonth ? 'blur' : null; ?>"
                                                role="row"
                                                style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                                <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                                $quarterSum = 0; ?>
                                            </td>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <td class="bold odd total-block text-right can-hover cash-bank-block-month-total-flow-sum <?= $checkMonth ? 'blur' : null; ?>"
                                        role="row">
                                        <?= isset($blocks[FlowOfFundsReportSearch::$purseBlockByType[$typeID]]['totalFlowSum']['flowSum']) ?
                                            TextHelper::invoiceMoneyFormat($blocks[FlowOfFundsReportSearch::$purseBlockByType[$typeID]]['totalFlowSum']['flowSum'], 2) : 0; ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <tr class="expenditure_type sub-block <?= $class; ?>" id="<?= $typeID; ?>"
                                data-flow_type="<?= FlowOfFundsReportSearch::$purseBlockByType[$typeID]; ?>">
                                <td role="row" class="checkbox-td">
                                    <?= CheckboxX::widget([
                                        'id' => 'flow-of-funds-type-' . $typeID,
                                        'name' => 'flow-of-funds-type',
                                        'value' => true,
                                        'options' => [
                                            'class' => 'flow-of-funds-type',
                                        ],
                                        'pluginOptions' => [
                                            'size' => 'xs',
                                            'threeState' => false,
                                            'inline' => false,
                                            'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                            'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                        ],
                                    ]); ?>
                                    <label for="flow-of-funds-type-<?= $typeID; ?>">
                                        <?= $typeName; ?>
                                    </label>
                                </td>
                                <?php $key = $quarterSum = 0; ?>
                                <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>
                                    <?php $key++;
                                    $quarter = ceil($key / 3); ?>
                                    <td class="odd text-right can-hover quarter-month-<?= ceil($key / 3); ?> quarter-month-<?= $typeID . '-' . $key; ?>
                                            <?= ($checkMonth && $monthNumber == date('m')) || !$checkMonth ? null : 'blur'; ?>"
                                        role="row"
                                        style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                        <?= isset($types[$typeID][$monthNumber]) ?
                                            TextHelper::invoiceMoneyFormat($types[$typeID][$monthNumber]['flowSum'], 2) : 0; ?>
                                    </td>
                                    <?php $quarterSum += isset($types[$typeID][$monthNumber]) ?
                                        $types[$typeID][$monthNumber]['flowSum'] : 0; ?>
                                    <?php if ($key % 3 == 0): ?>
                                        <td class="odd text-right can-hover quarter-block quarter-<?= $key / 3; ?> quarter-<?= $typeID . '-' . ($key / 3); ?>
                                                <?= $checkMonth ? 'blur' : null; ?>"
                                            role="row"
                                            style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                            <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                            $quarterSum = 0; ?>
                                        </td>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <td class="odd total-block text-right can-hover total-flow-sum-<?= $typeID; ?> <?= $checkMonth ? 'blur' : null; ?>"
                                    role="row">
                                    <?= isset($types[$typeID]['totalFlowSum']) ?
                                        TextHelper::invoiceMoneyFormat($types[$typeID]['totalFlowSum'], 2) : 0; ?>
                                </td>
                            </tr>
                            <?php if (isset($data[$typeID])): ?>
                                <?php foreach ($expenditureItems[$typeID] as $expenditureItemID => $expenditureItemName): ?>
                                    <?php if (isset($data[$typeID][$expenditureItemID])): ?>
                                        <tr data-id="<?= $typeID; ?>" data-item_id="<?= $expenditureItemID; ?>"
                                            class="item-block hidden item-<?= $expenditureItemID . ' ' . $class; ?>"
                                            data-flow_type="<?= FlowOfFundsReportSearch::$purseBlockByType[$typeID]; ?>">
                                            <td width="20%" class="item-name-<?= $class . '-' . $expenditureItemID; ?>"
                                                style="padding-left: 40px;">
                                                <?= $expenditureItemName; ?>
                                            </td>
                                            <?php $key = $quarterSum = 0; ?>
                                            <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>
                                                <?php $key++;
                                                $quarter = ceil($key / 3); ?>
                                                <td class="odd text-right can-hover quarter-month-<?= ceil($key / 3); ?> item-month-<?= $expenditureItemID . '-' . $key; ?>
                                                        <?= ($checkMonth && $monthNumber == date('m')) || !$checkMonth ? null : 'blur'; ?>"
                                                    role="row" width="10%"
                                                    style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                                    <?= isset($data[$typeID][$expenditureItemID][$monthNumber]) ?
                                                        TextHelper::invoiceMoneyFormat($data[$typeID][$expenditureItemID][$monthNumber]['flowSum'], 2) :
                                                        0; ?>
                                                </td>
                                                <?php $quarterSum += isset($data[$typeID][$expenditureItemID][$monthNumber]) ?
                                                    $data[$typeID][$expenditureItemID][$monthNumber]['flowSum'] : 0; ?>
                                                <?php if ($key % 3 == 0): ?>
                                                    <td class="odd text-right quarter-block can-hover quarter-<?= $key / 3; ?> item-quarter-<?= $expenditureItemID . '-' . $key / 3 ?>
                                                            <?= $checkMonth ? 'blur' : null; ?>"
                                                        role="row" width="10%"
                                                        style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                                        <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                                        $quarterSum = 0; ?>
                                                    </td>
                                                <?php endif; ?>
                                            <?php endforeach ?>
                                            <td class="odd total-block text-right can-hover item-total-flow-sum-<?= $expenditureItemID; ?> <?= $checkMonth ? 'blur' : null; ?>"
                                                role="row" width="10%">
                                                <?= isset($data[$typeID][$expenditureItemID]['totalFlowSum']) ?
                                                    TextHelper::invoiceMoneyFormat($data[$typeID][$expenditureItemID]['totalFlowSum'], 2) : 0; ?>
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <?php if ($class == 'expense'): ?>
                                <tr>
                                    <td class="odd bold total-block-text" role="row">
                                        <?= FlowOfFundsReportSearch::$purseGrowingBalanceLabelByType[$typeID]; ?>
                                    </td>
                                    <?php $key = $quarterSum = 0; ?>
                                    <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>
                                        <?php $key++;
                                        $quarter = ceil($key / 3);
                                        $amount = isset($data['growingBalanceByBlock'][FlowOfFundsReportSearch::$purseBlockByType[$typeID]][$monthNumber]) ?
                                            $data['growingBalanceByBlock'][FlowOfFundsReportSearch::$purseBlockByType[$typeID]][$monthNumber]['flowSum'] : 0; ?>
                                        <td class="odd text-right bold quarter-month-<?= ceil($key / 3); ?> total-month-<?= $key; ?>
                                        <?= ($checkMonth && $monthNumber == date('m')) || !$checkMonth ? null : 'blur'; ?>"
                                            role="row"
                                            style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                            <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                                        </td>
                                        <?php if ($key % 3 == 0): ?>
                                            <td class="odd text-right bold quarter-<?= $key / 3; ?> remainder-month-quarter-<?= $key / 3; ?>
                                            <?= $checkMonth ? 'blur' : null; ?>" role="row"
                                                style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                                <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                                            </td>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <td class="odd text-right bold total-month-total-flow-sum <?= $checkMonth ? 'blur' : null; ?>"
                                        role="row">
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <tr>
                            <td class="odd bold total-month-text" role="row">Результат по месяцу</td>
                            <?php $key = $quarterSum = 0; ?>
                            <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>
                                <?php $key++;
                                $quarter = ceil($key / 3); ?>
                                <td class="odd text-right bold quarter-month-<?= ceil($key / 3); ?> total-month-<?= $key; ?>
                                        <?= ($checkMonth && $monthNumber == date('m')) || !$checkMonth ? null : 'blur'; ?>"
                                    role="row"
                                    style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                    <?= TextHelper::invoiceMoneyFormat(isset($balance[$monthNumber]) ? $balance[$monthNumber] : 0, 2); ?>
                                </td>
                                <?php $quarterSum += isset($balance[$monthNumber]) ? $balance[$monthNumber] : 0; ?>
                                <?php if ($key % 3 == 0): ?>
                                    <td class="odd text-right bold quarter-<?= $key / 3; ?> total-month-quarter-<?= $key / 3; ?>
                                            <?= $checkMonth ? 'blur' : null; ?>"
                                        role="row"
                                        style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                        <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                        $quarterSum = 0; ?>
                                    </td>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <td class="odd text-right bold total-month-total-flow-sum <?= $checkMonth ? 'blur' : null; ?>"
                                role="row">
                                <?= TextHelper::invoiceMoneyFormat(isset($balance['totalFlowSum']) ? $balance['totalFlowSum'] : 0, 2); ?>
                            </td>
                        </tr>
                        <tr class="not-drag cancel-drag">
                            <td class="odd bold remainder-month-text" role="row">Остаток на конец месяца</td>
                            <?php $key = 0; ?>
                            <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>
                                <?php $key++;
                                $quarter = ceil($key / 3); ?>
                                <td class="odd text-right bold quarter-month-<?= ceil($key / 3); ?> remainder-month-<?= $key; ?>
                                        <?= $checkMonth ? 'blur' : null; ?>" role="row"
                                    style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                    <?= TextHelper::invoiceMoneyFormat(isset($growingBalance[$monthNumber]) ? $growingBalance[$monthNumber] : 0, 2); ?>
                                </td>
                                <?php if ($key % 3 == 0): ?>
                                    <td class="odd text-right bold quarter-<?= $key / 3; ?> remainder-month-quarter-<?= $key / 3; ?>
                                            <?= $checkMonth ? 'blur' : null; ?>" role="row"
                                        style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                        <?= TextHelper::invoiceMoneyFormat(isset($growingBalance[$monthNumber]) ? $growingBalance[$monthNumber] : 0, 2); ?>
                                    </td>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <td class="odd text-right bold remainder-month-empty" role="row"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
