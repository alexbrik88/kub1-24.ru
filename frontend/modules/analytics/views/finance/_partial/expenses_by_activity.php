<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.04.2019
 * Time: 12:54
 */

use common\components\helpers\Html;
use yii\helpers\Url;
use frontend\modules\analytics\models\ExpensesSearch;
use kartik\checkbox\CheckboxX;
use common\components\TextHelper;

/* @var $this yii\web\View
 * @var $activeTab integer
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $searchModel ExpensesSearch
 * @var $data array
 */

$isCurrentYear = $searchModel->isCurrentYear;
?>
<div class="portlet box darkblue" style="margin-bottom: 5px;">
    <?= Html::beginForm(['expenses', 'activeTab' => $activeTab], 'GET', [
        'validateOnChange' => true,
    ]); ?>
    <div class="search-form-default">
        <div class="col-md-12 pull-right serveces-search" style="padding-left: 5px;">
            <div class="input-group" style="display: block;">
                <div class="input-cont">
                    <div class="col-md-2 p-l-0">
                        <?= Html::activeDropDownList($searchModel, 'year', $searchModel->getYearFilter(), [
                            'class' => 'form-control',
                            'style' => 'display: inline-block;',
                            'value' => $searchModel->year,
                        ]); ?>
                    </div>
                    <div class="col-md-10" style="padding: 0 0 7px 0;">
                        <?= Html::a('<i class="fa fa-file-excel-o"></i> Скачать в Excel', Url::to(['/analytics/finance/get-xls', 'type' => $activeTab, 'year' => $searchModel->year]), [
                            'class' => 'get-xls-link download-odds-xls pull-right',
                            'style' => 'margin-right: 1px;',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <div class="dataTables_wrapper dataTables_extended_wrapper no-footer scroll-table"
                 id="datatable_ajax_wrapper">
                <div class="scroll-table-wrapper">
                    <table class="table table-striped table-bordered table-hover flow-of-funds">
                        <thead class="not-fixed">
                        <tr class="heading quarters-flow-of-funds" role="row">
                            <th width="20%">
                                <?= CheckboxX::widget([
                                    'id' => 'main-checkbox-side',
                                    'name' => 'main-checkbox-side',
                                    'value' => false,
                                    'options' => [
                                        'class' => 'main-checkbox-side',
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]); ?> Статьи
                            </th>
                            <th colspan="<?= $currentMonthNumber < 4 && $isCurrentYear ? 3 : 1; ?>"
                                class="text-left quarter-th" data-quarter="1"
                                style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber < 4 && $isCurrentYear ? '180px' : '172px'; ?>;">
                                <?= CheckboxX::widget([
                                    'id' => 'first-quarter',
                                    'name' => 'quarter-month-1',
                                    'value' => !($currentQuarter == 1 && $isCurrentYear),
                                    'options' => [
                                        'class' => 'quarter-checkbox',
                                        'data' => [
                                            'month-class' => 'quarter-month-1',
                                            'total-quarter-class' => 'quarter-1',
                                        ],
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]); ?>
                                <label for="first-quarter">
                                    1 <?= $currentMonthNumber < 4 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $searchModel->year; ?>
                                </label>
                            </th>
                            <th colspan="<?= $currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear ? 3 : 1; ?>"
                                class="text-left quarter-th" data-quarter="2"
                                style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear ? '180px' : '172px'; ?>;">
                                <?= CheckboxX::widget([
                                    'id' => 'second-quarter',
                                    'name' => 'quarter-month-1',
                                    'value' => !($currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear),
                                    'options' => [
                                        'class' => 'quarter-checkbox',
                                        'data' => [
                                            'month-class' => 'quarter-month-2',
                                            'total-quarter-class' => 'quarter-2',
                                        ],
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]); ?>
                                <label for="second-quarter">
                                    2 <?= $currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $searchModel->year; ?>
                                </label>
                            </th>
                            <th colspan="<?= $currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear ? 3 : 1; ?>"
                                class="text-left quarter-th" data-quarter="3"
                                style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear ? '180px' : '172px'; ?>;">
                                <?= CheckboxX::widget([
                                    'id' => 'third-quarter',
                                    'name' => 'quarter-month-1',
                                    'value' => !($currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear),
                                    'options' => [
                                        'class' => 'quarter-checkbox',
                                        'data' => [
                                            'month-class' => 'quarter-month-3',
                                            'total-quarter-class' => 'quarter-3',
                                        ],
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]); ?>
                                <label for="third-quarter">
                                    3 <?= $currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $searchModel->year; ?>
                                </label>
                            </th>
                            <th colspan="<?= $currentMonthNumber > 9 && $isCurrentYear ? 3 : 1; ?>"
                                class="text-left quarter-th" data-quarter="4"
                                style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber > 9 && $isCurrentYear ? '180px' : '172px'; ?>;">
                                <?= CheckboxX::widget([
                                    'id' => 'fourth-quarter',
                                    'name' => 'quarter-month-1',
                                    'value' => !($currentMonthNumber > 9 && $isCurrentYear),
                                    'options' => [
                                        'class' => 'quarter-checkbox',
                                        'data' => [
                                            'month-class' => 'quarter-month-4',
                                            'total-quarter-class' => 'quarter-4',
                                        ],
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]); ?>
                                <label for="fourth-quarter">
                                    4 <?= $currentMonthNumber > 9 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $searchModel->year; ?>
                                </label>
                            </th>
                            <th width="10%" class="text-center" style="border-right: 1px solid #ddd;">
                                <?= $searchModel->year; ?>
                            </th>
                        </tr>
                        <tr class="heading" role="row">
                            <th width="20%"></th>
                            <?php foreach (ExpensesSearch::$month as $key => $month): ?>
                                <?php $quarter = ceil($key / 3); ?>
                                <th class="quarter-month-<?= $quarter; ?> text-left" width="10%"
                                    data-month="<?= $key; ?>"
                                    style="min-width: 117px;display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                    <?= $month; ?>
                                </th>
                                <?php if ($key % 3 == 0): ?>
                                    <th class="text-left quarter-<?= $quarter; ?>" width="10%"
                                        style="min-width: 117px;display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                        Итого
                                    </th>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <th class="text-center" width="10%">Итого</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="not-drag cancel-drag expenditure_type main-block"
                            id="<?= ExpensesSearch::BLOCK_EXPENSES; ?>">
                            <td class="odd operation-actitivities-block">
                                <?= CheckboxX::widget([
                                    'id' => 'flow-of-funds-type-' . ExpensesSearch::BLOCK_EXPENSES,
                                    'name' => 'flow-of-funds-type',
                                    'value' => false,
                                    'options' => [
                                        'class' => 'flow-of-funds-type',
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]); ?>
                                <label class="bold" for="flow-of-funds-type-<?= ExpensesSearch::BLOCK_EXPENSES; ?>">
                                    Расходы
                                </label>
                            </td>
                            <?php $key = $quarterSum = 0; ?>
                            <?php foreach (ExpensesSearch::$month as $monthNumber => $monthText): ?>
                                <?php $key++;
                                $quarter = ceil($key / 3); ?>
                                <td class="odd text-right can-hover quarter-month-<?= ceil($key / 3); ?> quarter-month-<?= ExpensesSearch::BLOCK_EXPENSES . '-' . $key; ?>"
                                    role="row"
                                    style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                    <?= isset($data['types'][ExpensesSearch::BLOCK_EXPENSES][$monthNumber]) ?
                                        TextHelper::invoiceMoneyFormat($data['types'][ExpensesSearch::BLOCK_EXPENSES][$monthNumber]['flowSum'], 2) : 0; ?>
                                </td>
                                <?php $quarterSum += isset($data['types'][ExpensesSearch::BLOCK_EXPENSES][$monthNumber]) ?
                                    $data['types'][ExpensesSearch::BLOCK_EXPENSES][$monthNumber]['flowSum'] : 0; ?>
                                <?php if ($key % 3 == 0): ?>
                                    <td class="odd text-right can-hover quarter-block quarter-<?= $key / 3; ?> quarter-<?= ExpensesSearch::BLOCK_EXPENSES . '-' . ($key / 3); ?>"
                                        role="row"
                                        style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                        <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                        $quarterSum = 0; ?>
                                    </td>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <td class="odd total-block text-right can-hover total-flow-sum-<?= ExpensesSearch::BLOCK_EXPENSES; ?>"
                                role="row">
                                <?= isset($data['types'][ExpensesSearch::BLOCK_EXPENSES]['totalFlowSum']) ?
                                    TextHelper::invoiceMoneyFormat($data['types'][ExpensesSearch::BLOCK_EXPENSES]['totalFlowSum'], 2) : 0; ?>
                            </td>
                        </tr>
                        <?php if (isset($data[ExpensesSearch::BLOCK_EXPENSES])): ?>
                            <?php foreach ($data['itemName'][ExpensesSearch::BLOCK_EXPENSES] as $expenditureItemID => $expenditureItemName): ?>
                                <?php if (isset($data[ExpensesSearch::BLOCK_EXPENSES][$expenditureItemID])): ?>
                                    <tr data-id="<?= ExpensesSearch::BLOCK_EXPENSES; ?>"
                                        data-item_id="<?= $expenditureItemID; ?>"
                                        class="item-block item-<?= $expenditureItemID . ' expense'; ?>">
                                        <td width="20%" class="item-name-<?= 'expense-' . $expenditureItemID; ?>"
                                            style="padding-left: 30px;">
                                            <?= $expenditureItemName; ?>
                                        </td>
                                        <?php $key = $quarterSum = 0; ?>
                                        <?php foreach (ExpensesSearch::$month as $monthNumber => $monthText): ?>
                                            <?php $key++;
                                            $quarter = ceil($key / 3); ?>
                                            <td class="odd text-right can-hover quarter-month-<?= ceil($key / 3); ?> item-month-<?= $expenditureItemID . '-' . $key; ?>"
                                                role="row" width="10%"
                                                style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                                <?= isset($data[ExpensesSearch::BLOCK_EXPENSES][$expenditureItemID][$monthNumber]) ?
                                                    TextHelper::invoiceMoneyFormat($data[ExpensesSearch::BLOCK_EXPENSES][$expenditureItemID][$monthNumber]['flowSum'], 2) :
                                                    0; ?>
                                            </td>
                                            <?php $quarterSum += isset($data[ExpensesSearch::BLOCK_EXPENSES][$expenditureItemID][$monthNumber]) ?
                                                $data[ExpensesSearch::BLOCK_EXPENSES][$expenditureItemID][$monthNumber]['flowSum'] : 0; ?>
                                            <?php if ($key % 3 == 0): ?>
                                                <td class="odd text-right quarter-block can-hover quarter-<?= $key / 3; ?> item-quarter-<?= $expenditureItemID . '-' . $key / 3 ?>"
                                                    role="row"
                                                    style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;"
                                                    width="10%">
                                                    <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                                    $quarterSum = 0; ?>
                                                </td>
                                            <?php endif; ?>
                                        <?php endforeach ?>
                                        <td class="odd total-block text-right can-hover item-total-flow-sum-<?= $expenditureItemID; ?>"
                                            role="row" width="10%">
                                            <?= isset($data[ExpensesSearch::BLOCK_EXPENSES][$expenditureItemID]['totalFlowSum']) ?
                                                TextHelper::invoiceMoneyFormat($data[ExpensesSearch::BLOCK_EXPENSES][$expenditureItemID]['totalFlowSum'], 2) : 0; ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <tr class="not-drag cancel-drag">
                            <td class="odd bold remainder-month-text" role="row">Итого на конец месяца</td>
                            <?php $key = $quarterSum = 0; ?>
                            <?php foreach (ExpensesSearch::$month as $monthNumber => $monthText): ?>
                                <?php $key++;
                                $quarter = ceil($key / 3); ?>
                                <td class="odd text-right bold quarter-month-<?= ceil($key / 3); ?> remainder-month-<?= $key; ?>"
                                    role="row"
                                    style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                    <?= TextHelper::invoiceMoneyFormat(isset($data['growingBalance'][$monthNumber]) ? $data['growingBalance'][$monthNumber] : 0, 2); ?>
                                </td>
                                <?php $quarterSum += isset($data['growingBalance'][$monthNumber]) ? $data['growingBalance'][$monthNumber] : 0; ?>
                                <?php if ($key % 3 == 0): ?>
                                    <td class="odd text-right bold quarter-<?= $key / 3; ?> remainder-month-quarter-<?= $key / 3; ?>"
                                        role="row"
                                        style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                        <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                        $quarterSum = 0; ?>
                                    </td>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <td class="odd text-right bold remainder-month-empty" role="row">
                                <?= TextHelper::invoiceMoneyFormat(isset($data['growingBalance']['totalFlowSum']) ? $data['growingBalance']['totalFlowSum'] : 0, 2); ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
