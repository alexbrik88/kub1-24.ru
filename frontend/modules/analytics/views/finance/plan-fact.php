<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 28.02.2019
 * Time: 0:33
 */

use common\components\helpers\Html;
use yii\helpers\Url;
use frontend\modules\analytics\models\PlanFactSearch;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/* @var $this yii\web\View
 * @var $activeTab integer
 * @var $searchModel PlanFactSearch
 * @var $data array
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $checkMonth boolean
 */
$this->title = 'План-Факт (ПФ)';
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized', 'tooltip_pay-odds'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-deviation',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized', 'tooltip_pay-odds'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);
?>
<div class="portlet box odds-report-header"
     style="<?= $searchModel->company->isFreeTariff ? 'margin-bottom: 15px;' : null; ?>">
    <div class="btn-group pull-right title-buttons">
        <div class="portlet-body employee-wrapper">
            <div class="tabbable tabbable-tabdrop">
                <ul class="nav nav-tabs li-border float-r segmentation-tabs">
                    <li class="<?= $activeTab == PlanFactSearch::TAB_BY_ACTIVITY ? 'active' : null; ?>">
                        <?= Html::a('ПФ по видам деятельности',
                            Url::to(['plan-fact', 'activeTab' => PlanFactSearch::TAB_BY_ACTIVITY, 'PlanFactSearch' => [
                                'year' => $searchModel->year,
                            ]]), [
                                'aria-expanded' => 'false',
                                'style' => 'margin-right: 0;',
                            ]); ?>
                    </li>
                    <li class="<?= $activeTab == PlanFactSearch::TAB_BY_PURSE ? 'active' : null; ?>"
                        style="margin-right: 0;float: right;">
                        <?= Html::a('ПФ по кошелькам',
                            Url::to(['plan-fact', 'activeTab' => PlanFactSearch::TAB_BY_PURSE, 'PlanFactSearch' => [
                                'year' => $searchModel->year,
                            ]]), [
                                'aria-expanded' => 'false',
                                'style' => 'margin-right: 0;',
                            ]); ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <h3 class="page-title" style="<?= $searchModel->company->isFreeTariff ? 'margin-bottom: 5px;' : null; ?>">
        <?= $this->title; ?>
        <img src="/img/open-book.svg" class="plan-fact-panel-trigger">
    </h3>
    <?php if ($searchModel->company->isFreeTariff): ?>
        <span class="free-tariff-warning">Отчет доступен полностью только на платном тарифе</span>
    <?php endif; ?>
</div>
<div class="tab-content">
    <div class="tab-pane fade in active">
        <?php if ($activeTab == PlanFactSearch::TAB_BY_ACTIVITY): ?>
            <?= $this->render('_partial/plan_fact_by_activity', [
                'searchModel' => $searchModel,
                'activeTab' => $activeTab,
                'data' => $data,
                'currentMonthNumber' => $currentMonthNumber,
                'currentQuarter' => $currentQuarter,
                'checkMonth' => $checkMonth
            ]); ?>
        <?php elseif ($activeTab == PlanFactSearch::TAB_BY_PURSE): ?>
            <?= $this->render('_partial/plan_fact_by_purse', [
                'searchModel' => $searchModel,
                'activeTab' => $activeTab,
                'data' => $data,
                'currentMonthNumber' => $currentMonthNumber,
                'currentQuarter' => $currentQuarter,
                'checkMonth' => $checkMonth
            ]); ?>
        <?php endif; ?>
        <?= Html::hiddenInput('activeTab', $activeTab, [
            'id' => 'active-tab_report',
        ]); ?>
    </div>
</div>
<?= $this->render('_partial/plan_fact_panel'); ?>
<div id="visible-right-menu" style="display: none;">
    <div id="visible-right-menu-wrapper" style="z-index: 10050!important;"></div>
</div>
<div id="hellopreloader" style="display: none;">
    <div id="hellopreloader_preload"></div>
</div>
<div class="tooltip-template" style="display: none;">
    <span id="tooltip_financial-operations-block" style="display: inline-block; text-align: center;">
        Привлечение денег (кредиты, займы)<br>
        в компанию и их возврат. <br>
        Выплата дивидендов.<br>
        Предоставление займов и депозитов.
    </span>
    <span id="tooltip_operation-activities-block" style="display: inline-block; text-align: center;">
        Движение денег, связанное с основной деятельностью компании <br>
        (оплата от покупателей, зарплата, аренда, покупка товаров и т.д.)
    </span>
    <span id="tooltip_investment-activities-block" style="display: inline-block; text-align: center;">
        Покупка и продажа оборудования и других основных средств. <br>
        Затраты на новые проекты и поступление выручки от них. <br>
    </span>
    <span id="item-plan-tooltip_template" style="display: inline-block;">
        <span class="pull-left" style="min-width: 100px;">Факт</span>
        <span class="pull-right fact-amount"></span><br>

        <span class="pull-left" style="min-width: 100px;">План</span>
        <span class="pull-right plan-amount"></span><br>

        <span class="pull-left text-bold" style="min-width: 100px;">Разница</span>
        <span class="pull-right diff-amount text-bold"></span><br>

        <span class="pull-left text-bold" style="min-width: 100px;">Отклонение</span>
        <span class="pull-right deviation text-bold"></span><br>
    </span>
    <span id="tooltip_deviation_block" class="text-center" style="display: inline-block;">
        Допустимое отклонение <br>
        Плана от Факта
    </span>
</div>
<?php $this->registerJs('
    $(".tooltip2-hover:not(.blur)").tooltipster({
        multiple: true,
        theme: [\'tooltipster-noir\', \'tooltipster-noir-customized\', \'tooltip_pay-odds\'],
        contentCloning: true,
        contentAsHTML: true,
        trigger: "hover",
        functionBefore: function (instance) {
            var $template = $("#item-plan-tooltip_template");
            var $item = instance._$origin;
            var $deviation = null;
            var $factAmount = null;
            var $planAmount = null;
            var $diffAmount = null;

            if ($item.data("deviation") == "-") {
                $deviation = $item.data("deviation")
            } else {
                $deviation = $item.data("deviation") + "%";
            }
            if ($item.data("fact-amount") == "-") {
                $factAmount = $item.data("fact-amount")
            } else {
                $factAmount = $item.data("fact-amount") + "<span style=\"padding-left: 5px;\">₽</span>";
            }
            if ($item.data("plan-amount") == "-") {
                $planAmount = $item.data("plan-amount")
            } else {
                $planAmount = $item.data("plan-amount") + "<span style=\"padding-left: 5px;\">₽</span>";
            }
            if ($item.data("diff-amount") == "-") {
                $diffAmount = $item.data("diff-amount")
            } else {
                $diffAmount = $item.data("diff-amount") + "<span style=\"padding-left: 5px;\">₽</span>";
            }

            $template.find(".fact-amount").html($factAmount);
            $template.find(".plan-amount").html($planAmount);
            $template.find(".diff-amount").html($diffAmount);
            $template.find(".deviation").text($deviation);
            instance.content($template);
        }
    });

    var slider = document.getElementById("range-deviation_input");
    var output = document.getElementById("deviation-text");
    var tableItems = $("table.flow-of-funds tbody td.tooltip2-hover");

    slider.oninput = function() {
        var $filterDeviationVal = +this.value;

        output.innerHTML = $filterDeviationVal + "%";
        tableItems.each(function (e) {
            var $deviation = Math.abs(parseFloat($(this).data("deviation").replace(\' \', \'\').replace(\',\', \'.\')));
            if (!isNaN($deviation) && $deviation < $filterDeviationVal) {
                $(this).addClass("filter-deviation");
            } else if (!isNaN($deviation) && $deviation > $filterDeviationVal) {
                $(this).removeClass("filter-deviation");
            }
        });
    }

     $(".plan-fact-panel-trigger").click(function (e) {
        $(".plan-fact-panel").toggle("fast");
        $("#visible-right-menu").show();
        $("html").attr("style", "overflow: hidden;");
        return false;
    });

    $(document).on("click", ".panel-block .side-panel-close, .panel-block .side-panel-close-button", function (e) {
        var $panel = $(this).closest(".panel-block");

        $panel.toggle("fast");
        $("#visible-right-menu").hide();
        $("html").removeAttr("style");
    });

    $("#visible-right-menu").click(function (e) {
        var $panel = $(".panel-block:visible");

        $panel.toggle("fast");
        $(this).hide();
        $("html").removeAttr("style");
    });
'); ?>
