<?php

use frontend\modules\analytics\models\BalanceSearch;
use yii\helpers\Html;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use kartik\checkbox\CheckboxX;
use frontend\modules\analytics\models\FlowOfFundsReportSearch;
use common\components\helpers\Month;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\analytics\models\BalanceSearch */
/* @var $balance array */

$this->title = 'Баланс';
$currentMonthNumber = date('n');
$currentQuarter = (int)ceil(date('m') / 3);
$isCurrentYear = date('Y') == $model->year;
$activeCheckbox = explode(',', Yii::$app->session->remove('activeCheckbox'));

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-balance',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>

<style>
    .balance-report-table tr > th:last-child,
    .balance-report-table tr > td:last-child {
        display: none!important;
    }
</style>

<div class="portlet box odds-report-header">
    <h3 class="page-title">
        <?= $this->title; ?>
        <img src="/img/open-book.svg" class="balance-panel-trigger">
    </h3>
</div>
<div class="portlet box darkblue">
    <?= Html::beginForm(['balance',], 'GET', [
        'validateOnChange' => true,
    ]); ?>
    <div class="search-form-default">
        <div class="col-md-12 pull-right serveces-search" style="padding-left: 5px;">
            <div class="input-group" style="display: block;">
                <div class="input-cont">
                    <div class="col-md-2 p-l-0">
                        <?= Html::activeDropDownList($model, 'year', $model->getYearFilter(), [
                            'name' => 'year',
                            'class' => 'form-control',
                            'style' => 'display: inline-block;',
                            'value' => $model->year,
                        ]); ?>
                    </div>
                    <div class="col-md-10" style="padding-right: 0; padding-left: 0">
                        <?= Html::a('<i class="fa fa-file-excel-o"></i> Скачать в Excel',
                            Url::to(['/analytics/finance/get-xls', 'type' => BalanceSearch::TAB_BALANCE, 'year' => $model->year]), [
                            'class' => 'get-xls-link download-odds-xls pull-right',
                        ]); ?>
                        <?= Html::a('<img src="/img/icons/settings.png"> Инструкция', 'javascript:;', [
                            'class' => 'settings-balance-panel-trigger pull-right',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>
    <div class="portlet-body accounts-list">
        <div class="balance-report scroll-table-wrapper" style="">
            <table class="table table-bordered balance-report-table" style="width: auto;">
                <thead>
                <tr class="heading quarters-flow-of-funds" role="row">
                    <th style="min-width: 270px;">
                        <?= CheckboxX::widget([
                            'id' => 'main-checkbox-side',
                            'name' => 'main-checkbox-side',
                            'value' => true,
                            'options' => [
                                'class' => 'main-checkbox-side',
                            ],
                            'pluginOptions' => [
                                'size' => 'xs',
                                'threeState' => false,
                                'inline' => false,
                                'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                            ],
                        ]); ?> Статьи
                    </th>
                    <th colspan="<?= $currentMonthNumber < 4 && $isCurrentYear ? 3 : 1; ?>"
                        class="text-left quarter-th"
                        style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber < 4 && $isCurrentYear ? '180px' : '172px'; ?>;">
                        <?= CheckboxX::widget([
                            'id' => 'first-quarter',
                            'name' => 'quarter-month-1',
                            'value' => !($currentQuarter == 1 && $isCurrentYear),
                            'options' => [
                                'class' => 'quarter-checkbox',
                                'data' => [
                                    'month-class' => 'quarter-month-1',
                                    'total-quarter-class' => 'quarter-1',
                                ],
                            ],
                            'pluginOptions' => [
                                'size' => 'xs',
                                'threeState' => false,
                                'inline' => false,
                                'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                            ],
                        ]); ?>
                        <label for="first-quarter">
                            1 <?= $currentMonthNumber < 4 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $model->year; ?>
                        </label>
                    </th>
                    <th colspan="<?= $currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear ? 3 : 1; ?>"
                        class="text-left quarter-th"
                        style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear ? '180px' : '172px'; ?>;">
                        <?= CheckboxX::widget([
                            'id' => 'second-quarter',
                            'name' => 'quarter-month-1',
                            'value' => !($currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear),
                            'options' => [
                                'class' => 'quarter-checkbox',
                                'data' => [
                                    'month-class' => 'quarter-month-2',
                                    'total-quarter-class' => 'quarter-2',
                                ],
                            ],
                            'pluginOptions' => [
                                'size' => 'xs',
                                'threeState' => false,
                                'inline' => false,
                                'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                            ],
                        ]); ?>
                        <label for="second-quarter">
                            2 <?= $currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $model->year; ?>
                        </label>
                    </th>
                    <th colspan="<?= $currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear ? 3 : 1; ?>"
                        class="text-left quarter-th"
                        style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear ? '180px' : '172px'; ?>;">
                        <?= CheckboxX::widget([
                            'id' => 'third-quarter',
                            'name' => 'quarter-month-1',
                            'value' => !($currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear),
                            'options' => [
                                'class' => 'quarter-checkbox',
                                'data' => [
                                    'month-class' => 'quarter-month-3',
                                    'total-quarter-class' => 'quarter-3',
                                ],
                            ],
                            'pluginOptions' => [
                                'size' => 'xs',
                                'threeState' => false,
                                'inline' => false,
                                'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                            ],
                        ]); ?>
                        <label for="third-quarter">
                            3 <?= $currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $model->year; ?>
                        </label>
                    </th>
                    <th colspan="<?= $currentMonthNumber > 9 && $isCurrentYear ? 3 : 1; ?>"
                        class="text-left quarter-th"
                        style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber > 9 && $isCurrentYear ? '180px' : '172px'; ?>;">
                        <?= CheckboxX::widget([
                            'id' => 'fourth-quarter',
                            'name' => 'quarter-month-1',
                            'value' => !($currentMonthNumber > 9 && $isCurrentYear),
                            'options' => [
                                'class' => 'quarter-checkbox',
                                'data' => [
                                    'month-class' => 'quarter-month-4',
                                    'total-quarter-class' => 'quarter-4',
                                ],
                            ],
                            'pluginOptions' => [
                                'size' => 'xs',
                                'threeState' => false,
                                'inline' => false,
                                'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                            ],
                        ]); ?>
                        <label for="fourth-quarter">
                            4 <?= $currentMonthNumber > 9 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $model->year; ?>
                        </label>
                    </th>
                    <th width="10%" class="text-center" style="border-right: 1px solid #ddd;">
                        <?= $model->year; ?>
                    </th>
                </tr>
                <tr class="heading" role="row">
                    <th width="20%"></th>
                    <?php foreach (FlowOfFundsReportSearch::$month as $key => $month): ?>
                        <?php $quarter = ceil($key / 3); ?>
                        <th class="quarter-month-<?= $quarter; ?> text-left" width="10%"
                            style="min-width: 117px;display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                            <?php if ($key == date('m') && $model->year == date('Y')): ?>
                                <?= date('d') . ' ' . Month::$monthGenitiveRU[$key]; ?>
                            <?php else: ?>
                                <?= $month; ?>
                            <?php endif; ?>
                        </th>
                        <?php if ($key % 3 == 0): ?>
                            <th class="text-left quarter-<?= $quarter; ?>" width="10%"
                                style="min-width: 117px;display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                Итого
                            </th>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <th class="text-center" width="10%">Итого</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($balance as $item) : ?>
                    <?= $model->renderRow($item) ?>
                <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="update-balance-item-panel">
    <?php $form = ActiveForm::begin([
        'action' => Url::to(['update-balance-item',]),
        'id' => 'update-contractor-form',
    ]); ?>
    <?php foreach (Month::$monthShort as $monthNumber => $monthName): ?>
        <div class="month-block">
            <span class="name text-center" style="text-align: center;">
                <?= $monthName; ?> <?= substr($model->year, 2, 4); ?>
            </span>
            <?= Html::textInput("BalanceItemAmount[{$monthNumber}]", null, [
                'class' => 'js_input_to_money_format amount-input',
                'style' => 'width: 100%;',
            ]); ?>
        </div>
    <?php endforeach; ?>
    <?= Html::hiddenInput('itemID', null, [
        'id' => 'item-id',
    ]); ?>
    <?= Html::hiddenInput('year', $model->year); ?>
    <?= Html::hiddenInput('activeCheckbox', null, [
        'id' => 'active-checkbox',
    ]); ?>
    <div class="text-center buttons">
        <?= Html::submitButton('Сохранить', [
            'class' => 'btn darkblue',
            'style' => 'width: 12%!important;',
        ]); ?>
        <?= Html::a('Очистить данные', 'javascript:;', [
            'class' => 'btn darkblue clear-balance-inputs',
            'style' => 'width: 15%!important;',
        ]); ?>
        <?= Html::a('Отменить', 'javascript:;', [
            'class' => 'btn darkblue undo-update-balance-item',
            'style' => 'width: 12%!important;',
        ]); ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?= $this->render('_partial/balance_panel'); ?>
<?= $this->render('_partial/balance_settings_panel'); ?>
<div id="visible-right-menu" style="display: none;">
    <div id="visible-right-menu-wrapper" style="z-index: 10050!important;"></div>
</div>
<div id="hellopreloader" style="display: none;">
    <div id="hellopreloader_preload"></div>
</div>
<div class="tooltip-template" style="display: none;">
<span id="tooltip-balance-liabilities" style="display: inline-block; text-align: center;">
    <span style="font-weight: bold;">Обязательства.</span>
    <br>
    Откуда получены денежные средства.
</span>
<span id="tooltip-balance-assets" style="display: inline-block; text-align: center;">
    <span style="font-weight: bold;">Чем вы владеете.</span>
    <br>
    Куда вложены и на что потрачены
    <br>
    денежные средства.
</span>
</div>

<?php // COMPANY_53146
if (YII_ENV_DEV || Yii::$app->user->identity->company->id == 53146): ?>
    <?= $this->render('_partial/balance_chart'); ?>
<?php endif; ?>

<?php $this->registerJs('
    var $activeCheckbox = ' . json_encode($activeCheckbox) . ';

    $(document).ready(function (e) {
        if ($activeCheckbox.indexOf("main-checkbox-side") != -1) {
            $("#main-checkbox-side").click();
        } else {
            for (var i in $activeCheckbox) {
                if ($activeCheckbox[i] !== "") {
                    if ($activeCheckbox[i] == "w2" || $activeCheckbox[i] == "w3" || $activeCheckbox[i] == "w4") {
                        if ($("#w2").val() == 1) {
                            $("#w2").click();
                        }
                    } else if ($activeCheckbox[i] == "w7" || $activeCheckbox[i] == "w8") {
                        if ($("#w7").val() == 1) {
                            $("#w7").click();
                        }
                    } else {
                        $("#" + $activeCheckbox[i]).click();
                    }
                }
            }
        }
    });

    $(document).on("click", ".update-balance-item", function (e) {
        var $panel = $(".update-balance-item-panel");
        var $line = $(this).closest("tr");

        $(".balance-report-table tr.active-balance").removeClass("active-balance");
        if ($panel.is(":visible") && +$panel.find("#item-id").val() == +$line.data("itemid")) {
            $panel.find(".undo-update-balance-item").click();
        } else {
            if ($panel.is(":visible")) {
                $panel.find(".undo-update-balance-item").click();
                setTimeout(animate, 420);
            } else {
                    animate();
            }
            function animate() {
                $line.addClass("active-balance");
                $("#item-id").val($line.data("itemid"));
                $line.find(".quarter-month-1, .quarter-month-2, .quarter-month-3, .quarter-month-4").each(function (i) {
                    var $amount = $(this).text().trim();

                    ++i;
                    if ($amount !== "" && $amount !== "0,00") {
                        $("input[name=\"BalanceItemAmount[" + i + "]\"]").val($amount.replace(",", ".").replace(/ /g, ""));
                    }
                });
                var $checked = [];
                $(".balance-type, .main-checkbox-side").each(function (i) {
                    if ($(this).val() == 0) {
                        $checked[i] = $(this).attr("id");
                    }
                });
                $("#active-checkbox").val($checked);
                $panel.css("top", (+$line.offset().top - 10) + "px");
                $panel.slideDown();
            }
        }
    });

    $(document).on("click", ".undo-update-balance-item", function (e) {
        var $panel = $(this).closest(".update-balance-item-panel");

        $(".balance-report-table tr.active-balance").removeClass("active-balance");
        $panel.find("input.amount-input, input#item-id").val("");
        $panel.slideUp();
    });

    $(document).on("click", ".clear-balance-inputs", function (e) {
        var $panel = $(this).closest(".update-balance-item-panel");

        $panel.find("input.amount-input").val("");
    });

    $(".balance-panel-trigger").click(function (e) {
        $(".balance-panel").toggle("fast");
        $("#visible-right-menu").show();
        $("html").attr("style", "overflow: hidden;");
        return false;
    });

    $(".settings-balance-panel-trigger").click(function (e) {
        $(".balance-settings-panel").toggle("fast");
        $("#visible-right-menu").show();
        $("html").attr("style", "overflow: hidden;");
        return false;
    });

    $(document).on("click", ".panel-block .side-panel-close, .panel-block .side-panel-close-button", function (e) {
        var $panel = $(this).closest(".panel-block");

        $panel.toggle("fast");
        $("#visible-right-menu").hide();
        $("html").removeAttr("style");
    });

    $("#visible-right-menu").click(function (e) {
        var $panel = $(".panel-block:visible");

        $panel.toggle("fast");
        $(this).hide();
        $("html").removeAttr("style");
    });
'); ?>
