<?php
use \yii\web\JsExpression;
use \common\components\TextHelper;

$this->title = 'Точка безубыточности';

?>

<style>
    .graph-break-even .part {
        margin-bottom:10px;
        line-height: 20px;
    }
    .graph-break-even .value {
        font-size: 18px;
        font-weight: bold;
        color: #333;
    }
    .graph-break-even .caption {
        color:#999;
        font-size: 13px;
        text-transform: uppercase;
    }
    .graph-break-even .caption > i {
        font-size:16px;
        font-style: normal;
    }
    .graph-break-even .caption > a{
        font-size:11px;
        text-transform: none;
    }
    .graph-break-even-table {
        width:100%;
        height:36px;
    }
    .graph-break-even-table td {
        border-bottom:2px solid #999;

    }
    .graph-break-even-table td:first-child {
        width: 108px!important;
    }
    .graph-break-even-table td:nth-child(2n) {
        text-align: center;
        vertical-align: middle;
        font-size:22px;
        font-weight:bold;
    }
    .graph-break-even-table td:nth-child(2n+1) {
        color:#999;
        font-size: 12px;
        text-transform: uppercase;
    }
    .graph-break-even-table td i {
        font-style: normal;
    }
</style>


<div class="portlet box odds-report-header">
    <div class="btn-group pull-right title-buttons">
        <div class="portlet-body employee-wrapper">
            <div class="tabbable tabbable-tabdrop">
            </div>
        </div>
    </div>
    <h3 class="page-title" style="<?= Yii::$app->user->identity->company->isFreeTariff ? 'margin-bottom: 5px;' : null; ?>">
        <?= $this->title; ?>
        <img src="/img/open-book.svg" class="odds-panel-trigger">
    </h3>
    <?php /* if (Yii::$app->user->identity->company->isFreeTariff): ?>
        <span class="free-tariff-warning">Отчет доступен полностью только на платном тарифе</span>
    <?php endif; */ ?>
</div>

<?php

$tax_amount = 1500000; // сумма налогов
$fixed_cost = 1500000; // фикс. затраты
$break_even = 3000000; // точка Б
$revenue_curr = 7000000; // текущая вал. приб.
$cost_curr = 5000000; // тек. расходы

$xData   = [0, 200, 400, 600, 800, 1000, 1200, 1400, 1600, 1800, 2000, 2200];

$revenue = $costs = $fixed_costs = [];//
foreach ($xData as $index => $xValue) {
    $revenue[] = [$xValue, $index * 1000000];
    $costs[] = [$xValue, $index * 500000 + $fixed_cost];
    $fixed_costs[] = [$xValue, $fixed_cost];
}

$pointBreakEven = [[600, $break_even]];
$pointRevenueCurr = [[1400, $revenue_curr]];
$pointCostCurr = [[1400, $cost_curr]];

?>

<div class="row">
    <div style="margin:0 25px 10px 40px;">
        <div class="col-md-8">
            <div class="col-lg-4 col-md-12" style="padding:5px">
                <table class="graph-break-even-table">
                    <tr>
                        <td>Выручка</td>
                        <td><i id="revenue_tip_1"></i> ₽</td>
                    </tr>
                </table>
            </div>
            <div class="col-lg-4 col-md-12" style="padding:5px">
                <table class="graph-break-even-table">
                    <tr>
                        <td>Суммарные затраты</td>
                        <td><i id="revenue_tip_2"></i> ₽</td>
                    </tr>
                </table>
            </div>
            <div class="col-lg-4 col-md-12" style="padding:5px">
                <table class="graph-break-even-table">
                    <tr>
                        <td>Операционная прибыль</td>
                        <td><i id="revenue_tip_3"></i> ₽</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <?= \miloschuman\highcharts\Highcharts::widget([
            'id' => 'chart-break-even',
            'scripts' => [
                'themes/grid-light',
                'highcharts-more'
            ],
            'options' => [
                'chart' => [
                    'events' => [
                        'load' => new JsExpression('function(event) {
                        draggablePlotLine(this.yAxis[0], "foo");
                        console.log("Chart loaded"); }')
                    ],
                ],
                'title' => ['text' => ''],
                'exporting' => [
                    'enabled' => false,
                ],
                'xAxis' => [
                    [
                        'min' => 0,
                        'max' => 2200,
                        'tickmarkPlacement' => 'on',
                        'tickInterval' => 200,
                        'startOnTick' => true,
                        'endOnTick' => true
                    ]
                ],
                'yAxis' => [
                    'title' => '₽',
                    'min' => 0,
                    'plotLines' => [
                        [
                            'zIndex' => 4,
                            'color' => 'rgba(153,153,153,1)',
                            'width' => 2,
                            'value' => $break_even,
                            'dashStyle' => null,
                            'id' => 'foo',
                            'onDragStart' => new JsExpression('function (new_value) {
                                recalcChart(new_value) }'),
                            'onDragChange' => new JsExpression('function (new_value) {
                                recalcChart(new_value) }'),
                            'onDragFinish' => new JsExpression('function (new_value) {
                                recalcChart(new_value) }'),
                        ]
                    ],
                ],
                'series' => [
                    // POINTS
                    [
                        'name' => 'Точка безубыточности',
                        'data' => $pointBreakEven,
                        'marker' => [
                            'symbol' => 'circle',
                            'fillColor' => '#FFFFFF',
                            'radius' => 5,
                            'lineWidth' => 3,
                            'lineColor' => 'rgba(50,50,50,1)'
                        ],
                        'zIndex' => 2,
                        'showInLegend' => false,
                        'stickyTracking' => false
                    ],
                    [
                        'name' => 'Выручка',
                        'data' => $pointRevenueCurr,
                        'marker' => [
                            'symbol' => 'circle',
                            'fillColor' => '#FFFFFF',
                            'radius' => 5,
                            'lineWidth' => 3,
                            'lineColor' => 'rgba(127,221,127,1)'
                        ],
                        'zIndex' => 2,
                        'showInLegend' => false,
                        'stickyTracking' => false
                    ],
                    [
                        'name' => 'Суммарные затраты',
                        'data' => $pointCostCurr,
                        'marker' => [
                            'symbol' => 'circle',
                            'fillColor' => '#FFFFFF',
                            'radius' => 5,
                            'lineWidth' => 3,
                            'lineColor' => 'rgba(253,45,1,1)'
                        ],
                        'zIndex' => 2,
                        'showInLegend' => false,
                        'stickyTracking' => false
                    ],
                    // LINES
                    [
                        'type' => 'line',
                        'name' => 'Выручка',
                        'data' => $revenue,
                        'color' => 'rgba(127,221,127,1)',
                        'marker' => [
                            'symbol' => 'circle',
                            'radius' => 3
                        ],
                        'zIndex' => 1,
                        'enableMouseTracking' => false
                    ],
                    [
                        'type' => 'line',
                        'name' => 'Суммарные затраты',
                        'data' => $costs,
                        'color' => 'rgba(253,45,1,1)',
                        'marker' => [
                            'symbol' => 'circle',
                            'radius' => 3
                        ],
                        'zIndex' => 1,
                        'enableMouseTracking' => false
                    ],
                    [
                        'type' => 'line',
                        'name' => 'Фиксированные затраты',
                        'data' => $fixed_costs,
                        'color' => 'rgba(50,50,50,1)',
                        'marker' => [
                            'symbol' => 'circle',
                            'radius' => 3
                        ],
                        'enableMouseTracking' => false,
                        'showInLegend' => false,
                    ],
                    // POLYGONS
                    [
                        'name' => 'Polygon1',
                        'data' => null,
                        'type' => 'polygon',
                        'color' => 'rgba(253,45,1,.3)',
                        'lineWidth' => 0.25,
                        'lineColor' => '#333',
                        'fillOpacity' => 0.3,
                        'zIndex' => 0,
                        'marker' => [
                            'enabled' => false
                        ],
                        'enableMouseTracking' => false,
                        'showInLegend' => false,
                    ],
                ],
                'plotOptions' => [
                    'polygon' => [
                        'animation' => [
                            'duration' => 0
                        ]
                    ]
                ]
            ],

        ])
        ?>
    </div>
    <div class="col-md-4">
        <div class="graph-break-even">
            <div class="part">
                <div class="value">
                    Апрель, 2019
                </div>
            </div>
            <div class="part">
                <div class="caption">
                    <i style="color: rgba(127,221,127,1)">◉</i>
                    Выручка
                </div>
                <div class="value">
                    <?= TextHelper::moneyFormat($revenue_curr) ?> ₽
                </div>
            </div>
            <div class="part">
                <div class="caption">
                    <i style="color: rgba(253,45,1,1)">◉</i>
                    Суммарные затраты
                </div>
                <div class="value">
                    <?= TextHelper::moneyFormat($cost_curr) ?> ₽
                </div>
            </div>
            <div class="part">
                <div class="caption">
                    <i style="color: rgba(50,50,50,1)">◉</i>
                    Точка безубыточности
                </div>
                <div class="value">
                    <?= TextHelper::moneyFormat($break_even) ?> ₽
                </div>
            </div>
            <div class="part">
                <div class="caption">
                    Запас прочности
                </div>
                <div class="value">
                    <?= TextHelper::moneyFormat($revenue_curr - $break_even) ?> ₽ <!-- =(Выручка - Точка безубыточности) -->
                </div>
            </div>
            <div class="part">
                <div class="caption">
                    Переменные затраты
                </div>
                <div class="value">
                    <?= TextHelper::moneyFormat(($cost_curr - $fixed_cost) / $revenue_curr, 2) ?> ₽ на 1 ₽ выручки
                </div>
            </div>
            <div class="part">
                <div class="caption" style="position:relative;">
                    <i style="color: rgba(50,50,50,1); font-size: 14px;">▬</i>
                    <i style="color: rgba(50,50,50,1); position:absolute;left:2px;top:0;">●</i>
                    Фиксированные затраты
                </div>
                <div class="value">
                    <?= TextHelper::moneyFormat($fixed_cost) ?> ₽
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    var break_even_X = 600;
    var break_even_Y = <?= (int)$break_even ?>;
    var fixed_costs_Y = <?= (int) $fixed_cost ?>;
    var tax_Y = <?= (int) $tax_amount ?>;

    function nFormatter(num, digits) {

        var negative = false;
        if (num < 0) {
            num = Math.abs(num);
            negative = true;
        }

        var si = [
            { value: 1, symbol: "" },
            { value: 1E3, symbol: "k" },
            { value: 1E6, symbol: "M" },
        ];
        var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
        var i;
        for (i = si.length - 1; i > 0; i--) {
            if (num >= si[i].value) {
                break;
            }
        }

        return (((negative) ? -1 : 1 ) * num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
    }

    function draggablePlotLine(axis, plotLineId) {
        var clickX, clickY;

        var getPlotLine = function () {
            for (var i = 0; i < axis.plotLinesAndBands.length; i++) {
                if (axis.plotLinesAndBands[i].id === plotLineId) {
                    return axis.plotLinesAndBands[i];
                }
            }
        };

        var getValue = function() {
            var plotLine = getPlotLine();
            var translation = axis.horiz ? plotLine.svgElem.translateX : plotLine.svgElem.translateY;
            var new_value = axis.toValue(translation) - axis.toValue(0) + plotLine.options.value;
            new_value = Math.max(axis.min, Math.min(axis.max, new_value));
            return new_value;
        };

        var drag_start = function (e) {
            $(document).bind({
                'mousemove.line': drag_step,
                'mouseup.line': drag_stop
            });

            var plotLine = getPlotLine();
            clickX = e.pageX - plotLine.svgElem.translateX;
            clickY = e.pageY - plotLine.svgElem.translateY;
            if (plotLine.options.onDragStart) {
                plotLine.options.onDragStart(getValue());
            }
        };

        var drag_step = function (e) {
            var plotLine = getPlotLine();
            var new_translation = axis.horiz ? e.pageX - clickX : e.pageY - clickY;
            var new_value = axis.toValue(new_translation) - axis.toValue(0) + plotLine.options.value;
            new_value = Math.max(axis.min, Math.min(axis.max, new_value));
            new_translation = axis.toPixels(new_value + axis.toValue(0) - plotLine.options.value);
            plotLine.svgElem.translate(
                axis.horiz ? new_translation : 0,
                axis.horiz ? 0 : new_translation);

            if (plotLine.options.onDragChange) {
                plotLine.options.onDragChange(new_value);
            }
        };

        var drag_stop = function () {
            $(document).unbind('.line');

            var plotLine = getPlotLine();
            var plotLineOptions = plotLine.options;
            //Remove + Re-insert plot line
            //Otherwise it gets messed up when chart is resized
            if (plotLine.svgElem.hasOwnProperty('translateX')) {
                plotLineOptions.value = getValue()
                axis.removePlotLine(plotLineOptions.id);
                axis.addPlotLine(plotLineOptions);

                if (plotLineOptions.onDragFinish) {
                    plotLineOptions.onDragFinish(plotLineOptions.value);
                }
            }

            getPlotLine().svgElem
                .css({'cursor': 'pointer'})
                .translate(0, 0)
                .on('mousedown', drag_start);
        };
        drag_stop();
    }

    function recalcChart(new_Y) {

        var chart = $('#chart-break-even').highcharts();
        var revenue = chart.series[3];
        var costs   = chart.series[4];
        var polygon = chart.series[6];

        var costs_k = (costs.points[1].x - costs.points[0].x) / (costs.points[1].y - costs.points[0].y);
        var revenue_k = (revenue.points[1].x - revenue.points[0].x) / (revenue.points[1].y - revenue.points[0].y);

        var revenue_X = revenue_k * (new_Y);
        var costs_X = costs_k * (new_Y - fixed_costs_Y);
        var costs_Y = revenue_X / costs_k + fixed_costs_Y;
        var revenue_Y = costs_X / revenue_k;

        if (new_Y > break_even_Y) {
            polygon.setData([[break_even_X, break_even_Y], [revenue_X - 0.0001, new_Y], [revenue_X, costs_Y] ]);
            polygon.options.color = 'rgba(127,221,127,.3)';
            polygon.update(polygon.options);
        } else {
            polygon.setData([[break_even_X, break_even_Y], [revenue_X - 0.0001, new_Y], [revenue_X, costs_Y] ]);
            polygon.options.color = 'rgba(253,45,1,.3)';
            polygon.update(polygon.options);
        }

        $('#revenue_tip_1').text(nFormatter(Math.ceil(new_Y) || 0, 1));
        $('#revenue_tip_2').text(nFormatter(Math.ceil(costs_Y) || 0, 1));
        $('#revenue_tip_3').text(nFormatter(Math.ceil(new_Y - costs_Y) || 0, 1));

        //console.log(costs_Y);
    }

    $(document).ready(function() {
        $('#revenue_tip_1').text(nFormatter(Math.ceil(break_even_Y) || 0, 1));
        $('#revenue_tip_2').text(nFormatter(Math.ceil(break_even_Y) || 0, 1));
        $('#revenue_tip_3').text(nFormatter(Math.ceil(0) || 0, 1));
    });
</script>