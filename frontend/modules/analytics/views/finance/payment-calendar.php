<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 21.02.2019
 * Time: 23:27
 */

use frontend\modules\analytics\models\PaymentCalendarSearch;
use common\components\helpers\Html;
use yii\helpers\Url;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use frontend\modules\analytics\models\PlanCashFlows;

/* @var $this yii\web\View
 * @var $activeTab integer
 * @var $searchModel PaymentCalendarSearch
 * @var $data array
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $itemsDataProvider \yii\data\ActiveDataProvider
 * @var $hasFlows bool
 */

$this->title = 'Платежный календарь (ПК)';
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized', 'tooltip_pay-odds'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>
    <div class="portlet box odds-report-header">
        <div class="btn-group pull-right title-buttons">
            <div class="portlet-body employee-wrapper">
                <div class="tabbable tabbable-tabdrop">
                    <ul class="nav nav-tabs li-border float-r segmentation-tabs">
                        <li class="<?= $activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY ? 'active' : null; ?>">
                            <?= Html::a('ПК по видам деятельности',
                                Url::to(['payment-calendar', 'activeTab' => PaymentCalendarSearch::TAB_BY_ACTIVITY, 'PaymentCalendarSearch' => [
                                    'year' => $searchModel->year,
                                ]]), [
                                    'aria-expanded' => 'false',
                                    'style' => 'margin-right: 0;',
                                ]); ?>
                        </li>
                        <li class="<?= $activeTab == PaymentCalendarSearch::TAB_BY_PURSE ? 'active' : null; ?>"
                            style="margin-right: 0;float: right;">
                            <?= Html::a('ПК по кошелькам',
                                Url::to(['payment-calendar', 'activeTab' => PaymentCalendarSearch::TAB_BY_PURSE, 'PaymentCalendarSearch' => [
                                    'year' => $searchModel->year,
                                ]]), [
                                    'aria-expanded' => 'false',
                                    'style' => 'margin-right: 0;',
                                ]); ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <h3 class="page-title">
            <?= $this->title; ?>
            <img src="/img/open-book.svg" class="payment_calendar-panel-trigger">
        </h3>
    </div>
    <div class="tab-content">
        <div class="tab-pane fade in active">
            <?php if ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY): ?>
                <?= $this->render('_partial/payment_calendar_by_activity', [
                    'searchModel' => $searchModel,
                    'activeTab' => $activeTab,
                    'data' => $data,
                    'currentMonthNumber' => $currentMonthNumber,
                    'currentQuarter' => $currentQuarter,
                    'hasFlows' => $hasFlows,
                ]); ?>
            <?php elseif ($activeTab == PaymentCalendarSearch::TAB_BY_PURSE): ?>
                <?= $this->render('_partial/payment_calendar_by_purse', [
                    'searchModel' => $searchModel,
                    'activeTab' => $activeTab,
                    'data' => $data,
                    'currentMonthNumber' => $currentMonthNumber,
                    'currentQuarter' => $currentQuarter,
                    'hasFlows' => $hasFlows,
                ]); ?>
            <?php endif; ?>
            <?= Html::hiddenInput('activeTab', $activeTab, [
                'id' => 'active-tab_report',
            ]); ?>
        </div>
    </div>
<?= $this->render('_partial/plan_item_table', [
    'searchModel' => $searchModel,
    'itemsDataProvider' => $itemsDataProvider,
    'activeTab' => $activeTab,
]); ?>
<?= $this->render('_partial/payment-calendar-panel'); ?>
<?= $this->render('_partial/plan_item_panel'); ?>

<?php // COMPANY_53146
if (YII_ENV_DEV || Yii::$app->user->identity->company->id == 53146): ?>
    <?= $this->render('_partial/plan_charts_panel'); ?>
<?php endif; ?>

<div id="hellopreloader" style="display: none;">
    <div id="hellopreloader_preload"></div>
</div>
<div id="visible-right-menu" style="display: none;">
    <div id="visible-right-menu-wrapper" style="z-index: 10050!important;"></div>
</div>
<?php if (Yii::$app->session->remove('showAutoPlan')): ?>
    <?php $this->registerJs('
        $(document).ready(function (e) {
            $(".auto-plan-items").click();
        });
    '); ?>
<?php endif; ?>
<?php $this->registerJs('
    $(".payment_calendar-panel-trigger").click(function (e) {
        $(".payment_calendar-panel").toggle("fast");
        $("#visible-right-menu").show();
        $("html").attr("style", "overflow: hidden;");
        return false;
    });

    $(document).on("click", ".panel-block .side-panel-close, .panel-block .side-panel-close-button", function (e) {
        var $panel = $(this).closest(".panel-block");

        $panel.toggle("fast");
        if ($panel.find(".plan_item-form").length > 0) {
            $panel.find(".plan_item-form").empty();
        }
        $("#visible-right-menu").hide();
        $("html").removeAttr("style");
    });

    $("#visible-right-menu").click(function (e) {
        var $panel = $(".panel-block:visible");

        if ($panel.find(".plan_item-form").length > 0) {
            $panel.find(".plan_item-form").empty();
        }
        $panel.toggle("fast");
        $(this).hide();
        $("html").removeAttr("style");
    });

    $(".add-item-payment-calendar").click(function (e) {
        var $panel = $(".plan_item-panel");

        $panel.find(".plan_item-header").text("Добавить плановую операцию");
        $.post($(this).data("url"), null, function (data) {
            $panel.find(".plan_item-form").append(data);
            $panel.toggle("fast");
            $("#visible-right-menu").show();
            $("html").attr("style", "overflow: hidden;");
        });

        return false;
    });

    $(document).on("click", ".update-item-payment-calendar", function (e) {
        showUpdatePanel($(this).data("url"));
    });

    $(document).on("click", ".plan-item-table tbody td:not(.action-line, .joint-operation-checkbox-td)", function (e) {
        showUpdatePanel($(this).closest("tr").find(".update-item-payment-calendar").data("url"));
    });

    function showUpdatePanel($url) {
        var $panel = $(".plan_item-panel");

        $panel.find(".plan_item-header").text("Изменить плановую операцию");
        $.post($url, null, function (data) {
            $panel.find(".plan_item-form").append(data);
            $panel.toggle("fast");
            $("#visible-right-menu").show();
            $("html").attr("style", "overflow: hidden;");
        });

        return false;
    }

    $(".auto-plan-items").click(function (e) {
        var $autoPlanBlock = $(".auto-plan-block");
        var $reportTable = $(".flow-of-funds:not(.auto-plan-block), #paymentcalendarsearch-year, .add-item-payment-calendar, .download-odds-xls");

        if (!$(this).hasClass("active")) {
            $(this).addClass("active");
            $autoPlanBlock.removeClass("hidden");
            $reportTable.addClass("hidden");
            $(".items-table-block").hide();
            $(this).text("Сохранить");
            $(this).closest("div").removeClass("col-md-10").addClass("col-md-8");
            $("#paymentcalendarsearch-year").closest("div").prepend("<span class=\"auto-plan-items_portlet-title\">Настройка АвтоПланирования</span>").removeClass("col-md-2").addClass("col-md-4");
            $(".undo-auto-plan-items").removeClass("hidden");
        } else {
            $("#save-auto-plan").modal();
        }
    });

    $(".auto-plan-block .submit-auto_plan").click(function (e) {
        $(".auto-plan-block").addClass("hidden");
        $(".flow-of-funds:not(.auto-plan-table), #paymentcalendarsearch-year, .add-item-payment-calendar, .download-odds-xls").removeClass("hidden");
        $(".auto-plan-items").removeClass("active").text("АвтоПланирование");
        $(".items-table-block").show();
        $(".auto-plan-items").closest("div").removeClass("col-md-8").addClass("col-md-10");
        $("#paymentcalendarsearch-year").closest("div").removeClass("col-md-4").addClass("col-md-2");
        $(".auto-plan-items_portlet-title").remove();
    });

    $(".update-repeated-operations-button").click(function (e) {
        $("#plancashflows-updaterepeatedtype").val($("#update-repeated-operations input:checked").val());
        $("#save-repeated-flows").modal("hide");
        $("#js-plan_cash_flow_form").submit();
    });
'); ?>