<?php

namespace frontend\modules\analytics\views\credits;

use frontend\modules\analytics\models\credits\CreditForm;
use yii\web\View;

/**
 * @var $this View
 * @var CreditForm $form
 */

$this->title = 'Редактировать кредит';

?>

<?= $this->render('widgets/credit-form', compact('form')) ?>
