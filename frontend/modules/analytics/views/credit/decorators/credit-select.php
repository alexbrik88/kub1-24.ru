<?php

namespace frontend\modules\crm\views;

use frontend\modules\analytics\assets\credits\CreditSelectAsset;
use frontend\modules\analytics\models\credits\CreditSelectFormFactory;
use frontend\modules\crm\assets\RowSelectAsset;
use frontend\modules\crm\widgets\GridButtonWidget;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\web\View;

/**
 * @var View $this
 * @var string $content
 */

RowSelectAsset::register($this);
CreditSelectAsset::register($this);

$form = (new CreditSelectFormFactory)->createForm();

?>

<?= Html::beginForm(null, 'post', ['id' => 'rowSelectForm']); ?>

<?= $content ?>

<div id="rowSelectSummary" class="wrap wrap_btns check-condition">
    <div class="row align-items-center justify-content-end">
        <div class="column flex-shrink-0">
            <span class="checkbox-txt total-txt-foot ml-3" style="padding-left: 6px">
                Выбрано количество: <strong class="pl-2 total-count">0</strong>
            </span>
        </div>

        <div class="column flex-shrink-0">
            <span class="checkbox-txt total-txt-foot ml-3">
                Сумма текущей задолженности: <strong class="pl-2 total-diff">0</strong>
            </span>
        </div>

        <div class="column ml-auto"></div>

        <div class="column">
            <?= GridButtonWidget::widget([
                'label' => 'Удалить',
                'icon' => 'garbage',
                'options' => [
                    'class' => 'button-clr button-regular button-width button-hover-transparent',
                    'data-toggle' => 'modal',
                    'data-target' => '#deleteDialog',
                ],
            ]) ?>
        </div>
    </div>
</div>

<?php Modal::begin([
    'options' => ['id' => 'deleteDialog'],
    'headerOptions' => ['class' => 'text-center'],
    'closeButton' => false,
]); ?>


<h4 class="modal-title text-center" data-can-delete="1">
    Вы действительно хотите удалить записи?
</h4>
<h4 class="modal-title text-center hidden" data-can-delete="0">
    Кредит не может быть удален, т.к. есть операции по деньгам привязанные к нему
</h4>

<div class="form-group text-center mb-0" data-can-delete="1">
    <?= Html::submitButton('Да', [
        'name' => Html::getInputName($form, 'delete'),
        'value' => 1,
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2',
    ]) ?>

    <?= Html::button('Нет', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-2',
        'data-dismiss' => 'modal',
    ]) ?>
</div>

<div class="form-group text-center mb-0 hidden" data-can-delete="0">
    <?= Html::button('Ок', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-2',
        'data-dismiss' => 'modal',
    ]) ?>
</div>

<?php Modal::end(); ?>

<?= Html::endForm() ?>
