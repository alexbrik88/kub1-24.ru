<?php

namespace frontend\modules\crm\views;

use frontend\modules\analytics\assets\credits\FlowSelectAsset;
use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\analytics\models\credits\FlowSelectFormFactory;
use frontend\modules\crm\assets\RowSelectAsset;
use frontend\modules\crm\widgets\GridButtonWidget;
use kartik\select2\Select2;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\web\View;

/**
 * @var View $this
 * @var Credit $credit
 * @var string $content
 */

RowSelectAsset::register($this);
FlowSelectAsset::register($this);

$form = (new FlowSelectFormFactory)->createForm($credit);

?>

<?= Html::beginForm(null, 'post', ['id' => 'rowSelectForm']); ?>

<?= $content ?>

<div id="rowSelectSummary" class="wrap wrap_btns check-condition">
    <div class="row align-items-center justify-content-end">
        <div class="column flex-shrink-0">
            <span class="checkbox-txt total-txt-foot ml-3" style="padding-left: 6px">
                Выбрано операций: <strong class="pl-2 total-count">0</strong>
            </span>
        </div>

        <div class="column flex-shrink-0">
            <span class="checkbox-txt total-txt-foot ml-3">
                Приход: <strong class="pl-2 total-income">0</strong>
            </span>
        </div>

        <div class="column flex-shrink-0">
            <span class="checkbox-txt total-txt-foot ml-3">
                Расход: <strong class="pl-2 total-expense">0</strong>
            </span>
        </div>

        <div class="column flex-shrink-0">
            <span class="checkbox-txt total-txt-foot ml-3">
                Разница: <strong class="pl-2 total-diff">0</strong>
            </span>
        </div>

        <div class="column ml-auto"></div>

        <div class="column">
            <?= GridButtonWidget::widget([
                'label' => 'Статья',
                'icon' => 'article',
                'options' => [
                    'class' => 'button-clr button-regular button-width button-hover-transparent',
                    'data-toggle' => 'modal',
                    'data-target' => '#changeItemDialog',
                ],
            ]) ?>
        </div>

        <div class="column">
            <?= GridButtonWidget::widget([
                'label' => 'Удалить',
                'icon' => 'garbage',
                'options' => [
                    'class' => 'button-clr button-regular button-width button-hover-transparent',
                    'data-toggle' => 'modal',
                    'data-target' => '#deleteDialog',
                ],
            ]) ?>
        </div>
    </div>
</div>

<?php Modal::begin([
    'options' => ['id' => 'changeItemDialog'],
    'size' => Modal::SIZE_SMALL,
    'closeButton' => false,
    'title' => 'Изменить статью',
]); ?>

<div class="row">
    <div class="form-group col-12">
        <label class="label"><?= Html::encode($form->getAttributeLabel('income_item_id')) ?></label>
        <?= Select2::widget([
            'model' => $form,
            'attribute' => 'income_item_id',
            'data' => [null => null] + $form->getItems('income_item_id'),
            'pluginOptions' => ['width' => '100%'],
            'hideSearch' => true,
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="form-group col-12">
        <label class="label"><?= Html::encode($form->getAttributeLabel('expenditure_item_id')) ?></label>
        <?= Select2::widget([
            'model' => $form,
            'attribute' => 'expenditure_item_id',
            'data' => [null => null] + $form->getItems('expenditure_item_id'),
            'pluginOptions' => ['width' => '100%'],
            'hideSearch' => true,
        ]) ?>
    </div>
</div>

<div class="row mt-4">
    <div class="form-group col-6 text-left mb-0">
        <?= Html::submitButton('Применить', [
            'class' => 'button-regular button-width button-regular_red button-clr',
            'data-style' => 'expand-right',
        ]) ?>
    </div>

    <div class="form-group col-6 text-right mb-0">
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'data-dismiss' => 'modal'
        ]) ?>
    </div>
</div>

<?php Modal::end(); ?>

<?php Modal::begin([
    'options' => ['id' => 'deleteDialog'],
    'headerOptions' => ['class' => 'text-center'],
    'closeButton' => false,
    'title' => 'Вы действительно хотите удалить записи?',
]); ?>

<div class="form-group text-center mb-0">
    <?= Html::submitButton('Да', [
        'name' => Html::getInputName($form, 'delete'),
        'value' => 1,
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2',
    ]) ?>

    <?= Html::button('Нет', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-2',
        'data-dismiss' => 'modal',
    ]) ?>
</div>

<?php Modal::end(); ?>

<?= Html::endForm() ?>
