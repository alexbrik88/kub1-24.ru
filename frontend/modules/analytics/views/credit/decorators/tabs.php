<?php

namespace frontend\modules\crm\views;

use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\crm\widgets\TabsWidget;
use yii\web\View;

/**
 * @var View $this
 * @var Credit $credit
 * @var string $content
 */

?>

<?= TabsWidget::widget([
    'options' => [
        'class' => 'nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mb-4',
    ],
    'items' => [
        ['label' => 'Реестр операций', 'url' => ['credit/registry', 'credit_id' => $credit->credit_id]],
        ['label' => 'График платежей', 'url' => ['credit/schedule', 'credit_id' => $credit->credit_id]],
    ],
]) ?>

<div>
    <?= $content ?>
</div>
