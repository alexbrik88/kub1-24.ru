<?php

namespace frontend\modules\analytics\views\credits;

use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\analytics\models\credits\CreditFlowRepository;
use frontend\modules\crm\assets\RowSelectAsset;
use frontend\modules\crm\widgets\PjaxWidget;
use yii\web\View;
use yii\widgets\ContentDecorator;

/**
 * @var View $this
 * @var Credit $credit
 * @var CreditFlowRepository $repository
 */

$this->title = 'Реестр операций';
$placeholder = 'Поиск по описанию...';
$tableAttribute = 'credits_flow_table';

RowSelectAsset::register($this);

?>

<?php PjaxWidget::begin(['id' => 'pjaxContent']) ?>

<?= $this->render('widgets/credit-header', compact('credit')) ?>

<?php ContentDecorator::begin(['viewFile' => __DIR__ . '/decorators/tabs.php', 'params' => compact('credit')]); ?>

<?= $this->render('widgets/search-filter', compact('repository', 'placeholder', 'tableAttribute')) ?>

<?= $this->render('widgets/flow-grid', compact('credit', 'repository')) ?>

<?php ContentDecorator::end() ?>

<?php PjaxWidget::end() ?>
