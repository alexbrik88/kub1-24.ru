<?php

namespace frontend\modules\analytics\views\credits;

use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\crm\assets\RowSelectAsset;
use frontend\modules\crm\widgets\PjaxWidget;
use frontend\widgets\TableViewWidget;
use yii\web\View;
use yii\widgets\ContentDecorator;

/**
 * @var View $this
 * @var Credit $credit
 * @var bool $debug
 */

$this->title = 'График платежей';

RowSelectAsset::register($this);

?>

<?php PjaxWidget::begin(['id' => 'pjaxContent']) ?>

<?= $this->render('widgets/credit-header', compact('credit')) ?>

<?php ContentDecorator::begin(['viewFile' => __DIR__ . '/decorators/tabs.php', 'params' => compact('credit')]); ?>

<div class="table-settings row row_indents_s">
    <div class="col-12">
        <div class="row align-items-center">
            <div class="column flex-grow-1 d-flex flex-wrap justify-content-end">
                <?= TableViewWidget::widget(['attribute' => 'credits_debt_table']) ?>
            </div>
        </div>
    </div>
</div>

<?= $this->render('widgets/debt-grid', compact('credit', 'debug')) ?>

<?php ContentDecorator::end() ?>

<?php PjaxWidget::end() ?>
