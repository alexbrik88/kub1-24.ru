<?php

namespace frontend\modules\crm\views;

use common\components\date\DateHelper;
use common\models\cash\CashFlowsBase;
use frontend\modules\analytics\assets\credits\CreditHeaderAsset;
use frontend\modules\analytics\assets\credits\FlowFormAsset;
use frontend\modules\analytics\assets\credits\FlowSelectAsset;
use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\analytics\models\credits\CreditDocument;
use frontend\modules\analytics\models\credits\CreditLabelHelper;
use frontend\modules\crm\widgets\AjaxFormDialog;
use frontend\modules\crm\widgets\GridButtonWidget;
use frontend\themes\kub\components\Icon;
use frontend\themes\kub\modules\documents\widgets\DocumentFileScanWidget;
use frontend\themes\kub\widgets\SpriteIconWidget;
use Yii;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var Credit $credit
 */

CreditHeaderAsset::register($this);
FlowFormAsset::register($this);
FlowSelectAsset::register($this);

$document = $credit->document;
$statusClasses = [
    Credit::STATUS_ACTIVE => 'credit-status-active',
    Credit::STATUS_ARCHIVE => 'credit-status-archive',
    Credit::STATUS_EXPIRED => 'credit-status-expired',
];
$statusIcons = [
    Credit::STATUS_ACTIVE => 'new-doc',
    Credit::STATUS_EXPIRED => 'calendar',
    Credit::STATUS_ARCHIVE => 'stop',
];
?>

<?= AjaxFormDialog::widget() ?>

<?= Html::a('Назад к списку', ['finance/loans'], ['class' => 'link mb-2', 'data-pjax' => 0]) ?>

<div class="wrap wrap_padding_small pl-4 pr-3 pb-2 mb-2">
    <div class="pl-1 pb-1">
        <div class="page-in row">
            <div class="col-9 column pr-4">
                <div class="pr-2">
                    <div class="row align-items-center justify-content-between mb-3">
                        <h4 class="column mb-2" style="max-width: 550px;">
                            <?= Html::encode(sprintf(
                                '%s № %d от %s',
                                CreditLabelHelper::getTitle($credit->credit_type),
                                $credit->agreement_number,
                                DateHelper::format($credit->agreement_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)
                            )); ?>
                        </h4>
                        <div class="column" style="margin-bottom: auto;">

                            <?= GridButtonWidget::widget([
                                'icon' => 'info',
                                'options' => [
                                    'title' => 'Последние действия',
                                    'class' => 'button-regular button-regular_red button-clr w-44 mb-2 mr-2',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#lastActions',
                                ],
                            ]) ?>

                            <?= Html::a(
                                SpriteIconWidget::widget(['icon' => 'pencil']),
                                ['credit/update', 'credit_id' => $credit->credit_id],
                                [
                                    'title' => 'Редактировать',
                                    'class' => 'button-regular button-regular_red button-clr w-44 mb-2 mr-2',
                                    'data-pjax' => 0,
                                ]
                            ) ?>

                        </div>
                    </div>

                    <div class="row" style="min-width: 550px;">
                        <div class="col-3 mb-4 pb-2">
                            <div class="label weight-700 mb-3">Вид кредита</div>
                            <div><?= Html::encode($credit->typeTitle) ?></div>
                        </div>
                        <div class="col-3 mb-4 pb-2">
                            <div class="label weight-700 mb-3">
                                <?= CreditLabelHelper::getLabel('credit_amount', $credit->credit_type) ?>
                            </div>
                            <div>
                                <?= Yii::$app->formatter->format($credit->credit_amount, Credit::DECIMAL_FORMAT) ?>
                                <?= Html::encode($credit->currency->name) ?>
                            </div>
                        </div>
                        <div class="col-3 mb-4 pb-2">
                            <div class="label weight-700 mb-3">Ставка</div>
                            <div><?= $credit->credit_percent_rate ?>% годовых</div>
                        </div>

                        <div class="col-3 mb-4 pb-2">
                            <div class="label weight-700 mb-3">Дней до погашения</div>
                            <div>
                                <?= (($credit->credit_status != Credit::STATUS_ARCHIVE) ? $credit->daysLeft : '-') ?>
                                дн.
                            </div>
                        </div>
                    </div>

                    <div class="row" style="min-width: 550px;">
                        <div class="col-6 mb-4 pb-2">
                            <div class="label weight-700 mb-3">Кредитор</div>
                            <div><?= Html::encode($credit->contractorTitle) ?></div>
                        </div>
                        <div class="col-6 mb-4 pb-2">
                            <div class="label weight-700 mb-3">
                                <?= CreditLabelHelper::getLabel('wallet_value', $credit->credit_type) ?>
                            </div>
                            <div><?= Html::encode($credit->walletTitle) ?></div>
                        </div>
                    </div>

                    <div class="row" style="min-width: 550px;">
                        <div class="col-3 mb-4 pb-2">
                            <div class="label weight-700 mb-3">Дата получения</div>
                            <div><?= DateHelper::format($credit->credit_first_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?></div>
                        </div>

                        <div class="col-3 mb-4 pb-2">
                            <div class="label weight-700 mb-3">Дата погашения</div>
                            <div><?= DateHelper::format($credit->credit_last_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?></div>
                        </div>

                        <div class="col-3 mb-4 pb-2">
                            <div class="label weight-700 mb-3">
                                <?= CreditLabelHelper::getLabel('debt_diff', $credit->credit_type) ?>
                            </div>
                            <div><?= Yii::$app->formatter->format($credit->debt_diff, Credit::DECIMAL_FORMAT) ?></div>
                        </div>

                        <div class="col-3 mb-4 pb-2">
                            <div class="label weight-700 mb-3">
                                Остаток по процентам
                            </div>
                            <div><?= Yii::$app->formatter->format($credit->interest_diff, Credit::DECIMAL_FORMAT) ?></div>
                        </div>
                    </div>

                    <div class="row" style="min-width: 550px;">
                        <div class="col-3 mb-4 pb-2">
                            <div class="label weight-700 mb-3">
                                <?= CreditLabelHelper::getLabel('payment_type', $credit->credit_type) ?>
                            </div>
                            <div><?= Html::encode($credit->paymentTypeTitle) ?></div>
                        </div>
                        <div class="col-3 mb-4 pb-2">
                            <div class="label weight-700 mb-3">Ежемесячные платежи</div>
                            <div><?= Html::encode($credit->paymentModeTitle) ?></div>
                        </div>
                        <div class="col-3 mb-4 pb-2">
                            <div class="label weight-700 mb-3">Комиссия/Дисконт</div>
                            <div><?= $credit->credit_commission_rate ?>%</div>
                        </div>
                        <div class="col-3 mb-4 pb-2">
                            <div class="label weight-700 mb-3">Ставка при просрочке</div>
                            <div><?= $credit->credit_expiration_rate ?>%</div>
                        </div>
                    </div>
                    <div class="row" style="min-width: 550px;">
                        <?php if ($credit->credit_tranche_depth == Credit::TYPE_OVERDRAFT): ?>
                        <div class="col-3 mb-4 pb-2">
                            <div class="label weight-700 mb-3">Глубина транша</div>
                            <div><?= $credit->credit_tranche_depth ?> дн.</div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-3 column pl-0">
                <div class="pb-2 mb-1">
                    <?= Html::button($this->render('//svg-sprite', ['ico' => $statusIcons[$credit->credit_status]]) . Html::tag('span', $credit->statusTitle), [
                        'class' => 'button-regular pl-0 pr-0 w-100 '. $statusClasses[$credit->credit_status],
                    ]); ?>
                </div>

                <?php /*
                <div class="pb-2 mb-1">
                    <div class="dropdown">
                        <?= Html::button('
                            <span>Добавит операцию</span>
                            <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                            </svg>
                        ', [
                            'class' => 'button-width button-clr button-regular button-hover-transparent w-100',
                            'data-toggle' => 'dropdown',
                            'aria-expanded' => 'false'
                        ]); ?>

                        <?= Dropdown::widget([
                            'options' => [
                                'class' => 'dropdown-menu form-filter-list list-clr w-100'
                            ],
                            'items' => [
                                [
                                    'label' => 'Операцию по банку',
                                    'url' => '#',
                                    'linkOptions' => [
                                        'class' => 'ajax-form-button',
                                        'data-url' => Url::to([
                                            'credit/create-flow',
                                            'credit_id' => $credit->credit_id,
                                            'type' => CashFlowsBase::WALLET_BANK,
                                        ]),
                                        'data-title' => 'Добавить операцию по кредиту'
                                    ],
                                ],
                                [
                                    'label' => 'Операцию по кассе',
                                    'url' => '#',
                                    'linkOptions' => [
                                        'class' => 'ajax-form-button',
                                        'data-url' => Url::to([
                                            'credit/create-flow',
                                            'credit_id' => $credit->credit_id,
                                            'type' => CashFlowsBase::WALLET_CASHBOX,
                                        ]),
                                        'data-title' => 'Добавить операцию по кредиту'
                                    ],
                                ],
                                [
                                    'label' => 'Операцию по e-money',
                                    'url' => '#',
                                    'linkOptions' => [
                                        'class' => 'ajax-form-button',
                                        'data-url' => Url::to([
                                            'credit/create-flow',
                                            'credit_id' => $credit->credit_id,
                                            'type' => CashFlowsBase::WALLET_EMONEY,
                                        ]),
                                        'data-title' => 'Добавить операцию по e-money'
                                    ],
                                ],
                            ],
                        ]); ?>
                    </div>
                </div> */ ?>

                <div class="mb-1 mt-3">
                    <?= $this->render('@frontend/modules/cash/views/default/_partial/create-buttons', [
                        'contractorId' => $credit->contractor_id,
                        'creditId' => $credit->credit_id,
                        'canCreate' => true,
                        'buttons' => [
                            'create' => true,
                            'import' => false
                        ]
                    ]) ?>
                </div>

                <div class="pb-2 mb-1 text-right">
                    <?= DocumentFileScanWidget::widget([
                        'maxFileCount' => CreditDocument::MAX_FILE_COUNT,
                        'model' => $document,
                        'hasFreeScan' => $document->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
                        'uploadUrl' => Url::to(['file-upload', 'type' => $document->type, 'id' => $document->id]),
                        'deleteUrl' => Url::to(['file-delete', 'type' => $document->type, 'id' => $document->id]),
                        'listUrl' => Url::to(['file-list', 'type' => $document->type, 'id' => $document->id]),
                        'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
                        'scanListUrl' => Url::to(['scan-list', 'type' => $document->type, 'id' => $document->id]),
                        'scanBindUrl' => Url::to(['scan-bind', 'type' => $document->type, 'id' => $document->id]),
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    'id' => 'lastActions',
    'title' => 'Последние действия',
    'options' => [
        'class' => 'doc-history-modal fade',
    ],
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
    'footer' => Html::button('OK', [
        'class' => 'button-regular button-regular_red button-clr w-44',
        'data-dismiss' => 'modal',
    ]),
]) ?>

<div class="created-by">
    <?= DateHelper::format($credit->created_at, DateHelper::FORMAT_USER_DATE) ?>
    Создал
    <?= $credit->employee ? $credit->employee->fio : ''; ?>
</div>

<?php Modal::end(); ?>

<?php $this->registerJs(<<<JS
    $(document).ready(function() {
        initMain();
    });
JS); ?>
