<?php

namespace frontend\modules\analytics\views\credits;

use common\components\grid\GridView;
use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\analytics\models\credits\DebtCalculatorFactory;
use yii\data\ArrayDataProvider;
use yii\i18n\Formatter;
use yii\web\View;

/**
 * @var View $this
 * @var Credit $credit
 * @var bool $debug
 */

$calculator = (new DebtCalculatorFactory)->createCalculator($credit);
if ($calculator->schedule->credit->payment_type == Credit::PAYMENT_TYPE_END_WITH_PERCENTS) {
    $lastModel = end($calculator->schedule->periods);
    $lastModel->number = 1;
    $allModels = [$lastModel];
} else {
    $allModels = $calculator->schedule->periods;
}

$provider = new ArrayDataProvider([
    'allModels' => $allModels,
    'pagination' => false,
]);

$totals = [
    'total' => array_sum(ArrayHelper::getColumn($calculator->schedule->periods, 'total')),
    'repaid' => array_sum(ArrayHelper::getColumn($calculator->schedule->periods, 'repaid')),
    'interest' => array_sum(ArrayHelper::getColumn($calculator->schedule->periods, 'interest')),
];

$layout = <<<HTML
<div class="wrap wrap_padding_none">
    <div class="custom-scroll-table">
        <div class="table-wrap">
            {items}
        </div>
    </div>
</div>
HTML;

?>

<div class="table-container products-table clearfix">
<?= GridView::widget([
    'id' => 'product-grid',
    'dataProvider' => $provider,
    'configAttribute' => 'credits_debt_table',
    'formatter' => ['class' => Formatter::class, 'nullDisplay' => ''],
    'tableOptions' => [
        'class' => 'table table-style table-count-list',
        'id' => 'datatable_ajax',
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],
    'headerRowOptions' => ['class' => 'heading'],
    'footerRowOptions' => ['class' => 'bold'],
    'options' => ['class' => 'dataTables_wrapper dataTables_extended_wrapper'],
    'rowOptions' => ['role' => 'row'],
    'pager' => ['options' => ['class' => 'pagination pull-right']],
    'layout' => $layout,
    'showFooter' => true,
    'columns' => [
        [
            'attribute' => 'number',
            'label' => '№№ платежа',
            'footer' => 'Итого',
        ],
        [
            'attribute' => 'firstDate',
            'format' => ['date', 'php:d.m.Y'],
            'visible' => $debug,
        ],
        [
            'attribute' => 'lastDate',
            'label' => 'Дата платежа',
            'format' => ['date', 'php:d.m.Y'],
        ],
        [
            'attribute' => 'days',
            'label' => 'Дней в периоде',
            'contentOptions' => ['style' => 'width: 1px'],
            'visible' => $debug,
        ],
        [
            'attribute' => 'total',
            'label' => 'Сумма платежа итого',
            'format' => Credit::DECIMAL_FORMAT,
            'footer' => TextHelper::moneyFormat($totals['total'], 2)
        ],
        [
            'attribute' => 'repaid',
            'label' => 'Платеж по основному долгу',
            'format' => Credit::DECIMAL_FORMAT,
            'footer' => TextHelper::moneyFormat($totals['repaid'], 2)
        ],
        [
            'attribute' => 'interest',
            'label' => 'Платеж по процентам',
            'format' => Credit::DECIMAL_FORMAT,
            'footer' => TextHelper::moneyFormat($totals['interest'], 2)
        ],
        [
            'attribute' => 'amount',
            'label' => 'Остаток долга',
            'format' => Credit::DECIMAL_FORMAT,
            'headerOptions' => ['width' => '30%'],
        ],
        [
            'attribute' => 'startBalance',
            'label' => 'Начальный баланс',
            'visible' => $debug,
        ],
        [
            'attribute' => 'payments',
            'label' => 'Сумма платежей',
            'visible' => $debug,
        ],
        [
            'attribute' => 'endBalance',
            'label' => 'Конечный баланс',
            'visible' => $debug,
        ],
        [
            'attribute' => 'paymentsInterest',
            'label' => 'Платежи по %',
            'visible' => $debug,
        ],
        [
            'attribute' => 'paymentsRepaid',
            'label' => 'Погашено платежами',
            'visible' => $debug,
        ],
    ],
]) ?>
</div>
