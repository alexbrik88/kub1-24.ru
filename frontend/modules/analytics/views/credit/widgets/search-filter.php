<?php

namespace frontend\modules\analytics\views\credits;

use frontend\modules\analytics\models\credits\RepositoryInterface;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\base\Model;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

RepositoryInterface::class;

/**
 * @var View $this
 * @var Model $repository
 * @var string $placeholder
 * @var string $tableAttribute
 * @var string[][] $configs
 */

?>

<?php $widget = ActiveForm::begin([
    'id' => 'searchFilterForm',
    'method' => 'GET',
    'action' => Url::current([$repository->formName() => null]),
    'options' => [
        'data-pjax' => true,
    ],
    'fieldConfig' => [
        'labelOptions' => ['class' => 'label'],
        'options' => ['class' => 'form-group col-6 mb-3'],
    ],
]) ?>

<div class="table-settings row row_indents_s">
    <div class="col-6">
        <div class="row align-items-center">
            <div class="column flex-grow-1 d-flex flex-wrap justify-content-end">
            <?php if (isset($configs)): ?>
                <?= TableConfigWidget::widget([
                    'items' => $configs,
                    'buttonClass' => 'button-regular button-regular_red button-clr w-44 mr-2'
                ]) ?>
            <?php endif; ?>
                <?= TableViewWidget::widget(['attribute' => $tableAttribute]) ?>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="d-flex flex-nowrap align-items-center">
            <?= $widget->field($repository, 'search', [
                'options' => ['class' => 'form-group flex-grow-1 mr-2'],
            ])->textInput([
                'placeholder' => $placeholder,
                'type' => 'search',
            ])->label(false) ?>

            <div class="form-group">
                <?= Html::submitButton('Найти', [
                    'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                ]) ?>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end() ?>
