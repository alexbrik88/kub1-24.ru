<?php

namespace frontend\modules\analytics\views\credits;

use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\TextHelper;
use frontend\components\StatisticPeriod;
use frontend\modules\analytics\assets\credits\CreditGridAsset;
use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\analytics\models\credits\CreditorRepository;
use frontend\modules\analytics\models\credits\CreditRepository;
use frontend\modules\analytics\models\credits\CurrencyRepository;
use frontend\modules\analytics\models\credits\ListFactory;
use frontend\modules\crm\widgets\AjaxFormDialog;
use frontend\modules\crm\widgets\DataColumnWidget;
use yii\bootstrap4\Html;
use yii\grid\CheckboxColumn;
use yii\i18n\Formatter;
use yii\web\View;
use yii\widgets\ContentDecorator;

/**
 * @var View $this
 * @var CreditRepository $repository
 */

CreditGridAsset::register($this);

$statusColors = [
    Credit::STATUS_ACTIVE => 'credit-status-active',
    Credit::STATUS_ARCHIVE => 'credit-status-archive',
    Credit::STATUS_EXPIRED => 'credit-status-expired',
];
$canEdit = true;
$provider = $repository->getProvider();
$emptyText = sprintf(
    'В выбранном периоде («%s») нет добавленных кредитов. Измените период, чтобы увидеть добавленные кредиты',
    StatisticPeriod::getSessionName()
);

if ($repository->getAllCount() == 0) {
    $emptyText = 'Вы ещё не добавили ни одного кредита. ';

    if ($canEdit) {
        $emptyText .= Html::a('Добавить кредит', ['credit/create']);
    }
}

?>

<?= AjaxFormDialog::widget() ?>

<?php ContentDecorator::begin(['viewFile' => __DIR__ . '/../decorators/credit-select.php']); ?>

<div class="table-container products-table clearfix">
<?= GridView::widget([
    'id' => 'creditGrid',
    'dataProvider' => $provider,
    'filterModel' => $repository,
    'emptyText' => $emptyText,
    'configAttribute' => 'credits_credit_table',
    'formatter' => [
        'class' => Formatter::class,
        'nullDisplay' => '',
    ],
    'tableOptions' => [
        'class' => 'table table-style table-count-list',
        'id' => 'datatable_ajax',
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],
    'headerRowOptions' => ['class' => 'heading'],
    'options' => ['class' => 'dataTables_wrapper dataTables_extended_wrapper'],
    'rowOptions' => ['role' => 'row'],
    'pager' => ['options' => ['class' => 'pagination']],
    'layout' => $this->render('//layouts/grid/layout', [
        'totalCount' => $provider->getTotalCount(),
        'scroll' => true,
    ]),
    'columns' => [
        [
            'class' => CheckboxColumn::class,
            'name' => 'CreditSelectForm[credit_id]',
            'checkboxOptions' => function (Credit $credit) {
                return [
                    'value' => $credit->credit_id,
                    'data-diff' => (float) $credit->debt_diff,
                    'data-can-delete' => (int) !$credit->getCreditFlows()->exists(),
                ];
            },
        ],
        [
            'attribute' => 'agreement_number',
            'label' => 'Номер договора',
            'format' => 'raw',
            'value' => function (Credit $credit): string {
                return Html::a(
                    $credit->agreement_number,
                    ['credit/registry', 'credit_id' => $credit->credit_id],
                    ['data-pjax' => 0]
                );
            }
        ],
        [
            'attribute' => 'credit_amount',
            'label' => 'Сумма по договору',
            'headerOptions' => ['style' => 'min-width: 100px'],
            'format' => Credit::DECIMAL_FORMAT,
            'class' => DataColumnWidget::class,
            'configAttribute' => 'credits_credit_amount',
        ],
        [
            'attribute' => 'currency_id',
            'value' => 'currency.name',
            'label' => 'Валюта',
            'headerOptions' => ['class' => 'dropdown-filter'],
            'filter' => [null => 'Все'] + (new ListFactory)
                    ->createList(CurrencyRepository::class)
                    ->getItems(),
            's2width' => '200px',
            'enableSorting' => false,
            'class' => DataColumnWidget::class,
            'configAttribute' => 'credits_currency_id',
        ],
        [
            'attribute' => 'credit_first_date',
            'label' => 'Дата получения',
            'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
            'class' => DataColumnWidget::class,
            'configAttribute' => 'credits_credit_first_date',
        ],
        [
            'attribute' => 'credit_last_date',
            'label' => 'Дата погашения',
            'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
        ],
        [
            'attribute' => 'daysLeft',
            'label' => 'Осталось, дн.',
            'value' => function(Credit $credit): string {
                return ($credit->credit_status != Credit::STATUS_ARCHIVE) ? $credit->daysLeft : '-';
            }
        ],
        [
            'attribute' => 'credit_percent_rate',
            'label' => '%% годовых',
            'headerOptions' => ['style' => 'min-width: 110px'],
            'format' => Credit::PERCENT_FORMAT,
        ],
        [
            'attribute' => 'contractor_id',
            'value' => 'contractorTitle',
            'label' => 'Кредитор',
            'contentOptions' => ['class' => 'contractor-cell'],
            'headerOptions' => ['class' => 'dropdown-filter'],
            'filter' => [null => 'Все'] + (new ListFactory)
                ->createList(CreditorRepository::class)
                ->getItems(),
            's2width' => '200px',
            'enableSorting' => false,
            'class' => DataColumnWidget::class,
            'configAttribute' => 'credits_contractor_id',
        ],
        [
            'attribute' => 'credit_type',
            'value' => 'typeTitle',
            'label' => 'Вид кредита',
            'headerOptions' => ['class' => 'dropdown-filter'],
            'filter' => [null => 'Все'] + Credit::TYPE_LIST,
            's2width' => '200px',
            'enableSorting' => false,
            'class' => DataColumnWidget::class,
            'configAttribute' => 'credits_credit_type',
        ],
        [
            'attribute' => 'debt_amount',
            'label' => 'Получено',
            'format' => Credit::DECIMAL_FORMAT,
            'class' => DataColumnWidget::class,
            'configAttribute' => 'credits_debt_amount',
        ],
        [
            'attribute' => 'debt_repaid',
            'label' => 'Погашено',
            'format' => Credit::DECIMAL_FORMAT,
            'class' => DataColumnWidget::class,
            'configAttribute' => 'credits_debt_repaid',
        ],
        [
            'attribute' => 'debt_diff',
            'label' => 'Остаток долга',
            'format' => Credit::DECIMAL_FORMAT,
        ],
        [
            'attribute' => 'payment_date',
            'label' => 'Дата уплаты %%',
            'headerOptions' => ['style' => 'min-width: 100px'],
            'contentOptions' => function (Credit $credit) use ($statusColors): array {
                return ($credit->credit_status == Credit::STATUS_EXPIRED) ?
                    ['class' => $statusColors[$credit->credit_status]]
                    : [];
            },
            'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
        ],
        [
            'attribute' => 'interest_repaid',
            'label' => 'Уплаченные %%',
            'format' => Credit::DECIMAL_FORMAT,
            'class' => DataColumnWidget::class,
            'configAttribute' => 'credits_interest_repaid',
        ],
        [
            'attribute' => 'interest_diff',
            'label' => 'Осталось уплатить %%',
            'format' => 'raw',
            'class' => DataColumnWidget::class,
            'value' => function (Credit $credit) {
                return ($credit->interest_diff > 0)
                    ? TextHelper::moneyFormat($credit->interest_diff, 2)
                    : '0,00';
            }
        ],
        [
            'attribute' => 'interest_amount',
            'label' => 'Сумма %% всего',
            'headerOptions' => ['style' => 'min-width: 100px'],
            'format' => Credit::DECIMAL_FORMAT,
            'class' => DataColumnWidget::class,
            'configAttribute' => 'credits_interest_amount',
        ],
        [
            'attribute' => 'credit_status',
            'value' => 'statusTitle',
            'label' => 'Статус',
            'headerOptions' => ['class' => 'dropdown-filter'],
            'filter' => [null => 'Все'] + Credit::STATUS_LIST,
            's2width' => '200px',
            'enableSorting' => false,
        ],
    ],
]) ?>
</div>

<?php ContentDecorator::end(); ?>
