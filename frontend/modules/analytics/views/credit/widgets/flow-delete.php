<?php

namespace frontend\modules\analytics\views\credits;

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

?>

<?php ActiveForm::begin() ?>

<div class="form-group text-center mb-0">
    <?= Html::submitButton('Да', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2',
    ]) ?>

    <?= Html::button('Нет', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-2',
        'data-dismiss' => 'modal',
    ]) ?>
</div>

<?php ActiveForm::end() ?>

<?php $this->assetBundles = []; ?>
