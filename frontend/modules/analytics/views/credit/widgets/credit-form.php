<?php

namespace frontend\modules\analytics\views\credits;

use common\models\cash\CashFlowsBase;
use frontend\modules\analytics\assets\credits\CreditFormAsset;
use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\analytics\models\credits\CreditForm;
use frontend\modules\analytics\models\credits\CreditLabelHelper;
use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\web\JsExpression;
use yii\web\View;

/**
 * @var $this View
 * @var CreditForm $form
 */

CreditFormAsset::register($this);

$datePickerTemplate = <<<HTML
{label}
<div class="date-picker-wrap">
    {input}
    <svg class="date-picker-icon svg-icon input-toggle"><use xlink:href="/img/svg/svgSprite.svg#calendar"></use></svg>
</div>
HTML;

$headerOptions = [
    'enableError' => false,
    'options' => ['class' => 'form-group mb-0'],
];

$credit_id = $form->credit->credit_id;

$questionIconOptions = [
    'wallet_value' => ['class' => 'tooltip-help', 'data-tooltip-content' => '#tooltip_wallet_value', 'style' => 'position:absolute; top:0; left:137px'],
    'credit_tranche_depth' => ['class' => 'tooltip-help', 'data-tooltip-content' => '#tooltip_credit_tranche_depth', 'style' => 'position:absolute; top:0; left:151px', 'id' => 'creditTrancheGroupHelp'],
    'credit_commission_rate' => ['class' => 'tooltip-help', 'data-tooltip-content' => '#tooltip_credit_commission_rate', 'style' => 'position:absolute; top:0; left:126px'],
    'credit_expiration_rate' => ['class' => 'tooltip-help', 'data-tooltip-content' => '#tooltip_credit_expiration_rate', 'style' => 'position:absolute; top:0; left:146px'],
    'payment_type' => ['class' => 'tooltip-help', 'data-tooltip-content' => '#tooltip_payment_type', 'style' => 'position:absolute; top:0; left:183px'],
    'payment_mode' => ['class' => 'tooltip-help', 'data-tooltip-content' => '#tooltip_payment_mode', 'style' => 'position:absolute; top:0; left:150px'],
    'payment_first' => ['class' => 'tooltip-help', 'data-tooltip-content' => '#tooltip_payment_first', 'style' => 'position:absolute; top:0; left:195px'],
    'credit_year_length' => ['class' => 'tooltip-help', 'data-tooltip-content' => '#tooltip_credit_year_length', 'style' => 'position:absolute; top:0; left:199px'],
];

?>

<?php $widget = ActiveForm::begin([
    'options' => ['id' => 'creditForm'],
    'fieldConfig' => [
        'labelOptions' => ['class' => 'label'],
        'options' => ['class' => 'form-group'],
    ],
]) ?>

<div class="wrap">
    <div class="row row_indents_s flex-nowrap align-items-center">
        <div class="column form-title d-inline-block">
            <div class="form-group mb-0">Договор №</div>
        </div>
        <div class="col-2">
            <?= $widget
                ->field($form, 'agreement_number', $headerOptions)
                ->textInput(['placeholder' => $form->getAttributeLabel('agreement_number')])
                ->label(false) ?>
        </div>
        <div class="col-2">
            <?= $widget
                ->field($form, 'agreement_name', $headerOptions)
                ->textInput(['placeholder' => $form->getAttributeLabel('agreement_name')])
                ->label(false) ?>
        </div>
        <div class="column">
            <div class="form-group mb-0">от</div>
        </div>
        <div class="column" style="position: relative">
            <?= $widget
                ->field($form, 'agreement_date', ['template' => $datePickerTemplate] + $headerOptions)
                ->textInput([
                    'class' => 'form-control form-control_small date-picker',
                    'data-date-viewmode' => 'years',
                ])
                ->label(false) ?>
        </div>
    </div>
</div>

<div class="wrap">

    <div class="row">
        <div class="col-6">
            <?= $widget->field($form, 'credit_type')->widget(Select2::class, [
                'data' => Credit::TYPE_LIST,
                'hideSearch' => true,
                'pluginOptions' => ['width' => '100%'],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <?= $widget->field($form, 'contractor_id')->widget(Select2::class, [
                'data' => ['add-modal-contractor' => Icon::PLUS . ' Добавить кредитора '] + $form->getItems('contractor_id'),
                'hideSearch' => false,
                'pluginOptions' => [
                    'width' => '100%',
                    'escapeMarkup' => new JsExpression('function (value) { return value }'),
                ],
                'options' => ['placeholder' => ''],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-3">
            <?= $widget->field($form, 'credit_amount')
                ->textInput()
                ->label(CreditLabelHelper::getLabel('credit_amount', $form->credit_type), [
                    'data-label-'.Credit::TYPE_CREDIT => CreditLabelHelper::getLabel('credit_amount', Credit::TYPE_CREDIT),
                    'data-label-'.Credit::TYPE_LOAN => CreditLabelHelper::getLabel('credit_amount', Credit::TYPE_LOAN),
                    'data-label-'.Credit::TYPE_OVERDRAFT => CreditLabelHelper::getLabel('credit_amount', Credit::TYPE_OVERDRAFT),
                ])
            ?>
        </div>

        <div class="col-3">
            <?= $widget->field($form, 'currency_id')->widget(Select2::class, [
                'data' => $form->getItems('currency_id'),
                'hideSearch' => true,
                'disabled' => true,// TODO: 'disabled' => (!$form->credit->isNewRecord),
                'pluginOptions' => ['width' => '100%'],
            ]) ?>
        </div>

        <div class="col-6">
            <?= $widget->field($form, 'wallet_value')->widget(Select2::class, [
                'data' => [
                    $form->getAttributeLabel('checking_accountant_id') => $form->getWalletItems(CashFlowsBase::WALLET_BANK),
                    $form->getAttributeLabel('cashbox_id') => $form->getWalletItems(CashFlowsBase::WALLET_CASHBOX),
                    $form->getAttributeLabel('emoney_id') => $form->getWalletItems(CashFlowsBase::WALLET_EMONEY),
                ],
                'hideSearch' => true,
                'pluginOptions' => ['width' => '100%'],
            ]) ?>
            <?= Html::tag('span', Icon::QUESTION, $questionIconOptions['wallet_value']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-3">
            <?= $widget
                ->field($form, 'credit_first_date', ['template' => $datePickerTemplate])
                ->textInput([
                    'class' => 'form-control date-picker',
                    'data-date-viewmode' => 'years',
                ]) ?>
        </div>

        <div class="col-3">
            <?= $widget
                ->field($form, 'credit_last_date', ['template' => $datePickerTemplate])
                ->textInput([
                    'class' => 'form-control date-picker',
                    'data-date-viewmode' => 'years',
                ]) ?>
        </div>

        <div class="col-3">
            <?= $widget->field($form, 'credit_tranche_depth', ['options' => ['id' => 'creditTrancheGroup']])->textInput() ?>
            <?= Html::tag('span', Icon::QUESTION, $questionIconOptions['credit_tranche_depth']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-3">
            <?= $widget->field($form, 'credit_percent_rate')->textInput() ?>
        </div>

        <div class="col-3">
            <?= $widget->field($form, 'credit_commission_rate')->textInput() ?>
            <?= Html::tag('span', Icon::QUESTION, $questionIconOptions['credit_commission_rate']) ?>
        </div>

        <div class="col-3">
            <?= $widget->field($form, 'credit_expiration_rate')->textInput() ?>
            <?= Html::tag('span', Icon::QUESTION, $questionIconOptions['credit_expiration_rate']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <?= $widget->field($form, 'payment_type')->widget(Select2::class, [
                'data' => Credit::PAYMENT_TYPE_LIST,
                'hideSearch' => true,
                'pluginOptions' => ['width' => '100%'],
            ])->label(CreditLabelHelper::getLabel('payment_type', $form->credit_type), [
                'data-label-'.Credit::TYPE_CREDIT => CreditLabelHelper::getLabel('payment_type', Credit::TYPE_CREDIT),
                'data-label-'.Credit::TYPE_LOAN => CreditLabelHelper::getLabel('payment_type', Credit::TYPE_LOAN),
                'data-label-'.Credit::TYPE_OVERDRAFT => CreditLabelHelper::getLabel('payment_type', Credit::TYPE_OVERDRAFT),
                'data-label-left-'.Credit::TYPE_CREDIT => '160px',
                'data-label-left-'.Credit::TYPE_LOAN => '148px',
                'data-label-left-'.Credit::TYPE_OVERDRAFT => '183px',
            ])  ?>
            <?= Html::tag('span', Icon::QUESTION, $questionIconOptions['payment_type']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <?= $widget->field($form, 'payment_mode')->widget(Select2::class, [
                'data' => Credit::PAYMENT_MODE_LIST,
                'hideSearch' => true,
                'pluginOptions' => ['width' => '100%'],
            ]) ?>
            <?= Html::tag('span', Icon::QUESTION, $questionIconOptions['payment_mode']) ?>
        </div>

        <div class="col-3">
            <?= $widget->field($form, 'payment_day', ['options' => ['id' => 'paymentDayGroup']])->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <?= $widget->field($form, 'payment_first')->widget(Select2::class, [
                'data' => Credit::PAYMENT_FIRST_LIST,
                'hideSearch' => true,
                'pluginOptions' => ['width' => '100%'],
            ]) ?>
            <?= Html::tag('span', Icon::QUESTION, $questionIconOptions['payment_first']) ?>
        </div>

        <div class="col-3">
            <?= $widget->field($form, 'credit_year_length')->widget(Select2::class, [
                'data' => Credit::YEAR_LENGTH_LIST,
                'hideSearch' => true,
                'pluginOptions' => ['width' => '100%'],
            ]) ?>
            <?= Html::tag('span', Icon::QUESTION, $questionIconOptions['credit_year_length']) ?>
        </div>
    </div>
</div>

<div class="wrap wrap_btns check-condition visible mb-0">
    <div class="row align-items-center justify-content-between">
        <div class="column">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span>', [
                'class' => 'button-width button-clr button-regular button-regular_red ladda-button',
                'data-style' => 'expand-right',
            ]); ?>
        </div>
        <div class="column ml-auto">
            <?= Html::a('Отменить', $credit_id ? ['credit/registry', 'credit_id' => $credit_id] : ['finance/loans'], [
                'class' => 'button-width button-clr button-regular button-hover-grey',
            ]); ?>
        </div>
    </div>
</div>

<div style="display: none">
    <div id="tooltip_wallet_value">
        <strong>Счет для зачисления</strong> – расчетный счет, касса или электронная платежная система<br/>
        (Яндекс.Деньги, QIWI Кошелек и др.) куда будут зачислены денежные средства по кредиту или займу.<br/>
        По умолчанию установлен основной расчетный счет.
    </div>
    <div id="tooltip_credit_commission_rate">
        <strong>Комиссия/Дисконт</strong> – сумма или процентная ставка установленная кредитным договором помимо уплаты процентов.<br/>
    </div>
    <div id="tooltip_credit_tranche_depth">
        <strong>Глубина транша</strong> – этот термин актуален только для кредитования в форме овердрафта или кредитной линии,<br/>
        и подразумевает количество календарных дней (обычно 30, 45, 60, 90) на которое предоставляется каждая сумма (транш)<br/>
        в рамках установленного кредитного лимита.
    </div>
    <div id="tooltip_credit_expiration_rate">
        <strong>Ставка при просрочке</strong> – размер повышенной процентной ставки, пеня (иная штрафная санкция, установленная в %),<br/>
        которые применяется к заемщику в случае нарушения им сроков уплаты ежемесячных платежей.
    </div>
    <div id="tooltip_payment_type">
        <strong>Вид платежа по кредиту</strong> – это способ погашения основного долга по кредиту в зависимости от вида кредита.<br/>
        Предусмотрено три варианта:<br/>
        <ul class="pl-3">
            <li>В конце срока кредит (устанавливается по умолчанию) – кредит в полном объеме погашается в конце срока.<br/>Проценты начисляются на полную сумму кредита и уплачиваются ежемесячно;</li>
            <li>В конце срока кредит и %% по кредиту - кредит и проценты по кредиту в полном объеме погашаются в конце срока</li>
            <li>Дифференцированный – кредит погашается равными долями в течение срока действия кредитного договора.<br/>Проценты начисляются на фактический остаток задолженности, поэтому с каждым месяцем<br/>сумма платежа «кредит + проценты» снижается;</li>
            <li>Аннуитетный – сумма ежемесячного платежа «кредит + проценты» является постоянной на весь срок кредитования.<br/>Аннуитетный платеж включает в себя и погашение долга по кредиту, и уплату процентов;</li>
        </ul>
    </div>
    <div id="tooltip_payment_mode">
        <strong>Ежемесячные платежи</strong> – дата (день) осуществления платежа по процентам или платежа «кредит + проценты».<br/>
        По умолчанию устанавливается «последний день месяца»;
    </div>
    <div id="tooltip_payment_first">
        <strong>Первый ежемесячный платеж</strong> – период осуществления первого платежа (проценты или «кредит + проценты»).<br/>
        Существует практика предоставления кредитором льгот или каникул по первому платежу.<br/>
        <ul class="pl-3">
            <li>«Стандартно» - устанавливается по умолчанию при отсутствии льгот или каникул;</li>
            <li>«Первый платеж только %» - распространяется на кредиты с дифференцированным или аннуитетным видом платежа в случае <br/>предоставления кредитором каникул по погашению суммы основного долга по кредиту (тела кредита) в первый месяц;</li>
            <li>«Платеж со второго месяца» - выбирается в случае предоставления кредитором каникул по уплате платежей в полном объеме<br/> (кредита и процентов) в течение первого месяца.</li>
        </ul>
    </div>
    <div id="tooltip_credit_year_length">
        <strong>Число дней в году</strong> – расчетное количество дней в году для начисления процентов по кредиту.<br/>
        Обычно соответствует календарному количеству, поэтому по умолчанию установлено «по календарю».<br/>
        Однако кредитным договором может быть установлено отличное от календарного количество дней: 360 или 365.
    </div>
</div>

<?php ActiveForm::end() ?>

<?= $this->render('@frontend/modules/cash/views/order/_partial/modal-add-contractor') ?>

<?php

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-help',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);