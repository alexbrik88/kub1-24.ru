<?php

namespace frontend\modules\analytics\views\credits;

use common\components\grid\GridView;
use frontend\components\StatisticPeriod;
use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\analytics\models\credits\CreditFlow;
use frontend\modules\analytics\models\credits\CreditFlowRepository;
use frontend\modules\crm\widgets\GridButtonWidget;
use yii\grid\ActionColumn;
use yii\grid\CheckboxColumn;
use yii\helpers\Url;
use yii\i18n\Formatter;
use yii\web\View;
use yii\widgets\ContentDecorator;

/**
 * @var View $this
 * @var Credit $credit
 * @var CreditFlowRepository $repository
 */

$provider = $repository->getProvider($pagination = true);
$emptyText = ($repository->getAllCount() == 0) ? 'Вы ещё не добавили ни одной операции.' : sprintf(
    'В выбранном периоде («%s») нет добавленных операций. Измените период, чтобы увидеть добавленные операции',
    StatisticPeriod::getSessionName()
);

?>

<?php ContentDecorator::begin([
    'viewFile' => __DIR__ . '/../decorators/flow-select.php', 'params' => compact('credit'),
]); ?>

<div class="table-container products-table clearfix">
    <?= GridView::widget([
        'id' => 'product-grid',
        'dataProvider' => $provider,
        'filterModel' => $repository,
        'emptyText' => $emptyText,
        'configAttribute' => 'credits_flow_table',
        'formatter' => ['class' => Formatter::class, 'nullDisplay' => ''],
        'tableOptions' => [
            'class' => 'table table-style table-count-list',
            'id' => 'datatable_ajax',
            'aria-describedby' => 'datatable_ajax_info',
            'role' => 'grid',
        ],
        'headerRowOptions' => ['class' => 'heading'],
        'options' => ['class' => 'dataTables_wrapper dataTables_extended_wrapper'],
        'rowOptions' => ['role' => 'row'],
        'pager' => ['options' => ['class' => 'pagination']],
        'layout' => $this->render('//layouts/grid/layout', [
            'totalCount' => $provider->getTotalCount(),
            'scroll' => true,
        ]),
        'columns' => [
            [
                'class' => CheckboxColumn::class,
                'name' => 'FlowSelectForm[credit_flow_id]',
                'checkboxOptions' => function (CreditFlow $creditFlow) {
                    return [
                        'value' => $creditFlow->credit_flow_id,
                        'data-income' => $creditFlow->flow->incomeAmount,
                        'data-expense' => $creditFlow->flow->expenseAmount,
                    ];
                },
            ],
            [
                'attribute' => 'flow.date',
                'format' => ['date', 'php:d.m.Y'],
                'label' => 'Дата',
            ],
            [
                'attribute' => 'flow.incomeAmount',
                'label' => 'Получено',
                'format' => function (?float $value, Formatter $formatter): string {
                    return $value ? $formatter->format($value, Credit::DECIMAL_FORMAT) : '-';
                }
            ],
            [
                'attribute' => 'flow.expenseAmount',
                'label' => 'Погашено',
                'format' => function (?float $value, Formatter $formatter): string {
                    return $value ? $formatter->format($value, Credit::DECIMAL_FORMAT) : '-';
                }
            ],
            [
                'attribute' => 'wallet_id',
                'label' => 'Тип операции',
                'headerOptions' => ['class' => 'dropdown-filter'],
                'filter' => [null => 'Все'] + CreditFlowRepository::WALLET_LIST,
                's2width' => '200px',
                'enableSorting' => false,
                'format' => function (int $wallet_id): string {
                    return CreditFlowRepository::WALLET_LIST[$wallet_id];
                },
            ],
            [
                'attribute' => 'description',
                'label' => 'Назначение',
                'headerOptions' => ['style' => 'width: 100%'],
            ],
            [
                'attribute' => 'item_id',
                'value' => 'flow.item_id',
                'label' => 'Статья',
                'headerOptions' => ['class' => 'dropdown-filter'],
                'filter' => [null => 'Все'] + $repository->getInvoiceItems(),
                's2width' => '200px',
                'enableSorting' => false,
                'format' => function (?int $item_id) use ($repository): ?string {
                    return $repository->getInvoiceItems()[$item_id] ?? null;
                }
            ],
            [
                'class' => ActionColumn::class,
                'template' => '{update}{delete}',
                'contentOptions' => ['class' => 'text-nowrap'],
                'buttons' => [
                    'update' => function (string $url, CreditFlow $creditFlow): string {
                        return GridButtonWidget::widget([
                            'icon' => 'pencil',
                            'options' => [
                                'title' => 'Изменить',
                                'class' => 'button-clr link ajax-form-button mr-2',
                                'data-url' => Url::to([
                                    'credit/update-flow',
                                    'credit_id' => $creditFlow->credit_id,
                                    'type' => $creditFlow->wallet_id,
                                    'credit_flow_id' => $creditFlow->credit_flow_id,
                                ]),
                                'data-title' => 'Изменить движение по кредиту',
                            ],
                        ]);
                    },
                    'delete' => function (string $url, CreditFlow $creditFlow): string {
                        return GridButtonWidget::widget([
                            'icon' => 'garbage',
                            'options' => [
                                'title' => 'Удалить',
                                'class' => 'button-clr link ajax-form-button',
                                'data-url' => Url::to([
                                    'credit/delete-flow',
                                    'credit_id' => $creditFlow->credit_id,
                                    'type' => $creditFlow->wallet_id,
                                    'credit_flow_id' => $creditFlow->credit_flow_id,
                                ]),
                                'data-title' => '<span class="text-center d-block">Вы действительно хотите удалить запись?</span>',
                            ],
                        ]);
                    },
                ],
            ],
        ],
    ]) ?>
</div>

<?php ContentDecorator::end(); ?>
