<?php

namespace frontend\modules\analytics\views\credits;

use common\models\cash\CashFlowsBase;
use frontend\modules\analytics\models\credits\AbstractFlowForm;
use frontend\modules\analytics\models\credits\BankFlowForm;
use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\analytics\models\credits\EmoneyFlowForm;
use frontend\modules\analytics\models\credits\OrderFlowForm;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\web\View;

/**
 * @var View $this
 * @var AbstractFlowForm $form
 * @var Credit[] $credits
 */

$datePickerTemplate = <<<HTML
<div class="date-picker-wrap">
{input}
<svg class="date-picker-icon svg-icon input-toggle"><use xlink:href="/img/svg/svgSprite.svg#calendar"></use></svg>
</div>
HTML;

?>

<?php $widget = ActiveForm::begin([
    'options' => [
        'id' => 'flowForm',
    ],
    'fieldConfig' => [
        'options' => ['class' => 'form-group col-6'],
        'labelOptions' => ['class' => 'label'],
        'checkOptions' => [
            'class' => '',
            'labelOptions' => ['class' => 'label'],
        ],
    ],
]); ?>

<div class="row">
    <?= $widget->field($form, 'flow_type', [
        'inline' => true,
        'labelOptions' => ['class' => 'label mb-3'],
        'radioOptions' => [
            'class' => '',
            'labelOptions' => ['class' => 'radio-label radio-txt-bold'],
        ],
    ])->radioList($form->getFlowTypes()) ?>

<?php if ($form instanceof OrderFlowForm): ?>
    <?= $widget->field($form, 'cashbox_id')->widget(Select2::class, [
        'data' => $form->getItems('cashbox_id'),
        'options' => ['placeholder' => ''],
        'hideSearch' => true,
        'pluginOptions' => ['width' => '100%'],
    ]) ?>
<?php endif; ?>

<?php if ($form instanceof EmoneyFlowForm): ?>
    <?= $widget->field($form, 'emoney_id')->widget(Select2::class, [
        'data' => $form->getItems('emoney_id'),
        'options' => ['placeholder' => ''],
        'hideSearch' => true,
        'pluginOptions' => ['width' => '100%'],
    ]) ?>
<?php endif; ?>

<?php if ($form instanceof BankFlowForm): ?>
    <?= $widget->field($form, 'checking_accountant_id')->widget(Select2::class, [
        'data' => $form->getItems('checking_accountant_id'),
        'options' => ['placeholder' => ''],
        'hideSearch' => true,
        'pluginOptions' => ['width' => '100%'],
    ]) ?>
<?php endif; ?>

    <?= $widget->field($form, 'contractor_id')->widget(Select2::class, [
        'data' => $form->getItems('creditor_id'),
        'options' => [
            'class' => 'js-creditor-select',
        ],
        'hideSearch' => false,
        'pluginOptions' => ['width' => '100%'],
    ]) ?>

    <?= $widget->field($form, 'income_item_id', ['options' => ['id' => 'incomeItemId']])->widget(Select2::class, [
        'data' => $form->getItems('income_item_id'),
        'options' => ['placeholder' => ''],
        'hideSearch' => true,
        'pluginOptions' => ['width' => '100%'],
    ]) ?>

    <?= $widget->field($form, 'expenditure_item_id', ['options' => ['id' => 'expenditureItemId']])->widget(Select2::class, [
        'data' => $form->getItems('expenditure_item_id'),
        'options' => ['placeholder' => ''],
        'hideSearch' => true,
        'pluginOptions' => ['width' => '100%'],
    ]) ?>

    <?= $widget->field($form, 'amount')->textInput() ?>

    <?= $widget->field($form, 'payment_order_number', ['options' => ['class' => 'form-group col-3']])->textInput() ?>

    <div class="form-group col-3"></div>

    <?= $widget->field($form, 'date', [
        'options' => ['class' => 'form-group col-3'],
        'inputTemplate' => $datePickerTemplate,
        'enableClientValidation' => false,
    ])->textInput([
        'class' => 'form-control date-picker',
        'data-date-viewmode' => 'years',
    ]) ?>

    <div class="form-group col-3"></div>

    <?= $widget->field($form, 'recognitionDate', [
        'options' => ['class' => 'form-group col-3'],
        'inputTemplate' => $datePickerTemplate,
    ])->textInput([
        'class' => 'form-control date-picker',
        'data-date-viewmode' => 'years',
        'readonly' => true,
        'disabled' => true,
    ])->label(null, ['id' => 'recognitionDateLabel']) ?>

    <?= $widget->field($form, 'is_prepaid_expense', [
        'options' => ['class' => 'form-group col-3', 'style' => 'padding-top: 36px'],
    ])->checkbox(['readonly' => true, 'disabled' => true]) ?>

    <?= $widget->field($form, 'description', ['options' => ['class' => 'form-group col-12']])->textarea() ?>

    <?= $widget->field($form, 'credit_id')->widget(Select2::class, [
        'data' => array_map(fn (Credit $credit) => $credit->getAgreementTitle(), $credits),
        'hideSearch' => true,
        'options' => [
            'class' => 'js-credit-select',
            'options' => array_map(fn (Credit $credit) => [
                'data-creditor-id' => $credit->contractor_id,
                'data-amount-with-currency' => $credit->getAmountWithCurrency(),
            ], $credits),
        ],
        'pluginOptions' => [
            'width' => '100%',
        ],
    ]) ?>

    <?= $widget->field($form, 'credit_amount')->textInput([
        'disabled' => true,
        'class' => 'form-control js-credit-amount-input',
    ]) ?>

    <?= $widget->field($form, 'project_id')->widget(Select2::class, [
        'data' => $form->getItems('project_id'),
        'options' => ['placeholder' => ''],
        'hideSearch' => true,
        'pluginOptions' => ['width' => '100%'],
    ]) ?>
</div>

<div class="row mt-4">
    <div class="form-group col-6 text-left mb-0">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-width button-regular_red button-clr',
            'data-style' => 'expand-right',
        ]) ?>
    </div>

    <div class="form-group col-6 text-right mb-0">
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'data-dismiss' => 'modal'
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php

$type = Html::encode(is_scalar($form->flow_type) ? $form->flow_type : CashFlowsBase::FLOW_TYPE_INCOME);
$this->registerJs(<<<JS
    $('#flowForm [name*="flow_type"][value="{$type}"]').trigger('change');
JS);

$this->registerJs(<<<JS
    $('.js-credit-select').on('change', function () {
        $('.js-credit-amount-input').val($(this).find('option:selected').data('amount-with-currency'));
    });

    $('.js-creditor-select').on('change', function () {
        const creditor_id = $(this).val();
        const select = $('.js-credit-select');

        select.find('option').each(function () {
            $(this).prop('disabled', $(this).data('creditor-id') != creditor_id);
        });

        select.val(select.find('option:not([disabled])').first().attr('value'));
        select.trigger('change');
    }).trigger('change');
JS);

$this->registerCss(<<<CSS
    .select2-container.select2-container--open .select2-results__option[aria-disabled] {
        display: none;
    }
CSS);

$this->assetBundles = [];

?>
