<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Дашборд';
$currentMonthNumber = date('n');
$currentQuarter = (int)ceil(date('m') / 3);

function getPatternHover($color) {
    return ['hover' => [
        'color' => [
            'pattern' => [
                'path' => 'M 0 0 L 5 5 M 4 -1 L 6 1 M -1 4 L 1 6',
                'color' => $color,
                'width' => 5,
                'height' => 5
            ]
        ]
    ]
    ];
}

?>

<style>
    .analytics-dashboard > .row > .col-md-3,
    .analytics-dashboard > .row > .col-md-6 {
        margin-bottom:10px;
    }
    .pad-5 {
        padding-left:5px;
        padding-right:5px;
    }
    #chart-plan-fact-income-outcome {
        height: 150px;
    }
    #chart-plan-fact-income-outcome-2 {
        height: 150px;
    }
    #chart-actives-and-profit,
    #chart-actives-structure,
    #chart-passives-structure {
        height: 300px;
    }

    #chart-revenue-structure,
    #chart-expenses-structure {
        height: 345px;
    }

    #chart-revenue-and-gross-profit,
    #chart-expenses {
        height: 290px;
        border: 1px dashed #999;
    }
    .chart-in-table {
        border: 1px dashed #999;
        margin-bottom:10px;
    }
    .chart-in-table > table {
        width:100%;
        margin: 5px 0 10px 0;
    }
    .chart-in-table > table td {
        padding:0 10px;
        font-weight:bold;
        font-size:14px;
    }

    #chart-plan-fact-income-outcome {
        border-top: 1px dashed #999;
        border-left: 1px dashed #999;
        border-right: 1px dashed #999;
    }
    #chart-plan-fact-income-outcome-2 {
        border-bottom: 1px dashed #999;
        border-left: 1px dashed #999;
        border-right: 1px dashed #999;
    }
    #chart-actives-and-profit,
    #chart-revenue-structure,
    #chart-expenses-structure,
    #chart-actives-structure,
    #chart-passives-structure {
        border: 1px dashed #999;
    }
    #table-debt {
        height: 300px;
        border: 1px dashed #999;
    }
    #table-debt table {
        width:100%;

    }
    #table-debt table td {
        padding:8px;
        vertical-align: middle;
        height: 60px;
    }
    #table-debt table td.caption {
        font-size: 13px;
        font-weight:bold;
        max-width: 100px;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    #table-debt table td.value {
        font-size: 13px;
        font-weight: bold;
        vertical-align: middle;
        text-align: right;
    }
    #table-debt table tr:nth-child(1),
    #table-debt table tr:nth-child(2),
    #table-debt table tr:nth-child(3),
    #table-debt table tr:nth-child(4){
        border-bottom: 1px dashed #999;
    }

</style>

<div class="analytics-dashboard">

    <div class="portlet box">
        <div class="btn-group pull-right title-buttons">
        </div>
        <h3 class="page-title"><?= $this->title; ?></h3>
    </div>

    <div class="row">
        <div class="col-md-6 pad-5">
            <?= $this->render('partial/plan_fact_income_outcome', []) ?>
        </div>
        <div class="col-md-3 pad-5">
            <?= $this->render('partial/actives_and_profit', []) ?>
        </div>
        <div class="col-md-3 pad-5">
            <?= $this->render('partial/table_debt', []) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 pad-5">
            <?= $this->render('partial/revenue_structure', []) ?>
        </div>
        <div class="col-md-3 pad-5">
            <?= $this->render('partial/revenue_and_gross_profit', []) ?>
        </div>
        <div class="col-md-3 pad-5">
            <?= $this->render('partial/expenses', []) ?>
        </div>
        <div class="col-md-3 pad-5">
            <?= $this->render('partial/expenses_structure', []) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 pad-5">
            <?= $this->render('partial/actives_structure', []) ?>
        </div>
        <div class="col-md-6 pad-5">
            <?= $this->render('partial/passives_structure', []) ?>
        </div>
    </div>

    <svg xmlns='http://www.w3.org/2000/svg' width='0' height='0'>
        <defs>
            <pattern id="patternActivesAndProfit" width="10" height="10" patternUnits="userSpaceOnUse">
                <rect width='10' height='10' fill='rgba(93,173,226,1)'/>
                <path d='M 0 0 L 10 10 M 9 -1 L 11 1 M -1 9 L 1 11' stroke='white' stroke-width='1.25'/>
            </pattern>
        </defs>
    </svg>

</div>