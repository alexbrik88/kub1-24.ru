<?php
$currMonth = date('m') - 1;
$currYear = date('y');
$monthArr = ['янв', 'фев', 'март', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'];
$month = [];
for ($i=$currMonth+1; $i<12; $i++) {
    $month[] = $monthArr[$i].'.'.($currYear-1);
}
for ($i=0; $i<=$currMonth; $i++) {
    $month[] = $monthArr[$i].'.'.($currYear);
}

$capital   = array_map(function($n){return 10E6*$n;}, [6,6,6,5,5,5,4,4,4,3,3,3]); // капитал
$creditors = array_map(function($n){return 10E6*$n;}, [2,2,2,3,3,3,4,4,4,5,5,5]); // кредиторы
$loans     = array_map(function($n){return 10E6*$n;}, [2,2,2,3,3,3,4,4,4,5,5,5]);; // займы
?>

<?= \miloschuman\highcharts\Highcharts::widget([
    'id' => 'chart-passives-structure',
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'exporting' => [
            'enabled' => false,
        ],
        'chart' => [
            'type' => 'column',
        ],
        'plotOptions' => [
            'column' => [
                'dataLabels' => [
                    //'enabled' => true,
                ],
                'stacking' => true
            ]
        ],
        'legend' => [
            'itemStyle' => [
                'fontSize' => '11px'
            ]
        ],
        'tooltip' => [
            'shared' => true,
            'headerFormat' => '<b>{point.x}</b><br>',
            'pointFormat' => '<b>{series.name}:</b> {point.y}<br>',
            'footerFormat' => '<b>Итого: {point.total:,.0f}</b>'
        ],
        'title' => [
            'text' => 'Структура пассивов',
            'style' => [
                'font-size' => '12px'
            ]
        ],
        'yAxis' => [
            ['min' => 0, 'index' => 0, 'title' => ''],
        ],
        'xAxis' => [
            ['categories' => $month],

        ],
        'series' => [
            ['name' => 'Капитал', 'data' => $capital, 'color' => 'rgba(183,225,115,1)', 'states' => getPatternHover('rgba(183,225,115,1)')],
            ['name' => 'Кредиторы', 'data' => $creditors, 'color' => 'rgba(160,64,0,1)', 'states' => getPatternHover('rgba(160,64,0,1)')],
            ['name' => 'Кредиты / Займы', 'data' => $loans, 'color' => 'rgba(41,138,188,1)', 'states' => getPatternHover('rgba(41,138,188,1)')]
        ]
    ],
]); ?>
