<?php
use yii\web\JsExpression;

$months = ['янв', 'фев', 'март', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'];

// 1 graph (Приход/Расход план/факт)
$factIncome  = [26000, 22000, 22000, 24000];
$factOutcome = [15000, 20000, 18000, 15000];
$planIncome  = array_reverse($factIncome);
$planOutcome = array_reverse($factOutcome);
$planPlanIncome = [25000, 25000, 25000, 25000, 25000, 25000, 22000, 22000];
$planPlanOutcome = [20000, 18000, 15000, 20000, 18000, 15000, 20000, 18000];

$columns1 = array_merge($factIncome, $planPlanIncome);
$columns2 = array_merge($factOutcome, $planPlanOutcome);

// 2 graph (Остатки)
$balanceFactPrev = [2000, 5000, 6000, 7000, 3000, 4000, 5000, 2000, 4000, 5000, 6000, 7000]; // прошлый год
$balanceFact = [0, 2000, 3000, 4000]; // факт
$balancePlan = array_merge($balanceFact, [ -2000, 3000, 5000, 4000, 2000, 3000, 4000, 5000]); // план
$balancePlanPlan = [-6000, 3000, 4000, 4000]; // план
?>

<div class="row">
    <div class="col-md-12">
        <div style="min-height:100px">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-plan-fact-income-outcome',
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'exporting' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'column',
                        'events' => [
                            'load' => new JsExpression('redrawPlanMonths()')
                        ],
                    ],
                    'legend' => [
                        'layout' => 'vertical',
                        'align' => 'right',
                        'verticalAlign' => 'top',
                        'x' => -10,
                        'y' => 0,
                        'floating' => true,
                        'borderWidth' => 0,
                        'backgroundColor' => '#FFFFFF',
                        'shadow' => true,
                        'itemStyle' => [
                            'fontSize' => '11px'
                        ]
                    ],
                    'tooltip' => [
                        'formatter' => new jsExpression("
                        function(args) {
                            var index = this.series.data.indexOf( this.point );
                            var series_index = this.series.index;
                            console.log(index, window.chartCurrMonth);

                            if (index > (window.chartCurrMonth - 1)) {
                            
                                return '<b>' + this.x + '</b>' +
                                    '<br/>' + args.chart.series[2].name + ': ' + args.chart.series[0].data[index].y + ' Р' +
                                    '<br/>' + args.chart.series[3].name + ': ' + args.chart.series[1].data[index].y + ' Р';
                            }
                            
                            return '<b>' + this.x + '</b>' +
                                ((series_index == 0 || series_index == 2) ?                                
                                    ('<br/>' + args.chart.series[0].name + ': ' + args.chart.series[0].data[index].y + ' Р' +
                                    '<br/>' + args.chart.series[2].name + ': ' + args.chart.series[2].data[index].y + ' Р') :                                
                                    ('<br/>' + args.chart.series[1].name + ': ' + args.chart.series[1].data[index].y + ' Р' +
                                    '<br/>' + args.chart.series[3].name + ': ' + args.chart.series[3].data[index].y + ' Р'));
                        }
                    ")
                    ],
                    'title' => [
                        'text' => 'План-факт',
                        'style' => [
                            'font-size' => '12px'
                        ]
                    ],
                    'yAxis' => [
                        ['min' => 0, 'index' => 0, 'title' => ''],
                    ],
                    'xAxis' => [
                        ['categories' => $months],
                    ],
                    'series' => [
                        [
                            'name' => 'Приход Факт',
                            'data' => $columns1,
                            'color' => 'rgba(93,173,226,1)',
                            'borderColor' => 'rgba(93,173,226,.3)'
                        ],
                        [
                            'name' => 'Расход Факт',
                            'data' => $columns2,
                            'color' => 'rgba(255,213,139,1)',
                            'borderColor' => 'rgba(255,213,139,.3)'
                        ],
                        [
                            'name' => 'Приход План',
                            'data' => $planIncome,
                            'marker' => [
                                'symbol' => 'c-rect',
                                'lineWidth' => 3,
                                'lineColor' =>  'rgba(21,67,96,1)',
                                'radius' => 6
                            ],
                            'type' => 'scatter',
                            'pointPlacement' => -0.15,
                            'stickyTracking' => false,
                        ],
                        [
                            'name' => 'Расход План',
                            'data' => $planOutcome,
                            'marker' => [
                                'symbol' => 'c-rect',
                                'lineWidth' => 3,
                                'lineColor' =>  'rgba(50,50,50,1)',
                                'radius' => 6
                            ],
                            'type' => 'scatter',
                            'pointPlacement' => 0.15,
                            'stickyTracking' => false,
                        ]
                    ],
                    'plotOptions' => [
                        'scatter' => [
                            //'pointWidth' => 20,
                            'tooltip' => [
                                'crosshairs' => true,
                                'headerFormat' => '{point.x}',
                                'pointFormat' => '<br /><b>{series.name}: {point.y} Р</b>',
                            ]
                        ],
                        'series' => [
                            //'pointWidth' => 20,
                            'tooltip' => [
                                'crosshairs' => true,
                                'headerFormat' => '{point.x}',
                                'pointFormat' => '<br /><b>{series.name}: {point.y} Р</b>',
                            ]
                        ]
                    ],
                ],
            ]); ?>
        </div>
        <div style="min-height:100px;">
            <?= \miloschuman\highcharts\Highcharts::widget([
                'id' => 'chart-plan-fact-income-outcome-2',
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                    'modules/pattern-fill'
                ],
                'options' => [
                    'exporting' => [
                        'enabled' => false
                    ],
                    'chart' => [
                        'type' => 'areaspline',
                        'events' => [
                            'load' =>  null
                        ],
                    ],
                    'legend' => [
                        'itemStyle' => [
                            'fontSize' => '11px'
                        ]
                    ],
                    'tooltip' => [
                        'formatter' => new jsExpression("
                                function(args) {
                                    var index = this.series.data.indexOf( this.point );
                                    var series_index = this.series.index;

                                    if (series_index == 2 || series_index == 0) {
                                        return '<b>' + this.x + '</b>' +
                                            '<br/>' + args.chart.series[series_index].name + ': ' + args.chart.series[series_index].data[index].y + ' Р' +
                                            '<br/>' + args.chart.series[1].name + ': ' + args.chart.series[1].data[index].y + ' Р';
                                    }
                                    else {
                                        return '<b>' + this.x + '</b>' +
                                            '<br/>' + args.chart.series[series_index].name + ': ' + args.chart.series[series_index].data[index].y + ' Р';
                                    }
                                }
                            ")
                    ],
                    'title' => ['text' => ''],
                    'yAxis' => [
                        ['index' => 0, 'title' => ''],
                    ],
                    'xAxis' => [
                        ['categories' => $months],
                    ],
                    'series' => [
                        [
                            'name' => 'Остаток Факт',
                            'data' => $balanceFact,
                            'color' => 'rgba(26,184,93,1)',
                            'fillColor' => 'rgba(46,204,113,1)',
                            'negativeColor' => 'red',
                            'negativeFillColor' => 'rgba(231,76,60,1)',
                            'zIndex' => 2
                        ],
                        [
                            'name' => 'Остаток Факт (предыдущий год)',
                            'data' => $balanceFactPrev,
                            'color' => 'rgba(129,145,146,1)',
                            'fillColor' => 'rgba(149,165,166,1)',
                            'zIndex' => 0
                        ],
                        [
                            'name' => 'Остаток План',
                            'data' => $balancePlan,
                            'color' => 'rgba(26,184,93,1)',
                            'negativeColor' => 'red',
                            'fillColor' => [
                                'pattern' => [
                                    //'image' => '/img/pattern1.png',
                                    'path' => 'M 0 0 L 10 10 M 9 -1 L 11 1 M -1 9 L 1 11',
                                    'color' => '#27ae60',
                                    'width' => 10,
                                    'height' => 10
                                ]
                            ],
                            'negativeFillColor' => [
                                'pattern' => [
                                    'path' => 'M 0 0 L 10 10 M 9 -1 L 11 1 M -1 9 L 1 11',
                                    'color' => '#e74c3c',
                                    'width' => 10,
                                    'height' => 10
                                ]
                            ],
                            'marker' => [
                                'symbol' => 'square'
                            ],
                            'zIndex' => 1
                        ],
                    ],
                    'plotOptions' => [
                        'areaspline' => [
                            'fillOpacity' => .9,
                            'marker' => [
                                'enabled' => false,
                                'symbol' => 'circle',
                            ],
                            'dataLabels' => [
                                'enabled' => true
                            ],
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<script>

    var chartCurrMonth = "<?= count($factIncome) ?>";

    function redrawPlanMonths() {

        var custom_pattern = function(color) {
            return {
                pattern: {
                    path: 'M 0 0 L 10 10 M 9 - 1 L 11 1 M - 1 9 L 1 11',
                    width: 10,
                    height: 10,
                    color: color
                }
            }
        }

        var chartToLoad = window.setInterval(function() {
            var chart = $('#chart-plan-fact-income-outcome').highcharts();
            if(typeof(chart) !== 'undefined') {
                for (var i = (chartCurrMonth); i < 12; i++) {
                    chart.series[0].points[i].color = custom_pattern("rgba(93,173,226,1)");
                    chart.series[1].points[i].color = custom_pattern("rgba(255,213,139,1)");
                }
                chart.series[0].redraw();
                chart.series[1].redraw();

                window.clearInterval(chartToLoad);
            }

        }, 100);
    }

    $(document).ready(function() {

        Highcharts.SVGRenderer.prototype.symbols['c-rect'] = function (x, y, w, h) {
            return ['M', x, y + h / 2, 'L', x + w, y + h / 2];
        };

        Highcharts.seriesTypes.areaspline.prototype.drawLegendSymbol = function(legend) {
            this.options.marker.enabled = true;
            Highcharts.LegendSymbolMixin.drawLineMarker.apply(this, arguments);
            this.options.marker.enabled = false;
        }
    });
</script>