<?php

use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;

$this->beginContent('@frontend/views/layouts/main.php');

$year = isset(Yii::$app->controller->year) ? Yii::$app->controller->year : null;
?>
<div class="debt-report-content container-fluid" style="padding: 0; margin-top: -10px;">
    <?php NavBar::begin([
        'options' => [
            'class' => 'navbar-report navbar-default',
        ],
        'brandOptions' => [
            'style' => 'margin-left: 0;'
        ],
        'containerOptions' => [
            'style' => 'padding: 0;'
        ],
        'innerContainerOptions' => [
            'class' => 'container-fluid',
            'style' => 'padding: 0;'
        ],
    ]);
    echo Nav::widget([
        'id' => 'debt-report-menu',
        'items' => [
            [
                'label' => 'ОДДС',
                'url' => ['/analytics/finance/odds', 'FlowOfFundsReportSearch' => ['year' => $year,]],
                'active' => Yii::$app->controller->action->id == 'odds',
            ],
            [
                'label' => 'План-факт',
                'url' => ['/analytics/finance/plan-fact', 'PlanFactSearch' => ['year' => $year,]],
                'active' => Yii::$app->controller->action->id == 'plan-fact',
            ],
            [
                'label' => 'План',
                'url' => ['/analytics/finance/payment-calendar', 'PaymentCalendarSearch' => ['year' => $year,]],
                'active' => Yii::$app->controller->action->id == 'payment-calendar',
            ],
            [
                'label' => 'ОПиУ (P&L)',
                'url' => ['/analytics/finance/profit-and-loss', 'ProfitAndLossSearchModel' => ['year' => $year,]],
                'visible' => (YII_ENV_DEV || in_array(Yii::$app->user->identity->company->id, [486])),
                'active' => Yii::$app->controller->action->id == 'profit-and-loss',
            ],
            [
                'label' => 'Баланс',
                'url' => ['/analytics/finance/balance', 'year' => $year,],
                'visible' => (YII_ENV_DEV || in_array(Yii::$app->user->identity->company->id, [349, 486, 628, 11270])),
                'active' => Yii::$app->controller->action->id == 'balance',
            ],
            [
                'label' => 'Расходы',
                'url' => ['/analytics/finance/expenses', 'ExpensesSearch' => ['year' => $year,]],
                'active' => Yii::$app->controller->action->id == 'expenses',
            ],
            [
                'label' => 'Затраты по оплате',
                'url' => ['/analytics/expenses-report/payments'],
            ],
            // COMPANY_53146
            [
                'label' => 'Точка Б',
                'url' => ['/analytics/finance/break-even'],
                'visible' => (YII_ENV_DEV || in_array(Yii::$app->user->identity->company->id, [53146])),
            ],
        ],
        'options' => ['class' => 'navbar-nav'],
    ]);
    NavBar::end();
    ?>
    <?= $content; ?>
</div>
<?php $this->endContent(); ?>
<script>
    $(window).on('load', function () {
        $('#debt-report-menu').tabdrop();
    });
</script>
