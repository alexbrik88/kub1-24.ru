<?php
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use common\widgets\Modal;
use common\models\Company;
use common\components\ImageHelper;
use yii\helpers\Url;

$this->beginContent('@frontend/views/layouts/main.php');
$company = Yii::$app->user->identity->company;
?>

<div class="invoice-report-content container-fluid" style="padding: 0; margin-top: -10px;">

    <?php
    NavBar::begin([
        'brandLabel' => 'Отчет',
        'brandUrl' => null,
        'options' => [
            'class' => 'navbar-report navbar-default',
        ],
        'brandOptions' => [
            'style' => 'margin-left: 0;'
        ],
        'innerContainerOptions' => [
            'class' => 'container-fluid',
            'style' => 'padding: 0;'
        ],
    ]);
    echo Nav::widget([
        'items' => [
            ['label' => 'Затраты по оплате', 'url' => ['/analytics/expenses-report/payments']],
            ['label' => 'Затраты по документам', 'url' => ['/analytics/expenses-report/documents']],
        ],
        'options' => ['class' => 'navbar-nav navbar-right'],
    ]);
    NavBar::end();
    ?>

    <?php echo $content ?>
    <?php if ($company->show_popup_business_analyse): ?>
        <?php Modal::begin([
            'header' => '<h2 class="header-name" style="text-transform: uppercase;">
            Анализ бизнеса
            </h2>',
            'footer' => $this->render('//layouts/modal/_partial_footer', [
                'type' => Company::AFTER_REGISTRATION_BUSINESS_ANALYSE,
            ]),
            'id' => 'modal-loader-items'
        ]); ?>
        <div class="col-xs-12" style="padding: 0" id="modal-loader">
            <?= $this->render('//layouts/modal/_template_submodal', [
                'type' => 3,
                'description' => 'ЗАБУДЬТЕ об ОТЧЕТАХ в ЭКСЕЛЬ!<br>
                    Все отчеты строятся автоматически и без ошибок.<br>
                    Графики по месяцам. Аналитика. Источники прибыли.',
                'video' => 'https://www.youtube.com/embed/jXesTUBlxl4',
                'link' => Url::to(['/analytics/flow-of-funds-report/index']),
                'image' => ImageHelper::getThumb('img/modal_registration/block-5.jpg', [680, 340], [
                    'class' => 'hide-video',
                    'style' => 'max-width: 100%',
                ]),
                'previousModal' => 2,
                'nextModal' => 4,
            ]); ?>
        </div>
        <style>
            #modal-loader-items .modal-body {
                padding: 0;
            }
        </style>
        <?php Modal::end(); ?>
        <?php $this->registerJs('
        $(document).ready(function () {
            $(".modal#modal-loader-items").modal();
        });
    '); ?>
    <?php endif; ?>
</div>

<?php $this->endContent(); ?>
