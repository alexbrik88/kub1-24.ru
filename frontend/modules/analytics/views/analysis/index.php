<?php

use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\GridView;
use frontend\modules\analytics\models\AnalysisSearch;
use frontend\widgets\RangeButtonWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this \yii\web\View */
/* @var $searchModel \frontend\modules\analytics\models\AnalysisSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = 'ABC анализ покупателей';

$this->registerJs('
    $(document).on("click", ".overal-result-table .result-row", function() {
        $("#analysissearch-id").val("");
        $("#analysissearch-group").val($(this).data("group"));
        $("#abc-analysis-grid").yiiGridView("applyFilter");
    });
');

$series = [];
foreach ($searchModel->overallResult as $row) {
    $item = [
        'name' => $searchModel->getGroupLabel($row['group']),
        'color' => $searchModel->getGroupColor($row['group']),
        'data' => [
            round($row['contractor_count_part'], 2),
            round($row['group_sum_part'], 2),
        ],
    ];
    array_unshift($series, $item);
}
?>

<style type="text/css">
.overal-result-table .result-row {
    cursor: pointer;
}
.overal-result-table .result-row:hover td {
    background-color: #f1f1f1;
}
</style>

<?php $pjax = Pjax::begin([
    'id' => 'abc-analysis-pjax',
]); ?>

<div class="row">
    <div class="col-md-3 col-md-push-9">
        <div class="row">
            <div class="col-sm-12">
                <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
            </div>
        </div>

        <?php if ($series) : ?>
            <div class="row">
                <div class="col-sm-12">
                <?= \miloschuman\highcharts\Highcharts::widget([
                    'htmlOptions' => [
                        'style' => 'height: 160px; padding: 0;',
                    ],
                    'options' => [
                        'chart' =>['type' => 'column'],
                        'title' => false,
                        'xAxis' => [
                            'categories' => ['Покупатели', 'Выручка'],
                        ],
                        'yAxis' => [
                            'title' => false,
                        ],
                        'series' => $series,
                        'plotOptions' => [
                            'column' => [
                                'stacking' => 'percent',
                                'dataLabels' => [
                                    'enabled' => true,
                                    'style' => [
                                        'color' => '#333333',
                                        'textOutline' => null
                                    ],
                                ],
                            ],
                            'series' => [
                                'showInLegend' => false,
                                'borderWidth' => 0,
                            ],
                        ],
                    ]
                ]); ?>
                </div>
            </div>
        <?php endif ?>
    </div>
    <div class="col-md-9 col-md-pull-3 created-scroll">
        <table class="table table-bordered table-hover overal-result-table" style="width: auto;">
            <tr class="heading">
                <th>Группа</th>
                <th style="text-align: center;">Сумма</th>
                <th style="text-align: center;">% суммы</th>
                <th style="text-align: center;">Кол-во покупателей</th>
                <th style="text-align: center;">% покупателей</th>
            </tr>
            <?php foreach ($searchModel->overallResult as $row) : ?>
                <tr class="result-row" data-group="<?= $row['group'] ?>">
                    <td style="color: <?= $searchModel->getGroupColor($row['group']) ?>;">
                        <?= $searchModel->getGroupLabel($row['group']) ?>
                    </td>
                    <td style="text-align: right;"><?= number_format($row['group_sum'] / 100, 2, ',', '&nbsp;') ?></td>
                    <td style="text-align: right; font-weight: bold; color: <?= $searchModel->getGroupColor($row['group']) ?>;">
                        <?= round($row['group_sum_part'], 2) ?>
                    </td>
                    <td style="text-align: right;"><?= $row['contractor_count'] ?></td>
                    <td style="text-align: right; font-weight: bold; color: <?= $searchModel->getGroupColor($row['group']) ?>;">
                        <?= round($row['contractor_count_part'], 2) ?>
                    </td>
                </tr>
            <?php endforeach ?>
            <tr class="result-row" data-group="" style="font-weight: bold;">
                <td>Итого</td>
                <td style="text-align: right;"><?= number_format($searchModel->totalSum / 100, 2, ',', ' ') ?></td>
                <td style="text-align: right;">100</td>
                <td style="text-align: right;"><?= $searchModel->tmpRowCount ?></td>
                <td style="text-align: right;">100</td>
            </tr>
        </table>
    </div>
</div>

<div class="portlet box darkblue blk_wth_srch">
    <div class="portlet-title row-fluid">
        <div class="caption list_recip col-sm-3">
            Покупатели: <?= $dataProvider->totalCount ?>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-container" style="">
            <?= GridView::widget([
                'id' => 'abc-analysis-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable documents_table fix-thead',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper bank-scroll-table',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'columns' => [
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'id',
                        'headerOptions' => [
                            'width' => '25%',
                            'class' => 'nowrap-normal max10list',
                        ],
                        'format' => 'html',
                        'filter' => $searchModel->getContractorFilterItems(),
                        'value' => function ($data) {
                            return Html::a($data->nameWithType, [
                                '/contractor/view',
                                'type' => $data->type,
                                'id' => $data->id,
                            ]);
                        },
                    ],
                    [
                        'attribute' => 'paid_sum',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-right',
                        ],
                        'format' => 'html',
                        'value' => function ($data) {
                            return number_format($data['paid_sum'] / 100, 2, ',', '&nbsp;');
                        }
                    ],
                    [
                        'attribute' => 'part',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-right',
                        ],
                        'value' => function ($data) {
                            return round($data['part'], 2);
                        }
                    ],
                    [
                        'class' => DropDownDataColumn::className(),
                        'attribute' => 'group',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-right',
                        ],
                        'dropdownOptions' => [
                            'style' => 'width: 100px; min-width: auto; left: 5px',
                        ],
                        'filter' => ['' => 'Все'] + AnalysisSearch::$groups,
                        'value' => 'groupValue',
                    ],
                    [
                        'attribute' => 'paid_count',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-right',
                        ],
                    ],
                    [
                        'attribute' => 'average',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-right',
                        ],
                        'format' => 'html',
                        'value' => function ($data) {
                            return number_format($data['average'] / 100, 2, ',', '&nbsp;');
                        }
                    ],
                    [
                        'attribute' => 'first',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-right',
                        ],
                        'format' => 'html',
                        'value' => function ($data) {
                            return date('d.m.Y', $data['first']);
                        }
                    ],
                    [
                        'attribute' => 'last',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-right',
                        ],
                        'format' => 'html',
                        'value' => function ($data) {
                            return date('d.m.Y', $data['last']);
                        }
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<?php $pjax->end(); ?>
