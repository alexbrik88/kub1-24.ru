<?php

use common\components\grid\GridView;
use common\models\cash\CashFlowsBase;
use miloschuman\highcharts\Highcharts;
use miloschuman\highcharts\HighchartsAsset;
use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\widgets\Pjax;

/* @var \yii\web\View $this */
/* @var \frontend\modules\analytics\models\ExpensesPaymentsSearch $searchModel */
/* @var array $dataArray */

$this->title = 'Затраты по оплате';

$totalRow = $dataArray['totalRow'];
$highchartsOptions = $searchModel->highchartsOptions;
$highchartsOptionsJson = Json::encode($highchartsOptions);

HighchartsAsset::register($this)->withScripts([
    'modules/exporting',
    'themes/grid-light',
]);
?>
<?php Pjax::begin([
    'id' => 'expenses-pjax',
    'linkSelector' => '.expenses-pjax-link',
    'timeout' => 10000,
]); ?>

<div class="row" style="margin-bottom: 25px;">
    <div class="col-sm-3 col-sm-offset-9">
        <div style="position: relative;">
            <div class="btn default p-t-7 p-b-7"  data-toggle="dropdown" style="width: 100%">
                <?= $searchModel->periodName ?> <b class="fa fa-angle-down"></b>
            </div>
            <?= Dropdown::widget([
                'items' => $searchModel->periodItems,
            ]); ?>
        </div>
    </div>
</div>

<?php if ($highchartsOptions['series']) : ?>
    <div class="row">
        <div id="high-charts-container" style="min-height: 400px;"></div>
    </div>
<?php endif ?>

<div class="portlet box darkblue blk_wth_srch">
    <div class="portlet-title row-fluid">
        <div class="caption list_recip col-sm-3">
            <?= $this->title ?>
        </div>
        <div class="pull-right list_recip">
            <?= Html::radioList('accounting', $searchModel->accounting, [
                '' => 'Все',
                CashFlowsBase::ACCOUNTING => 'Только для учёта в бухгалтерии',
                CashFlowsBase::NON_ACCOUNTING => 'Не для учёта в бухгалтерии',
            ], [
                'item' => function ($index, $label, $name, $checked, $value) use ($searchModel) {
                    return Html::a(
                        Html::radio($name, $checked, [
                            'value' => $value,
                            'label' => $label,
                            'labelOptions' => [
                                'class' => 'radio-inline',
                            ],
                        ]),
                        [
                            'payments', 'period' => $searchModel->period, 'accounting' => $value === '' ? null : $value,
                        ],
                        [
                            'class' => 'expenses-pjax-link',
                            'style' => 'text-underline: none; color: #fff; vertical-align: baseline;',
                        ]
                    );
                },
            ]); ?>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container">
            <div class="dataTables_wrapper dataTables_extended_wrapper">
                <table class="table report-table table-striped table-bordered table-hover dataTable status_nowrap overfl_text_hid invoice-table">
                    <thead>
                        <tr>
                            <?php
                            foreach ($searchModel->tableColumns as $column) {
                                echo Html::beginTag('th', ['style' => ($column['attribute'] != 'expenses') ? 'text-align: center;' : null]);
                                echo $column['label'];
                                if ($column['attribute'] != 'expenses') {
                                    echo Html::beginTag('div', [
                                        'class' => 'report-value-cell',
                                        'style' => 'font-weight: normal;',
                                    ]);
                                    echo 'Сумма' . Html::tag('span', '%%<i class="fa">', ['class' => 'percent']);
                                    echo Html::endTag('div');
                                }
                                echo Html::endTag('th');
                            }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($dataArray['dataRowArray'] as $row) {
                            echo Html::beginTag(
                                'tr',
                                ($row['row_type'] == 'expenses') ?
                                ['class' => 'expenses-row', 'data-expenses-id' => $row['expenses_id']] :
                                ['class' => "contractor-row expenses-id-{$row['expenses_id']} hidden"]
                            );
                            foreach ($searchModel->tableColumns as $column) {
                                $value = !empty($row[$column['attribute']]) ? $row[$column['attribute']] : 0;
                                echo Html::beginTag('td');
                                echo Html::beginTag('div', [
                                    'data-value' => ($column['attribute'] == 'expenses') ? null : $value,
                                    'class' => ($column['attribute'] == 'expenses') ? null : 'report-value-cell',
                                ]);
                                if ($column['type'] == 'amount') {
                                    echo number_format($value / 100, 2, ',', ' ');
                                    $columnTotal = $totalRow[ $column['attribute'] ];
                                    $percent = $columnTotal ? round($value / $columnTotal * 100, 2) : 0;
                                    $caret = '';
                                    $color = '';
                                    $compareTotal = $totalRow[ $column['compareArrtibute'] ];
                                    $compareValue = !empty($row[$column['compareArrtibute']]) ? $row[$column['compareArrtibute']] : 0;
                                    $comparePercent = $compareTotal ? round($compareValue / $compareTotal * 100, 2) : 0;
                                    if ($percent > $comparePercent) {
                                        $caret = 'fa-caret-up';
                                    }
                                    if ($percent < $comparePercent) {
                                        $caret = 'fa-caret-down';
                                    }
                                    echo Html::tag('span', number_format($percent, 2, ',', ' ') . Html::tag('i', '', [
                                        'class' => "fa $caret",
                                    ]), ['class' => 'percent']);
                                } else {
                                    echo $value;
                                }
                                echo Html::endTag('div');
                                echo Html::endTag('td');
                            }
                            echo Html::endTag('tr');
                        }
                        ?>
                        <tr>
                            <?php
                            foreach ($searchModel->tableColumns as $column) {
                                $value = !empty($totalRow[ $column['attribute'] ]) ? $totalRow[ $column['attribute'] ] : 0;
                                echo Html::beginTag('th', ['style' => 'font-size: 14px;']);
                                    echo Html::beginTag('div', [
                                        'data-value' => ($column['attribute'] == 'expenses') ? null : $value,
                                        'class' => ($column['attribute'] == 'expenses') ? null : 'report-value-cell',
                                    ]);
                                    if ($column['attribute'] == 'expenses') {
                                        echo $value;
                                    } else {
                                        echo number_format($value / 100, 2, ',', ' ');
                                        echo Html::tag('span', ($value ? 100 : 0) . '<i class="fa">', ['class' => 'percent']);
                                    }
                                    echo Html::endTag('div');
                                echo Html::endTag('th');
                            }
                            ?>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.highchartsOptions = <?= $highchartsOptionsJson ?>;
</script>
<?php Pjax::end(); ?>

<?php
$js = <<<JS
function createHighcharts() {
    Highcharts.chart('high-charts-container', window.highchartsOptions);
}

$(document).on("click", ".expenses-row", function() {
    $(".expenses-id-" + $(this).data("expenses-id")).toggleClass("hidden");
});

$(document).on("pjax:complete", function() {
    createHighcharts();
});

createHighcharts();
JS;

if ($highchartsOptions['series']) {
    $this->registerJs($js);
}
