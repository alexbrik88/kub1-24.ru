<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 21.01.2017
 * Time: 16:13
 */

use common\components\grid\GridView;
use common\models\Contractor;
use yii\bootstrap\Html;
use common\components\grid\DropDownSearchDataColumn;
use common\components\TextHelper;
use common\components\debts\DebtsHelper;
use frontend\modules\analytics\models\DebtReportSearch;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\analytics\models\DebtReportSearch */
$this->title = 'Отчет по месяцам';
?>
<div class="row">
    <?= \miloschuman\highcharts\Highcharts::widget([
        'scripts' => [
            'modules/exporting',
            'themes/grid-light',
        ],
        'options' => [
            'title' => [
                'text' => '',
            ],
            'lang' => [
                'printChart' => 'На печать',
                'downloadPNG' => 'Скачать PNG',
                'downloadJPEG' => 'Скачать JPEG',
                'downloadPDF' => 'Скачать PDF',
                'downloadSVG' => 'Скачать SVG',
                'contextButtonTitle' => 'Меню',
            ],
            'xAxis' => [
                'categories' => ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            ],
            'yAxis' => [
                'title' => ['text' => 'Сумма']
            ],
            'series' => [
                [
                    'type' => 'column',
                    'color' => '#dfba49',
                    'name' => 'Выставленно',
                    'pointWidth' => 17,
                    'data' => [
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '01', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '02', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '03', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '04', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '05', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '06', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '07', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '08', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '09', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '10', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '11', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::BILLED_INVOICES, false, null, '12', $searchModel->year)),
                    ],
                ],
                [
                    'type' => 'column',
                    'name' => 'Оплачено',
                    'color' => '#45b6af',
                    'pointWidth' => 17,
                    'data' => [
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '01', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '02', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '03', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '04', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '05', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '06', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '07', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '08', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '09', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '10', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '11', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::PAYED_INVOICES, false, null, '12', $searchModel->year)),
                    ],
                ],
                [
                    'type' => 'column',
                    'name' => 'Не оплачено',
                    'color' => '#f3565d',
                    'pointWidth' => 17,
                    'data' => [
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '01', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '02', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '03', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '04', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '05', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '06', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '07', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '08', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '09', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '10', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '11', $searchModel->year)),
                        (float) TextHelper::moneyFormatFromIntToFloat(DebtsHelper::getDebtMonth(DebtReportSearch::DEBT_INVOICES, false, null, '12', $searchModel->year)),
                    ],
                ],
            ],
        ]
    ]); ?>
</div>
<?php \yii\widgets\Pjax::begin([
    'timeout' => 10000,
]); ?>
<div class="portlet box darkblue">
    <div class="portlet-title float-left">
        <div class="caption">
            По покупателям
        </div>
    </div>
    <?= Html::beginForm(['debtor-month'], 'GET', [
        'validateOnChange' => true,
    ]); ?>
    <div class="search-form-default">
        <div class="col-md-9 pull-right serveces-search"
             style="max-width: 595px;">
            <div class="input-group" style="display: block;">
                <div class="input-cont">
                    <div class="col-md-5"></div>
                    <div class="col-md-4 col-xs-6 " style="padding-right: 0; padding-left: 0">
                        <?= Html::activeDropDownList($searchModel, 'debtType', $searchModel->debtItems, [
                            'class' => 'form-control',
                            'style' => 'display: inline-block;',
                        ]); ?>
                    </div>
                    <div class="col-md-3 col-xs-6" style="padding-right: 0;">
                        <?= Html::activeDropDownList($searchModel, 'year', $searchModel->getYearFilter(), [
                            'class' => 'form-control',
                            'style' => 'display: inline-block;',
                            'value' => $searchModel->year ? $searchModel->year : date('Y'),
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>


    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '---'],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable customers_table',
                    'id' => 'datatable_ajax',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'columns' => [
                    [
                        'attribute' => 'name',
                        'label' => 'Название',
                        'class' => DropDownSearchDataColumn::className(),
                        'format' => 'raw',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '20%',
                        ],
                        'filter' => $searchModel->getContractorFilter(),
                        'value' => function (Contractor $model) {
                            return Html::a($model->nameWithType, \yii\helpers\Url::to(['/contractor/view', 'id' => $model->id, 'type' => $model->type]));
                        },
                    ],
                    [
                        'attribute' => 'debt_january_sum',
                        'label' => 'янв.',
                        'format' => 'raw',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_january_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_february_sum',
                        'label' => 'февр.',
                        'format' => 'raw',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_february_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_march_sum',
                        'label' => 'март',
                        'format' => 'raw',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_march_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_april_sum',
                        'label' => 'апр.',
                        'format' => 'raw',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_april_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_may_sum',
                        'label' => 'май',
                        'format' => 'raw',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_may_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_june_sum',
                        'label' => 'июнь',
                        'format' => 'raw',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_june_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_jule_sum',
                        'label' => 'июль',
                        'format' => 'raw',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_jule_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_august_sum',
                        'label' => 'авг.',
                        'format' => 'raw',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_august_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_september_sum',
                        'label' => 'сент.',
                        'format' => 'raw',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_september_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_october_sum',
                        'label' => 'окт.',
                        'format' => 'raw',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_october_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_november_sum',
                        'label' => 'нояб.',
                        'format' => 'raw',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_november_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_december_sum',
                        'label' => 'дек.',
                        'format' => 'raw',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_december_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_all_sum',
                        'label' => 'Итого',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_all_sum, 2);
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
<?php \yii\widgets\Pjax::end(); ?>
