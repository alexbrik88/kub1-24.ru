<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.01.2017
 * Time: 4:14
 */

use common\components\debts\DebtsHelper;
use common\components\helpers\Html;
use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\Contractor;
use common\components\grid\DropDownSearchDataColumn;
use common\models\EmployeeCompany;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\analytics\models\DebtReportSearch
 * @var $currentDebt integer
 * @var $debtSum10 integer
 * @var $debtSum30 integer
 * @var $debtSum60 integer
 * @var $debtSum90 integer
 * @var $debtSumMore90 integer
 * @var $allDebtSum integer
 * @var $formatSum integer
 */

$this->title = 'Отчет по клиентам';

$debtSumCountAll = 0;
$currentSum = 0;

$debtSumCountInvoiceAll = 0;
$currentInvoiceCount = 0;
$company = Yii::$app->user->identity->company;
?>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-hover" style="width: auto;">
            <tbody>
            <tr role="row">
                <td></td>
                <td class="text-left" style="font-weight: 600;">Сумма</td>
                <td class="text-left" style="font-weight: 600;">%%</td>
                <td class="text-left" style="font-weight: 600;">Кол-во<br>должников</td>
                <td class="text-left" style="font-weight: 600;">Кол-во<br>счетов</td>
                <td class="text-left" style="font-weight: 600; max-width: 140px;">Средняя сумма долга должника</td>
                <td class="text-left" style="font-weight: 600; max-width: 140px;">Средняя сумма просроченного счета</td>
            </tr>
            <tr role="row">
                <td class="text-left">Текущие неоплаченные счета
                </td>
                <td class="text-right">
                    <?= TextHelper::invoiceMoneyFormat($currentDebt, 2); ?>
                </td>
                <td class="text-right">
                    <?= round(($currentDebt / $formatSum) * 100, 2); ?>%
                </td>
                <td class="text-right">
                    <?php echo $currentSum = DebtsHelper::getCurrentDebtCount();
                    $debtSumCountAll += $currentSum; ?>
                </td>
                <td class="text-right">
                    <?php echo $currentInvoiceCount = DebtsHelper::getCurrentDebtCount();
                    $debtSumCountInvoiceAll += $currentInvoiceCount; ?>
                </td>
                <td class="text-right">
                    <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($currentDebt / $currentSum, 2), 2) : 0; ?>
                </td>
                <td class="text-right">
                    <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($currentDebt / $currentInvoiceCount, 2), 2) : 0; ?>
                </td>
            </tr>
            <tr role="row">
                <td class="text-left">1-10 дней просрочено</td>
                <td class="text-right">
                    <?= TextHelper::invoiceMoneyFormat($debtSum10, 2); ?>
                </td>
                <td class="text-right">
                    <?= round(($debtSum10 / $formatSum) * 100, 2); ?>%
                </td>
                <td class="text-right">
                    <?php echo $currentSum = DebtsHelper::getDebtsSumCount(DebtsHelper::PERIOD_0_10);
                    $debtSumCountAll += $currentSum; ?>
                </td>
                <td class="text-right">
                    <?php echo $currentInvoiceCount = DebtsHelper::getDebtsSumInvoiceCount(DebtsHelper::PERIOD_0_10);
                    $debtSumCountInvoiceAll += $currentInvoiceCount; ?>
                </td>
                <td class="text-right">
                    <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($debtSum10 / $currentSum, 2), 2) : 0; ?>
                </td>
                <td class="text-right">
                    <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($debtSum10 / $currentInvoiceCount, 2), 2) : 0; ?>
                </td>
            </tr>
            <tr role="row">
                <td class="text-left">11-30 дней просрочено</td>
                <td class="text-right">
                    <?= TextHelper::invoiceMoneyFormat($debtSum30, 2); ?>
                </td>
                <td class="text-right">
                    <?= round(($debtSum30 / $formatSum) * 100, 2); ?>%
                </td>
                <td class="text-right">
                    <?php echo $currentSum = DebtsHelper::getDebtsSumCount(DebtsHelper::PERIOD_11_30);
                    $debtSumCountAll += $currentSum; ?>
                </td>
                <td class="text-right">
                    <?php echo $currentInvoiceCount = DebtsHelper::getDebtsSumInvoiceCount(DebtsHelper::PERIOD_11_30);
                    $debtSumCountInvoiceAll += $currentInvoiceCount; ?>
                </td>
                <td class="text-right">
                    <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($debtSum30 / $currentSum, 2), 2) : 0; ?>
                </td>
                <td class="text-right">
                    <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($debtSum30 / $currentInvoiceCount, 2), 2) : 0; ?>
                </td>
            </tr>
            <tr role="row">
                <td class="text-left">31-60 дней просрочено</td>
                <td class="text-right">
                    <?= TextHelper::invoiceMoneyFormat($debtSum60, 2); ?>
                </td>
                <td class="text-right">
                    <?= round(($debtSum60 / $formatSum) * 100, 2); ?>%
                </td>
                <td class="text-right">
                    <?php echo $currentSum = DebtsHelper::getDebtsSumCount(DebtsHelper::PERIOD_31_60);
                    $debtSumCountAll += $currentSum; ?>
                </td>
                <td class="text-right">
                    <?php echo $currentInvoiceCount = DebtsHelper::getDebtsSumInvoiceCount(DebtsHelper::PERIOD_31_60);
                    $debtSumCountInvoiceAll += $currentInvoiceCount; ?>
                </td>
                <td class="text-right">
                    <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($debtSum60 / $currentSum, 2), 2) : 0; ?>
                </td>
                <td class="text-right">
                    <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($debtSum60 / $currentInvoiceCount, 2), 2) : 0; ?>
                </td>
            </tr>
            <tr role="row">
                <td class="text-left">61-90 дней просрочено</td>
                <td class="text-right">
                    <?= TextHelper::invoiceMoneyFormat($debtSum90, 2); ?>
                </td>
                <td class="text-right">
                    <?= round(($debtSum90 / $formatSum) * 100, 2); ?>%
                </td>
                <td class="text-right">
                    <?php echo $currentSum = DebtsHelper::getDebtsSumCount(DebtsHelper::PERIOD_61_90);
                    $debtSumCountAll += $currentSum; ?>
                </td>
                <td class="text-right">
                    <?php echo $currentInvoiceCount = DebtsHelper::getDebtsSumInvoiceCount(DebtsHelper::PERIOD_61_90);
                    $debtSumCountInvoiceAll += $currentInvoiceCount; ?>
                </td>
                <td class="text-right">
                    <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($debtSum90 / $currentSum, 2), 2) : 0; ?>
                </td>
                <td class="text-right">
                    <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($debtSum90 / $currentInvoiceCount, 2), 2) : 0; ?>
                </td>
            </tr>
            <tr role="row">
                <td class="text-left">Больше 90 дней просрочено</td>
                <td class="text-right">
                    <?= TextHelper::invoiceMoneyFormat($debtSumMore90, 2); ?>
                </td>
                <td class="text-right">
                    <?= round(($debtSumMore90 / $formatSum) * 100, 2); ?>%
                </td>
                <td class="text-right">
                    <?php echo $currentSum = DebtsHelper::getDebtsSumCount(DebtsHelper::PERIOD_MORE_90);
                    $debtSumCountAll += $currentSum; ?>
                </td>
                <td class="text-right">
                    <?php echo $currentInvoiceCount = DebtsHelper::getDebtsSumInvoiceCount(DebtsHelper::PERIOD_MORE_90);
                    $debtSumCountInvoiceAll += $currentInvoiceCount; ?>
                </td>
                <td class="text-right">
                    <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($debtSumMore90 / $currentSum, 2), 2) : 0; ?>
                </td>
                <td class="text-right">
                    <?= $currentSum != 0 ? TextHelper::invoiceMoneyFormat(round($debtSumMore90 / $currentInvoiceCount, 2), 2) : 0; ?>
                </td>
            </tr>
            <tr role="row">
                <td class="text-left" style="font-weight: 600;">Вся задолженность</td>
                <td class="text-right" style="font-weight: 600;">
                    <?= TextHelper::invoiceMoneyFormat($allDebtSum, 2); ?>
                </td>
                <td class="text-right" style="font-weight: 600;">100 %</td>
                <td class="text-right" style="font-weight: 600;"><?= $debtSumCountAll; ?></td>
                <td class="text-right" style="font-weight: 600;"><?= $debtSumCountInvoiceAll; ?></td>
                <td class="text-right" style="font-weight: 600;">
                    <?= $debtSumCountAll != 0 ? TextHelper::invoiceMoneyFormat($allDebtSum / $debtSumCountAll, 2) : 0; ?>
                </td>
                <td class="text-right" style="font-weight: 600;">
                    <?= $debtSumCountInvoiceAll != 0 ? TextHelper::invoiceMoneyFormat($allDebtSum / $debtSumCountInvoiceAll, 2) : 0; ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="portlet box darkblue">
    <div class="portlet-title float-left">
        <div class="caption ">
            Список должников
        </div>
    </div>
    <?= Html::beginForm(['debtor'], 'GET'); ?>
    <div class="search-form-default">
        <div class="col-md-9 pull-right serveces-search"
             style="max-width: 595px;">
            <div class="input-group">
                <div class="input-cont">
                    <?= Html::activeTextInput($searchModel, 'title', [
                        'placeholder' => 'Поиск...',
                        'class' => 'form-control',
                    ]) ?>
                </div>
                <span class="input-group-btn">
                    <?= Html::submitButton('Найти', [
                        'class' => 'btn green-haze',
                    ]); ?>
                </span>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>


    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '---'],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable customers_table',
                    'id' => 'datatable_ajax',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'columns' => [
                    [
                        'attribute' => 'name',
                        'label' => 'Название',
                        'class' => DropDownSearchDataColumn::className(),
                        'format' => 'raw',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '20%',
                        ],
                        'filter' => $searchModel->getContractorFilter(),
                        'value' => function (Contractor $model) {
                            return Html::a($model->nameWithType, \yii\helpers\Url::to(['/contractor/view', 'id' => $model->id, 'type' => $model->type]));
                        },
                    ],
                    [
                        'attribute' => 'current_debt_sum',
                        'label' => 'Текущие неоплаченные счета',
                        'format' => 'raw',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '15%',
                        ],
                        'value' => function (Contractor $data) {
                            return TextHelper::invoiceMoneyFormat($data->current_debt_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_0_10_sum',
                        'label' => '1-10 Дней',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_0_10_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_11_30_sum',
                        'label' => '11-30 Дней',
                        'format' => 'raw',
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_11_30_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_31_60_sum',
                        'label' => '31-60 Дней',
                        'format' => 'raw',
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_31_60_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_61_90_sum',
                        'label' => '61-90 Дней',
                        'format' => 'raw',
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_61_90_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_more_90_sum',
                        'label' => 'Больше 90 Дней',
                        'format' => 'raw',
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_more_90_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_all_sum',
                        'label' => 'Итого',
                        'format' => 'raw',
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_all_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'responsible_employee_id',
                        'label' => 'От&shy;вет&shy;ствен&shy;ный',
                        'encodeLabel' => false,
                        'class' => DropDownSearchDataColumn::className(),
                        'filter' => $searchModel->getResponsibleItemsByQuery($dataProvider->query),
                        'value' => function (Contractor $model) use ($company) {
                            $employee = EmployeeCompany::findOne([
                                'employee_id' => $model->responsible_employee_id,
                                'company_id' => $company->id,
                            ]);

                            return $employee ? $employee->getFio(true) : '';
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>