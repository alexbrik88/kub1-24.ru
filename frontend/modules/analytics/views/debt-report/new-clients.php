<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 21.03.2017
 * Time: 6:32
 */

use common\components\TextHelper;
use miloschuman\highcharts\Highcharts;
use yii\helpers\Html;
use frontend\modules\analytics\models\DebtReportSearch;
use common\models\Contractor;
use common\components\grid\GridView;
use common\components\grid\DropDownSearchDataColumn;

/* @var $monthChart [] */
/* @var $clientsChart [] */
/* @var $searchModel DebtReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отчет по новым клиентам';
?>
<div class="row">
    <?= Highcharts::widget([
        'scripts' => [
            'modules/exporting',
            'themes/grid-light',
        ],
        'options' => [
            'title' => [
                'text' => '',
            ],
            'lang' => [
                'printChart' => 'На печать',
                'downloadPNG' => 'Скачать PNG',
                'downloadJPEG' => 'Скачать JPEG',
                'downloadPDF' => 'Скачать PDF',
                'downloadSVG' => 'Скачать SVG',
                'contextButtonTitle' => 'Меню',
            ],
            'xAxis' => [
                'categories' => ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            ],
            'yAxis' => [
                [
                    'title' => ['text' => 'Сумма'],
                ],
            ],
            'series' => [
                [
                    'type' => 'column',
                    'color' => '#dfba49',
                    'name' => 'Оплачено',
                    'pointWidth' => 17,
                    'data' => [
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['01']['payedByNewClients'] + $monthChart['01']['payedByOldClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['02']['payedByNewClients'] + $monthChart['02']['payedByOldClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['03']['payedByNewClients'] + $monthChart['03']['payedByOldClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['04']['payedByNewClients'] + $monthChart['04']['payedByOldClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['05']['payedByNewClients'] + $monthChart['05']['payedByOldClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['06']['payedByNewClients'] + $monthChart['06']['payedByOldClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['07']['payedByNewClients'] + $monthChart['07']['payedByOldClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['08']['payedByNewClients'] + $monthChart['08']['payedByOldClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['09']['payedByNewClients'] + $monthChart['09']['payedByOldClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['10']['payedByNewClients'] + $monthChart['10']['payedByOldClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['11']['payedByNewClients'] + $monthChart['11']['payedByOldClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['12']['payedByNewClients'] + $monthChart['12']['payedByOldClients']),
                    ],
                ],
                [
                    'type' => 'column',
                    'name' => 'Оплачено текущими клиентами',
                    'color' => '#45b6af',
                    'pointWidth' => 17,
                    'data' => [
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['01']['payedByOldClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['02']['payedByOldClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['03']['payedByOldClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['04']['payedByOldClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['05']['payedByOldClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['06']['payedByOldClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['07']['payedByOldClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['08']['payedByOldClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['09']['payedByOldClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['10']['payedByOldClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['11']['payedByOldClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['12']['payedByOldClients']),
                    ],
                ],
                [
                    'type' => 'column',
                    'name' => 'Оплачено новыми клиентами',
                    'color' => '#8775A7',
                    'pointWidth' => 17,
                    'data' => [
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['01']['payedByNewClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['02']['payedByNewClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['03']['payedByNewClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['04']['payedByNewClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['05']['payedByNewClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['06']['payedByNewClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['07']['payedByNewClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['08']['payedByNewClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['09']['payedByNewClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['10']['payedByNewClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['11']['payedByNewClients']),
                        (float) TextHelper::moneyFormatFromIntToFloat($monthChart['12']['payedByNewClients']),
                    ],
                ],
                [
                    'type' => 'line',
                    'name' => 'Средний чек',
                    'color' => '#db4437',
                    'pointPlacement' => -0.2,
                    'data' => [
                        (float) TextHelper::moneyFormatFromIntToFloat(
                            ($clientsChart['01']['newClients'] + $clientsChart['01']['oldClients']) ?
                            ($monthChart['01']['payedByNewClients'] + $monthChart['01']['payedByOldClients']) /
                            ($clientsChart['01']['newClients'] + $clientsChart['01']['oldClients']) : 0
                        ),
                        (float) TextHelper::moneyFormatFromIntToFloat(
                            ($clientsChart['02']['newClients'] + $clientsChart['02']['oldClients']) ?
                            ($monthChart['02']['payedByNewClients'] + $monthChart['02']['payedByOldClients']) /
                            ($clientsChart['02']['newClients'] + $clientsChart['02']['oldClients']) : 0
                        ),
                        (float) TextHelper::moneyFormatFromIntToFloat(
                            ($clientsChart['03']['newClients'] + $clientsChart['03']['oldClients']) ?
                            ($monthChart['03']['payedByNewClients'] + $monthChart['03']['payedByOldClients']) /
                            ($clientsChart['03']['newClients'] + $clientsChart['03']['oldClients']) : 0
                        ),
                        (float) TextHelper::moneyFormatFromIntToFloat(
                            ($clientsChart['04']['newClients'] + $clientsChart['04']['oldClients']) ?
                            ($monthChart['04']['payedByNewClients'] + $monthChart['04']['payedByOldClients']) /
                            ($clientsChart['04']['newClients'] + $clientsChart['04']['oldClients']) : 0
                        ),
                        (float) TextHelper::moneyFormatFromIntToFloat(
                            ($clientsChart['05']['newClients'] + $clientsChart['05']['oldClients']) ?
                            ($monthChart['05']['payedByNewClients'] + $monthChart['05']['payedByOldClients']) /
                            ($clientsChart['05']['newClients'] + $clientsChart['05']['oldClients']) : 0
                        ),
                        (float) TextHelper::moneyFormatFromIntToFloat(
                            ($clientsChart['06']['newClients'] + $clientsChart['06']['oldClients']) ?
                            ($monthChart['06']['payedByNewClients'] + $monthChart['06']['payedByOldClients']) /
                            ($clientsChart['06']['newClients'] + $clientsChart['06']['oldClients']) : 0
                        ),
                        (float) TextHelper::moneyFormatFromIntToFloat(
                            ($clientsChart['07']['newClients'] + $clientsChart['07']['oldClients']) ?
                            ($monthChart['07']['payedByNewClients'] + $monthChart['07']['payedByOldClients']) /
                            ($clientsChart['07']['newClients'] + $clientsChart['07']['oldClients']) : 0
                        ),
                        (float) TextHelper::moneyFormatFromIntToFloat(
                            ($clientsChart['08']['newClients'] + $clientsChart['08']['oldClients']) ?
                            ($monthChart['08']['payedByNewClients'] + $monthChart['08']['payedByOldClients']) /
                            ($clientsChart['08']['newClients'] + $clientsChart['08']['oldClients']) : 0
                        ),
                        (float) TextHelper::moneyFormatFromIntToFloat(
                            ($clientsChart['09']['newClients'] + $clientsChart['09']['oldClients']) ?
                            ($monthChart['09']['payedByNewClients'] + $monthChart['09']['payedByOldClients']) /
                            ($clientsChart['09']['newClients'] + $clientsChart['09']['oldClients']) : 0
                        ),
                        (float) TextHelper::moneyFormatFromIntToFloat(
                            ($clientsChart['10']['newClients'] + $clientsChart['10']['oldClients']) ?
                            ($monthChart['10']['payedByNewClients'] + $monthChart['10']['payedByOldClients']) /
                            ($clientsChart['10']['newClients'] + $clientsChart['10']['oldClients']) : 0
                        ),
                        (float) TextHelper::moneyFormatFromIntToFloat(
                            ($clientsChart['11']['newClients'] + $clientsChart['11']['oldClients']) ?
                            ($monthChart['11']['payedByNewClients'] + $monthChart['11']['payedByOldClients']) /
                            ($clientsChart['11']['newClients'] + $clientsChart['11']['oldClients']) : 0
                        ),
                        (float) TextHelper::moneyFormatFromIntToFloat(
                            ($clientsChart['12']['newClients'] + $clientsChart['12']['oldClients']) ?
                            ($monthChart['12']['payedByNewClients'] + $monthChart['12']['payedByOldClients']) /
                            ($clientsChart['12']['newClients'] + $clientsChart['12']['oldClients']) : 0
                        ),
                    ],
                ],
            ],
        ]
    ]); ?>
</div>
<div class="row">
    <?= Highcharts::widget([
        'scripts' => [
            'modules/exporting',
            'themes/grid-light',
        ],
        'options' => [
            'title' => [
                'text' => '',
            ],
            'lang' => [
                'printChart' => 'На печать',
                'downloadPNG' => 'Скачать PNG',
                'downloadJPEG' => 'Скачать JPEG',
                'downloadPDF' => 'Скачать PDF',
                'downloadSVG' => 'Скачать SVG',
                'contextButtonTitle' => 'Меню',
            ],
            'xAxis' => [
                'categories' => ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            ],
            'yAxis' => [
                'title' => ['text' => 'Количество']
            ],
            'series' => [
                [
                    'type' => 'column',
                    'color' => '#dfba49',
                    'name' => 'Всего клиентов',
                    'pointWidth' => 17,
                    'data' => [
                        $clientsChart['01']['newClients'] + $clientsChart['01']['oldClients'],
                        $clientsChart['02']['newClients'] + $clientsChart['02']['oldClients'],
                        $clientsChart['03']['newClients'] + $clientsChart['03']['oldClients'],
                        $clientsChart['04']['newClients'] + $clientsChart['04']['oldClients'],
                        $clientsChart['05']['newClients'] + $clientsChart['05']['oldClients'],
                        $clientsChart['06']['newClients'] + $clientsChart['06']['oldClients'],
                        $clientsChart['07']['newClients'] + $clientsChart['07']['oldClients'],
                        $clientsChart['08']['newClients'] + $clientsChart['08']['oldClients'],
                        $clientsChart['09']['newClients'] + $clientsChart['09']['oldClients'],
                        $clientsChart['10']['newClients'] + $clientsChart['10']['oldClients'],
                        $clientsChart['11']['newClients'] + $clientsChart['11']['oldClients'],
                        $clientsChart['12']['newClients'] + $clientsChart['12']['oldClients'],
                    ],
                ],
                [
                    'type' => 'column',
                    'name' => 'Текущих клиентов',
                    'color' => '#45b6af',
                    'pointWidth' => 17,
                    'data' => [
                        $clientsChart['01']['oldClients'],
                        $clientsChart['02']['oldClients'],
                        $clientsChart['03']['oldClients'],
                        $clientsChart['04']['oldClients'],
                        $clientsChart['05']['oldClients'],
                        $clientsChart['06']['oldClients'],
                        $clientsChart['07']['oldClients'],
                        $clientsChart['08']['oldClients'],
                        $clientsChart['09']['oldClients'],
                        $clientsChart['10']['oldClients'],
                        $clientsChart['11']['oldClients'],
                        $clientsChart['12']['oldClients'],
                    ],
                ],
                [
                    'type' => 'column',
                    'name' => 'Новых клиентов',
                    'color' => '#8775A7',
                    'pointWidth' => 17,
                    'data' => [
                        $clientsChart['01']['newClients'],
                        $clientsChart['02']['newClients'],
                        $clientsChart['03']['newClients'],
                        $clientsChart['04']['newClients'],
                        $clientsChart['05']['newClients'],
                        $clientsChart['06']['newClients'],
                        $clientsChart['07']['newClients'],
                        $clientsChart['08']['newClients'],
                        $clientsChart['09']['newClients'],
                        $clientsChart['10']['newClients'],
                        $clientsChart['11']['newClients'],
                        $clientsChart['12']['newClients'],
                    ],
                ],
            ],
        ]
    ]); ?>
</div>
<?php \yii\widgets\Pjax::begin([
    'timeout' => 10000,
]); ?>
<div class="portlet box darkblue">
    <div class="portlet-title float-left">
        <div class="caption">
            По новым клиентам
        </div>
    </div>
    <?= Html::beginForm(['new-clients'], 'GET', [
        'validateOnChange' => true,
    ]); ?>
    <div class="search-form-default">
        <div class="col-md-9 pull-right serveces-search"
             style="max-width: 595px;">
            <div class="input-group" style="display: block;">
                <div class="input-cont">
                    <div class="col-md-5"></div>
                    <div class="col-md-4 col-xs-6"
                         style="padding-right: 0; padding-left: 0">
                    </div>
                    <div class="col-md-3 col-xs-6" style="padding-right: 0;">
                        <?= Html::activeDropDownList($searchModel, 'year', $searchModel->getYearFilter(), [
                            'class' => 'form-control',
                            'style' => 'display: inline-block;',
                            'value' => $searchModel->year ? $searchModel->year : date('Y'),
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '---'],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable customers_table',
                    'id' => 'datatable_ajax',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'columns' => [
                    [
                        'attribute' => 'name',
                        'class' => DropDownSearchDataColumn::className(),
                        'label' => 'Название',
                        'format' => 'raw',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '20%',
                        ],
                        'filter' => $searchModel->getNewContractorsFilter(),
                        'value' => function (Contractor $model) {
                            return Html::a($model->nameWithType, \yii\helpers\Url::to(['/contractor/view', 'id' => $model->id, 'type' => $model->type]));
                        },
                    ],
                    [
                        'attribute' => 'debt_january_sum',
                        'label' => 'янв.',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_january_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_february_sum',
                        'label' => 'февр.',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_february_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_march_sum',
                        'label' => 'март',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_march_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_april_sum',
                        'label' => 'апр.',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_april_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_may_sum',
                        'label' => 'май',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_may_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_june_sum',
                        'label' => 'июнь',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_june_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_jule_sum',
                        'label' => 'июль',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_jule_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_august_sum',
                        'label' => 'авг.',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_august_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_september_sum',
                        'label' => 'сент.',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_september_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_october_sum',
                        'label' => 'окт.',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_october_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_november_sum',
                        'label' => 'нояб.',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_november_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'debt_december_sum',
                        'label' => 'дек.',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Contractor $model) {
                            return TextHelper::invoiceMoneyFormat($model->debt_december_sum, 2);
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
<?php \yii\widgets\Pjax::end(); ?>

