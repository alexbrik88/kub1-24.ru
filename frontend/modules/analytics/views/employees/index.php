<?php

use frontend\modules\analytics\models\EmployeesSearch;
use yii\bootstrap\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\analytics\models\EmployeesSearch */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->title = 'Ключевые показатели эффективности сотрудников';

$urlDate = $searchModel->date->format('Y-m-01');
$dateItems = [];
foreach ($searchModel->dateItems as $key => $value) {
    $dateItems[] = [
        'label' => $value,
        'url' => ['/analytics/employees/index', 'date' => $key],
        'linkOptions' => ['class' => $urlDate == $key ? 'active' : ''],
    ];
}

$currMonth = $searchModel->date;
$prevMonth = $searchModel->prevDate;
$currName = Yii::$app->formatter->asDate($currMonth, 'LLLL yyyy');
$prevName = Yii::$app->formatter->asDate($prevMonth, 'LLLL yyyy');
$currKey = $currMonth->format('Ym');
$prevKey = $prevMonth->format('Ym');
$categories = [];
foreach ($searchModel->getDatePeriod() as $date) {
    $categories[] = strtr(Yii::$app->formatter->asDate($date, 'LLL yyyy'), [' ' => '<br/>']);
}

$currAllSum = 0;
$prevAllSum = 0;
$currAllCount = 0;
$prevAllCount = 0;
$currAllPaid = 0;
$prevAllPaid = 0;
$currAllDebt = 0;
$prevAllDebt = 0;
foreach ($dataProvider->models as $model) {
    $currAllSum += $model->getReportData('sum', $currKey);
    $prevAllSum += $model->getReportData('sum', $prevKey);
    $currAllCount += $model->getReportData('count', $currKey);
    $prevAllCount += $model->getReportData('count', $prevKey);
    $currAllPaid += $model->getReportData('paid', $currKey);
    $prevAllPaid += $model->getReportData('paid', $prevKey);
    $currAllDebt += $model->getReportData('debt', $currKey);
    $prevAllDebt += $model->getReportData('debt', $prevKey);
}
$diffSum = $prevAllSum > 0 ? round(($currAllSum - $prevAllSum) * 100 / $prevAllSum, 2) : null;
$diffSum = $diffSum !== null ? (($diffSum > 0 ? '+' : '') . $diffSum . '%') : '-';
$diffCount = $prevAllCount > 0 ? round(($currAllCount - $prevAllCount) * 100 / $prevAllCount, 2) : null;
$diffCount = $diffCount !== null ? (($diffCount > 0 ? '+' : '') . $diffCount . '%') : '-';
$diffPaid = $prevAllPaid > 0 ? round(($currAllPaid - $prevAllPaid) * 100 / $prevAllPaid, 2) : null;
$diffPaid = $diffPaid !== null ? (($diffPaid > 0 ? '+' : '') . $diffPaid . '%') : '-';
$diffDebt = $prevAllDebt > 0 ? round(($currAllDebt - $prevAllDebt) * 100 / $prevAllDebt, 2) : null;
$diffDebt = $diffDebt !== null ? (($diffDebt > 0 ? '+' : '') . $diffDebt . '%') : '-';
?>

<div class="portlet box">
    <h3 class="page-title"><?= $this->title ?></h3>
</div>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Отчет за
            <div style="display: inline-block; width: 200px;">
                <div class="dropdown">
                    <?= Html::tag('div', $currName, [
                        'class' => 'dropdown-toggle',
                        'data-toggle' => 'dropdown',
                        'style' => 'display: inline-block; border-bottom: 1px dashed #fff; cursor: pointer;',
                    ])?>
                    <?= Dropdown::widget([
                        'id' => 'employee-rating-dropdown',
                        'items' => array_reverse($dateItems),
                    ])?>
                </div>
            </div>
        </div>
    </div>

    <div class="portlet-body accounts-list">
        <div class="balance-report scroll-table-wrapper" style="">
            <table class="employee-report">
                <tbody>
                    <?php foreach ($dataProvider->models as $model) : ?>
                        <?php
                        $currSum = $model->getReportData('sum', $currKey);
                        $prevSum = $model->getReportData('sum', $prevKey);
                        $averageSum = array_sum($model->getReportData('sum')) / 12;
                        $currCount = $model->getReportData('count', $currKey);
                        $prevCount = $model->getReportData('count', $prevKey);
                        $averageCount = array_sum($model->getReportData('count')) / 12;
                        $currPaid = $model->getReportData('paid', $currKey);
                        $prevPaid = $model->getReportData('paid', $prevKey);
                        $averagePaid = array_sum($model->getReportData('paid')) / 12;
                        $currDebt = $model->getReportData('debt', $currKey);
                        $prevDebt = $model->getReportData('debt', $prevKey);
                        $averageDebt = array_sum($model->getReportData('debt')) / 12;
                        ?>
                        <tr class="name-row">
                            <td colspan="6">
                                <?= $model->getFio(true) ?>
                            </td>
                        </tr>
                        <tr class="param-headers">
                            <td class=""></td>
                            <td class=""><?= mb_convert_case($currName, MB_CASE_TITLE, "UTF-8") ?></td>
                            <td class=""><?= mb_convert_case($prevName, MB_CASE_TITLE, "UTF-8") ?></td>
                            <td class="">%%</td>
                            <td class="">Среднее (12 месяцев)</td>
                            <td class=""></td>
                        </tr>
                        <tr>
                            <td class="param-label">
                                <div class="chart-collapse" data-target="#sum-chart-<?=$model->employee_id?>">+</div>
                                Счета <i class="fa fa-rub"></i>
                            </td>
                            <td class="value-cell"><?= number_format($currSum / 100, 2, ',', ' ') ?></td>
                            <td class="value-cell"><?= number_format($prevSum / 100, 2, ',', ' ') ?></td>
                            <td class="value-cell">
                                <?= (($p = ($prevSum > 0 ?
                                    round(($currSum - $prevSum) * 100 / $prevSum, 2) :
                                    null)) !== null && $p > 0 ? '+' : '') . $p; ?>
                            </td>
                            <td class="value-cell"><?= number_format($averageSum / 100, 2, ',', ' ') ?></td>
                            <td class=""></td>
                        </tr>
                        <tr class="param-chart">
                            <td colspan="6">
                                <div id="sum-chart-<?=$model->employee_id?>" style="height: 0; overflow: hidden;">
                                    <?php
                                    $data = [];
                                    foreach (array_values($model->getReportData('sum')) as $value) {
                                        $data[] = $value / 100;
                                    }
                                    ?>
                                    <table>
                                        <tr>
                                            <td class="param-chart-cell">
                                                <?= $this->render('index/_item_chart', [
                                                    'container' => 'sum-chart-box-' . $model->employee_id,
                                                    'categories' => $categories,
                                                    'series' => [['name' => 'Счета', 'data' => $data, 'color' => '#57b8ae']],
                                                    'format' => '<b>{point.y:,.2f} <i class="fa fa-rub"></i></b>',
                                                    'yTitle' => 'Счета <i class="fa fa-rub"></i>',
                                                ]); ?>
                                            </td>
                                            <td class="param-total-cell">
                                                <div>Отчетный месяц</div>
                                                <div><?= $currName ?></div>
                                                <div>
                                                    <?= number_format($currSum / 100, 2, ',', ' ') ?>
                                                    <i class="fa fa-rub"></i>
                                                </div>
                                                <div><?= ($p !== null && $p > 0 ? '+' : '') . $p; ?> %</div>
                                                <div>ПО КОМПАНИИ</div>
                                                <div><?= $diffSum ?></div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="param-label">
                                <div class="chart-collapse" data-target="#count-chart-<?=$model->employee_id?>">+</div>
                                Счета (шт)
                            </td>
                            <td class="value-cell"><?= (int) $currCount; ?></td>
                            <td class="value-cell"><?= (int) $prevCount; ?></td>
                            <td class="value-cell">
                                <?= (($p = ($prevCount > 0 ?
                                    round(($currCount - $prevCount) * 100 / $prevCount, 2) :
                                    null)) !== null && $p > 0 ? '+' : '') . $p; ?>
                            </td>
                            <td class="value-cell"><?= round($averageCount, 2); ?></td>
                            <td class=""></td>
                        </tr>
                        <tr class="param-chart">
                            <td colspan="6">
                                <div id="count-chart-<?=$model->employee_id?>" style="height: 0; overflow: hidden;">
                                    <?php
                                    $data = [];
                                    foreach (array_values($model->getReportData('count')) as $value) {
                                        $data[] = $value;
                                    }
                                    ?>
                                    <table>
                                        <tr>
                                            <td class="param-chart-cell">
                                                <?= $this->render('index/_item_chart', [
                                                    'container' => 'count-chart-box-' . $model->employee_id,
                                                    'categories' => $categories,
                                                    'series' => [['name' => 'Счета', 'data' => $data, 'color' => '#57b8ae']],
                                                    'format' => '<b>{point.y} шт</b>',
                                                    'yTitle' => 'Счета (шт)',
                                                ]); ?>
                                            </td>
                                            <td class="param-total-cell">
                                                <div>Отчетный месяц</div>
                                                <div><?= $currName ?></div>
                                                <div><?= (int) $currCount; ?> шт</div>
                                                <div><?= ($p !== null && $p > 0 ? '+' : '') . $p; ?> %</div>
                                                <div>ПО КОМПАНИИ</div>
                                                <div><?= $diffCount ?></div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="param-label">
                                <div class="chart-collapse" data-target="#paid-chart-<?=$model->employee_id?>">+</div>
                                Оплата <i class="fa fa-rub"></i>
                            </td>
                            <td class="value-cell"><?= number_format($currPaid / 100, 2, ',', ' ') ?></td>
                            <td class="value-cell"><?= number_format($prevPaid / 100, 2, ',', ' ') ?></td>
                            <td class="value-cell">
                                <?= (($p = ($prevPaid > 0 ?
                                    round(($currPaid - $prevPaid) * 100 / $prevPaid, 2) :
                                    null)) !== null && $p > 0 ? '+' : '') . $p; ?>
                            </td>
                            <td class="value-cell"><?= number_format($averagePaid / 100, 2, ',', ' ') ?></td>
                            <td class=""></td>
                        </tr>
                        <tr class="param-chart">
                            <td colspan="6">
                                <div id="paid-chart-<?=$model->employee_id?>" style="height: 0; overflow: hidden;">
                                    <?php
                                    $data = [];
                                    foreach (array_values($model->getReportData('paid')) as $value) {
                                        $data[] = $value / 100;
                                    }
                                    ?>
                                    <table>
                                        <tr>
                                            <td class="param-chart-cell">
                                                <?= $this->render('index/_item_chart', [
                                                    'container' => 'paid-chart-box-' . $model->employee_id,
                                                    'categories' => $categories,
                                                    'series' => [['name' => 'Оплата', 'data' => $data, 'color' => '#57b8ae']],
                                                    'format' => '<b>{point.y:,.2f} <i class="fa fa-rub"></i></b>',
                                                    'yTitle' => 'Оплата <i class="fa fa-rub"></i>',
                                                ]); ?>
                                            </td>
                                            <td class="param-total-cell">
                                                <div>Отчетный месяц</div>
                                                <div><?= $currName ?></div>
                                                <div>
                                                    <?= number_format($currPaid / 100, 2, ',', ' ') ?>
                                                    <i class="fa fa-rub"></i>
                                                </div>
                                                <div><?= ($p !== null && $p > 0 ? '+' : '') . $p; ?> %</div>
                                                <div>ПО КОМПАНИИ</div>
                                                <div><?= $diffPaid ?></div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="param-label">
                                <div class="chart-collapse" data-target="#debt-chart-<?=$model->employee_id?>">+</div>
                                Долг <i class="fa fa-rub"></i>
                            </td>
                            <td class="value-cell"><?= number_format($currDebt / 100, 2, ',', ' ') ?></td>
                            <td class="value-cell"><?= number_format($prevDebt / 100, 2, ',', ' ') ?></td>
                            <td class="value-cell">
                                <?= (($p = ($prevDebt > 0 ?
                                    round(($currDebt - $prevDebt) * 100 / $prevDebt, 2) :
                                    null)) !== null && $p > 0 ? '+' : '') . $p; ?>
                            </td>
                            <td class="value-cell"><?= number_format($averageDebt / 100, 2, ',', ' ') ?></td>
                            <td class=""></td>
                        </tr>
                        <tr class="param-chart">
                            <td colspan="6">
                                <div id="debt-chart-<?=$model->employee_id?>" style="height: 0; overflow: hidden;">
                                    <?php
                                    $data = [];
                                    foreach (array_values($model->getReportData('debt')) as $value) {
                                        $data[] = $value / 100;
                                    }
                                    ?>
                                    <table>
                                        <tr>
                                            <td class="param-chart-cell">
                                                <?= $this->render('index/_item_chart', [
                                                    'container' => 'debt-chart-box-' . $model->employee_id,
                                                    'categories' => $categories,
                                                    'series' => [['name' => 'Долг', 'data' => $data, 'color' => '#f3565d']],
                                                    'format' => '<b>{point.y:,.2f} <i class="fa fa-rub"></i></b>',
                                                    'yTitle' => 'Долг <i class="fa fa-rub"></i>',
                                                ]); ?>
                                            </td>
                                            <td class="param-total-cell">
                                                <div>Отчетный месяц</div>
                                                <div><?= $currName ?></div>
                                                <div>
                                                    <?= number_format($currDebt / 100, 2, ',', ' ') ?>
                                                    <i class="fa fa-rub"></i>
                                                </div>
                                                <div><?= ($p !== null && $p > 0 ? '+' : '') . $p; ?> %</div>
                                                <div>ПО КОМПАНИИ</div>
                                                <div><?= $diffDebt ?></div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr class="">
                            <td colspan="6">
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php
$this->registerJs("
    $(document).on('click', '.chart-collapse', function() {
        var target = $(this).data('target');
        if ($(target).height() == 0) {
            $(target).animate({'height': $(target).get(0).scrollHeight}, 300);
            $(this).closest('tr').css({'border-top-width': '1px'});
            $(target).closest('tr').css({'border-bottom-width': '1px'});
            $(this).html('-');
        } else {
            $(target).animate({'height': 0}, 300);
            $(this).closest('tr').css({'border-top-width': 0});
            $(target).closest('tr').css({'border-bottom-width': 0});
            $(this).html('+');
        }
    });
");
?>