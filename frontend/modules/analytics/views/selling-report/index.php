<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.11.2017
 * Time: 9:32
 */

use frontend\modules\analytics\models\SellingReportSearch;
use yii\bootstrap\Html;
use yii\bootstrap\Dropdown;
use yii\helpers\Json;
use miloschuman\highcharts\HighchartsAsset;
use common\components\TextHelper;
use yii\widgets\Pjax;

/* @var \yii\web\View $this */
/* @var SellingReportSearch $searchModel */
/* @var $chartsOptions [] */
/* @var $tableData [] */
/* @var $totalDateData [] */

$this->title = 'Продажи по выставленным счетам';

$subPeriodColumns = $searchModel->getSubPeriodColumns();
$highchartsOptions = Json::encode($chartsOptions);

HighchartsAsset::register($this)->withScripts([
    'modules/exporting',
    'themes/grid-light',
]);
?>

<style type="text/css">
#selling-report-table tr th {
    vertical-align: middle;
}
#selling-report-table tr td {
    padding: 5px;
}
#selling-report-table tr.total-row td {
    font-weight: bold;
}
#selling-report-table tr > .count,
#selling-report-table tr > .sum,
#selling-report-table tr > .percent {
    padding: 5px;
    padding-right: 0;
    text-align: right;
    font-size: 13px;
    line-height: 15px;
    font-weight: normal;
}
#selling-report-table .count,
#selling-report-table .sum {
    border-right-width: 0;
}
#selling-report-table tr > th.percent {
    text-align: left;
    width: 2%;
}
#selling-report-table tr > td.percent {
    padding-left: 0;
}
#selling-report-table td > div.percent {
    position: relative;
    width: 52px;
    margin: 0;
    margin-left: -2px;
    padding-right: 10px;
    text-align: right;
}
#selling-report-table .percent i {
    position: absolute;
    top: 0;
    right: 2px;
}
#selling-report-table .sum * {
    background-color: transparent;
}
#selling-report-table .sum table {
    width: auto;
    margin-right: 0px;
    margin-left: auto;
}
#selling-report-table .sum table td {
    padding: 0 5px;
    border-width: 0;
    border-right: 1px solid #333333;
}
</style>
<?php Pjax::begin([
    'id' => 'selling-pjax',
    'linkSelector' => '.selling-pjax-link',
    'timeout' => 10000,
]); ?>
    <div class="row" style="margin-bottom: 25px;">
        <div class="col-sm-3 col-sm-offset-9">
            <div style="position: relative;">
                <div class="btn default p-t-7 p-b-7" data-toggle="dropdown" style="width: 100%">
                    <?= $searchModel->getPeriodName() ?> <b class="fa fa-angle-down"></b>
                </div>
                <?= Dropdown::widget([
                    'items' => $searchModel->getPeriodItems(),
                ]); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div id="high-charts-container" style="min-height: 400px;"></div>
    </div>

    <div class="portlet box darkblue blk_wth_srch">
        <div class="portlet-title row-fluid">
            <div class="caption list_recip col-md-6">
                <?= $this->title ?>
            </div>
            <div class="pull-right list_recip">
                <?= Html::radioList('type', $searchModel->type, [
                    null => 'Все',
                    SellingReportSearch::TYPE_PAID => 'Оплаченные',
                    SellingReportSearch::TYPE_NOT_PAID => 'Неоплаченные',
                ], [
                    'item' => function ($index, $label, $name, $checked, $value) use ($searchModel) {
                        return Html::a(
                            Html::radio($name, $checked, [
                                'value' => $value,
                                'label' => $label,
                                'labelOptions' => [
                                    'class' => 'radio-inline',
                                ],
                            ]),
                            [
                                'index', 'period' => $searchModel->period, 'type' => $value,
                            ],
                            [
                                'class' => 'selling-pjax-link',
                                'style' => 'text-underline: none; color: #fff; vertical-align: baseline;',
                            ]
                        );
                    },
                ]); ?>
            </div>
        </div>
        <div class="portlet-body accounts-list selling-report-list">
            <div class="table-container">
                <div class="dataTables_wrapper dataTables_extended_wrapper">
                    <table id="selling-report-table"
                        class="table report-table table-bordered table-hover dataTable status_nowrap invoice-table">
                        <thead>
                            <tr>
                                <?php foreach ($searchModel->tableColumns as $column) : ?>
                                    <?php $align = $column['attribute'] == 'expenses' ? '' : ' text-align: center;'; ?>
                                    <?= Html::tag('th', $column['label'], [
                                        'style' => 'padding: 5px 5px 0;' . $align,
                                        'colspan' => $column['attribute'] == 'expenses' ? null : 2,
                                        'rowspan' => $column['attribute'] == 'expenses' ? 2 : null,
                                    ]); ?>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <?php foreach ($searchModel->tableColumns as $column) : ?>
                                    <?php if ($column['attribute'] != 'expenses') : ?>
                                        <th class="sum">
                                            <table>
                                                <tr>
                                                    <td>Кол-во</td>
                                                    <td>Сумма</td>
                                                </tr>
                                            </table>
                                        </th>
                                        <th class="percent">%%</th>
                                    <?php endif ?>
                                <?php endforeach; ?>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($tableData as $oneLine): ?>
                            <tr data-id="<?= $oneLine['product_id']; ?>">
                                <td>
                                    <?= $oneLine['product_title']; ?>
                                </td>
                                <?php foreach ($subPeriodColumns as $period): ?>
                                    <?php if (isset($oneLine['data'][$period['attribute']])) : ?>
                                        <?php
                                        $count = $oneLine['data'][$period['attribute']]['count'];
                                        $amount = $oneLine['data'][$period['attribute']]['amount'];
                                        $percent = $oneLine['data'][$period['attribute']]['percent'];
                                        $arrow_status = $oneLine['data'][$period['attribute']]['arrow_status'];
                                        ?>
                                        <td class="sum">
                                            <table>
                                                <tr>
                                                    <td><?= $count; ?></td>
                                                    <td><?= number_format($amount/100, 2, ',', '&nbsp;') ?></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="percent">
                                            <div class="percent">
                                                <?= $percent ?>
                                                <?php if ($arrow_status !== null) : ?>
                                                    <?php if ($arrow_status) : ?>
                                                        <i class="fa fa-caret-up"></i>
                                                    <?php else : ?>
                                                        <i class="fa fa-caret-down"></i>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </div>
                                        </td>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <td class="sum">
                                    <table>
                                        <tr>
                                            <td><?= $oneLine['totalCount']; ?></td>
                                            <td><?= number_format($oneLine['totalAmount']/100, 2, ',', '&nbsp;') ?></td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="percent">
                                    <div class="percent">
                                        <?php $total = $totalDateData['totalAmount'] ? $totalDateData['totalAmount'] : 1; ?>
                                        <?= round($oneLine['totalAmount'] / $total * 100, 2); ?>
                                        <?php if ($oneLine['arrow_status'] !== null) : ?>
                                            <?php if ($oneLine['arrow_status']) : ?>
                                                <i class="fa fa-caret-up"></i>
                                            <?php else : ?>
                                                <i class="fa fa-caret-down"></i>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        <tr class="total-row">
                            <td>Итого</td>
                            <?php foreach ($subPeriodColumns as $period): ?>
                                <?php if (isset($totalDateData[$period['attribute']])): ?>
                                    <td class="sum">
                                        <table>
                                            <tr>
                                                <td><?= $totalDateData[$period['attribute']]['totalCount'] ?></td>
                                                <td><?= number_format($totalDateData[$period['attribute']]['totalAmount']/100, 2, ',', '&nbsp;') ?></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="percent">
                                        <div class="percent">
                                            100,00
                                        </div>
                                    </td>
                                <?php else: ?>
                                    <td class="sum">
                                        <table>
                                            <tr>
                                                <td>0</td>
                                                <td>0,00</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="percent"><div class="percent">0,00</div></td>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <?php if (!empty($totalDateData)): ?>
                                <td class="sum">
                                    <table>
                                        <tr>
                                            <td><?= $totalDateData['totalCount'] ?></td>
                                            <td><?= number_format($totalDateData['totalAmount']/100, 2, ',', '&nbsp;') ?></td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="percent">
                                    <div class="percent">
                                        100,00
                                    </div>
                                </td>
                            <?php else: ?>
                                <td class="sum">
                                    <table>
                                        <tr>
                                            <td>0</td>
                                            <td>0,00</td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="percent"><div class="percent">0,00</div></td>
                            <?php endif; ?>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        window.highchartsOptions = <?= $highchartsOptions ?>;
    </script>
<?php Pjax::end(); ?>
<?php
$js = <<<JS
function createHighcharts() {
    Highcharts.chart('high-charts-container', window.highchartsOptions);
}

$(document).on("pjax:complete", function() {
    createHighcharts();
});

createHighcharts();
JS;

$this->registerJs($js);

