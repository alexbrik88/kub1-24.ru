<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.04.2018
 * Time: 18:46
 */

use yii\helpers\Html;
use frontend\modules\analytics\models\ProfitSearch;
use kartik\checkbox\CheckboxX;
use common\components\TextHelper;

/* @var $this yii\web\View
 * @var $searchModel ProfitSearch
 * @var $receiptData []
 * @var $priceData []
 * @var $consumptionData []
 * @var $grossProfit []
 * @var $profitOrLesion []
 * @var $tax []
 * @var $netProfit []
 */

$this->title = 'Отчет о прибылях и убытках';
$currentMonthNumber = date('n');
$currentQuarter = (int)ceil(date('m') / 3);
$isCurrentYear = date('Y') == $searchModel->year;
$isAccrualMethod = $searchModel->type == ProfitSearch::TYPE_ACT_AND_PACKING_LIST;
$isCashMethod = $searchModel->type == ProfitSearch::TYPE_INVOICE;
?>
<div class="portlet box">
    <div class="btn-group pull-right title-buttons">
    </div>
    <h3 class="page-title"><?= $this->title; ?></h3>
</div>
<div class="portlet box darkblue">
    <div class="portlet-title float-left">
        <div class="caption">
        </div>
    </div>
    <?= Html::beginForm(['index'], 'GET', [
        'validateOnChange' => true,
    ]); ?>
    <div class="search-form-default">
        <div class="col-md-9 pull-right serveces-search"
             style="max-width: 595px;">
            <div class="input-group" style="display: block;">
                <div class="input-cont">
                    <div class="col-md-5"></div>
                    <div class="col-md-4 col-xs-6 " style="padding-right: 0; padding-left: 0">
                    </div>
                    <div class="col-md-3 col-xs-6" style="padding-right: 0;">
                        <?= Html::activeDropDownList($searchModel, 'year', $searchModel->getYearFilter(), [
                            'class' => 'form-control',
                            'style' => 'display: inline-block;',
                            'value' => $searchModel->year ? $searchModel->year : date('Y'),
                        ]); ?>
                        <?= Html::activeHiddenInput($searchModel, 'type'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>
    <div class="col-md-12">
        <span class="btn-group" style="position: absolute; right: 0; bottom: 50px;">
            <?= Html::a('Метод начисления', ['index',
                'ProfitSearch' => [
                    'type' => ProfitSearch::TYPE_ACT_AND_PACKING_LIST,
                    'year' => $searchModel->year
                ],
            ], [
                'class' => 'btn btn-default profit' . ($isAccrualMethod ? ' active' : ''),
                'style' => 'width: 185px;',
            ]); ?>
            <?= Html::a('Кассовый метод', ['index',
                'ProfitSearch' => [
                    'type' => ProfitSearch::TYPE_INVOICE,
                    'year' => $searchModel->year
                ],
            ], [
                'class' => 'btn btn-default profit' . ($isCashMethod ? ' active' : ''),
                'style' => 'width: 185px;',
            ]); ?>
        </span>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <div class="dataTables_wrapper dataTables_extended_wrapper no-footer scroll-table"
                 id="datatable_ajax_wrapper">
                <div class="scroll-table-wrapper">
                    <table class="table table-striped table-bordered table-hover profit-report"
                           style="width: 98%; margin: 0 auto 20px;">
                        <thead class="not-fixed">
                        <tr class="heading quarters-profit-report" role="row">
                            <th width="20%"></th>
                            <th colspan="<?= $currentMonthNumber < 4 && $isCurrentYear ? 3 : 1; ?>"
                                class="text-left quarter-th"
                                style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber < 4 && $isCurrentYear ? '180px' : '117px'; ?>;">
                                <?= CheckboxX::widget([
                                    'id' => 'first-quarter',
                                    'name' => 'quarter-month-1',
                                    'value' => !($currentQuarter == 1 && $isCurrentYear),
                                    'options' => [
                                        'class' => 'quarter-checkbox',
                                        'data' => [
                                            'month-class' => 'quarter-month-1',
                                            'total-quarter-class' => 'quarter-1',
                                        ],
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]); ?>
                                <label for="first-quarter">
                                    1 <?= $currentMonthNumber < 4 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $searchModel->year; ?>
                                </label>
                            </th>
                            <th colspan="<?= $currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear ? 3 : 1; ?>"
                                class="text-left quarter-th"
                                style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear ? '180px' : '117px'; ?>;">
                                <?= CheckboxX::widget([
                                    'id' => 'second-quarter',
                                    'name' => 'quarter-month-1',
                                    'value' => !($currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear),
                                    'options' => [
                                        'class' => 'quarter-checkbox',
                                        'data' => [
                                            'month-class' => 'quarter-month-2',
                                            'total-quarter-class' => 'quarter-2',
                                        ],
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]); ?>
                                <label for="second-quarter">
                                    2 <?= $currentMonthNumber > 3 && $currentMonthNumber < 7 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $searchModel->year; ?>
                                </label>
                            </th>
                            <th colspan="<?= $currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear ? 3 : 1; ?>"
                                class="text-left quarter-th"
                                style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear ? '180px' : '117px'; ?>;">
                                <?= CheckboxX::widget([
                                    'id' => 'third-quarter',
                                    'name' => 'quarter-month-1',
                                    'value' => !($currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear),
                                    'options' => [
                                        'class' => 'quarter-checkbox',
                                        'data' => [
                                            'month-class' => 'quarter-month-3',
                                            'total-quarter-class' => 'quarter-3',
                                        ],
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]); ?>
                                <label for="third-quarter">
                                    3 <?= $currentMonthNumber > 6 && $currentMonthNumber < 10 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $searchModel->year; ?>
                                </label>
                            </th>
                            <th colspan="<?= $currentMonthNumber > 9 && $isCurrentYear ? 3 : 1; ?>"
                                class="text-left quarter-th"
                                style="border-bottom: 1px solid #ddd;min-width: <?= $currentMonthNumber > 9 && $isCurrentYear ? '180px' : '117px'; ?>;">
                                <?= CheckboxX::widget([
                                    'id' => 'fourth-quarter',
                                    'name' => 'quarter-month-1',
                                    'value' => !($currentMonthNumber > 9 && $isCurrentYear),
                                    'options' => [
                                        'class' => 'quarter-checkbox',
                                        'data' => [
                                            'month-class' => 'quarter-month-4',
                                            'total-quarter-class' => 'quarter-4',
                                        ],
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]); ?>
                                <label for="fourth-quarter">
                                    4 <?= $currentMonthNumber > 9 && $isCurrentYear ? 'квартал' : 'кв.'; ?> <?= $searchModel->year; ?>
                                </label>
                            </th>
                            <th width="10%" class="text-center" style="border-right: 1px solid #ddd;">
                                <?= $searchModel->year; ?>
                            </th>
                        </tr>
                        <tr class="heading" role="row">
                            <th width="20%">Статьи</th>
                            <?php foreach (ProfitSearch::$month as $key => $month): ?>
                                <?php $quarter = ceil($key / 3); ?>
                                <th class="quarter-month-<?= $quarter; ?> text-left" width="10%"
                                    style="min-width: 117px;display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                    <?= $month; ?>
                                </th>
                                <?php if ($key % 3 == 0): ?>
                                    <th class="text-left quarter-<?= $quarter; ?>" width="10%"
                                        style="min-width: 117px;display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                        Итого
                                    </th>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <th class="text-center" width="10%">Итого</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="odd bold">
                                Выручка
                            </td>
                            <?php $key = $quarterSum = 0; ?>
                            <?php foreach (ProfitSearch::$month as $monthNumber => $monthText): ?>
                                <?php $key++;
                                $quarter = ceil($key / 3); ?>
                                <td class="odd text-right bold quarter-month-<?= ceil($key / 3); ?>" role="row"
                                    style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                    <?= TextHelper::invoiceMoneyFormat($receiptData[$monthNumber], 2); ?>
                                </td>
                                <?php $quarterSum += $receiptData[$monthNumber]; ?>
                                <?php if ($key % 3 == 0): ?>
                                    <td class="odd text-right bold quarter-<?= $key / 3; ?>" role="row"
                                        style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                        <?= TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                        $quarterSum = 0; ?>
                                    </td>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <td class="bold odd text-right" role="row">
                                <?= TextHelper::invoiceMoneyFormat($receiptData['totalAmount'], 2); ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="odd bold">
                                Себестоимость
                            </td>
                            <?php $key = $quarterSum = 0; ?>
                            <?php foreach (ProfitSearch::$month as $monthNumber => $monthText): ?>
                                <?php $key++;
                                $quarter = ceil($key / 3); ?>
                                <td class="odd text-right bold quarter-month-<?= ceil($key / 3); ?>" role="row"
                                    style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                    <?= TextHelper::invoiceMoneyFormat($priceData[$monthNumber], 2); ?>
                                </td>
                                <?php $quarterSum += $priceData[$monthNumber]; ?>
                                <?php if ($key % 3 == 0): ?>
                                    <td class="odd text-right bold quarter-<?= $key / 3; ?>" role="row"
                                        style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                        <?= TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                        $quarterSum = 0; ?>
                                    </td>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <td class="bold odd text-right" role="row">
                                <?= TextHelper::invoiceMoneyFormat($priceData['totalAmount'], 2); ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="odd bold">
                                Валовая прибыль
                            </td>
                            <?php $key = $quarterSum = 0; ?>
                            <?php foreach (ProfitSearch::$month as $monthNumber => $monthText): ?>
                                <?php $key++;
                                $quarter = ceil($key / 3); ?>
                                <td class="odd text-right bold quarter-month-<?= ceil($key / 3); ?>" role="row"
                                    style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                    <?= TextHelper::invoiceMoneyFormat($grossProfit[$monthNumber], 2); ?>
                                </td>
                                <?php $quarterSum += ($grossProfit[$monthNumber]); ?>
                                <?php if ($key % 3 == 0): ?>
                                    <td class="odd text-right bold quarter-<?= $key / 3; ?>" role="row"
                                        style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                        <?= TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                        $quarterSum = 0; ?>
                                    </td>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <td class="bold odd text-right" role="row">
                                <?= TextHelper::invoiceMoneyFormat($grossProfit['totalAmount'], 2); ?>
                            </td>
                        </tr>
                        <tr class="consumption-total-row">
                            <td role="row" class="checkbox-td">
                                <?= CheckboxX::widget([
                                    'id' => 'profit-consumption-type',
                                    'name' => 'profit-consumption-type',
                                    'value' => true,
                                    'options' => [
                                        'class' => 'profit-consumption-type',
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]); ?>
                                <label for="profit-consumption-type">
                                    Расходы
                                </label>
                            </td>
                            <?php $key = $quarterSum = 0; ?>
                            <?php foreach (ProfitSearch::$month as $monthNumber => $monthText): ?>
                                <?php $key++;
                                $quarter = ceil($key / 3); ?>
                                <td class="odd text-right bold quarter-month-<?= ceil($key / 3); ?>" role="row"
                                    style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                    <?= TextHelper::invoiceMoneyFormat($consumptionData['total'][$monthNumber], 2); ?>
                                </td>
                                <?php $quarterSum += ($consumptionData['total'][$monthNumber]); ?>
                                <?php if ($key % 3 == 0): ?>
                                    <td class="odd text-right bold quarter-<?= $key / 3; ?>" role="row"
                                        style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                        <?= TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                        $quarterSum = 0; ?>
                                    </td>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <td class="bold odd text-right" role="row">
                                <?= TextHelper::invoiceMoneyFormat($consumptionData['total']['totalAmount'], 2); ?>
                            </td>
                        </tr>
                        <?php foreach ($consumptionData['items'] as $consumptionItem): ?>
                            <tr class="hidden profit-consumption-type-hidden">
                                <td style="padding-left: 40px;">
                                    <?= $consumptionItem['name']; ?>
                                </td>
                                <?php $key = $quarterSum = 0; ?>
                                <?php foreach (ProfitSearch::$month as $monthNumber => $monthText): ?>
                                    <?php $key++;
                                    $quarter = ceil($key / 3); ?>
                                    <td class="odd text-right quarter-month-<?= ceil($key / 3); ?>" role="row"
                                        style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                        <?= TextHelper::invoiceMoneyFormat($consumptionItem[$monthNumber], 2); ?>
                                    </td>
                                    <?php $quarterSum += ($consumptionItem[$monthNumber]); ?>
                                    <?php if ($key % 3 == 0): ?>
                                        <td class="odd text-right quarter-<?= $key / 3; ?>" role="row"
                                            style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                            <?= TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                            $quarterSum = 0; ?>
                                        </td>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <td class="odd text-right" role="row">
                                    <?= TextHelper::invoiceMoneyFormat($consumptionItem['totalAmount'], 2); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        <tr>
                            <td class="odd bold">
                                Прибыль (Убыток)
                            </td>
                            <?php $key = $quarterSum = 0; ?>
                            <?php foreach (ProfitSearch::$month as $monthNumber => $monthText): ?>
                                <?php $key++;
                                $quarter = ceil($key / 3); ?>
                                <td class="odd text-right bold quarter-month-<?= ceil($key / 3); ?>" role="row"
                                    style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                    <?= TextHelper::invoiceMoneyFormat($profitOrLesion[$monthNumber], 2); ?>
                                </td>
                                <?php $quarterSum += ($profitOrLesion[$monthNumber]); ?>
                                <?php if ($key % 3 == 0): ?>
                                    <td class="odd text-right bold quarter-<?= $key / 3; ?>" role="row"
                                        style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                        <?= TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                        $quarterSum = 0; ?>
                                    </td>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <td class="bold odd text-right" role="row">
                                <?= TextHelper::invoiceMoneyFormat($profitOrLesion['totalAmount'], 2); ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="odd bold">
                                Налог
                            </td>
                            <?php $key = $quarterSum = 0; ?>
                            <?php foreach (ProfitSearch::$month as $monthNumber => $monthText): ?>
                                <?php $key++;
                                $quarter = ceil($key / 3); ?>
                                <td class="odd text-right bold quarter-month-<?= ceil($key / 3); ?>" role="row"
                                    style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                    <?= TextHelper::invoiceMoneyFormat($tax[$monthNumber], 2); ?>
                                </td>
                                <?php $quarterSum += ($tax[$monthNumber]); ?>
                                <?php if ($key % 3 == 0): ?>
                                    <td class="odd text-right bold quarter-<?= $key / 3; ?>" role="row"
                                        style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                        <?= TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                        $quarterSum = 0; ?>
                                    </td>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <td class="bold odd text-right" role="row">
                                <?= TextHelper::invoiceMoneyFormat($tax['totalAmount'], 2); ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="odd bold">
                                Чистая прибыль
                            </td>
                            <?php $key = $quarterSum = 0; ?>
                            <?php foreach (ProfitSearch::$month as $monthNumber => $monthText): ?>
                                <?php $key++;
                                $quarter = ceil($key / 3); ?>
                                <td class="odd text-right bold quarter-month-<?= ceil($key / 3); ?>" role="row"
                                    style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? null : 'none'; ?>;">
                                    <?= TextHelper::invoiceMoneyFormat($netProfit[$monthNumber], 2); ?>
                                </td>
                                <?php $quarterSum += ($netProfit[$monthNumber]); ?>
                                <?php if ($key % 3 == 0): ?>
                                    <td class="odd text-right bold quarter-<?= $key / 3; ?>" role="row"
                                        style="display: <?= $currentQuarter == $quarter && $isCurrentYear ? 'none' : null; ?>;">
                                        <?= TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                        $quarterSum = 0; ?>
                                    </td>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <td class="bold odd text-right" role="row">
                                <?= TextHelper::invoiceMoneyFormat($netProfit['totalAmount'], 2); ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
