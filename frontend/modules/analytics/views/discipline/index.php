<?php

use common\assets\MorrisChartAsset;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\GridView;
use frontend\modules\analytics\models\AnalysisSearch;
use frontend\modules\analytics\models\DisciplineSearch;
use frontend\widgets\RangeButtonWidget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use yii\bootstrap\Dropdown;

/* @var $this \yii\web\View */
/* @var $searchModel DisciplineSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */

MorrisChartAsset::register($this);

$this->title = 'Платежная дисциплина';

$monthColumns = [];
foreach ($searchModel->monthArray as $month) {
    $monthColumns[] = [
        'attribute' => "m{$month}",
        'headerOptions' => [
            'rowspan' => 1,
            'colspan' => 1,
            'style' => 'border-right-width: 1px;'
        ],
        'contentOptions' => [
            'class' => 'text-right',
        ],
        'value' => function ($data) use ($month) {
            $attr = "m{$month}";

            return round(max($data->$attr, 0));
        },
    ];
}

$this->registerJs('
    $(document).on("click", ".overal-result-table .result-row", function() {
        $("#discipline-grid-filters select, #discipline-grid-filters input").val("");
        $("#disciplinesearch-group").val($(this).data("group"));
        $("#discipline-grid").yiiGridView("applyFilter");
    });
');

$seriesData = [];
foreach ($searchModel->overallResult as $key => $row) {
    if ($row['contractor_part'] > 0) {
        $item = [
            'label' => $searchModel->getGroupLabel($row['group']),
            'color' => $searchModel->getGroupColor($row['group']),
            'value' => round($row['contractor_part'], 2),
        ];
        array_unshift($seriesData, $item);
    }
}
?>

<style type="text/css">
.overal-result-table .result-row {
    cursor: pointer;
}
.overal-result-table .result-row:hover td {
    background-color: #f1f1f1;
}
</style>

<?php $pjax = Pjax::begin([
    'id' => 'discipline-pjax',
]); ?>

<div class="row">
    <div class="col-md-3 col-md-push-9">
        <div class="row">
            <div class="col-sm-12">
                <div style="position: relative;">
                    <div class="btn default p-t-7 p-b-7"  data-toggle="dropdown" style="width: 100%">
                        <?= $searchModel->periodName ?> <b class="fa fa-angle-down"></b>
                    </div>
                    <?= Dropdown::widget([
                        'items' => $searchModel->periodItems,
                    ]); ?>
                </div>
            </div>
        </div>
        <?php if ($seriesData) : ?>
            <div class="row">
                <div class="col-sm-12">
                    <div id="morris_chart_donut" style="width:100%; margin: 0 auto; max-width: 180px; height: 180px;"></div>
                </div>
            </div>
        <?php endif ?>
    </div>
    <div class="col-md-9 col-md-pull-3 created-scroll">
        <table class="table table-bordered table-hover overal-result-table" style="width: auto;">
            <tr class="heading">
                <th>Группа</th>
                <th style="text-align: center;">Кол-во покупателей</th>
                <th style="text-align: center;">% покупателей</th>
                <th style="text-align: center;">Среднее кол-во дней на оплату</th>
            </tr>
            <?php foreach ($searchModel->overallResult as $row) : ?>
                <tr class="result-row" data-group="<?= $row['group'] ?>">
                    <td class="text-bold" style="color: <?= $searchModel->getGroupColor($row['group']); ?>;">
                        <?= $searchModel->getGroupLabel($row['group']); ?>
                    </td>
                    <td class="text-right"><?= $row['contractor_count'] ?></td>
                    <td class="text-right text-bold" style="color: <?= $searchModel->getGroupColor($row['group']); ?>;">
                        <?= round($row['contractor_part'], 2); ?>
                    </td>
                    <td class="text-right"><?= round($row['days_count']) ?></td>
                </tr>
            <?php endforeach ?>
            <tr class="result-row text-bold" data-group="">
                <td>Итого</td>
                <td class="text-right"><?= $searchModel->tmpRowCount ?></td>
                <td class="text-right">100</td>
                <td class="text-right"><?= $searchModel->averageDays ?></td>
            </tr>
        </table>
    </div>
</div>

<div class="portlet box darkblue blk_wth_srch">
    <div class="portlet-title row-fluid">
        <div class="caption list_recip col-sm-3">
            Покупатели: <?= $dataProvider->totalCount ?>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-container" style="">
            <?= GridView::widget([
                'id' => 'discipline-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable documents_table fix-thead',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper bank-scroll-table',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'columns' => array_merge(
                    [
                        [
                            'class' => DropDownSearchDataColumn::className(),
                            'attribute' => 'id',
                            'headerOptions' => [
                                'rowspan' => 2,
                                'colspan' => 1,
                                //'width' => '12%',
                                'class' => 'nowrap-normal max10list',
                            ],
                            'format' => 'html',
                            'filter' => $searchModel->getContractorFilterItems(),
                            'value' => function ($data) {
                                return Html::a($data->nameWithType, [
                                    '/contractor/view',
                                    'type' => $data->type,
                                    'id' => $data->id,
                                ]);
                            },
                        ],
                        [
                            'attribute' => 'delay',
                            'headerOptions' => [
                                'rowspan' => 2,
                                'colspan' => 1,
                            ],
                            'contentOptions' => [
                                'class' => 'text-right',
                            ],
                        ],
                        [
                            'label' => 'Просрочка (дн.)',
                            'headerOptions' => [
                                'type' => 'header',
                                'class' => 'text-center',
                                'rowspan' => 1,
                                'colspan' => count($searchModel->monthArray),
                                'style' => 'border-bottom: 1px solid #ddd;',
                            ],
                            'contentOptions' => [
                                'class' => 'text-right',
                            ],
                        ],
                    ],
                    $monthColumns,
                    [
                        [
                            'attribute' => 'days',
                            'headerOptions' => [
                                'rowspan' => 2,
                                'colspan' => 1,
                            ],
                            'contentOptions' => [
                                'class' => 'text-right',
                            ],
                            'value' => function ($data) {
                                return round(max($data->days, 0));
                            },
                        ],
                        [
                            'attribute' => 'overdue',
                            'headerOptions' => [
                                'rowspan' => 2,
                                'colspan' => 1,
                            ],
                            'contentOptions' => [
                                'class' => 'text-right',
                            ],
                            'value' => function ($data) {
                                return round(max($data->overdue, 0));
                            },
                        ],
                        [
                            //'class' => DropDownDataColumn::className(),
                            'attribute' => 'group',
                            'headerOptions' => [
                                'rowspan' => 2,
                                'colspan' => 1,
                            ],
                            'contentOptions' => [
                                'class' => 'text-right',
                            ],
                            'filter' => ['' => 'Все'] + DisciplineSearch::$groups,
                            'value' => 'groupLabel',
                        ],
                        [
                            //'class' => DropDownDataColumn::className(),
                            'attribute' => 'abc',
                            'headerOptions' => [
                                'rowspan' => 2,
                                'colspan' => 1,
                            ],
                            'contentOptions' => [
                                'class' => 'text-right',
                            ],
                            'filter' => ['' => 'Все'] + AnalysisSearch::$groups,
                            'value' => 'abcValue',
                        ],
                    ]
                ),
            ]); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    //$(".modal#modal-loader-items").modal();
    var morrisDonut = Morris.Donut({
        element: "morris_chart_donut",
        formatter: function (y, data) { return y + "%" },
        data: <?= Json::encode($seriesData) ?>
    });
    morrisDonut.select(0);
});
</script>

<?php $pjax->end(); ?>
