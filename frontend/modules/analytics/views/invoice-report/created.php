<?php
use common\components\date\DateHelper;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\EmployeeCompany;
use common\models\product\Product;
use frontend\components\StatisticPeriod;
use frontend\modules\documents\components\DocConverter;
use frontend\modules\documents\components\FilterHelper;
use frontend\rbac\permissions;
use PhpOffice\PhpWord\IOFactory;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\models\InvoiceReportSearch */

$this->title = 'Отчет по выставленным счетам';

$statisticItemsArray = $searchModel->createdStatistic();

$emploeeFilterItems = ArrayHelper::map($statisticItemsArray, 'id', function ($row) {
    return $row['lastname'] . ' ' . mb_substr($row['firstname'], 0, 1) . '.' . mb_substr($row['patronymic'], 0, 1) . '.';
});

$sumTotal = 0;
$sumPayed = 0;
$sumNopayed = 0;
?>

<div class="row">
    <div class="col-md-3 col-md-push-9">
        <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
    </div>
    <div class="col-md-9 col-md-pull-3 created-scroll">
        <table class="table table-striped table-bordered table-hover">
            <tr class="heading">
                <th>Сотрудник</th>
                <th>Выставлено</th>
                <th>Не оплачено</th>
                <th>Оплачено</th>
            </tr>
            <?php if ($statisticItemsArray) :
                foreach ($statisticItemsArray as $row) :
                    $sumTotal += $row['all_total_amount'];
                    $sumNopayed += $row['all_nopayed_amount'];
                    $sumPayed += $row['all_payed_amount'];
                    ?>
                    <tr>
                        <td><?= $row['lastname'] ?> <?= mb_substr($row['firstname'], 0, 1) ?>
                            .<?= mb_substr($row['patronymic'], 0, 1) ?>.
                        </td>
                        <td><?= TextHelper::invoiceMoneyFormat($row['all_total_amount'], 2) ?></td>
                        <td><?= TextHelper::invoiceMoneyFormat($row['all_nopayed_amount'], 2) ?></td>
                        <td><?= TextHelper::invoiceMoneyFormat($row['all_payed_amount'], 2) ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php else : ?>
                <tr>
                    <td>--/--</td>
                    <td>0,00</td>
                    <td>0,00</td>
                    <td>0,00</td>
                </tr>
            <?php endif; ?>
            <tr>
                <td><b>Итого</b></td>
                <td><b><?= TextHelper::invoiceMoneyFormat($sumTotal, 2) ?></b>
                </td>
                <td><b><?= TextHelper::invoiceMoneyFormat($sumNopayed, 2) ?></b>
                </td>
                <td><b><?= TextHelper::invoiceMoneyFormat($sumPayed, 2) ?></b>
                </td>
            </tr>
            <tr>
                <td>%%</td>
                <td></td>
                <td><?= TextHelper::invoiceMoneyFormat($sumTotal ? $sumNopayed / $sumTotal * 100 * 100 : 0, 2) ?>
                    %
                </td>
                <td><?= TextHelper::invoiceMoneyFormat($sumTotal ? $sumPayed / $sumTotal * 100 * 100 : 0, 2) ?>
                    %
                </td>
            </tr>
        </table>
    </div>
</div>

<div class="portlet box darkblue blk_wth_srch">
    <div class="portlet-title row-fluid">
        <div class="caption list_recip col-md-3 col-sm-3">
            Список счетов
        </div>
    </div>

    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable status_nowrap overfl_text_hid invoice-table',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'columns' => [
                    [
                        'attribute' => 'contractor_id',
                        'label' => 'Контрагент',
                        'class' => DropDownSearchDataColumn::className(),
                        'enableSorting' => false,
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '20%',
                        ],
                        'filter' => FilterHelper::getContractorList($searchModel->type, Invoice::tableName(), true, false, false),
                        'format' => 'raw',
                        'value' => 'contractor_name_short',
                    ],

                    [
                        'attribute' => 'document_date',
                        'label' => 'Дата счёта',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '12%',
                        ],
                        'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
                    ],
                    [
                        'attribute' => 'document_number',
                        'label' => '№ счёта',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '12%',
                        ],
                        'format' => 'raw',
                        'value' => function (Invoice $data) {
                            return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                                'model' => $data,
                            ])
                                ? Html::a($data->fullNumber, ['/documents/invoice/view',
                                    'type' => $data->type,
                                    'id' => $data->id,
                                    'contractorId' => $data->contractor_id,
                                ])
                                : $data->fullNumber;
                        },
                    ],
                    [
                        'label' => 'Сумма',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '12%',
                        ],
                        'attribute' => 'total_amount_with_nds',
                        'format' => 'raw',
                        'value' => function (Invoice $model) {
                            $amount = '<span>' . TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2) . '</span>';
                            if ($model->invoice_status_id == InvoiceStatus::STATUS_PAYED_PARTIAL) {
                                $amount .= ' / <span style="color: #45b6af;" title="Оплаченная сумма">' . TextHelper::invoiceMoneyFormat($model->payment_partial_amount, 2) . '</span>';
                            } elseif ($model->invoice_status_id == InvoiceStatus::STATUS_OVERDUE && $model->remaining_amount !== null) {
                                $amount .= ' / <span style="color: #f3565d;" title="Неоплаченная сумма">' . TextHelper::invoiceMoneyFormat($model->remaining_amount, 2) . '</span>';
                            }

                            return $amount;
                        },
                    ],
                    [
                        'label' => 'Дата оплаты',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '12%',
                        ],
                        'value' => function (Invoice $model) {
                            if ($model->invoice_status_id == InvoiceStatus::STATUS_PAYED) {
                                $dateRange = StatisticPeriod::getSessionPeriod();
                                $payDate = date_create(date('Y-m-d', $model->invoice_status_updated_at));
                                if ($payDate >= date_create($dateRange['from']) && $payDate <= date_create($dateRange['to'])) {
                                    return $model->invoice_status_updated_at;
                                }
                            }

                            return null;
                        },
                        'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
                    ],
                    [
                        'label' => 'Тип оплаты',
                        'class' => DropDownSearchDataColumn::className(),
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '12%',
                        ],
                        'attribute' => 'paymentType',
                        'filter' => $searchModel->paymentTypeItems,
                        'value' => function (Invoice $model) {
                            $type = $model->getPaymentType();
                            if (isset(Invoice::$invoicePaymentLabel[$type])) {
                                return Invoice::$invoicePaymentLabel[$type];
                            }

                            return 'Не оплачен';
                        },
                    ],
                    [
                        'label' => 'Выставил счет',
                        'class' => DropDownSearchDataColumn::className(),
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '20%',
                        ],
                        'attribute' => 'document_author_id',
                        'filter' => ['' => 'Все'] + $emploeeFilterItems,
                        'value' => function (Invoice $model) {
                            $author = EmployeeCompany::findOne([
                                'employee_id' => $model->document_author_id,
                                'company_id' => $model->company_id,
                            ]);
                            return $author ? $author->getFio(true) : '';
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
