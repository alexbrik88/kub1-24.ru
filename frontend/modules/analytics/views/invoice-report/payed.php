<?php
use common\components\date\DateHelper;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use common\models\product\Product;
use frontend\components\StatisticPeriod;
use frontend\modules\documents\components\DocConverter;
use frontend\modules\documents\components\FilterHelper;
use frontend\rbac\permissions;
use PhpOffice\PhpWord\IOFactory;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\document\status\InvoiceStatus;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\analytics\models\InvoiceReportSearch */

$this->title = 'Отчет по оплаченным счетам';

$statisticItemsArray = $searchModel->payedStatistic();

$userArray = [];
foreach ($statisticItemsArray as $row) {
    $user = EmployeeCompany::findOne([
        'employee_id' => $row['document_author_id'],
        'company_id' => $searchModel->company_id,
    ]) ? : Employee::findOne($row['document_author_id']);

    $userArray[$row['document_author_id']] = $user ? $user->getFio(true) : '';
}
asort($userArray);
$sumDebtStart = 0;
$sumCreated = 0;
$sumPayed = 0;
?>

<div class="row">
    <div class="col-md-3 col-md-push-9">
        <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
    </div>
    <div class="col-md-9 col-md-pull-3 created-scroll">
        <table class="table table-striped table-bordered table-hover">
            <tr class="heading">
                <th style="width: 20%;">Сотрудник</th>
                <th style="width: 18%;">Долг на начало периода</th>
                <th style="width: 18%;">Выставлено за период</th>
                <th style="width: 18%;">Оплачено за период</th>
                <th style="width: 8%;">%%</th>
            </tr>
            <?php if ($statisticItemsArray) :
                foreach ($statisticItemsArray as $row) {
                    $sumDebtStart += $row['all_debtStart_amount'];
                    $sumCreated += $row['all_created_amount'];
                    $sumPayed += $row['all_payed_amount'];
                }
                foreach ($statisticItemsArray as $row) :
                    $percent = $sumPayed ? $row['all_payed_amount'] / $sumPayed * 100 : 0;
                    ?>
                    <tr>
                        <td><?= $userArray[$row['document_author_id']] ?>.</td>
                        <td><?= TextHelper::invoiceMoneyFormat($row['all_debtStart_amount'], 2) ?></td>
                        <td><?= TextHelper::invoiceMoneyFormat($row['all_created_amount'], 2) ?></td>
                        <td><?= TextHelper::invoiceMoneyFormat($row['all_payed_amount'], 2) ?></td>
                        <td><?= TextHelper::invoiceMoneyFormat($percent * 100, 2) ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td><b>Итого</b></td>
                    <td>
                        <b><?= TextHelper::invoiceMoneyFormat($sumDebtStart, 2) ?></b>
                    </td>
                    <td>
                        <b><?= TextHelper::invoiceMoneyFormat($sumCreated, 2) ?></b>
                    </td>
                    <td>
                        <b><?= TextHelper::invoiceMoneyFormat($sumPayed, 2) ?></b>
                    </td>
                    <td><b>100</b></td>
                </tr>
            <?php else : ?>
                <tr>
                    <td>--/--</td>
                    <td>0,00</td>
                    <td>0,00</td>
                    <td>0,00</td>
                    <td>0,00</td>
                </tr>
            <?php endif; ?>
        </table>
    </div>
</div>

<div class="portlet box darkblue blk_wth_srch">
    <div class="portlet-title row-fluid">
        <div class="caption list_recip col-md-3 col-sm-3">
            Список счетов
        </div>
    </div>

    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable status_nowrap overfl_text_hid invoice-table',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => (Yii::$app->controller->id === 'default') ? "{items}\n{pager}" : $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'columns' => [
                    [
                        'attribute' => 'contractor_id',
                        'label' => 'Контрагент',
                        'class' => DropDownSearchDataColumn::className(),
                        'enableSorting' => false,
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '20%',
                        ],
                        'filter' => FilterHelper::getContractorList($searchModel->type, Invoice::tableName(), true, false, false),
                        'format' => 'raw',
                        'value' => 'contractor_name_short',
                    ],

                    [
                        'attribute' => 'document_date',
                        'label' => 'Дата счёта',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '12%',
                        ],
                        'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
                    ],
                    [
                        'attribute' => 'document_number',
                        'label' => '№ счёта',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '12%',
                        ],
                        'format' => 'raw',
                        'value' => function (Invoice $data) {
                            return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                                'model' => $data,
                            ])
                                ? Html::a($data->fullNumber, ['/documents/invoice/view',
                                    'type' => $data->type,
                                    'id' => $data->id,
                                    'contractorId' => $data->contractor_id,
                                ])
                                : $data->fullNumber;
                        },
                    ],
                    [
                        'label' => 'Сумма счета',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '12%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-right',
                        ],
                        'attribute' => 'total_amount_with_nds',
                        'format' => 'raw',
                        'value' => function (Invoice $model) {
                            return TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2);
                        },
                    ],
                    [
                        'label' => 'Оплачено',
                        'attribute' => 'flow_sum',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '12%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-right',
                        ],
                        'format' => 'raw',
                        'value' => function (Invoice $model) {
                            return TextHelper::invoiceMoneyFormat($model->flow_sum, 2);
                        },
                    ],
                    [
                        'attribute' => 'flow_date',
                        'label' => 'Дата оплаты',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '12%',
                        ],
                        'value' => 'flow_date',
                        'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
                    ],
                    [
                        'label' => 'Тип оплаты',
                        'attribute' => 'flow_way',
                        'class' => DropDownSearchDataColumn::className(),
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '12%',
                        ],
                        'filter' => ['' => 'Все'] + $searchModel->flowWays,
                        'value' => 'flowWay',
                    ],
                    [
                        'label' => 'Выставил счет',
                        'class' => DropDownSearchDataColumn::className(),
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '20%',
                        ],
                        'attribute' => 'document_author_id',
                        'filter' => ['' => 'Все'] + $userArray,
                        'value' => function (Invoice $model) use ($userArray) {
                            return isset($userArray[$model->document_author_id]) ? $userArray[$model->document_author_id] : '';
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
