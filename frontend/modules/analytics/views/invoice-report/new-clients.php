<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 20.03.2017
 * Time: 3:31
 */

use common\components\TextHelper;
use frontend\modules\analytics\models\InvoiceReportSearch;
use common\models\document\Invoice;
use common\components\grid\GridView;
use common\components\date\DateHelper;
use common\models\document\PaymentForm;
use common\components\grid\DropDownSearchDataColumn;
use common\models\document\status\InvoiceStatus;

/* @var $newClients [] */
/* @var $allSum [] */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel InvoiceReportSearch */

$this->title = 'Отчет по новым клиентам';
?>
<div class="row">
    <div class="col-md-3 col-md-push-9">
        <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
    </div>
    <div class="col-md-9 col-md-pull-3 created-scroll">
        <table class="table table-striped table-bordered table-hover">
            <tr class="heading">
                <th style="width: 24%;">Сотрудник</th>
                <th style="width: 22%;">Долг на начало периода</th>
                <th style="width: 22%;">Выставлено за период</th>
                <th style="width: 22%;">Оплачено за период</th>
                <th style="width: 10%;">%%</th>
            </tr>
            <?php foreach ($newClients as $newClient): ?>
                <tr>
                    <td><?= $newClient['author']; ?></td>
                    <td><?= TextHelper::invoiceMoneyFormat($newClient['debtOnStartPeriod'], 2); ?></td>
                    <td><?= TextHelper::invoiceMoneyFormat($newClient['newInvoicesSum'], 2); ?></td>
                    <td><?= TextHelper::invoiceMoneyFormat($newClient['debtOnEndPeriod'], 2); ?></td>
                    <td><?= round(($newClient['debtOnEndPeriod'] / $allSum['allDebtOnEndPeriod']) * 100, 2); ?></td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td><b>Итого</b></td>
                <td>
                    <b><?= TextHelper::invoiceMoneyFormat($allSum['allDebtOnStartPeriod'], 2); ?></b>
                </td>
                <td>
                    <b><?= TextHelper::invoiceMoneyFormat($allSum['allNewInvoicesSum'], 2); ?></b>
                </td>
                <td>
                    <b><?= TextHelper::invoiceMoneyFormat($allSum['allDebtOnEndPeriod'], 2); ?></b>
                </td>
                <td><b>100</b></td>
            </tr>

        </table>
    </div>
</div>
<div class="portlet box darkblue blk_wth_srch">
    <div class="portlet-title row-fluid">
        <div class="caption list_recip col-md-4 col-sm-4">
            Список счетов по новым клиентам
        </div>
    </div>

    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable status_nowrap overfl_text_hid invoice-table',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => (Yii::$app->controller->id === 'default') ? "{items}\n{pager}" : $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'columns' => [
                    [
                        'attribute' => 'contractor_id',
                        'label' => 'Контрагент',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '25%',
                        ],
                        'format' => 'raw',
                        'value' => function (Invoice $model) {
                            return $model->contractor->nameWithType;
                        },
                    ],
                    [
                        'attribute' => 'document_date',
                        'label' => 'Дата счета',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'format' => 'raw',
                        'value' => function (Invoice $model) {
                            return DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                        },
                    ],
                    [
                        'attribute' => 'document_number',
                        'label' => '№ счета',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'format' => 'raw',
                        'value' => function (Invoice $model) {
                            return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                                'model' => $model,
                            ])
                                ? \yii\helpers\Html::a($model->fullNumber, ['/documents/invoice/view',
                                'type' => $model->type,
                                'id' => $model->id,
                                'contractorId' => $model->contractor_id,
                            ])
                                : $model->fullNumber;
                        },
                    ],
                    [
                        'attribute' => 'total_amount_with_nds',
                        'label' => 'Сумма счета',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '15%',
                        ],
                        'format' => 'raw',
                        'value' => function (Invoice $model) {
                            $amount = '<span>' . TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2) . '</span>';
                            if ($model->invoice_status_id == InvoiceStatus::STATUS_PAYED_PARTIAL) {
                                $amount .= ' / <span style="color: #45b6af;" title="Оплаченная сумма">' . TextHelper::invoiceMoneyFormat($model->payment_partial_amount, 2) . '</span>';
                            } elseif ($model->invoice_status_id == InvoiceStatus::STATUS_OVERDUE && $model->remaining_amount !== null) {
                                $amount .= ' / <span style="color: #f3565d;" title="Неоплаченная сумма">' . TextHelper::invoiceMoneyFormat($model->remaining_amount, 2) . '</span>';
                            }

                            return $amount;
                        },
                    ],
                    [
                        'attribute' => 'invoice_status_updated_at',
                        'label' => 'Дата оплаты',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'format' => 'raw',
                        'value' => function (Invoice $model) {
                            return date(DateHelper::FORMAT_USER_DATE, $model->invoice_status_updated_at);
                        },
                    ],
                    [
                        'attribute' => 'payment_form_id',
                        'label' => 'Тип оплаты',
                        'class' => DropDownSearchDataColumn::className(),
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '15%',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->paymentTypeNewClients,
                        'value' => function (Invoice $model) {
                            return PaymentForm::findOne($model->getPaymentType()) ? PaymentForm::findOne($model->getPaymentType())->name : null;
                        },
                    ],
                    [
                        'attribute' => 'document_author_id',
                        'label' => 'Выставил счет',
                        'class' => DropDownSearchDataColumn::className(),
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '15%',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getAuthorFilter(),
                        'value' => function (Invoice $model) {
                            return $model->documentAuthor->getFio(true);
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>