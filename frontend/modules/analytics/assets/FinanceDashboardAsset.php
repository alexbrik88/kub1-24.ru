<?php

namespace frontend\modules\analytics\assets;

use yii\web\AssetBundle;

/**
 * Class FinanceDashboardAsset
 */
class FinanceDashboardAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/modules/analytics/assets/web';

    /**
     * @var array
     */
    public $css = [
        'css/finance-dashboard.css',
        'css/finance-dashboard-chart.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'js/finance-dashboard.js'
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
