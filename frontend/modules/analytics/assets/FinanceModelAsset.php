<?php

namespace frontend\modules\analytics\assets;

use yii\web\AssetBundle;

/**
 * Class FinanceModelAsset
 */
class FinanceModelAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/modules/analytics/assets/web';

    /**
     * @var array
     */
    public $css = [
        'css/finance-model.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'js/finance-model.js'
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
