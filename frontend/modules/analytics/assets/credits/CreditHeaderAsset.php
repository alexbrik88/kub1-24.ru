<?php

namespace frontend\modules\analytics\assets\credits;

use yii\web\AssetBundle;

class CreditHeaderAsset extends AssetBundle
{
    /**
     * @inheritDoc
     */
    public $css = [
        'credit-header.css',
    ];

    /**
     * @inheritDoc
     */
    public $sourcePath = __DIR__ . '/dist';

    /**
     * @inheritDoc
     */
    public $depends = [
        'yii\bootstrap4\BootstrapPluginAsset',
    ];
}
