<?php

namespace frontend\modules\analytics\assets\credits;

use yii\web\AssetBundle;

class FlowFormAsset extends AssetBundle
{
    /**
     * @inheritDoc
     */
    public $js = [
        'flow-form.js',
    ];

    /**
     * @inheritDoc
     */
    public $sourcePath = __DIR__ . '/dist';

    /**
     * @inheritDoc
     */
    public $depends = [
        'yii\bootstrap4\BootstrapPluginAsset',
    ];
}
