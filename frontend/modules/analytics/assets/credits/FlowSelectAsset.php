<?php

namespace frontend\modules\analytics\assets\credits;

use yii\web\AssetBundle;

class FlowSelectAsset extends AssetBundle
{
    /**
     * @inheritDoc
     */
    public $js = [
        'flow-select.js',
    ];

    /**
     * @inheritDoc
     */
    public $sourcePath = __DIR__ . '/dist';

    /**
     * @inheritDoc
     */
    public $depends = [
        'yii\bootstrap4\BootstrapPluginAsset',
    ];
}
