$(document).ready(function() {

    $('#deleteDialog').on('show.bs.modal', function() {
        let canDelete = 1;
        $('#rowSelectForm [type="checkbox"][data-diff]:checked').each(function () {
            canDelete *= parseInt($(this).data('can-delete')) || 0;
        });
        if (canDelete) {
            $(this).find('[data-can-delete="1"]').removeClass('hidden');
            $(this).find('[data-can-delete="0"]').addClass('hidden');
        } else {
            $(this).find('[data-can-delete="1"]').addClass('hidden');
            $(this).find('[data-can-delete="0"]').removeClass('hidden');
        }
    });

    $(document).on('change', '#rowSelectForm [type="checkbox"]', function () {
        var diff = 0;
        var element = $('#rowSelectSummary .total-diff');

        $('#rowSelectForm [type="checkbox"][data-diff]:checked').each(function () {
            var checkBox = $(this);

            diff += parseFloat(checkBox.data('diff'));
        });

        element.text(number_format(diff, 2, ',', ' '));

        if (diff > 0) {
            element.parent().removeClass('invisible');
        } else {
            element.parent().addClass('invisible');
        }
    });
});
