$(document).ready(function() {
    var TYPE_INCOME = 1;

    $(document).on('change', '#flowForm [name*="[flow_type]"]', function() {
        var value = parseInt($(this).val());

        console.trace();

        $('#recognitionDateLabel').text((value === TYPE_INCOME) ? 'Дата признания дохода' : 'Дата признания расхода');

        if (value === TYPE_INCOME) {
            $('#incomeItemId').removeClass('d-none');
            $('#expenditureItemId').addClass('d-none');
        } else {
            $('#incomeItemId').addClass('d-none');
            $('#expenditureItemId').removeClass('d-none');
        }
    });
});
