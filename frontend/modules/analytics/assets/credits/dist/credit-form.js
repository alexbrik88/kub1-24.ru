$(document).ready(function() {
    var PAYMENT_TYPE_END_BODY_WITH_PERCENTS = 4;
    var PAYMENT_MODE_AGREEMENT = 3;
    var TYPE_OVERDRAFT = 3;

    $(document).on('change', '#creditForm [name$="[payment_mode]"]', function() {
        var value = parseInt($(this).val());
        var paymentDayGroup = $('#paymentDayGroup');

        if (value === PAYMENT_MODE_AGREEMENT) {
            paymentDayGroup.removeClass('invisible');
        } else {
            paymentDayGroup.addClass('invisible');
        }
    });

    $(document).on('change', '#creditForm [name$="[credit_type]"]', function() {
        var value = parseInt($(this).val());
        var creditTrancheGroup = $('#creditTrancheGroup, #creditTrancheGroupHelp');

        if (value === TYPE_OVERDRAFT) {
            creditTrancheGroup.removeClass('invisible');
        } else {
            creditTrancheGroup.addClass('invisible');
        }
    });

    $(document).on('change', '#creditForm [name$="[credit_type]"]', function() {
        const labelCreditAmount = $('#creditForm .field-creditform-credit_amount').find('label');
        const labelPaymentType  = $('#creditForm .field-creditform-payment_type').find('label');

        if (labelCreditAmount.length) {
            labelCreditAmount.html(labelCreditAmount.data('label-' + this.value));
        }

        if (labelPaymentType.length) {
            labelPaymentType.html(labelPaymentType.data('label-' + this.value));
            labelPaymentType.closest('.row').find('.tooltip-help').first().css({left:labelPaymentType.data('label-left-' + this.value)});
        }
    });

    $(document).on('change', '#creditForm [name$="[payment_type]"]', function() {
        var value = parseInt($(this).val());
        if (value === PAYMENT_TYPE_END_BODY_WITH_PERCENTS) {
            $('#creditform-payment_mode').prop('disabled', true);
            $('#select2-creditform-payment_mode-container').css({opacity:0});
        } else {
            $('#creditform-payment_mode').prop('disabled', false);
            $('#select2-creditform-payment_mode-container').css({opacity:1});
        }
    });

    $(document).ready(function () {
        $('#creditForm select').trigger('change');
    })
});
