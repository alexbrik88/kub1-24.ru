$(document).ready(function() {
    $(document).on('change', '#rowSelectForm [type="checkbox"]', function () {
        var income = 0;
        var expense = 0;

        $('#rowSelectForm [type="checkbox"][data-income][data-expense]:checked').each(function () {
            var checkBox = $(this);

            income += parseFloat(checkBox.data('income'));
            expense += parseFloat(checkBox.data('expense'));
        });

        var diff = Math.abs(income - expense);

        $('#rowSelectSummary .total-income').text(number_format(income, 2, ',', ' '));
        $('#rowSelectSummary .total-expense').text(number_format(expense, 2, ',', ' '));
        $('#rowSelectSummary .total-diff').text(number_format(diff, 2, ',', ' '));

        if (income > 0 && expense > 0) {
            $('#rowSelectSummary .total-diff').parent().removeClass('invisible');
        } else {
            $('#rowSelectSummary .total-diff').parent().addClass('invisible');
        }
    });
});
