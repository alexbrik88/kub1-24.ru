$(document).ready(function() {
    var ITEM_LOAN = "4";
    var ITEM_CREDIT = "5";
    var ITEM_LOAN_REPAYMENT = "18";
    var ITEM_REPAYMENT_CREDIT = "37";
    var ITEM_PAYMENT_PERCENT = "38";
    var values = [ITEM_LOAN, ITEM_CREDIT, ITEM_LOAN_REPAYMENT, ITEM_REPAYMENT_CREDIT, ITEM_PAYMENT_PERCENT];
    var creditListOptions = $('#creditList').find('option');

    $(document).on('change', '.modal:visible [name$="[income_item_id]"], .modal:visible [name$="[expenditure_item_id]"]', function () {
        var value = $(this).val();
        var creditList = $('#creditList');
        var invoiceList = $('#invoiceList');
        var creditSelect = $('select', creditList);
        var filterSelect = $(creditSelect).attr('data-filter-target');
        var filterAttribute = $(creditSelect).attr('data-filter-attribute');
        var filterId, creditId;

        if (this.disabled)
            return;

        if (value.length && values.indexOf(value) !== -1 && creditList.length) {
            invoiceList.addClass('d-none');
            creditList.removeClass('d-none');
            // filter by contractor
            if ($(filterSelect).length) {
                creditId = $(creditSelect).val();
                filterId = $(filterSelect).val();
                $(creditSelect)
                    .html('')
                    .append($(creditListOptions)[1])
                    .append($(creditListOptions).filter('['+filterAttribute+'='+filterId+']'));

                if (creditId && $(creditSelect).find('option[value='+creditId+']').length) {
                    $(creditSelect).val(creditId).trigger('change');
                } else {
                    $(creditSelect).val('').trigger('change');
                }
            }
        } else {
            invoiceList.removeClass('d-none');
            creditList.addClass('d-none');
            $(creditSelect).html('').append($(creditListOptions)[1]).val('').trigger('change');
        }
    });

    $(document).on('change', 'form [name$="[credit_id]"]', function() {
        var credit_id = $(this).val();
        var form = $(this).closest('form').first();

        if (credit_id) {
            var ajax = $.get('/analytics/credit/data?credit_id=' + credit_id);

            ajax.done(function(data) {
                form.find('[name$="[credit_amount]"]').val(data['amountWithCurrency']);
            });
        } else {
            form.find('[name$="[credit_amount]"]').val('');
        }
    });

    $('form [name$="[income_item_id]"], form [name$="[expenditure_item_id]"]').trigger('change');
});
