<?php

namespace frontend\modules\analytics\assets\credits;

use yii\web\AssetBundle;

class CreditFormAsset extends AssetBundle
{
    /**
     * @inheritDoc
     */
    public $css = [
        'credit-form.css',
    ];

    /**
     * @inheritDoc
     */
    public $js = [
        'credit-form.js',
    ];

    /**
     * @inheritDoc
     */
    public $sourcePath = __DIR__ . '/dist';

    /**
     * @inheritDoc
     */
    public $depends = [
        'yii\bootstrap4\BootstrapPluginAsset',
        'frontend\modules\crm\assets\AjaxFormAsset',
    ];
}
