<?php

namespace frontend\modules\analytics\assets\credits;

use yii\web\AssetBundle;

class CreditSelectAsset extends AssetBundle
{
    /**
     * @inheritDoc
     */
    public $js = [
        'credit-select.js',
    ];

    /**
     * @inheritDoc
     */
    public $sourcePath = __DIR__ . '/dist';

    /**
     * @inheritDoc
     */
    public $depends = [
        'yii\bootstrap4\BootstrapPluginAsset',
    ];
}
