<?php

namespace frontend\modules\analytics\assets;

use yii\web\AssetBundle;

/**
 * Class ScenarioAnalysisAssets
 */
class ScenarioAnalysisAssets extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/modules/analytics/assets/web';

    /**
     * @var array
     */
    public $css = [
        'css/scenario-analysis.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'js/scenario-analysis.js'
    ];

    /**
     * @var array
     */
    public $depends = [
        'frontend\themes\kub\assets\KubAsset',
    ];
}
