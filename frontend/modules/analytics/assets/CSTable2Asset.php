<?php

namespace frontend\modules\analytics\assets;

use yii\web\AssetBundle;

/**
 * Class AnalyticsAsset
 */
class CSTable2Asset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/modules/analytics/assets/web';

    /**
     * @var array
     */
    public $css = [
        'css/cs-table-2.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'js/cs-table-2.js'
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
