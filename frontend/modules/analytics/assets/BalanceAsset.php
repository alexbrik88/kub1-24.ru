<?php

namespace frontend\modules\analytics\assets;

use yii\web\AssetBundle;

/**
 * Class WhatIfAssets
 */
class BalanceAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/modules/analytics/assets/web';

    /**
     * @var array
     */
    public $js = [
        'js/balance.js'
    ];

    /**
     * @var array
     */
    public $depends = [];
}
