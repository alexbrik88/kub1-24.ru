CSTable2 = {
    wrapper: "#cs-table-1x",
    topScrollbar: "#cs-table-11x",
    fixedColumnWidth: 250,
    refreshEvents: [],
    reinitEvents: [],
    _scrollLeft: 0,
    init: function(params) {
        this.prototype = $.extend(this, params);
        this.addCss();
        this.setStickyColumn();
        this.bindEvents();
        this.syncScrollbars();
        this.refresh();
    },
    setStickyColumn: function() {
        $(this.wrapper).addClass('sticky');
    },
    bindEvents: function() {
        this.refreshEvents.forEach((obj) => {
            $(document).on(obj.event, obj.selector, function(e) {
                CSTable2.refresh();
            });
        });
        this.reinitEvents.forEach((obj) => {
            $(document).on(obj.event, obj.selector, function(e) {
                document.getElementById(CSTable2.wrapper.replace('#','')).scrollLeft = CSTable2._scrollLeft;
                CSTable2.reinit();
            });
        });
    },
    syncScrollbars: function()
    {
        const w1 = document.getElementById(this.wrapper.replace('#',''));
        const w2 = document.getElementById(this.topScrollbar.replace('#',''));

        if (!w1 || !w2) {
            console.log('CSTable2: scroll selectors not found');
            return;
        }

        w1.onscroll = function() {
            CSTable2._scrollLeft = w2.scrollLeft = w1.scrollLeft;
        };
        w2.onscroll = function() {
            CSTable2._scrollLeft = w1.scrollLeft = w2.scrollLeft;
        };
    },
    reinit: function()
    {
        this.setStickyColumn();
        this.syncScrollbars();
        this.refresh();
    },
    refresh: function()
    {
        $(this.topScrollbar + ' > .table-wrap').width($(this.wrapper + ' > .table-wrap > .table').width() + this.fixedColumnWidth /*padding-left*/);
    },
    addCss: function() {
        const style = document.createElement('style');
        const code =
            this.wrapper + ".sticky .table thead tr th:first-child { width: " + this.fixedColumnWidth + "px!important; }" +
            this.wrapper + ".sticky .table tbody tr td:first-child { width: " + this.fixedColumnWidth + "px!important; }" +
            this.wrapper + ".sticky .table-wrap { padding-left: " + this.fixedColumnWidth + "px!important; }";

        if (style.styleSheet) {
            // IE
            style.type = 'text/css';
            style.styleSheet.cssText = code;
        } else {
            // Other browsers
            style.innerHTML = code;
        }
        document.body.appendChild(style);
    },
};