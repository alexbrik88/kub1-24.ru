
!function( $ ) {
    $(document).on('blur', '.scenario-analysis input.form-control', function (e) {
        $(this.form).trigger('submit');
    });
    $(document).on('click', '.scenario-analysis table .collapse-row-trigger', function(e) {
        e.preventDefault();
        var target = $(this).data('target');
        $('.scenario-analysis table .collapse-row-trigger[data-target="'+target+'"]').toggleClass('active');
        $('.scenario-analysis table tr[data-id="'+target+'"]').toggleClass('hidden');
    });
    $(document).on('click', '.scenario-analysis table .collapse-all-trigger', function() {
        $(this).toggleClass('active');
        let table = $(this).closest('table');
        let isActive = $(this).hasClass('active');
        console.log(isActive);
        $('tr.group-row .collapse-row-trigger', table).toggleClass('active', isActive);
        $('tr.group-item-row', table).toggleClass('hidden', !isActive);
    });
}( window.jQuery );
