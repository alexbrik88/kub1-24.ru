BalanceTable = {
    init: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        // change page
        $('#button-change-balance-page').on('click', function() {
            const page = $(this).data('page');
            const firstRun = $(this).data('first-run') || 0;
            BalanceTable.changePage(page, firstRun);
        });
        // close detaliation table
        $(document).on('click', '.close-balance-item-detalization', function() {
            $('#balance-article-grid-container').html('');
            $('#balance-store-grid-container').html('');
        });
    },
    getActiveCheckboxes: function() {
        let activeCheckboxes = [];
        $('.table-balance tbody').find('.table-collapse-btn').each(function(i,v) {
            const el = $(v);
            if (el.hasClass('active')) {
                activeCheckboxes.push(el.data('target'));
            }
        });
        return activeCheckboxes;
    },
    getAllCheckboxes: function() {
        let checkboxes = [];
        $('.table-balance tbody').find('.table-collapse-btn').each(function(i,v) {
            const el = $(v);
            checkboxes.push(el.data('target'));
        });
        return checkboxes;
    },
    clickActiveCheckboxes: function(checkboxes) {
        $('.table-balance tbody').find('.table-collapse-btn').each(function(i,v) {
            const el = $(v);
            if (checkboxes.includes(el.data('target'))) {
                el.click();
            }
        });
    },
    changePage: function(page, firstRun) {
        const activeCheckboxes = (Number(firstRun) === 1)
            ? this.getAllCheckboxes()
            : this.getActiveCheckboxes()
        location.href = 'change-balance-page?page=' + page + '&activeCheckboxes=' + activeCheckboxes.join(',');
    },
    refreshPage: function() {
        const activeCheckboxes = BalanceTable.getActiveCheckboxes();
        location.href = 'refresh-balance-page?activeCheckboxes=' + activeCheckboxes.join(',');
    }
};

BalanceWalletModal = {
    init: function() {
        this.newCheckingAccount.bindEvents();
        this.newCashbox.bindEvents();
        this.newEmoney.bindEvents();
        this.updateInitialBalance.bindEvents();
        this.createInitialBalance.bindEvents();
    },
    newCheckingAccount: {
        bindEvents: function() {
            // show modal
            $(document).on("click", ".js-action-cash-add-bank", function() {
                $("#add-company-rs").modal();
            });
            // submit form
            $(document).on("beforeSubmit", "form.form-checking-accountant", function () {
                $.post($(this).attr("action"), $(this).serialize(), function (data) {
                    //
                    BalanceTable.refreshPage();
                });
                return false;
            });
        }
    },
    newCashbox: {
        bindEvents: function() {
            // show modal
            $(document).on("click", ".js-action-cash-add-cashbox", function() {
                $("#add-company-cashbox").modal();
            });
            // submit form
            $(document).on("beforeSubmit", "form#cashbox-form", function () {
                $.post($(this).attr("action"), $(this).serialize(), function (data) {
                    //
                    BalanceTable.refreshPage();
                });
                return false;
            });
        }
    },
    newEmoney: {
        bindEvents: function() {
            // show modal
            $(document).on("click", ".js-action-cash-add-emoney", function() {
                $("#add-company-emoney").modal();
            });
            // submit form
            $(document).on("beforeSubmit", "form#emoney-form", function () {
                $.post($(this).attr("action"), $(this).serialize(), function (data) {
                    //
                    BalanceTable.refreshPage();
                });
                return false;
            });
        }
    },
    createInitialBalance: {
        modal: "#ajax-modal-box",
        bindEvents: function() {
            const that = this;
            const modal = that.modal;
            $(modal).on("ajax-modal-box-loaded", function() {
                that.disableSomeFields(modal);
                that.setSomeFieldsDefaults(modal);
            });
        },
        setSomeFieldsDefaults: function(modal) {
            const defaultDescriptionIds = [
                "#cashbankflowsform-description",
                "#cashorderflows-description",
                "#cashemoneyflows-description"
            ];
            const defaultDescriptionVal = "Начальный остаток";

            $('textarea', modal).each(function(i, input) {
                const id = "#" + $(input).attr('id');
                if (defaultDescriptionIds.includes(id))
                    $(input).val(defaultDescriptionVal).trigger('change');
            });
        },
        disableSomeFields: function(modal)
        {
            const enabledIds = [
                // bank
                "#cashbankflowsform-date",
                "#cashbankflowsform-amount",
                "#cashbankflowsform-description",
                // cashbox
                "#cashorderflows-amount",
                "#cashorderflows-date",
                "#cashorderflows-description",
                "#cashorderflows-number",
                // emoney
                "#cashemoneyflows-amount",
                "#cashemoneyflows-date",
                "#cashemoneyflows-date_time",
                "#cashemoneyflows-description",
                "#cashemoneyflows-number",
            ];

            const alwaysNullIds = [
                // bank
                "#cashbankflowsform-recognitiondateinput",
                // cashbox
                "#cashorderflows-recognitiondateinput",
                // emoney
                "#cashemoneyflows-recognitiondateinput",
            ];

            $('input[type=text], input[type=checkbox], select, textarea', modal).each(function(i, input) {
                const id = "#" + $(input).attr('id');
                if (!enabledIds.includes(id))
                    $(input).prop('disabled', true);
                if (alwaysNullIds.includes(id))
                    $(input).val(null).trigger('change');
            });
        }
    },
    updateInitialBalance: {
        modal: "#update-movement",
        bindEvents: function() {
            const that = this;
            // show modal
            $(document).on("click", ".js-action-cash-edit", function (event) {
                const button = $(this);
                const modal = $(that.modal);
                $.ajax({
                    type: "get",
                    url: button.data("url"),
                    data: {},
                    success: function (data) {
                        modal.find(".modal-body").append(data);
                        that.disableSomeFields(modal);
                        that.disableSomeButtons(modal);
                        modal.modal();
                    }
                });
            });
            // hide modal
            $(document).on("hide.bs.modal", that.modal, function (event) {
                $(that.modal + " .modal-body").empty();
            });
        },
        disableSomeButtons: function (modal) {
            const button = $('.cash-modal-split-to-pieces', modal);
            if (button.length)
                button.prop('disabled', true);
        },
        disableSomeFields: function(modal)
        {
            const enabledIds = [
                // bank
                "#cashbankflowsform-date",
                "#cashbankflowsform-amount",
                "#cashbankflowsform-description",
                // cashbox
                "#cashorderflows-amount",
                "#cashorderflows-date",
                "#cashorderflows-description",
                // emoney
                "#cashemoneyflows-amount",
                "#cashemoneyflows-date",
                "#cashemoneyflows-date_time",
                "#cashemoneyflows-description",
            ];

            const alwaysNullIds = [
                // bank
                "#cashbankflowsform-recognitiondateinput",
                // cashbox
                "#cashorderflows-recognitiondateinput",
                // emoney
                "#cashemoneyflows-recognitiondateinput",
            ];

            $('input[type=text], input[type=checkbox], select, textarea', modal).each(function(i, input) {
                const id = "#" + $(input).attr('id');
                if (!enabledIds.includes(id))
                    $(input).prop('disabled', true);
                if (alwaysNullIds.includes(id))
                    $(input).val(null).trigger('change');
            });
        }
    }
};

BalanceInitial = {
    modalDate: "#modal-balance-initial-date",
    modalProfit: "#modal-balance-initial-undistributed-profit",
    modalCapital: "#modal-balance-initial-capital",
    modalStore: "#modal-balance-initial-store",
    modalCredit: "#modal-balance-initial-credit",
    modalFixedAssetsOther: "#modal-balance-initial-fixed-assets-other",
    init: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        const that = this;
        // show one of modals
        $(document).on("click", ".js-change-balance-initial-date", function() {
            $(that.modalDate).modal();
        });
        $(document).on("click", ".js-change-balance-initial-undistributed-profit", function() {
            $(that.modalProfit).modal();
        });
        $(document).on("click", ".js-change-balance-initial-capital", function() {
            $(that.modalCapital).modal();
        });
        $(document).on("click", ".js-change-balance-initial-store", function() {
            $(that.modalStore).modal();
        });
        $(document).on("click", ".js-change-balance-initial-credit", function() {
            $(that.modalCredit).modal();
        });
        $(document).on("click", ".js-change-balance-initial-fixed-assets-other", function() {
            $(that.modalFixedAssetsOther).modal();
        });

        // save one of modals
        $(document).on("click", that.modalDate + " .btn-save", function() {
            $.post($(this).data('url'), {'main-date': $(that.modalDate).find('input').val()}, function(data) {
                location.reload(); });
        });
        $(document).on("click", that.modalCapital + " .btn-save", function() {
            $.post($(this).data('url'), {'capital': $(that.modalCapital).find('input').val()}, function(data) {
                location.reload(); });
        });
        $(document).on("click", that.modalProfit + " .btn-save", function() {
            $.post($(this).data('url'), {'undistributed-profit': $(that.modalProfit).find('input').val()}, function(data) {
                location.reload(); });
        });
        $(document).on("click", that.modalFixedAssetsOther + " .btn-save", function() {
            $.post($(this).data('url'), {'fixed-assets-other': $(that.modalFixedAssetsOther).find('input').val()}, function(data) {
                location.reload(); });
        });
    }
};

BalanceArticleInitial = {
    pjaxContainer: "#balance-article-form-container",
    _lastUrl: null,
    init: function() {
        this.bindEvents();
    },
    bindEvents: function()
    {
        const that = this;
        // show modal
        $(document).on("click", ".balance-article-modal-link", function(e) {
            e.preventDefault();
            that._lastUrl = $(this).data("url");
            $.pjax({
                url: $(this).data("url"),
                container: that.pjaxContainer,
                push: false,
                scrollTo: false
            });
        });
        // pjax success
        $(document).on("pjax:success", that.pjaxContainer, function() {
            $("#balance-article-modal-header").html($("[data-header]").data("header"));
            $(that.pjaxContainer + " .tooltip2").tooltipster({
                theme: ["tooltipster-kub"],
                trigger: "hover",
                side: "right",
            });
            if (that._lastUrl.includes('category=')) {
                $('#balancearticleform-category').prop('disabled', true);
            }
            $("#balance-article-modal-container").modal();
        });
        // change category
        $(document).on("change", ".balance-article-category", function(e) {
            let subcategories = balanceArticlesSubcategoriesMapByCategories[$(this).val()];
            let subcategoriesInput = $(".balance-article-subcategory");

            subcategoriesInput.find("option").remove();
            subcategoriesInput.append(new Option("", ""));
            for (let categoryId in subcategories) {
                subcategoriesInput.append(new Option(subcategories[categoryId], categoryId));
            }
            subcategoriesInput.trigger("change");
        });
        // change subCategory
        $(document).on("change", ".balance-article-subcategory", function(e) {
            let usefulLifeInMonth = usefulLifeInMonthSubcategoriesMap[$(this).val()];
            let usefulLifeInMonthInput = $("#balancearticleform-useful_life_in_month");

            if (usefulLifeInMonth === null) {
                usefulLifeInMonth = "Не амортизируется";
            }

            usefulLifeInMonthInput.val(usefulLifeInMonth);
        });
    }
};

BalanceArticleGrid = {
    pjaxContainer: "#balance-article-grid-container",
    init: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        const that = this;
        // show grid
        $(document).on("click", ".balance-article-grid-link", function(e) {
            e.preventDefault();
            $.pjax({
                url: $(this).data("url"),
                container: that.pjaxContainer,
                push: false,
                scrollTo: false
            });
            $('#hellopreloader').show();
            $('#hellopreloader #hellopreloader_preload').show().css('opacity', 0.7);
            BalanceStoreGrid.remove();
        });
        // pjax success
        $(document).on("pjax:success", that.pjaxContainer, function() {
            $('.items-table-block').removeClass('hidden');
            $('#hellopreloader #hellopreloader_preload').hide();
            $('html, body').animate({
                scrollTop: $('.items-table-block').offset().top - 50
            }, "slow");
        });
        // detalization modal
        $(document).on("click", "#modal-balance-store-initial-date .btn-save", function(e) {
            const data = $('.joint-operation-checkbox:checked, #modal-balance-store-initial-date input').serialize();
            $.post($(this).data('url'), data, function(data) {
                location.reload(); });
        });
    },
    remove: function() {
        $(this.pjaxContainer).html('');
    }
};

BalanceStoreGrid = {
    pjaxContainer: "#balance-store-grid-container",
    init: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        const that = this;
        // show grid
        $(document).on("click", ".balance-store-grid-link", function(e) {
            e.preventDefault();
            $.pjax({
                url: $(this).data("url"),
                container: that.pjaxContainer,
                push: false,
                scrollTo: false
            });
            $('#hellopreloader').show();
            $('#hellopreloader #hellopreloader_preload').show().css('opacity', 0.7);
            BalanceArticleGrid.remove();
        });
        // pjax success
        $(document).on("pjax:success", that.pjaxContainer, function() {
            $('.items-table-block').removeClass('hidden');
            $('#hellopreloader #hellopreloader_preload').hide();
            $('html, body').animate({
                scrollTop: $('.items-table-block').offset().top - 50
            }, "slow");
        });
    },
    remove: function() {
        $(this.pjaxContainer).html('');
    }
};