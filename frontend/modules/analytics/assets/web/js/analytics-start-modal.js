
!function( $ ) {

    $(document).ready(function () {
        var $modal = $("#analytics-start-modal");
        var startUrl = $modal.data("url");
        var promoDuration = 60; /* seconds */
        var promoItemsCount = 5;
        var progressTimer;
        var promoTimer;

        if (startUrl) {
            $(".modal-body", $modal).load(startUrl);
        }

        var getModalContainer = function (el) {
            return $(".modal-body", $modal);
        };

        var changePromoItem = function () {
            var interval = Math.round(promoDuration * 1000 / promoItemsCount);
            var promoTimer = setInterval(function() {
                let $current = $('#analytics-start-modal .promo_item.show');
                let $next = $current.next('.promo_item') || $('#analytics-start-modal .promo_item').first();
                $current.collapse('hide');
                $next.collapse('show');
            }, interval);
        };

        var animatedCounter = function (el) {
            changePromoItem();
            var interval = 100;
            var durationTime = promoDuration * 1000;
            var leftTime = durationTime;
            var progressTimer = setInterval(function() {
                leftTime -= interval;
                $(el).html((Math.round(leftTime/10)/100).toFixed(1));
                let passed = (durationTime - leftTime)/(durationTime/100);
                $(".progress-bar", $modal).css("width", passed + "%").attr("aria-valuenow", passed);
                if (leftTime <= 0) {
                    clearInterval(progressTimer);
                    $(".counter-toggle-hide", $modal).toggleClass("hidden");
                }
            }, interval);
        };

        var checkModalContent = function () {
            $('.counter-element', $modal).each(function(i, el) {
                animatedCounter(el);
            });
        };

        $(document).on("click", "[data-target=\"#analytics-start-modal\"]", function (e) {
            $(".modal-body", $modal).html("").load("/tax/robot-start/activity");
        });

        $(document).on("click", "#analytics-start-modal [href]:not(a)", function (e) {
            e.preventDefault();
            var url = this.getAttribute("href");
            if ($(this).hasClass('ladda-button')) {
                if (!this.hasAttribute( 'data-style' )) {
                    this.setAttribute( 'data-style', 'expand-right' );
                }
                var l = Ladda.create(this);
                l.start();
            }
            if ($(this).hasClass("not-ajax")) {
                window.location.replace(url);
            } else {
                $.ajax(url, {
                    success: function (data, textStatus, jqXHR) {
                        $(".modal-body", $modal).html(data);
                        checkModalContent();
                    }
                });
            }
            return false;
        });

        $(document).on("submit", "#analytics-start-modal form", function (e) {
            var form = this;
            var laddaBtn = $('.ladda-button:submit', form);
            var formData = new FormData(form);
            if (laddaBtn.length > 0) {
                var button = laddaBtn[0];
                if (!button.hasAttribute( 'data-style' )) {
                    button.setAttribute( 'data-style', 'expand-right' );
                }
                var l = Ladda.create(button);
                l.start();
            }
            if (form.action.includes(location.hostname)) {
                e.preventDefault();
                $.ajax(form.action, {
                    method: form.method,
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (data, textStatus, jqXHR) {
                        $(".modal-body", $modal).html(data);
                        checkModalContent();
                    }
                });
                return false;
            }
        });

        $( document ).ajaxComplete(function( event, xhr, settings ) {
            Ladda.stopAll();
        });

        $( document ).on('hidden.bs.modal', '#analytics-start-modal', function( event ) {
            clearInterval(promoTimer);
            clearInterval(progressTimer);
            $.pjax.reload('#sidemenu-pjax-container');
        });
    });

}( window.jQuery );
