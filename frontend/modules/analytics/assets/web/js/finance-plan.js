    FinancePlans = {
        groupID: null,
        tabID: null,
        planID: null,
        pjaxContainer: "#pjax-finance-plan-table",
        tableContainer: ".finance-model-shop-month:not(.scrollable-table-clone)",
        tableContainerClone: ".finance-model-shop-month.scrollable-table-clone",
        panel: {
            autofill: "#update-planning-auto-fill-item-panel",
            primeCost: '#edit-prime-cost-item-panel',
            variableCost: '#edit-variable-cost-item-panel',
            fixedCost: '#edit-fixed-cost-item-panel',
            taxCost: '#edit-tax-panel',
            oddsIncome: "#edit-odds-income-item-panel",
            oddsExpense: "#edit-odds-expense-item-panel",
            groupExpense: "#edit-group-expense-item-panel",
            oddsSystemIncome: "#edit-odds-system-income-item-panel",
            oddsSystemExpense: "#edit-odds-system-expense-item-panel",
            otherIncome: '#edit-other-income-item-panel',
            otherExpense: '#edit-other-expense-item-panel',
        },
        panelData: {
            autofill: {
                income: [],
                expense: [],
            },
            primeCost: [],
            variableCost: [],
            fixedCost: [],
            taxCost: [],
            odds: {
                income: [],
                expense: [],
            },
            groupAutofill: {
                expense: [],
            },
            oddsSystem: {
                income: [],
                expense: [],
            },
        },
        select2Data: {
            odds: {
                itemsByFlowOfFundsType: {}
            }
        },
        tabs: {
            plan: 'plan',
            expenses: 'expenses'
        },
        credit: {
            modal: '#modal-plan-credit',
            form: '#creditForm',
            pjaxContainer:  '#pjax-plan-credit',
            url: '/analytics/finance-plan-credit-ajax/',
            trigger: {
                incomeItemIds: [4, 5],
                expenditureItemIds: [18,37,38]
            },
            showModal: function(action, creditId) {
                const that = this;
                const title = $(that.modal).find('.modal-title');
                const group = FinancePlans.groupID;
                const plan = FinancePlans.planID;
                const item = Number($('#odds-income_item_id').val());

                title.html(title.data('title-' + Number(item === 4)));
                creditId = creditId || '';

                $.pjax.reload(that.pjaxContainer, {
                    'url': that.url + action + '/?group=' + group + '&plan=' + plan + '&credit=' + creditId + '&item=' + item,
                    'push': false,
                    'replace': false,
                    'scrollTo': false,
                    'timeout': 60000,
                }).done(function() {
                    $(that.modal).modal('show');
                }).fail(function() {
                    window.toastr.error('Внутренняя ошибка сервера #2');
                });

                Ladda.stopAll();
            },
            hideModal: function() {
                const that = this;
                $(that.modal).modal('hide');
                Ladda.stopAll();
            },
            deleteCredit: function(creditId) {
                const that = this;
                const group = FinancePlans.groupID;
                const plan = FinancePlans.planID;

                return $.post(that.url + 'delete/?group=' + group + '&plan=' + plan + '&credit=' + creditId, function(data) {
                    if (data.success)
                        window.toastr.error('Удалено');
                });
            },
        },
        balanceArticle: {
            modal: '#modal-balance-article',
            form: '#balanceArticleForm',
            pjaxContainer:  '#pjax-balance-article',
            url: '/analytics/finance-plan-balance-article/',
            trigger: {
                incomeItemIds: [],
                expenditureItemIds: [71, 101]
            },
            showModal: function(action, articleId) {
                const that = this;
                const title = $(that.modal).find('.modal-title');
                const group = FinancePlans.groupID;
                const plan = FinancePlans.planID;
                const item = Number($('#odds-expenditure_item_id').val());

                title.html(title.data('title-' + Number(item === 101)));
                articleId = articleId || '';

                $.pjax.reload(that.pjaxContainer, {
                    'url': that.url + action + '/?group=' + group + '&plan=' + plan + '&article=' + articleId + '&item=' + item,
                    'push': false,
                    'replace': false,
                    'scrollTo': false,
                    'timeout': 60000,
                }).done(function() {
                    $(that.modal).modal('show');
                }).fail(function() {
                    window.toastr.error('Внутренняя ошибка сервера #2');
                });

                Ladda.stopAll();
            },
            hideModal: function() {
                const that = this;
                $(that.modal).modal('hide');
                Ladda.stopAll();
            },
            deleteArticle: function(articleId) {
                const that = this;
                const group = FinancePlans.groupID;
                const plan = FinancePlans.planID;

                return $.post(that.url + 'delete/?group=' + group + '&plan=' + plan + '&article=' + articleId, function(data) {
                    if (data.success)
                        window.toastr.error('Удалено');
                });
            },
        },
        userConfigCheckbox: {
            ebit: '#config-finance_model_ebit',
            amortization: '#config-finance_model_amortization',
            receivedPercent: '#config-finance_model_percent_received',
            paidPercent: '#config-finance_model_percent_paid'
        },
        init: function(options) {
            this.setOptions(options);
            this.bindEvents();
            this.bindTableEvents();
            this.bindAutofillEvents();
            this.bindPrimeCostEvents();
            this.bindVariableCostEvents();
            this.bindFixedCostEvents();
            this.bindTaxEvents();
            this.bindOddsIncomeEvents();
            this.bindOddsExpenseEvents();
            this.bindGroupExpenseEvents();
            this.bindOddsSystemIncomeEvents();
            this.bindOddsSystemExpenseEvents();
            this.bindConfigEvents();
            this.bindCreditEvents();
            this.bindBalanceArticleEvents();
            this.bindIncomeEvents();
            this.bindExpenseEvents();
        },
        setOptions: function(options) {
            $.extend(this, options);
        },
        bindBalanceArticleEvents: function() {
            const that = this;

            // submit form
            $(document).on('submit', that.balanceArticle.form, function(e) {
                const form = $(this);
                let l = Ladda.create(form.find('button[type=submit]')[0]);

                e.preventDefault();

                l.start();
                $.post(form.attr('action'), form.serialize(), function(data) {
                    that._reloadPjax({
                        floorMap: that._getFloorMap(),
                    }).done(function() {
                        that.balanceArticle.hideModal();
                        if (data.success) {
                            const amortizationCheckbox = $(that.userConfigCheckbox.amortization);
                            if (!amortizationCheckbox.prop('checked')) {
                                amortizationCheckbox.click();
                            }
                        }
                        if (data.message) {
                            window.toastr.error(data.message);
                        }
                    }).fail(function() {
                        that.balanceArticle.hideModal();
                    });
                });
                return false;
            });

            // Show delete modal
            $(document).on('click', '.delete-odds-balance-article-js', function() {
                const articleId = $(this).closest('tr').data('balance_article_id') || 0;
                if (articleId) {
                    $('#confirm-delete-odds-balance-article-js').data('balance_article_id', articleId);
                    $('#confirm-delete-odds-balance-article').modal('show');
                } else {
                    window.toastr.error('Ошибка #3')
                }
            });

            // Delete balance article
            $(document).on('click', '#confirm-delete-odds-balance-article-js', function() {
                const articleId = $(this).data('balance_article_id') || 0;
                if (articleId)
                    that.balanceArticle.deleteArticle(articleId).done(function() {
                        that._reloadPjax({
                            floorMap: that._getFloorMap(),
                        });
                        $('#confirm-delete-odds-balance-article').modal('hide');
                    }).fail(function() {
                        window.toastr.error('Не удалось удалить кредит');
                        $('#confirm-delete-odds-balance-article').modal('hide');
                    });
            });

            // Update balance article
            $(document).on('click', '.update-odds-balance-article-js', function() {
                const articleId = $(this).closest('tr').data('balance_article_id') || 0;
                if (articleId) {
                    that.balanceArticle.showModal('update', articleId);
                } else {
                    window.toastr.error('Ошибка #4')
                }
            });            
            
            // change category
            $(document).on('change', '.balance-article-category', function(e) {
                if (window.balanceArticlesSubcategoriesMapByCategories) {
                    const subcategories = balanceArticlesSubcategoriesMapByCategories[$(this).val()];
                    const subcategoriesInput = $(this).closest('.modal').find('.balance-article-subcategory');
                    subcategoriesInput.find('option').remove();
                    subcategoriesInput.append(new Option('', ''));
                    for (let categoryId in subcategories) {
                        if (subcategories.hasOwnProperty(categoryId))
                            subcategoriesInput.append(new Option(subcategories[categoryId], categoryId));
                    }
                    subcategoriesInput.trigger('change');
                }
            });
            
            // change subcategory
            $(document).on('change', '.balance-article-subcategory', function(e) {
                if (window.usefulLifeInMonthSubcategoriesMap) {
                    const usefulLifeInMonth = usefulLifeInMonthSubcategoriesMap[$(this).val()] || 'Не амортизируется';
                    const usefulLifeInMonthInput = $(this).closest('.modal').find('.balance-article-useful-life');
                    usefulLifeInMonthInput.val(usefulLifeInMonth);
                }
            });
        },
        bindCreditEvents: function() {
            const that = this;

            // submit form
            $(document).on('submit', that.credit.form, function(e) {
                const form = $(this);
                let l = Ladda.create(form.find('button[type=submit]')[0]);

                e.preventDefault();

                l.start();
                $.post(form.attr('action'), form.serialize(), function(data) {
                    that._reloadPjax({
                        floorMap: that._getFloorMap(),
                    }).done(function() {
                        that.credit.hideModal();
                        if (data.message)
                            window.toastr.error(data.message);
                        if (data.success) {
                            const paidPercentCheckbox = $(that.userConfigCheckbox.paidPercent);
                            if (!paidPercentCheckbox.prop('checked')) {
                                paidPercentCheckbox.click();
                            }
                        }
                    }).fail(function() {
                        that.credit.hideModal();
                    });
                });
                return false;
            });

            // Show delete modal
            $(document).on('click', '.delete-odds-credit-js', function() {
                const creditId = $(this).closest('tr').data('credit_id') || 0;
                if (creditId) {
                    $('#confirm-delete-odds-credit-js').data('credit_id', creditId);
                    $('#confirm-delete-odds-credit').modal('show');
                } else {
                    window.toastr.error('Ошибка #3')
                }
            });

            // Delete credit
            $(document).on('click', '#confirm-delete-odds-credit-js', function() {
                const creditId = $(this).data('credit_id') || 0;
                if (creditId)
                    that.credit.deleteCredit(creditId).done(function() {
                    that._reloadPjax({
                        floorMap: that._getFloorMap(),
                    });
                    $('#confirm-delete-odds-credit').modal('hide');
                }).fail(function() {
                    window.toastr.error('Не удалось удалить кредит');
                    $('#confirm-delete-odds-credit').modal('hide');
                });
            });

            // Update credit
            $(document).on('click', '.update-odds-credit-js', function() {
                const creditId = $(this).closest('tr').data('credit_id') || 0;
                if (creditId) {
                    that.credit.showModal('update', creditId);
                } else {
                    window.toastr.error('Ошибка #4')
                }
            });
        },
        bindEvents: function() {
            const that = this;

            // Edit cell
            $(document).on("change", that.tableContainer + " .custom-value", function() {
                that._reloadPjax({
                    floorMap: that._getFloorMap(),
                    userData: that._getUserData()
                });
            });

            // Edit group cell
            $(document).on("change", that.tableContainer + " .custom-value-group", function() {
                that._reloadPjax({
                    floorMap: that._getFloorMap(),
                    userData: that._getGroupUserData()
                });
            });

            // After reloaded Pjax
            $(document).on("pjax:success", that.pjaxContainer, function(e) {
                const activeMenuItem = $('#finance-plan-menu').find('.nav-item.active');
                that.tabID = activeMenuItem.attr('data-tab') || null;
                that.planID = activeMenuItem.attr('data-plan') || null;
                $('body').removeClass('modal-open');
            });
        },

        bindIncomeEvents: function() {
            const that = this;
            const panelID = that.panel.otherIncome;

            // Show other-income panel
            $(document).on('click', '.edit-other-income-js', function(e) {
                const panel = $(panelID);
                const line = $(this).closest("tr");
                const top = Number(line.offset().top) - 220;
                const attr = $(line).data('attr');
                const rowID = $(line).data('row_id');

                const panelData = that.panelData.otherIncome[rowID];
                const itemField = panel.find('#financeplanincomeitem-other-income_item_id');
                const existsItemField = panel.find('#financeplanincomeitem-other-exists_income_item_id');
                const deleteButton = panel.find('.delete-other-income-js');

                if (panel.is(':visible')) {
                    panel.slideUp(150);
                    return;
                } else {
                    that._closeAllPanels();
                }

                if (rowID && that.panelData.otherIncome[rowID]) {
                    itemField.prop('disabled', true).closest('.toggled-element').hide();
                    existsItemField.val(panelData.name).closest('.toggled-element').show();
                    deleteButton.show();
                } else {
                    itemField.prop('disabled', false).closest('.toggled-element').show();
                    existsItemField.val('').closest('.toggled-element').hide();
                    deleteButton.hide();
                }

                panel.find('input.attr').val(attr);
                panel.find('input.row_id').val(rowID);
                panel.css("top", top + "px");
                panel.slideDown(150);
            });

            // Hide other-income panel
            $(document).on('click', '.cancel-edit-other-income-js', function(e) {
                const panel = $(panelID);
                panel.slideUp(150);
            });

            // Send other-income form
            $(document).on('submit', '#form-edit-other-income', function() {
                const form = $(this);
                $.post(this.action, form.serialize(), function (data) {
                    that._reloadPjax({
                        floorMap: that._getFloorMap(),
                    });
                });
                return false;
            });

            // Delete other-income item
            $(document).on('click', '.delete-other-income-js', function() {
                $('#confirm-delete-other-income').modal('show');
            });

            // Delete other-income item
            $(document).on('click', '#confirm-delete-other-income-js', function() {
                const autofillPanel = $(that.panel.autofill);
                const form = $('#form-edit-other-income');
                const attr = $(autofillPanel).find('input.attr').val();
                const rowID = $(autofillPanel).find('input.row_id').val();

                form.find('input.attr').val(attr);
                form.find('input.row_id').val(rowID);
                form.find('input.is_deleted').val(1);
                $.post(form[0].action, form.serialize(), function (data) {
                    that._reloadPjax({
                        floorMap: that._getFloorMap(),
                    });
                });
            });

            // Change select "action" (new row autofill)
            $(document).on('change', '#new-autofill-action-2', function(e) {
                const panel = $(panelID);
                const amountField = panel.find('#new-autofill-amount-2');
                const limitAmountField = panel.find('#new-autofill-limit_amount-2');
                const label = limitAmountField.closest('div').find('.label');
                const tooltip = label.find('.tooltip2');

                if ($(this).val() === '0') {
                    amountField.val('').attr('disabled', true);
                    limitAmountField.val('').attr('disabled', true);
                } else {
                    amountField.removeAttr('disabled');
                    limitAmountField.removeAttr('disabled');
                }

                if ($(this).val() === '2') {
                    label.find('.dynamic-label').text('Мин. значение');
                    tooltip.tooltipster('content', '<span>В данном поле вы можете указать значение ниже которого не может быть значение в ячейке</span>');
                } else {
                    label.find('.dynamic-label').text('Макс. значение');
                    tooltip.tooltipster('content', '<span>В данном поле вы можете указать значение выше которого не может быть значение в ячейке</span>');
                }
            });
        },        
        
        
        
        
        
        bindExpenseEvents: function() {
            const that = this;
            const panelID = that.panel.otherExpense;

            // Show other-expense panel
            $(document).on('click', '.edit-other-expense-js', function(e) {
                const panel = $(panelID);
                const line = $(this).closest("tr");
                const top = Number(line.offset().top) - 220;
                const attr = $(line).data('attr');
                const rowID = $(line).data('row_id');

                const panelData = that.panelData.otherExpense[rowID];
                const itemField = panel.find('#financeplanexpenditureitem-other-expenditure_item_id');
                const existsItemField = panel.find('#financeplanexpenditureitem-other-exists_expenditure_item_id');
                const deleteButton = panel.find('.delete-other-expense-js');

                if (panel.is(':visible')) {
                    panel.slideUp(150);
                    return;
                } else {
                    that._closeAllPanels();
                }

                if (rowID && that.panelData.otherExpense[rowID]) {
                    itemField.prop('disabled', true).closest('.toggled-element').hide();
                    existsItemField.val(panelData.name).closest('.toggled-element').show();
                    deleteButton.show();
                } else {
                    itemField.prop('disabled', false).closest('.toggled-element').show();
                    existsItemField.val('').closest('.toggled-element').hide();
                    deleteButton.hide();
                }

                panel.find('input.attr').val(attr);
                panel.find('input.row_id').val(rowID);
                panel.css("top", top + "px");
                panel.slideDown(150);
            });

            // Hide other-expense panel
            $(document).on('click', '.cancel-edit-other-expense-js', function(e) {
                const panel = $(panelID);
                panel.slideUp(150);
            });

            // Send other-expense form
            $(document).on('submit', '#form-edit-other-expense', function() {
                const form = $(this);
                $.post(this.action, form.serialize(), function (data) {
                    that._reloadPjax({
                        floorMap: that._getFloorMap(),
                    });
                });
                return false;
            });

            // Delete other-expense item
            $(document).on('click', '.delete-other-expense-js', function() {
                $('#confirm-delete-other-expense').modal('show');
            });

            // Delete other-expense item
            $(document).on('click', '#confirm-delete-other-expense-js', function() {
                const autofillPanel = $(that.panel.autofill);
                const form = $('#form-edit-other-expense');
                const attr = $(autofillPanel).find('input.attr').val();
                const rowID = $(autofillPanel).find('input.row_id').val();

                form.find('input.attr').val(attr);
                form.find('input.row_id').val(rowID);
                form.find('input.is_deleted').val(1);
                $.post(form[0].action, form.serialize(), function (data) {
                    that._reloadPjax({
                        floorMap: that._getFloorMap(),
                    });
                });
            });

            // Change select "action" (new row autofill)
            $(document).on('change', '#new-autofill-action-2', function(e) {
                const panel = $(panelID);
                const amountField = panel.find('#new-autofill-amount-2');
                const limitAmountField = panel.find('#new-autofill-limit_amount-2');
                const label = limitAmountField.closest('div').find('.label');
                const tooltip = label.find('.tooltip2');

                if ($(this).val() === '0') {
                    amountField.val('').attr('disabled', true);
                    limitAmountField.val('').attr('disabled', true);
                } else {
                    amountField.removeAttr('disabled');
                    limitAmountField.removeAttr('disabled');
                }

                if ($(this).val() === '2') {
                    label.find('.dynamic-label').text('Мин. значение');
                    tooltip.tooltipster('content', '<span>В данном поле вы можете указать значение ниже которого не может быть значение в ячейке</span>');
                } else {
                    label.find('.dynamic-label').text('Макс. значение');
                    tooltip.tooltipster('content', '<span>В данном поле вы можете указать значение выше которого не может быть значение в ячейке</span>');
                }
            });            
        },
        bindPrimeCostEvents: function() {
            const that = this;
            const panelID = that.panel.primeCost;

            // Show prime-cost panel
            $(document).on('click', '.edit-prime-cost-js', function(e) {
                const panel = $(panelID);
                const line = $(this).closest("tr");
                const top = Number(line.offset().top) - 220;
                const attr = $(line).data('attr');
                const rowID = $(line).data('row_id');

                const panelData = that.panelData.primeCost[rowID];
                const itemField = panel.find('#financeplanprimecostitem-prime_cost_item_id');
                const existsItemField = panel.find('#financeplanprimecostitem-exists_prime_cost_item_id');
                const relationField = panel.find('#financeplanprimecostitem-relation_type');
                const actionField = panel.find('#financeplanprimecostitem-action');
                const actionSymbol = panel.find('.action-symbol');
                const deleteButton = panel.find('.delete-prime-cost-js');

                if (panel.is(':visible')) {
                    panel.slideUp(150);
                    return;
                } else {
                    that._closeAllPanels();
                }

                if (rowID && that.panelData.primeCost[rowID]) {
                    itemField.prop('disabled', true).closest('.toggled-element').hide();
                    existsItemField.val(panelData.name).closest('.toggled-element').show();
                    relationField.val(panelData.relation).trigger('change');
                    actionField.val(panelData.action).trigger('change');
                    actionSymbol.html(actionSymbol.data('relation-' + panelData.relation) || '');
                    deleteButton.show();
                } else {
                    itemField.prop('disabled', false).closest('.toggled-element').show();
                    existsItemField.val('').closest('.toggled-element').hide();
                    relationField.val(0).trigger('change');
                    actionField.val('').trigger('change');
                    actionSymbol.html('');
                    deleteButton.hide();
                }

                panel.find('input.attr').val(attr);
                panel.find('input.row_id').val(rowID);
                panel.css("top", top + "px");
                panel.slideDown(150);
            });

            // Hide prime-cost panel
            $(document).on('click', '.cancel-edit-prime-cost-js', function(e) {
                const panel = $(panelID);
                panel.slideUp(150);
            });

            // Send prime-cost form
            $(document).on('submit', '#form-edit-prime-cost', function() {
                const form = $(this);
                $.post(this.action, form.serialize(), function (data) {
                    that._reloadPjax({
                        floorMap: that._getFloorMap(),
                    });
                });
                return false;
            });

            // Delete prime-cost item
            $(document).on('click', '.delete-prime-cost-js', function() {
                $('#confirm-delete-prime-cost').modal('show');
            });

            // Delete prime-cost item confirm
            $(document).on('click', '#confirm-delete-prime-cost-js', function() {
                const form = $('#form-edit-prime-cost');
                form.find('.is_deleted').val(1);
                $.post(form[0].action, form.serialize(), function (data) {
                    that._reloadPjax({
                        floorMap: that._getFloorMap(),
                    });
                });
            });

            // Change prime-cost relation type
            $(document).on('change', '#financeplanprimecostitem-relation_type', function() {
                const panel = $(panelID);
                const actionSymbol = panel.find('.action-symbol');
                actionSymbol.html(actionSymbol.data('relation-' + this.value) || '');
            });
        },
        bindVariableCostEvents: function() {
            const that = this;
            const panelID = that.panel.variableCost;

            // Show variable-cost panel
            $(document).on('click', '.edit-variable-cost-js', function(e) {
                const panel = $(panelID);
                const line = $(this).closest("tr");
                const top = Number(line.offset().top) - 220;
                const attr = $(line).data('attr');
                const rowID = $(line).data('row_id');

                const panelData = that.panelData.variableCost[rowID];
                const itemField = panel.find('#financeplanexpenditureitem-expenditure_item_id');
                const existsItemField = panel.find('#financeplanexpenditureitem-exists_expenditure_item_id');
                const relationField = panel.find('#financeplanexpenditureitem-relation_type');
                const actionField = panel.find('#financeplanexpenditureitem-action');
                const actionSymbol = panel.find('.action-symbol');
                const deleteButton = panel.find('.delete-variable-cost-js');

                if (panel.is(':visible')) {
                    panel.slideUp(150);
                    return;
                } else {
                    that._closeAllPanels();
                }

                if (rowID && that.panelData.variableCost[rowID]) {
                    itemField.prop('disabled', true).closest('.toggled-element').hide();
                    existsItemField.val(panelData.name).closest('.toggled-element').show();
                    relationField.val(panelData.relation).trigger('change');
                    actionField.val(panelData.action).trigger('change');
                    actionSymbol.html(actionSymbol.data('relation-' + panelData.relation) || '');
                    deleteButton.show();
                } else {
                    itemField.prop('disabled', false).closest('.toggled-element').show();
                    existsItemField.val('').closest('.toggled-element').hide();
                    relationField.val(0).trigger('change');
                    actionField.val('').trigger('change');
                    actionSymbol.html('');
                    deleteButton.hide();
                }

                panel.find('input.attr').val(attr);
                panel.find('input.row_id').val(rowID);
                panel.css("top", top + "px");
                panel.slideDown(150);
            });

            // Hide variable-cost panel
            $(document).on('click', '.cancel-edit-variable-cost-js', function(e) {
                const panel = $(panelID);
                panel.slideUp(150);
            });

            // Send variable-cost form
            $(document).on('submit', '#form-edit-variable-cost', function() {
                const form = $(this);
                $.post(this.action, form.serialize(), function (data) {
                    that._reloadPjax({
                        floorMap: that._getFloorMap(),
                    });
                });
                return false;
            });

            // Delete variable-cost item
            $(document).on('click', '.delete-variable-cost-js', function() {
                $('#confirm-delete-variable-cost').modal('show');
            });

            // Delete variable-cost item confirm
            $(document).on('click', '#confirm-delete-variable-cost-js', function() {
                const form = $('#form-edit-variable-cost');
                form.find('.is_deleted').val(1);
                $.post(form[0].action, form.serialize(), function (data) {
                    that._reloadPjax({
                        floorMap: that._getFloorMap(),
                    });
                });
            });

            // Change variable-cost relation type
            $(document).on('change', '#financeplanexpenditureitem-relation_type', function() {
                const panel = $(panelID);
                const actionSymbol = panel.find('.action-symbol');
                actionSymbol.html(actionSymbol.data('relation-' + this.value) || '');
            });
        },
        bindFixedCostEvents: function() {
            const that = this;
            const panelID = that.panel.fixedCost;

            // Show fixed-cost panel
            $(document).on('click', '.edit-fixed-cost-js', function(e) {
                const panel = $(panelID);
                const line = $(this).closest("tr");
                const top = Number(line.offset().top) - 220;
                const attr = $(line).data('attr');
                const rowID = $(line).data('row_id');

                const panelData = that.panelData.fixedCost[rowID];
                const itemField = panel.find('#financeplanexpenditureitem-fixed-expenditure_item_id');
                const existsItemField = panel.find('#financeplanexpenditureitem-fixed-exists_expenditure_item_id');
                const deleteButton = panel.find('.delete-fixed-cost-js');

                if (panel.is(':visible')) {
                    panel.slideUp(150);
                    return;
                } else {
                    that._closeAllPanels();
                }

                if (rowID && that.panelData.fixedCost[rowID]) {
                    itemField.prop('disabled', true).closest('.toggled-element').hide();
                    existsItemField.val(panelData.name).closest('.toggled-element').show();
                    deleteButton.show();
                } else {
                    itemField.prop('disabled', false).closest('.toggled-element').show();
                    existsItemField.val('').closest('.toggled-element').hide();
                    deleteButton.hide();
                }

                panel.find('input.attr').val(attr);
                panel.find('input.row_id').val(rowID);
                panel.css("top", top + "px");
                panel.slideDown(150);
            });

            // Hide fixed-cost panel
            $(document).on('click', '.cancel-edit-fixed-cost-js', function(e) {
                const panel = $(panelID);
                panel.slideUp(150);
            });

            // Send fixed-cost form
            $(document).on('submit', '#form-edit-fixed-cost', function() {
                const form = $(this);
                $.post(this.action, form.serialize(), function (data) {
                    that._reloadPjax({
                        floorMap: that._getFloorMap(),
                    });
                });
                return false;
            });

            // Delete fixed-cost item
            $(document).on('click', '.delete-fixed-cost-js', function() {
                $('#confirm-delete-fixed-cost').modal('show');
            });

            // Delete fixed-cost item
            $(document).on('click', '#confirm-delete-fixed-cost-js', function() {
                const autofillPanel = $(that.panel.autofill);
                const form = $('#form-edit-fixed-cost');
                const attr = $(autofillPanel).find('input.attr').val();
                const rowID = $(autofillPanel).find('input.row_id').val();

                form.find('input.attr').val(attr);
                form.find('input.row_id').val(rowID);
                form.find('input.is_deleted').val(1);
                $.post(form[0].action, form.serialize(), function (data) {
                    that._reloadPjax({
                        floorMap: that._getFloorMap(),
                    });
                });
            });

            // Change select "action" (new row autofill)
            $(document).on('change', '#new-autofill-action', function(e) {
                const panel = $(panelID);
                const amountField = panel.find('#new-autofill-amount');
                const limitAmountField = panel.find('#new-autofill-limit_amount');
                const label = limitAmountField.closest('div').find('.label');
                const tooltip = label.find('.tooltip2');

                if ($(this).val() === '0') {
                    amountField.val('').attr('disabled', true);
                    limitAmountField.val('').attr('disabled', true);
                } else {
                    amountField.removeAttr('disabled');
                    limitAmountField.removeAttr('disabled');
                }

                if ($(this).val() === '2') {
                    label.find('.dynamic-label').text('Мин. значение');
                    tooltip.tooltipster('content', '<span>В данном поле вы можете указать значение ниже которого не может быть значение в ячейке</span>');
                } else {
                    label.find('.dynamic-label').text('Макс. значение');
                    tooltip.tooltipster('content', '<span>В данном поле вы можете указать значение выше которого не может быть значение в ячейке</span>');
                }
            });
        },
        bindAutofillEvents: function() {
            const that = this;
            const panelID = that.panel.autofill;

            // Show autofill panel
            $(document).on('click', '.edit-autofill-js', function(e) {
                const panel = $(panelID);
                const line = $(this).closest("tr");
                const top = Number(line.offset().top) - 220;
                const attr = $(line).data('attr');
                const rowID = $(line).data('row_id');
                const startMonthField = panel.find('#autofill-start_month');
                const startAmountField = panel.find('#autofill-start_amount');
                const actionField = panel.find('#autofill-action');
                const amountField = panel.find('#autofill-amount');
                const limitAmountField = panel.find('#autofill-limit_amount');

                const allDeleteButtons = panel.find('.delete-row-js');
                const deleteFixedCost = panel.find('.delete-fixed-cost-js');
                const deleteOddsIncome = panel.find('.delete-odds-income-js');
                const deleteOddsExpense = panel.find('.delete-odds-expense-js');
                const deleteGroupExpense = panel.find('.delete-group-expense-js');
                const deleteOtherIncome = panel.find('.delete-other-income-js');
                const deleteOtherExpense = panel.find('.delete-other-expense-js');

                let panelData;

                if (that.tabID === that.tabs.expenses) {
                    panelData = that.panelData.groupAutofill.expense[rowID];
                } else if ((line.data('attr').indexOf('odds') === 0)) {
                    panelData = (line.data('attr').indexOf('Expense') !== -1)
                        ? that.panelData.odds.expense[rowID]
                        : that.panelData.odds.income[rowID];
                } else {
                    panelData = (line.data('attr').indexOf('expense') === 0)
                        ? that.panelData.autofill.expense[rowID]
                        : that.panelData.autofill.income[rowID];
                }

                // panels
                if (panel.is(':visible')) {
                    panel.find("input.amount-input, input.amount-avg-input").val("");
                    panel.slideUp(150);
                    return;
                } else {
                    that._closeAllPanels();
                }

                // delete buttons
                if (that.tabID === that.tabs.expenses) {
                    allDeleteButtons.hide();
                    deleteGroupExpense.show();
                } else if ((line.data('attr').indexOf('odds') === 0)) {
                    allDeleteButtons.hide();
                    if (line.data('attr').indexOf('Expense') !== -1) {
                        deleteOddsExpense.show();
                    } else {
                        deleteOddsIncome.show();
                    }
                } else if (line.data('id').indexOf('fixed') !== -1) {
                    allDeleteButtons.hide();
                    deleteFixedCost.show();
                } else if (line.data('id').indexOf('other') !== -1) {
                    allDeleteButtons.hide();
                    if (line.data('attr').indexOf('expense') !== -1) {
                        deleteOtherExpense.show();
                    } else {
                        deleteOtherIncome.show();
                    }
                } else {
                    allDeleteButtons.hide();
                }

                if (rowID && panelData) {
                    if (panelData.action === 0) {
                        amountField.attr('disabled', true);
                        limitAmountField.attr('disabled', true);
                    } else {
                        amountField.removeAttr('disabled');
                        limitAmountField.removeAttr('disabled');
                    }
                    startMonthField.val(panelData.start_month).trigger('change');
                    startAmountField.val(panelData.start_amount / 100).trigger('change');
                    actionField.val(panelData.action).trigger('change');
                    amountField.val(panelData.amount / 100).trigger('change');
                    limitAmountField.val(panelData.limit_amount / 100).trigger('change');
                } else {
                    let firstMonth = $('#autofill-start_month option:eq(0)').val();
                    startMonthField.val(firstMonth).trigger('change');
                    startAmountField.val(0).trigger('change');
                    actionField.val(0).trigger('change');
                    amountField.val('').trigger('change');
                    limitAmountField.val('').trigger('change');
                }

                panel.find('input.attr').val(attr);
                panel.find('input.row_id').val(rowID);
                panel.css("top", top + "px");
                panel.slideDown(150);
            });

            // Hide autofill panel
            $(document).on('click', '.cancel-edit-autofill-js', function(e) {
                const panel = $(panelID);

                panel.find("input.amount-input, input.amount-avg-input").val("");
                panel.slideUp(150);
            });

            // Change select "action"
            $(document).on('change', '#autofill-action', function(e) {
                const panel = $(panelID);
                const amountField = panel.find('#autofill-amount');
                const limitAmountField = panel.find('#autofill-limit_amount');
                const label = limitAmountField.closest('div').find('.label');
                const tooltip = label.find('.tooltip2');

                if ($(this).val() === '0') {
                    amountField.val('').attr('disabled', true);
                    limitAmountField.val('').attr('disabled', true);
                } else {
                    amountField.removeAttr('disabled');
                    limitAmountField.removeAttr('disabled');
                }

                if ($(this).val() === '2') {
                    label.find('.dynamic-label').text('Мин. значение');
                    tooltip.tooltipster('content', '<span>В данном поле вы можете указать значение ниже которого не может быть значение в ячейке</span>');
                } else {
                    label.find('.dynamic-label').text('Макс. значение');
                    tooltip.tooltipster('content', '<span>В данном поле вы можете указать значение выше которого не может быть значение в ячейке</span>');
                }
            });

            // Save autofill
            $(document).on('click', '.complete-auto-planning-item', function(e) {
                const panel = $(panelID);
                $(this).prop('disabled', true);
                that._reloadPjax({
                    floorMap: that._getFloorMap(),
                    autofill: that._getAutofillData(panel),
                });
                //panel.slideUp(150);
            });
        },
        bindTaxEvents: function() {
            const that = this;
            const panelID = that.panel.taxCost;

            // Show tax-cost panel
            $(document).on('click', '.edit-tax-js', function(e) {
                const panel = $(panelID);
                const line = $(this).closest("tr");
                const top = Number(line.offset().top) - 220;
                const attr = $(line).data('attr');

                const panelData = that.panelData.taxCost['tax'];
                const relationField = panel.find('#financeplanexpenditureitem-tax_relation_type');
                const actionField = panel.find('#financeplanexpenditureitem-action');
                const actionSymbol = panel.find('.action-symbol');

                if (panel.is(':visible')) {
                    panel.slideUp(150);
                    return;
                } else {
                    that._closeAllPanels();
                }

                if (that.panelData.taxCost['tax']) {
                    relationField.val(panelData.relation).trigger('change');
                    actionField.val(panelData.action).trigger('change');
                    actionSymbol.html(actionSymbol.data('relation-' + panelData.relation) || '');
                } else {
                    relationField.val(0).trigger('change');
                    actionField.val('').trigger('change');
                    actionSymbol.html('');
                }

                panel.find('input.attr').val(attr);
                panel.css("top", top + "px");
                panel.slideDown(150);
            });

            // Hide tax-cost panel
            $(document).on('click', '.cancel-edit-tax-js', function(e) {
                const panel = $(panelID);
                panel.slideUp(150);
            });

            // Send tax-cost form
            $(document).on('submit', '#form-edit-tax', function() {
                const form = $(this);
                $.post(this.action, form.serialize(), function (data) {
                    that._reloadPjax({
                        floorMap: that._getFloorMap(),
                    });
                });
                return false;
            });

            // Change tax relation type
            $(document).on('change', '#financeplanexpenditureitem-tax_relation_type', function() {
                const panel = $(panelID);
                const actionLabel = panel.find('.action-label');
                const actionSymbol = panel.find('.action-symbol');
                actionLabel.html(actionLabel.data('relation-' + this.value) || '');
                actionSymbol.html(actionSymbol.data('relation-' + this.value) || '');
            });
        },
        bindOddsIncomeEvents: function() {
            const that = this;
            const panelID = that.panel.oddsIncome;

            // Show odds-income panel
            $(document).on('click', '.edit-odds-income-js', function(e) {
                const panel = $(panelID);
                const line = $(this).closest("tr");
                const top = Number(line.offset().top) - 220;
                const attr = $(line).data('attr');
                const rowID = $(line).data('row_id');
                const rowType = $(line).data('row_type');

                const panelData = that.panelData.odds.income[rowID];
                const itemField = panel.find('#odds-income_item_id');
                const existsItemField = panel.find('#odds-exists_income_item_id');
                const deleteButton = panel.find('.delete-odds-income-js');

                if (panel.is(':visible')) {
                    panel.slideUp(150);
                    return;
                } else {
                    that._closeAllPanels();
                }

                if (rowID && that.panelData.odds.income[rowID]) {
                    itemField.prop('disabled', true).closest('.toggled-element').hide();
                    existsItemField.val(panelData.name).closest('.toggled-element').show();
                    deleteButton.show();
                } else {
                    itemField.prop('disabled', false).closest('.toggled-element').show();
                    existsItemField.val('').closest('.toggled-element').hide();
                    deleteButton.hide();
                }

                // filter items by block type
                that._filterOddsSelect2(panelID, rowType, attr);

                panel.find('input.attr').val(attr);
                panel.find('input.row_id').val(rowID);
                panel.find('input.row_type').val(rowType);
                panel.css("top", top + "px");
                panel.slideDown(150);
            });

            // Hide odds-income panel
            $(document).on('click', '.cancel-edit-odds-income-js', function(e) {
                const panel = $(panelID);
                panel.slideUp(150);
            });

            // Send odds-income form
            $(document).on('submit', '#form-edit-odds-income', function() {
                const form = $(this);
                const itemId = Number($('#odds-income_item_id').val());
                if (that.credit.trigger.incomeItemIds.indexOf(itemId) !== -1) {
                    that.credit.showModal('create');
                    return false;
                }
                $.post(this.action, form.serialize(), function (data) {
                    that._reloadPjax({
                        floorMap: that._getFloorMap(),
                    });
                });
                return false;
            });

            // Delete odds-income item
            $(document).on('click', '.delete-odds-income-js', function() {
                $('#confirm-delete-odds-income').modal('show');
            });

            // Delete odds-income item
            $(document).on('click', '#confirm-delete-odds-income-js', function() {
                const autofillPanel = $(that.panel.autofill);
                const form = $('#form-edit-odds-income');
                const attr = $(autofillPanel).find('input.attr').val();
                const rowID = $(autofillPanel).find('input.row_id').val();
                const rowType = $(autofillPanel).find('input.row_type').val();

                form.find('input.attr').val(attr);
                form.find('input.row_id').val(rowID);
                form.find('input.row_type').val(rowType);
                form.find('input.is_deleted').val(1);
                $.post(form[0].action, form.serialize(), function (data) {
                    that._reloadPjax({
                        floorMap: that._getFloorMap(),
                    });
                });
            });
        },
        bindOddsExpenseEvents: function() {
            const that = this;
            const panelID = that.panel.oddsExpense;

            // Show odds-expense panel
            $(document).on('click', '.edit-odds-expense-js', function(e) {
                const panel = $(panelID);
                const line = $(this).closest("tr");
                const top = Number(line.offset().top) - 220;
                const attr = $(line).data('attr');
                const rowID = $(line).data('row_id');
                const rowType = $(line).data('row_type');

                const panelData = that.panelData.odds.expense[rowID];
                const itemField = panel.find('#odds-expense_item_id');
                const existsItemField = panel.find('#odds-exists_expenditure_item_id');
                const deleteButton = panel.find('.delete-odds-expense-js');

                if (panel.is(':visible')) {
                    panel.slideUp(150);
                    return;
                } else {
                    that._closeAllPanels();
                }

                if (rowID && that.panelData.odds.expense[rowID]) {
                    itemField.prop('disabled', true).closest('.toggled-element').hide();
                    existsItemField.val(panelData.name).closest('.toggled-element').show();
                    deleteButton.show();
                } else {
                    itemField.prop('disabled', false).closest('.toggled-element').show();
                    existsItemField.val('').closest('.toggled-element').hide();
                    deleteButton.hide();
                }

                // filter items by block type
                that._filterOddsSelect2(panelID, rowType, attr);

                panel.find('input.attr').val(attr);
                panel.find('input.row_id').val(rowID);
                panel.find('input.row_type').val(rowType);
                panel.css("top", top + "px");
                panel.slideDown(150);
            });

            // Hide odds-expense panel
            $(document).on('click', '.cancel-edit-odds-expense-js', function(e) {
                const panel = $(panelID);
                panel.slideUp(150);
            });

            // Send odds-expense form
            $(document).on('submit', '#form-edit-odds-expense', function() {
                const form = $(this);
                const itemId = Number($('#odds-expenditure_item_id').val());
                if (that.credit.trigger.expenditureItemIds.indexOf(itemId) !== -1) {
                    that.credit.showModal('create');
                    return false;
                }
                if (that.balanceArticle.trigger.expenditureItemIds.indexOf(itemId) !== -1) {
                    that.balanceArticle.showModal('create');
                    return false;
                }
                // InvoiceExpenditureItem::ITEM_BORROWING_EXTRADITION
                if (itemId === 45) {
                    const receivedPercentCheckbox = $(that.userConfigCheckbox.receivedPercent);
                    if (!receivedPercentCheckbox.prop('checked')) {
                        receivedPercentCheckbox.click();
                    }
                }
                $.post(this.action, form.serialize(), function (data) {
                    that._reloadPjax({
                        floorMap: that._getFloorMap(),
                    });
                });
                return false;
            });

            // Delete odds-expense item
            $(document).on('click', '.delete-odds-expense-js', function() {
                $('#confirm-delete-odds-expense').modal('show');
            });

            // Delete odds-expense item
            $(document).on('click', '#confirm-delete-odds-expense-js', function() {
                const autofillPanel = $(that.panel.autofill);
                const form = $('#form-edit-odds-expense');
                const attr = $(autofillPanel).find('input.attr').val();
                const rowID = $(autofillPanel).find('input.row_id').val();
                const rowType = $(autofillPanel).find('input.row_type').val();

                form.find('input.attr').val(attr);
                form.find('input.row_id').val(rowID);
                form.find('input.row_type').val(rowType);
                form.find('input.is_deleted').val(1);
                $.post(form[0].action, form.serialize(), function (data) {
                    that._reloadPjax({
                        floorMap: that._getFloorMap(),
                    });
                });
            });
        },
        bindTableEvents: function() {
            const that = this;

            // Toggle column
            $(that.pjaxContainer).on('click', '[data-collapse-tiger]', function() {
                var target = $(this).data('target');
                var collapseCount = $(this).data('columns-count') || 3;
                $(this).toggleClass('active');
                $('[data-id="'+target+'"][data-collapse-cell]').toggleClass('d-none');
                $('[data-id="'+target+'"][data-collapse-cell-total]').toggleClass('d-none');
                if ( $(this).hasClass('active') ) {
                    $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', collapseCount);
                } else {
                    $('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', '1');
                }

                // resize top scroll
                let tableWidth = $(this).closest(".scrollable-table.double-scrollbar-top").width();
                $(this).closest(".scrollable-table-container").find(".scrollable-table-bar > div").css("width", tableWidth);
            });

            // Toggle row
            $(that.pjaxContainer).on('click', '[data-collapse-row-tiger]', function() {
                const target = $(this).data('target');
                $(this).toggleClass('active');
                if ( $(this).hasClass('active') ) {
                    $('[data-id="'+target+'"]').removeClass('d-none');
                } else {
                    const level1 = $('[data-id="'+target+'"]');
                    // level 1
                    level1.addClass('d-none');
                    level1.find('[data-collapse-row-tiger]').removeClass('active');
                    level1.each(function(i, row) {
                        let level2 = $('[data-id="'+ $(row).find('[data-collapse-row-tiger]').data('target') +'"]');
                        // level 2
                        level2.addClass('d-none');
                        level2.find('[data-collapse-row-tiger]').removeClass('active');
                        level2.each(function(i, row) {

                            $('[data-id="'+ $(row).find('[data-collapse-row-tiger]').data('target') +'"]').addClass('d-none');
                        });
                    });
                }
                if ( $('[data-collapse-row-tiger].active').length <= 0 ) {
                    $('[data-collapse-all-tiger]').removeClass('active')
                } else {
                    $('[data-collapse-all-tiger]').addClass('active')
                }
            });

            // Toggle all rows
            $(that.pjaxContainer).on('click', '[data-collapse-all-tiger]', function() {
                const _this = $(that.pjaxContainer).find('button[data-collapse-all-tiger]');
                const table = $(that.pjaxContainer);
                const row = table.find('tr[data-id]');
                _this.toggleClass('active');
                if ( _this.hasClass('active') ) {
                    row.removeClass('d-none');
                    $(table).find('tbody .table-collapse-btn').addClass('active');
                } else {
                    row.addClass('d-none');
                    $(table).find('tbody .table-collapse-btn').removeClass('active');
                }
            });

            // Sanitize inputs
            $(document).on('paste change', '.amount-input', function(e) {
                const decimals = $(this).hasClass('int') ? 0 : 2;
                e.preventDefault();
                const value = e.type === 'paste'
                    ? (e.originalEvent || e).clipboardData.getData('text/plain')
                    : this.value;
                this.value = number_format(that._sanitize(value), decimals, ',', ' ');
            });

            $(document).on('keydown', '.amount-input', function(e) {
                if(e.keyCode === 13) $(this).blur();
            });

            $(document).on('blur', '.amount-input', function(e) {
                const decimals = $(this).hasClass('int') ? 0 : 2;
                this.value = number_format(that._sanitize(this.value), decimals, ',', ' ');
            });
        },
        bindGroupExpenseEvents: function() {
            const that = this;
            const panelID = that.panel.groupExpense;

            // Show group-expense panel
            $(document).on('click', '.edit-group-expense-js', function(e) {
                const panel = $(panelID);
                const line = $(this).closest("tr");
                const top = Number(line.offset().top) - 220;
                const attr = $(line).data('attr');
                const rowID = $(line).data('row_id');
                const rowType = $(line).data('row_type');

                if (panel.is(':visible')) {
                    panel.slideUp(150);
                    return;
                } else {
                    that._closeAllPanels();
                }

                panel.find('input.attr').val(attr);
                panel.find('input.row_id').val(rowID);
                panel.find('input.row_type').val(rowType);
                panel.css("top", top + "px");
                panel.slideDown(150);
            });

            // Hide group-expense panel
            $(document).on('click', '.cancel-edit-group-expense-js', function(e) {
                const panel = $(panelID);
                panel.slideUp(150);
            });

            // Send group-expense form
            $(document).on('submit', '#form-edit-group-expense', function() {
                const form = $(this);
                $.post(this.action, form.serialize(), function (data) {
                    that._reloadPjax({
                        floorMap: that._getFloorMap(),
                    });
                });
                return false;
            });

            // Delete group-expense item
            $(document).on('click', '.delete-group-expense-js', function() {
                $('#confirm-delete-group-expense').modal('show');
            });

            // Delete group-expense item
            $(document).on('click', '#confirm-delete-group-expense-js', function() {
                const autofillPanel = $(that.panel.autofill);
                const form = $('#form-edit-group-expense');
                const attr = $(autofillPanel).find('input.attr').val();
                const rowID = $(autofillPanel).find('input.row_id').val();
                const rowType = $(autofillPanel).find('input.row_type').val();

                form.find('input.attr').val(attr);
                form.find('input.row_id').val(rowID);
                form.find('input.row_type').val(rowType);
                form.find('input.is_deleted').val(1);
                $.post(form[0].action, form.serialize(), function (data) {
                    that._reloadPjax({
                        floorMap: that._getFloorMap(),
                    });
                });
            });

            // Change select "action" (new row autofill)
            $(document).on('change', '#group-new-autofill-action', function(e) {
                const panel = $(panelID);
                const amountField = panel.find('#group-new-autofill-amount');
                const limitAmountField = panel.find('#group-new-autofill-limit_amount');
                const label = limitAmountField.closest('div').find('.label');
                const tooltip = label.find('.tooltip2');

                if ($(this).val() === '0') {
                    amountField.val('').attr('disabled', true);
                    limitAmountField.val('').attr('disabled', true);
                } else {
                    amountField.removeAttr('disabled');
                    limitAmountField.removeAttr('disabled');
                }

                if ($(this).val() === '2') {
                    label.find('.dynamic-label').text('Мин. значение');
                    tooltip.tooltipster('content', '<span>В данном поле вы можете указать значение ниже которого не может быть значение в ячейке</span>');
                } else {
                    label.find('.dynamic-label').text('Макс. значение');
                    tooltip.tooltipster('content', '<span>В данном поле вы можете указать значение выше которого не может быть значение в ячейке</span>');
                }
            });
        },
        bindOddsSystemIncomeEvents: function() {
            const that = this;
            const panelID = that.panel.oddsSystemIncome;

            // Show odds-system-income panel
            $(document).on('click', '.edit-odds-system-income-js', function(e) {
                const panel = $(panelID);
                const line = $(this).closest("tr");
                const top = Number(line.offset().top) - 220;
                const attr = $(line).data('attr');
                const rowID = $(line).data('row_id');
                const rowPaymentType = $(line).data('row_payment_type') || 1;

                const panelData = that.panelData.oddsSystem.income[rowID];
                const actionField = panel.find('.action-input');
                const paymentDelayField = panel.find('.payment_delay-select');
                const itemName = panel.find('.toggle-name');
                const actionLabel = panel.find('.toggle-action-label');
                const actionTooltip = panel.find('.toggle-action-tooltip');

                if (panel.is(':visible')) {
                    panel.slideUp(150);
                    return;
                } else {
                    that._closeAllPanels();
                }

                if (rowID && that.panelData.oddsSystem.income[rowID]) {
                    actionField.val(panelData.action).trigger('change');
                    paymentDelayField.val(panelData.payment_delay).trigger('change');
                } else {
                    actionField.val('').trigger('change');
                    paymentDelayField.val('').trigger('change');
                }

                if (Number(rowPaymentType) === 1) {
                    paymentDelayField.closest('.toggle-field').hide();
                } else {
                    paymentDelayField.closest('.toggle-field').show();
                }

                itemName.val(itemName.data('payment_type-' + rowPaymentType));
                actionLabel.html(actionLabel.data('payment_type-' + rowPaymentType));
                $(actionTooltip).tooltipster('content', actionTooltip.data('payment_type-' + rowPaymentType));

                panel.find('input.attr').val(attr);
                panel.find('input.row_id').val(rowID);
                panel.css("top", top + "px");
                panel.slideDown(150);
            });

            // Hide odds-system-income panel
            $(document).on('click', '.cancel-edit-odds-system-income-js', function(e) {
                const panel = $(panelID);
                panel.slideUp(150);
            });

            // Send odds-system-income form
            $(document).on('submit', '#form-edit-odds-system-income', function() {
                const form = $(this);
                $.post(this.action, form.serialize(), function (data) {
                    that._reloadPjax({
                        floorMap: that._getFloorMap(),
                    });
                });
                return false;
            });
        },
        bindOddsSystemExpenseEvents: function() {
            const that = this;
            const panelID = that.panel.oddsSystemExpense;

            // Show odds-system-expense panel
            $(document).on('click', '.edit-odds-system-expense-js', function(e) {
                const panel = $(panelID);
                const line = $(this).closest("tr");
                const top = Number(line.offset().top) - 220;
                const attr = $(line).data('attr');
                const rowID = $(line).data('row_id');
                const rowPaymentType = $(line).data('row_payment_type') || 1;

                const panelData = that.panelData.oddsSystem.expense[rowID];
                const actionField = panel.find('.action-input');
                const paymentDelayField = panel.find('.payment_delay-select');
                const itemName = panel.find('.toggle-name');
                const actionLabel = panel.find('.toggle-action-label');
                const actionTooltip = panel.find('.toggle-action-tooltip');

                if (panel.is(':visible')) {
                    panel.slideUp(150);
                    return;
                } else {
                    that._closeAllPanels();
                }

                if (rowID && that.panelData.oddsSystem.expense[rowID]) {
                    actionField.val(panelData.action).trigger('change');
                    paymentDelayField.val(panelData.payment_delay).trigger('change');
                } else {
                    actionField.val('').trigger('change');
                    paymentDelayField.val('').trigger('change');
                }

                if (Number(rowPaymentType) === 1) {
                    paymentDelayField.closest('.toggle-field').hide();
                } else {
                    paymentDelayField.closest('.toggle-field').show();
                }

                itemName.val(itemName.data('payment_type-' + rowPaymentType));
                actionLabel.html(actionLabel.data('payment_type-' + rowPaymentType));
                $(actionTooltip).tooltipster('content', actionTooltip.data('payment_type-' + rowPaymentType));

                panel.find('input.attr').val(attr);
                panel.find('input.row_id').val(rowID);
                panel.css("top", top + "px");
                panel.slideDown(150);
            });

            // Hide odds-system-expense panel
            $(document).on('click', '.cancel-edit-odds-system-expense-js', function(e) {
                const panel = $(panelID);
                panel.slideUp(150);
            });

            // Send odds-system-expense form
            $(document).on('submit', '#form-edit-odds-system-expense', function() {
                const form = $(this);
                $.post(this.action, form.serialize(), function (data) {
                    that._reloadPjax({
                        floorMap: that._getFloorMap(),
                    });
                });
                return false;
            });
        },
        bindConfigEvents: function() {
            const that = this;
            const optCheckboxes = [
                '#config-finance_model_amortization',
                '#config-finance_model_ebit',
                '#config-finance_model_percent_received',
                '#config-finance_model_percent_paid'
            ];
            const toggleRows = [
                'tr.rel_finance_model_amortization',
                'tr.rel_finance_model_ebit',
                'tr.rel_finance_model_percent_received',
                'tr.rel_finance_model_percent_paid'
            ];
            // related checkboxes
            $(optCheckboxes.join(',')).on('change', function() {
                let hidden = true;
                $(optCheckboxes.join(',')).each(function(i,ch) {
                    if ($(ch).prop('checked'))
                        hidden = false;
                });
                if (hidden) {
                    setTimeout(function() {$(that.pjaxContainer).find(toggleRows.join(',')).addClass('hidden')}, 750);
                } else {
                    setTimeout(function() {$(that.pjaxContainer).find(toggleRows.join(',')).removeClass('hidden')}, 750);
                }
            });
            // ebit enable/disable
            $(that.userConfigCheckbox.amortization).on('change', function() {
                const ebitCheckbox = $(that.userConfigCheckbox.ebit);
                const amortizationCheckbox = $(this);
                if (amortizationCheckbox.prop('checked')) {
                    ebitCheckbox.prop('disabled', false).uniform();
                } else {
                    if (ebitCheckbox.prop('checked')) {
                        ebitCheckbox.click();
                    }
                    ebitCheckbox.prop('disabled', true).uniform();
                }
            });
        },
        _getFloorMap: function() {
            let floorMap = {};
            $(this.tableContainerClone).find('[data-collapse-row-tiger]').each(function(i,v) {
                floorMap[$(v).data('target')] = $(v).hasClass('active') ? 1 : 0;
            });
            $(this.tableContainer).find('[data-collapse-tiger]').each(function(i,v) {
                floorMap[$(v).data('target')] = $(v).hasClass('active') ? 1 : 0;
            });

            return floorMap;
        },
        _getUserData: function() {
            let userData = {};
            $(this.tableContainer + " .custom-value").each(function(i,v) {
                const re = /([^\[]+)\[([^\]]+)\]/g;
                const attr = re.exec($(v).attr('name'));

                if (typeof userData[attr[1]] == "undefined")
                    userData[attr[1]] = {};

                userData[attr[1]][attr[2]] = $(v).val();
            });

            return userData;
        },
        _getGroupUserData: function() {
            let userData = {};
            $(this.tableContainer + " .custom-value-group").each(function(i,v) {
                const re = /([^\[]+)\[([^\]]+)\]/g;
                const attr = re.exec($(v).attr('name'));

                if (typeof userData[attr[1]] == "undefined")
                    userData[attr[1]] = {};

                userData[attr[1]][attr[2]] = $(v).val();
            });

            return userData;
        },
        _getAutofillData: function(panel) {
            return {
                attr: panel.find('input.attr').val(),
                row_id: panel.find('input.row_id').val(),
                start_month: $('#autofill-start_month', panel).val(),
                start_amount: $('#autofill-start_amount', panel).val(),
                amount: $('#autofill-amount', panel).val(),
                limit_amount: $('#autofill-limit_amount', panel).val(),
                action: $('#autofill-action', panel).val(),
            };
        },
        _sanitize: function(val) {
            return String(val).replace(/\s/g, '').replace(',', '.');
        },
        _reloadPjax: function(data) {
            const that = this;
            const url = this._getPjaxUrl();

            return $.pjax.reload(that.pjaxContainer, {
                "type": "post",
                "url": url + "?group=" + that.groupID + "&plan=" + that.planID,
                "data": data,
                "push": false,
                "replace": false,
                "scrollTo": false,
                "timeout": 60000
            });
        },
        _getPjaxUrl: function() {
            switch (this.tabID) {
                case this.tabs.expenses:
                    return 'update-expenses-ajax';
                case this.tabs.plan:
                default:
                    return 'update-plan-ajax';
            }
        },
        _closeAllPanels: function() {
            const that = this;
            $(that.pjaxContainer).find('.finance-plan-float-panel:visible').slideUp(150);
        },
        _filterOddsSelect2: function(panelID, rowType, flowAttr)
        {
            const that = this;
            const itemsByFlowOfFundsType = that.select2Data.odds.itemsByFlowOfFundsType;
            const panel = $(panelID);
            const select2 = panel.find('select#odds-expenditure_item_id, select#odds-income_item_id').first();
            let items = [];

            if (itemsByFlowOfFundsType[rowType] && itemsByFlowOfFundsType[rowType][flowAttr]) {
                items = itemsByFlowOfFundsType[rowType][flowAttr];
                select2.find('option').each(function(i, opt) {
                    if (opt.value !== 'add-modal-income-item' && opt.value !== 'add-modal-expenditure-item')
                        $(opt).prop('disabled', items.indexOf(String(opt.value)) === -1);
                });
                select2.find('optgroup').each(function(i, optGroup) {
                    let hasItems = false;
                    $(optGroup).find('option').each(function(i, opt) {
                        if (!$(opt).prop('disabled')) {
                            hasItems = true;
                            return false;
                        }
                    });
                    $(optGroup).prop('disabled', !hasItems);
                });
            } else {
                // disable filter if array itemsByFlowOfFundsType is not initialized
                select2.find('option').each(function(i, opt) {
                    $(opt).prop('disabled', false);
                });
                return;
            }

            select2.trigger('change.select2');
        }
    };