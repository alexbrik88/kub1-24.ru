FinanceModelEditShop = {
    TYPE_SHOP: '1',
    TYPE_INTERNET_SHOP: '2',
    init: function() {
        this.bindEvents();
        this.recalc();
    },
    bindEvents: function() {

        const that = this;

        // tabs
        $('#finance-model-shop-type').on('change', function() {
            const type = $(this).val();
            const tabShop = $('.shop-type-' + that.TYPE_SHOP);
            const tabInternetShop = $('.shop-type-' + that.TYPE_INTERNET_SHOP);
            const dropdownFillBy = $('.dropdown-fill-by');
            if (type === that.TYPE_SHOP) {
                tabShop.slideDown();
                tabInternetShop.slideUp();
            }
            if (type === that.TYPE_INTERNET_SHOP) {
                tabShop.slideUp();
                tabInternetShop.slideDown();
            }
            tabShop.find('input').attr('disabled', $(this).val() === that.TYPE_INTERNET_SHOP);
            tabInternetShop.find('input').attr('disabled', $(this).val() === that.TYPE_SHOP);
            dropdownFillBy.hide().filter('[data-type="'+type+'"]').show();
            that.recalc();
        });

        // inputs
        $('.custom-value')
            .on('paste change', function (e) {
                const decimals = $(this).hasClass('int') ? 0 : 2;
                e.preventDefault();
                const value = e.type === 'paste'
                    ? (e.originalEvent || e).clipboardData.getData('text/plain')
                    : this.value;
                this.value = number_format(that._sanitize(value), decimals, ',', ' ');
                that.recalc();
            })
            // press "Enter"
            .on('keydown', function(e){
                if(e.keyCode === 13) $(this).blur();
            })
            .on('blur', function(e){
                const decimals = $(this).hasClass('int') ? 0 : 2;
                this.value = number_format(that._sanitize(this.value), decimals, ',', ' ');
            });

        // form
        $('#finance-model-shop-form').on('keydown', function(e) {
            if(e.keyCode === 13) return false;
        });

        // fill-by-shop
        $('.button-fill-by').on('click', function() {
            const modelId = $(this).data('model-id');
            const shopId = $(this).data('shop-id');
            if (modelId && shopId)
                that._fillShopByShop(modelId, shopId);
        });
    },
    recalc: function() {
        const shopType = $('#finance-model-shop-type').val();
        if (shopType === this.TYPE_SHOP)
            this._recalcShop();
        if (shopType === this.TYPE_INTERNET_SHOP)
            this._recalcInternetShop();

        if ($('#shop_id').val()) {
            this._recalcCostsByRevenue();
        }
    },
    _recalcCostsByRevenue: function()
    {
        const that = this;
        const table = $('.finance-model-shop-month tbody');
        let tmp = {
            'revenue': [],
            'total_costs_variable': [],
            'margin': [],
            'marginality': [],
            'total_costs_fixed': [],
            'total_operating_profit': [],
            'income': [],
            'expense': [],
            'total_ebidta': [],
            'received_percents': [],
            'paid_percents': [],
            'amortization': [],
            'total_profit_before_tax': [],
            'tax_rate': [],
            'total_net_profit': [],
            'profit_distribution': [],
            'total_undistributed_profit': [],
        };

        $(table).find('tr').each(function(row, tr) {

            let attr = $(tr).data('attr');

            $(tr).find('td').each(function(col, td) {
                if (col === 0)
                    return true;

                let pos = col - 1;

                if (attr === 'revenue') {
                    tmp[attr][pos] = Number(that._sanitize($(td).text()) || 0);

                } else if (attr === 'total_costs_variable') {
                    let percent = Number($(tr).data('percent')) / 100;
                    tmp[attr][pos] = tmp['revenue'][pos] * percent;
                    $(td).html(number_format(tmp[attr][pos], 2, ',', ' '));

                } else if (attr === 'margin') {
                    tmp[attr][pos] = tmp['revenue'][pos] - tmp['total_costs_variable'][pos];
                    $(td).html(number_format(tmp[attr][pos], 2, ',', ' '));

                } else if (attr === 'marginality') {
                    tmp[attr][pos] = tmp['margin'][pos] / ((tmp['revenue'][pos] !== 0) ? tmp['revenue'][pos] : 1);
                    $(td).html(number_format(tmp[attr][pos] * 100, 2, ',', ' ') + '%');

                } else if (attr === 'total_costs_fixed') {
                    tmp[attr][pos] = Number(that._sanitize($(td).text()) || 0);

                } else if (attr === 'total_operating_profit') {
                    tmp[attr][pos] = tmp['margin'][pos] - tmp['total_costs_fixed'][pos];
                    $(td).html(number_format(tmp[attr][pos], 2, ',', ' '));

                } else if (attr === 'income' || attr === 'expense') {
                    tmp[attr][pos] = Number(that._sanitize($(td).text()) || 0);

                } else if (attr === 'total_ebidta') {
                    tmp[attr][pos] = tmp['total_operating_profit'][pos] + (tmp['income'][pos] - tmp['expense'][pos]);
                    $(td).html(number_format(tmp[attr][pos], 2, ',', ' '));

                } else if (attr === 'received_percents' || attr === 'paid_percents' || attr === 'amortization') {
                    tmp[attr][pos] = Number(that._sanitize($(td).text()) || 0);

                } else if (attr === 'total_profit_before_tax') {
                    tmp[attr][pos] = tmp['total_ebidta'][pos] + (tmp['received_percents'][pos] - tmp['paid_percents'][pos] - tmp['amortization'][pos]);
                    $(td).html(number_format(tmp[attr][pos], 2, ',', ' '));

                } else if (attr === 'tax_rate') {
                    tmp[attr][pos] = Number(that._sanitize($(td).text().replace(/[^0-9,.]/g, '')) || 0);

                } else if (attr === 'total_net_profit') {
                    tmp[attr][pos] = tmp['total_profit_before_tax'][pos] * (1 - tmp['tax_rate'][pos] / 100);
                    $(td).html(number_format(tmp[attr][pos], 2, ',', ' '));

                } else if (attr === 'profit_distribution') {
                    tmp[attr][pos] = Number(that._sanitize($(td).text()) || 0);

                } else if (attr === 'total_undistributed_profit') {
                    tmp[attr][pos] = tmp['total_net_profit'][pos] - tmp['profit_distribution'][pos];
                    $(td).html(number_format(tmp[attr][pos], 2, ',', ' '));
                }
            });
        });

        // console.log(tmp);
    },
    _recalcShop: function() {
        const that = this;
        const table = $('.finance-model-shop-month[data-type="1"] tbody');
        let tmp = {
            'check_count': [],
            'average_check': []
        };
        $(table).find('tr').each(function(row, tr) {

            if ($(tr).hasClass('empty-row'))
                return false;

            let attr = $(tr).data('attr');
            let decimals = (attr === 'check_count') ? 0 : 2;
            let totalCount = 0;
            let totalAmount = 0;
            let quarterAmount = 0;
            let quarterCount = 0;
            let amount = 0;

            if (attr === 'average_check') {
                $(tr).find('td').each(function(col, td) {
                    if ($(td).hasClass('month-block')) {
                        amount = $(td).find('input').val();
                        totalCount += Number(1);
                        totalAmount += Number(that._sanitize(amount));
                        quarterCount += Number(1);
                        quarterAmount += Number(that._sanitize(amount));
                        tmp[attr][col] = Number(that._sanitize(amount));
                    } else if ($(td).hasClass('quarter-block')) {
                        $(td).html(number_format(quarterAmount / quarterCount, decimals, ',', ' '));
                        quarterAmount = quarterCount = 0;
                    } else if ($(td).hasClass('total-block')) {
                        $(td).html(number_format(totalAmount / totalCount, decimals, ',', ' '));
                    }
                });
                $(tr).find('td.avg').html(number_format(totalAmount / totalCount, decimals, ',', ' '));
            }
            else if (attr === 'check_count') {
                $(tr).find('td').each(function(col, td) {
                    if ($(td).hasClass('month-block')) {
                        amount = $(td).find('input').val();
                        totalAmount += Number(that._sanitize(amount));
                        quarterAmount += Number(that._sanitize(amount));
                        tmp[attr][col] = Number(that._sanitize(amount));
                    } else if ($(td).hasClass('quarter-block')) {
                        $(td).html(number_format(quarterAmount, decimals, ',', ' '));
                        quarterAmount = 0;
                    } else if ($(td).hasClass('total-block')) {
                        $(td).html(number_format(totalAmount, decimals, ',', ' '));
                    }
                });
                $(tr).find('td.avg').html(number_format(totalAmount / totalCount, decimals, ',', ' '));
            }
            else if (attr === 'revenue') {
                $(tr).find('td').each(function(col, td) {
                    if ($(td).hasClass('month-block')) {
                        amount = tmp['check_count'][col] * tmp['average_check'][col];
                        totalCount += 1;
                        totalAmount += amount;
                        quarterAmount += amount;
                        $(td).html(number_format(amount, decimals, ',', ' '))
                    } else if ($(td).hasClass('quarter-block')) {
                        $(td).html(number_format(quarterAmount, decimals, ',', ' '));
                        quarterAmount = 0;
                    } else if ($(td).hasClass('total-block')) {
                        $(td).html(number_format(totalAmount, decimals, ',', ' '));
                    }
                });
                $(tr).find('td.avg').html(number_format(totalAmount / totalCount, decimals, ',', ' '));
            }
        });
    },
    _recalcInternetShop: function() {
        const that = this;
        const table = $('.finance-model-shop-month[data-type="2"] tbody');
        let tmp = {
            'visit_count': [],
            'conversion': [],
            'check_count': [],
            'average_check': []
        };
        $(table).find('tr').each(function(row, tr) {
            let attr = $(this).data('attr');
            let decimals = (attr === 'check_count' || attr === 'visit_count') ? 0 : 2;
            let totalCount = 0;
            let totalAmount = 0;
            let quarterAmount = 0;
            let quarterCount = 0;
            let amount = 0;

            if (attr === 'average_check' || attr === 'conversion') {
                $(tr).find('td').each(function(col, td) {
                    if ($(td).hasClass('month-block')) {
                        amount = $(td).find('input').val();
                        totalCount += Number(1);
                        totalAmount += Number(that._sanitize(amount));
                        quarterCount += Number(1);
                        quarterAmount += Number(that._sanitize(amount));
                        tmp[attr][col] = Number(that._sanitize(amount));
                    } else if ($(td).hasClass('quarter-block')) {
                        $(td).html(number_format(quarterAmount / quarterCount, decimals, ',', ' '));
                        quarterAmount = quarterCount = 0;
                    } else if ($(td).hasClass('total-block')) {
                        $(td).html(number_format(totalAmount / totalCount, decimals, ',', ' '));
                    }
                });
                $(tr).find('td.avg').html(number_format(totalAmount / totalCount, decimals, ',', ' '));
            }
            else if (attr === 'visit_count') {
                $(tr).find('td').each(function(col, td) {
                    if ($(td).hasClass('month-block')) {
                        amount = $(td).find('input').val();
                        totalCount += Number(1);
                        totalAmount += Number(that._sanitize(amount));
                        quarterAmount += Number(that._sanitize(amount));
                        tmp[attr][col] = Number(that._sanitize(amount));
                    } else if ($(td).hasClass('quarter-block')) {
                        $(td).html(number_format(quarterAmount, decimals, ',', ' '));
                        quarterAmount = 0;
                    } else if ($(td).hasClass('total-block')) {
                        $(td).html(number_format(totalAmount, decimals, ',', ' '));
                    }
                });
                $(tr).find('td.avg').html(number_format(totalAmount / totalCount, decimals, ',', ' '));
                //$(tr).find('td.total-block').html(number_format(totalAmount, decimals, ',', ' '));
            }
            else if (attr === 'check_count') {
                $(tr).find('td').each(function(col, td) {
                    if ($(td).hasClass('month-block')) {
                        amount = tmp['visit_count'][col] * (1/100 * tmp['conversion'][col]);
                        totalCount += 1;
                        totalAmount += amount;
                        quarterAmount += amount;
                        tmp[attr][col] = amount;
                        $(td).html(number_format(amount, decimals, ',', ' '));
                    } else if ($(td).hasClass('quarter-block')) {
                        $(td).html(number_format(quarterAmount, decimals, ',', ' '));
                        quarterAmount = 0;
                    } else if ($(td).hasClass('total-block')) {
                        $(td).html(number_format(totalAmount, decimals, ',', ' '));
                    }
                });
                $(tr).find('td.avg').html(number_format(totalAmount / totalCount, decimals, ',', ' '));
                //$(tr).find('td.total-block').html(number_format(totalAmount, decimals, ',', ' '));
            }
            else if (attr === 'revenue') {
                $(tr).find('td').each(function(col, td) {
                    if ($(td).hasClass('month-block')) {
                        amount = tmp['check_count'][col] * tmp['average_check'][col];
                        totalCount += 1;
                        totalAmount += amount;
                        quarterAmount += amount;
                        $(td).html(number_format(amount, decimals, ',', ' '))
                    } else if ($(td).hasClass('quarter-block')) {
                        $(td).html(number_format(quarterAmount, decimals, ',', ' '));
                        quarterAmount = 0;
                    } else if ($(td).hasClass('total-block')) {
                        $(td).html(number_format(totalAmount, decimals, ',', ' '));
                    }
                });
                $(tr).find('td.avg').html(number_format(totalAmount / totalCount, decimals, ',', ' '));
                //$(tr).find('td.total-block').html(number_format(totalAmount, decimals, ',', ' '));
            }
        });
    },
    _fillShopByShop: function(modelId, shopId) {
        const that = this;
        const url = '/analytics/finance-model/get-shop-months-ajax';
        $.ajax(url + '?model_id=' + modelId + '&shop_id=' + shopId, {
            method: 'POST',
            data: null,
            contentType: false,
            processData: false,
            success: function (data) {
                const table = $('.finance-model-shop-month:visible');
                let input, decimals;
                if (data) {
                    $.each(data, function(attr, months) {
                        $.each(months, function(month, value) {
                            value = Number(value) / (attr === 'average_check' ? 100 : 1);
                            input = $(table).find('input[name="'+attr+'['+month+']"]');
                            console.log(input);
                            if (input.length === 1) {
                                decimals = input.hasClass('int') ? 0 : 2;
                                input.val(number_format(that._sanitize(value), decimals, ',', ' '));
                            }
                        });
                    });
                    that.recalc();
                    that._showFlash('Данные заполнены');
                }
            }
        });
    },
    _showFlash: function(text) {
        if (window.toastr) {
            window.toastr.success(text, "", {
                "closeButton": true,
                "showDuration": 1000,
                "hideDuration": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 1000,
                "escapeHtml": false
            });
        }
    },
    _sanitize: function(val) {
        return String(val).replace(/\s/g, '').replace(',', '.');
    },
};

PlanningAutoFillModelEditOnFly = {
    panelID: "#update-planning-auto-fill-item-panel",
    monthData: [],
    init: function() {
        this.bindEvents();
        this.monthData = jQuery.parseJSON($(this.panelID + " #auto-fill-month-start-planning").attr("data-month"));
    },
    bindEvents: function() {
        const that = this;

        $(document).on('click', '#marketing-planning-form .nav-link', function(e) {
            that.monthData = jQuery.parseJSON($(that.panelID + " #auto-fill-month-start-planning").attr("data-month"));
        });

        $(document).on('click', '.edit-costs-js', function(e) {
            const panel = $(that.panelID);
            const line = $(this).closest("tr");
            const top = Number(line.offset().top) - 160;
            const attr = $(line).data('attr');

            if (panel.is(':visible')) {
                $('.cancel-edit-costs-js').click();
                return;
            }

            panel.find('input.attr').val(attr).trigger('change');
            that.getAmountByMonth(panel.find('#auto-fill-month-start-planning').val());

            let monthData = jQuery.extend({}, that.monthData);
            if (attr === 'churn_rate') {
                let firstMonthKey = Object.keys(monthData).reduce(function (r, a, i) {
                    return !i || +monthData[a] < +monthData[r] ? a : r;
                }, undefined);
                delete monthData[firstMonthKey];
            }

            let monthStartInput = panel.find('#auto-fill-month-start-planning');
            monthStartInput.find('option').remove();
            for (var prop in monthData) {
                monthStartInput.append('<option value="' + prop + '">' + monthData[prop] + '</option>');
            }

            const amountField = panel.find('#auto-fill-amount-planning');
            const limitAmountField = panel.find('#auto-fill-limit_amount-planning');
            let calculatingSetting = $(this).attr('data-calculating-setting');
            if (calculatingSetting !== '') {
                if (+calculatingSetting.action === 0) {
                    amountField.attr('disabled', true);
                    limitAmountField.attr('disabled', true);
                    amountField.val('');
                    limitAmountField.val('');
                } else {
                    amountField.removeAttr('disabled');
                    limitAmountField.removeAttr('disabled');
                }

                calculatingSetting = jQuery.parseJSON(calculatingSetting);
                panel.find('#auto-fill-month-start-planning').val(calculatingSetting.start_year + calculatingSetting.start_month);
                panel.find('#auto-fill-start_amount-planning').val(calculatingSetting.start_amount / 100).trigger('change');
                panel.find('#auto-fill-action-planning').val(calculatingSetting.action);
                amountField.val(calculatingSetting.amount / 100).trigger('change');
                limitAmountField.val(calculatingSetting.limit_amount / 100).trigger('change');
            } else {
                let newMonthStartValue = Object.keys(monthData).reduce(function (r, a, i) {
                    return !i || +monthData[a] < +monthData[r] ? a : r;
                }, undefined);
                monthStartInput.val(newMonthStartValue);
                that.getAmountByMonth(newMonthStartValue);
                amountField.val('');
                limitAmountField.val('');
                panel.find('#auto-fill-action-planning').val(0).trigger('change');
            }

            panel.css("top", top + "px");
            panel.slideDown();
        });

        // hide panel
        $(document).on('click', '.cancel-edit-costs-js', function(e) {
            const panel = $(that.panelID);

            panel.find("input.amount-input, input.amount-avg-input").val("");
            panel.slideUp();
        });

        // inputs
        $(document).on('paste change', '.amount-input', function(e) {
            const decimals = $(this).hasClass('int') ? 0 : 2;
            e.preventDefault();
            const value = e.type === 'paste'
                ? (e.originalEvent || e).clipboardData.getData('text/plain')
                : this.value;
            this.value = number_format(that._sanitize(value), decimals, ',', ' ');
        });

        $(document).on('keydown', '.amount-input', function(e) {
            if(e.keyCode === 13) $(this).blur();
        });

        $(document).on('blur', '.amount-input', function(e) {
            const decimals = $(this).hasClass('int') ? 0 : 2;
            this.value = number_format(that._sanitize(this.value), decimals, ',', ' ');
        });

        $(document).on('change', '#auto-fill-month-start-planning', function(e) {
            that.getAmountByMonth($(this).val());
        });

        $(document).on('change', '#auto-fill-action-planning', function(e) {
            const panel = $(that.panelID);
            const amountField = panel.find('#auto-fill-amount-planning');
            const limitAmountField = panel.find('#auto-fill-limit_amount-planning');

            if (+$(this).val() === 0) {
                amountField.attr('disabled', true);
                limitAmountField.attr('disabled', true);
                amountField.val('');
                limitAmountField.val('');
            } else {
                amountField.removeAttr('disabled');
                limitAmountField.removeAttr('disabled');
            }

            const label = limitAmountField.closest('div').find('.label');
            const tooltip = label.find('.tooltip2');
            if (+$(this).val() === 2) {
                label.find('.dynamic-label').text('Мин. значение');
                tooltip.tooltipster('content', '<span>В данном поле вы можете указать значение ниже которого не может быть значение в ячейке</span>')
            } else {
                label.find('.dynamic-label').text('Макс. значение');
                tooltip.tooltipster('content', '<span>В данном поле вы можете указать значение выше которого не может быть значение в ячейке</span>')
            }
        });

        $(document).on('click', '.complete-auto-planning-item', function(e) {
            const panel = $(that.panelID);
            const channel = $('.tab-pane.active .planning-table').data('channel');
            const data = $('.tab-pane .custom-value, #update-planning-auto-fill-item-panel input, #update-planning-auto-fill-item-panel select').serialize();

            $(".scrollable-table-clone.table .custom-value").attr("disabled", true);
            $.pjax.reload('#channel-planning-table-pjax-' + channel, {
                'url': 'reload-channel-planning-table?id=' + $('#marketing-plan-id-input').val() + '&channel=' + channel,
                'type': 'post',
                'data': data,
                'push': false,
                'replace': false,
                'timeout': 10000,
                'scrollTo': false,
                'container': '#channel-planning-table-pjax' + channel,
            }).done(function() {
                $('.tab-pane.active .planning-table .global-field').each(function() {
                    let className = $(this).attr('data-class');
                    $('.' + className).val($(this).val());
                });
            });

            panel.slideUp();
        });
    },
    _sanitize: function(val) {
        return String(val).replace(/\s/g, '').replace(',', '.');
    },
    getAmountByMonth: function(itemId) {
        const panel = $(this.panelID);
        console.log('.' + panel.find('input.attr').val() + '-' + $('.tab-pane.active').find('.planning-table').data('channel') + '-' + itemId);
        const itemMonthValue = $('.' + panel.find('input.attr').val() + '-' + $('.tab-pane.active').find('.planning-table').data('channel') + '-' + itemId).val();

        panel.find('#auto-fill-start_amount-planning').val(itemMonthValue);
    },
}

FinanceModelEditCostsOnFly = {
    panelID: "#update-finance-model-item-panel",
    init: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        const that = this;
        // show panel
        $('.edit-costs-js').on('click', function() {
            const panel = $(that.panelID);
            const line = $(this).closest("tr");
            const wrap = $(this).closest(".wrap");
            const top = Number(line.offset().top) - Number(wrap.offset().top) + Number(line.height());
            const attr = $(line).data('attr');

            if (panel.is(':visible')) {
                $('.cancel-edit-costs-js').click();
                return;
            }

            panel.find('input.attr').val(attr);
            panel.find('input.amount-avg-input').val(line.find('td.avg').text().trim());
            panel.find('input.amount-input').each(function(i,input) {
                let month = $(input).data('month');
                let tableMonthCell = $(line).find('td[data-month="'+month+'"]');
                $(input).val(tableMonthCell.text().trim());
            });

            panel.css("top", top + "px");
            panel.slideDown();
        });
        // hide panel
        $('.cancel-edit-costs-js').on('click', function() {
            const panel = $(that.panelID);
            panel.find("input.amount-input, input.amount-avg-input").val("");
            panel.slideUp();
        });
        // inputs
        $('.amount-input, .amount-avg-input')
            .on('paste change', function (e) {
                const decimals = $(this).hasClass('int') ? 0 : 2;
                e.preventDefault();
                const value = e.type === 'paste'
                    ? (e.originalEvent || e).clipboardData.getData('text/plain')
                    : this.value;
                this.value = number_format(that._sanitize(value), decimals, ',', ' ');
            })
            // press "Enter"
            .on('keydown', function(e){
                if(e.keyCode === 13) $(this).blur();
            })
            .on('blur', function(e){
                const decimals = $(this).hasClass('int') ? 0 : 2;
                this.value = number_format(that._sanitize(this.value), decimals, ',', ' ');
            });
        // avg input
        $('.amount-avg-input')
            .on('keyup', function() {
                const panel = $(that.panelID);
                const avgVal = $(this).val();
                panel.find('input.amount-input').each(function(i,input) {
                    $(input).val(avgVal.trim());
                });
            })
            .on('blur', function() {
                const panel = $(that.panelID);
                const avgVal = $(this).val();
                panel.find('input.amount-input').each(function(i,input) {
                    $(input).val(avgVal.trim());
                });
            });
    },
    _sanitize: function(val) {
        return String(val).replace(/\s/g, '').replace(',', '.');
    },
};

FinanceModelEditShopCostsFixed = {
    form: '#finance-model-shop-form',
    init: function() {
        this.bindEvents();
        this.recalc();
    },
    bindEvents: function() {

        const that = this;

        // inputs
        $('.custom-value, .custom-value-avg')
            .on('paste change', function (e) {
                const decimals = $(this).hasClass('int') ? 0 : 2;
                e.preventDefault();
                const value = e.type === 'paste'
                    ? (e.originalEvent || e).clipboardData.getData('text/plain')
                    : this.value;
                this.value = number_format(that._sanitize(value), decimals, ',', ' ');
                that.recalc();
            })
            // press "Enter"
            .on('keydown', function(e){
                if(e.keyCode === 13) $(this).blur();
            })
            .on('blur', function(e){
                const decimals = $(this).hasClass('int') ? 0 : 2;
                this.value = number_format(that._sanitize(this.value), decimals, ',', ' ');
            });
        // avg input
        $('.custom-value-avg')
            .on('keyup', function() {
                const tr = $(this).closest('tr');
                const avgVal = $(this).val();
                $(tr).find('input.custom-value').each(function(i,input) {
                    $(input).val(avgVal.trim());
                });
            })
            .on('blur', function() {
                const tr = $(this).closest('tr');
                const avgVal = $(this).val();
                $(tr).find('input.custom-value').each(function(i,input) {
                    $(input).val(avgVal.trim());
                });
            });

        // form
        $(that.form).on('keydown', function(e) {
            if(e.keyCode === 13) return false;
        });
    },
    recalc: function() {

        const that = this;
        const table = $(that.form).find('table tbody');
        let tmp = {};
        $(table).find('tr').each(function(row, tr) {
            let attr = $(this).data('attr');
            let decimals = 2;
            let totalCount = 0;
            let totalAmount = 0;
            let amount = 0;

            if (attr === 'total') {
                $(tr).find('td').each(function(col, td) {
                    if (col > 1) {
                        amount = tmp[col];
                        totalCount += 1;
                        totalAmount += tmp[col];
                        $(td).html(number_format(amount, decimals, ',', ' '))
                    }
                });
                $(tr).find('td.avg').html(number_format(totalAmount / totalCount, decimals, ',', ' '));
            } else {
                $(tr).find('td').each(function(col, td) {
                    let input = $(td).find('input.custom-value');
                    if (typeof tmp[col] === 'undefined')
                        tmp[col] = 0;

                    if (input.length) {
                        amount = $(input).val();
                        totalCount += Number(1);
                        totalAmount += Number(that._sanitize(amount));
                        tmp[col] += Number(that._sanitize(amount));
                    }
                });
                $(tr).find('input.custom-value-avg').val(number_format(totalAmount / totalCount, decimals, ',', ' '));
            }
        });

    },
    _showFlash: function(text) {
        if (window.toastr) {
            window.toastr.success(text, "", {
                "closeButton": true,
                "showDuration": 1000,
                "hideDuration": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 1000,
                "escapeHtml": false
            });
        }
    },
    _sanitize: function(val) {
        return String(val).replace(/\s/g, '').replace(',', '.');
    },
};

FinanceModelEditShopCostsVariable = {
    MAX_PERCENT: 100,
    form: '#finance-model-shop-form',
    init: function() {
        this.bindEvents();
        this.recalc();
    },
    bindEvents: function() {

        const that = this;

        // inputs
        $('.custom-value-percent')
            .on('paste change', function (e) {
                const decimals = $(this).hasClass('int') ? 0 : 2;
                e.preventDefault();
                const value = e.type === 'paste'
                    ? (e.originalEvent || e).clipboardData.getData('text/plain')
                    : this.value;
                this.value = number_format(Math.min(that._sanitize(value), that.MAX_PERCENT), decimals, ',', ' ');
                that.recalc();
            })
            // press "Enter"
            .on('keydown', function(e){
                if(e.keyCode === 13) $(this).blur();
            })
            .on('keyup', function() {
                that.recalc();
            })
            .on('blur', function() {
                that.recalc();
            });

        // form
        $(that.form).on('keydown', function(e) {
            if(e.keyCode === 13) return false;
        });
    },
    recalc: function() {

        const that = this;
        const table = $(that.form).find('table tbody');
        let tmp = {};
        $(table).find('tr').each(function(row, tr) {
            let attr = $(this).data('attr');
            let decimals = 2;
            let totalCount = 0;
            let totalAmount = 0;
            let amount = 0;
            let input = $(tr).find('input.custom-value-percent');
            let percent = Number(that._sanitize(input.val()));

            if (attr === 'total') {
                $(tr).find('td').each(function(col, td) {
                    if (col > 1) {
                        amount = tmp[col];
                        totalCount += 1;
                        totalAmount += tmp[col];
                        $(td).html(number_format(amount, decimals, ',', ' '))
                    }
                });
            } else {
                $(tr).find('td').each(function (col, td) {
                    if ($(this).hasClass('td-value')) {
                        amount = percent / 100 * Number($(this).data('revenue')) / 100;
                        $(td).html(number_format(amount, decimals, ',', ' '));

                        if (typeof tmp[col] === 'undefined')
                            tmp[col] = 0;

                        totalCount += Number(1);
                        totalAmount += Number(that._sanitize(amount));
                        tmp[col] += Number(that._sanitize(amount));
                    }
                });
            }
        });

    },
    _showFlash: function(text) {
        if (window.toastr) {
            window.toastr.success(text, "", {
                "closeButton": true,
                "showDuration": 1000,
                "hideDuration": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 1000,
                "escapeHtml": false
            });
        }
    },
    _sanitize: function(val) {
        return String(val).replace(/\s/g, '').replace(',', '.');
    },
};

FinanceModelCSTable = {
    wrapper: "#cs-table-1x",
    wrapperSticky: "#cs-table-2x",
    topScrollbar: "#cs-table-11x",
    collapseColBtn: "[data-collapse-trigger]",
    collapseRowBtn: "[data-collapse-row-trigger]",
    collapseAllBtn: "[data-collapse-all-trigger]",
    _firstColumnWidth: 300,
    _resizeFinished: null,
    init: function() {
        this.setSticky();
        this.bindEvents();
        this.syncScrollbars();
    },
    setSticky: function() {
        this._setStickyCell();
        this._setStickyHeader();
        this._setStickyColumn();
        this._refreshTopScrollbar();
    },
    bindEvents: function() {
        this._bindScrollEvent();
        this._bindCollapseEvent();
        this._bindPseudoCollapseEvent();
        this._bindWindowResizeEvent();
    },
    syncScrollbars: function()
    {
        const w1 = document.getElementById(this.wrapper.replace('#',''));
        const w2 = document.getElementById(this.topScrollbar.replace('#',''));

        w1.onscroll = function() {
            w2.scrollLeft = w1.scrollLeft;
        };
        w2.onscroll = function() {
            w1.scrollLeft = w2.scrollLeft;
        };
    },
    refresh: function()
    {
        this._refreshStickyColumnsWidth();
        this._refreshTopScrollbar();
    },
    toggleHiddenColumn: function() { // callback
        CSTable.refresh();
    },
    _bindScrollEvent: function() {
        const wrapperSticky = this.wrapperSticky.replace('#', '');
        $(this.wrapper).on('scroll', function(e) {
            const parentX = e.target.getBoundingClientRect().x;
            const x = e.target.children[0].getBoundingClientRect().x;
            document.getElementById(wrapperSticky).children[0].style.marginLeft = (x - parentX) + "px";
        });
    },
    _bindCollapseEvent: function() {
        const that = this;

        $(that.wrapper + ' ' + that.collapseColBtn).unbind();
        $(that.wrapper + ' ' + that.collapseRowBtn).unbind();
        $(that.wrapper + ' ' + that.collapseAllBtn).unbind();

        $(that.wrapper + ' ' + that.collapseColBtn).click(function(e) {
            const target = $(this).data('target');
            const collapseCount = $(this).hasClass('active') ? $(this).data('colspan-close') : $(this).data('colspan-open');
            $(this).toggleClass('active').closest('th').attr('colspan', collapseCount);
            $(that.wrapper).find('table').find('[data-id="'+target+'"][data-collapse-cell], [data-id="'+target+'"][data-collapse-cell-total]').toggleClass('d-none');
            $(that.wrapperSticky).find('table').find('[data-id="'+target+'"][data-collapse-cell], [data-id="'+target+'"][data-collapse-cell-total]').toggleClass('d-none');
            that.refresh();
        });
        $(that.wrapper + ' ' + that.collapseRowBtn).click(function(e) {
            const target = $(this).data('target');
            $(this).toggleClass('active');
            if ($(this).hasClass('active')) {
                $('[data-id="'+target+'"]').removeClass('d-none');
            } else {
                $('[data-id="'+target+'"]').addClass('d-none');
                $('[data-id="'+target+'"]').find(that.collapseRowBtn).removeClass('active');
                $('[data-id="'+target+'"]').each(function(i, row) {
                });
            }
            if ( $(that.collapseRowBtn + '.active').length <= 0 ) {
                $(that.collapseAllBtn).removeClass('active');
            } else {
                $(that.collapseAllBtn).addClass('active');
            }
            that.refresh();
        });
        $(that.wrapper + ' ' + this.collapseAllBtn).click(function(e) {
            var _this = $(this);
            var table = $(this).closest('.table');
            var row = table.find('tr[data-id]');
            _this.toggleClass('active');
            if ( _this.hasClass('active') ) {
                row.removeClass('d-none');
                $(table).find('tbody .table-collapse-btn').addClass('active');
            } else {
                row.addClass('d-none');
                $(table).find('tbody .table-collapse-btn').removeClass('active');
            }
            that.refresh();
        });
    },
    _bindPseudoCollapseEvent: function() {
        const that = this;
        $(that.wrapperSticky + ' ' + that.collapseColBtn).click(function(e) {
            const target = $(this).data('target');
            const collapseCount = $(this).hasClass('active') ? $(this).data('colspan-close') : $(this).data('colspan-open');
            $(this).toggleClass('active').closest('th').attr('colspan', collapseCount);
            $(that.wrapper + ' ' + that.collapseColBtn).filter('[data-target="'+target+'"]').trigger('click');
        });
        $(that.wrapperSticky + ' ' + that.collapseRowBtn).click(function(e) {
            $(this).toggleClass('active');
            $(that.wrapper + ' ' + that.collapseRowBtn).trigger('click');
        });
        $(that.wrapperSticky + ' ' + that.collapseAllBtn).click(function(e) {
            $(this).toggleClass('active');
            $(that.wrapper + ' ' + that.collapseAllBtn).trigger('click');
        });
    },
    _bindWindowResizeEvent: function() {
        const that = this;
        $(window).resize(function() {
            that._refreshTopScrollbar();
            clearTimeout(that._resizeFinished);
            that._resizeFinished = setTimeout(function(){
                console.log('Resized finished.');
                that._refreshStickyColumnsWidth();
            }, 1);
        });
    },
    _setStickyColumn: function() {
        $(this.wrapper).addClass('sticky');
    },
    _setStickyHeader: function() {
        const that = this;
        const thead = $(that.wrapper + ' table thead').clone();
        $(thead).appendTo(that.wrapperSticky + ' table');
        $(that.wrapper).addClass('hide-head');
        that.refresh();
    },
    _setStickyCell: function() {
        console.log($(this.wrapper).find('thead tr:first-child th:first-child').outerHeight());
        const td = $(this.wrapper).find('thead tr:first-child th:first-child').clone();
        $(td).appendTo(this.wrapperSticky + ' .fixed-first-cell');
    },
    _refreshTopScrollbar: function() {
        $(this.topScrollbar + ' > .table-wrap').width($(this.wrapper + ' > .table-wrap > .table').width() + this._firstColumnWidth /*padding-left*/ - 10 /*chrome tolerance*/ );
    },
    _refreshStickyColumnsWidth: function() {
        const that = this;
        $(that.wrapper).find('thead tr').each(function(j,tr) {
            const newTr = $(that.wrapperSticky).find('thead tr').eq(j);
            $(tr).find('th').each(function(i,td) {
                if (i === 0 || ($(td).attr('colspan') > 1))
                    return true;
                let width = $(td).outerWidth() + 'px';
                $(newTr).find('th').eq(i).css({'min-width': width, 'width': width});
            });

        });
    }
};

FinanceModelCSTableSimple = {
    options: {
        selectorTop: '#sds-top',
        selectorBottom: '#sds-bottom',
        refreshByClick: [],
        refreshByChange: [],
        fixedColumnWidth: 0,
    },
    init: function() {
        const that = this;
        this.syncScrollbars();
        this.refresh();
        $(this.options.selectorTop).show();
        $(this.options.selectorBottom).addClass('sticky');
        this.options.refreshByClick.forEach((obj) => {
            $(obj).on('click', (e) => that.refresh());
        });
        this.options.refreshByChange.forEach((obj) => {
            $(obj).on('change', (e) => setTimeout(function() { that.refresh() }, 50));
        });
        setTimeout(that.refresh(), 1000);
        window.addEventListener('resize', function(event){
            that.refresh();
        });
    },
    syncScrollbars: function()
    {
        const w1 = document.getElementById(this.options.selectorTop.replace('#',''));
        const w2 = document.getElementById(this.options.selectorBottom.replace('#',''));

        if (!w1 || !w2) {
            console.log('SimpleDoubleScroll: selectors not found');
            return;
        }

        w1.onscroll = function() {
            w2.scrollLeft = w1.scrollLeft;
        };
        w2.onscroll = function() {
            w1.scrollLeft = w2.scrollLeft;
        };
    },
    refresh: function() {
        //console.log($(this.options.selectorBottom + ' > .table-wrap > .table').width(), this.options.fixedColumnWidth);
        $(this.options.selectorTop + ' > .table-wrap').width($(this.options.selectorBottom + ' > .table-wrap > .table').width() + Number(this.options.fixedColumnWidth));
    },
};