$(document).ready(function () {

    // chart plan symbol
    Highcharts.SVGRenderer.prototype.symbols['c-rect'] = function (x, y, w, h) {
        return ['M', x, y + h / 2, 'L', x + w, y + h / 2];
    };

});