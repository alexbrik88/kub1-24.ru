CSTable = {
    wrapper: "#cs-table-1x",
    wrapperSticky: "#cs-table-2x",
    topScrollbar: "#cs-table-11x",
    collapseColBtn: "[data-collapse-trigger-cs]",
    collapseRowBtn: "[data-collapse-row-trigger-cs]",
    collapseAllBtn: "[data-collapse-all-trigger-cs]",
    init: function() {
        this.setSticky();
        this.bindEvents();
        this.syncScrollbars();
    },
    setSticky: function() {
        this._setStickyCell();
        this._setStickyHeader();
        this._setStickyColumn();
        this._refreshTopScrollbar();
    },
    bindEvents: function() {
        this._bindScrollEvent();
        this._bindCollapseEvent();
        this._bindPseudoCollapseEvent();
    },
    syncScrollbars: function()
    {
        const w1 = document.getElementById(this.wrapper.replace('#',''));
        const w2 = document.getElementById(this.topScrollbar.replace('#',''));
        
        w1.onscroll = function() {
            w2.scrollLeft = w1.scrollLeft;
        };
        w2.onscroll = function() {
            w1.scrollLeft = w2.scrollLeft;
        };
    },
    refresh: function()
    {
        this._refreshStickyColumnsWidth();
        this._refreshStickyColumnsHeight();
        this._refreshTopScrollbar();
    },
    toggleHiddenColumn: function() { // callback
        CSTable.refresh();
    },
    _bindScrollEvent: function() {
        const wrapperSticky = this.wrapperSticky.replace('#', '');
        $(this.wrapper).on('scroll', function(e) {
            const parentX = e.target.getBoundingClientRect().x;
            const x = e.target.children[0].getBoundingClientRect().x;
            document.getElementById(wrapperSticky).children[0].style.marginLeft = (x - parentX) + "px";
        });
    },
    _bindCollapseEvent: function() {
        const that = this;
        $(that.wrapper + ' ' + that.collapseColBtn).click(function(e) {
            const target = $(this).data('target');
            const collapseCount = $(this).hasClass('active') ? $(this).data('colspan-close') : $(this).data('colspan-open');
            $(this).toggleClass('active').closest('th').attr('colspan', collapseCount);
            $(that.wrapper).find('table').find('[data-id="'+target+'"][data-collapse-cell]').toggleClass('d-none');
            $(that.wrapperSticky).find('table').find('[data-id="'+target+'"][data-collapse-cell]').toggleClass('d-none');
            that.refresh();
        });
        $(this.wrapper + ' ' + that.collapseRowBtn).click(function(e) {
            const target = $(this).data('target');
            $(this).toggleClass('active');
            if ($(this).hasClass('active')) {
                $('[data-id="'+target+'"]').removeClass('d-none');
            } else {
                $('[data-id="'+target+'"]').addClass('d-none');
                $('[data-id="'+target+'"]').find(that.collapseRowBtn).removeClass('active');
                $('[data-id="'+target+'"]').each(function(i, row) {
                });
            }
            if ( $(that.collapseRowBtn + '.active').length <= 0 ) {
                $(that.collapseAllBtn).removeClass('active');
            } else {
                $(that.collapseAllBtn).addClass('active');
            }
            that.refresh();
        });
        $(this.wrapper + ' ' + this.collapseAllBtn).click(function(e) {
            var _this = $(this);
            var table = $(this).closest('.table');
            var row = table.find('tr[data-id]');
            _this.toggleClass('active');
            if ( _this.hasClass('active') ) {
                row.removeClass('d-none');
                $(table).find('tbody .table-collapse-btn').addClass('active');
            } else {
                row.addClass('d-none');
                $(table).find('tbody .table-collapse-btn').removeClass('active');
            }
            that.refresh();
        });
    },
    _bindPseudoCollapseEvent: function() {
        const that = this;
        $(that.wrapperSticky + ' ' + that.collapseColBtn).click(function(e) {
            const target = $(this).data('target');
            const collapseCount = $(this).hasClass('active') ? $(this).data('colspan-close') : $(this).data('colspan-open');
            $(this).toggleClass('active').closest('th').attr('colspan', collapseCount);
            $(that.wrapper + ' ' + that.collapseColBtn).filter('[data-target="'+target+'"]').trigger('click');
        });
        $(that.wrapperSticky + ' ' + that.collapseRowBtn).click(function(e) {
            $(this).toggleClass('active');
            $(that.wrapper + ' ' + that.collapseRowBtn).trigger('click');
        });
        $(that.wrapperSticky + ' ' + that.collapseAllBtn).click(function(e) {
            $(this).toggleClass('active');
            $(that.wrapper + ' ' + that.collapseAllBtn).trigger('click');
        });
    },
    _setStickyColumn: function() {
        $(this.wrapper).addClass('sticky');
    },
    _setStickyHeader: function() {
        const that = this;
        const thead = $(that.wrapper + ' table thead').clone();
        $(thead).appendTo(that.wrapperSticky + ' table');
        $(that.wrapper).addClass('hide-head');
        that.refresh();
    },
    _setStickyCell: function() {
        console.log($(this.wrapper).find('thead tr:first-child th:first-child').outerHeight());
        const td = $(this.wrapper).find('thead tr:first-child th:first-child').clone();
        $(td).appendTo(this.wrapperSticky + ' .fixed-first-cell');
    },
    _refreshTopScrollbar: function() {
        $(this.topScrollbar + ' > .table-wrap').width($(this.wrapper + ' > .table-wrap > .table').width() + 250 /*padding-left*/);
    },
    _refreshStickyColumnsWidth: function() {
        const that = this;
        $(this.wrapper).find('thead tr').each(function(j,tr) {
            const newTr = $(that.wrapperSticky).find('thead tr').eq(j);
            $(tr).find('th').each(function(i,td) {
                if (($(td).attr('colspan') > 1))
                    return true;
                let width = $(td).outerWidth() + 'px';
                $(newTr).find('th').eq(i).css({'min-width': width});
            });

        });
    },
    _refreshStickyColumnsHeight: function() {}
};

SimpleDoubleScroll = {
    defaults: {
        selectorTop: '#sds-top',
        selectorBottom: '#sds-bottom',
        refreshByClick: [],
        fixedColumnWidth: 0,
    },
    init: function(options) {
        const that = this;
        this.options = $.extend(this.defaults, options);
        this.syncScrollbars();
        this.refresh();
        $(this.options.selectorTop).show();
        this.options.refreshByClick.forEach((obj) =>{
            $(obj).on('click', (e) => that.refresh());
        });
    },
    syncScrollbars: function()
    {
        const w1 = document.getElementById(this.options.selectorTop.replace('#',''));
        const w2 = document.getElementById(this.options.selectorBottom.replace('#',''));

        if (!w1 || !w2) {
            console.log('SimpleDoubleScroll: selectors not found');
            return;
        }

        w1.onscroll = function() {
            w2.scrollLeft = w1.scrollLeft;
        };
        w2.onscroll = function() {
            w1.scrollLeft = w2.scrollLeft;
        };
    },
    refresh: function() {
        $(this.options.selectorTop + ' > .table-wrap').width($(this.options.selectorBottom + ' > .table-wrap > .table').width() + this.options.fixedColumnWidth);
    },
};