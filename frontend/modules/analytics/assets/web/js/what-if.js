
!function( $ ) {
    $(document).on('input change', '.what-if-modeling .kv-type-range > input.form-control', function (e) {
        let form = this.form;
        let val = parseFloat(this.value);
        if (val > 0) {
            $(this).attr('data-color', 'green');
        } else if (val < 0) {
            $(this).attr('data-color', 'red');
        } else {
            $(this).attr('data-color', '');
        }
        $(form).trigger('submit');
    });
    $(document).on('click', '.what-if table .collapse-row-trigger', function() {
        let target = $(this).data('target');
        $('.what-if table .collapse-row-trigger[data-target="'+target+'"]').toggleClass('active');
        $('.what-if table tr[data-id="'+target+'"]').toggleClass('hidden');
    });
    $(document).on('click', '.what-if table .collapse-all-trigger', function() {
        $(this).toggleClass('active');
        let isActive = $(this).hasClass('active');
        $('.what-if table tr.group-row .collapse-row-trigger').toggleClass('active', isActive);
        $('.what-if table tr.group-item-row').toggleClass('hidden', !isActive);
    });
    $(document).on('click', '.what-if-modeling-reset', function() {
        let $form = $('#what-if-modeling-form');
        $('input', $form).val(0);
        $form.trigger('submit');
    });
}( window.jQuery );
