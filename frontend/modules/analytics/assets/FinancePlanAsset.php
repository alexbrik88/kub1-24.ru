<?php

namespace frontend\modules\analytics\assets;

use yii\web\AssetBundle;

/**
 * Class FinancePlanAsset
 */
class FinancePlanAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/modules/analytics/assets/web';

    /**
     * @var array
     */
    public $css = [
        'css/finance-plan.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'js/finance-plan.js'
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
