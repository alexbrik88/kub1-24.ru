<?php

namespace frontend\modules\analytics\assets;

use yii\web\AssetBundle;

/**
 * Class AnalyticsAsset
 */
class AnalyticsAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/modules/analytics/assets/web';

    /**
     * @var array
     */
    public $css = [
        'css/analytics.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'js/analytics.js'
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
