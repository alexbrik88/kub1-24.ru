<?php

namespace frontend\modules\analytics\assets;

use yii\web\AssetBundle;

/**
 * Class WhatIfAssets
 */
class WhatIfAssets extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/modules/analytics/assets/web';

    /**
     * @var array
     */
    public $css = [
        'css/what-if.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'js/what-if.js'
    ];

    /**
     * @var array
     */
    public $depends = [
        'frontend\themes\kub\assets\KubAsset',
    ];
}
