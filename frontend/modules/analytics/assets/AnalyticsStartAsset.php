<?php

namespace frontend\modules\analytics\assets;

use yii\web\AssetBundle;

/**
 * Class AnalyticsStartAsset
 */
class AnalyticsStartAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/modules/analytics/assets/web';

    /**
     * @var array
     */
    public $css = [
        'css/analytics-start-modal.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'js/analytics-start-modal.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
