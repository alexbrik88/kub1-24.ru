<?php

namespace frontend\modules\cards\behaviors;

use common\modules\cards\models\CardAccount;
use frontend\controllers\WebTrait;
use frontend\modules\cards\models\ActiveAccountForm;
use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\web\Controller;

/**
 * @property-read Controller $owner
 */
class ActiveAccountBehavior extends Behavior
{
    use WebTrait;

    /**
     * @var string
     */
    public string $redirectUrl;

    /**
     * @var CardAccount|null
     */
    private ?CardAccount $account = null;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        if (!isset($this->redirectUrl)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function events(): array
    {
        return [
            Controller::EVENT_BEFORE_ACTION => 'beforeAction',
        ];
    }

    /**
     * @return bool
     */
    public function beforeAction(): bool
    {
        if ($this->isFilterRequest())
            return true;

        $accountId = $this->request->get('account_id');
        $activeAccountForm = new ActiveAccountForm($this->user->identity, [
            'account_id' => (string) $accountId,
        ]);

        if ($accountId !== null) {
            if ($activeAccountForm->validate()) {
                $activeAccountForm->setActiveAccount();
            }

            $this->response->redirect($this->redirectUrl);

            return false;
        }

        $this->account = $activeAccountForm->getActiveAccount();

        return true;
    }

    /**
     * @return CardAccount|null
     */
    public function getAccount(): ?CardAccount
    {
        return $this->account;
    }

    /**
     * @return bool
     */
    public function isFilterRequest(): bool
    {
        return (bool)\Yii::$app->request->get('CardOperationRepository');
    }
}
