<?php

namespace frontend\modules\cards\widgets;

use common\models\employee\Employee;
use common\modules\cards\models\CardAccount;
use common\modules\cards\models\CardAccountRepository;
use frontend\themes\kub\widgets\AbstractImportButtonWidget;
use yii\helpers\Url;

class ZenmoneyButtonWidget extends AbstractImportButtonWidget
{
    /**
     * @var string
     */
    public $identifier;

    /**
     * @inheritDoc
     */
    protected function getConnectUrl(string $redirectUrl): string
    {
        return Url::to([
            '/cards/zenmoney/connect',
            'redirectUrl' => sprintf('%s#%s', $redirectUrl, $this->getButtonId('import')),
        ]);
    }

    /**
     * @inheritDoc
     */
    protected function getImportUrl(string $redirectUrl): string
    {
        return Url::to([
            '/cards/zenmoney/import',
            'redirectUrl' => $redirectUrl,
            'identifier' => $this->identifier,
        ]);
    }

    /**
     * @inheritDoc
     * @throws
     */
    protected function hasAccounts(): bool
    {
        $employee = $this->getUser()->getIdentity();

        if ($employee instanceof Employee) {
            $models = CardAccountRepository::findActiveAccounts($employee->company, CardAccount::ACCOUNT_TYPE_ZENMONEY);

            if ($models) {
                return true;
            }
        }

        return false;
    }
}
