<?php

namespace frontend\modules\cards;

use yii\base\Module as BaseModule;

class Module extends BaseModule
{
    /**
     * @inheritDoc
     */
    public $controllerNamespace = 'frontend\modules\cards\controllers';
}
