<?php

namespace frontend\modules\cards\controllers;

use common\components\date\DateHelper;
use common\modules\cards\models\CardBillForm;
use common\modules\cards\models\CardOperation;
use frontend\modules\cash\models\CashContractorType;
use Yii;
use frontend\rbac\UserRole;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\modules\cards\models\CardBill;
use yii\web\Response;

class BillController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'ajax' => [
                'class' => 'common\components\filters\AjaxFilter',
            ],
        ];
    }

    public function actionDelete($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var CardBillForm $model */
        $model = $this->findModel($id);

        if (!$model->hasMovement()) {
            return ['status' => $model->delete()];
        }

        return ['status' => 0];
    }

    public function actionUpdate($id)
    {
        /** @var CardBillForm $model */
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            if ($model->save()) {
                return $this->renderAjax('update', [
                    'model' => $model,
                ]);
            }

        } else {

            /** @var CardOperation $startBalanceOperation */
            $startBalanceOperation = CardOperation::find()
                ->where(['bill_id' => $id, 'contractor_id' => CashContractorType::BALANCE_TEXT])
                ->one();

            if ($startBalanceOperation) {
                $model->createStartBalance = 1;
                $model->startBalanceAmount = round($startBalanceOperation->amount / 100, 2);
                $model->startBalanceDate = DateHelper::format($startBalanceOperation->date, 'd.m.Y', 'Y-m-d');
            }
            elseif ($minDate = CardOperation::find()->where(['bill_id' => $id])->min('date')) {
                if ($minDateObj = date_create_from_format('Y-m-d', $minDate))
                    $model->startBalanceDate = $minDateObj->modify("-1 days")->format('d.m.Y');
            }
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    protected function findModel($id)
    {
        $model = CardBillForm::find()->alias('b')->joinWith('accountRelation a')->where([
            'b.id' => $id,
            'a.company_id' => Yii::$app->user->identity->company->id
        ])->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
