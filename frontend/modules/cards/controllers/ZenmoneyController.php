<?php

namespace frontend\modules\cards\controllers;

use common\components\filters\AccessControl;
use common\modules\cards\commands\ZenmoneyImportCommand;
use common\modules\cards\components\ZenmoneyComponent;
use common\modules\cards\models\CardAccount;
use frontend\controllers\WebTrait;
use frontend\modules\cards\models\ZenmoneyDisconnectForm;
use frontend\modules\cards\models\ZenmoneyTokenForm;
use frontend\rbac\permissions\Cash;
use yii\filters\AjaxFilter;
use yii\helpers\Url;
use yii\web\Controller;

class ZenmoneyController extends Controller
{
    use WebTrait;

    /** @var string[] */
    private const REDIRECT_URI = ['/cards'];

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'accessControl' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            Cash::INDEX,
                        ],
                    ],
                ],
            ],
            'ajaxFilter' => [
                'class' => AjaxFilter::class,
                'only' => [
                    'connect',
                    'import',
                    'set-auto-import',
                    'job-status',
                ],
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function actions()
    {
        return [
            'code' => [
                'class' => CodeAction::class,
                'componentClass' => ZenmoneyComponent::class,
                'redirectUrl' => Url::to(self::REDIRECT_URI),
            ],
            'token' => [
                'class' => TokenAction::class,
                'formClass' => ZenmoneyTokenForm::class,
                'redirectUrl' => Url::to(self::REDIRECT_URI),
                'successMessage' => 'Интеграция с Дзен мани подключена.',
            ],
            'connect' => [
                'class' => ConnectAction::class,
                'redirectUrl' => Url::to(self::REDIRECT_URI),
            ],
            'disconnect' => [
                'class' => DisconnectAction::class,
                'formClass' => ZenmoneyDisconnectForm::class,
                'commandClass' => ZenmoneyImportCommand::class,
                'redirectUrl' => Url::to(self::REDIRECT_URI),
                'successMessage' => 'Интеграция с Дзен мани отключена.',
                'errorMessage' => 'Произошла ошибка при отключении интеграции с Дзен мани.',
            ],
            'import' => [
                'class' => ImportAction::class,
                'commandClass' => ZenmoneyImportCommand::class,
                'redirectUrl' => Url::to(self::REDIRECT_URI),
                'accountType' => CardAccount::ACCOUNT_TYPE_ZENMONEY,
            ],
            'set-auto-import' => [
                'class' => SetAutoImportAction::class,
                'commandClass' => ZenmoneyImportCommand::class,
                'accountType' => CardAccount::ACCOUNT_TYPE_ZENMONEY,
            ],
            'quick-import' => [
                'class' => QuickImportAction::class,
                'commandClass' => ZenmoneyImportCommand::class,
                'redirectUrl' => Url::to(self::REDIRECT_URI),
                'accountType' => CardAccount::ACCOUNT_TYPE_ZENMONEY,
            ],
            'job-status' => [
                'class' => JobStatusAction::class,
                'commandClass' => ZenmoneyImportCommand::class,
            ],
        ];
    }
}
