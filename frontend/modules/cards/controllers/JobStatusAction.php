<?php

namespace frontend\modules\cards\controllers;

use common\modules\import\components\ImportCommandManager;
use frontend\controllers\ActionInterface;
use frontend\controllers\WebTrait;
use frontend\modules\cards\models\ImportJobDataRepository;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\web\BadRequestHttpException;
use yii\web\Response;

class JobStatusAction extends Action implements ActionInterface
{
    use WebTrait;

    /**
     * @var string
     */
    public $commandClass;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->commandClass)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function run(): Response
    {
        $identifier = $this->request->get('identifier');

        if (!is_scalar($identifier)) {
            throw new BadRequestHttpException();
        }

        $jobData = ImportJobDataRepository::findLastByIdentifier(
            (string) $identifier,
            $this->user->identity->company,
            ImportCommandManager::getInstance()->getTypeByClass($this->commandClass)
        );

        $this->response->format = Response::FORMAT_JSON;
        $this->response->data = ['status' => ($jobData && $jobData->finished_at)];

        return $this->response;
    }
}
