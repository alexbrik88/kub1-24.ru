<?php

namespace frontend\modules\cards\controllers;

use common\modules\import\components\ImportCommandManager;
use frontend\controllers\ActionInterface;
use frontend\controllers\WebTrait;
use frontend\modules\cards\models\AutoImportForm;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\web\Response;

class SetAutoImportAction extends Action implements ActionInterface
{
    use WebTrait;

    /**
     * @var string
     */
    public $commandClass;

    /**
     * @var int
     */
    public $accountType;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!isset($this->commandClass, $this->accountType)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function run(): Response
    {
        $identifier = $this->request->get('identifier');
        $form = new AutoImportForm(
            $this->user->identity->company,
            ImportCommandManager::getInstance()->getTypeByClass($this->commandClass),
            $this->accountType,
            $identifier = is_scalar($identifier) ? $identifier : '',
        );

        $this->response->format = Response::FORMAT_JSON;
        $this->response->data = ($form->load($this->request->post()) && $form->validate() && $form->setAutoImport());

        return $this->response;
    }
}
