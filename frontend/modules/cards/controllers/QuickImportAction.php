<?php

namespace frontend\modules\cards\controllers;

use common\modules\import\components\ImportCommandManager;
use frontend\controllers\ActionInterface;
use frontend\controllers\WebTrait;
use frontend\modules\cards\models\ImportForm;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\web\Response;

class QuickImportAction extends Action implements ActionInterface
{
    use WebTrait;

    /**
     * @var int|null
     */
    public ?int $accountType = null;

    /**
     * @var string
     */
    public string $commandClass = '';

    /**
     * @var string
     */
    public string $redirectUrl = '';

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        if (empty($this->redirectUrl) || empty($this->commandClass) || $this->accountType === null) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function run(): Response
    {
        $identifier = $this->request->get('identifier');
        $form = new ImportForm(
            $this->user->identity,
            ImportCommandManager::getInstance()->getTypeByClass($this->commandClass),
            $this->accountType,
            is_scalar($identifier) ? $identifier : '',
        );

        if ($form->validate()) {
            if (!$form->createJob()) {
                $this->session->setFlash('error', 'Произошла ошибка при создании задачи на выгрузку операций.');
            } else {
                $this->session->setFlash('success', 'Задача на загрузку операций создана.');
            }
        }

        return $this->response->redirect($this->redirectUrl);
    }
}
