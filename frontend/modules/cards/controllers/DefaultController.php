<?php

namespace frontend\modules\cards\controllers;

use common\components\filters\AccessControl;
use common\modules\cards\commands\ZenmoneyImportCommand;
use common\modules\cards\models\CardAccount;
use common\modules\cards\models\CardBillList;
use common\modules\cards\models\CardOperationRepository;
use common\modules\cards\models\ContractorList;
use common\modules\cards\models\ProjectList;
use common\modules\cards\models\ReasonList;
use common\modules\import\components\ImportCommandManager;
use common\modules\import\components\ImportCommandType;
use frontend\components\StatisticPeriod;
use frontend\controllers\WebTrait;
use frontend\modules\cards\behaviors\ActiveAccountBehavior;
use frontend\modules\cards\models\ImportJobDataRepository;
use frontend\modules\cards\models\OperationSelectForm;
use frontend\rbac\permissions\Cash;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

/**
 * @property-read CardAccount|null $account
 */
class DefaultController extends Controller
{
    use WebTrait;

    /** @var string[] */
    private const COMMAND_TYPES = [
        CardAccount::ACCOUNT_TYPE_ZENMONEY => ZenmoneyImportCommand::class,
    ];

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'accessControl' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            Cash::INDEX,
                        ],
                    ],
                ],
            ],
            'activeAccount' => [
                'class' => ActiveAccountBehavior::class,
                'redirectUrl' => Url::to(['/cards/default/index']),
            ],
        ];
    }

    /**
     * @return Response
     * @throws BadRequestHttpException
     */
    public function actionIndex(): Response
    {

        $form = new OperationSelectForm($this->user->identity->company);

        if ($form->load($this->request->post()) && $form->validate()) {
            $form->updateOperation();

            return $this->refresh();
        }

        $repository = new CardOperationRepository([
            'company' => $this->user->identity->company,
            'account' => $this->account,
            'dateFrom' => StatisticPeriod::getDateFrom(),
            'dateTo' => StatisticPeriod::getDateTo(),
        ]);

        if ($repository->load($this->request->get()) && !$repository->validate()) {
            throw new BadRequestHttpException($repository->getErrorSummary(false));
        }

        $lastJobData = null;

        if ($this->account) {
            $lastJobData = ImportJobDataRepository::findJobDataByAccount(
                $this->account,
                $this->getCommandType($this->account)
            );
        }

        $this->response->content = $this->render('index', [
            'form' => $form,
            'employee' => $this->user->identity,
            'account' => $this->account,
            'repository' => $repository,
            'billList' => new CardBillList($this->user->identity->company, $this->account),
            'contractorList' => new ContractorList($this->user->identity->company, $this->account),
            'reasonList' => new ReasonList($this->user->identity->company, $this->account),
            'projectList' => new ProjectList($this->user->identity->company, $this->account),
            'lastJobData' => $lastJobData,
        ]);

        return $this->response;
    }

    /**
     * @param CardAccount $account
     * @return ImportCommandType
     */
    private function getCommandType(CardAccount $account): ImportCommandType
    {
        return ImportCommandManager::getInstance()->getTypeByClass(self::COMMAND_TYPES[$account->account_type]);
    }
}
