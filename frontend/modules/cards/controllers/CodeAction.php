<?php

namespace frontend\modules\cards\controllers;

use common\modules\import\components\AuthComponentInterface;
use frontend\controllers\ActionInterface;
use frontend\controllers\WebTrait;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\web\Response;

class CodeAction extends Action implements ActionInterface
{
    use WebTrait;

    /**
     * @var string|AuthComponentInterface
     */
    public $componentClass;

    /**
     * @var string
     */
    public $redirectUrl;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        if (!isset($this->redirectUrl)) {
            throw new InvalidConfigException();
        }

        if (!is_a($this->componentClass, AuthComponentInterface::class, true)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function run(): Response
    {
        $redirectUrl = $this->request->get('redirectUrl', $this->redirectUrl);
        $this->session->set('redirectUrl', $redirectUrl);

        /** @var AuthComponentInterface $component */
        $component = call_user_func([$this->componentClass, 'getInstance']);

        return $this->controller->redirect($component->getAuthorizationUrl());
    }
}
