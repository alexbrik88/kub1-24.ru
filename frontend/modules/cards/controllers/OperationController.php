<?php

namespace frontend\modules\cards\controllers;

use common\components\filters\AccessControl;
use frontend\controllers\WebTrait;
use frontend\modules\cards\models\OperationDeleteForm;
use frontend\modules\cards\models\OperationUpdateForm;
use frontend\rbac\permissions\Cash;
use yii\filters\AjaxFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class OperationController extends Controller
{
    use WebTrait;

    /** @var string[] */
    private const REDIRECT_URI = ['/cards/default/index'];

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'accessControl' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            Cash::INDEX,
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return Response|bool
     * @throws NotFoundHttpException
     */
    public function actionUpdate()
    {
        $redirectUrl = $this->request->get('redirectUrl', self::REDIRECT_URI);
        $form = new OperationUpdateForm($this->user->identity->company, $this->getOperationId());

        if ($form->operation === null) {
            throw new NotFoundHttpException();
        }

        if ($form->load($this->request->post()) && $form->validate() && $form->updateOperation()) {

            if (\Yii::$app->request->post('fromOdds')) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                return true;
            }

            return $this->redirect($redirectUrl);
        }

        $this->response->content = $this->renderAjax('update-form', [
            'form' => $form,
        ]);

        return $this->response;
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDelete(): Response
    {
        $redirectUrl = $this->request->get('redirectUrl', self::REDIRECT_URI);
        $form = new OperationDeleteForm($this->user->identity->company, $this->getOperationId());

        if ($form->operation === null) {
            throw new NotFoundHttpException();
        }

        $isFormLoaded = $form->load($this->request->post()) ?: $form->load($this->request->get());

        if ($isFormLoaded && $form->validate() && $form->deleteOperation()) {
            return $this->redirect($redirectUrl);
        }

        $this->response->content = $this->renderAjax('delete-form', [
            'form' => $form,
        ]);

        return $this->response;
    }

    /**
     * @return int
     * @throws NotFoundHttpException
     */
    private function getOperationId(): int
    {
        $operationId = $this->request->get('id');

        if (!is_numeric($operationId)) {
            throw new NotFoundHttpException();
        }

        return (int) $operationId;
    }
}
