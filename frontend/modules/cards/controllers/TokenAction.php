<?php

namespace frontend\modules\cards\controllers;

use common\modules\import\models\TokenFormInterface;
use frontend\controllers\ActionInterface;
use frontend\controllers\WebTrait;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\web\Response;

class TokenAction extends Action implements ActionInterface
{
    use WebTrait;

    /**
     * @var string|TokenFormInterface
     */
    public $formClass;

    /**
     * @var string
     */
    public $redirectUrl;

    /**
     * @var string
     */
    public $successMessage;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!isset($this->redirectUrl, $this->successMessage)) {
            throw new InvalidConfigException();
        }

        if (!is_a($this->formClass, TokenFormInterface::class, true)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function run(): Response
    {
        /** @var Model|TokenFormInterface $form */
        $form = $this->formClass::createTokenForm($this->user->identity->company);
        $form->load($this->request->get(), '');

        if ($form->validate() && $form->updateToken()) {
            $this->session->setFlash('success', $this->successMessage);
        } elseif ($form->hasErrors('code')) {
            $this->session->setFlash('error', $form->getFirstError('code'));
        }

        $redirectUrl = $this->session->get('redirectUrl', $this->redirectUrl);

        return $this->controller->redirect($redirectUrl);
    }
}
