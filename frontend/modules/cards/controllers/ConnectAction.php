<?php

namespace frontend\modules\cards\controllers;

use frontend\controllers\ActionInterface;
use frontend\controllers\WebTrait;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\web\Response;

class ConnectAction extends Action implements ActionInterface
{
    use WebTrait;

    /**
     * @var string
     */
    public $redirectUrl;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!isset($this->redirectUrl)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function run(): Response
    {
        $redirectUrl = $this->request->get('redirectUrl', $this->redirectUrl);
        $this->response->content = $this->controller->renderAjax('connect', [
            'redirectUrl' => $redirectUrl,
        ]);

        return $this->response;
    }
}
