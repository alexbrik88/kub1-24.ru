<?php

namespace frontend\modules\cards\models;

use common\models\Company;
use common\models\employee\Employee;
use common\modules\cards\components\ZenmoneyComponent;
use common\modules\cards\components\ZenmoneyException;
use common\modules\cards\components\ZenmoneyIntegration;
use common\modules\cards\models\AccountAlreadyUsedException;
use common\modules\cards\models\ZenmoneyIdentityRepository;
use common\modules\import\models\AbstractTokenForm;
use DateTimeImmutable;
use yii\web\User;

class ZenmoneyTokenForm extends AbstractTokenForm
{
    /**
     * @var int Нет нормального способа полуть это через api
     */
    public $user_id;

    /**
     * @var Employee|null
     */
    private ?Employee $employee;

    /**
     * ZenmoneyTokenForm constructor.
     * @param Company $company
     * @param User $user
     * @param array $config
     */
    public function __construct(Company $company, User $user, array $config = [])
    {
        $this->employee = $user->identity;

        parent::__construct($this->employee->company, $config);
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            [['user_id'], 'required'],
            [['user_id'], 'integer', 'min' => 1],
        ]);
    }

    /**
     * @inheritDoc
     */
    public function updateToken(): bool
    {
        try {
            $credentials = ZenmoneyComponent::getInstance()->getCredentials($this->code);
            $accessToken = $credentials['access_token'];
            $expiresIn = $credentials['expires_in'];

            $integration = new ZenmoneyIntegration($accessToken);
            $integration->getDiff((new DateTimeImmutable())->getTimestamp()); // Тестирование полученного токена
            $identity = ZenmoneyIdentityRepository::createZenmoneyIdentity(
                $this->getCompany(),
                $this->user_id,
                $accessToken,
                $expiresIn
            );

            if ($identity->save()) {
                $this->employee->currentEmployeeCompany->last_card_account_id = $identity->cardAccount->id;
                $this->employee->currentEmployeeCompany->save(true, ['last_card_account_id']);

                return true;
            }
        } catch (ZenmoneyException|AccountAlreadyUsedException $exception) {
            $this->addError('code', $exception->getMessage());
        }

        return false;
    }
}
