<?php

namespace frontend\modules\cards\models;

use common\models\employee\Employee;
use common\modules\cards\models\CardAccount;
use common\modules\cards\models\CardAccountRepository;
use yii\base\Model;

class ActiveAccountForm extends Model
{
    /**
     * @var string
     */
    public string $account_id = '';

    /**
     * @var Employee
     */
    private $employee;

    /**
     * @param Employee $employee
     * @param array $config
     */
    public function __construct(Employee $employee, array $config = [])
    {
        $this->employee = $employee;

        parent::__construct($config);
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['account_id'], 'integer'],
            [
                ['account_id'],
                'exist',
                'targetClass' => CardAccount::class,
                'targetAttribute' => ['account_id' => 'id'],
                'filter' => ['company_id' => $this->employee->company->id],
            ],
        ];
    }

    /**
     * @return bool
     */
    public function setActiveAccount(): bool
    {
        $this->employee->currentEmployeeCompany->last_card_account_id = $this->account_id;

        return $this->employee->currentEmployeeCompany->save(true, ['last_card_account_id']);
    }

    /**
     * @return CardAccount|null
     */
    public function getActiveAccount(): ?CardAccount
    {
        if ($this->employee->currentEmployeeCompany->last_card_account_id) {
            $accountId = (int) $this->employee->currentEmployeeCompany->last_card_account_id;

            return CardAccountRepository::findAccountById($accountId, $this->employee->company);
        }

        return null;
    }
}
