<?php

namespace frontend\modules\cards\models;

use common\models\Company;
use common\modules\cards\models\CardAccount;
use common\modules\import\components\ImportCommandType;
use common\modules\import\models\ImportJobData;
use common\modules\import\models\ImportJobDataRepository as BaseRepository;
use yii\db\ActiveRecordInterface;

class ImportJobDataRepository extends BaseRepository
{
    /**
     * @param CardAccount $account
     * @param ImportCommandType $commandType
     * @return ActiveRecordInterface|ImportJobData|null
     * @deprecated
     */
    public static function findJobDataByAccount(
        CardAccount $account,
        ImportCommandType $commandType
    ): ?ImportJobData {
        $repository = new static($account->company, [
            'type' => $commandType->getCommandId(),
        ]);

        return $repository
            ->getQuery()
            ->andWhere(['like', 'params', sprintf('"identifier":"%s"', $account->identifier)])
            ->orderBy(['id' => SORT_DESC])
            ->one();
    }

    /**
     * @param string $identifier
     * @param Company $company
     * @param ImportCommandType $commandType
     * @return ActiveRecordInterface|ImportJobData|null
     */
    public static function findLastByIdentifier(
        string $identifier,
        Company $company,
        ImportCommandType $commandType
    ): ?ImportJobData {
        $repository = new static($company, [
            'type' => $commandType->getCommandId(),
        ]);

        return $repository
            ->getQuery()
            ->andWhere(['like', 'params', sprintf('"identifier":"%s"', $identifier)])
            ->orderBy(['id' => SORT_DESC])
            ->one();
    }
}
