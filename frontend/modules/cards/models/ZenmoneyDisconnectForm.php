<?php

namespace frontend\modules\cards\models;

use common\models\Company;
use common\modules\cards\models\CardAccount;
use common\modules\cards\models\CardIdentifierList;
use common\modules\cards\models\ZenmoneyIdentityRepository;
use common\modules\import\components\ImportCommandType;
use common\modules\import\models\AbstractDisconnectForm;
use common\modules\import\models\DisconnectFormInterface;

class ZenmoneyDisconnectForm extends AbstractDisconnectForm implements DisconnectFormInterface
{
    /**
     * @inheritDoc
     */
    public static function createDisconnectForm(
        Company $company,
        ImportCommandType $commandType,
        string $identifier
    ): DisconnectFormInterface {
        $identifierList = new CardIdentifierList($company, CardAccount::ACCOUNT_TYPE_ZENMONEY);

        return new static($company, $commandType, $identifierList, $identifier);
    }

    /**
     * @return bool
     */
    protected function removeAccount(): bool
    {
        if (ZenmoneyIdentityRepository::removeByAccountId($this->identifier, $this->getCompany())) {
            return true;
        }

        return false;
    }
}
