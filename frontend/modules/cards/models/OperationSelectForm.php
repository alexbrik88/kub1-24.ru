<?php

namespace frontend\modules\cards\models;

use common\models\Company;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\modules\cards\models\CardOperationRepository;
use yii\base\Model;

class OperationSelectForm extends Model
{
    /**
     * @var string[]
     */
    public $id = [];

    /**
     * @var string
     */
    public string $delete = '';

    /**
     * @var string
     */
    public string $income_item_id = '';

    /**
     * @var string
     */
    public string $expenditure_item_id = '';

    /**
     * @var Company
     */
    private Company $company;

    /**
     * @param Company $company
     * @param array $config
     */
    public function __construct(Company $company, array $config = [])
    {
        $this->company = $company;

        parent::__construct($config);
    }

    /**
     * @return bool
     */
    public function updateOperation(): bool
    {
        if (empty($this->id)) {
            return false;
        }

        if (strlen($this->delete)) {
            CardOperationRepository::deleteOperation($this->id, $this->company);

            return true;
        }

        if (empty($this->income_item_id) && empty($this->expenditure_item_id)) {
            return false;
        }

        if ($this->income_item_id) {
            CardOperationRepository::updateIncomeItem(
                $this->id,
                $this->income_item_id ? (int) $this->income_item_id : null,
                $this->company
            );
        }

        if ($this->expenditure_item_id) {
            CardOperationRepository::updateExpenditureItem(
                $this->id,
                $this->expenditure_item_id ? (int) $this->expenditure_item_id : null,
                $this->company
            );
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['id'], 'required'],
            [['id'], 'each', 'rule' => ['integer']],
            [['delete'], 'string'],
            [['expenditure_item_id'], 'integer'],
            [
                ['expenditure_item_id'],
                'exist',
                'targetClass' => InvoiceExpenditureItem::class,
                'targetAttribute' => 'id',
                'filter' => ['or', ['company_id' => $this->company->id], ['company_id' => null]],
            ],
            [['income_item_id'], 'integer'],
            [
                ['income_item_id'],
                'exist',
                'targetClass' => InvoiceIncomeItem::class,
                'targetAttribute' => 'id',
                'filter' => ['or', ['company_id' => $this->company->id], ['company_id' => null]],
            ],
        ];
    }
}
