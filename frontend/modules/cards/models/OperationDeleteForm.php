<?php

namespace frontend\modules\cards\models;

use common\models\Company;
use common\modules\cards\models\CardOperation;
use common\modules\cards\models\CardOperationRepository;
use yii\base\Model;

class OperationDeleteForm extends Model
{
    /**
     * @var CardOperation|null
     */
    public ?CardOperation $operation;

    /**
     * @var string
     */
    public string $delete = '';

    /**
     * @param Company $company
     * @param int $operationId
     * @param array $config
     */
    public function __construct(Company $company, int $operationId, array $config = [])
    {
        $this->operation = CardOperationRepository::findOperationById($operationId, $company);

        parent::__construct($config);
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['delete'], 'string'],
        ];
    }

    /**
     * @return bool
     */
    public function deleteOperation(): bool
    {
        CardOperationRepository::deleteOperation([$this->operation->id], $this->operation->company);

        return true;
    }
}
