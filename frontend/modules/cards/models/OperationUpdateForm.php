<?php

namespace frontend\modules\cards\models;

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\project\Project;
use common\modules\cards\models\CardAccountList;
use common\modules\cards\models\CardOperation;
use common\modules\cards\models\CardOperationRepository;
use frontend\modules\cash\models\CashContractorType;
use Yii;
use yii\base\Model;

class OperationUpdateForm extends Model
{
    /**
     * @var string
     */
    public string $amount = '';

    /**
     * @var string
     */
    public string $flow_type = '';

    /**
     * @var string
     */
    public string $account_id = '';

    /**
     * @var string
     */
    public string $contractor_id = '';

    /**
     * @var string
     */
    public string $expenditure_item_id = '';

    /**
     * @var string
     */
    public string $income_item_id = '';

    /**
     * @var string
     */
    public string $date = '';

    /**
     * @var string
     */
    public string $recognition_date = '';

    /**
     * @var string
     */
    public string $time = '';

    /**
     * @var string
     */
    public string $description = '';

    /**
     * @var string
     */
    public string $project_id = '';

    /**
     * @var CardOperation|null
     */
    public ?CardOperation $operation;

    /**
     * @var Company
     */
    private Company $company;

    /**
     * @var CardAccountList
     */
    private CardAccountList $accountList;

    /**
     * @param Company $company
     * @param int $operationId
     * @param array $config
     */
    public function __construct(Company $company, int $operationId, array $config = [])
    {
        $this->company = $company;
        $this->accountList = new CardAccountList($company);
        $this->operation = CardOperationRepository::findOperationById($operationId, $company);

        if ($this->operation) {
            $this->setFormData();
        }

        parent::__construct($config);
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels(): array
    {
        return [
            'flow_type' => 'Тип',
            'account_id' => 'Лицевой счет',
            'date' => 'Дата',
            'recognition_date' => 'Дата признания',
            'time' => 'Время',
            'contractor_id' => $this->getContractorLabel(),
            'amount' => 'Сумма',
            'description' => 'Назначение',
            'expenditure_item_id' => 'Статья расхода',
            'income_item_id' => 'Статья прихода',
        ];
    }

    /**
     * @return CardAccountList
     */
    public function getAccountList(): CardAccountList
    {
        return $this->accountList;
    }

    /**
     * @return int|null
     */
    public function getContractorType(): ?int
    {
        if ($this->operation && $this->operation->flow_type == CardOperation::FLOW_TYPE_EXPENSE) {
            return Contractor::TYPE_SELLER;
        }

        if ($this->operation && $this->operation->flow_type == CardOperation::FLOW_TYPE_INCOME) {
            return Contractor::TYPE_CUSTOMER;
        }

        return null;
    }

    /**
     * @return bool
     */
    public function updateOperation(): bool
    {
        if ($this->operation === null) {
            return false;
        }
        // TODO: check unique
        $this->operation->setAttributes($this->getFormData(), false);

        return $this->operation->save();
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['account_id'], 'required'],
            [['account_id'], 'integer'],
            [['account_id'], 'in', 'range' => array_keys($this->getAccountList()->getItems())],
            [['date'], 'required'],
            [['date', 'recognition_date'], 'date', 'format' => 'php:' . DateHelper::FORMAT_USER_DATE],
            [['time'], 'required'],
            [['time'], 'date', 'format' => 'php:' . DateHelper::FORMAT_TIME],
            [['description'], 'trim'],
            [['description'], 'string', 'max' => 255],
            [['expenditure_item_id'], 'integer'],
            [
                ['expenditure_item_id'],
                'exist',
                'targetClass' => InvoiceExpenditureItem::class,
                'targetAttribute' => 'id',
                'filter' => ['or', ['company_id' => $this->company->id], ['company_id' => null]],
            ],
            [['income_item_id'], 'integer'],
            [
                ['income_item_id'],
                'exist',
                'targetClass' => InvoiceIncomeItem::class,
                'targetAttribute' => 'id',
                'filter' => ['or', ['company_id' => $this->company->id], ['company_id' => null]],
            ],
            [['contractor_id'], 'validateContractor', 'skipOnEmpty' => false],
            [['amount'], 'required'],
            [
                ['amount'],
                'number',
                'numberPattern' => Yii::$app->params['numberPattern'],
                'min' => 0.01,
                'max' => Yii::$app->params['maxCashSum'] * 100,
            ],
            [['project_id'], 'filter', 'filter' => function ($value) {
                return (int)$value > 0 ? $value : '';
            }],
            [
                ['project_id'],
                'exist',
                'targetClass' => Project::class,
                'targetAttribute' => 'id',
                'filter' => ['company_id' => $this->company->id],
                'skipOnEmpty' => true
            ],
        ];
    }

    /**
     * @return void
     */
    private function setFormData(): void
    {

        $this->setAttributes([
            'flow_type' => (string) $this->operation->flow_type,
            'account_id' => (string) $this->operation->account_id,
            'contractor_id' => (string) $this->operation->contractor_id,
            'expenditure_item_id' => (string) $this->operation->expenditure_item_id,
            'income_item_id' => (string) $this->operation->income_item_id,
            'amount' => TextHelper::invoiceMoneyFormat($this->operation->amount, 2, '.', ''),
            'description' => $this->operation->description,
            'time' => $this->operation->time,
            'date' => DateHelper::format(
                $this->operation->date,
                DateHelper::FORMAT_USER_DATE,
                DateHelper::FORMAT_DATE
            ),
            'recognition_date' => DateHelper::format(
                $this->operation->date,
                DateHelper::FORMAT_USER_DATE,
                DateHelper::FORMAT_DATE
            ),
            'project_id' => (string) $this->operation->project_id,
        ], false);
    }

    /**
     * @return array
     */
    private function getFormData(): array
    {
        return [
            'account_id' => $this->account_id,
            'contractor_id' => $this->contractor_id,
            'expenditure_item_id' => ($this->operation->flow_type == CardOperation::FLOW_TYPE_EXPENSE)
                ? $this->expenditure_item_id
                : null,
            'income_item_id' => ($this->operation->flow_type == CardOperation::FLOW_TYPE_INCOME)
                ? $this->income_item_id
                : null,
            'amount' => $this->amount * 100,
            'description' => $this->description,
            'time' => $this->time,
            'date' => DateHelper::format(
                $this->date,
                DateHelper::FORMAT_DATE,
                DateHelper::FORMAT_USER_DATE,
            ),
            'recognition_date' => DateHelper::format(
                $this->date,
                DateHelper::FORMAT_DATE,
                DateHelper::FORMAT_USER_DATE,
                ),
            'project_id' => $this->project_id
        ];
    }

    /**
     * @return string
     */
    private function getContractorLabel(): string
    {
        $contractorType = $this->getContractorType();

        if ($contractorType) {
            return Contractor::$contractorTitleSingle[$contractorType];
        }

        return 'Контрагент';
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateContractor($attribute, $params)
    {
        if ($this->$attribute) {
            $contractor = Contractor::find()->andWhere([
                'id' => $this->contractor_id,
            ])->andWhere([
                'or',
                ['company_id' => null],
                ['company_id' => $this->company->id],
            ])->one();

            if ($contractor === null) {
                $type = CashContractorType::findOne(['name' => $this->contractor_id]);
                if ($type == null) {
                    $this->addError($attribute, 'Контрагент не найден..');
                }
            }
        } else {
            $this->addError($attribute, 'Необходимо заполнить.');
        }
    }
}
