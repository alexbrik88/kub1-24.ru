<?php

namespace frontend\modules\cards\models;

use common\models\Company;
use common\modules\cards\models\CardIdentifierList;
use common\modules\import\models\AbstractAutoImportForm;
use common\modules\import\components\ImportCommandType;

class AutoImportForm extends AbstractAutoImportForm
{
    /**
     * @var int
     */
    private $accountType;

    /**
     * @param Company $company
     * @param ImportCommandType $commandType
     * @param int $accountType
     * @param string $identifier
     */
    public function __construct(
        Company $company,
        ImportCommandType $commandType,
        int $accountType,
        string $identifier
    ) {
        $this->accountType = $accountType;
        $identifierList = new CardIdentifierList($company, $accountType);

        parent::__construct($company, $commandType, $identifierList, $identifier);
    }
}
