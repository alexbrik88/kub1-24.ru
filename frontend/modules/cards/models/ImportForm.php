<?php

namespace frontend\modules\cards\models;

use common\components\date\DateHelper;
use common\models\employee\Employee;
use common\modules\cards\models\CardIdentifierList;
use common\modules\import\components\ImportCommandType;
use common\modules\import\models\AbstractImportForm2;
use common\modules\import\models\AutoImportFormInterface;

class ImportForm extends AbstractImportForm2
{
    /**
     * @var string
     */
    private $accountType;

    /**
     * @param Employee $employee
     * @param ImportCommandType $commandType
     * @param int $accountType
     * @param string $identifier
     */
    public function __construct(
        Employee $employee,
        ImportCommandType $commandType,
        int $accountType,
        string $identifier
    ) {
        $this->accountType = $accountType;
        $identifierList = new CardIdentifierList($employee->company, $accountType);

        parent::__construct($employee, $commandType, $identifierList, $identifier);
    }

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->dateTo = date(DateHelper::FORMAT_USER_DATE);
    }

    /**
     * @inheritDoc
     */
    public function createAutoImportForm(): AutoImportFormInterface
    {
        return new AutoImportForm(
            $this->getEmployee()->company,
            $this->getCommandType(),
            $this->accountType,
            $this->identifier
        );
    }
}
