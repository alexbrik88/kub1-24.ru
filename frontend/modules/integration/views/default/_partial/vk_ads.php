<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 04.03.2020
 * Time: 22:11
 */

use yii\helpers\Url;

/* @var $this yii\web\View
 * @var array $vkAds
 * @var bool $canManage
 */
?>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">ВКонтакте</div>
        <span class="<?= $vkAds['active'] ? 'status-active' : 'status-inactive' ?>"></span>
    </div>
    <div class="portlet-body">
        <p>Вы сможете выгружать статистику за произвольные даты</p>
        <?= $this->render('_link', [
            'text' => $vkAds['active'] ? 'Данные' : 'Подключить',
            'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze btn-block',
            'url' => Url::to('/integration/vk-ads/index'),
            'canManage' => $canManage,
        ]) ?>
        <?php if ($vkAds['active']): ?>
            <?= $this->render('_link', [
                'text' => 'Отключить',
                'class' => 'btn btn__ins btn-sm default btn_marg_down red-haze btn-block',
                'url' => Url::to('/integration/vk-ads/disconnect'),
                'canManage' => $canManage,
            ]) ?>
        <?php endif; ?>
    </div>
</div>