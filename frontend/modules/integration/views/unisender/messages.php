<?php

namespace frontend\modules\integration\views;

use frontend\modules\integration\models\unisender\ImportForm;
use yii\bootstrap\ActiveForm;
use common\components\date\DateHelper;
use common\components\helpers\Html;
use frontend\widgets\RangeButtonWidget;
use common\components\grid\GridView;
use common\models\unisender\UnisenderMessage;
use Yii;
use yii\helpers\Url;
use yii\web\View;

$this->title = 'Unisender';

/**
 * @var View $this
 * @var ImportForm $model
 */

?>
<div class="portlet box">
    <h3 class="page-title yd-title"><?= $this->title; ?></h3>

    <?php $form = ActiveForm::begin([
        'id' => 'get-yandex-direct-report',
    ]); ?>

    <div class="form-group">
        <?= $form->field($model, 'dateFrom', [
            'options' => [
                'class' => 'col-md-4',
            ],
            'wrapperOptions' => [
                'class' => 'yd-input',
            ],
            'template' => Yii::$app->params['formDatePickerTemplate'],
        ])->textInput([
            'class' => 'form-control date-picker min_wid_picker',
        ])->label('с'); ?>

        <?= $form->field($model, 'dateTo', [
            'options' => [
                'class' => 'col-md-4',
                'style' => 'margin-right: 40px;',
            ],
            'wrapperOptions' => [
                'class' => 'yd-input',
            ],
            'template' => Yii::$app->params['formDatePickerTemplate'],
        ])->textInput([
            'class' => 'form-control date-picker min_wid_picker',
        ])->label('по') ?>

        <div class="btn-group pull-right title-buttons">
            <?= Html::submitButton('ВЫГРУЗИТЬ', [
                'class' => 'btn yellow',
            ]); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

    <div class="row" style="margin-top: 20px;margin-bottom: 25px;">
        <div class="col-md-9 col-sm-9"></div>
        <div class="col-md-3 col-sm-3">
            <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
        </div>
    </div>

    <div class="portlet box darkblue">
        <div class="portlet-title"></div>
        <div class="portlet-body accounts-list">
            <div class="scroll-table-wrapper">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable customers_table fix-thead',
                        'id' => 'datatable_ajax',
                        'aria-describedby' => 'datatable_ajax_info',
                        'role' => 'grid',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    'options' => [
                        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                    'columns' => [
                        [
                            'label' => '',
                            'format' => 'raw',
                            'value' => function (UnisenderMessage $message) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-refresh"></span>',
                                    Url::to(['update-status', 'id' => $message->id]),
                                    [
                                        'title' => 'Обновить статус',
                                    ]
                                );
                            },
                        ],
                        [
                            'attribute' => 'start_date',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (UnisenderMessage $message) {
                                return date(DateHelper::FORMAT_SUER_DATETIME_WITHOUT_SECONDS, $message->start_date);
                            },
                        ],
                        [
                            'attribute' => 'subject',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (UnisenderMessage $message) {
                                return $message->subject;
                            },
                        ],
                        [
                            'attribute' => 'status',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (UnisenderMessage $message) {
                                return UnisenderMessage::STATUSES_MAP[$message->status] ?? 'unknown';
                            },
                        ],
                        [
                            'attribute' => 'sender_name',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (UnisenderMessage $message) {
                                return $message->sender_name;
                            },
                        ],
                        [
                            'attribute' => 'sender_email',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (UnisenderMessage $message) {
                                return $message->sender_email;
                            },
                        ],
                        [
                            'attribute' => 'total_contacts_count',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (UnisenderMessage $message) {
                                return $message->total_contacts_count;
                            },
                        ],
                        [
                            'attribute' => 'sent_messages_count',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (UnisenderMessage $message) {
                                return $message->sent_messages_count;
                            },
                        ],
                        [
                            'attribute' => 'delivered_messages_count',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (UnisenderMessage $message) {
                                return $message->delivered_messages_count;
                            },
                        ],
                        [
                            'attribute' => 'read_unique_count',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (UnisenderMessage $message) {
                                return $message->read_unique_count;
                            },
                        ],
                        [
                            'attribute' => 'read_count',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (UnisenderMessage $message) {
                                return $message->read_count;
                            },
                        ],
                        [
                            'attribute' => 'clicked_unique_count',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (UnisenderMessage $message) {
                                return $message->clicked_unique_count;
                            },
                        ],
                        [
                            'attribute' => 'clicked_count',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (UnisenderMessage $message) {
                                return $message->clicked_count;
                            },
                        ],
                        [
                            'attribute' => 'unsubscribed_contacts_count',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (UnisenderMessage $message) {
                                return $message->unsubscribed_contacts_count;
                            },
                        ],
                        [
                            'attribute' => 'spam_contacts_count',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (UnisenderMessage $message) {
                                return $message->spam_contacts_count;
                            },
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
