<?php

namespace frontend\modules\integration\controllers;

use common\models\employee\Employee;
use common\models\unisender\UnisenderMessage;
use common\modules\import\components\ImportCommandManager;
use common\tasks\ExportUnisender;
use frontend\components\FrontendController;
use frontend\components\StatisticPeriod;
use frontend\controllers\WebTrait;
use frontend\modules\integration\models\unisender\ImportForm;
use frontend\modules\integration\models\unisender\MessagesSearch;
use frontend\modules\integration\models\unisender\UnisenderSettingsForm;
use frontend\modules\integration\models\unisender\UpdateStatusForm;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class UnisenderController extends FrontendController
{
    use WebTrait;

    /**
     * @return array
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => 'common\components\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\UserRole::ROLE_CHIEF,
                        ],
                    ],
                ],
            ],
        ]);
    }

    public function actionConnect()
    {
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $model = new UnisenderSettingsForm();
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

            if ($model->storeToUser($user)) {
                Yii::$app->session->setFlash('success', 'Настройки интеграции сохранены');

                return $this->redirect('messages');
            }
        } else {
            $model->load($user->integration(Employee::INTEGRATION_UNISENDER), '');
        }

        return $this->render('connect', [
            'model' => $model,
        ]);
    }

    public function actionDisconnect()
    {
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $integration = $user->integration;
        if (isset($integration[Employee::INTEGRATION_UNISENDER])) {
            unset($integration[Employee::INTEGRATION_UNISENDER]);
        }
        $user->integration = empty($integration) ? null : $integration;
        if ($user->save(true, ['integration'])) {
            Yii::$app->session->setFlash('success', 'Интеграция с Unisender отключена.');
        } else {
            Yii::$app->session->setFlash('success', 'Произошла ошибка при отключении интеграции с Unisender.');
        }

        return $this->redirect('/integration');
    }

    public function actionMessages()
    {
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $searchModel = new MessagesSearch();
        $dataProvider = $searchModel->search($user, Yii::$app->request->get());
        $form = new ImportForm(
            $this->user->identity,
            ImportCommandManager::getInstance()->getTypeByClass(ExportUnisender::class)
        );

        if ($form->load($this->request->post()) && $form->validate()) {
            if (!$form->createJob()) {
                Yii::$app->session->setFlash('error', 'Произошла ошибка при создании задачи на выгрузку писем.');
            } else {
                Yii::$app->session->setFlash('success', 'Задача на выгрузку писем создана.');
            }
        }

        return $this->render('messages', [
            'model' => $form,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdateStatus($id)
    {
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $model = $this->findModel($id);
        $form = new UpdateStatusForm($user, $model);
        if ($form->update()) {
            Yii::$app->session->setFlash('success', 'Статус обновлен.');
        } else {
            Yii::$app->session->setFlash('error', 'Произошла ошибка при обновлении статуса.');
        }

        return $this->redirect('messages');
    }

    private function handleGetOperationsRequest(Employee $user, $result, $dateFrom, $dateTo)
    {
        if ($result['result'] === false && isset($result['redirectUrl'])) {
            Yii::$app->session->setFlash('error', 'Неверный ключ доступа к API.');

            return $this->redirect($result['redirectUrl']);
        }

        if ($result['result'] === false) {
            $message = $result['message'] ?? 'Произошла ошибка при выгрузке писем.';
            Yii::$app->session->setFlash('error', $message);
        } else {
            $user->setStatisticRangeDates($dateFrom, $dateTo);
            $user->setStatisticRangeName(StatisticPeriod::CUSTOM_RANGE, $dateFrom, $dateTo);
            Yii::$app->session->setFlash('success', 'Письма успешно сохранены.');
        }

        return $this->redirect('messages');
    }

    /**
     * @param $id
     * @return UnisenderMessage
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = UnisenderMessage::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }

        return $model;
    }
}
