<?php

namespace frontend\modules\integration\controllers;

use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\models\employee\Employee;
use frontend\components\FrontendController;
use frontend\rbac\UserRole;
use Yii;

/**
 * Настройка интеграции разных модулей
 * Контроллер реализует только список модулей, настрйки и интерфейс по каждому модулю реализуется в "своём" контроллере.
 */
class DefaultController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => 'common\components\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\UserRole::ROLE_CHIEF,
                        ],
                        // Удалить условие, когда задача будет принята
                        'matchCallback' => function ($rule, $action) {
                            return YII_ENV_DEV || in_array(Yii::$app->user->identity->company->id, [486, 23083, 1, 1119]);
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     */
    public function actionIndex(): string
    {
        $employee = Yii::$app->user->identity;
        /** @var Employee $employee */
        $config = $employee->company->integration;
        $popupMessage = Yii::$app->session->get('popupMessage');
        if ($popupMessage !== null) {
            Yii::$app->session->remove('popupMessage');
        }

        return $this->render('index', [
            'user' => $employee,
            'canManage' => Yii::$app->user->can(UserRole::ROLE_CHIEF),
            'popupMessage' => $popupMessage,
            'email' => [
                'active' => is_array($config[Employee::INTEGRATION_EMAIL] ?? null),
                'configured' => is_array($config[Employee::INTEGRATION_EMAIL] ?? null),
            ],
            'amocrm' => [
                'active' => is_array($config[Employee::INTEGRATION_AMOCRM] ?? null),
                'configured' => is_array($config[Employee::INTEGRATION_AMOCRM] ?? null),
            ],
            'facebook' => [
                'active' => Yii::$app->request->cookies->get('integrationFBUser-' . $employee->company_id) && Yii::$app->request->cookies->get('integrationFBToken-' . $employee->company_id),
            ],
            'evotor' => [
                'active' => $config[Employee::INTEGRATION_EVOTOR] ?? false,
                'configured' => $config[Employee::INTEGRATION_EVOTOR] ?? false,
            ],
            'vkAds' => [
                'active' => is_array($config[Employee::INTEGRATION_VK] ?? null),
            ],
            'unisender' => [
                'active' => is_array($config[Employee::INTEGRATION_UNISENDER] ?? null),
            ],
            'insales' => [
                'active' => $config[Employee::INTEGRATION_INSALES] ?? false,
                'configured' => $config[Employee::INTEGRATION_INSALES] ?? false,
            ],
            'bitrix24' => [
                'active' => isset($config[Employee::INTEGRATION_BITRIX24]),
                'configured' => isset($config[Employee::INTEGRATION_BITRIX24]),
            ],
            'googleAds' => [
                'active' => isset(
                    $config[Employee::INTEGRATION_GOOGLE_ADS]['accessToken'],
                    $config[Employee::INTEGRATION_GOOGLE_ADS]['expiresAt']
                ),
            ],
            'yandexDirect' => [
                'active' => is_array($config[Employee::INTEGRATION_YANDEX_DIRECT] ?? null),
            ],
            'dreamkas' => [
                'active' => true,
                'configured' => is_array($config[Employee::INTEGRATION_DREAMKAS] ?? null)
            ]
        ]);
    }

}
