<?php

namespace frontend\modules\integration\controllers;

use yii\base\Action;
use yii\web\Request;
use yii\web\Response;
use Yii;

class TokenRedirectAction extends Action
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->request = Yii::$app->request;
    }

    /**
     * @return Response
     */
    public function run(): Response
    {
        $url = "/analytics/marketing/{$this->controller->id}/token";
        $query = http_build_query($this->request->get());

        if ($query) {
            $url .= '?' . $query;
        }

        return $this->controller->redirect($url);
    }
}
