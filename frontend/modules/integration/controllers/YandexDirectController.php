<?php

namespace frontend\modules\integration\controllers;

use frontend\components\FrontendController;

class YandexDirectController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => 'common\components\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\UserRole::ROLE_CHIEF,
                        ],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @inheritDoc
     */
    public function actions()
    {
        return [
            'token' => [
                'class' => TokenRedirectAction::class,
            ],
        ];
    }
}
