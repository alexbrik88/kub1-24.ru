<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.10.2019
 * Time: 0:47
 */

namespace frontend\modules\integration\models\unisender;

use common\components\sender\unisender\unisenderApi;
use common\models\employee\Employee;
use common\models\unisender\UnisenderMessage;
use yii\base\Model;

class UpdateStatusForm extends Model
{
    /** @var Employee */
    private $user;

    /** @var unisenderApi */
    private $unisenderApi;

    /** @var UnisenderMessage */
    private $message;

    public function __construct(Employee $user, UnisenderMessage $message, array $config = [])
    {
        $this->user = $user;
        $this->message = $message;
        $unisenderParams = $this->user->integration(Employee::INTEGRATION_UNISENDER);
        $this->unisenderApi = new unisenderApi($unisenderParams['apiKey'] ?? null);
        parent::__construct($config);
    }

    public function update()
    {
        $message = $this->message;
        $response = json_decode($this->unisenderApi->getCampaignStatus(['campaign_id' => $message->unisender_id]));
        if (isset($response->error)) {
            return false;
        }

        if (!isset($response->result)) {
            return false;
        }

        $message->status = $response->result->status;
        if (!$message->save(true, ['status'])) {
            return false;
        }

        return true;
    }
}