<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.10.2019
 * Time: 0:43
 */

namespace frontend\modules\integration\models\unisender;

use common\components\sender\unisender\unisenderApi;
use common\models\employee\Employee;
use Yii;
use yii\base\Model;

class UnisenderSettingsForm extends Model
{
    public $apiKey;


    /** @inheritDoc */
    public function rules(): array
    {
        return [
            [['apiKey'], 'required'],
            [['apiKey'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'apiKey' => 'Ключ доступа к API',
        ];
    }

    public function storeToUser(Employee $user)
    {
        if (!$this->validate()) {
            return false;
        }

        $unisenderApi = new unisenderApi($this->apiKey);
        $result = json_decode($unisenderApi->getLists());
        if (isset($result->error) && $result->code === 'invalid_api_key') {
            $this->addError('password', 'Неверный ключ доступа к API.');
            return false;
        }

         $this->apiKey = base64_encode(Yii::$app->security->encryptByPassword($this->apiKey, Yii::$app->params['moneta']['encrypt_password']));
         $user->saveIntegration(Employee::INTEGRATION_UNISENDER, $this->toArray());

        return $user->save(false, ['integration']);
    }
}