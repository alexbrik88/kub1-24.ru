<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.10.2019
 * Time: 14:56
 */

namespace frontend\modules\integration\models\unisender;

use common\models\employee\Employee;
use common\models\unisender\UnisenderMessage;
use frontend\components\StatisticPeriod;
use yii\data\ActiveDataProvider;

class MessagesSearch extends UnisenderMessage
{
    public function search(Employee $user, $params = [])
    {
        $period = StatisticPeriod::getSessionPeriod();
        $query = UnisenderMessage::find()
            ->andWhere([
                'AND',
                ['employee_id' => $user->id],
                ['company_id' => $user->currentEmployeeCompany->company_id],
                ['between', 'start_date', strtotime($period['from'] . ' 00:00:00'), strtotime($period['to'] . ' 23:59:59')]
            ])
            ->orderBy(['start_date' => SORT_DESC]);

        $this->load($params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}