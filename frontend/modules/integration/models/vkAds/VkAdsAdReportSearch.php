<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.03.2020
 * Time: 0:35
 */

namespace frontend\modules\integration\models\vkAds;

use common\models\employee\Employee;
use yii\data\ActiveDataProvider;

class VkAdsAdReportSearch extends AbstractReportSearch
{
    public $campaignId;
    public $search;

    public function rules()
    {
        return [
            [['search'], 'string'],
        ];
    }

    public function search(Employee $user, $params = [])
    {
        $this->load($params);
        $query = $this->getBaseQuery($user)->joinWith('ad ad');

        $query->andFilterWhere(['LIKE', 'ad.name', $this->search]);

        $this->maxDate = $this->getBaseQuery($user)->max('date');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

    public function getBaseQuery(Employee $user)
    {
        return parent::getBaseQuery($user)->andWhere(['r.campaign_id' => $this->campaignId]);
    }
}
