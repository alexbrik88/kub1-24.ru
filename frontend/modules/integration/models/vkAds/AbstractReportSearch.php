<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.03.2020
 * Time: 0:37
 */

namespace frontend\modules\integration\models\vkAds;

use common\models\employee\Employee;
use common\models\vkAds\VkAdsReport;
use frontend\components\StatisticPeriod;
use yii\base\Model;
use yii\db\ActiveQuery;

abstract class AbstractReportSearch extends Model
{
    protected $maxDate;

    abstract public function search(Employee $user, $params = []);

    public function getMaxDate()
    {
        return $this->maxDate;
    }

    /**
     * @param Employee $user
     * @return ActiveQuery
     */
    public function getBaseQuery(Employee $user)
    {
        $period = StatisticPeriod::getSessionPeriod();

        return VkAdsReport::find()
            ->alias('r')
            ->andWhere(['r.company_id' => $user->currentEmployeeCompany->company_id])
            ->andWhere(['between', 'r.date', $period['from'], $period['to']]);
    }
}