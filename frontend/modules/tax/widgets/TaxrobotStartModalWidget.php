<?php

namespace frontend\modules\tax\widgets;

use common\models\Company;
use common\models\company\RegistrationPageType;
use frontend\modules\tax\assets\TaxrobotStartJsAsset;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 *
 */
class TaxrobotStartModalWidget extends \yii\base\Widget
{
    /**
     * @var common\components\TaxRobotHelper
     */
    public $taxRobot;

    /**
     * @var commom\models\Company
     */
    public $company;

    /**
     * @var boolean
     */
    public $isFirstVisit;

    /**
     * @var bool
     */
    private static $_can;

    private static $_banking;

    /**
     * @var array
     */
    public static $regPageTypes = [
        RegistrationPageType::PAGE_TYPE_BANK_ROBOT,
        RegistrationPageType::PAGE_TYPE_DECLARATION_TEMPLATE_IP,
        RegistrationPageType::PAGE_TYPE_BANK_OTKRITIE_TAXROBOT,
    ];

    /**
     * @return
     */
    public static function can(Company $company)
    {
        if (!isset(self::$_can)) {
            $pageTypeId = $company->registration_page_type_id;
            self::$_banking = Yii::$app->session->remove('robot-start-banking');

            $startModal = ArrayHelper::getValue(Yii::$app->params, 'tax-robot-start-modal');
            //$startIds = (array) ArrayHelper::getValue(Yii::$app->params, 'tax-robot-start-ids');

            self::$_can = (
                isset(self::$_banking) ||
                (isset($startModal) && $startModal) ||
                //(!empty($startIds) && in_array($company->id, $startIds)) ||
                ($company->strict_mode /*&& in_array($pageTypeId, self::$regPageTypes)*/)
            );
        }

        return self::$_can;
    }

    /**
     * Renders the widget.
     */
    public function run()
    {

        if (self::can($this->company)) {
            TaxrobotStartJsAsset::register($this->view);

            echo $this->render('taxrobotStartModalWidget', [
                'taxRobot' => $this->taxRobot,
                'company' => $this->company,
                'isFirstVisit' => $this->isFirstVisit,
                'banking' => self::$_banking,
            ]);
        }
    }
}
