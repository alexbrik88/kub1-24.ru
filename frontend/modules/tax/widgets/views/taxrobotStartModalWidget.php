<?php

use frontend\components\Icon;
use frontend\modules\tax\assets\TaxrobotStartCssAsset;
use yii\bootstrap\Modal;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $company common\models\Company */
/** @var $taxRobot common\components\TaxRobotHelper */
/** @var $banking mixed */
/** @var $isFirstVisit boolean */

TaxrobotStartCssAsset::register($this);

$alias = ArrayHelper::getValue($banking, 'alias');
$account_id = ArrayHelper::getValue($banking, 'account_id');

$isBanking = isset($alias, $account_id);
?>

<?php Modal::begin([
    'id' => 'taxrobot-start-modal',
    'closeButton' => false,
    'options' => [
        'data-index-url' => Url::to(['/tax/robot-start/index']),
        'data-file-url' => Url::to(['/tax/robot-start/upload-file']),
        'data-upload-url' => Url::to(['/tax/robot-start/upload']),
        'data-banking-url' => $isBanking ? Url::to([
            '/tax/robot-start/banking',
            'alias' => $alias,
            'account_id' => $account_id,
        ]) : null,
    ],
    'clientOptions' => [
        'show' => $isFirstVisit || $isBanking,
    ],
]); ?>

<?php Modal::end(); ?>
