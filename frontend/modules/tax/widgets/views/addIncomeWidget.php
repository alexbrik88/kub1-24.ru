<?php

use common\components\TaxRobotHelper;
use common\models\cash\CashFlowsBase;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use frontend\modules\cash\modules\ofd\widgets\OfdModalWidget;
use frontend\components\Icon;
use yii\bootstrap\Modal;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $taxRobot common\components\TaxRobotHelper */
/* @var $company common\models\Company */
/* @var $accountArray common\models\company\CheckingAccountant[] */
/* @var $cashbox common\models\cash\Cashbox */
/* @var $hasIncomes boolean */
/* @var $hasPeriodIncomes boolean */

$p = Banking::currentRouteEncode();
$bankUrl = [
    '/cash/banking/default/select',
    'p' => $p,
];
foreach ($accountArray as $account) {
    if (($class = Banking::classByBik($account->bik)) !== null) {
        $bankUrl = [
            '/cash/banking/'.$class::ALIAS.'/default/index',
            'account_id' => $account->id,
            'p' => $p,
        ];
        break;
    }
}
$redirectUrl = Url::to(Banking::currentRoute());
?>

<?php Modal::begin([
    'id' => 'add-income-modal',
    'header' => '<h1>Добавить ДОХОД</h1>',
]) ?>

    <div>
        для расчета налогов и заполнения декларации.
    </div>
    <div class="text-bold">
        Выберите один из способов добавления доходов по вашему ИП:
    </div>

    <div class="fx-row mt-4 align-items-stretch" style="height: 120px;">
        <div class="col-sm-4 text-center">
            <?= Html::a(Icon::get('bank-3', [
                'class' => 'mr-3',
            ]).'Загрузить выписку из банка', $bankUrl, [
                'class' => 'button-regular button-hover-content-red w-100 h-100 banking-module-open-link',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
        <div class="col-sm-4 text-center">
            <?= Html::a(Icon::get('1c', [
                'class' => 'mr-3',
            ]).'Загрузить выписку файлом', [
                "/cash/banking/default/file",
                'p' => $p,
                's' => 'f',
            ], [
                'class' => 'button-regular button-hover-content-red w-100 h-100 banking-module-open-link',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
        <div class="col-sm-4 text-center">
            <?= Html::a(Icon::get('mix', [
                'class' => 'mr-3',
            ]).Html::tag('span', 'Добавить вручную', [
                'style' => 'display: inline-block; width: 100%;'
            ]), '#', [
                'class' => 'button-regular button-hover-content-red w-100 h-100',
                'data-toggle' => 'modal',
                'data-target' => '#manual_entry_modal',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
    </div>
    <div class="fx-row mt-4 align-items-stretch">
        <div class="col-sm-4 text-center">
            <?= Html::a('Выбрать', $bankUrl, [
                'class' => 'button-regular button-regular_red min-w-130 banking-module-open-link',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
        <div class="col-sm-4 text-center">
            <?= Html::a('Выбрать', [
                "/cash/banking/default/index",
                'p' => $p,
                's' => 'f',
            ], [
                'class' => 'button-regular button-regular_red min-w-130 banking-module-open-link',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
        <div class="col-sm-4 text-center">
            <?= Html::button('Выбрать', [
                'class' => 'button-regular button-regular_red min-w-130',
                'data-toggle' => 'modal',
                'data-target' => '#manual_entry_modal',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
    </div>

<?php Modal::end(); ?>

<?= BankingModalWidget::widget() ?>
