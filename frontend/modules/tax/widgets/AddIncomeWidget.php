<?php

namespace frontend\modules\tax\widgets;

use Yii;
use yii\helpers\Html;

/**
 *
 */
class AddIncomeWidget extends \yii\base\Widget
{
    /**
     * @var common\components\TaxRobotHelper
     */
    public $taxRobot;

    /**
     * @var commom\models\Company
     */
    public $company;

    /**
     * @var common\models\cash\Cashbox
     */
    public $cashbox;

    /**
     * @var array the HTML attributes for the widget container tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $accountArray = [];

    /**
     * Renders the widget.
     */
    public function run()
    {
        $hasIncomes = $this->company->getCashBankFlows()->exists() || $this->company->getCashOrderFlows()->exists();
        $hasPeriodIncomes = $this->taxRobot->getIncomeSearchQuery()->select('id')->exists();

        echo $this->render('addIncomeWidget', [
            'taxRobot' => $this->taxRobot,
            'company' => $this->company,
            'accountArray' => $this->accountArray,
            'cashbox' => $this->cashbox,
            'hasIncomes' => $hasIncomes,
            'hasPeriodIncomes' => $hasPeriodIncomes,
        ]) . "\n";

        if (!$hasPeriodIncomes) {
            $this->view->registerJs('
                $("#taxrobot-next-step-btn.taxrobot-pay-panel-trigger").toggleClass("taxrobot-pay-panel-trigger", false);
                $(document).on("click", "#taxrobot-next-step-btn:not(.taxrobot-pay-panel-trigger)", function (e) {
                    e.preventDefault();
                    $("#add-income-modal").modal("show");
                    $("#add-income-modal .next-step-note").toggleClass("hidden", false);
                });
                $(document).on("hidden.bs.modal", "#add-income-modal", function () {
                    $("#add-income-modal .next-step-note").toggleClass("hidden", true);
                });
            ');
        }
    }
}
