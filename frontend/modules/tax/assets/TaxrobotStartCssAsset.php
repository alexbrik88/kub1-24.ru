<?php

namespace frontend\modules\tax\assets;

use yii\web\AssetBundle;

/**
 * Class TaxrobotStartCssAsset
 */
class TaxrobotStartCssAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/modules/tax/assets/web';

    /**
     * @var array
     */
    public $css = [
        'css/taxrobot-start-modal.css',
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
