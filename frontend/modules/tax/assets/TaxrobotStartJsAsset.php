<?php

namespace frontend\modules\tax\assets;

use yii\web\AssetBundle;

/**
 * Class TaxrobotStartJsAsset
 */
class TaxrobotStartJsAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/modules/tax/assets/web';

    /**
     * @var array
     */
    public $js = [
        'js/taxrobot-start-modal.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
