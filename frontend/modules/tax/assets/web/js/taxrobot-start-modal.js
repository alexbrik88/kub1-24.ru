
!function( $ ) {

    $(document).ready(function () {
        var $modal = $("#taxrobot-start-modal");
        var startUrl = $modal.data("banking-url") || $modal.data("index-url");

        $(".modal-body", $modal).load(startUrl);

        var getModalContainer = function (el) {
            return $(".modal-body", $modal);
            /*var container = $(el).closest(".taxrobot-start-banking", $modal);
            if (container.length == 0) {
                container = $(".modal-body", $modal);
            }

            return container;*/
        };

        var animatedCounter = function (el) {
            var time = 15 * 1000; // 15 sec
            var current = 1500;
            var end = 0;
            var interval = time / current;
            var timer = setInterval(function() {
                current -= 1;
                $(el).html((current/100).toFixed(2));
                if (current == end) {
                    clearInterval(timer);
                    $(".counter-toggle-hide", $modal).toggleClass("hidden");
                }
            }, interval);
        };

        var checkModalContent = function () {
            $('#taxrobotstartform-inn', $modal).each(function(i, el) {
                $(el).suggestions({
                    serviceUrl: 'https://dadata.ru/api/v2',
                    token: '78497656dfc90c2b00308d616feb9df60c503f51',
                    type: 'PARTY',
                    count: 10,

                    onSelect: function(suggestion) {
                        $(el).val(suggestion.data.inn);
                    }
                });
            });

            $('#taxrobotstartform-address', $modal).each(function(i, el) {
                $(el).suggestions({
                    serviceUrl: 'https://dadata.ru/api/v2',
                    token: '78497656dfc90c2b00308d616feb9df60c503f51',
                    type: 'ADDRESS',
                    onSelect: function(suggestion) {
                        var error = 'Уточните адрес.';
                        if (suggestion.data) {
                            $('#taxrobotstartform-ifns').val(suggestion.data.tax_office_legal || '');
                            $('#taxrobotstartform-oktmo').val(suggestion.data.oktmo.slice(0,8) || '');
                            $('#taxrobotstartform-index').val(suggestion.data.postal_code || '');
                        }
                    }
                });
            });

            $('.counter-element', $modal).each(function(i, el) {
                animatedCounter(el);
            });

            $('.date-picker', $modal).each(function(i, el) {
                $(el).datepicker(kubDatepickerConfig);
            });
        };

        $(document).on("click", "[data-target=\"#taxrobot-start-modal\"]", function (e) {
            $(".modal-body", $modal).html("").load("/tax/robot-start/activity");
        });

        $(document).on("click", "#taxrobot-start-modal [href]:not(a)", function (e) {
            e.preventDefault();
            var url = this.getAttribute("href");
            if ($(this).hasClass('ladda-button')) {
                if (!this.hasAttribute( 'data-style' )) {
                    this.setAttribute( 'data-style', 'expand-right' );
                }
                var l = Ladda.create(this);
                l.start();
            }
            if ($(this).hasClass("not-ajax")) {
                window.location.replace(url);
            } else {
                $.ajax(url, {
                    success: function (data, textStatus, jqXHR) {
                        $(".modal-body", $modal).html(data);
                        checkModalContent();
                    }
                });
            }
            return false;
        });

        $(document).on("submit", "#taxrobot-start-modal form:not(#vidimus_form_ajax)", function (e) {
            if ($(this).closest("#banking-module-pjax").length == 0) {
                var form = this;
                var laddaBtn = $('.ladda-button:submit', form);
                var formData = new FormData(form);
                if (laddaBtn.length > 0) {
                    var button = laddaBtn[0];
                    if (!button.hasAttribute( 'data-style' )) {
                        button.setAttribute( 'data-style', 'expand-right' );
                    }
                    var l = Ladda.create(button);
                    l.start();
                }
                if (form.action.includes(location.hostname)) {
                    e.preventDefault();
                    $.ajax(form.action, {
                        method: form.method,
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function (data, textStatus, jqXHR) {
                            $(".modal-body", $modal).html(data);
                            checkModalContent();
                        }
                    });
                    return false;
                }
            }
        });

        $(document).on("submit", "#taxrobot-start-modal #vidimus_form_ajax", function (e) {
            e.preventDefault();
            var form = this;
            var laddaBtn = $('.ladda-button:submit', form);
            var formData = new FormData(form);
            $.ajax($modal.data("upload-url"), {
                method: form.method,
                data: formData,
                contentType: false,
                processData: false,
                success: function (data, textStatus, jqXHR) {
                    $(".modal-body", $modal).html(data);
                    checkModalContent();
                }
            });
            if (laddaBtn.length > 0 && laddaBtn.closest("#banking-module-pjax").length == 0) {
                var button = laddaBtn[0];
                if (!button.hasAttribute( 'data-style' )) {
                    button.setAttribute( 'data-style', 'expand-right' );
                }
                var l = Ladda.create(button);
                l.start();
            }
        });

        $(document).on("click", "#taxrobot-start-modal .banking-modal-scroll-down", function (e) {
            e.preventDefault();
            $modal.animate({scrollTop: $(".modal-content", $modal).height()}, 250);
        });

        $(document).on('pjax:complete', "#taxrobot-start-modal #banking-module-pjax", function() {
            checkModalContent();
        });

        $( document ).ajaxComplete(function( event, xhr, settings ) {
            Ladda.stopAll();
        });
    });

}( window.jQuery );
