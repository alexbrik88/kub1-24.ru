<?php

namespace frontend\modules\tax;

use common\models\company\CompanyType;
use frontend\rbac\UserRole;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * tax module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\tax\controllers';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            $user = Yii::$app->user;
                            if (($employee = $user->identity) === null) {
                                return false;
                            }

                            if (!$user->can(UserRole::ROLE_CHIEF)) {
                                throw new ForbiddenHttpException('У Вас недостаточно прав для данного действия. Обратитесь к руководителю.');
                            }
                            if (($company = $employee->company) === null) {
                                throw new ForbiddenHttpException('Не найдена текущая компания пользователя.');
                            }
                            if (!($company->company_type_id == CompanyType::TYPE_IP && (
                                $action->controller->id == 'robot-start' ||
                                ($action->controller->id == 'robot' && (in_array($action->id, ['index', 'company', 'params']) ))
                            ))) {
                                if (!($company->getCanTaxModule() || $company->getCanOOOTaxModule())) {
                                    throw new ForbiddenHttpException('Ваша текущая компания не подходит под критерии оказания услуги КУБ.Бухгалтерия.');
                                }
                            }

                            return true;
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
        $company = (Yii::$app->user && Yii::$app->user->identity && Yii::$app->user->identity->company) ?
            Yii::$app->user->identity->company :
            null;

        if ($company && $company->company_type_id == CompanyType::TYPE_IP) {
            Yii::$app->set('errorHandler', [
                'class' => 'yii\web\ErrorHandler',
                'errorAction' => 'tax/robot/error',
            ]);
            Yii::$app->get('errorHandler')->register();
        }
    }
}
