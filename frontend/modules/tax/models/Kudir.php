<?php

namespace frontend\modules\tax\models;

use common\components\helpers\ModelHelper;
use Yii;
use common\models\Company;
use common\models\employee\Employee;
use common\models\document\status\TaxDeclarationStatus;
use frontend\models\log\Log;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\models\log\LogEntityType;
use yii\helpers\Html;
use common\models\document\AbstractDocument;
use frontend\models\log\ILogMessage;
use common\models\ICompanyStrictQuery;

/**
 * This is the model class for table "kudir".
 *
 * @property integer $id
  * @property integer $company_id
 * @property integer $created_at
 * @property integer $document_author_id
 * @property integer $status_id
 * @property integer $status_updated_at
 * @property integer $status_author_id
 * @property string $document_date
 * @property integer $tax_year
 *
 * @property Company $company
 * @property Employee $documentAuthor
 * @property Employee $statusAuthor
 * @property TaxDeclarationStatus $status
 */
class Kudir extends AbstractDocument implements ILogMessage
{
    /**
     * The maximum uploaded file count
     */
    const MAX_FILES = 3;

    public static $uploadDirectory = 'kudir';
    public $files_upload;
    public $files_delete;

    /**
     * @var string
     */
    public $urlPart = 'kudir';

    /**
     * @inheritdoc
     */
    public $printablePrefix = 'КУДиР';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kudir';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['document_date', 'tax_year'], 'required'],
            [['company_id', 'created_at', 'document_author_id', 'status_id', 'status_updated_at', 'status_author_id', 'tax_year'], 'integer'],
            [['document_date'], 'safe'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['document_author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['document_author_id' => 'id']],
            [['status_author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['status_author_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaxDeclarationStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['tax_year'], 'unique', 'filter' => ['company_id' => $this->company_id]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'created_at' => 'Created At',
            'document_author_id' => 'Document Author ID',
            'status_id' => 'Status ID',
            'status_updated_at' => 'Status Updated At',
            'status_author_id' => 'Status Author ID',
            'document_date' => 'Дата',
            'tax_year' => 'Отчетный год'
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            ModelHelper::HtmlEntitiesModelAttributes($this);

            if ($insert) {
                $this->created_at = time();
                $this->status_id = TaxDeclarationStatus::STATUS_CREATED;
                $this->status_updated_at = time();
                $this->status_author_id = Yii::$app->user->id;
                $this->document_author_id = Yii::$app->user->id;
            }

            return true;
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'document_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'status_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(TaxDeclarationStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentStatus()
    {
        return $this->getStatus();
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        $text = 'КУДиР за ' . $log->getModelAttributeNew('tax_year');

        $link = Html::a($text, [
            '/tax/kudir/view',
            'id' => $log->getModelAttributeNew('id'),
        ]);

        $innerText = null;

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . $innerText . ' была создана.';
            case LogEvent::LOG_EVENT_DELETE:
                return $text . $innerText . ' была удалёна.';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . $innerText . ' была изменена.';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                $status_id = $log->getModelAttributeNew('status_id');
                $status_name = TaxDeclarationStatus::findOne($status_id)->name;
                return $link . $innerText . ' статус "' . $status_name . '"';
            default:
                return $log->message;
        }
    }

    /**
     * @inheritdoc
     */
    public function getPdfFileName()
    {
        return $this->getPrintTitle() . '.pdf';
    }

    /**
     * @return string
     */
    public function getPrintTitle()
    {
        return $this->printablePrefix;
    }

    public function getFullNumber()
    {
        return "";
    }

    public function getFile() {}

    public function beforeDelete()
    {
        return true;
    }
}
