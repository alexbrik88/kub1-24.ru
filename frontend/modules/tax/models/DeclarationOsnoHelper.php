<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 29.03.2019
 * Time: 0:29
 */

namespace frontend\modules\tax\models;


use common\components\helpers\Month;
use yii\base\InvalidParamException;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class DeclarationOsnoHelper
 * @package frontend\modules\tax\models
 */
class DeclarationOsnoHelper extends Model
{
    protected $_month;
    protected $_quarter;
    protected $_year;
    protected $_period;
    protected $_periodArray;
    protected $_szvmPeriodArray;    
    protected $_defaultPeriod;

    const TYPE_SINGLE = 1;
    const TYPE_NDS = 2;
    const TYPE_ORG = 3;
    const TYPE_BALANCE = 4;
    const TYPE_SZVM = 5;

    public static $actionByType = [
        self::TYPE_SINGLE => 'declaration',
        self::TYPE_NDS => 'value-added-declaration',
        self::TYPE_ORG => 'profit-declaration',
        self::TYPE_BALANCE => 'balance',
        self::TYPE_SZVM => 'szvm',
    ];

    public static $nameByType = [
        self::TYPE_SINGLE => 'Единая упрощенная декларация',
        self::TYPE_NDS => 'Декларация на добавленную стоимость',
        self::TYPE_ORG => 'Декларация на прибыль организации',
        self::TYPE_BALANCE => 'Бухгалтерский баланс',
        self::TYPE_SZVM => 'Сведения о застрахованных лицах',
    ];

    /**
     * @param $id
     */
    public function setPeriod($id)
    {
        $this->_period = (object)$this->getPeriodArray()[$id];
        list($this->_year, $this->_quarter) = explode('_', $this->_period->id);
    }

    public function setMonth($month)
    {
        $this->_month = $month;
    }

    /**
     * @return object
     */
    public function getPeriod()
    {
        if ($this->_period === null) {
            $this->_period = (object)$this->getDefaultPeriod();
        }

        return $this->_period;
    }

    /**
     * @return array
     */
    public function getPeriodArray()
    {
        if ($this->_periodArray === null) {
            $currentYear = (int)date('Y');
            $lastYear = $currentYear - 1;
            $prevYear = $currentYear - 2;
            $periodArray = [
                "{$prevYear}_4" => [
                    'id' => "{$prevYear}_4",
                    'label' => "{$prevYear}&nbsp;год",
                    'shortLabel' => "{$prevYear}&nbsp;год",
                    'dateFrom' => date_create("{$prevYear}-01-01 00:00:00.000000"),
                    'dateTill' => date_create("{$prevYear}-12-31 23:59:59.999999"),
                ],
                "{$lastYear}_1" => [
                    'id' => "{$lastYear}_1",
                    'label' => "1-й&nbsp;квартал {$lastYear}&nbsp;года",
                    'shortLabel' => "1&nbsp;квартал {$lastYear}&nbsp;года",
                    'dateFrom' => date_create("{$lastYear}-01-01 00:00:00.000000"),
                    'dateTill' => date_create("{$lastYear}-03-31 23:59:59.999999"),
                ],
                "{$lastYear}_2" => [
                    'id' => "{$lastYear}_2",
                    'label' => "1-е&nbsp;полугодие {$lastYear}&nbsp;года",
                    'shortLabel' => "полугодие {$lastYear}&nbsp;года",
                    'dateFrom' => date_create("{$lastYear}-01-01 00:00:00.000000"),
                    'dateTill' => date_create("{$lastYear}-06-30 23:59:59.999999"),
                ],
                "{$lastYear}_3" => [
                    'id' => "{$lastYear}_3",
                    'label' => "9-ть&nbsp;месяцев {$lastYear}&nbsp;года",
                    'shortLabel' => "9&nbsp;месяцев {$lastYear}&nbsp;года",
                    'dateFrom' => date_create("{$lastYear}-01-01 00:00:00.000000"),
                    'dateTill' => date_create("{$lastYear}-09-30 23:59:59.999999"),
                ],
                "{$lastYear}_4" => [
                    'id' => "{$lastYear}_4",
                    'label' => "{$lastYear}&nbsp;год",
                    'shortLabel' => "{$lastYear}&nbsp;год",
                    'dateFrom' => date_create("{$lastYear}-01-01 00:00:00.000000"),
                    'dateTill' => date_create("{$lastYear}-12-31 23:59:59.999999"),
                ],
                "{$currentYear}_1" => [
                    'id' => "{$currentYear}_1",
                    'label' => "1-й&nbsp;квартал {$currentYear}&nbsp;года",
                    'shortLabel' => "1&nbsp;квартал {$currentYear}&nbsp;года",
                    'dateFrom' => date_create("{$currentYear}-01-01 00:00:00.000000"),
                    'dateTill' => date_create("{$currentYear}-03-31 23:59:59.999999"),
                ],
            ];
            $currentMonth = (int)date('n');
            if ($currentMonth > 3) {
                $periodArray["{$currentYear}_2"] = [
                    'id' => "{$currentYear}_2",
                    'label' => "1-е&nbsp;полугодие {$currentYear}&nbsp;года",
                    'shortLabel' => "полугодие {$currentYear}&nbsp;года",
                    'dateFrom' => date_create("{$currentYear}-01-01 00:00:00.000000"),
                    'dateTill' => date_create("{$currentYear}-06-30 23:59:59.999999"),
                ];
                if ($currentMonth > 6) {
                    $periodArray["{$currentYear}_3"] = [
                        'id' => "{$currentYear}_3",
                        'label' => "9-ть&nbsp;месяцев {$currentYear}&nbsp;года",
                        'shortLabel' => "9&nbsp;месяцев {$currentYear}&nbsp;года",
                        'dateFrom' => date_create("{$currentYear}-01-01 00:00:00.000000"),
                        'dateTill' => date_create("{$currentYear}-09-30 23:59:59.999999"),
                    ];
                    if ($currentMonth > 9) {
                        $periodArray["{$currentYear}_4"] = [
                            'id' => "{$currentYear}_4",
                            'label' => "{$currentYear}&nbsp;год",
                            'shortLabel' => "{$currentYear}&nbsp;год",
                            'dateFrom' => date_create("{$currentYear}-01-01 00:00:00.000000"),
                            'dateTill' => date_create("{$currentYear}-12-31 23:59:59.999999"),
                        ];
                    }
                }
            }
            krsort($periodArray);

            $this->_periodArray = $periodArray;
        }

        return $this->_periodArray;
    }

    /**
     * @return array
     */
    public function getSzvmPeriodArray()
    {
        if ($this->_szvmPeriodArray === null) {
            $periodArray = [];
            for ($i=1; $i<=3; $i++) {
                $month = 3 * ($this->_quarter - 1) + $i;
                $lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $this->_year);
                $periodArray["{$month}"] = [
                    'id' => "{$this->_year}_4",
                    'label' => mb_strtolower(ArrayHelper::getValue(Month::$monthFullRU, $month) . "&nbsp;{$this->_year}&nbsp;года", 'utf-8'),
                    'shortLabel' => ArrayHelper::getValue(Month::$monthShort, $month) . "&nbsp;{$this->_year}&nbsp;года",
                    'dateFrom' => date_create("{$this->_year}-{$month}-01 00:00:00.000000"),
                    'dateTill' => date_create("{$this->_year}-{$month}-{$lastDay} 23:59:59.999999"),
                    'month' => $month
                ];
            }
            $this->_szvmPeriodArray = $periodArray;
        }

        return $this->_szvmPeriodArray;
    }

    /**
     * @return stdClass
     */
    public function getDefaultPeriod()
    {
        if ($this->_defaultPeriod === null) {
            $periodArray = $this->getPeriodArray();
            $this->_defaultPeriod = (object)current(array_slice($periodArray, 1, 1));
        }

        return $this->_defaultPeriod;
    }

    /**
     * @return integer
     */
    public function getYear()
    {
        if ($this->_year === null) {
            list($this->_year, $this->_quarter) = explode('_', $this->getPeriod()->id);
        }

        return $this->_year;
    }

    /**
     * @return integer
     */
    public function getQuarter()
    {
        if ($this->_quarter === null) {
            list($this->_year, $this->_quarter) = explode('_', $this->getPeriod()->id);
        }

        return $this->_quarter;
    }

    public function getMonth()
    {
        return $this->_month ?: 1;
    }

    /**
     * @param null $type
     * @return array
     */
    public function getPeriodDropdownItems($type = null)
    {
        $action = 'all';
        if ($type) {
            $action = self::$actionByType[$type];
        }
        $items = [];
        foreach ($this->getPeriodArray() as $period) {
            $items[] = [
                'label' => $period['label'],
                'url' => [
                    "/tax/declaration-osno/{$action}",
                    'period' => $period['id'] == $this->defaultPeriod->id ? null : $period['id'],
                ],
                'linkOptions' => ['class' => $period['id'] == $this->period->id ? 'active' : ''],
            ];
        }

        return $items;
    }

    public function getSzvmPeriodLabel()
    {
        return mb_strtolower(ArrayHelper::getValue(Month::$monthFullRU, $this->_month) . "&nbsp;{$this->_year}&nbsp;года", 'utf-8');
    }

    /**
     * @return array
     */
    public function getSzvmPeriodDropdownItems()
    {
        $action = self::$actionByType[self::TYPE_SZVM];
        $items = [];
        foreach ($this->getSzvmPeriodArray() as $month) {
            $items[] = [
                'label' => $month['label'],
                'url' => [
                    "/tax/declaration-osno/{$action}",
                    'period' => $this->period->id,
                    'month' => $month['month']
                ],
                'linkOptions' => ['class' => $month['month'] == $this->_month ? 'active' : ''],
            ];
        }

        return $items;
    }

    /**
     * @return int
     */
    public function getTaxPeriod()
    {
        switch (substr($this->period->id, 5, 6)) {
            case 1:
                return 3;
            case 2:
                return 6;
            case 3:
                return 9;
            case 4:
                return 0;
            default:
                throw new InvalidParamException('Invalid period param.');
        }
    }

    /**
     * @return int
     */
    public function getQuarterNumber()
    {
        switch (substr($this->period->id, 5, 6)) {
            case 1:
                return '01';
            case 2:
                return '02';
            case 3:
                return '03';
            case 4:
                return '04';
            default:
                throw new InvalidParamException('Invalid period param.');
        }
    }

    /**
     * @return int
     */
    public function getNdsTaxPeriod()
    {
        switch (substr($this->period->id, 5, 6)) {
            case 1:
                return 21;
            case 2:
                return 22;
            case 3:
                return 23;
            case 4:
                return 24;
            default:
                throw new InvalidParamException('Invalid period param.');
        }
    }

    /**
     * @param $text
     * @return null|string
     */
    public function getDottedBoxes($text, $class = null)
    {
        $result = null;
        $data = str_split($text);
        foreach ($data as $symbol) {
            $result .= "<td class=\"dotbox {$class}\">{$symbol}</td>";
        }

        return $result;
    }

    /**
     * @return array
     */
    public function buildItems($selected_no_operations = null)
    {
        $result = [];
        foreach (self::$actionByType as $typeKey => $type) {
            $name = self::$nameByType[$typeKey];

            if ($typeKey === self::TYPE_SINGLE) {
                if ($selected_no_operations == 'now') continue;
                $result[] = [
                    'id' => $typeKey,
                    'type' => 'single',
                    'action' => self::$actionByType[$typeKey],
                    'name' => $name . ' (на бумаге)',
                    'month' => null,
                ];
            } elseif (in_array($typeKey, [self::TYPE_NDS, self::TYPE_ORG])) {
                if ($selected_no_operations == 'from_registration') continue;
                $result[] = [
                    'id' => $typeKey,
                    'type' => ($typeKey == self::TYPE_NDS) ? 'nds' : 'org',
                    'action' => self::$actionByType[$typeKey],
                    'name' => $name . ' (электронно)',
                    'month' => null,
                ];
            } elseif ($typeKey == self::TYPE_SZVM) {
                for ($i = ($this->getPeriod()->dateTill->format('n') - 2); $i <= $this->getPeriod()->dateTill->format('n'); $i++) {
                    $result[] = [
                        'id' => $typeKey,
                        'type' => 'szvm',
                        'action' => self::$actionByType[$typeKey],
                        'name' => $name . (' ' . Month::$monthFullRU[$i] . ' ' . $this->getPeriod()->dateTill->format('Y')),
                        'month' => $i,
                    ];
                }
            } elseif ($typeKey == self::TYPE_BALANCE) {
                if ($selected_no_operations == 'from_registration') continue;
                if ($this->quarter != 4) continue;
                    $result[] = [
                        'id' => $typeKey,
                        'type' => 'balance',
                        'action' => self::$actionByType[$typeKey],
                        'name' => $name,
                        'month' => null,
                    ];
            }
        }

        return $result;
    }
}