<?php

namespace frontend\modules\tax\models;

use common\components\DadataClient;
use common\components\TaxRobotHelper;
use common\components\validators\PhoneValidator;
use common\models\Company;
use common\models\Ifns;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyActivity;
use common\models\company\CompanyTaxationType;
use common\models\company\EmployeeCount;
use common\models\dictionary\bik\BikDictionary;
use frontend\models\log\LogHelper;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class TaxrobotStartForm
 * @package frontend\modules\subscribe\forms
 */
class TaxrobotStartForm extends Model
{
    const SCENARIO_ACTIVITY = 'activity';
    const SCENARIO_TAX_SYSTEM = 'tax-system';
    const SCENARIO_TAX_RATE = 'tax-rate';
    const SCENARIO_STAFF = 'staff';
    const SCENARIO_FAIL = 'fail';
    const SCENARIO_ADDRESS = 'address';
    const SCENARIO_ACCOUNT = 'account';

    public $company_activity_id;
    public $tax_system;
    public $tax_rate;
    public $employee_count_id;
    public $subject;
    public $user_name;
    public $user_phone;
    public $address;
    public $index;
    public $ifns;
    public $oktmo;
    public $inn;
    public $rs;
    public $bik;

    public $bikList = [];
    public $accountId;

    public static $taxSystemItems = [
        1 => 'УСН «Доходы»',
        2 => 'УСН «Доходы минус расходы»',
        3 => 'ОСНО',
        4 => 'ЕНВД',
        5 => 'ЕСХН',
        6 => 'ПСН',
    ];

    public static $taxRateItems = [
        1 => '1%',
        2 => '2%',
        3 => '3%',
        4 => '4%',
        5 => '5%',
        6 => '6%',
    ];

    protected $_company;
    protected $_redirectUrl;
    protected $_taxRobot;
    protected $_dadata;

    public function __construct(Company $company, $config = [])
    {
        $this->_company = $company;
        $this->inn = $company->inn;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_ACTIVITY => [
                'company_activity_id',
            ],
            self::SCENARIO_TAX_SYSTEM => [
                'tax_system',
            ],
            self::SCENARIO_TAX_RATE => [
                'tax_rate',
            ],
            self::SCENARIO_STAFF => [
                'employee_count_id',
            ],
            self::SCENARIO_FAIL => [
                'subject',
                'tax_system',
                'user_name',
                'user_phone',
            ],
            self::SCENARIO_ADDRESS => [
                'address',
                'index',
                'ifns',
                'oktmo',
            ],
            self::SCENARIO_ACCOUNT => [
                'inn',
                'rs',
                'bik',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'company_activity_id',
                    'tax_rate',
                    'employee_count_id',
                    'subject',
                    'user_name',
                    'user_phone',
                    'address',
                    'index',
                    'ifns',
                    'oktmo',
                    'inn',
                    'rs',
                    'bik',
                ], 'required',
            ],
            ['tax_system', 'required', 'on' => self::SCENARIO_TAX_SYSTEM],
            [
                [
                    'subject',
                    'user_name',
                    'user_phone',
                    'address',
                    'index',
                    'ifns',
                    'oktmo',
                ], 'string',
                'max' => 255,
            ],
            [
                ['inn'], 'string',
                'length' => 12,
                'max' => 12,
            ],
            [
                'inn', 'validateByDadata',
            ],
            [
                'address', 'validateAddress',
            ],
            [
                ['rs'], 'string',
                'length' => 20,
                'max' => 20,
            ],
            [
                ['bik'], 'in',
                'range' => $this->bikList,
            ],
            [
                ['bik'], 'exist',
                'targetClass' => BikDictionary::class,
                'message' => 'Значение "БИК" неверно. Возможно ваш банк изменил БИК. Проверьте на сайте банка.'
            ],
            ['user_phone', PhoneValidator::className()],
            ['company_activity_id', 'exist', 'targetClass' => CompanyActivity::class, 'targetAttribute' => 'id'],
            ['employee_count_id', 'exist', 'targetClass' => EmployeeCount::class, 'targetAttribute' => 'id'],
            ['tax_system', 'in', 'range' => array_keys(self::$taxSystemItems)],
            ['tax_rate', 'in', 'range' => array_keys(self::$taxRateItems)],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateByDadata($attribute, $params)
    {
        if (empty($this->getDadata($this->$attribute))) {
            $this->addError($attribute, 'Уточните ваш ИНН');
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateAddress($attribute, $params)
    {
        if (Ifns::findOne($this->ifns) === null) {
            $this->addError($attribute, 'Уточните адрес регистрации');
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function getDadata($inn)
    {
        if (!isset($this->_dadata[$inn])) {
            $this->_dadata[$inn] = DadataClient::getCompanyAttributes($inn);
        }

        return $this->_dadata[$inn];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_activity_id' => 'Вид деятельности',
            'tax_system' => 'Система налогообложения',
            'tax_rate' => 'Налоговая ставка',
            'employee_count_id' => 'Сколько сотрудников',
            'user_name' => 'Ваше имя',
            'user_phone' => 'Ваш телефон',
            'address' => 'Адрес',
            'index' => 'Индекс',
            'ifns' => 'ИФНС',
            'oktmo' => 'ОКТМО',
            'inn' => 'ИНН',
            'rs' => 'Номер расчетного счета',
            'bik' => 'БИК банка',
        ];
    }

    /**
     * @return TaxRobotHelper
     */
    public function getTaxRobot()
    {
        return $this->_taxRobot;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @return array|string
     */
    public function getRedirectUrl()
    {
        return $this->_redirectUrl;
    }

    /**
     * @return string
     */
    public function getTaxName()
    {
        return $this->tax_system ?
            ArrayHelper::getValue(self::$taxSystemItems, $this->tax_system) :
            ArrayHelper::getValue($this->company, ['companyTaxationType', 'name']);
    }

    /**
     * @return string
     */
    public function getStaffName()
    {
        return ArrayHelper::getValue($this->company, ['employeeCount', 'name']);
    }

    /**
     * @return Payment
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        switch ($this->getScenario()) {
            case self::SCENARIO_ACTIVITY:
                return $this->saveActivity();
                break;

            case self::SCENARIO_TAX_SYSTEM:
                return $this->saveTaxSystem();
                break;

            case self::SCENARIO_TAX_RATE:
                return $this->saveTaxRate();
                break;

            case self::SCENARIO_FAIL:
                return $this->saveFail();
                break;

            case self::SCENARIO_STAFF:
                return $this->saveStaff();
                break;

            case self::SCENARIO_ADDRESS:
                return $this->saveAddress();
                break;

            case self::SCENARIO_ACCOUNT:
                return $this->saveAccount();
                break;

            default:
                return false;
                break;
        }
    }

    /**
     * @return boolean
     */
    public function saveActivity()
    {
        $this->company->company_activity_id = $this->company_activity_id;

        return $this->company->save(false, ['company_activity_id']);
    }

    /**
     * @return boolean
     */
    public function saveTaxSystem()
    {
        $model = $this->company->companyTaxationType ?? new CompanyTaxationType([
            'company_id' => $this->company->id
        ]);

        $model->setAttributes([
            'osno' => $this->tax_system == 3 || $this->tax_system == 5 ? 1 : 0,
            'usn' => $this->tax_system == 1 || $this->tax_system == 2 ? 1 : 0,
            'envd' => $this->tax_system == 4 ? 1 : 0,
            'psn' => $this->tax_system == 6 ? 1 : 0,
            'usn_type' => $this->tax_system == 2 ? CompanyTaxationType::INCOME_EXPENSES : CompanyTaxationType::INCOME,
            'usn_percent' => null,
        ]);

        return $model->save(false);
    }

    /**
     * @return boolean
     */
    public function saveTaxRate()
    {
        $model = $this->company->companyTaxationType ?? new CompanyTaxationType([
            'company_id' => $this->company->id
        ]);

        $model->usn_percent = $this->tax_rate;

        return $model->save(false, ['usn_percent']);
    }

    /**
     * @return boolean
     */
    public function saveFail()
    {
        $user_email = Yii::$app->user->identity->email;
        $taxName = $this->getTaxName();
        $staffName = $this->getStaffName();

        $text = "Имя: {$this->user_name}\n";
        $text .= "Телефон: {$this->user_phone}\n";
        $text .= "E-mail: {$user_email}\n";
        $text .= "Система налогообложения: {$taxName}\n";
        $text .= "Количество сотрудников: {$staffName}\n";

        $mail = Yii::$app->mailer->compose()
            ->setFrom(\Yii::$app->params['emailList']['admin'])
            ->setTo(\Yii::$app->params['taxrobot-fail-email'])
            ->setReplyTo($user_email)
            ->setSubject($this->subject)
            ->setTextBody($text);

        return $mail->send();
    }

    /**
     * @return boolean
     */
    public function saveStaff()
    {
        $this->company->employee_count_id = $this->employee_count_id;

        return $this->company->save(false, ['employee_count_id']);
    }

    /**
     * @return boolean
     */
    public function saveAddress()
    {
        $this->company->ifns_ga = $this->ifns;
        $this->company->oktmo = $this->oktmo;
        $this->company->address_legal = $this->company->address_actual = implode(', ', [
            $this->index,
            $this->address,
        ]);

        return $this->company->save(false, [
            'ifns_ga',
            'oktmo',
            'address_legal',
            'address_actual',
        ]);
    }

    /**
     * @return boolean
     */
    public function saveAccount()
    {
        $company = $this->company;
        $model = $company->getCheckingAccountants()->andWhere([
            'rs' => $this->rs,
            'bik' => $this->bik,
        ])->orderBy(['type' => SORT_ASC])->one() ? : new CheckingAccountant([
            'company_id' => $company->id,
            'rs' => $this->rs,
            'bik' => $this->bik,
        ]);

        $data = $this->getDadata($this->inn);
        $company->scenario = Company::SCENARIO_USER_UPDATE;
        $company->chief_is_chief_accountant = true;
        $company->load($data, '');
        $company->populateRelation('mainAccountant', $model);
        $company->save(false);

        if ($model->isNewRecord) {
            if ($model->save()) {
                $this->accountId = $model->id;
                return true;
            }
            \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);

            return false;
        }
        $this->accountId = $model->id;

        if ($model->type == CheckingAccountant::TYPE_CLOSED) {
            $model->type = CheckingAccountant::TYPE_ADDITIONAL;
            return $model->save(true, ['type']);
        }

        return true;
    }
}
