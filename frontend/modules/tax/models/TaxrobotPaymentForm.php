<?php

namespace frontend\modules\tax\models;

use common\components\date\DateHelper;
use common\components\pdf\PdfRenderer;
use common\components\TaxRobotHelper;
use common\models\address\Country;
use common\models\Company;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\Order;
use common\models\product\Product;
use common\models\product\ProductUnit;
use common\models\service\Payment;
use common\models\service\PaymentType;
use common\models\service\TaxrobotPaymentOrder;
use common\models\service\SubscribeHelper;
use common\models\TaxRate;
use frontend\models\Documents;
use frontend\models\log\LogHelper;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\modules\export\models\one_c\OneCExport;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class TaxrobotPaymentForm
 * @package frontend\modules\subscribe\forms
 */
class TaxrobotPaymentForm extends Model
{
    public $year;
    public $quarter;
    public $paymentTypeId;

    protected $_taxRobot;
    protected $_company;
    protected $_payment;
    protected $_invoice;
    protected $_pdfContent;

    public function __construct(TaxRobotHelper $taxRobot, $config = [])
    {
        $this->_taxRobot = $taxRobot;
        $this->_company = $taxRobot->company;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['year', 'quarter', 'paymentTypeId'], 'required'],
            // [['year'], 'in', 'range' => range(date('Y')-1, date('Y')+1)],
            // [['quarter'], 'in', 'range' => [1, 2, 3, 4]],
            [['paymentTypeId'], 'in', 'range' => [
                PaymentType::TYPE_ONLINE,
                PaymentType::TYPE_INVOICE,
            ]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'year' => 'Год',
            'quarter' => 'Квартал',
            'paymentTypeId' => 'Способ оплаты',
        ];
    }

    /**
     * @return Payment
     */
    public function getNewPayment()
    {
        $payment = new Payment([
            'company_id' => $this->company->id,
            'payment_for' => Payment::FOR_TAXROBOT,
            'sum' => TaxRobotHelper::$servicePrice,
            'type_id' => $this->paymentTypeId,
        ]);

        $paymentOrder = new TaxrobotPaymentOrder([
            'company_id' => $this->company->id,
            'year' => $this->taxRobot->year,
            'quarter' => $this->taxRobot->quarter,
            'price' => TaxRobotHelper::$servicePrice,
            'discount' => 0,
            'sum' => TaxRobotHelper::$servicePrice,
        ]);
        $paymentOrder->populateRelation('company', $this->company);
        $paymentOrder->populateRelation('payment', $payment);
        $orderArray = [$paymentOrder];
        $payment->populateRelation('taxrobotOrders', $orderArray);

        return $payment;
    }

    /**
     * @return Payment
     */
    public function savePayment(Payment $payment)
    {
        if (!$payment->save() || !$payment->taxrobotOrders) {
            return false;
        }
        foreach ($payment->taxrobotOrders as $order) {
            $order->payment_id = $payment->id;
            if (!$order->save()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function createPayment()
    {
        $payment = Payment::find()->alias('payment')->joinWith('taxrobotOrders order')->andWhere([
            'payment.company_id' => $this->company->id,
            'payment.payment_for' => Payment::FOR_TAXROBOT,
            'payment.sum' => TaxRobotHelper::$servicePrice,
            'payment.type_id' => $this->paymentTypeId,
            'payment.is_confirmed' => false,
            'order.year' => $this->taxRobot->year,
            'order.quarter' => $this->taxRobot->quarter,
        ])->andWhere([
            '>=', 'created_at', date_create('today')->getTimestamp()
        ])->with('outInvoice')->one();

        if ($payment === null) {
            $payment = $this->getNewPayment();

            if (!$this->savePayment($payment)) {
                \Yii::$app->session->setFlash('error', 'Ошибка при создании платежа. Попробуйте ещё раз');

                return false;
            }

        }

        $this->_payment = $payment;

        $invoice = $payment->outInvoice;

        if ($invoice === null) {
            $invoice = self::createInvoice($this->payment, $this->company, $this->taxRobot->getPeriodLabel());

            if (!$invoice) {
                \Yii::$app->session->setFlash('error', 'Ошибка при создании счёта. Попробуйте ещё раз.');

                return false;
            }

            $payment->populateRelation('outInvoice' , $invoice);
        }

        $this->_invoice = $invoice;

        return true;
    }

    /**
     * @inheritdoc
     */
    public function invoicePayment()
    {
        $this->_pdfContent = $this->invoiceRenderer->output(false);
        $subject = 'Счет на оплату "БУХГАЛТЕРИЯ ИП НА УСН 6%" за ' .
            str_replace("&nbsp;", " ", $this->taxRobot->period->label);

        $mailer = \Yii::$app->mailer
            ->compose([
                'html' => 'system/subscribe/invoice-payment/html',
                'text' => 'system/subscribe/invoice-payment/text',
            ], [
                'tariff' => null,
                'company' => $this->company,
                'invoice' => $this->invoice,
                'subject' => $subject,
            ])
            ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
            ->setTo($this->company->email)
            ->setSubject($subject)
            ->attachContent($this->_pdfContent, [
                'fileName' => 'invoice.pdf',
                'contentType' => 'application/pdf',
            ]);

        if ($this->invoice->contractor_bik && $this->invoice->contractor_rs) {
            $content = Yii::$app->view->render('@frontend/modules/documents/views/invoice/1C_payment_order.php', [
                'model' => $this->invoice,
            ]);
            $name = ($this->invoice->is_invoice_contract && $this->invoice->type == Documents::IO_TYPE_OUT) ? 'счет-договора' : 'счета';
            $mailer->attachContent($content, [
                'fileName' => 'Платежное_поручение_для_' . $name . '_№' .
                    mb_ereg_replace("([^\w\s\d\-_])", '', $this->invoice->fullNumber) . '.txt',
                'contentType' => 'text/plain',
            ]);
        }

        if (!$mailer->send()) {
            \Yii::$app->session->setFlash('error', 'Ошибка при отправке счета. Попробуйте ещё раз.');

            return false;
        }

        return true;
    }

    /**
     * @return boolean
     */
    public function makePayment()
    {
        if (!$this->validate()) {
            return false;
        }

        $logTransaction = LogHelper::$useTransaction;
        LogHelper::$useTransaction = false;

        $created = \Yii::$app->db->transaction(function ($db) {
            if ($this->createPayment()) {
                switch ($this->paymentTypeId) {
                    case PaymentType::TYPE_INVOICE:
                        if ($this->invoicePayment()) {
                            \Yii::$app->session->setFlash(
                                'success',
                                "Счет на оплату отправлен на ваш e-mail: {$this->company->email}."
                            );

                            return true;
                        }
                        break;
                }

                return true;
            }
            $db->transaction->rollBack();

            return false;
        });

        LogHelper::$useTransaction = $logTransaction;

        return $created;
    }

    /**
     * Product for TaxRobot Invoice
     *
     * @param  $save boolean
     * @return Product
     */
    public static function getInvoiceProduct($save = true)
    {
        $product = Yii::$app->kubCompany->getProducts()->andWhere([
            'title' => TaxRobotHelper::$serviceName,
            'is_deleted' => false,
            'not_for_sale' => false,
        ])->one();

        if ($product === null) {
            $product = new Product([
                'creator_id' => null,
                'company_id' => null,
                'production_type' => Product::PRODUCTION_TYPE_SERVICE,
                'title' => TaxRobotHelper::$serviceName,
                'code' => Product::DEFAULT_VALUE,
                'product_unit_id' => ProductUnit::UNIT_COUNT,
                'box_type' => Product::DEFAULT_VALUE,
                'count_in_place' => Product::DEFAULT_VALUE,
                'place_count' => Product::DEFAULT_VALUE,
                'mass_gross' => Product::DEFAULT_VALUE,
                'has_excise' => 0,
                'price_for_buy_nds_id' => TaxRate::RATE_WITHOUT,
                'price_for_buy_with_nds' => TaxRobotHelper::$servicePrice * 100, // stored in kopecks
                'price_for_sell_nds_id' => TaxRate::RATE_WITHOUT,
                'price_for_sell_with_nds' => TaxRobotHelper::$servicePrice * 100, // stored in kopecks
                'country_origin_id' => Country::COUNTRY_WITHOUT,
                'customs_declaration_number' => Product::DEFAULT_VALUE,
                'object_guid' => OneCExport::generateGUID(),
            ]);

            if ($save && !$product->save(false)) {
                throw new \yii\base\Exception("Error Create TaxRobot Product", 1);
            }
        }

        return $product;
    }

    /**
     * @return Order
     */
    public static function orderByProduct(Product $product, Invoice $invoice, $params = [])
    {
        $order = new Order();
        $order->populateRelation('invoice', $invoice);
        $order->populateRelation('product', $product);
        $order->populateRelation('purchaseTaxRate', $product->priceForBuyNds);
        $order->populateRelation('saleTaxRate', $product->priceForSellNds);

        $order->purchase_tax_rate_id = $product->price_for_buy_nds_id;
        $order->sale_tax_rate_id = $product->price_for_sell_nds_id;
        $order->product_id = $product->id;
        $order->product_title = ArrayHelper::getValue($params, 'title', $product->title);
        $order->product_code = $product->code;
        $order->article = $product->article;
        $order->box_type = $product->box_type;
        $order->unit_id = $product->product_unit_id;
        $order->place_count = $product->place_count;
        $order->count_in_place = $product->count_in_place;
        $order->mass_gross = $product->mass_gross;
        $order->excise = $product->has_excise;
        $order->excise_price = $product->excise;
        $order->country_id = $product->country_origin_id;
        $order->custom_declaration_number = $product->customs_declaration_number;

        if (($price = ArrayHelper::getValue($params, 'price')) === null) {
            if ($invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT) {
                $price = $product->price_for_sell_with_nds / (1 + $product->priceForSellNds->rate);
            } else {
                $price = $product->price_for_sell_with_nds;
            }
        }

        $order->price = $price / 100;
        $order->quantity = 1;
        $order->discount = ArrayHelper::getValue($params, 'discount', 0);
        $order->markup = 0;
        $order->number = 1;

        $order->calculateOrder();

        return $order;
    }

    /**
     * Create TaxRobot out Invoice
     * @param Payment $payment
     * @param Company $contractorCompany
     * @param string $periodLabel
     * @return Invoice|false - false if save failed
     */
    public static function createInvoice(Payment $payment, Company $contractorCompany, $periodLabel = null)
    {
        /* @var Company $company KUB-service company model */
        $company = Yii::$app->kubCompany;
        $contractor = SubscribeHelper::getContractorByCompany($contractorCompany);
        $orderCount = count($payment->orders);

        $invoice = new Invoice([
            'type' => Documents::IO_TYPE_OUT,
            'company_id' => $company->id,
            'contractor_id' => $contractor->id,
            'production_type' => Product::PRODUCTION_TYPE_SERVICE,
            'document_number' => (string) Invoice::getNextDocumentNumber($company, Documents::IO_TYPE_OUT, null, date(DateHelper::FORMAT_DATE)),
            'document_date' => date(DateHelper::FORMAT_DATE),
            'document_author_id' => $company->employeeChief->id,
            'payment_limit_date' => date(DateHelper::FORMAT_DATE, strtotime('+' . SubscribeHelper::PAYMENT_PERIOD_LIMIT . ' day')),
            'invoice_expenditure_item_id' => InvoiceExpenditureItem::ITEM_OTHER,
            'object_guid' => OneCExport::generateGUID(),
            'is_subscribe_invoice' => true,
            'service_payment_id' => $payment->id,
            'price_precision' => 2,
            'has_discount' => false,
            'nds_view_type_id' => $company->companyTaxationType->osno ?
                                  ($company->isNdsExclude ? Invoice::NDS_VIEW_OUT : Invoice::NDS_VIEW_IN) :
                                  Invoice::NDS_VIEW_WITHOUT,
        ]);

        $invoice->populateRelation('company', $company);
        $invoice->populateRelation('contractor', $contractor);

        $product = self::getInvoiceProduct();

        $orderArray = [];
        foreach ($payment->taxrobotOrders as $paymentOrder) {
            $orderArray[] = self::orderByProduct($product, $invoice, [
                'title' => TaxRobotHelper::$serviceName . ($periodLabel ? ' за ' . $periodLabel : ''),
                'price' => $paymentOrder->price * 100,
                'discount' => $paymentOrder->discount,
                'quantity' => 1,
            ]);
        }

        $invoice->populateRelation('orders', $orderArray);
        $invoice->total_amount_has_nds = $invoice->total_amount_nds > 0;

        $invoice->total_mass_gross = (string) array_sum(ArrayHelper::getColumn($invoice->orders, 'mass_gross'));
        $invoice->total_place_count = (string) array_sum(ArrayHelper::getColumn($invoice->orders, 'place_count'));

        $save = LogHelper::save($invoice, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function (Invoice $model) {
            if ($model->save()) {
                foreach ($model->orders as $order) {
                    $order->invoice_id = $model->id;
                    if (!$order->save()) {
                        return false;
                    }
                }

                return true;
            }

            return false;
        });

        return $save ? $invoice : null;
    }

    /**
     * @return PdfRenderer
     */
    public function getInvoiceRenderer()
    {
        return Invoice::getRenderer(null, $this->invoice, PdfRenderer::DESTINATION_STRING);
    }

    /**
     * @return TaxRobotHelper
     */
    public function getTaxRobot()
    {
        return $this->_taxRobot;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @return Payment
     */
    public function getPayment()
    {
        return $this->_payment;
    }

    /**
     * @return Payment
     */
    public function getInvoice()
    {
        return $this->_invoice;
    }

    /**
     * @return Payment
     */
    public function getPdfContent()
    {
        return $this->_pdfContent;
    }

}
