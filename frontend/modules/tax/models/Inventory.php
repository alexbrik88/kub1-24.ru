<?php

namespace frontend\modules\tax\models;

use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\components\helpers\Month;
use common\components\pdf\Printable;
use Yii;
use common\models\User;
use common\models\Company;
use yii\base\Model;
use yii\behaviors\TimestampBehavior;
use yii\bootstrap\Html;
use yii\db\Connection;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "inventory".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property int $main_company_id
 * @property int $author_id
 * @property int $status_author_id
 * @property int $status_id
 * @property int $status_updated_at
 * @property int $company_id
 * @property string $document_number
 * @property string $document_date
 * @property int $type
 * @property string $control_organ_name_full
 * @property string $control_organ_address
 * @property string $receipt
 * @property int $totalValue
 * @property string $comment
 * @property string $control_organ
 * @property string $shipped_date
 *
 * @property Company $company
 */
class Inventory extends Model implements Printable
{
    public $company;
    public $document_number;
    public $document_date;
    public $control_organ_name_full;
    public $control_organ_address;
    public $control_organ;
    public $receipt;
    public $totalValue;
    public $shipped_date;
    public $type;
    public $tax_year;
    public $tax_quarter;
    public $tax_month;
    public $szvm;
    public $nds;
    public $org;
    public $balance;

    /**
     * @var string
     */
    protected $printablePrefix = 'Опись';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inventory';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'main_company_id' => 'Main Company ID',
            'author_id' => 'Author ID',
            'status_author_id' => 'Status Author ID',
            'status_id' => 'Status ID',
            'status_updated_at' => 'Status Updated At',
            'company_id' => 'Клиент',
            'document_number' => 'Document Number',
            'document_date' => 'Document Date',
            'type' => 'Контр. орган',
            'control_organ_name_full' => 'Полное наименование органа',
            'control_organ_address' => 'Адрес органа',
            'receipt' => 'Receipt',
            'totalValue' => 'Total Value',
            'comment' => 'Comment',
            'control_organ' => 'Контр. орган',
            'shipped_date' => 'Дата отправки',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getPdfFileName()
    {
        return $this->getPrintTitle() . '.pdf';
    }

    /**
     * @return string
     */
    public function getPrintTitle()
    {
        return $this->printablePrefix;
    }

    public function hasAttribute($attribute) {

        return $this->hasProperty($attribute);
    }

    public function getMonthName() {
        return mb_strtolower(ArrayHelper::getValue(Month::$monthFullRU, $this->tax_month));
    }
}