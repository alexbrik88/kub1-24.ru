<?php

namespace frontend\modules\tax\models;

use common\components\TaxRobotHelper;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\PaymentOrder;
use common\models\document\status\PaymentOrderStatus;
use common\models\TaxKbk;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class TaxrobotNeedPaySearch
 * @package frontend\modules\cash\models
 */
class TaxrobotNeedPaySearch extends PaymentOrder
{
    public $expenditure_id;
    public $status_id;

    protected $_taxrobot;
    protected $_filterQuery;

    /**
     * @param $taxrobot TaxRobotHelper
     */
    public function __construct(TaxRobotHelper $taxrobot, $config = [])
    {
        $this->_taxrobot = $taxrobot;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contractor_name', 'expenditure_id', 'status_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
        ]);
    }

    /**
     * @inheritdoc
     */
    public function formName()
    {
        return 'Search';
    }

    /**
     * @param array $params
     * @param array $dateRange
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $query = $this->_taxrobot->getNeedPaySearchQuery();

        $dataProvider = Yii::createObject([
            'class' => ActiveDataProvider::className(),
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'document_date',
                    'document_number',
                    'sum',
                ],
                'defaultOrder' => [
                    'document_date' => SORT_DESC,
                ],
            ],
            'pagination' => false,
        ]);

        $this->load($params);

        $query->andFilterWhere([
            'contractor_name' => $this->contractor_name,
            'payment_order_status_id' => $this->status_id,
            'kbk' => TaxKbk::find()->where([
                'expenditure_item_id' => $this->expenditure_id,
            ])->select('id')->column(),
        ]);

        $this->_filterQuery = clone $query;

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getContractorFilterItems()
    {
        $query = clone $this->_filterQuery;
        $nameArray = $query->select(['contractor_name'])->distinct()->orderBy(['contractor_name' => SORT_ASC])->column();

        return ['' => 'Все'] + array_combine($nameArray, $nameArray);
    }

    /**
     * @return array
     */
    public function getExpenditureFilterItems()
    {
        $query = clone $this->_filterQuery;
        $modelArray = InvoiceExpenditureItem::find()->joinWith('taxKbk tax_kbk', false)->where([
            'tax_kbk.id' => $query->select('kbk')->distinct()->column(),
        ])->all();

        return ['' => 'Все'] + ArrayHelper::map($modelArray, 'id', 'name');
    }

    /**
     * @return array
     */
    public function getStatusFilterItems()
    {
        $query = clone $this->_filterQuery;
        $modelArray = PaymentOrderStatus::find()->where([
            'id' => $query->select('payment_order_status_id')->distinct()->column(),
        ])->all();

        return ['' => 'Все'] + ArrayHelper::map($modelArray, 'id', 'name');
    }
}
