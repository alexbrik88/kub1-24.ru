<?php

namespace frontend\modules\tax\models;

use common\components\DadataClient;
use common\components\TaxRobotHelper;
use common\models\cash\CashBankFlows;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\dictionary\bik\BikDictionary;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\document\PaymentDetails;
use common\models\document\PaymentType;
use common\models\service\SubscribeTariffGroup;
use frontend\models\log\LogHelper;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use Yii;
use yii\helpers\ArrayHelper;
use common\models\document\TaxpayersStatus;

/**
 * Class ManualEntryForm
 * @package frontend\modules\tax\models
 */
class ManualEntryForm extends \yii\base\Model
{
    public $income_1;
    public $income_2;
    public $income_3;
    public $income_4;
    public $oms_1;
    public $oms_2;
    public $oms_3;
    public $oms_4;
    public $pfr_1;
    public $pfr_2;
    public $pfr_3;
    public $pfr_4;
    public $pfr_over_1;
    public $pfr_over_2;
    public $pfr_over_3;
    public $pfr_over_4;
    public $usn_1;
    public $usn_2;
    public $usn_3;
    public $usn_4;
    public $empty_fields = 1;
    public $bik;
    public $rs;
    public $bank_name;
    public $bank_city;
    public $ks;

    public static $quarters = [1, 2, 3, 4];
    public static $attributes = [
        'income',
        'oms',
        'pfr',
        'pfr_over',
        'usn',
    ];
    public static $entryAttributes = [
        'income_1',
        'income_2',
        'income_3',
        'income_4',
        'oms_1',
        'oms_2',
        'oms_3',
        'oms_4',
        'pfr_1',
        'pfr_2',
        'pfr_3',
        'pfr_4',
        'pfr_over_1',
        'pfr_over_2',
        'pfr_over_3',
        'pfr_over_4',
        'usn_1',
        'usn_2',
        'usn_3',
        'usn_4',
    ];

    protected $_taxRobot;
    protected $_company;
    protected $_account;
    protected $_contractor;
    protected $_flowsArray;
    protected $_lastWorkingDays = [];

    /**
     * @inheritdoc
     */
    public function __construct(TaxRobotHelper $taxRobot, $config = [])
    {
        $this->_taxRobot = $taxRobot;
        $this->_company = $taxRobot->company;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        foreach ($this->getFlowsArray() as $key => $value) {
            $this->$key = $value->amount ? bcdiv($value->amount, 100, 2) : '';
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                self::$entryAttributes, 'trim',
            ],
            [
                self::$entryAttributes, 'filter', 'filter' => function ($value) {
                    return str_replace(',', '.', $value);
                },
            ],
            [
                self::$entryAttributes, 'number',
                'numberPattern' => '/^(-|\+)?[0-9]+(\.[0-9]{0,2})?$/',
                'min' => 0,
                'message' => 'Не верный формат',
                'tooSmall' => 'Не может быть меньше 0',
            ],
            [
                'empty_fields', 'emptyFieldsValidator',
            ],
            [
                ['bik', 'rs'], 'required',
                'message' => 'Необходимо заполнить',
                'when' => function ($model) {
                    return !$this->company->mainCheckingAccountant;
                }
            ],
            [['bik'], 'exist',
                'targetClass' => BikDictionary::class,
                'message' => 'Значение "БИК" неверно. Возможно ваш банк изменил БИК. Проверьте на сайте банка.'
            ],
            [['rs'], 'string', 'length' => 20, 'max' => 20,],
        ];
    }

    /**
     * @inheritdoc
     */
    public function emptyFieldsValidator($attribute, $params)
    {
        $hasValue = false;
        $v = [];
        foreach (self::$attributes as $a) {
            foreach (self::$quarters as $q) {
                $attr= "{$a}_{$q}";
                $hasValue = $hasValue || (isset($this->$attr) && $this->$attr > 0);
                $v[] = $hasValue;
            }
        }
        if (!$hasValue) {
            $this->addError($attribute, 'Необходимо указать доходы и оплаченные налоги');
        }
    }

    /**
     * @return array
     */
    public function getAccountData()
    {
        $bank = $this->bik ? BikDictionary::find()->byBik($this->bik)->byActive()->one() : null;

        return [
            'company_id' => $this->company->id,
            'rs' => $this->rs ? : null,
            'bik' => $bank ? $bank->bik : null,
            'bank_name' => $bank ? $bank->name : null,
            'bank_city' => $bank ? $bank->city : null,
            'ks' => $bank ? $bank->ks : null,
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'income' => 'Налогооблагаемые доходы',
            'oms' => 'Фиксированные взносы ОМС',
            'pfr' => 'Фиксированные взносы ПФР',
            'pfr_over' => 'Взносы в ПФР 1%',
            'usn' => 'Налог УСН Доход',
            'bank_name' => 'Банк',
            'bank_city' => 'Город',
            'bik' => 'БИК',
            'ks' => 'Корреспондентский счет',
            'rs' => 'Расчетный счет',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getYear()
    {
        return $this->_taxRobot->getYear();
    }

    /**
     * @inheritdoc
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @inheritdoc
     */
    public function getAccount()
    {
        if (!isset($this->_account)) {
            $this->_account = $this->company->mainCheckingAccountant ? :
                new CheckingAccountant($this->getAccountData());
        }

        return $this->_account;
    }

    /**
     * @inheritdoc
     */
    public function getFlowsArray()
    {
        if (!isset($this->_flowsArray)) {
            foreach (self::$attributes as $a) {
                foreach (self::$quarters as $q) {
                    $this->_flowsArray["{$a}_{$q}"] = $this->company->getCashBankFlows()->andWhere([
                        'is_taxrobot_manual_entry' => 1,
                        'taxrobot_manual_entry_year' => $this->getYear(),
                        'taxrobot_manual_entry_attribute' => "{$a}_{$q}",
                    ])->one() ? : $this->newFlow($a, $q);
                }
            }
        }

        return $this->_flowsArray;
    }

    /**
     * @inheritdoc
     */
    public function newFlow($a, $q)
    {
        $company = $this->company;
        $date = $this->lastWorkingDay($q);
        $isOut = $a != 'income';

        $model = new CashBankFlows([
            'scenario' => 'create',
            'flow_type' => $isOut ? CashBankFlows::FLOW_TYPE_EXPENSE : CashBankFlows::FLOW_TYPE_INCOME,
            'company_id' => $this->company->id,
            'date' => $date->format('d.m.Y'),
            'recognition_date' => $date->format('Y-m-d'),
            'amount' => '',
            'description' => $this->purposeOfPayment($a, $q),
            'expenditure_item_id' => $this->expenditureItemId($a),
            'income_item_id' => $isOut ? null : InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER,
            'contractor_id' => $this->contractorId($a),
            'payment_order_number' => '1',
            'taxpayers_status_id' => $isOut ? ($company->getIsLikeIP() ? TaxpayersStatus::DEFAULT_IP : TaxpayersStatus::DEFAULT_OOO) : null,
            'kbk' => $isOut ? $this->getKbk($a) : null,
            'oktmo_code' => $company->oktmo ? : ($company->okato ? : 0),
            'payment_details_id' => $isOut ? PaymentDetails::DETAILS_1 : null,
            'tax_period_code' => $isOut ? $this->taxPeriodCode($a, $q) : null,
            'document_number_budget_payment' => $isOut ? '0' : null,
            'document_date_budget_payment' => $isOut ? $date->format('Y-m-d') : null,
            'payment_type_id' => $isOut ? PaymentType::TYPE_0 : null,
            'uin_code' => $isOut ? '0' : null,
            'is_taxrobot_manual_entry' => 1,
            'taxrobot_manual_entry_year' => $this->getYear(),
            'taxrobot_manual_entry_attribute' => "{$a}_{$q}",
        ]);

        return $model;
    }

    /**
     * @inheritdoc
     */
    public function taxPeriodCode($a, $q)
    {
        switch ($a) {
            case 'oms':
            case 'pfr':
            case 'pfr_over':
                return "ГД.00.{$this->getYear()}";
                break;

            case 'usn':
                return "КВ.0{$q}.{$this->getYear()}";
                break;

            default:
                return null;
                break;
        }
    }

    /**
     * @inheritdoc
     */
    public function getKbk($a)
    {
        switch ($a) {
            case 'oms':
                return TaxRobotHelper::$kbkOms;
                break;

            case 'pfr':
                return TaxRobotHelper::$kbkPfr;
                break;

            case 'pfr_over':
                return TaxRobotHelper::$kbkOver300;
                break;

            case 'usn':
                return TaxRobotHelper::$kbkUsn6;
                break;

            default:
                return null;
                break;
        }
    }

    /**
     * @inheritdoc
     */
    public function contractorId($a)
    {
        if ($a == 'income') {
            return '';
        } else {
            if (!isset($this->_contractor)) {
                $ifns = $this->company->ifns;
                $model = $this->company->getContractors()->andWhere([
                    'type' => Contractor::TYPE_SELLER,
                    'is_deleted' => false,
                    'ITN' => $ifns->g6,
                    'PPC' => $ifns->g7,
                ])->one();
                if ($model === null) {
                    $attributes = DadataClient::getContractorAttributes($ifns->g6, $ifns->g7) ? : [
                        'name' => $ifns->contractorName,
                        'ITN' => $ifns->g6,
                        'PPC' => $ifns->g7,
                        'legal_address' => $ifns->g1,
                    ];
                    $model = new Contractor([
                        'company_id' => $this->company->id,
                        'type' => Contractor::TYPE_SELLER,
                        'company_type_id' => CompanyType::TYPE_UFK,
                        'director_name' => '',
                        'chief_accountant_is_director' => false,
                        'contact_is_director' => false,
                    ]);
                    $model->load($attributes, '');
                    $model->BIC = $ifns->g9;
                    $model->current_account = $ifns->g11;
                    $model->save(false);
                }
                $this->_contractor = $model;
            }

            return $this->_contractor->id;
        }
    }

    /**
     * @inheritdoc
     */
    public function lastWorkingDay($q)
    {
        $year = $this->getYear();

        if (!isset($this->_lastWorkingDays[$year][$q])) {
            $month = $q * 3;
            $date = date_create_from_format('Y-n-d|', "{$year}-{$month}-01")->modify('last day of this month');
            $daysOffset = 7 - intval($date->format('N'));
            if ($daysOffset > 0) {
                $date->modify("-{$daysOffset} days");
            }
            $this->_lastWorkingDays[$year][$q] = $date;
        }

        return $this->_lastWorkingDays[$year][$q];
    }

    /**
     * @inheritdoc
     */
    public function expenditureItemId($a)
    {
        switch ($a) {
            case 'oms':
                return InvoiceExpenditureItem::ITEM_OMS_FIXED_PAYMENT;
                break;

            case 'pfr':
                return InvoiceExpenditureItem::ITEM_PFR_FIXED_PAYMENT;
                break;

            case 'pfr_over':
                return InvoiceExpenditureItem::ITEM_INSURANCE_PAYMENT_IP;
                break;

            case 'usn':
                return InvoiceExpenditureItem::ITEM_TAX_USN_6;
                break;

            case 'income':
            default:
                return null;
                break;
        }
    }

    /**
     * @inheritdoc
     */
    public function purposeOfPayment($a, $q)
    {
        switch ($a) {
            case 'income':
                return 'Оплата от покупателя';
                break;

            case 'oms':
                return Yii::$app->getI18n()->format(TaxRobotHelper::$purposeOms, [
                    'year' => $this->getYear(),
                ], Yii::$app->language);
                break;

            case 'pfr':
                return Yii::$app->getI18n()->format(TaxRobotHelper::$purposePfr, [
                    'year' => $this->getYear(),
                ], Yii::$app->language);
                break;

            case 'pfr_over':
                return Yii::$app->getI18n()->format(TaxRobotHelper::$purposeOver300, [
                    'amount' => number_format(TaxRobotHelper::$onePercentOver/100, 0, '.', ' '),
                    'year' => $this->getYear(),
                ], Yii::$app->language);
                break;

            case 'usn':
                $year = $this->getYear();
                switch ($q) {
                    case 1:
                        $period = "1-й квартал {$year} года";
                        break;
                    case 2:
                        $period = "1-е полугодие {$year} года";
                        break;
                    case 3:
                        $period = "9-ть месяцев {$year} года";
                        break;

                    case 3:
                        $period = "{$year} год";
                        break;
                }
                return Yii::$app->getI18n()->format(TaxRobotHelper::$purposeUsn6, [
                    'period' => $period ?? '',
                ], Yii::$app->language);
                break;
        }
    }

    /**
     * @inheritdoc
     */
    public function save($runValidation = true)
    {
        if ($runValidation && !$this->validate()) {
            return false;
        }

        $flowsArray = $this->getFlowsArray();

        return Yii::$app->db->transaction(function ($db) use ($flowsArray) {
            if ($this->account->isNewRecord && !$this->account->save()) {
                $db->transaction->rollBack();
                \common\components\helpers\ModelHelper::logErrors($this->account, __METHOD__);

                return false;
            }
            foreach (self::$entryAttributes as $attr) {
                if ($this->$attr > 0) {
                    $flowsArray[$attr]->amount = $this->$attr;
                    if ($flowsArray[$attr]->isNewRecord) {
                        $flowsArray[$attr]->bank_name = $this->account->bank_name;
                        $flowsArray[$attr]->rs = $this->account->rs;
                    }
                    if (!$flowsArray[$attr]->save()) {
                        $db->transaction->rollBack();
                        \common\components\helpers\ModelHelper::logErrors($flowsArray[$attr], __METHOD__);

                        return false;
                    }
                } elseif (!$flowsArray[$attr]->isNewRecord) {
                    $flowsArray[$attr]->delete();
                }
            }

            return true;
        });
    }
}
