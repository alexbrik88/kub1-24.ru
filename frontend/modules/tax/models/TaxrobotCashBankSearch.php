<?php

namespace frontend\modules\tax\models;


use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use frontend\components\StatisticPeriod;
use frontend\modules\cash\components\FilterHelper;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class TaxrobotCashBankSearch
 * @package frontend\modules\cash\models
 */
class TaxrobotCashBankSearch extends CashBankFlows
{
    public $reason_id;
    private $_filterQuery;
    public $contractor_query;
    public $bik;

    // Исключаемые статьи прихода
    protected $_excludeIncomeItems = [
        2, // Взнос наличными
        3, // Возврат
        4, // Займ
        5, // Кредит
        6, // От учредителя
        9, // Перевод собственных средств
        10, // Возврат займа
        13, // Возврат средств из бюджета
        14, // Обеспечительный платёж
    ];
    // Включаемые статьи расхода (ПФР, ОМС)
    protected $_expenditureItems = [
        28, // Страховые взносы за ИП
        46, // Фиксированный платеж в ОМС
        53, // Фиксированный платеж в ПФР
    ];

    public static $TYPE_PFR = 28;
    public static $TYPE_OMS = 46;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['expenditure_item_id', 'flow_type'], 'integer'],
            [['reason_id', 'contractor_query'], 'string'],
            [['contractor_id', 'billPaying', 'bik'], 'safe'],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'reason_id' => 'Тип',
        ]);
    }

    /**
     * @param array $params
     * @param array $dateRange
     * @return ActiveDataProvider
     */
    public function search(array $params = [], $dateRange = null)
    {
        $query = $this->find()
            ->alias('cbf')
            ->joinWith('contractor')
            ->joinWith('invoices')
            ->andWhere(['cbf.company_id' => Yii::$app->user->identity->company->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'date' => [
                        'asc' => [
                            'cbf.date' => SORT_ASC,
                            'cbf.created_at' => SORT_ASC,
                        ],
                        'desc' => [
                            'cbf.date' => SORT_DESC,
                            'cbf.created_at' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'amountIncome' => [
                        'asc' => [
                            'cbf.flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'cbf.amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'cbf.flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'cbf.amount' => SORT_DESC,
                        ],
                    ],
                    'amountExpense' => [
                        'asc' => [
                            'cbf.flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'cbf.amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'cbf.flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'cbf.amount' => SORT_DESC,
                        ],
                    ],
                    'contractor_id',
                    'description',
                    'created_at',
                    'billPaying' => [
                        'asc' => ['invoice.document_number' => SORT_ASC, 'invoice.document_additional_number' => SORT_ASC],
                        'desc' => ['invoice.document_number' => SORT_DESC, 'invoice.document_additional_number' => SORT_ASC],
                        'default' => SORT_ASC
                    ],
                ],
                'defaultOrder' => [
                    'date' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (empty($this->id)) {
            if ($this->bik !== 'all') {
                $query
                    ->joinWith('checkingAccountants account')
                    ->andWhere(['account.bik' => $this->bik]);
            }
            $dateRange = empty($dateRange) ? StatisticPeriod::getDefaultPeriod() : $dateRange;
            $query->andWhere(['between', 'cbf.date', $dateRange['from'], $dateRange['to']]);
        }

        if ($this->flow_type == -1) {
            $this->flow_type = null;
        }

        if ($this->contractor_query) {
            $query->andFilterWhere(['like', 'contractor.name', $this->contractor_query]);
        }

        if (!$this->reason_id) {
            $query->andWhere(['or',
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_EXPENSE],
                    ['expenditure_item_id' => $this->_expenditureItems],
                ],
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_INCOME],
                    ['not', ['cbf.income_item_id' => $this->_excludeIncomeItems]],
                ],
            ]);
        } elseif (!empty($this->reason_id)) {
            if (substr($this->reason_id, 0, 2) === 'e_') {
                $this->expenditure_item_id = substr($this->reason_id, 2);
            } elseif (substr($this->reason_id, 0, 2) === 'i_') {
                $this->income_item_id = substr($this->reason_id, 2);
            }
            $query->andFilterWhere([
                'cbf.expenditure_item_id' => $this->expenditure_item_id,
                'cbf.income_item_id' => $this->income_item_id,
            ]);
        }

        $query->andFilterWhere([
            'cbf.id' => $this->id,
            'cbf.flow_type' => $this->flow_type,
            'cbf.contractor_id' => $this->contractor_id,
        ]);

        $this->_filterQuery = clone $query;

        return $dataProvider;
    }

    /**
     * @return array
     */
    public static function getReasonDropDown()
    {
        $result = [];

        // The next code searches for distinct and existing operation's type
        $expen_items = Yii::$app->db->createCommand("SELECT DISTINCT iei.id, iei.name FROM cash_bank_flows cbf LEFT JOIN invoice_expenditure_item iei ON cbf.expenditure_item_id = iei.id  WHERE cbf.expenditure_item_id IS not NULL AND cbf.company_id = '" . Yii::$app->user->identity->company->id . "'")->queryAll();

        foreach ($expen_items as $item) {
            $result_items['e_' . $item['id']] = $item['name'];
        }

        $income_items = Yii::$app->db->createCommand("SELECT DISTINCT iii.name, iii.id FROM cash_bank_flows cbf LEFT JOIN invoice_income_item iii ON cbf.income_item_id = iii.id WHERE cbf.income_item_id IS not NULL AND cbf.company_id = '" . Yii::$app->user->identity->company->id . "'")->queryAll();

        foreach ($income_items as $item) {
            $result_items['i_' . $item['id']] = $item['name'];
        }

        if (!empty($result_items)) {
            return $result_items;
        }
        // Old code

        /** @var InvoiceExpenditureItem[] $items */
        $items = InvoiceExpenditureItem::find()->orderBy(['sort' => 'asc'])->all();

        if (!empty($items)) {
            foreach ($items as $item) {
                $result['e_' . $item->id] = $item->name;
            }
        }

        /** @var InvoiceIncomeItem[] $items */
        $items = InvoiceIncomeItem::find()->orderBy(['sort' => 'asc'])->all();

        if (!empty($items)) {
            foreach ($items as $item) {
                $result['i_' . $item->id] = $item->name;
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getContractorFilterItems()
    {
        $query = clone $this->_filterQuery;

        return FilterHelper::getContractorFilterItems($query);
    }

    /**
     * @return array
     */
    public function getReasonFilterItems()
    {
        $query = clone $this->_filterQuery;
        $e_reasonArray = $query
            ->distinct()
            ->select('CONCAT("e_", `expenditure`.`id`) `reason_id`, `expenditure`.`name` `reason_name`')
            ->leftJoin(InvoiceExpenditureItem::tableName() . ' `expenditure`', '`expenditure`.`id` = `cbf`.`expenditure_item_id`')
            ->andWhere(['not', ['expenditure.id' => null]])
            ->createCommand()
            ->queryAll();

        $query = clone $this->_filterQuery;
        $i_reasonArray = $query
            ->distinct()
            ->select('CONCAT("i_", `income`.`id`) `reason_id`, `income`.`name` `reason_name`')
            ->leftJoin(InvoiceIncomeItem::tableName() . ' `income`', '`income`.`id` = `cbf`.`income_item_id`')
            ->andWhere(['not', ['income.id' => null]])
            ->createCommand()
            ->queryAll();

        $reasonArray = ArrayHelper::map(array_merge($e_reasonArray, $i_reasonArray), 'reason_id', 'reason_name');
        natsort($reasonArray);

        return $reasonArray;
    }

    /** USN TOTALS */

    /**
     * @param $dateRange
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getExpenditureTotals($dateRange) {

        $query = CashBankFlows::find()->select([
            'expenditure_item_id',
            'SUM([[amount]]) AS [[total_amount]]',
            'SUM(IF([[is_taxable]] = true, [[amount]], 0)) AS [[taxable_amount]]',
        ])->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
            ->andWhere(['expenditure_item_id' => $this->_expenditureItems])
            ->andWhere(['between', 'date', $dateRange['from'], $dateRange['to']])
            ->andWhere(['company_id' => Yii::$app->user->identity->company->id])
            ->groupBy(['expenditure_item_id']);


        return $query->asArray(true)->all();
    }

    /**
     * @param $dateRange
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getIncomeTotals($dateRange) {

        $query = CashBankFlows::find()->select([
            'income_item_id',
            'SUM([[amount]]) AS [[total_amount]]',
            'SUM(IF([[is_taxable]] = true, [[amount]], 0)) AS [[taxable_amount]]',
        ])->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
            ->andWhere(['not', ['income_item_id' => $this->_excludeIncomeItems]])
            ->andWhere(['between', 'date', $dateRange['from'], $dateRange['to']])
            ->andWhere(['company_id' => Yii::$app->user->identity->company->id])
            ->groupBy(['income_item_id']);

        return $query->asArray(true)->all();
    }

    public function getSumTaxToPay($dateRange) {

        $company = Yii::$app->user->identity->company;

        // Приход
        $incomeTotals = $this->getIncomeTotals($dateRange);
        $sumIncome = 0;
        foreach ($incomeTotals as $value) {
            $sumIncome += $value['taxable_amount'];
        }

        // Расход
        $expenditureTotals = $this->getExpenditureTotals($dateRange);
        $sumExpenditure = 0;
        foreach ($expenditureTotals as $value) {
            $sumExpenditure += $value['taxable_amount'];
        }

        $sumTaxTotal = $sumIncome * $company->companyTaxationType->usn_percent / 100; // начислено
        $sumTaxToPay = round($sumTaxTotal - $sumExpenditure); // к уплате

        return ($sumTaxToPay > 0) ? $sumTaxToPay : 0;
    }

}