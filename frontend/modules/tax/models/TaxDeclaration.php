<?php

namespace frontend\modules\tax\models;

use common\components\date\DateHelper;
use common\components\helpers\ModelHelper;
use common\models\cash\CashOrderFlows;
use Yii;
use common\models\Company;
use common\models\employee\Employee;
use common\models\document\status\TaxDeclarationStatus;
use frontend\models\log\Log;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\models\log\LogEntityType;
use yii\helpers\Html;
use common\models\document\AbstractDocument;
use frontend\models\log\ILogMessage;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;

/**
 * This is the model class for table "tax_declaration".
 *
 * @property integer $id
 * @property string $uid
 * @property integer $company_id
 * @property integer $created_at
 * @property integer $document_author_id
 * @property integer $status_id
 * @property integer $status_updated_at
 * @property integer $status_author_id
 * @property string $document_date
 * @property integer $document_correction_number
 * @property string $document_knd
 * @property string $tax_period
 * @property integer $tax_year
 * @property integer $tax_month
 * @property integer $tax_quarter
 * @property string $tax_service_ifns
 * @property string $tax_service_location
 * @property string $declaration_maker_code
 * @property string $declaration_maker_fio
 * @property string $declaration_maker_organization
 * @property integer $add_signature
 * @property integer $taxpayer_sign
 * @property integer $downloaded_quarter
 * @property boolean $single
 * @property boolean $nds
 * @property boolean $org
 * @property boolean $balance
 * @property boolean $szvm
 *
 * @property Company $company
 * @property Employee $documentAuthor
 * @property Employee $statusAuthor
 * @property TaxDeclarationStatus $status
 * @property TaxDeclarationQuarter[] $taxDeclarationQuarters
 */
class TaxDeclaration extends AbstractDocument implements ILogMessage
{
    // Исключаемые статьи прихода
    protected $_excludeIncomeItems = [
        2, // Взнос наличными
        3, // Возврат
        4, // Займ
        5, // Кредит
        6, // От учредителя
        9, // Перевод собственных средств
        10, // Возврат займа
        13, // Возврат средств из бюджета
        14, // Обеспечительный платёж
    ];
    // Включаемые статьи расхода (ПФР, ОМС)
    protected $_expenditureItems = [
        28, // Страховые взносы за ИП
        46, // Фиксированный платеж в ОМС
        53, // Фиксированный платеж в ПФР
    ];


    /**
     * The maximum uploaded file count
     */
    const MAX_FILES = 3;

    public static $uploadDirectory = 'tax-declaration';
    public $files_upload;
    public $files_delete;

    /**
     * @var string
     */
    public $urlPart = 'declaration';

    /**
     * @inheritdoc
     */
    public $printablePrefix = 'Налоговая_декларация';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax_declaration';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['document_date', 'tax_year', 'document_correction_number', 'document_knd', 'tax_service_ifns', 'tax_service_location'], 'required'],
            [['company_id', 'created_at', 'document_author_id', 'status_id', 'status_updated_at', 'status_author_id',
                'document_correction_number', 'tax_year', 'add_signature', 'taxpayer_sign', 'nds', 'org', 'balance', 'single',
                'szvm', 'tax_month', 'tax_year'], 'integer'],
            [['document_date'], 'safe'],
            [['uid'], 'string', 'max' => 5],
            [['document_knd'], 'string', 'max' => 7],
            [['tax_period'], 'string', 'max' => 2],
            [['tax_service_ifns'], 'string', 'max' => 4],
            [['tax_service_location'], 'string', 'max' => 3],
            [['declaration_maker_code'], 'string', 'max' => 1],
            [['declaration_maker_fio'], 'string', 'max' => 60],
            [['declaration_maker_organization'], 'string', 'max' => 160],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['document_author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['document_author_id' => 'id']],
            [['status_author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['status_author_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaxDeclarationStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['tax_year'], 'unique', 'filter' => ['company_id' => $this->company_id]],
            [['downloaded_quarter'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => 'Uid',
            'company_id' => 'Company ID',
            'created_at' => 'Created At',
            'document_author_id' => 'Document Author ID',
            'status_id' => 'Status ID',
            'status_updated_at' => 'Status Updated At',
            'status_author_id' => 'Status Author ID',
            'document_date' => 'Дата подготовки (для налоговой)',
            'document_correction_number' => 'Номер корректировки',
            'document_knd' => 'Форма КНД',
            'tax_period' => 'Налоговый период',
            'tax_year' => 'Отчетный год',
            'tax_service_ifns' => 'Предоставляется в налоговый орган (код)',
            'tax_service_location' => 'По месту нахождения (код)',
            'declaration_maker_code' => 'Декларацию подает',
            'declaration_maker_fio' => 'Ф.И.О.',
            'declaration_maker_organization' => 'Организация-представитель',
            'add_signature' => 'Добавить подпись',
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            ModelHelper::HtmlEntitiesModelAttributes($this);

            if ($insert) {
                $this->created_at = time();
                $this->status_id = TaxDeclarationStatus::STATUS_CREATED;
                $this->status_updated_at = time();
                $this->status_author_id = Yii::$app->user->id;
                $this->document_author_id = Yii::$app->user->id;
            }

            return true;
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'document_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'status_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(TaxDeclarationStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentStatus()
    {
        return $this->getStatus();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxDeclarationQuarters()
    {
        return $this->hasMany(TaxDeclarationQuarter::className(), ['tax_declaration_id' => 'id']);
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        $text = 'Налоговая декларация за ' . $log->getModelAttributeNew('tax_year');

        $link = Html::a($text, [
            '/tax/declaration/view-robot',
            'id' => $log->getModelAttributeNew('id'),
        ]);

        $innerText = null;

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . $innerText . ' была создана.';
            case LogEvent::LOG_EVENT_DELETE:
                return $text . $innerText . ' была удалёна.';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . $innerText . ' была изменена.';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                $status_id = $log->getModelAttributeNew('status_id');
                $status_name = TaxDeclarationStatus::findOne($status_id)->name;
                return $link . $innerText . ' статус "' . $status_name . '"';
            default:
                return $log->message;
        }
    }

    /**
     * @inheritdoc
     */
    public function getPdfFileName()
    {
        return $this->getPrintTitle() . '.pdf';
    }

    /**
     * @return string
     */
    public function getPrintTitle()
    {
        return $this->printablePrefix . '_за_' . $this->getFullNumber();
    }

    public function getFullNumber()
    {
        return $this->tax_year;
    }

    public function getFile()
    {
    }

    public function getMaxTaxYear()
    {

        return $this->find()
            ->where(['company_id' => Yii::$app->user->identity->company->id])
            ->max('tax_year') ?: 2016;

    }

    /**
     * @param TaxDeclarationQuarter[] $quarters
     * @param int $taxpayer_sign
     * @return bool
     */
    public function validateQuarters($quarters, $taxpayer_sign)
    {

        $taxpayer_koef = ($taxpayer_sign == 1) ? 2 : 1;

        /** @var TaxDeclarationQuarter $quarterModel */
        foreach ($quarters as $key => $quarterModel) {

            //if ($quarterModel->prepayment_tax_amount - $quarterModel->outcome_amount * $taxpayer_koef < 0) {
            //    $quarterModel->addError('outcome_amount', 'Сумма уплаченных страховых взносов не может превышать ' . ($taxpayer_koef == 1 ? 'сумму':'половину суммы') . ' авансового налога ('.$quarterModel->quarter_number.' период)');
            //}

            if ($key > 0) {
                if ($quarterModel->income_amount < $quarters[$key - 1]->income_amount)
                    $quarterModel->addError('income_amount', 'Введите "Доход за период" нарастающим итогом (' . $quarterModel->quarter_number . ' период)');
                if ($quarterModel->prepayment_tax_amount < $quarters[$key - 1]->prepayment_tax_amount)
                    $quarterModel->addError('prepayment_tax_amount', 'Введите "Исчисленный авансовый платеж" нарастающим итогом (' . $quarterModel->quarter_number . ' период)');
                if ($quarterModel->outcome_amount < $quarters[$key - 1]->outcome_amount)
                    $quarterModel->addError('outcome_amount', 'Введите "Уплаченные страховые взносы" нарастающим итогом (' . $quarterModel->quarter_number . ' период)');
            }

            if (!$quarterModel->validate(null, false)) {

                return false;
            }
        }

        return true;
    }

    /**
     * @param $quarters
     * @return mixed
     */
    public function fillQuartersFromPost($quarters)
    {
        $oktmo = Yii::$app->request->post('oktmo');
        $tax_rate = Yii::$app->request->post('tax_rate');
        $income_amount = Yii::$app->request->post('income_amount');
        $outcome_amount = Yii::$app->request->post('outcome_amount');
        $prepayment_tax_amount = Yii::$app->request->post('prepayment_tax_amount');

        foreach ($quarters as &$quarter) {

            $num = $quarter->quarter_number;
            if (is_array($oktmo) && isset($oktmo[$num])) {
                $quarter->oktmo = $oktmo[$num];
            }
            if (is_array($tax_rate) && isset($tax_rate[$num])) {
                $quarter->tax_rate = $tax_rate[$num];
            }
            if (is_array($income_amount) && isset($income_amount[$num])) {
                $quarter->income_amount = $income_amount[$num];
            }
            if (is_array($outcome_amount) && isset($outcome_amount[$num])) {
                $quarter->outcome_amount = $outcome_amount[$num];
            }
            if (is_array($prepayment_tax_amount) && isset($prepayment_tax_amount[$num])) {
                $quarter->prepayment_tax_amount = $prepayment_tax_amount[$num];
            }

            $quarter->tax_amount = 0;

            $quarter->created_at = time();
        }

        return $quarters;
    }

    public function calculateQuartersTax($quarters, $taxpayer_sign)
    {

        $previous_tax_amount = 0;

        foreach ($quarters as &$quarter) {

            $quarter->tax_amount = round($quarter->prepayment_tax_amount - $quarter->outcome_amount) - $previous_tax_amount;

            if ($taxpayer_sign == 2 && $quarter->tax_amount < 0)
                $quarter->tax_amount = 0;
            if ($taxpayer_sign == 1 && $quarter->tax_amount < $quarter->prepayment_tax_amount / 2)
                $quarter->tax_amount = round($quarter->prepayment_tax_amount / 2);

            $previous_tax_amount = $quarter->tax_amount;
        }

        return $quarters;
    }


    /**
     * @param $quarters
     * @return bool
     */
    public function saveQuarters($quarters = null)
    {
        if (!$quarters)
            $quarters = $this->taxDeclarationQuarters;

        foreach ($quarters as $quarterModel) {
            $quarterModel->tax_declaration_id = $this->primaryKey;

            $quarterModel->save(false);
        }

        return true;
    }

    public function getQuarterIncomeBankFlowsSum($year, $quarter)
    {

        $date = new \DateTime();
        $monthFrom = ($quarter - 1) * 3 + 1;
        $dateFrom = $date->setDate($year, $monthFrom, 1);
        $dateTill = clone $dateFrom;
        $dateTill = $dateTill->modify('last day of + 2 month');

        $query = CashBankFlows::find()
            ->alias('cbf')
            ->distinct()
            ->joinWith('contractor')
            ->joinWith('invoices')
            ->andWhere(['cbf.company_id' => $this->company_id, 'flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
            ->andWhere(['not', ['income_item_id' => $this->_excludeIncomeItems]])
            ->andWhere(['is_taxable' => true])
            ->andWhere(['between', 'date', $dateFrom->format('Y-m-d'), $dateTill->format('Y-m-d')]);

        return $query->sum('amount');
    }

    public function getQuarterOutcomeBankFlowsSum($year, $quarter)
    {

        $date = new \DateTime();
        $monthFrom = ($quarter - 1) * 3 + 1;
        $dateFrom = $date->setDate($year, $monthFrom, 1);
        $dateTill = clone $dateFrom;
        $dateTill = $dateTill->modify('last day of + 2 month');

        $query = CashBankFlows::find()
            ->alias('cbf')
            ->distinct()
            ->joinWith('contractor')
            ->joinWith('invoices')
            ->andWhere([
                'cbf.company_id' => $this->company_id,
                'flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE,
                'expenditure_item_id' => $this->_expenditureItems,
                'is_taxable' => true
            ])
            ->andWhere(['between', 'date', $dateFrom->format('Y-m-d'), $dateTill->format('Y-m-d')]);

        return $query->sum('amount');
    }

    public function getQuarterIncomeOrderFlowsSum($year, $quarter)
    {

        $date = new \DateTime();
        $monthFrom = ($quarter - 1) * 3 + 1;
        $dateFrom = $date->setDate($year, $monthFrom, 1);
        $dateTill = clone $dateFrom;
        $dateTill = $dateTill->modify('last day of + 2 month');

        $query = CashOrderFlows::find()
            ->alias('cbf')
            ->andWhere([
                'cbf.company_id' => $this->company_id,
                'flow_type' => CashFlowsBase::FLOW_TYPE_INCOME,
                'is_taxable' => true,
                'is_accounting' => true
            ])
            ->andWhere(['between', 'date', $dateFrom->format('Y-m-d'), $dateTill->format('Y-m-d')]);

        return $query->sum('cbf.amount');
    }

    public function getQuarterOutcomeOrderFlowsSum($year, $quarter)
    {

        $date = new \DateTime();
        $monthFrom = ($quarter - 1) * 3 + 1;
        $dateFrom = $date->setDate($year, $monthFrom, 1);
        $dateTill = clone $dateFrom;
        $dateTill = $dateTill->modify('last day of + 2 month');

        $query = CashOrderFlows::find()
            ->alias('cbf')
            ->andWhere([
                'cbf.company_id' => $this->company_id,
                'flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE,
                'is_taxable' => true,
                'is_accounting' => true
            ])
            ->andWhere(['between', 'date', $dateFrom->format('Y-m-d'), $dateTill->format('Y-m-d')]);

        return $query->sum('cbf.amount');
    }

    public function updateDownloadedQuarterNum()
    {

        if ($this->tax_year < date('Y')) {
            $for_quarter = 4;
        } else {
            $for_quarter = intval((date('n') + 2) / 3) - 1;
        }

        if ($for_quarter > $this->downloaded_quarter) {
            $this->downloaded_quarter = $for_quarter;
            $this->updateAttributes(['downloaded_quarter']);
        }
    }

    public function getXmlToImport()
    {
        /** @var TaxDeclarationQuarter[] $quarters */
        $quarters = $this->taxDeclarationQuarters;
        $company = $this->company;

        $ifns = $company->ifns_ga;
        $uuid = $this->uuid();
        $date = DateHelper::format($this->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $revertDate = DateHelper::format($this->document_date, 'Ymd', DateHelper::FORMAT_DATE);
        $filename = "NO_USN_{$ifns}_{$ifns}_{$company->inn}_{$revertDate}_{$uuid}";

        $dom = new \DOMDocument('1.0', 'windows-1251');
        $dom->formatOutput = true;
        $root = $dom->createElement('Файл');
        $dom->appendChild($root);
        $root->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        $root->setAttribute('ИдФайл', $filename);
        $root->setAttribute('ВерсПрог', '1С:БУХГАЛТЕРИЯ 3.0.67.54');
        $root->setAttribute('ВерсФорм', '5.05');

        $document = $dom->createElement('Документ');
        $document->setAttribute('КНД', '1152017');
        $document->setAttribute('ДатаДок', $date);
        $document->setAttribute('Период', '34');
        $document->setAttribute('ОтчетГод', $this->tax_year);
        $document->setAttribute('КодНО', $ifns);
        $document->setAttribute('НомКорр', $this->document_correction_number);
        $document->setAttribute('ПоМесту', '120');
        $root->appendChild($document);

        // Сведения о налогоплательщике
        $svnp = $dom->createElement('СвНП');
        $svnp->setAttribute('Тлф', $company->phone);
        $document->appendChild($svnp);

        $npfl = $dom->createElement('НПФЛ');
        $npfl->setAttribute('ИННФЛ', $company->inn);
        $svnp->appendChild($npfl);

        $fio = $dom->createElement('ФИО');
        $fio->setAttribute('Фамилия', $company->ip_lastname);
        $fio->setAttribute('Имя', $company->ip_firstname);
        $fio->setAttribute('Отчество', $company->ip_patronymic);
        $npfl->appendChild($fio);

        $podpisant = $dom->createElement('Подписант');
        $podpisant->setAttribute('ПрПодп', '1');
        $document->appendChild($podpisant);

        $usn = $dom->createElement('УСН');
        $document->appendChild($usn);

        // Сумма налога (авансового платежа по налогу), уплачиваемого в связи с применением упрощенной
        // системы налогообложения (объект налогообложения - доходы), подлежащая уплате (уменьшению), по данным налогоплательщика
        $sumnalpu = $dom->createElement('СумНалПУ_НП');
        $sumnalpu->setAttribute('ОКТМО', $company->oktmo);
        //$sumnalpu->setAttribute('АвПУКв', $quarters[0]->prepayment_tax_amount); // аванс квартал
        //$sumnalpu->setAttribute('АвПУУменПг', $quarters[1]->prepayment_tax_amount); // аванс полугодие
        //$sumnalpu->setAttribute('АвПУУмен9м', $quarters[2]->prepayment_tax_amount); // аванс 9м
        $sumnalpu->setAttribute('НалПУУменПер', $quarters[3]->tax_amount); // к уплате
        $usn->appendChild($sumnalpu);

        // Расчет налога, уплачиваемого в связи с применением упрощенной системы налогообложения
        $raschnal1 = $dom->createElement('РасчНал1');
        $raschnal1->setAttribute('ПризНП', $this->taxpayer_sign);
        $raschnal1->setAttribute('ПризСтав', $company->companyTaxationType->getUsnDeclarationTaxRateIndicator());
        $sumnalpu->appendChild($raschnal1);

        // Сумма полученных доходов (налоговая база для исчисления налога (авансового платежа по налогу)) нарастающим итогом
        $dohod = $dom->createElement('Доход');
        $dohod->setAttribute('СумЗаКв', $quarters[0]->income_amount);
        $dohod->setAttribute('СумЗаПг', $quarters[1]->income_amount);
        $dohod->setAttribute('СумЗа9м', $quarters[2]->income_amount);
        $dohod->setAttribute('СумЗаНалПер', $quarters[3]->income_amount);
        $raschnal1->appendChild($dohod);

        // Ставка налога
        $stavka = $dom->createElement('Ставка');
        $stavka->setAttribute('СтавкаКв', number_format($quarters[0]->tax_rate, 1));
        $stavka->setAttribute('СтавкаПг', number_format($quarters[1]->tax_rate, 1));
        $stavka->setAttribute('Ставка9м', number_format($quarters[2]->tax_rate, 1));
        $stavka->setAttribute('СтавкаНалПер', number_format($quarters[3]->tax_rate, 1));
        $raschnal1->appendChild($stavka);

        // Сумма исчисленного налога (авансового платежа по налогу)
        $isch = $dom->createElement('Исчисл');
        $isch->setAttribute('СумЗаКв', $quarters[0]->prepayment_tax_amount);
        $isch->setAttribute('СумЗаПг', $quarters[1]->prepayment_tax_amount);
        $isch->setAttribute('СумЗа9м', $quarters[2]->prepayment_tax_amount);
        $isch->setAttribute('СумЗаНалПер', $quarters[3]->prepayment_tax_amount);
        $raschnal1->appendChild($isch);

        // Сумма страховых взносов, выплаченных работникам пособий по временной нетрудоспособности и платежей (взносов)
        // по договорам добровольного личного страхования (нарастающим итогом), предусмотренных пунктом 3.1 статьи 346.21
        // Налогового кодекса Российской Федерации
        $umennal = $dom->createElement('УменНал');
        $umennal->setAttribute('СумЗаКв', $quarters[0]->outcome_amount);
        $umennal->setAttribute('СумЗаПг', $quarters[1]->outcome_amount);
        $umennal->setAttribute('СумЗа9м', $quarters[2]->outcome_amount);
        $umennal->setAttribute('СумЗаНалПер', $quarters[3]->outcome_amount);
        $raschnal1->appendChild($umennal);

        $output = $dom->saveXML() . "\n";

        return ['filename' => $filename, 'output' => $output];
    }

    private function uuid()
    {
        $data = random_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    public function getIsOsno() {
        return ($this->single || $this->nds || $this->balance || $this->org || $this->szvm);
    }

    // bar codes
    public function getUsnBarcode($page): string
    {
        $barcode2020 = [
            1 => '@web/img/barcode-03012017.png',
            2 => '@web/img/barcode-03012024.png',
            3 => '@web/img/barcode-03012048.png'
        ];
        $barcode2021 = [
            1 => '@web/img/barcode_1_0301_3014.png',
            2 => '@web/img/barcode_2_0301_3021.png',
            3 => '@web/img/barcode_4_0301_3045.png',
        ];

        return ($this->tax_year < 2021)
            ? ($barcode2020[$page] ?? '')
            : ($barcode2021[$page] ?? '');
    }
}
