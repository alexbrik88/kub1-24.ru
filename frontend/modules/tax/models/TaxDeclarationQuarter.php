<?php

namespace frontend\modules\tax\models;

use Yii;

/**
 * This is the model class for table "tax_declaration_quarter".
 *
 * @property integer $id
 * @property integer $tax_declaration_id
 * @property integer $quarter_number
 * @property string $oktmo
 * @property string $income_amount
 * @property string $outcome_amount
 * @property string $tax_rate
 * @property string $tax_amount
 * @property string $prepayment_tax_amount
 * @property integer $created_at
 *
 * @property TaxDeclaration $taxDeclaration
 */
class TaxDeclarationQuarter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax_declaration_quarter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['oktmo', 'tax_rate', 'income_amount', 'outcome_amount', 'prepayment_tax_amount'], 'required'],
            [['oktmo', 'tax_rate', 'income_amount', 'outcome_amount', 'prepayment_tax_amount'], function ($attribute) {

                if (!strlen(trim($this->{$attribute}))) {
                    $this->addError($attribute, 'Необходимо заполнить "' . $this->getAttributeLabel($attribute) . '" в ' . $this->quarter_number . ' периоде');
                }
                if (!is_numeric($this->{$attribute})) {
                    $this->addError($attribute, 'Значение "' . $this->getAttributeLabel($attribute) . '" некорректно в ' . $this->quarter_number . ' периоде');
                }
                if ($attribute == 'tax_rate' && $this->{$attribute} > 100) {
                    $this->addError($attribute, 'Значение "' . $this->getAttributeLabel($attribute) . '" некорректно в ' . $this->quarter_number . ' периоде');
                }
            }],
            [['tax_declaration_id', 'quarter_number', 'created_at'], 'integer'],
            [['income_amount', 'outcome_amount', 'prepayment_tax_amount'], 'integer'],
            [['oktmo'], 'string', 'max' => 11],
            [['tax_declaration_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaxDeclaration::className(), 'targetAttribute' => ['tax_declaration_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tax_declaration_id' => 'Tax Declaration ID',
            'quarter_number' => 'Квартал',
            'oktmo' => 'Код по ОКТМО',
            'income_amount' => 'Доход за период',
            'outcome_amount' => 'Уплаченные страховые взносы',
            'prepayment_tax_amount' => 'Исчисленный авансовый платеж',
            'tax_amount' => 'Рассчитанный авансовый платеж',
            'tax_rate' => 'Ставка налога',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxDeclaration()
    {
        return $this->hasOne(TaxDeclaration::className(), ['id' => 'tax_declaration_id']);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->created_at = time();
            }

            return true;
        }

        return false;
    }
}
