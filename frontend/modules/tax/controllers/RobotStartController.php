<?php

namespace frontend\modules\tax\controllers;

use common\components\DadataClient;
use common\components\filters\AccessControl;
use common\components\TaxRobotHelper;
use common\models\bank\Bank;
use common\models\cash\CashBankStatementUpload;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusBuilder;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusDrawer;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusUploader;
use frontend\modules\tax\models\TaxrobotStartForm;
use frontend\models\log\LogHelper;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\rbac\UserRole;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

/**
 * RobotStartController controller for the `tax` module
 */
class RobotStartController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
            'ajax' => [
                'class' => 'yii\filters\AjaxFilter',
                'except' => [
                    'banking-redirect',
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->renderAjax('index');
    }

    /**
     * @return mixed
     */
    public function actionSelect()
    {
        return $this->renderAjax('select');
    }

    /**
     * @return mixed
     */
    public function actionActivity()
    {
        $model = new TaxrobotStartForm(Yii::$app->user->identity->company, [
            'scenario' => TaxrobotStartForm::SCENARIO_ACTIVITY,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->renderAjax('tax-system', ['model' => $model]);
        }

        return $this->renderAjax('activity', ['model' => $model]);
    }

    /**
     * @return mixed
     */
    public function actionTaxSystem()
    {
        $model = new TaxrobotStartForm(Yii::$app->user->identity->company, [
            'scenario' => TaxrobotStartForm::SCENARIO_TAX_SYSTEM,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->tax_system == 1) {
                return $this->renderAjax('tax-rate', ['model' => $model]);
            } else {
                return $this->renderAjax('tax-fail', ['model' => $model]);
            }
        }

        return $this->renderAjax('tax-system', ['model' => $model]);
    }

    /**
     * @return mixed
     */
    public function actionTaxRate()
    {
        $model = new TaxrobotStartForm(Yii::$app->user->identity->company, [
            'scenario' => TaxrobotStartForm::SCENARIO_TAX_RATE,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->renderAjax('staff', ['model' => $model]);
        }

        return $this->renderAjax('tax-rate', ['model' => $model]);
    }

    /**
     * @return mixed
     */
    public function actionTaxFail()
    {
        $model = new TaxrobotStartForm(Yii::$app->user->identity->company, [
            'scenario' => TaxrobotStartForm::SCENARIO_FAIL,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->renderAjax('request', ['model' => $model]);
        }

        return $this->renderAjax('tax-fail', ['model' => $model]);
    }

    /**
     * @return mixed
     */
    public function actionStaff()
    {
        $model = new TaxrobotStartForm(Yii::$app->user->identity->company, [
            'scenario' => TaxrobotStartForm::SCENARIO_STAFF,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->employee_count_id == 1) {
                return $this->renderAjax('upload-select', ['model' => $model]);
            } else {
                return $this->renderAjax('staff-fail', ['model' => $model]);
            }
        }

        return $this->renderAjax('staff', ['model' => $model]);
    }

    /**
     * @return mixed
     */
    public function actionAddress()
    {
        $model = new TaxrobotStartForm(Yii::$app->user->identity->company, [
            'scenario' => TaxrobotStartForm::SCENARIO_ADDRESS,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->renderAjax('wait');
        }

        return $this->renderAjax('address', ['model' => $model]);
    }

    /**
     * @return mixed
     */
    public function actionStaffFail()
    {
        $model = new TaxrobotStartForm(Yii::$app->user->identity->company, [
            'scenario' => TaxrobotStartForm::SCENARIO_FAIL,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->renderAjax('request', ['model' => $model]);
        }

        return $this->renderAjax('staff-fail', ['model' => $model]);
    }

    /**
     * @return mixed
     */
    public function actionBankingSelect()
    {
        return $this->renderAjax('banking-select', ['bankingData' => $this->bankingData()]);
    }

    /**
     * @return mixed
     */
    public function actionAccount($alias)
    {
        $bankingData = $this->bankingData();

        if (!isset($bankingData[$alias])) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $model = new TaxrobotStartForm(Yii::$app->user->identity->company, [
            'scenario' => TaxrobotStartForm::SCENARIO_ACCOUNT,
            'bikList' => $bankingData[$alias]['bikList'],
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $route = "/cash/banking/{$alias}/default/index";

            return $this->renderAjax('banking', [
                'route' => $route,
                'alias' => $alias,
                'account_id' => $model->accountId,
            ]);
        }

        return $this->renderAjax('account', ['model' => $model, 'bankingData' => $bankingData[$alias]]);
    }

    /**
     * @return mixed
     */
    public function actionBanking($alias, $account_id)
    {
        $route = "/cash/banking/{$alias}/default/index";

        return $this->renderAjax('banking', [
            'alias' => $alias,
            'account_id' => $account_id,
            'route' => $route,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionBankingRedirect($alias, $account_id)
    {
        Yii::$app->session->set('robot-start-banking', [
            'alias' => $alias,
            'account_id' => $account_id,
        ]);

        return $this->redirect(['/tax/robot/company']);
    }

    /**
     * @return mixed
     */
    public function actionUploadSelect()
    {
        return $this->renderAjax('upload-select');
    }

    /**
     * @return mixed
     */
    public function actionUploadFile()
    {
        if (Yii::$app->request->isPost) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            $error = '';
            $error_code = 500;
            $company = Yii::$app->user->identity->company;
            $file = UploadedFile::getInstanceByName('uploadfile');
            if ($file !== null) {
                $vidimusBuilder = new VidimusBuilder($file, $company);
                $vidimusBuilder->findCompanyInn = true; // empty($company->inn);
                $vidimusBuilder->findCompanyIfns = true; // empty($company->ifns);

                try {
                    $vidimuses = $vidimusBuilder->build();
                    $vidimusDrawer = new VidimusDrawer($company, $vidimuses, $vidimusBuilder->uploadData());
                    $vidimusDrawer->cancelButton = false;
                    $content = $vidimusDrawer->draw($vidimusBuilder->bankInfo, ['upload']);

                    return $this->renderAjax('vidimus', ['content' => $content]);
                } catch (\Exception $e) {
                    $error = $e->getMessage();
                    $error_code = $e->getCode();
                }
            }

            return $this->renderAjax('vidimus', ['content' => $error]);
        }

        return $this->renderAjax('upload-file');
    }

    /**
     * @return mixed
     */
    public function actionUpload()
    {
        set_time_limit(90);
        $company = Yii::$app->user->identity->company;
        $uploadData = Yii::$app->request->post('uploadData');
        $companyData = $uploadData['companyData'] ?? [];;

        $mainAccount = $company->getCheckingAccountants()->andWhere([
            'rs' => $rs,
        ])->orderBy([
            'type' => SORT_ASC,
        ])->one();

        $accounts = [];
        if (isset($uploadData['accounts'])) {
            foreach ($uploadData['accounts'] as $rs => $data) {
                $account = $company->getCheckingAccountants()->andWhere([
                    'rs' => $rs,
                ])->orderBy([
                    'type' => SORT_ASC,
                ])->one();
                if ($account === null) {
                    $account = new CheckingAccountant([
                        'company_id' => $company->id,
                        'rs' => $rs,
                        'bik' => $data['bik'],
                    ]);

                    if (!$account->save()) {
                        \common\components\helpers\ModelHelper::logErrors($account, __METHOD__);
                        return $this->renderAjax('upload-error', [
                            'message' => "Не удалось сохранить данные р/с '{$rs}' вашей компании из загружаемой выписки.",
                        ]);
                    }
                }
                $accounts[] = $account;
            }
        }

        $inn = ArrayHelper::getValue($companyData, 'inn');
        if (!empty($inn)) {
            $data = DadataClient::getCompanyAttributes($inn);
            if (empty($data)) {
                return $this->renderAjax('upload-error', [
                    'message' => 'Не найдены данные по вашей компании, для которой загружается выписка.',
                ]);
            }
            $company->scenario = Company::SCENARIO_USER_UPDATE;
            $company->chief_is_chief_accountant = true;
            $company->load($data, '');
            if ($mainAccount === null && !empty($accounts)) {
                $mainAccount = reset($accounts);
            }
            $company->populateRelation('mainAccountant', $mainAccount);

            if (!$company->save(false)) {
                \common\components\helpers\ModelHelper::logErrors($company, __METHOD__);

                return $this->renderAjax('upload-error', [
                    'message' => 'Не удалось сохранить данные компании.',
                ]);
            }
        }

        $ifns = ArrayHelper::getValue($companyData, 'ifns_ga');
        $oktmo = ArrayHelper::getValue($companyData, 'oktmo');
        if (!empty($ifns)) {
            $company->ifns_ga = $ifns;
            $company->oktmo = $oktmo;
            $company->save(true, ['ifns_ga', 'oktmo']);
        }

        $vidimus = Yii::$app->request->post('vidimus', []);
        $source = Yii::$app->request->post('source');
        $service = Yii::$app->request->post('service');
        if ($vidimus && in_array($source, [
            CashBankStatementUpload::SOURCE_FILE,
            CashBankStatementUpload::SOURCE_BANK,
        ])) {
            $uploader = new VidimusUploader($company, $accounts);
            $uploader->upload($vidimus, $source, $uploadData);

            return $this->renderAjax('upload-success');
        } else {
            return $this->renderAjax('upload-error', [
                'message' => 'Не удалось загрузить выписку.',
            ]);
        }
    }

    /**
     * @return mixed
     */
    public function actionUploadSuccess()
    {
        return $this->renderAjax('upload-success');
    }

    /**
     * @return mixed
     */
    public function actionUploadBank()
    {
        return $this->renderAjax('upload-bank');
    }

    /**
     * @return mixed
     */
    public function actionWait()
    {
        $company = Yii::$app->user->identity->company;

        if (empty($company->ifns)) {
            $model = new TaxrobotStartForm($company, [
                'scenario' => TaxrobotStartForm::SCENARIO_ADDRESS,
            ]);

            return $this->renderAjax('address', ['model' => $model]);
        }

        // Подтверждение данных по ИП
        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => 'tax_robot_accepted',
            'value' => '1',
            'expire' => time() + 3600 * 24 * 365
        ]));

        return $this->renderAjax('wait');
    }

    /**
     * @return mixed
     */
    protected function bankingData()
    {
        $bankingData = [];
        foreach (Banking::$modelClassArray as $class) {
            if ($bank = Bank::findOne(['bik' => $class::BIK, 'is_blocked' => false])) {
                $bankingData[$class::ALIAS] = [
                    'bank' => $bank,
                    'bankingClass' => $class,
                    'alias' => $class::ALIAS,
                    'name' => $class::NAME,
                    'bikList' => $class::$bikList,
                    'logo' => $bank->getUploadDirectory() . $bank->logo_link,
                ];
            }
        }

        return $bankingData;
    }
}
