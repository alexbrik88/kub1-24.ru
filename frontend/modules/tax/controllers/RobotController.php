<?php

namespace frontend\modules\tax\controllers;

use common\components\DadataClient;
use common\components\filters\AccessControl;
use common\components\TaxRobotHelper;
use common\models\bank\BankingParams;
use common\models\Company;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use common\models\company\MenuItem;
use common\models\Contractor;
use common\models\document\PaymentOrder;
use common\models\document\status\PaymentOrderStatus;
use common\models\document\status\TaxDeclarationStatus;
use common\models\document\TaxpayersStatus;
use common\models\employee\Employee;
use common\models\service\Payment;
use common\models\service\PaymentType;
use common\models\service\TaxrobotPaymentOrder;
use frontend\components\FrontendController;
use frontend\components\StatisticPeriod;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\models\log\LogHelper;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\modules\tax\models\Inventory;
use frontend\modules\tax\models\ManualEntryForm;
use frontend\modules\tax\models\TaxrobotCashBankSearch;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\components\DocumentBaseController;
use frontend\modules\subscribe\forms\OnlinePaymentForm;
use frontend\modules\subscribe\models\OnlinePaymentLog;
use frontend\modules\subscribe\models\OnlinePaymentSaver;
use frontend\modules\tax\models\TaxDeclaration;
use frontend\modules\tax\models\TaxDeclarationQuarter;
use frontend\modules\tax\models\TaxrobotPaymentForm;
use frontend\modules\tax\models\TaxrobotNeedPaySearch;
use frontend\rbac\UserRole;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\modules\tax\models\Kudir;
use common\models\cash\form\CashBankFlowsForm;
use common\components\date\DateHelper;

/**
 * Default controller for the `tax` module
 */
class RobotController extends DocumentBaseController
{
    public $layout = 'robot';

    /** @var  TaxRobotHelper */
    public $taxRobot;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'error',
                        ],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'taxable' => ['POST'],
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'add-modal-contractor' => [
                'class' => 'frontend\components\AddModalContractorAction',
                'headerContent' => function ($action, $model) {
                    return $this->renderAjax(($model->type == Contractor::TYPE_CUSTOMER) ?
                        '@frontend/views/contractor/form_modal/create-contractor_header' :
                        '@frontend/views/contractor/form_modal/create-contractor_header_fns_only' , [
                        'model' => $model,
                    ]);
                },
                'bodyContent' => function ($action, $model) {
                    return $this->renderAjax('@frontend/views/contractor/form_modal/create-contractor_body', [
                        'model' => $model,
                        'documentType' => $model->type,
                        'onlyFNS' => ($model->type == Contractor::TYPE_SELLER)
                    ]);
                },
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex($type = null)
    {
        $this->taxRobot->actionIsAllowed($this->action->id);

        return $this->redirect($this->taxRobot->redirectAction);
    }

    /**
     * @return mixed
     */
    public function actionCompany() {

        $post = Yii::$app->request->post();

        \frontend\modules\cash\modules\banking\widgets\BankingModalWidget::$rendered = true;

        if (Yii::$app->request->post('not_use_account')) {

            if (isset($post['CheckingAccountant']) && !trim($post['CheckingAccountant']['bik']) && !trim($post['CheckingAccountant']['rs']))
                return $this->companyNoAccountAction();
        }

        return $this->companyAction();
    }

    /**
     * @return mixed
     */
    public function companyAction()
    {
        $model = $this->taxRobot->company;
        $model->scenario = Company::SCENARIO_TAX_ROBOT;
        $isFirstVisit = \common\models\CompanyPageFirstTime::isFirst($model, $this->action);

        if (($account = $model->mainCheckingAccountant) === null) {
            $account = new CheckingAccountant(['company_id' => $model->id, 'type' => CheckingAccountant::TYPE_MAIN]);
        }

        $additionalAccounts = CheckingAccountant::find()
            ->byCompany($model->id)
            ->byType(CheckingAccountant::TYPE_ADDITIONAL)
            ->all();

        // To delete accounts
        $deleteRs = Yii::$app->request->post('delete_rs');
        if (!empty($deleteRs)) {
            foreach ((array)$deleteRs as $deleteId) {
                $deleteModel = CheckingAccountant::findOne($deleteId);
                if ($deleteModel) {
                    $deleteModel->delete();
                }
            }
        }

        // To add new accounts
        $newAccounts = [];
        for ($i=1; $i<=10; $i++) {
            $newAccounts[] = new CheckingAccountant(['id' => 'new_'.$i, 'isNewRecord'=>false, 'company_id' => $model->id, 'type' => CheckingAccountant::TYPE_ADDITIONAL]);
        }

        if (Yii::$app->request->post('ajax') && $model->load(Yii::$app->request->post()) && $account->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $aaValidation = [];
            foreach ($additionalAccounts as $aa) {
                if ($aa->load(Yii::$app->request->post()))
                    $aaValidation = array_merge($aaValidation, ActiveForm::validate($aa));
            }

            $naValidation = [];
            foreach ($newAccounts as $na) {
                if ($na->load(Yii::$app->request->post()) && trim($na->bik))
                    $naValidation = array_merge($naValidation, ActiveForm::validate($na));
            }

            $this->updateValidAttributes($model);

            return array_merge(ActiveForm::validate($model), ActiveForm::validate($account), $aaValidation, $naValidation);
        }

        if ($model->load(Yii::$app->request->post()) && $account->load(Yii::$app->request->post())) {
            if (!$model->validate(['ifns_ga'])) {
                $model->ifns_ga = null;
            }

            if ($account->save() && $model->save()) {

                foreach ($additionalAccounts as $aa) {
                    if ($aa->load(Yii::$app->request->post()))
                        $aa->save();
                }

                foreach ($newAccounts as $na) {
                    if ($na->load(Yii::$app->request->post()) && trim($na->bik)) {
                        $na->id = null;
                        $na->isNewRecord = true;
                        $na->save();
                    }
                }

                $this->saveFnsContractor($model->ifns_ga);

                return $this->redirect("params");
            }
        }

        if (Yii::$app->session->getFlash('error')) {
            $model->validate();
            $account->validate();
        }

        return $this->render('company', [
            'model' => $model,
            'account' => $account,
            'additionalAccounts' => $additionalAccounts,
            'newAccounts' => $newAccounts,
            'isFirstVisit' => $isFirstVisit,
        ]);
    }

    /**
     * @return mixed
     */
    public function companyNoAccountAction()
    {
        $model = $this->taxRobot->company;
        $model->scenario = Company::SCENARIO_TAX_ROBOT;

        if (($account = $model->mainCheckingAccountant) === null) {
            $account = new CheckingAccountant(['company_id' => $model->id, 'type' => CheckingAccountant::TYPE_MAIN]);
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $this->updateValidAttributes($model);

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            if (!$model->validate(['ifns_ga'])) {
                $model->ifns_ga = null;
            }

            if ($model->save()) {

                $this->saveFnsContractor($model->ifns_ga);

                return $this->redirect("params");
            }
        }

        if (Yii::$app->session->getFlash('error')) {
            $model->validate();
            $account->validate();
        }

        return $this->render('company', [
            'model' => $model,
            'account' => $account,
            'additionalAccounts' => [],
            'newAccounts' => []
        ]);
    }


    /**
     * @return mixed
     */
    public function actionParams()
    {
        $taxRobot = $this->taxRobot;

        $model = $taxRobot->company;
        $model->scenario = Company::SCENARIO_TAX_PSN;
        $taxation = $model->companyTaxationType;
        $taxation->populateRelation('company', $model);

        if (Yii::$app->request->post('ajax')) {
            $model->load(Yii::$app->request->post());
            $taxation->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model, $taxation);
        }
        if ($model->load(Yii::$app->request->post()) && $taxation->load(Yii::$app->request->post())) {
            if ($taxation->usn) {
                $usn_percent_index = $taxation->usn_type;
                if ($usn_percent_index == 1 && Yii::$app->request->post('tmp_usn_percent_1') !== null) {
                    $taxation->usn_percent = Yii::$app->request->post('tmp_usn_percent_1') ? : 15;
                } elseif (Yii::$app->request->post('tmp_usn_percent_0') !== null) {
                    $taxation->usn_percent = Yii::$app->request->post('tmp_usn_percent_0') ? : 6;
                }
            }

            if (!$taxation->psn) {
                $model->ip_patent_city = null;
                $model->patentDate = null;
                $model->patentDateEnd = null;
            }

            if ($model->save() && $taxation->save()) {
                // Подтверждение данных по ИП
                Yii::$app->response->cookies->add(new \yii\web\Cookie([
                    'name' => 'tax_robot_accepted',
                    'value' => '1',
                    'expire' => time() + 3600 * 24 * 365
                ]));

                // todo: перенести список ОФД в БД, добавить признак в компанию
                if ($ofds = Yii::$app->request->post('Ofd')) {
                    if (!empty($ofds['taxcom']))
                        // Есть аккаунт TaxCOM
                        Yii::$app->response->cookies->add(new \yii\web\Cookie([
                            'name' => 'taxcom_user',
                            'value' => '1',
                            'expire' => time() + 3600 * 24 * 365
                        ]));
                } else if (Yii::$app->request->cookies['taxcom_user']) {
                    Yii::$app->response->cookies->remove('taxcom_user');
                }

                return $this->redirect("bank");
            }
        }

        $accounts = CheckingAccountant::find()
            ->select(['bank_name, COUNT(*) AS cnt'])
            ->byCompany($model->id)
            ->byType([CheckingAccountant::TYPE_MAIN, CheckingAccountant::TYPE_ADDITIONAL])
            ->groupBy(['bank_name'])
            ->orderBy('type')
            ->asArray()->all();

        \common\models\company\CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 85, true);

        return $this->render('params', [
            'model' => $model,
            'taxation' => $taxation,
            'accounts' => $accounts,
        ]);
    }

    /**
     * @param string $bik
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionBank()
    {
        $taxRobot = $this->taxRobot;

        $company = $taxRobot->company;

        \common\models\DiscountType::checkForType4($company);

        $params = ArrayHelper::merge($_POST, Yii::$app->request->queryParams);
        $dataProvider = $taxRobot->getIncomeDataProvider(ArrayHelper::getValue($params, 'TaxRobot'));

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        \common\models\company\CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 86, true);

        $preloadBankingsAccounts = $this->getPreloadBankingAccounts($company);

        return $this->render('bank', [
            'taxRobot' => $taxRobot,
            'dataProvider' => $dataProvider,
            'company' => $company,
            'paymentModel' => new TaxrobotPaymentForm($taxRobot),
            'manualEntryModel' => new ManualEntryForm($taxRobot),
            'preloadBankingAccounts' => $preloadBankingsAccounts
        ]);
    }

    /**
     * @param string $bik
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionManualEntry()
    {
        $taxRobot = $this->taxRobot;

        $model = new ManualEntryForm($taxRobot);

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash(
                'success',
                "Суммы доходов и уплаченных налогов за {$model->year} год сохранены"
            );
        } else {
            Yii::$app->session->setFlash(
                'success',
                "Не удалось сохранить суммы доходов и уплаченных налогов за {$model->year} год"
            );
        }

        return $this->redirect(Yii::$app->request->referrer ? : ['bank']);
    }

    public function afterAction($action, $result)
    {
        if ($action->id != 'error') {
            $taxRobot = $this->taxRobot;

            $company = $taxRobot->company;

            if ($company && $company->show_help_taxrobot_banking) {
                $company->updateAttributes(['show_help_taxrobot_banking' => false]);
            }
        }

        return parent::afterAction($action, $result); // TODO: Change the autogenerated stub
    }

    /**
     * @param $company
     * @return array
     */
    public function getPreloadBankingAccounts($company)
    {
        $firstRun = !Yii::$app->session->get('taxrobot_preload_banking_accounts', []);
        $preloadBankingsAccounts = [];

        if ($firstRun && $company->show_popup_taxrobot_banking) {
            if ($accountants = $company->checkingAccountants) {
                foreach ($accountants as $accountant) {
                    foreach (Banking::$modelClassArray as $banking) {
                        if (in_array($accountant->bik, $banking::$bikList)) {
                            $preloadBankingsAccounts[] = ['id' => $accountant->id, 'bank_alias' => $banking::ALIAS];
                        }
                    }
                }
            }

            Yii::$app->session->set('taxrobot_preload_banking_accounts', $preloadBankingsAccounts);
        }

        else if ($preloadBankingsAccounts = Yii::$app->session->get('taxrobot_preload_banking_accounts', [])) {
            foreach ($preloadBankingsAccounts as $key => $accountantArr) {
                if (BankingParams::find()
                    ->andWhere(['bank_alias' => $accountantArr['bank_alias']])
                    ->andWhere(['param_name' => 'access_token'])
                    ->andWhere(['company_id' => $company->id])
                    ->exists()) {

                    unset($preloadBankingsAccounts[$key]);
                }
            }

            $preloadBankingsAccounts = array_values($preloadBankingsAccounts);
            Yii::$app->session->set('taxrobot_preload_banking_accounts', $preloadBankingsAccounts);
        }

        return (array)$preloadBankingsAccounts;
    }

    public function actionClearPreloadBankingAccounts()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $company = $this->taxRobot->company;
        $accountId = Yii::$app->request->post('account_id');
        $preloadBankingsAccounts = Yii::$app->session->get('taxrobot_preload_banking_accounts', []);

        foreach ($preloadBankingsAccounts as $key => $accountantArr) {
            if ($accountId == $accountantArr['id']) {
                unset($preloadBankingsAccounts[$key]);
                break;
            }
        }

        $preloadBankingsAccounts = array_values($preloadBankingsAccounts);
        Yii::$app->session->set('taxrobot_preload_banking_accounts', $preloadBankingsAccounts);

        if (!empty($preloadBankingsAccounts)) {

            return [
                'next' => true,
                'account_id' => current($preloadBankingsAccounts)['id'],
                'bank_alias' => current($preloadBankingsAccounts)['bank_alias'],
                'arr' => $preloadBankingsAccounts,
            ];
        }

        $company->updateAttributes(['show_popup_taxrobot_banking' => false]);

        return [
            'next' => false,
            'account_id' => null,
            'bank_alias' => null,
            //'arr' => $preloadBankingsAccounts,
            'sess' => Yii::$app->session->get('taxrobot_preload_banking_accounts')
        ];
    }

    /**
     * @param string $bik
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionPay()
    {
        $taxRobot = $this->taxRobot;

        $company = $taxRobot->company;

        $model = new TaxrobotPaymentForm($taxRobot);

        $result = '';

        if ($model->load(Yii::$app->request->post()) && $model->makePayment()) {
            if ($model->paymentTypeId == PaymentType::TYPE_ONLINE) {
                $result = $this->renderPartial('_pay_online', [
                    'form' => new OnlinePaymentForm([
                        'scenario' => OnlinePaymentForm::SCENARIO_SEND,
                        'company' => $company,
                        'payment' => $model->payment,
                        'user' => $taxRobot->employee,
                    ]),
                ]);
            } else {
                $result = $this->renderPartial('_pay_invoice', [
                    'invoice' => $model->invoice,
                ]);
            }
        }

        return $this->renderAjax('_pay_form', [
            'taxRobot' => $taxRobot,
            'model' => $model,
            'result' => $result,
        ]);
    }

    /**
     * @param $id
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionCreateMovement($type, $flow_type)
    {
        if (!in_array($type, [TaxRobotHelper::TYPE_BANK, TaxRobotHelper::TYPE_ORDER]) ||
            !in_array($flow_type, [CashFlowsBase::FLOW_TYPE_EXPENSE, CashFlowsBase::FLOW_TYPE_INCOME])
        ) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        switch ($type) {
            case TaxRobotHelper::TYPE_BANK:
                $checkingAccountant = $company->mainCheckingAccountant;
                $model = new CashBankFlowsForm([
                    'scenario' => 'create',
                    'company_id' => $company->id,
                    'flow_type' => $flow_type,
                    'rs' => $checkingAccountant ? $checkingAccountant->rs : null,
                    'bank_name' => $checkingAccountant ? $checkingAccountant->bank_name : null,
                    'date' => Yii::$app->request->get('skipDate') ? null : date(DateHelper::FORMAT_USER_DATE),
                ]);
                break;
            case TaxRobotHelper::TYPE_ORDER:
                $cashbox = $company->getCashboxes()->orderBy(['is_main' => SORT_DESC])->one();
                $model = new CashOrderFlows([
                    'flow_type' => $flow_type,
                    'is_accounting' => 1,
                    'company_id' => $company->id,
                    'author_id' => $employee->id,
                    'date' => Yii::$app->request->get('skipDate') ? null : date(DateHelper::FORMAT_USER_DATE),
                    'cashbox_id' => ArrayHelper::getValue($cashbox, 'id'),
                    'is_accounting' => ArrayHelper::getValue($cashbox, 'is_accounting', true),
                ]);
                break;

            default:
                $model = null;
                break;
        }

        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())
            && LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE)
        ) {
            $model->linkSelectedInvoices();

            return $this->redirect(Yii::$app->request->referrer ?: ['bank']);
        }

        return $this->renderAjax("@frontend/modules/cash/views/{$type}/_form", [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionUpdateMovement($type, $id)
    {
        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        switch ($type) {
            case TaxRobotHelper::TYPE_BANK:
                if ($model = CashBankFlowsForm::findOne(['id' => $id, 'company_id' => $company->id])) {
                    $model->setScenario('update');
                }
                break;
            case TaxRobotHelper::TYPE_ORDER:
                $model = CashOrderFlows::findOne(['id' => $id, 'company_id' => $company->id]);
                break;

            default:
                $model = null;
                break;
        }

        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())
            && LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_UPDATE)
        ) {
            $model->linkSelectedInvoices();

            return $this->redirect(Yii::$app->request->referrer ?: ['bank']);
        }

        return $this->renderAjax("@frontend/modules/cash/views/{$type}/_form", [
            'model' => $model,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCalculation()
    {
        $taxRobot = $this->taxRobot;

        $company = $taxRobot->company;

        $params = ArrayHelper::merge($_POST, Yii::$app->request->queryParams);
        $dataProvider = $taxRobot->getExpenseDataProvider(ArrayHelper::getValue($params, 'TaxRobot'));

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('calculation', [
            'taxRobot' => $taxRobot,
            'dataProvider' => $dataProvider,
            'company' => $company,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionPayment()
    {
        $taxRobot = $this->taxRobot;

        $taxRobot->processPeriodPayments();

        $searchModel = new TaxrobotNeedPaySearch($taxRobot);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('payment', [
            'company' => $taxRobot->company,
            'taxRobot' => $taxRobot,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionTaxable($type, $id)
    {
        $company = Yii::$app->user->identity->company;

        switch ($type) {
            case TaxRobotHelper::TYPE_BANK:
                $model = CashBankFlows::findOne(['id' => $id, 'company_id' => $company->id]);
                break;
            case TaxRobotHelper::TYPE_ORDER:
                $model = CashOrderFlows::findOne(['id' => $id, 'company_id' => $company->id]);
                break;

            default:
                $model = null;
                break;
        }

        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if (($is_taxable = Yii::$app->request->post('is_taxable')) !== null) {
            $model->updateAttributes(['is_taxable' => (bool) $is_taxable]);
        }

        return Yii::$app->request->isAjax ? $model->is_taxable : $this->redirect(Yii::$app->request->referrer ? : ['bank']);
    }

    /**
     * @return mixed
     */
    public function actionTaxableGroup(array $flow_ids, $is_taxable)
    {
        $company = Yii::$app->user->identity->company;

        foreach ($flow_ids as $flow) {

            list($type, $id) = explode('_', $flow);

            switch ($type) {
                case TaxRobotHelper::TYPE_BANK:
                    $model = CashBankFlows::findOne(['id' => $id, 'company_id' => $company->id]);
                    break;
                case TaxRobotHelper::TYPE_ORDER:
                    $model = CashOrderFlows::findOne(['id' => $id, 'company_id' => $company->id]);
                    break;
                default:
                    $model = null;
                    break;
            }

            if ($model) {
                $model->updateAttributes(['is_taxable' => (bool)$is_taxable]);
            }
        }

        return Yii::$app->request->isAjax ? $model->is_taxable : $this->redirect(Yii::$app->request->referrer ? : ['bank']);
    }

    /**
     * @return mixed
     */
    public function actionDelete()
    {
        $company = Yii::$app->user->identity->company;

        $idList = (array) Yii::$app->request->post('flowId');

        foreach ($idList as $key => $value) {
            $data = explode('_', $value);
            switch (ArrayHelper::getValue($data, 0)) {
                case TaxRobotHelper::TYPE_BANK:
                    $model = CashBankFlows::findOne(['id' => ArrayHelper::getValue($data, 1), 'company_id' => $company->id]);
                    break;
                case TaxRobotHelper::TYPE_ORDER:
                    $model = CashOrderFlows::findOne(['id' => ArrayHelper::getValue($data, 1), 'company_id' => $company->id]);
                    break;

                default:
                    $model = null;
                    break;
            }

            if ($model !== null) {
                LogHelper::delete($model, LogEntityType::TYPE_CASH);
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? : ['bank']);
    }

    /**
     * @return mixed
     */
    public function actionDeleteOne()
    {
        $id = Yii::$app->request->get('id');
        $model = CashBankFlows::findOne(['id' => $id, 'company_id' => Yii::$app->user->identity->company->id]);

        if ($model !== null) {
            LogHelper::delete($model, LogEntityType::TYPE_CASH);
        }

        return $this->redirect(Yii::$app->request->referrer ? : ['bank']);
    }

    /**
     * @param $id
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionUpdateFlow($id)
    {
        $company = Yii::$app->user->identity->company;
        $model = CashBankFlows::findOne(['id' => $id, 'company_id' => $company->id]);
        $model->setScenario('update');

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())
            && LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE)
        ) {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('part_calculation/modal-update', [
                    'model' => $model,
                ]);
            } else {
                return $this->redirect(Yii::$app->request->get('redirect', ['calculation']));
            }
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('part_calculation/modal-update', [
                    'model' => $model,
                ]);
            } else {
                return $this->render('part_calculation/modal-update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * @return mixed|string|\yii\web\Response
     * @throws \yii\base\Exception
     */
    public function actionCreateFlow($flow_type)
    {
        $company = Yii::$app->user->identity->company;
        $checkingAccountant = $company->mainCheckingAccountant;
        $model = new CashBankFlowsForm([
            'scenario' => 'create',
            'company_id' => $company->id,
            'rs' => $checkingAccountant ? $checkingAccountant->rs : null,
            'bank_name' => $checkingAccountant ? $checkingAccountant->bank_name : null,
            'date' => date(DateHelper::FORMAT_USER_DATE),
            'flow_type' => $flow_type,
        ]);

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())
            && LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE)
        ) {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('part_calculation/modal-create', [
                    'model' => $model,
                ]);
            } else {
                return $this->redirect(Yii::$app->request->get('redirect', ['calculation']));
            }
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('part_calculation/modal-create', [
                    'model' => $model,
                ]);
            } else {
                return $this->render('part_calculation/modal-create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * @param $actionType
     * @param $id
     * @param $filename
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionDocumentPrint($actionType, $id = null, $type = null, $filename = null)
    {
        $taxRobot = $this->taxRobot;

        $model = $taxRobot->getPaymentOrder();

        if ($actionType == 'print') {
            $model->updateStatus(PaymentOrderStatus::STATUS_PRINTED);
        }

        if ($actionType == 'pdf')
            \common\models\company\CompanyFirstEvent::checkEvent($taxRobot->company, 95);
        else
            \common\models\company\CompanyFirstEvent::checkEvent($taxRobot->company, 96);

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view', [
            'company' => $taxRobot->company,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionDeclaration()
    {
        $taxRobot = $this->taxRobot;

        $isEmptyDeclaration = Yii::$app->request->get('empty');

        $company = $taxRobot->company;

        $year = $taxRobot->getYear();

        /** @var TaxDeclaration $model */
        $model = TaxDeclaration::findOne([
            'company_id' => $company->id,
            'tax_year' => $year
        ]);

        if (!$model)
            $model = $this->createNullDeclaration($year);

        $model = $this->updateDeclarationTaxRate($model);

        if (!$isEmptyDeclaration && $taxRobot->isPaid) {
            $model = $this->updateDeclarationQuartersByCashFlows($model, $taxRobot->period->id);
        }

        if ($isEmptyDeclaration)
            $model = $this->updateNullDeclarationQuarters($model);

        /** @var Kudir $kudir */
        $kudir = Kudir::findOne([
            'company_id' => $company->id,
            'tax_year' => $year
        ]);

        if (!$kudir) {
            $kudir = new Kudir();
            $kudir->company_id = $company->id;
            $kudir->tax_year = $year;
            $kudir->document_date = date('d.m.Y');
            if ($taxRobot->isPaid) {
                $kudir->save();
            }
        }

        $taxSearchModel = new TaxrobotCashBankSearch(['bik' => 'all']);
        $sumTaxToPay = $taxSearchModel->getSumTaxToPay([
            'from' => $taxRobot->period->dateFrom->format('Y-m-d'),
            'to' => $taxRobot->period->dateTill->format('Y-m-d')]
        );

        if ($isEmptyDeclaration)
            \common\models\company\CompanyFirstEvent::checkEvent($company, 89); // Нулевая декларация. Клик на ссылку
        else
            \common\models\company\CompanyFirstEvent::checkEvent($company, 92); // Декларация. Клик на ссылку

        return $this->render('declaration', [
            'taxRobot' => $taxRobot,
            'company' => $company,
            'model' => $model,
            'kudir' => $kudir,
            'sumTaxToPay' => $sumTaxToPay,
            'isEmptyDeclaration' => $isEmptyDeclaration
        ]);
    }

    private function createNullDeclaration($year) {

        $company = $this->taxRobot->company;

        /** @var TaxDeclaration $model */
        $model = new TaxDeclaration();
        $model->company_id = $company->id;
        $model->taxpayer_sign = 2;
        $model->tax_year = $year;
        $model->document_knd = "1152017";
        $model->tax_period = "34";
        $model->tax_service_location = "120";
        $model->tax_service_ifns = (string)$company->ifns->ga;
        $model->document_date = date('Y-m-d');
        $model->document_correction_number = "0";

        $model->created_at = time();
        $model->status_id = TaxDeclarationStatus::STATUS_CREATED;
        $model->status_updated_at = time();
        $model->status_author_id = Yii::$app->user->id;
        $model->document_author_id = Yii::$app->user->id;

        /** @var TaxDeclarationQuarter[] $quarters */
        $quarters = [];
        for ($number=1; $number<=4; $number++) {

            $m = new TaxDeclarationQuarter();
            $m->quarter_number = $number;
            $m->tax_rate = $company->companyTaxationType->usn_percent ?: "6.0";
            $m->oktmo = $model->company->oktmo;
            $m->income_amount = "0.00";
            $m->outcome_amount = "0.00";
            $m->tax_amount = "0.00";
            $m->prepayment_tax_amount = "0.00";

            $quarters[] = $m;
        }

        $model->save(false);
        $model->saveQuarters($quarters);

        LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE);

        return $model;
    }

    private function updateDeclarationTaxRate($model) {

        $quarters = $model->taxDeclarationQuarters;

        /** @var TaxDeclarationQuarter $m */
        foreach ($quarters as $m) {
            $m->oktmo = $model->company->oktmo;
            $m->tax_rate = $this->taxRobot->company->companyTaxationType->usn_percent ?: "6.0";
            $m->save(false);
        }

        return $model;
    }

    private function updateNullDeclarationQuarters($model) {

        /** @var TaxDeclaration $model */
        /** @var TaxDeclarationQuarter $m */

        $quarters = $model->taxDeclarationQuarters;

        $number = 1;
        foreach ($quarters as $m) {

            $m->quarter_number = $number;

            $m->income_amount = 0;

            $m->outcome_amount = 0;

            $m->prepayment_tax_amount = 0;

            $m->tax_amount = 0;

            $m->save(false);

            $number++;
        }

        return $model;
    }

    private function updateDeclarationQuartersByCashFlows($model, $period = null) {

        $maxQuarterNumber = 4;
        if ($period) {
            $maxQuarterNumber = (int)str_replace("{$model->tax_year}_", "", $period);
        }

        $prev_income_amount = 0;
        $prev_outcome_amount = 0;
        $prev_tax_amount = 0;

        /** @var TaxDeclaration $model */
        /** @var TaxDeclarationQuarter $m */

        $quarters = $model->taxDeclarationQuarters;

        $number = 1;
        foreach ($quarters as $m) {

            if ($m->quarter_number <= $maxQuarterNumber) {

                $quarterIncomeKop = $model->getQuarterIncomeOrderFlowsSum($model->tax_year, $number)
                                  + $model->getQuarterIncomeBankFlowsSum($model->tax_year, $number);
                $quarterOutcomeKop = $model->getQuarterOutcomeOrderFlowsSum($model->tax_year, $number)
                                  + $model->getQuarterOutcomeBankFlowsSum($model->tax_year, $number);

            } else {
                $quarterIncomeKop = 0;
                $quarterOutcomeKop = 0;
            }

            $m->quarter_number = $number;

            $m->income_amount = round($quarterIncomeKop / 100 + $prev_income_amount);

            $m->outcome_amount = round($quarterOutcomeKop / 100 + $prev_outcome_amount);

            if ($model->taxpayer_sign == 2 && $m->outcome_amount > $m->income_amount)
                $m->outcome_amount = $m->income_amount;
            elseif ($model->taxpayer_sign == 1 && $m->outcome_amount > $m->income_amount / 2)
                $m->outcome_amount = $m->income_amount / 2;

            $m->prepayment_tax_amount = round($m->income_amount * $m->tax_rate / 100);

            $m->tax_amount = round($m->prepayment_tax_amount - $m->outcome_amount - $prev_tax_amount);

            if ($model->taxpayer_sign == 2 && $m->tax_amount < 0)
                $m->tax_amount = 0;
            if ($model->taxpayer_sign == 1 && $m->tax_amount < $m->prepayment_tax_amount / 2)
                $m->tax_amount = round($m->prepayment_tax_amount / 2);

            $m->save(false);

            $prev_tax_amount += $m->tax_amount;
            $prev_income_amount += $quarterIncomeKop / 100;
            $prev_outcome_amount += $quarterOutcomeKop / 100;

            $number++;
        }

        return $model;
    }

    /**
     * @param $actionType
     * @param $id
     * @param $filename
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionDeclarationPrint($actionType, $id = null, $type = null, $filename = null)
    {
        $taxRobot = $this->taxRobot;
        $company = $taxRobot->company;

        /** @var TaxDeclaration $model */
        $model = TaxDeclaration::findOne([
            'company_id' => $company->id,
            'id' => $id,
        ]);

        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        // Is empty declaration
        if (Yii::$app->request->get('empty'))
            \common\models\company\CompanyFirstEvent::checkEvent($company, 90);
        else
            \common\models\company\CompanyFirstEvent::checkEvent($company, 93);

        $model->updateDownloadedQuarterNum();

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, '@frontend/modules/tax/views/declaration/pdf-view', [
            'company' => $company,
        ]);
    }

    /**
     * Displays a single PaymentOrder model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionImport()
    {
        $taxRobot = $this->taxRobot;

        $company = $taxRobot->company;
        $model = $taxRobot->getPaymentOrder();

        $model->updateStatus(PaymentOrderStatus::STATUS_IMPORT_IN_BANK);

        return Yii::$app->response->sendContentAsFile(
            $this->renderPartial('import', ['modelArray' => [$model], 'company' => $company]),
            'Платежное_поручение.txt',
            ['mimeType' => 'text/plain']
        );
    }

    public function actionViewPaymentOrder($id)
    {
        $taxRobot = $this->taxRobot;

        return $this->render('view-payment-order', [
            'model' => $this->findPaymentOrderModel($id),
            'taxRobot' => $taxRobot,
            'period' => $taxRobot->getUrlPeriodId(),
        ]);
    }

    public function actionUpdatePaymentOrder($id)
    {
        $model = $this->findPaymentOrderModel($id);
        $this->taxRobot->paymentOrderMaxSum($model);

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) &&
            LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE)
        ) {
            return $this->redirect([
                'view-payment-order',
                'id' => $id,
                'period' => $this->taxRobot->getUrlPeriodId(),
            ]);
        } else {
            return $this->render('update-payment-order', [
                'model' => $model,
                'company' => Company::findOne(['id' => $model->company_id]),
                'taxRobot' => $this->taxRobot,
            ]);
        }
    }

    /**
     * @param \yii\base\Action $action
     * @return bool|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $user = Yii::$app->user->identity;
            $company = $user->company;
            $this->taxRobot = new TaxRobotHelper($company, $user);
            if ($action->id == 'error') {
                return true;
            }
            if (($period = Yii::$app->request->get('period')) !== null
            ||  ($period = Yii::$app->session->get('taxrobot_ip_period')) !== null) {
                if (in_array($period, array_keys($this->taxRobot->getPeriodArray()))) {
                    $this->taxRobot->setPeriod($period);
                    Yii::$app->session->set('taxrobot_ip_period', $period);
                } else {
                    throw new NotFoundHttpException('The requested page does not exist.');
                }
            }

            if (Yii::$app->request->isAjax && in_array($action->id, ['clear-preload-banking-accounts'])) {

                return true;
            }

            if (!$this->taxRobot->actionIsAllowed($action->id)) {
                $this->redirect($this->taxRobot->redirectAction)->send();

                return false;
            }

            $this->taxRobot->load(Yii::$app->request->get());

            $eventActions = [
                'company',
                'params',
                'bank',
            ];
            if (in_array($action->id, $eventActions)) {
                \common\models\company\CompanyFirstEvent::checkEvent($company, 84, true);
            }
            if (Yii::$app->request->get('from_savetax_popup')) {
                \common\models\company\CompanyFirstEvent::checkEvent($company, 99, true);
            }

            /* @var $menuItem MenuItem */
            $menuItem = Yii::$app->user->identity->menuItem;
            if (!$menuItem->accountant_item) {
                $menuItem->accountant_item = true;
                $menuItem->save(false, ['accountant_item']);
            }

            return true;
        }

        return false;
    }

    protected function getRealPaymentOrder($year, $quarter)
    {
        $company = Yii::$app->user->identity->company;

        $taxrobotPaymentOrder = TaxrobotPaymentOrder::find()->where([
            'company_id' => $company->id,
            'year' => $year,
            'quarter' => $quarter
        ])->one();

        /** @var PaymentOrder $model */
        $model = $taxrobotPaymentOrder ? PaymentOrder::findOne([
            'id' => $taxrobotPaymentOrder->payment_order_id,
            'company_id' => $company->id,
        ]) : null;

        // Create Order
        if ($model === null) {

            $model = $this->taxRobot->paymentOrder;
            $model->sum = $model->sum / 100;
            $model->document_number = PaymentOrder::getNextDocumentNumber($company);
            $model->save(false);

            // save relations
            $taxrobotPaymentOrders = TaxrobotPaymentOrder::find()->where([
                'company_id' => $company->id,
                'year' => $year,
                'quarter' => $quarter,
            ])->all();

            foreach ($taxrobotPaymentOrders as $paymentOrder) {
                $paymentOrder->payment_order_id = $model->primaryKey;
                $paymentOrder->save(false);
            }

            LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE);
        }

        // Update order sum
        if ($model->sum != $this->taxRobot->paymentOrder->sum) {
            $model->sum = $this->taxRobot->paymentOrder->sum;
            $model->sum_in_words = $this->taxRobot->paymentOrder->sum_in_words;
            $model->save(false, ['sum', 'sum_in_words']);
        }

        return $model;
    }

    protected function findPaymentOrderModel($id)
    {
        $model = $this->taxRobot->company->getPaymentOrders()->andWhere([
            'id' => $id,
        ])->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $actionType
     * @param $id
     * @param $filename
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionInventoryPrint($actionType, $id = null, $type = null, $filename = null)
    {
        $company = Yii::$app->user->identity->company;

        /** @var TaxDeclaration $declarationModel */
        $declarationModel = TaxDeclaration::findOne([
            'company_id' => $company->id,
            'id' => $id,
        ]);
        if ($declarationModel === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $model = new Inventory();
        $model->company = $company;
        $model->document_number = "1";
        $model->document_date = $declarationModel->document_date;
        $model->control_organ_address = $company->ifns->g1;
        $model->control_organ = $company->ifns->gb;
        $model->shipped_date = $declarationModel->document_date;
        $model->tax_year = $declarationModel->tax_year;
        $model->totalValue = 100;
        //$model->type = null;
        //$model->control_organ_name_full = 'control_organ_name_full';
        //$model->receipt = 'receipt';

        // Empty declaration
        if (Yii::$app->request->get('empty'))
            \common\models\company\CompanyFirstEvent::checkEvent($company, 91);
        else
            \common\models\company\CompanyFirstEvent::checkEvent($company, 94);

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, '@frontend/modules/tax/views/inventory/pdf-view', [
            'company' => $this->taxRobot->company,
        ]);
    }

    private function updateValidAttributes($model) {

        if ($model instanceof Company && !$model->validate()) {

            $modelOld = Company::findOne($model->id);

            $attributes = [
                'inn',
                'egrip',
                'tax_authority_registration_date',
                'ip_firstname',
                'ip_lastname',
                'ip_patronymic',
                'address_legal',
                'ifns_ga',
                'oktmo',
                'address_legal_city',
                'has_chief_patronymic'
            ];

            $errorAttributes = array_keys($model->getErrors());

            foreach ($attributes as $key=>$attr)
                if (!in_array($attr, $errorAttributes) && $modelOld->{$attr} != $model->{$attr})
                    $model->updateAttributes([$attr]);

            if (preg_match('/^[0-9]{4}$/', $model->ifns_ga) && $model->ifns_ga != $modelOld->ifns_ga) {
                $model->updateAttributes(['ifns_ga']);
            }
        }
    }

    public function actionShowCubFeatures() {

        /* @var Employee $user */
        $user = Yii::$app->user->identity;
        if (!$user->menuItem->invoice_item) {
            $user->menuItem->invoice_item = true;
            $user->menuItem->save();
        }

        return $this->redirect('/site/index');
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionChangeDeclarationDate($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var TaxDeclaration $model */
        $model = TaxDeclaration::findOne([
            'company_id' => Yii::$app->user->identity->company->id,
            'id' => $id
        ]);

        if ($model && $newDate = Yii::$app->request->post('newDate')) {

            $model->document_date = $newDate;

            if ($model->save(true, ['document_date']))

                return ['value' => DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)];

        } else {

            return ['value' => null, 'error' => 'Model not found.'];
        }

        return ['value' => null, 'error' => $model->getErrors()];
    }

    /**
     * @return array|string
     */
    public function actionCreateCheckingAccountant()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        /* @var $model CheckingAccountant */
        $model = new CheckingAccountant([
            'company_id' => $company->id,
        ]);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $rsData["add-checking-accountant"] = '[ + Добавить расчетный счет ]';

            $companyRs = $company->getCheckingAccountants()
                ->select(['bank_name', 'rs', 'id', 'bik'])
                ->andWhere(['not', ['type' => CheckingAccountant::TYPE_CLOSED]])
                ->orderBy(['type' => SORT_ASC])
                ->indexBy('rs')
                ->asArray()->all();
            foreach ($companyRs as $key => $rs) {
                $rsData[$rs['rs']] = $rs['rs'] . ', ' . $rs['bank_name'];
            }

            return [
                'result' => true,
                'options' => $rsData,
                'companyRs' => $companyRs,
                'val' => $model->rs,
            ];
        }

        return $this->renderAjax('parts_bank/modal_rs/_modal_form', [
            'checkingAccountant' => $model,
            'title' => 'Добавить расчетный счет',
            'id' => 'add-company-rs',
            'company' => $company
        ]);
    }

    private function saveFnsContractor($ifns_ga) {

        /* @var $company Company */
        $company = Yii::$app->user->identity->company;

        $dadata = new DadataClient();
        $fns_data = $dadata->getFns($ifns_ga);

        if (empty($fns_data) || empty($fns_data['suggestions']) || empty($fns_data['suggestions'][0]))
            return false;

        if (!($fns = ArrayHelper::getValue($fns_data['suggestions'][0], 'data')))
            return false;

        $contractor = Contractor::findOne([
            'company_id' => $company->id,
            'type' => Contractor::TYPE_SELLER,
            'ITN'  => $fns['inn']
        ]);

        if ($contractor === null) {
            $contractor = new Contractor([
                'company_id' => $company->id,
                'type' => Contractor::TYPE_SELLER,
                'status' => Contractor::ACTIVE,
                'chief_accountant_is_director' => true,
                'contact_is_director' => true,
                'name' => $fns['name'],
                'legal_address' => $fns['address'],
                'actual_address' => $fns['address'],
                'ITN' => $fns['inn'],
                'PPC' => $fns['kpp'],
                'current_account' => $fns['bank_account'],
                'bank_name' => $fns['bank_name'],
                'BIC' => $fns['bank_bic'],
                'corresp_account' => null,
                'taxation_system' => 0,
                'is_deleted' => false,
                'object_guid' => OneCExport::generateGUID(),
                'director_post_name' => 'НАЧАЛЬНИК'
            ]);
        } else { //update the contractor
            $contractor->name = $fns['name'];
            $contractor->legal_address = $fns['address'];
            $contractor->actual_address = $fns['address'];
            $contractor->ITN = $fns['inn'];
            $contractor->PPC = $fns['kpp'];
            $contractor->current_account = $fns['bank_account'];
            $contractor->bank_name = $fns['bank_name'];
            $contractor->BIC = $fns['bank_bic'];
        }

        if (!$contractor->save(false)) {
            return false;
        }

        return true;
    }
}
