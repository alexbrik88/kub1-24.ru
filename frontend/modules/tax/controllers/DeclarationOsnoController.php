<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 26.03.2019
 * Time: 18:46
 */

namespace frontend\modules\tax\controllers;


use common\components\DadataClient;
use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\pdf\Mpdf;
use common\components\pdf\PdfRenderer;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyFirstEvent;
use common\models\document\status\TaxDeclarationStatus;
use common\models\service\SubscribeTariffGroup;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\documents\components\DocumentBaseController;
use frontend\modules\documents\models\TaxDeclarationSearch;
use frontend\modules\tax\models\DeclarationOsnoHelper;
use frontend\modules\tax\models\Inventory;
use frontend\modules\tax\models\Kudir;
use frontend\modules\tax\models\TaxDeclaration;
use frontend\modules\tax\models\TaxrobotPaymentForm;
use frontend\rbac\UserRole;
use Yii;
use common\models\Company;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use frontend\modules\documents\assets\DocumentPrintAsset;

/**
 * Class DeclarationOsnoController
 * @package frontend\modules\tax\controllers
 */
class DeclarationOsnoController extends DocumentBaseController
{
    /**
     * @var string
     */
    private $_redirectAction = 'company';
    public $layout = null;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @param null $type
     * @return mixed|Response
     */
    public function actionIndex($type = null)
    {
        $this->actionIsAllowed($this->action->id);

        return $this->redirect($this->_redirectAction);
    }

    /**
     * @param null $type
     * @return string
     */
    public function actionIndexList($type = null)
    {
        if (!$this->actionIsAllowed($this->action->id)) {
            return $this->redirect($this->_redirectAction);
        }
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $searchModel = new TaxDeclarationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('index-list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'company' => $company,
        ]);
    }



    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAll()
    {
        if (!$this->actionIsAllowed($this->action->id)) {
            return $this->redirect($this->_redirectAction);
        }
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $declarationHelper = new DeclarationOsnoHelper();
        if (($period = Yii::$app->request->get('period')) !== null) {
            if (in_array($period, array_keys($declarationHelper->getPeriodArray()))) {
                $declarationHelper->setPeriod($period);
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        $selected_no_operations = ($cookie_no_operations = Yii::$app->request->cookies['ooo_no_operations']) ? $cookie_no_operations->value : null;
        $dataProvider = new ArrayDataProvider([
            'allModels' => $declarationHelper->buildItems($selected_no_operations),
        ]);

        return $this->render('all', [
            'company' => $company,
            'declarationHelper' => $declarationHelper,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return array|string|Response
     */
    public function actionCompany()
    {
        /* @var $model Company */
        $model = Yii::$app->user->identity->company;
        $model->scenario = Company::SCENARIO_DECLARATION_OOO;
        if ($model->inn) {
            $data = DadataClient::getCompanyData($model->inn);
            $model->oktmo = $model->oktmo ?: (isset($data['address']['data']['oktmo']) ? $data['address']['data']['oktmo'] : null);
        }
        if ($model->kpp) {
            $model->ifns_ga = $model->ifns_ga ?: substr($model->kpp, 0, 4);
        }
        if (($account = $model->mainCheckingAccountant) === null) {
            $account = new CheckingAccountant(['company_id' => $model->id, 'type' => CheckingAccountant::TYPE_MAIN]);
        }
        $additionalAccounts = $this->getAdditionalAccounts($model);
        $newAccounts = $this->getNewAccounts($model);
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $account->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return array_merge(
                ActiveForm::validate($model),
                ActiveForm::validate($account),
                $this->validateAccounts($additionalAccounts),
                $this->validateAccounts($newAccounts, true)
            );
        }
        if ($model->load(Yii::$app->request->post()) && $account->load(Yii::$app->request->post())) {
            if ($account->save() && $model->save() && $this->saveAccounts([$additionalAccounts, $newAccounts])) {
                return $this->redirect('params');
            }
        }

        return $this->render('company', [
            'model' => $model,
            'account' => $account,
            'additionalAccounts' => $additionalAccounts,
            'newAccounts' => $newAccounts
        ]);
    }

    /**
     * @return array|string|Response
     */
    public function actionParams()
    {
        if (!$this->actionIsAllowed($this->action->id)) {
            return $this->redirect($this->_redirectAction);
        }
        /* @var $model Company */
        $model = Yii::$app->user->identity->company;
        $model->scenario = Company::SCENARIO_TAX_PSN;
        if (Yii::$app->request->isAjax && $model->companyTaxationType->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return array_merge(ActiveForm::validate($model->companyTaxationType));
        }
        if ($model->companyTaxationType->load(Yii::$app->request->post())) {
            if ($model->companyTaxationType->save()) {
                // Подтверждение данных по ИП
                Yii::$app->response->cookies->add(new \yii\web\Cookie([
                    'name' => 'tax_robot_accepted',
                    'value' => '1',
                    'expire' => time() + 3600 * 24 * 7
                ]));
                // Не было операций с начала регистрации
                Yii::$app->response->cookies->add(new \yii\web\Cookie([
                    'name' => 'ooo_no_operations',
                    'value' => Yii::$app->request->post('ooo_no_operations'),
                    'expire' => time() + 3600 * 24 * 365
                ]));
                // todo: перенести список ОФД в БД, добавить признак в компанию
                if ($ofds = Yii::$app->request->post('Ofd')) {
                    if (!empty($ofds['taxcom']))
                        // Есть аккаунт TaxCOM
                        Yii::$app->response->cookies->add(new \yii\web\Cookie([
                            'name' => 'taxcom_user',
                            'value' => '1',
                            'expire' => time() + 3600 * 24 * 365 * 10
                        ]));
                } else if (Yii::$app->request->cookies['taxcom_user']) {
                    Yii::$app->response->cookies->remove('taxcom_user');
                }

                return $this->redirect(['all']);
            }
        }
        $accounts = CheckingAccountant::find()
            ->select(['bank_name, COUNT(*) AS cnt'])
            ->byCompany($model->id)
            ->byType([CheckingAccountant::TYPE_MAIN, CheckingAccountant::TYPE_ADDITIONAL])
            ->groupBy(['bank_name'])
            ->orderBy('type')
            ->asArray()->all();
        CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 85, true);

        return $this->render('params', [
            'model' => $model,
            'accounts' => $accounts,
            'isPaid' => $this->getIsPaid()
        ]);
    }

    /**
     * @return integer
     */
    public function getIsPaid()
    {
        $company = Yii::$app->user->identity->company;
        if (ArrayHelper::getValue(Yii::$app->params, 'tax-robot-paid', false)) {
            return true;
        }

        return $company->getHasActualSubscription(SubscribeTariffGroup::OOO_OSNO_NULL_REPORTING);
    }

    /**
     * @return string|Response
     * @throws \yii\base\Exception
     */
    public function actionDeclaration()
    {
        if (!$this->actionIsAllowed($this->action->id)) {
            return $this->redirect($this->_redirectAction);
        }
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $declarationHelper = new DeclarationOsnoHelper();
        if (($period = Yii::$app->request->get('period')) !== null) {
            if (in_array($period, array_keys($declarationHelper->getPeriodArray()))) {
                $declarationHelper->setPeriod($period);
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        /** @var TaxDeclaration $model */
        $model = TaxDeclaration::findOne([
            'company_id' => $company->id,
            'tax_year' => $declarationHelper->year,
            'tax_quarter' => $declarationHelper->quarter,
            'single' => true
        ]);
        if (!$model) {
            $model = $this->createNullDeclaration($company, DeclarationOsnoHelper::TYPE_SINGLE, $declarationHelper->year, $declarationHelper->quarter);
        }

        /* КУДиР для ООО на ОСНО не ведется
        $kudir = Kudir::findOne([
            'company_id' => $company->id,
            'tax_year' => date('Y')
        ]);
        if (!$kudir) {
            $kudir = new Kudir();
            $kudir->company_id = $company->id;
            $kudir->tax_year = date('Y');
            $kudir->document_date = date('d.m.Y');
            $kudir->save();
        }
        */

        return $this->render('declaration', [
            'company' => $company,
            'model' => $model,
            'declarationHelper' => $declarationHelper,
        ]);
    }

    /**
     * @return string|Response
     * @throws \yii\base\Exception
     */
    public function actionValueAddedDeclaration()
    {
        if (!$this->actionIsAllowed($this->action->id)) {
            return $this->redirect($this->_redirectAction);
        }
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $declarationHelper = new DeclarationOsnoHelper();
        if (($period = Yii::$app->request->get('period')) !== null) {
            if (in_array($period, array_keys($declarationHelper->getPeriodArray()))) {
                $declarationHelper->setPeriod($period);
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        /** @var TaxDeclaration $model */
        $model = TaxDeclaration::findOne([
            'company_id' => $company->id,
            'tax_year' => $declarationHelper->year,
            'tax_quarter' => $declarationHelper->quarter,
            'nds' => true,
        ]);
        if (!$model) {
            $model = $this->createNullDeclaration($company, DeclarationOsnoHelper::TYPE_NDS, $declarationHelper->year, $declarationHelper->quarter);
        }

        return $this->render('value-added-declaration', [
            'company' => $company,
            'model' => $model,
            'declarationHelper' => $declarationHelper,
        ]);
    }

    /**
     * @return string|Response
     * @throws \yii\base\Exception
     */
    public function actionProfitDeclaration()
    {
        if (!$this->actionIsAllowed($this->action->id)) {
            return $this->redirect($this->_redirectAction);
        }
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $declarationHelper = new DeclarationOsnoHelper();
        if (($period = Yii::$app->request->get('period')) !== null) {
            if (in_array($period, array_keys($declarationHelper->getPeriodArray()))) {
                $declarationHelper->setPeriod($period);
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        /** @var TaxDeclaration $model */
        $model = TaxDeclaration::findOne([
            'company_id' => $company->id,
            'tax_year' => $declarationHelper->year,
            'tax_quarter' => $declarationHelper->quarter,
            'org' => true
        ]);
        if (!$model) {
            $model = $this->createNullDeclaration($company, DeclarationOsnoHelper::TYPE_ORG, $declarationHelper->year, $declarationHelper->quarter);
        }

        return $this->render('profit-declaration', [
            'company' => $company,
            'model' => $model,
            'declarationHelper' => $declarationHelper,
        ]);
    }

    /**
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionBalance()
    {
        if (!$this->actionIsAllowed($this->action->id)) {
            return $this->redirect($this->_redirectAction);
        }
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $declarationHelper = new DeclarationOsnoHelper();
        if (($period = Yii::$app->request->get('period')) !== null) {
            if (in_array($period, array_keys($declarationHelper->getPeriodArray()))) {
                $declarationHelper->setPeriod($period);
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        /** @var TaxDeclaration $model */
        $model = TaxDeclaration::findOne([
            'company_id' => $company->id,
            'tax_year' => $declarationHelper->year,
            'balance' => true
        ]);
        if (!$model) {
            $model = $this->createNullDeclaration($company, DeclarationOsnoHelper::TYPE_BALANCE, $declarationHelper->year);
        }

        return $this->render('balance', [
            'company' => $company,
            'model' => $model,
            'declarationHelper' => $declarationHelper,
        ]);
    }

    /**
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionSzvm()
    {
        if (!$this->actionIsAllowed($this->action->id)) {
            return $this->redirect($this->_redirectAction);
        }

        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $declarationHelper = new DeclarationOsnoHelper();
        if (($period = Yii::$app->request->get('period')) !== null && ($month = Yii::$app->request->get('month')) !== null) {
            if (in_array($period, array_keys($declarationHelper->getPeriodArray())) && $month >= 1 && $month <= 12) {
                $declarationHelper->setPeriod($period);
                $declarationHelper->setMonth($month);
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        } else {
            return $this->redirect('/tax/declaration-osno/all');
        }

        /** @var TaxDeclaration $model */
        $model = TaxDeclaration::findOne([
            'company_id' => $company->id,
            'tax_year' => $declarationHelper->year,
            'tax_month' => $declarationHelper->month,
            'szvm' => true
        ]);
        if (!$model) {
            $model = $this->createNullDeclaration($company, DeclarationOsnoHelper::TYPE_SZVM, $declarationHelper->year, $declarationHelper->quarter, $declarationHelper->month);
        }

        return $this->render('szvm', [
            'company' => $company,
            'model' => $model,
            'declarationHelper' => $declarationHelper,
        ]);
    }

    /**
     * @param $type
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyDelete()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $company = Yii::$app->user->identity->company;

        $declarations = Yii::$app->request->post('TaxDeclaration');
        foreach ($declarations as $id => $declaration) {
            if ($declaration['checked']) {
                /** @var TaxDeclaration $model */
                $model = TaxDeclaration::findOne([
                    'company_id' => $company->id,
                    'id' => $id
                ]);
                if ($model) {
                    LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT);
                    Yii::$app->session->setFlash('success', 'Отчетность успешно удалена');
                }
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['/tax/declaration-osno/index-list']);
    }

    public function actionManyDocumentCreate()
    {
        $company = Yii::$app->user->identity->company;

        if (!($multiple = Yii::$app->request->get('multiple'))) {
            Yii::$app->session->setFlash('error', "Невозможно распечатать документы.");
            return $this->redirect(['/tax/declaration-osno/all']);
        }

        $documents = explode(',', $multiple);
        $ids = [];
        foreach ($documents as $document) {
            @list($type, $year, $quarter, $month) = explode('_', $document);
            if ($type && $year && strlen($quarter) && strlen($month)) {
                switch ($type) {
                    case 'single':
                        $model = TaxDeclaration::findOne(['company_id' => $company->id, 'tax_year' => $year, 'tax_quarter' => $quarter, 'single' => true]);
                        if (!$model) $model = $this->createNullDeclaration($company, DeclarationOsnoHelper::TYPE_SINGLE, $year, $quarter);
                        break;
                    case 'nds':
                        $model = TaxDeclaration::findOne(['company_id' => $company->id, 'tax_year' => $year, 'tax_quarter' => $quarter, 'nds' => true]);
                        if (!$model) $model = $this->createNullDeclaration($company, DeclarationOsnoHelper::TYPE_NDS, $year, $quarter);
                        break;
                    case 'org':
                        $model = TaxDeclaration::findOne(['company_id' => $company->id, 'tax_year' => $year, 'tax_quarter' => $quarter, 'org' => true]);
                        if (!$model) $model = $this->createNullDeclaration($company, DeclarationOsnoHelper::TYPE_ORG, $year, $quarter);
                        break;
                    case 'balance':
                        $model = TaxDeclaration::findOne(['company_id' => $company->id, 'tax_year' => $year, 'balance' => true]);
                        if (!$model) $model = $this->createNullDeclaration($company, DeclarationOsnoHelper::TYPE_BALANCE, $year);
                        break;
                    case 'szvm':
                        $model = TaxDeclaration::findOne(['company_id' => $company->id, 'tax_year' => $year, 'tax_quarter' => $quarter, 'tax_month' => $month, 'szvm' => true]);
                        if (!$model) $model = $this->createNullDeclaration($company, DeclarationOsnoHelper::TYPE_SZVM, $year, $quarter, $month);
                        break;
                    default:
                        $model = new TaxDeclaration();
                        break;
                }

                $ids[] = $model->getPrimaryKey();
            }

        }

        if ($ids) {
            return $this->redirect(['many-document-print', 'actionType' => 'pdf', 'multiple' => implode(',', $ids)]);
        }

        Yii::$app->session->setFlash('error', "Невозможно распечатать документы ");
        return $this->redirect(['/tax/declaration-osno/all']);
    }

    /**
     * @param $actionType
     * @param $type
     * @param $multiple
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionManyDocumentPrint($actionType, $multiple)
    {
        include_once(\Yii::getAlias('@vendor') . '/mpdf/mpdf/mpdf.php');
        $mpdf = new Mpdf('' /*not works*/, 'A4-P', 0, 'arial', 0, 0, 0, 0, 0, 0);
        $mpdf->SetDisplayMode(PdfRenderer::DISPLAY_MODE_FULLPAGE);

        $company = Yii::$app->user->identity->company;
        $IDS = explode(',', $multiple);

        foreach ($IDS as $i=>$id) {
            /** @var TaxDeclaration $model */
            $model = TaxDeclaration::findOne([
                'company_id' => $company->id,
                'id' => $id
            ]);

            if (!$model) continue; // TEMP

            $view = null;
            if ($model->single) $view = '@frontend/modules/tax/views/declaration/pdf-view-end';
            if ($model->nds) $view = '@frontend/modules/tax/views/declaration/pdf-view-nds';
            if ($model->org) $view = '@frontend/modules/tax/views/declaration/pdf-view-org';
            if ($model->balance) $view = '@frontend/modules/tax/views/balance/pdf-view';
            if ($model->szvm) $view = '@frontend/modules/tax/views/default/szvm';

            if (!$view) continue; // TEMP

            $params = [
                'asset' => DocumentPrintAsset::className(),
                'model' => $model,
                'company' => $model->company,
                'declarationHelper' => new DeclarationOsnoHelper()
            ];

            $_controller = \Yii::$app->controller;
            $_controller->layout = '@frontend/views/layouts/pdf';
            $_controller->view->params['asset'] = DocumentPrintAsset::className();
            $html = $_controller->render($view, $params);
            $mpdf->WriteHTML($html);
            if ((1+$i) < count($IDS))
                $mpdf->AddPage();

            // PRINTED
            $model->setStatusPrinted();
        }

        $filename = 'Отчетность-'.date('d-m-Y').'.pdf';
        $mpdf->SetTitle($filename);
        return $mpdf->Output($filename, PdfRenderer::DESTINATION_BROWSER);
    }

    /**
     * @param $actionType
     * @param $id
     * @return string|void|Response
     * @throws \yii\base\Exception
     */
    public function actionDeclarationPrint($actionType, $id)
    {
        /** @var TaxDeclaration $model */
        $model = TaxDeclaration::findOne([
            'id' => $id,
            'company_id' => Yii::$app->user->identity->company_id,
        ]);
        $declarationHelper = new DeclarationOsnoHelper();
        if (($period = Yii::$app->request->get('period')) !== null) {
            if (in_array($period, array_keys($declarationHelper->getPeriodArray()))) {
                $declarationHelper->setPeriod($period);
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        $this->view->title = $model->printablePrefix . '_за_' . $declarationHelper->period->label;

        // PRINTED
        $model->setStatusPrinted();

        if (Yii::$app->request->get('nds')) {
            return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, '@frontend/modules/tax/views/declaration/pdf-view-nds.php', [
                'company' => $model->company,
                'declarationHelper' => $declarationHelper,
            ]);
        }

        if (Yii::$app->request->get('org')) {
            return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, '@frontend/modules/tax/views/declaration/pdf-view-org.php', [
                'company' => $model->company,
                'declarationHelper' => $declarationHelper,
            ]);
        }

        if (Yii::$app->request->get('balance')) {
            return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, '@frontend/modules/tax/views/balance/pdf-view.php', [
                'company' => $model->company,
                'declarationHelper' => $declarationHelper,
            ]);
        }

        if (Yii::$app->request->get('szvm')) {
            return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, '@frontend/modules/tax/views/default/szvm.php', [
                'company' => $model->company,
                'declarationHelper' => $declarationHelper,
            ]);
        }

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, '@frontend/modules/tax/views/declaration/pdf-view-end.php', [
            'company' => $model->company,
            'declarationHelper' => $declarationHelper,
        ]);
    }

    /**
     * @param $actionType
     * @param null $id
     * @param null $type
     * @param null $filename
     * @return string|void
     * @throws \yii\base\Exception
     */
    public function actionInventoryPrint($actionType, $id = null, $type = null, $filename = null)
    {
        $company = Yii::$app->user->identity->company;

        /** @var TaxDeclaration $model */
        $declarationModel = TaxDeclaration::findOne([
            'id' => $id,
            'company_id' => $company->id,
        ]);

        $model = new Inventory();
        $model->company = $company;
        $model->document_number = "1";
        $model->document_date = $declarationModel->document_date;
        $model->control_organ_address = $company->ifns->g1;
        $model->control_organ = $company->ifns->gb;
        $model->shipped_date = $declarationModel->document_date;
        $model->tax_year = $declarationModel->tax_year;
        $model->tax_quarter = $declarationModel->tax_quarter;
        $model->tax_month = $declarationModel->tax_month;
        $model->totalValue = 100;
        $model->szvm = $declarationModel->szvm;
        $model->nds = $declarationModel->nds;
        $model->org = $declarationModel->org;
        $model->balance = $declarationModel->balance;

        CompanyFirstEvent::checkEvent($company, 91);

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, '@frontend/modules/tax/views/inventory/pdf-view', [
            'company' => $company,
        ]);
    }

    /**
     * @param $id
     * @return array
     */
    public function actionChangeDeclarationDate($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /** @var TaxDeclaration $model */
        $model = TaxDeclaration::findOne([
            'company_id' => Yii::$app->user->identity->company->id,
            'id' => $id,
        ]);
        if ($model && $newDate = Yii::$app->request->post('newDate')) {
            $model->document_date = $newDate;
            if ($model->save(true, ['document_date']))
                return [
                    'value' => DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                ];
        } else {
            return [
                'value' => null,
                'error' => 'Model not found.',
            ];
        }

        return [
            'value' => null,
            'error' => $model->getErrors(),
        ];
    }

    /**
     * @param $company
     * @param $year
     * @param null $type
     * @param $month
     * @return TaxDeclaration
     * @throws \yii\base\Exception
     */
    private function createNullDeclaration($company, $type, $year, $quarter = null, $month = null)
    {
        /** @var TaxDeclaration $model */
        $model = new TaxDeclaration();
        $model->company_id = $company->id;
        $model->taxpayer_sign = 2;
        $model->tax_year = $year;
        $model->document_knd = "1152017";
        $model->tax_period = "34";
        $model->tax_service_location = "214";
        $model->tax_service_ifns = (string)$company->ifns->ga;
        $model->document_date = date('Y-m-d');
        $model->document_correction_number = "0";
        $model->created_at = time();
        $model->status_id = TaxDeclarationStatus::STATUS_CREATED;
        $model->status_updated_at = time();
        $model->status_author_id = Yii::$app->user->id;
        $model->document_author_id = Yii::$app->user->id;
        switch ($type) {
            case DeclarationOsnoHelper::TYPE_NDS:
                $model->tax_quarter = $quarter;
                $model->nds = true;
                break;
            case DeclarationOsnoHelper::TYPE_ORG:
                $model->tax_quarter = $quarter;
                $model->org = true;
                break;
            case DeclarationOsnoHelper::TYPE_BALANCE:
                $model->balance = true;
                break;
            case DeclarationOsnoHelper::TYPE_SZVM:
                $model->szvm = true;
                $model->tax_quarter = $quarter;
                $model->tax_month = $month;
                break;
            case DeclarationOsnoHelper::TYPE_SINGLE:
                $model->tax_quarter = $quarter;
                $model->single = true;
                break;
        }
        $model->save(false);
        LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE);

        return $model;
    }

    /**
     * @param $action
     * @return bool
     */
    private function actionIsAllowed($action)
    {
        $setFlash = ($action === 'index') ? false : true;
        if ($action == 'company') {
            return true;
        }
        /** @var Company $company */
        $company = Yii::$app->user->identity->company;
        $company->scenario = Company::SCENARIO_DECLARATION_OOO;
        if (!$company->validate() || $company->mainCheckingAccountant === null || !$company->companyTaxationType->validate()) {
            $this->_redirectAction = 'company';
            if ($setFlash) {
                Yii::$app->session->setFlash('error', "Необходимо заполнить реквизиты.");
            }
            return false;
        }
        if ($action == 'params') {
            return true;
        }
        if (!$company->companyTaxationType->validate()) {
            $this->_redirectAction = 'params';
            if ($setFlash) {
                Yii::$app->session->setFlash('error', "Необходимо указать параметры вашего ООО.");
            }
            return false;
        }
        if (!Yii::$app->request->cookies['tax_robot_accepted'] || !Yii::$app->request->cookies['ooo_no_operations']) {
            $this->_redirectAction = 'params';
            if ($setFlash) {
                Yii::$app->session->setFlash('error', "Необходимо подтвердить данные по вашему ООО.");
            }
            return false;
        }
        if ($action == 'index') {
            $this->_redirectAction = 'params';
            return false;
        }
        if (!$this->isPaid) {
            $this->_redirectAction = 'params';
            Yii::$app->session->setFlash('error', "Необходимо отплатить тариф \"Нулевая отчетность ООО на ОСНО\".");
            return false;
        }

        return true;
    }

    /**
     * @param $company
     * @return array|\yii\db\ActiveRecord[]
     */
    private function getAdditionalAccounts($company)
    {
        return CheckingAccountant::find()
            ->byCompany($company->id)
            ->byType(CheckingAccountant::TYPE_ADDITIONAL)
            ->all();
    }

    /**
     * @param $company
     * @return array
     */
    private function getNewAccounts($company)
    {
        $newAccounts = [];
        for ($i = 1; $i <= 10; $i++) {
            $newAccounts[] = new CheckingAccountant(['id' => 'new_' . $i, 'isNewRecord' => false, 'company_id' => $company->id, 'type' => CheckingAccountant::TYPE_ADDITIONAL]);
        }

        return $newAccounts;
    }

    /**
     * @param $accounts
     * @param bool $skipEmpty
     * @return array
     */
    private function validateAccounts($accounts, $skipEmpty = false)
    {
        $validation = [];
        foreach ((array)$accounts as $a) {
            if ($a->load(Yii::$app->request->post()) && (!$skipEmpty || trim($a->bik)))
                $validation = array_merge($validation, ActiveForm::validate($a));
        }

        return $validation;
    }

    /**
     * @param $accounts
     * @return bool
     */
    private function saveAccounts($accounts)
    {
        list($additionalAccounts, $newAccounts) = $accounts;
        foreach ($additionalAccounts as $account) {
            if (!$account->load(Yii::$app->request->post()) || !$account->save())
                return false;
        }

        foreach ($newAccounts as $account) {
            if ($account->load(Yii::$app->request->post()) && trim($account->bik)) {
                $account->id = null;
                $account->isNewRecord = true;
                if (!$account->save())
                    return false;
            }
        }

        return true;
    }
}