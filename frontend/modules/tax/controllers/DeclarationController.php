<?php

namespace frontend\modules\tax\controllers;

use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\TaxRobotHelper;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyType;
use common\models\document\PaymentOrder;
use common\models\document\status\TaxDeclarationStatus;
use common\models\ICompanyStrictQuery;
use common\models\service\Payment;
use common\models\service\PaymentType;
use common\models\service\TaxrobotPaymentOrder;
use frontend\components\FrontendController;
use frontend\components\StatisticPeriod;
use frontend\modules\cash\models\CashBankSearch;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\components\DocumentBaseController;
use frontend\modules\subscribe\forms\OnlinePaymentForm;
use frontend\modules\subscribe\models\OnlinePaymentLog;
use frontend\modules\subscribe\models\OnlinePaymentSaver;
use frontend\modules\tax\models\DeclarationOsnoHelper;
use frontend\modules\tax\models\TaxDeclaration;
use frontend\modules\tax\models\TaxDeclarationQuarter;
use frontend\modules\tax\models\TaxrobotPaymentForm;
use frontend\rbac\permissions\Employee;
use frontend\rbac\UserRole;
use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\widgets\ActiveForm;
use frontend\modules\documents\models\TaxDeclarationSearch;
use frontend\models\log\LogHelper;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;

/**
 * Tax declaration controller
 */
class DeclarationController extends DocumentBaseController
{
    public $layout = 'reporting-panel';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [];
    }

    /**
     * @param null $type
     * @return string
     */
    public function actionIndex($type = null)
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        if (!($company->company_type_id == CompanyType::TYPE_IP)) {
            return $this->redirect(['/tax/declaration-osno/index-list']);
        }
        $searchModel = new TaxDeclarationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'company' => $company,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        /** @var TaxDeclaration $model */
        $model = $this->loadModel($id);
        $declarationOsnoHelper = new DeclarationOsnoHelper();

        if ($model->isOsno)
            $this->layout = null;

        return $this->render('view', [
            'company' => $model->company,
            'model' => $model,
            'declarationOsnoHelper' => $declarationOsnoHelper,
        ]);

    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        /** @var TaxDeclaration $model */
        $model = new TaxDeclaration();
        $model->company_id = Yii::$app->user->identity->company->id;
        $model->taxpayer_sign = 2;
        $model->tax_year = date('Y');
        $model->document_date = ($model->tax_year + 1) . "-01-10";

        /** @var TaxDeclarationQuarter[] $quarters */
        $quarters = [];
        for ($number = 1; $number <= 4; $number++) {

            $m = new TaxDeclarationQuarter();
            $m->quarter_number = $number;
            $m->tax_rate = "6.0";
            $m->oktmo = $model->company->oktmo;
            $m->income_amount = "0.00";
            $m->outcome_amount = "0.00";
            $m->tax_amount = "0.00";
            $m->prepayment_tax_amount = "0.00";

            $quarters[] = $m;
        }

        // Save
        if (Yii::$app->request->post()) {

            $model->load(Yii::$app->request->post());
            $quarters = $model->fillQuartersFromPost($quarters);

            if ($model->validate() && $model->validateQuarters($quarters, $model->taxpayer_sign)) {

                $quarters = $model->calculateQuartersTax($quarters, $model->taxpayer_sign);

                $model->save();

                $model->saveQuarters($quarters);

                LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE);

                Yii::$app->session->setFlash('success', 'Налоговая декларация добавлена');

                return $this->redirect(['view', 'id' => $model->id]);
            }

        }

        return $this->render('create', [
            'model' => $model,
            'quarters' => $quarters
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id)
    {
        /** @var TaxDeclaration $model */
        $model = $this->loadModel($id);
        /** @var TaxDeclarationQuarter[] $quarters */
        $quarters = $model->taxDeclarationQuarters;

        // Save
        if (Yii::$app->request->post()) {

            $document_correction_number = $model->document_correction_number;

            $model->load(Yii::$app->request->post());
            $quarters = $model->fillQuartersFromPost($quarters);

            if ($model->validate() && $model->validateQuarters($quarters, $model->taxpayer_sign)) {

                $quarters = $model->calculateQuartersTax($quarters, $model->taxpayer_sign);

                if ($model->document_correction_number != $document_correction_number) {
                    $model->status_id = TaxDeclarationStatus::STATUS_CORRECTED;
                    $model->status_updated_at = time();
                    $model->status_author_id = Yii::$app->user->id;

                    LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);
                } else {

                    LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE);
                }

                $model->save();

                $model->saveQuarters($quarters);

                Yii::$app->session->setFlash('success', 'Декларация обновлена');

                return $this->redirect(['view', 'id' => $id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'quarters' => $model->taxDeclarationQuarters
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        if (!$model = $this->findModel($id))
            return $this->redirect(['index']);

        $model->delete();

        Yii::$app->session->setFlash('success', 'Декларация удалена');

        LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_DELETE);

        return $this->redirect(['index']);
    }

    public function loadModel($id = null, $ioType = null)
    {
        $strictByCompany = true;
        $modelClass = TaxDeclaration::className();

        $query = $modelClass::find();

        if ($strictByCompany && $query instanceof ICompanyStrictQuery) {
            if (!(Yii::$app->user->identity instanceof Employee)) {
                throw new ForbiddenHttpException();
            }

            $query->byCompany(Yii::$app->user->identity->company->id);
        }

        $query->andWhere([
            $modelClass::tableName() . '.id' => $id,
        ]);

        $model = $query->one();

        if ($model === null) {
            $email = \Yii::$app->params['emailList']['support'];
            throw new NotFoundHttpException(
                "Загрузка декларации не удалась. Попробуйте повторить попытку или обратитесь в службу технической поддержки <a href=\"mailto:$email\">$email</a></p>"
            );
        }

        return $model;
    }

    /**
     * @param $id
     * @return TaxDeclaration null|static
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = TaxDeclaration::findOne([
            'id' => $id,
            'company_id' => Yii::$app->user->identity->company->id
        ]);

        return $model;
    }

    /**
     * @param $actionType
     * @param null $id
     * @param null $type
     * @param null $filename
     * @return string|void
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionDocumentPrint($actionType, $id = null, $type = null, $filename = null)
    {
        /** @var TaxDeclaration $model */
        $model = $this->loadModel($id);

        if ($actionType == 'print') {
            $model->status_id = TaxDeclarationStatus::STATUS_PRINTED;
            $model->status_updated_at = time();
            $model->status_author_id = Yii::$app->user->id;
            $model->save(false, ['status_id', 'status_updated_at', 'status_author_id']);

            LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);
        }

        $model->updateDownloadedQuarterNum();

        // TEMP FOR END (Единая упращенная налоговая декларация)
        if (Yii::$app->request->get('end'))
            return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view-end', [
                'company' => $model->company,
            ]);

        // TEMP FOR NDS (по налогу на доб. стоимость)
        if (Yii::$app->request->get('nds'))
            return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view-nds', [
                'company' => $model->company,
            ]);

        // TEMP FOR ORGANIZATIONS (по налогу на прибыль организаций)
        if (Yii::$app->request->get('org'))
            return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view-org', [
                'company' => $model->company,
            ]);

        if (Yii::$app->request->get('balance')) {
            return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, '@frontend/modules/tax/views/balance/pdf-view.php', [
                'company' => $model->company,
            ]);
        }

        if (Yii::$app->request->get('szvm')) {
            return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, '@frontend/modules/tax/views/default/szvm.php', [
                'company' => $model->company,
            ]);
        }

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view', [
            'company' => $model->company,
        ]);
    }

    public function actionXml($id)
    {
        /** @var TaxDeclaration $model */
        $model = $this->loadModel($id);

        $company = $model->company;

        list($filename, $output) = array_values($model->getXmlToImport());

        // Is empty declaration
        if (Yii::$app->request->get('empty'))
            \common\models\company\CompanyFirstEvent::checkEvent($company, 90);
        else
            \common\models\company\CompanyFirstEvent::checkEvent($company, 93);

        $model->updateDownloadedQuarterNum();

        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/xml; charset=windows-1251');
        $headers->add('Content-disposition', 'attachment;filename="' . $filename . '.xml"');

        return Yii::$app->response->sendContentAsFile($output, $filename . '.xml', [
            'mimeType' => 'text/xml',
            'inline' => true,
        ]);

    }

    private function uuid()
    {
        $data = random_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    /**
     * @return mixed
     */
    public function actionViewRobot($id)
    {
        /** @var TaxDeclaration $model */
        $model = $this->loadModel($id);

        $empty = true;
        foreach ($model->taxDeclarationQuarters as $quarter) {
            if ($quarter->income_amount > 0) {
                $empty = false;
                break;
            }
        }

        return $this->redirect('/tax/robot/declaration' . ($empty ? '?empty=1' : ''));

    }

}
