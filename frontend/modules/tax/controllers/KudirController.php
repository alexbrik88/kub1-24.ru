<?php

namespace frontend\modules\tax\controllers;

use common\components\filters\AccessControl;
use common\components\pdf\PdfRenderer;
use common\components\pdf\Printable;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\components\DocumentBaseController;
use frontend\modules\documents\components\Message;
use frontend\rbac\UserRole;
use Yii;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\widgets\ActiveForm;
use frontend\models\log\LogHelper;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\modules\tax\models\Kudir;
use frontend\modules\documents\models\KudirSearch;
use common\models\employee\Employee;
use common\models\ICompanyStrictQuery;
use common\models\document\InvoiceExpenditureItem;

/**
 * Kudir controller
 */
class KudirController extends DocumentBaseController
{
    public $layout = 'reporting-panel';

    protected $_company;
    protected $_year;

    // Исключаемые статьи прихода
    protected $_excludeItems = [
        2, // Взнос наличными
        3, // Возврат
        4, // Займ
        5, // Кредит
        6, // От учредителя
        9, // Перевод собственных средств
        10, // Возврат займа
        13, // Возврат средств из бюджета
        14, // Обеспечительный платёж
    ];
    // Включаемые статьи расхода (ПФР, ОМС)
    protected $_expenditure_items = [
        28, // Страховые взносы за ИП
        46, // Фиксированный платеж в ОМС
        53, // Фиксированный платеж в ПФР
    ];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [];
    }

    /**
     * @param null $type
     * @return string
     */
    public function actionIndex($type = null)
    {
        $searchModel = new KudirSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionView($id)
    {
        /** @var Kudir $model */
        $model = $this->loadModel($id);
        $this->_company = Yii::$app->user->identity->company;
        $this->_year = $model->tax_year;

        $quarters = [
            1 => array_merge($this->getQuarterIncomeBankFlows(1), $this->getQuarterIncomeOrderFlows(1)),
            2 => array_merge($this->getQuarterIncomeBankFlows(2), $this->getQuarterIncomeOrderFlows(2)),
            3 => array_merge($this->getQuarterIncomeBankFlows(3), $this->getQuarterIncomeOrderFlows(3)),
            4 => array_merge($this->getQuarterIncomeBankFlows(4), $this->getQuarterIncomeOrderFlows(4)),
        ];

        return $this->render('view', [
            'model' => $model,
            'quarters' => $quarters
        ]);

    }

    public function getQuarterIncomeBankFlows($quarter) {

        $date = new \DateTime();
        $monthFrom = ($quarter - 1) * 3 + 1;
        $dateFrom = $date->setDate($this->_year, $monthFrom, 1);
        $dateTill = clone $dateFrom;
        $dateTill = $dateTill->modify('last day of + 2 month');

        $query = CashBankFlows::find()
            ->alias('cbf')
            ->joinWith('contractor')
            ->joinWith('invoices')
            ->andWhere(['cbf.company_id' => $this->_company->id, 'flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
            ->andWhere(['is_taxable' => true])
            ->andWhere(['not', ['income_item_id' => $this->_excludeItems]])
            ->andWhere(['between', 'date', $dateFrom->format('Y-m-d'), $dateTill->format('Y-m-d')]);

        return $query->all();
    }

    public function getQuarterOutcomeBankFlows($quarter) {

        $date = new \DateTime();
        $monthFrom = ($quarter - 1) * 3 + 1;
        $dateFrom = $date->setDate($this->_year, $monthFrom, 1);
        $dateTill = clone $dateFrom;
        $dateTill = $dateTill->modify('last day of + 2 month');

        $query = CashBankFlows::find()
            ->alias('cbf')
            ->joinWith('contractor')
            ->joinWith('invoices')
            ->andWhere([
                'cbf.company_id' => $this->_company->id,
                'flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE,
                'expenditure_item_id' => $this->_expenditure_items,
                'is_taxable' => true
                ])
            ->andWhere(['between', 'date', $dateFrom->format('Y-m-d'), $dateTill->format('Y-m-d')]);

        return $query->all();
    }

    public function getQuarterIncomeOrderFlows($quarter) {

        $date = new \DateTime();
        $monthFrom = ($quarter - 1) * 3 + 1;
        $dateFrom = $date->setDate($this->_year, $monthFrom, 1);
        $dateTill = clone $dateFrom;
        $dateTill = $dateTill->modify('last day of + 2 month');

        $query = CashOrderFlows::find()
            ->alias('cbf')
            ->andWhere([
                'cbf.company_id' => $this->_company->id,
                'flow_type' => CashFlowsBase::FLOW_TYPE_INCOME,
                'is_taxable' => true,
                'is_accounting' => true
            ])
            ->andWhere(['between', 'date', $dateFrom->format('Y-m-d'), $dateTill->format('Y-m-d')]);

        return $query->all();
    }

    public function getQuarterOutcomeOrderFlows($quarter) {

        $date = new \DateTime();
        $monthFrom = ($quarter - 1) * 3 + 1;
        $dateFrom = $date->setDate($this->_year, $monthFrom, 1);
        $dateTill = clone $dateFrom;
        $dateTill = $dateTill->modify('last day of + 2 month');

        $query = CashOrderFlows::find()
            ->alias('cbf')
            ->andWhere([
                'cbf.company_id' => $this->_company->id,
                'flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE,
                'is_taxable' => true,
                'is_accounting' => true
            ])
            ->andWhere(['between', 'date', $dateFrom->format('Y-m-d'), $dateTill->format('Y-m-d')]);

        return $query->all();
    }

    // TEMP
    public function actionDocumentPrint($actionType, $id = null, $type = null, $filename = null)
    {
        /** @var Kudir $model */
        $model = $this->loadModel($id);
        $this->_company = Yii::$app->user->identity->company;
        $this->_year = $model->tax_year;

        $lastQuarter = 4;
        if ($period  = \Yii::$app->request->get('period')) {
            $lastQuarter = (int)str_replace("{$model->tax_year}_", "", $period);
        }

        $quartersBankFlows = [
            1 => ($lastQuarter >= 1) ? array_merge($this->getQuarterIncomeBankFlows(1), $this->getQuarterIncomeOrderFlows(1)) : [],
            2 => ($lastQuarter >= 2) ? array_merge($this->getQuarterIncomeBankFlows(2), $this->getQuarterIncomeOrderFlows(2)) : [],
            3 => ($lastQuarter >= 3) ? array_merge($this->getQuarterIncomeBankFlows(3), $this->getQuarterIncomeOrderFlows(3)) : [],
            4 => ($lastQuarter >= 4) ? array_merge($this->getQuarterIncomeBankFlows(4), $this->getQuarterIncomeOrderFlows(4)) : [],
        ];

        $quartersBankFlowsOutcome = [
            1 => ($lastQuarter >= 1) ? array_merge($this->getQuarterOutcomeBankFlows(1), $this->getQuarterOutcomeOrderFlows(1)) : [],
            2 => ($lastQuarter >= 2) ? array_merge($this->getQuarterOutcomeBankFlows(2), $this->getQuarterOutcomeOrderFlows(2)) : [],
            3 => ($lastQuarter >= 3) ? array_merge($this->getQuarterOutcomeBankFlows(3), $this->getQuarterOutcomeOrderFlows(3)) : [],
            4 => ($lastQuarter >= 4) ? array_merge($this->getQuarterOutcomeBankFlows(4), $this->getQuarterOutcomeOrderFlows(4)) : [],
        ];

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view', [
            'viewL'=> 'partial/_pdf-view-6',
            'company' => $this->_company,
            'quartersBankFlows' => $quartersBankFlows,
            'quartersBankFlowsOutcome' => $quartersBankFlowsOutcome
        ]);
    }

    // TEMP
    public function actionDocumentPrintPatent($actionType, $id = null, $type = null, $filename = null)
    {
        /** @var Kudir $model */
        $model = $this->loadModel($id);
        $this->_company = Yii::$app->user->identity->company;
        $this->_year = $model->tax_year;

        $quartersBankFlows = [
            1 => array_merge($this->getQuarterIncomeBankFlows(1), $this->getQuarterIncomeOrderFlows(1)),
            2 => array_merge($this->getQuarterIncomeBankFlows(2), $this->getQuarterIncomeOrderFlows(2)),
            3 => array_merge($this->getQuarterIncomeBankFlows(3), $this->getQuarterIncomeOrderFlows(3)),
            4 => array_merge($this->getQuarterIncomeBankFlows(4), $this->getQuarterIncomeOrderFlows(4)),
        ];

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view-patent', [
            'company' => $model->company,
            'quartersBankFlows' => $quartersBankFlows
        ]);
    }

    // TEMP
    protected function _documentPrint($model, $asset, $actionType, $view, $params = [])
    {
        if (!($model instanceof Printable)) {
            throw new Exception('Model must be instance of Printable.');
        }

        /** @var AbstractDocument $model */
        $renderer = new PdfRenderer([
            'view' => $view,
            'params' => array_merge([
                'model' => $model,
                'message' => null,
                'viewL' => ArrayHelper::getValue($params, 'viewL'),
            ], $params),

            'destination' => PdfRenderer::DESTINATION_STRING,
            'filename' => $this->getPdfFileName($model),
            'displayMode' => ArrayHelper::getValue($params, 'displayMode', PdfRenderer::DISPLAY_MODE_FULLPAGE),
            'format' => ArrayHelper::getValue($params, 'documentFormat', 'A4-P'),
        ]);

        $this->view->params['asset'] = $asset;
        switch ($actionType) {
            case 'pdf':
                return $renderer->outputTwoOrientations();
            case 'print':
            default:
                if ($this->action->id != 'out-view') {
                    Yii::$app->view->registerJs('window.print();');
                }

                return $renderer->renderHtmlTwoOrientations(ArrayHelper::getValue($params, 'viewL'));
        }
    }

    // TEMP
    protected function getPdfFileName($model)
    {
        return 'КУДиР_за_' . $model->tax_year . '.pdf';
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        /** @var Kudir $model */
        $model = new Kudir();
        $model->company_id = Yii::$app->user->identity->company->id;
        $model->tax_year = date('Y');
        $model->document_date = date('d.m.Y');

        // Save
        if (Yii::$app->request->post()) {

            $model->load(Yii::$app->request->post());

            if ($model->validate()) {

                $model->save();

                LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE);

                Yii::$app->session->setFlash('success', 'Книга добавлена');

                return $this->redirect(['view', 'id' => $model->id]);
            }

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id)
    {
        /** @var Kudir $model */
        $model = $this->loadModel($id);

        // Save
        if (Yii::$app->request->post()) {

            $model->load(Yii::$app->request->post());

            if ($model->validate()) {

                LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE);

                $model->save();

                Yii::$app->session->setFlash('success', 'Книга обновлена');

                return $this->redirect(['view', 'id' => $id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        if (!$model = $this->findModel($id))
            return $this->redirect(['index']);

        $model->delete();

        Yii::$app->session->setFlash('success', 'Книга удалена');

        LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_DELETE);

        return $this->redirect(['index']);
    }

    public function loadModel($id = null, $ioType = null)
    {
        $modelClass = Kudir::className();
        $query = $modelClass::find();
        if (!(Yii::$app->user->identity instanceof Employee)) {
            throw new ForbiddenHttpException();
        }
        $query->andWhere([
            $modelClass::tableName() . '.id' => $id,
            $modelClass::tableName() . '.company_id' => Yii::$app->user->identity->company->id,
        ]);

        $model = $query->one();

        if ($model === null) {
            $email = \Yii::$app->params['emailList']['support'];
            throw new NotFoundHttpException(
                "Загрузка не удалась. Попробуйте повторить попытку или обратитесь в службу технической поддержки <a href=\"mailto:$email\">$email</a></p>"
            );
        }

        return $model;
    }

    /**
     * @param $id
     * @return Kudir null|static
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = Kudir::findOne([
            'id' => $id,
            'company_id' => Yii::$app->user->identity->company->id
        ]);

        return $model;
    }
}
