<?php

use common\components\date\DateHelper;
use common\models\Agreement;
use common\widgets\Modal;
use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\company\CompanyType;

/* @var $this yii\web\View
 * @var $model frontend\modules\tax\models\TaxDeclaration
 * @var $isIp boolean
 * @var $declarationOsnoHelper \frontend\modules\tax\models\DeclarationOsnoHelper
 */

$changeStatusDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->created_at,
]);

$status = $model->status;
$styleClass = $status->getStyleClass();
$iconClass = $status->getIcon();

$company = $model->company;
$ifns = $company->ifns;
?>

<div class="control-panel col-xs-12 pad0">
    <div class="col-xs-12 col-sm-3 pad3">
        <div class="btn full_w marg <?= $styleClass; ?>"
             style="padding-left:0px; padding-right:0px;text-align: center;"
             title="Дата изменения статуса">
            <?= date(DateHelper::FORMAT_USER_DATE, $model->status_updated_at ?: $model->created_at) ?>
        </div>
    </div>
    <div class="col-xs-12 col-sm-9 pad0">
        <div class="col-xs-12 pad3">
            <div class="btn full_w marg <?= $styleClass; ?>" title="Статус">
                <i class="pull-left icon <?= $iconClass; ?>"></i>
                <?= $model->status->name ?>
            </div>
        </div>
    </div>
</div>

<style>
    .main_inf_no-bord td {
        border: none !important;
    }
</style>
<div class="col-xs-12 col-lg-9 pull-right pad3" style="max-width: 720px;">
    <div class="portlet">
        <div class="customer-info bord-dark" style="margin:10px 0">
            <div class="portlet-body no_mrg_bottom main_inf_no-bord" style="padding-bottom:2px">
                <table class="table no_mrg_bottom">
                    <tbody>
                    <tr>
                        <td>
                            <span class="customer-characteristic">
                                <?php if ($isIp): ?>
                                    Налоговая декларация за
                                <?php elseif ($model->nds): ?>
                                    Декларация на добавленную стоимость за
                                <?php elseif ($model->org): ?>
                                    Декларация на прибыль организации за
                                <?php elseif ($model->balance): ?>
                                    Бухгалтерский баланс за
                                <?php elseif ($model->szvm): ?>
                                    Сведения о застрахованных лицах за
                                <?php else: ?>
                                    Единая (упрощенная) налоговая декларация ОСНО за
                                <?php endif; ?>
                            </span>
                            <?php if ($isIp): ?>
                                <?= $model->tax_year ?> год
                            <?php else: ?>
                                <?= date('Y') == $model->tax_year ? $declarationOsnoHelper->period->label : "{$model->tax_year} год"; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <?php if ($isIp): ?>
                        <tr>
                            <td>
                                <span class="customer-characteristic">Сумма налогов за год:</span>
                                <?= \common\components\TextHelper::invoiceMoneyFormat($model->taxDeclarationQuarters[3]['tax_rate'] * $model->taxDeclarationQuarters[3]['income_amount']) ?>
                                руб.
                            </td>
                        </tr>
                    <?php endif; ?>
                    <tr>
                        <td>
                            <div class="customer-characteristic" style="display:inline-block;vertical-align: top;">
                                Дата декларации:
                            </div>
                            <div id="date_internal_view" style="display:inline-block">
                                <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="customer-characteristic">В инспекцию:</span>
                            <?= $company->ifns_ga . ', ' . $ifns->gb ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="customer-characteristic">Адрес инспекции:</span>
                            <?= $ifns->g1 ?>
                        </td>
                    </tr>
                    <?php if ($isIp): ?>
                        <tr>
                            <td>
                                <span class="customer-characteristic">Ответственный:</span>
                                <?= \common\models\employee\Employee::findOne(Yii::$app->user->id)->getShortFio() ?>
                            </td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php if (!$model->nds): ?>
        <div class="portlet" style="margin-top: -10px">
            <div class="customer-info" style="border:none">
                <div class=" no_mrg_bottom main_inf_no-bord">
                    <table class="table no_mrg_bottom">
                        <tr>
                            <td>
                                <span class="customer-characteristic">Способы сдать декларацию УСН</span><br/><br/>
                                <span class="customer-characteristic">1. Отправить по почте</span><br/>
                                Для этого <a target="_blank"
                                             href="/tax/<?= $isIp ? 'robot' : 'declaration-osno'; ?>/declaration-print/?actionType=pdf&id=<?= ($model->id) ?>&filename=<?= ($model->getPrintTitle() . '.pdf') ?>&end=<?= !$isIp; ?>">распечатайте
                                    декларацию</a> в 1-м экземпляре
                                и <a target="_blank"
                                     href="/tax/<?= $isIp ? 'robot' : 'declaration-osno'; ?>/inventory-print/?actionType=pdf&id=<?= ($model->id) ?>&filename=<?= ('Опись_' . $model->getPrintTitle() . '.pdf') ?>">опись
                                    вложения</a> в 2-х экземплярах, подпишите их,
                                поставьте на титульном листе декларации
                                печать (если она у вас есть).
                                Отправьте отчёт ценным письмом с описью вложения
                                на адрес инспекции.
                                У вас на руках должен остаться экземпляр описи с отметкой
                                работника почты, который подтвердит сдачу отчёта.
                                <br/><br/>
                                <span class="customer-characteristic">2. Отправить через электронную систему сдачи отчетности</span><br/>
                                Для этого <?php if ($isIp): ?><a download href="/tax/declaration/xml/?id=<?= ($model->id) ?>">скачайте
                                    декларацию</a><?php else: ?>скачайте декларацию<?php endif; ?> в формате для электронной отчетности, загрузите её в вашу систему и
                                подпишите вашей ЭЦП.
                                <br/><br/>
                                <span class="customer-characteristic">3. Отнести отчёт в инспекцию самостоятельно</span><br/>
                                Для этого <a target="_blank"
                                             href="/tax/<?= $isIp ? 'robot' : 'declaration-osno'; ?>/declaration-print/?actionType=pdf&id=<?= ($model->id) ?>&filename=<?= ($model->getPrintTitle() . '.pdf') ?>&end=<?= !$isIp; ?>">распечатайте
                                    декларацию</a> в 2-х экземплярах,
                                подпишите каждый, на титульном листе поставьте
                                печать (если она у вас есть), и отнесите в инспекцию.
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>