<?php
use frontend\modules\tax\models\TaxDeclaration;
use common\components\date\DateHelper;
use yii\helpers\Html;

/** @var TaxDeclaration $model */

if (!function_exists('printOrEmpty')) {
    function printOrEmpty($value) {
        return ($value > 0) ? (int)$value : str_repeat("&mdash;", 10);
    }
}
?>

<table class="table-preview no-border">
    <tr>
        <td width="25%" rowspan="2" style="vertical-align:top;">
            <?= Html::img($model->getUsnBarcode(3), ['width'=>'165px', 'class'=>'barcode']);?>
        </td>
        <td class="font-size-11 text-right">ИНН</td>
        <td colspan="3" class="font-main"><?= $model->company->inn ?></td>
        <td rowspan="2"></td>
    </tr>
    <tr>
        <!--<td width="25%"></td>-->
        <td width="40px" class="font-size-11 text-right">КПП</td>
        <td width="18%" class="font-main"><?= str_repeat("&mdash;", 5) ?></td>
        <td width="6%" class="font-size-11 text-right">Стр.</td>
        <td width="9%" class="font-main">003</td>
        <!--<td></td>-->
    </tr>
</table>
<br/>
<br/>
<table class="table-preview no-border">
    <tr>
        <td class="font-size-11 font-bold text-center">
            Раздел 2.1.1. Расчет налога, уплачиваемого в связи с применением упрощенной системы
            налогообложения (объект налогообложения - доходы)
        </td>
    </tr>
</table>
<br/>
<br/>

<table class="table-preview no-border">
    <tr>
        <td width="60%" class="small-italic">Показатели</td>
        <td width="8%" class="small-italic">Код строки</td>
        <td class="small-italic">Значения показателей (в рублях)</td>
    </tr>
    <tr style="padding-bottom:20px;">
        <td class="small-italic">1</td>
        <td class="small-italic">2</td>
        <td class="small-italic">3</td>
    </tr>

    <tr class="middle">
        <td class="font-bold">Код признака применения налоговой ставки:</td>
        <td class="text-center"></td>
        <td class="font-main"></td>
    </tr>
    <tr class="middle">
        <td style="font-size:7pt;">
            1 - налоговая ставка в размере 6 %, установленная пунктом 1 статьи 346.20 Налогового
            кодекса Российской Федерации, или налоговая ставка, установленная законом субъекта
            Российской Федерации, применяется в течение налогового периода;<br/>
            2 - налоговая ставка в размере 8 %, установленная пунктом 1.1 статьи 346.20 Налогового
            кодекса Российской Федерации, применяется начиная с квартала, по итогам которого
            доходы превысили 150 млн. рублей, но не превысили 200 млн. рублей и (или) в течение
            которого средняя численность работников превысила 100 человек, но не превысила 130
            человек
        </td>
        <td class="text-center">101</td>
        <td class="font-main text-right"><?= $model->company->companyTaxationType->getUsnDeclarationTaxRateIndicator() ?></td>
    </tr>

    <tr class="middle">
        <td class="font-bold">Признак налогоплательщика:</td>
        <td class="text-center"></td>
        <td class="font-main"></td>
    </tr>
    <tr class="middle">
        <td style="font-size:7pt;">
            1 - налогоплательщик, производящий выплаты и иные вознаграждения<br/> физическим лицам;<br/>
            2 - индивидуальный предприниматель, не производящий выплаты и<br/> иные вознаграждения физическим лицам
        </td>
        <td class="text-center">102</td>
        <td class="font-main text-right"><?= $model->taxpayer_sign ?></td>
    </tr>

    <tr class="middle">
        <td class="font-bold">
            Сумма полученных доходов (налоговая база для исчисления налога
            (авансового платежа по налогу)) нарастающим итогом:
        </td>
        <td class="text-center"></td>
        <td class="font-main"></td>
    </tr>
    <tr class="middle">
        <td>за первый квартал</td>
        <td class="text-center">110</td>
        <td class="font-main text-right"><?= printOrEmpty($quarters[0]->income_amount) ?></td>
    </tr>
    <tr class="middle">
        <td>за полугодие</td>
        <td class="text-center">111</td>
        <td class="font-main text-right"><?= printOrEmpty($quarters[1]->income_amount) ?></td>
    </tr>
    <tr class="middle">
        <td>за девять месяцев</td>
        <td class="text-center">112</td>
        <td class="font-main text-right"><?= printOrEmpty($quarters[2]->income_amount) ?></td>
    </tr>
    <tr class="middle">
        <td>за налоговый период</td>
        <td class="text-center">113</td>
        <td class="font-main text-right"><?= printOrEmpty($quarters[3]->income_amount) ?></td>
    </tr>

    <tr class="middle">
        <td class="font-bold">Ставка налога (%):</td>
        <td class="text-center"></td>
        <td class="font-main"></td>
    </tr>
    <tr class="middle">
        <td>за первый квартал</td>
        <td class="text-center">120</td>
        <td class="font-main"><?= number_format($quarters[0]->tax_rate,1) ?></td>
    </tr>
    <tr class="middle">
        <td>за полугодие</td>
        <td class="text-center">121</td>
        <td class="font-main"><?= number_format($quarters[1]->tax_rate, 1) ?></td>
    </tr>
    <tr class="middle">
        <td>за девять месяцев</td>
        <td class="text-center">122</td>
        <td class="font-main"><?= number_format($quarters[2]->tax_rate, 1) ?></td>
    </tr>
    <tr class="middle">
        <td>за налоговый период</td>
        <td class="text-center">123</td>
        <td class="font-main"><?= number_format($quarters[3]->tax_rate, 1) ?></td>
    </tr>

    <tr class="middle">
        <td class="font-bold">Сумма исчисленного налога (авансового платежа по налогу):</td>
        <td class="text-center"></td>
        <td class="font-main"></td>
    </tr>
    <tr class="middle">
        <td>за первый квартал <br/> (стр. 110 х стр. 120 / 100)</td>
        <td class="text-center">130</td>
        <td class="font-main text-right"><?= printOrEmpty($quarters[0]->prepayment_tax_amount) ?></td>
    </tr>
    <tr class="middle">
        <td>за полугодие <br/> (стр. 111 х стр. 121 / 100)</td>
        <td class="text-center">131</td>
        <td class="font-main text-right"><?= printOrEmpty($quarters[1]->prepayment_tax_amount) ?></td>
    </tr>
    <tr class="middle">
        <td>за девять месяцев <br/> (стр. 112 х стр. 122 / 100)</td>
        <td class="text-center">132</td>
        <td class="font-main text-right"><?= printOrEmpty($quarters[2]->prepayment_tax_amount) ?></td>
    </tr>
    <tr class="middle">
        <td>за налоговый период <br/> (стр. 113 х стр. 123 / 100)</td>
        <td class="text-center">133</td>
        <td class="font-main text-right"><?= printOrEmpty($quarters[3]->prepayment_tax_amount) ?></td>
    </tr>

    <tr class="middle">
        <td class="font-bold" style="text-align:justify;">Сумма страховых взносов, выплаченных работникам пособий по временной нетрудоспособности и платежей (взносов) по договорам добровольного личного страхования (нарастающим итогом), предусмотренных пунктом 3.1 статьи 346.21 Налогового кодекса Российской Федерации, уменьшающая сумму исчисленного за налоговый (отчетный) период налога (авансового платежа по налогу):</td>
        <td class="text-center"></td>
        <td class="font-main"></td>
    </tr>
    <tr class="middle">
        <td>за первый квартал<br/>для стр. 102 = «1»: стр. 140 <= стр. 130 / 2<br/>для стр. 102 = «2»: стр. 140 <= стр. 130</td>
        <td class="text-center">140</td>
        <td class="font-main text-right"><?= printOrEmpty($quarters[0]->outcome_amount) ?></td>
    </tr>
    <tr class="middle">
        <td>за полугодие<br/>для стр. 102 = «1»: стр. 141 <= стр. 131 / 2<br/>для стр. 102 = «2»: стр. 141 <= стр. 131</td>
        <td class="text-center">141</td>
        <td class="font-main text-right"><?= printOrEmpty($quarters[1]->outcome_amount) ?></td>
    </tr>
    <tr class="middle">
        <td>за девять месяцев<br/>для стр. 102 = «1»: стр. 142 <= стр. 132 / 2<br/>для стр. 102 = «2»: стр. 142 <= стр. 132</td>
        <td class="text-center">142</td>
        <td class="font-main text-right"><?= printOrEmpty($quarters[2]->outcome_amount) ?></td>
    </tr>
    <tr class="middle">
        <td>за налоговый период<br/>для стр. 102 = «1»: стр. 143 <= стр. 133 / 2<br/>для стр. 102 = «2»: стр. 143 <= стр. 133</td>
        <td class="text-center">143</td>
        <td class="font-main text-right"><?= printOrEmpty($quarters[3]->outcome_amount) ?></td>
    </tr>
</table>
