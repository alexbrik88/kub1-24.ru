<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use common\components\date\DateHelper;
use common\models\AgreementTemplate;
use dosamigos\tinymce\TinyMce;
use yii\helpers\Url;

/* @var $form yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */
/* @var $model frontend\modules\tax\models\TaxDeclaration */

$textAreaStyle = 'width: 100%; font-size:14px; text-align:justify; white-space: pre-line;';

$company = Yii::$app->user->identity->company;
$company_id = $company->id;

$isNewRecord = $model->isNewRecord;

$tax_years_list = [];
$date_max = date('Y') + 1;
$date_min = $model->company->ip_certificate_date ? date('Y', strtotime($model->company->ip_certificate_date)) : 2010;
if ($date_max > $date_min) {
    for ($i = $date_max; $i >= $date_min; $i--)
        $tax_years_list[$i] = $i;
} else {
    $tax_years_list[date('Y') + 1] = [date('Y') + 1];
    $tax_years_list[date('Y')] = [date('Y')];
}
?>

    <style>
        .tax-declaration-form .control-label {
            font-weight: bold;
        }
        .tax-declaration-form .form-column-max-width {
            max-width: 750px;
        }
        .tax-declaration-form .label-max-width {
            max-width: 350px;
            padding-right: 0;
        }
        .tax-declaration-form .label-horizontal label {
            padding-bottom:5px;
        }
        .tax-declaration-form #tax_year {
            display: inline-block;
            width: 80px;
            margin-left: 5px;
            margin-right: 5px;
            height: 100%;
            vertical-align: middle;
        }
        .tax-declaration-form input,
        .tax-declaration-form select {margin-left:0;}
    </style>

    <?= $form->errorSummary($model); ?>

    <?php foreach ($quarters as $quarter) {
        echo $form->errorSummary($quarter);
    } ?>

    <div class="row tax-declaration-form">
        <div class="col-sm-12">
            <div class="portlet">

                <div class="portlet-title">
                    <div class="caption" style="width: 100%">
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td valign="middle" style="width:1%; white-space:nowrap;">
                                <span>
                                    <?= Html::hiddenInput('', (int)$model->isNewRecord, [
                                        'id' => 'isNewRecord',
                                    ]) ?>

                                    <strong>Налоговая декларация за </strong>

                                    <?= Html::dropDownList('TaxDeclaration[tax_year]', $model->tax_year, $tax_years_list, [
                                        'id' => 'tax_year',
                                        'class' => 'form-control',
                                    ]); ?>

                                    <?php /*
                                    <?= Html::activeTextInput($model, 'tax_year', [
                                        'id' => 'tax_year',
                                        'data-required' => 1,
                                        'class' => 'form-control',
                                        'value' => ($model->isNewRecord) ? ($model->getMaxTaxYear() + 1) : $model->tax_year
                                    ]); ?> */ ?>

                                    <strong> год</strong>
                                    <br class="box-br">
                                </span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="portlet-body">
                    <div class="col-lg-12">

                        <div class="form-group row">
                            <div class="col-sm-5 label-max-width">
                                <label class="control-label">
                                    Форма КНД<span class="required" aria-required="true">*</span>:
                                </label>
                            </div>
                            <div class="col-sm-7">
                                <?= Html::activeTextInput($model, 'document_knd', [
                                    'maxlength' => true,
                                    'data-required' => 1,
                                    'readonly' => true,
                                    'class' => 'form-control',
                                    'style' => 'width:130px',
                                    'value' => '1152017'
                                ]); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-5 label-max-width">
                                <label class="control-label">
                                    Налоговый период (код)<span class="required" aria-required="true">*</span>:
                                </label>
                            </div>
                            <div class="col-sm-7">
                                <?= Html::activeTextInput($model, 'tax_period', [
                                    'maxlength' => true,
                                    'data-required' => 1,
                                    'readonly' => true,
                                    'class' => 'form-control',
                                    'style' => 'width:60px',
                                    'placeholder' => 'код',
                                    'value' => '34',
                                ]); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-5 label-max-width">
                                <label class="control-label">
                                    Номер корректировки<span class="required" aria-required="true">*</span>:
                                </label>
                            </div>
                            <div class="col-sm-7">
                                <?= Html::activeTextInput($model, 'document_correction_number', [
                                    'class' => 'form-control',
                                    'data-required' => 1,
                                    'style' => 'width:60px',
                                    'value' => ($model->isNewRecord) ? "0" : $model->document_correction_number
                                ]); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-5 label-max-width">
                                <label class="control-label">
                                    Предоставляется в налоговый орган (код)<span class="required" aria-required="true">*</span>:
                                </label>
                            </div>
                            <div class="col-sm-7">
                                <?= Html::activeTextInput($model, 'tax_service_ifns', [
                                    'maxlength' => true,
                                    'data-required' => 1,
                                    'class' => 'form-control',
                                    'style' => 'width:130px',
                                    'placeholder' => 'код',
                                    'value' => ($model->isNewRecord) ? $model->company->ifns_ga : $model->tax_service_ifns
                                ]); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-5 label-max-width">
                                <label class="control-label">
                                    По месту нахождения (учета) (код)<span class="required" aria-required="true">*</span>:
                                </label>
                            </div>
                            <div class="col-sm-7">
                                <?= Html::activeTextInput($model, 'tax_service_location', [
                                    'maxlength' => true,
                                    'data-required' => 1,
                                    'class' => 'form-control',
                                    'style' => 'width:130px',
                                    'placeholder' => 'код',
                                    'value' => 120
                                ]); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-5 label-max-width">
                                <label class="control-label">
                                    Признак налогоплательщика<span class="required" aria-required="true">*</span>:
                                </label>
                            </div>
                            <div class="col-sm-7">
                                <?= Html::dropDownList('TaxDeclaration[taxpayer_sign]', $model->taxpayer_sign, [
                                    '1' => '1 - производящий выплаты физ. лицам',
                                    '2' => '2 - не производящий выплаты физ. лицам'], [
                                        'id' => 'taxpayer_sign',
                                        'class' => 'form-control',
                                        'style' => 'max-width:350px',
                                ]); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-5 label-max-width">
                                <label class="control-label">
                                    Дата подготовки (для налоговой)<span class="required" aria-required="true">*</span>:
                                </label>
                            </div>
                            <div class="col-sm-7">
                                <table>
                                    <tr>
                                        <td>
                                            <div class="input-icon">
                                                <i class="fa fa-calendar"></i>
                                                <?= Html::activeTextInput($model, 'document_date', [
                                                    'id' => 'document_date',
                                                    'class' => 'form-control date-picker',
                                                    'style' => 'width:130px',
                                                    'data-date-viewmode' => 'years',
                                                    'value' => DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                                                ]); ?>
                                            </div>
                                        </td>
                                        <td style="padding-left: 10px">
                                            <span style="font-style: italic; color: #999;">Дата должна быть позже 31 декабря года, за который подается декларация</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <div class="col-sm-12" style="margin-top:3px;">

            <div class="table-container" style="overflow-x:scroll;">
                <div class="dataTables_wrapper dataTables_extended_wrapper">

                <table class="table table-striped table-bordered table-hover customers_table tax_calculate_table">
                    <thead>
                    <tr class="heading" role="row">
                        <th width="15%" class="text-center">Период</th>
                        <th width="15%" class="text-center">Код по ОКТМО</th>
                        <th width="10%" class="text-center">Ставка налога (%)</th>
                        <th width="15%" class="text-center">Доход за период нарастающим итогом</th>
                        <th width="15%" class="text-center">Исчисленный авансовый платеж (налог)</th>
                        <th width="15%" class="text-center">Уплаченные страховые взносы</th>
                        <th width="15%" class="text-center">Рассчитанный авансовый платеж (налог)</th>
                    </tr>
                    </thead>
                    <tbody id="tbody">
                        <?php
                            $_PERIODS = [
                                1 => '1 квартал',
                                2 => 'Полугодие',
                                3 => '9 месяцев',
                                4 => 'Налоговый период (год)'
                            ];

                        /** @var \frontend\modules\tax\models\TaxDeclarationQuarter $quarter */
                        foreach ($quarters as $quarter): $num = $quarter->quarter_number; ?>
                            <tr>
                                <td>
                                    <?= $_PERIODS[$num] ?>
                                </td>
                                <td class="col-oktmo">
                                    <?= Html::textInput("oktmo[{$num}]", $quarter->oktmo, [
                                        'maxlength' => 11,
                                        'data-required' => 1,
                                        'class' => 'form-control oktmo'
                                    ]); ?>
                                </td>
                                <td class="col-tax_rate">
                                    <?= Html::textInput("tax_rate[{$num}]", $quarter->tax_rate, [
                                        'maxlength' => true,
                                        'data-required' => 1,
                                        'class' => 'form-control tax_rate',
                                        'onchange' => 'tax_calculate()'
                                    ]); ?>
                                </td>
                                <td>
                                    <?= Html::textInput("income_amount[{$num}]", $quarter->income_amount, [
                                        'maxlength' => true,
                                        'data-required' => 1,
                                        'class' => 'form-control income_amount',
                                        'onchange' => 'tax_calculate()'
                                    ]); ?>
                                </td>
                                <td>
                                    <?= Html::textInput("prepayment_tax_amount[{$num}]", $quarter->prepayment_tax_amount, [
                                        'maxlength' => true,
                                        'data-required' => 1,
                                        'class' => 'form-control prepayment_tax_amount',
                                        'onchange' => 'tax_calculate()',
                                    ]); ?>
                                </td>
                                <td>
                                    <?= Html::textInput("outcome_amount[{$num}]", $quarter->outcome_amount, [
                                        'maxlength' => true,
                                        'data-required' => 1,
                                        'class' => 'form-control outcome_amount',
                                        'onchange' => 'tax_calculate()'
                                    ]); ?>
                                </td>
                                <td class="text-center" style="font-size:14px;">
                                    <strong class="tax_amount">
                                        <?= $quarter->tax_amount ?>
                                    </strong>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

                </div>
            </div>

        </div>

    </div>



<?php
$this->registerJs(<<<JS

tax_calculate = function() { 
    var table = $('#tbody');            
    var taxpayer_sign = $('#taxpayer_sign').val();
    var previous_tax_amount = 0;
    
    $(table).find('tr').each(function() {
       
       var income_amount = $(this).find('input.income_amount').val();
       var outcome_amount = $(this).find('input.outcome_amount').val();
       var prepayment_tax_amount = $(this).find('input.prepayment_tax_amount').val();
       var tax_rate = $(this).find('input.tax_rate').val();
       var tax_amount;
       
       if (isNaN(income_amount) || isNaN(outcome_amount) || isNaN(tax_rate)) {
           $(this).find('.tax_amount').html('<span style="color:red">Ошибка!</span>');
       } else {
           
           if (isNaN(prepayment_tax_amount) || prepayment_tax_amount == '') {
               prepayment_tax_amount = Math.round(tax_rate * income_amount / 100);
               $(this).find('input.prepayment_tax_amount').val(prepayment_tax_amount);
           }

           tax_amount = Math.round(tax_rate * income_amount / 100) - outcome_amount - previous_tax_amount;
           
           if ((taxpayer_sign == 2 && tax_amount < 0))
               tax_amount = 0;
          if ((taxpayer_sign == 1 && tax_amount < prepayment_tax_amount/2))
               tax_amount = Math.round(prepayment_tax_amount/2);

          $(this).find('.tax_amount').html(Math.round(tax_amount));
          
         previous_tax_amount = tax_amount;
       }
       
    });
}

$(document).ready(function() {
   tax_calculate();
   $('#taxpayer_sign').change(function() {
       tax_calculate();
   });
   $('#tax_year').change(function() {
       var next_year = parseInt($(this).val()) + 1;
       $('#document_date').datepicker("setDate", "10-01-" + next_year); 
   });
});

JS
);

