<?php
use himiklab\thumbnail\EasyThumbnailImage;
use yii\helpers\Html;
use common\components\date\DateHelper;

$formattedDate = DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
if (!function_exists('getEmptyDottedBoxes')) {
    function getEmptyDottedBoxes($count)
    {
        $str = '';
        for ($i = 0; $i < $count; $i++) {
            $str .= '<td class="dotbox">&nbsp;&nbsp;</td>';
        }

        return $str;
    }
}

/** @var \frontend\modules\tax\models\TaxDeclaration $model */
?>

<style media="print">
    @page {
        size: auto;
        margin: 0;
    }
    .page-break  {
        page-break-after: always;
    }
</style>

<style>
    .table {margin:0; padding:0;}
    .table td {font-size:8pt;vertical-align: bottom;padding-bottom:2px;}
    .table td.font-size-11 {font-size:10pt;}
    .table td.font-main {font-size:15pt;padding-bottom:0;}
    .table td.border-bottom {border-bottom:1px solid #000;}
    .table td.tip {padding:0 0 3px 0;}
    .table td.ver-top {vertical-align:top;}
    .table td.dotbox {width:16px; font-size:16pt; border:1px dotted #333;text-align:center;padding:0 2px;}
    .table td.hh {line-height: 20pt;}
    .table td.small-italic {font-style:italic; font-size:8pt;text-align:center;}
    .table tr.middle td {vertical-align:middle;padding:3px 0;}
    .table td.pad-r-5 {padding-right: 4pt}
    .table td.ver-mid {vertical-align:middle;}
    .table-preview td.text-center {text-align: center;}
    .table-preview td.text-right {text-align: right;}
    <?php if (@$_GET['red']) { ?>.table td {border:1px solid red} <?php } ?>
</style>

<div class="page-content-in p-center pad-pdf-p page-break">
    <table class="table no-border">
        <tr>
            <td width="25%" rowspan="2" style="vertical-align:top;">
                <?= Html::img($model->getUsnBarcode(1), ['width'=>'165px', 'class'=>'barcode']);?>
            </td>
            <td class="font-size-11 text-right">ИНН</td>
            <td colspan="3" class="font-main"><?= $model->company->inn ?></td>
            <td rowspan="2"></td>
        </tr>
        <tr>
            <!--<td width="25%"></td>-->
            <td width="40px" class="font-size-11 text-right">КПП</td>
            <td width="18%" class="font-main"><?= str_repeat("&mdash;", 5) ?></td>
            <td width="6%" class="font-size-11 text-right">Стр.</td>
            <td width="9%" class="font-main">001</td>
            <!--<td></td>-->
        </tr>
    </table>
    <table class="table no-border">
        <tr>
            <td class="text-right font-bold">Форма по КНД <?= $model->document_knd ?></td>
        </tr>
    </table>
    <table class="table no-border">
        <tr>
            <td class="font-size-11 font-bold text-center">
                Налоговая декларация по налогу, уплачиваемому<br/>
                в связи с применением упрощенной системы налогообложения
            </td>
        </tr>
    </table>
    <table class="table no-border">
        <tr>
            <td width="18%">Номер корректировки</td>
            <td class="font-main"><?= (int) $model->document_correction_number ?></td>
            <td width="38%" class="text-right">Налоговый период (код)</td>
            <td class="font-main"><?= $model->tax_period ?></td>
            <td width="26%" class="text-right">Отчетный год</td>
            <td class="font-main text-right"><?= $model->tax_year ?></td>
        </tr>
    </table>
    <table class="table no-border">
        <tr>
            <td width="35%">Представляется в налоговый орган (код)</td>
            <td class="font-main"><?= $model->company->ifns_ga ?></td>
            <td width="46%" class="text-right">по месту нахождения (учета) (код)</td>
            <td class="font-main text-right"><?= $model->tax_service_location ?></td>
        </tr>
    </table>
    <table class="table no-border" style="margin-top:5px;">
        <tr><td class="font-main"><?= $model->company->ip_lastname ?></td></tr>
        <tr><td class="font-main"><?= $model->company->ip_firstname ?></td></tr>
        <tr><td class="font-main"><?= $model->company->ip_patronymic ?></td></tr>
        <tr><td class="font-main border-bottom">&nbsp;</td></tr>
        <tr><td class="text-center tip">(налогоплательщик)</td></tr>
    </table>
    <table class="table no-border">
        <tr>
            <td width="20%">Форма реорганизации, ликвидация (код)</td>
            <td width="10%" class="font-main">&mdash;</td>
            <td width="25%">ИНН/КПП реорганизованной организации</td>
            <td class="font-main"><?= str_repeat("&mdash;", 7) ?></td>
            <td width="2%" class="font-main text-center">/</td>
            <td class="font-main"><?= str_repeat("&mdash;", 7) ?></td>
        </tr>
    </table>
    <table class="table no-border">
        <tr>
            <td width="25%">Номер контактного телефона</td>
            <td class="font-main"><?= $model->company->phone ?></td>
        </tr>
    </table>
    <table class="table no-border">
        <tr>
            <td width="5%">На</td>
            <td width="10%" class="font-main">3</td>
            <td width="10%">страницах</td>
            <td class="text-right">с приложением подтверждающих документов или их копий на</td>
            <?= getEmptyDottedBoxes(3) ?>
            <td>листах</td>
        </tr>
        <tr>
            <td colspan="8" class="border-bottom"></td>
        </tr>
    </table>
    <table class="table no-border">
        <tr>
            <!--left-->
            <td width="50%" style="border-right:1px solid #000;padding:5px;vertical-align: top;">
                <table class="table no-border">
                    <tr>
                        <td class="text-center font-bold">
                            Достоверность и полноту сведений, указанных<br/>
                            в настоящей декларации подтверждаю
                        </td>
                    </tr>
                </table>
                <table class="table no-border">
                    <tr>
                        <td width="20%"></td>
                        <td width="8%" class="text-center font-main ver-top">
                            <?= $model->declaration_maker_code ?>
                        </td>
                        <td class="m-l" style="padding:0;font-size:7pt">
                            1 - налогоплательщик<br/>2 - представитель налогоплательщика
                        </td>
                    </tr>
                </table>
                <table class="table no-border">
                    <tr><td class="border-bottom hh">&nbsp;<br/></td></tr>
                    <tr><td class="border-bottom hh">&nbsp;<br/></td></tr>
                    <tr><td class="border-bottom hh">&nbsp;<br/></td></tr>
                    <tr>
                        <td class="hh" style="vertical-align:top; text-align:center; font-size:7.5pt;">
                            (фамилия, имя, отчество * полностью)
                        </td>
                    </tr>
                    <tr><td class="border-bottom">&nbsp;</td></tr>
                    <tr><td class="border-bottom hh">&nbsp;</td></tr>
                    <tr><td class="border-bottom hh">&nbsp;</td></tr>
                    <tr><td class="border-bottom hh">&nbsp;</td></tr>
                    <tr><td class="border-bottom hh">&nbsp;</td></tr>
                    <tr><td class="border-bottom hh">&nbsp;</td></tr>
                    <tr><td class="border-bottom hh">&nbsp;</td></tr>
                    <tr><td class="border-bottom hh">&nbsp;</td></tr>
                    <tr>
                        <td style="vertical-align:top; text-align:center; font-size:7.5pt;">
                            (наименование организации - представителя налогоплательщика)
                        </td>
                    </tr>
                </table>
                <br/>
                <table class="table no-border">
                    <tr>
                        <td width="15%">Подпись</td>
                        <td width="30%" class="border-bottom"></td>
                        <td width="10%">Дата</td>
                        <td class="font-main"><?= $formattedDate ?></td>
                    </tr>
                </table>
                <br/>
                <table class="table no-border">
                    <tr>
                        <td class="text-center">Наименование документа<br/>подтверждающего полномочия представителя</td>
                    </tr>
                    <tr>
                        <td class="border-bottom">&nbsp;</td>
                    </tr>
                </table>

                <br/><br/>

                <table class="table no-border">
                    <tr>
                        <td>* отчество указывается при наличии</td>
                    </tr>
                </table>
            </td>
            <!--right-->
            <td width="50%" style="padding:5px;vertical-align: top;">
                <table class="table no-border">
                    <tr>
                        <td class="text-center font-bold" style="padding-top:18px;">
                            Заполняется работником налогового органа
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">Сведения о представлении декларации</td>
                    </tr>
                </table>
                <br/>
                <table class="table no-border">
                    <tr>
                        <td width="66%">Данная декларация представлена (код)</td>
                        <?= getEmptyDottedBoxes(2) ?>
                        <td></td>
                    </tr>
                </table>
                <br/>
                <table class="table no-border">
                    <tr>
                        <td width="8%">на</td>
                        <?= getEmptyDottedBoxes(3) ?>
                        <td style="padding-left: 10px;">страницах</td>
                    </tr>
                </table>
                <br/>
                <table class="table no-border">
                    <tr>
                        <td width="50%" class="text-right" style="padding-top:0;padding-bottom:0;padding-right:10px;">с приложением</td>
                        <td colspan="3"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right" style="padding-top:0;padding-bottom:0;padding-right:10px;">
                            подтверждающих документов<br/>или их копий на
                        </td>
                        <?= getEmptyDottedBoxes(3) ?>
                        <td>листах</td>
                    </tr>
                </table>
                <br/>
                <table class="table no-border">
                    <tr>
                        <td style="padding-bottom:0" width="36%">Дата представления декларации</td>
                        <?= getEmptyDottedBoxes(2) ?>
                        <td class="font-main text-center" width="10px">.</td>
                        <?= getEmptyDottedBoxes(2) ?>
                        <td class="font-main text-center"  width="10px">.</td>
                        <?= getEmptyDottedBoxes(4) ?>
                        <td></td>
                    </tr>
                </table>

                <br/><br/><br/>

                <table class="table no-border">
                    <tr>
                        <td width="31%" style="padding-bottom:0">
                            Зарегистрирована за №
                        </td>
                        <?= getEmptyDottedBoxes(13) ?>
                        <td></td>
                    </tr>
                </table>

                <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

                <table class="table no-border">
                    <tr>
                        <td width="10px"></td>
                        <td width="50%" class="border-bottom">&nbsp;</td>
                        <td width="10px"></td>
                        <td class="border-bottom"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="text-center">фамилия</td>
                        <td></td>
                        <td class="text-center">подпись</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>