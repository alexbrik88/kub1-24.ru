<?php

use frontend\rbac\permissions;
use yii\bootstrap\Html;
use frontend\widgets\ConfirmModalWidget;
use yii\helpers\Url;
use common\models\document\status\TaxDeclarationStatus;

/* @var $this yii\web\View
 * @var $model frontend\modules\tax\models\TaxDeclaration
 * @var $isIp boolean
 */

$pageRoute = ['/tax/declaration/view', 'id' => $model->id];

?>
<div class="row action-buttons margin-no-icon">
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (FALSE && $canUpdate): ?>
            <button class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs send-message-panel-trigger">
                Отправить
            </button>
            <button class="btn darkblue widthe-100 hidden-lg send-message-panel-trigger" title="Отправить">
                <span class="ico-Send-smart-pls fs"></span>
            </button>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php
        echo Html::a('Печать', [
            'document-print',
            'actionType' => 'print',
            'id' => $model->id,
            'filename' => $model->getPrintTitle(),
            'end' => !$isIp && !$model->nds && !$model->org && !$model->balance && !$model->szvm,
            'nds' => $model->nds,
            'org' => $model->org,
            'balance' => $model->balance,
            'szvm' => $model->szvm,
        ], [
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
            'target' => '_blank',
        ]);
        echo Html::a('<i class="fa fa-print fa-2x"></i>', [
            'document-print',
            'actionType' => 'print',
            'id' => $model->id,
            'filename' => $model->getPrintTitle(),
            'end' => !$isIp && !$model->nds && !$model->org && !$model->balance && !$model->szvm,
            'nds' => $model->nds,
            'org' => $model->org,
            'balance' => $model->balance,
            'szvm' => $model->szvm,
        ], [
            'class' => 'btn darkblue widthe-100 hidden-lg',
            'target' => '_blank',
            'title' => 'Печать',
        ]);
        ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <style>
            .dropdown-menu-mini {
                width: 100%;
                min-width: 200px;
                border-color: #4276a4 !important;
            }

            .dropdown-menu-mini a {
                padding: 7px 0px;
                text-align: center;
            }
        </style>
        <span class="dropup">
            <?= Html::a('Скачать', '#', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs dropdown-toggle',
                'data-toggle' => 'dropdown'
            ]); ?>
            <?= Html::a('<i class="glyphicon glyphicon-download" style="font-size: 17px;"></i>', '#', [
                'class' => 'btn darkblue widthe-100 hidden-lg dropdown-toggle',
                'data-toggle' => 'dropdown'
            ]); ?>
            <?= \yii\bootstrap\Dropdown::widget([
                'options' => [
                    'style' => '',
                    'class' => 'dropdown-menu-mini'
                ],
                'items' => [
                    [
                        'label' => 'Скачать PDF файл',
                        'encode' => false,
                        'url' => [
                            'document-print',
                            'actionType' => 'pdf',
                            'id' => $model->id,
                            'filename' => $model->getPdfFileName(),
                            'end' => !$isIp && !$model->nds && !$model->org && !$model->balance && !$model->szvm,
                            'nds' => $model->nds,
                            'org' => $model->org,
                            'balance' => $model->balance,
                            'szvm' => $model->szvm,
                        ],
                        'linkOptions' => [
                            'target' => '_blank',
                        ]
                    ],
                    [
                        'label' => 'Скачать файл для ИФНС',
                        'encode' => false,
                        'url' => ['xml', 'id' => $model->id],
                        'linkOptions' => [
                            'download' => 'download',
                        ]
                    ],
                ],
            ]); ?>
        </span>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (FALSE && Yii::$app->user->can(permissions\document\Document::CREATE)): ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => 'Копировать',
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ],
                'confirmUrl' => Url::to(['copy', 'id' => $model->id,]),
                'message' => 'Вы уверены, что хотите скопировать эту декларацию?',
            ]); ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => '<i class="fa fa-files-o fa-2x"></i>',
                    'title' => 'Копировать',
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                ],
                'confirmUrl' => Url::to(['copy', 'id' => $model->id,]),
                'message' => 'Вы уверены, что хотите скопировать эту декларацию?',
            ]); ?>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php
        if (Yii::$app->user->can(permissions\document\Document::UPDATE_STATUS)): ?>
            <?php if (!$model->status_id == TaxDeclarationStatus::STATUS_ACCEPTED) : ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => 'Подписана',
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    ],
                    'confirmUrl' => Url::to([
                        'update-status',
                        'id' => $model->id,
                        'status' => 'accepted'
                    ]),
                    'message' => 'Вы уверены, что хотите установить для декларации статус "Подписана"?'
                ]); ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => '<i class="fa fa-ok fa-2x"></i>',
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                    ],
                    'confirmUrl' => Url::to([
                        'update-status',
                        'id' => $model->id,
                        'status' => 'accepted'
                    ]),
                    'message' => 'Вы уверены, что хотите установить для декларации статус "Подписана"?'
                ]); ?>
            <?php endif; ?>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (Yii::$app->user->can(permissions\document\Document::UPDATE_STATUS)): ?>
            <?php /*
            <?= Html::a('Отправить', [
                '/cash/ofd/default/import',
                'p' => \frontend\modules\cash\modules\ofd\components\Ofd::routeEncode($pageRoute),
            ], [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs ofd-module-open-link',
            ]); ?>
            <?= Html::a('<i class="fa fa-download fa-2x"></i>', [
                '/cash/ofd/default/import',
                'p' => \frontend\modules\cash\modules\ofd\components\Ofd::routeEncode($pageRoute),
            ], [
                'class' => 'btn darkblue widthe-100 hidden-lg ofd-module-open-link',
                'title' => 'Отправить',
            ]); ?>
            <?= \frontend\modules\cash\modules\ofd\widgets\OfdModalWidget::widget([
                'pageTitle' => $this->title,
                'pageUrl' => Url::to($pageRoute),
            ]) ?> */ ?>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (Yii::$app->user->can(permissions\document\Document::DELETE)): ?>
            <?= Html::button('Удалить', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                'data-toggle' => 'modal',
                'href' => '#delete-confirm',
            ]); ?>
            <?= Html::button('<i class="fa fa-trash-o fa-2x"></i>', [
                'class' => 'btn darkblue widthe-100 hidden-lg',
                'data-toggle' => 'modal',
                'href' => '#delete-confirm',
                'title' => 'Удалить',
            ]); ?>
        <?php endif; ?>
    </div>
</div>

<?php if (Yii::$app->user->can(permissions\document\Document::DELETE)) {
    echo ConfirmModalWidget::widget([
        'options' => [
            'id' => 'delete-confirm',
        ],
        'toggleButton' => false,
        'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id]),
        'confirmParams' => [],
        'message' => "Вы уверены, что хотите удалить эту декларацию?",
    ]);
}; ?>
