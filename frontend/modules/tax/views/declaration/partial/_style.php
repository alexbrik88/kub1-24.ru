<style>
    .step-by-step {
        color: #171717;
    }

    .step-by-step-menu ul {
        list-style: none;
        margin: 0;
        padding: 0;
        width: 100%;
    }

    .step-by-step-menu ul li {
        width: 19.99%;
    }

    .step-by-step-menu ul li a:hover, .step-by-step-menu ul li a:active {
        text-decoration: none;
    }

    .step-by-step-menu ul li, .step-by-step-menu ul li a {
        display: block;
        float: left;
        color: #171717;
    }

    .step-by-step-menu ul li a {
        height: 125px;
    }

    .step-by-step-menu ul li .image-block {
        padding: 13px 9px;
        border: 5px solid #969a9a;
        border-radius: 10px !important;
    }

    .step-by-step-menu ul li .text-block {
        padding-top: 4px;
        font-size: 14px;
        text-align: center;
        position: absolute;
        margin-left: -65px;
        width: 208px;
        font-weight: 700;
    }

    .step-by-step-menu ul li .image-block > div {
        width: 48px;
        height: 43px;
        background-size: 100%;
        background-repeat: no-repeat;
        background-position: 0px 0px;
    }

    .step-by-step-menu .line {
        padding-top: 35px;
        border: none;
        width: 300px;
        display: table-cell;
    }

    .step-by-step-menu .line > div {
        width: 100%;
        height: 4px;
        background-color: #969a9a;
    }

    /* active */
    .step-by-step-menu .active .line > div {
        background-color: #4276a4;
    }

    .step-by-step-menu .active .image-block {
        border-color: #4276a4;
    }

    .step-by-step-menu .active .image-block > div {
        background-position: 0px -43px;
    }

    .step-by-step-menu .active .text-block {
        color: #4276a4 !important;
    }

    /* finish */
    .step-by-step-menu .finish .line > div {
        background-color: #7bbb47;
    }

    .step-by-step-menu .finish .image-block {
        border-color: #7bbb47;
    }

    .step-by-step-menu .finish .image-block > div {
        background-position: 0px -86px;
    }

    .step-by-step-menu .finish .text-block {
        color: #7bbb47 !important;
    }

    /* style borders*/
    .bord-light-b {
        border-bottom: 1px solid #ececec;
    }

    /* info */

    .step-by-step-info p {
        margin: 0px;
        padding: 0px;
    }

    /* style form */
    .step-by-step-form{
        min-height: 400px;
    }

    .step-by-step-form h3 {
        padding-bottom: 10px;
        font-size: 23px;
    }
    .step-by-step-form h4 {
        display: inline-block;
        padding-top: 10px;
        font-size: 18px;
        border-bottom: 1px dashed #171717;
    }

    .step-by-step-form .form-group {
        padding: 10px 0px;
    }

    .step-by-step-form .form-group label {
        padding: 3px 0px 0px 0px;
        font-weight: 100 !important;
    }

    .step-by-step-form .form-group input {
        padding: 4px;
        border: 1px solid #cccccc;
    }

    .step-by-step-form .form-group-check-box {
        padding: 0px 11px 0px 0px;
    }

    .step-by-step-form .form-group-check-box label {
        width: 100px;
        float: right;
        text-align: right;
        padding: 0px;
        font-style: italic;
    }

    .step-by-step-form .form-group-check-box .checker {
        float: right;
        padding-top: 3px;
    }

    .step-by-step-form .mini-inp label, .step-by-step-form .mini-inp input {
        max-width: 70px;
    }

    ::-webkit-input-placeholder {
        /* Chrome/Opera/Safari */
        font-style: italic;
        color: #cccccc;
    }

    ::-moz-placeholder {
        /* Firefox 19+ */
        font-style: italic;
        color: #cccccc;
    }

    :-ms-input-placeholder {
        /* IE 10+ */
        font-style: italic;
        color: #cccccc;
    }

    :-moz-placeholder {
        /* Firefox 18- */
        font-style: italic;
        color: #cccccc;
    }

    .step-by-step .darkblue {
        color: #ffffff;
    }

    .step-by-step button {
        color: #ffffff;
        padding: 7px 15px;
        min-width: 180px;
    }

    /* table */

    .table-block {

    }

    .table-block .date .top{
        height: 30px;
        width: 100px;
        border-bottom: 1px solid #cecece;
        margin: 0 auto;
    }
    .table-block .date .bottom{
        height: 30px;
        width: 100px;
        margin: 0 auto;
    }
    .table-block .date .num{
        border: 1px solid #cecece;
        padding: 5px;
        height: 30px;
        width: 30px;
    }

    .border-light{
        border: 1px solid #cecece;
    }

    .border-light-t{
        border-top: 1px solid #cecece;
    }
    .border-light-b{
        border-bottom: 1px solid #cecece;
    }
    .border-light-l{
        border-left: 1px solid #cecece;
    }
    .border-light-r{
        border-right: 1px solid #cecece;
    }
    .border2-light-t{
        border-top: 2px solid #cecece;
    }
    .border2-light-b{
        border-bottom: 2px solid #cecece;
    }
    .border2-light-l{
        border-left: 2px solid #cecece;
    }
    .border2-light-r{
        border-right: 2px solid #cecece;
    }
    .pad5-l {
        padding-left: 5px !important;
    }
    .pad5-t {
        padding-top: 5px !important;
    }
    .btn.darkblue {
        color: #ffffff;
    }
</style>