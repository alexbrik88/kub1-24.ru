<?php
use frontend\modules\tax\models\TaxDeclaration;
use himiklab\thumbnail\EasyThumbnailImage;
use yii\helpers\Html;
use common\components\date\DateHelper;

$formattedDate = DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

$TD_showOKTMO = function($model, $prev_quarter, $current_quarter) {

    return ($model->taxDeclarationQuarters[$current_quarter]->oktmo != $model->taxDeclarationQuarters[$prev_quarter]->oktmo) ?
        "<td class=\"font-main\">{$model->taxDeclarationQuarters[$current_quarter]->oktmo}</td>" :
        "<td class=\"font-main text-right\">".str_repeat("&mdash;", 10)."</td>";
};

/** @var TaxDeclaration $model */
?>

<div class="page-content-in p-center pad-pdf-p page-break">

    <table class="table no-border">
        <tr>
            <td width="25%" rowspan="2" style="vertical-align:top;">
                <?= Html::img($model->getUsnBarcode(2), ['width'=>'165px', 'class'=>'barcode']);?>
            </td>
            <td class="font-size-11 text-right">ИНН</td>
            <td colspan="3" class="font-main"><?= $model->company->inn ?></td>
            <td rowspan="2"></td>
        </tr>
        <tr>
            <!--<td width="25%"></td>-->
            <td width="40px" class="font-size-11 text-right">КПП</td>
            <td width="18%" class="font-main"><?= str_repeat("&mdash;", 5) ?></td>
            <td width="6%" class="font-size-11 text-right">Стр.</td>
            <td width="9%" class="font-main">002</td>
            <!--<td></td>-->
        </tr>
    </table>
    <br/>
    <br/>
    <table class="table no-border">
        <tr>
            <td class="font-size-11 font-bold text-center">
                Раздел 1.1. Сумма налога (авансового платежа по налогу), уплачиваемого в связи с применением упрощённой
                системы налогообложения (объект налогообложения - доходы), подлежащая уплате (уменьшению),
                по данным налогоплательщика
            </td>
        </tr>
    </table>
    <br/>
    <br/>

    <table class="table no-border">
        <tr>
            <td width="60%" class="small-italic">Показатели</td>
            <td width="8%" class="small-italic">Код строки</td>
            <td class="small-italic">Значения показателей (в рублях)</td>
        </tr>
        <tr style="padding-bottom:20px;">
            <td class="small-italic">1</td>
            <td class="small-italic">2</td>
            <td class="small-italic">3</td>
        </tr>

        <tr class="middle">
            <td>Код по ОКТМО</td>
            <td class="text-center">010</td>
            <td class="font-main"><?= $model->taxDeclarationQuarters[0]->oktmo ?></td>
        </tr>
        <tr class="middle">
            <td>
                Сумма авансового платежа к уплате по сроку не позднее<br/> двадцать пятого апреля отчетного года<br/>
                (стр. 130 - стр. 140) разд. 2.1.1 - стр. 160 разд. 2.1.2, <br/>
                если (стр. 130 - стр. 140) разд. 2.1.1 - стр. 160 разд. 2.1.2 >= 0
            </td>
            <td class="text-center">020</td>
            <td class="font-main text-right"><?= $model->taxDeclarationQuarters[0]->tax_amount > 0 ?
                    $model->taxDeclarationQuarters[0]->tax_amount :
                    str_repeat("&mdash;", 10)?>
            </td>
        </tr>

        <tr class="middle">
            <td>Код по ОКТМО</td>
            <td class="text-center">030</td>
            <?= $TD_showOKTMO($model, 0, 1) ?>
        </tr>
        <tr class="middle">
            <td>
                Сумма авансового платежа к уплате по сроку не позднее<br/> двадцать пятого июля отчетного года<br/>
                (стр. 131 - стр. 141) разд. 2.1.1 - стр. 161 разд. 2.1.2 - стр. 020, если<br/>
                (стр. 131 - стр. 141) разд. 2.1.1 - стр. 161 разд. 2.1.2 - стр. 020 >= 0
            </td>
            <td class="text-center">040</td>
            <td class="font-main text-right"><?= $model->taxDeclarationQuarters[1]->tax_amount > 0 ?
                    $model->taxDeclarationQuarters[1]->tax_amount :
                    str_repeat("&mdash;", 10)?>
            </td>
        </tr>
        <tr class="middle">
            <td>
                Сумма авансового платежа к уменьшению по сроку не позднее<br/> двадцать пятого июля отчетного года<br/>
                стр. 020 - ((стр. 131 - стр. 141) разд. 2.1.1 - стр. 161 разд. 2.1.2), если<br/>
                (стр. 131 - стр. 141) разд. 2.1.1 - стр. 161 разд. 2.1.2 - стр. 020 < 0
            </td>
            <td class="text-center">050</td>
            <td class="font-main text-right"><?= $model->taxDeclarationQuarters[1]->tax_amount < 0 ?
                    abs($model->taxDeclarationQuarters[1]->tax_amount) :
                    str_repeat("&mdash;", 10)?>
            </td>
        </tr>

        <tr class="middle">
            <td>Код по ОКТМО</td>
            <td class="text-center">060</td>
            <?= $TD_showOKTMO($model, 1, 2) ?>
        </tr>
        <tr class="middle">
            <td>
                Сумма авансового платежа к уплате по сроку не позднее<br/> двадцать пятого октября отчетного года<br/>
                (стр. 132 - стр. 142) разд. 2.1.1 - стр. 162 разд. 2.1.2<br/>
                (стр. 020 + стр. 040 - стр. 050), если (стр. 132 - стр. 142) разд. 2.1.1 - <br/>
                стр. 162 разд. 2.1.2 - (стр. 020 + стр. 040 - стр. 050) >= 0
            </td>
            <td class="text-center">070</td>
            <td class="font-main text-right"><?= $model->taxDeclarationQuarters[2]->tax_amount > 0 ?
                    $model->taxDeclarationQuarters[2]->tax_amount :
                    str_repeat("&mdash;", 10)?>
            </td>
        </tr>
        <tr class="middle">
            <td>
                Сумма авансового платежа к уменьшению по сроку не позднее<br/> двадцать пятого октября отчетного года<br/>
                (стр. 020 + стр. 040 - стр. 050) - ((стр.132 - стр.142) разд. 2.1.1 - стр.162 разд.<br/>
                2.1.2), если (стр. 132 - стр. 142) разд. 2.1.1 - стр. 162 разд. 2.1.2 - (стр. 020 + <br/>
                стр. 040 - стр. 050) < 0
            </td>
            <td class="text-center">080</td>
            <td class="font-main text-right"><?= $model->taxDeclarationQuarters[1]->tax_amount < 0 ?
                    abs($model->taxDeclarationQuarters[1]->tax_amount) :
                    str_repeat("&mdash;", 10)?>
            </td>
        </tr>

        <tr class="middle">
            <td>Код по ОКТМО</td>
            <td class="text-center">090</td>
            <?= $TD_showOKTMO($model, 2, 3) ?>
        </tr>
        <tr class="middle">
            <td>
                Сумма налога, подлежащая доплате за налоговый<br/>
                период (календарный год) по сроку*<br/>
                (стр. 133 - стр. 143) разд. 2.1.1 - стр. 163 разд. 2.1.2 - <br/>
                (стр.020 + стр.040 - стр. 050 + стр. 070 - стр. 080), если<br/>
                (стр. 133 - стр. 143) разд. 2.1.1 - стр. 163 разд. 2.1.2 -<br/>
                (стр. 020 + стр. 040 - стр. 050 + стр. 070 - стр. 080) >= 0
            </td>
            <td class="text-center">100</td>
            <td class="font-main text-right"><?= $model->taxDeclarationQuarters[3]->tax_amount > 0 ?
                    $model->taxDeclarationQuarters[3]->tax_amount :
                    str_repeat("&mdash;", 10)?>
            </td>
        </tr>
        <tr class="middle">
            <td>
                Сумма налога, уплаченная в связи с применением патентной системы<br/>
                налогообложения, подлежащая зачету
            </td>
            <td class="text-center">101</td>
            <td class="font-main text-right">
                <?= str_repeat("&mdash;", 10) ?>
            </td>
        </tr>
        <tr class="middle">
            <td>
                Сумма налога к уменьшению за налоговый период<br/>
                (календарный год) по сроку*<br/>
                (стр. 020 + стр. 040 - стр. 050 + стр. 070 - стр. 080) - ((стр. 133 - стр. 143)<br/>
                разд. 2.1.1 - стр.163 разд. 2.1.2), если (стр. 133 - стр. 143) разд. 2.1.1 - стр.163<br/>
                разд. 2.1.2 - (стр. 020 + стр. 040 - стр. 050 + стр. 070 - стр. 080) < 0
            </td>
            <td class="text-center">110</td>
            <td class="font-main text-right"><?= $model->taxDeclarationQuarters[3]->tax_amount < 0 ?
                    abs($model->taxDeclarationQuarters[3]->tax_amount) :
                    str_repeat("&mdash;", 10)?>
            </td>
        </tr>
    </table>

    <br/>
    <table class="table no-border">
        <tr>
            <td>
                * для организаций - не позднее 31 марта года, следующего за истекшим налоговым периодом;<br/>
                для индивидуальных предпринимателей - не позднее 30 апреля года, следующего за истекшим налоговым периодом.
            </td>
        </tr>
    </table>

    <br/>

    <table class="table no-border">
        <tr>
            <td class="text-center font-bold">
                Достоверность и полноту сведений, указанных на данной странице, подтверждаю:
            </td>
        </tr>
    </table>
    <br/>
    <table class="table no-border">
        <tr>
            <td width="25%"></td>
            <td width="15%" class="text-center border-bottom">
            </td>
            <td>(подпись)</td>
            <td width="15%" class="text-center border-bottom">
                <?= $formattedDate ?>
            </td>
            <td>(дата)</td>
            <td></td>
        </tr>
    </table>
</div>