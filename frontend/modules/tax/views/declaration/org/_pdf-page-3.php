<?php
use frontend\modules\tax\models\TaxDeclaration;
use himiklab\thumbnail\EasyThumbnailImage;
use yii\helpers\Html;
use common\components\date\DateHelper;

$formattedDate = DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

/** @var TaxDeclaration $model */
?>

<div class="page-content-in p-center pad-pdf-p page-break">

    <?= $this->render('_pdf-pages-head', [
        'model' => $model,
        'title' => 'Расчет налога',
        'subtitle' => 'Лист 02',
        'pageNum'  => '003',
        'barcode' => 'barcode_00213059.png',
        'taxpayerOrganizationCode' => $taxpayerOrganizationCode
    ]) ?>

    <table class="table no-border">
        <tr>
            <td width="53%" class="small-italic">Показатели</td>
            <td width="13%" class="small-italic">Код строки</td>
            <td class="small-italic">Значения показателей</td>
        </tr>
        <tr style="padding-bottom:20px;">
            <td class="small-italic">1</td>
            <td class="small-italic">2</td>
            <td class="small-italic">3</td>
        </tr>
        <!--010-->
        <tr class="middle spp">
            <td>Доходы от реализации (стр. 040 Приложения №1 к Листу 02)</td>
            <td class="text-center">010</td>
            <td class="font-main"><?= printOrEmpty($columnData['010']) ?></td>
        </tr>
        <!--020-->
        <tr class="middle spp">
            <td>Внереализационные доходы (стр. 100 Приложения №1 к Листу 02)</td>
            <td class="text-center">020</td>
            <td class="font-main"><?= printOrEmpty($columnData['020']) ?></td>
        </tr>
        <!--030-->
        <tr class="middle spp">
            <td>Расходы, уменьшающие сумму доходов от реализации (стр.130 Приложения №2 к Листу 02)</td>
            <td class="text-center">030</td>
            <td class="font-main"><?= printOrEmpty($columnData['030']) ?></td>
        </tr>
        <!--040-->
        <tr class="middle spp">
            <td>Внереализационные расходы (стр. 200+стр.300 Приложения №2 к Листу 02)</td>
            <td class="text-center">040</td>
            <td class="font-main"><?= printOrEmpty($columnData['040']) ?></td>
        </tr>
        <!--050-->
        <tr class="middle spp">
            <td>Убытки (стр.360 Приложения №3 к Листу 02)</td>
            <td class="text-center">050</td>
            <td class="font-main"><?= printOrEmpty($columnData['050']) ?></td>
        </tr>
        <!--060-->
        <tr class="middle spp">
            <td>
                <span style="font-weight:bold">Итого прибыль (убыток)</span>
                (стр.010+стр.020-стр.030-стр.040+стр.050)
            </td>
            <td class="text-center">060</td>
            <td class="font-main"><?= printOrEmpty($columnData['060']) ?></td>
        </tr>
        <!--070-->
        <tr class="middle spp">
            <td>Доходы, исключаемые из прибыли</td>
            <td class="text-center">070</td>
            <td class="font-main"><?= printOrEmpty($columnData['070']) ?></td>
        </tr>
        <!--080-->
        <tr class="middle spp">
            <td>Прибыль, полученная Банком России от осуществления деятельности,
                связанной с выполнением его функций, и облагаемая по налоговой ставке 0%</td>
            <td class="text-center">080</td>
            <td class="font-main"><?= printOrEmpty($columnData['080']) ?></td>
        </tr>
        <!--100-->
        <tr class="middle spp">
            <td>Налоговая база<br/>
                (стр.060 - стр.070 - стр.080 - стр.400 Приложения №2 к Листу 02 + стр.100 Листов 05 + стр.530 Листа 06</td>
            <td class="text-center">100</td>
            <td class="font-main"><?= printOrEmpty($columnData['100']) ?></td>
        </tr>
        <!--110-->
        <tr class="middle spp">
            <td>Сумма убытка или части убытка, уменьшающего налоговую базу за
                отчетный (налоговый) период (стр.150 Приложения №4 к Листу 02)</td>
            <td class="text-center">110</td>
            <td class="font-main"><?= printOrEmpty($columnData['110']) ?></td>
        </tr>
        <!--120-->
        <tr class="middle spp">
            <td>Налоговая база для исчисления налога (стр.10-стр.110)</td>
            <td class="text-center">120</td>
            <td class="font-main"><?= printOrEmpty($columnData['120']) ?></td>
        </tr>
        <!--130-->
        <tr class="middle spp">
            <td class="lp">в том числе в бюджет субъекта Российской Федерации (с учетом положений пункта 1 статьи 284
                Налогового кодекса Российской Федерации)</td>
            <td class="text-center">130</td>
            <td class="font-main"><?= printOrEmpty($columnData['130']) ?></td>
        </tr>
        <!--140-->
        <tr class="middle spp">
            <td>Ставка налога - всего, (%)</td>
            <td class="text-center">140</td>
            <td class="font-main"><?= $columnData['140'] ?></td>
        </tr>
        <tr class="middle">
            <td class="lp">в том числе:</td>
            <td></td>
            <td></td>
        </tr>
        <!--150-->
        <tr class="middle spp">
            <td class="lp">в федеральный бюджет</td>
            <td class="text-center">150</td>
            <td class="font-main"><?= $columnData['150'] ?></td>
        </tr>
        <!--160-->
        <tr class="middle spp">
            <td class="lp">в бюджет субъекта Российской Федерации</td>
            <td class="text-center">160</td>
            <td class="font-main"><?= $columnData['160'] ?></td>
        </tr>
        <!--170-->
        <tr class="middle spp">
            <td class="lp">в бюджет субъекта Российской Федерации (с учетом положений пункта 1 статьи 284
                Налогового кодекса Российской Федерации</td>
            <td class="text-center">170</td>
            <td class="font-main"><?= printOrEmpty($columnData['170'], 3) ?></td>
        </tr>
        <!--180-->
        <tr class="middle spp">
            <td>Сумма исчисленного налога - всего</td>
            <td class="text-center">180</td>
            <td class="font-main"><?= printOrEmpty($columnData['180']) ?></td>
        </tr>
        <tr class="middle">
            <td class="lp">в том числе:</td>
            <td></td>
            <td></td>
        </tr>
        <!--190-->
        <tr class="middle spp">
            <td class="lp">в федеральный бюджет</td>
            <td class="text-center">190</td>
            <td class="font-main"><?= printOrEmpty($columnData['190']) ?></td>
        </tr>
        <!--200-->
        <tr class="middle spp">
            <td class="lp">в бюджет субъекта Российской Федерации <br/>
                (стр.120-стр.130) х стр.160 : 100 + (стр.130 х стр.170 : 100)</td>
            <td class="text-center">200</td>
            <td class="font-main"><?= printOrEmpty($columnData['200']) ?></td>
        </tr>

    </table>

</div>