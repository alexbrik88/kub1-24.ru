<?php

use frontend\modules\tax\models\TaxDeclaration;
use frontend\rbac\UserRole;
use common\models\company\CompanyType;
use common\components\helpers\Html;
use frontend\modules\tax\models\DeclarationOsnoHelper;

/* @var TaxDeclaration $model
 * @var DeclarationOsnoHelper $declarationOsnoHelper
 */

$isIp = $model->company->company_type_id == CompanyType::TYPE_IP;

$this->title = $isIp ?
    'Налоговая декларация' :
    ($model->nds ?
        'Декларация на добавленную стоимость' :
        ($model->org ?
            'Декларация на прибыль организации' :
            ($model->balance ?
                'Бухгалтерский баланс' :
                ($model->szvm ?
                    'Сведения о застрахованных лицах' :
                'Единая (упрощенная) налоговая декларация ОСНО'))));
$this->context->layoutWrapperCssClass = 'out-act out-document payment-order';

$canUpdate = $canUpdateStatus = UserRole::ROLE_CHIEF;
if (!function_exists('getEmptyDottedBoxes')) {
    function getEmptyDottedBoxes($count)
    {
        $str = '';
        for ($i = 0; $i < $count; $i++) {
            $str .= '<td class="dotbox">&nbsp;&nbsp;</td>';
        }

        return $str;
    }
}
if (!function_exists('printOrEmpty')) {
    function printOrEmpty($value, $repeat = 11)
    {
        return ($value > 0) ? round($value) : str_repeat("&mdash;", $repeat);
    }
}

?>
<?= $this->render('partial/_style') ?>
<div class="page-content-in" style="padding-bottom: 30px;">
    <div>
        <?= Html::a('Назад к списку', 'index', [
            'class' => 'back-to-customers']); ?>
    </div>
    <div class="col-xs-12 pad0">
        <div class="col-xs-12 <?= $isIp ? 'col-lg-7' : 'col-lg-8'; ?> pad0">
            <?= $this->render('partial/_viewText', [
                'model' => $model,
                'canUpdate' => $canUpdate,
                'canUpdateStatus' => $canUpdateStatus,
                'declarationOsnoHelper' => $declarationOsnoHelper,
                'isIp' => $isIp,
            ]); ?>
        </div>
        <div class="col-xs-12 <?= $isIp ? 'col-lg-5' : 'col-lg-4'; ?> pad0 pull-right"
             style="padding-bottom: 5px !important; max-width: 480px;">
            <div class="col-xs-12" style="padding-right:0!important;">
                <?= $this->render('partial/_viewStatus', [
                    'model' => $model,
                    'canUpdate' => $canUpdate,
                    'canUpdateStatus' => $canUpdateStatus,
                    'declarationOsnoHelper' => $declarationOsnoHelper,
                    'isIp' => $isIp,
                ]); ?>
            </div>
        </div>
    </div>
    <div id="buttons-bar-fixed">
        <?= $this->render('partial/_viewActionsButtons', [
            'model' => $model,
            'canUpdate' => $canUpdate,
            'canUpdateStatus' => $canUpdateStatus,
            'isIp' => $isIp,
        ]); ?>
    </div>
</div>

