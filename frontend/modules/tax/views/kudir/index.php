<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\components\grid\GridView;
use common\components\grid\DropDownSearchDataColumn;
use common\models\AgreementTemplate;
use common\models\employee\Employee;
use frontend\widgets\BtnConfirmModalWidget;
use yii\grid\ActionColumn;

$this->title = 'КУДиР';

$emptyMessage = 'У вас пока не создано ни одной КУДиР.';
?>

<div class="portlet box">
    <div class="btn-group pull-right">
        <?php /*
        <?= Html::a('<i class="fa fa-plus"></i> ДОБАВИТЬ', Url::to(['create']), [
            'class' => 'btn yellow',
        ]); ?>
        */ ?>
    </div>
    <h3 class="page-title"><?= $this->title; ?></h3>
</div>

<div class="portlet box darkblue">
    <div class="portlet-title row-fluid">
        <div class="caption col-md-6">
            Список: <?= $dataProvider->totalCount ?>
        </div>
        <div class="tools search-tools tools_button col-md-5 col-sm-5">
            <div class="form-body">
                <?php $form = \yii\widgets\ActiveForm::begin([
                    'method' => 'GET',
                    'action' => ['index'],
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'options' => [
                            'class' => '',
                        ],
                    ],
                ]); ?>
                <div class="search_cont">
                    <div class="wimax_input">
                        <?= $form->field($searchModel, 'find_by')->textInput([
                            'placeholder' => 'Отчетный год',
                        ]); ?>
                    </div>
                    <div class="wimax_button">
                        <?= Html::submitButton('НАЙТИ', [
                            'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                        ]) ?>
                    </div>
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
    </div>

    <div class="portlet-body">
        <div class="table-container">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'emptyText' => $emptyMessage,
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable documents_table status_nowrap agreements-templates-table fix-thead',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],

                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount, 'scroll' => true]),
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                'columns' => [
                    [
                        'attribute' => 'tax_year',
                        'label' => 'Отчетный год',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $data]) ?
                                Html::a($data['tax_year'], ['/tax/kudir/view', 'id' => $data['id']]) :
                                $data['tax_year'];
                        },
                    ],
                    [
                        'attribute' => 'created_at',
                        'label' => 'Дата создания',
                        'headerOptions' => ['style' => 'width:10%;'],
                        'format' => ['date', 'php:d.m.Y'],
                    ],
                    [
                        'attribute' => 'status',
                        'label' => 'Статус',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-left text-ellipsis',
                        ],
                        'class' => DropDownSearchDataColumn::className(),
                        'value' => function ($data) {
                            return \common\models\document\status\KudirStatus::findOne(['id'=>$data['status_id']])->name;
                        },
                        'format' => 'raw',
                        'filter' => [
                            '' => 'Все',
                            1 => 'Создана',
                            3 => 'Принята',
                            4 => 'Передана',
                            5 => 'Подписана'
                        ]
                    ],
                    [
                        'attribute' => 'document_author_id',
                        'label' => 'Ответственный',
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-left text-ellipsis',
                        ],
                        'class' => DropDownSearchDataColumn::className(),
                        'value' => function ($data) {
                            $employee = Employee::findOne([
                                'id' => $data['document_author_id']
                            ]);

                            return (!empty($employee)) ? $employee->getShortFio() : '';
                        },
                        'format' => 'raw',
                        'filter' => $searchModel->getCreators()
                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>