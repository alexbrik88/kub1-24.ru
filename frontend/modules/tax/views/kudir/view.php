<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\tax\models\Kudir;
use frontend\rbac\UserRole;
use common\components\date\DateHelper;

$this->title = 'Книга учета доходов и расходов';
$this->context->layoutWrapperCssClass = 'out-act out-document payment-order';

$canUpdate = $canUpdateStatus = UserRole::ROLE_CHIEF;

?>

<div class="page-content-in" style="padding-bottom: 30px;">
    <div>
        <?php
        echo \yii\helpers\Html::a('Назад к списку', 'index', [
            'class' => 'back-to-customers']);
        ?>
    </div>
    <div class="col-xs-12 pad0">
        <div class="col-xs-12 col-lg-7 pad0">
            <?= $this->render('partial/_viewText', [
                'model' => $model,
                'canUpdate' => $canUpdate,
                'canUpdateStatus' => $canUpdateStatus
            ]); ?>
        </div>
        <div class="col-xs-12 col-lg-5 pad0 pull-right" style="padding-bottom: 5px !important; max-width: 480px;">
            <div class="col-xs-12" style="padding-right:0px !important;">
                <?= $this->render('partial/_viewStatus', [
                    'model' => $model,
                    'canUpdate' => $canUpdate,
                    'canUpdateStatus' => $canUpdateStatus
                ]); ?>
            </div>
        </div>
    </div>
    <div id="buttons-bar-fixed">
        <?= $this->render('partial/_viewActionsButtons', [
            'model' => $model,
            'canUpdate' => $canUpdate,
            'canUpdateStatus' => $canUpdateStatus
        ]); ?>
    </div>
</div>

