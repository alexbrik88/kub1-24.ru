<?php
use himiklab\thumbnail\EasyThumbnailImage;
use yii\helpers\Html;
use common\components\date\DateHelper;

/** @var \common\models\Company $company */
?>

<?= $this->render('partial/_pdf-view-1', [
    'model' => $model,
    'company' => $company
]); ?>
<?= $this->render('partial/_pdf-view-2', [
    'model' => $model,
    'company' => $company,
    'quartersBankFlows' => $quartersBankFlows
]); ?>
<?= $this->render('partial/_pdf-view-3', [
    'model' => $model,
    'company' => $company,
    'quartersBankFlows' => $quartersBankFlows
]); ?>
<?= $this->render('partial/_pdf-view-4', [
    'model' => $model,
    'company' => $company,
    'quartersBankFlows' => $quartersBankFlows
]); ?>
<?= $this->render('partial/_pdf-view-5', [
    'model' => $model,
    'company' => $company,
    'quartersBankFlows' => $quartersBankFlows
]); ?>