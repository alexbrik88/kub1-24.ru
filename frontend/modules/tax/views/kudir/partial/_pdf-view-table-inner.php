<?php
use common\components\date\DateHelper;
use common\components\TextHelper;
?>

<?php if ($bankFlows instanceof \common\models\cash\CashBankFlows): ?>

    <tr>
        <td class="text-center">
            <?= ($num_pp) ?>
        </td>
        <td class="">
            <?= '№ ' . $bankFlows->payment_order_number .  ' от ' .
            DateHelper::format($bankFlows->recognition_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)  ?>
        </td>
        <td class="ver-top">
            <?= $bankFlows->incomeReason->name // Содержание операции ?>
            "<?= $bankFlows->contractor !== null ?
                $bankFlows->contractor->nameWithType :
                $bankFlows->cashContractor->text;
            ?>"
        </td>
        <td class="ver-top text-right">
            <?= is_numeric($bankFlows->amountIncome) // Приход
                ? TextHelper::invoiceMoneyFormat($bankFlows->amountIncome, 2)
                : ''; ?>
        </td>
        <td class="ver-top text-right">
        </td>
    </tr>

<?php else: ?>

    <tr>
        <td class="text-center">
            <?= ($num_pp) ?>
        </td>
        <td class="">
            <?= '№ ' . $bankFlows->number .  ' от ' .
            DateHelper::format($bankFlows->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)  ?>
        </td>
        <td class="ver-top">
            Операции по кассе: <?= $bankFlows->description // Содержание операции ?>
        </td>
        <td class="ver-top text-right">
            <?= is_numeric($bankFlows->amount) // Приход
                ? TextHelper::invoiceMoneyFormat($bankFlows->amount, 2)
                : ''; ?>
        </td>
        <td class="ver-top text-right">
        </td>
    </tr>

<?php endif; ?>