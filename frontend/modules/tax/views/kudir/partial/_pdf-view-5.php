<?php
use common\components\date\DateHelper;
use common\components\TextHelper;
?>
<!-- IV КВАРТАЛ -->
<div class="page-content-in p-center pad-pdf-p <?= (@$_GET['actionType'] == 'print') ? 'page-break' : '' ?>">
    <table class="table no-border">
        <tr>
            <td class="text-center font-size-9">I. Доходы и расходы</td>
        </tr>
    </table>
    <br/>
    <table class="table">
        <tr>
            <td colspan="3" class="text-center">Регистрация</td>
            <td colspan="2" class="text-center">Сумма</td>
        </tr>
        <tr class="middle">
            <td width="5%"  class="text-center">№<br/>п/п</td>
            <td width="10%" class="text-center">Дата и номер первичного документа</td>
            <td class="text-center">Содержание операции</td>
            <td width="15%" class="text-center">Доходы, учитываемые при исчислении налоговой базы</td>
            <td width="15%" class="text-center">Расходы, учитываемые при исчислении налоговой базы</td>
        </tr>
        <tr>
            <td class="text-center">1</td>
            <td class="text-center">2</td>
            <td class="text-center">3</td>
            <td class="text-center">4</td>
            <td class="text-center">5</td>
        </tr>

        <?php
        $num_pp = 0;
        $total = 0;
        $quarterTotal = 0;

        for ($q=1; $q<=3; $q++)
            foreach ($quartersBankFlows[$q] as $bankFlows) {
                $num_pp++;
                if (is_numeric($bankFlows->amountIncome))
                    $total += $bankFlows->amountIncome;
                elseif (is_numeric($bankFlows->amount))
                    $total += $bankFlows->amount;
            }

        foreach ($quartersBankFlows[4] as $bankFlows) { $num_pp++;
            echo $this->render('_pdf-view-table-inner', [
                'bankFlows' => $bankFlows,
                'num_pp' => $num_pp
            ]);
            if (is_numeric($bankFlows->amountIncome)) $quarterTotal += $bankFlows->amountIncome;
        }

        $total += $quarterTotal;
        ?>

        <tr>
            <td colspan="3">Итого за IV квартал</td>
            <td class="ver-top text-right"><?= ($quarterTotal > 0) ? TextHelper::invoiceMoneyFormat($quarterTotal, 2) : '' ?></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3">Итого за год</td>
            <td class="ver-top text-right"><?= ($total > 0) ? TextHelper::invoiceMoneyFormat($total, 2) : '' ?></td>
            <td></td>
        </tr>
    </table>
    <br/>
    <table class="table no-border">
        <tr>
            <td colspan="2" class="font-size-9">Справка к разделу I:</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"> <br/> </td>
        </tr>
        <tr>
            <td width="5%">010</td>
            <td>Сумма полученных доходов за налоговый период</td>
            <td width="20%" class="border-bottom text-right"><?= ($total > 0) ? TextHelper::invoiceMoneyFormat($total, 2) : '' ?></td>
        </tr>
        <tr>
            <td>020</td>
            <td>Сумма произведенных расходов за налоговый период</td>
            <td class="border-bottom"></td>
        </tr>
        <tr>
            <td class="ver-top">030</td>
            <td>Сумма разницы между суммой уплаченного минимального налога и суммой<br/>
                исчисленного в общем порядке налога за предыдущий налоговый период
            </td>
            <td class="border-bottom"></td>
        </tr>
        <tr>
            <td colspan="3" style="padding-top:10px">Итого получено:</td>
        </tr>
        <tr>
            <td class="ver-top">040</td>
            <td>- доходов<br/>
                (код стр. 010 - код стр. 020 - код стр. 030)</td>
            <td class="border-bottom text-right"><?= ($total > 0) ? TextHelper::invoiceMoneyFormat($total, 0) : '' ?></td>
        </tr>
        <tr>
            <td class="ver-top">041</td>
            <td>- убытков<br/>
                (код стр. 020 + код стр. 030) - код стр. 010)</td>
            <td class="border-bottom"></td>
        </tr>
    </table>
</div>
