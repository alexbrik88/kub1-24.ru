<?php

use frontend\rbac\permissions;
use yii\bootstrap\Html;
use frontend\widgets\ConfirmModalWidget;
use yii\helpers\Url;
use common\models\document\status\KudirStatus;

/* @var $this yii\web\View
 * @var $model frontend\modules\tax\models\Kudir;
 */
?>
<div class="row action-buttons margin-no-icon">
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (FALSE && $canUpdate): ?>
            <button class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs send-message-panel-trigger">
                Отправить
            </button>
            <button class="btn darkblue widthe-100 hidden-lg send-message-panel-trigger" title="Отправить">
                <span class="ico-Send-smart-pls fs"></span>
            </button>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php
        echo Html::a('КУДиР (УСН 6%)', ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'filename' => $model->getPdfFileName(),], [
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
            'target' => '_blank',
        ]);
        echo Html::a('<i class="glyphicon glyphicon-download"></i>',
            ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'filename' => $model->getPdfFileName(),], [
            'class' => 'btn darkblue widthe-100 hidden-lg',
            'target' => '_blank',
            'title' => 'Печать',
        ]);
        ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?= Html::a('КУДиР (Патент)', ['document-print-patent', 'actionType' => 'pdf', 'id' => $model->id, 'filename' => $model->getPdfFileName()], [
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
            'target' => '_blank',
        ]); ?>
        <?= Html::a('<i class="glyphicon glyphicon-download" style="font-size: 17px;"></i>',
            ['document-print-patent', 'actionType' => 'pdf', 'id' => $model->id, 'filename' => $model->getPdfFileName()], [
            'class' => 'btn darkblue widthe-100 hidden-lg',
            'target' => '_blank',
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (FALSE && Yii::$app->user->can(permissions\document\Document::CREATE)): ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => 'Копировать',
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ],
                'confirmUrl' => Url::to(['copy', 'id' => $model->id,]),
                'message' => 'Вы уверены, что хотите скопировать эту декларацию?',
            ]); ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => '<i class="fa fa-files-o fa-2x"></i>',
                    'title' => 'Копировать',
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                ],
                'confirmUrl' => Url::to(['copy', 'id' => $model->id,]),
                'message' => 'Вы уверены, что хотите скопировать эту книгу?',
            ]); ?>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">

    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (FALSE && Yii::$app->user->can(permissions\document\Document::DELETE)): ?>
            <?= Html::button('Удалить', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                'data-toggle' => 'modal',
                'href' => '#delete-confirm',
            ]); ?>
            <?= Html::button('<i class="fa fa-trash-o fa-2x"></i>', [
                'class' => 'btn darkblue widthe-100 hidden-lg',
                'data-toggle' => 'modal',
                'href' => '#delete-confirm',
                'title' => 'Удалить',
            ]); ?>
        <?php endif; ?>
    </div>
</div>

<?php if (Yii::$app->user->can(permissions\document\Document::DELETE)) {
    echo ConfirmModalWidget::widget([
        'options' => [
            'id' => 'delete-confirm',
        ],
        'toggleButton' => false,
        'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id]),
        'confirmParams' => [],
        'message' => "Вы уверены, что хотите удалить эту книгу?",
    ]);
}; ?>
