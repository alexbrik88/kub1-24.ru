<?php

use frontend\modules\tax\assets\TaxrobotStartCssAsset;
use frontend\modules\tax\models\TaxrobotStartForm;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $company common\models\Company */

TaxrobotStartCssAsset::register($this);

$model = new TaxrobotStartForm($company);
$model->scenario = TaxrobotStartForm::SCENARIO_FAIL;
$model->tax_system = 2;
$model->subject = 'Программа еще учится работать с вашей системой налогообложения…';

$this->registerJs(<<<JS
    $(document).on("submit", "#tax-fail-form", function (e) {
        let modal = $(this).closest(".modal");
        let form = this;
        var button = $('button:submit', form)[0];
        var formData = new FormData(form);
        var l = Ladda.create(button);
        l.start();
        e.preventDefault();
        $.ajax(form.action, {
            method: form.method,
            data: formData,
            contentType: false,
            processData: false,
            success: function (data, textStatus, jqXHR) {
                l.stop();
                $(".modal-body", modal).html(data);
            }
        });
    });
JS
);
?>

<?php Modal::begin([
    'id' => 'taxrobot-start-modal',
    'toggleButton' => false,
    'closeButton' => false,
]); ?>

<div class="taxrobot-start-header">
    <h4>
        Ваша система налогообложения?
    </h4>
</div>

<div class="taxrobot-start-content">
    <div class="mb-4">
        <button class="button-regular button-hover-transparent">
            <?= TaxrobotStartForm::$taxSystemItems[2] ?>
        </button>
    </div>

    <h5>
        <strong>Программа еще учится работать с вашей системой налогообложения…</strong>
    </h5>
    <h5>
        <strong>НО, у нас хорошие новости для вас!</strong>
    </h5>

    <div>
        Пока программа учится, наши бухгалтера помогут рассчитать ваши налоги и сдать отчетность.
        Оставьте ваши контакты, и мы с вами свяжемся
    </div>

    <div class="mt-4">
        <?php $form = ActiveForm::begin([
            'id' => 'tax-fail-form',
            'action' => ['robot-start/tax-fail'],
        ]); ?>

            <?= Html::activeHiddenInput($model, 'subject') ?>
            <?= Html::activeHiddenInput($model, 'tax_system') ?>

            <div class="row mt-4">
                <div class="col-sm-4">
                    <?= $form->field($model, 'user_name')->textInput(['maxLength' => true]) ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'user_phone')->textInput([
                        'maxLength' => true,
                    ])->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                        'options' => [
                            'placeholder' => '+7(XXX) XXX-XX-XX',
                        ],
                    ]) ?>
                </div>
                <div class="col-sm-4">
                    <label class="control-label">&nbsp;</label>
                    <div>
                        <?= Html::submitButton('Отправить', [
                            'class' => 'button-regular button-hover-transparent min-w-130 ladda-button ladda-custom',
                            'data-style' => 'expand-right',
                        ]) ?>
                    </div>
                </div>
            </div>

        <?php ActiveForm::end() ?>
    </div>
</div>

<?php Modal::end(); ?>
