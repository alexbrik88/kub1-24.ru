<?php

use common\models\document\status\TaxDeclarationStatus;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use frontend\widgets\ConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\bootstrap\Dropdown;
use yii\helpers\Url;

$this->title = 'Налоговая декларация';
$this->context->layoutWrapperCssClass = 'out-act out-document payment-order';
/** @var \frontend\modules\tax\models\TaxDeclaration $model */
/** @var \frontend\modules\tax\models\Kudir $kudir */
/** @var \common\models\Company $company */
/** @var $isEmptyDeclaration boolean */

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

$taxation = $company->companyTaxationType;
$canUpdate = $canUpdateStatus = UserRole::ROLE_CHIEF;
?>

<?= $this->render('_style') ?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'tax-robot-declaration-pjax',
]); ?>

<div class="row">
    <div class="col-xs-12 step-by-step" style="margin-bottom: 35px;">
        <?= $this->render('_steps', ['step' => 6]) ?>
    </div>
    <div class="col-xs-12">
        <div class="" style="font-size:16px; font-weight: bold;">
            Налоговая декларация УСН за
            <div style="display: inline-block; width: 200px;">
                <div class="dropdown">
                    <?= Html::tag('div', $taxRobot->period->label, [
                        'class' => 'dropdown-toggle',
                        'data-toggle' => 'dropdown',
                        'style' => 'display: inline-block; border-bottom: 1px dashed #000; cursor: pointer;',
                    ])?>
                    <?= Dropdown::widget([
                        'id' => 'employee-rating-dropdown',
                        'encodeLabels' => false,
                        'items' => $taxRobot->getPeriodDropdownItems('declaration'),
                    ])?>
                </div>
            </div>
            <span style="<?=(preg_match('/^[0-9]{4}_4$/', $taxRobot->period->id)) ? 'display:none;':'' ?> font-size:13px; font-style: italic; padding-left: 10px; color: red">
                ВНИМАНИЕ! Декларация сдается ТОЛЬКО за календарный год.
            </span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 step-by-step">
        <!-- step 5 -->
        <div class="col-xs-12 pad0 mar-t-15">
            <div class="col-xs-12 col-lg-7 pad0">
                <div class="portlet customer-info project-info">
                    <?= $this->render('parts_declaration/view', [
                        'model' => $model,
                        'canUpdate' => $canUpdate,
                        'canUpdateStatus' => $canUpdateStatus
                    ]); ?>
                </div>
            </div>
            <div class="col-xs-12 col-lg-5 pad0 pull-right" style="padding-bottom: 5px !important; max-width: 480px;">
                <div class="col-xs-12" style="padding-right:0px !important;">
                    <?= $this->render('parts_declaration/viewStatus', [
                        'model' => $model,
                        'canUpdate' => $canUpdate,
                        'canUpdateStatus' => $canUpdateStatus,
                        'sumTaxToPay' => $sumTaxToPay,
                        'isEmptyDeclaration' => $isEmptyDeclaration,
                        'period' => $taxRobot->getUrlPeriodId()
                    ]); ?>
                </div>
            </div>

            <!--- buttons -->
            <div id="buttons-bar-fixed">
                <div class="row action-buttons margin-no-icon" style="padding-right:30px">
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                        <?= Html::a('Назад', [($isEmptyDeclaration) ? 'bank' : 'payment', 'period' => $taxRobot->getUrlPeriodId()], [
                            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                            'data' => [
                                'pjax' => '0',
                            ],
                        ]); ?>
                        <?= Html::a('<i class="fa fa-arrow-left fa-2x"></i>', [($isEmptyDeclaration) ? 'bank' : 'payment', 'period' => $taxRobot->getUrlPeriodId()], [
                            'title' => 'Назад',
                            'class' => 'btn darkblue widthe-100 hidden-lg',
                            'data' => [
                                'pjax' => '0',
                            ],
                        ]); ?>
                    </div>
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    </div>
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                        <?php
                        echo Html::a('Печать', ['declaration-print',
                                'actionType' => 'print',
                                'id' => $model->id,
                                'empty' => $isEmptyDeclaration,
                                'filename' => $model->getPrintTitle()], [
                            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                            'target' => '_blank',
                            'data' => [
                                'pjax' => '0',
                            ],
                        ]);
                        echo Html::a('<i class="fa fa-print fa-2x"></i>', ['declaration-print',
                                'actionType' => 'print',
                                'id' => $model->id,
                                'empty' => $isEmptyDeclaration,
                                'filename' => $model->getPrintTitle()], [
                            'class' => 'btn darkblue widthe-100 hidden-lg',
                            'target' => '_blank',
                            'title' => 'Печать',
                            'data' => [
                                'pjax' => '0',
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                        <style>
                            .dropdown-menu-mini {
                                width: 100%;
                                min-width: 200px;
                                border-color: #4276a4 !important;
                            }

                            .dropdown-menu-mini a {
                                padding: 7px 0px;
                                text-align: center;
                            }
                        </style>
                        <span class="dropup">
                            <?= Html::a('Скачать', '#', [
                                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs dropdown-toggle dropdown-linkjs',
                                'data-toggle' => 'dropdown',
                            ]); ?>
                            <?= Html::a('<i class="glyphicon glyphicon-download" style="font-size: 17px;"></i>', '#', [
                                'class' => 'btn darkblue widthe-100 hidden-lg dropdown-toggle dropdown-linkjs',
                                'data-toggle' => 'dropdown'
                            ]); ?>
                            <?= \yii\bootstrap\Dropdown::widget([
                                'options' => [
                                    'class' => 'dropdown-menu-mini min-w-190 dropdown-centerjs'
                                ],
                                'items' => [
                                    [
                                        'label' => 'Скачать PDF файл',
                                        'encode' => false,
                                        'url' => ['declaration-print',
                                            'actionType' => 'pdf',
                                            'id' => $model->id,
                                            'empty' => $isEmptyDeclaration,
                                            'filename' => $model->getPdfFileName()],
                                        'linkOptions' => [
                                            'target' => '_blank',
                                            'data-pjax' => 0
                                        ]
                                    ],
                                    [
                                        'label' => 'Скачать файл для ИФНС',
                                        'encode' => false,
                                        'url' => ['/tax/declaration/xml',
                                            'id' => $model->id,
                                            'empty' => $isEmptyDeclaration
                                        ],
                                        'linkOptions' => [
                                            'download' => 'download',
                                            'data-pjax' => 0
                                        ]
                                    ],
                                ],
                            ]); ?>
                        </span>
                    </div>
                    <div class="button-bottom-page-lg button-bottom-page-lg-double col-sm-2 col-xs-2">
                        <?= Html::a('<span class="hidden-xs">Книга учета доходов и расходов</span><span class="hidden-sm hidden-md hidden-lg">КУДиР</span>',
                            (!$taxRobot->isPaid) ? '#' : ['/tax/kudir/' . ($taxation->psn ? 'document-print-patent' : 'document-print'),
                                'actionType' => 'pdf',
                                'id' => $kudir->id,
                                'filename' => $kudir->getPdfFileName(),
                                'period' => $taxRobot->period->id
                            ], [
                            'class' => 'btn darkblue darkblue-invert tooltip-hover kudir-btn',
                            'data-tooltip-content' => '#tooltip_kudir',
                            'data' => [
                                'pjax' => '0',
                            ],
                            'target' => '_blank',
                            'disabled' => (!$taxRobot->isPaid) ? true : false
                        ]); ?>
                    </div>
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    </div>
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                        <?php /*
                        <?php if (Yii::$app->user->can(permissions\document\Document::DELETE)): ?>
                            <?= Html::button('Удалить', [
                                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                                'data-toggle' => 'modal',
                                'href' => '#delete-confirm',
                            ]); ?>
                            <?= Html::button('<i class="fa fa-trash-o fa-2x"></i>', [
                                'class' => 'btn darkblue widthe-100 hidden-lg',
                                'data-toggle' => 'modal',
                                'href' => '#delete-confirm',
                                'title' => 'Удалить',
                            ]); ?>
                        <?php endif; ?>
                        */ ?>
                    </div>
                </div>

                <?php if (Yii::$app->user->can(permissions\document\Document::DELETE)) {
                    echo ConfirmModalWidget::widget([
                        'options' => [
                            'id' => 'delete-confirm',
                        ],
                        'toggleButton' => false,
                        'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id]),
                        'confirmParams' => [],
                        'message' => "Вы уверены, что хотите удалить эту декларацию?",
                    ]);
                }; ?>
            </div>
        </div>

        <div class="tooltip-template hidden">
            <span class="tooltip-kudir-body text-top" id="tooltip_kudir">
                КУДиР не нужно отправлять в налоговую, но следует хранить 4 года и <br/>предоставлять по требованию налоговой и других контролирующих органов.<br/>
                Поэтому распечатайте её сейчас либо вы это можете сделать позже.
            </span>
        </div>
    </div>
</div>

<?php \yii\widgets\Pjax::end(); ?>

<?= $this->registerJs('
    $(document).ready(function (e) {
        var $left = Math.ceil((+$(".dropdown-centerjs").width() - +$(".dropdown-linkjs:visible").width()) / 2);
        console.log($left);
        $(".dropdown-centerjs").css("left", "-" + $left + "px");
    });
'); ?>
