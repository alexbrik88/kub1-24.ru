<?php

use common\components\widgets\BikTypeahead;
use common\components\widgets\IfnsTypeahead;
use common\models\address\AddressDictionary;
use common\models\Company;
use common\models\company\CheckingAccountant;
use backend\models\Bank;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/* @var $this yii\web\View */
/* @var $model Company */
/* @var $account CheckingAccountant */

$this->title = 'Укажите код ОКАТО и номер ИФНС';

$this->registerJs('
function previousValue(currentLevel) {
    var prevValue = "",
        prevLevel = 0;
    $(".js-ifns-search").each(function(i, item) {
        var itemLevel = parseInt($(item).data("aolevel"));
        if (item.value && itemLevel < currentLevel && itemLevel > prevLevel) {
            prevValue = item.value;
            prevLevel = itemLevel;
        }
    });

    return prevValue;
}
function disableAllLevels() {
    $(".js-ifns-search").each(function(i, item) {
        var itemLevel = $(item).attr("data-aolevel");
        if (itemLevel > 1)
            $(item).select2("enable", false);
    });
}
function enableNextLevel(currLevel) {
    $(".js-ifns-search").each(function(i, item) {
        var itemLevel = $(item).attr("data-aolevel");
        if (itemLevel > currLevel) {
            $(item).select2("enable");
            return false;
        }
    });
}
', $this::POS_HEAD);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-click',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>

<?= $this->render('_style') ?>

<?php $form = ActiveForm::begin([
    'id' => 'form-update-company',
    'fieldClass' => 'common\components\floatLabelField\FloatLabelActiveField',
    'fieldConfig' => [
        'template' => "{input}\n{label}\n{error}\n",
    ],
    'options' => [
        'class' => 'form-md-underline'
    ]
]); ?>

<div class="row">
    <div class="hidden-xs hidden-sm hidden-md col-xs-12 step-by-step">

        <?= $this->render('_steps', ['step' => 2]) ?>

        <div class="pad_sm_t" style="padding-top: 10px">
            <div class="col-xs-12 col-lg-12 pad0">

                <!-- step 2 -->
                <div class="col-xs-12 pad0 step-by-step-form" style="min-height: auto;">
                    <div class="col-xs-12 pad0 bord-light-b" style="margin-bottom:25px">
                        <span class="marg pad10-b font-bold" style="font-size:16px">Шаг 2: Укажите код ОКАТО и номер ИФНС для платежки на уплату налога</span>
                    </div>
                    <div class="row">
                        <!--LEFT-->
                        <div class="col-xs-7">

                            <div class="row">
                                <div class="col-xs-12 col-md-6 mb5">
                                    <?= $form->field($model, 'okato')->textInput(['maxlength' => true])->label('Код ОКАТО'); ?>
                                    <span class="tooltip2-click valign-middle ico-question sbs-tooltip" data-tooltip-content="#tooltip_what_is_okato"></span>
                                </div>
                                <div class="col-xs-12 col-md-6 mb5">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6 mb5">
                                    <?= $form->field($model, 'ifns_ga')->textInput([
                                        //$form->field($model, 'ifns_ga')->widget(IfnsTypeahead::className())->textInput([
                                        'class' => 'form-control input-sm dictionary-ifns',
                                        'maxlength' => true
                                    ])->label('Номер ИФНС'); ?>
                                    <span class="help-block dictionary-ifns-tip" data-ifns="" style="display:block!important;"></span>
                                    <span class="tooltip2-click valign-middle ico-question sbs-tooltip" data-tooltip-content="#tooltip_what_is_ifns"></span>
                                </div>
                                <div class="col-xs-12 col-md-6 mb5">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-md-12" style="margin-top:20px; margin-bottom:20px;">
                                    <span class="font-bold" style="display: inline-block; font-size:16px">
                                        Поиск кода ОКАТО и номера ИФНС по адресу регистрации ИП
                                    </span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-md-6 mb1">
                                    <div class="form-group form-md-underline">
                                        <label> Субъект РФ: </label>
                                        <?= $this->render('parts_ifns/_ifns_input', ['name' => 'subject', 'level' => 1]) ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6 mb1">
                                    <div class="form-group form-md-underline">
                                        <label> Район: </label>
                                        <?= $this->render('parts_ifns/_ifns_input', ['name' => 'area', 'level' => 3]) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6 mb1">
                                    <div class="form-group form-md-underline">
                                        <label> Город: </label>
                                        <?= $this->render('parts_ifns/_ifns_input', ['name' => 'city', 'level' => 4]) ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6 mb1">
                                    <div class="form-group form-md-underline">
                                        <label> Населенный пункт: </label>
                                        <?= $this->render('parts_ifns/_ifns_input', ['name' => 'toun', 'level' => 6]) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6 mb1">
                                    <div class="form-group form-md-underline">
                                        <label> Улица: </label>
                                        <?= $this->render('parts_ifns/_ifns_input', ['name' => 'street', 'level' => 7]) ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6 mb1">
                                    <div class="form-group form-md-underline">
                                        <label> Номер здания/сооружения: </label>
                                        <?= $this->render('parts_ifns/_ifns_input', ['name' => 'building', 'level' => 8]) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6 mb1">
                                    <?= $form->field($model, 'address_legal_postcode')->label('Индекс')->textInput(); ?>
                                </div>
                                <div class="col-xs-12 col-md-6 mb1">
                                    <?= Html::button('Поиск', [
                                        'id' => 'get-fias-ittem',
                                        'class' => 'btn green-haze full_w',
                                        'style' => 'margin-top:14px',
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- buttons -->
                <div class="col-xs-12 pad0 buttons-block" style="padding-top:55px !important">
                    <?= Html::a('Вернуться', ['company'], [
                        'class' => 'btn darkblue',
                        'style' => 'min-width: 180px;'
                    ]) ?>
                    <?= Html::submitButton('Далее', [
                        'id' => 'okato-ifns-submit',
                        'class' => 'btn darkblue pull-right',
                        'disabled' => empty($model->okato) || empty($model->ifns_ga),
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $form->end(); ?>

<?php Modal::begin([
    'id' => 'fias-result-modal',
    'header' => '<h1>Код ОКАТО и номер ИФНС</h1>',
    'toggleButton' => false,
]); ?>

<div class="fias-result fias-result-error hidden" style="text-align: center;">
    <h4>Адрес не найден</h4>
</div>
<div class="fias-result fias-result-success hidden">
    <div class="row">
        <div class="col-sm-6" style="max-width: 70px;"><span style="font-weight: bold;">ОКАТО:</span></div>
        <div class="col-sm-6">
            <span id="fias-result-okato" class="fias-result-value"></span>
            <a href="#" id="copy-fias-okato" class="fias-result hidden" onclick="$('#company-okato').val($('#fias-result-okato').text())">
                Сохранить
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6" style="max-width: 70px;"><span style="font-weight: bold;">ИФНС:</span></div>
        <div class="col-sm-6">
            <span id="fias-result-ifns" class="fias-result-value"></span>
            <a href="#" id="copy-fias-ifns" class="fias-result hidden" onclick="$('#company-ifns_ga').val($('#fias-result-ifns').text())">
                Сохранить
            </a>
        </div>
    </div>
    <div style="text-align: center;margin-top:15px">
        <?= Html::button('Сохранить', [
            'id' => 'copy-fias-result',
            'class' => 'btn darkblue',
            'style' => 'color: #fff;',
        ]); ?>
    </div>
</div>

<div class="tooltip_templates" style="display: none;">
    <span id="tooltip_search_okato" style="display: inline-block; text-align: center;">
        Заполните адрес регистрации ИП, по данному адресу будет найден ваш код ОКАТО и номер ИФНС.
    </span>
    <span id="tooltip_what_is_okato" style="display: inline-block; text-align: center;">
        <p> <strong> Зачем нужен ОКАТО? </strong></p>
        <p> ОКАТО - Общероссийский классификатор объектов  <br>
            административно-территориального деления. Код ОКАТО предназначен для обеспечения достоверности, <br>
            сопоставимости и автоматизированной обработки информации в разрезах административно-территориального <br/>деления в таких сферах, как статистика, экономика и другие.
        </p>
    </span>
    <span id="tooltip_what_is_ifns" style="display: inline-block; text-align: center;">
        <p> <strong> Зачем нужен номер ИФНС? </strong></p>
        <p> Номер ИФНС - это номер Инспекционной Федеральной Налоговой Службы в которой вы регистрировали свое ИП. <br/> Данный номер необходим для заполнения реквизитов налоговой в платежке на уплату налогов.</p>
    </span>
</div>

<?php Modal::end(); ?>

<?php $this->registerJs('
$(document).on("change", ".js-ifns-search", function() {
    $(".fias-result").addClass("hidden");
    $(".fias-result-value").html("");
    var level = parseInt($(this).data("aolevel"));
    $(".js-ifns-search").each(function(i, item) {
        if (parseInt($(item).data("aolevel")) > level) {
            $(item).val("").trigger("change");
            $(item).select2("enable", false);
        }
    });
    enableNextLevel(level);
});
$(document).on("change", "#company-okato, #company-ifns_ga", function() {
    if ($("#company-okato").val() && $("#company-ifns_ga").val()) {
        $("#okato-ifns-submit").attr("disabled", false);
    } else {
        $("#okato-ifns-submit").attr("disabled", true);
    }
});
$("#fias-result-modal").on("hidden.bs.modal", function () {
    $(".fias-result").addClass("hidden");
});
$(document).on("click", "#copy-fias-result", function(e) {
    e.preventDefault();
    var okato = ("нет данных" != $("#fias-result-okato").text()) ? $("#fias-result-okato").text() : "";
    var ifns  = ("нет данных" != $("#fias-result-ifns").text())  ? $("#fias-result-ifns").text() : "";
    $("#company-okato").val(okato);
    $("#company-ifns_ga").val(ifns);
    $("#fias-result-modal").modal("hide");
    // update tip
    var q = $("#fias-result-ifns").text();
    postIfns(q);   
});
$(document).on("click", "#get-fias-ittem", function(e) {
    e.preventDefault();
    $(".fias-result").addClass("hidden");
    $(".fias-result-value").html("");
    $.ajax({
        url:"/dictionary/fias-item",
        data: {guid: previousValue(9)},
        success: function(data) {
            if (data.result) {
                console.log(data.result);
                $("#fias-result-okato").html(data.result.OKATO || "нет данных");
                $("#fias-result-ifns").html(data.result.IFNSUL || "нет данных");
                $(".fias-result-success").removeClass("hidden");
            } else {
                console.log("error");
                $(".fias-result-error").removeClass("hidden");
            }
            $("#fias-result-modal").modal("show");
        }
    });
});

function postIfns(q) {
    $.post( "/dictionary/ifns/?q=" + q, function( data ) {
      if (data.length > 0) {
          var first_ifns = data[0];
          $(".dictionary-ifns-tip").html(first_ifns.ga + " " + first_ifns.gb);
          $(".dictionary-ifns-tip").data("ifns", first_ifns.ga);
           console.log($(".dictionary-ifns-tip").data("ifns"));
      } else {
          $(".dictionary-ifns-tip").html("ИФНС не найден. Проверьте введённые данные.");
          $(".dictionary-ifns-tip").data("ifns", "");
      }
    });
}
$(".dictionary-ifns").on("input", function(e) {

    var q = $(this).val();

    if (q.trim().length > 0) {
        $("#company-ifns_ga").parents(".form-group").find(".help-block").html("");
        $(".dictionary-ifns-tip").show(200);
        postIfns(q);
    } else {
        $(".dictionary-ifns-tip").hide(200);
    }

});
$(".dictionary-ifns-tip").bind("click", function() {
    var ifns = $(this).data("ifns");
    if (ifns)
        $("#company-ifns_ga").val(ifns);
});
$(document).ready(function() {
    var q = $(".dictionary-ifns").val();
    if (q.trim().length > 0) {
        postIfns(q);
    }

    disableAllLevels();

});


') ?>