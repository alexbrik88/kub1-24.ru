<?php

use common\components\TaxRobotHelper;
use common\models\company\CompanyType;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\Html;
/** @var $company \common\models\Company */

$THE_YEAR = 2019;
$SHOW_PER_LOGINS = 5;

$showOnStartPage = date('Ymd') < "20200425";
$user = (!\Yii::$app->user->isGuest) ? Yii::$app->user->identity : null;
$company = ($user) ? $user->company : null;
$presentOnPage = isset($onStartPage) && Yii::$app->getRequest()->url == '/tax/robot/bank';
$hide = Yii::$app->request->get('reduce') == 'hide';
?>

<?php IF (FALSE): // DISABLED AT 24.04.2020  ?>

<?php if (!$hide && $company && !$presentOnPage): ?>

<div id="kub-worth-modal" class="confirm-modal fade modal"
     role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <?php if (isset($onStartPage)): ?>
                <div class="txt-block">
                    <div class="txt-head">
                        Для <?= $company->getShortName() ?>
                    </div>
                    <div class="txt-body">
                        <span class="colored">Уменьшим</span> Ваши налоги <br/>
                        за <?= $THE_YEAR ?> год <span class="colored">минимум на <span class="saved-sum">0</span> ₽</span>. <br/>
                        Подробности <br/>
                        в разделе "Бухгалтерия ИП"
                    </div>
                </div>
            <?php else: ?>
                <div class="txt-block">
                    <div class="txt-head">
                        Для <?= $company->getShortName() ?>
                    </div>
                    <div class="txt-body">
                        <span class="colored">Уменьшим</span> Ваши налоги <br/>
                        за <?= $THE_YEAR ?> год <span class="colored">минимум на <span class="saved-sum">0</span> ₽</span>, <br/>
                        уже на следующем шаге!
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php $this->registerJs('
function getSavedSumOnTaxes(taxSum)
{
    var savedSum = "";

    if (taxSum <= 1E5) {
        savedSum = "";
    } else if (taxSum <= 3E5) {
        savedSum = "6 000";
    } else if (taxSum <= 5E5) {
        savedSum = "18 000";
    } else if (taxSum <= 1E6) {
        savedSum = "30 000";
    } else if (taxSum <= 3E6) {
        savedSum = "43 000";
    } else if (taxSum <= 5E6) {
        savedSum = "62 000";
    } else if (taxSum <= 10E6) {
        savedSum = "80 000";
    } else if (taxSum <= 20E6) {
        savedSum = "133 000";
    } else if (taxSum <= 30E6) {
        savedSum = "230 000";
    } else if (taxSum <= 40E6) {
        savedSum = "320 000";
    } else if (taxSum <= 50E6) {
        savedSum = "400 000";
    } else if (taxSum <= 60E6) {
        savedSum = "500 000";
    } else if (taxSum <= 80E6) {
        savedSum = "600 000";
    } else {
        savedSum = "800 000";
    }

    return savedSum;
}

$(document).on("click", ".taxrobot-pay-panel-trigger", function (e) {
    e.preventDefault();
    var taxSum = parseInt($("#totalTaxSum").attr("data-sum") / 100);
    var savedSum = getSavedSumOnTaxes(taxSum);
    if (savedSum && window.matchMedia("only screen and (min-width: 1024px)").matches) {
        $("#kub-worth-modal .saved-sum").html(savedSum);
        $("#kub-worth-modal").modal("show");
    }
});') ?>

<?php if (isset($onStartPage) && $showOnStartPage) {
    $taxRobot = new TaxRobotHelper($company, $user);
    $isCanView = ($company->company_type_id == CompanyType::TYPE_IP && $company->getCanTaxModule());

    if ($isCanView && !$taxRobot->getIsPaid()) {

        $isViewedInSession = Yii::$app->session->get('ip_usn_6_saved_tax_modal_showed', 0);
        $isViewedInCookie = Yii::$app->request->cookies->getValue('ip_usn_6_saved_tax_modal_showed', $SHOW_PER_LOGINS);

        // Test
        if (Yii::$app->request->get('savetax_popup')) {
            $isViewedInSession = false;
            $isViewedInCookie = 999;
        }

        if (!$isViewedInSession) {

            if ($isViewedInCookie >= $SHOW_PER_LOGINS) {

                $taxRobot->setPeriod("{$THE_YEAR}_4");
                $taxSum = round(1 / 100 * $taxRobot->getIncomeSearchQuery()->andWhere(['is_taxable' => true])->sum('amount'));
                if ($taxSum > 100000) {

                    Yii::$app->response->cookies->add(new \yii\web\Cookie([
                        'name' => 'ip_usn_6_saved_tax_modal_showed',
                        'value' => 1,
                        'expire' => 0
                    ]));

                    $this->registerJs('
                    $(document).ready(function () {
                        var taxSum = ' . (int)$taxSum . ';
                        var savedSum = getSavedSumOnTaxes(taxSum);
                        if (savedSum) {
                            $("#kub-worth-modal .saved-sum").html(savedSum);
                            $("#kub-worth-modal").modal("show");
                        }
                    });
                    $("#kub-worth-modal .modal-dialog").on("click", function() {
                        location.href = "/tax/robot/index?from_savetax_popup=1";
                    });
                ');
                }
            } else {

                Yii::$app->response->cookies->add(new \yii\web\Cookie([
                    'name' => 'ip_usn_6_saved_tax_modal_showed',
                    'value' => ++$isViewedInCookie,
                    'expire' => 0
                ]));
            }

            Yii::$app->session->set('ip_usn_6_saved_tax_modal_showed', 1);
        }
    }

} ?>

<style>
    #kub-worth-modal {
        width: 670px !important;
    }
    #kub-worth-modal .modal-content {
        position: relative;
        background-image: url(/img/kub-worth-bg.png);
        height: 430px;
    }
    #kub-worth-modal .close {
        margin: 10px;
        opacity: 1;
    }
    #kub-worth-modal .txt-block {
        padding: 25px 38px;
        font-size: 22px;
    }
    #kub-worth-modal .txt-head {
        color: #3e3d5d;
        font-weight: bold;
        font-size: 36px;
        margin-bottom: 5px;
    }
    #kub-worth-modal .colored {
        color: #5e379e;
        font-weight: bold;
    }

    <?php if (isset($onStartPage)): ?>
        #kub-worth-modal .modal-content {
            background-image: url(/img/kub-worth-start-bg.png);
        }
    <?php else: ?>
        @media (max-width: 1600px) {
            #kub-worth-modal {
                margin-left: -540px!important;
            }
        }
    <?php endif; ?>
</style>

<?php endif; ?>

<?php ENDIF; ?>
