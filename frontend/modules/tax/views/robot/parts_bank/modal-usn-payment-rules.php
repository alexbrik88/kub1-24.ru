<?php

use yii\helpers\Html;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>
<div class="odds-panel">
    <div class="main-block">
        <p class="bold" style="font-size:19px;">
            Какие приходы не нужно учитывать в УСН
        </p>

        <p>
            Доходы Индивидуального Предпринимателя всегда увеличивают сумму налога. Однако не все приходы на расчетный счёт и в кассу являются доходом:
        </p>

        <ul>
            <li>поступления на расчетные счёта ИП личных денег;</li>
            <li>возвраты платежей от поставщиков;</li>
            <li>полученные кредиты и займы;</li>
            <li>возврат займов, которые выдавал сам ИП;</li>
            <li>гранты;</li>
            <li>обеспечительный платёж (например, при сдаче недвижимости в аренду) или залог;</li>
            <li>деньги, полученные ИП как агентом по агентскому договору, кроме агентского вознаграждения;</li>
            <li>доходы от бизнеса на другой системе налогообложения, если вы совмещаете УСН с ЕНВД или патентом;</li>
            <li>другие доходы из ст. 251 НК.</li>
        </ul>

        <p>
            Данные поступления не должны  учитываться при расчёте налога УСН.
        </p>

        <br/>

        <p class="bold">
            Остались вопросы?
        </p>

        <p>
            Напишите нам на почту: <a href="mailto:support@kub-24.ru">support@kub-24.ru</a>
        </p>

        <div style="height: 50px">&nbsp;</div>

    </div>
    <span class="side-panel-close" title="Закрыть">
        <span class="side-panel-close-inner"></span>
    </span>
</div>