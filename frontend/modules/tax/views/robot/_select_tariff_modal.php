<?php

use common\models\service\PaymentType;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\modules\subscribe\forms\PaymentForm;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\modules\cash\modules\banking\components\Banking;

$company = $taxRobot->company;

$icons = [
    SubscribeTariffGroup::STANDART => 'tariff_1.png',
    SubscribeTariffGroup::LOGISTICS => 'tariff_2.png',
    SubscribeTariffGroup::B2B_PAYMENT => 'tariff_3.png',
    SubscribeTariffGroup::TAX_DECLAR_IP_USN_6 => 'tariff_4.svg',
    SubscribeTariffGroup::ANALYTICS => 'tariff_5.png',
    SubscribeTariffGroup::OOO_OSNO_NULL_REPORTING => 'tariff_6.png',
    SubscribeTariffGroup::TAX_IP_USN_6 => 'tariff_7.png',
];
$needSelect = count($groupIds) > 1;
?>

<div class="page-shading-panel taxrobot-pay-panel-close hidden"></div>
<div class="taxrobot-pay-panel">
    <span class="side-panel-close taxrobot-pay-panel-close" title="Закрыть">
        <span class="side-panel-close-inner"></span>
    </span>
    <div class="main-block" style="height: 100%; position: relative;">
        <?php if ($needSelect) : ?>
            <div class="main-content pt-0">
                <div class="tariff-block-wrapper mt-0" style="font-size: 18px;">
                    <strong>Выберите тариф</strong>
                </div>
                <?php foreach ($groupIds as $groupId) : ?>
                    <?php
                    $group = SubscribeTariffGroup::findOne($groupId);
                    $tariff = $group->getActualTariffs()->orderBy(['price' => SORT_ASC])->one();
                    $discount = $company->getMaxDiscount($tariff->id);
                    $discVal = $discount ? $discount->value : 0;
                    $price = round(max(0, $tariff->price - ($tariff->price * $discVal / 100)));
                    ?>
                    <div class="tariff-block-wrapper" data-group="<?= $groupId ?>">
                        <div class="header">
                            <table style="width: 100%; height: ">
                                <tr>
                                    <td style="width: 10%; vertical-align: middle; text-align: center;">
                                        <?= Html::tag('div', Html::img('/img/icons/'.$icons[$groupId], [
                                            'alt' => '',
                                        ]), [
                                            'style' => '
                                                display: inline-flex;
                                                align-items: center;
                                                justify-content: center;
                                                width:60px;
                                                height:60px;
                                                background-color:#4276a4;
                                                border-radius:30px!important;
                                            ',
                                        ]) ?>
                                    </td>
                                    <td style="padding-left: 10px; font-size: 18px;">
                                        <?= implode('<br>ИП', explode(' ИП', $group->name)) ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="padding-top: 30px;">
                            <div class="text-bold">
                                <div style="display: inline-block; font-size: 18px;">
                                    Оплата за <?= $tariff->getTariffName() ?>
                                </div>
                                <table>
                                    <tr>
                                        <td style="font-size: 44px;"><?= $price ?>&nbsp₽</td>
                                        <td class="pad-l-10">
                                            <?php if ($discount) : ?>
                                                <div>
                                                    <del><?= $tariff->price ?> ₽</del>
                                                    <span style="color: red;">СКИДКА <?=round($discVal)?>%</span>
                                                </div>
                                                <div>
                                                    при оплате до <?= date('d.m.Y', $discount->active_to) ?>
                                                </div>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <?= round($price / $tariff->duration_month) ?>
                                            ₽ / месяц
                                        </td>
                                    </tr>
                                </table>
                                <button  class="widthe-100 btn darkblue-invert darkblue-invert-hover-blue tariff-block-button" data-group="<?= $groupId ?>">Выбрать</button>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
                Чтобы получить <span style="text-decoration: underline">расчет налога</span>,
                <span style="text-decoration: underline">готовые платежки для уплаты</span> и
                <span style="text-decoration: underline">заполненную декларацию</span>, выберите один из тарифов.
            </div>
        <?php endif ?>
        <?php foreach ($groupIds as $groupId) : ?>
            <?= $this->render('_select_tariff_form', [
                'taxRobot' => $taxRobot,
                'groupId' => $groupId,
                'needSelect' => $needSelect,
            ]) ?>
        <?php endforeach ?>
    </div>
</div>

<?php
if (Yii::$app->request->get('pay') && !$taxRobot->getIsPaid()) {
$this->registerJs('
    taxrobotPayPanelOpen();
');
}