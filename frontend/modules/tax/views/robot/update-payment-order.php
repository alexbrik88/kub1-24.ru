<?php

use common\components\date\DateHelper;
use common\components\TaxRobotHelper;
use common\components\widgets\BikTypeahead;
use common\models\cash\CashBankFlows;
use common\models\company\CompanyType;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use common\models\dictionary\bik\BikDictionary;
use common\models\document\OperationType;
use common\models\document\PaymentDetails;
use common\models\document\PaymentType;
use common\models\document\TaxpayersStatus;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $company common\models\Company */
/* @var $form yii\bootstrap\ActiveForm */

//$this->title = 'Обновить платёжное поручение: ' . ' ' . $model->document_number;
$this->params['breadcrumbs'][] = ['label' => 'Payment Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
$this->context->layoutWrapperCssClass = 'out-act out-document payment-order';

$rsItems = [];
$rsOptions = [];
$accountsArray = $model->company->getCheckingAccountants()
    ->andWhere(['not', ['type' => CheckingAccountant::TYPE_CLOSED]])
    ->orderBy(['type' => SORT_ASC])
    ->all();

foreach ($accountsArray as $account) {
    $rsItems[$account->rs] = $account->rs;
    $rsOptions[$account->rs] = [
        'data' => [
            'bank' => $account->bank_name,
            'city' => $account->bank_city,
            'bik' => $account->bik,
            'ks' => $account->ks,
        ]
    ];
}
?>
<div class="payment-order-update m-t-n-lg">

    <div class="portlet box">
        <h3 class="page-title"><?= Html::encode($this->title); ?></h3>
    </div>

    <?php

    $form_request = \Yii::$app->request->post($model->formName());

    if (isset($form_request['document_date_budget_payment'])) {
    $document_date_budget_payment_value = $form_request['document_date_budget_payment'];
    } else {
    if (!$model->isNewRecord && $model->document_date_budget_payment === null && $model->presence_status_budget_payment) {
    $document_date_budget_payment_value = 0;
    } else {
    $document_date_budget_payment_value = DateHelper::format($model->document_date_budget_payment, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
    }
    }

    $taxRobot = new TaxRobotHelper($company);

    $companyIfns = Json::encode($model->company->ifns);

    $this->registerJs("
    PaymentOrder.ifns = {$companyIfns};
    ");

    ?>
    <?php $form = ActiveForm::begin([
        'id' => 'asd',
        'fieldConfig' => [
            'template' => "{label}\n{input}",
        ],
    ]); ?>
    <div class="form-body">
        <?= Html::activeHiddenInput($model, 'invoiceIDs'); ?>

        <?= $form->errorSummary($model); ?>

        <?= $form->field($model, 'presence_status_budget_payment')->label('Бюджетный платёж', [
            'class' => 'checkbox-inline',
        ])->checkbox([
            'id' => 'budget-payment-checkbox',
            'class' => 'm-l-1'
        ]) ?>

        <div class="portlet customer-info">
            <div class="portlet-title">

                <div class="col-md-10 caption">

                    <?= $form->field($model, 'document_number')->label('платежное поручение №', [
                        'class' => 'control-label bold-text',
                        'style' => 'font-size: 21px',
                    ])->textInput([
                        'maxlength' => 45,
                        'class' => 'form-control field-width pad-n',
                        'style' => 'width: 130px',
                        'value' => $model->document_number,
                    ]) ?>

                </div>
                <div class="actions">
                    <div class="height-30 enter-data pull-left">
                        <?= $form->field($model, 'taxpayers_status_id')->dropDownList(ArrayHelper::map(TaxpayersStatus::find()->andWhere(['not', ['id' => TaxpayersStatus::$deprecated]])->all(), 'id', 'name'), [
                            'class' => 'form-control js_select',
                            'prompt' => '',
                            'data-default-value' => $model->company->company_type_id == CompanyType::TYPE_IP ?
                                TaxpayersStatus::DEFAULT_IP :
                                TaxpayersStatus::DEFAULT_OOO,
                        ])->label(false); ?>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-offset-7">
                        <div class="data-block pull-left document_date">
                            <?= $form->field($model, 'document_date')->textInput([
                                'class' => 'date-picker',
                                'size' => 16,
                                'data' => [
                                    'date-viewmode' => 'years',
                                ],
                                'value' => DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                            ])->label(false); ?>

                            <div class="data-block__title">
                                дата
                            </div>
                        </div>
                        <div class="data-block pull-left">
                            <div class="data-block__data">

                            </div>
                            <div class="data-block__title">
                                вид платежа
                            </div>
                        </div>
                    </div>
                </div>
                <div class="summery clearfix">
                    <div class="height-30 summery__sum pull-left">
                        Сумма прописью
                    </div>
                    <div class="height-30 summery__sum-in-words pull-left"></div>
                </div>
                <div class="summery clearfix">
                </div>
                <div class="clearfix payment-order__table">
                    <div class="width-60 left-block pull-left">
                        <div class="width-50 height-30 pull-left border-bootom-right">ИНН <?= $company->inn; ?></div>
                        <div class="width-50 height-30 pull-left border-bootom">КПП <?= $company->kpp; ?></div>
                        <div class="width-100 pull-left">
                            <div class="height-60">
                                <?= $company->getTitle($company->company_type_id == CompanyType::TYPE_IP ? false : true); ?>
                            </div>
                            <div class="height-30 bold-text border-bootom">Плательщик</div>
                        </div>
                        <div class="width-100 pull-left">
                            <div class="height-30">
                                <?= $form->field($model, 'company_bank_name')->textInput([
                                    'readonly' => true,
                                ])->label(false); ?>
                                <?= $form->field($model, 'company_bank_city')->hiddenInput()->label(false); ?>
                            </div>
                            <div class="height-30 bold-text border-bootom">Банк плательщика</div>
                        </div>
                        <div class="width-100 pull-left">
                            <div class="height-30">
                                <?= $form->field($model, 'contractor_bank_name')->textInput([
                                    'readonly' => true,
                                    'class' => 'form-control',
                                ])->label(false); ?>
                                <?= $form->field($model, 'contractor_bank_city')->hiddenInput()->label(false); ?>
                            </div>
                            <div class="height-30 border-bootom">
                                <?= Html::activeLabel($model, 'contractor_bank_name', [
                                    'label' => 'Банк получателя',
                                    'class' => 'control-label bold-text',
                                ]); ?>
                            </div>
                        </div>
                        <div class="width-50 height-30 pull-left border-bootom-right">
                            <?= $form->field($model, 'contractor_inn')->label('ИНН', [
                                'class' => 'control-label',
                            ])->textInput([
                                'maxlength' => true,
                                'class' => 'form-control field-width contractor_bank_config',
                                'style' => 'width: 85%; padding-bottom: 2px;',
                            ]) ?>
                        </div>
                        <div class="width-50 height-30 pull-left border-bootom">
                            <?= $form->field($model, 'contractor_kpp')->label('КПП', [
                                'class' => 'control-label',
                            ])->textInput([
                                'maxlength' => true,
                                'class' => 'form-control field-width contractor_bank_config',
                                'style' => 'width: 85%; padding-bottom: 2px;',
                            ]) ?>
                        </div>
                        <div class="width-100 pull-left">
                            <div class="height-90">
                                <?= $form->field($model, 'contractor_name', [
                                    'options' => [
                                        'class' => 'form-group contractor-text-name' .
                                            ($model->relation_with_in_invoice ? ' hidden' : null),
                                    ],
                                ])->textInput([
                                    'class' => 'form-control',
                                ])->label(false); ?>

                                <?= $form->field($model, 'contractor_id', ['template' => "{input}", 'options' => [
                                    'class' => 'contractor-list-name' .
                                        (!$model->relation_with_in_invoice ? ' hidden' : null),
                                    'style' => 'width: 98%;',
                                ]])->widget(Select2::classname(), [
                                    'data' => Contractor::getALLContractorList(Contractor::TYPE_SELLER, false),
                                    'options' => [
                                        'class' => 'form-control contractor-select',
                                        'disabled' => !$model->relation_with_in_invoice,
                                    ],
                                ]); ?>
                            </div>
                            <div class="height-30">
                                <?= Html::activeLabel($model, 'contractor_name', [
                                    'label' => 'Получатель',
                                    'class' => 'control-label bold-text',
                                ]); ?>
                            </div>
                        </div>
                    </div>
                    <div class="width-40 right-block pull-left border-left">
                        <div class="width-30 height-60 pull-left border-bootom-right">

                            <?= Html::activeLabel($model, 'sum', [
                                'label' => 'Сумма',
                                'class' => 'control-label',
                            ]); ?>

                        </div>
                        <div class="width-70 height-60 pull-left border-bootom">

                            <?= $form->field($model, 'sum')->textInput([
                                'class' => 'summery__sum-in-digits form-control js_input_to_money_format pad-n',
                                'value' => $model->sum / 100,
                                'data-default-value' => $taxRobot->getTaxPayableAmount(),
                            ])->label(false); ?>

                        </div>
                        <div class="width-30 height-60 pull-left border-bootom-right">Сч №</div>
                        <div class="width-70 height-60 pull-left">
                            <?= $form->field($model, 'company_rs', [
                                'template' => "{input}",
                                'options' => [
                                    'class' => '',
                                ],
                            ])->widget(Select2::classname(), [
                                'data' => $rsItems,
                                'hideSearch' => true,
                                'pluginOptions' => [
                                    'templateResult' => new JsExpression('function (data, container) {
                                        if (data.id && data.element) {
                                            container.innerHTML = $(data.element).data("bank") + "<br>" + data.id;
                                        }
                                        return container;
                                    }'),
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                    'data-update-url' => Url::to(['update-checking-accountant']),
                                    'data-create-url' => Url::to(['create-checking-accountant']),
                                    'options' => $rsOptions,
                                ]
                            ])->label(false); ?>
                        </div>

                        <div class="width-30 height-30 pull-left border-bootom-right">БИК</div>

                        <div class="width-70 height-30 pull-left">
                            <?= $form->field($model, 'company_bik')->textInput([
                                'readonly' => true,
                            ])->label(false); ?>
                        </div>
                        <div class="width-30 height-30 pull-left border-bootom-right">Сч №</div>
                        <div class="width-70 height-30 pull-left border-bootom">
                            <?= $form->field($model, 'company_ks')->textInput([
                                'readonly' => true,
                            ])->label(false); ?>
                        </div>

                        <div class="width-30 height-30 pull-left border-bootom-right">
                            <?= Html::activeLabel($model, 'contractor_bik', [
                                'label' => 'БИК',
                                'class' => 'control-label',
                            ]); ?>
                        </div>
                        <div class="width-70 height-30 pull-left border-bootom">
                            <?= $form->field($model, 'contractor_bik')->label(false)->widget(BikTypeahead::classname(), [
                                'remoteUrl' => Url::to(['/dictionary/bik']),
                                'related' => [
                                    '#paymentorder-contractor_bank_name' => 'name',
                                    '#paymentorder-contractor_bank_city' => 'city',
                                    '#paymentorder-contractor_corresponding_account' => 'ks',
                                ],
                                'options' => [
                                    'id' => 'paymentorder-contractor_bik',
                                    'class' => 'form-control',
                                ],
                            ]); ?>
                        </div>

                        <div class="width-30 height-30 pull-left border-bootom-right">
                            <?= Html::activeLabel($model, 'contractor_corresponding_account', [
                                'label' => 'Сч №',
                                'class' => 'control-label',
                            ]); ?>
                        </div>
                        <div class="width-70 height-30 pull-left border-bootom">
                            <?= $form->field($model, 'contractor_corresponding_account')->textInput([
                                'readonly' => true,
                                'class' => 'form-control',
                            ])->label(false); ?>
                        </div>

                        <div class="width-30 height-60 pull-left border-right">
                            <?= Html::activeLabel($model, 'contractor_current_account', [
                                'label' => 'Сч №',
                                'class' => 'control-label',
                            ]); ?>
                        </div>
                        <div class="width-70 height-60 pull-left">
                            <?= $form->field($model, 'contractor_current_account')->textInput([
                                'maxlength' => true,
                                'class' => 'form-control',
                            ])->label(false); ?>
                        </div>

                        <div class="width-100 pull-left">
                            <div class="width-50 pull-left border-top">
                                <div class="width-60 height-30 pull-left border-bootom-right">
                                    <?= Html::activeLabel($model, 'operation_type_id', [
                                        'label' => 'Вид оп.',
                                        'class' => 'control-label',
                                    ]); ?>
                                </div>
                                <div class="width-40 height-30 pull-left">
                                    <?= $form->field($model, 'operation_type_id')
                                        ->dropDownList(ArrayHelper::map(OperationType::find()->all(), 'id', 'name'), [
                                            'class' => 'form-control',
                                        ])->label(false); ?>
                                </div>
                                <div class="width-60 height-30 pull-left border-bootom-right">
                                    <label class="control-label">Наз. пл.</label>
                                </div>
                                <div class="width-40 height-30 pull-left">
                                </div>
                                <div class="width-60 height-30 pull-left border-right">Код</div>
                                <div class="width-40 height-30 pull-left">
                                    <?= $form->field($model, 'uin_code')->textInput([
                                        'class' => 'form-control budget-payment pad-n',
                                        'maxlength' => true,
                                        'data-default-value' => '0',
                                    ])->label(false); ?>
                                </div>
                            </div>
                            <div class="width-50 pull-left border-left border-top">
                                <div class="width-60 height-30 pull-left border-bootom-right">Срок пл.</div>
                                <div class="width-40 height-30 pull-left">
                                    <?= $form->field($model, 'payment_limit_date')->textInput([
                                        'class' => 'date-picker',
                                        'style' => 'line-height: normal;',
                                        'size' => 16,
                                        'data' => [
                                            'date-viewmode' => 'years',
                                        ],
                                        'autocomplete' => 'off',
                                        'value' => DateHelper::format($model->payment_limit_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                                    ])->label(false); ?>
                                </div>

                                <div class="width-60 height-30 pull-left border-bootom-right">
                                    <?= Html::activeLabel($model, 'ranking_of_payment', [
                                        'label' => 'Очер. пл.',
                                        'class' => 'control-label',
                                    ]); ?>
                                </div>
                                <div class="width-40 height-30 pull-left">
                                    <?= $form->field($model, 'ranking_of_payment')->textInput([
                                        'class' => 'form-control pad-n',
                                    ])->label(false); ?>
                                </div>
                                <div class="width-60 height-30 pull-left border-right">Рез. поле</div>
                                <div class="width-40 height-30 pull-left"></div>
                            </div>
                        </div>
                    </div>

                    <div class="width-100 pull-left border-top">
                        <div class="height-30 pull-left border-right" style="width:25%">
                            <?= $form->field($model, 'kbk')->label(false)->textInput([
                                'maxlength' => true,
                                'disabled' => $model->relation_with_in_invoice,
                                'class' => 'form-control budget-payment',
                                'data-default-value' => '18210501011011000110',
                            ]); ?>
                        </div>
                        <div class="height-30 pull-left border-right" style="width:20%">
                            <?= $form->field($model, 'oktmo_code')->textInput([
                                'maxlength' => true,
                                'disabled' => $model->relation_with_in_invoice,
                                'class' => 'form-control budget-payment',
                                'data-default-value' => $model->company->oktmo ?: ($model->company->okato ?: 0),
                            ])->label(false); ?>
                        </div>
                        <div class="height-30 pull-left border-right" style="width:10%">
                            <?= $form->field($model, 'payment_details_id')
                                ->dropDownList(ArrayHelper::map(PaymentDetails::find()->all(), 'id', 'code'), [
                                    'prompt' => '',
                                    'class' => 'form-control budget-payment',
                                    'disabled' => $model->relation_with_in_invoice,
                                    'data-default-value' => PaymentDetails::DETAILS_1,
                                ])->label(false); ?>
                        </div>
                        <div class="height-30 pull-left border-right" style="width:15%">
                            <?= $form->field($model, 'tax_period_code')->textInput([
                                'class' => 'form-control budget-payment pad-n',
                                //'disabled' => $model->relation_with_in_invoice,
                                'data-default-value' => $model->tax_period_code,
                            ])->label(false); ?>
                        </div>
                        <div class="height-30 pull-left border-right" style="width:10.2%">
                            <?= $form->field($model, 'document_number_budget_payment')->textInput([
                                'class' => 'form-control budget-payment pad-n',
                                'disabled' => $model->relation_with_in_invoice,
                                'data-default-value' => '0',
                            ])->label(false); ?>
                        </div>
                        <div class="height-30 pull-left border-right" style="width:11.9%;">
                            <?= $form->field($model, 'document_date_budget_payment')->textInput([
                                'class' => 'form-control date-picker budget-payment',
                                'style' => 'line-height: normal;',
                                'size' => 16,
                                'data' => [
                                    'date-viewmode' => 'years',
                                ],
                                'disabled' => $model->relation_with_in_invoice,
                                'value' => $document_date_budget_payment_value,
                                'data-default-value' => '0',
                            ])->label(false); ?>
                        </div>
                        <div class="height-30 pull-left" style="width:7.9%">
                            <?= $form->field($model, 'payment_type_id')
                                ->dropDownList(ArrayHelper::map(PaymentType::find()->all(), 'id', 'code'), [
                                    'prompt' => '',
                                    'class' => 'form-control budget-payment',
                                    'disabled' => $model->relation_with_in_invoice,
                                    'data-default-value' => PaymentType::TYPE_0,
                                ])->label(false); ?>
                        </div>
                    </div>
                </div>

                <?= $form->field($model, 'purpose_of_payment')->textarea([
                    'class' => 'form-control',
                    'data-default-value' => 'Налог, взимаемый с налогоплательщиков, выбравших в качестве объекта налогообложения доходы',
                ])->label(false); ?>

                <p class="height-30 bold-text">
                    <?= Html::activeLabel($model, 'purpose_of_payment', [
                        'label' => 'Назначение платежа',
                        'class' => 'control-label bold-text',
                    ]); ?>
                </p>
            </div>
        </div>
        <div class="form-actions">
            <div class="row action-buttons">
                <div class="spinner-button col-sm-1 col-xs-1">
                    <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                        'data-style' => 'expand-right',
                    ]); ?>
                    <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', ['class' => 'btn darkblue widthe-100 hidden-lg', 'title' => 'Сохранить',]) ?>
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="spinner-button col-sm-1 col-xs-1">
                    <?= Html::a('Отменить', [
                        'view-payment-order',
                        'id' => $model->id,
                        'period' => $taxRobot->getUrlPeriodId(),
                    ], [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                        'data-style' => 'expand-right',
                    ]); ?>
                    <?= Html::a('<i class="fa fa-reply fa-2x"></i>', [
                        'view-payment-order',
                        'id' => $model->id,
                        'period' => $taxRobot->getUrlPeriodId(),
                    ], ['class' => 'btn darkblue widthe-100 hidden-lg', 'title' => 'Отменить',]); ?>
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php $this->registerJs('
    $(document).on("change", "#paymentorder-company_rs", function () {
        var form = this.form;
        var rs = $(":selected", this);
        if ($(this).val() && rs) {
            $("#paymentorder-company_bank_name", form).val(rs.data("bank"));
            $("#paymentorder-company_bank_city", form).val(rs.data("city"));
            $("#paymentorder-company_bik", form).val(rs.data("bik"));
            $("#paymentorder-company_ks", form).val(rs.data("ks"));
        }
    });
'); ?>
