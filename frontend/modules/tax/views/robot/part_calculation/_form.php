<?php
/**
 * @var $this  yii\web\View
 * @var $model common\models\cash\CashBankFlows
 * @var $form  yii\bootstrap\ActiveForm
 */

use common\models\cash\CashBankFlows;
use common\models\Contractor;
use common\models\cash\CashBankReasonType;
use common\models\document\InvoiceIncomeItem;
use frontend\widgets\ExpenditureDropdownWidget;
use frontend\modules\cash\models\CashContractorType;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;

$company = Yii::$app->user->identity->company;
$accounArray = $company->getCheckingAccountants()
    ->orderBy(['type' => SORT_ASC, 'rs' => SORT_ASC])->all();

$rsArray = ArrayHelper::map($accounArray, 'rs', function($data) {
    return $data->rs . ', ' . $data->bank_name;
});
$rsBankArray = ArrayHelper::map($accounArray, 'rs', 'bank_name');

if (empty($flowTypeItems)) {
    $flowTypeItems = [CashBankFlows::FLOW_TYPE_EXPENSE => 'Расход'];
}

$incomeItems = InvoiceIncomeItem::getList(Yii::$app->user->identity->company->id);

$sellerArray = ArrayHelper::map(CashContractorType::find()
    ->andWhere(['!=', 'name', $model->getNewCashContractorId()])
    ->andWhere(['!=', 'name', CashContractorType::BALANCE_TEXT])->all(), 'name', 'text')
    +
    $company->sortedContractorList(Contractor::TYPE_SELLER);

$customerArray = ArrayHelper::map(CashContractorType::find()
    ->andWhere(['!=', 'name', $model->getNewCashContractorId()])->all(), 'name', 'text')
    +
    $company->sortedContractorList(Contractor::TYPE_CUSTOMER);

$expenditureArray = [28 => 'Фиксированный платеж в ПФР', 46 => 'Фиксированный платеж в ОМС'];

?>

<?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'options' => [
        'class' => 'form-horizontal',
        'is_new_record' => $model->isNewRecord ? 1 : 0,
    ],
    'enableClientValidation' => true,
    'enableAjaxValidation' => true,
    'validateOnSubmit' => true,
    'validateOnBlur' => false,
    'id' => 'js-cash_flow_update_form',
    'action' => empty($action) ? '' : $action,
])); ?>
<div class="form-body">
    <?php if (!empty($redirect)) : ?>
        <?= Html::hiddenInput('redirect', $redirect) ?>
    <?php endif ?>
    <?php $radioConfig = [
        'labelOptions' => [
            'class' => 'col-xs-12 col-md-3 control-label width-label bold-text',
        ],
        'wrapperOptions' => [
            'class' => 'col-xs-10 col-md-8 margin-left-15',
        ],
    ]; ?>
    <?= $form->field($model, 'flow_type', $radioConfig)
        ->label('Тип')
        ->radioList($model->isNewRecord ? $flowTypeItems : [$model->flow_type => $flowTypeItems[$model->flow_type]], [
            'class' => 'row',
            'uncheck' => null,
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::radio($name, $checked, [
                    'value' => $value,
                    'label' => $label,
                    'labelOptions' => [
                        'class' => 'col-xs-6',
                        'style' => 'padding-top: 7px; max-width: 140px;',
                    ],
                ]);
            },
        ]); ?>

    <?= $form->field($model, 'rs', [
        'options' => [
            'class' => 'form-group',
        ],
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label bold-text',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-9',
        ],
    ])->widget(Select2::classname(), [
        'data' => $rsArray,
        'value' => $model->rs,
        'options' => [
            'placeholder' => '',
        ],
    ]); ?>

    <?= $form->field($model, 'contractor_id', [
        'options' => [
            'id' => 'seller_contractor_wrapper',
            'class' => 'form-group hidden',
        ],
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label bold-text',
            'label' => 'Поставщик',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-9',
        ],
    ])->widget(Select2::classname(), [
        'data' => $sellerArray,
        'options' => [
            'id' => 'seller_contractor_id',
            'placeholder' => '',
            'disabled' => true,
        ],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]); ?>

    <?= $form->field($model, 'contractor_id', [
        'options' => [
            'id' => 'customer_contractor_wrapper',
            'class' => 'form-group hidden',
        ],
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label bold-text',
            'label' => 'Покупатель',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-9',
        ],
    ])->widget(Select2::classname(), [
        'data' => $customerArray,
        'options' => [
            'id' => 'customer_contractor_id',
            'placeholder' => '',
            'disabled' => true,
        ],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]); ?>

    <?= $form->field($model, 'expenditure_item_id', [
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label font-bold',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-9',
        ],
        'options' => [
            'id' => 'js-expenditure_item_id_wrapper',
            'class' => 'form-group',
            'style' => ($model->flow_type == $model::FLOW_TYPE_EXPENSE) ? '' : 'display:none',
        ],
    ])->widget(Select2::classname(), [
        'data' => $expenditureArray,
        'options' => [
            'placeholder' => '',
        ],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]); ?>

    <?= $form->field($model, 'income_item_id', [
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label font-bold',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-9',
        ],
        'options' => [
            'id' => 'js-income_item_id_wrapper',
            'class' => 'form-group',
            'style' => ($model->flow_type == $model::FLOW_TYPE_INCOME) ? '' : 'display:none',
        ],
    ])->widget(ExpenditureDropdownWidget::classname(), [
        'income' => true,
        'options' => [
            'prompt' => '',
        ],
        'pluginOptions' => [
        ]
    ]); ?>
    <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
        'inputId' => 'cashbankflowsform-income_item_id',
        'type' => 'income',
    ]) ?>

    <?= $form->field($model, 'amount', [
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label font-bold',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-5',
        ],
    ])->textInput([
        'value' => !empty($model->amount) ? str_replace('.', ',', $model->amount / 100) : '',
        'class' => 'form-control js_input_to_money_format',
        'style' => 'width: 100%; max-width: 125px;',
    ]); ?>

    <?= $form->field($model, 'date', [
        'inputTemplate' => '<div class="input-icon"><i class="fa fa-calendar"></i>{input}</div>',
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label bold-text',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-5',
        ],
    ])->textInput([
        'class' => 'form-control date-picker',
        'style' => 'width: 100%; max-width: 125px;',
        'data' => [
            'date-viewmode' => 'years',
        ],
    ]); ?>

    <?= $form->field($model, 'payment_order_number', [
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label font-bold',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-5',
        ],
    ])->textInput(['style' => 'width: 100%; max-width: 125px;',]); ?>

    <?= $form->field($model, 'description', [
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label bold-text',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-9',
        ],
    ])->textarea(['style' => 'resize: none;', 'rows' => '2']); ?>


    <?= $form->field($model, 'invoices_list', [
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label bold-text',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-9',
        ],
    ])->widget(\frontend\modules\cash\widgets\InvoiceListInputWidget::className()); ?>

    <?= $form->field($model, 'recognitionDateInput', [
        'inputTemplate' => '<div class="input-icon"><i class="fa fa-calendar"></i>{input}</div>',
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label bold-text',
            'label' => 'Дата признания ' . Html::tag(
                'span',
                $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? 'дохода' : 'расхода',
                ['id' => 'recognition-date-type']
            ),
        ],
        'wrapperOptions' => [
            'class' => 'col-md-5',
        ],
    ])->textInput([
        'class' => 'form-control date-picker',
        'style' => 'width: 100%; max-width: 125px;',
        'data' => [
            'date-viewmode' => 'years',
        ],
    ]); ?>


    <div class="form-actions">
        <div class="row action-buttons" id="buttons-fixed">
            <div class="spinner-button col-sm-1 col-xs-1 text-left" style="width: 24%;">
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                    'style' => 'width: 130px!important;',
                ]); ?>
                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
                    'title' => 'Сохранить',
                ]); ?>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="spinner-button col-sm-2 col-xs-1 text-right" style="width: 24%; float: right;">
                <?php if (Yii::$app->request->isAjax):?>
                    <?= Html::button('Отменить', [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs back',
                        'onclick' => new \yii\web\JsExpression('$(this).closest(".modal").modal("hide")'),
                        'style' => 'width: 130px!important;',
                    ]) ?>
                    <?= Html::button('<i class="fa fa-reply fa-2x"></i></button>', [
                        'class' => 'btn darkblue widthe-100 hidden-lg back',
                        'onclick' => new \yii\web\JsExpression('$(this).closest(".modal").modal("hide")'),
                    ]) ?>
                <?php else:?>
                    <?= Html::a('Отменить', ['index'], [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs back',
                        'style' => 'width: 130px!important;',
                    ]) ?>
                    <?= Html::a('<i class="fa fa-reply fa-2x"></i></button>', ['index'], [
                        'class' => 'btn darkblue widthe-100 hidden-lg back',
                    ]) ?>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>
<?php $form->end(); ?>

<?php
if (!$model->isNewRecord) {
    $modalTitle = ($model->flow_type == $model::FLOW_TYPE_INCOME) ? 'Приход' : 'Расход';
    $this->registerJs("$('#js-modal_update_title').html('{$modalTitle}')");

    $this->registerJs('
        var $reasonTypeDropDown = $("select[name=\'' . Html::getInputName($model, 'reason_id') . '\']");
        var flow_type = ' . $model->flow_type . ';
    ');
}

$this->registerJs('
    var bankArray = ' . \yii\helpers\Json::encode($rsBankArray) . ';
    var setInputsByType = function(form) {
        var value = $("#cashbankflowsform-flow_type input[type=radio]:checked", form).val();
        if (typeof value == "undefined")
            value = $("#cashbankflows-flow_type input[type=radio]:checked", form).val();
        console.log(value);
        if (value == "'.$model::FLOW_TYPE_EXPENSE.'") {
            $("#seller_contractor_wrapper", form).removeClass("hidden");
            $("#seller_contractor_id", form).attr("disabled", false);
            $("#customer_contractor_wrapper", form).addClass("hidden");
            $("#customer_contractor_id", form).attr("disabled", true);
            $("#js-expenditure_item_id_wrapper", form).show();
            $("#js-income_item_id_wrapper", form).hide();
            $("#recognition-date-type", form).text("расхода");
        } else {
            $("#customer_contractor_wrapper", form).removeClass("hidden");
            $("#customer_contractor_id", form).attr("disabled", false);
            $("#seller_contractor_wrapper", form).addClass("hidden");
            $("#seller_contractor_id", form).attr("disabled", true);
            $("#js-expenditure_item_id_wrapper", form).hide();
            $("#js-income_item_id_wrapper", form).show();
            $("#recognition-date-type", form).text("дохода");
        }
    }

    //$("#cashbankflowsform-flow_type input[type=radio]").on("change", function() {
    //    $("#js-cash_flow_update_form .has-error").removeClass("has-error");
    //    $("#js-cash_flow_update_form .help-block.help-block-error").html("");
    //    setInputsByType(this.form);
    //});

    $(document).on("change", "#cashbankflowsform-rs", function() {
        var rsVal = $(this).val();
        if (bankArray[rsVal]) {
            $("#cashbankflowsform-bank_name").val(bankArray[rsVal]);
        }
    });
    $(document).on("change", "#cashbankflows-rs", function() {
        var rsVal = $(this).val();
        if (bankArray[rsVal]) {
            $("#cashbankflows-bank_name").val(bankArray[rsVal]);
        }
    });

    $(".date-picker").datepicker({
        keyboardNavigation: false,
        forceParse: false,
        language: "ru",
        autoclose: true
    }).on("change.dp", dateChanged);

    function dateChanged(ev) {
        if (ev.bubbles == undefined) {
            var $input = $("[name=\'" + ev.currentTarget.name +"\']");
            if (ev.currentTarget.value == "") {
                if ($input.data("last-value") == null) {
                    $input.data("last-value", ev.currentTarget.defaultValue);
                }
                var $lastDate = $input.data("last-value");
                $input.datepicker("setDate", $lastDate);
            } else {
                $input.data("last-value", ev.currentTarget.value);
            }
        }
    }

    setInputsByType(document.getElementById("js-cash_flow_update_form"));

    $("#cashbankflowsform-flow_type input[type=radio]:not(.md-radiobtn)").uniform();
');
