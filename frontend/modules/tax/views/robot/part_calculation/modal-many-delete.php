<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 31.05.2018
 * Time: 17:09
 */

use yii\helpers\Url;
use yii\bootstrap\Html;
?>
<div id="many-delete" class="confirm-modal fade modal"
     role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none; margin-top: -51.5px;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body">
                    <div class="row">Вы уверены, что хотите
                        удалить выбранные элементы?
                    </div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-6">
                        <?= Html::a('ДА', null, [
                            'class' => 'btn darkblue text-white pull-right modal-many-delete',
                            'data-url' => Url::to(['delete']),
                        ]); ?>
                    </div>
                    <div class="col-xs-6">
                        <?= Html::button('НЕТ', [
                            'class' => 'btn darkblue text-white',
                            'data-dismiss' => 'modal',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
