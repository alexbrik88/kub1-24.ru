<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $urlPeriod string */

$nextCss = $taxRobot->getIsDeclarationPaid() ? '' : ' taxrobot-pay-panel-trigger';
?>

    <div class="col-xs-12 pad0 buttons-block">
        <?= Html::a('Назад', ['calculation', 'period' => $urlPeriod], [
            'class' => 'btn darkblue hidden-xs pull-left',
            'style' => 'min-width:180px',
            'data' => [
                'pjax' => '0',
            ],
        ]); ?>
        <?= Html::a('<i class="fa fa-arrow-left fa-2x"></i>', ['calculation', 'period' => $urlPeriod], [
            'title' => 'Назад',
            'class' => 'btn darkblue pull-left hidden-md hidden-sm hidden-lg',
            'data' => [
                'pjax' => '0',
            ],
        ]); ?>

        <?= Html::a('Далее', ['declaration', 'period' => $urlPeriod], [
            'id' => 'submit',
            'class' => 'btn darkblue pull-right hidden-xs' . $nextCss,
            'style' => 'min-width:180px',
            'data' => [
                'pjax' => '0',
            ],
        ]); ?>
        <?= Html::a('<i class="fa fa-arrow-right fa-2x"></i>', ['declaration', 'period' => $urlPeriod], [
            'id' => 'submit',
            'title' => 'Далее',
            'class' => 'btn darkblue pull-right hidden-md hidden-sm hidden-lg' . $nextCss,
            'data' => [
                'pjax' => '0',
            ],
        ]); ?>
    </div>
