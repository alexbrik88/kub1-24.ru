<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\components\date\DateHelper;
use frontend\rbac\permissions\document\Document;
use yii\helpers\ArrayHelper;
use common\components\TaxRobotHelper;
use frontend\modules\tax\models\TaxrobotCashBankSearch;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
?>
<div class="portlet">
    <div class="customer-info" style="min-height:auto!important">
        <div class="portlet-body no_mrg_bottom main_inf_no-bord">
            <table class="table no_mrg_bottom">
                <tr>
                    <td>
                        <span class="customer-characteristic">
                           Бюджетный платеж:
                        </span>
                        <span> налог УСН за <?= $taxRobot->period->label?></span><br/>
                        <span class="customer-characteristic">
                           Налог УСН Доходы:
                        </span>
                        <span><?= \common\components\TextHelper::moneyFormat($model->sum/100, 2) ?> ₽ </span><br/>
                        <span class="customer-characteristic">
                           Получатель:
                        </span>
                        <span>
                            <?= $model->contractor ? Html::a($model->contractor_name, [
                                '/contractor/view',
                                'type' => $model->contractor->type,
                                'id' => $model->contractor->id,
                            ], [ 'data-pjax' => '0']) : $model->contractor_name; ?>
                        </span>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="portlet" style="margin-top: -10px">
    <div class="customer-info" style="border:none">
        <div class=" no_mrg_bottom main_inf_no-bord">
            <table class="table no_mrg_bottom">
                <tr>
                    <td>
                        <span>
                            Платежку можно оплатить позднее, она находится в разделе
                            <a data-pjax="0" href="/documents/payment-order/index">Платежные поручения</a>.
                        </span>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>