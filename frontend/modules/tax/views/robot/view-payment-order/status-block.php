<?php
use common\components\date\DateHelper;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
?>
<div class="status-panel col-xs-12 pad0">
    <div class="col-xs-12 col-sm-3 pad3">
        <div class="btn full_w marg darkblue status-date"
             style="padding-left:0px; padding-right:0px;text-align: center; "
             title="Дата создания">
            <?= date(DateHelper::FORMAT_USER_DATE, $model->payment_order_status_updated_at); ?>
        </div>
    </div>
    <div class="col-xs-12 col-sm-9 pad0">
        <div class="col-xs-12 pad3">
            <div class="btn full_w marg darkblue"
                 title="Статус">
                <span class="icon pull-left icon-doc"></span>
                <span class="status-name"><?= $model->paymentOrderStatus->name; ?></span>
            </div>
        </div>
    </div>
</div>