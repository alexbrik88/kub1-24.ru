<?php

use common\components\ImageHelper;
use common\models\document\status\PaymentOrderStatus;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use frontend\models\Documents;
use frontend\rbac\UserRole;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $taxRobot common\components\TaxRobotHelper */
/* @var $needPay boolean */

?>

<div class="row action-buttons margin-no-icon">
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?= Html::a('Назад', ['payment', 'period' => $urlPeriod], [
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
            'data' => [
                'pjax' => '0',
            ],
        ]); ?>
        <?= Html::a('<i class="fa fa-arrow-left fa-2x"></i>', ['payment', 'period' => $urlPeriod], [
            'title' => 'Назад',
            'class' => 'btn darkblue widthe-100 hidden-lg',
            'data' => [
                'pjax' => '0',
            ],
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?= Html::a('Импорт в файл', [
            '/documents/payment-order/import',
            'id' => $model->id,
        ], [
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
            'data' => [
                'pjax' => '0',
            ],
        ]); ?>
        <?= Html::a('<i class="fa fa-download fa-2x"></i>', [
            '/documents/payment-order/import',
            'id' => $model->id,
        ], [
            'title' => 'Импорт в файл',
            'class' => 'btn darkblue widthe-100 hidden-lg',
            'data' => [
                'pjax' => '0',
            ],
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?= Html::a('Печать', [
            '/documents/payment-order/document-print',
            'actionType' => 'print',
            'id' => $model->id,
            'type' => Documents::IO_TYPE_IN,
            'filename' => $model->getPrintTitle(),
        ], [
            'target' => '_blank',
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs no-reload-status print',
            'data' => [
                'pjax' => '0',
            ],
        ]); ?>
        <?= Html::a('<i class="fa fa-print fa-2x"></i>', [
            '/documents/payment-order/document-print',
            'actionType' => 'print',
            'id' => $model->id,
            'type' => Documents::IO_TYPE_IN,
            'filename' => $model->getPrintTitle(),
        ], [
            'target' => '_blank',
            'title' => 'Печать',
            'class' => 'btn darkblue widthe-100 hidden-lg no-reload-status print',
            'data' => [
                'pjax' => '0',
            ],
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?= Html::a('PDF', [
            '/documents/payment-order/document-print',
            'actionType' => 'pdf',
            'id' => $model->id,
            'type' => Documents::IO_TYPE_IN,
            'filename' => $model->getPrintTitle(),
        ], [
            'target' => '_blank',
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
            'data' => [
                'pjax' => '0',
            ],
        ]); ?>
        <?= Html::a('<i class="fa fa-file-pdf-o fa-2x"></i>', [
            '/documents/payment-order/document-print',
            'actionType' => 'pdf',
            'id' => $model->id,
            'type' => Documents::IO_TYPE_IN,
            'filename' => $model->getPrintTitle(),
        ], [
            'target' => '_blank',
            'title' => 'PDF',
            'class' => 'btn darkblue widthe-100 hidden-lg',
            'data' => [
                'pjax' => '0',
            ],
        ]); ?>
    </div>
    <div class="button-bottom-page-lg button-bottom-page-lg-double col-sm-2 col-xs-2">
        <?php if ($needPay &&
            $model->payment_order_status_id != PaymentOrderStatus::STATUS_SENT_TO_BANK &&
            (Yii::$app->user->can(UserRole::ROLE_CHIEF) || Yii::$app->user->can(UserRole::ROLE_ACCOUNTANT)) &&
            ($paymentAccount = $model->company->getBankingPaymentAccountants()->one()) !== null &&
            ($alias = Banking::aliasByBik($paymentAccount->bik)) &&
            ($bank = Banking::getBankByBik($paymentAccount->bik))
        ) : ?>
            <?php $image = ImageHelper::getThumb($bank->getUploadDirectory() . $bank->little_logo_link, [32, 32], [
                'class' => 'little_logo_bank',
                'style' => 'display: inline-block;',
            ]);?>
            <?= Html::a($image.'<span>Отправить в ' . $bank->bank_name.'</span>', [
                "/cash/banking/{$alias}/default/payment",
                'account_id' => $paymentAccount->id,
                'po_id' => $model->id,
                'p' => Banking::routeEncode(['/documents/payment-order/view', 'id' => $model->id]),
            ], [
                'class' => 'banking-module-open-link',
                'data' => [
                    'pjax' => '0',
                ],
                'style' => ''
            ]); ?>
            <?= BankingModalWidget::widget([
                'pageTitle' => $this->title,
                'pageUrl' => Url::to(['robot/payment', 'period' => $urlPeriod]),
            ]) ?>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
</div>
