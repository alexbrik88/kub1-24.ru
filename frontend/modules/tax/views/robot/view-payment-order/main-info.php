<?php
use yii\helpers\Html;
use php_rutils\RUtils;
use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\company\CompanyType;
use frontend\modules\documents\widgets\CreatedByWidget;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $taxRobot common\components\TaxRobotHelper */

$company = $taxRobot->company;
?>
<div class="pad0">
    <div class="col-xs-12 pad3">
        <div class="col-xs-12 pad0 font-bold" style="height: inherit">

            <div class="actions" style="margin-top: -3px;">
                <?= \frontend\modules\documents\widgets\DocumentLogWidget::widget([
                    'model'=>$model,
                ]); ?>
                <?php if ($needPay && Yii::$app->user->can(frontend\rbac\permissions\document\PaymentOrder::UPDATE)): ?>
                    <?= Html::a(' <i class="icon-pencil"></i> ', [
                        'update-payment-order',
                        'id' => $model->id,
                        'period' => $taxRobot->getUrlPeriodId(),
                    ], [
                        'class' => 'btn darkblue btn-sm btn-ie',
                        'title' => 'Редактировать',
                        'data' => [
                            'pjax' => '0',
                        ],
                    ]) ?>
                <?php endif; ?>
            </div>
            <div class="col-xs-12 pad3" style="margin-bottom: 10px;margin-top: 10px;">
                <div class="data-block pull-left" style="margin-right: 16.5%;">
                    <div class="data-block__data" style="height: 20px;width: 100%;font-weight: bold;">
                    </div>
                    <div class="data-block__title" style="font-weight: 400;">
                        Поступ. в банк плат.
                    </div>
                </div>
                <div class="data-block pull-left" style="margin-right: 24.4%;">
                    <div class="data-block__data" style="height: 20px;width: 100%;font-weight: bold;">
                    </div>
                    <div class="data-block__title" style="font-weight: 400;">
                        Списано со сч. плат.
                    </div>
                </div>
                <div class="data-block pull-left" style="margin: 0;">
                    <div style="position: absolute;top: -3px;font-weight: 400;display: inline-block;padding: 3px 10px;border: 1px solid #000;min-width: 26px;">
                        0401060
                    </div>
                </div>
            </div>
            <div style="padding-top: 22px !important;">
                <p style="font-size: 17px;text-transform: uppercase;">
                    платежное поручение №<?= $model->document_number; ?>
                </p>
            </div>
        </div>
    </div>
    <div class="row no-margin-l">
        <div class="col-md-offset-5">
            <div class="data-block pull-left">
                <div class="data-block__data" style="height: 20px;font-weight: bold;">
                    <?= RUtils::dt()->ruStrFTime([
                        'format' => DateHelper::FORMAT_USER_DATE,
                        'date' => $model->document_date,
                    ]); ?>
                </div>
                <div class="data-block__title">
                    Дата
                </div>
            </div>
            <div class="data-block pull-left">
                <div class="data-block__data" style="height: 20px;font-weight: bold;">
                </div>
                <div class="data-block__title">
                    Вид платежа
                </div>
            </div>
            <div class="data-block pull-left" style="margin: 0;">
                <div
                    style="position: absolute;top: 122px;display: inline-block; padding: 3px 5px; border: 1px solid #000; min-width: 26px;">
                    <?= $model->taxpayersStatus ? $model->taxpayersStatus->code : '&nbsp;'; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="summery clearfix">
        <div class="height-30 summery__sum pull-left">
            Сумма прописью
        </div>
        <div class="height-30 summery__sum-in-words pull-left">
            <?= $model->sum_in_words; ?>
        </div>
    </div>
    <div class="clearfix payment-order__table">
        <div class="width-60 left-block pull-left">
            <div class="width-50 height-30 pull-left border-bootom-right">ИНН <?= $model->company_inn; ?></div>
            <div class="width-50 height-30 pull-left border-bootom">КПП <?= $model->company_kpp; ?></div>
            <div class="width-100 pull-left">
                <div class="height-60">
                    <?= $company->getTitle($company->company_type_id == CompanyType::TYPE_IP ? false : true); ?>
                </div>
                <div class="height-30 bold-text border-bootom">Плательщик</div>
            </div>
            <div class="width-100 pull-left">
                <div class="height-30"><?= $model->company_bank_name; ?></div>
                <div class="height-30 bold-text border-bootom">Банк плательщика</div>
            </div>
            <div class="width-100 pull-left">
                <div class="height-30"><?= $model->contractor_bank_name; ?></div>
                <div class="height-30 bold-text border-bootom">Банк получателя</div>
            </div>
            <div class="width-50 height-30 pull-left border-bootom-right">
                ИНН <?= $model->contractor_inn; ?></div>
            <div class="width-50 height-30 pull-left border-bootom">КПП <?= $model->contractor_kpp; ?></div>
            <div class="width-100 pull-left">
                <div class="height-90"><?= $model->contractor_name; ?></div>
                <div class="height-30 bold-text">Получатель</div>
            </div>
        </div>
        <div class="width-40 right-block pull-left border-left">
            <div class="width-30 height-60 pull-left border-bootom-right">Сумма</div>
            <div
                class="width-70 height-60 pull-left border-bootom"><?= TextHelper::invoiceMoneyFormat($model->sum,
                    2); ?></div>
            <div class="width-30 height-60 pull-left border-bootom-right">Сч №</div>
            <div class="width-70 height-60 pull-left"><?= $model->company_rs; ?></div>
            <div class="width-30 height-30 pull-left border-bootom-right">БИК</div>
            <div class="width-70 height-30 pull-left"><?= $model->company_bik; ?></div>
            <div class="width-30 height-30 pull-left border-bootom-right">Сч №</div>
            <div class="width-70 height-30 pull-left border-bootom"><?= $model->company_ks; ?></div>
            <div class="width-30 height-30 pull-left border-bootom-right">БИК</div>
            <div class="width-70 height-30 pull-left border-bootom"><?= $model->contractor_bik; ?></div>
            <div class="width-30 height-30 pull-left border-bootom-right">Сч №</div>
            <div
                class="width-70 height-30 pull-left border-bootom"><?= $model->contractor_corresponding_account; ?></div>
            <div class="width-30 height-60 pull-left border-right">Сч №</div>
            <div class="width-70 height-60 pull-left"><?= $model->contractor_current_account; ?></div>
            <div class="width-100 pull-left">
                <div class="width-50 pull-left border-top">
                    <div class="width-60 height-30 pull-left border-bootom-right">Вид оп.</div>
                    <div class="width-40 height-30 pull-left">
                        <?= (!empty($model->operation_type_id)) ? $model->operationType->code : ''; ?>
                    </div>
                    <div class="width-60 height-30 pull-left border-bootom-right">Наз. пл.</div>
                    <div class="width-40 height-30 pull-left"></div>
                    <div class="width-60 height-30 pull-left border-right">Код</div>
                    <div class="width-40 height-30 pull-left"><?= $model->uin_code; ?></div>
                </div>
                <div class="width-50 pull-left border-left border-top">
                    <div class="width-60 height-30 pull-left border-bootom-right">Срок пл.</div>
                    <div
                        class="width-40 height-30 pull-left"><?= DateHelper::format($model->payment_limit_date,
                            DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?></div>
                    <div class="width-60 height-30 pull-left border-bootom-right">Очер. пл.</div>
                    <div class="width-40 height-30 pull-left"><?= $model->ranking_of_payment; ?></div>
                    <div class="width-60 height-30 pull-left border-right">Рез. поле</div>
                    <div class="width-40 height-30 pull-left"></div>
                </div>
            </div>
        </div>
        <div class="width-100 pull-left border-top">
            <div class="height-30 pull-left border-right" style="width:30%"><?= $model->kbk; ?></div>
            <div class="height-30 pull-left border-right" style="width:20%"><?= $model->oktmo_code; ?></div>
            <div class="height-30 pull-left border-right" style="width:5%">
                <?= (!empty($model->payment_details_id)) ? $model->paymentDetails->code : ''; ?>
            </div>
            <div class="height-30 pull-left border-right"
                 style="width:15%"><?= $model->tax_period_code; ?></div>
            <div class="height-30 pull-left border-right"
                 style="width:10.2%"><?= $model->document_number_budget_payment ?></div>
            <div class="height-30 pull-left border-right" style="width:11.9%;">
                <?php if ($model->document_date_budget_payment !== null) {
                    echo RUtils::dt()->ruStrFTime([
                        'format' => \common\components\date\DateHelper::FORMAT_USER_DATE,
                        'date' => $model->document_date_budget_payment,
                    ]);
                } elseif ($model->presence_status_budget_payment) {
                    echo 0;
                } ?>
            </div>
            <div class="height-30 pull-left" style="width:7.9%">
                <?= (!empty($model->payment_type_id)) ? $model->paymentType->code : ''; ?>
            </div>
        </div>
    </div>
    <div style="padding: 0 5px;">
        <?= $model->purpose_of_payment; ?>
    </div>
    <div class="payment-purpose col-md-12 p-l-r-0">
        <div class="data-block pull-left col-md-4 p-l-r-0">
            <div class="data-block__data text-left">Назначение платежа</div>
            <div class="data-block__title"></div>
        </div>
        <div class="data-block pull-left col-md-4 p-l-r-0">
            <div class="data-block__data"></div>
            <div class="data-block__title">Подписи</div>
        </div>
        <div class="data-block pull-left col-md-4 p-l-r-0">
            <div class="data-block__data"></div>
            <div class="data-block__title">Отметки банка</div>
        </div>
    </div>
    <div class="print-and-signature col-md-12 p-l-r-0">
        <div class="data-block pull-left col-md-4 p-l-r-0">
            <div style="height: 20px;width: 100%;">М.П.</div>
            <div class="data-block__title"></div>
        </div>
        <div class="data-block pull-left col-md-4 p-l-r-0">
            <div class="data-block__data"></div>
            <div class="data-block__title"></div>
        </div>
        <div class="data-block pull-left col-md-4 p-l-r-0">
            <div style="height: 20px;width: 100%;"></div>
            <div class="data-block__title"></div>
        </div>
    </div>
    <div class="print-and-signature last col-md-12 p-l-r-0">
        <div class="data-block pull-left col-md-4 p-l-r-0">
            <div style="height: 20px;width: 100%;"></div>
            <div class="data-block__title"></div>
        </div>
        <div class="data-block pull-left col-md-4 p-l-r-0">
            <div class="data-block__data"></div>
            <div class="data-block__title"></div>
        </div>
        <div class="data-block pull-left col-md-4 p-l-r-0">
            <div style="height: 20px;width: 100%;"></div>
            <div class="data-block__title"></div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
