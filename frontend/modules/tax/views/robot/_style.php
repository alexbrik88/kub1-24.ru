<style>
    .step-by-step {
        color: #171717;
        display: block!important;
    }

    .step-by-step-menu ul {
        list-style: none;
        margin: 0;
        padding: 0;
        width: 100%;
    }

    .step-by-step-menu ul li {
        width: 19.99%;
        border:1px solid #eee;
    }

    .step-by-step-menu ul li a:hover, .step-by-step-menu ul li a:active {
        text-decoration: none;
    }

    .step-by-step-menu ul li, .step-by-step-menu ul li a {
        display: block;
        float: left;
        color: #171717;
    }

    .step-by-step-menu ul li a {
        height: 125px;
    }

    .step-by-step-menu ul li .image-block {
        width: 90px;
        height: 90px;
        border-radius: 45px !important;
        padding: 23px 28px;
        background: #eee;
        float:left;
        margin-top: 15px;
        margin-left: 5px;
    }
    @media (min-width: 1376px) {
        .step-by-step-menu ul li .image-block {
            margin-left: 25px;
        }
    }

    .step-by-step-menu ul li .text-block {
        padding-top: 4px;
        font-size: 14px;
        font-weight: 700;
        --margin-left: 130px;
        --margin-top: 35px;
        --max-width: 140px;
    }

    /** DISPLAY AS TABLE */
    .step-by-step-menu ul li a {
        display:table;
    }
    .step-by-step-menu ul li .image-block,
    .step-by-step-menu ul li .text-block {
        display:table-cell;
        vertical-align: middle;
    }
    .step-by-step-menu ul li .text-block {
        padding-left: 10px;
    }


    .step-by-step-menu ul li .image-block > div {
        width: 32px;
        height: 44px;
        background-size: 100%;
        background-repeat: no-repeat;
        background-position: 0 0;
    }

    .step-by-step-menu .line {
        padding-top: 35px;
        border: none;
        width: 300px;
        display: table-cell;
    }

    .step-by-step-menu .line > div {
        width: 100%;
        height: 4px;
        background-color: #969a9a;
    }

    /* active */
    .step-by-step-menu .active {
        border: 1px solid #67d77d;
    }

    .step-by-step-menu .active .line > div {
        background-color: #67d77d;
    }

    .step-by-step-menu .active .image-block {
        background-color: #67d77d;
    }

    .step-by-step-menu .active .image-block > div {
        --background-position: 0px -43px;
    }

    .step-by-step-menu .active .text-block {
        color: #000 !important;
    }

    /* finish */
    .step-by-step-menu .finish .line > div {
        background-color: #67d77d;
    }

    .step-by-step-menu .finish .line > div {
        background-color: #67d77d;
    }

    .step-by-step-menu .finish .image-block {
        border-color: #67d77d;
    }

    .step-by-step-menu .finish .image-block > div {
        --background-position: 0px -86px;
    }

    .step-by-step-menu .finish {
        background-color: #67d77d !important;
    }
    .step-by-step-menu .finish .text-block {
        color: #fff !important;
    }

    /* style borders*/
    .bord-light-b {
        border-bottom: 1px solid #ececec;
    }

    /* info */

    .step-by-step-info p {
        margin: 0px;
        padding: 0px;
    }

    /* style form */
    .step-by-step-form{
        min-height: 400px;
        margin-top:25px;
    }

    .step-by-step-form h3 {
        padding-bottom: 10px;
        font-size: 23px;
    }
    .step-by-step-form h4 {
        display: inline-block;
        padding-top: 10px;
        font-size: 18px;
        border-bottom: 1px dashed #171717;
    }

    .step-by-step-form .mini-inp label, .step-by-step-form .mini-inp input {
        max-width: 70px;
    }

    ::-webkit-input-placeholder {
        /* Chrome/Opera/Safari */
        font-style: italic;
        color: #cccccc;
    }

    ::-moz-placeholder {
        /* Firefox 19+ */
        font-style: italic;
        color: #cccccc;
    }

    :-ms-input-placeholder {
        /* IE 10+ */
        font-style: italic;
        color: #cccccc;
    }

    :-moz-placeholder {
        /* Firefox 18- */
        font-style: italic;
        color: #cccccc;
    }

    .step-by-step .darkblue.btn {
        color: #ffffff;
    }

    .step-by-step button {
        color: #ffffff;
        padding: 7px 15px;
    }
    @media (min-width: 768px) {
        min-width: 180px;
    }

    /* table */

    .table-block {

    }

    .table-block .date .top{
        height: 30px;
        width: 100px;
        border-bottom: 1px solid #cecece;
        margin: 0 auto;
    }
    .table-block .date .bottom{
        height: 30px;
        width: 100px;
        margin: 0 auto;
    }
    .table-block .date .num{
        border: 1px solid #cecece;
        padding: 5px;
        height: 30px;
        width: 30px;
    }

    .border-light{
        border: 1px solid #cecece;
    }

    .border-light-t{
        border-top: 1px solid #cecece;
    }
    .border-light-b{
        border-bottom: 1px solid #cecece;
    }
    .border-light-l{
        border-left: 1px solid #cecece;
    }
    .border-light-r{
        border-right: 1px solid #cecece;
    }
    .border2-light-t{
        border-top: 2px solid #cecece;
    }
    .border2-light-b{
        border-bottom: 2px solid #cecece;
    }
    .border2-light-l{
        border-left: 2px solid #cecece;
    }
    .border2-light-r{
        border-right: 2px solid #cecece;
    }
    .pad5-l {
        padding-left: 5px !important;
    }
    .pad5-t {
        padding-top: 5px !important;
    }
    .btn.darkblue {
        color: #ffffff;
    }

    /* MXFI COLORS */
    .select2-container--krajee .select2-selection__clear {
        top: -0.1rem;
    }
    .has-error .twitter-typeahead .tt-input, .has-error .twitter-typeahead .tt-hint,
    .has-success .twitter-typeahead .tt-input, .has-success .twitter-typeahead .tt-hint,
    .has-success .twitter-typeahead .tt-input:focus, .has-success .twitter-typeahead .tt-hint:focus {
        box-shadow:none;
        -webkit-box-shadow: none;
    }
    .step-by-step-form .form-group p {
        padding:0;
    }
    .form-group.form-md-line-input.has-success .form-control:not([readonly]) {
        border-bottom: 1px solid #c2cad8;
    }
    .form-group.form-md-line-input.has-success .form-control.edited:not([readonly]) ~ label, .form-group.form-md-line-input.has-success .form-control.form-control-static ~ label, .form-group.form-md-line-input.has-success .form-control:focus:not([readonly]) ~ label, .form-group.form-md-line-input.has-success .form-control.focus:not([readonly]) ~ label {
        color: #888888;
    }
    .form-md-underline .form-group.form-md-line-input .form-control:focus:not([readonly]) ~ .help-block {
        color: red;
    }
    .step-by-step-form .mb5 {
        margin-bottom: 5px;
    }
    .step-by-step-form .mb1 {
        margin-bottom: 1px;
    }
    .form-group.field-company-address_legal_postcode {
        padding-top: 15px;
    }

    .sbs-tooltip {
        display: block;
        position: absolute;
        top: 25px;
        right: -15px;
    }

    .dictionary-ifns-tip {
        cursor: pointer;
    }

    .step-by-step-form .form-group label {
        color: #888888;
    }

    .darkblue-invert {
        color:#4276a4!important;
        border:1px solid #4276a4!important;
        background-color:#fff!important;
        margin-top:15px;
    }

    .button-bottom-page-lg-double {
        width: 24.9%;
        text-align: left;
    }

    .color-link  {
        color: #5b9bd1;
    }
    .cursor-pointer {
        cursor:pointer;
    }

    .my-dropdown:first-child {
        border-top: 1px solid #999;
        cursor: pointer;
    }
    .my-dropdown-body {
        border-bottom: 1px solid #999;
    }
    .my-dropdown-body tr {
        height:24px;
    }
    .my-dropdown .caption {
        font-weight: bold;
        height:34px;
        border-bottom: 1px solid #999;
    }
    .my-dropdown table {
        width:100%;
    }
    .my-dropdown .caption {
        font-size:14px;
    }
    .my-dropdown table td:first-child {
        text-align: left;
    }
    .my-dropdown table td:nth-child(2) {
        text-align: right;
    }

    #taxable-group .btn-group {
        border: 1px solid #ddd;
    }
    #taxable-group .btn {
        width: 49px;
        color: #333;
        background-color: #fff;
    }
    #taxable-group .btn:last-child {
        border-left:1px solid #ddd;
    }
    #taxable-group .btn:hover {
        background-color: #00c5ec;
    }
    #get-fias-ittem > img {
        margin-top: -2px;
    }
    .sbs-el.disabled {
        cursor: not-allowed;
    }
    .odds-panel-trigger {
        padding-left:5px;
        cursor:pointer;
        width:35px;
        margin-top:-3px;
    }
    .mar-t-20 {
        margin-top: 20px;
    }
    .form-group.form-md-line-input.has-error .help-block {
        color: red !important;
        opacity: 1 !important;
    }
    #get-fias-ittem.error,
    #not_use_account.error {
        border-color: red !important;
    }
    .form-group.form-md-line-input.has-error .form-control {
        border-bottom: 1px solid red;
    }
    .form-group.form-md-line-input.has-error .form-control.edited:not([readonly]) ~ label,
    .form-group.form-md-line-input.has-error .form-control.form-control-static ~ label,
    .form-group.form-md-line-input.has-error .form-control:focus:not([readonly]) ~ label,
    .form-group.form-md-line-input.has-error .form-control.focus:not([readonly]) ~ label {
        color: red;
    }
    .has-error .help-block,
    .has-error .help-inline,
    .has-error .control-label,
    .has-error .radio,
    .has-error .checkbox,
    .has-error .radio-inline,
    .has-error .checkbox-inline {
        color: red;
    }

    #select2-customer_contractor_id-results .select2-results__option:first-child,
    #select2-cashorderflows-contractorinput-results .select2-results__option:first-child,
    #select2-cashbankflowsform-rs-results .select2-results__option:first-child,
    #select2-seller_contractor_id-results .select2-results__option:first-child {
        background: #ffb848;
        color: #fff;
    }

    #fias-result-modal .modal-header {
        padding-top: 3px;
        padding-bottom: 3px;
    }
    #fias-result-modal .modal-header .close {
        margin-top: 10px !important;
    }


    /* TaxRobot Modal */
    @media (max-width: 640px) {
        #buttons-bar-fixed {
            padding:10px;
        }
        .modal-scrollable .modal.fade.in {
            top: 4%!important;
            left: 4%!important;
        }
        .modal .modal-dialog {
            margin-right: 4%!important;
        }
        .not-clickable {
            display: none!important;
        }
        .page-footer {
            position: relative;
            margin-bottom:60px;
        }
        .page-container {
            padding-bottom: 0!important;
        }
    }
    @media (max-width:340px) {
        .page-content-wrapper .page-content {
            width: 100%;
        }
        .form-md-underline .form-group.form-md-line-input.form-md-floating-label {
            white-space: nowrap;
            overflow: hidden;
        }
    }
    .get-null-declaration {
        margin-top:0;
        min-width: 180px;
    }
    .kudir-btn {
        min-width: 260px;
        margin-top:0;
    }
    @media (max-width: 400px) {
        .get-null-declaration {
            min-width: unset;
            width: 100px;
            white-space: normal;
            padding: 5px 10px;
            text-align: center;
            font-size: 13px;
            line-height: 14px;
        }
    }
    @media (max-width: 500px) {
        .kudir-btn {
            min-width: unset;
            width: 100px;
        }
        .taxrobot-pay-panel {
            width:300px;
        }
        .taxrobot-pay-panel .side-panel-close {
            top: 20px;
            left: 260px;
            z-index: 2;
        }
    }
    @media (max-width: 300px) {
        .taxrobot-pay-panel {
            width:220px;
        }
        .taxrobot-pay-panel .main-block {
            padding:10px;
        }
        .taxrobot-pay-panel .side-panel-close {
            left: 180px;
        }
    }

</style>