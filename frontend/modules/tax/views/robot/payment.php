<?php

use common\components\date\DateHelper;
use common\components\grid\DropDownSearchDataColumn;
use common\components\TextHelper;
use common\models\document\PaymentOrder;
use common\models\document\status\PaymentOrderStatus;
use frontend\models\Documents;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use frontend\modules\documents\widgets\PaymentOrderPaid;
use frontend\rbac\permissions;
use kartik\select2\Select2;
use yii\bootstrap\Dropdown;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $model2 common\models\document\PaymentOrder */
/* @var $company common\models\Company */
/* @var $taxRobot common\components\TaxRobotHelper */
/* @var $searchModel frontend\modules\tax\models\TaxrobotNeedPaySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Платеж по налогу УСН';

$sum = [
    'Usn6' => [],
    'Oms' => [],
    'Pfr' => [],
    'Over300' => [],
];
foreach ($dataProvider->getModels() as $model) {
    $type = $taxRobot->getPaymentTypeByPaymentOrder($model);
    $period = $taxRobot->getPeriodLabelByCode($model->tax_period_code, true);
    if (isset($sum[$type][$period])) {
        $sum[$type][$period] += $model->sum;
    } else {
        $sum[$type][$period] = $model->sum;
    }
}
$usn = ArrayHelper::remove($sum, 'Usn6');
$sumTax = array_sum($sum['Oms']) + array_sum($sum['Pfr']) + array_sum($sum['Over300']);
$sumUsn6 = array_sum($usn);

$urlPeriod = $taxRobot->getUrlPeriodId();

$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canStatus = Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = Yii::$app->getUser()->can(permissions\document\Document::VIEW);

$bankingRoute = ['/tax/robot/payment'];
if (($urlPeriod = $taxRobot->getUrlPeriodId()) !== null) {
    $bankingRoute['period'] = $urlPeriod;
}
$fixedPayments = $taxRobot->getFixedPayments($taxRobot->getYear());
?>

<?= $this->render('_style') ?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'tax-robot-payment-pjax',
]); ?>

<div class="row">
    <div class="col-xs-12 step-by-step" style="margin-bottom: 35px;">
        <?= $this->render('_steps', ['step' => 5]) ?>
    </div>
    <div class="col-xs-12">
        <div class="bord-light-b" style="font-size:16px; font-weight: bold;">
            Платежи по взносам и налогу УСН за
            <div style="display: inline-block; width: 200px;">
                <div class="dropdown">
                    <?= Html::tag('div', $taxRobot->period->label, [
                        'class' => 'dropdown-toggle',
                        'data-toggle' => 'dropdown',
                        'style' => 'display: inline-block; border-bottom: 1px dashed #000; cursor: pointer;',
                    ])?>
                    <?= Dropdown::widget([
                        'id' => 'employee-rating-dropdown',
                        'encodeLabels' => false,
                        'items' => $taxRobot->getPeriodDropdownItems('payment'),
                    ])?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="page-content-in mar-t-20" style="padding-bottom: 30px;">
    <div class="row" style="">
        <div class="col-xs-12 col-lg-7">
            <div class="portlet customer-info">
                <div class="marg pad10-b font-bold" style="font-size:16px">
                    Необходимо оплатить
                </div>
                Если в списке есть платежи, которые вы уже оплатили, то либо
                <?= Html::a('загрузите выписку', [
                    '/cash/banking/default/index',
                    'p' => Banking::routeEncode($bankingRoute),
                ], [
                    'class' => 'banking-module-open-link',
                    'data' => [
                        'pjax' => '0',
                    ],
                ]) ?>,
                что бы оплата налогов проставилась автоматически,
                либо поставьте напротив платежного поручения галочку
                и с помощью кнопки «Оплачено» измените статус платежного поручения на «Оплачено»
            </div>
            <div class="portlet customer-info">
                <div class="my-dropdown">
                    <table>
                        <tr class="caption">
                            <td>Страховые взносы за себя</td>
                            <td><?= number_format($sumTax/ 100, 2, '.', ' ') ?> ₽</td>
                        </tr>
                    </table>
                    <div class="my-dropdown-body" style="display:none">
                        <table>
                            <?php foreach ($sum as $type => $data) : ?>
                                <?php foreach ($data as $period => $amount) : ?>
                                    <tr>
                                        <td><?= $taxRobot->{"get{$type}Title"}($period) ?></td>
                                        <td><?= number_format($amount/100, 2, '.', ' ') ?> ₽</td>
                                    </tr>
                                <?php endforeach ?>
                            <?php endforeach ?>
                        </table>
                    </div>
                </div>
                <div class="my-dropdown">
                    <table>
                        <tr class="caption" style="cursor: pointer;">
                            <td>Платежи по налогу УСН Доходы</td>
                            <td><?= number_format($sumUsn6 / 100, 2, '.', ' ') ?> ₽</td>
                        </tr>
                    </table>
                    <div class="my-dropdown-body" style="display:none">
                        <table>
                            <?php foreach ($usn as $period => $amount) : ?>
                                <tr>
                                    <td><?= $taxRobot->getUsn6Title($period) ?></td>
                                    <td><?= number_format($amount/100, 2, '.', ' ') ?> ₽</td>
                                </tr>
                            <?php endforeach ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-lg-5 pull-right mar-b-20" style="max-width: 480px;">
            <div class="bord-dark" style="padding: 5px 10px;">
                <div class="text-bold" style="font-size:16px">
                    Фиксированные взносы ИП
                </div>
                Каждый ИП должен заплатить в <?= $taxRobot->getYear() ?> г. фиксированные взносы:
                <br>
                В ПФР (пенсионное страхование) - <?= number_format($fixedPayments->pfr/100, 2, '.', ' ') ?> ₽
                <br>
                В ОМС (медицинское страхование) - <?= number_format($fixedPayments->oms/100, 2, '.', ' ') ?> ₽
                <span style="padding:5px 0; display: block">
                    Если ваше ИП зарегистрировано в <?= $taxRobot->getYear() ?> г.,
                    то вы должны заплатить не всю сумму фиксированных взносов, а меньшую сумму, т.к. суммы выше рассчитаны за все дни года.
                </span>
                <div class="text-bold">
                    Сроки уплаты взносов
                </div>
                Суммы фиксированных взносов за <?= $taxRobot->getYear() ?> год,
                нужно перечислить в бюджет не позднее 31.12.<?= $taxRobot->getYear() ?>.
                <div class="text-bold">
                    Важно!
                </div>
                Данные платежи уменьшают налог УСН Доходы. Поэтому оплачивая их в начале года, вы сможете уменьшить УСН Доходы и точно не переплатите в бюджет.
                <br>
                Также можно разделить суммы на 4 и платить поквартально:
                <br>
                В ПФР – <?= number_format($fixedPayments->pfr/100/4, 2, '.', ' ') ?> ₽ в квартал
                <br>
                В ОМС – <?= number_format($fixedPayments->oms/100/4, 2, '.', ' ') ?> ₽ в квартал
            </div>
        </div>
    </div>

    <div class="portlet box darkblue">
        <div class="portlet-title">
            <div class="caption">
                Список платёжных поручений
            </div>
            <div class="actions joint-operations col-sm-6" style="display:none; min-width: 430px; padding-right: 0 !important;">
                <?php if ($canStatus) : ?>
                    <?= PaymentOrderPaid::widget([
                        'id' => 'many-paid',
                        'toggleButton' => [
                            'tag' => 'a',
                            'label' => 'Оплачено',
                            'class' => 'btn btn-default btn-sm',
                            'data-pjax' => 0,
                        ],
                        'pjaxContainer' => '#tax-robot-payment-pjax',
                    ]) ?>
                <?php endif ?>
                <?php if ($canPrint) : ?>
                    <?= Html::a('Импорт в банк', Url::to(['/documents/payment-order/import']), [
                        'class' => 'btn btn-default btn-sm multiple-import-link no-ajax-loading',
                        'data-url' => Url::to(['/documents/payment-order/import']),
                        'data-pjax' => 0,
                    ]); ?>
                <?php endif ?>
                <?php if ($canDelete) : ?>
                    <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                        'class' => 'btn btn-default btn-sm',
                        'data-toggle' => 'modal',
                        'data-pjax' => 0,
                    ]); ?>
                    <div id="many-delete" class="confirm-modal fade modal" role="dialog"
                         tabindex="-1" aria-hidden="true"
                         style="display: none; margin-top: -51.5px;">
                        <div class="modal-dialog ">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="form-body">
                                        <div class="row">Вы уверены, что хотите удалить
                                            выбранные платежные поручения?
                                        </div>
                                    </div>
                                    <div class="form-actions row">
                                        <div class="col-xs-6">
                                            <?= Html::a('ДА', null, [
                                                'class' => 'btn darkblue pull-right modal-many-delete',
                                                'data-url' => Url::to(['/documents/payment-order/many-delete',]),
                                            ]); ?>
                                        </div>
                                        <div class="col-xs-6">
                                            <button type="button" class="btn darkblue"
                                                    data-dismiss="modal">НЕТ
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif ?>
                <?php if ($canPrint) : ?>
                    <?= Html::a('<i class="fa fa-print"></i> Печать', Url::to([
                        '/documents/payment-order/many-document-print',
                        'actionType' => 'pdf',
                        'type' => Documents::IO_TYPE_IN,
                        'multiple' => '',
                    ]), [
                        'class' => 'btn btn-default btn-sm multiple-print',
                        'target' => '_blank',
                        'data-pjax' => 0,
                    ]); ?>
                <?php endif ?>
            </div>
        </div>
        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <div
                    class="dataTables_wrapper dataTables_extended_wrapper">

                    <?= common\components\grid\GridView::widget([
                        'id' => 'payment-order-search',
                        'filterModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'emptyText' => 'Вы еще не создали ни одного платежного поручения.',
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover dataTable documents_table',
                            'aria-describedby' => 'datatable_ajax_info',
                            'role' => 'grid',
                        ],

                        'headerRowOptions' => [
                            'class' => 'heading',
                        ],

                        'options' => [
                            'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                        ],
                        'layout' => "{items}",
                        'columns' => [
                            [
                                'header' => Html::checkbox('', false, [
                                    'class' => 'joint-operation-main-checkbox',
                                ]),
                                'headerOptions' => [
                                    'class' => 'text-center',
                                    'width' => '5%',
                                ],
                                'contentOptions' => [
                                    'class' => 'text-center',
                                ],
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return Html::checkbox('PaymentOrder[' . $model->id . '][checked]', false, [
                                        'class' => 'joint-operation-checkbox',
                                        'data' => [
                                            'income' => 0,
                                            'expense' => bcdiv($model->sum, 100, 2),
                                        ],
                                    ]);
                                },
                            ],
                            [
                                'attribute' => 'document_date',
                                'label' => 'Дата',
                                'headerOptions' => [
                                ],
                                'value' => function ($model) {
                                    return DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                                },
                            ],
                            [
                                'attribute' => 'document_number',
                                'label' => '№',
                                'headerOptions' => [
                                ],
                                'format' => 'raw',
                                'value' => function ($model) use ($urlPeriod) {
                                    return Html::a($model->document_number, [
                                        'view-payment-order',
                                        'id' => $model->id,
                                        'period' => $urlPeriod,
                                        'part' => 'unpaid',
                                    ], [
                                        'data' => [
                                            'pjax' => '0',
                                        ],
                                    ]);
                                },
                            ],
                            [
                                'label' => 'Сумма',
                                'headerOptions' => [
                                    'width' => '9%',
                                ],
                                'contentOptions' => [
                                    'class' => 'text-right',
                                ],
                                'format' => 'html',
                                'attribute' => 'sum',
                                'value' => function ($model) {
                                    $price = \common\components\TextHelper::invoiceMoneyFormat($model->sum, 2);
                                    return '<span class="price" data-price="' . str_replace(" ", "", $price) . '">' . $price . '</span>';
                                },
                            ],
                            [
                                'class' => 'common\components\grid\DropDownSearchDataColumn',
                                'attribute' => 'contractor_name',
                                'label' => 'Контрагент',
                                'enableSorting' => false,
                                'format' => 'html',
                                'filter' => $searchModel->getContractorFilterItems(),
                                'value' => function ($model) {
                                    return '<span title="' . Html::encode($model->contractor_name) . '">' . $model->contractor_name . '</span>';
                                },
                            ],
                            [
                                'attribute' => 'purpose_of_payment',
                                'label' => 'Назначение',
                                'enableSorting' => false,
                            ],
                            [
                                'class' => 'common\components\grid\DropDownSearchDataColumn',
                                'attribute' => 'status_id',
                                'label' => 'Статус',
                                'filter' => $searchModel->getStatusFilterItems(),
                                'value' => 'paymentOrderStatus.name',
                            ],
                            [
                                'class' => 'common\components\grid\DropDownSearchDataColumn',
                                'attribute' => 'expenditure_id',
                                'label' => 'Статья',
                                'filter' => $searchModel->getExpenditureFilterItems(),
                                'value' => 'expenditureItem.name',
                            ],
                        ],
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div id="buttons-bar-fixed">
        <?= $this->render('parts_payment/action-buttons', [
            'taxRobot' => $taxRobot,
            'urlPeriod' => $urlPeriod,
        ]); ?>
    </div>
</div>

<?= $this->render('_pay_extra_modal', [
    'taxRobot' => $taxRobot,
]); ?>

<style>
    #summary-container table.pull-left td:nth-child(3),
    #summary-container table.pull-left td:nth-child(5) {
        display:none!important;
    }
</style>
<?= \frontend\modules\cash\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a('<i class="fa fa-print"></i> Печать', Url::to([
            '/documents/payment-order/many-document-print',
            'actionType' => 'pdf',
            'type' => Documents::IO_TYPE_IN,
            'multiple' => '',
        ]), [
            'class' => 'btn darkblue text-white btn-sm multiple-print',
            'target' => '_blank',
            'data-pjax' => 0,
        ]) : null,
        $canDelete ? Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
            'class' => 'btn btn-sm darkblue text-white',
            'data-toggle' => 'modal',
        ]) : null,
        $canPrint ? Html::a('Импорт в банк', Url::to(['/documents/payment-order/import']), [
            'class' => 'btn darkblue text-white btn-sm multiple-import-link no-ajax-loading',
            'data-url' => Url::to(['/documents/payment-order/import']),
            'data-pjax' => 0,
        ]) : null,
        $canStatus ? PaymentOrderPaid::widget([
            'id' => 'many-paid',
            'toggleButton' => [
                'tag' => 'a',
                'label' => 'Оплачено',
                'class' => 'btn darkblue text-white btn-sm',
                'data-pjax' => 0,
            ],
            'pjaxContainer' => '#tax-robot-payment-pjax',
        ]) : null,
    ],
]); ?>





<?php \yii\widgets\Pjax::end(); ?>

<?= BankingModalWidget::widget([
    'pageTitle' => $this->title,
    'pageUrl' => Url::to(['robot/payment', 'period' => $urlPeriod]),
]) ?>

<?php $this->registerJs(<<<JS
$(document).on('click', '.my-dropdown', function() {
    var inner = $(this).find('.my-dropdown-body');
    if ($(inner).is(':hidden'))
        $(inner).show(250);
    else
        $(inner).hide(250);
});
JS
)?>
