<style>
    #robot-first-show-modal .carousel-control.left,
    #robot-first-show-modal .carousel-control.right {
        background-image:none!important;
    }
    #robot-first-show-modal .glyphicon-chevron-right,
    #robot-first-show-modal .glyphicon-chevron-left {
        color: #000!important;
    }
    #robot-first-show-modal .glyphicon-chevron-left {
        margin-left: -30px;
    }
    #robot-first-show-modal .glyphicon-chevron-right {
        margin-right: -30px;
    }
</style>
<?php
use common\widgets\Modal;
use yii\helpers\Html;
use yii\bootstrap\Carousel;

$header = ' Так будет выглядеть ваша декларация';

$carousel = [];
foreach (range(1, 3) as $n) {
    $src = "/img/examples/declaration-{$n}.jpg";
    if (($timestamp = @filemtime(Yii::getAlias("@frontend/web{$src}"))) > 0) {
        $src .= "?v={$timestamp}";
    }
    $carousel[] = [
        'content' => Html::img($src),
        'caption' => '',
        'options' => ['class' => 'my-class']
    ];
}

Modal::begin([
    'id' => 'robot-first-show-modal',
    'header' => Html::tag('h1', Html::tag('span', '', [
            'class' => 'glyphicon glyphicon-info-sign pull-left',
            'style' => 'font-size:inherit;line-height:inherit;',
        ]) . $header),
    'headerOptions' => ['style' => 'background-color: #00b7af; color: #fff;'],
    'footerOptions' => ['style' => 'text-align: center;'],
    'clientOptions' => ['show' => false],
    'footer' => Html::button('Ок', [
        'class' => 'btn yellow',
        'data-dismiss' => 'modal',
        'aria-hidden' => 'true',
    ]),
]);

echo Html::beginTag('div', ['class' => 'text-center']);

echo Carousel::widget([
    'items' => $carousel,
    'options' => ['class' => 'carousel slide', 'data-interval' => '60000'],
    'controls' => [
        '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>',
        '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'
    ]
]);

echo Html::endTag('div');
Modal::end();

$this->registerJs(<<<JS
    $(document).ready(function() {
        if (window.matchMedia("only screen and (min-width: 1024px)").matches) {
            $("#robot-first-show-modal").modal("show");
        }
    });
JS
);
