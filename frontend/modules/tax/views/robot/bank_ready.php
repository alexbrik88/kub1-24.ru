<?php

use frontend\components\Icon;
use yii\bootstrap\Modal;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $taxRobot common\components\TaxRobotHelper */

?>

<?php Modal::begin([
    'id' => 'bank_ready_modal',
    'header' => 'Готово!',
    'clientOptions' => [
        'show' => true,
    ],
]); ?>

    <div class="mb-3" style="
        position: relative;
        width: 100%;
        padding-top: 40%;
        margin: 0 auto;
        background-image: url(/images/declaration_for_10_min.png);
        background-position: center;
        background-size: 150%;
    "></div>
    <div class="text-center">
        <div>
            Налоги по <strong><?= $taxRobot->company->title ?></strong> рассчитаны,
            <br>
            платежки по налогам подготовлены, декларация заполнена.
            <br>
            <strong>
                Для скачивания платежек и декларации, необходимо оплатить сервис
            </strong>
        </div>
        <?= Html::button('Оплатить доступ к сервису', [
            'data-dismiss' => 'modal',
            'class' => 'button-regular button-regular_red mt-3 taxrobot-pay-panel-trigger',
        ]) ?>
    </div>

<?php Modal::end(); ?>
