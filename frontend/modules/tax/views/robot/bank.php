<?php

use backend\models\Bank;
use common\components\date\DateHelper;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\components\ImageHelper;
use common\components\TaxRobotHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\cash\form\CashBankFlowsForm;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\document\InvoiceExpenditureItem;
use common\models\service\SubscribeTariffGroup;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use frontend\modules\cash\widgets\StatisticWidget;
use frontend\modules\cash\widgets\SummarySelectWidget;
use frontend\modules\tax\widgets\AddIncomeWidget;
use frontend\rbac\permissions;
use frontend\widgets\RobotCheckingAccountantFilterWidget;
use frontend\widgets\BoolleanSwitchWidget;
use kartik\switchinput\SwitchInput;
use yii\bootstrap\Modal;
use yii\bootstrap\Dropdown;
use yii\grid\ActionColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use \frontend\modules\cash\modules\ofd\components\Ofd;
use \common\models\company\RegistrationPageType;
use \frontend\modules\cash\modules\ofd\widgets\OfdModalWidget;
use common\models\bank\BankingParams;

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\SqlDataProvider
 * @var $company Company
 */

$this->title = 'Доходы за ' . $taxRobot->getPeriodLabel();

$p = Banking::currentRouteEncode();

$canCreate = Yii::$app->user->can(frontend\rbac\permissions\Cash::CREATE);
$canUpdate = Yii::$app->user->can(frontend\rbac\permissions\Cash::UPDATE);
$canDelete = Yii::$app->user->can(permissions\Cash::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);

$accountArray = $company->getCheckingAccountants()->orderBy([
    'type' => SORT_ASC,
    'rs' => SORT_ASC,
])->all();
$account = end($accountArray);

// Для НУЛЕВОЙ декларации
if (empty($account)) {
    $account = new CheckingAccountant(['company_id' => $company->id, 'type' => CheckingAccountant::TYPE_MAIN]);
}

$cashbox = $company->getCashboxes()->orderBy(['is_main' => SORT_DESC])->one();

$contractorList = $taxRobot->getContractorList($taxRobot->getIncomeSearchQuery());
$statistic = ArrayHelper::index($taxRobot->getIncomeStatistic(), 'flow_name', 'payment_type');
$bankStatistic = ArrayHelper::getValue($statistic, TaxRobotHelper::TYPE_BANK, []);
$orderStatistic = ArrayHelper::getValue($statistic, TaxRobotHelper::TYPE_ORDER, []);

$bankLogo = Html::tag('img', '', [
   'src' => '/img/icons/bank1.png',
    'style' => 'width:27px;'
]);

$bankingRoute = ['/tax/robot/bank'];
if (($urlPeriod = $taxRobot->getUrlPeriodId()) !== null) {
    $bankingRoute['period'] = $urlPeriod;
}
$bankIntegrationUrl = null;
$bankLoadStatementUrl = null;
$tooltipBankLoadStatementUrl = null;
$bankData = [];
foreach ($accountArray as $account) {
    $bank = Bank::find()->andWhere(['bik' => $account->bik])->byStatus(!Bank::BLOCKED)->one();
    if ($bank !== null && $bank->little_logo_link) {
        $logo = ImageHelper::getThumb($bank->getUploadDirectory() . $bank->little_logo_link, [32, 32], [
            'class' => 'little_logo_bank',
            'style' => 'display: inline-block;',
        ]);
    } else {
        $logo = $bankLogo;
    }

    $alias = Banking::aliasByBik($account->bik);

    $bankData[$account->rs] = [
        'active' => $account->type != CheckingAccountant::TYPE_CLOSED,
        'logo' => $logo,
        'bank_name' => $account->bank_name,
        'flow_name' => $account->rs,
        'total_amount' => 0,
        'taxable_amount' => 0,
        'account_id' => $account->id,
        'banking_url' => ($alias) ?
            Url::to(["/cash/banking/{$alias}/default/index", 'p' => $p]) :
            Url::to(["/cash/banking/default/index", 'p' => $p])
    ];

    if ($alias) {
        $bankIntegrationUrl = $bankData[$account->rs]['banking_url'];
        if (Banking::hasIntegration($company->id, $alias)) {
            $bankLoadStatementUrl = $bankData[$account->rs]['banking_url'];
        }
    }
}
if ($bankIntegrationUrl || $bankLoadStatementUrl) {
    $tooltipBankLoadStatementUrl = $bankIntegrationUrl;
    foreach ($accountArray as $account) {
        $alias = Banking::aliasByBik($account->bik);
        if (!$alias) {
            $bankLoadStatementUrl = $bankIntegrationUrl = Url::to(["/cash/banking/default/index", 'p' => $p, 'account_id' => $account->id]);
            break;
        }
    }
} else {
    $tooltipBankLoadStatementUrl = Url::to(["/cash/banking/default/index", 'p' => $p, 'account_id' => $account->id]);
}

foreach ($bankStatistic as $account_rs => $data) {
    if (isset($bankData[$data['flow_name']])) {
        $bankData[$data['flow_name']]['active'] = true;
        $bankData[$data['flow_name']]['total_amount'] = $data['total_amount'];
        $bankData[$data['flow_name']]['taxable_amount'] = $data['taxable_amount'];
    } else {
        $bankData[$data['flow_name']] = [
            'active' => true,
            'logo' => $bankLogo,
            'bank_name' => '',
            'flow_name' => $data['flow_name'],
            'total_amount' => 0,
            'taxable_amount' => 0,
            'account_id' => null,
            'banking_url' => null,
        ];
    }
}

function moneyFormat($val)
{
    return number_format($val / 100, 2, '.', ' ');
}

$cashboxes = \common\models\cash\Cashbox::find()->where(['company_id' => $company->id])->all();
$cashboxRegNumbers = ArrayHelper::map((array)$cashboxes, 'name', 'reg_number');

$isTaxcomUser = Yii::$app->request->cookies['taxcom_user'] || $company->registration_page_type_id == RegistrationPageType::PAGE_TYPE_TAX_COM_OFD;
$isPulsateNullDeclaration = ($company->mainCheckingAccountant === null || $company->registration_page_type_id == RegistrationPageType::PAGE_TYPE_NULL_DECLARATION_TEMPLATE_IP);
$isPulsateBank = !$isPulsateNullDeclaration;

$showReadyModal = $dataProvider->totalCount > 0 && Yii::$app->session->remove('show_bank_ready_modal');

$js = <<<JS
var updateMoneyMovement = function(url) {
    $('#update-movement-modal').modal('show');
    $.pjax({url: url, push: false, container: '#update-movement-pjax'});
}
$(document).on("click", ".update-attribute-tooltip", function (e) {
    $(".update-attribute-tooltip").tooltipster("close");
    $(this).tooltipster("open");
});
$(document).on("submit", "form.update-attribute-tooltip-form", function (e) {
    e.preventDefault();
    var form = $(this);
    $.ajax({
        url: this.action,
        method: this.method,
        data: form.serialize(),
        success: function() {
            $(".update-attribute-tooltip").tooltipster("close");
            $.pjax.reload("#tax-robot-pjax");
        }
    })
});
$(document).on("click", "a.update-movement-link", function(e) {
    e.preventDefault();
    updateMoneyMovement(this.href);
});
$(document).on("click", "tr.flow-row > td:not(.no-update-modal)", function(e) {
    var row = $(this).closest('tr.flow-row');
    updateMoneyMovement(row.data("update-url"));
});
$(document).on('hidden.bs.modal', '#update-movement-modal', function (e) {
    $('.modal-header h1', this).html('');
    $('#update-movement-pjax', this).html('');
})
$(document).on("pjax:complete", "#tax-robot-pjax", function(e) {
});
$('.collapse').on('show.bs.collapse hidden.bs.collapse', function (e) {
    $($(e.target).data('chevron')).toggleClass('fa-chevron-down fa-chevron-left')
})
$(document).on("click", "#taxable-group .btn", function() {
    var flow_ids = [];
    $('.joint-operation-checkbox:checked').each(function() {
        flow_ids.push($(this).val());
    });

    $.get("/tax/robot/taxable-group", {
        is_taxable: $(this).attr('data-taxable'),
        flow_ids: flow_ids,
    }, function (data) {
        window.location.reload(false);
    });
});
JS;
$this->registerJs($js);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-group-taxable',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
// helpers tooltip
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-first-r',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-light'],
        'contentAsHTML' => true,
        'position' => 'left',
    ],
]);
$requisitesPositionTooltip = 'top: -45px;right: ' . (empty($statistic) ? '850' : '570') .'px';
$addProductPositionTooltip = 'top: -45px;left:-300px';
$logoPositionTooltip = 'top: 10px;left:-350px';
$savePositionTooltip = 'top: -80px;right: 485px';
$showHelp = $company->show_help_taxrobot_banking || Yii::$app->request->get('show_help');
?>

<?= $this->render('_style') ?>

<style type="text/css">
tr.flow-row > td:not(.no-update-modal) {
    cursor: pointer;
}
</style>

<?php \yii\widgets\Pjax::begin([
    'id' => 'tax-robot-pjax',
]); ?>

<div class="row">
    <div class="hidden-xs hidden-sm hidden-md col-xs-12 step-by-step" style="margin-bottom: 35px;">
        <?= $this->render('_steps', ['step' => 3]) ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 pad-t-5 mar-b-20" style="font-size:16px; font-weight: bold;">
        <div style="padding-bottom:5px;display: inline-block;">
            ДОХОДЫ за
            <div style="display: inline-block; width: 200px;">
                <div class="dropdown">
                    <?= Html::tag('div', $taxRobot->period->label, [
                        'class' => 'dropdown-toggle',
                        'data-toggle' => 'dropdown',
                        'style' => 'display: inline-block; border-bottom: 1px dashed #000; cursor: pointer;',
                    ])?>
                    <?= Dropdown::widget([
                        'id' => 'employee-rating-dropdown',
                        'encodeLabels' => false,
                        'items' => $taxRobot->getPeriodDropdownItems(),
                    ])?>
                </div>
            </div>
            <?php if (empty($statistic)): ?>
                <div class="hidden-lg" style="font-size:13px;margin-right:25px;font-weight: normal">
                    Чтобы рассчитать налог,<br/>
                    загрузите данные за <?= $taxRobot->period->label ?>.
                </div>
            <?php endif; ?>
        </div>

        <div style="width:650px;float:right">
            <div class="row">
                <div class="col-xs-4 pull-right">
                    <div class="button-pulsate" style="max-width:200px;">
                        <?= Html::button('Добавить ДОХОД', [
                            'class' => 'btn yellow w100proc no-padding toggle-add-income-modal',
                            'data' => [
                                'pulsate' => 1,
                                'toggle' => 'modal',
                                'target' => '#add-income-modal',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" style="">
    <div class="col-sm-12" style="font-size: 14px;">
        <div class="row pad-b-5 text-bold" style="">
            <div class="col-xs-6">
                Какие приходы не нужно учитывать в УСН
                <img class="odds-panel-trigger" src="/img/open-book.svg">
            </div>
            <div class="col-xs-2 text-right">Всего приходов</div>
            <div class="col-xs-3 text-right">Доходы для расчета налогов</div>
        </div>
        <div class="cursor-pointer pad-t-5 pad-b-5 bor-t-ddd" data-toggle="collapse" data-target="#tax-bank-details">
            <div class="row">
                <div class="col-xs-6">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 40px; font-size: 24px;">
                                <img src="/img/icons/bank1.png" style="width:27px;margin-top: -7px;">
                            </td>
                            <td class="uppercase text-bold">
                                Рублевые счета
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-xs-2 text-right" style="height: 34px; padding-top: 7px;">
                    <?= moneyFormat($bankTotal = array_sum(ArrayHelper::getColumn($bankStatistic, 'total_amount', false))); ?>
                </div>
                <div class="col-xs-3 text-right" style="height: 34px; padding-top: 7px;">
                    <?= moneyFormat($bankTaxable = array_sum(ArrayHelper::getColumn($bankStatistic, 'taxable_amount', false))); ?>
                </div>
                <div class="col-xs-1 text-right" style="height: 34px; padding-top: 7px;">
                    <i class="bank-collapse-chevron fa fa-chevron-left"></i>
                </div>
            </div>
        </div>
        <div id="tax-bank-details" class="collapsed in" data-chevron=".bank-collapse-chevron">
            <?php $i = 0; ?>
            <?php foreach ($bankData as $data) : ?>
                <?php if ($data['active']) : ?>
                    <div class="<?= $i++ > 0 ? 'pad-l-40' : ''; ?>">
                        <div class="bor-t-ddd"></div>
                    </div>
                    <div class="row pad-t-5 pad-b-5 banking-module-open-current"
                         style="cursor:pointer"
                         data-url="<?= Url::to(['/cash/banking/default/index', 'p' => Banking::routeEncode($bankingRoute), 'account_id' => $data['account_id']]) ?>">
                        <div class="col-xs-6 ">
                            <div class="pad-l-40">
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="width: 50px; vertical-align: middle;">
                                            <?= $data['logo'] ?>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <div>
                                                <?= $data['bank_name'] ?>
                                            </div>
                                            <div style="font-size: 10px;">
                                                <?= $data['flow_name'] ?>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-xs-2 text-right" style="height: 34px; padding-top: 7px;">
                            <?= moneyFormat($data['total_amount']) ?>
                        </div>
                        <div class="col-xs-3 text-right" style="height: 34px; padding-top: 7px;">
                            <?= moneyFormat($data['taxable_amount']) ?>
                        </div>
                    </div>
                <?php endif ?>
            <?php endforeach ?>
        </div>
        <div class="cursor-pointer pad-t-5 pad-b-5 bor-t-ddd" data-toggle="collapse" data-target="#tax-order-details">
            <div class="row" <?php if (!$orderStatistic && !$isTaxcomUser): ?> onclick="$('.ofd-module-open-link').click()" <?php endif; ?>>
                <div class="col-xs-6">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 40px; font-size: 24px;">
                                <i class="fa fa-money"></i>
                            </td>
                            <td class="uppercase text-bold">
                                Касса
                                <?php if ($isTaxcomUser): ?>
                                    <?= Html::a('<img width="30" height="45" src="/img/icons/taxcom.png"/> Загрузить из ОФД TaxCOM', [
                                        '/cash/ofd/taxcom/default/index',
                                        'p' => $p,
                                    ], [
                                        'class' => 'ofd-module-open-link',
                                        'style' => 'font-size: 13px;font-weight: 400;letter-spacing: normal; text-transform: none; margin-left:15px;',
                                        'data' => [
                                            'pjax' => '0',
                                        ],
                                    ]) ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-xs-2 text-right" style="height: 34px; padding-top: 7px;">
                    <?= moneyFormat($orderTotal = array_sum(ArrayHelper::getColumn($orderStatistic, 'total_amount', false))); ?>
                </div>
                <div class="col-xs-3 text-right" style="height: 34px; padding-top: 7px;">
                    <?= moneyFormat($orderTaxable = array_sum(ArrayHelper::getColumn($orderStatistic, 'taxable_amount', false))); ?>
                </div>
                <div class="col-xs-1 text-right" style="height: 34px; padding-top: 7px;">
                    <i class="order-collapse-chevron fa fa-chevron-left"></i>
                </div>
            </div>
        </div>
        <?php if ($orderStatistic) : ?>
            <div id="tax-order-details" class="collapse" data-chevron=".order-collapse-chevron">
                <?php $i = 0; ?>
                <?php foreach ($orderStatistic as $data) : ?>
                    <?php $isOfdCashbox = (!empty($cashboxRegNumbers[$data['flow_name']])) ?>
                    <div class="<?= $i++ > 0 ? 'pad-l-40' : ''; ?>">
                        <div class="bor-t-ddd"></div>
                    </div>
                    <div class="row pad-t-5 pad-b-5">
                        <div class="col-xs-6 ">
                            <div class="pad-l-40">
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="width: 50px; vertical-align: middle; font-size:24px">
                                            <?php if ($isOfdCashbox): ?>
                                                <img width="27" height="38" src="/img/icons/taxcom.png"/>
                                            <?php else: ?>
                                                <i class="fa fa-money"></i>
                                            <?php endif; ?>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <?php if ($isOfdCashbox): ?>
                                                <div>
                                                    ОФД TaxCOM
                                                </div>
                                                <div style="font-size: 10px;">
                                                    <?= $data['flow_name'] ?>
                                                </div>
                                            <?php else: ?>
                                                <div>
                                                    <?= $data['flow_name'] ?>
                                                </div>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-xs-2 text-right" style="height: 34px; padding-top: 7px;">
                            <?= moneyFormat($data['total_amount']) ?>
                        </div>
                        <div class="col-xs-3 text-right" style="height: 34px; padding-top: 7px;">
                            <?= moneyFormat($data['taxable_amount']) ?>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        <?php endif ?>
        <div class="pad-t-5 pad-b-5 bor-t-ddd bor-b-ddd">
            <div class="row text-bold">
                <div class="col-xs-6">
                    <table style="width: 100%;height:34px">
                        <tr>
                            <td style="width: 40px;">
                                <img src="/img/icons/equality-sign.png" style="width:27px">
                            </td>
                            <td class="uppercase text-bold">
                                Итого
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-xs-2 text-right" style="height: 34px; padding-top: 7px;">
                    <?= moneyFormat($bankTotal + $orderTotal) ?>
                </div>
                <div  id="tooltip_add_product_block" class="col-xs-3">
                    <div id="totalTaxSum" class="text-right" style="height: 34px; padding-top: 7px; position: relative" data-sum="<?=($bankTaxable + $orderTaxable)?>">
                        <?= moneyFormat($bankTaxable + $orderTaxable) ?>
                        <!--help-->
                        <a style="position: absolute;<?= $addProductPositionTooltip; ?>" class="tooltip-first-r" data-tooltip-content="#tooltip_help_add_product">&nbsp;</a>
                        <div class="not-clickable">
                            &nbsp;
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pad-t-5 pad-b-5" style="height:44px;">
        </div>
    </div>
</div>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption" style="padding-top: 10px;">
            Приход
        </div>
<?php /*
        <div class="dropdown pull-right" style="margin-top: 4px;" id="tooltip_help_logo_block">
            <?= Html::tag('div', '<i class="glyphicon glyphicon-plus-sign"></i> Добавить доход ' . Html::tag('i', '', [
                'class' => 'fa fa-chevron-down',
                'style' => 'font-size: 12px;',
            ]), [
                'class' => 'btn darkblue darkblue-invert',
                'style' => 'margin-top:0;margin-bottom:2px;height:33px',
                'data-toggle' => 'dropdown',
            ]) ?>
            <?= Dropdown::widget([
                'id' => 'employee-rating-dropdown',
                'encodeLabels' => false,
                'items' => [
                    [
                        'label' => 'Добавить по банку',
                        'url' => [
                            '/cash/bank/create',
                            'flow_type' => CashFlowsBase::FLOW_TYPE_INCOME,
                            'redirect' => Url::to(['/tax/robot/bank', 'period' => $urlPeriod]),
                            'skipDate' => 1,
                            'canAddContractor' => 1,
                            'canAddAccount' => 1
                        ],
                        'linkOptions' => [
                            'style' => 'padding: 8px 12px;',
                            'class' => 'update-movement-link update-bank-movement-link',
                            'data' => [
                                'pjax' => '0',
                            ],
                        ]
                    ],
                    [
                        'label' => 'Добавить по кассе',
                        'url' => [
                            '/cash/order/create',
                            'flow_type' => CashFlowsBase::FLOW_TYPE_INCOME,
                            'redirect' => Url::to(['/tax/robot/bank', 'period' => $urlPeriod]),
                            'skipDate' => 1,
                            'id' => $cashbox->id,
                            'canAddContractor' => 1
                        ],
                        'visible' => (boolean) $cashbox,
                        'linkOptions' => [
                            'style' => 'padding: 8px 12px;',
                            'class' => 'update-movement-link update-order-movement-link',
                            'data' => [
                                'pjax' => '0',
                            ],
                        ]
                    ],
                ],
                'options' => [
                    'style' => 'width: 100%; margin-top: 2px;'
                ]
            ])?>
            <!--help-->
            <a style="position: absolute;<?= $logoPositionTooltip; ?>" class="tooltip-first-r" data-tooltip-content="#tooltip_help_logo">&nbsp;</a>
            <div class="not-clickable">
                &nbsp;
            </div>
        </div>
*/ ?>
        <div class="actions joint-operations col-md-3 col-sm-3"
             style="display:none; width: 250px;">
            <?php if ($canDelete) : ?>
                <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                    'class' => 'btn btn-default btn-sm',
                    'data-toggle' => 'modal',
                ]); ?>
                <?= $this->render('parts_bank/modal-many-delete'); ?>
            <?php endif ?>
            <?php if ($canUpdate) : ?>
                <?= Html::a('<i class="fa fa-calculator" style="padding-right: 3px;"></i>Учитывать?', '#', [
                    'data-tooltip-content' => "#taxable-group",
                    'class' => 'btn btn-default btn-sm tooltip-group-taxable',
                    'onclick' => new \yii\web\JsExpression('return false')
                ]) ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?= common\components\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $taxRobot,
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable documents_table fix-thead',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],
                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper bank-scroll-table',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'rowOptions' => function ($data, $key, $index, $grid) {
                    return [
                        'class' => 'flow-row',
                        'data' => [
                            'update-url' => Url::to(['update-movement', 'type' => $data['payment_type'], 'id' => $data['id']]),
                        ]
                    ];
                },
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'columns' => [
                    [
                        'header' => Html::checkbox('', false, [
                            'class' => 'joint-operation-main-checkbox',
                        ]),
                        'headerOptions' => [
                            'class' => 'text-center',
                            'width' => '5%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-center no-update-modal',
                        ],
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Html::checkbox('flowId[]', false, [
                                'class' => 'joint-operation-checkbox no-update-modal',
                                'value' => $data['payment_type'] . '_' . $data['id'],
                                'data' => [
                                    'income' => bcdiv($data['amount'], 100, 2),
                                    'expense' => 0,
                                ],
                            ]);
                        },
                    ],
                    [
                        'attribute' => 'date',
                        'label' => 'Дата',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
                    ],
                    [
                        'attribute' => 'amount',
                        'label' => 'Приход',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-right',
                        ],
                        'value' => function ($data) {
                            return moneyFormat($data['amount']);
                        }
                    ],
                    [
                        'attribute' => 'payment_type',
                        'label' => 'Тип оплаты',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'format' => 'raw',
                        'filter' => ['' => 'Все'] + TaxRobotHelper::$typeList,
                        'value' => function ($data) {
                            return TaxRobotHelper::typeName($data['payment_type']) .
                                    TaxRobotHelper::icon($data['payment_type'], [
                                        'class' => 'pad-l-10',
                                        'style' => 'color: #5b9bd1;',
                                    ]);
                        }
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'contractor_id',
                        'label' => 'Контрагент',
                        'headerOptions' => [
                            'width' => '12%',
                            'class' => 'nowrap-normal max10list',
                        ],
                        'format' => 'raw',
                        'filter' => ['' => 'Все'] + $contractorList,
                        'value' => function ($data) use ($contractorList) {
                            return ArrayHelper::getValue($contractorList, $data['contractor_id'], '');
                        }
                    ],
                    [
                        'attribute' => 'description',
                        'label' => 'Назначение',
                        'headerOptions' => [
                            'width' => '30%',
                        ],
                    ],
                    [
                        'class' => DropDownDataColumn::className(),
                        'attribute' => 'is_taxable',
                        'label' => 'Учитывать в налогах',
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                        'contentOptions' => [
                            'class' => 'no-update-modal',
                        ],
                        'filter' => ['' => 'Все', 1 => 'Да', 0 => 'Нет'],
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Html::tag('span', $data['is_taxable'] ? 'Да' : 'Нет') . "\n" . BoolleanSwitchWidget::widget([
                                'id' => 'change-taxable-' . $data['payment_type'] . '_' . $data['id'],
                                'action' => ['taxable', 'type' => $data['payment_type'], 'id' => $data['id']],
                                'inputName' => 'is_taxable',
                                'inputValue' => $data['is_taxable'],
                                'label' => 'Учитывать?',
                            ]);
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<!-- buttons -->
<div id="buttons-bar-fixed<?=($showHelp)?'-disable' : '' ?>" style="<?=($showHelp)?'margin-bottom: 100px':'' ?>">
    <div class="col-xs-12 pad0 buttons-block text-center">
            <?= Html::a('Назад', ['params'], [
                'class' => 'btn darkblue pull-left hidden-xs',
                'style' => 'min-width: 180px;',
                'data' => ['pjax' => '0']
            ]) ?>
            <?= Html::a('<i class="fa fa-arrow-left fa-2x"></i>', ['params'], [
                'class' => 'btn darkblue pull-left hidden-lg hidden-md hidden-sm',
                'title' => 'Назад',
                'data' => ['pjax' => '0']
            ]) ?>


            <?php if (!$taxRobot->getHasIncomeInThisYear()) : ?>
                <div class="button-pulsate text-center" style="display:inline-block">
                    <?= Html::a('Нулевая налоговая декларация', ['declaration',
                            'empty' => 1,
                            'period' => ($taxRobot->period) ? $taxRobot->period->id : ''
                        ], [
                        'class' => 'btn darkblue darkblue-invert get-null-declaration',
                        'data' => [
                            'pjax' => '0',
                            'pulsate' => (int)$isPulsateNullDeclaration,
                            'pulsate-count' => 9
                        ],
                    ]) ?>
                </div>
            <?php endif; ?>

            <div class="pull-right" id="tooltip_help_save_block">
                <?= Html::a('Далее', ['calculation', 'period' => $urlPeriod], [
                    'id' => 'taxrobot-next-step-btn',
                    'class' => 'btn darkblue hidden-xs' . ($taxRobot->isPaid ? '' : ' taxrobot-pay-panel-trigger'),
                    'style' => 'min-width: 180px;',
                    'data' => ['pjax' => '0']
                ]); ?>
                <?= Html::a('<i class="fa fa-arrow-right fa-2x"></i>', ['calculation', 'period' => $urlPeriod], [
                    'class' => 'btn darkblue hidden-lg hidden-md hidden-sm' . ($taxRobot->isPaid ? '' : ' taxrobot-pay-panel-trigger'),
                    'title' => 'Далее',
                    'data' => ['pjax' => '0']
                ]); ?>
                <!--help-->
                <a style="position: absolute; <?= $savePositionTooltip; ?>" class="tooltip-first-r" data-tooltip-content="#tooltip_help_save">&nbsp;</a>
                <div class="not-clickable">
                    &nbsp;
                </div>
            </div>
    </div>
    <?= Html::hiddenInput('isPaid', (int)$taxRobot->isPaid, ['id' => 'isPaid']) ?>
</div>

<style>#summary-container table td:nth-child(4){display:none}</style>
<?= SummarySelectWidget::widget([
    'buttons' => [
        $canUpdate ? Html::tag('span', '<i class="fa fa-calculator" style="padding-right: 3px;"></i>Учитывать?', [
            'data-tooltip-content' => "#taxable-group",
            'class' => 'btn btn-sm darkblue text-white tooltip-group-taxable',
        ]): null,
        $canDelete ? Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
            'class' => 'btn btn-sm darkblue text-white',
            'data-toggle' => 'modal',
        ]) : null,
    ],
]); ?>

<?= $this->render('_select_tariff_modal', [
    'taxRobot' => $taxRobot,
    'groupIds' => [
        SubscribeTariffGroup::TAX_IP_USN_6,
        SubscribeTariffGroup::TAX_DECLAR_IP_USN_6,
    ],
]); ?>

<?php \yii\widgets\Pjax::end(); ?>

<?= AddIncomeWidget::widget([
    'company' => $company,
    'accountArray' => $accountArray,
    'cashbox' => $cashbox,
    'taxRobot' => $taxRobot,
]) ?>

<?php Modal::begin([
    'id' => 'update-movement-modal',
    'header' => Html::tag('h1', ''),
]); ?>

    <?php \yii\widgets\Pjax::begin([
        'id' => 'update-movement-pjax',
        'enablePushState' => false,
        'linkSelector' => '.update-movement-link',
    ]); ?>

    <?php \yii\widgets\Pjax::end(); ?>

<?php Modal::end(); ?>

<div class="tooltip_templates" style="display: none;">
    <div id="taxable-group">
        <div class="btn-group">
            <span class="btn" data-taxable="1">Да</span>
            <span class="btn" data-taxable="0">Нет</span>
        </div>
    </div>
</div>

<div class="modal fade t-p-f modal_scroll_center" id="add-new" tabindex="-1" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body" id="block-modal-new-product-form">

            </div>
        </div>
    </div>
</div>

<?= $this->render('parts_bank/modal-usn-payment-rules'); ?>
<?= $this->render('parts_bank/modal-kub-worth', ['company' => $company]); ?>

<?php if (Yii::$app->session->get('taxrobot_show_pay_popup')): ?>
    <?php $this->registerJs('
        $(document).ready(function(){
            $(".taxrobot-pay-panel-trigger").click();
        });
    ');
    ?>
    <?php Yii::$app->session->set('taxrobot_show_pay_popup', false); ?>
<?php endif; ?>

<?php
$newCheckingAccountant = new CheckingAccountant([
    'company_id' => $company->id,
    'type' => ($company->mainCheckingAccountant) ? CheckingAccountant::TYPE_ADDITIONAL : CheckingAccountant::TYPE_MAIN
]);
echo $this->render('parts_bank/modal_rs/_modal_form', [
    'checkingAccountant' => $newCheckingAccountant,
    'title' => 'Добавить расчетный счет',
    'id' => 'add-company-rs',
    'company' => $company,
]);
?>

<?= $this->render('manual_entry', [
    'model' => $manualEntryModel,
]) ?>

<?= $showReadyModal ? $this->render('bank_ready', [
    'taxRobot' => $taxRobot,
]) : null; ?>

<?= $this->render('bank_wait', [
    'taxRobot' => $taxRobot,
]); ?>

<?php if (!$showReadyModal && Yii::$app->session->getFlash('error')): ?>
    <?php $this->registerJs('
        $(document).ready(function(){
            $(".taxrobot-pay-panel-trigger").click();
        });
    ');
    ?>
<?php endif; ?>

<?php if (!$taxRobot->isPaid) {
    $this->registerJs('$(".sbs-step-4").addClass("taxrobot-pay-panel-trigger").removeAttr("href");');
} ?>

<?php
// First show all bankings upload statements modals in rotation
if (!empty($preloadBankingAccounts)) {
    $bankingAccount = $preloadBankingAccounts[0];
    $this->registerJs('
        var TaxrobotBankingPreload = {
            accountId: ' . (int)$bankingAccount['id'] . ',
            bankingAlias: "'. $bankingAccount['bank_alias'] . '",
            p: "' . Banking::routeEncode($bankingRoute) . '",
            urlUpdateSession: "/tax/robot/clear-preload-banking-accounts",
            showAccountFromSession: function() {
                $.post(this.urlUpdateSession, {account_id: this.accountId}, function (data) {

                    TaxrobotBankingPreload.showModal();

                    TaxrobotBankingPreload.accountId = data.account_id;
                    TaxrobotBankingPreload.bankingAlias = data.bank_alias;

                });
            },
            showModal: function() {

                if (!this.accountId)
                    return false;

                var url = "/cash/banking/" + this.bankingAlias + "/default/index?account_id=" + this.accountId + "&p=" + this.p;
                $.pjax({url: url, container: "#banking-module-pjax", push: $("#banking-module-modal").data("push-state")});
                $("#banking-module-modal").modal("show");
            }
        };
        $(document).ready(function(){
            TaxrobotBankingPreload.showAccountFromSession();
        });
        $(document).on("hidden.bs.modal", "#banking-module-modal", function() {
            TaxrobotBankingPreload.showAccountFromSession();
        });
    ');
} ?>

<?php $this->registerJs('
$(document).on("click", ".tooltip-help-load-statement", function(e) {
    if ($(this).attr("data-url")) {
        e.preventDefault();
        $.pjax({url: $(this).attr("data-url"), container: "#banking-module-pjax", push: $("#banking-module-modal").data("push-state")});
        $("#banking-module-modal").modal("show");
    }
});
$(document).on("click", ".banking-module-open-current", function(e) {
    e.preventDefault();
    $.pjax({url: $(this).attr("data-url"), container: "#banking-module-pjax", push: $("#banking-module-modal").data("push-state")});
    $("#banking-module-modal").modal("show");
});

$(document).ready(function(){
    $(document).on("click", ".odds-panel-trigger", function(){
        $(".odds-panel").toggle("fast");
        $(this).toggleClass("active");
        return false;
    });
    $(document).on("click", ".odds-panel .side-panel-close", function () {
        $(".odds-panel").toggle("fast");
        $(".odds-panel-trigger").toggleClass("active");
    });

    $(".button-pulsate").find("[data-pulsate]").each(function(i,v) {
        if ($(v).data("pulsate"))
            $(this).parents(".button-pulsate").pulsate({
            color: "#bf1c56",
            reach: 20,
            repeat: $(v).data("pulsate-count") || 3
        });
    });

});

$(document).on("click", ".sbs-step-4", function(e) {
    if (parseInt($("#totalTaxSum").attr("data-sum")) <= 0) {
        taxrobotPayPanelOpen();
        return false;
    }
});
$(document).on("click", ".sbs-step-5", function(e) {
    if (parseInt($("#totalTaxSum").attr("data-sum")) <= 0) {
        taxrobotPayPanelOpen();
        return false;
    }
});
$(document).on("click", ".sbs-step-6", function(e) {
    if (parseInt($("#totalTaxSum").attr("data-sum")) <= 0 || !$("#isPaid").val()) {
        taxrobotPayPanelOpen();
        return false;
    }
});
$(document).on("click", ".uppercase .ofd-module-open-link", function(e) {
    e.stopPropagation();
});
$(document).on("change", "#cashbankflowsform-rs", function (e) {
    var value = $(this).val() || $(this).text();
    if (value == "add-checking-accountant") {
        e.preventDefault();
        $("#cashbankflowsform-rs").val("").trigger("change");
        $(".modal.new-company-rs").modal();
    }
});

$(document).on("submit", "form.form-checking-accountant", function () {
    $this = $(this);
    $.post($this.attr("action"), $(this).serialize(), function (data) {
        if (data.result == true) {
            var select = $("#cashbankflowsform-rs");
            select.find("option").remove();
            for (var key in data.options) {
                select.append($("<option></option>").attr("value", key).text(data.options[key]));
            }
            $companyRs = data.companyRs;
            if (data.val) {
                select.val(data.val);
            }
            $("#add-company-rs").find("form")[0].reset();
            $this.closest(".modal").modal("hide");
            $(".field-cashbankflowsform-rs").removeClass("has-error").find(".help-block").html("");
            Ladda.stopAll();

        } else {
            $this.html($(data.html).find("form").html());
        }
    });

    return false;
});

'); ?>

<?php if ($showHelp) {
    echo $this->render('parts_bank/first_show_help.php', [
        'company' => $company,
        'loadStatementUrl' => $tooltipBankLoadStatementUrl
    ]);
} ?>
