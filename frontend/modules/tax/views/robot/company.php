<?php

use backend\models\Bank;
use common\components\widgets\BikTypeahead;
use common\components\widgets\AddressTypeahead;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\dictionary\address\AddressDictionary;
use frontend\models\Documents;
use frontend\modules\tax\widgets\TaxrobotStartModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\company\CompanyType;
use common\components\date\DateHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model Company */
/* @var $account CheckingAccountant */
/* @var $isFirstVisit buulean */

$this->title = 'Заполните реквизиты';

if ($isFirstVisit) {
    $this->params['hideCarrot'] = true;
}

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-click',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);
// helpers tooltip
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-first-r',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-light'],
        'contentAsHTML' => true,
        'position' => 'left',
    ],
]);
$company = $model;

$this->registerJsFile( '@web/scripts/jquery.autocomplete.js', ['depends' => 'yii\web\JqueryAsset'] );

$buttonFindIfns = '<span class="glyphicon glyphicon-search"></span> Найти код ИФНС и ОКТМО';
$buttonCheckIfns = '<img src="/img/icons/warning1.png"/> Проверить код ИФНС и ОКТМО';

$requisitesPositionTooltip = 'top: -45px;right: -45px';
$addProductPositionTooltip = 'top: -45px;right: 45px';
$logoPositionTooltip = 'top: 20px;right: -45px';
$savePositionTooltip = 'top: -85px;left: 225px';
$showHelp = !$company->inn || Yii::$app->request->get('show_help');
$canStartModal = TaxrobotStartModalWidget::can($model);
?>

<?= $this->render('_style') ?>
<style>
    .show-first-account-number{<?=(empty($additionalAccounts)) ? 'display:none':''?>}
    .form-group.form-md-line-input .help-block {
        white-space: nowrap;
    }
</style>

<?= TaxrobotStartModalWidget::widget([
    'company' => $company,
    'isFirstVisit' => $isFirstVisit,
]) ?>

<div class="row">
    <div class="hidden-xs hidden-sm hidden-md col-xs-12 step-by-step">
        <?= $this->render('_steps', ['step' => 1]) ?>
        <div class="pad_sm_t" style="padding-top: 10px">
            <div class="col-xs-12 col-lg-12 pad0">
                <?php $form = ActiveForm::begin([
                    'id' => 'company-update-form',
                    'enableClientValidation' => false,
                    'enableAjaxValidation' => true,
                    'fieldClass' => 'common\components\floatLabelField\FloatLabelActiveField',
                    'fieldConfig' => [
                        'template' => "{input}\n{label}\n{error}\n",
                    ],
                    'options' => [
                        'class' => 'form-md-underline'
                    ],
                ]); ?>

                <?= Html::activeHiddenInput($model, 'company_type_id'); ?>
                <?= Html::activeHiddenInput($model, 'okato'); ?>
                <?php /*<?= Html::activeHiddenInput($model, 'oktmo'); ?>*/ ?>
                <?php /*<?= Html::activeHiddenInput($model, 'ifns_ga'); ?>*/ ?>
                <?php /*<?= Html::activeHiddenInput($model, 'address_actual'); ?>*/ ?>
                <?= Html::hiddenInput('not_use_account', 0) ?>

                <!-- step 1 -->
                <div class="col-xs-12 pad0 step-by-step-form">
                    <div class="col-xs-12 pad0 bord-light-b">
                        <span class="marg pad10-b font-bold" style="font-size:16px">Шаг 1: Заполните данные по вашему ИП</span>

                        <?= $canStartModal ? Html::button('Автоматическое заполнение', [
                            'class' => 'btn darkblue pull-right',
                            'data-toggle' => 'modal',
                            'data-target' => '#taxrobot-start-modal',
                        ]) : ''; ?>
                    </div>

                    <div class="col-xs-12 pad0" style="margin-top: 25px;margin-bottom: 10px;">
                        <span class="marg pad10-b font-bold" style="font-size:16px">Реквизиты ИП</span>
                    </div>

                    <div class="row">

                        <div class="col-xs-6 mb5">
                            <div class="row">
                                <div class="col-xs-12 col-md-6" id="tooltip_requisites_block">
                                    <?= $form->field($model, 'inn')->textInput([
                                        'maxlength' => true,
                                    ])->label('<span class="is-empty">Введите ваш </span>ИНН') ?>
                                    <!--help-->
                                    <a style="position: absolute;<?= $requisitesPositionTooltip; ?>" class="tooltip-first-r" data-tooltip-content="#tooltip_requisites">&nbsp;</a>
                                    <div class="not-clickable">
                                        &nbsp;
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <?= $form->field($model, 'egrip')->textInput([
                                        'maxlength' => true,
                                    ]) ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6 mb5">
                            <div class="row">
                                <div class="col-xs-12 col-md-6" style="position:relative">
                                    <?= $form->field($model, 'taxRegistrationDate')->textInput([
                                        'maxlength' => true,
                                        'class' => 'form-control input-sm date-picker' . ($model->taxRegistrationDate ? ' edited' : ''),
                                        'autocomplete' => 'off',
                                        'value' => $model->taxRegistrationDate,
                                        'disabled' => !empty($model->taxRegistrationDate),
                                    ])->label('Дата регистрации ИП') ?>
                                    <?= Html::tag('i', '', [
                                        'id' => 'show-taxregistrationdate',
                                        'class' => 'fa fa-calendar',
                                        'style' => 'position: absolute; top: 30px; right: 15px; color: #cecece; cursor: pointer;',
                                    ]) ?>
                                </div>
                                <div class="col-xs-12 col-md-6">

                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-xs-6 mb5">
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <?= $form->field($model, 'ip_lastname')->label('Фамилия')->textInput(); ?>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <?= $form->field($model, 'ip_firstname')->label('Имя')->textInput(); ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6 mb5">
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <?= $form->field($model, 'ip_patronymic')->label('Отчество')->textInput(); ?>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <?= $form->field($model, 'has_chief_patronymic', [
                                        'template' => "{label}\n{input}",
                                        'options' => [
                                            'class' => 'form-group form-md-line-input form-md-floating-label',
                                            'style' => 'display: inline-block;',
                                        ],
                                        'labelOptions' => [
                                            'class' => 'control-label',
                                            'style' => 'padding: 6px 0; margin-right: 9px;',
                                        ]
                                    ])->checkbox([], true) ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6 mb5">
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <?= $form->field($model, 'address_legal')->label('Адрес по прописке')->textInput(); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 mb5">
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <?= $form->field($model, 'okved')->label('ОКВЭД')->textInput(); ?>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-9 mb5" id="tooltip_add_product_block">
                            <div class="row">
                                <div class="col-xs-12 col-md-4">
                                    <?= $form->field($model, 'ifns_ga')->label('Код ИФНС')->textInput(); ?>
                                    <span class="tooltip2-click valign-middle ico-question sbs-tooltip" data-tooltip-content="#tooltip_what_is_ifns" style="right:-7px; top:23px;"></span>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <?= $form->field($model, 'oktmo')->label('Код ОКТМО')->textInput(); ?>
                                    <span class="tooltip2-click valign-middle ico-question sbs-tooltip" data-tooltip-content="#tooltip_what_is_oktmo" style="right:-7px; top:23px;"></span>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <span>
                                        <?= Html::button(($model->ifns_ga && $model->oktmo) ? $buttonFindIfns : $buttonCheckIfns, [
                                            'class' => 'btn darkblue darkblue-invert' . (($model->ifns_ga && $model->oktmo) ? '' : ' error'),
                                            'id' => 'get-fias-ittem',
                                        ]); ?>
                                    </span>
                                </div>
                            </div>
                            <!--help-->
                            <a style="position: absolute; <?= $addProductPositionTooltip; ?>" class="tooltip-first-r" data-tooltip-content="#tooltip_help_add_product">&nbsp;</a>
                            <div class="not-clickable">
                                &nbsp;
                            </div>
                        </div>
                        <div class="col-xs-3 mb5">
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12" style="margin-top: 15px;margin-bottom: 10px;">
                            <span class="marg pad10-b font-bold" style="font-size:16px"> Расчетный счет<span class="show-first-account-number"> №1</span></span>
                        </div>

                        <div class="col-xs-6 mb5" id="tooltip_help_logo_block">
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <?= $form->field($account, 'bik')->textInput([
                                        'maxlength' => true,
                                        'class' => 'form-control input-sm dictionary-bik' . ($account->bik ? ' edited' : ''),
                                        'data' => [
                                            'url' => Url::to(['/dictionary/bik']),
                                            'target-name' => '#' . Html::getInputId($account, 'bank_name'),
                                            'target-city' => '#' . Html::getInputId($account, 'bank_city'),
                                            'target-ks' => '#' . Html::getInputId($account, 'ks'),
                                            'target-rs' => '#' . Html::getInputId($account, 'rs'),
                                            'target-collapse' => '#company-bank-block',
                                        ]
                                    ])->label('БИК вашего банка') ?>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <?= $form->field($account, 'rs')->textInput(['class' => 'form-control input-sm rs-input']); ?>
                                </div>
                            </div>
                            <!--help-->
                            <a style="position: absolute; <?= $logoPositionTooltip; ?>" class="tooltip-first-r" data-tooltip-content="#tooltip_help_logo">&nbsp;</a>
                            <div class="not-clickable">
                                &nbsp;
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-xs-6 mb5">
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <?= $form->field($account, 'bank_name')->textInput([
                                        'maxlength' => true,
                                        'readonly' => true,
                                    ]); ?>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <?= $form->field($account, 'ks')->textInput([
                                        'maxlength' => true,
                                        'readonly' => true,
                                    ]); ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6 mb5">
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <?= $form->field($account, 'bank_city')->textInput([
                                        'maxlength' => true,
                                        'readonly' => true,
                                    ]); ?>
                                    <div style="position:absolute;right:-20px;bottom:5px;">
                                        <span style="margin: 0;" class="tooltip2-click ico-question" data-tooltip-content="#tooltip_city"></span>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    &nbsp;
                                </div>
                            </div>
                        </div>

                        <?php $i = 1; ?>
                        <?php foreach ($additionalAccounts as $additionalAccount) : ?>
                            <?php $i++; ?>

                            <div class="company-account" data-id="<?= $additionalAccount->id ?>">
                                <div class="col-xs-12" style="margin-top: 15px;margin-bottom: 10px;">
                                    <span class="marg pad10-b font-bold" style="font-size:16px">
                                        Расчетный счет №<span><?=$i?></span>
                                    </span>
                                    <?= \frontend\widgets\BtnConfirmModalWidget::widget([
                                        'toggleButton' => [
                                            'label' => '',
                                            'class' => 'close',
                                            'tag' => 'a',
                                            'style' => 'float:none; margin-left: 10px;'
                                        ],
                                        'modelId' => $additionalAccount->id,
                                        'message' => 'Вы уверены, что хотите удалить расчетный счет?',
                                    ]); ?>
                                </div>
                                <div class="col-xs-6 mb5">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <?= $form->field($additionalAccount, 'bik')->textInput([
                                                'maxlength' => true,
                                                'class' => 'form-control input-sm dictionary-bik' . ($additionalAccount->bik ? ' edited' : ''),
                                                'data' => [
                                                    'url' => Url::to(['/dictionary/bik']),
                                                    'target-name' => '#' . Html::getInputId($additionalAccount, 'bank_name'),
                                                    'target-city' => '#' . Html::getInputId($additionalAccount, 'bank_city'),
                                                    'target-ks' => '#' . Html::getInputId($additionalAccount, 'ks'),
                                                    'target-rs' => '#' . Html::getInputId($additionalAccount, 'rs'),
                                                    'target-collapse' => '#company-bank-block',
                                                ]
                                            ])->label('БИК вашего банка') ?>

                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <?= $form->field($additionalAccount, 'rs')->textInput(['class' => 'form-control input-sm rs-input']); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-xs-6 mb5">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <?= $form->field($additionalAccount, 'bank_name')->textInput([
                                                'maxlength' => true,
                                                'readonly' => true,
                                            ]); ?>
                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <?= $form->field($additionalAccount, 'ks')->textInput([
                                                'maxlength' => true,
                                                'readonly' => true,
                                            ]); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 mb5">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <?= $form->field($additionalAccount, 'bank_city')->textInput([
                                                'maxlength' => true,
                                                'readonly' => true,
                                            ]); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                        <?php endforeach; ?>

                        <?php foreach ($newAccounts as $newAccount) : ?>
                            <?php $i++; ?>

                            <div class="new-account company-account" data-id="<?= "new-{$i}" ?>" style="display: none">
                                <div class="col-xs-12" style="margin-top: 15px;margin-bottom: 10px;">
                                    <span class="marg pad10-b font-bold" style="font-size:16px">
                                        Расчетный счет №<span><?=$i?></span>
                                    </span>
                                    <?= \frontend\widgets\BtnConfirmModalWidget::widget([
                                        'toggleButton' => [
                                            'label' => '',
                                            'class' => 'close',
                                            'tag' => 'a',
                                            'style' => 'float:none; margin-left: 10px;'
                                        ],
                                        'modelId' => "new-{$i}",
                                        'message' => 'Вы уверены, что хотите удалить расчетный счет?',
                                    ]); ?>
                                </div>
                                <div class="col-xs-6 mb5">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <?= $form->field($newAccount, 'bik')->textInput([
                                                'maxlength' => true,
                                                'class' => 'form-control input-sm dictionary-bik' . ($newAccount->bik ? ' edited' : ''),
                                                'data' => [
                                                    'url' => Url::to(['/dictionary/bik']),
                                                    'target-name' => '#' . Html::getInputId($newAccount, 'bank_name'),
                                                    'target-city' => '#' . Html::getInputId($newAccount, 'bank_city'),
                                                    'target-ks' => '#' . Html::getInputId($newAccount, 'ks'),
                                                    'target-rs' => '#' . Html::getInputId($newAccount, 'rs'),
                                                    'target-collapse' => '#company-bank-block',
                                                ]
                                            ])->label('БИК вашего банка') ?>

                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <?= $form->field($newAccount, 'rs')->textInput(['class' => 'form-control input-sm rs-input']); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-xs-6 mb5">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <?= $form->field($newAccount, 'bank_name')->textInput([
                                                'maxlength' => true,
                                                'readonly' => true,
                                            ]); ?>
                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <?= $form->field($newAccount, 'ks')->textInput([
                                                'maxlength' => true,
                                                'readonly' => true,
                                            ]); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 mb5">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <?= $form->field($newAccount, 'bank_city')->textInput([
                                                'maxlength' => true,
                                                'readonly' => true,
                                            ]); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                        <?php endforeach; ?>
                        <div class="col-xs-12 mb5">

                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="col-md-6 pad0">
                                        <?= Html::button('<span class="glyphicon glyphicon-plus-sign"></span> Добавить расчетный счет', [
                                            'class' => 'btn darkblue darkblue-invert',
                                            'id' => 'add-new-account',
                                        ]); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?php if ($account !== $model->mainCheckingAccountant): ?>
                                            <?= Html::button(' У меня нет расчетного счета', [
                                                'id' => 'not_use_account',
                                                'class' => 'btn darkblue darkblue-invert error tooltip2-hover',
                                                'data-tooltip-content' => '#tooltip_not_use_account',
                                                'style' => 'margin-top:15px;display:none'
                                            ]); ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    &nbsp;
                                </div>
                            </div>
                        </div>
                        <div class="clearfix" style="padding-bottom:30px"></div>

                        <div id="buttons-bar-fixed<?=($showHelp)?'-disable' : '' ?>">
                            <div class="col-xs-12 pad0 buttons-block" id="tooltip_help_save_block" style="width:180px">
                                <div>
                                    <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                                        'class' => 'btn darkblue btn-save hidden-xs mt-ladda-btn ladda-button',
                                        'data-style' => 'expand-right',
                                        'style' => 'width: 130px!important; color: #fff;',
                                    ]); ?>
                                    <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                                        'class' => 'btn darkblue btn-save hidden-lg hidden-md hidden-sm',
                                        'title' => 'Сохранить',
                                    ]); ?>
                                    <!--help-->
                                    <a style="position: absolute; <?= $savePositionTooltip; ?>" class="tooltip-first-r" data-tooltip-content="#tooltip_help_save">&nbsp;</a>
                                    <div class="not-clickable">
                                        &nbsp;
                                    </div>

                                </div>
                            </div>
                            <button class="taxrobot-company-panel-trigger" style="display:none!important"></button>
                            <?= $canStartModal ? Html::button('Автоматическое заполнение', [
                                'class' => 'btn darkblue pull-right',
                                'data-toggle' => 'modal',
                                'data-target' => '#taxrobot-start-modal',
                            ]) : ''; ?>
                        </div>
                    </div>
                </div>
                <div style="clear:both"></div>

                <?php $form->end(); ?>
            </div>
        </div>
    </div>
</div>
<div class="tooltip_templates" style="display: none;">
    <span id="tooltip_city" style="display: inline-block; text-align: center;">
        Необходимо для Платежного поручения.
    </span>
    <span id="tooltip_what_is_oktmo" style="display: inline-block; text-align: center;">
        <p> <strong> Зачем нужен код ОКТМО? </strong></p>
        <p> ОКТМО — это общероссийский классификатор территорий муниципальных образований. <br/> Коды ОКТМО необходимо указывать в платежных поручениях на уплату налогов и сборов, а так же в налоговых декларациях."</p>
    </span>
    <span id="tooltip_what_is_ifns" style="display: inline-block; text-align: center;">
        <p> <strong> Зачем нужен номер ИФНС? </strong></p>
        <p> Номер ИФНС - это номер Инспекционной Федеральной Налоговой Службы в которой вы регистрировали свое ИП. <br/> Данный номер необходим для заполнения реквизитов налоговой в платежке на уплату налогов.</p>
    </span>
    <span id="tooltip_not_use_account" style="display: inline-block; text-align: center;">
        <p>Расчетный счет нужен для учета дохода.<br/>
           Если у вас нет расч/счета, то возможно вам нужна НУЛЕВАЯ налоговая декларация.<br/>
           Нажмите эту кнопку и далее действуйте по шагам</p>
    </span>
</div>

<?php Modal::begin([
    'id' => 'fias-result-modal',
    'header' => '<h3>Определение кода ИФНС и ОКТМО<br/>по адресу прописки</h3>',
    'toggleButton' => false,
]); ?>

    <div class="fias-result fias-result-success form-horizontal">
        <div class="form-group">
            <label class="col-md-3 control-label bold-text" style="padding-right: 0;">Адрес по прописке:</label>
            <div class="col-md-9 inp_one_line-product">
                <input class="form-control fias-query-value" id="fias-address" type="text">
                <span class="fias-query-help" style="color:red;font-style:italic;font-size:12px">Начните вводить адрес и выберите нужный из предложенного списка</span>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label bold-text">ИФНС:</label>
            <div class="col-md-9 inp_one_line-product">
                <input id="fias-result-ifns" class="form-control fias-result-value" disabled>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label bold-text">ОКТМО:</label>
            <div class="col-md-9 inp_one_line-product">
                <input id="fias-result-oktmo" class="form-control fias-result-value" disabled>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label bold-text">Индекс:</label>
            <div class="col-md-9 inp_one_line-product">
                <input id="fias-result-index" class="form-control fias-result-value" disabled>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                Код ИФНС, ОКТМО и индекс определяются по адресу автоматически.
            </div>
        </div>

        <div style="text-align: center;margin-top:15px">
            <?= Html::button('Сохранить', [
                'id' => 'copy-fias-result',
                'class' => 'btn darkblue',
                'style' => 'color: #fff;',
            ]); ?>

        </div>
    </div>

<?php Modal::end(); ?>


<?= $this->render('//company/form/_company_inn_api') ?>


<?= $this->render('_company_modal') ?>

<?php
$this->registerJs(<<<JS
    $('.dictionary-bik').devbridgeAutocomplete({
        serviceUrl: function() {
            return $(this).data('url');
        },
        minLength: 1,
        paramName: 'q',
        dataType: 'json',
        showNoSuggestionNotice: true,
        noSuggestionNotice: 'БИК не найден. Возможно ваш банк изменил БИК. Проверьте на сайте банка.',
        beforeRender: function (container, suggestions) {
            if (suggestions.length > 1) {
                container.prepend('<div class="text-truncate pl-2">Выберите вариант или продолжите ввод</div>');
            }
        },
        transformResult: function (response) {
            return {
                suggestions: $.map(response, function (item, key) {
                    item.value = item.name;
                    return item;
                }),
            };
        },
        onSelect: function (suggestion) {
            $(this).val(suggestion.bik);
            $($(this).data('target-name')).val(suggestion.name);
            $($(this).data('target-city')).val(suggestion.city);
            $($(this).data('target-ks')).val(suggestion.ks);
            if (!$($(this).data('target-rs')).val())
                $($(this).data('target-rs')).val('40802810').focus();
            $($(this).data('target-collapse')).collapse('show');
            $('#add-new-account').prop('disabled', false);
            if (suggestion.city && $(this).data('target-city')) {
                var city = suggestion.city[0].toUpperCase() + suggestion.city.slice(1).toLowerCase();
                $($(this).data('target-city')).addClass('edited').val(city);
            }
        }
    });

    window.isTaxRobot = true;

    $('#add-new-account').click(function(e) {
        e.preventDefault();
        $(this).prop('disabled', true);
        $('.show-first-account-number').show();
        $('.new-account:hidden').first().show(250);
    });

    $(document).on("click", "#get-fias-ittem", function(e) {
        e.preventDefault();
        $(".fias-result-value").html("");
        $("#fias-result-modal").modal("show");
    });

    $("#fias-address").suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: "78497656dfc90c2b00308d616feb9df60c503f51",
        type: "ADDRESS",
        onSelect: function(suggestion) {
            if (suggestion.data) {
                if (suggestion.data.tax_office_legal) {
                    $('#fias-result-ifns').val(suggestion.data.tax_office_legal);
                } else {
                    $('#fias-result-ifns').val('');
                }
                if (suggestion.data.oktmo) {
                    $('#fias-result-oktmo').val(suggestion.data.oktmo.slice(0,8));
                } else {
                    $('#fias-result-oktmo').val('');
                }
                if (suggestion.data.postal_code) {
                    $('#fias-result-index').val(suggestion.data.postal_code);
                } else {
                    $('#fias-result-index').val('');
                }
            }

            $('#fias-address').parents(".form-group").removeClass("has-error").find(".help-block").remove();

            console.log(suggestion.data);
        }
    });

    $(document).on("click", "#copy-fias-result", function(e) {
        e.preventDefault();
        var form_group = $('#fias-address').parents(".form-group");

        if ($('#fias-address').val().trim() == "") {
            $(form_group).addClass("has-error");
            $(form_group).find('.help-block').remove();
            $(form_group).find('.fias-query-help').after("<div class=\"help-block\">Необходимо заполнить</div>");
            return false;
        }

        if ($("#fias-result-oktmo").val()) {
            $("#company-oktmo").val($("#fias-result-oktmo").val()).addClass('edited').parent().removeClass('has-error');
        }
        if ($("#fias-result-ifns").val()) {
            $("#company-ifns_ga").val($("#fias-result-ifns").val()).addClass('edited').parent().removeClass('has-error');
        }
        if ($("#fias-result-index").val()) {
            $("#company-address_legal").val($("#fias-result-index").val() + ', ' + $("#fias-address").val());
        }

        if ($("#company-ifns_ga").val() && $("#company-oktmo").val())
            $("#get-fias-ittem").html("<span class='glyphicon glyphicon-search'></span> Найти код ИФНС и ОКТМО");


        $('#company-inn').parents('.form-group').find('label').css({'color':'#999'});
        $('#company-address_legal, #company-ifns_ga, #company-oktmo').addClass('edited').parent().removeClass('has-error').addClass('has-success');
        $("#get-fias-ittem").removeClass('error');
        window.ifns_error = 0;
        window.oktmo_error = 0;

        $('#company-update-form').find('.has-error').removeClass('has-error');

        $("#fias-result-modal").modal("hide");

        $("#company-ifns_ga").trigger("change");
    });

    $('#company-has_chief_patronymic').change(function() {
       if ($(this).prop('checked'))
           $('#company-ip_patronymic').val('').removeClass('edited').parent().removeClass('has-error');
    });

    $(document).on("click", "#show-taxregistrationdate", function() {
        $('#company-taxregistrationdate').datepicker('show');
    });

    function pulsateInnField()
    {
       $('#company-inn').parents('.form-group').find('label').css({'color':'red'});
       $("#company-inn").pulsate({
           color: "#f00",
           reach: 20,
           repeat: 3
       });
    }

    $('#company-inn').on('change', function() {
       if (!$('#company-inn').val()) {
           pulsateInnField();
       } else {
           $('#company-inn').parents('.form-group').find('label').css({'color':'#999'});
       }
    });

   window.ifns_error = 0;
   window.oktmo_error = 0;
   window.show_pulsate = 0;
    $('#company-update-form').on('afterValidateAttribute', function (event, attribute, message) {

        //console.log(attribute);
        if (attribute.container == '.field-checkingaccountant-rs' && message.length
            || attribute.container == '.field-checkingaccountant-bik' && message.length) {

            $('#not_use_account').show();
        }

        if (attribute.container == '.field-company-ifns_ga') {
            window.ifns_error = (message.length) ? 1 : 0;
            window.show_pulsate = 1;
        }
        if (attribute.container == '.field-company-oktmo') {
            window.oktmo_error = (message.length) ? 1 : 0;
            window.show_pulsate = 1;
        }

        if (window.ifns_error || window.oktmo_error) {
            //$("#get-fias-ittem").html("<img src='/img/icons/warning1.png'/> Проверить код ИФНС и ОКТМО");
            if (window.show_pulsate) {
                window.show_pulsate = 0;
                $("#get-fias-ittem").addClass('error').parent().pulsate({
                   color: "#f00",
                   reach: 20,
                   repeat: 3
                });
            }
        } else {
            $("#get-fias-ittem").removeClass('error');
        }
    });

    $('#not_use_account').on('click', function() {
       var form  = $('#company-update-form');
       var input = $(form).find('input[name="not_use_account"]');
       // use accounts
       if ($(input).val() == 1) {
           $('#add-new-account').removeAttr('disabled');
           $(input).val(0);
           $(form).submit();
           $(this).addClass('error');
       // not use accounts
       } else {
           $(input).val(1);
           $(form).find('.has-error').removeClass('has-error');
           $('.new-account').hide().find('input').val("");
           $('#add-new-account').prop('disabled', true);
           $('#checkingaccountant-bik, #checkingaccountant-rs, #checkingaccountant-bank_name, #checkingaccountant-ks').val("");
           $(this).removeClass('error');
       }
    });

    $('.sbs-el').bind('click', function(e) {
        e.preventDefault();
        $('#company-update-form').yiiActiveForm('validate');

        if ($("#company-update-form").find(".has-error").length) {
            taxrobotCompanyPanelOpen();
        }

        $('#company-update-form').yiiActiveForm('submitForm');
    });

    function repaintAccountNumbers() {
        $('.company-account').each(function(i,v) {
           $(v).find('span.font-bold > span').html(2+i);
        });
    }

    $(document).on("click", ".btn-confirm-yes", function() {
        var account_id = $(this).data('model_id');
        var account_block = $('.company-account').filter('[data-id="' + account_id + '"]');

        if (account_block.length) {

            if (!$(account_block).find('.dictionary-bik').val().trim())
               $('#add-new-account').removeAttr('disabled');

            if (!$(account_block).hasClass('new-account')) {
                $('#company-update-form').append('<input type="hidden" name="delete_rs[]" value="' + account_id + '" />');
            }

            $(account_block).remove();
            repaintAccountNumbers();
        }

        return false;
    });

    $('.rs-input').change(function(){
       $(this).val($(this).val().replace(/ +/g, ''));
    });

JS
);

if (!$isFirstVisit && (Yii::$app->session->remove('show_example_popup') || Yii::$app->request->get('show_example'))) {
    echo $this->render('_first_show_modal');
    $this->registerJs("
        $(document).on('hidden.bs.modal', '#robot-first-show-modal', function() {
            if (!$('#company-inn').val()) {
                pulsateInnField();
            }
        });
    ");
    $this->registerCss('.dropdown-notification-list-head { display:none!important }');
} else {
    $this->registerJs("
        $(document).ready(function() {
            if (!$('#company-inn').val()) {
                pulsateInnField();
            }
        });
    ");
}
?>

<?php if ($isFirstVisit || $showHelp) {
    echo $this->render('part_company/first_show_help.php', [
        'company' => $company,
        'isFirstVisit' => $isFirstVisit,
    ]);
} ?>
