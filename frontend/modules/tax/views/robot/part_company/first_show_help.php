<?php

?>
<div>
    <style>
        .black-screen {
            position: absolute;
            width: 100%;
            background-color: #000000ab;
            z-index: 10000;
            height: 100%;
            top: 0;
            display: none;
        }

        .tooltip-block-show {
            z-index: 10000;
            background-color: white;
        }

        .not-clickable {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            z-index: 99999;
            display: none;
        }

        .tooltip-block-show .not-clickable {
            display: block;
        }

        .tooltipster-light {
            z-index: 10050 !important;
        }

        .tooltipster-light .tooltipster-box {
            background: transparent;
            border: none;
            margin-left: 0px !important;
        }

        .tooltipster-light .tooltipster-content {
            /* color: #4276a4 !important;
             font-weight: bold;
             padding: 0px !important;
             font-family: "Segoe Print";*/
        }

        .tooltipster-light .tooltipster-arrow-background, .tooltipster-light .tooltipster-arrow-border {
            display: none !important;
        }

        .hidden1024 {
            display: none;
        }

        @media (min-width: 1024px) {
            .hidden1024 {
                display: block;
            }
        }

    </style>

    <?php if (TRUE): ?>
        <script>

            window.addEventListener('load', function () {
                var tooltipFirst = (function () {
                    var active = false;
                    var wight = 1;
                    var t = $('.tooltip-first-r');
                    var currentComp = <?= $company->id ?: 0 ?>;

                    var elements = {
                        requisites: 'tooltip_requisites',
                        addProduct: 'tooltip_help_add_product',
                        logo: 'tooltip_help_logo',
                        save: 'tooltip_help_save',
                    };

                    var blocks = {
                        requisites: 'tooltip_requisites_block',
                        addProduct: 'tooltip_add_product_block',
                        logo: 'tooltip_help_logo_block',
                        save: 'tooltip_help_save_block'
                    };

                    var reposition = function () {
                        t.tooltipster('reposition');
                    };

                    var init = function () {
                        var t = $('.tooltip-first-r');
                        t.tooltipster('open');
                        $('.black-screen').addClass('show');

                        hide.noActive();

                        $(document).on('click', '#' + elements.requisites + " a", function () {
                            hide.element(elements.requisites);
                            hide.noActive();
                        });

                        $(document).on('click', '#' + elements.addProduct + " a", function () {
                            hide.element(elements.addProduct);
                            hide.noActive();
                        });

                        $(document).on('click', '#' + elements.logo + " a", function () {
                            hide.element(elements.logo);
                            hide.noActive();
                        });

                        $(document).on('click', '#' + elements.save + " a", function () {
                            hide.element(elements.save);
                            hide.noActive();
                        });

                        // Esc key
                        $(document).on('keyup', function (evt) {
                            if (evt.keyCode == 27) {
                                hide.noActive(true);
                            }
                        });

                        // click black screen
                        $(document).on('click', '.black-screen', function () {
                            hide.noActive(true);
                        });

                        // click white blocks
                        $(document).on('click', '.not-clickable', function () {
                            hide.noActive(true);
                        });

                        tooltipFirst.active = true;
                    };

                    var countHide = 0;
                    var hide = {
                        element: function (idTooltip, hideAll) {
                            if (hideAll == null) {
                                Company.add(idTooltip);
                            }
                            $('#' + idTooltip).remove();
                            countHide++;
                        },

                        noActive: function (hideAll) {
                            if (hideAll == null) {
                                hideAll = false;
                            }

                            $.each(elements, function (def, idElements) {
                                if (hideAll) {
                                    hide.element(idElements, hideAll);
                                }
                            });

                            $.each(blocks, function (def, idElements) {
                                if (hideAll) {
                                    $('#' + idElements).removeClass('tooltip-block-show');
                                } else {
                                    $('#' + idElements).addClass('tooltip-block-show');
                                }
                            });

                            if (hideAll || countHide === 4) {
                                $('.black-screen').removeClass('show');
                            }

                        }
                    };

                    var Company = {
                        has: function (idTooltip, idComp) {
                            var companies = Company.getCompanies(idTooltip);
                            var isHas = false;
                            $.each(companies, function (def, idCompany) {
                                if (idComp == idCompany) {
                                    isHas = true;
                                    return true;
                                }
                            });
                            return isHas;
                        },

                        add: function (idTooltip) {
                            if (!Company.has(idTooltip, currentComp)) {
                                var companies = Company.getCompanies(idTooltip);
                                companies.push(currentComp);
                                companies = companies.join(',');
                                document.cookie = idTooltip + "=" + companies;
                            }
                        },

                        getCompanies: function (idTooltip) {
                            if (!idTooltip) {
                                return [];
                            }
                            var companies = Company.getCookie(idTooltip);
                            if (companies) {
                                return companies.split(',')
                            }
                            return [];
                        },

                        getCookie: function (name) {
                            var matches = document.cookie.match(new RegExp(
                                "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
                            ));
                            return matches ? decodeURIComponent(matches[1]) : false;
                        }

                    };

                    return {
                        init: init,
                        reposition: reposition,
                        hide: hide,
                        wight: wight
                    }
                })();

                <?php if ($isFirstVisit) : ?>
                    if ($('#taxrobot-start-modal').length > 0) {
                        $(document).on('hide.bs.modal', '#taxrobot-start-modal', function () {
                            if (!tooltipFirst.active) {
                                tooltipFirst.init();
                            }
                        });
                    } else {
                        tooltipFirst.init();
                    }
                <?php else : ?>
                    tooltipFirst.init();
                <?php endif ?>

                if ($(window).width() < 1024) {
                    tooltipFirst.hide.noActive(true);
                }

                $(window).resize(function () {
                    if ($(window).width() < 1024) {
                        if (tooltipFirst.wight == 1) {
                            tooltipFirst.hide.noActive(true);
                        }
                        tooltipFirst.wight = 0;
                    } else {
                        tooltipFirst.wight = 1;
                    }
                });

            });


        </script>
    <?php endif ?>

    <!-- helpers tooltip.end -->

    <div class="tooltip_help_templates" style="display:none">
        <div id="tooltip_requisites">
            <div class="hidden1024 pad0" style="width: 578px; position: absolute; color: white">
                <div style="display: block; width: 35px; float:left; padding-top: 30px;">
                    <img style="width: 55px;height: 37px;" src="/img/tips/arrow/1b.png">
                </div>
                <div style="display: block;padding-left: 29px;width: 341px;">
                    <div style="float: left;width: 45px">
                        <img style="height:25px;" src="/img/tips/arrow/circle.png">
                        <div style="margin-top:-22px; text-align: center;width: inherit;"> 1</div>
                    </div>
                    <div style="float: left;width: 261px;">
                        <h4 style="margin: 0px; padding-top:3px"><strong> Автозаполнение по ИНН ИП</strong></h4>
                        <p style="padding-top: 10px;padding-bottom: 10px;"> Укажите ИНН своего ИП. <br> Остальные данные подтянутся автоматически</p>
                        <a href="javascript:void(0);"
                           style="padding: 7px 25px;border: 1px solid white;border-radius: 3px !important;color:white;text-transform: uppercase;">
                            Понятно </a>
                    </div>
                </div>
            </div>
        </div>
        <div id="tooltip_help_add_product">
            <div class="hidden1024 pad0" style="width: 578px;position: absolute;color: white;margin-top: -11px;">
                <div style="display: block;width: 92px;float: left;margin-top: -10px !important;">
                    <img style="width: 88px;height: 39px;" src="/img/tips/arrow/2.png">
                </div>
                <div style="width: 304px;float: left;">
                    <div style="float: left;width: 45px">
                        <img style="height:25px;" src="/img/tips/arrow/circle.png">
                        <div style="margin-top:-22px; text-align: center;width: inherit;"> 2</div>
                    </div>
                    <div style="float: left;width: 221px;">
                        <h4 style="margin: 0px; padding-top:3px"><strong> Добавить код ИФНС и ОКТМО </strong></h4>
                        <p style="padding-top: 10px;padding-bottom: 10px;"> Добавьте адрес регистрации ИП, <br> чтобы определить код налоговой и ОКТМО </p>
                        <a href="javascript:void(0);"
                           style="padding: 7px 25px;border: 1px solid white;border-radius: 3px !important;color:white;text-transform: uppercase;">
                            Понятно </a>
                    </div>
                </div>
            </div>
        </div>

        <div id="tooltip_help_logo">
            <div class="hidden1024 pad0" style="width: 471px;position: absolute;color: white;">
                <div style="display: block;width: 92px;float: left;">
                    <img style="width: 85px;height: 35px;" src="/img/tips/arrow/3.png">
                </div>
                <div style="width: 304px;float: left;padding-top: 13px;">
                    <div style="float: left;width: 45px">
                        <img style="height:25px;" src="/img/tips/arrow/circle.png">
                        <div style="margin-top:-22px; text-align: center;width: inherit;"> 3</div>
                    </div>
                    <div style="float: left;width: 250px;">
                        <h4 style="margin: 0px; padding-top:3px">
                            <strong> Реквизиты для платежек </strong>
                        </h4>
                        <p style="padding-top: 10px;padding-bottom: 10px;"> Заполните БИК банка и номер расчетного счета ИП для подготовки платежек </p>
                        <a href="javascript:void(0);" class="companyImages-button"
                           style="padding: 7px 25px;border: 1px solid white;border-radius: 3px !important;color:white;text-transform: uppercase;">
                            Понятно </a>
                    </div>
                </div>
            </div>
        </div>

        <div id="tooltip_help_save">
            <div class="hidden1024 pad0" style="width: 578px; position: absolute; color: white">
                <div style="display: block; width: 35px; float:left; padding-top: 60px;">
                    <img style="width: 55px;height: 37px;" src="/img/tips/arrow/1b.png">
                </div>
                <div style="display: block;padding-left: 29px;width: 341px;">
                    <div style="float: left;width: 45px">
                        <img style="height:25px;" src="/img/tips/arrow/circle.png">
                        <div style="margin-top:-22px; text-align: center;width: inherit;"> 4</div>
                    </div>
                    <div style="float: left;width: 221px;">
                        <h4 style="margin: 0px; padding-top:3px"><strong>Сохраните реквизиты ИП</strong></h4>
                        <p style="padding-top: 10px;padding-bottom: 10px;"> для перехода на следующий шаг</p>
                        <a href="javascript:void(0);"
                           style="padding: 7px 25px;border: 1px solid white;border-radius: 3px !important;color:white;text-transform: uppercase;">
                            Понятно </a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>