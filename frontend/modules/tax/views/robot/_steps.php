<?php

use yii\helpers\Url;

$period = $this->context->taxRobot->getPeriod();
$periodId = $this->context->taxRobot->getUrlPeriodId();

function getStepClass($itemStep, $step) {
    return $itemStep < $step ? 'finish' : ($itemStep == $step ? 'active' : '');
}
function getStepImg($itemStep, $step) {
    return '/img/icons/forma1_'.$itemStep.($itemStep == $step ? '_active':'').'.png';
}
?>

<style>
    .sbs-menu {
        height: 70px;
    }
    .sbs-menu > a, .sbs-menu > a:hover, .sbs-menu > a:focus { text-decoration: none; }
    .sbs-el {
        width: 16.65%;
        display: block;
        float:left;
        height: 70px;
        margin: 0;
        border: 1px solid #eee;
    }
    .sbs-el > table {
        width:100%;
        height:100%;
    }
    .sbs-el td.image-block {
        width: 50px;
        height: 50px;
        border-radius: 25px !important;
        padding: 8px 14px;
        background: #eee;
        float:left;
        margin-top: 10px;
        margin-left: 10px;
    }
    @media (max-width: 1250px) and (min-width: 640px) {
        .sbs-el td.image-block {
            display:none;
        }
    }
    @media (max-width: 639px) {
        .sbs-el td.text-block {
            display:none;
        }
        .sbs-el td.image-block {
            margin-left:5px;
        }
    }
    .sbs-el td.text-block {
        vertical-align: middle;
        text-align: center;
        padding: 0 10px;
        font-size: 14px;
        font-weight: 700;
        color: #000 !important;
    }

    .sbs-el .image-block > div {
        background-repeat: no-repeat;
        background-size: 70% !important;
    }

    /* active */
    .sbs-menu .active {
        border: 1px solid #67d77d;
    }

    .sbs-menu .active .image-block {
        background-color: #67d77d;
    }

    .sbs-menu .active .text-block {
        color: #000 !important;
    }

    /* finish */
    .sbs-menu .finish .image-block {
        border-color: #67d77d;
    }

    .sbs-menu .finish {
        background-color: #67d77d !important;
        border: 1px solid #67d77d;
        border-right: 1px solid #fff !important;
    }
    .sbs-menu .finish .text-block {
        color: #fff !important;
    }

    /* images */
    .sbs-step-3 .image-block > div {
        margin-top:3px;
        margin-left:-1px;
    }
    .sbs-step-4 .image-block > div {
        margin-top:4px;
        margin-left:-2px;
    }
    .sbs-step-5 .image-block > div,
    .sbs-step-6 .image-block > div {
        margin-left:-1px;
    }

</style>

<div class="sbs-menu">
    <!-- Step 1 -->
    <a class="sbs-el sbs-step-1 <?= getStepClass(1, $step) ?>" href="<?= Url::to(['company']) ?>" data-pjax="0">
        <table>
            <tr>
                <td class="image-block">
                    <div style="width:32px; height:43px; background-image: url(<?= getStepImg(1, $step) ?>" title="Реквизиты вашего ИП"></div>
                </td>
                <td class="text-block">
                    Реквизиты вашего ИП
                </td>
            </tr>
        </table>
    </a>
    <!-- Step 2 -->
    <a class="sbs-el sbs-step-2 <?= getStepClass(2, $step) ?>" href="<?= Url::to(['params']) ?>" data-pjax="0">
        <table>
            <tr>
                <td class="image-block">
                    <div style="width:33px; height:43px; background-image: url(<?= getStepImg(2, $step) ?>)" title="Параметры вашего ИП"></div>
                </td>
                <td class="text-block">
                    Параметры вашего ИП
                </td>
            </tr>
        </table>
    </a>
    <!-- Step 3 -->
    <a class="sbs-el sbs-step-3 <?= getStepClass(3, $step) ?>" href="<?= Url::to(['bank', 'period' => $periodId]) ?>" data-pjax="0">
        <table>
            <tr>
                <td class="image-block">
                    <div style="width:40px; height:43px; background-image: url(<?= getStepImg(3, $step) ?>)" title="Доходы за <?= $period->shortLabel ?>"></div>
                </td>
                <td class="text-block">
                    Доходы за <br>
                    <?= $period->shortLabel ?>
                </td>
            </tr>
        </table>
    </a>
    <!-- Step 4 -->
    <a class="sbs-el sbs-step-4 <?= getStepClass(4, $step) ?>" href="<?= Url::to(['calculation', 'period' => $periodId]) ?>" data-pjax="0">
        <table>
            <tr>
                <td class="image-block">
                    <div style="width:37px; height:37px; background-image: url(<?= getStepImg(4, $step) ?>)" title="Рассчитать налог"></div>
                </td>
                <td class="text-block">
                    Рассчитать налог
                </td>
            </tr>
        </table>
    </a>
    <!-- Step 5 -->
    <a class="sbs-el sbs-step-5 <?= getStepClass(5, $step) ?>" href="<?= Url::to(['payment', 'period' => $periodId]) ?>" data-pjax="0">
        <table>
            <tr>
                <td class="image-block">
                    <div style="width:33px; height:43px; background-image: url(<?= getStepImg(5, $step) ?>)" title="Заплатить налог"></div>
                </td>
                <td class="text-block">
                    Заплатить налог
                </td>
            </tr>
        </table>
    </a>
    <!-- Step 6 -->
    <a class="sbs-el sbs-step-6 <?= getStepClass(6, $step) ?>" href="<?= Url::to(['declaration', 'period' => $periodId]) ?>" data-pjax="0">
        <table>
            <tr>
                <td class="image-block">
                    <div style="width:36px; height:44px; background-image: url(<?= getStepImg(6, $step) ?>)" title="Налоговая декларация"></div>
                </td>
                <td class="text-block">
                    Налоговая декларация
                </td>
            </tr>
        </table>
    </a>

</div>
