<?php

use kartik\select2\Select2;
use yii\web\JsExpression;

echo Select2::widget([
    'id' => 'select-' . $name,
    'name' => $name,
    'options' => [
        'placeholder' => '',
        'class' => 'form-control js-ifns-search',
        'data-aolevel' => $level,
    ],
    'pluginOptions' => [
        'allowClear' => false,
        'minimumInputLength' => 0,
        'language' => [
            'inputTooShort' => new JsExpression('function(){ return "Введите первую букву" }')
        ],
        'ajax' => [
            'url' => "/dictionary/fias",
            'dataType' => 'json',
            'delay' => 250,
            'data' => new JsExpression('function(params) { return {q:params.term, level: '.$level.', guid: previousValue('.$level.')}; }'),
            'processResults' => new JsExpression('
                function (data) {
                    return {
                        results: $.map(data, function(obj) {
                            return { id: obj.AOGUID, text: obj.FULLNAME };
                        })
                    };
                }
            '),
        ],
    ],
]);