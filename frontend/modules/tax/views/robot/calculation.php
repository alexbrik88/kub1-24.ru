<?php

use common\components\widgets\BikTypeahead;
use common\components\widgets\IfnsTypeahead;
use common\models\address\AddressDictionary;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\document\InvoiceExpenditureItem;
use backend\models\Bank;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use common\models\company\CompanyType;
use \php_rutils\RUtils;
use yii\bootstrap\Dropdown;
use common\models\cash\form\CashBankFlowsForm;
use common\components\date\DateHelper;
use common\models\cash\CashFlowsBase;
use frontend\rbac\permissions;
use common\components\TaxRobotHelper;
use frontend\modules\tax\models\TaxrobotCashBankSearch;

/* @var $this yii\web\View */
/* @var $model Company */
/* @var $account CheckingAccountant */

$this->title = 'Расчет налога за ' . str_replace("&nbsp;", " ", $taxRobot->period->label);

$canCreate = Yii::$app->user->can(frontend\rbac\permissions\Cash::CREATE);
$canUpdate = Yii::$app->user->can(frontend\rbac\permissions\Cash::UPDATE);
$canDelete = Yii::$app->user->can(permissions\Cash::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);

$accountArray = $company->getCheckingAccountants()->orderBy([
    'type' => SORT_ASC,
    'rs' => SORT_ASC,
])->all();
$account = end($accountArray);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-click',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub',],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

$currentPeriod = $taxRobot->period->label;
$quarter = $taxRobot->getQuarter();

$flowTypeItems = [CashFlowsBase::FLOW_TYPE_EXPENSE => CashFlowsBase::getFlowTypes()[CashFlowsBase::FLOW_TYPE_EXPENSE]];

$taxableTotalAmount = $taxRobot->getTaxableTotalAmount(); // налогооблагаемый приход
$sumPFR = $taxRobot->getUsn6ReductionByPFR();
$sumOMS = $taxRobot->getUsn6ReductionByOMS();
$sumOver300 = $taxRobot->getUsn6ReductionByOver300();
$taxTotalAmount = $taxRobot->getUsn6TotalAmount(); // начислено
$taxPayableAmount = $taxRobot->getUsn6needPayAmount($quarter); // к уплате
$taxAdvanceAmount = $taxRobot->getUsn6PreviousTotalAmount($quarter); // налоговый аванс

$over300taxable = $taxRobot->getOver300ThisTaxableAmount();
$over300unlimit = $taxRobot->getOver300ThisUnlimitAmount();
$over300limit = $taxRobot->getOver300ThisLimit();
$over300amount = $taxRobot->getOver300ThisAmount();
$over300paid = $taxRobot->getOver300ThisPaidAmount();
$over300remaining = $taxRobot->getOver300ThisRemainingAmount();

function moneyFormat($val) {
    return number_format($val / 100, 2, '.', ' ');
}

$usn6PaidArray = $taxRobot->getUsn6Paid();
$usn6paidTotal = 0;
$usn6paidData = [];
foreach (range(1, 4) as $q) {
    if ($usn6PaidArray[$q] > 0 || $q < $quarter) {
        $usn6paidTotal += $usn6PaidArray[$q];
        $p = "{$taxRobot->getYear()}_{$q}";
        $usn6paidData[] = [
            'periodLabel' => TaxRobotHelper::periodLabel($p),
            'sum' => $usn6PaidArray[$q],
        ];
    }
}
?>

<?= $this->render('_style') ?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'tax-robot-calculation-pjax',
]); ?>

    <div class="row">
        <div class="hidden-xs hidden-sm hidden-md col-xs-12 step-by-step">

            <?= $this->render('_steps', ['step' => 4]) ?>

            <div class="pad_sm_t" style="padding-top: 10px">
                <div class="col-xs-12 col-lg-12 pad0">

                    <!-- step 2 -->
                    <div class="col-xs-12 pad0 step-by-step-form" style="min-height: auto;">
                        <div class="col-xs-12 pad0 bord-light-b" style="margin-bottom:25px;font-size:16px; font-weight: bold;">
                            Расчет налога за
                            <div style="display: inline-block; width: 200px;">
                                <div class="dropdown">
                                    <?= Html::tag('div', $currentPeriod, [
                                        'class' => 'dropdown-toggle',
                                        'data-toggle' => 'dropdown',
                                        'style' => 'display: inline-block; border-bottom: 1px dashed #000; cursor: pointer;',
                                    ])?>
                                    <?= Dropdown::widget([
                                        'id' => 'employee-rating-dropdown',
                                        'encodeLabels' => false,
                                        'items' => $taxRobot->getPeriodDropdownItems('calculation'),
                                    ])?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-lg-7 pad0">
                        <div class="portlet customer-info">
                            <span class="marg pad10-b font-bold" style="font-size:16px">
                                Уменьшение налога УСН
                                <img class="odds-panel-trigger" src="/img/open-book.svg" style="padding-left:5px; cursor:pointer">
                            </span><br/>
                            Проверьте все ли платежи, на которые можно уменьшить налог попали в расчет.<br/>
                            Ниже, вы можете добавить неучтённые платежи, чтобы уменьшить сумму налога.
                        </div>
                        <div class="portlet customer-info">
                            <div class="my-dropdown">
                                <table>
                                    <tr class="caption">
                                        <td>Страховые взносы за себя</td>
                                        <td> <?= moneyFormat($sumOMS + $sumPFR + $sumOver300) ?> ₽</td>
                                    </tr>
                                </table>
                                <div class="my-dropdown-body" style="display:none">
                                    <table>
                                        <tr>
                                            <td>1% от дохода свыше 300 000 ₽</td>
                                            <td><?= moneyFormat($sumOver300) ?> ₽</td>
                                        </tr>
                                        <tr>
                                            <td>Пенсионное страхование</td>
                                            <td><?= moneyFormat($sumPFR) ?> ₽</td>
                                        </tr>
                                        <tr>
                                            <td>Медицинское страхование</td>
                                            <td><?= moneyFormat($sumOMS) ?> ₽</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="my-dropdown" style="cursor: pointer;">
                                <table>
                                    <tr class="caption">
                                        <td>Платежи по налогу УСН Доходы</td>
                                        <td> <?= moneyFormat($usn6paidTotal) ?> ₽</td>
                                    </tr>
                                </table>
                                <?php if ($quarter > 1) : ?>
                                    <div class="my-dropdown-body" style="display:none">
                                        <table>
                                            <?php foreach ($usn6paidData as $data) : ?>
                                                <tr>
                                                    <td>Налог УСН за <?= $data['periodLabel'] ?></td>
                                                    <td><?= moneyFormat($data['sum']) ?> ₽</td>
                                                </tr>
                                            <?php endforeach ?>
                                        </table>
                                    </div>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-5 pad0 pull-right" style="padding-bottom: 5px !important; max-width: 480px;">
                        <div class="col-xs-12" style="padding-right:0px !important;">
                            <div class="col-xs-12 col-lg-9 pull-right pad3" style="max-width: 720px;">
                                <div class="portlet">
                                    <div class="customer-info bord-dark" style="margin:10px 0">
                                        <div class="portlet-body no_mrg_bottom main_inf_no-bord" style="padding-bottom:2px">
                                            <div id="tax-calculation-wrap" class="panel-group pad-l-5 pad-r-5" style="margin: 0;">
                                                <div class="text-bold" style="font-size:16px">
                                                    Сумма налога УСН к уплате:
                                                </div>
                                                <div class="text-bold" style="font-size:20px">
                                                    <?= moneyFormat($taxPayableAmount) ?> ₽
                                                </div>
                                                <div class="panel">
                                                    <div>
                                                        <?= Html::a('Расчет суммы налога УСН', '#calc-sum-tax', [
                                                            'data-toggle' => 'collapse',
                                                            'data-parent' => '#tax-calculation-wrap',
                                                        ]) ?>
                                                    </div>
                                                    <div id="calc-sum-tax" class="collapse">
                                                        <div>
                                                            Доход за <?=$currentPeriod?>: <?= moneyFormat($taxableTotalAmount) ?> ₽
                                                        </div>
                                                        <div>
                                                            Расчет налога:
                                                        </div>
                                                        <div>
                                                            <?= moneyFormat($taxableTotalAmount) ?> *
                                                            <?=$company->companyTaxationType->usn_percent ?>% =
                                                            <?= moneyFormat($taxTotalAmount) ?> ₽
                                                        </div>
                                                        <div>
                                                            Из налога вычитаем уплаченные
                                                            <br/>
                                                            страховые взносы:
                                                        </div>
                                                        <div>
                                                            − <?= moneyFormat($taxRobot->getUsn6ReductionAmount()) ?> ₽
                                                        </div>
                                                        <?php if ($quarter > 1) : ?>
                                                            <?php
                                                            $q = $quarter-1;
                                                            $p = "{$taxRobot->getYear()}_{$q}";
                                                            ?>
                                                            <div>
                                                                Вычитаем налог УСН, который уплачен за
                                                                <?= TaxRobotHelper::periodLabel($p) ?>:
                                                            </div>
                                                            <div>
                                                                − <?= moneyFormat($usn6paidTotal) ?> ₽
                                                            </div>
                                                        <?php endif ?>
                                                        <div>
                                                            Сумма налога, которую осталось<br/>
                                                            заплатить:
                                                        </div>
                                                        <div class="text-bold" style="font-size:14px">
                                                            <?= moneyFormat(max(0, $taxPayableAmount)) ?> ₽
                                                        </div>
                                                        <?php if ($taxPayableAmount < 0) : ?>
                                                            <div>
                                                                Налог не может быть отрицательным
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <div class="text-bold mar-t-10" style="font-size:16px">
                                                    1% с дохода свыше 300 000 ₽ к уплате:
                                                </div>
                                                <div class="text-bold" style="font-size:20px">
                                                    <?= moneyFormat($over300remaining) ?> ₽
                                                </div>
                                                <div class="panel">
                                                    <div>
                                                        <?= Html::a('Расчет суммы взносов', '#calc-over-sum-tax', [
                                                            'data-toggle' => 'collapse',
                                                            'data-parent' => '#tax-calculation-wrap',
                                                        ]) ?>
                                                    </div>
                                                    <div id="calc-over-sum-tax" class="collapse">
                                                        <div>
                                                            Доход за <?=$taxRobot->getYear()?> год:
                                                            <?= moneyFormat($over300taxable) ?> ₽
                                                        </div>
                                                        <div>
                                                            Расчет страхового взноса:
                                                        </div>
                                                        <div>
                                                            (<?= moneyFormat($over300taxable) ?> -
                                                            <?= moneyFormat(TaxRobotHelper::$onePercentOver) ?>) * 1% =
                                                            <?= moneyFormat($over300unlimit) ?> ₽
                                                        </div>
                                                        <div>
                                                            Из полученной суммы вычитаем уплаченные страховые взносы с дохода свыше
                                                            <?= moneyFormat(TaxRobotHelper::$onePercentOver) ?> ₽
                                                            за предыдущие кварталы этого года:
                                                        </div>
                                                        <div>
                                                            − <?= moneyFormat(min($over300paid, $over300limit)) ?> ₽
                                                        </div>
                                                        <div>
                                                            Сумма страхового взноса, которую осталось заплатить:
                                                        </div>
                                                        <div class="text-bold" style="font-size:14px">
                                                            <?= moneyFormat($over300remaining) ?> ₽
                                                        </div>
                                                        <?php if ($over300amount < $over300unlimit) : ?>
                                                            <div>
                                                                (Лимит страховых взносов на ОПС в 2019 году - <?= moneyFormat($over300limit) ?> руб.)
                                                            </div>
                                                        <?php endif ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-lg-12">
                        <?= $this->render('part_calculation/bank_table', [
                            'taxRobot' => $taxRobot,
                            'dataProvider' => $dataProvider,
                            'company' => $company,
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- buttons -->
    <div id="buttons-bar-fixed">
        <div class="col-xs-12 pad0 buttons-block">
            <?= Html::a('Назад', ['bank', 'period' => $taxRobot->getUrlPeriodId()], [
                'class' => 'btn darkblue pull-left hidden-xs',
                'style' => 'min-width: 180px;',
                'data' => ['pjax' => '0']
            ]) ?>
            <?= Html::a('<i class="fa fa-arrow-left fa-2x"></i>', ['bank', 'period' => $taxRobot->getUrlPeriodId()], [
                'class' => 'btn darkblue pull-left hidden-lg hidden-md hidden-sm',
                'title' => 'Назад',
                'data' => ['pjax' => '0']
            ]) ?>
            <?= Html::a('Далее', ['payment', 'period' => $taxRobot->getUrlPeriodId()], [
                'id' => 'submit',
                'class' => 'btn darkblue pull-right hidden-xs',
                'style' => 'min-width: 180px;',
                'data' => ['pjax' => '0']
            ]); ?>
            <?= Html::a('<i class="fa fa-arrow-right fa-2x"></i>', ['payment', 'period' => $taxRobot->getUrlPeriodId()], [
                'id' => 'submit',
                'class' => 'btn darkblue pull-right hidden-lg hidden-md hidden-sm',
                'title' => 'Далее',
                'data' => ['pjax' => '0']
            ]); ?>
        </div>
    </div>

<style>#summary-container table td:nth-child(3){display:none}</style>
<?= \frontend\modules\cash\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canDelete ? Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
            'class' => 'btn btn-sm darkblue text-white',
            'data-toggle' => 'modal',
        ]) : null,
    ],
]); ?>

<?= $this->render('part_calculation/modal-usn-payment-rules', [
    'sumPFR' => $sumPFR,
    'sumOMS' => $sumOMS,
    'sumOver300' => $sumOver300,
    'year' => $taxRobot->getYear(),
]); ?>

<?php \yii\widgets\Pjax::end(); ?>

<!-- ADD/UPDATE FLOW -->
<?php Modal::begin([
    'id' => 'update-movement-modal',
    'header' => Html::tag('h1', ''),
]); ?>

    <?php \yii\widgets\Pjax::begin([
        'id' => 'update-movement-pjax',
        'enablePushState' => false,
        'linkSelector' => '.update-movement-link',
    ]); ?>

    <?php \yii\widgets\Pjax::end(); ?>

<?php Modal::end(); ?>


<?php $this->registerJs(<<<JS
$.pjax.defaults.timeout = 10000;
var updateMoneyMovement = function(url) {
    $('#update-movement-modal').modal('show');
    $.pjax({url: url, push: false, container: '#update-movement-pjax'});
}
$(document).on("click", ".update-movement-link", function(e) {
    e.preventDefault();
    updateMoneyMovement(this.href);
});
$(document).on("click", "tr.flow-row > td:not(.no-update-modal)", function(e) {
    var row = $(this).closest('tr.flow-row');
    updateMoneyMovement(row.data("update-url"));
});
$(document).on('hidden.bs.modal', '#update-movement-modal', function (e) {
    $('.modal-header h1', this).html('');
    $('#update-movement-pjax', this).html('');
})

$(document).on("show.bs.modal", "#update-movement-modal", function(event) {
    $(".alert-success").remove();
});

$(document).on("hide.bs.modal", "#update-movement", function(event) {
    if (event.target.id === "update-movement") {
        $("#update-movement .modal-body").empty();
    }
});

$(document).on('click', '.my-dropdown', function() {
    var inner = $(this).find('.my-dropdown-body');
    if ($(inner).is(':hidden'))
        $(inner).show(250);
    else
        $(inner).hide(250);
});
JS
)?>

<?php $this->registerJs('
$(document).ready(function(){
    $(document).on("click", ".odds-panel-trigger", function(){
        $(".odds-panel").toggle("fast");
        $(this).toggleClass("active");
        return false;
    });
    $(document).on("click", ".odds-panel .side-panel-close", function () {
        $(".odds-panel").toggle("fast");
        $(".odds-panel-trigger").toggleClass("active");
    });
});
'); ?>