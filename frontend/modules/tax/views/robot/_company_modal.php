<?php

use common\models\service\PaymentType;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\modules\subscribe\forms\PaymentForm;
use yii\widgets\ActiveForm;
use yii\helpers\Html;


?>

<style>
    .taxrobot-company-panel {
        position: fixed;
        top: 0;
        right: 0;
        display: none;
        -moz-border-radius-topright: 20px;
        -webkit-border-top-right-radius: 20px;
        -moz-border-radius-bottomright: 20px;
        -webkit-border-bottom-right-radius: 20px;
        width: 400px;
        height: 100%;
        filter: alpha(opacity=85);
        background-color: #ffffff;
    }
    .taxrobot-company-panel {
        z-index: 10051;
    }
    .taxrobot-company-panel {
        border-left: 1px solid #e4e4e4;
    }
    .taxrobot-company-panel .main-block {
        padding: 20px 30px 10px 30px;
        position: absolute;
        width: 100%;
        height: 95%;
        overflow-y: scroll;
    }
    .taxrobot-company-panel .header {
        text-align: left;
        font-size: 21px;
        font-weight: 700;
    }
    .taxrobot-company-panel .side-panel-close {
        position: absolute;
        top: 10px;
        right: 10px;
        background-size: 12px 12px;
        width: 12px;
        height: 12px;
        opacity: 1;
    }
</style>

<div class="page-shading-panel taxrobot-company-panel-close hidden"></div>
<div class="taxrobot-company-panel">
    <div class="main-block" style="height: 100%;">
        <div class="header">
            <button type="button" class="close side-panel-close taxrobot-company-panel-close" aria-hidden="true">×&times;</button>
            <table style="width: 100%; height: ">
                <tr>
                    <td style="width: 10%; vertical-align: middle; text-align: center;">
                        <?= Html::tag('div', '<img src="/img/icons/tariff_4.png" style="width: 40px; height: 40px;">', [
                            'style' => '
                                display: inline-block;
                                padding: 10px;
                                background-color: #4276a4;
                                border-radius: 30px!important;
                            ',
                        ]) ?>
                    </td>
                    <td style="padding-left: 10px; font-size: 18px;">
                        Бухгалтерия ИП на УСН 6%
                    </td>
                </tr>
            </table>
        </div>

        <div style="padding-top: 30px;">
            <?= Html::beginForm(['/subscribe/default/payment'], 'post', [
                'class' => 'tariff-group-payment-form',
            ]) ?>
                <div class="mar-t-20">
                    <div class="text-bold">
                        Для корректного расчета налогов и заполнения налоговой декларации,
                        заполните все необходимые данные по вашему ИП.
                    </div>
                </div>

                <div id="js-form-alert" class="mar-t-20"></div>

                <div class="form-submit-result hidden"></div>
            <?= Html::endForm() ?>
        </div>
    </div>

</div>

<?php

$this->registerJs(<<<JS
    var taxrobotCompanyPanelOpen = function() {
        $(".taxrobot-company-panel-trigger").addClass("active");
        $(".page-shading-panel").removeClass("hidden");
        $(".taxrobot-company-panel").show("fast");
        $("#visible-right-menu-message-panel").show();
        $("html").attr("style", "overflow: hidden;");
        $(".taxrobot-company-panel .main-block").scrollTop(0);
    }

    var taxrobotCompanyPanelClose = function() {
        $(".taxrobot-company-panel-trigger").removeClass("active");
        $(".taxrobot-company-panel").hide("fast", function() {
            $(".page-shading-panel").addClass("hidden");
        });
        $("#visible-right-menu-message-panel").hide();
        $("html").removeAttr("style");
    }

    $(document).on("click", ".taxrobot-company-panel-trigger", function (e) {
        e.preventDefault();
        taxrobotCompanyPanelOpen();
        $.post("check-popup-pay-event", []);
    });

    $(document).on("click", ".taxrobot-company-panel-close", function () {
        taxrobotCompanyPanelClose();
    });

JS
);
