<?php

use common\components\widgets\BikTypeahead;
use common\components\widgets\IfnsTypeahead;
use common\models\address\AddressDictionary;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyTaxationType;
use backend\models\Bank;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use common\models\company\CompanyType;
use \php_rutils\RUtils;

/* @var $this yii\web\View */
/* @var $model Company */
/* @var $account CheckingAccountant */

$this->title = 'Параметры вашего ИП';

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

$taxation = $model->companyTaxationType;
$taxation->usn = 1;
$taxation->osno = $taxation->envd = $taxation->psn = 0;
$taxation->usn_type = CompanyTaxationType::INCOME;

if (empty($accounts)) {
    $accounts_title = 'Нет расчетных счетов в банках';
} else {
    $accounts_title = [];
    foreach ($accounts as $value) {
        $accounts_title[] =
            $value['cnt'] . ' ' .
            RUtils::numeral()->choosePlural($value['cnt'], ['расчетный счет', 'расчетных счета', 'расчетных счетов']) .
            ' в ' . $value['bank_name'];
    }
    $accounts_title = implode(', ', $accounts_title);
}
$questions = [
    'no_accounts' => $accounts_title,
    'no_workers' => 'Нет сотрудников',
    'no_foreign_currency' =>  'Нет валютных счетов',
    'no_acquiring' => 'Нет эквайринга',
    'no_cashbox' => 'Нет кассы'
];
$ofds = [
    'Ofd[taxcom]'  => 'Такском',
    'Ofd[2]'  => 'Платформа ОФД',
    'Ofd[3]'  => 'Первый ОФД',
    'Ofd[4]'  => 'Ярус',
    'Ofd[5]'  => 'Петер-Сервис Спецтехнологии (OFD.RU)',
    'Ofd[6]'  => 'Тензор',
    'Ofd[7]'  => 'СКБ Контур',
    'Ofd[8]'  => 'Тандер',
    'Ofd[9]'  => 'Калуга Астрал',
    'Ofd[10]' => 'Яндекс.ОФД',
    'Ofd[11]' => 'ЭнвижнГруп',
    'Ofd[12]' => 'Вымпел-Коммуникации',
    'Ofd[13]' => 'Мультикарта',
];
?>

<?php
$questions_messages['no_accounts'] = '
    <div id="no_accounts_message" style="display:none">
        <p>Если данные по расчетным счетам не соответствуют действительности, то вернитесь на предыдущий шаг и либо добавьте отсутствующий счет, либо удалите счет, которого нет.</p>
        <p>Если у вас есть счет, но по нему нет оборотов, то укажите его.</p>
        <p>Если у вас был счет, но вы его закрыли, то не удаляйте его.</p>
    </div>';
$questions_messages['no_workers'] = '
    <div id="no_workers_message" style="display:none">
        <p>Если у вас есть сотрудники, то наш автоматический расчет налогов и подготовка декларации вам не подойдет. Мы скоро доработаем возможность расчета налогов при наличие сотрудников.</p>
        <p>Напишите нам на почту <a href="mailto:support@kub-24.ru">support@kub-24.ru</a> и мы порекомендуем вам одного из наших партнеров – проверенную бухгалтерскую компанию. Они рассчитают вам стоимость подготовки декларации и расчета налогов с учётом наличию у вас сотрудников. От нас вы получите скидку 20% на их услуги.</p>
        <p><a href="/tax/robot/show-cub-features">Посмотреть все возможности КУБа</a></p>
    </div>';
$questions_messages['no_foreign_currency'] = '
    <div id="no_foreign_currency_message" style="display:none">
        <p>Если у вас есть валютные счета и по ним были операции, то наш автоматический расчет налогов и подготовка декларации вам не подойдет. Мы скоро доработаем возможность учета операций по валютным счетам.</p>
        <p>Напишите нам на почту <a href="mailto:support@kub-24.ru">support@kub-24.ru</a> и мы порекомендуем вам одного из наших партнеров – проверенную бухгалтерскую компанию. Они рассчитают вам стоимость подготовки декларации и расчета налогов с учётом наличию у вас операций по валютному счету. От нас вы получите скидку 20% на их услуги.</p>
        <p><a href="/tax/robot/show-cub-features">Посмотреть все возможности КУБа</a></p>
    </div>';
$questions_messages['no_acquiring'] = '
    <div id="no_acquiring_message" style="display:none">
        <p>Если у вас есть эквайринг, то наш автоматический расчет налогов и подготовка декларации вам не подойдет. Мы скоро доработаем возможность учета операций с учётом эквайринга.</p>
        <p>Напишите нам на почту <a href="mailto:support@kub-24.ru">support@kub-24.ru</a> и мы порекомендуем вам одного из наших партнеров – проверенную бухгалтерскую компанию. Они рассчитают вам стоимость подготовки декларации и расчета налогов с учётом наличию у вас эквайринга. От нас вы получите скидку 20% на их услуги.</p>
        <p><a href="/tax/robot/show-cub-features">Посмотреть все возможности КУБа</a></p>
    </div>';
$questions_messages['no_cashbox'] = '
    <div id="no_cashbox_message" style="display:none">
        <p>Если у вас есть касса, то пока мы не можем загрузить данные из ОФД (Оператора фискальных данных). Вы сможете добавить общую сумму дохода по кассе вручную. Мы скоро добавим возможность загружать данные из ОФД.</p>
        <p>Укажите ваш ОФД и мы вам сообщим о возможности загрузки данных из вашего ОФД.</p>
        <div class="row">
        ';
        $i=0;
        foreach ($ofds as $key=>$value) {
            $i++;
            if ($i == 1) {
                $questions_messages['no_cashbox'] .= '<div class="col-xs-3">';
            }

            $isChecked = ($key == 'Ofd[taxcom]' && Yii::$app->request->cookies['taxcom_user']);

            $questions_messages['no_cashbox'] .= '<p>' . Html::label(Html::checkbox(htmlspecialchars($key), $isChecked, ['class' => 'form-group']) . $value, null, ['class' => 'control-label', 'style' => 'color:#171717']) . '</p>';
            if ($i == 5) {
                $questions_messages['no_cashbox'] .= '</div>';
                $i=0;
            }
        }
        if ($i > 0)
            $questions_messages['no_cashbox'] .= '</div>';
            $questions_messages['no_cashbox'] .= '
        </div>
        <div class="clearfix"></div>
        <p><a href="/tax/robot/show-cub-features">Посмотреть все возможности КУБа</a></p>
    </div>';
?>

<?= $this->render('_style') ?>

<?php $form = ActiveForm::begin([
    'id' => 'form-ip-params',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldClass' => 'common\components\floatLabelField\FloatLabelActiveField',
    'fieldConfig' => [
        'template' => "{input}\n{label}\n{error}\n",
    ],
    'options' => [
        'class' => 'form-md-underline'
    ]
]); ?>

    <div class="row">
        <div class="hidden-xs hidden-sm hidden-md col-xs-12 step-by-step">

            <?= $this->render('_steps', ['step' => 2]) ?>

            <div class="pad_sm_t" style="padding-top: 10px">
                <div class="col-xs-12 col-lg-12 pad0">

                    <!-- step 2 -->
                    <div class="col-xs-12 pad0 step-by-step-form" style="min-height: 735px;">
                        <div class="col-xs-12 pad0 bord-light-b">
                            <span class="marg pad10-b font-bold" style="font-size:16px">Шаг 2: Укажите параметры вашего ИП</span>
                        </div>
                        <div class="row">

                            <div class="col-xs-12" style="margin-top: 25px;margin-bottom: 10px;">
                                <span class="marg pad10-b font-bold" style="font-size:16px">Система налогообложения</span>
                            </div>

                            <div class="col-xs-12 mb5 taxation-system">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12">
                                        <?= $form->field($taxation, 'osno', [
                                            'template' => "{label}\n{input}",
                                            'options' => [
                                                'class' => 'form-group form-md-line-input form-md-floating-label tooltip2-hover',
                                                'style' => 'display: inline-block; margin-right:25px;',
                                                'data-tooltip-content' => '#tooltip_osno'
                                            ],
                                            'labelOptions' => [
                                                'class' => 'control-label',
                                            ]
                                        ])->checkbox(['disabled' => (boolean) $taxation->usn]) ?>
                                        <?= $form->field($taxation, 'usn', [
                                            'template' => "{label}\n{input}",
                                            'options' => [
                                                'class' => 'form-group form-md-line-input form-md-floating-label',
                                                'style' => 'display: inline-block; margin-right:25px;',
                                            ],
                                            'labelOptions' => [
                                                'class' => 'control-label',
                                            ]
                                        ])->checkbox(['disabled' => (boolean) $taxation->osno]) ?>
                                        <?= $form->field($taxation, 'envd', [
                                            'template' => "{label}\n{input}",
                                            'options' => [
                                                'class' => 'form-group form-md-line-input form-md-floating-label',
                                                'style' => 'display: inline-block; margin-right:25px;',
                                            ],
                                            'labelOptions' => [
                                                'class' => 'control-label',
                                            ]
                                        ])->checkbox([]) ?>
                                        <?= $form->field($taxation, 'psn', [
                                            'template' => "{label}\n{input}",
                                            'options' => [
                                                'class' => 'form-group form-md-line-input form-md-floating-label',
                                                'style' => 'display: inline-block; margin-right:25px;',
                                            ],
                                            'labelOptions' => [
                                                'class' => 'control-label',
                                            ]
                                        ])->checkbox(['disabled' => $model->company_type_id != CompanyType::TYPE_IP]) ?>

                                        <div class="tax-usn-config collapse <?=($taxation->usn ? ' in' : '')?>">
                                            <?= $form->field($taxation, 'usn_type', [
                                                'options' => [
                                                    'class' => '',
                                                    'style' => 'max-width: 300px',
                                                ],
                                            ])->radioList(\common\models\company\CompanyTaxationType::$usnType, [
                                                'class' => 'md-radio-list',
                                                'style' => 'margin-top: 10px;display: inline-block;',
                                                'item' => function ($index, $label, $name, $checked, $value) use ($taxation) {
                                                    if ($index == 0) $label = 'Доходы';
                                                    if ($index == 1) $label = 'Доходы минус Расходы';
                                                    $item  = Html::beginTag('div', ['class'=>'md-radio']);
                                                    $item .= Html::radio($name, $checked, ['id' => 'usn_radio_'.$index, 'class' => 'md-radiobtn', 'style' => 'display: inline-block;', 'value' => $value]);
                                                    $item .= Html::tag('label', '<span class="inc"></span><span class="check"></span><span class="box"></span>'. $label, ['for' => 'usn_radio_'.$index, 'style' => 'width:190px']);
                                                    $item .= Html::textInput('tmp_usn_percent_'.$index,
                                                            ($checked && $taxation->usn_percent) ? $taxation->usn_percent : (($index == 1) ? 15 : 6), [
                                                                'class' => 'form-control',
                                                                'style' => 'display: inline-block; width: 50px; margin-left: 30px;text-align:right;padding-right:5px;',
                                                            ]) . ' %';
                                                    $item .= Html::endTag('div');

                                                    return $item;
                                                },
                                            ])->label(false) . "\n";
                                            ?>
                                        </div>

                                        <style>.form-md-underline .select2-container .select2-selection--single{height:28px!important;}</style>
                                        <div class="tax-patent-config collapse <?=($taxation->psn ? ' in' : '')?>">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-6" style="position: relative;margin-top:4px">
                                                            <div class="form-group form-md-underline">
                                                                <label> Субъект РФ, где получен патент: </label>
                                                                <?php
                                                                echo Select2::widget([
                                                                    'id' => 'select-ip_patent_city',
                                                                    'name' => 'Company[ip_patent_city]',
                                                                    'value' => ($taxation->psn) ? $model->ip_patent_city : null,
                                                                    'options' => [
                                                                        'placeholder' => '',
                                                                        'class' => 'form-control js-ifns-search',
                                                                    ],
                                                                    'pluginOptions' => [
                                                                        'allowClear' => false,
                                                                        'minimumInputLength' => 0,
                                                                        'language' => [
                                                                            'inputTooShort' => new JsExpression('function(){ return "Введите первую букву" }')
                                                                        ],
                                                                        'ajax' => [
                                                                            'url' => "/dictionary/fias",
                                                                            'dataType' => 'json',
                                                                            'delay' => 250,
                                                                            'data' => new JsExpression('function(params) { return {q:params.term, level:1, guid: null}; }'),
                                                                            'processResults' => new JsExpression('
                                                                        function (data) {
                                                                            return {
                                                                                results: [{id:"---", text:"---"}].concat($.map(data, function(obj) {
                                                                                    return { id: obj.FULLNAME, text: obj.FULLNAME };
                                                                                }))
                                                                            };
                                                                        }
                                                                    '),
                                                                        ],
                                                                    ],
                                                                ]);
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-md-6" style="position: relative;">
                                                            <?= $form->field($model, 'patentDate')->textInput([
                                                                'maxlength' => true,
                                                                'class' => 'form-control input-sm date-picker' . ($model->patentDate ? ' edited' : ''),
                                                                'autocomplete' => 'off',
                                                                'value' => ($taxation->psn) ? $model->patentDate : null,
                                                            ])->label('Дата начала действия патента') ?>
                                                            <?= Html::tag('i', '', [
                                                                'class' => 'fa fa-calendar',
                                                                'style' => 'position: absolute; top: 30px; right: 15px; color: #cecece; cursor: pointer;',
                                                            ]) ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-6" style="position: relative;">
                                                            <?= $form->field($model, 'patentDateEnd')->textInput([
                                                                'maxlength' => true,
                                                                'class' => 'form-control input-sm date-picker' . ($model->patentDateEnd ? ' edited' : ''),
                                                                'autocomplete' => 'off',
                                                                'value' => ($taxation->psn) ? $model->patentDateEnd : null,
                                                            ])->label('Дата окончания действия патента') ?>
                                                            <?= Html::tag('i', '', [
                                                                'class' => 'fa fa-calendar',
                                                                'style' => 'position: absolute; top: 30px; right: 15px; color: #cecece; cursor: pointer;',
                                                            ]) ?>
                                                        </div>
                                                        <div class="col-xs-2 col-md-6" style="position: relative;">
                                                            &nbsp;
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div id="message-robot-disabled-by-usn_type" style="display:<?=($taxation->usn && $taxation->usn_type == 1) ? 'block':'none'?>">
                                <div class="col-xs-12 col-md-12">
                                    <p style="color:red">1. Для системы налогообложения «Доходы минус Расходы» мы не сможем сделать автоматический расчет налогов и подготовку налоговой декларации.</p>
                                    <p>2. Напишите нам на почту support@kub-24.ru и мы порекомендуем вам одного из наших партнеров – проверенную бухгалтерскую компанию. <br/>Они рассчитают вам стоимость ведения бухгалтерии. От нас вы получите скидку до 20% на их услуги.</p>
                                    <p>3. Наш сервис помогает предпринимателям</p>
                                    <ul>
                                        <li>Выставлять счета, акты ,товарные накладные</li>
                                        <li>Контролировать должников</li>
                                        <li>Вести управленческий учет</li>
                                        <li>Видеть финансовую аналитику по бизнесу</li>
                                        <li>Учитывать товар</li>
                                    </ul>
                                    <p>
                                        <?= Html::a('Посмотреть все возможности КУБа', ['/tax/robot/show-cub-features'], [
                                            'class' => 'btn darkblue',
                                            'style' => 'min-width: 180px;margin-top:25px',
                                        ]) ?>
                                    </p>
                                </div>
                            </div>

                            <div id="message-robot-enabled" style="display:<?=(!$taxation->usn || ($taxation->usn && $taxation->usn_type == 0)) ? 'block':'none'?>">
                                <div class="col-xs-12 col-md-12 form-group">
                                    <div class="marg pad10-b font-bold" style="font-size:16px;padding-bottom:15px">Для правильного расчета налога, подтвердите, что у вашего ИП</div>
                                    <?php foreach ($questions as $key=>$value) : ?>
                                        <div class="form-md-line-input form-md-floating-label" style="margin-bottom: 10px;">
                                            <span class="md-checkbox">
                                                <input type="checkbox" id="<?=($key)?>_checkbox" data-message="<?=($key)?>_message" class="md-check questions-checkboxes" name="<?=($key)?>_checkbox" value="1" checked>
                                                <label for="<?=($key)?>_checkbox" style="color:#171717;font-weight:bold; position:relative">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                    <?=($value)?>
                                                </label>
                                            </span>

                                            <div style="padding-left:30px"><?= isset($questions_messages[$key]) ? $questions_messages[$key] : ''; ?></div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="clearfix" style="padding-bottom:30px"></div>

                    <!-- buttons -->
                    <div id="buttons-bar-fixed">
                        <div class="col-xs-12 pad0 buttons-block">
                            <?= Html::a('Назад', ['company'], [
                                'class' => 'btn darkblue hidden-xs',
                                'style' => 'min-width: 180px;'
                            ]) ?>
                            <?= Html::a('<i class="fa fa-arrow-left fa-2x"></i>', ['company'], [
                                'class' => 'btn darkblue hidden-lg hidden-md hidden-sm',
                                'title' => 'Назад'
                            ]) ?>
                            <div class="button-pulsate pull-right" style="max-width: 180px">
                                <?= Html::submitButton('Подтвердить', [
                                    'id' => 'ip-params-submit',
                                    'class' => 'btn darkblue hidden-xs',
                                    'style' => 'min-width: 180px;'
                                ]); ?>
                                <?= Html::submitButton('<i class="fa fa-arrow-right fa-2x"></i>', [
                                    'id' => 'ip-params-submit',
                                    'class' => 'btn darkblue hidden-lg hidden-md hidden-sm',
                                    'title' => 'Подтвердить'
                                ]); ?>
                            </div>
                            <div class="pull-right" style="font-size:13px;margin-right:15px">
                                Нажимая на кнопку «Подтвердить», вы подтверждаете<br/>
                                данные по вашему ИП для расчета налога.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $form->end(); ?>

<?= $this->render('tax-fail', [
    'company' => $model,
]) ?>

<div class="tooltip_templates" style="display: none;">
    <span id="tooltip_osno" style="display: inline-block; text-align: center;">
        Для ОСНО мы не сможем вам рассчитать налоги<br/> и подготовить декларацию
    </span>
</div>

<?php $this->registerJs(<<<JS
    // Система налогообложения
    function resetDisabledCheckboxes() {
        $('.taxation-system').find('.form-group').removeClass('has-success').removeClass('has-error');
    }

    $(document).on("change", "#companytaxationtype-osno", function() {
        var form = this.form;
        if (this.checked) {
            $("#companytaxationtype-usn", form).prop("checked", false).prop("disabled", true);
            $(".nds-view-osno", form).collapse("show");
        } else {
            $("#companytaxationtype-usn", form).prop("disabled", false);
            $(".nds-view-osno", form).collapse("hide");
            $(".nds-view-osno input[type=radio]", form).prop("checked", false);
        }
        resetDisabledCheckboxes();
    });
    $(document).on("change", "#companytaxationtype-usn", function() {
        var form = this.form;
        if (this.checked) {
            $("#companytaxationtype-osno", form).prop("checked", false).prop("disabled", true);
            $(".tax-usn-config", form).collapse("show");
        } else {
            $("#companytaxationtype-osno", form).prop("disabled", false);
            $(".tax-usn-config", form).collapse("hide");
            $("#companytaxationtype-usn_type input[type=radio][value=0]", form).prop("checked", true);
            $("#companytaxationtype-usn_type input[type=radio][value=1]", form).prop("checked", false);
            $("#companytaxationtype-usn_percent", form).val("");
        }
        resetDisabledCheckboxes();
    });
    $(document).on("change", "#companytaxationtype-psn", function() {
        var form = this.form;
        if (this.checked) {
            $("#companytaxationtype-envd", form).prop("checked", false).prop("disabled", true);
            $(".tax-patent-config", form).collapse("show");
        } else {
            $("#companytaxationtype-envd", form).prop("disabled", false);
            $(".tax-patent-config", form).collapse("hide");
        }
        resetDisabledCheckboxes();
    });

    // Подсказки
    $('.tax-usn-config input[type=radio]').change(function() {
        if (this.value == 1) {
            $('#ip-params-submit').prop('disabled', true);
            $('#message-robot-disabled-by-usn_type').show(250);
            $('#message-robot-enabled').hide(250);
            $('#taxrobot-start-modal').modal('show');
        } else {
            $('#ip-params-submit').prop('disabled', false);
            $('#message-robot-disabled-by-usn_type').hide(250);
            $('#message-robot-enabled').show(250);
        }
    });
    $('.questions-checkboxes').change(function() {
       var msg_id = $(this).attr('data-message');
       if ($(this).prop('checked'))
           $('#'+msg_id).hide(250);
       else
           $('#'+msg_id).show(250);

       var disabled_next_steps = false;
       $('.questions-checkboxes').each(function() {
           if (!$(this).prop('checked') && !($(this).attr('name') == 'no_cashbox_checkbox')) {
               disabled_next_steps = true;
               return false;
           }
       });

       if (disabled_next_steps) {
           $('#ip-params-submit').attr('disabled', 'disabled');
           $('.sbs-step-3, .sbs-step-4, .sbs-step-5, .sbs-step-6').addClass('disabled');
       } else {
           $('#ip-params-submit').removeAttr('disabled');
           $('.sbs-step-3, .sbs-step-4, .sbs-step-5, .sbs-step-6').removeClass('disabled');
       }

    });
    $(document).on("click", ".sbs-el", function(e) {
          e.preventDefault();
          if ($(this).hasClass('disabled'))
            return false;
          else
            window.location.href = $(this).attr('href');
    });

    $(document).ready(function() {
        $("#ip-params-submit").parents(".button-pulsate").pulsate({
            color: "#bf1c56",
            reach: 20,
            repeat: 3
        });
    });


JS
)?>