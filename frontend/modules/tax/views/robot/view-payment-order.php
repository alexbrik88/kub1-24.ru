<?php

use common\components\TaxRobotHelper;
use common\models\document\PaymentOrder;
use common\models\document\status\PaymentOrderStatus;
use yii\bootstrap\Dropdown;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $taxRobot common\components\TaxRobotHelper */

$year = mb_substr($model->tax_period_code, -4);
if ($model->kbk == TaxRobotHelper::$kbkUsn6) {
    $this->title = $taxRobot->getUsn6Title($taxRobot->getPeriodLabelByCode($model->tax_period_code));
    $title = 'Налог УСН Доходы';
    $payment = 'налог УСН за ';
} elseif ($model->kbk == TaxRobotHelper::$kbkPfr && mb_strpos($model->purpose_of_payment, '300') !== false) {
    $this->title = $taxRobot->getOver300Title($year);
    $title = 'Взносы в ПФР';
    $payment = 'взносы в размере 1% за';
} elseif ($model->kbk == TaxRobotHelper::$kbkPfr) {
    $this->title = $taxRobot->getPfrTitle($year);
    $title = 'Взносы в ПФР';
    $payment = 'взносы в ПФР за';
} elseif ($model->kbk == TaxRobotHelper::$kbkOms) {
    $this->title = $taxRobot->getOmsTitle($year);
    $title = 'Взносы в ОМС';
    $payment = 'взносы в ОМС за';
} else {
    $this->title = $model->purpose_of_payment;
    $title = '';
    $payment = '';
}

$this->context->layoutWrapperCssClass = 'out-act out-document payment-order';
$needPay = $model->payment_order_status_id != PaymentOrderStatus::STATUS_PAID;
$urlPeriod = $taxRobot->getUrlPeriodId();
?>

<?= $this->render('_style') ?>

<div class="row">
    <div class="col-xs-12 step-by-step" style="margin-bottom: 35px;">
        <?= $this->render('_steps', ['step' => 5]) ?>
    </div>
    <div class="col-xs-12">
        <div style="position: relative;">
            <?= Html::a('Назад к списку платежек', [
                'payment',
                'period' => $urlPeriod,
            ], [
                'style' => 'position: absolute; top: -23px;'
            ]) ?>
        </div>
        <div class="bord-light-b" style="font-size:16px; font-weight: bold;">
            <?= $this->title ?>
        </div>
    </div>
</div>

<div class="page-content-in" style="padding-bottom: 30px;">
    <div class="row">
        <div class="col-md-12 col-lg-7" style="margin-top: 8px;">
            <div style=" padding: 0 5px 50px; min-width: 520px;max-width: 735px; border: 1px solid rgb(66, 118, 164);">
                <?= $this->render('view-payment-order/main-info', [
                    'model' => $model,
                    'taxRobot' => $taxRobot,
                    'needPay' => $needPay,
                    'urlPeriod' => $urlPeriod,
                ]); ?>
            </div>
        </div>
        <div class="col-md-12 col-lg-5 pull-right mar-t-5 mar-b-5"
             style="max-width: 480px;">
            <div class="col-xs-12" style="padding-right:0 !important;">
                <div class="control-panel col-xs-12 pad0 pull-right">
                    <?= $this->render('view-payment-order/status-block', [
                        'model' => $model,
                    ]); ?>
                </div>
                <div class="col-xs-12 col-lg-9 pull-right pad3" style="max-width: 720px;margin-top: 5px;">
                    <?= $this->render('view-payment-order/additional-info', [
                        'model' => $model,
                        'title' => $title,
                        'payment' => $payment,
                        'taxRobot' => $taxRobot,
                        'needPay' => $needPay,
                        'urlPeriod' => $urlPeriod,
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

    <div id="buttons-bar-fixed">
        <?= $this->render('view-payment-order/action-buttons', [
            'model' => $model,
            'taxRobot' => $taxRobot,
            'needPay' => $needPay,
            'urlPeriod' => $urlPeriod,
        ]); ?>
    </div>
</div>
