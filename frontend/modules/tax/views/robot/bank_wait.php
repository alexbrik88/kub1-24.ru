<?php

use frontend\components\Icon;
use yii\bootstrap\Modal;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $taxRobot common\components\TaxRobotHelper */

?>

<?php Modal::begin([
    'id' => 'bank_wait_modal',
    'closeButton' => false,
]); ?>

    <div class="taxrobot-start-header">
        <h4 class="counter-toggle-hide wait">
            Подождите, рассчитываем налоги и заполняем декларацию…
        </h4>
        <h4 class="counter-toggle-hide ready hidden">
            Готово!
        </h4>
    </div>

    <div class="taxrobot-start-content">
        <div class="mb-3" style="
            position: relative;
            width: 100%;
            padding-top: 40%;
            margin: 0 auto;
            background-image: url(/images/declaration_for_10_min.png);
            background-position: center;
            background-size: 150%;
        "></div>
        <div class="counter-toggle-hide wait text-center">
            Осталось:
            <strong style="display: inline-block; font-size: 20px; width: 50px">
                <span class="counter-element link">15</span>
            </strong>
            секунд
        </div>
        <div class="counter-toggle-hide ready hidden text-center">
            <div>
                Налоги по <strong><?= $taxRobot->company->title ?></strong> рассчитаны,
                <br>
                платежки по налогам подготовлены, декларация заполнена.
                <br>
                <strong>
                    Для скачивания платежек и декларации, необходимо оплатить сервис
                </strong>
            </div>
            <?= Html::button('Оплатить доступ к сервису', [
                'data-dismiss' => 'modal',
                'class' => 'button-regular button-regular_red mt-3 taxrobot-pay-panel-trigger',
            ]) ?>
        </div>
    </div>

<?php Modal::end(); ?>

<?php $this->registerJs(<<<JS
    (function( $ ) {
        var waitModal = $("#bank_wait_modal");
        var timer;
        var showWaitModal = function () {
            $("#banking-module-modal").modal("hide");
            waitModal.modal("show");
        };
        var animatedCounter = function (el) {
            var time = 15 * 1000; // 15 sec
            var current = 1500;
            var end = 0;
            var interval = time / current;
            timer = setInterval(function() {
                current -= 1;
                $(el).html((current/100).toFixed(2));
                if (current == end) {
                    clearInterval(timer);
                    $(".counter-toggle-hide", waitModal).toggleClass("hidden");
                }
            }, interval);
        };

        $(document).on("submit", "form.taxrobot-not-paid-form", function (e) {
            console.log("submit");
            e.preventDefault();
            showWaitModal();
        });
        $(document).on("click", "form button.taxrobot-not-paid", function (e) {
            console.log("click");
            e.preventDefault();
            showWaitModal();
        });
        waitModal.on("show.bs.modal", function () {
            $('.counter-element', this).each(function(i, el) {
                animatedCounter(el);
            });
        });
        waitModal.on("hidden.bs.modal", function () {
            clearInterval(timer);
            $('.counter-element', this).html("15");
            $(".counter-toggle-hide wait", waitModal).toggleClass("hidden", false);
            $(".counter-toggle-hide ready", waitModal).toggleClass("hidden", true);
        });
    }( window.jQuery ));
JS
) ?>