<?php $declarationHelper = isset($declarationHelper) ? $declarationHelper : new \frontend\modules\tax\models\DeclarationOsnoHelper(); ?>
<?=
$this->render('partial/_pdf-page-1', [
    'model' => $model,
    'company' => $company,
    'declarationHelper' => $declarationHelper,
]);
?>
<?=
$this->render('partial/_pdf-page-2', [
    'model' => $model,
    'company' => $company,
    'declarationHelper' => $declarationHelper,
]);
?>
<?=
$this->render('partial/_pdf-page-3', [
    'model' => $model,
    'company' => $company,
    'declarationHelper' => $declarationHelper,
]);
?>
<?=
$this->render('partial/_pdf-page-4', [
    'model' => $model,
    'company' => $company,
    'declarationHelper' => $declarationHelper,
]);
?>
