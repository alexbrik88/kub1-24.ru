<?php
use common\components\date\DateHelper;

$documentDate = date('Y-m-d');
$formattedDate = DateHelper::format($documentDate, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

?>

<style media="print">
    @page {
        size: auto;
        margin: 0;
    }
    .page-break  {
        page-break-after: always;
    }
</style>

<style>

    .pad-pdf-p {
        padding-left: 40px !important;
        padding-right: 30px !important;
    }

    .table {margin:0; padding:0;}
    .table td {font-size:9.5pt;vertical-align: middle;padding:1px 2px;}
    .table td.th {padding-top:5px;padding-bottom:5px;}
    .table td.bold {font-weight:bold}
    .table td.no-bt {border-top:none;}
    .table td.no-bb {border-bottom:none;}
    .table tr.fs-8 td {font-size:8.5pt;}
    .table td.fs-10 {font-size:10pt;}
    .table td.fs-11 {font-size:11pt;}
    .table td.bt {border-top:1px solid #000;}
    .table td.bb {border-bottom:1px solid #000;}
    .table td.bl {border-left:1px solid #000;}
    .table td.br {border-right:1px solid #000;}
    .table td.bt2 {border-top:2px solid #000;}
    .table td.bb2 {border-bottom:2px solid #000;}
    .table td.bl2 {border-left:2px solid #000;}
    .table td.br2 {border-right:2px solid #000;}
    .table td.tip {padding:0 0 3px 0; font-size:8pt; text-align: center;}
    .table td.pad-3 {padding-top:3pt;padding-bottom:3pt;}
    .table td.ver-bottom {vertical-align:bottom}
    .table td.ver-top {vertical-align:top}
    .table td.pad-l {padding-left:10pt}
</style>

<div class="page-content-in p-center pad-pdf-p page-break">

    <table class="table no-border">
        <tr>
            <td class="text-center fs-11 bold">
                Бухгалтерский баланс
            </td>
        </tr>
        <tr>
            <td class="text-center fs-10 bold">
                на 31 декабря <?= $model->tax_year ?> г.
            </td>
        </tr>
    </table>

    <?= $this->render('_pdf-pages-head.php', [
            'okud' => '0710001',
            'company' => $company,
            'model' => $model
    ]) ?>

    <div style="margin:20pt"></div>

    <table class="table">
        <tr>
            <td width="11%" class="th text-center">Пояснения</td>
            <td width="39%" class="th text-center">Наименование показателя</td>
            <td width="6%" class="th text-center">Код</td>
            <td class="th text-center bb2">На 31 декабря <?= $model->tax_year ?> г.</td>
            <td class="th text-center bb2">На 31 декабря <?= $model->tax_year - 1 ?> г.</td>
            <td class="th text-center bb2">На 31 декабря <?= $model->tax_year - 2 ?> г.</td>
        </tr>
        <tr>
            <td class="no-bb"></td>
            <td class="no-bb text-center bold">АКТИВ</td>
            <td class="no-bb"></td>
            <td class="no-bb bl2"></td>
            <td class="no-bb"></td>
            <td class="no-bb br2"></td>
        </tr>
        <!--I. ВНЕОБОРОТНЫЕ АКТИВЫ-->
        <tr>
            <td class="no-bt no-bb"></td>
            <td class="no-bt no-bb text-center bold">I. ВНЕОБОРОТНЫЕ АКТИВЫ</td>
            <td class="no-bt no-bb"></td>
            <td class="no-bt no-bb bl2"></td>
            <td class="no-bt no-bb"></td>
            <td class="no-bt no-bb br2"></td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Нематериальные активы</td>
            <td class="no-bt text-center">1110</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Результаты исследований и разработок</td>
            <td class="no-bt text-center">1120</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Нематериальные поисковые активы</td>
            <td class="no-bt text-center">1130</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Материальные поисковые активы</td>
            <td class="no-bt text-center">1140</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Основные средства</td>
            <td class="no-bt text-center">1150</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Доходные вложения в материальные ценности</td>
            <td class="no-bt text-center">1160</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Финансовые вложения</td>
            <td class="no-bt text-center">1170</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Отложенные налоговые активы</td>
            <td class="no-bt text-center">1180</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt bb2">Прочие внеоборотные активы</td>
            <td class="no-bt text-center bb2">1190</td>
            <td class="no-bt text-right bl2 bb2">-</td>
            <td class="no-bt text-right bb2">-</td>
            <td class="no-bt text-right br2 bb2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Итого по разделу I</td>
            <td class="no-bt text-center">1100</td>
            <td class="no-bt text-right bl2 bb2">-</td>
            <td class="no-bt text-right bb2">-</td>
            <td class="no-bt text-right br2 bb2">-</td>
        </tr>
        <!--II. ОБОРОТНЫЕ АКТИВЫ-->
        <tr>
            <td class="no-bb"></td>
            <td class="no-bb text-center bold">II. ОБОРОТНЫЕ АКТИВЫ</td>
            <td class="no-bb"></td>
            <td class="no-bb bl2"></td>
            <td class="no-bb"></td>
            <td class="no-bb br2"></td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Запасы</td>
            <td class="no-bt text-center">1210</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Налог на добавленную стоимость по приобретенным ценностям</td>
            <td class="no-bt text-center">1220</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Дебиторская задолженность</td>
            <td class="no-bt text-center">1230</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Финансовые вложения (за исключением денежных эквивалентов)</td>
            <td class="no-bt text-center">1240</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Денежные средства и денежные эквиваленты</td>
            <td class="no-bt text-center">1250</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt bb2">Прочие оборотные активы</td>
            <td class="no-bt text-center bb2">1260</td>
            <td class="no-bt text-right bl2 bb2">-</td>
            <td class="no-bt text-right bb2">-</td>
            <td class="no-bt text-right br2 bb2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Итого по разделу II</td>
            <td class="no-bt text-center">1200</td>
            <td class="no-bt text-right bl2 bb2">-</td>
            <td class="no-bt text-right bb2">-</td>
            <td class="no-bt text-right br2 bb2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt bold">БАЛАНС</td>
            <td class="no-bt text-center">1600</td>
            <td class="no-bt text-right bl2 bb2">-</td>
            <td class="no-bt text-right bb2">-</td>
            <td class="no-bt text-right br2 bb2">-</td>
        </tr>
    </table>
</div>