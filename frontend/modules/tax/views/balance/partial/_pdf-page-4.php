<?php
use common\components\date\DateHelper;

$documentDate = date('Y-m-d');
$formattedDate = DateHelper::format($documentDate, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

?>

<div class="page-content-in p-center pad-pdf-p">

    <table class="table no-border">
        <tr>
            <td class="text-right" style="font-size:8pt;padding-bottom:5pt;">
                Форма 0710002 с.2
            </td>
        </tr>
    </table>

    <table class="table">
        <tr>
            <td width="11%" class="th text-center">Пояснения</td>
            <td width="49%" class="th text-center">Наименование показателя</td>
            <td width="6%" class="th text-center">Код</td>
            <td class="th text-center bb2">За Январь - Декабрь <?= $model->tax_year ?> г.</td>
            <td class="th text-center bb2">За Январь - Декабрь <?= $model->tax_year - 1 ?> г.</td>
        </tr>
        <tr>
            <td></td>
            <td>Результат от переоценки внеоборотных активов, не включаемый в чистую прибыль (убыток) периода</td>
            <td class="text-center">2510</td>
            <td class="text-right bl2">-</td>
            <td class="text-right br2">-</td>
        </tr>
        <tr>
            <td></td>
            <td>Результат от прочих операций, не включаемыйв чистую прибыль (убыток) периода</td>
            <td class="text-center">2520</td>
            <td class="text-right bl2">-</td>
            <td class="text-right br2">-</td>
        </tr>
        <tr>
            <td></td>
            <td>Совокупный финансовый результат периода</td>
            <td class="text-center">2500</td>
            <td class="text-right bl2">-</td>
            <td class="text-right br2">-</td>
        </tr>
        <tr>
            <td></td>
            <td>Справочно<br/>Базовая прибыль (убыток) на акцию</td>
            <td class="text-center">2900</td>
            <td class="text-right bl2">-</td>
            <td class="text-right br2">-</td>
        </tr>
        <tr>
            <td></td>
            <td class="pad-l">Разводненная прибыль (убыток) на акцию</td>
            <td class="text-center">2910</td>
            <td class="text-right bl2 bb2">-</td>
            <td class="text-right br2 bb2">-</td>
        </tr>
    </table>


    <table class="table no-border" style="margin-top:500pt">
        <tr>
            <td width="15%">Руководитель</td>
            <td width="15%" class="bb bold"></td>
            <td width="2%"></td>
            <td width="30%" class="bb"></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="tip">(подпись)</td>
            <td></td>
            <td class="tip">(расшифровка подписи)</td>
        </tr>
    </table>
    <table class="table no-border" style="margin-top:20pt">
        <tr>
            <td width="25%" class="text-center bb">
                <?= $formattedDate ?>
            </td>
            <td></td>
        </tr>
    </table>

</div>