<?php
$reportDate = strtotime($model->tax_year . '-12-31');
?>
<table class="table no-border">
    <tr>
        <td width="12%"></td>
        <td width="7%"></td>
        <td></td>
        <td width="10%"></td>
        <td width="12%"></td>
        <td width="6%"></td>
        <td width="3%"></td>
        <td width="3%"></td>
        <td width="6%"></td>
    </tr>
    <tr class="fs-8">
        <td colspan="5"></td>
        <td colspan="4" class="text-center bold bl bb br bt">Коды</td>
    </tr>
    <tr class="fs-8">
        <td colspan="3"></td>
        <td colspan="2" class="text-right">Форма по ОКУД</td>
        <td colspan="4" class="bt2 bl2 br2 bb text-center bold">
            <?= $okud ?>
        </td>
    </tr>
    <tr class="fs-8">
        <td colspan="3"></td>
        <td colspan="2" class="text-right">Дата (число, месяц, год)</td>
        <td class="bl2 text-center pad-3 bold">
            <?= date('d', $reportDate) ?>
        </td>
        <td colspan="2" class="bl br text-center pad-3 bold">
            <?= date('m', $reportDate) ?>
        </td>
        <td class="br2 text-center pad-3 bold">
            <?= date('Y', $reportDate) ?>
        </td>
    </tr>
    <tr class="fs-8">
        <td>Организация</td>
        <td colspan="3" class="bb bold">
            <?= $company->name_full ?>
        </td>
        <td class="text-right">по ОКПО</td>
        <td colspan="4" class="bl2 bt bb br2 bold text-center">
            <?= $company->okpo ?>
        </td>
    </tr>
    <tr class="fs-8">
        <td colspan="4" class="pad-3">Идентификационный номер налогоплательщика</td>
        <td class="text-right">ИНН</td>
        <td colspan="4" class="bl2 bt bb br2 bold text-center">
            <?= $company->inn ?>
        </td>
    </tr>
    <tr class="fs-8">
        <td colspan="2">Вид экономической деятельности</td>
        <td colspan="2" class="bb bold ver-bottom">
            <?= '' //todo: расшифровка ОКВЭД ?>
        </td>
        <td class="text-right">по<br/>ОКВЭД</td>
        <td colspan="4" class="bl2 bt bb br2 bold text-center">
            <?= $company->okved ?>
        </td>
    </tr>
    <tr class="fs-8">
        <td colspan="5">Организационно-правовая форма / форма собственности</td>
        <td colspan="2" class="bl2 br"></td>
        <td colspan="2" class="br2"></td>
    </tr>
    <tr class="fs-8">
        <td colspan="3" class="bold bb">
            <?= $company->companyType->name_full ?> /
            Частная собственность
        </td>
        <td colspan="2" class="text-right ver-top">по ОКОПФ / ОКФС</td>
        <td colspan="2" class="text-center bold bl2 br bb ver-top">
            <?= '' //todo: код ОКОПФ ?>
        </td>
        <td colspan="2" class="text-center bold br2 bb ver-top">
            <?= '' //todo: код ОКФС ?>
        </td>
    </tr>
    <tr class="fs-8">
        <td colspan="2">Единица измерения:</td>
        <td>в тыс. рублей</td>
        <td colspan="2" class="text-right">по ОКЕИ</td>
        <td colspan="4" class="bl2 bb2 br2 bt bold text-center">
            384
        </td>
    </tr>
    <tr class="fs-8" style="padding-top:7pt">
        <td colspan="9">Местонахождение (адрес)</td>
    </tr>
    <tr class="fs-8">
        <td colspan="5" class="bold bb">
            <?= $company->getAddressLegalFull() ?>
        </td>
        <td colspan="4"></td>
    </tr>

</table>