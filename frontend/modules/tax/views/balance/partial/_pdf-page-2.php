<?php
use common\components\date\DateHelper;

$documentDate = date('Y-m-d');
$formattedDate = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => strtotime($documentDate),
]);
?>

<div class="page-content-in p-center pad-pdf-p page-break">

    <table class="table no-border">
        <tr>
            <td class="text-right" style="font-size:8pt;padding-bottom:5pt;">
                Форма 0710001 с.2
            </td>
        </tr>
    </table>

    <table class="table">
        <tr>
            <td width="11%" class="th text-center">Пояснения</td>
            <td width="39%" class="th text-center">Наименование показателя</td>
            <td width="6%" class="th text-center">Код</td>
            <td class="th text-center bb2">На 31 декабря <?= $model->tax_year ?> г.</td>
            <td class="th text-center bb2">На 31 декабря <?= $model->tax_year - 1 ?> г.</td>
            <td class="th text-center bb2">На 31 декабря <?= $model->tax_year - 2 ?> г.</td>
        </tr>
        <tr>
            <td class="no-bb"></td>
            <td class="no-bb text-center bold">ПАССИВ</td>
            <td class="no-bb"></td>
            <td class="no-bb bl2"></td>
            <td class="no-bb"></td>
            <td class="no-bb br2"></td>
        </tr>
        <!--III. КАПИТАЛ И РЕЗЕРВЫ-->
        <tr>
            <td class="no-bt no-bb"></td>
            <td class="no-bt no-bb text-center bold">III. КАПИТАЛ И РЕЗЕРВЫ</td>
            <td class="no-bt no-bb"></td>
            <td class="no-bt no-bb bl2"></td>
            <td class="no-bt no-bb"></td>
            <td class="no-bt no-bb br2"></td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Уставный капитал (складочный капитал, уставный фонд, вклады товарищей)</td>
            <td class="no-bt text-center">1310</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Собственные акции, выкупленные у акционеров</td>
            <td class="no-bt text-center">1320</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Переоценка внеоборотных активов</td>
            <td class="no-bt text-center">1340</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Добавочный капитал (без переоценки)</td>
            <td class="no-bt text-center">1350</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Резервный капитал</td>
            <td class="no-bt text-center">1360</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt bb2">Нераспределенная прибыль (непокрытый убыток)</td>
            <td class="no-bt text-center bb2">1370</td>
            <td class="no-bt text-right bl2 bb2">-</td>
            <td class="no-bt text-right bb2">-</td>
            <td class="no-bt text-right br2 bb2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Итого по разделу III</td>
            <td class="no-bt text-center">1300</td>
            <td class="no-bt text-right bl2 bb2">-</td>
            <td class="no-bt text-right bb2">-</td>
            <td class="no-bt text-right br2 bb2">-</td>
        </tr>
        <!--IV. ДОЛГОСРОЧНЫЕ ОБЯЗАТЕЛЬСТВА-->
        <tr>
            <td class="no-bb"></td>
            <td class="no-bb text-center bold">IV. ДОЛГОСРОЧНЫЕ ОБЯЗАТЕЛЬСТВА</td>
            <td class="no-bb"></td>
            <td class="no-bb bl2"></td>
            <td class="no-bb"></td>
            <td class="no-bb br2"></td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Заемные средства</td>
            <td class="no-bt text-center">1410</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Отложенные налоговые обязательства</td>
            <td class="no-bt text-center">1420</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Оценочные обязательства</td>
            <td class="no-bt text-center">1430</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt bb2">Прочие обязательства</td>
            <td class="no-bt text-center bb2">1450</td>
            <td class="no-bt text-right bl2 bb2">-</td>
            <td class="no-bt text-right bb2">-</td>
            <td class="no-bt text-right br2 bb2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Итого по разделу IV</td>
            <td class="no-bt text-center">1400</td>
            <td class="no-bt text-right bl2 bb2">-</td>
            <td class="no-bt text-right bb2">-</td>
            <td class="no-bt text-right br2 bb2">-</td>
        </tr>
        <!--V. КРАТКОСРОЧНЫЕ ОБЯЗАТЕЛЬСТВА-->
        <tr>
            <td class="no-bb"></td>
            <td class="no-bb text-center bold">V. КРАТКОСРОЧНЫЕ ОБЯЗАТЕЛЬСТВА</td>
            <td class="no-bb"></td>
            <td class="no-bb bl2"></td>
            <td class="no-bb"></td>
            <td class="no-bb br2"></td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Заемные средства</td>
            <td class="no-bt text-center">1510</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Кредиторская задолженность</td>
            <td class="no-bt text-center">1520</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Доходы будущих периодов</td>
            <td class="no-bt text-center">1530</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Оценочные обязательства</td>
            <td class="no-bt text-center">1540</td>
            <td class="no-bt text-right bl2">-</td>
            <td class="no-bt text-right">-</td>
            <td class="no-bt text-right br2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt bb2">Прочие обязательства</td>
            <td class="no-bt text-center bb2">1550</td>
            <td class="no-bt text-right bl2 bb2">-</td>
            <td class="no-bt text-right bb2">-</td>
            <td class="no-bt text-right br2 bb2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt">Итого по разделу V</td>
            <td class="no-bt text-center">1500</td>
            <td class="no-bt text-right bl2 bb2">-</td>
            <td class="no-bt text-right bb2">-</td>
            <td class="no-bt text-right br2 bb2">-</td>
        </tr>
        <tr>
            <td class="no-bt"></td>
            <td class="no-bt bold">БАЛАНС</td>
            <td class="no-bt text-center">1700</td>
            <td class="no-bt text-right bl2 bb2">-</td>
            <td class="no-bt text-right bb2">-</td>
            <td class="no-bt text-right br2 bb2">-</td>
        </tr>
    </table>



    <table class="table no-border" style="margin-top:300pt">
        <tr>
            <td width="15%">Руководитель</td>
            <td width="15%" class="bb bold"></td>
            <td width="2%"></td>
            <td width="30%" class="bb"></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="tip">(подпись)</td>
            <td></td>
            <td class="tip">(расшифровка подписи)</td>
        </tr>
    </table>
    <table class="table no-border" style="margin-top:20pt">
        <tr>
            <td width="25%" class="text-center bb">
                <?= $formattedDate ?>
            </td>
            <td></td>
        </tr>
    </table>

</div>