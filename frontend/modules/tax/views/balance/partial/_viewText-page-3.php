<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.05.2019
 * Time: 16:12
 */

use frontend\modules\tax\models\DeclarationOsnoHelper;

/* @var \frontend\modules\tax\models\TaxDeclaration $model */
/* @var DeclarationOsnoHelper $declarationHelper */
?>
<table class="table-preview no-border">
    <tr>
        <td class="text-center fs-11 bold">
            Отчет о финансовых результатах
        </td>
    </tr>
    <tr>
        <td class="text-center fs-10 bold">
            за Январь - Декабрь <?= $model->tax_year ?> г.
        </td>
    </tr>
</table>
<?= $this->render('_viewText-pages-head.php', [
    'okud' => '0710001',
    'company' => $model->company,
    'model' => $model
]); ?>
<div style="margin:20pt"></div>
<table class="table-preview border">
    <tr>
        <td width="11%" class="th text-center">Пояснения</td>
        <td width="49%" class="th text-center">Наименование показателя</td>
        <td width="6%" class="th text-center">Код</td>
        <td class="th text-center bb2">За Январь - Декабрь <?= $model->tax_year ?> г.</td>
        <td class="th text-center bb2">За Январь - Декабрь <?= $model->tax_year - 1 ?> г.</td>
    </tr>
    <tr>
        <td></td>
        <td>Выручка</td>
        <td class="text-center">2110</td>
        <td class="text-right bl2">-</td>
        <td class="text-right br2">-</td>
    </tr>
    <tr>
        <td></td>
        <td>Себестоимость продаж</td>
        <td class="text-center">2120</td>
        <td class="text-right bl2">-</td>
        <td class="text-right br2">-</td>
    </tr>
    <tr>
        <td></td>
        <td>Валовая прибыль (убыток)</td>
        <td class="text-center">2100</td>
        <td class="text-right bl2">-</td>
        <td class="text-right br2">-</td>
    </tr>
    <tr>
        <td></td>
        <td>Коммерческие расходы</td>
        <td class="text-center">2210</td>
        <td class="text-right bl2">-</td>
        <td class="text-right br2">-</td>
    </tr>
    <tr>
        <td></td>
        <td>Управленческие расходы</td>
        <td class="text-center">2220</td>
        <td class="text-right bl2">-</td>
        <td class="text-right br2">-</td>
    </tr>
    <tr>
        <td></td>
        <td class="pad-l">Прибыль (убыток) от продаж</td>
        <td class="text-center">2200</td>
        <td class="text-right bl2">-</td>
        <td class="text-right br2">-</td>
    </tr>
    <tr>
        <td></td>
        <td>Доходы от участия в других организациях</td>
        <td class="text-center">2310</td>
        <td class="text-right bl2">-</td>
        <td class="text-right br2">-</td>
    </tr>
    <tr>
        <td></td>
        <td>Проценты к получению</td>
        <td class="text-center">2320</td>
        <td class="text-right bl2">-</td>
        <td class="text-right br2">-</td>
    </tr>
    <tr>
        <td></td>
        <td>Проценты к уплате</td>
        <td class="text-center">2330</td>
        <td class="text-right bl2">-</td>
        <td class="text-right br2">-</td>
    </tr>
    <tr>
        <td></td>
        <td>Прочие доходы</td>
        <td class="text-center">2340</td>
        <td class="text-right bl2">-</td>
        <td class="text-right br2">-</td>
    </tr>
    <tr>
        <td></td>
        <td>Прочие расходы</td>
        <td class="text-center">2350</td>
        <td class="text-right bl2">-</td>
        <td class="text-right br2">-</td>
    </tr>
    <tr>
        <td></td>
        <td class="pad-l">Прибыль (убыток) до налогообложения</td>
        <td class="text-center">2300</td>
        <td class="text-right bl2">-</td>
        <td class="text-right br2">-</td>
    </tr>
    <tr>
        <td></td>
        <td>Текущий налог на прибыль</td>
        <td class="text-center">2410</td>
        <td class="text-right bl2">-</td>
        <td class="text-right br2">-</td>
    </tr>
    <tr>
        <td></td>
        <td class="pad-l">в т.ч. постоянные налоговые обязательства(активы)</td>
        <td class="text-center">2421</td>
        <td class="text-right bl2">-</td>
        <td class="text-right br2">-</td>
    </tr>
    <tr>
        <td></td>
        <td>Изменение отложенных налоговых обязательств</td>
        <td class="text-center">2430</td>
        <td class="text-right bl2">-</td>
        <td class="text-right br2">-</td>
    </tr>
    <tr>
        <td></td>
        <td>Изменение отложенных налоговых активов</td>
        <td class="text-center">2450</td>
        <td class="text-right bl2">-</td>
        <td class="text-right br2">-</td>
    </tr>
    <tr>
        <td></td>
        <td class="bb2">Прочее</td>
        <td class="text-center bb2">2460</td>
        <td class="text-right bl2 bb2">-</td>
        <td class="text-right br2 bb2">-</td>
    </tr>
    <tr>
        <td></td>
        <td class="pad-l">Чистая прибыль (убыток)</td>
        <td class="text-center">2400</td>
        <td class="text-right bl2 bb2">-</td>
        <td class="text-right br2 bb2">-</td>
    </tr>
</table>
