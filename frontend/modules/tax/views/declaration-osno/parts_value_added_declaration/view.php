<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 04.04.2019
 * Time: 23:59
 */

use frontend\modules\tax\models\DeclarationOsnoHelper;

/* @var DeclarationOsnoHelper $declarationHelper */
/* @var $this yii\web\View */
/* @var $model frontend\modules\tax\models\TaxDeclaration */

if (!function_exists('getEmptyDottedBoxes')) {
    function getEmptyDottedBoxes($count)
    {
        $str = '';
        for ($i = 0; $i < $count; $i++) {
            $str .= '<td class="dotbox">&nbsp;&nbsp;</td>';
        }

        return $str;
    }
}
?>
<style>
    .preview-tax-tabs {}
    .preview-tax-tabs > .navbar {min-height:0;margin-bottom: 0;float:right;}
    .nav-tax-tabs > li {float:left;position:relative;display:block;}
    .nav-tax-tabs > li.active > a, .nav-tax-tabs > li.active > a:hover, .nav-tax-tabs > li.active > a:focus {background:#4276a4;color:#fff;}
    .nav-tax-tabs > li > a {padding: 5px 10px;}
    .table-preview {width:100%}
    .table-preview td.font-size-11 {font-size:12px;}
    .table-preview td.font-main {font-size:14px;font-weight:bold;padding-bottom:0;}
    .table-preview td.border-bottom {border-bottom:1px solid #000;}
    .table-preview td.tip {padding:0 0 3px 0;}
    .table-preview td.ver-top {vertical-align:top;}
    .table-preview td.dotbox {width:16px; font-size:15pt; border:1px dotted #333;text-align:center;padding:0 2px;}
    .table-preview td.hh {line-height: 20pt;}
    .table-preview td.small-italic {font-style:italic; font-size:8pt;text-align:center;}
    .table-preview tr.middle td {vertical-align:middle;padding:4px 0;}
    .table-preview td.text-uppercase {text-transform: uppercase;}
    .table-preview td.tip {padding: 0 0 3px 0;font-size: 6pt !important;text-align: center;}
    tr.no-border > td {border: none!important;}
    .preview-page {
        border-bottom: 2px dotted #666;
        padding-bottom: 50px;
        margin-bottom: 50px;
    }
</style>
<div class="page-content-in m-size-div container-first-account-table no_min_h pad0"
     style="">
    <div class="col-xs-12 pad5 pre-view-table">
        <div class="col-xs-12 pad3 preview-declaration" style="margin-top: 15px;">
            <div class="col-xs-12 pad0" style="font-size: 12px;">
                <div class="preview-page">
                    <?= $this->render('@frontend/modules/tax/views/declaration/nds/_viewText-page-1', [
                        'model' => $model,
                        'declarationHelper' => $declarationHelper,
                    ]); ?>
                </div>
                <div class="preview-page">
                    <?= $this->render('@frontend/modules/tax/views/declaration/nds/_viewText-page-2', [
                        'model' => $model,
                        'declarationHelper' => $declarationHelper,
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
