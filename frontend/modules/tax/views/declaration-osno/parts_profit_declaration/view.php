<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 01.05.2019
 * Time: 0:33
 */

use frontend\modules\tax\models\DeclarationOsnoHelper;

/* @var DeclarationOsnoHelper $declarationHelper */
/* @var $this yii\web\View */
/* @var $model frontend\modules\tax\models\TaxDeclaration */

if (!function_exists('getEmptyDottedBoxes')) {
    function getEmptyDottedBoxes($count)
    {
        $str = '';
        for ($i = 0; $i < $count; $i++) {
            $str .= '<td class="dotbox">&nbsp;&nbsp;</td>';
        }

        return $str;
    }
}
if (!function_exists('printOrEmpty')) {
    function printOrEmpty($value, $repeat = 11)
    {
        return ($value > 0) ? round($value) : str_repeat("&mdash;", $repeat);
    }
}
?>
<style>
    .preview-tax-tabs {
    }

    .preview-tax-tabs > .navbar {
        min-height: 0;
        margin-bottom: 0;
        float: right;
    }

    .nav-tax-tabs > li {
        float: left;
        position: relative;
        display: block;
    }

    .nav-tax-tabs > li.active > a, .nav-tax-tabs > li.active > a:hover, .nav-tax-tabs > li.active > a:focus {
        background: #4276a4;
        color: #fff;
    }

    .nav-tax-tabs > li > a {
        padding: 5px 10px;
    }

    .table-preview {
        width: 100%
    }

    .table-preview td.font-size-11 {
        font-size: 12px;
    }

    .table-preview td.font-main {
        font-size: 14px;
        font-weight: bold;
        padding-bottom: 0;
    }

    .table-preview td.border-bottom {
        border-bottom: 1px solid #000;
    }

    .table-preview td.tip {
        padding: 0 0 3px 0;
    }

    .table-preview td.ver-top {
        vertical-align: top;
    }

    .table-preview td.dotbox {
        width: 16px;
        font-size: 15pt;
        border: 1px dotted #333;
        text-align: center;
        padding: 0 2px;
    }

    .table-preview td.hh {
        line-height: 20pt;
    }

    .table-preview td.small-italic {
        font-style: italic;
        font-size: 8pt;
        text-align: center;
    }

    .table-preview tr.middle td {
        vertical-align: middle;
        padding: 4px 0;
    }

    .table-preview td.text-uppercase {
        text-transform: uppercase;
    }

    .table-preview td.tip {
        padding: 0 0 3px 0;
        font-size: 6pt !important;
        text-align: center;
    }

    tr.no-border > td {
        border: none !important;
    }

    .preview-page {
        border-bottom: 2px dotted #666;
        padding-bottom: 50px;
        margin-bottom: 50px;
    }
</style>
<div class="page-content-in m-size-div container-first-account-table no_min_h pad0"
     style="">
    <div class="col-xs-12 pad5 pre-view-table">
        <div class="col-xs-12 pad3 preview-declaration" style="margin-top: 15px;">
            <div class="col-xs-12 pad0" style="font-size: 12px;">
                <div class="preview-page">
                    <?= $this->render('@frontend/modules/tax/views/declaration/org/_viewText-page-1', [
                        'model' => $model,
                        'declarationHelper' => $declarationHelper,
                    ]); ?>
                </div>
                <div class="preview-page">
                    <?= $this->render('@frontend/modules/tax/views/declaration/org/_viewText-page-2', [
                        'model' => $model,
                        'declarationHelper' => $declarationHelper,
                        'columnData' => [
                            '010' => $model->company->oktmo,
                            '030' => '18210101011011000110',
                            '040' => 0,
                            '050' => 0,
                            '060' => '18210101011011000110',
                            '070' => 0,
                            '080' => 0
                        ],
                    ]); ?>
                </div>
                <div class="preview-page">
                    <?= $this->render('@frontend/modules/tax/views/declaration/org/_viewText-page-3', [
                        'model' => $model,
                        'declarationHelper' => $declarationHelper,
                        'taxpayerOrganizationCode' => 1,
                        'columnData' => [
                            '010' => 0,
                            '020' => 0,
                            '030' => 0,
                            '040' => 0,
                            '050' => 0,
                            '060' => 0,
                            '070' => 0,
                            '080' => 0,
                            '100' => 0,
                            '110' => 0,
                            '120' => 0,
                            '130' => 0,
                            '140' => '20.000',
                            '150' => '3.00',
                            '160' => '17.000',
                            '170' => 0,
                            '180' => 0,
                            '190' => 0,
                            '200' => 0,
                        ],
                    ]); ?>
                </div>
                <div class="preview-page">
                    <?= $this->render('@frontend/modules/tax/views/declaration/org/_viewText-page-4', [
                        'model' => $model,
                        'declarationHelper' => $declarationHelper,
                        'taxpayerOrganizationCode' => 1,
                        'columnData' => [
                            '210' => 0,
                            '220' => 0,
                            '230' => 0,
                            '240' => 0,
                            '250' => 0,
                            '260' => 0,
                            '265' => 0,
                            '266' => 0,
                            '267' => 0,
                            '270' => 0,
                            '271' => 0,
                            '280' => 0,
                            '281' => 0,
                            '290' => 0,
                            '300' => 0,
                            '310' => 0,
                            '320' => 0,
                            '330' => 0,
                            '340' => 0,
                            '350' => 0,
                            '351' => 0,
                        ],
                    ]); ?>
                </div>
                <div class="preview-page">
                    <?= $this->render('@frontend/modules/tax/views/declaration/org/_viewText-page-5', [
                        'model' => $model,
                        'declarationHelper' => $declarationHelper,
                        'taxpayerOrganizationCode' => 1,
                        'columnData' => [
                            '010' => 0,
                            '011' => 0,
                            '012' => 0,
                            '013' => 0,
                            '014' => 0,
                            '020' => 0,
                            '021' => 0,
                            '022' => 0,
                            '023' => 0,
                            '024' => 0,
                            '027' => 0,
                            '030' => 0,
                            '040' => 0,
                            '100' => 0,
                            '101' => 0,
                            '102' => 0,
                            '103' => 0,
                            '104' => 0,
                            '105' => 0,
                            '106' => 0,
                        ],
                    ]); ?>
                </div>
                <div class="preview-page">
                    <?= $this->render('@frontend/modules/tax/views/declaration/org/_viewText-page-6', [
                        'model' => $model,
                        'declarationHelper' => $declarationHelper,
                        'taxpayerOrganizationCode' => 1,
                        'columnData' => [
                            '010' => 0,
                            '020' => 0,
                            '030' => 0,
                            '040' => 0,
                            '041' => 0,
                            '042' => 0,
                            '043' => 0,
                            '045' => 0,
                            '046' => 0,
                            '047' => 0,
                            '048' => 0,
                            '049' => 0,
                            '050' => 0,
                            '051' => 0,
                            '052' => 0,
                            '053' => 0,
                            '054' => 0,
                            '055' => 0,
                            '059' => 0,
                            '060' => 0,
                            '061' => 0,
                        ],
                    ]); ?>
                </div>
                <div class="preview-page">
                    <?= $this->render('@frontend/modules/tax/views/declaration/org/_viewText-page-7', [
                        'model' => $model,
                        'declarationHelper' => $declarationHelper,
                        'taxpayerOrganizationCode' => 1,
                        'columnData' => [
                            '070' => 0,
                            '071' => 0,
                            '072' => 0,
                            '073' => 0,
                            '080' => 0,
                            '090' => 0,
                            '100' => 0,
                            '110' => 0,
                            '120' => 0,
                            '130' => 0,
                            '131' => 0,
                            '132' => 0,
                            '133' => 0,
                            '134' => 0,
                            '135' => '1',
                            '200' => 0,
                            '201' => 0,
                            '202' => 0,
                            '204' => 0,
                            '205' => 0,
                            '206' => 0,
                        ],
                    ]); ?>
                </div>
                <div class="preview-page">
                    <?= $this->render('@frontend/modules/tax/views/declaration/org/_viewText-page-8', [
                        'model' => $model,
                        'declarationHelper' => $declarationHelper,
                        'taxpayerOrganizationCode' => 1,
                        'columnData' => [
                            '300' => 0,
                            '301' => 0,
                            '302' => 0,
                            '400' => 0,
                            '401' => ['year' => '', 'sum' => 0],
                            '402' => ['year' => '', 'sum' => 0],
                            '403' => ['year' => '', 'sum' => 0],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
