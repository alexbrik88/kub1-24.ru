<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\components\grid\GridView;
use common\components\grid\DropDownSearchDataColumn;
use common\models\employee\Employee;
use common\models\company\CompanyType;
use common\components\TextHelper;
use \frontend\modules\tax\models\DeclarationOsnoHelper;
use frontend\rbac\UserRole;

/* @var $company \common\models\Company */

$this->title = 'Нулевая отчетность в налоговую и ПФР';
$emptyMessage = 'У вас пока не создано ни одной отчетности.';

$canDelete = Yii::$app->user->can(UserRole::ROLE_CHIEF);
$canPrint = Yii::$app->user->can(UserRole::ROLE_CHIEF);
?>
<div class="portlet box">
    <div class="btn-group pull-right">
        <?= Html::a('<i class="fa fa-plus"></i> ДОБАВИТЬ', Url::to(['/tax/declaration-osno/index']), [
            'class' => 'btn yellow',
        ]); ?>
    </div>
    <h3 class="page-title"><?= $this->title; ?></h3>
</div>

<div class="portlet box darkblue">
    <div class="portlet-title row-fluid">
        <div class="caption col-md-4">
            Список отчетности: <?= $dataProvider->totalCount ?>
        </div>
        <div class="tools search-tools tools_button col-md-4">
            <div class="form-body">
                <?php $form = \yii\widgets\ActiveForm::begin([
                    'method' => 'GET',
                    'action' => ['index-list'],
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'options' => [
                            'class' => '',
                        ],
                    ],
                ]); ?>
                <div class="search_cont">
                    <div class="wimax_input">
                        <?= $form->field($searchModel, 'find_by')->textInput([
                            'placeholder' => 'Отчетный год',
                        ]); ?>
                    </div>
                    <div class="wimax_button">
                        <?= Html::submitButton('НАЙТИ', [
                            'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                        ]) ?>
                    </div>
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
        <div class="actions joint-operations col-md-4" style="display:none;">
            <?php if ($canDelete) : ?>
                <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                    'class' => 'btn btn-default btn-sm hidden-md hidden-sm hidden-xs',
                    'data-toggle' => 'modal',
                ]); ?>
                <?= Html::a('<i class="glyphicon glyphicon-trash"></i>', '#many-delete', [
                    'class' => 'btn btn-default btn-sm hidden-lg',
                    'data-toggle' => 'modal',
                ]); ?>
                <!--Delete modal-->
                <div id="many-delete" class="confirm-modal fade modal" role="dialog"
                     tabindex="-1" aria-hidden="true"
                     style="display: none; margin-top: -51.5px;">
                    <div class="modal-dialog ">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="form-body">
                                    <div class="row">Вы уверены, что хотите удалить
                                        выбранную отчетность?
                                    </div>
                                </div>
                                <div class="form-actions row">
                                    <div class="col-xs-6">
                                        <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
                                            'class' => 'btn darkblue pull-right modal-many-delete ladda-button',
                                            'data-url' => Url::to(['many-delete']),
                                            'data-style' => 'expand-right',
                                        ]); ?>
                                    </div>
                                    <div class="col-xs-6">
                                        <button type="button" class="btn darkblue"
                                                data-dismiss="modal">НЕТ
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif ?>
            <?php if ($canPrint) : ?>
                <?= Html::a('<i class="fa fa-print"></i> Печать', [
                    'many-document-print',
                    'actionType' => 'pdf',
                    'multiple' => '',
                ], [
                    'class' => 'btn btn-default btn-sm multiple-print hidden-md hidden-sm hidden-xs',
                    'target' => '_blank',
                ]); ?>
                <?= Html::a('<i class="fa fa-print"></i>', [
                    'many-document-print',
                    'actionType' => 'pdf',
                    'multiple' => '',
                ], [
                    'class' => 'btn btn-default btn-sm multiple-print hidden-lg',
                    'target' => '_blank',
                ]); ?>
            <?php endif ?>
        </div>
    </div>

    <div class="portlet-body">
        <div class="table-container">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'emptyText' => $emptyMessage,
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable documents_table status_nowrap agreements-templates-table fix-thead',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],
                'rowOptions'   => function ($data, $key, $index, $grid) {
                    return [
                        'data' => ['key' => $data['id']],
                    ];
                },
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],

                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount, 'scroll' => true]),
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                'columns' => [
                    [

                        'header' => Html::checkbox('', false, [
                            'class' => 'joint-operation-main-checkbox',
                        ]),
                        'headerOptions' => [
                            'class' => 'text-center pad0',
                            'width' => '5%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-center pad0-l pad0-r',
                        ],
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Html::checkbox('TaxDeclaration[' . $data['id'] . '][checked]', false, [
                                'class' => 'joint-operation-checkbox',
                            ]);
                        },
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'period_name_id',
                        'label' => 'Отчетный период',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '12%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-right'
                        ],
                        'format' => 'raw',
                        'value' => function ($data) {
                            $monthName = \yii\helpers\ArrayHelper::getValue(\common\components\helpers\Month::$monthFullRU, $data['tax_month']);
                            $period =
                                ($data['szvm']) ? ($monthName.' '.$data['tax_year'].' г.') :
                                    ($data['balance'] ? ($data['tax_year'].' г.') :
                                        ($data['tax_quarter'].'-й квартал '.$data['tax_year'].' г.'));

                            return $period;
                        },
                        'filter' => $searchModel->getPeriodFilterNames()
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'declaration_name_id',
                        'label' => 'Название отчета',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '20%',
                        ],
                        'value' => function ($data) use ($company) {
                            $name = ($data['nds'] ? 'Декларация на добавленную стоимость' :
                                      ($data['org'] ? 'Декларация на прибыль организации' :
                                        ($data['balance'] ? 'Бухгалтерский баланс' :
                                          ($data['szvm'] ? 'Сведения о застрахованных лицах' :
                                            'Единая упрощенная декларация'))));

                            return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $data]) ?
                                Html::a($name, ['/tax/declaration/view', 'id' => $data['id']]) :
                                $name;
                        },
                        'format' => 'raw',
                        'filter' => [
                            0 => 'Все',
                            DeclarationOsnoHelper::TYPE_SINGLE => 'Единая упрощенная декларация',
                            DeclarationOsnoHelper::TYPE_NDS => 'Декларация на добавленную стоимость',
                            DeclarationOsnoHelper::TYPE_ORG => 'Декларация на прибыль организации',
                            DeclarationOsnoHelper::TYPE_BALANCE => 'Бухгалтерский баланс',
                            DeclarationOsnoHelper::TYPE_SZVM => 'Сведения о застрахованных лицах'
                        ]
                    ],
                    [
                        'attribute' => 'document_correction_number',
                        'label' => '№ корректировки',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '8%',
                        ],
                    ],
                    [
                        'attribute' => 'created_at',
                        'label' => 'Дата создания',
                        'headerOptions' => ['style' => 'width:10%;'],
                        'format' => ['date', 'php:d.m.Y'],
                    ],
                    [
                        'attribute' => 'status',
                        'label' => 'Статус',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-left text-ellipsis',
                        ],
                        'class' => DropDownSearchDataColumn::className(),
                        'value' => function ($data) {
                            return \common\models\document\status\TaxDeclarationStatus::findOne(['id'=>$data['status_id']])->name;
                        },
                        'format' => 'raw',
                        'filter' => [
                            '' => 'Все',
                            1 => 'Создана',
                            3 => 'Принята',
                            4 => 'Передана',
                            5 => 'Скорректирована'
                        ]
                    ],
                    [
                        'attribute' => 'document_author_id',
                        'label' => 'Ответственный',
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-left text-ellipsis',
                        ],
                        'class' => DropDownSearchDataColumn::className(),
                        'value' => function ($data) {
                            $employee = \common\models\EmployeeCompany::findOne([
                                'employee_id' => $data['document_author_id'],
                                'company_id' => $data['company_id']
                            ]);

                            return (!empty($employee)) ? $employee->getShortFio() : '';
                        },
                        'format' => 'raw',
                        'filter' => $searchModel->getCreators()
                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>