<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 04.05.2019
 * Time: 0:10
 */

use common\components\date\DateHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use frontend\rbac\permissions\document\Document;
use frontend\modules\tax\models\DeclarationOsnoHelper;

/* @var $this yii\web\View */
/* @var $model frontend\modules\tax\models\TaxDeclaration; */
/* @var DeclarationOsnoHelper $declarationHelper */

$changeStatusDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->created_at,
]);
$status = $model->status;
$styleClass = $status->getStyleClass();
$iconClass = $status->getIcon();
$company = $model->company;
$ifns = $company->ifns;
$canUpdate = Yii::$app->user->can(Document::UPDATE_STATUS, [
    'model' => $model,
]);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
    ],
]);
?>
    <div class="control-panel col-xs-12 pad0" style="margin-top:-3px">
        <div class="col-xs-12 col-sm-3 pad3">
            <div class="btn full_w marg <?= $styleClass; ?>"
                 style="padding-left:0px; padding-right:0px;text-align: center;"
                 title="Дата изменения статуса">
                <?= date(DateHelper::FORMAT_USER_DATE, $model->status_updated_at ?: $model->created_at) ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-9 pad0">
            <div class="col-xs-12 pad3">
                <div class="btn full_w marg <?= $styleClass; ?>" title="Статус">
                    <i class="pull-left icon <?= $iconClass; ?>"></i>
                    <?= $model->status->name ?>
                </div>
            </div>
        </div>
    </div>
    <style>
        .main_inf_no-bord td {
            border: none !important;
        }
    </style>
    <div class="col-xs-12 col-lg-9 pull-right pad3" style="max-width: 720px;">
        <div class="portlet">
            <div class="customer-info bord-dark" style="margin:10px 0">
                <div class="portlet-body no_mrg_bottom main_inf_no-bord" style="padding-bottom:2px">
                    <table class="table no_mrg_bottom">
                        <tbody>
                        <tr>
                            <td>
                                <span class="customer-characteristic">Сведения о застрахованных лицах за</span>
                                <?= $declarationHelper->szvmPeriodLabel; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="customer-characteristic" style="display:inline-block;vertical-align: top;">
                                    Дата декларации:
                                </div>
                                <div id="date_internal_view" class="tooltip2"
                                     data-tooltip-content="#document_date_datepicker" style="display:inline-block">
                                    <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?>
                                </div>
                                <?php if ($canUpdate) : ?>
                                    <?= Html::tag('span', '', [
                                        'id' => 'date_internal_update',
                                        'class' => 'glyphicon glyphicon-pencil',
                                        'style' => 'cursor: pointer; display:inline-block; color:#5b9bd1',
                                    ]); ?>
                                    <?= Html::activeHiddenInput($model, 'document_date', [
                                        'id' => 'date_internal_input',
                                        'value' => DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                                        'data-url' => Url::to(['change-declaration-date', 'id' => $model->id]),
                                    ]); ?>
                                <?php endif ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="portlet" style="margin-top: -10px">
            <div class="customer-info" style="border:none">
                <div class=" no_mrg_bottom main_inf_no-bord">
                    <table class="table no_mrg_bottom">
                        <tr>
                            <td>
                                <span class="customer-characteristic">
                                    Способы сдать Сведения о застрахованных лицах
                                </span><br/><br/>
                                <span class="customer-characteristic">
                                    1. Отправить по почте
                                </span><br/>
                                Для этого <a target="_blank" data-pjax="0" href="/tax/declaration-osno/declaration-print/?actionType=pdf&szvm=1&id=<?=($model->id)?>filename=<?=($model->getPrintTitle().'.pdf')?>">распечатайте сведения</a> в 1-м экземпляре
                                и <a target="_blank" data-pjax="0" href="/tax/declaration-osno/inventory-print/?actionType=pdf&szvm=1&id=<?=($model->id)?>&filename=<?=('Опись_'.$model->getPrintTitle().'.pdf')?>">опись вложения</a> в 2-х экземплярах, подпишите их,
                                поставьте на титульном листе сведений
                                печать (если она у вас есть).
                                Отправьте отчёт ценным письмом с описью вложения
                                на адрес инспекции.
                                У вас на руках должен остаться экземпляр описи с отметкой
                                работника почты, который подтвердит сдачу отчёта.
                                <br/><br/>
                                <span class="customer-characteristic">
                                    2. Отправить через электронную систему сдачи отчетности</span>
                                <br/>
                                Для этого скачайте сведения в формате для электронной отчетности, загрузите её в вашу систему и подпишите вашей ЭЦП.
                                <br/><br/>
                                <span class="customer-characteristic">2. Отнести отчёт в инспекцию самостоятельно</span><br/>
                                Для этого <a target="_blank" data-pjax="0" href="/tax/declaration-osno/declaration-print/?actionType=pdf&szvm=1&id=<?=($model->id)?>&filename=<?=($model->getPrintTitle().'.pdf')?>">распечатайте сведения</a> в 2-х экземплярах,
                                подпишите каждый, на титульном листе поставьте
                                печать (если она у вас есть), и отнесите в инспекцию.
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="tooltipster_templates" style="display: none;">
        <div id="document_date_datepicker">
            <?= Html::tag('div', null, ['class' => 'datepicker']); ?>
            <?= Html::tag('i', '', [
                'id' => 'date_internal_cancel',
                'class' => 'fa fa-reply pull-right',
                'style' => 'cursor: pointer; font-size: 20px; color:#5b9bd1; margin-top:10px',
            ]); ?>
            <?= Html::tag('i', '', [
                'id' => 'date_internal_save',
                'class' => 'fa fa-floppy-o pull-left',
                'style' => 'cursor: pointer; font-size: 20px; color:#5b9bd1; margin-top:10px',
            ]); ?>
        </div>
    </div>
<?php if ($canUpdate) {
    $this->registerJs('
        $(document).ready(function() {
            $(function() {
                var dateDiv = $("#document_date_datepicker > .datepicker");
                $(dateDiv).datepicker({
                    keyboardNavigation: false,
                    forceParse: false,
                    language: "ru",
                    autoclose: false
                });
                $(dateDiv).datepicker("setDate", $("#date_internal_input").val());
            });
            $( "#document_date_datepicker > .datepicker" ).on("click", function(e) {
                e.stopPropagation();
                var jsDate = $(this).datepicker("getDate");
                if (jsDate !== null && jsDate instanceof Date) {
                    var day = (jsDate.getDate() > 9) ? jsDate.getDate() : "0" + jsDate.getDate();
                    var month = ((jsDate.getMonth() + 1) > 9) ? (jsDate.getMonth() + 1) : ("0" + (jsDate.getMonth() + 1));
                    var year = jsDate.getFullYear();
                    $("#date_internal_input").val(day + "." + month + "." + year);
                }
            });
        });
        $(document).on("click", "#date_internal_update", function () {
            $(".tooltip2").tooltipster("open");
        });
        $(document).on("click", "#date_internal_save", function () {
            $.post($("#date_internal_input").data("url"), {newDate: $("#date_internal_input").val()}, function (data) {
                if (data.value) {
                    $("#date_internal_view").text(data.value);
                    $(".document-date-js").text(data.value);
                } else {
                    window.toastr.error("Не удалось сохранить дату.", "", {
                        "closeButton": true,
                        "showDuration": 1000,
                        "hideDuration": 1000,
                        "timeOut": 5000,
                        "extendedTimeOut": 1000,
                        "escapeHtml": false,
                    });
                }
                $(".tooltip2").tooltipster("close");
            })
        });
        $(document).on("click", "#date_internal_cancel", function () {
            $(".tooltip2").tooltipster("close");
        });
    ');
}