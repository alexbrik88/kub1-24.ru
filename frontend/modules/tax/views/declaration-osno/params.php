<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 26.03.2019
 * Time: 20:38
 */

use common\models\Company;
use common\models\company\CheckingAccountant;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use \php_rutils\RUtils;
use yii\helpers\Html;
use common\models\company\CompanyType;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use common\components\date\DateHelper;

/* @var $this yii\web\View */
/* @var $model Company */
/* @var $account CheckingAccountant */

$this->title = 'Параметры вашего ООО';

$taxation = $model->companyTaxationType;

if (empty($accounts)) {
    $accounts_title = 'Нет расчетных счетов в банках';
} else {
    $accounts_title = [];
    foreach ($accounts as $value) {
        $accounts_title[] =
            $value['cnt'] . ' ' .
            RUtils::numeral()->choosePlural($value['cnt'], ['расчетный счет', 'расчетных счета', 'расчетных счетов']) .
            ' в ' . $value['bank_name'];
    }
    $accounts_title = implode(', ', $accounts_title);
}
$questions = [
    'no_operations_rs' => 'Нет операций по расчетному счету',
    'no_operations_order' => 'Нет операций по кассе',
    'no_salary' => 'Нет сотрудников и Нет начислений и выплат ЗП',
    'no_property' => 'Нет имущества',
    'no_patent_and_envd' => 'Нет Патента или ЕНВД',
];
$ofds = [
    'Ofd[taxcom]' => 'Такском',
    'Ofd[2]' => 'Платформа ОФД',
    'Ofd[3]' => 'Первый ОФД',
    'Ofd[4]' => 'Ярус',
    'Ofd[5]' => 'Петер-Сервис Спецтехнологии (OFD.RU)',
    'Ofd[6]' => 'Тензор',
    'Ofd[7]' => 'СКБ Контур',
    'Ofd[8]' => 'Тандер',
    'Ofd[9]' => 'Калуга Астрал',
    'Ofd[10]' => 'Яндекс.ОФД',
    'Ofd[11]' => 'ЭнвижнГруп',
    'Ofd[12]' => 'Вымпел-Коммуникации',
    'Ofd[13]' => 'Мультикарта',
];

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'contentCloning' => true,
    ],
]);
?>
<?php
$no_operations = [
    'from_registration' => 'НЕ было операций с начала регистрации ООО, т.е. с ' .
        DateHelper::format($model->tax_authority_registration_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE).' г.',
    'now' => 'БЫЛИ операции, но сейчас нет операций'
];
$selected_no_operations = ($cookie_no_operations = Yii::$app->request->cookies['ooo_no_operations']) ? $cookie_no_operations->value : null;

$questions_messages['no_salary'] = '    
    <div id="no_salary_message" style="display:none">
        <p>Нулевую отчетность нельзя сдавать, если у вас производится:</p>
        <p>1) начисление и/или выплата ЗП работникам с расчётного счёта и/или из кассы.</p>
        <p>2) начисление и/или выплаты по договорам подряда и иным договорам гражданско-правовым характерам (ГПХ) в пользу любых физ.лиц, в том числе не являющимися сотрудниками предприятия.</p>
        <p>Напишите нам на почту <a href="mailto:support@kub-24.ru">support@kub-24.ru</a> и мы порекомендуем вам одного из наших партнеров – проверенную бухгалтерскую компанию. Они рассчитают вам стоимость подготовки декларации и расчета налогов с учётом наличия у вас ЗП. От нас вы получите скидку 20% на их услуги.</p>
        <p><a href="/tax/robot/show-cub-features">Посмотреть все возможности КУБа</a></p>
    </div>';
$questions_messages['no_patent_and_envd'] = '    
    <div id="no_patent_and_envd_message" style="display:none">
        <p>Если у вас есть действующий патент или организация состоит на учёте как плательщик ЕНВД то подготовка нулевой декларации вам не подойдет.</p>
        <p>Напишите нам на почту <a href="mailto:support@kub-24.ru">support@kub-24.ru</a> и мы порекомендуем вам одного из наших партнеров – проверенную бухгалтерскую компанию. Они рассчитают вам стоимость подготовки декларации и расчета налогов с учётом наличия у вас действующего патента или организации, состоящей на учёте как плательщик ЕНВД. От нас вы получите скидку 20% на их услуги.</p>
        <p><a href="/tax/robot/show-cub-features">Посмотреть все возможности КУБа</a></p>
    </div>';
$questions_messages['no_operations_rs'] = '    
    <div id="no_operations_rs_message" style="display:none">
        <p>Если у вас есть операции по расчетному счету, то вы не можете сдавать нулевую отчетность.</p>
        <p>Напишите нам на почту <a href="mailto:support@kub-24.ru">support@kub-24.ru</a> и мы порекомендуем вам одного из наших партнеров – проверенную бухгалтерскую компанию. Они рассчитают вам стоимость подготовки декларации и расчета налогов с учётом наличия у вас операций по расчетным счетам. От нас вы получите скидку 20% на их услуги.</p>
        <p><a href="/tax/robot/show-cub-features">Посмотреть все возможности КУБа</a></p>
    </div>';
$questions_messages['no_operations_order'] = '
    <div id="no_operations_order_message" style="display:none">
        <p>Если у вас есть операции по кассе, то вам нельзя сдавать нулевую налоговую отчетность.</p>
        <p>Напишите нам на почту <a href="mailto:support@kub-24.ru">support@kub-24.ru</a> и мы порекомендуем вам одного из наших партнеров – проверенную бухгалтерскую компанию. Они рассчитают вам стоимость подготовки декларации и расчета налогов с учётом наличия у вас операций по кассе. От нас вы получите скидку 20% на их услуги.</p>
        <p><a href="/tax/robot/show-cub-features">Посмотреть все возможности КУБа</a></p>
    </div>';
$questions_messages['no_property'] = '
    <div id="no_property_message" style="display:none">
        <p>Если у вас есть имущество, то вам нельзя сдавать нулевую налоговую отчетность.</p>
        <p>Напишите нам на почту <a href="mailto:support@kub-24.ru">support@kub-24.ru</a> и мы порекомендуем вам одного из наших партнеров – проверенную бухгалтерскую компанию. Они рассчитают вам стоимость подготовки декларации и расчета налогов с учётом наличия у вас имущества. От нас вы получите скидку 20% на их услуги.</p>
        <p><a href="/tax/robot/show-cub-features">Посмотреть все возможности КУБа</a></p>        
    </div>';
?>
<?= $this->render('_style'); ?>
<?php $form = ActiveForm::begin([
    'id' => 'form-ip-params',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldClass' => 'common\components\floatLabelField\FloatLabelActiveField',
    'fieldConfig' => [
        'template' => "{input}\n{label}\n{error}\n",
    ],
    'options' => [
        'class' => 'form-md-underline'
    ]
]); ?>
    <div class="row">
        <div class="hidden-xs hidden-sm hidden-md col-xs-12 step-by-step">
            <?= $this->render('_steps', ['step' => 2, 'company' => $model]); ?>
            <div class="pad_sm_t" style="padding-top: 10px">
                <div class="col-xs-12 col-lg-12 pad0">
                    <div class="col-xs-12 pad0 step-by-step-form" style="min-height: 735px;">
                        <div class="col-xs-12 pad0 bord-light-b">
                        <span class="marg pad10-b font-bold"
                              style="font-size:16px">Шаг 2: Укажите параметры вашего ООО</span>
                        </div>
                        <div class="row">
                            <div class="col-xs-12" style="margin-top: 25px;margin-bottom: 10px;">
                                <span class="marg pad10-b font-bold"
                                      style="font-size:16px">Система налогообложения</span>
                            </div>
                            <div class="col-xs-12 mb5 taxation-system">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12">
                                        <?= $form->field($taxation, 'osno', [
                                            'template' => "{label}\n{input}",
                                            'options' => [
                                                'class' => 'form-group form-md-line-input form-md-floating-label tooltip2-hover',
                                                'style' => 'display: inline-block; margin-right:25px;',
                                                'data-tooltip-content' => '#tooltip_osno'
                                            ],
                                            'labelOptions' => [
                                                'class' => 'control-label',
                                            ]
                                        ])->checkbox() ?>
                                        <?= $form->field($taxation, 'usn', [
                                            'template' => "{label}\n{input}",
                                            'options' => [
                                                'class' => 'form-group form-md-line-input form-md-floating-label tooltip3-hover',
                                                'style' => 'display: inline-block; margin-right:25px;',
                                                'data-tooltip-content' => '#tooltip_osno_only'
                                            ],
                                            'labelOptions' => [
                                                'class' => 'control-label',
                                            ]
                                        ])->checkbox(['disabled' => true]) ?>
                                        <?= $form->field($taxation, 'envd', [
                                            'template' => "{label}\n{input}",
                                            'options' => [
                                                'class' => 'form-group form-md-line-input form-md-floating-label tooltip3-hover',
                                                'style' => 'display: inline-block; margin-right:25px;',
                                                'data-tooltip-content' => '#tooltip_osno_only'
                                            ],
                                            'labelOptions' => [
                                                'class' => 'control-label',
                                            ]
                                        ])->checkbox(['disabled' => true]) ?>
                                        <?= $form->field($taxation, 'psn', [
                                            'template' => "{label}\n{input}",
                                            'options' => [
                                                'class' => 'form-group form-md-line-input form-md-floating-label tooltip3-hover',
                                                'style' => 'display: inline-block; margin-right:25px;',
                                                'data-tooltip-content' => '#tooltip_osno_only'
                                            ],
                                            'labelOptions' => [
                                                'class' => 'control-label',
                                            ]
                                        ])->checkbox(['disabled' => true]) ?>
                                        <style>.form-md-underline .select2-container .select2-selection--single {
                                                height: 28px !important;
                                            }</style>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div id="message-robot-enabled"
                                 style="display:<?= (!$taxation->usn || ($taxation->usn && $taxation->usn_type == 0)) ? 'block' : 'none' ?>">
                                <div class="col-xs-12 col-md-12 form-group">
                                    <div class="marg pad10-b font-bold" style="font-size:16px;padding-bottom:15px">
                                        Для правильной подготовки нулевой отчетности, подтвердите, что у вашего ООО
                                        <span class="ooo_report_period" style="display:none"> за отчетный период</span>
                                    </div>

                                    <div class="mt-radio-list">
                                        <?php foreach ($no_operations as $key => $value) : ?>
                                            <label class="mt-radio mt-radio-outline" style="margin-bottom:15px;display: block">
                                                <input type="radio" name="ooo_no_operations" class="ooo_no_operations" value="<?=($key)?>" <?=($selected_no_operations == $key) ? 'checked=""':'' ?>>
                                                <strong style="color:#171717;font-weight:bold;padding-left:5px;cursor:pointer;"><?=$value?></strong>
                                                <span></span>
                                            </label>
                                        <?php endforeach; ?>
                                    </div>

                                    <div id="ooo_questions_wrapper" style="display:none;">
                                        <?php foreach ($questions as $key => $value) : ?>
                                            <div class="form-md-line-input form-md-floating-label"
                                                 style="margin-bottom: 10px;">
                                                <span class="md-checkbox">
                                                    <input type="checkbox" id="<?= ($key) ?>_checkbox"
                                                           data-message="<?= ($key) ?>_message"
                                                           class="md-check questions-checkboxes"
                                                           name="<?= ($key) ?>_checkbox" value="1" checked>
                                                    <label for="<?= ($key) ?>_checkbox"
                                                           style="color:#171717;font-weight:bold; position:relative">
                                                        <span class="inc"></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                        <?= ($value) ?>
                                                    </label>
                                                </span>
                                                <div style="padding-left:30px"><?= isset($questions_messages[$key]) ? $questions_messages[$key] : ''; ?></div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix" style="padding-bottom:30px"></div>
                    <div id="buttons-bar-fixed">
                        <div class="col-xs-12 pad0 buttons-block">
                            <?= Html::a('Назад', ['company'], [
                                'class' => 'btn darkblue',
                                'style' => 'min-width: 180px;'
                            ]) ?>
                            <?php if ($isPaid): ?>
                                <div class="button-pulsate pull-right" style="max-width: 180px">
                                    <?= Html::submitButton('Подтвердить', [
                                        'id' => 'ip-params-submit',
                                        'class' => 'btn darkblue ',
                                        'disabled' => ($selected_no_operations) ? false : true
                                    ]); ?>
                                </div>
                            <?php else: ?>
                                <div class="button-pulsate pull-right" style="max-width: 180px">
                                    <?= Html::button('Подтвердить', [
                                        'id' => 'ip-params-submit',
                                        'class' => 'btn darkblue taxrobot-pay-panel-trigger',
                                        'disabled' => ($selected_no_operations) ? false : true
                                    ]); ?>
                                </div>
                            <?php endif; ?>
                            <div class="pull-right" style="font-size:13px;margin-right:15px">
                                Нажимая на кнопку «Подтвердить», вы подтверждаете<br/>
                                данные по вашему ООО для подготовки отчетности.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $form->end(); ?>
<div style="display:none;">
    <div id="tooltip_osno_only">Отчетность доступна только для ООО на ОСНО</div>
</div>

<?= $this->render('_pay_modal', [
    'company' => $model,
]); ?>

<?php if (!$isPaid):
    $this->registerJs("$('.sbs-step-3').addClass('taxrobot-pay-panel-trigger');");
endif; ?>

<?php $this->registerJs(<<<JS
    $(document).ready(function() {
        var activeRadio = $('.ooo_no_operations:checked');
        var questionsBlock = $('#ooo_questions_wrapper'); 
        if ($(activeRadio).length) {
            $(activeRadio).parents('.mt-radio').after(questionsBlock);
            $(questionsBlock).show();
        }
    })    
    $('.ooo_no_operations').change(function() {       
        var activeRadio = $('.ooo_no_operations:checked');
        var questionsBlock = $('#ooo_questions_wrapper');
        $(activeRadio).parents('.mt-radio').after(questionsBlock);
        $(questionsBlock).show();
        if ($(this).val() == 'from_registration') {
            $('.ooo_report_period').hide();
        } else { 
            $('.ooo_report_period').show();
        }
        if ($('.questions-checkboxes:not(:checked)').length) {
            $('#ip-params-submit').attr('disabled', 'disabled');
        } else {
            $('#ip-params-submit').removeAttr('disabled', '');
        }        
    });
    $('.questions-checkboxes').change(function() {
       var msg_id = $(this).attr('data-message'); 
       if ($(this).prop('checked'))
           $('#'+msg_id).hide(250);
       else
           $('#'+msg_id).show(250);
       
       var disabled_next_steps = false;
       $('.questions-checkboxes').each(function() {
           if (!$(this).prop('checked') && !($(this).attr('name') == 'no_cashbox_checkbox')) {
               disabled_next_steps = true;
               return false;
           }
       });
       if (disabled_next_steps) {
           $('#ip-params-submit').attr('disabled', 'disabled');
           $('.sbs-step-3').addClass('disabled');
       } else {
           $('#ip-params-submit').removeAttr('disabled');
           $('.sbs-step-3').removeClass('disabled');
       }
       
    });
    $(document).on("click", ".sbs-el", function(e) {
          e.preventDefault();
          if ($(this).hasClass('disabled') || $('#ip-params-submit').prop("disabled") || $(this).hasClass('taxrobot-pay-panel-trigger'))
            return false;
          else
            window.location.href = $(this).attr('href');        
    });
    //$(document).ready(function() {
    //    $("#ip-params-submit").parents(".button-pulsate").pulsate({
    //        color: "#bf1c56",
    //        reach: 20,
    //        repeat: 3
    //    }); 
    //});
JS
) ?>