<?php

use backend\models\Bank;
use common\components\widgets\BikTypeahead;
use common\components\widgets\AddressTypeahead;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\dictionary\address\AddressDictionary;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\company\CompanyType;
use common\components\date\DateHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;

$typeArray = ['' => ''] + CompanyType::find()->inContractor()
    ->select(['name_short', 'id'])
    ->orderBy([
        'IF([[id]] < 5, [[id]], 5)' => SORT_ASC,
        'name_short' => SORT_ASC,
    ])
    ->indexBy('id')
    ->column();
?>
<?= Html::activeHiddenInput($model, 'name_short'); ?>
<?= Html::activeHiddenInput($model, 'ogrn'); ?>
<div class="col-xs-6">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'inn')->textInput([
                'maxlength' => true,
            ])->label('<span class="is-empty">Автозаполнение по </span>ИНН') ?>
        </div>
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'kpp')->textInput([
                'maxlength' => true,
            ]) ?>

        </div>
    </div>
</div>

<div class="col-xs-6">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'taxRegistrationDate')->textInput([
                'maxlength' => true,
                'class' => 'form-control input-sm date-picker' . ($model->taxRegistrationDate ? ' edited' : ''),
                'autocomplete' => 'off',
                'value' => $model->taxRegistrationDate,
                'disabled' => !empty($model->taxRegistrationDate),
            ])->label('Дата регистрации ООО') ?>
            <?= Html::tag('i', '', [
                'id' => 'show-taxregistrationdate',
                'class' => 'fa fa-calendar',
                'style' => 'position: absolute; top: 30px; right: 15px; color: #cecece; cursor: pointer;',
            ]) ?>
        </div>
        <div class="col-xs-12 col-md-6">

        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="col-xs-9">
    <div class="row">
        <div class="col-xs-12 col-md-4">

            <?= $form->field($model, 'company_type_id')->dropDownList($typeArray)->label('Форма собственности'); ?>

        </div>
        <div class="col-xs-12 col-md-8">

            <?= $form->field($model, 'name_full')->label('Название')->textInput(); ?>

        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="col-xs-6">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <?= $form->field($model, 'address_legal')->label('Юридический адрес')->textInput(); ?>
        </div>
    </div>
</div>
<div class="col-xs-6 mb5">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'okved')->label('ОКВЭД')->textInput(); ?>
        </div>
        <div class="col-xs-12 col-md-6">
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="col-xs-6 mb5">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'ifns_ga')->label('Код ИФНС')->textInput(); ?>
            <span class="tooltip2-click valign-middle ico-question sbs-tooltip"
                  data-tooltip-content="#tooltip_what_is_ifns" style="right:-7px; top:23px;"></span>
        </div>
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'oktmo')->label('Код ОКТМО')->textInput(); ?>
            <span class="tooltip2-click valign-middle ico-question sbs-tooltip"
                  data-tooltip-content="#tooltip_what_is_oktmo" style="right:-7px; top:23px;"></span>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="col-xs-12 caption">
    <span>Руководитель</span>
</div>

<div class="col-xs-6">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'chief_post_name')->label('Должность')->textInput(); ?>
        </div>
        <div class="col-xs-12 col-md-6">
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="col-xs-6">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'chief_lastname')->label('Фамилия')->textInput(); ?>
        </div>
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'chief_firstname')->label('Имя')->textInput(); ?>
        </div>
    </div>
</div>

<div class="col-xs-6">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'chief_patronymic')->label('Отчество')->textInput(); ?>
        </div>
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'has_chief_patronymic', [
                'template' => "{label}\n{input}",
                'options' => [
                    'class' => 'form-group form-md-line-input form-md-floating-label',
                    'style' => 'display: inline-block;',
                ],
                'labelOptions' => [
                    'class' => 'control-label',
                    'style' => 'padding: 6px 0; margin-right: 9px;',
                ]
            ])->checkbox([], true) ?>
        </div>
    </div>
</div>

<div class="clearfix"></div>
