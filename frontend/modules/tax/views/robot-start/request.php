<?php

use frontend\modules\tax\models\TaxrobotStartForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $model frontend\modules\tax\models\TaxrobotStartForm */

$taxName = $model->company->companyTaxationType->name;
?>

<div class="taxrobot-start-header">
    <h4>
        СПАСИБО, ваша заявка принята!
    </h4>
</div>

<div class="taxrobot-start-content">
    <div>
        В течение 15-20 минут с вами свяжется менеджер
    </div>

    <h3>
        <strong>Пока ждете звонка,</strong>
    </h3>

    <div>
        Посмотрите, чем наш сервис КУБ-24 может быть вам полезен
    </div>

    <ul>
        <li>
            Выставление счетов, актов, товарных накладных, УПД и счетов-фактур
        </li>
        <li>
            Контроль должников
        </li>
        <li>
            Управленческие отчеты, которые покажут где ваша прибыль
        </li>
    </ul>

    <div class="mt-5">
        <?= Html::a('Подробнее', ['/site/index'], [
            'class' => 'button-regular button-hover-transparent ladda-button not-ajax min-w-130',
        ]) ?>
    </div>
</div>
