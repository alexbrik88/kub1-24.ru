<?php

use common\models\company\EmployeeCount;
use frontend\modules\tax\models\TaxrobotStartForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $model frontend\modules\tax\models\TaxrobotStartForm */

$items = EmployeeCount::find()->select('name')->indexBy('id')->column();
?>

<div class="taxrobot-start-header">
    <h4>
        Сколько у Вас сотрудников?
    </h4>
</div>

<div class="taxrobot-start-content">
    <div class="row">
        <?php foreach ($items as $key => $value) : ?>
            <?php $form = ActiveForm::begin([
                'id' => 'staff-form-'.$key,
                'action' => ['staff'],
                'options' => [
                    'class' => 'items-form',
                ],
            ]); ?>

                <?= Html::activeHiddenInput($model, 'employee_count_id', [
                    'id' => Html::getInputId($model, 'employee_count_id')."_.$key",
                    'value' => $key,
                ]) ?>

                <?= Html::submitButton($value, [
                    'class' => 'button-regular button-hover-transparent min-w-130',
                ]) ?>

            <?php ActiveForm::end() ?>
        <?php endforeach ?>
    </div>
</div>

<div class="return-button">
    <?= Html::button('Назад', [
        'href' => Url::to(['tax-rate']),
        'class' => 'button-regular button-hover-transparent',
    ]) ?>
</div>