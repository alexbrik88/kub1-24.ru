<?php

use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */

?>

<div class="taxrobot-start-header">
    <h4>
        Загрузка выписки
    </h4>
</div>

<div class="taxrobot-start-content">
    <?= \yii\bootstrap\Alert::widget([
        'body' => $message,
        'options' => [
            'class' => 'alert-danger',
        ],
    ]); ?>
</div>

<div class="return-button hide-in-banking">
    <?= Html::button('Назад', [
        'href' => Url::to(['upload-select']),
        'class' => 'button-regular button-hover-transparent',
    ]) ?>
</div>
