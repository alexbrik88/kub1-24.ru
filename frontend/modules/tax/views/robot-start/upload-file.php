<?php

use frontend\components\Icon;
use kartik\file\FileInput;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/** @var $this yii\web\View */

$main1 = <<<TPL
<div class="mt-4 d-flex justify-content-between">{browse}{upload}</div>
<div class="mt-4 d-flex justify-content-start">{caption}{remove}</div>
<div class="mt-4 kv-upload-progress hide"></div>
TPL;
$caption = <<<TPL
<div class="file-caption-name"></div>
TPL;
?>

<div class="taxrobot-start-header">
    <h4>
        Загрузка выписки файлом
    </h4>
</div>

<style type="text/css">
.upload-file-remove {
    margin: 0;
    padding: 0;
    background: none;
    border: none;
    color: #007bff;
    font: inherit;
    cursor: pointer;
    outline: inherit;
}
</style>

<div class="taxrobot-start-content">
    <div class="">
        Выгрузите из Клиент-Банка файл формата 1С (расширение: .txt) и загрузите его тут.
    </div>

    <?= Html::beginForm(['upload-file'], 'post', [
        'id' => 'upload-file-form',
        'enctype' => 'multipart/form-data',
    ]) ?>

        <div class="mt-4">
            <?= FileInput::widget([
                'id' => 'taxrobot-start-file',
                'name' => 'uploadfile',
                'options' => [
                    'accept' => '.txt',
                ],
                'pluginOptions' => [
                    'showPreview' => false,
                    'showCaption' => true,
                    'showRemove' => true,
                    'showUpload' => true,
                    'browseIcon' => Icon::get('1c', ['class' => 'mr-2']),
                    'browseClass' => 'button-regular button-hover-transparent',
                    'browseLabel' => 'Загрузить файл 1С',
                    'uploadIcon' => '',
                    'uploadClass' => 'button-regular button-regular_red min-w-130 ladda-button ladda-custom',
                    'removeIcon' => '<i class="fa fa-times-circle"></i>',
                    'removeClass' => 'upload-file-remove',
                    'removeLabel' => '',
                    'layoutTemplates' => [
                        'main1' => $main1,
                        'caption' => $caption,
                    ],
                ],
            ]) ?>
        </div>

    <?= Html::endForm() ?>
</div>

<div class="return-button">
    <?= Html::button('Назад', [
        'href' => Url::to(['upload-select']),
        'class' => 'button-regular button-hover-transparent',
    ]) ?>
</div>
