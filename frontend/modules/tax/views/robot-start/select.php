<?php

use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */

$url = Url::to(['/tax/robot-start/activity']);
?>

<div class="taxrobot-start-header">
    <h4>
        Выберите способ заполнения данных по ИП
    </h4>
</div>

<div class="taxrobot-start-content">
    <div class="fx-row">
        <div class="col-6 text-center">
            <?= Html::img('/img/taxrobot/2.1.png', [
                'style' => 'max-width: 100%;',
                'alt' => '',
            ]) ?>
        </div>
        <div class="col-6 text-center">
            <?= Html::img('/img/taxrobot/2.2.png', [
                'style' => 'max-width: 100%;',
                'alt' => '',
            ]) ?>
        </div>
    </div>
    <div class="fx-row mb-4">
        <div class="col-6">
            <div class="d-flex align-items-center justify-content-between h-100 p-3" data-dismiss="modal"
                style="border: solid 2px #e2e5eb; border-radius: 4px !important; cursor: pointer;">
                <div class="mr-5">
                    <strong>Вручную</strong>
                    <br>
                    Данные вводите
                    <br>
                    вручную.
                    <br>
                    Время - <strong>до 15 минут</strong>
                </div>
                <div>
                    <?= Html::img('/img/taxrobot/2.3.png', [
                        'style' => 'max-width: 100%; max-height: 80px;',
                        'alt' => '',
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="d-flex align-items-center justify-content-between h-100 p-3" href="<?= $url ?>"
                style="border: solid 2px #4679AE; border-radius: 4px !important; cursor: pointer;">
                <div class="mr-5">
                    <strong>Автоматически</strong>
                    <br>
                    Все данные загружаются автоматически.
                    <br>
                    Время - <strong>до 3 минут</strong>
                </div>
                <div>
                    <?= Html::img('/img/taxrobot/2.4.png', [
                        'style' => 'max-width: 100%; max-height: 80px;',
                        'alt' => '',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6"> justify-content-between
            К этому варианту всегда можно вернуться
        </div>
        <div class="col-sm-6">
            При необходимости данные можно откорректировать вручную
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-sm-6 text-center">
            <?= Html::button('Выбрать "Вручную"', [
                'class' => 'button-regular button-regular_red',
                'style' => 'width: 100%; max-width: 230px;',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
        <div class="col-sm-6 text-center">
            <?= Html::button('Выбрать "Автоматически"', [
                'class' => 'button-regular button-regular_red',
                'style' => 'width: 100%; max-width: 230px;',
                'href' => $url,
            ]) ?>
        </div>
    </div>
</div>
