<?php

use frontend\modules\tax\models\TaxrobotStartForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $model frontend\modules\tax\models\TaxrobotStartForm */

?>

<div class="taxrobot-start-header">
    <h4>
        Определение кода ИФНС и ОКТМО по адресу прописки
    </h4>
</div>

<div class="taxrobot-start-content">
    <div class="mb-4">
        Данные необходимы для определения реквизитов налоговой в платежном поручении
    </div>
    <?php $form = ActiveForm::begin([
        'id' => 'address-form',
        'action' => ['address'],
        //'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    ]); ?>

        <?= $form->field($model, 'address')->textInput([
            'maxLength' => true,
        ])->hint(Html::tag('span', 'Начните вводить адрес и выберите нужный из предложенного списка', [
            'style' => 'color:red;font-style:italic;font-size:12px',
        ]))->label('Адрес по прописке') ?>

        <div class="row">
            <div class="col-xs-4">
                <?= $form->field($model, 'ifns')->textInput([
                    'maxLength' => true,
                    'readonly' => true,
                ]) ?>
            </div>
            <div class="col-xs-4">
                <?= $form->field($model, 'oktmo')->textInput([
                    'maxLength' => true,
                    'readonly' => true,
                ]) ?>
            </div>
            <div class="col-xs-4">
                <?= $form->field($model, 'index')->textInput([
                    'maxLength' => true,
                    'readonly' => true,
                ]) ?>
            </div>
        </div>

        <div class="mt-4">
            <?= Html::submitButton('Сохранить', [
                'class' => 'button-regular button-hover-transparent min-w-130 ladda-button ladda-custom',
                'data-style' => 'expand-right',
            ]) ?>
        </div>

    <?php ActiveForm::end() ?>
</div>

<div class="return-button">
    <?= Html::button('Назад', [
        'href' => Url::to(['upload-success']),
        'class' => 'button-regular button-hover-transparent min-w-130',
    ]) ?>
</div>