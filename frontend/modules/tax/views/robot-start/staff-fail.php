<?php

use frontend\modules\tax\models\TaxrobotStartForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $model frontend\modules\tax\models\TaxrobotStartForm */

$model->subject = 'Программа еще учится рассчитывать налоги и готовить отчетность по сотрудникам…';
?>

<div class="taxrobot-start-header">
    <h4>
        <strong>Программа еще учится рассчитывать налоги и готовить отчетность по сотрудникам…</strong>
    </h4>
</div>

<div class="taxrobot-start-content">
    <h5>
        НО, у нас хорошие новости для вас!
    </h5>

    <div>
        Пока программа учится, наши бухгалтера помогут рассчитать ваши налоги и сдать отчетность.
        Оставьте ваши контакты, и мы с вами свяжемся
    </div>

    <div class="mt-4">
        <?php $form = ActiveForm::begin([
            'id' => 'staff-fail-form',
            'action' => ['staff-fail'],
            //'fieldConfig' => Yii::$app->params['kubFieldConfig'],
        ]); ?>

            <?= Html::activeHiddenInput($model, 'subject') ?>

            <div class="row mb-">
                <div class="col-sm-4">
                    <?= $form->field($model, 'user_name')->textInput(['maxLength' => true]) ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'user_phone')->textInput([
                        'maxLength' => true,
                    ])->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                        'options' => [
                            'placeholder' => '+7(XXX) XXX-XX-XX',
                        ],
                    ]) ?>
                </div>
                <div class="col-sm-4">
                    <label class="control-label">&nbsp;</label>
                    <div>
                        <?= Html::submitButton('Отправить', [
                            'class' => 'button-regular button-hover-transparent min-w-130 ladda-button ladda-custom',
                            'data-style' => 'expand-right',
                        ]) ?>
                    </div>
                </div>
            </div>

        <?php ActiveForm::end() ?>
    </div>
</div>

<div class="return-button">
    <?= Html::button('Назад', [
        'href' => Url::to(['staff']),
        'class' => 'button-regular button-hover-transparent',
    ]) ?>
</div>