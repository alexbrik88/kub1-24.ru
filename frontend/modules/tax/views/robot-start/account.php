<?php

use common\components\ImageHelper;
use frontend\modules\tax\models\TaxrobotStartForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $model frontend\modules\tax\models\TaxrobotStartForm */
/** @var $bankingData array */

?>

<div class="taxrobot-start-header">
    <h4>
        Загрузка выписки из банка
    </h4>
</div>

<div class="taxrobot-start-content">
    <div class="row">
        <?= Html::tag('div', ImageHelper::getThumb($bankingData['logo'], [250, 150], [
            'class' => 'bank-logo',
            'style' => 'cursor: pointer;',
            'alt' => $bankingData['name'],
        ]), [
            'class' => 'col-sm-4 mb-4',
        ]) ?>
        <div class="col-sm-8">
            Для обеспечения безопасности данных используется протокол зашифрованного соединения SSL
            - надежный протокол для передачи конфиденциальной банковской информации
            и соблудаются требования международного стандарта PCI DSS по хранению и передаче
            конфиденциальной информации в банковской сфере.
        </div>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'tax-fail-form',
        //'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    ]); ?>

        <div class="row mt-4">
            <div class="col-sm-6">
                <?= $form->field($model, 'inn')->textInput([
                    'maxLength' => 20
                ])->label('Укажите ваш ИНН') ?>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-sm-6">
                <?= $form->field($model, 'rs')->textInput([
                    'maxLength' => 20
                ])->label('Укажите номер расчетного счета') ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'bik')->dropDownList(array_combine(
                    $bankingData['bikList'],
                    $bankingData['bikList']
                ))->label('Укажите БИК банка') ?>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-sm-6">
                <?= Html::submitButton('Сохранить', [
                    'class' => 'button-regular button-hover-transparent min-w-130 ladda-button ladda-custom',
                    'data-style' => 'expand-right',
                ]) ?>
            </div>
            <div class="col-sm-6 text-right">
                <?= Html::button('Отменить', [
                    'href' => Url::to(['banking-select']),
                    'class' => 'button-regular button-hover-transparent min-w-130',
                ]) ?>
            </div>
        </div>
    <?php ActiveForm::end() ?>
</div>
