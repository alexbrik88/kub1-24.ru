<?php

use frontend\rbac\UserRole;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;

$this->beginContent('@frontend/views/layouts/main.php');
?>
    <div class="debt-report-content container-fluid" style="padding: 0; margin-top: -10px;">
        <?php NavBar::begin([
            'options' => [
                'class' => 'navbar-report navbar-default',
            ],
            'brandOptions' => [
                'style' => 'margin-left: 0;'
            ],
            'containerOptions' => [
                'style' => 'padding: 0;'
            ],
            'innerContainerOptions' => [
                'class' => 'container-fluid',
                'style' => 'padding: 0;'
            ],
        ]);
        echo Nav::widget([
            'id' => 'debt-report-menu',
            'items' => [
                ['label' => 'Налоговая декларация', 'url' => ['/tax/declaration'], 'active' => Yii::$app->controller->id == 'declaration' || Yii::$app->controller->id == 'declaration-osno'],
                ['label' => 'КУДиР', 'url' => ['/tax/kudir'], 'active' => Yii::$app->controller->id == 'kudir'],
            ],
            'options' => ['class' => 'navbar-nav'],
        ]);
        NavBar::end();
        ?>
        <?= $content; ?>
    </div>
<?php $this->endContent(); ?>