<?php
use himiklab\thumbnail\EasyThumbnailImage;
use yii\helpers\Html;
use common\components\date\DateHelper;
use frontend\modules\tax\models\DeclarationBarcodeHelper;
use common\models\Company;

//////////////////////////////////////////////
$document_date = date('Y-m-d', strtotime($model->document_date));
$form_type = 'исхд'; // заполняется обязательно одним из кодов: "исхд", "доп", "отмн"
/////////////////////////////////////////////

$formattedDate = DateHelper::format($document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

/** @var Company $company */
?>

<style media="print">
    @page {
        size: auto;
        margin: 0;
    }
    .page-break  {
        page-break-after: always;
    }
</style>

<style>
    .table {margin:0; padding:0;}
    .table td {font-size:11pt; vertical-align: bottom; padding:4px 2px 0;}
    .table td.font-big {font-size:13pt;}
    .table td.font-medium {font-size:8.5pt;}
    .table td.font-small {font-size:7pt;}
    .table td.text-underline {text-decoration: underline}
    .table td.bb {border-bottom:1px solid #000}
    .table td.cc {text-align: center; vertical-align: middle}
    .table td.bb-data {border-bottom:1px solid #000; font-weight:bold}
    <?php if (@$_GET['red']) { ?>.table td {border:1px solid red} <?php } ?>
</style>

<div class="page-content-in p-center pad-pdf-p">

    <table class="table no-border">
        <tr>
            <td width="65%"></td>
            <td class="font-bold font-big">
                УТВЕРЖДЕНА
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                постановлением Правления ПФР <br/>
                от 1 февраля 2016 г. № 83п
            </td>
        </tr>
        <tr>
            <td class="text-underline">
                Форма СЗВ-М
            </td>
            <td></td>
        </tr>
        <tr>
            <td colspan="2" class="text-center font-bold font-big">
                Сведения о застрахованных лицах
            </td>
        </tr>
    </table>

    <table class="table no-border">
        <tr style="display: none">
            <td width="25%"></td>
            <td width="10%"></td>
            <td width="20%"></td>
            <td width="8%"></td>
            <td width="15%"></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="6">1. Реквизиты страхователя (заполняются обязательно):</td>
        </tr>
        <tr>
            <td colspan="2">Регистрационный номер в ПФР</td>
            <td colspan="2" class="bb-data">
                <?= ($company->company_type_id == \common\models\company\CompanyType::TYPE_IP) ?
                    $company->pfr_ip :
                    $company->pfr;
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">Наименование (краткое)</td>
            <td colspan="4" class="bb-data">
                <?= $company->getShortName() ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">ИНН</td>
            <td class="bb-data">
                <?= $company->inn ?>
            </td>
            <td class="text-center">КПП</td>
            <td class="bb-data">
                <?= $company->kpp ?>
            </td>
        </tr>
        <tr>
            <td>2. Отчетный период</td>
            <td class="bb-data text-center">
                <?= str_pad($model->tax_month, 2, "0", STR_PAD_LEFT) ?>
            </td>
            <td>календарного года</td>
            <td class="bb-data text-center">
                <?= $model->tax_year ?>
            </td>
            <td colspan="2" class="font-small">(заполняется обязательно)</td>
        </tr>
        <tr>
            <td colspan="6" class="font-small">
                (01 – январь, 02 – февраль, 03 – март, 04 – апрель, 05 – май, 06 – июнь, 07 – июль, 08 – август, 09 – сентябрь, 10 – октябрь, 11 – ноябрь, 12 – декабрь)
            </td>
        </tr>
        <tr>
            <td>3. Тип формы (код)</td>
            <td class="bb-data text-center">
                <?= $form_type ?>
            </td>
            <td colspan="4" class="font-small">(заполняется обязательно одним из кодов: "исхд", "доп", "отмн")</td>
        </tr>
        <tr>
            <td colspan="6" class="font-small">
                "исхд" – исходная форма, впервые подаваемая страхователем о застрахованных лицах за данный отчетный период<br/>
            </td>
        <tr>
            <td colspan="6" class="font-small">
                "доп" – дополняющая форма, подаваемая с целью дополнения ранее принятых ПФР сведений о застрахованных лицах за данный отчетный период<br/>
            </td>
        </tr>
        <tr>
            <td colspan="6" class="font-small">
                "отмн" – отменяющая форма, подаваемая с целью отмены ранее неверно поданных сведений о застрахованных лицах за указанный отчетный период<br/>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                4. Сведения о застрахованных лицах:
            </td>
        </tr>
        <tr>
            <td colspan="6" class="font-small">
                (указываются данные о застрахованных лицах – работниках, с которыми в отчетном периоде заключены,
                продолжают действовать или прекращены трудовые договоры, гражданско-правовые договоры, предметом которых
                является выполнение работ, оказание услуг, договоры авторского заказа, договоры об отчуждении
                исключительного права на произведения науки, литературы, искусства, издательские лицензионные договоры,
                лицензионные договоры о предоставлении права использования произведения науки, литературы, искусства)
            </td>
        </tr>
    </table>

    <table class="table" style="margin-top:5pt">
        <tr>
            <td width="7%" class="cc font-medium">№ п/п</td>
            <td class="cc font-medium">Фамилия, имя, отчество (при наличии) застрахованного лица (заполняются в именительном падеже)</td>
            <td width="25%" class="cc font-medium">Страховой номер индивидуального лицевого счета (заполняется обязательно)</td>
            <td width="25%" class="cc font-medium">ИНН (заполняется при наличии у страхователя данных об ИНН физического лица)</td>
        </tr>
        <?php
        $key = 0;
        /* @var $employeeCompany \common\models\EmployeeCompany */
        foreach ($company->getEmployeeCompanies()
                     ->joinWith('employee')
                     ->andWhere(['employee_company.is_working' => true])
                     ->andWhere(['employee.is_deleted' => false])
                     ->andWhere(['employee_company.in_szvm' => true])
                     ->all() as $employeeCompany): ?>
            <tr>
                <td class="text-right"><?= ++$key; ?></td>
                <td><?= $employeeCompany->getFio(); ?></td>
                <td><?= $employeeCompany->snils; ?></td>
                <td><?= $employeeCompany->inn; ?></td>
            </tr>
        <?php endforeach; ?>
        <?php for ($i = 0; $i < 20 - $key; $i++): ?>
            <tr>
                <td>&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        <?php endfor; ?>
    </table>

    <table class="table no-border" style="margin-top:20pt">
        <tr>
            <td width="40%" class="bb text-center">
                <?= $company->chief_post_name ?>
            </td>
            <td width="1%"></td>
            <td width="20%" class="bb text-center"></td>
            <td width="1%"></td>
            <td class="bb text-center">
                <?= $company->getChief_fio() ?>
            </td>
        </tr>
        <tr>
            <td class="text-center font-small">(наименование должности руководителя)</td>
            <td></td>
            <td class="text-center font-small">(подпись)</td>
            <td></td>
            <td class="text-center font-small">(Ф.И.О.)</td>
        </tr>
    </table>

    <table class="table no-border" style="margin-top:20pt">
        <tr>
            <td width="10%">Дата</td>
            <td width="20%">
                <?= $formattedDate ?>
            </td>
            <td width="40%" class="text-center">М.П.</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td class="text-center font-small">(при ее наличии)</td>
        </tr>
    </table>

</div>