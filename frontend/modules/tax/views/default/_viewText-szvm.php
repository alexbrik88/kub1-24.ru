<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 04.05.2019
 * Time: 0:19
 */

use common\components\helpers\Html;
use frontend\modules\tax\models\DeclarationOsnoHelper;
use common\components\date\DateHelper;

/* @var \frontend\modules\tax\models\TaxDeclaration $model */
/* @var DeclarationOsnoHelper $declarationHelper */

$formattedDate = DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
$form_type = 'исхд'; // заполняется обязательно одним из кодов: "исхд", "доп", "отмн"
$company = $model->company;
?>
<table class="table-preview no-border">
    <tr>
        <td width="65%"></td>
        <td class="font-bold font-big">
            УТВЕРЖДЕНА
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            постановлением Правления ПФР <br/>
            от 1 февраля 2016 г. № 83п
        </td>
    </tr>
    <tr>
        <td class="text-underline">
            Форма СЗВ-М
        </td>
        <td></td>
    </tr>
    <tr>
        <td colspan="2" class="text-center font-bold font-big">
            Сведения о застрахованных лицах
        </td>
    </tr>
</table>
<table class="table-preview no-border">
    <tr style="display: none">
        <td width="25%"></td>
        <td width="10%"></td>
        <td width="20%"></td>
        <td width="8%"></td>
        <td width="15%"></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="6">1. Реквизиты страхователя (заполняются обязательно):</td>
    </tr>
    <tr>
        <td colspan="2">Регистрационный номер в ПФР</td>
        <td colspan="2" class="bb-data">
            <?= ($company->company_type_id == \common\models\company\CompanyType::TYPE_IP) ?
                $company->pfr_ip :
                $company->pfr;
            ?>
        </td>
    </tr>
    <tr>
        <td colspan="2">Наименование (краткое)</td>
        <td colspan="4" class="bb-data">
            <?= $company->getShortName() ?>
        </td>
    </tr>
    <tr>
        <td colspan="2">ИНН</td>
        <td class="bb-data">
            <?= $company->inn ?>
        </td>
        <td class="text-center">КПП</td>
        <td class="bb-data">
            <?= $company->kpp ?>
        </td>
    </tr>
    <tr>
        <td>2. Отчетный период</td>
        <td class="bb-data text-center">
            <?= str_pad($model->tax_month, 2, "0", STR_PAD_LEFT) ?>
        </td>
        <td>календарного года</td>
        <td class="bb-data text-center">
            <?= $model->tax_year ?>
        </td>
        <td colspan="2" class="font-small">(заполняется обязательно)</td>
    </tr>
    <tr>
        <td colspan="6" class="font-small">
            (01 – январь, 02 – февраль, 03 – март, 04 – апрель, 05 – май, 06 – июнь, 07 – июль, 08 – август, 09 –
            сентябрь, 10 – октябрь, 11 – ноябрь, 12 – декабрь)
        </td>
    </tr>
    <tr>
        <td>3. Тип формы (код)</td>
        <td class="bb-data text-center">
            <?= $form_type ?>
        </td>
        <td colspan="4" class="font-small">(заполняется обязательно одним из кодов: "исхд", "доп", "отмн")</td>
    </tr>
    <tr>
        <td colspan="6" class="font-small">
            "исхд" – исходная форма, впервые подаваемая страхователем о застрахованных лицах за данный отчетный
            период<br/>
        </td>
    <tr>
        <td colspan="6" class="font-small">
            "доп" – дополняющая форма, подаваемая с целью дополнения ранее принятых ПФР сведений о застрахованных лицах
            за данный отчетный период<br/>
        </td>
    </tr>
    <tr>
        <td colspan="6" class="font-small">
            "отмн" – отменяющая форма, подаваемая с целью отмены ранее неверно поданных сведений о застрахованных лицах
            за указанный отчетный период<br/>
        </td>
    </tr>
    <tr>
        <td colspan="6">
            4. Сведения о застрахованных лицах:
        </td>
    </tr>
    <tr>
        <td colspan="6" class="font-small">
            (указываются данные о застрахованных лицах – работниках, с которыми в отчетном периоде заключены,
            продолжают действовать или прекращены трудовые договоры, гражданско-правовые договоры, предметом которых
            является выполнение работ, оказание услуг, договоры авторского заказа, договоры об отчуждении
            исключительного права на произведения науки, литературы, искусства, издательские лицензионные договоры,
            лицензионные договоры о предоставлении права использования произведения науки, литературы, искусства)
        </td>
    </tr>
</table>
<table class="table-preview border" style="margin-top:5pt">
    <tr>
        <td width="7%" class="cc font-medium">№ п/п</td>
        <td class="cc font-medium">Фамилия, имя, отчество (при наличии) застрахованного лица (заполняются в именительном
            падеже)
        </td>
        <td width="25%" class="cc font-medium">Страховой номер индивидуального лицевого счета (заполняется
            обязательно)
        </td>
        <td width="25%" class="cc font-medium">ИНН (заполняется при наличии у страхователя данных об ИНН физического
            лица)
        </td>
    </tr>
    <?php
    $key = 0;
    /* @var $employeeCompany \common\models\EmployeeCompany */
    foreach ($company->getEmployeeCompanies()
                 ->joinWith('employee')
                 ->andWhere(['employee_company.is_working' => true])
                 ->andWhere(['employee.is_deleted' => false])
                 ->andWhere(['employee_company.in_szvm' => true])
                 ->all() as $employeeCompany): ?>
        <tr>
            <td class="text-right"><?= ++$key; ?></td>
            <td><?= $employeeCompany->getFio(); ?></td>
            <td><?= $employeeCompany->snils; ?></td>
            <td><?= $employeeCompany->inn; ?></td>
        </tr>
    <?php endforeach; ?>
    <?php for ($i = 0; $i < 20 - $key; $i++): ?>
        <tr>
            <td>&nbsp;</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    <?php endfor; ?>
</table>
<table class="table-preview no-border" style="margin-top:20pt">
    <tr>
        <td width="40%" class="bb text-center">
            <?= $company->chief_post_name ?>
        </td>
        <td width="1%"></td>
        <td width="20%" class="bb text-center"></td>
        <td width="1%"></td>
        <td class="bb text-center">
            <?= $company->getChief_fio() ?>
        </td>
    </tr>
    <tr>
        <td class="text-center font-small">(наименование должности руководителя)</td>
        <td></td>
        <td class="text-center font-small">(подпись)</td>
        <td></td>
        <td class="text-center font-small">(Ф.И.О.)</td>
    </tr>
</table>
<table class="table-preview no-border" style="margin-top:20pt">
    <tr>
        <td width="10%">Дата</td>
        <td width="20%" class="document-date-js">
            <?= $formattedDate ?>
        </td>
        <td width="40%" class="text-center">М.П.</td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td class="text-center font-small">(при ее наличии)</td>
    </tr>
</table>
