<tr>
    <td class="text-right pad5"> Форма по КНД 1152017</td>
</tr>
<tr>
    <td class="text-center pad10 size_xs">
        <div style="width: 80%; margin: 0 auto">
            <strong>
                Налоговая декларация по налогу, уплачиваему <br>
                в связи с приминением упрощенной системы налогообложения
            </strong>
        </div>
    </td>
</tr>
<tr>
    <td>
        <table>
            <tr>
                <td> Номер корректировки <span class="size_l pad5-l"> &nbsp; 0 </span> </td>
                <td class="text-center"> Налоговый период (код) <span class="size_l pad5-l"> &nbsp; 34 </span> </td>
                <td class="text-right"> Отчетный год <span class="size_l pad5-l"> &nbsp; 2017 </span> </td>
            </tr>
        </table>

        <table>
            <tr>
                <td class="pad5-t pad5-b"> Представляеться в налоговый орган (код) <span class="size_l pad5-l"> &nbsp; 6195 </span> </td>
                <td class="pad5-t pad5-b text-right"> по месту нахождения (учета) (код) <span class="size_l pad5-l"> &nbsp; 120 </span> </td>
            </tr>
        </table>

        <table>
            <tr>
                <td class="pad10-t pad10-b">
                    <span class="size_l pad5-l" style="text-transform: uppercase"> МИНАДЗЕ </span> <br>
                    <span class="size_l pad5-l" style="text-transform: uppercase"> ИРАКЛИЙ </span> <br>
                    <span class="size_l pad5-l" style="text-transform: uppercase"> ЮРЬЕВИЧ </span>
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    <hr style="margin: 0px">
                </td>
            </tr>
            <tr>
                <td class="text-center size_xs">
                    (налогоплательщик)
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td class="pad10-t">
        <table>
            <tr class="pad10-t pad10-b">
                <td width="29%" class="v-center pad10-t pad10-b"> Форма реорганизации, ____ <br> Ликвидация (код)</td>
                <td width="28%" class="pad10-t pad10-b"> Инн/КПП реорганизованной организации </td>
                <td width="43%" class="pad10-t pad10-b text-right"> ____________________ / ____________________ </td>
            </tr>
            <tr class="pad10-t pad10-b">
                <td width="30%" class="v-center pad10-t pad10-b"> Номер контактного телефона </td>
                <td width="28%" class="pad10-t pad10-b">  <span class="size_l" style="text-align: left"> (495) 545-47-46 </span>  </td>
                <td width="43%" class="text-right"> &nbsp; </td>
            </tr>
        </table>
    </td>
</tr>

<tr>
    <td class="pad10-t pad10-b">
        <table>
            <tr>
                <td class="v-center"> На</td>
                <td class="v-center text-center size_l" width="10%"> 3 </td>
                <td class="v-center text-center"> страницах с приложением подтверждающих документов или их копий на </td>
                <td class="v-center text-center" width="10%"> <table class="dotted-cells"> <tr><td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> </tr></table> </td>
                <td class="v-center text-right" width="10%"> листах </td>
            </tr>
        </table>
    </td>
</tr>

<tr>
    <td class="text-right pad10-t">
        <hr style="margin: 0px">
    </td>
</tr>
<tr>
    <td>
        <table>
            <tr>
                <td class="text-center pad10-t pad5-r bord-dark-r" width="50%">
                    <div>
                                 <span class="size_s bold">
                                    Достоверность и полноту сведений, указаных <br>
                                    в настоящей декларации, подтверждаю:
                                 </span>
                    </div>
                    <table>
                        <tr>
                            <td class="text-right size_l pad10-r" width="25%">
                                1
                            </td>
                            <td class="size_xs" width="75%" style="text-align: left">
                                1 - налогоплательщик <br>
                                2 - представитель налогоплательщика
                            </td>
                        </tr>
                    </table>
                    <br>
                    <table class="lines">
                        <tr> <td> &nbsp; </td> </tr>
                        <tr> <td> &nbsp; </td> </tr>
                        <tr> <td>  <span class="size_xs"> (фамилия, имя, отчество * полностью) </span> </td> </tr>
                        <tr> <td style="border: none"> &nbsp; </td> </tr>
                        <tr> <td> &nbsp; </td> </tr>
                        <tr> <td> &nbsp; </td> </tr>
                        <tr> <td> &nbsp; </td> </tr>
                        <tr> <td> &nbsp; </td> </tr>
                        <tr> <td> &nbsp; </td> </tr>
                        <tr> <td> &nbsp; </td> </tr>
                        <tr> <td class="pad10-b"> <span class="size_xs"> (наименование организации - представителя налогоплательщика) </span> <br></td> </tr>
                    </table>

                    <table>
                        <tr>
                            <td class="text-left pad10-t pad10-b" width="45%">
                                Подпись <span class="under-line">
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    &nbsp;&nbsp;&nbsp;
                                                </span>
                                &nbsp;Дата
                            </td>
                            <td class="size_xs text-right" width="55%">
                                <table class="dotted-cells">
                                    <tr>
                                        <td>&nbsp;</td> <td>&nbsp;</td>
                                        <td class="dot">.</td>
                                        <td>&nbsp;</td> <td>&nbsp;</td>
                                        <td class="dot">.</td>
                                        <td>&nbsp;</td> <td>&nbsp;</td><td>&nbsp;</td> <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <table class="bord-bottom">
                        <tr>
                            <td>
                                <span class="size_xs"> Наименоввание документа, <br> подтверждающего полномочия представителя </span>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                </td>
                <td class="pad10-t pad5-l" width="50%">
                    <table width="100%">
                        <tr>
                            <td colspan="12" class="text-center">
                                <span class="size_s bold"> Заполняется работником налогового органа </span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12" class="pad10-t text-center">
                                <span> Сведения о представлении декларации </span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="11" class="pad10-t v-bottom"> Данная декларация представлена (код) </td>
                            <td class="pad10-t">
                                <table class="dotted-cells">
                                    <tr>
                                        <td>&nbsp;</td> <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-left pad10-t v-center"> <span> на </span> </td>
                            <td colspan="1" class="text-left pad10-t v-center">
                                <table class="dotted-cells">
                                    <tr>
                                        <td>&nbsp;</td> <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                            <td colspan="9" class="text-left pad10-t v-center" > страницах </td>
                        </tr>

                        <tr>
                            <td colspan="7" class="pad10-t pad3-r text-right">
                                <div>с приложением</div>
                                <div>подтверждающих документов</div>
                                <div>или их копий на</div>
                            </td>
                            <td colspan="3" class="pad10-t text-center v-center">
                                <table class="dotted-cells">
                                    <tr>
                                        <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                            <td colspan="2" class="pad10-t pad3-l v-center"> листах </td>
                        </tr>

                        <tr>
                            <td colspan="5" class="pad10-t pad3-r v-center"> Дата предоставления декларации </td>
                            <td colspan="7" class="pad10-t v-center">
                                <table class="dotted-cells">
                                    <tr>
                                        <td>&nbsp;</td> <td>&nbsp;</td>
                                        <td class="dot">.</td>
                                        <td>&nbsp;</td> <td>&nbsp;</td>
                                        <td class="dot">.</td>
                                        <td>&nbsp;</td> <td>&nbsp;</td><td>&nbsp;</td> <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="pad10-t v-center size_xxs"> Зарегистрирована за № </td>
                            <td colspan="9" class="pad10-t pad3-r v-center">
                                <table class="dotted-cells">
                                    <tr>
                                        <td>&nbsp;</td> <td>&nbsp;</td>
                                        <td>&nbsp;</td> <td>&nbsp;</td>
                                        <td>&nbsp;</td> <td>&nbsp;</td>
                                        <td>&nbsp;</td> <td>&nbsp;</td>
                                        <td>&nbsp;</td> <td>&nbsp;</td>
                                        <td>&nbsp;</td> <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td width="80%"><span class="black-square" style="display: inline"> &nbsp;&nbsp;&nbsp; </span> &nbsp; *отчество указываеться при наличии</td>
                            <td width="20%" style="text-align: right"><div class="black-square"> &nbsp;&nbsp;&nbsp;&nbsp; </div></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="v-center text-right" style="padding-right: 30px">
                    <span class="plus"> + </span>
                    <span>
                                <img style="height: 70px; width: 200px;display: inline"
                                     class="pad3-l pad3-r text-center"
                                     src="/img/task9599/0-3.jpg">
                            </span>
                    <span class="plus"> + </span>
                </td>
            </tr>
        </table>
    </td>
</tr>