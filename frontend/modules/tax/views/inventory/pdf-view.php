<?php

use frontend\modules\tax\models\Inventory;
use common\components\TextHelper;
use common\models\company\CompanyType;

/* @var $this yii\web\View */
/* @var $model Inventory */
/* @var $multiple [] */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->document_date,
]);


$this->title = $model->getPrintTitle();
?>
    <style type="text/css">
        <?php if (is_file($file = \Yii::getAlias('@frontend/web/css/print/common.css'))) {
            echo $this->renderFile($file);
        }
        if (is_file($file = \Yii::getAlias('@frontend/web/css/print/documents-invoice.css'))) {
            echo $this->renderFile($file);
        } ?>
        .page-setting {
            display: table;
            height: 29.47cm;
            box-sizing: border-box;
            width: 100%;
            max-width: 834px;
        }

        table {
            width: 700px;
            border-collapse: collapse;
            line-height: 1.4;
        }

        TD, TH {
            padding: 3px 6px;
            border: 1px solid black;
            vertical-align: top;
            font-size: 15px

        }

        .white-bord {
            border-color: white;
        }

        .middle-table {
            vertical-align: middle;
        }

        .size_s {
            font-size: 12px;

        }

        .size_l {
            font-size: 20px;
        }

        .height_m {
            height: 50px;
        }

        .text-red {
            color: #d21414;
        }

        .align-center {
            text-align: center;
        }

        .align-right {
            text-align: right;
        }

        .padding-left-20 {
            padding-left: 20px;
        }

        .padding-right-20 {
            padding-right: 20px;
        }
    </style>
    <div class="page-content-in p-center pad-pdf-p" style="box-sizing: content-box;">
        <table border="1">
            <tr>
                <td colspan="4" class="align-right white-bord"> Ф.107.</td>
            </tr>
            <tr>
                <td colspan="4" class="size_l align-center white-bord" style="border-bottom: 1px solid black;"><strong>
                        ОПИСЬ </strong></td>
            </tr>
            <tr class="height_m">
                <td colspan="2" width="40%"> Вложение в</td>
                <td colspan="2" width="60%" style="padding: 0 28px;">
                    <table style="width: 90%;">
                        <tbody>
                        <tr>
                            <td colspan="4" class="align-center" style="padding: 0;border-top: 1px solid white;"> ценное
                                письмо с уведомлением
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </td>
            </tr>
            <tr>
                <td colspan="2" style="border-bottom: 1px solid white;"> Наименование организации</td>
                <td colspan="2" style="border-bottom: 1px solid white;">
                    <strong>
                        <?= $model->company->getShortName() ?>
                    </strong>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="border-bottom: 1px solid white;"> &nbsp;</td>
                <td colspan="2" style="border-bottom: 1px solid white; height: 40px;"> &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2"> ИНН/КПП</td>
                <td colspan="2">
                    <?= $model->company->inn . '/' . $model->company->kpp; ?>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Рег. номер</td>
                <td colspan="2"> &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" class="height_m"> Куда <br> (место назначения)</td>
                <td colspan="2" class="middle-table">
                    <?= $model->control_organ_address; ?>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="height_m"> На имя <br> (наименование получателя)</td>
                <td colspan="2" class="middle-table">
                    <?= $model->control_organ; ?>
                </td>
            </tr>
            <tr class="align-center">
                <td class="size_s" width="6.5%"> № пп</td>
                <td class="size_s"> Наименование предметов</td>
                <td class="size_s"> Кол-во предметов</td>
                <td class="size_s"> Ценность в руб.</td>
            </tr>

            <tr class="height_m">
                <td class="align-center middle-table">
                    1
                </td>
                <td>
                    <?php if ($model->company->company_type_id == CompanyType::TYPE_IP): ?>
                        Декларация по налогу, уплачиваемому в связи с применением упрощенной системы налогообложения за <?= ($model->tax_year) ?> год
                    <?php elseif ($model->szvm): ?>
                        Сведения о застрахованных лицах за <?= "{$model->monthName} {$model->tax_year} года" ?>
                    <?php else: ?>
                        Единая (упрощенная) налоговая декларация за <?= ($model->tax_year) ?> год
                    <?php endif; ?>
                </td>
                <td class="middle-table align-center">1</td>
                <td class="middle-table align-center">
                    <?= TextHelper::invoiceMoneyFormat($model->totalValue, 2); ?>
                </td>
            </tr>

            <tr class="height_m">
                <td colspan="2" class="size_s"> Общий итого ценности в руб. <br> (цифрами)</td>
                <td colspan="2" class="padding-right-20 middle-table text-right">
                    <?= TextHelper::invoiceMoneyFormat($model->totalValue, 2); ?>
                </td>
            </tr>
            <tr class="height_m">
                <td colspan="2" class="size_s"> Отправитель <br> (подпись)</td>
                <td colspan="2" class="padding-left-20 middle-table"> &nbsp;</td>
            </tr>
            <tr class="height_m">
                <td colspan="2" class="size_s"> Проверил <br> (подпись работника связи)</td>
                <td colspan="2" class="padding-left-20 middle-table"> &nbsp;</td>
            </tr>
            <tr>
                <td colspan="4" class="size_s"> Никаких помарок и правок не допускается.</td>
            </tr>
        </table>
    </div>
<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id): ?>
    <pagebreak/>
<?php endif; ?>