<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10/26/15
 * Time: 4:27 PM
 * Email: t.kanstantsin@gmail.com
 */

use common\components\date\DateHelper;
use common\models\company\CompanyType;
use common\models\service;
use common\models\service\PaymentType;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\models\AffiliateProgramForm;
use frontend\modules\subscribe\forms\PaymentForm;
use yii\bootstrap\ActiveForm;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/* @var \yii\web\View $this */
/* @var \yii\data\ActiveDataProvider $paymentDataProvider */
/* @var \frontend\modules\subscribe\models\PaymentSearch $paymentSearchModel */
/* @var service\Subscribe[] $actualSubscribes */
/* @var common\models\Company $company */
/* @var $affiliateProgramForm AffiliateProgramForm */
/* @var $tariffId integer */

$this->title = 'Оплата сервиса';

$model = new PaymentForm($company, [
    'employee' => Yii::$app->user->identity,
]);
$formData = $model->getFormData();

$session = Yii::$app->session;
$pdfUid = $session->remove('paymentDocument.uid');
$done = (Yii::$app->request->get('status') == 'done');
$paymentType = Yii::$app->request->get('payment-type');
$left = $company->getInvoiceLeft();

if ($done && $pdfUid) {
    $this->registerJs('
        var link = document.createElement("a");
        link.href = "' . Url::to(['document-print', 'uid' => $pdfUid]) . '"
        link.target = "_blank";
        document.body.appendChild(link);
        link.click();
    ');
}

if ($done && $paymentType == 3) {
    $this->registerJs('
       $(document).ready(function() {
        $("#info-after-pay").modal("show");
    })
    ');
}

$tariffArray = ArrayHelper::getValue($formData, 'tariffArray', []);
$tariffPrices = ArrayHelper::getValue($formData, 'tariffPrices', []);
$discountByCount = ArrayHelper::getValue($formData, 'discountByCount', []);
$groupArray = ArrayHelper::getValue($formData, 'groupArray', []);
$tariffDurations = ArrayHelper::getValue($formData, 'tariffDurations', []);
$tariffPerQuantity = ArrayHelper::getValue($formData, 'tariffPerQuantity', []);
$limit = SubscribeTariff::find()->select('tariff_limit')->actual()->andWhere([
    'tariff_group_id' => SubscribeTariffGroup::STANDART,
])->orderBy(['price' => SORT_ASC])->scalar();

$expiresArray = [];
foreach ($groupArray as $group) {
    $subscribes = service\SubscribeHelper::getPayedSubscriptions($company->id, $group->id);
    if ($subscribes) {
        $limitLeft = reset($subscribes)->getLimitLeft();
        $expiresArray[] = [
            'name' => $group->name,
            'date' => $date = service\SubscribeHelper::getExpireDate($subscribes),
            'days' => Yii::t('yii', '{n, plural, =0{# дня} 1{# день} one{# день} few{# дня} other{# дней}}', [
                'n' => service\SubscribeHelper::getExpireLeftDays($date),
            ]),
            'left' => $limitLeft < 1000 ? $limitLeft.' '.ArrayHelper::getValue(SubscribeTariffGroup::$limitItemLabel, [
                $group->id,
                'of'
            ]) : null,
            'is_trial' => $group->id == SubscribeTariffGroup::STANDART && end($subscribes)->tariff_id == SubscribeTariff::TARIFF_TRIAL,
        ];
    }
}
if (count($expiresArray) > 1 && $expiresArray[0]['is_trial']) {
    $exp = array_shift($expiresArray);
    $expiresArray[] = $exp;
}

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-pay',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'contentCloning' => true,
        'interactive' => true,
        'functionBefore' => new JsExpression('function(instance, helper) {
            var content = $($(helper.origin).data("tooltip-content"));
            instance.content(content);
        }'),
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-click',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'contentCloning' => true,
        'interactive' => true,
    ],
]);

$icons = [
    SubscribeTariffGroup::STANDART => 'tariff_1.png',
    SubscribeTariffGroup::LOGISTICS => 'tariff_2.png',
    SubscribeTariffGroup::B2B_PAYMENT => 'tariff_3.png',
    SubscribeTariffGroup::TAX_DECLAR_IP_USN_6 => 'tariff_4.svg',
    SubscribeTariffGroup::ANALYTICS => 'tariff_5.png',
    SubscribeTariffGroup::OOO_OSNO_NULL_REPORTING => 'tariff_6.png',
    SubscribeTariffGroup::TAX_IP_USN_6 => 'tariff_7.png',
    SubscribeTariffGroup::CHECK_CONTRACTOR => 'tariff_8.svg',
    SubscribeTariffGroup::DOCUMENT_RECOGNITION => 'tariff_9.svg',
    SubscribeTariffGroup::PRICE_LIST => 'price-list.png',
];
?>

<?= $this->render('_style'); ?>

<?= Yii::$app->session->getFlash('emptyCompany'); ?>

<div class="alert alert-danger hidden" role="alert">
    В связи с работами на стороне Мерчанта Робокасса, оплата картами физ.лиц временно не доступна.
    Просим производить платежи с расчетного счета компании, по клику на кнопку «Выставить счет».
    Приносим извинения за доставленные неудобства.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<?php if ($expiresArray && ($expire = array_shift($expiresArray))) : ?>
    <h3 class="page-title">
        Текущая подписка "<?= $expire['name'] ?>" активна до
        <?= date(DateHelper::FORMAT_USER_DATE, $expire['date']); ?>
        (осталось: <?= $expire['days'] ?><?= $expire['left'] !== null ? ' / '.$expire['left'] : ''; ?>)
    </h3>
<?php else : ?>
    <h3 class="page-title">
        Текущая подписка БЕСПЛАТНО <?= $left < 1000 ? "(осталось: $left счетов)" : ''; ?>
    </h3>
<?php endif; ?>

<?php if ($expiresArray) : ?>
    <div class="mar-b-20">
    <?php foreach ($expiresArray as $expire) : ?>
        <div>
            "<?= $expire['name'] ?>"
            до <?= date(DateHelper::FORMAT_USER_DATE, $expire['date']); ?>
            (осталось: <?= \Yii::t('yii', '{n, plural, =0{# дня} 1{# день} one{# день} few{# дня} other{# дней}}', [
                'n' => $expire['days'],
            ]); ?><?= $expire['left'] !== null ? ' / '.$expire['left'] : ''; ?>)
        </div>
    <?php endforeach; ?>
    </div>
<?php endif; ?>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Оплатить подписку
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <?= Html::beginTag('div', [
            'id' => 'payment-box-1',
            'data' => [
                'discounts' => $discountByCount,
                'prices' => $tariffPrices,
                'durations' => $tariffDurations,
                'quantity' => $tariffPerQuantity,
            ],
        ]) ?>

            <div class="subscribe-tariff-row">
                <div class="panel-promo-code" data-promo-code="1">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="subscribe-tariff-payment">
                                <div id="subscribe-tariff-block" class="subscr-tariff-blk sum_payment_amount" style="">
                                    <?= $this->render('_payment_form_2', [
                                        'company' => $company,
                                        'tariffArray' => $tariffArray,
                                        'tariffId' => 1,
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div>
                                <?= $this->render('_promoCodeForm', [
                                    'promoCodeForm' => new \frontend\modules\subscribe\forms\PromoCodeForm(),
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="subscribe-tariff-items" class="row-5ths" style="width: auto; margin: 0 -5px; padding: 0;">
                <?php foreach ($groupArray as $group) : ?>
                    <?= $this->render('_tariff_block', [
                        'company' => $company,
                        'group' => $group,
                        'discountByCount' => $discountByCount,
                        'limit' => $limit,
                        'icon' => $icons[$group->id] ?? null,
                    ]) ?>
                <?php endforeach; ?>
                    <div class="clearfix"></div>
                </div>
            </div>

        <?= Html::endTag('div') ?> <!-- #payment-box-1 -->

        <div style="display: none;">
            <?= Html::a('', null, [
                'id' => 'document-print-link',
                'target' => '_blank',
                'data-url' => Url::to(['document-print', 'uid' => 'pdfUid']),
            ]) ?>
        </div>
    </div>
</div>

<?= $this->render('partial/_companies_subscribes', [
    'company' => $company,
    'groupArray' => $groupArray
]); ?>

<?= $this->render('partial/_affiliate_program', [
    'company' => $company,
    'tariffArray' => $tariffArray,
]); ?>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Тариф БЕСПЛАТНО
            <span style="font-style: italic;">
                (подключается автоматически после окончания оплаты)
            </span>
        </div>
    </div>
    <div class="portlet-body free-tariff">
        Ограничения на тарифе:
        <ul class="free-tariff-list">
            <li>Не более 5 счетов в месяц</li>
            <li>
                Только 1 организация.
                <span style="font-style: italic;">
                    Т.е. если вы владеете более 1-й компании,
                    то вы не сможете добавить еще компании.
                </span>
            </li>
            <li>Не более 3-х сотрудников</li>
            <li>Место на диске - 1 ГБ</li>
        </ul>
    </div>
</div>

<?= $this->render('_payment_history', [
    'paymentDataProvider' => $paymentDataProvider,
    'paymentSearchModel' => $paymentSearchModel,
]); ?>

<?= $this->render('partial/_modals_tariff_info'); ?>

    <!-- Small modal -->
    <div class="modal fade" id="payment-box-2" tabindex="-1" role="modal" aria-hidden="true" style="border: none;">
        <div class="modal-dialog" style="width: 350px !important;margin: 0 auto;">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom: none; padding: 20px 10px 0px 0px">
                    <button type="button" class="close" data-dismiss="modal" style="height: 20px;width: 20px;"
                            aria-hidden="true"></button>
                </div>
                <div class="modal-body" style="padding: 0px 35px 20px 35px;font-size: 1.3em;text-align: center;margin: 0 auto;line-height: 1.8;">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="info-after-pay" tabindex="-1" role="modal" aria-hidden="true" style="border: none;">
        <div class="modal-dialog" style="width: 350px !important;margin: 0 auto;">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom: none; padding: 20px 10px 0px 0px">
                    <button type="button" class="close" data-dismiss="modal" style="height: 20px;width: 20px;"
                            aria-hidden="true"></button>
                </div>
                <div class="modal-body" style="padding: 0px 35px 20px 35px;font-size: 1.3em;text-align: center;margin: 0 auto;line-height: 1.8;">
                    <div class="text-center pad5">
                        <img style="height: 4em;" src="/img/service/ok-2.1.png"> <br>
                        <div>
                            <p>
                                Счет отправлен на
                                <br>
                                <strong id="send-to-email"></strong>.
                            </p>
                            <p>
                                Если письмо не придет в течении 5 минут - ищите в спаме или напишите на
                                <br>
                                <span>support@kub-24.ru</span>
                            </p>
                        </div>

                        <a class="btn" data-dismiss="modal" style="background-color: #45B6AF; color: #fff;"> ОК </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="bill-invoice-remuneration" tabindex="-1"
         role="modal"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true"></button>
                    <h3 style="text-align: center; margin: 0">Получить
                        вознаграждение на счет</h3>
                </div>
                <div class="modal-body">
                    <?php $affiliateForm = ActiveForm::begin([
                        'action' => Url::to(['/company/withdraw-money', 'page' => 'subscribe']),
                        'options' => [
                            'class' => 'form-horizontal form-checking-accountant',
                            'id' => 'form-bill-invoice-remuneration',
                        ],
                        'enableClientValidation' => true,
                    ]); ?>


                    <span>
                        Доступно к выводу: <b><?= $company->affiliate_sum; ?> руб.</b>
                    </span>

                    <?= $affiliateForm->field($affiliateProgramForm, 'withdrawal_amount', [
                        'options' => [
                            'class' => 'form-group',
                            'style' => 'margin-top: 15px; margin-bottom: 0;',
                        ],
                        'labelOptions' => [
                            'class' => 'col-md-2 control-label bold-text',
                        ],
                        'wrapperOptions' => [
                            'class' => 'col-md-10 inp_one_line-product',
                        ],
                        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                    ])->textInput([
                        'placeholder' => 'Укажите сумму для вывода вознаграждения',
                    ])->label('Вывести:'); ?>

                    <?= $affiliateForm->field($affiliateProgramForm, 'type', [
                        'options' => [
                            'class' => 'form-group',
                        ],
                        'labelOptions' => [
                            'class' => 'col-md-2 control-label bold-text',
                        ],
                        'wrapperOptions' => [
                            'class' => 'col-md-7 inp_one_line-product',
                        ],
                        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                    ])->radioList([
                        AffiliateProgramForm::RS_TYPE => 'на расчетный счет компании',
                        AffiliateProgramForm::INDIVIDUAL_ACCOUNT_TYPE => 'на счет физического лица',
                    ])->label(''); ?>

                    <div class="form-actions" style="margin-bottom: 10px;">
                        <div class="row action-buttons">
                            <div class="col-sm-4 col-xs-4">
                                <?= Html::submitButton('Оформить заказ', [
                                    'class' => 'btn darkblue text-white hidden-md hidden-sm hidden-xs',
                                    'style' => 'width: 100% !important',
                                ]); ?>
                                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                                    'class' => 'btn darkblue text-white hidden-lg',
                                    'title' => 'Оформить заказ',
                                    'style' => 'width: 100% !important',
                                ]); ?>
                            </div>
                            <div class="col-sm-4 col-xs-4"></div>
                            <div class="col-sm-4 col-xs-4">
                                <?= Html::a('Отменить', null, [
                                    'class' => 'btn darkblue hidden-md hidden-sm hidden-xs close-modal-button',
                                    'style' => 'width: 100% !important',
                                ]); ?>
                                <?= Html::a('<i class="fa fa-floppy-o fa-2x"></i>', null, [
                                    'class' => 'btn darkblue hidden-lg close-modal-button',
                                    'title' => 'Отменить',
                                    'style' => 'width: 100% !important',
                                ]); ?>
                            </div>
                        </div>
                    </div>
                    <span>После оформления заказа, наш сотрудник свяжется с вами и обсудит детали.</span>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="tooltip-template hidden">
        <?php foreach ($groupArray as $group) : ?>
            <span id="tooltip_pay_<?= $group->id ?>" class="tooltip_pay_type" style="display: inline-block; text-align: center;">
                <span class="pay-tariff-wrap"><span class="text-bold">Тариф:</span> "<?= $group->name ?>"</span>
                <span class="pay-tariff-wrap"><span class="text-bold">Период:</span> <span class="pay-tariff-period"></span></span>
                <table class="mar-b-10">
                    <tr class="pay-tariff-wrap ">
                        <td style="vertical-align: top;">
                            <span class="text-bold">
                                За <span class="count-one">компанию</span><span class="count-many hidden">компании</span>:&nbsp;
                            </span>
                        </td>
                        <td class="pay-tariff-companies"></td>
                    </tr>
                </table>
                <span style="display: block;font-size: 16px;font-weight: bold;margin-bottom: 10px;">Способ оплаты</span>
                <?= Html::beginForm(['payment'], 'post', [
                    'class' => 'tariff-group-payment-form',
                ]) ?>
                    <?= Html::activeHiddenInput($model, 'tariffId', [
                        'class' => 'tariff-id-input',
                    ]) ?>
                    <?= Html::activeHiddenInput($model, 'companyList[]', [
                        'class' => 'company-list-input template',
                        'disabled' => true,
                    ]) ?>
                    <div style="height: 57px; overflow: hidden;">
                        <table style="margin: 0 auto;">
                            <tr>
                                <td valign="top" style="height: 57px; padding-right: 1px; o">
                                    <?= Html::button('Картой', [
                                        'class' => 'btn form-submit-button ladda-button',
                                        'data-name' => Html::getInputName($model, 'paymentTypeId'),
                                        'data-value' => PaymentType::TYPE_ONLINE,
                                        'data-style' => "expand-down",
                                        'style' => 'width: 140px; background: #45b6af;color: white;',
                                    ]); ?>
                                    <?php /*
                                    <?= Html::a('Картой', '#tooltip_robokassa_not_allowed', [
                                        'class' => 'btn',
                                        'style' => 'width: 140px; background: #45b6af;color: white;',
                                        'data-toggle' => 'modal',
                                        'onclick' => new JsExpression("$('.tooltip2-pay').tooltipster('close')")
                                    ]); ?>*/ ?>
                                </td>
                                <td valign="top" style="height: 57px; padding-left: 1px; o">
                                    <?= Html::button('Выставить счет', [
                                        'class' => 'btn form-submit-button ladda-button',
                                        'data-name' => Html::getInputName($model, 'paymentTypeId'),
                                        'data-value' => PaymentType::TYPE_INVOICE,
                                        'data-style' => "expand-down",
                                        'style' => 'width: 140px; background: #ffaa24;color: #ffffff;',
                                    ]); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                <?= Html::endForm() ?>
            </span>
        <?php endforeach; ?>

        <div id="tariff-details-pricelist">
            <?php
            $tariffList = SubscribeTariff::find()->actual()->andWhere([
                'tariff_group_id' => SubscribeTariffGroup::PRICE_LIST,
                'duration_month' => 1,
                'duration_day' => 0,
            ])->orderBy(['price' => SORT_ASC])->all();
            $icoPlus = '<span class="big-ico plus">+</span>';
            $icoMinus = '<span class="big-ico minus">—</span>';
            ?>
            <table class="table table-striped table-bordered mb-0">
                <thead>
                    <tr class="heading">
                        <th></th>
                        <?php foreach ($tariffList as $tariff) : ?>
                            <th class="pl-1" style="width: 20%"><?= $tariff->label ?></th>
                        <?php endforeach ?>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="">Количество прайс-листов</td>
                        <?php foreach ($tariffList as $id => $tariff): ?>
                            <td class="pl-2"><?= $tariff->tariff_limit ?: 'Безлимит' ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="">Количество товаров в прайс-листе</td>
                        <?php foreach ($tariffList as $id => $tariff): ?>
                            <?php $value = $tariff->getNamedParamValue('products_count', SubscribeTariffGroup::PRICE_LIST); ?>
                            <td class="pl-2"><?= $value ? ('до ' . $value): $icoMinus ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="">Онлайн обновления цены и остатков</td>
                        <?php foreach ($tariffList as $id => $tariff): ?>
                            <?php $value = $tariff->getNamedParamValue('has_auto_update', SubscribeTariffGroup::PRICE_LIST); ?>
                            <td class="pl-2"><?= $value ? $icoPlus : $icoMinus ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="">Просмотр карточки товара</td>
                        <?php foreach ($tariffList as $id => $tariff): ?>
                            <?php $value = $tariff->getNamedParamValue('has_product_view', SubscribeTariffGroup::PRICE_LIST); ?>
                            <td class="pl-2"><?= $value ? $icoPlus : $icoMinus ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="">Онлайн оформление заказов</td>
                        <?php foreach ($tariffList as $id => $tariff): ?>
                            <?php $value = $tariff->getNamedParamValue('has_checkout', SubscribeTariffGroup::PRICE_LIST); ?>
                            <td class="pl-2">
                                <?php if ($tariff->price > 0): ?>
                                    <?= $value ? $icoPlus : $icoMinus ?>
                                <?php else: ?>
                                    <?= PriceList::FREE_TARIFF_MAX_ORDERS_COUNT ?> заказов
                                <?php endif; ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="">Получение уведомлений</td>
                        <?php foreach ($tariffList as $id => $tariff): ?>
                            <?php $value = $tariff->getNamedParamValue('has_notification', SubscribeTariffGroup::PRICE_LIST); ?>
                            <td class="pl-2"><?= $value ? $icoPlus : $icoMinus ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="">Тариф "Выставление счетов"</td>
                        <?php foreach ($tariffList as $id => $tariff): ?>
                            <?php $tariffStandart = $tariff->containsStandartTariff; ?>
                            <td class="pl-1">
                                <?php if ($tariff->price > 0): ?>
                                    <?= $tariffStandart ? (($tariffStandart->tariff_limit ? ($tariffStandart->tariff_limit . ' счетов') : 'Безлимит') . ' в месяц') : $icoMinus ?>
                                <?php else: ?>
                                    <?= PriceList::FREE_TARIFF_MAX_ORDERS_COUNT ?> счетов
                                <?php endif; ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="confirm-modal fade modal" id="tooltip_robokassa_not_allowed" tabindex="-1" role="modal"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h2>Прием оплаты картой временно не доступен</h2>
                </div>
                <div class="modal-body">
                    На стороне «Робокасса» проводятся технические работы.<br/>
                    Приносим извинения за доставленные неудобства.<br/>
                    Производите оплату через выставление счета и оплату по банку. Присылайте платежку, по ней зачислим деньги.
                </div>
            </div>
        </div>
    </div>
<?php
$js = <<<JS
$(document).ready(function() {
    var paymentBox = $('#payment-box-1');
    var discountArray = eval(paymentBox.data('discounts'));
    var pricesArray = eval(paymentBox.data('prices'));
    var durationsArray = eval(paymentBox.data('durations'));
    var quantityArray = eval(paymentBox.data('quantity'));

    var tariffItemsBlock = $('#subscribe-tariff-items');
    var promocodeBlock = $('#promo-code-block');
    var currentTariffPrice = 0;

    var pracesByDiscount = function(price, discount) {
        return (price - (price * discount / 100));
    }

    var recalculatePraces = function() {
        var checkedCompany = $('input.company-id-checker:checked');

        $('.subscribe-tariff:not(.hidden)', tariffItemsBlock).each(function () {
            var tariffGroupBlock = $(this);
            var tariffCompanies = [];
            var tariffCompaniesList = '';
            var tariffPrice = 0;
            var tariffGroupId = parseInt(tariffGroupBlock.data('tariff-group-id'));
            var tariffId = parseInt(tariffGroupBlock.data('tariff-id'));
            var tariffBasePrice = pricesArray[tariffId];
            var tariffDuration = durationsArray[tariffId];
            var tariffQuantity= quantityArray[tariffId];
            var tooltip = $('#tooltip_pay_' + tariffGroupId);
            var companyListInput = $('input.company-list-input:disabled', tooltip);

            $('input.tariff-id-input', tooltip).val(tariffId);
            $('input.company-list-input:enabled', tooltip).remove();

            checkedCompany.each(function () {
                var company = $(this);
                var tariffIds = eval(company.data('tariffs'))
                if (tariffIds.indexOf(tariffId) != -1) {
                    tariffCompanies.push(company);
                }
            });
            var itemCount = tariffCompanies.length;

            if (itemCount > 0) {
                var discounts = discountArray[tariffId];
                tariffCompanies.forEach(function (company) {
                    var discountByTariff = eval(company.data('discounts'))[tariffId];
                    var discountByCount = 0;
                    Object.keys(discounts).forEach(function (key) {
                        if (key <= itemCount) {
                            discountByCount = discounts[key];
                        }
                    });
                    tariffCompaniesList += company.closest('label').text() + '<br/>';
                    tariffPrice += pracesByDiscount(tariffBasePrice, Math.max(discountByTariff, discountByCount));
                    companyListInput.clone().val(company.val()).prop('disabled', false).insertAfter(companyListInput);
                });
            }

            tariffPrice = Math.round(tariffPrice);

            tariffGroupBlock.data('tariff-price', tariffPrice);
            $('.current-tariff-price', this).text(tariffPrice);
            $('.current-tariff-price-per-month', this).text(Math.round(tariffPrice / tariffQuantity));
            $('.pay-tariff-period', tooltip).html(tariffGroupBlock.data('tariff-period'));
            $('.pay-tariff-companies', tooltip).html(tariffCompaniesList);
            $('.count-one', tooltip).toggleClass('hidden', itemCount > 1 ? true : false);
            $('.count-many', tooltip).toggleClass('hidden', itemCount > 1 ? false : true);
        });
    }
    var processingRequestData = function(data) {
        Ladda.stopAll();
        if (data.content) {
            $('#payment-box-2 .modal-body').html(data.content);
        } else if ($('#payment-box-2').is(':visible')) {
            $('#payment-box-2').modal('hide');
        }
        if (data.alert && typeof data.alert === 'object') {
            for (alertType in data.alert) {
                var offset = $('.navbar-fixed-top').outerHeight(true) +20;
                $.alert('#js-alert', data.alert[alertType], {type: alertType, topOffset: offset});
            }
        }
        if (data.done) {
            if (data.email && (data.type == '2' || data.type == '3')) {
                $("#send-to-email").html(data.email);
                $("#info-after-pay").modal("show");
            }
            if (data.link) {
                $('#document-print-link').attr('href', data.link);
                $('#document-print-link')[0].click();
            }
        }
    }

    $(document).on("click", ".choose-tariff", function(e) {
        e.preventDefault();
        var tariff = $(this);
        var tariff_id = tariff.data('tariff-id');
        var subscribeTariff = tariff.closest('.subscribe-tariff');
        subscribeTariff.data('tariff-id', tariff_id);
        subscribeTariff.data('tariff-period', tariff.html());

        tariff.closest('.dropdown').removeClass('open').find('.dropdown-toggle').html(tariff.html());

        $('.tariff-view-toggle', subscribeTariff).hide();
        $('.tariff-view-toggle[data-tariff-id="'+tariff_id+'"]', subscribeTariff).show();

        recalculatePraces();
    });

    $(document).on("change", "input.company-id-checker", function(e) {
        recalculatePraces();
    });

    $(document).on('click', '.subscribe-tariff .tariff', function (e) {
        var subscribeTariff = $(this).closest('.subscribe-tariff');

        $('.subscribe-tariff').removeClass('active');
        $(subscribeTariff).addClass('active');
    });

    $(document).on('submit', 'form.tariff-group-payment-form', function (e) {
        e.preventDefault();
    });

    $(document).on('click', 'form.tariff-group-payment-form .form-submit-button', function (e) {
        e.preventDefault();
        var button = $(this);
        if (button.attr("data-clicked")) {
            return false;
        }
        button.attr("data-clicked", true);
        setTimeout('button.removeAttr("data-clicked")', 3000);
        var form = button.closest('form');
        var formData = form.serializeArray();
        formData.push({ name: button.data('name'), value: button.data('value') });
        $.post(form.attr('action'), formData, function(data) {
            processingRequestData(data);
        });
    });

    $(document).on('click', 'a.tariff-toggle-link', function (e) {
        e.preventDefault();
        var toggleBox = $(this).closest('.dropdown');
        toggleBox.toggleClass('open', false);
        toggleBox.find('.dropdown-toggle').text($(this).text());
        $('li', toggleBox).toggleClass('active', false);
        $(this).closest('li').toggleClass('active', true);
        var target = $($(this).data('target'));
        var targetWrap = target.closest('.subscribe-tariff-content');
        $('.subscribe-tariff', targetWrap).toggleClass('hidden', true);
        target.toggleClass('hidden', false);
        recalculatePraces();
    });

    recalculatePraces();
});
JS;
$this->registerJs($js);
?>