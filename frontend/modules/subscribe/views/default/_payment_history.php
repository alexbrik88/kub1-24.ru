<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10/27/15
 * Time: 11:55 AM
 * Email: t.kanstantsin@gmail.com
 */

use common\components\grid\DropDownDataColumn;
use common\models\document\Act;
use common\models\service\Payment;
use common\models\service\PaymentType;
use frontend\modules\documents\components\FilterHelper;
use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var \yii\data\ActiveDataProvider $paymentDataProvider */
/* @var \frontend\modules\subscribe\models\PaymentSearch $paymentSearchModel */
?>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            История платежей
        </div>
    </div>
    <div class="portlet-body">
        <?= common\components\grid\GridView::widget([
            'dataProvider' => $paymentDataProvider,
            'filterModel' => $paymentSearchModel,

            'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '---'],
            'layout' => "{items}\n{pager}",
            'options' => [
                'class' => 'overflow-x',
            ],
            'tableOptions' => [
                'class' => 'table table-striped table-bordered table-hover dataTable customers_table overfl_text_hid',
                'role' => 'grid',
            ],
            'headerRowOptions' => [
                'class' => 'heading',
            ],
            'pager' => [
                'options' => [
                    'class' => 'pagination pull-right',
                ],
            ],

            'columns' => [
                [
                    'attribute' => 'payment_date',
                    'format' => 'datetime',
                    'headerOptions' => [
                        'style' => 'width: 25%;',
                    ],
                ],
                [
                    'attribute' => 'sum',
                    'headerOptions' => [
                        'style' => 'width: 15%;',
                    ],
                    'value' => function (Payment $model) {
                        return \common\components\TextHelper::moneyFormat($model->sum, 2);
                    },
                ],
                [
                    'attribute' => 'type_id',
                    'headerOptions' => [
                        'style' => 'width: 15%;',
                    ],
                    'filter' => FilterHelper::getFilterArray($paymentSearchModel->getTypeFilterItems(), 'id', 'name', true, false),
                    'value' => function (Payment $model) {
                        return $model->getPaymentName();
                    },
                ],
                [
                    'label' => 'Акт',
                    'headerOptions' => [
                        'style' => 'width: 10%;',
                    ],
                    'format' => 'html',
                    'value' => function ($model) {
                        if ($model->type_id == PaymentType::TYPE_INVOICE && $model->outInvoice && $model->outInvoice->acts) {
                            $list = array_map(function ($act) {
                                if (empty($act->uid)) {
                                    $act->updateAttributes(['uid' => Act::generateUid()]);
                                }

                                return Html::a("№ {$act->fullNumber}", [
                                    '/documents/act/out-view',
                                    'view' => 'pdf',
                                    'uid' => $act->uid,
                                ]);
                            }, $model->outInvoice->acts);

                            return implode(', ', $list);
                        }

                        return '---';
                    },
                ],
                [
                    'label' => 'Примечание',
                    'headerOptions' => [
                        'style' => 'width: 25%;',
                    ],
                    'value' => function ($model) {
                        if (in_array($model->type_id, [PaymentType::TYPE_RECEIPT, PaymentType::TYPE_INVOICE])) {
                            $text = '';
                            $date = new \DateTime("@$model->created_at");
                            if ($date) {
                                $text .= 'Отправлен' . ($model->type_id == PaymentType::TYPE_RECEIPT ? 'а ' : ' ');
                                $text .= 'на e-mail ' . $date->format('d.m.Y');
                            }

                            return $text;
                        }

                        return '';
                    },
                ],
            ],
        ]); ?>
    </div>
</div>
