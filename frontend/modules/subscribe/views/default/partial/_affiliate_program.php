<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 10.07.2017
 * Time: 18:33
 */

use common\components\ImageHelper;
use common\models\service\Subscribe;
use common\models\service\SubscribeTariffGroup;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var \yii\web\View $this */
/* @var common\models\Company $company */
/* @var common\models\service\SubscribeTariff[] $tariffArray */

$percent = Subscribe::REWARD_PERCENT;
?>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Партнерская программа - «Зарабатывай вместе с КУБом»
        </div>
        <div class="tools arrow-tools">
            <a href="javascript:;" class="expand checkall-slide" data-original-title="" title=""></a>
        </div>
    </div>
    <div class="portlet-body affiliate-program-subscribe" style="display: none;">
        <table
            class="table table-striped table-bordered table-hover dataTable customers_table overfl_text_hid"
            role="grid">
            <tbody>
            <tr>
                <td width="60%" style="padding-right: 8px!important;">
                    <ul class="affiliate-program-list p-l-0">
                        <li style="margin-bottom: 20px;">
                            Вы можете заработать, рекомендуя сервис КУБ.
                            Ваш заработок составит <?= $percent ?>%
                            от сумм оплаченных пользователями,
                            пришедшими по вашей рекомендации.
                            Заработанные деньги вы сможете получить на свой расчетный счет
                            или использовать для оплаты сервиса КУБ.
                        </li>
                        <?php foreach ($tariffArray as $tariff) : ?>
                            <?php if ($tariff->tariff_group_id == SubscribeTariffGroup::STANDART) : ?>
                                <?php
                                $sum = round($tariff->price * $percent / 100);
                                $period = Yii::$app->i18n->format(
                                    '{n, plural, one{# месяц} few{# месяца} many{# месяцев} other{# месяцев}}',
                                    ['n' => $tariff->duration_month],
                                    'ru_RU'
                                );
                                ?>
                                <li>
                                    Вы получите <?= $sum ?> руб. с каждой оплаты за <?= $period ?>.
                                </li>
                            <?php endif ?>
                        <?php endforeach; ?>
                        <li style="font-size: 14px;font-weight: bold;padding-top: 20px;">
                            Как начать зарабатывать?
                        </li>
                        <li>
                            Нажмите на кнопку ниже и появится ссылка.
                            Эта ссылка уникальная и привязана к вам.
                            Скопируйте эту ссылку и отправьте партнерам,
                            контрагентам и знакомым предпринимателям,
                            чтобы они перешли по ней на сервис КУБ и зарегистрировались.
                            Когда они начнут платить за КУБ, вы начнете зарабатывать.
                        </li>
                    </ul>
                </td>
                <td width="30%" style="padding-left: 8px!important;">
                    <ul class="affiliate-program-list p-l-0">
                        <li style="font-size: 14px;font-weight: bold;">
                            Ваша партнерская ссылка:
                        </li>
                        <li style="white-space: initial;">
                            <?php if ($company->affiliate_link): ?>
                                <?= Html::a(Yii::$app->params['serviceSite'] . '/registration?req=' . $company->affiliate_link,
                                    Yii::$app->params['serviceSite'] . '/registration?req=' . $company->affiliate_link, [
                                        'target' => '_blank',
                                        'class' => 'affiliate_invite',
                                    ]); ?>
                                <?= ImageHelper::getThumb('img/copy.png', [20, 20], [
                                    'class' => 'copy-affiliate-link',
                                    'title' => 'Скопировать ссылку',
                                ]); ?>
                            <?php else: ?>
                                <div class="form-actions">
                                    <div class="row action-buttons">
                                        <div class="col-sm-6 col-xs-6">
                                            <?= Html::a('Получить ссылку', 'javascript:;', [
                                                'class' => 'btn darkblue text-white widthe-100 hidden-md hidden-sm hidden-xs generate-affiliate-link',
                                                'data-url' => Url::to(['/company/generate-affiliate-link', 'page' => 'subscribe']),
                                            ]); ?>
                                            <?= Html::a('<i class="fa fa-floppy-o fa-2x"></i>', 'javascript:;', [
                                                'class' => 'btn darkblue text-white widthe-100 hidden-lg generate-affiliate-link',
                                                'title' => 'Получить ссылку',
                                                'data-url' => Url::to(['/company/generate-affiliate-link', 'page' => 'subscribe']),
                                            ]); ?>
                                        </div>
                                        <div
                                            class="col-sm-6 col-xs-6"></div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </li>
                        <li style="font-size: 14px;font-weight: bold;padding-top: 30px;">
                            Ваше
                            вознаграждение: <?= $company->affiliate_sum; ?>
                            руб.
                        </li>
                        <li>
                            Привлечено
                            пользователей: <?= $company->getInvitedCompanies()->count(); ?>
                        </li>
                        <li>
                            Пользователей
                            оплатило: <?= $company->getPayedInvitedCompaniesCount(); ?>
                        </li>
                        <li style="padding-top: 20px;">
                            <div class="form-actions">
                                <div class="row action-buttons">
                                    <div class="col-sm-6 col-xs-6">
                                        <?= Html::a('Выставить счет', null, [
                                            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                                            'disabled' => $company->affiliate_link ? false : true,
                                            'data-toggle' =>  $company->affiliate_link ? 'modal' : null,
                                            'data-target' =>  $company->affiliate_link ? '#bill-invoice-remuneration' : null,
                                        ]); ?>
                                        <?= Html::a('<i class="fa fa-floppy-o fa-2x"></i>', $company->affiliate_link ? 'javascript:;' : null, [
                                            'class' => 'btn darkblue widthe-100 hidden-lg',
                                            'title' => 'Выставить счет',
                                            'disabled' => $company->affiliate_link ? false : true,
                                            'data-toggle' =>  $company->affiliate_link ? 'modal' : null,
                                            'data-target' =>  $company->affiliate_link ? '#bill-invoice-remuneration' : null,
                                        ]); ?>
                                    </div>
                                    <div class="col-sm-6 col-xs-6"></div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
