<?php

use common\components\date\DateHelper;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\service\Subscribe;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\modules\subscribe\models\PaymentSearch;
use yii\db\Expression;
use common\models\company\ActiveSubscribe;
use common\components\helpers\ArrayHelper;

/** @var Company $company */
/** @var Company $cmp */

function getExpiresArray($company, $groupArray)
{
    $expiresArray = [];
    foreach ($groupArray as $group) {
        $subscribes = SubscribeHelper::getPayedSubscriptions($company->id, $group->id);
        if ($subscribes) {
            $activeSubscribe = ActiveSubscribe::findOne(['company_id' => $company->id, 'tariff_group_id' => $group->id]) ?: end($subscribes);
            $expiresArray[$group->id] = [
                'name' => $group->name,
                'date' => $date = SubscribeHelper::getExpireDate($subscribes),
                'days' => SubscribeHelper::getExpireLeftDays($date),
                'is_trial' => $group->id == SubscribeTariffGroup::STANDART && $activeSubscribe && $activeSubscribe->subscribe->tariff_id == SubscribeTariff::TARIFF_TRIAL,
                'is_promo' => $group->id == SubscribeTariffGroup::STANDART && $activeSubscribe && $activeSubscribe->subscribe->tariff_id == NULL,
                'is_free' => $group->id == SubscribeTariffGroup::STANDART && $company->isFreeTariff
            ];
        } elseif ($group->id == SubscribeTariffGroup::STANDART) {
            $expiresArray[$group->id] = [
                'name' => 'Бесплатно',
                'date' => 0,
                'days' => '',
                'is_trial' => false,
                'is_promo' => false,
                'is_free' => true
            ];
        }
    }

    return $expiresArray;
}

$ids = \Yii::$app->user->identity->getCompanies()->select('id')->column();

$paidCompanies = \Yii::$app->user->identity->getCompanies()
    ->alias('company')
    ->leftJoin(['companyType' => CompanyType::tableName()], "{{company}}.[[company_type_id]] = {{companyType}}.[[id]]")
    ->leftJoin(['companyActiveSubscribe' => ActiveSubscribe::tableName()], "{{company}}.[[id]] = {{companyActiveSubscribe}}.[[company_id]]")
    ->leftJoin(['subscribe' => Subscribe::tableName()], "{{companyActiveSubscribe}}.[[subscribe_id]] = {{subscribe}}.[[id]]")
    ->where(['{{companyActiveSubscribe}}.[[tariff_group_id]]' => ArrayHelper::getColumn($groupArray,'id')])
    ->andWhere(['NOT', ['{{subscribe}}.[[tariff_id]]' => SubscribeTariff::TARIFF_TRIAL]])
    ->andWhere(['IS NOT', '{{subscribe}}.[[tariff_id]]', NULL])
    ->isBlocked(false)
    ->all();

$paidCompanyArray = [];
foreach ($paidCompanies as $cmp) {

    $expiresArray = getExpiresArray($cmp, $groupArray);

    if (!$expiresArray[1]['is_free'] || count($expiresArray) > 1) {

        $paidCompanyArray[] = [
            'id' => $cmp->id,
            'name' => $cmp->getShortName(),
            'expires' => $expiresArray,
            'days_left' => (int)$expiresArray[SubscribeTariffGroup::STANDART]['days'],
            'invoices_limit' => $cmp->getInvoiceLeft(),
        ];
    }
}

$unpaidCompanies = \Yii::$app->user->identity->getCompanies()
    ->alias('company')
    ->leftJoin(['companyType' => CompanyType::tableName()], "{{company}}.[[company_type_id]] = {{companyType}}.[[id]]")
    ->isBlocked(false)
    ->where(['NOT IN', '{{company}}.[[id]]', ArrayHelper::getColumn($paidCompanyArray, 'id')])
    ->all();

$unpaidCompanyArray = [];
foreach ($unpaidCompanies as $cmp) {
    $expiresArray = getExpiresArray($cmp, $groupArray);

    if (count($expiresArray) > 1) {

        $paidCompanyArray[] = [
            'id' => $cmp->id,
            'name' => $cmp->getShortName(),
            'expires' => $expiresArray,
            'days_left' => (int)$expiresArray[SubscribeTariffGroup::STANDART]['days'],
            'invoices_limit' => $cmp->getInvoiceLeft(),
        ];

    } else {

        $unpaidCompanyArray[] = [
            'id' => $cmp->id,
            'name' => $cmp->getShortName(),
            'expires' => $expiresArray,
            'days_left' => (int)$expiresArray[SubscribeTariffGroup::STANDART]['days'],
            'invoices_limit' => $cmp->getInvoiceLeft(),
            'is_promo' => (int)$expiresArray[SubscribeTariffGroup::STANDART]['is_promo'],
        ];

    }
}

usort($paidCompanyArray, function ($item1, $item2) {
    return $item2['days_left'] <=> $item1['days_left'];
});

usort($unpaidCompanyArray, function ($item1, $item2) {
    return $item2['is_promo'] <=> $item1['is_promo'] ?: $item2['invoices_limit'] <=> $item1['invoices_limit'];
});
usort($unpaidCompanyArray, function ($item1, $item2) {
    return $item2['days_left'] <=> $item1['days_left'];
});

$companies = array_merge($paidCompanyArray, $unpaidCompanyArray);

// TEST
$pcIds = ArrayHelper::getColumn($paidCompanies, 'id');
$pcaIds = ArrayHelper::getColumn($paidCompanyArray, 'id');
$upcIds = ArrayHelper::getColumn($unpaidCompanies, 'id');
$upcaIds = ArrayHelper::getColumn($unpaidCompanyArray, 'id');
?>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Текущая подписка
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover dataTable customers_table overfl_text_hid">
            <thead>
                <tr class="heading">
                    <th>Наименование</th>
                    <th>Действующий тариф</th>
                    <th>Осталось дней</th>
                    <th>Активен до</th>
                    <th>Осталось (счетов)</th>
                    <?php if (Yii::$app->request->get('test')): ?>
                        <th>Id</th>
                        <th>paidCompanies</th>
                        <th>paidCompaniesArr</th>
                        <th>unpaidCompanies</th>
                        <th>unpaidCompaniesArr</th>
                    <?php endif; ?>
                </tr>
            </thead>
            <tbody>

                <?php foreach ($companies as $cmp): ?>
                    <?php $expiresArray = $cmp['expires']; ?>
                    <tr>
                        <td><?= $cmp['name'] ?></td>
                        <td>
                            <?php foreach ($cmp['expires'] as $group_id=>$exp): ?>
                                <?php if ($exp['is_free']): ?>
                                    Бесплатно
                                <?php elseif ($exp['is_trial']): ?>
                                    Пробный период
                                <?php else: ?>
                                    <?= $exp['name'] ?> <?= ($exp['is_promo'] ? '(промокод)' : '') ?>
                                <?php endif; ?>
                                <br/>
                            <?php endforeach; ?>
                        </td>
                        <td>
                            <?php foreach ($cmp['expires'] as $exp): ?>
                                <?= ($exp['days'] > 0) ? Yii::t('yii', '{n, plural, =0{# дня} 1{# день} one{# день} few{# дня} other{# дней}}', ['n' => $exp['days']]) : ''  ?>
                                <br/>
                            <?php endforeach; ?>
                        </td>
                        <td>
                            <?php foreach ($cmp['expires'] as $exp): ?>
                                <?= (!$exp['is_free']) ? date('d.m.Y', $exp['date']) : '' ?>
                                <br/>
                            <?php endforeach; ?>
                        </td>
                        <td>
                            <?= ($cmp['invoices_limit'] !== null) ? $cmp['invoices_limit'] : '' ?>
                        </td>

                        <?php if (Yii::$app->request->get('test')): ?>
                            <td><?= $cmp['id'] ?></td>
                            <td><?= in_array($cmp['id'], $pcIds) ? '+':'' ?></td>
                            <td><?= in_array($cmp['id'], $pcaIds) ? '+':'' ?></td>
                            <td><?= in_array($cmp['id'], $upcIds) ? '+':'' ?></td>
                            <td><?= in_array($cmp['id'], $upcaIds) ? '+':'' ?></td>
                        <?php endif; ?>

                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>