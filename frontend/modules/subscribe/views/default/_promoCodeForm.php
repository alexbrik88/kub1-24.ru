<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 11/16/15
 * Time: 4:16 PM
 * Email: t.kanstantsin@gmail.com
 */

/* @var \yii\web\View $this */
/* @var \frontend\modules\subscribe\forms\PromoCodeForm $promoCodeForm */

/* @var \yii\widgets\ActiveForm $form */
?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'activate-promo-code-pjax',
    'enablePushState' => false,
]); ?>

<?php $form = \yii\widgets\ActiveForm::begin([
    'id' => 'activate-promo-code-form',
    'method' => 'POST',
    'action' => ['activate-promo-code'],
    'options' => [
        'data-pjax' => true,
    ],
]); ?>
<div class="row act_promo_code_container">
    <div class="col-md-3 text-right error_msg">
        <?= \yii\helpers\Html::error($promoCodeForm, 'code', [
            'class' => 'text-danger text-left',
        ]); ?>
    </div>
</div>

<div class="row" style="margin: 0;">
    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12" style="float: right;">
        <?= \yii\helpers\Html::submitButton('Активировать', [
            'style' => 'display: block; width: 100%;',
            'class' => 'btn btn-large yellow',
        ]); ?>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12" style="float: right; margin: 0;">
        <?= $form->field($promoCodeForm, 'code', [
            'template' => "{label}\n{input}\n{hint}",
            'options' => [
                'class' => 'form-group pro-cod_input',
            ],
        ])->label(false)->textInput([
            'placeholder' => 'Введите промокод',
        ]); ?>
    </div>
</div>
<?php $form->end(); ?>

<?php \yii\widgets\Pjax::end(); ?>
