<?php

use common\components\date\DateHelper;
use common\components\helpers\Html;
use common\models\company\CompanyType;
use common\models\service\PaymentType;
use common\models\service\SubscribeTariff;
use frontend\models\AffiliateProgramForm;
use frontend\modules\subscribe\forms\PaymentForm;
use yii\bootstrap\ActiveForm;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\Pjax;

$paymentMethodForm = new PaymentForm($company);
$paymentMethodForm->load(\Yii::$app->request->post());
$paymentMethodForm->tariffId = $tariffId;

$userCompanyArray = \Yii::$app->user->identity->getCompanies()
    ->alias('company')
    ->leftJoin(['companyType' => CompanyType::tableName()], "{{company}}.[[company_type_id]] = {{companyType}}.[[id]]")
    ->isBlocked(false)
    ->orderBy([
        new Expression('IF({{company}}.[[id]] = :id, 0, 1) ASC'),
        new \yii\db\Expression("ISNULL({{companyType}}.[[name_short]])"),
        "companyType.name_short" => SORT_ASC,
        "company.name_short" => SORT_ASC,
    ])
    ->params([':id' => $company->id])
    ->all();

$userCompanyList = ArrayHelper::map($userCompanyArray, 'id', function ($model) use ($tariffId) {
    $discount = $model->getDiscount($tariffId);
    return $model->shortName . ($discount ? " <span style=\"font-weight: bold; color: red;\">Скидка -{$discount}%</span>" : '');
});
$companyDiscount = ArrayHelper::map($userCompanyArray, 'id', function ($model) use ($tariffId) {
    return $model->getDiscount($tariffId);
});

$paymentTypeArray = [
    [
        'id' => PaymentType::TYPE_ONLINE,
        'title' => 'Онлайн оплата',
        'view' => 'payment_type/online',
    ],
    [
        'id' => PaymentType::TYPE_INVOICE,
        'title' => 'Выставить счёт',
        'view' => 'payment_type/take_invoice',
    ],
    [
        'id' => PaymentType::TYPE_REWARD,
        'title' => 'Использовать вознаграждение',
        'view' => 'payment_type/reward',
    ],
];

?>
<?php $pjax = Pjax::begin([
    'id' => 'subscribe-tariff-block-pjax',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'linkSelector' => false,
]); ?>

<?php $form = ActiveForm::begin([
    'id' => 'subscribe-tariff-form',
    'action' => ['payment'],
    'validationUrl' => ['validate'],
    'enableAjaxValidation' => true,
]); ?>

<?= Html::activeHiddenInput($paymentMethodForm, 'tariffId', [
    'id' => 'payment-method-tariff',
]); ?>
    <div class="col-sm-12">
        <?= $form->field($paymentMethodForm, 'companyList', [
            'template' => "{label}\n<br>\n{input}\n{hint}\n{error}",
            'options' => [
                'style' => 'margin-top: 10px;',
            ],
            'labelOptions' => [
                'class' => 'subscribe-form-label',
            ],
        ])->checkboxList($userCompanyList, [
            'style' => 'max-height: 120px; overflow-y: auto;',
            'item' => function ($index, $label, $name, $checked, $value) use ($companyDiscount) {
                return Html::tag('label', Html::checkbox($name, $checked, [
                    'value' => $value,
                    'class' => 'company-id-checker',
                    'data-discount' => $companyDiscount[$value],
                ]) . ' ' . $label, [
                    'style' => 'display: block;',
                ]);
            },
        ]); ?>

        <p>
            Сумма к оплате:
            <span class="subscribe-tariff-price font-bold btn-success sum_btn"></span>
            <span class="subscribe-selected-companies"></span>
        </p>

        <p>Выбор способа оплаты:</p>
    </div>

    <div class="col-sm-12">
        <div class="row">

            <?php foreach ($paymentTypeArray as $paymentType) : ?>
                <?php $inputId = 'payment-type-' . $paymentType['id']; ?>
                <div class="payment-type col-md-3 col-sm-3">
                    <div class="bg_payment">
                        <div class="title">
                            <?= Html::label('<span class="ladda-label">' . $paymentType['title'] . '</span><span class="ladda-spinner"></span>', $inputId, [
                                'class' => 'payment-type-choose btn btn-large btn-success ladda-button',
                                'data-style' => 'expand-right',
                                'data-id' => $paymentType['id'],
                            ]); ?>
                            <?= Html::radio(Html::getInputName($paymentMethodForm, 'paymentTypeId'), false, [
                                'id' => $inputId,
                                'value' => $paymentType['id'],
                                'class' => 'hide payment-type-radio',
                            ]); ?>
                        </div>

                        <div class="payment-type_text_container">
                            <?= $this->render($paymentType['view'], [
                                'form' => $form,
                                'paymentMethodForm' => $paymentMethodForm,
                            ]); ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

<?php $form->end(); ?>

<?php $pjax->end(); ?>