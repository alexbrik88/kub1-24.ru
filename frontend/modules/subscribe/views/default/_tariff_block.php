<?php

use common\models\service\SubscribeTariffGroup;
use frontend\components\Icon;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var \yii\web\View $this */
/* @var \common\models\Company $company */
/* @var \common\models\service\SubscribeTariffGroup $group */
/* @var string $icon */

$tariffArray = $group->actualTariffs;
$tariffData = [];
$dropdownData = [];
$dropdownItems = [];
if (in_array($group->id, SubscribeTariffGroup::$hasLimit)) {
    foreach ($tariffArray as $tariff) {
        $dropdownLabel = $tariff->label ? 'Тариф '.$tariff->label : $group->limitLabel($tariff->tariff_limit);
        $key = $group->id.'-'.yii\helpers\Inflector::slug($dropdownLabel);
        $tariffData[$key][] = $tariff;
        if (!isset($dropdownData[$key])) {
            $dropdownData[$key] = $dropdownLabel;
        }
    }
} else {
    $tariffData['tariff_tab_'.$group->id] = $tariffArray;
}

foreach ($dropdownData as $key => $label) {
    $dropdownItems[] = [
        'label' => $label,
        'url' => '#',
        'linkOptions' => [
            'class' => 'tariff-toggle-link',
            'data-target' => '#'.$key,
        ]
    ];
}
$isTabActive = true;
?>

<?php if ($tariffArray && $group->activeByCompany($company)) : ?>
    <div class="col-md-5ths">
        <div class="subscribe-tariff-box">
            <div class="bordered" style="height: 190px; position: relative;">
                <div class="tariff-icon">
                    <img src="<?= $icon ? '/img/icons/'. $icon : ''; ?>" style="max-width: 100%; max-height: 100%;"/>
                </div>
                <div class="tariff-group-name">
                    <?= implode('<br>ИП', explode(' ИП', $group->name)) ?>
                    <?php if ($group->hint) : ?>
                        <div style="font-size: 13px; font-weight: normal; text-transform: none; line-height: normal;">
                            <?= $group->hint ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div style="position: absolute; bottom: 10px;">
                    <?php if ($dropdownItems) : ?>
                        <div class="dropdown">
                            <?= Html::tag('div', $dropdownItems[0]['label'], [
                                'class' => 'dropdown-toggle',
                                'data-toggle' => 'dropdown',
                                'style' => 'display: inline-block; border-bottom: 1px dashed #000; cursor: pointer;',
                            ])?>
                            <?php if ($group->id == SubscribeTariffGroup::PRICE_LIST) : ?>
                                <?= Icon::get('question', [
                                    'class' => 'tooltip-question-icon ml-1 tooltip-click',
                                    'data-tooltip-content' => '#tariff-details-pricelist'
                                ]) ?>
                            <?php endif ?>
                            <?= \yii\bootstrap\Dropdown::widget([
                                'items' => $dropdownItems,
                            ])?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

            <div class="subscribe-tariff-content">
                <?php foreach ($tariffData as $tab => $tariffArray) : ?>
                    <?php
                    $tariff = reset($tariffArray);
                    $discount = $company->getDiscount($tariff->id);
                    $price = round($tariff->price - $tariff->price * $discount / 100);
                    $tariffDropdown = [];
                    foreach ($tariffArray as $t) {
                        $tariffDropdown[] = [
                            'label' => $t->getReadableDuration(),
                            'url' => '#',
                            'linkOptions' => [
                                'class' => 'choose-tariff',
                                'data' => [
                                    'tariff-id' => $t->id,
                                ],
                            ],
                        ];
                    }
                    ?>
                    <?= Html::beginTag('div', [
                        'id' => $tab,
                        'class' => 'subscribe-tariff' . ($isTabActive ? '': ' hidden'),
                        'data' => [
                            'tariff-id' => $tariff->id,
                            'tariff-group-id' => $group->id,
                            'tariff-price' => $tariff->price,
                            'tariff-period' => $tariff->getReadableDuration(),
                            'companies' => '',
                        ],
                    ]) ?>

                        <div class="bordered" style="height: 110px; padding: 10px 15px;">
                            <?php if ($group->id != SubscribeTariffGroup::STANDART) : ?>
                                <?php foreach ($tariffArray as $key=>$t): ?>
                                    <div class="tariff-view-toggle" data-tariff-id="<?= $t->id ?>"
                                        style="<?= ($key>0) ? 'display:none;' : '' ?>">
                                        <?php if ($st = $t->containsStandartTariff) : ?>
                                            Тариф "Выставление счетов"<br/>
                                            <?= $st->getLimitText() ?><br/>
                                            <strong>ВКЛЮЧЕН</strong> в стоимость<br/>
                                            этого тарифа
                                        <?php else : ?>
                                            Тариф "Выставление счетов"<br/>
                                            <strong>НЕ включен</strong> в стоимость<br/>
                                            этого тарифа
                                        <?php endif; ?>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                        <div class="bordered" style="min-height: 176px;">
                            <div class="tariff-options">
                                <div style="display: inline-block;">Оплата за </div>
                                <div style="display: inline-block; width: 100px;">
                                    <?php if (count($tariffDropdown) == 1) : ?>
                                        <?= $tariffDropdown[0]['label'] ?>
                                    <?php else : ?>
                                        <div class="dropdown">
                                            <?= Html::tag('div', $tariffDropdown[0]['label'], [
                                                'class' => 'dropdown-toggle',
                                                'data-toggle' => 'dropdown',
                                                'style' => 'display: inline-block; border-bottom: 1px dashed #000; cursor: pointer;',
                                            ])?>
                                            <?= \yii\bootstrap\Dropdown::widget([
                                                'id' => 'employee-rating-dropdown',
                                                'encodeLabels' => false,
                                                'items' => $tariffDropdown,
                                            ])?>
                                        </div>
                                    <?php endif ?>
                                </div>
                            </div>

                            <div class="tariff-price" data-tariff-price="<?= $price ?>" data-tariff-id="<?= $tariff->id ?>">
                                <?php if ($discount) : ?>
                                    <span style="text-decoration: line-through;">
                                        <?= $tariff->price ?>
                                        ₽
                                    </span>
                                    <br>
                                    <span class="current-tariff-price">
                                        <?= $price ?>
                                    </span>
                                <?php else : ?>
                                    <span class="current-tariff-price">
                                        <?= $tariff->price ?>
                                    </span>
                                <?php endif ?>
                                ₽
                            </div>
                            <div class="tariff-price-per-month">
                                <span class="current-tariff-price-per-month">
                                    <?= round($price / $tariff->duration_month) ?>
                                </span>
                                ₽ / <?= $group->perOneLabel() ?>
                            </div>
                            <?php foreach ($tariffArray as $key => $t) : ?>
                                <div class="tariff-view-toggle tariff-discount-proposition" data-tariff-id="<?= $t->id ?>" <?= $key ? ' style="display:none"' : ''; ?>>
                                    <?= $t->proposition ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="bordered">
                            <div class="tariff-description">
                                <?php foreach (explode('|', $group->description) as $string) : ?>
                                    <p><?= $string ?></p>
                                <?php endforeach; ?>
                                <div class="text-opacity"></div>
                            </div>
                            <div style="text-align: center">
                                <div class="tariff-read-more" data-toggle="modal" data-target="#tariff-info-<?= $group->id ?>">Подробнее</div>
                                <?= Html::tag('button', 'Оплатить', [
                                    'class' => 'tariff btn yellow but_weight tooltip2-pay tariff-pay-btn',
                                    'data-tooltip-content' => '#tooltip_pay_'.$group->id,
                                ]) ?>
                            </div>
                        </div>

                    <?= Html::endTag('div') ?> <!-- .subscribe-tariff -->

                    <?php $isTabActive = false ?>
                <?php endforeach ?>
            </div>
        </div>
    </div>
<?php endif ?>
