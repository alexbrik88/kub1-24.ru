<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 11/23/15
 * Time: 4:37 PM
 * Email: t.kanstantsin@gmail.com
 */

namespace frontend\modules\subscribe\widgets;


use common\components\date\DateHelper;
use common\models\service\SubscribeHelper;
use frontend\widgets\ModalWidget;
use yii\base\InvalidConfigException;
use yii\bootstrap\Widget;

/**
 * Class ReminderWidget
 * @package frontend\modules\subscribe\widgets
 */
class ReminderWidget extends Widget
{
    public $isTrial;

    /**
     * @var
     */
    public $expireDate;

    /**
     * @var
     */
    public $expireDays;

    /**
     * Days before subscription ends to show reminder
     * @var int
     */
    public $remindStart = 10;
    /**
     * Days before subscription ends to show __colorized__ reminder
     * @var int
     */
    public $remindWarningStart = 5;

    /**
     * Key in session to check, if reminder was already shown
     * @var string
     */
    public $sessionKey = 'subscribe-reminder';

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if ($this->expireDate === null) {
            throw new InvalidConfigException('Expire date must be defined.');
        }

        $this->expireDays = SubscribeHelper::getExpireLeftDays($this->expireDate);
    }

    /**
     * @inheritdoc
     * @throws \Exception
     */
    public function run()
    {
        if ($this->expireDate === false) {
            return null;
        }

        if ($this->checkSession()) {
            return null;
        }

        $expireDays = $this->expireDays;

        switch (true) {
            case $this->remindStart >= $expireDays && $expireDays > $this->remindWarningStart:
                $reminderBackground = 'default';
                break;
            case $this->remindWarningStart >= $expireDays && $expireDays > 0:
                $reminderBackground = 'warning';
                break;
            default:
                // 0 or more than {remindStart} days - no warning needed.
                return null;
        }

        return $this->render('reminder', [
            'isTrial' => $this->isTrial,
            'expireDate' => date(DateHelper::FORMAT_USER_DATE, $this->expireDate),
            'expireDays' => $expireDays,

            'reminderBackground' => $reminderBackground,
        ]);
    }

    /**
     * Check if reminder was shown
     * @return bool
     */
    private function checkSession()
    {
        $newValue = $this->expireDays;
        $sessionValue = \Yii::$app->session->get($this->sessionKey);

        if ($newValue === $sessionValue) {
            return true;
        } else {
            \Yii::$app->session->set($this->sessionKey, $newValue);

            return false;
        }
    }
}
