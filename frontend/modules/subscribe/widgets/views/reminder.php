<?php

use common\components\ImageHelper;
use php_rutils\RUtils;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var boolean $isTrial */
/* @var string $reminderBackground */
/* @var string $expireDate */
/* @var int $expireDays */

$variants = ['день', 'дня', 'дней'];
?>

<?php Modal::begin([
    'id' => 'service-reminder-trial',
    'header' => Html::tag('h1', 'НАПОМИНАНИЕ'),
    'options' => [
        'style' => ''
    ]
]); ?>
    <div class="form-group text-center">
        <?= Html::img('/images/reminder.png', [
            'alt' => '',
            'style' => 'max-width: 100%;',
        ]); ?>
    </div>
    <div class="mb-5 text-center" style="font-size: 14px;">
        <strong>
            Через
            <span class="days-left">
                <?= RUtils::numeral()->getPlural($expireDays, $variants) ?>
            </span>
            ваш пробный период закончится.
        </strong>
        <br>
        Для подолжения работы без ограничений,
        <br>
        сделайте оплату сегодня
    </div>
    <div class="mb-5 text-center">
        <?= Html::a('Перейти к оплате', [
            '/subscribe/default/index',
        ], [
            'class' => 'button-regular button-regular_red',
            'style' => 'width: 250px;'
        ]); ?>
    </div>
<?php Modal::end(); ?>

<?php
$js = <<<JS
    $('#service-reminder-trial').modal('show');
JS;
$this->registerJs($js);