<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 11/18/15
 * Time: 1:17 PM
 * Email: t.kanstantsin@gmail.com
 */

/* @var \yii\web\View $this */
/* @var \common\models\service\Subscribe $subscribe */
/* @var \common\models\document\Invoice $invoice */

?>
Платеж на сумму <?= $data['sum'] ?> руб. успешно зачислен!

Ваша подписка активирована.
Для использования сервиса КУБ, пройдите в личный кабинет.


С уважением,
Команда КУБ
