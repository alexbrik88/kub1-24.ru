<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 11/18/15
 * Time: 1:17 PM
 * Email: t.kanstantsin@gmail.com
 */

/* @var \yii\web\View $this */
/* @var \common\models\service\Subscribe $subscribe */

$printUrl = \Yii::$app->urlManager->createAbsoluteUrl([
    Yii::getAlias('@subscribeUrl/default/pdf-receipt'), 'actionType' => 'print', 'subscribeId' => $subscribe->id,
]);
$pdfUrl = \Yii::$app->urlManager->createAbsoluteUrl([
    Yii::getAlias('@subscribeUrl/default/pdf-receipt'), 'actionType' => 'pdf', 'subscribeId' => $subscribe->id,
]);
?>

Квитанция Сбербанка на оплату подписки сервиса kub-24.
<br/>
<br/>

