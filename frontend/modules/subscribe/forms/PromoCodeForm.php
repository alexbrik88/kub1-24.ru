<?php

/**
 * Created by Konstantin Timoshenko
 * Date: 10/26/15
 * Time: 5:58 PM
 * Email: t.kanstantsin@gmail.com
 */

namespace frontend\modules\subscribe\forms;


use common\components\date\DateHelper;
use common\models\Company;
use common\models\selling\SellingSubscribe;
use common\models\service\Payment;
use common\models\service\PaymentType;
use common\models\service\PromoCode;
use common\models\service\PromoCodeGroup;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use Yii;
use yii\base\Model;
use yii\db\Expression;
use yii\web\NotFoundHttpException;

/**
 * Class PromoCodeForm
 * @package frontend\modules\subscribe\forms
 */
class PromoCodeForm extends Model
{

    /**
     * @var string
     */
    public $code;

    /**
     * @var PromoCode
     */
    public $promoCode;

    /**
     * @var Company
     */
    public $company;

    /**
     * @var Subscribe
     */
    public $subscribe;

    /**
     * @var Subscribe
     */
    protected $payment;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code'], 'trim'],
            [['code'], 'required'],
            [['code'], function ($attribute) {
                if ($this->promoCode === null || !($this->promoCode instanceof PromoCode)
                    || ($this->promoCode->for_company_experience_id !== null
                        && $this->promoCode->for_company_experience_id != $this->company->experience_id)
                ) {
                    $this->addError($attribute, 'Промокод не найден.');

                    return;
                }
                $this->company->created_at = time()- (60*60*24);

                if ($this->promoCode->group_id == PromoCodeGroup::GROUP_INDIVIDUAL) {
                    if ($this->promoCode->is_used) {
                        $this->addError($attribute, 'Промокод уже использован.');

                        return;
                    }
                } else {
                    $paymentCount = Payment::find()
                        ->byPromoCode($this->promoCode->id)
                        ->byCompany($this->company->id)
                        ->count();

                    if ($paymentCount === false) {
                        $this->addError($attribute, 'Ошибка при выполнении запроса.');

                        return;
                    }

                    if ((int)$paymentCount > 0) {
                        $this->addError($attribute, 'Вы использовали уже этот промокод.');

                        return;
                    }
                }

                if ($this->promoCode->days_from_registration) {
                    $expires = date_create()
                        ->setTimestamp($this->company->created_at)
                        ->modify("today +{$this->promoCode->days_from_registration} days")
                        ->getTimestamp();

                    if ($expires <= time()) {
                        $this->addError($attribute, 'Время активации промокода истекло.');

                        return;
                    }
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => 'Промокод'
        ];
    }

    /**
     * @inheritdocl
     */
    public function beforeValidate()
    {
        $this->promoCode = PromoCode::find()
            ->isStarted(true)
            ->isExpired(false)
            ->byCode($this->code)
            ->one();

        return parent::beforeValidate();
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        return Yii::$app->db->transaction(function ($db) {
            if ($this->_save()) {
                return true;
            }

            $db->transaction->rollBack();

            return false;
        });
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    protected function _save()
    {
        $this->payment = new Payment([
            'company_id' => $this->company->id,
            'payment_date' => time(),
            'invoice_number' => null,
            'sum' => (string)0,
            'type_id' => PaymentType::TYPE_PROMO_CODE,
            'promo_code_id' => $this->promoCode->id,
        ]);
        if ($this->promoCode->type == PromoCode::TYPE_CABINET) {
            $this->payment->payment_for = Payment::FOR_STORE_CABINET;
            $this->payment->is_confirmed = true;
        } elseif ($this->promoCode->type == PromoCode::TYPE_OUT_INVOICE) {
            $this->payment->payment_for = Payment::FOR_OUT_INVOICE;
            $this->payment->is_confirmed = true;
        }

        if ($this->promoCode->updateAttributes([
                'is_used' => true,
                'activated_at' => $this->promoCode->activated_at ?: time(),
                'used_count' => new Expression('used_count + 1'),
            ])
            && $this->payment->save()
        ) {
            if ($this->promoCode->type == PromoCode::TYPE_SUBSCRIBE) {
                if (!$this->createSubscribe()) {
                    return false;
                }
            }

            $sellingSubscribe = new SellingSubscribe();
            $sellingSubscribe->company_id = $this->company->id;
            if ($this->promoCode->code == '77777') {
                $sellingSubscribe->type = SellingSubscribe::TYPE_PROMO_CODE_77777;
            } else {
                if ($this->promoCode->group_id == PromoCodeGroup::GROUP_INDIVIDUAL) {
                    $sellingSubscribe->type = SellingSubscribe::TYPE_PROMO_CODE_INDIVIDUAL;
                } else {
                    $sellingSubscribe->type = SellingSubscribe::TYPE_PROMO_CODE_GROUPED;
                }
            }
            $sellingSubscribe->main_type = SellingSubscribe::TYPE_MAIN_FREE;
            $sellingSubscribe->invoice_number = null;
            $creationDate = $this->promoCode->type == PromoCode::TYPE_SUBSCRIBE ? $this->subscribe->created_at : time();
            $activationDate = $this->promoCode->type == PromoCode::TYPE_SUBSCRIBE ? $this->subscribe->activated_at : $creationDate;
            if ($this->promoCode->type == PromoCode::TYPE_SUBSCRIBE) {
                $endDate = $this->subscribe->expired_at;
            } else {
                $date = new \DateTime();
                $date->setTimestamp($activationDate);
                $date->modify("-1 day");
                $date->setTime(23, 59, 59);

                if ($this->promoCode->duration_month) {
                    $date->modify("+{$this->promoCode->duration_month} month");
                }
                if ($this->promoCode->duration_day) {
                    $date->modify("+{$this->promoCode->duration_day} day");
                }
                $endDate = $date->getTimestamp();
            }
            $sellingSubscribe->creation_date = $creationDate;
            $sellingSubscribe->activation_date = $activationDate;
            $sellingSubscribe->end_date = $endDate;
            if ($sellingSubscribe->save()) {
                return true;
            }
        }

        return false;
    }

    /**
     *
     */
    protected function createSubscribe()
    {
        $this->subscribe = new Subscribe([
            'company_id' => $this->company->id,
            'payment_id' => $this->payment->id,
            'tariff_group_id' => $this->promoCode->tariff_group_id,
            'tariff_limit' => $this->promoCode->tariff_limit,
            'tariff_id' => null,
            'duration_month' => $this->promoCode->duration_month,
            'duration_day' => $this->promoCode->duration_day,
            'status_id' => SubscribeStatus::STATUS_PAYED,
        ]);
        $subscribeArray = [$this->subscribe];

        if ($tariffGroup = $this->promoCode->tariffGroup) {
            $tariff = $tariffGroup->getActualTariffs()->orderBy([
                'price' => SORT_ASC,
            ])->one();
            if ($tariff) {
                foreach ($tariff->containsTariffs as $containsTariff) {
                    $subscribeArray[] = new Subscribe([
                        'company_id' => $this->company->id,
                        'payment_id' => $this->payment->id,
                        'tariff_group_id' => $containsTariff->tariff_group_id,
                        'tariff_limit' => $containsTariff->tariff_limit,
                        'tariff_id' => $containsTariff->id,
                        'duration_month' => $this->promoCode->duration_month,
                        'duration_day' => $this->promoCode->duration_day,
                        'status_id' => SubscribeStatus::STATUS_PAYED,
                    ]);
                }
            }
        }

        foreach ($subscribeArray as $item) {
            if (!$item->save()) {
                \common\components\helpers\ModelHelper::logErrors($item, __METHOD__);

                return false;
            }
        }

        foreach ($subscribeArray as $item) {
            $this->company->getHasActualSubscription($item->tariff_group_id, true);
        }

        return true;
    }
}
