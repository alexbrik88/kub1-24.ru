<?php

namespace frontend\modules\subscribe\forms;

use common\models\Company;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\service\Payment;
use common\models\service\SubscribeHelper;
use yii\base\Exception;
use yii\base\Model;
use Yii;

/**
 * Class PaymentForm
 * @package frontend\modules\subscribe\forms
 *
 * @property string $merchantLogin
 * @property string $merchantPassword
 * @property string $merchantPassword2
 */
class OnlinePaymentForm extends Model
{
    const SCENARIO_SEND = 'send';
    const SCENARIO_RESULT = 'result';
    const SCENARIO_SUCCESS = 'success';

    public static $isConfirmedByScenario = [
        self::SCENARIO_RESULT => true,
        self::SCENARIO_SUCCESS => false,
    ];

    /**
     * @var Payment
     */
    public $payment;
    /**
     * @var Company
     */
    public $company;
    /**
     * @var Employee
     */
    public $user;

    /**
     * @var string
     */
    public $SignatureValue;

    /**
     * @var integer
     */
    public $InvId;
    /**
     * @var integer
     */
    public $OutSum;
    /**
     * @var string
     */
    public $Desc;
    /**
     * @var boolean
     */
    public $IsTest;
    /**
     * @var string
     */
    public $Culture;

    /**
     * @var integer
     */
    public $Shp_employeeId;
    /**
     * @var integer
     */
    public $Shp_companyId;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->getScenario() === static::SCENARIO_SEND) {
            if ($this->company === null) {
                throw new Exception('Компания не указана');
            }
            if ($this->user === null) {
                throw new Exception('Пользователь не указан');
            }
            if ($this->payment === null) {
                throw new Exception('Платеж не указан');
            }

            $this->InvId = $this->payment->id;
            $this->OutSum = $this->payment->sum;
            $this->Shp_employeeId = $this->user->id;
            $this->Shp_companyId = $this->company->id;
        } elseif ($this->getScenario() === static::SCENARIO_SUCCESS) {
            if ($this->company === null) {
                throw new Exception('Компания не указана');
            }
            if ($this->user === null) {
                throw new Exception('Пользователь не указан');
            }
        } elseif ($this->getScenario() === static::SCENARIO_RESULT) {
        }
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_SEND => [],
            self::SCENARIO_RESULT => [
                'InvId', 'OutSum', 'Shp_employeeId', 'Shp_companyId', 'SignatureValue',
                'Desc', 'IsTest', 'Culture',
            ],
            self::SCENARIO_SUCCESS => [
                'InvId', 'OutSum', 'Shp_employeeId', 'Shp_companyId', 'SignatureValue',
                'Desc', 'IsTest', 'Culture',
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['InvId', 'OutSum', 'Shp_employeeId', 'Shp_companyId', 'SignatureValue'], 'string'],
            [['SignatureValue'], function ($attribute) {
                if (strtoupper($this->SignatureValue) != strtoupper($this->createSignature())) {
                    $this->addError($attribute, 'Контрольная сумма (подпись) составлена не верно.');
                }
            }],
            [['InvId'], function ($attribute) {
                if ($this->payment === null) {
                    $this->addError($attribute, 'Подписка не найдена');
                } else {
                    if ($this->payment->company_id != (int) $this->Shp_companyId) {
                        $this->addError($attribute, 'Заказ не соответствует указанной компании.');
                    }
                }
            }],
            [['Shp_employeeId'], function ($attribute) {
                if ($this->user === null) {
                    $this->addError($attribute, 'Пользователь не найден');
                } else {
                    if (!EmployeeCompany::find()->where([
                        'employee_id' => $this->Shp_employeeId,
                        'company_id' => $this->Shp_companyId,
                    ])->exists()) {
                        $this->addError($attribute, 'Пользователь относится к другой компании.');
                    }
                }
            }],
            [['Shp_companyId'], function ($attribute) {
                if ($this->company === null) {
                    $this->addError($attribute, 'Компания не найдена');
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (!parent::beforeValidate()) {
            return false;
        }

        if ($this->getScenario() === static::SCENARIO_RESULT) {
            $this->setPaymentModel();
            $this->setCompanyModel();
            $this->setUserModel();
        } elseif ($this->getScenario() === static::SCENARIO_SUCCESS) {
            $this->setPaymentModel();
        }

        return true;
    }

    /**
     * Generates all fields needed in payment request
     * @return array
     */
    public function getFormFields()
    {
        return [
            'MrchLogin' => $this->merchantLogin,
            'OutSum' => $this->OutSum,
            'InvId' => $this->InvId,
            'Desc' => $this->getDesc(),
            'SignatureValue' => $this->createSignature(),
            'Culture' => $this->getCulture(),
            'IsTest' => $this->getIsTest(),
            // 'Recurring' => $this->autoPayment,

            // additional fields. MUST be in alphabetical order
            'Shp_companyId' => $this->Shp_companyId,
            'Shp_employeeId' => $this->Shp_employeeId,
        ];
    }

    /**
     * @return string
     */
    public function getDesc()
    {
        if ($this->getScenario() == self::SCENARIO_SEND) {
            if ($this->payment->payment_for == Payment::FOR_TAXROBOT) {
                return 'логин ' . $this->user->email . ', за расчет налога и платежку на уплату налога.';
            } elseif ($this->payment->payment_for == Payment::FOR_STORE_CABINET) {
                $tariff = $this->payment->storeTariff;
                return 'логин ' . $this->user->email . ', за пользование сервисом "КУБ", www.store.kub-24.ru количество кабинетов ' . $tariff->cabinets_count;
            } elseif ($this->payment->payment_for == Payment::FOR_OUT_INVOICE) {
                return 'логин ' . $this->user->email . ', за пользование сервисом "КУБ", www.kub-24.ru количество ссылок ' . $this->payment->outInvoiceTariff->links_count;
            } elseif ($this->payment->payment_for == Payment::FOR_ODDS) {
                return 'логин ' . $this->user->email . ', за пользование сервисом "КУБ", www.kub-24.ru. Настройка финансовых отчетов';
            } elseif ($this->payment->payment_for == Payment::FOR_ADD_INVOICE) {
                return 'логин ' . $this->user->email . ', за пользование сервисом "КУБ", www.kub-24.ru количество счетов ' . $this->payment->invoiceTariff->invoice_count;
            }
            return 'логин ' . $this->user->email . ', за пользование сервисом "КУБ", www.kub-24.ru срок покупаемой подписки '
                    . SubscribeHelper::getReadableDuration($this->payment->tariff);
        }

        return $this->Desc;
    }

    /**
     * @return mixed
     */
    public function getMerchantLogin()
    {
        return \Yii::$app->params['robokassa']['merchantLogin'];
    }

    /**
     * @return mixed
     */
    public function getMerchantPassword()
    {
        switch ($this->getScenario()) {
            case static::SCENARIO_RESULT:
                return $this->getMerchantPassword2();
            case static::SCENARIO_SEND:
            default:
                return $this->getMerchantPassword1();
        }
    }

    /**
     * @return mixed
     */
    protected function getMerchantPassword1()
    {
        return \Yii::$app->params['robokassa']['password1'];
    }

    /**
     * @return mixed
     */
    protected function getMerchantPassword2()
    {
        return \Yii::$app->params['robokassa']['password2'];
    }


    /**
     * @return mixed
     */
    public function getCulture()
    {
        if ($this->getScenario() == self::SCENARIO_SEND) {
            return \Yii::$app->params['robokassa']['culture'];
        }

        return $this->Culture;
    }

    /**
     * @return mixed
     */
    public function getIsTest()
    {
        if ($this->getScenario() == self::SCENARIO_SEND) {
            return \Yii::$app->params['robokassa']['isTest'];
        }

        return $this->IsTest;
    }

    /**
     * @return string
     */
    public function createSignature()
    {
        if ($this->getScenario() == self::SCENARIO_SEND) {
            return md5(join(':', [
                $this->merchantLogin, $this->OutSum, $this->InvId, $this->merchantPassword,

                // additional parameters. MUST be in alphabetical order
                'Shp_companyId=' . $this->Shp_companyId,
                'Shp_employeeId=' . $this->Shp_employeeId,
            ]));
        } else {
            return md5(join(':', [
                $this->OutSum, $this->InvId, $this->merchantPassword,

                // additional parameters. MUST be in alphabetical order
                'Shp_companyId=' . $this->Shp_companyId,
                'Shp_employeeId=' . $this->Shp_employeeId,
            ]));
        }
    }

    /**
     * @param $signature
     * @return bool
     */
    public function validateSignature($signature)
    {
        return $signature === $this->createSignature();
    }

    /**
     * Search Subscribe model by specified in request id
     */
    private function setPaymentModel()
    {
        $this->payment = Payment::findOne($this->InvId);
    }

    /**
     * Search Company model by specified in request id
     */
    private function setCompanyModel()
    {
        $this->company = Company::findOne($this->Shp_companyId);
    }

    /**
     * Search Employee model by specified in request id
     */
    private function setUserModel()
    {
        $this->user = Employee::findOne($this->Shp_employeeId);
    }

    /**
     * Confirm payment
     */
    public function confirmPayment()
    {
        if ($this->validate() && $this->payment) {
            return $this->payment->paid();
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        if ($this->hasErrors()) {
            \Yii::warning(static::className() . "->getErrors() " . var_export($this->getErrors(), true), 'robokassa');
        }

        parent::afterValidate();
    }
}
