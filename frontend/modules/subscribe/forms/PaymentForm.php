<?php

namespace frontend\modules\subscribe\forms;

use common\components\pdf\PdfRenderer;
use common\models\Company;
use common\models\Contractor;
use common\models\EmployeeCompany;
use common\models\document\Invoice;
use common\models\employee\Employee;
use common\models\service\Payment;
use common\models\service\PaymentOrder;
use common\models\service\PaymentType;
use common\models\service\Subscribe;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\models\Documents;
use frontend\modules\documents\assets\InvoicePrintAsset;
use frontend\modules\documents\components\Message;
use frontend\modules\subscribe\models\PaymentMethodFactory;
use Yii;
use yii\base\Model;
use yii\db\Connection;
use yii\helpers\ArrayHelper;

/**
 * Class PaymentMethodForm
 * @package frontend\modules\subscribe\forms
 */
class PaymentForm extends Model
{
    const SCENARIO_TYPE = 'select_type';
    const SCENARIO_COMPANY = 'select_company';
    const SCENARIO_REWARD = '';

    const PAY_EXTRA_MAX = 10;

    /**
     * Form attributes
     */
    public $tariffId;
    public $paymentTypeId;
    public $companyList;
    public $companyId;
    public $payExtra = 0;
    public $createInvoice = true;
    public $adminDiscount = 0;
    public $durationMonth = false;

    protected $_company;
    protected $_employee;
    protected $_tariff;
    protected $_tariffArray = [];
    protected $_companyArray = [];
    protected $_payment;
    protected $_invoice;
    protected $_pdfContent;
    protected $_formData;
    protected $_additionallyPaidAmount = 0;

    public static $mutuallyExclusive = [
        SubscribeTariffGroup::TAX_DECLAR_IP_USN_6 => [
            SubscribeTariffGroup::TAX_IP_USN_6,
        ],
        SubscribeTariffGroup::TAX_IP_USN_6 => [
            SubscribeTariffGroup::TAX_DECLAR_IP_USN_6,
        ],
        SubscribeTariffGroup::BI_FINANCE => [
            SubscribeTariffGroup::BI_FINANCE_PLUS,
            SubscribeTariffGroup::BI_ALL_INCLUSIVE,
            SubscribeTariffGroup::BI_FULL_CONSTRUCTION,
        ],
        SubscribeTariffGroup::BI_FINANCE_PLUS => [
            SubscribeTariffGroup::BI_FINANCE,
            SubscribeTariffGroup::BI_ALL_INCLUSIVE,
            SubscribeTariffGroup::BI_FULL_CONSTRUCTION,
        ],
        SubscribeTariffGroup::BI_MARKETING => [
            SubscribeTariffGroup::BI_ALL_INCLUSIVE,
            SubscribeTariffGroup::BI_FULL_CONSTRUCTION,
        ],
        SubscribeTariffGroup::BI_MARKETING_PLUS => [
            SubscribeTariffGroup::BI_ALL_INCLUSIVE,
            SubscribeTariffGroup::BI_FULL_CONSTRUCTION,
        ],
        SubscribeTariffGroup::BI_SALES => [
            SubscribeTariffGroup::BI_ALL_INCLUSIVE,
            SubscribeTariffGroup::BI_FULL_CONSTRUCTION,
        ],
        SubscribeTariffGroup::BI_PRODUCTS => [
            SubscribeTariffGroup::BI_ALL_INCLUSIVE,
            SubscribeTariffGroup::BI_FULL_CONSTRUCTION,
        ],
        SubscribeTariffGroup::BI_ALL_INCLUSIVE => [
            SubscribeTariffGroup::BI_FINANCE,
            SubscribeTariffGroup::BI_FINANCE_PLUS,
            SubscribeTariffGroup::BI_MARKETING,
            SubscribeTariffGroup::BI_MARKETING_PLUS,
            SubscribeTariffGroup::BI_SALES,
            SubscribeTariffGroup::BI_PRODUCTS,
            SubscribeTariffGroup::BI_FULL_CONSTRUCTION,
        ],
        SubscribeTariffGroup::BI_FULL_CONSTRUCTION => [
            SubscribeTariffGroup::BI_FINANCE,
            SubscribeTariffGroup::BI_FINANCE_PLUS,
            SubscribeTariffGroup::BI_MARKETING,
            SubscribeTariffGroup::BI_MARKETING_PLUS,
            SubscribeTariffGroup::BI_SALES,
            SubscribeTariffGroup::BI_PRODUCTS,
            SubscribeTariffGroup::BI_ALL_INCLUSIVE,
        ],
    ];

    /**
     * @param Company $company
     * @param array $config
     * @throws Exception
     */
    public function __construct(Company $company = null, $config = [])
    {
        if ($company) {
            $this->companyList = [$company->id];
            $this->companyId = $company->id;
        }
        $this->scenario = self::SCENARIO_TYPE;

        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_TYPE => [
                'tariffId',
                'paymentTypeId',
                'companyList',
                'payExtra',
                'createInvoice',
                'adminDiscount',
                'durationMonth',
            ],
            self::SCENARIO_COMPANY => [
                'tariffId',
                'paymentTypeId',
                'companyList',
                'payExtra',
                'createInvoice',
                'companyId',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $filterByEmployee = $this->_employee !== null ?
            ['employee_id' => $this->_employee->id] :
            null;

        return [
            [['companyList'], 'filter', 'filter' => function ($value) {
                return (array)$value;
            }],
            [['payExtra'], 'integer', 'min' => 0, 'max' => self::PAY_EXTRA_MAX],
            [['tariffId', 'paymentTypeId'], 'required'],
            [
                ['paymentTypeId'], 'in',
                'range' => Yii::$app->id === 'app-frontend' ? [
                    PaymentType::TYPE_ONLINE,
                    PaymentType::TYPE_INVOICE,
                    // PaymentType::TYPE_RECEIPT,
                    PaymentType::TYPE_REWARD,
                ] : [PaymentType::TYPE_INVOICE],
                'message' => 'Способ оплаты не указан.',
            ],
            [['createInvoice'], 'boolean'],
            [
                ['companyList'], 'required',
                'when' => function ($model) {
                    return in_array($model->paymentTypeId, [
                        PaymentType::TYPE_INVOICE,
                        // PaymentType::TYPE_RECEIPT,
                        PaymentType::TYPE_ONLINE,
                    ]);
                },
                'message' => 'Необходимо выбрать хотя бы одну компанию.',
            ],
            [['companyList'], 'each',
                'rule' => [
                    'exist',
                    'targetClass' => EmployeeCompany::className(),
                    'targetAttribute' => 'company_id',
                    'filter' => $filterByEmployee,
                ],
            ],
            [['tariffId'], 'tariffIdValidator'],
            [
                ['durationMonth'], 'integer',
                'min' => 1,
                'max' => 12,
                'when' => function ($model) {
                    return is_array($model->tariffId) && count($model->tariffId) > 1;
                },
            ],
            [
                ['companyId'],
                'required',
                'when' => function ($model) {
                    return in_array($model->paymentTypeId, [
                        PaymentType::TYPE_INVOICE,
                        // PaymentType::TYPE_RECEIPT,
                    ]);
                }
            ],
            [
                ['companyId'],
                'exist',
                'when' => function ($model) {
                    return in_array($model->paymentTypeId, [
                        PaymentType::TYPE_INVOICE,
                        // PaymentType::TYPE_RECEIPT,
                    ]);
                },
                'targetClass' => EmployeeCompany::className(),
                'targetAttribute' => 'company_id',
                'filter' => $filterByEmployee,
            ],
            [
                ['adminDiscount'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/', 'min' => 0, 'max' => 99.9999,
                'when' => function ($model) {
                    return $this->getIsAdmin();
                },
            ],
            [
                ['adminDiscount'], 'filter',
                'filter' => function ($value) {
                    return round($value, 4);
                },
                'when' => function ($model) {
                    return $this->getIsAdmin();
                },
            ],
        ];
    }

    /**
     * Get is admin mode
     */
    public function getIsAdmin()
    {
        return Yii::$app->id === 'app-backend' ||
            Yii::$app->controller->getUniqueId() === 'system/selling';
    }

    /**
     * @inheritdoc
     */
    public function tariffIdValidator($attribute, $params)
    {
        $tariffArray = SubscribeTariff::find()->where([
            'id' => $this->$attribute,
        ])->paid()->active()->all();

        if (empty($tariffArray)) {
            $this->addError($attribute, 'Тарифный план не найден.');

            return;
        } else {
            foreach ($tariffArray as $tariff) {
                if ($tariff->is_pay_extra) {
                    if ($forTariff = $tariff->payExtraForTariff) {
                        foreach ((array) $this->companyList as $companyId) {
                            $query = Subscribe::find()->active()->byCompany($companyId)->andWhere([
                                'tariff_id' => $forTariff->id,
                            ]);
                            if (!$query->exists()) {
                                $this->addError($attribute, 'Подписка для доплаты не найдена.');
                            }
                        }
                    } else {
                        $this->addError($attribute, 'Тарифный план для доплаты не найден.');
                    }
                }
                if ($this->durationMonth && $this->durationMonth != $tariff->duration_month) {
                    $this->addError($attribute, 'Тарифный план не соответствует выбранному периоду.');
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tariffId' => 'Тариф',
            'paymentTypeId' => 'Способ оплаты',
            'createInvoice' => 'Создать входящий счёт в сервисе',
            'companyList' => 'Выбрать компании',
            'companyId' => 'На какую компанию выставить счет',
            'adminDiscount' => 'Скидка %',
            'payExtra' => 'Оплатить дополнительно',
        ];
    }

    /**
     * @return Company
     */
    public function afterValidate()
    {
        $this->_company = Company::findOne($this->companyId);
        $this->_tariffArray = SubscribeTariff::find()
            ->alias('tariff')
            ->where([
                'tariff.id' => $this->tariffId,
            ])
            ->joinWith('tariffGroup group')
            ->orderBy([
                '({{group}}.[[id]] = :standart)' => SORT_DESC,
                'group.name' => SORT_ASC,
            ])
            ->addParams([
                ':standart' => SubscribeTariffGroup::STANDART,
            ])
            ->all();
        $this->_companyArray = Company::find()->where([
                'id' => $this->companyList,
            ])->all();
        if (empty($this->durationMonth) && count($this->_tariffArray) == 1) {
            $this->durationMonth = reset($this->_tariffArray)->duration_month;
        }

        parent::afterValidate();
    }

    /**
     * @return Employee
     */
    public function setEmployee(Employee $employee = null)
    {
        return $this->_employee = $employee;
    }

    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->_employee;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @return SubscribeTariff
     */
    public function getTariff()
    {
        return $this->getTariffArray()[0] ?? null;
    }

    /**
     * @return SubscribeTariff
     */
    public function getTariffArray()
    {
        return $this->_tariffArray;
    }

    /**
     * @return Payment
     */
    public function getPayment()
    {
        return $this->_payment;
    }

    /**
     * @return Invoice
     */
    public function getInvoice()
    {
        return $this->_invoice;
    }

    /**
     * @return Company[]
     */
    public function getForCompanyArray()
    {
        return $this->_companyArray;
    }

    /**
     * @return integer
     */
    public function getPayMultiple()
    {
        return count($this->companyList) + min(max(intval($this->payExtra), 0), self::PAY_EXTRA_MAX);
    }

    /**
     * @return array
     */
    public function getFormData($hasIp = true)
    {
        if ($this->_formData === null) {
            $tariffArray = [];
            $tariffPrices = [];
            $tariffDurations = [];
            $discountByCount = [];
            $groupQuery = SubscribeTariffGroup::find()->where([
                'is_active' => true,
            ])->orderBy([
                "priority" => SORT_ASC,
            ])->indexBy('id');
            if (!$hasIp) {
                $groupQuery->andWhere([
                    'not',
                    ['id' => [
                        SubscribeTariffGroup::TAX_DECLAR_IP_USN_6,
                        SubscribeTariffGroup::TAX_IP_USN_6,
                    ]]
                ]);
            }
            $groupArray = $groupQuery->all();
            foreach ($groupArray as $group) {
                $actualTariffs = $group->getActualTariffs()
                    ->andWhere(['is_active' => true])
                    ->orderBy(['price' => SORT_ASC])
                    ->all();
                $group->populateRelation('actualTariffs', $actualTariffs);
                foreach ($actualTariffs as $tariff) {
                    $discounts = $tariff->getDiscounts()->orderBy(['quantity' => SORT_ASC])->all();
                    $tariff->populateRelation('group', $group);
                    $tariff->populateRelation('discounts', $discounts);
                    $tariffPrices[$tariff->id] = $tariff->price;
                    $tariffDurations[$tariff->id] = $tariff->duration_month;
                    $tariffPerQuantity[$tariff->id] = in_array($group->id, SubscribeTariffGroup::$payPerQuantity) ?
                        $tariff->tariff_limit : $tariff->duration_month;
                    $discountByCount[$tariff->id] = ArrayHelper::map($discounts, 'quantity', 'percent');
                    $tariffArray[] = $tariff;
                }
            }

            $this->_formData = [
                'tariffArray' => $tariffArray,
                'tariffPrices' => $tariffPrices,
                'discountByCount' => $discountByCount,
                'groupArray' => $groupArray,
                'tariffDurations' => $tariffDurations,
                'tariffPerQuantity' => $tariffPerQuantity,
            ];
        }

        return $this->_formData;
    }

    /**
     * @return Payment
     */
    public function getNewPayment()
    {
        $tariffArray = $this->getTariffArray();
        $payment = new Payment([
            'company_id' => $this->company->id,
            'sum' => 0,
            'type_id' => $this->paymentTypeId,
            'tariff_id' => reset($tariffArray)->id ?? null,
            'is_confirmed' => false,
        ]);
        $employee = $this->employee;
        $orderArray = [];
        $payCount = $this->getPayMultiple();
        if (!$this->getIsAdmin()) {
            $this->adminDiscount = 0;
        }

        foreach ($tariffArray as $tariff) {
            $discountByCount = $tariff->discount($payCount);
            foreach ($this->forCompanyArray as $company) {
                if ($tariff->tariffGroup->activeByCompany($company)) {
                    $discountValue = max($this->adminDiscount, $discountByCount);
                    $discount = $company->getMaxDiscount($tariff->id);
                    if ($discount !== null) {
                        if ($discount->value > $discountValue) {
                            $discountValue = $discount->value;
                        } else {
                            $discount = null;
                        }
                    }
                    $discountValue = $this->adjustDiscount($tariff->price, $discountValue);
                    $paymentOrder = new PaymentOrder([
                        'company_id' => $company->id,
                        'tariff_id' => $tariff->id,
                        'discount_id' => $discount ? $discount->id : null,
                        'price' => (int) $tariff->price,
                        'discount' => $discountValue,
                        'sum' => round($tariff->price - $tariff->price * $discountValue / 100),
                        'employee_id' => $employee ? $employee->id : null,
                    ]);
                    $paymentOrder->populateRelation('company', $company);
                    $paymentOrder->populateRelation('payment', $payment);
                    $paymentOrder->populateRelation('tariff', $tariff);
                    $orderArray[] = $paymentOrder;

                    $payment->sum += $paymentOrder->sum;
                }
            }

            if ($this->payExtra > 0) {
                for ($i=0; $i < $this->payExtra; $i++) {
                    $discount = max(
                        $this->adminDiscount,
                        $discountByCount
                    );
                    $paymentOrder = new PaymentOrder([
                        'company_id' => null,
                        'tariff_id' => $tariff->id,
                        'price' => (int) $tariff->price,
                        'discount' => $discount,
                        'sum' => round($tariff->price - $tariff->price * $discount / 100),
                        'employee_id' => $employee ? $employee->id : null,
                    ]);
                    $paymentOrder->populateRelation('payment', $payment);
                    $paymentOrder->populateRelation('tariff', $tariff);
                    $orderArray[] = $paymentOrder;

                    $payment->sum += $paymentOrder->sum;
                }
            }
        }
        $payment->sum .= '';
        $payment->populateRelation('orders', $orderArray);

        return $payment;
    }

    public function adjustDiscount($price, $discount)
    {
        $sum = round($price * $discount / 100);
        $discount = round($sum * 100 / $price, 6);

        return $discount * 1;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function createPayment()
    {
        if (!$this->company || !$this->tariffArray || !$this->forCompanyArray) {
            throw new \Exception("The form configure error", 1);
        }

        $payment = $this->getNewPayment();

        if (!$payment->save()) {
            \Yii::$app->session->setFlash('error', 'Ошибка при создании платежа. Попробуйте ещё раз');
            \common\components\helpers\ModelHelper::logErrors($payment, __METHOD__);

            return false;
        }
        foreach ($payment->orders as $order) {
            $order->payment_id = $payment->id;
            if (!$order->save()) {
                \Yii::$app->session->setFlash('error', 'Ошибка при создании платежа. Попробуйте ещё раз.');
                \common\components\helpers\ModelHelper::logErrors($order, __METHOD__);

                return false;
            }
        }

        $invoice = SubscribeHelper::getInvoice($payment, $this->tariffArray, $this->company);

        if (!$invoice) {
            \Yii::$app->session->setFlash('error', 'Ошибка при создании счёта. Попробуйте ещё раз.');

            return false;
        }

        $this->_payment = $payment;
        $this->_invoice = $invoice;

        return true;
    }

    /**
     * @inheritdoc
     */
    public function invoicePayment()
    {
        if ($this->createInvoice && !SubscribeHelper::getInInvoiceByOut($this->invoice, $this->company)) {
            \Yii::$app->session->setFlash('error', 'Ошибка при создании входящего счёта. Попробуйте ещё раз.');

            return false;
        }

        $this->_pdfContent = $this->invoiceRenderer->output(false);
        $subject = 'Счет на оплату сервиса КУБ';

        $mailer = \Yii::$app->mailer
            ->compose([
                'html' => 'system/subscribe/invoice-payment/html',
                'text' => 'system/subscribe/invoice-payment/text',
            ], [
                'tariff' => $this->tariff,
                'company' => $this->company,
                'invoice' => $this->invoice,
                'subject' => $subject,
            ])
            ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
            ->setTo($this->company->email)
            ->setSubject($subject)
            ->attachContent($this->_pdfContent, [
                'fileName' => 'invoice.pdf',
                'contentType' => 'application/pdf',
            ]);

        if ($this->invoice->contractor_bik && $this->invoice->contractor_rs) {
            $content = Yii::$app->view->render('@frontend/modules/documents/views/invoice/1C_payment_order.php', [
                'model' => $this->invoice,
            ]);
            $name = ($this->invoice->is_invoice_contract && $this->invoice->type == Documents::IO_TYPE_OUT) ? 'счет-договора' : 'счета';
            $mailer->attachContent($content, [
                'fileName' => 'Платежное_поручение_для_' . $name . '_№' .
                    mb_ereg_replace("([^\w\s\d\-_])", '', $this->invoice->fullNumber) . '.txt',
                'contentType' => 'text/plain',
            ]);
        }

        if (!$mailer->send()) {
            \Yii::$app->session->setFlash('error', 'Ошибка при отправке счета. Попробуйте ещё раз.');

            return false;
        }

        return true;
    }

    /**
     * @param $fileName
     * @param $outInvoice
     * @return PdfRenderer
     */
    public function getInvoiceRenderer()
    {
        return Invoice::getRenderer(null, $this->invoice, PdfRenderer::DESTINATION_STRING);
    }

    /**
     * @inheritdoc
     */
    public function receiptPayment()
    {
        $this->_pdfContent = $this->receiptRenderer->output(false);
        $subject = 'Квитанция для Сбербанка для оплаты тарифа на ' . $this->tariff->getTariffName();

        $mailer = \Yii::$app->mailer
            ->compose([
                'html' => 'system/subscribe/receipt-payment/html',
                'text' => 'system/subscribe/receipt-payment/text',
            ], [
                'tariff' => $this->tariff,
                'company' => $this->company,
                'contractor' => Contractor::findOne(\Yii::$app->params['service']['contractor_id']),
                'subject' => $subject,
            ])
            ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
            ->setTo($this->company->email)
            ->setSubject($subject)
            ->attachContent($this->_pdfContent, [
                'fileName' => 'sberbank-receipt.pdf',
                'contentType' => 'application/pdf',
            ]);

        if (!$mailer->send()) {
            \Yii::$app->session->setFlash('error', 'Ошибка при отправке квитанции. Попробуйте ещё раз.');

            return false;
        }

        return true;
    }

    /**
     * @param $fileName
     * @param $outInvoice
     * @return PdfRenderer
     */
    public function getReceiptRenderer()
    {
        $renderer = new PdfRenderer([
            'view' => '@frontend/modules/subscribe/views/default/pdf-receipt',
            'params' => array_merge([
                'payment' => $this->payment,
                'tariff' => $this->tariff,
                'company' => $this->company,
                'contractor' => Contractor::findOne(\Yii::$app->params['service']['contractor_id']),
            ]),

            'destination' => PdfRenderer::DESTINATION_STRING,
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => 'A4-P',
        ]);
        \Yii::$app->controller->view->params['asset'] = InvoicePrintAsset::className();

        return $renderer;
    }

    /**
     * @return string|null
     */
    public function getPdfContent()
    {
        return $this->_pdfContent;
    }

    /**
     * @return boolean
     */
    public function makePayment($validale = true, $byUser = true)
    {
        if (!$this->validate()) {
            return false;
        }

        $created = \Yii::$app->db->transaction(function (Connection $db) use ($byUser) {
            if ($this->createPayment()) {
                return true;
            }
            $db->transaction->rollBack();

            return false;
        });

        if ($created) {
            switch ($this->paymentTypeId) {
                case PaymentType::TYPE_INVOICE:
                    if ($this->invoicePayment()) {
                        \Yii::$app->session->setFlash(
                            'success',
                            $byUser ?
                            "Счет на оплату отправлен на ваш e-mail: {$this->company->email}." :
                            "Счет на оплату отправлен на e-mail: {$this->company->email}."
                        );
                    }
                    break;

                case PaymentType::TYPE_RECEIPT:
                    if ($this->receiptPayment()) {
                        \Yii::$app->session->setFlash(
                            'success',
                            $byUser ?
                            "Квитанция на оплату отправлена на ваш e-mail: {$this->company->email}." :
                            "Квитанция на оплату отправлена на e-mail: {$this->company->email}."
                        );
                    }
                    break;
            }
            \common\models\company\CompanyFirstEvent::checkEvent($this->company, 82);
        }

        return $created;
    }
}