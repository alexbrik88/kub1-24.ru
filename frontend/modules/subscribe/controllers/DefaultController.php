<?php
namespace frontend\modules\subscribe\controllers;

use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\helpers\Html;
use common\components\pdf\PdfRenderer;
use common\components\TextHelper;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\company\CompanyVisit;
use common\models\Contractor;
use common\models\DiscountType;
use common\models\document\Invoice;
use common\models\selling\SellingSubscribe;
use common\models\service\Payment;
use common\models\service\PaymentType;
use common\models\service\PromoCode;
use common\models\service\PromoCodeGroup;
use common\models\service\Subscribe;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeTariff;
use frontend\components\FrontendController;
use frontend\models\AffiliateProgramForm;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\modules\subscribe\forms\PaymentForm;
use frontend\modules\subscribe\forms\PromoCodeForm;
use frontend\modules\subscribe\forms\OnlinePaymentForm;
use frontend\modules\subscribe\models\OnlinePayment;
use frontend\modules\subscribe\models\PaymentSearch;
use frontend\modules\subscribe\models\RewardPayment;
use frontend\rbac\permissions;
use Yii;
use yii\db\Connection;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class DefaultController
 * @package frontend\modules\cash\controllers
 */
class DefaultController extends FrontendController
{

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                        ],
                        'allow' => true,
                        'roles' => [permissions\Subscribe::INDEX],
                    ],
                    [
                        'actions' => [
                            'old',
                            'form',
                            'form2',
                            'pdf-receipt',
                            'pdf-invoice',
                            'make-payment',
                            'payment',
                            'validate',
                            'activate-promo-code',
                            'online-payment',
                            'summary',
                        ],
                        'allow' => true,
                        'roles' => [permissions\Subscribe::MANAGER],
                    ],
                    [
                        'actions' => ['create', 'document-print'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'actions' => [
                    'activate-promo-code' => ['post'],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            //if (Yii::$app->user->isMtsUser) {
            //    $url = ArrayHelper::getValue(Yii::$app->params, 'mts.purchaseUrl', ['/']);
            //    return Yii::$app->getResponse()->redirect($url)->send();
            //}

            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function actionIndex($tariffId = null)
    {
        $company = Yii::$app->user->identity->company;

        $companyArray = \Yii::$app->user->identity->getCompanies()
            ->alias('company')
            ->leftJoin(['companyType' => CompanyType::tableName()], "{{company}}.[[company_type_id]] = {{companyType}}.[[id]]")
            ->isBlocked(false)
            ->orderBy([
                new \yii\db\Expression('IF({{company}}.[[id]] = :id, 0, 1) ASC'),
                new \yii\db\Expression("ISNULL({{companyType}}.[[name_short]])"),
                "companyType.name_short" => SORT_ASC,
                "company.name_short" => SORT_ASC,
            ])
            ->params([':id' => $company->id])
            ->all();

        foreach ($companyArray as $item) {
            \common\models\DiscountType::checkForType4($item);
        }

        $model = new PaymentForm($company, [
            'employee' => Yii::$app->user->identity,
        ]);

        $paymentSearchModel = new PaymentSearch();
        $paymentDataProvider = $paymentSearchModel->search(Yii::$app->request->queryParams);

        $actualSubscribes = SubscribeHelper::getPayedSubscriptions($company->id);

        $affiliateProgramForm = new AffiliateProgramForm();
        $affiliateProgramForm->amount = $company->affiliate_sum;
        $affiliateProgramForm->type = AffiliateProgramForm::RS_TYPE;

        \common\models\company\CompanyFirstEvent::checkEvent($company, 75);

        return $this->render('index', [
            'model' => $model,
            'paymentDataProvider' => $paymentDataProvider,
            'paymentSearchModel' => $paymentSearchModel,
            'actualSubscribes' => $actualSubscribes,
            'company' => $company,
            'companyArray' => $companyArray,
            'affiliateProgramForm' => $affiliateProgramForm,
            'tariffId' => $tariffId,
            'showNewTariffs' => (Yii::$app->request->get('old', 'none') === 'none')
        ]);
    }

    /**
     * @return string
     */
    public function actionForm($tariffId = null)
    {
        if (Yii::$app->request->isAjax) {
            return $this->render('_payment_form', [
                'company' => Yii::$app->user->identity->company,
                'tariffId' => $tariffId,
            ]);
        } else {
            return $this->redirect(['index', 'tariffId' => $tariffId]);
        }
    }

    /**
     * @return string
     */
    public function actionForm2($tariffId = null)
    {
        if (Yii::$app->request->isAjax) {
            return $this->render('_payment_form_2', [
                'company' => Yii::$app->user->identity->company,
                'tariffId' => $tariffId,
            ]);
        } else {
            return $this->redirect(['index', 'tariffId' => $tariffId]);
        }
    }

    /**
     * @return string
     */
    public function actionCreate($id, $key, $tariff, $type)
    {
        $company = Company::findOne([
            'id' => $id,
            'subscribe_payment_key' => $key,
            'blocked' =>false,
        ]);

        if ($company === null) {
            throw new NotFoundHttpException('Страница не найдена или ссылка устарела.');
        }

        if (!in_array($tariff, SubscribeTariff::find()->select('id')->actual()->column())) {
            throw new NotFoundHttpException('Страница не найдена.');
        }

        if (!in_array($type, [PaymentType::TYPE_INVOICE, PaymentType::TYPE_ONLINE])) {
            throw new NotFoundHttpException('Страница не найдена.');
        }

        CompanyVisit::checkVisit($company);
        DiscountType::checkForType2($company);

        $payment = Payment::find()
            ->andWhere([
                'company_id' => $company->id,
                'type_id' => $type,
                'tariff_id' => $tariff,
                'is_confirmed' => false,
            ])
            ->andWhere(['>=', 'created_at', (new \DateTime('- 3 days'))->setTime(0, 0)->getTimestamp()])
            ->orderBy(['created_at' => SORT_DESC])->one();

        if (!$payment || count($payment->orders) != 1 || $payment->order->company_id != $company->id
            || $payment->order->discount != $company->getDiscount($tariff)) {
            $model = new PaymentForm($company, [
                'tariffId' => $tariff,
                'paymentTypeId' => $type,
            ]);
            $payment = $model->makePayment() ? $model->payment : null;
        }

        if ($payment !== null) {
            if ($type == PaymentType::TYPE_ONLINE) {
                $paymentForm = new OnlinePaymentForm([
                    'scenario' => OnlinePaymentForm::SCENARIO_SEND,
                    'company' => $company,
                    'payment' => $payment,
                    'user' => \Yii::$app->user->identity ? : $company->employeeChief,
                ]);

                return $this->renderPartial('create', [
                    'paymentForm' => $paymentForm,
                ]);
            } else {
                return $this->redirect(['/documents/invoice/out-view', 'uid' => $payment->outInvoice->uid]);
            }
        }

        throw new NotFoundHttpException('Страница не найдена или ссылка устарела.');
    }

    /**
     * @return string
     */
    public function actionValidate($company = null)
    {
        $model = new PaymentForm(\Yii::$app->user->identity->company, [
            'scenario' => $company ? PaymentForm::SCENARIO_COMPANY : PaymentForm::SCENARIO_TYPE,
        ]);

        if (Yii::$app->request->isAjax && !Yii::$app->request->isPjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

    /**
     * @return string
     */
    public function actionSummary()
    {
        $model = new PaymentForm(\Yii::$app->user->identity->company);
        $model->load(Yii::$app->request->post());

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $this->renderPartial('summary', ['model' => $model]);
    }

    /**
     * @return Response
     * @throws \Exception
     * @throws \yii\base\Exception
     */
    public function actionPayment($company = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $session = Yii::$app->session;

        if (Yii::$app->user->identity->company->strict_mode) {
            return $this->strictMode();
        }

        $model = new PaymentForm(\Yii::$app->user->identity->company, [
            'employee' => \Yii::$app->user->identity,
        ]);

        $isCreated = false;
        if ($model->load(\Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->paymentTypeId == PaymentType::TYPE_REWARD) {
                    return $this->actionUseReward();
                }

                if ($model->makePayment()) {
                    $isCreated = true;
                }
            } else {
                $session->setFlash('error', 'Форма оплаты заполнена неправильно.');
                \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);
            }
        }

        if ($isCreated) {
            \frontend\components\facebook\EventTracker::event('trypay');
            if ($model->paymentTypeId == PaymentType::TYPE_ONLINE) {
                if ($model->payment->is_confirmed) {
                    $session->setFlash('success', 'Подписка уже оплачена.');

                    return [
                        'alert' => Yii::$app->session->getAllFlashes(true),
                    ];
                }

                $paymentForm = new OnlinePaymentForm([
                    'scenario' => OnlinePaymentForm::SCENARIO_SEND,
                    'company' => $model->company,
                    'payment' => $model->payment,
                    'user' => Yii::$app->user->identity ? : $model->company->employeeChief,
                ]);

                if ($redirectAfterPay = Yii::$app->request->post('redirectAfterPay')) {
                    Yii::$app->session->set('redirectAfterPay', $redirectAfterPay);
                }

                return [
                    'alert' => $session->getAllFlashes(true),
                    'content' => $this->renderAjax('create', [
                        'paymentForm' => $paymentForm,
                    ]),
                ];
            } else {
                return [
                    'alert' => $session->getAllFlashes(true),
                    'done' => true,
                    'type' => $model->paymentTypeId,
                    'email' => $model->company->email,
                    'link' => Url::to([
                        '/documents/invoice/out-view',
                        'uid' => $model->payment->outInvoice->uid,
                        'email' => $model->company->email,
                    ]),
                ];
            }
        }

        return [
            'alert' => Yii::$app->session->getAllFlashes(true),
            'done' => false,
        ];
    }

    /**
     * @param string $uid
     * @return string|void
     * @throws NotFoundHttpException
     */
    public function actionDocumentPrint($uid)
    {
        $model = Invoice::find()->andWhere([
            'uid' => $uid,
            'is_subscribe_invoice' => true,
        ])->one();

        if ($model === null) {
            throw new NotFoundHttpException('Страница не найдена или устарела.');
        }
        $content = Invoice::getRenderer(null, $model, PdfRenderer::DESTINATION_STRING, true)->output(false);

        return Yii::$app->response->sendContentAsFile($content, $model->pdfFileName, [
            'inline' => true,
            'mimeType' => 'application/pdf',
        ]);
    }

    protected function strictMode()
    {
        \Yii::$app->session->setFlash('emptyCompany', Html::tag('div', Html::tag('button', '×', [
            'type' => 'button',
            'class' => 'close',
            'data-dismiss' => 'alert',
            'aria-hidden' => 'true',
            'style' => 'display: block;',
        ]) . 'Данное действие недоступно. ' . Html::a('Заполните информацию о компании и банковские реквизиты.', ['/company/update']), [
            'class' => 'alert-danger alert fade in',
        ]));

        return $this->redirect(['index']);
    }

    /**
     * @return string|Response
     * @throws \Exception
     */
    public function actionActivatePromoCode()
    {
        $this->layout = 'pdf';
        $model = new PromoCodeForm([
            'company' => Yii::$app->user->identity->company,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Промокод  успешно зарегистирован.');

            return $this->redirect(['index']);
        } else {
            return $this->renderAjax('_promoCodeForm', [
                'promoCodeForm' => $model,
            ]);
        }
    }

    /**
     * @return Response
     */
    public function actionUseReward()
    {
        $model = new RewardPayment(\Yii::$app->user->identity->company);

        if ($model->pay()) {
            Yii::$app->session->setFlash(
                'success',
                'Вознаграждение использовано.  На ваш аккаунт зачислено ' . $model->affiliateSum . ' руб. (' . $model->duration . ' дней).'
            );
        }

        if (Yii::$app->response->format == Response::FORMAT_JSON) {
            return [
                'alert' => Yii::$app->session->getAllFlashes(true),
                'type' => (string) PaymentType::TYPE_REWARD,
            ];
        }

        return $this->redirect(['index']);
    }

    /**
     * @param string $actionType
     * @param int $subscribeId
     * @return string|void
     * @throws NotFoundHttpException
     */
    public function actionPdfReceipt($actionType, $subscribeId)
    {
        /* @var Company $company */
        $company = \Yii::$app->user->identity->company;
        $subscribe = $this->getSubscribe($company, $subscribeId);

        $renderer = new PdfRenderer([
            'view' => 'pdf-receipt',
            'params' => [
                'subscribe' => $subscribe,
                'tariff' => $subscribe->tariff,
                'company' => $company,
                'contractor' => Contractor::findOne(\Yii::$app->params['service']['contractor_id']),
            ],

            'destination' => PdfRenderer::DESTINATION_STRING,
            'filename' => 'sberbank-receipt.pdf',
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => 'A4-P',
        ]);

        switch ($actionType) {
            case 'pdf':
                return $renderer->output();
            case 'print':
            default:
                return $renderer->renderHtml();
        }
    }

    /**
     * @param string $actionType
     * @param int $subscribeId
     * @return string|void
     * @throws NotFoundHttpException
     */
    public function actionPdfInvoice($actionType, $subscribeId)
    {
        /* @var Company $company */
        $company = \Yii::$app->user->identity->company;
        $subscribe = $this->getSubscribe($company, $subscribeId);

        /* @var Invoice $invoice */
        $invoice = Invoice::find()->andWhere([
            'company_id' => \Yii::$app->params['service']['company_id'],
            'type' => Documents::IO_TYPE_OUT,
            'document_number' => $subscribe->id,
        ])->one();

        $renderer = new PdfRenderer([
            'view' => '@documents/views/invoice/pdf-view',
            'params' => [
                'model' => $invoice,
                'message' => new Message(Documents::IO_TYPE_IN, $invoice->type),
                'ioType' => $invoice->type,
            ],

            'destination' => PdfRenderer::DESTINATION_STRING,
            'filename' => 'invoice.pdf',
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => 'A4-P',
        ]);

        switch ($actionType) {
            case 'pdf':
                return $renderer->output();
            case 'print':
            default:
                return $renderer->renderHtml();
        }
    }

    /**
     * @param Company $company
     * @param $id
     * @return Subscribe
     * @throws NotFoundHttpException
     */
    protected function getSubscribe(Company $company, $id)
    {
        $subscribe = Subscribe::find()->andWhere([
            'id' => $id,
            'company_id' => $company->id,
        ])->one();

        if ($subscribe === null) {
            throw new NotFoundHttpException('Подписка не найдена.');
        }

        return $subscribe;
    }
}
