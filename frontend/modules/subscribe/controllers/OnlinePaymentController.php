<?php
namespace frontend\modules\subscribe\controllers;

use common\components\filters\AccessControl;
use common\components\pdf\PdfRenderer;
use common\models\Company;
use common\models\document\Act;
use common\models\selling\SellingSubscribe;
use common\models\service\Payment;
use common\models\service\PaymentType;
use common\models\service\PromoCodeGroup;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use frontend\components\FrontendController;
use frontend\modules\subscribe\forms\OnlinePaymentForm;
use frontend\modules\subscribe\models\OnlinePaymentLog;
use frontend\modules\subscribe\models\OnlinePaymentSaver;
use frontend\rbac\permissions;
use Yii;
use yii\db\Connection;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class OnlinePaymentController
 * @package frontend\modules\cash\controllers
 */
class OnlinePaymentController extends FrontendController
{

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['result'],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
                            'pay', 'success', 'fail',
                        ],
                        'allow' => true,
                        'roles' => [permissions\Subscribe::INDEX],
                    ],
                ],
            ],
        ]);
    }

    /**
     * Note: same with visitcard
     * @param int $subscribeId
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPay($paymentId)
    {
        /* @var Company $company */
        $company = \Yii::$app->user->identity->company;
        $payment = $this->getPayment($company, $paymentId);

        if ($payment->is_confirmed) {
            \Yii::$app->session->setFlash('success', 'Подписка уже оплачена.');

            return $this->redirect(['@subscribeUrl']);
        }

        $paymentForm = new OnlinePaymentForm([
            'scenario' => OnlinePaymentForm::SCENARIO_SEND,
            'company' => $company,
            'payment' => $payment,
            'user' => \Yii::$app->user->identity,
        ]);

        return $this->render('pay', [
            'paymentForm' => $paymentForm,
        ]);
    }

    /**
     * Confirm payment
     * Log errors in file and send emails about it for administrator
     *
     * @todo: strict by domain.
     */
    public function actionResult()
    {
        $log = new OnlinePaymentLog([
            'queryParams' => \Yii::$app->request->queryParams,
            'bodyParams' => \Yii::$app->request->bodyParams,
        ]);

        $form = new OnlinePaymentForm([
            'scenario' => OnlinePaymentForm::SCENARIO_RESULT,
        ]);
        $form->load(\Yii::$app->request->{\Yii::$app->params['robokassa']['requestMethod']}(), '' /*form name is disabled*/);

        $log->hasError = !$form->confirmPayment();
        if (!$log->hasError && $form->payment->is_confirmed && !empty($form->user->email)) {
            $mail = \Yii::$app->mailer->compose([
                'html' => 'system/subscribe/confirm-payment/html',
                'text' => 'system/subscribe/confirm-payment/text',
            ], [
                'data' => ['sum' => $form->payment->sum],
                'subject' => 'Платеж успешно принят',
            ])
                ->setSubject('Платеж успешно принят')
                ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
                ->setTo($form->user->email);
            if (($invoice = $form->payment->outInvoice) && ($invoice->act || $invoice->createAct())) {
                $act = $invoice->act;
                $mail->attachContent(Act::getRenderer(null, $act, PdfRenderer::DESTINATION_STRING)->output(false), [
                    'fileName' => $act->pdfFileName,
                    'contentType' => 'application/pdf',
                ]);
            }

            $mail->send();
        }

        $log->collectPaymentData($form);
        $log->log();

        if (empty($form->errors)) {
            $inv_id = $form->InvId;

            echo "OK$inv_id\n";

            \Yii::$app->end();
        } else {
            echo '';

            \Yii::$app->end();
        }
    }

    /**
     * Create payment (if not exists) and confirm it
     */
    public function actionSuccess()
    {
        $form = new OnlinePaymentForm([
            'scenario' => OnlinePaymentForm::SCENARIO_SUCCESS,
            'company' => \Yii::$app->user->identity->company,
            'user' => \Yii::$app->user->identity,
        ]);
        $form->load(\Yii::$app->request->{\Yii::$app->params['robokassa']['requestMethod']}(), '' /*form name is disabled*/);

        if ($form->validate()) {
            \Yii::$app->session->setFlash('success', 'Оплата успешно зарегистрирована.');
        } else {
            \Yii::$app->session->setFlash('success', 'При оплате возникла ошибка. Обратитесь к администратору.');
        }

        if ($redirectAfterPay = Yii::$app->session->remove('redirectAfterPay')) {
            return $this->redirect([$redirectAfterPay]);
        }

        return $this->redirect(['@subscribeUrl']);
    }

    /**
     * @return Response
     */
    public function actionFail()
    {
        if ($redirectAfterPay = Yii::$app->session->remove('redirectAfterPay')) {
            return $this->redirect([$redirectAfterPay]);
        }

        return $this->redirect(['@subscribeUrl']);
    }

    /**
     * @param Company $company
     * @param $id
     * @return Payment
     * @throws NotFoundHttpException
     */
    protected function getPayment(Company $company, $id)
    {
        /* @var Payment $payment */
        $payment = Payment::find()->andWhere([
            'id' => $id,
            'company_id' => $company->id,
        ])->one();

        if ($payment === null) {
            throw new NotFoundHttpException('Платеж не найден.');
        }

        return $payment;
    }

}