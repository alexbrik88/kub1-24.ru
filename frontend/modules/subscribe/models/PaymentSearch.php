<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 3.12.15
 * Time: 16.09
 * Email: t.kanstantsin@gmail.com
 */

namespace frontend\modules\subscribe\models;

use common\models\service;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class PaymentSearch
 * @package frontend\modules\subscribe\models
 */
class PaymentSearch extends service\Payment
{
    private $_query;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'payment_date' => SORT_DESC,
                ],
                'attributes' => [
                    'payment_date' => [
                        'asc' => [
                            'ISNULL([[payment_date]])' => SORT_DESC,
                            'payment_date' => SORT_ASC,
                            'created_at' => SORT_ASC,
                        ],
                        'desc' => [
                            'ISNULL([[payment_date]])' => SORT_DESC,
                            'payment_date' => SORT_DESC,
                            'created_at' => SORT_DESC,
                        ],
                    ],
                    'sum' => [
                        'asc' => ['`sum` * 1' => SORT_ASC],
                        'desc' => ['`sum` * 1' => SORT_DESC],
                    ],
                ],
            ],
        ]);

        $this->load($params);

        $query->joinWith('subscribe');

        $query->andFilterWhere([
            self::tableName() . '.type_id' => $this->type_id,
            self::tableName() . '.company_id' => \Yii::$app->user->identity->company->id,
        ]);

        $this->_query = $query;

        return $dataProvider;
    }

    /**
     * @return service\PaymentType[]
     */
    public function getTypeFilterItems()
    {
        if ($this->_query instanceof \yii\db\ActiveQuery) {
            $query = clone $this->_query;
            $table = self::tableName();
            $type = service\PaymentType::tableName();
            $typeIdArray = $query
                ->distinct()
                ->select(["$table.type_id"])
                ->column();

            return service\PaymentType::find()->where(['id' => $typeIdArray])->all();
        } else {
            return service\PaymentType::find()->all();
        }
    }

}
