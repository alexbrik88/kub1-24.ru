<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 12/16/15
 * Time: 4:03 PM
 * Email: t.kanstantsin@gmail.com
 */

namespace frontend\modules\subscribe\models;

use common\models\Company;
use common\models\service\Payment;
use common\models\service\Subscribe;
use common\models\User;
use frontend\modules\subscribe\forms\OnlinePaymentForm;
use Yii;
use yii\base\BaseObject;

/**
 * Class OnlinePaymentLog
 * @package common\models\service
 */
class OnlinePaymentLog extends BaseObject
{

    /**
     * @var Company
     */
    public $company;
    /**
     * @var User
     */
    public $user;
    /**
     * @var PaymentForm
     */
    public $paymentForm;
    /**
     * @var Subscribe
     */
    public $subscribe;
    /**
     * @var Payment
     */
    public $payment;
    /**
     * @var array|null
     */
    public $queryParams;
    /**
     * @var array|null
     */
    public $bodyParams;

    public $hasError = false;

    /**
     *
     */
    public function log()
    {
        $data = [
            'hasError' => \Yii::$app->formatter->asBoolean($this->hasError),

            'queryParams' => $this->queryParams,
            'bodyParams' => $this->bodyParams,

            'company' => $this->company ? $this->company->attributes : null,
            'user' => $this->user ? $this->user->attributes : null,

            'paymentFormAttributes' => [
                'SignatureValue' => $this->paymentForm->SignatureValue,
                'InvId' => $this->paymentForm->InvId,
                'OutSum' => $this->paymentForm->OutSum,
                'Shp_companyId' => $this->paymentForm->Shp_companyId,
                'Shp_employeeId' => $this->paymentForm->Shp_employeeId,
                'IsTest' => $this->paymentForm->IsTest,
                'Culture' => $this->paymentForm->Culture,
            ],
            'paymentFormErrors' => $this->paymentForm->errors,

            'paymentAttributes' => $this->payment ? $this->payment->attributes : null,
            'paymentErrors' => $this->payment ? $this->payment->errors : null,
        ];

        @file_put_contents(
            Yii::getAlias('@runtime/logs/robo-result.log'),
            date('c')."\n".var_export($data, true)."\n\n",
            FILE_APPEND
        );

        \Yii::$app->mailer->compose([
            'html' => 'system/subscribe/payment-log/html',
        ], [
            'data' => $data,
            'subject' => \Yii::$app->params['robokassa'][$this->hasError ? 'errorLogSubject' : 'successLogSubject'],
        ])
            ->setSubject(\Yii::$app->params['robokassa'][$this->hasError ? 'errorLogSubject' : 'successLogSubject'])
            ->setFrom(\Yii::$app->params['emailList']['admin'])
            ->setTo(\Yii::$app->params['emailList']['admin'])
            ->send();
    }

    public function collectPaymentData(OnlinePaymentForm $paymentForm)
    {
        $this->paymentForm = $paymentForm;
        $this->payment = $paymentForm->payment;
        $this->company = $paymentForm->company;
        $this->user = $paymentForm->user;
    }
}
