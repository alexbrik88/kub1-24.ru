<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 11.07.2017
 * Time: 17:55
 */

namespace frontend\modules\subscribe\models;


use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\service\PromoCode;
use common\models\service\PromoCodeGroup;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\modules\subscribe\forms\PromoCodeForm;
use Yii;
use common\models\Company;
use yii\base\BaseObject;
use yii\db\Connection;
use yii\web\BadRequestHttpException;


/**
 * Class RewardPayment
 * @package frontend\modules\subscribe\models
 */
class RewardPayment extends BaseObject
{
    /**
     * @var Company
     */
    public $company;

    /**
     * @var
     */
    public $duration;

    /**
     * @var null
     */
    public $affiliateSum = 0;

    /**
     * @param Company $company
     * @param array $config
     * @throws Exception
     */
    public function __construct(Company $company, $config = [])
    {
        $this->company = $company;

        parent::__construct($config);
    }

    /**
     * @throws BadRequestHttpException
     */
    public function init()
    {
        parent::init();

        $this->duration = $this->calculateDuration();
    }

    /**
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function pay()
    {
        if (!$this->duration) {
            Yii::$app->session->setFlash('error', 'Вашего вознаграждения недостаточно для оплаты подписки.');

            return false;
        }
        return Yii::$app->db->transaction(function (Connection $db) {
            $promoCode = $this->createPromoCode();
            if ($promoCode) {
                $promoCodeForm = new PromoCodeForm();
                $promoCodeForm->company = $this->company;
                $promoCodeForm->code = $promoCode->code;
                if ($promoCodeForm->validate() && $promoCodeForm->save()) {
                    $this->company->payed_reward_for_kub += $this->company->affiliate_sum;
                    $this->affiliateSum = $this->company->affiliate_sum;
                    $this->company->affiliate_sum = 0;
                    if ($this->company->save(true, ['affiliate_sum', 'payed_reward_for_kub'])) {
                        return true;
                    }
                }
            }
            Yii::$app->session->setFlash('error', 'Ошибка при оплате.');
            $db->transaction->rollBack();

            return false;
        });
    }

    /**
     * @return int
     */
    public function calculateDuration()
    {
        /* @var $tariff SubscribeTariff */
        foreach (SubscribeTariff::find()->andWhere(['not', ['id' => SubscribeTariff::TARIFF_TRIAL]])->all() as $tariff) {
            if ($this->company->affiliate_sum < $tariff->price) {
                break;
            }
        }
        return (int) round(($this->company->affiliate_sum * 31) / ($tariff->price / $tariff->duration_month));
    }

    /**
     * @return bool|PromoCode
     */
    public function createPromoCode()
    {
        $promoCode = new PromoCode();
        do {
            $promoCode->code = TextHelper::randString(PromoCode::CODE_MAX_LENGTH);
        } while (PromoCode::find()->byCode($promoCode->code)->exists());
        $promoCode->name = 'Вознаграждение за привличение';
        $promoCode->duration_month = 0;
        $promoCode->duration_day = $this->duration;
        $promoCode->started_at = date(DateHelper::FORMAT_DATE);
        $promoCode->expired_at = date(DateHelper::FORMAT_DATE, strtotime('+ 2 days'));
        $promoCode->tariff_group_id = SubscribeTariffGroup::STANDART;
        $promoCode->group_id = PromoCodeGroup::GROUP_INDIVIDUAL;
        $promoCode->company_id = $this->company->id;
        if ($promoCode->save()) {
            return $promoCode;
        }

        return false;
    }
}