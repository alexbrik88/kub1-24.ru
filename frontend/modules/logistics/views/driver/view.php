<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 21.11.2018
 * Time: 17:31
 */

use common\components\helpers\Html;
use yii\helpers\Url;
use common\components\date\DateHelper;
use frontend\widgets\ConfirmModalWidget;
use common\models\file\widgets\FileUpload;
use frontend\modules\documents\widgets\CreatedByWidget;

/* @var $this yii\web\View */
/* @var $model \common\models\driver\Driver */
/* @var $driverPhone \common\models\driver\DriverPhone */

$this->title = $model->getFio();
$this->context->layoutWrapperCssClass = 'out-sf out-document out-act cash-order';
?>
<div class="page-content-in">
    <?= Html::a('Назад к списку', Url::to(['index']), [
        'class' => 'back-to-customers',
    ]); ?>
    <div class="col-xs-12 pad0">
        <div class="col-xs-12 col-lg-7 pad0">
            <div class="portlet customer-info customer-info-cash">
                <div class="portlet-title">
                    <div class="col-md-10 caption">
                        <?= $model->getFio(); ?>
                    </div>
                    <div class="actions">
                        <?= CreatedByWidget::widget([
                            'createdAt' => date("d.m.Y", $model->created_at),
                            'author' => $model->author ? $model->author->currentEmployeeCompany->getFio() : '',
                        ]); ?>
                        <?= Html::a('<i class="icon-pencil"></i>', Url::to(['update', 'id' => $model->id]), [
                            'class' => 'btn darkblue btn-sm',
                        ]); ?>
                    </div>
                </div>
                <div class="portlet-body no_mrg_bottom">
                    <table id="datatable_ajax" class="table">
                        <tr>
                            <td class="bold-text border-space">Где работает:</td>
                            <td class="">
                                <?= $model->contractor ?
                                    Html::a($model->contractor->getNameWithType(), Url::to(['/contractor/view', 'id' => $model->contractor_id, 'type' => $model->contractor->type])) :
                                    null; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Транспортное средство:</td>
                            <td class="">
                                <?= $model->vehicle ?
                                    Html::a($model->vehicle->getVehicleTypeText(), Url::to(['/logistics/vehicle/view', 'id' => $model->vehicle_id])) :
                                    ''; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Дата рождения:</td>
                            <td class="">
                                <?= DateHelper::format($model->birthday_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Документ:</td>
                            <td class="">
                                <?= $model->identificationType ? $model->identificationType->name : null; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-space">Серия:</td>
                            <td>
                                <span><?= $model->identification_series; ?></span>
                                <span style="margin-left: 20px;">
                                    Номер: <?= $model->identification_number; ?>
                                </span>
                                <span style="margin-left: 20px;">
                                    Дата выдачи: <?= DateHelper::format($model->identification_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-space">Кем выдан:</td>
                            <td class="">
                                <?= $model->identification_issued_by; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Водительское удостоверение:</td>
                        </tr>
                        <tr>
                            <td class="border-space">Серия:</td>
                            <td>
                                <span><?= $model->driver_license_series; ?></span>
                                <span style="margin-left: 20px;">
                                    Номер: <?= $model->driver_license_number; ?>
                                </span>
                                <span style="margin-left: 20px;">
                                    Дата выдачи: <?= DateHelper::format($model->driver_license_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-space">Кем выдан:</td>
                            <td class="">
                                <?= $model->driver_license_issued_by; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Телефоны:</td>
                        </tr>
                        <?php foreach ($model->getDriverPhones()->orderBy(['is_main' => SORT_DESC])->all() as $driverPhone): ?>
                            <tr>
                                <td class="border-space">
                                    <?= $driverPhone->phoneType ? $driverPhone->phoneType->name : null; ?>:
                                </td>
                                <td>
                                    <?= $driverPhone->phone . ($driverPhone->is_main ? ' Основной' : null); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        <tr>
                            <td class="bold-text border-space">Адреса:</td>
                        </tr>
                        <tr>
                            <td class="border-space">Город базирования:</td>
                            <td><?= $model->home_town; ?></td>
                        </tr>
                        <tr>
                            <td class="border-space">Адрес проживания:</td>
                            <td><?= $model->residential_address; ?></td>
                        </tr>
                        <tr>
                            <td class="border-space">Адрес регистрации:</td>
                            <td><?= $model->registration_address; ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Комментарий:</td>
                            <td class=""><?= $model->comment; ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-lg-5 pad0 pull-right">
            <div class="col-xs-12" style="padding-right:0 !important;">
                <div class="control-panel col-xs-12 pad0 pull-right">

                </div>
                <div class="col-xs-12 col-lg-9 pull-right pad3" style="max-width: 720px;">
                    <div class="portlet">
                        <div class="customer-info" style="min-height:auto !important">
                            <div class="portlet-body no_mrg_bottom main_inf_no-bord">
                                <table class="table no_mrg_bottom">
                                    <tr>
                                        <td>
                                            <span class="bold-text">Документы</span>
                                            <div style="margin-bottom: 5px;">
                                                <?= FileUpload::widget([
                                                    'uploadUrl' => Url::to(['file-upload', 'id' => $model->id,]),
                                                    'deleteUrl' => Url::to(['file-delete', 'id' => $model->id,]),
                                                    'listUrl' => Url::to(['file-list', 'id' => $model->id,]),
                                                ]); ?>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="">
                            <div style="margin: 15px 0 0;">
                                <span style="font-weight: bold;">Комментарий</span>
                                <?= Html::tag('span', '', [
                                    'id' => 'comment_update',
                                    'class' => 'glyphicon glyphicon-pencil',
                                    'style' => 'cursor: pointer;',
                                ]); ?>
                            </div>
                            <div id="comment_view" class="">
                                <?= Html::encode($model->comment); ?>
                            </div>
                            <?= Html::beginTag('div', [
                                'id' => 'comment_form',
                                'class' => 'hidden',
                                'style' => 'position: relative;',
                                'data-url' => Url::to(['comment', 'id' => $model->id]),
                            ]) ?>
                            <?= Html::tag('i', '', [
                                'id' => 'comment_save',
                                'class' => 'fa fa-floppy-o',
                                'style' => 'position: absolute; top: -22px; right: 0px; cursor: pointer; font-size: 20px;',
                            ]); ?>
                            <?= Html::textarea('comment', $model->comment, [
                                'id' => 'comment_input',
                                'rows' => 3,
                                'maxlength' => true,
                                'style' => 'width: 100%; padding-right: 35px; border: 1px solid #ddd;',
                            ]); ?>
                            <?= Html::endTag('div') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row action-buttons" id="buttons-fixed">
    <div class="spinner-button col-sm-1 col-xs-1">
        <?= Html::button('Отправить', [
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs send-message-panel-trigger',
        ]); ?>
        <?= Html::button('<span class="ico-Send-smart-pls fs"></span>', [
            'class' => 'btn darkblue widthe-100 hidden-lg send-message-panel-trigger',
            'title' => 'Отправить',
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width: 55%;"></div>
    <div class="spinner-button col-sm-1 col-xs-1"></div>
    <div class="spinner-button col-sm-1 col-xs-1">
        <?= ConfirmModalWidget::widget([
            'toggleButton' => [
                'label' => 'Удалить',
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
            ],
            'confirmUrl' => Url::to(['delete', 'id' => $model->id,]),
            'message' => 'Вы уверены, что хотите удалить водителя?',
        ]); ?>
        <?= ConfirmModalWidget::widget([
            'toggleButton' => [
                'label' => '<i class="fa fa-trash-o fa-2x"></i>',
                'title' => 'Удалить',
                'class' => 'btn darkblue widthe-100 hidden-lg',
            ],
            'confirmUrl' => Url::to(['delete', 'id' => $model->id,]),
            'message' => 'Вы уверены, что хотите удалить водителя?',
        ]); ?>
    </div>
</div>
<?= $this->render('@frontend/modules/documents/views/invoice/view/_send_message', [
    'model' => $model,
    'useContractor' => null,
]); ?>
<?php $this->registerJs('
    $(document).on("click", "#comment_update", function () {
        $("#comment_view").toggleClass("hidden");
        $("#comment_form").toggleClass("hidden");
    });
    $(document).on("click", "#comment_save", function () {
        $.post($("#comment_form").data("url"), $("#comment_input").serialize(), function (data) {
            $("#comment_view").text(data.value);
            $("#comment_form").addClass("hidden");
            $("#comment_view").removeClass("hidden");
        })
    });
');