<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 21.11.2018
 * Time: 21:32
 */

/* @var $this yii\web\View */
/* @var $model \common\models\driver\Driver */
/* @var $company \common\models\Company */
/* @var $newDriverPhone \common\models\driver\DriverPhone */

$this->title = $model->getFio();
?>
<div class="driver-update">
    <?= $this->render('_form', [
        'model' => $model,
        'company' => $company,
        'newDriverPhone' => $newDriverPhone,
    ]); ?>
</div>
