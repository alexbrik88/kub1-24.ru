<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 20.11.2018
 * Time: 11:39
 */

use common\components\helpers\Html;
use yii\helpers\Url;
use common\components\grid\GridView;
use common\components\grid\DataColumn;
use common\models\driver\Driver;
use common\components\grid\DropDownSearchDataColumn;
use common\models\Contractor;

/* @var $this yii\web\View
 * @var $searchModel \frontend\modules\logistics\models\DriverSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $company \common\models\Company
 */

$this->title = 'Водители';
?>
<div class="portlet box">
    <div class="btn-group pull-right title-buttons">
        <?= Html::a('<i class="fa fa-plus"></i> ДОБАВИТЬ', Url::to(['create']), [
            'class' => 'btn yellow',
        ]); ?>
    </div>
    <h3 class="page-title"><?= $this->title; ?></h3>
</div>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Список водителей: <?= $dataProvider->totalCount; ?>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-container clearfix" style="">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'dataColumnClass' => DataColumn::className(),
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'columns' => [
                    [
                        'attribute' => 'fio',
                        'label' => 'Фамилия Имя Отчество',
                        'contentOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Driver $model) {
                            return Html::a($model->getFio(), ['view', 'id' => $model->id]);
                        },
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'vehicle_id',
                        'label' => 'ТС',
                        'contentOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getVehicleFilter(),
                        'value' => function (Driver $model) {
                            $name = null;
                            if ($model->vehicle) {
                                $name .= $model->vehicle->getVehicleTypeText();
                                if ($model->vehicle->model) {
                                    $name .= (", {$model->vehicle->model}");
                                }
                                $name .= (" {$model->vehicle->state_number}");
                            }
                            return Html::a($name, Url::to(['/logistics/vehicle/view', 'id' => $model->vehicle_id]));
                        },
                    ],
                    [
                        'attribute' => 'home_town',
                        'label' => 'Город базирования',
                        'contentOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Driver $model) {
                            return $model->home_town;
                        },
                    ],
                    [
                        'attribute' => 'phone',
                        'label' => 'Телефон',
                        'contentOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Driver $model) {
                            return $model->mainDriverPhone ?
                                ($model->mainDriverPhone->phoneType->name . ': ' . $model->mainDriverPhone->phone) :
                                '';
                        },
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'contractor_id',
                        'label' => 'Где работает',
                        'contentOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getContractorFilter(),
                        'value' => function (Driver $model) {
                            return $model->contractor ?
                                Html::a($model->contractor->nameWithType, Url::to(['/contractor/view', 'id' => $model->contractor_id, 'type' => $model->contractor->type])) :
                                '';
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>