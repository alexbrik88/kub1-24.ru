<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 20.11.2018
 * Time: 22:06
 */

use yii\bootstrap\ActiveForm;
use common\components\helpers\Html;
use kartik\select2\Select2;
use common\models\Contractor;
use common\components\date\DateHelper;
use common\models\driver\IdentificationType;
use common\components\helpers\ArrayHelper;
use common\models\driver\PhoneType;
use yii\widgets\MaskedInput;

/* @var $company \common\models\Company */
/* @var $model \common\models\driver\Driver */
/* @var $newDriverPhone \common\models\driver\DriverPhone */
?>
<?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'id' => 'driver-form',
    'method' => 'POST',
    'options' => [
        'class' => 'form-horizontal b-shadow-hide',
        'novalidate' => 'novalidate',
        'enctype' => 'multipart/form-data',
    ],
    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'col-md-2 control-label bold-text width-212',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-7 inp_one_line',
        ],
        'hintOptions' => [
            'tag' => 'p',
            'class' => 'text-muted',
        ],
        'horizontalCssClasses' => [
            'offset' => 'col-md-offset-2',
            'hint' => 'col-md-7',
        ],
        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
    ],
    'enableClientValidation' => true,
    'enableAjaxValidation' => true,
    'validateOnSubmit' => true,
    'validateOnBlur' => true,
])); ?>
    <div class="form-body form-body_width">
        <?= $form->errorSummary($model); ?>

        <?= $form->field($model, 'last_name', [
            'options' => [
                'class' => 'form-group form-md-line-input form-md-floating-label driver-form-group',
                'style' => 'margin-right: 40px;',
            ],
            'labelOptions' => [
                'class' => '',
            ],
            'template' => "{input}\n{label}\n<span class='help-block'></span>",
        ])->textInput([
            'maxlength' => true,
            'class' => 'form-control' . ($model->last_name ? ' edited' : null),
        ]); ?>

        <?= $form->field($model, 'first_name', [
            'options' => [
                'class' => 'form-group form-md-line-input form-md-floating-label driver-form-group',
                'style' => 'margin-right: 40px;',
            ],
            'labelOptions' => [
                'class' => '',
            ],
            'template' => "{input}\n{label}\n<span class='help-block'></span>",
        ])->textInput([
            'maxlength' => true,
            'class' => 'form-control' . ($model->first_name ? ' edited' : null),
        ]); ?>

        <?= $form->field($model, 'patronymic', [
            'options' => [
                'class' => 'form-group form-md-line-input form-md-floating-label driver-form-group',
                'style' => 'margin-right: 40px;',
            ],
            'labelOptions' => [
                'class' => '',
            ],
            'template' => "{input}\n{label}\n<span class='help-block'></span>",
        ])->textInput([
            'maxlength' => true,
            'class' => 'form-control' . ($model->patronymic ? ' edited' : null),
        ]); ?>

        <?= $form->field($model, 'contractor_id')->widget(Select2::classname(), [
            'data' => ['add-modal-contractor' => '[ + Добавить покупателя ]'] + $company->sortedContractorList(Contractor::TYPE_SELLER),
            'options' => [
                'placeholder' => '',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]); ?>

        <?= $form->field($model, 'vehicle_id')->dropDownList($company->getVehicleList()); ?>

        <?= $form->field($model, 'birthday_date', [
            'wrapperOptions' => [
                'class' => 'col-md-2 inp_one_line',
            ],
            'template' => Yii::$app->params['formDatePickerTemplate'],
        ])->textInput([
            'class' => 'form-control date-picker min_wid_picker',
            'value' => DateHelper::format($model->birthday_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
        ]); ?>

        <?= $form->field($model, 'identification_type_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(IdentificationType::find()->orderBy('sort')->all(), 'id', 'name'),
            'options' => [
                'id' => 'driver-identification_type_id',
                'placeholder' => '',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]); ?>

        <div class="form-group">
            <?= $form->field($model, 'identification_series', [
                'options' => [
                    'class' => '',
                ],
                'labelOptions' => [
                    'class' => 'col-md-2 control-label width-212',
                ],
                'wrapperOptions' => [
                    'class' => 'col-md-2 inp_one_line',
                ],
                'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
            ])->textInput([
                'maxlength' => true,
            ]); ?>

            <?= $form->field($model, 'identification_number', [
                'options' => [
                    'class' => '',
                ],
                'labelOptions' => [
                    'class' => 'col-md-2 control-label',
                    'style' => 'width: 60px!important;padding-right: 0;',
                ],
                'wrapperOptions' => [
                    'class' => 'col-md-2 inp_one_line',
                ],
                'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
            ])->textInput([
                'maxlength' => true,
            ]); ?>

            <?= $form->field($model, 'identification_date', [
                'options' => [
                    'class' => '',
                ],
                'labelOptions' => [
                    'class' => 'col-md-2 control-label width-130',
                    'style' => 'width: 105px!important;padding-right: 0;',
                ],
                'wrapperOptions' => [
                    'class' => 'col-md-2 inp_one_line',
                ],
                'template' => Yii::$app->params['formDatePickerTemplate'],
            ])->textInput([
                'class' => 'form-control date-picker min_wid_picker',
                'value' => DateHelper::format($model->identification_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ]); ?>
        </div>

        <?= $form->field($model, 'identification_issued_by', [
            'labelOptions' => [
                'class' => 'col-md-2 control-label width-212',
            ],
        ])->textInput([
            'maxlength' => true,
        ]); ?>

        <div class="form-group">
            <label class="col-md-12 control-label bold-text header-label">
                Водительское удостоверение
            </label>
        </div>

        <div class="form-group">
            <?= $form->field($model, 'driver_license_series', [
                'options' => [
                    'class' => '',
                ],
                'labelOptions' => [
                    'class' => 'col-md-2 control-label width-212',
                ],
                'wrapperOptions' => [
                    'class' => 'col-md-2 inp_one_line',
                ],
                'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
            ])->textInput([
                'maxlength' => true,
            ]); ?>

            <?= $form->field($model, 'driver_license_number', [
                'options' => [
                    'class' => '',
                ],
                'labelOptions' => [
                    'class' => 'col-md-2 control-label width-130',
                    'style' => 'width: 60px!important;padding-right: 0;',
                ],
                'wrapperOptions' => [
                    'class' => 'col-md-2 inp_one_line',
                ],
                'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
            ])->textInput([
                'maxlength' => true,
            ]); ?>

            <?= $form->field($model, 'driver_license_date', [
                'options' => [
                    'class' => '',
                ],
                'labelOptions' => [
                    'class' => 'col-md-2 control-label width-130',
                    'style' => 'width: 105px!important;padding-right: 0;',
                ],
                'wrapperOptions' => [
                    'class' => 'col-md-2 inp_one_line',
                ],
                'template' => Yii::$app->params['formDatePickerTemplate'],
            ])->textInput([
                'class' => 'form-control date-picker min_wid_picker',
                'value' => DateHelper::format($model->identification_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ]); ?>
        </div>

        <?= $form->field($model, 'driver_license_issued_by', [
            'labelOptions' => [
                'class' => 'col-md-2 control-label width-212',
            ],
        ])->textInput([
            'maxlength' => true,
        ]); ?>

        <div class="form-group">
            <label class="col-md-12 control-label bold-text header-label">
                Телефоны
            </label>
        </div>
        <div class="driver_phone-list">
            <?php foreach ($model->driverPhones as $driverPhone): ?>
                <div class="form-group drive_phone-block">
                    <div class="col-md-2 inp_one_line">
                        <?= Html::activeDropDownList($driverPhone, 'phone_type_id',
                            ArrayHelper::map(PhoneType::find()->all(), 'id', 'name'), [
                                'class' => 'form-control',
                                'id' => "driverphone-phone_type_id-{$driverPhone->id}",
                                'name' => "DriverPhone[{$driverPhone->id}][phone_type_id]",
                            ]); ?>
                    </div>
                    <div class="col-md-4 inp_one_line">
                        <?= MaskedInput::widget([
                            'model' => $driverPhone,
                            'attribute' => 'phone',
                            'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                            'options' => [
                                'id' => "driverphone-phone-{$driverPhone->id}",
                                'class' => 'form-control',
                                'placeholder' => '+7(___) ___-__-__',
                                'name' => "DriverPhone[{$driverPhone->id}][phone]",
                            ],
                        ]); ?>
                    </div>
                    <div class="col-md-2 inp_one_line" style="margin-top: 6px;">
                        <?= Html::activeCheckbox($driverPhone, 'is_main', [
                            'label' => 'Основной',
                            'id' => "driverphone-is_main-{$driverPhone->id}",
                            'name' => "DriverPhone[{$driverPhone->id}][is_main]",
                        ]); ?>
                    </div>
                    <div class="col-md-1">
                        <span class="icon-close delete-driver_phone"></span>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <?= $form->field($model, 'phone', [
            'options' => [
                'class' => 'form-group',
                'style' => 'position: relative;top: -15px;margin-bottom: 0;',
            ],
        ])->hiddenInput()->label(false); ?>

        <span class="add-driver_phone" style="margin-bottom: 15px;">
            <i class="fa fa-plus"></i> ДОБАВИТЬ ТЕЛЕФОН
        </span>

        <div class="form-group template-driver_phone_block hidden">
            <div class="col-md-2 inp_one_line">
                <?= Html::activeDropDownList($newDriverPhone, 'phone_type_id',
                    ArrayHelper::map(PhoneType::find()->all(), 'id', 'name'), [
                        'class' => 'form-control',
                        'id' => 'driverphone-phone_type_id-new_0',
                        'name' => '',
                    ]); ?>
            </div>
            <div class="col-md-4 inp_one_line">
                <?= MaskedInput::widget([
                    'model' => $newDriverPhone,
                    'attribute' => 'phone',
                    'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                    'options' => [
                        'id' => 'driverphone-phone-new_0',
                        'class' => 'form-control',
                        'placeholder' => '+7(___) ___-__-__',
                        'name' => '',
                    ],
                ]); ?>
            </div>
            <div class="col-md-2 inp_one_line" style="margin-top: 6px;">
                <?= Html::activeCheckbox($newDriverPhone, 'is_main', [
                    'class' => 'no-uniform',
                    'label' => 'Основной',
                    'id' => 'driverphone-is_main-new_0',
                    'name' => '',
                ]); ?>
            </div>
            <div class="col-md-1">
                <span class="icon-close delete-driver_phone"></span>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-12 control-label bold-text">
                Адреса
            </label>
        </div>

        <?= $form->field($model, 'home_town', [
            'labelOptions' => [
                'class' => 'col-md-2 control-label width-212',
            ],
        ])->textInput([
            'maxlength' => true,
        ]); ?>

        <?= $form->field($model, 'residential_address', [
            'labelOptions' => [
                'class' => 'col-md-2 control-label width-212',
            ],
        ])->textInput([
            'maxlength' => true,
        ]); ?>

        <?= $form->field($model, 'registration_address', [
            'labelOptions' => [
                'class' => 'col-md-2 control-label width-212',
            ],
        ])->textInput([
            'maxlength' => true,
        ]); ?>

        <div class="form-group">
            <label class="col-md-12 control-label bold-text">
                Комментарий
            </label>
        </div>

        <?= $form->field($model, 'comment', [
            'wrapperOptions' => [
                'class' => 'col-md-7 inp_one_line',
            ],
            'template' => "{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
        ])->textarea([
            'rows' => 6,
        ]); ?>
    </div>
    <div class="form-actions">
        <div class="row action-buttons" id="buttons-fixed">
            <div class="spinner-button col-sm-1 col-xs-1">
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                    'form' => 'employee-form',
                    'data-style' => 'expand-right',
                ]); ?>
                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
                    'title' => 'Сохранить',
                    'form' => 'employee-form',
                ]); ?>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                <a href="<?= Yii::$app->request->referrer; ?>">
                    <button type="button" class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs">
                        Отменить
                    </button>
                    <button type="button" class="btn darkblue widthe-100 hidden-lg" title="Отменить">
                        <i class="fa fa-reply fa-2x"></i>
                    </button>
                </a>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>
    <div class="modal fade t-p-f modal_scroll_center mobile-modal" id="add-new" tabindex="-1" role="modal"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
                <div class="modal-body" id="block-modal-new-product-form">

                </div>
            </div>
        </div>
    </div>
<?php $this->registerJs('
    $(document).on("keyup change", ".driver-form-group input", function (e) {
        if ($(this).val() !== "") {
            $(this).addClass("edited");
        } else {
            $(this).removeClass("edited");
        }
    });
    
    var $phoneTmpID = 1;
    $(document).on("click", ".add-driver_phone", function (e) {
        var $phoneList = $(".driver_phone-list");
        var $phoneListLength = $phoneList.find(".drive_phone-block").length;
        
        if ($phoneListLength < 5) {
            var $driverPhoneBlock = $(".template-driver_phone_block").clone();
            
            $driverPhoneBlock.removeClass("hidden")
                .removeClass("template-driver_phone_block")
                .addClass("drive_phone-block");
            
            $driverPhoneBlock.find("#driverphone-phone_type_id-new_0")
                .attr("id", "driverphone-phone_type_id-new_" + $phoneTmpID)
                .attr("name", "DriverPhone[new][" + $phoneTmpID + "][phone_type_id]");
            
            $driverPhoneBlock.find("#driverphone-phone-new_0")
                .attr("id", "driverphone-phone-new_" + $phoneTmpID)
                .attr("name", "DriverPhone[new][" + $phoneTmpID + "][phone]")
                .inputmask({"mask": "+7(9{3}) 9{3}-9{2}-9{2}"});
                
            $driverPhoneBlock.find("input[type=\'checkbox\']")
                .attr("id", "driverphone-is_main-new_" + $phoneTmpID)
                .attr("name", "DriverPhone[new][" + $phoneTmpID + "][is_main]")
                .uniform("refresh");
            if ($phoneListLength == 0) {
                $driverPhoneBlock.find("input[type=\'checkbox\']").click();
            }
            $(".driver_phone-list").append($driverPhoneBlock);
            $phoneTmpID++;
        }
    });
    
    $(document).on("click", ".driver_phone-list .drive_phone-block .delete-driver_phone", function (e) {        
        if ($(".driver_phone-list .drive_phone-block").length > 1) {
            $(this).closest(".drive_phone-block").remove();
            if ($(".drive_phone-block input[type=\'checkbox\']:checked").length == 0) {
                $(".drive_phone-block input[type=\'checkbox\']:first").click();
            }
            checkPhone();
        }
    });
    
    $(document).on("change", ".drive_phone-block input[type=\'checkbox\']", function (e) {
       var $clickedID = $(this).attr("id");
       if ($(this).is(":checked")) {
           $(".drive_phone-block input[type=\'checkbox\']:checked:not(\'#" + $clickedID + "\')").prop("checked", false).uniform("refresh");
       } else if ($(".drive_phone-block input[type=\'checkbox\']:checked").length == 0) { 
           $(".drive_phone-block input[type=\'checkbox\']:first").prop("checked", true).uniform("refresh");
        }
    });
    
    $(document).on("change", ".driver_phone-list .drive_phone-block input:text", function (e) {
        checkPhone();
    });
    
    function checkPhone() {
        var $isSetPhone = false;
        $(".driver_phone-list .drive_phone-block input:text").each(function () {
            if ($(this).val() !== "") {
                $("#driver-phone").val($(this).val());
                $isSetPhone = true;
            }
        });
        if ($isSetPhone == false) {
            $("#driver-phone").val("");
        }
    }
');
