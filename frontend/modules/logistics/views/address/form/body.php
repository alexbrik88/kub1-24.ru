<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 27.11.2018
 * Time: 14:36
 */

use common\models\loadingAndUnloadingAddress\LoadingAndUnloadingAddress;
use common\components\helpers\Html;
use kartik\select2\Select2;
use common\models\Contractor;
use common\components\helpers\ArrayHelper;
use common\models\driver\PhoneType;

/* @var $this yii\web\View */
/* @var $model LoadingAndUnloadingAddress */
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $company \common\models\Company */
/* @var $newAddressContactPerson \common\models\loadingAndUnloadingAddress\LoadingAndUnloadingAddressContactPerson */

$config = [
    'options' => [
        'class' => 'col-md-12 pad0',
        'style' => 'margin-bottom: 15px;',
    ],
    'labelOptions' => [
        'class' => 'col-md-2 control-label',
    ],
    'inputOptions' => [
        'style' => 'width: 100%;',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-5 inp_one_line',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
?>
    <div class="row">
        <?= $form->field($model, 'address_type', $config)->radioList(LoadingAndUnloadingAddress::$types, [
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::tag('label', Html::radio($name, $checked, ['value' => $value]) . $label, [
                    'class' => 'radio-inline p-o radio-padding',
                ]);
            },
        ])->label('Тип:'); ?>

        <?= $form->field($model, 'contractor_id', $config)
            ->widget(Select2::classname(), [
                'data' => ['add-modal-contractor' => '[ + Добавить заказчика ]'] + $company->sortedContractorList(Contractor::TYPE_CUSTOMER),
                'options' => [
                    'placeholder' => '',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])->label('Заказчик:'); ?>

        <div class="col-md-12 pad0" style="margin-bottom: 15px;">
            <?= $form->field($model, 'city', array_merge($config, [
                'options' => [
                    'class' => '',
                ],
                'labelOptions' => [
                    'class' => 'col-md-2 control-label',
                ],
                'inputOptions' => [
                    'style' => 'width: 100%;',
                    'placeHolder' => 'Город',
                ],
                'wrapperOptions' => [
                    'class' => 'col-md-3 inp_one_line',
                ],
                'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
            ]))->label('Адрес:') ?>

            <?= $form->field($model, 'address', array_merge($config, [
                'options' => [
                    'class' => '',
                ],
                'inputOptions' => [
                    'style' => 'width: 100%;',
                ],
                'wrapperOptions' => [
                    'class' => 'col-md-4 inp_one_line',
                ],
                'template' => "{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
            ]))->label('Адрес:') ?>
        </div>

        <?= $form->field($model, 'consignor', $config)->label('Грузоотправитель:'); ?>

        <?= $form->field($model, 'consignee', $config)->label('Грузополучатель:'); ?>

        <div class="address-contact_person_list">
            <?php foreach ($model->loadingAndUnloadingAddressContactPeople as $loadingAndUnloadingAddressContactPerson): ?>
                <div class="col-md-12 pad0 address-contact_person-block" style="margin-bottom: 15px;">
                    <?= $form->field($loadingAndUnloadingAddressContactPerson, 'contact_person', array_merge($config, [
                        'options' => [
                            'class' => '',
                        ],
                        'labelOptions' => [
                            'class' => 'col-md-2 control-label',
                        ],
                        'inputOptions' => [
                            'name' => "LoadingAndUnloadingAddressContactPerson[{$loadingAndUnloadingAddressContactPerson->id}][contact_person]",
                            'id' => 'loadingandunloadingaddresscontactperson-contact_person-' . $loadingAndUnloadingAddressContactPerson->id,
                            'style' => 'width: 100%;',
                        ],
                        'wrapperOptions' => [
                            'class' => 'col-md-3 inp_one_line',
                        ],
                        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                    ]))->label('Контактное лицо:'); ?>

                    <?= $form->field($loadingAndUnloadingAddressContactPerson, 'phone_type_id', array_merge($config, [
                        'options' => [
                            'class' => '',
                        ],
                        'labelOptions' => [
                            'class' => 'col-md-1 control-label',
                        ],
                        'inputOptions' => [
                            'name' => "LoadingAndUnloadingAddressContactPerson[{$loadingAndUnloadingAddressContactPerson->id}][phone_type_id]",
                            'id' => 'loadingandunloadingaddresscontactperson-phone_type_id-' . $loadingAndUnloadingAddressContactPerson->id,
                            'style' => 'width: 100%;',
                        ],
                        'wrapperOptions' => [
                            'class' => 'col-md-2 inp_one_line',
                        ],
                        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                    ]))->dropDownList(ArrayHelper::map(PhoneType::find()->all(), 'id', 'name'))->label('Телефон:'); ?>

                    <?= $form->field($loadingAndUnloadingAddressContactPerson, 'phone', array_merge($config, [
                        'options' => [
                            'class' => '',
                        ],
                        'inputOptions' => [
                            'name' => "LoadingAndUnloadingAddressContactPerson[{$loadingAndUnloadingAddressContactPerson->id}][phone]",
                            'id' => 'loadingandunloadingaddresscontactperson-phone-' . $loadingAndUnloadingAddressContactPerson->id,
                            'style' => 'width: 100%;',
                        ],
                        'wrapperOptions' => [
                            'class' => 'col-md-2 inp_one_line',
                        ],
                        'template' => "{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                    ])); ?>

                    <?= $form->field($loadingAndUnloadingAddressContactPerson, 'is_main', array_merge($config, [
                        'options' => [
                            'class' => '',
                        ],
                        'inputOptions' => [
                            'style' => 'width: 100%;',
                        ],
                        'wrapperOptions' => [
                            'class' => 'col-md-2',
                            'style' => 'padding-top: 6px;width: 10%;padding-right: 0;',
                        ],
                        'horizontalCssClasses' => [
                            'offset' => '',
                            'hint' => 'col-md-8',
                        ],
                        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                    ]))->checkbox([
                        'name' => "LoadingAndUnloadingAddressContactPerson[{$loadingAndUnloadingAddressContactPerson->id}][is_main]",
                        'id' => 'loadingandunloadingaddresscontactperson-is_main-' . $loadingAndUnloadingAddressContactPerson->id,
                    ])->label('Основной'); ?>
                    <div class="col-md-1" style="width: 5%">
                        <span class="icon-close delete-contact_person"></span>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <div class="col-md-12 pad0">
            <?= $form->field($model, 'contactPerson', [
                'options' => [
                    'class' => '',
                    'style' => 'position: relative;top: -15px;margin-bottom: 0;',
                ],
                'horizontalCssClasses' => [
                    'offset' => '',
                    'hint' => 'col-md-8',
                ],
                'wrapperOptions' => [
                    'class' => 'col-md-6 inp_one_line',
                    'style' => 'margin-left: 17%;',
                ],
            ])->hiddenInput()->label(false); ?>
        </div>

        <span class="add-contact_person col-md-12" style="margin-bottom: 15px;">
            <i class="fa fa-plus"></i> Добавить Контактное лицо
        </span>

        <div class="col-md-12 pad0 template-address_contact_person_block hidden" style="margin-bottom: 15px;">
            <?= $form->field($newAddressContactPerson, 'contact_person', array_merge($config, [
                'options' => [
                    'class' => '',
                ],
                'labelOptions' => [
                    'class' => 'col-md-2 control-label',
                ],
                'inputOptions' => [
                    'name' => '',
                    'style' => 'width: 100%;',
                ],
                'wrapperOptions' => [
                    'class' => 'col-md-3 inp_one_line',
                ],
                'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
            ]))->label('Контактное лицо:'); ?>

            <?= $form->field($newAddressContactPerson, 'phone_type_id', array_merge($config, [
                'options' => [
                    'class' => '',
                ],
                'labelOptions' => [
                    'class' => 'col-md-1 control-label',
                ],
                'inputOptions' => [
                    'name' => '',
                    'style' => 'width: 100%;',
                ],
                'wrapperOptions' => [
                    'class' => 'col-md-2 inp_one_line',
                ],
                'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
            ]))->dropDownList(ArrayHelper::map(PhoneType::find()->all(), 'id', 'name'))->label('Телефон:'); ?>

            <?= $form->field($newAddressContactPerson, 'phone', array_merge($config, [
                'options' => [
                    'class' => '',
                ],
                'inputOptions' => [
                    'name' => '',
                    'style' => 'width: 100%;',
                ],
                'wrapperOptions' => [
                    'class' => 'col-md-2 inp_one_line',
                ],
                'template' => "{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
            ])); ?>

            <?= $form->field($newAddressContactPerson, 'is_main', array_merge($config, [
                'options' => [
                    'class' => '',
                ],
                'inputOptions' => [
                    'style' => 'width: 100%;',
                ],
                'wrapperOptions' => [
                    'class' => 'col-md-2',
                    'style' => 'padding-top: 6px;width: 10%;padding-right: 0;',
                ],
                'horizontalCssClasses' => [
                    'offset' => '',
                    'hint' => 'col-md-8',
                ],
                'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
            ]))->checkbox([
                'name' => '',
                'class' => 'no-uniform',
            ])->label('Основной'); ?>
            <div class="col-md-1" style="width: 5%">
                <span class="icon-close delete-contact_person"></span>
            </div>
        </div>

        <?= $form->field($model, 'status', $config)->dropDownList(LoadingAndUnloadingAddress::$statuses)->label('Статус:'); ?>

        <?= $form->field($model, 'map_width', $config)->label('Широта:'); ?>

        <?= $form->field($model, 'map_longitude', $config)->label('Долгота:'); ?>
    </div>
<?php $this->registerJs('
    var $contactPersonTmpID = 1;
    $(document).on("click", ".add-contact_person", function (e) {
        var $contactPersonList = $(".address-contact_person_list");
        var $contactPersonListLength = $contactPersonList.find(".address-contact_person-block").length;
        
        if ($contactPersonListLength < 3) {            
            var $addressContactPersonBlock = $(".template-address_contact_person_block").clone();
        
            $addressContactPersonBlock.removeClass("hidden")
                .removeClass("template-address_contact_person_block")
                .addClass("address-contact_person-block");
                
            $addressContactPersonBlock.find("#loadingandunloadingaddresscontactperson-contact_person")
                .attr("name", "LoadingAndUnloadingAddressContactPerson[new][" + $contactPersonTmpID + "][contact_person]")
                .attr("id", "loadingandunloadingaddresscontactperson-contact_person-new_" + $contactPersonTmpID);
                
            $addressContactPersonBlock.find("#loadingandunloadingaddresscontactperson-phone_type_id")
                .attr("name", "LoadingAndUnloadingAddressContactPerson[new][" + $contactPersonTmpID + "][phone_type_id]")
                .attr("id", "loadingandunloadingaddresscontactperson-phone_type_id-new_" + $contactPersonTmpID);
                
            $addressContactPersonBlock.find("#loadingandunloadingaddresscontactperson-phone")
                .attr("name", "LoadingAndUnloadingAddressContactPerson[new][" + $contactPersonTmpID + "][phone]")
                .attr("id", "loadingandunloadingaddresscontactperson-phone-new_" + $contactPersonTmpID)
                .inputmask({"mask": "+7(9{3}) 9{3}-9{2}-9{2}"});
                
            $addressContactPersonBlock.find("#loadingandunloadingaddresscontactperson-is_main")
                .attr("name", "LoadingAndUnloadingAddressContactPerson[new][" + $contactPersonTmpID + "][is_main]")
                .attr("id", "loadingandunloadingaddresscontactperson-is_main-new_" + $contactPersonTmpID)
                .uniform("refresh");
            
            $addressContactPersonBlock.find("#loadingandunloadingaddresscontactperson-is_main-new_" + $contactPersonTmpID)
                .closest("label")
                .attr("for", "loadingandunloadingaddresscontactperson-is_main-new_" + $contactPersonTmpID);
            
            if ($contactPersonListLength == 0) {
                $addressContactPersonBlock.find("input[type=\'checkbox\']").click();
            }
            $(".address-contact_person_list").append($addressContactPersonBlock);
            $contactPersonTmpID++;
        }
    });
    
    $(document).on("click", ".address-contact_person_list .address-contact_person-block .delete-contact_person", function (e) {        
        if ($(".address-contact_person_list .address-contact_person-block").length > 1) {
            $(this).closest(".address-contact_person-block").remove();
            if ($(".address-contact_person-block input[type=\'checkbox\']:checked").length == 0) {
                $(".address-contact_person-block input[type=\'checkbox\']:first").click();
            }
            checkContactPerson();
        }
    });
    
    $(document).on("change", ".address-contact_person-block input[type=\'checkbox\']", function (e) {
       var $clickedID = $(this).attr("id");
       if ($(this).is(":checked")) {
           $(".address-contact_person-block input[type=\'checkbox\']:checked:not(\'#" + $clickedID + "\')").prop("checked", false).uniform("refresh");
       } else if ($(".address-contact_person-block input[type=\'checkbox\']:checked").length == 0) { 
           $(".address-contact_person-block input[type=\'checkbox\']:first").prop("checked", true).uniform("refresh");
        }
    });
    
    $(document).on("change", ".address-contact_person_list .address-contact_person-block input:text", function (e) {
        checkContactPerson();
    });
    
    function checkContactPerson() {
        var $isSetContactPerson = false;
        $(".address-contact_person_list .address-contact_person-block").each(function () {
            var $contactPersonVal = $(this).find(".field-loadingandunloadingaddresscontactperson-contact_person input").val();
            if ($contactPersonVal !== "" && $(this).find(".field-loadingandunloadingaddresscontactperson-phone input").val() !== "") {
                $("#loadingandunloadingaddress-contactperson").val($contactPersonVal);
                $isSetContactPerson = true;
            }
        });
        if ($isSetContactPerson == false) {
            $("#loadingandunloadingaddress-contactperson").val("");
        }
    }
');