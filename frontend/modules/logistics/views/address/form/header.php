<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 27.11.2018
 * Time: 14:28
 */

use common\components\helpers\Html;
use common\components\date\DateHelper;

/* @var $this yii\web\View */
/* @var $model \common\models\loadingAndUnloadingAddress\LoadingAndUnloadingAddress */
?>
<div class="portlet-title">
    <div class="caption" style="width: 100%">
        <table cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td valign="middle" style="width:1%; white-space:nowrap;">
                    <span>
                        Адрес №
                        <?= Html::activeTextInput($model, 'number', [
                            'id' => 'account-number',
                            'data-required' => 1,
                            'class' => 'form-control',
                        ]); ?>

                        <br class="box-br">
                    </span>
                    <span class="box-margin-top-t">от</span>
                    <div class="input-icon box-input-icon-top"
                         style="display: inline-block; vertical-align: top; margin-left: 6px; margin-right: 8px;">
                        <i class="fa fa-calendar"></i>
                        <?= Html::activeTextInput($model, 'date', [
                            'id' => 'under-date',
                            'class' => 'form-control date-picker',
                            'size' => 16,
                            'data-date' => '12-02-2012',
                            'data-date-viewmode' => 'years',
                            'value' => DateHelper::format($model->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>

