<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 27.11.2018
 * Time: 14:00
 */

use common\components\helpers\Html;
use yii\helpers\Url;
use common\components\grid\GridView;
use common\models\loadingAndUnloadingAddress\LoadingAndUnloadingAddress;
use common\components\grid\DataColumn;
use common\components\grid\DropDownSearchDataColumn;

/* @var $this yii\web\View
 * @var $searchModel \frontend\modules\logistics\models\AddressSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 */
$this->title = 'Адреса Погрузки/Разгрузки';
?>
<div class="portlet box">
    <div class="btn-group pull-right title-buttons">
        <?= Html::a('<i class="fa fa-plus"></i> ДОБАВИТЬ', Url::to(['create']), [
            'class' => 'btn yellow',
        ]); ?>
    </div>
    <h3 class="page-title"><?= $this->title; ?></h3>
</div>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Список адресов: <?= $dataProvider->totalCount; ?>
        </div>
        <div class="actions joint-operations col-sm-4 pull-left" style="display:none;">
            <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                'class' => 'btn btn-default btn-sm',
                'data-toggle' => 'modal',
            ]); ?>
            <div id="many-delete" class="confirm-modal fade modal" role="dialog"
                 tabindex="-1" aria-hidden="true"
                 style="display: none; margin-top: -51.5px;">
                <div class="modal-dialog ">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="form-body">
                                <div class="row">Вы уверены, что хотите удалить выбранные адреса?
                                </div>
                            </div>
                            <div class="form-actions row">
                                <div class="col-xs-6">
                                    <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
                                        'class' => 'btn darkblue pull-right modal-many-delete ladda-button',
                                        'data-url' => Url::to(['many-delete',]),
                                        'data-style' => 'expand-right',
                                    ]); ?>
                                </div>
                                <div class="col-xs-6">
                                    <button type="button" class="btn darkblue" data-dismiss="modal">
                                        НЕТ
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-container clearfix" style="">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'dataColumnClass' => DataColumn::className(),
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'columns' => [
                    [
                        'header' => Html::checkbox('', false, [
                            'class' => 'joint-operation-main-checkbox',
                        ]),
                        'headerOptions' => [
                            'class' => 'text-center pad0',
                            'width' => '5%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-center pad0-l pad0-r',
                        ],
                        'format' => 'raw',
                        'value' => function (LoadingAndUnloadingAddress $model) {
                            return Html::checkbox('LoadingAndUnloadingAddress[' . $model->id . '][checked]', false, [
                                'class' => 'joint-operation-checkbox',
                            ]);
                        },
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'address_type',
                        'label' => 'Погрузка/Разгрузка',
                        'contentOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'format' => 'raw',
                        'filter' => [null => 'Все'] + LoadingAndUnloadingAddress::$types,
                        'value' => function (LoadingAndUnloadingAddress $model) {
                            return Html::a(LoadingAndUnloadingAddress::$types[$model->address_type] . ' №' . $model->number,
                                Url::to(['view', 'id' => $model->id]));
                        },
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'contractor_id',
                        'label' => 'Заказчик',
                        'contentOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getContractorFilter(),
                        'value' => function (LoadingAndUnloadingAddress $model) {
                            return $model->contractor ?
                                Html::a($model->contractor->getNameWithType(), Url::to(['/contractor/view', 'id' => $model->contractor_id, 'type' => $model->contractor->type])) :
                                '';
                        },
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'city',
                        'label' => 'Город',
                        'contentOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getCityFilter(),
                        'value' => function (LoadingAndUnloadingAddress $model) {
                            return $model->city;
                        },
                    ],
                    [
                        'attribute' => 'address',
                        'label' => 'Адрес',
                        'contentOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (LoadingAndUnloadingAddress $model) {
                            return $model->address;
                        },
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'mainContactPerson',
                        'label' => 'Контактное лицо',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getContactPersonFilter(),
                        'value' => function (LoadingAndUnloadingAddress $model) {
                            return $model->mainLoadingAndUnloadingAddressContactPerson ?
                                $model->mainLoadingAndUnloadingAddressContactPerson->contact_person :
                                '';
                        },
                    ],
                    [
                        'attribute' => 'mainContactPersonPhone',
                        'label' => 'Телефон',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (LoadingAndUnloadingAddress $model) {
                            return $model->mainLoadingAndUnloadingAddressContactPerson ?
                                $model->mainLoadingAndUnloadingAddressContactPerson->phone :
                                '';
                        },
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'status',
                        'label' => 'Статус',
                        'contentOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'format' => 'raw',
                        'filter' => [null => 'Все'] + LoadingAndUnloadingAddress::$statuses,
                        'value' => function (LoadingAndUnloadingAddress $model) {
                            return $model->status ? LoadingAndUnloadingAddress::$statuses[$model->status] : '';
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
