<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 27.11.2018
 * Time: 14:24
 */

/* @var $this yii\web\View */
/* @var $model \common\models\loadingAndUnloadingAddress\LoadingAndUnloadingAddress */
/* @var $company \common\models\Company */
/* @var $newAddressContactPerson \common\models\loadingAndUnloadingAddress\LoadingAndUnloadingAddressContactPerson */

$this->title = 'Добавить адрес';
?>
<div class="loading_and_unloading_address-create">
    <?= $this->render('_form', [
        'model' => $model,
        'company' => $company,
        'newAddressContactPerson' => $newAddressContactPerson,
    ]); ?>
</div>
<?php $this->registerJs('
    $(document).ready(function (e) {
        var $addressContactPersonBlock = $(".template-address_contact_person_block").clone();
        
        $addressContactPersonBlock.removeClass("hidden")
            .removeClass("template-address_contact_person_block")
            .addClass("address-contact_person-block");
            
        $addressContactPersonBlock.find("#loadingandunloadingaddresscontactperson-contact_person")
            .attr("name", "LoadingAndUnloadingAddressContactPerson[new][0][contact_person]")
            .attr("id", "loadingandunloadingaddresscontactperson-contact_person-new_0");
            
        $addressContactPersonBlock.find("#loadingandunloadingaddresscontactperson-phone_type_id")
            .attr("name", "LoadingAndUnloadingAddressContactPerson[new][0][phone_type_id]")
            .attr("id", "loadingandunloadingaddresscontactperson-phone_type_id-new_0");
            
        $addressContactPersonBlock.find("#loadingandunloadingaddresscontactperson-phone")
            .attr("name", "LoadingAndUnloadingAddressContactPerson[new][0][phone]")
            .attr("id", "loadingandunloadingaddresscontactperson-phone-new_0")
            .inputmask({"mask": "+7(9{3}) 9{3}-9{2}-9{2}"});
            
        $addressContactPersonBlock.find("#loadingandunloadingaddresscontactperson-is_main")
            .attr("name", "LoadingAndUnloadingAddressContactPerson[new][0][is_main]")
            .attr("id", "loadingandunloadingaddresscontactperson-is_main-new_0")
            .uniform("refresh")
            .click();
            
        $addressContactPersonBlock.find("#loadingandunloadingaddresscontactperson-is_main-new_0")
            .closest("label")
            .attr("for", "loadingandunloadingaddresscontactperson-is_main-new_0");
        
        $(".address-contact_person_list").append($addressContactPersonBlock);
    });
');
