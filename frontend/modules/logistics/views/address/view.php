<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 27.11.2018
 * Time: 19:53
 */

use common\components\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\ConfirmModalWidget;
use common\models\loadingAndUnloadingAddress\LoadingAndUnloadingAddress;
use frontend\modules\documents\widgets\CreatedByWidget;

/* @var $this yii\web\View */
/* @var $model LoadingAndUnloadingAddress */
/* @var $contactPerson \common\models\loadingAndUnloadingAddress\LoadingAndUnloadingAddressContactPerson */

$this->title = LoadingAndUnloadingAddress::$types[$model->address_type] . ' №' . $model->number;
$this->context->layoutWrapperCssClass = 'out-sf out-document out-act cash-order';
?>
<div class="page-content-in">
    <?= Html::a('Назад к списку', Url::to(['index']), [
        'class' => 'back-to-customers',
    ]); ?>
    <div class="col-xs-12 pad0">
        <div class="col-xs-12 col-lg-7 pad0">
            <div class="portlet customer-info customer-info-cash">
                <div class="portlet-title">
                    <div class="col-md-10 caption">
                        <?= $this->title; ?>
                    </div>
                    <div class="actions">
                        <?= CreatedByWidget::widget([
                            'createdAt' => date("d.m.Y", $model->created_at),
                            'author' => $model->author ? $model->author->currentEmployeeCompany->getFio() : '',
                        ]); ?>
                        <?= Html::a('<i class="icon-pencil"></i>', Url::to(['update', 'id' => $model->id]), [
                            'class' => 'btn darkblue btn-sm',
                            'title' => 'Редактировать',
                        ]); ?>
                    </div>
                </div>
                <div class="portlet-body no_mrg_bottom">
                    <table id="datatable_ajax" class="table">
                        <tr>
                            <td class="bold-text border-space">Тип:</td>
                            <td><?= LoadingAndUnloadingAddress::$types[$model->address_type]; ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Заказчик:</td>
                            <td>
                                <?= $model->contractor ?
                                    Html::a($model->contractor->getNameWithType(), Url::to(['/contractor/view', 'id' => $model->contractor_id, 'type' => $model->contractor->type])) :
                                    null; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Город:</td>
                            <td><?= $model->city; ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Адрес:</td>
                            <td><?= $model->address; ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Грузоотправитель:</td>
                            <td><?= $model->consignor; ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Грузополучатель:</td>
                            <td><?= $model->consignee; ?></td>
                        </tr>
                        <?php foreach ($model->getLoadingAndUnloadingAddressContactPeople()->orderBy(['is_main' => SORT_DESC])->all() as $contactPerson): ?>
                            <tr>
                                <td class="bold-text border-space">Контактное лицо:</td>
                                <td>
                                    <?= $contactPerson->contact_person; ?>
                                    <span style="margin-left: 20px;">
                                        <?= $contactPerson->phoneType->name . ': ' . $contactPerson->phone; ?>
                                    </span>
                                    <?php if ($contactPerson->is_main): ?>
                                        <span style="margin-left: 20px;">
                                            Основной
                                        </span>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        <tr>
                            <td class="bold-text border-space">Статус:</td>
                            <td><?= LoadingAndUnloadingAddress::$statuses[$model->status]; ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-lg-5 pad0 pull-right">
            <div id="map" style="width: 400px;height: 300px;float: right;"></div>
        </div>
    </div>
</div>
<div class="row action-buttons" id="buttons-fixed">
    <div class="spinner-button col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width: 55%;">
    </div>
    <div class="spinner-button col-sm-1 col-xs-1">
    </div>
    <div class="spinner-button col-sm-1 col-xs-1">
        <?= ConfirmModalWidget::widget([
            'toggleButton' => [
                'label' => 'Удалить',
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
            ],
            'confirmUrl' => Url::to(['delete', 'id' => $model->id]),
            'message' => 'Вы уверены, что хотите удалить адрес?',
        ]); ?>
        <?= ConfirmModalWidget::widget([
            'toggleButton' => [
                'label' => '<i class="fa fa-trash-o fa-2x"></i>',
                'title' => 'Удалить',
                'class' => 'btn darkblue widthe-100 hidden-lg',
            ],
            'confirmUrl' => Url::to(['delete', 'id' => $model->id]),
            'message' => 'Вы уверены, что хотите удалить адрес?',
        ]); ?>
    </div>
</div>
<?php if ($model->map_width && $model->map_longitude): ?>
    <?php $this->registerJs("
    var map, markers;

    function initMap() {
        var center = {
            lat:{$model->map_width},
            lng:{$model->map_longitude}
        }
    
        var mapOptions = {
            center:center,
            scrollwheel:false,
            zoom:10
        }
    
        map = new google.maps.Map(document.getElementById('map'),mapOptions);
        
        var marker = new google.maps.Marker({
            position: center,
            map: map,
        });
    }
    initMap();
"); ?>
<?php endif; ?>
