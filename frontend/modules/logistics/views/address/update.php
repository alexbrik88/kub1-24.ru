<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 27.11.2018
 * Time: 19:08
 */

/* @var $this yii\web\View */
/* @var $model \common\models\loadingAndUnloadingAddress\LoadingAndUnloadingAddress */
/* @var $company \common\models\Company */
/* @var $newAddressContactPerson \common\models\loadingAndUnloadingAddress\LoadingAndUnloadingAddressContactPerson */

$this->title = $model->city . ' ' . $model->address;
?>
<div class="driver-update">
    <?= $this->render('_form', [
        'model' => $model,
        'company' => $company,
        'newAddressContactPerson' => $newAddressContactPerson,
    ]); ?>
</div>