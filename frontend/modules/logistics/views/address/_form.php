<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 27.11.2018
 * Time: 14:26
 */

use yii\bootstrap\ActiveForm;
use common\components\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\models\loadingAndUnloadingAddress\LoadingAndUnloadingAddress */
/* @var $company \common\models\Company */
/* @var $newAddressContactPerson \common\models\loadingAndUnloadingAddress\LoadingAndUnloadingAddressContactPerson */
?>
<?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'id' => 'vehicle-form',
    'options' => [
        'enctype' => 'multipart/form-data',
        'class' => 'add-avtoschet',
    ],
    'enableClientValidation' => false,
])); ?>
<div class="form-body form-body_sml form-body-vehicle">
    <?= $form->errorSummary($model); ?>
    <div class="portlet">
        <?= $this->render('form/header', [
            'model' => $model,
        ]); ?>
        <div class="portlet-body">
            <?= $this->render('form/body', [
                'model' => $model,
                'form' => $form,
                'company' => $company,
                'newAddressContactPerson' => $newAddressContactPerson,
            ]); ?>
        </div>
    </div>
    <div class="form-actions">
        <div class="row action-buttons">
            <div class="spinner-button col-sm-1 col-xs-1">
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs create-invoice mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                ]); ?>
                <?= Html::submitButton('<span class="ico-Save-smart-pls fs"></span>', [
                    'class' => 'btn darkblue widthe-100 hidden-lg create-invoice',
                    'title' => 'Сохранить',
                ]); ?>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="spinner-button col-sm-1 col-xs-1">
                <?= Html::a('Отменить', Yii::$app->request->referrer, [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                ]); ?>
                <?= Html::a('<span class="ico-Cancel-smart-pls fs"></span>', Yii::$app->request->referrer, [
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                    'title' => 'Отменить',
                ]); ?>
            </div>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
