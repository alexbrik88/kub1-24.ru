<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 20.11.2018
 * Time: 11:30
 */

use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;

$this->beginContent('@frontend/views/layouts/main.php');
?>
    <div class="debt-report-content container-fluid" style="padding: 0; margin-top: -10px;">
        <?php NavBar::begin([
            'options' => [
                'class' => 'navbar-report navbar-default',
            ],
            'brandOptions' => [
                'style' => 'margin-left: 0;'
            ],
            'containerOptions' => [
                'style' => 'padding: 0;'
            ],
            'innerContainerOptions' => [
                'class' => 'container-fluid',
                'style' => 'padding: 0;'
            ],
        ]);
        echo Nav::widget([
            'id' => 'debt-report-menu',
            'items' => [
                ['label' => 'Водители', 'url' => ['/logistics/driver/index']],
                ['label' => 'Транспортные средства', 'url' => ['/logistics/vehicle/index']],
                ['label' => 'Адреса Погрузки/Разгрузки', 'url' => ['/logistics/address/index']],
            ],
            'options' => ['class' => 'navbar-nav'],
        ]);
        NavBar::end(); ?>
        <?= $content; ?>
    </div>
<?php $this->endContent(); ?>