<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 22.11.2018
 * Time: 12:11
 */

use common\components\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\vehicle\Vehicle;
use common\components\grid\GridView;
use common\components\grid\DataColumn;
use common\components\grid\DropDownSearchDataColumn;
use frontend\modules\documents\widgets\DocumentFileWidget;
use common\models\vehicle\VehicleType;

/* @var $this yii\web\View
 * @var $searchModel \frontend\modules\logistics\models\VehicleSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

$this->title = 'Транспортные средства';
?>
<div class="portlet box">
    <div class="btn-group pull-right title-buttons">
        <?= Html::a('<i class="fa fa-plus"></i> ДОБАВИТЬ', Url::to(['create']), [
            'class' => 'btn yellow',
        ]); ?>
    </div>
    <h3 class="page-title"><?= $this->title; ?></h3>
</div>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Список транспортных средств: <?= $dataProvider->totalCount; ?>
        </div>
        <div class="tools search-tools tools_button col-sm-4">
            <div class="form-body">
                <?php $form = ActiveForm::begin([
                    'method' => 'GET',
                    'action' => Url::current([$searchModel->formName() => null]),
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'options' => [
                            'class' => '',
                        ],
                    ],
                ]); ?>
                <div class="search_cont">
                    <div class="wimax_input" style="width: 100%!important;padding-right: 20px;float: right;">
                        <?= $form->field($searchModel, 'search')->textInput([
                            'placeholder' => 'Поиск...',
                        ]); ?>
                    </div>
                    <div class="wimax_button">
                        <?= Html::submitButton('НАЙТИ', [
                            'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                        ]) ?>
                    </div>
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
        <div class="actions joint-operations col-sm-4" style="display:none;">
            <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                'class' => 'btn btn-default btn-sm',
                'data-toggle' => 'modal',
            ]); ?>
            <div id="many-delete" class="confirm-modal fade modal" role="dialog"
                 tabindex="-1" aria-hidden="true"
                 style="display: none; margin-top: -51.5px;">
                <div class="modal-dialog ">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="form-body">
                                <div class="row">Вы уверены, что хотите удалить выбранные транспортные средства?
                                </div>
                            </div>
                            <div class="form-actions row">
                                <div class="col-xs-6">
                                    <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
                                        'class' => 'btn darkblue pull-right modal-many-delete ladda-button',
                                        'data-url' => Url::to(['many-delete',]),
                                        'data-style' => 'expand-right',
                                    ]); ?>
                                </div>
                                <div class="col-xs-6">
                                    <button type="button" class="btn darkblue" data-dismiss="modal">
                                        НЕТ
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-container clearfix" style="">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'dataColumnClass' => DataColumn::className(),
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'columns' => [
                    [
                        'header' => Html::checkbox('', false, [
                            'class' => 'joint-operation-main-checkbox',
                        ]),
                        'headerOptions' => [
                            'class' => 'text-center pad0',
                            'width' => '5%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-center pad0-l pad0-r',
                        ],
                        'format' => 'raw',
                        'value' => function (Vehicle $model) {
                            return Html::checkbox('Vehicle[' . $model->id . '][checked]', false, [
                                'class' => 'joint-operation-checkbox',
                            ]);
                        },
                    ],
                    [
                        'attribute' => 'number',
                        'label' => '№№',
                        'headerOptions' => [
                            'width' => '6%',
                        ],
                        'contentOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Vehicle $model) {
                            return Html::a($model->number, ['view', 'id' => $model->id]);
                        },
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'vehicle_type_id',
                        'label' => 'Тип ТС',
                        'headerOptions' => [
                            'width' => '11%',
                        ],
                        'contentOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getVehicleTypeFilter(),
                        'value' => function (Vehicle $model) {
                            return $model->getVehicleTypeText(true);
                        },
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'body_type_id',
                        'label' => 'Тип кузова',
                        'headerOptions' => [
                            'width' => '11%',
                        ],
                        'contentOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getBodyTypeFilter(),
                        'value' => function (Vehicle $model) {
                            return $model->bodyType ? $model->bodyType->name : '';
                        },
                    ],
                    [
                        'attribute' => 'model',
                        'label' => 'Марка ТС',
                        'headerOptions' => [
                            'width' => '11%',
                        ],
                        'contentOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Vehicle $model) {
                            return $model->model ? $model->model : '';
                        },
                    ],
                    [
                        'attribute' => 'state_number',
                        'label' => 'Гос. номер',
                        'headerOptions' => [
                            'width' => '11%',
                        ],
                        'contentOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Vehicle $model) {
                            return $model->getStateNumberFull(true);
                        },
                    ],
                    [
                        'attribute' => 'totalTonnage',
                        'label' => 'Тонн',
                        'headerOptions' => [
                            'width' => '7%',
                        ],
                        'contentOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Vehicle $model) {
                            $tonnage = $model->tonnage;
                            if ($model->vehicle_type_id == VehicleType::TYPE_TRACTOR && $model->semitrailerType) {
                                $tonnage += $model->semitrailerType->tonnage;
                            } elseif ($model->vehicle_type_id == VehicleType::TYPE_WAGON && $model->trailerType) {
                                $tonnage += $model->trailerType->tonnage;
                            }
                            return $tonnage;
                        },
                    ],
                    [
                        'attribute' => 'totalVolume',
                        'label' => 'Объем',
                        'headerOptions' => [
                            'width' => '7%',
                        ],
                        'contentOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Vehicle $model) {
                            $volume = $model->volume;
                            if ($model->vehicle_type_id == VehicleType::TYPE_TRACTOR && $model->semitrailerType) {
                                $volume += $model->semitrailerType->volume;
                            } elseif ($model->vehicle_type_id == VehicleType::TYPE_WAGON && $model->trailerType) {
                                $volume += $model->trailerType->volume;
                            }
                            return $volume;
                        },
                    ],
                    [
                        'attribute' => 'files',
                        'label' => 'Документы',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'format' => 'raw',
                        'value' => function (Vehicle $model) {
                            return Yii::$app->view->render('@documents/views/layouts/_doc-file-link', [
                                'model' => $model,
                            ]);
                        },
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'owner',
                        'label' => 'Кому принадлежит',
                        'headerOptions' => [
                            'width' => '20%',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getOwnerFilter(),
                        'value' => function (Vehicle $model) {
                            return $model->is_own ? 'Собственное ТС' :
                                ($model->contractor ?
                                    Html::a($model->contractor->getNameWithType(), Url::to(['/contractor/view', 'id' => $model->contractor_id, 'type' => $model->contractor->type])) :
                                    '');
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>