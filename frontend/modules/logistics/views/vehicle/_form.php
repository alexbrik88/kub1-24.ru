<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 22.11.2018
 * Time: 13:46
 */

use yii\bootstrap\ActiveForm;
use common\components\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\models\vehicle\Vehicle */
/* @var $company \common\models\Company */
?>
<?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'id' => 'vehicle-form',
    'options' => [
        'enctype' => 'multipart/form-data',
        'class' => 'add-avtoschet',
    ],
    'enableClientValidation' => false,
])); ?>
    <div class="form-body form-body_sml form-body-vehicle">
        <?= $form->errorSummary($model); ?>
        <div class="portlet">
            <?= $this->render('form/header', [
                'model' => $model,
            ]); ?>
            <div class="portlet-body">
                <?= $this->render('form/body', [
                    'model' => $model,
                    'form' => $form,
                    'company' => $company,
                ]); ?>
            </div>
        </div>
        <div class="form-actions">
            <div class="row action-buttons">
                <div class="spinner-button col-sm-1 col-xs-1">
                    <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs create-invoice mt-ladda-btn ladda-button',
                        'data-style' => 'expand-right',
                    ]); ?>
                    <?= Html::submitButton('<span class="ico-Save-smart-pls fs"></span>', [
                        'class' => 'btn darkblue widthe-100 hidden-lg create-invoice',
                        'title' => 'Сохранить',
                    ]); ?>
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="spinner-button col-sm-1 col-xs-1">
                    <?= Html::a('Отменить', Yii::$app->request->referrer, [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                        'data-style' => 'expand-right',
                    ]); ?>
                    <?= Html::a('<span class="ico-Cancel-smart-pls fs"></span>', Yii::$app->request->referrer, [
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                        'title' => 'Отменить',
                    ]); ?>
                </div>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>
<?php $this->registerJs('
    $(document).ready(function (e) {
        if ($("#vehicle-is_state_number_russian").is(":checked")) {
            $("#vehicle-state_number").inputmask({
                "mask": "q{1} 9{3} q{2} (9{2,3})",
                "definitions": {
                    "q": {
                        validator: "[А-яа-я]",
                    }
                }
            });
        }
    });
       
    $(document).on("change", "#vehicle-is_own", function (e) {
        var $isOwnBlock = $(".is_own_block");
        var $isNotOwnBlock = $(".is_not_own_block");
        
        if ($(this).is(":checked")) {
            $isOwnBlock.show();
            $isNotOwnBlock.hide();       
        } else {
            $isOwnBlock.hide();
            $isNotOwnBlock.show();   
        }
    });
    
    $(document).on("change", "#vehicle-vehicle_type_id", function (e) {
        var $type = $(this).val();
        
        $("#vehicle-form").find(".portlet-body > .row > div.col-md-12").each(function () {
            if ($(this).data("types") !== undefined) {
                if ($.inArray(+$type, $(this).data("types")) == -1) {
                    $(this).hide();
                } else {
                    $(this).show();
                }
            }
        });
        $weightName = null;
        switch (+$(this).val()) {
            case 2:
                $weightName = "полуприцепа";
                break;
            case 3:
                $weightName = "фургона";
                break;
            case 4:
                $weightName = "прицепа";
                break;        
        }
        $(".weight-name").text($weightName);
    });
    
    $(document).on("change", ".volume-part", function (e) {
        var $calculate = true;
        var $volume = 1;
        
        $(".volume-part").each(function (e) {
            $volume = $volume * $(this).val().replace(",", ".");
            if ($(this).val() == "") {
                $calculate = false;
            }
        });
        if ($calculate) {
            $("#vehicle-volume").val($volume);
        }
    });
    
    $(document).on("change", "#vehicle-is_state_number_russian", function (e) {
        if ($(this).is(":checked")) {
            $("#vehicle-state_number").inputmask({
                "mask": "q{1} 9{3} q{2} (9{2,3})",
                "definitions": {
                    "q": {
                        validator: "[А-яа-я]",
                    }
                }
            });
        } else {
            $("#vehicle-state_number").inputmask("remove").val("");
        }
    });
'); ?>