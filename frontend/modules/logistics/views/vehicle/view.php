<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 24.11.2018
 * Time: 15:37
 */

use common\components\helpers\Html;
use yii\helpers\Url;
use common\components\date\DateHelper;
use common\models\vehicle\VehicleType;
use common\models\file\widgets\FileUpload;
use frontend\widgets\ConfirmModalWidget;
use frontend\modules\documents\widgets\CreatedByWidget;

/* @var $this yii\web\View */
/* @var $model \common\models\vehicle\Vehicle */

$this->context->layoutWrapperCssClass = 'out-document';
$this->title = $model->getVehicleTitle(false);
?>
    <div class="page-content-in" style="padding-bottom: 30px;">
        <div style="col-xs-12 pad3">
            <?= Html::a('Назад к списку', Url::to(['index']), [
                'class' => 'back-to-customers',
            ]); ?>
        </div>
        <div class="col-xs-12 pad0">
            <div class="col-xs-12 col-lg-7 pad0">
                <div class="portlet customer-info">
                    <div class="portlet-title">
                        <div class="actions">
                            <?= CreatedByWidget::widget([
                                'createdAt' => date("d.m.Y", $model->created_at),
                                'author' => $model->author ? $model->author->currentEmployeeCompany->getFio() : '',
                            ]); ?>
                            <?= Html::a('<i class="icon-pencil"></i>', Url::to(['update', 'id' => $model->id]), [
                                'class' => 'btn darkblue btn-sm',
                                'title' => 'Редактировать',
                            ]); ?>
                        </div>
                        <div class="caption" style="width: 88%;">
                            <?= $model->getVehicleTitle(true, true); ?>
                        </div>
                    </div>
                    <div class="portlet-body no_mrg_bottom">
                        <table class="table no_mrg_bottom">
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-3 upd-info-label">
                                            №№ ТС:
                                        </div>
                                        <div class="col-sm-3 upd-info-value">
                                            <?= $model->number; ?>
                                        </div>
                                        <div class="col-sm-3 upd-info-label">
                                            Дата добавления:
                                        </div>
                                        <div class="col-sm-3 upd-info-value">
                                            <?= DateHelper::format($model->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-3 upd-info-label">
                                            Собственное ТС:
                                        </div>
                                        <div class="col-sm-3 upd-info-value">
                                            <?= $model->is_own ? 'Да' : 'Нет'; ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-3 upd-info-label">
                                            Тип ТС:
                                        </div>
                                        <div class="col-sm-3 upd-info-value">
                                            <?= $model->getVehicleTypeText(); ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr style="display: <?= $model->is_own ? 'table-row' : 'none'; ?>;">
                                <td>
                                    <div class="row">
                                        <div class="col-sm-3 upd-info-label">
                                            Вид топлива:
                                        </div>
                                        <div class="col-sm-3 upd-info-value">
                                            <?= $model->fuelType ? $model->fuelType->name : null; ?>
                                        </div>
                                        <div class="col-sm-3 upd-info-label">
                                            Норма расхода (л/100км):
                                        </div>
                                        <div class="col-sm-3 upd-info-value">
                                            <?= $model->fuel_consumption; ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr style="display: <?= $model->is_own ? 'table-row' : 'none'; ?>;">
                                <td>
                                    <div class="row">
                                        <div class="col-sm-3 upd-info-label">
                                            Дата начального пробега:
                                        </div>
                                        <div class="col-sm-3 upd-info-value">
                                            <?= DateHelper::format($model->initial_mileage_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr style="display: <?= $model->is_own ? 'table-row' : 'none'; ?>;">
                                <td>
                                    <div class="row">
                                        <div class="col-sm-3 upd-info-label">
                                            Начальный пробег:
                                        </div>
                                        <div class="col-sm-3 upd-info-value">
                                            <?= $model->initial_mileage; ?>
                                        </div>
                                        <div class="col-sm-3 upd-info-label">
                                            Текущий пробег:
                                        </div>
                                        <div class="col-sm-3 upd-info-value">
                                            <?= $model->current_mileage; ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr style="display: <?= $model->is_own ? 'none' : 'table-row'; ?>;">
                                <td>
                                    <div class="row">
                                        <div class="col-sm-3 upd-info-label">
                                            Перевозчик:
                                        </div>
                                        <div class="col-sm-9 upd-info-value">
                                            <?= $model->contractor ?
                                                Html::a($model->contractor->getNameWithType(), Url::to(['/contractor/view', 'id' => $model->contractor_id, 'type' => $model->contractor->type])) :
                                                null; ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr style="display: <?= in_array($model->vehicle_type_id, [VehicleType::TYPE_WAGON, VehicleType::TYPE_TRACTOR]) ? 'table-row' : 'none'; ?>;">
                                <td>
                                    <div class="row">
                                        <div class="col-sm-3 upd-info-label">
                                            Водитель:
                                        </div>
                                        <div class="col-sm-9 upd-info-value">
                                            <?= $model->driver ?
                                                Html::a($model->driver->getFio(), Url::to(['/logistics/driver/view', 'id' => $model->driver_id])) : null; ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-3 upd-info-label">
                                            Марка:
                                        </div>
                                        <div class="col-sm-9 upd-info-value">
                                            <?= $model->model; ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-3 upd-info-label">
                                            Номер ТС:
                                        </div>
                                        <div class="col-sm-3 upd-info-value">
                                            <?= $model->state_number; ?>
                                        </div>
                                        <div class="col-sm-3 upd-info-label">
                                            госномер РФ:
                                        </div>
                                        <div class="col-sm-3 upd-info-value">
                                            <?= $model->is_state_number_russian ? 'Да' : 'Нет'; ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr style="display: <?= $model->body_type_id == VehicleType::TYPE_TRACTOR ? 'none' : 'table-row' ?>;">
                                <td>
                                    <div class="row">
                                        <div class="col-sm-3 upd-info-label">
                                            Тип кузова:
                                        </div>
                                        <div class="col-sm-9 upd-info-value">
                                            <?= $model->bodyType ? $model->bodyType->name : ''; ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-3 upd-info-label">
                                            Цвет ТС:
                                        </div>
                                        <div class="col-sm-9 upd-info-value">
                                            <?= $model->color; ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr style="display: <?= $model->vehicle_type_id == VehicleType::TYPE_TRACTOR ? 'none' : 'table-row'; ?>;">
                                <td>
                                    <div class="row">
                                        <div class="col-sm-3 upd-info-label">
                                            Тоннаж (тонн):
                                        </div>
                                        <div class="col-sm-3 upd-info-value">
                                            <?= $model->tonnage; ?>
                                        </div>
                                        <div class="col-sm-3 upd-info-label">
                                            Вес ПП (тонн):
                                        </div>
                                        <div class="col-sm-3 upd-info-value">
                                            <?= $model->weight; ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr style="display: <?= $model->vehicle_type_id == VehicleType::TYPE_TRACTOR ? 'none' : 'table-row'; ?>;">
                                <td>
                                    <div class="row">
                                        <div class="col-sm-3 upd-info-label">
                                            Внутренние габариты (м)
                                        </div>
                                        <div class="col-sm-2 upd-info-value" style="width: 12%;">
                                            <b>Д:</b>
                                            <?= $model->length; ?>
                                        </div>
                                        <div class="col-sm-2 upd-info-value" style="width: 12%;">
                                            <b>Ш:</b>
                                            <?= $model->width; ?>
                                        </div>
                                        <div class="col-sm-2 upd-info-value" style="width: 12%;">
                                            <b>В:</b>
                                            <?= $model->height; ?>
                                        </div>
                                        <div class="col-sm-3 upd-info-value">
                                            <b>Объём (м3):</b>
                                            <?= $model->volume; ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr style="display:<?= $model->vehicle_type_id == VehicleType::TYPE_TRACTOR ? 'table-row' : 'none' ?>;">
                                <td>
                                    <div class="row">
                                        <div class="col-sm-3 upd-info-label">
                                            Полуприцеп:
                                        </div>
                                        <div class="col-sm-9 upd-info-value">
                                            <?= $model->semitrailerType ?
                                                Html::a(($model->semitrailerType->model ? ($model->semitrailerType->model . ' ') : null) . ' ' .
                                                    $model->semitrailerType->getStateNumberFull(),
                                                    Url::to(['view', 'id' => $model->semitrailer_type_id])) :
                                                null; ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr style="display:<?= $model->vehicle_type_id == VehicleType::TYPE_WAGON ? 'table-row' : 'none' ?>;">
                                <td>
                                    <div class="row">
                                        <div class="col-sm-3 upd-info-label">
                                            Прицеп:
                                        </div>
                                        <div class="col-sm-9 upd-info-value">
                                            <?= $model->trailerType ?
                                                Html::a(($model->trailerType->model ? ($model->trailerType->model . ' ') : null) . ' ' .
                                                    $model->trailerType->getStateNumberFull(),
                                                    Url::to(['view', 'id' => $model->trailer_type_id])) :
                                                null; ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-lg-5 pad0 pull-right">
                <div class="col-xs-12" style="padding-right:0 !important;">
                    <div class="control-panel col-xs-12 col-lg-9 pad3 pull-right">
                        <?= $model->getVehicleImg(); ?>
                    </div>
                    <div class="col-xs-12 col-lg-9 pull-right pad3" style="max-width: 720px;">
                        <div class="portlet">
                            <div class="customer-info" style="min-height:auto !important">
                                <div class="portlet-body no_mrg_bottom main_inf_no-bord">
                                    <table class="table no_mrg_bottom">
                                        <tr>
                                            <td>
                                                <span class="bold-text">Документы</span>
                                                <div style="margin-bottom: 5px;">
                                                    <?= FileUpload::widget([
                                                        'uploadUrl' => Url::to(['file-upload', 'id' => $model->id,]),
                                                        'deleteUrl' => Url::to(['file-delete', 'id' => $model->id,]),
                                                        'listUrl' => Url::to(['file-list', 'id' => $model->id,]),
                                                    ]); ?>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="">
                                <div style="margin: 15px 0 0;">
                                    <span style="font-weight: bold;">Комментарий</span>
                                    <?= Html::tag('span', '', [
                                        'id' => 'comment_update',
                                        'class' => 'glyphicon glyphicon-pencil',
                                        'style' => 'cursor: pointer;',
                                    ]); ?>
                                </div>
                                <div id="comment_view" class="">
                                    <?= Html::encode($model->comment); ?>
                                </div>
                                <?= Html::beginTag('div', [
                                    'id' => 'comment_form',
                                    'class' => 'hidden',
                                    'style' => 'position: relative;',
                                    'data-url' => Url::to(['comment', 'id' => $model->id]),
                                ]) ?>
                                <?= Html::tag('i', '', [
                                    'id' => 'comment_save',
                                    'class' => 'fa fa-floppy-o',
                                    'style' => 'position: absolute; top: -22px; right: 0px; cursor: pointer; font-size: 20px;',
                                ]); ?>
                                <?= Html::textarea('comment', $model->comment, [
                                    'id' => 'comment_input',
                                    'rows' => 3,
                                    'maxlength' => true,
                                    'style' => 'width: 100%; padding-right: 35px; border: 1px solid #ddd;',
                                ]); ?>
                                <?= Html::endTag('div') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="buttons-bar-fixed">
            <div class="row action-buttons margin-no-icon">
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    <?= Html::button('Отправить', [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs send-message-panel-trigger',
                    ]); ?>
                    <?= Html::button('<span class="ico-Send-smart-pls fs"></span>', [
                        'class' => 'btn darkblue widthe-100 hidden-lg send-message-panel-trigger',
                        'title' => 'Отправить',
                    ]); ?>
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    <?= ConfirmModalWidget::widget([
                        'toggleButton' => [
                            'label' => 'Копировать',
                            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                        ],
                        'confirmUrl' => Url::to(['copy', 'id' => $model->id]),
                        'message' => 'Вы уверены, что хотите скопировать это траспортное средство?',
                    ]);
                    ?>
                    <?= ConfirmModalWidget::widget([
                        'toggleButton' => [
                            'label' => '<i class="fa fa-files-o fa-2x"></i>',
                            'title' => 'Копировать',
                            'class' => 'btn darkblue widthe-100 hidden-lg',
                        ],
                        'confirmUrl' => Url::to(['copy', 'id' => $model->id]),
                        'message' => 'Вы уверены, что хотите скопировать это траспортное средство?',
                    ]);
                    ?>
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    <?= Html::button('Удалить', [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                        'data-toggle' => 'modal',
                        'href' => '#delete-confirm',
                    ]); ?>
                    <?= Html::button('<i class="fa fa-trash-o fa-2x"></i>', [
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                        'data-toggle' => 'modal',
                        'href' => '#delete-confirm',
                    ]); ?>
                    <?= ConfirmModalWidget::widget([
                        'options' => [
                            'id' => 'delete-confirm',
                        ],
                        'toggleButton' => false,
                        'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id]),
                        'confirmParams' => [],
                        'message' => "Вы уверены, что хотите удалить транспортное средство?",
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
<?= $this->render('@frontend/modules/documents/views/invoice/view/_send_message', [
    'model' => $model,
    'useContractor' => null,
]); ?>
<?php $this->registerJs('
    $(document).on("click", "#comment_update", function () {
        $("#comment_view").toggleClass("hidden");
        $("#comment_form").toggleClass("hidden");
    });
    $(document).on("click", "#comment_save", function () {
        $.post($("#comment_form").data("url"), $("#comment_input").serialize(), function (data) {
            $("#comment_view").text(data.value);
            $("#comment_form").addClass("hidden");
            $("#comment_view").removeClass("hidden");
        })
    });
');