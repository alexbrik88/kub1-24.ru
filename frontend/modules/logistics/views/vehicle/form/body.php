<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 22.11.2018
 * Time: 14:07
 */

use common\components\helpers\Html;
use common\components\helpers\ArrayHelper;
use common\models\vehicle\VehicleType;
use common\models\vehicle\FuelType;
use common\components\date\DateHelper;
use kartik\select2\Select2;
use common\models\Contractor;
use common\models\driver\Driver;
use common\models\vehicle\Vehicle;
use common\models\vehicle\BodyType;

/* @var $this yii\web\View */
/* @var $model \common\models\vehicle\Vehicle */
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $company \common\models\Company */

$config = [
    'options' => [
        'class' => 'col-md-12 pad0',
        'style' => 'margin-bottom: 15px;',
    ],
    'labelOptions' => [
        'class' => 'col-md-2 control-label',
    ],
    'inputOptions' => [
        'style' => 'width: 100%;',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-5 inp_one_line',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
?>
<div class="row">
    <div class="col-md-12 pad0" style="margin-bottom: 15px;">
        <div class="col-md-2">
            <label class="control-label" for="vehicle-is_own" style="padding-top: 0!important;">
                Собственное ТС:
            </label>
        </div>
        <div class="col-md-10">
            <?= Html::activeCheckbox($model, 'is_own', [
                'class' => 'form-control',
                'label' => false,
            ]); ?>
        </div>
    </div>
    <?= $form->field($model, 'vehicle_type_id', $config)
        ->dropDownList(ArrayHelper::map(VehicleType::find()->all(), 'id', 'name'))
        ->label('Тип ТС:'); ?>
    <div class="col-md-12 pad0 is_own_block"
         style="margin-bottom: 15px;display: <?= $model->is_own ? 'block' : 'none'; ?>;">
        <div class="col-md-2">
            <label class="control-label" for="vehicle-fuel_type_id">
                Вид топлива:
            </label>
        </div>
        <div class="col-md-2">
            <?= Html::activeDropDownList($model, 'fuel_type_id',
                ArrayHelper::map(FuelType::find()->all(), 'id', 'name'), [
                    'class' => 'form-control',
                ]); ?>
        </div>
        <div class="col-md-3 p-r-0" style="width: 19%;">
            <label class="control-label" for="vehicle-fuel_consumption">
                Норма расхода (л/100км):
            </label>
        </div>
        <div class="col-md-2">
            <?= Html::activeTextInput($model, 'fuel_consumption', [
                'class' => 'form-control',
            ]); ?>
        </div>
    </div>
    <div class="col-xs-12 pad0 is_own_block"
         style="margin-bottom: 15px;display: <?= $model->is_own ? 'block' : 'none'; ?>;">
        <div class="col-md-2">
            <label for="vehicle-initial_mileage_date" class="control-label" style="padding-top: 0!important;">
                Дата начального пробега:
            </label>
        </div>
        <div class="col-md-2">
            <div class="input-icon">
                <i class="fa fa-calendar"></i>
                <?= Html::activeTextInput($model, 'initial_mileage_date', [
                    'class' => 'form-control date-picker',
                    'data-date-viewmode' => 'years',
                    'value' => DateHelper::format($model->initial_mileage_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                ]); ?>
            </div>
        </div>
    </div>
    <div class="col-md-12 pad0 is_own_block"
         style="margin-bottom: 15px;display: <?= $model->is_own ? 'block' : 'none'; ?>;">
        <div class="col-md-2">
            <label class="control-label" for="vehicle-initial_mileage">
                Начальный пробег:
            </label>
        </div>
        <div class="col-md-2">
            <?= Html::activeTextInput($model, 'initial_mileage', [
                'class' => 'form-control',
            ]); ?>
        </div>
        <div class="col-md-2">
            <label class="control-label" for="vehicle-current_mileage">
                Текущий пробег:
            </label>
        </div>
    </div>
    <?= $form->field($model, 'contractor_id', array_merge($config, [
        'options' => [
            'class' => 'col-md-12 pad0 is_not_own_block',
            'style' => 'margin-bottom: 15px;display:' . ($model->is_own ? 'none;' : 'block;'),
        ],
    ]))->widget(Select2::classname(), [
        'data' => ['add-modal-contractor' => '[ + Добавить перевозчика ]'] + $company->sortedContractorList(Contractor::TYPE_SELLER),
        'options' => [
            'placeholder' => '',
        ],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ])->label('Перевозчик:'); ?>

    <?= $form->field($model, 'driver_id', array_merge($config, [
        'options' => [
            'data-types' => json_encode([VehicleType::TYPE_WAGON, VehicleType::TYPE_TRACTOR,]),
            'class' => 'col-md-12 pad0',
            'style' => 'margin-bottom: 15px;display:' . (in_array($model->vehicle_type_id, [VehicleType::TYPE_WAGON, VehicleType::TYPE_TRACTOR]) ? 'block;' : 'none;'),
        ],
    ]))
        ->dropDownList([null => ''] + ArrayHelper::map(Driver::find()
                ->andWhere(['company_id' => $company->id])
                ->all(), 'id', 'fio'))
        ->label('Водитель:'); ?>

    <?= $form->field($model, 'model', array_merge($config, [
        'wrapperOptions' => [
            'class' => 'col-md-3 inp_one_line',
        ],
    ]))->label('Марка:'); ?>

    <div class="col-md-12 field-vehicle-initial_mileage required pad0" style="margin-bottom: 15px;">
        <div class="col-md-2">
            <label class="control-label" for="vehicle-initial_mileage">
                Номер ТС:
            </label>
        </div>
        <div class="col-md-3">
            <?= Html::activeTextInput($model, 'state_number', [
                'class' => 'form-control',
                'style' => 'width: 100%',
            ]); ?>
            <p class="help-block help-block-error"></p>
        </div>
        <div class="col-md-2" style="padding-top: 6px;">
            <?= Html::activeCheckbox($model, 'is_state_number_russian') ?>
        </div>
    </div>

    <?= $form->field($model, 'body_type_id', array_merge($config, [
        'options' => [
            'data-types' => json_encode([VehicleType::TYPE_WAGON, VehicleType::TYPE_SEMITRAILER, VehicleType::TYPE_TRAILER]),
            'class' => 'col-md-12 pad0',
            'style' => 'margin-bottom: 15px;display:' . ($model->body_type_id == VehicleType::TYPE_TRACTOR ? 'none;' : 'block;'),
        ],
    ]))->widget(Select2::classname(), [
        'data' => ArrayHelper::map(BodyType::find()->orderBy([
                'sort' => SORT_ASC,
                'name' => SORT_ASC,
        ])->all(), 'id', 'name'),
        'options' => [
            'id' => 'body_type_id',
            'placeholder' => '',
        ],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ])->label('Тип кузова:'); ?>

    <?= $form->field($model, 'color', array_merge($config, [
        'wrapperOptions' => [
            'class' => 'col-md-3 inp_one_line',
        ],
    ]))->label('Цвет ТС:'); ?>

    <div class="col-md-12 pad0"
         data-types="<?= json_encode([VehicleType::TYPE_WAGON, VehicleType::TYPE_SEMITRAILER, VehicleType::TYPE_TRAILER]); ?>"
         style="margin-bottom: 15px;display: <?= $model->vehicle_type_id == VehicleType::TYPE_TRACTOR ? 'none' : 'block'; ?>;">
        <div class="col-md-2">
            <label class="control-label" for="vehicle-tonnage">
                Тоннаж (тонн):
            </label>
        </div>
        <div class="col-md-1">
            <?= Html::activeTextInput($model, 'tonnage', [
                'class' => 'form-control',
                'style' => 'width: 100%;',
            ]); ?>
        </div>
        <div class="col-md-2 p-r-0" style="width: 18%;">
            <label class="control-label" for="vehicle-weight">
                Вес <span class="weight-name"></span> (тонн):
            </label>
        </div>
        <div class="col-md-1">
            <?= Html::activeTextInput($model, 'weight', [
                'class' => 'form-control',
                'style' => 'width: 100%;',
            ]); ?>
        </div>
    </div>

    <div class="col-md-12 pad0"
         data-types="<?= json_encode([VehicleType::TYPE_WAGON, VehicleType::TYPE_SEMITRAILER, VehicleType::TYPE_TRAILER]); ?>"
         style="margin-bottom: 15px;display: <?= $model->vehicle_type_id == VehicleType::TYPE_TRACTOR ? 'none' : 'block'; ?>;">
        <div class="col-md-2">
            <label class="control-label" for="vehicle-length">
                Внутренние габариты (м)
            </label>
        </div>
        <div class="col-md-1" style="width: 10%;">
            <b>Д:</b>
            <?= Html::activeTextInput($model, 'length', [
                'type' => 'number',
                'class' => 'form-control volume-part',
                'style' => 'width: 78%;display: inline-block;',
                'min' => 0,
                'max' => 10000000,
                'step' => 'any',
            ]); ?>
        </div>
        <div class="col-md-1" style="width: 10%;">
            <b>Ш:</b>
            <?= Html::activeTextInput($model, 'width', [
                'type' => 'number',
                'class' => 'form-control volume-part',
                'style' => 'width: 73%;display: inline-block;',
                'min' => 0,
                'max' => 10000000,
                'step' => 'any',
            ]); ?>
        </div>
        <div class="col-md-1" style="width: 10%;">
            <b>В:</b>
            <?= Html::activeTextInput($model, 'height', [
                'type' => 'number',
                'class' => 'form-control volume-part',
                'style' => 'width: 78%;display: inline-block;',
                'min' => 0,
                'max' => 10000000,
                'step' => 'any',
            ]); ?>
        </div>
        <div class="col-md-2">
            <b>Объём (м3):</b>
            <?= Html::activeTextInput($model, 'volume', [
                'class' => 'form-control',
                'style' => 'width: 45%;display: inline-block;',
                'readOnly' => true,
            ]); ?>
        </div>
    </div>

    <?= $form->field($model, 'semitrailer_type_id', array_merge($config, [
        'options' => [
            'data-types' => json_encode([VehicleType::TYPE_TRACTOR]),
            'class' => 'col-md-12 pad0',
            'style' => 'margin-bottom: 15px;display:' . ($model->vehicle_type_id == VehicleType::TYPE_TRACTOR ? 'block;' : 'none;'),
        ],
        'wrapperOptions' => [
            'class' => 'col-md-3 inp_one_line',
        ],
    ]))->dropDownList([null => ''] + ArrayHelper::map(Vehicle::find()
            ->andWhere(['company_id' => $company->id])
            ->andWhere(['vehicle_type_id' => VehicleType::TYPE_SEMITRAILER])
            ->all(), 'id', function (Vehicle $model) {
            $name = $model->model ? ($model->model . ' ') : null;
            $name .= $model->state_number;

            return $name;
        }))->label('Полуприцеп:'); ?>

    <?= $form->field($model, 'trailer_type_id', array_merge($config, [
        'options' => [
            'data-types' => json_encode([VehicleType::TYPE_WAGON]),
            'class' => 'col-md-12 pad0',
            'style' => 'margin-bottom: 15px;display:' . ($model->vehicle_type_id == VehicleType::TYPE_WAGON ? 'block;' : 'none;'),
        ],
        'wrapperOptions' => [
            'class' => 'col-md-3 inp_one_line',
        ],
    ]))->dropDownList([null => ''] + ArrayHelper::map(Vehicle::find()
            ->andWhere(['company_id' => $company->id])
            ->andWhere(['vehicle_type_id' => VehicleType::TYPE_TRAILER])
            ->all(), 'id', function (Vehicle $model) {
            $name = $model->model ? ($model->model . ' ') : null;
            $name .= $model->state_number;

            return $name;
        }))->label('Прицеп:'); ?>
</div>
<div class="modal fade t-p-f modal_scroll_center mobile-modal" id="add-new" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body" id="block-modal-new-product-form">

            </div>
        </div>
    </div>
</div>