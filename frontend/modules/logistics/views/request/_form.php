<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.12.2018
 * Time: 23:14
 */

use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use common\models\Contractor;
use common\widgets\Modal;
use yii\widgets\Pjax;
use common\components\date\DateHelper;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use common\components\helpers\ArrayHelper;
use common\models\vehicle\Vehicle;
use common\models\logisticsRequest\LogisticsRequestFrom;
use common\models\logisticsRequest\LogisticsRequestCondition;
use common\models\logisticsRequest\LogisticsRequestLoadingAndUnloading;
use dosamigos\datetimepicker\DateTimePicker;
use common\components\helpers\Html;
use frontend\widgets\RequestAdditionalExpensesDropdownWidget;
use yii\helpers\Url;
use common\models\vehicle\VehicleType;

/* @var $this yii\web\View */
/* @var $model \common\models\logisticsRequest\LogisticsRequest */
/* @var $company \common\models\Company */
/* @var $logisticsRequestLoading \common\models\logisticsRequest\LogisticsRequestLoadingAndUnloading */
/* @var $logisticsRequestUnloading \common\models\logisticsRequest\LogisticsRequestLoadingAndUnloading */
/* @var $logisticsRequestContractEssenceCustomerAll \common\models\logisticsRequest\LogisticsRequestContractEssence */
/* @var $logisticsRequestContractEssenceCustomerForContractor \common\models\logisticsRequest\LogisticsRequestContractEssence */
/* @var $logisticsRequestContractEssenceCarrierAll \common\models\logisticsRequest\LogisticsRequestContractEssence */
/* @var $logisticsRequestContractEssenceCarrierForContractor \common\models\logisticsRequest\LogisticsRequestContractEssence */

$config = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'col-md-3 control-label',
        'style' => 'max-width: 140px;',
    ],
    'inputOptions' => [
        'style' => 'width: 100%;',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-9',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>
<?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'id' => 'request-form',
    'options' => [
        'enctype' => 'multipart/form-data',
        'class' => 'add-avtoschet',
    ],
    'enableClientValidation' => false,
])); ?>
    <div class="form-body form-body_sml form-body-request">
        <?= $form->errorSummary($model); ?>
        <div class="portlet">
            <?= $this->render('form/header', [
                'model' => $model,
            ]); ?>
            <div class="portlet-body">
                <div class="row" style="display: flex;">
                    <div class="col-md-6" style="border-right: 1px solid;display: flex;flex-direction: column;">
                        <?= $form->field($model, 'customer_id', $config)
                            ->widget(Select2::classname(), [
                                'data' => ['add-modal-contractor' => '[ + Добавить заказчика ]'] + $company->sortedContractorList(Contractor::TYPE_CUSTOMER),
                                'options' => [
                                    'placeholder' => '',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                ],
                            ])->label('Заказчик:'); ?>

                        <?= $form->field($model, 'customer_contact_person', $config)->label('Контактное лицо:'); ?>

                        <div class="form-group">
                            <div class="col-md-3" style="max-width: 140px;">
                                <label class="control-label">
                                    Договор
                                </label>
                            </div>
                            <div class="col-md-9">
                                <?= $this->render('form/_basis_document', [
                                    'model' => $model,
                                    'contractor' => $model->customer,
                                    'attribute' => 'customerAgreement',
                                ]); ?>
                            </div>
                        </div>

                        <div class="form-group" style="flex-grow: 1;">
                            <?= $form->field($model, 'customer_request_number', array_merge($config, [
                                'options' => [
                                    'class' => '',
                                ],
                                'labelOptions' => [
                                    'class' => 'col-md-3 control-label',
                                    'style' => 'padding-top: 0;max-width: 140px;',
                                ],
                                'wrapperOptions' => [
                                    'class' => 'col-md-4',
                                ],
                            ]))->label('№ заявки Заказчика:'); ?>

                            <?= $form->field($model, 'customer_request_date', [
                                'options' => [
                                    'class' => '',
                                ],
                                'labelOptions' => [
                                    'class' => 'col-md-1 control-label',
                                ],
                                'inputOptions' => [
                                    'style' => 'width: 100%;',
                                ],
                                'wrapperOptions' => [
                                    'class' => 'col-md-3',
                                ],
                                'template' => Yii::$app->params['formDatePickerTemplate'],
                            ])->textInput([
                                'class' => 'form-control date-picker min_wid_picker',
                                'value' => DateHelper::format($model->customer_request_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                            ])->label('от'); ?>
                            <span style="top: 2px;position: relative;" class="tooltip2 ico-question"
                                  data-tooltip-content="#tooltip_customer_request"></span>
                        </div>
                        <div class="">
                            <div class="form-group" style="margin-bottom: 25px;">
                                <label class="col-md-3 control-label">Оплата</label>
                            </div>
                            <div class="form-group">
                                <?= $form->field($model, 'customer_rate', array_merge($config, [
                                    'options' => [
                                        'class' => '',
                                    ],
                                    'labelOptions' => [
                                        'class' => 'col-md-3 control-label',
                                        'style' => 'max-width: 140px;',
                                    ],
                                    'wrapperOptions' => [
                                        'class' => 'col-md-3',
                                    ],
                                ]))->label('Ставка:'); ?>

                                <?= $form->field($model, 'customer_form_id', array_merge($config, [
                                    'options' => [
                                        'class' => '',
                                    ],
                                    'labelOptions' => [
                                        'class' => 'col-md-2 control-label',
                                    ],
                                    'wrapperOptions' => [
                                        'class' => 'col-md-4',
                                    ],
                                ]))->dropDownList([null => ''] + ArrayHelper::map(LogisticsRequestFrom::find()->all(), 'id', 'name'))
                                    ->label('Форма:'); ?>
                            </div>
                            <div class="form-group">
                                <?= $form->field($model, 'customer_condition_id', array_merge($config, [
                                    'options' => [
                                        'class' => '',
                                    ],
                                    'labelOptions' => [
                                        'class' => 'col-md-3 control-label',
                                        'style' => 'max-width: 140px;',
                                    ],
                                    'wrapperOptions' => [
                                        'class' => 'col-md-4',
                                    ],
                                ]))->dropDownList([null => ''] + ArrayHelper::map(LogisticsRequestCondition::find()->all(), 'id', 'name'))
                                    ->label('Условие:'); ?>

                                <?= $form->field($model, 'customer_delay', array_merge($config, [
                                    'options' => [
                                        'class' => '',
                                    ],
                                    'labelOptions' => [
                                        'class' => 'col-md-2 control-label p-l-0',
                                    ],
                                    'inputOptions' => [
                                        'style' => 'width: 55%;display: inline-block;',
                                        'type' => 'number',
                                        'min' => 0,
                                        'step' => 'any',
                                    ],
                                    'wrapperOptions' => [
                                        'class' => 'col-md-3 p-r-0 p-l-0',
                                        'style' => 'width: 19%;',
                                    ],
                                    'template' => "{label}\n{beginWrapper}\n{input}<span class='text-bold' style='margin-left: 5px;'>дн.</span>\n{error}\n{hint}\n{endWrapper}",
                                ]))->label('Отсрочка:'); ?>
                                <span style="top: 2px;position: relative;margin-left: 0;"
                                      class="tooltip2 ico-question"
                                      data-tooltip-content="#tooltip_customer_delay"></span>
                            </div>
                            <div class="form-group">
                                <div class="field-logisticsrequest-customer_additional_expenses_id">
                                    <label class="col-md-3 control-label"
                                           for="logisticsrequest-customer_additional_expenses_id"
                                           style="max-width: 140px;">
                                        Доп расходы:
                                    </label>
                                    <div class="col-md-4">
                                        <?= $form->field($model, 'customer_additional_expenses_id', [
                                            'template' => "{input}",
                                            'options' => [
                                                'class' => '',
                                                'style' => 'max-width: 500px;',
                                            ]])->widget(RequestAdditionalExpensesDropdownWidget::classname(), [
                                            'options' => [
                                                'prompt' => '',
                                            ],
                                        ]); ?>
                                    </div>
                                    <?= $this->render('form/expenses_item_form', [
                                        'inputId' => 'logisticsrequest-customer_additional_expenses_id',
                                        'type' => 'customer',
                                    ]); ?>
                                </div>
                                <div class="col-md-1 p-l-0 p-r-0" style="width: 4%;">
                                <span style="top: 2px;position: relative;margin-left: 0;"
                                      class="tooltip2 ico-question"
                                      data-tooltip-content="#tooltip_customer_additional_expenses"></span>
                                </div>
                                <?= $form->field($model, 'customer_amount', array_merge($config, [
                                    'options' => [
                                        'class' => '',
                                    ],
                                    'inputOptions' => [
                                        'class' => 'form-control is_own-disabled',
                                        'style' => 'width: 100%;',
                                        'disabled' => (bool)$model->is_own,
                                    ],
                                    'labelOptions' => [
                                        'class' => 'col-md-2 control-label',
                                    ],
                                    'wrapperOptions' => [
                                        'class' => 'col-md-2 p-l-0 p-r-0',
                                        'style' => 'width: 19%;',
                                    ],
                                ]))->label('Сумма:'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6" style="border-left: 1px solid;">
                        <?= $form->field($model, 'carrier_id', $config)->widget(Select2::classname(), [
                            'data' => ['add-modal-contractor' => '[ + Добавить перевозчика ]'] +
                                ['is_own' => 'Собственный транспорт'] +
                                $company->sortedContractorList(Contractor::TYPE_SELLER),
                            'options' => [
                                'placeholder' => '',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ])->label('Перевозчик:'); ?>

                        <?= Html::activeHiddenInput($model, 'is_own'); ?>

                        <?= $form->field($model, 'carrier_contact_person', $config)->label('Контактное лицо:'); ?>

                        <div class="form-group required row">
                            <div class="col-md-3" style="max-width: 140px;">
                                <label class="control-label">
                                    Договор
                                </label>
                            </div>
                            <div class="col-md-9">
                                <?= $this->render('form/_basis_document', [
                                    'model' => $model,
                                    'contractor' => $model->carrier,
                                    'attribute' => 'carrierAgreement',
                                    'options' => [
                                        'class' => 'is_own-disabled',
                                        'disabled' => (bool)$model->is_own,
                                    ],
                                ]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?= $form->field($model, 'carrier_request_number', array_merge($config, [
                                'options' => [
                                    'class' => '',
                                ],
                                'labelOptions' => [
                                    'class' => 'col-md-3 control-label',
                                    'style' => 'padding-top: 0;max-width: 140px;',
                                ],
                                'wrapperOptions' => [
                                    'class' => 'col-md-4',
                                ],
                            ]))->label('№ заявки Перевозчика:'); ?>

                            <?= $form->field($model, 'carrier_request_date', [
                                'options' => [
                                    'class' => '',
                                ],
                                'labelOptions' => [
                                    'class' => 'col-md-1 control-label',
                                ],
                                'inputOptions' => [
                                    'style' => 'width: 100%;',
                                ],
                                'wrapperOptions' => [
                                    'class' => 'col-md-3',
                                ],
                                'template' => Yii::$app->params['formDatePickerTemplate'],
                            ])->textInput([
                                'class' => 'form-control date-picker min_wid_picker',
                                'value' => DateHelper::format($model->customer_request_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                            ])->label('от'); ?>
                            <span style="top: 2px;position: relative;" class="tooltip2 ico-question"
                                  data-tooltip-content="#tooltip_carrier_request"></span>
                        </div>

                        <?= $form->field($model, 'driver_id', array_merge($config, [
                            'template' => "{label}\n{beginWrapper}\n{input}<span class=\"driver-phone\"></span>\n{error}\n{hint}\n{endWrapper}",
                        ]))->dropDownList(ArrayHelper::map($company->drivers, 'id', 'fio'))->label('Водитель:'); ?>
                        <?= $form->field($model, 'vehicle_id', $config)
                            ->dropDownList(ArrayHelper::map($company->getVehicles()
                                ->andWhere(['in', 'vehicle_type_id', [VehicleType::TYPE_TRACTOR, VehicleType::TYPE_WAGON]])->all(), 'id', function (Vehicle $model) {
                                $name = $model->model ? ($model->model . ' ') : null;
                                $name .= $model->state_number;

                                return $name;
                            }))->label('Транспорт:'); ?>

                        <div class="form-group" style="margin-bottom: 25px;">
                            <label class="col-md-3 control-label">Оплата</label>
                        </div>
                        <div class="form-group">
                            <?= $form->field($model, 'carrier_rate', array_merge($config, [
                                'options' => [
                                    'class' => '',
                                ],
                                'inputOptions' => [
                                    'class' => 'form-control is_own-disabled',
                                    'style' => 'width: 100%;',
                                    'disabled' => (bool)$model->is_own,
                                ],
                                'labelOptions' => [
                                    'class' => 'col-md-3 control-label',
                                    'style' => 'max-width: 140px;',
                                ],
                                'wrapperOptions' => [
                                    'class' => 'col-md-3',
                                ],
                            ]))->label('Ставка:'); ?>

                            <?= $form->field($model, 'carrier_form_id', array_merge($config, [
                                'options' => [
                                    'class' => '',
                                ],
                                'inputOptions' => [
                                    'class' => 'form-control is_own-disabled',
                                    'style' => 'width: 100%;',
                                    'disabled' => (bool)$model->is_own,
                                ],
                                'labelOptions' => [
                                    'class' => 'col-md-2 control-label',
                                ],
                                'wrapperOptions' => [
                                    'class' => 'col-md-4',
                                ],
                            ]))->dropDownList([null => ''] + ArrayHelper::map(LogisticsRequestFrom::find()->all(), 'id', 'name'))
                                ->label('Форма:'); ?>
                        </div>
                        <div class="form-group">
                            <?= $form->field($model, 'carrier_condition_id', array_merge($config, [
                                'options' => [
                                    'class' => '',
                                ],
                                'inputOptions' => [
                                    'class' => 'form-control is_own-disabled',
                                    'style' => 'width: 100%;',
                                    'disabled' => (bool)$model->is_own,
                                ],
                                'labelOptions' => [
                                    'class' => 'col-md-3 control-label',
                                    'style' => 'max-width: 140px;',
                                ],
                                'wrapperOptions' => [
                                    'class' => 'col-md-4',
                                ],
                            ]))->dropDownList([null => ''] + ArrayHelper::map(LogisticsRequestCondition::find()->all(), 'id', 'name'))
                                ->label('Условие:'); ?>

                            <?= $form->field($model, 'carrier_delay', array_merge($config, [
                                'options' => [
                                    'class' => '',
                                ],
                                'labelOptions' => [
                                    'class' => 'col-md-2 control-label p-l-0',
                                ],
                                'inputOptions' => [
                                    'class' => 'form-control is_own-disabled',
                                    'style' => 'width: 55%;display: inline-block;',
                                    'type' => 'number',
                                    'min' => 0,
                                    'step' => 'any',
                                    'disabled' => (bool)$model->is_own,
                                ],
                                'wrapperOptions' => [
                                    'class' => 'col-md-3 p-r-0 p-l-0',
                                    'style' => 'width: 19%;',
                                ],
                                'template' => "{label}\n{beginWrapper}\n{input}<span class='text-bold' style='margin-left: 5px;'>дн.</span>\n{error}\n{hint}\n{endWrapper}",
                            ]))->label('Отсрочка:'); ?>
                            <span style="top: 2px;position: relative;margin-left: 0;"
                                  class="tooltip2 ico-question"
                                  data-tooltip-content="#tooltip_carrier_delay"></span>
                        </div>
                        <div class="form-group">
                            <div class="field-logisticsrequest-carrier_additional_expenses_id">
                                <label class="col-md-3 control-label"
                                       for="logisticsrequest-carrier_additional_expenses_id" style="max-width: 140px;">
                                    Доп расходы:
                                </label>
                                <div class="col-md-4">
                                    <?= $form->field($model, 'carrier_additional_expenses_id', [
                                        'template' => "{input}",
                                        'options' => [
                                            'class' => '',
                                            'style' => 'max-width: 500px;',
                                        ]])->widget(RequestAdditionalExpensesDropdownWidget::classname(), [
                                        'options' => [
                                            'prompt' => '',
                                        ],
                                    ]); ?>
                                </div>
                                <?= $this->render('form/expenses_item_form', [
                                    'inputId' => 'logisticsrequest-carrier_additional_expenses_id',
                                    'type' => 'carrier',
                                ]); ?>
                            </div>
                            <div class="col-md-1 p-l-0 p-r-0" style="width: 4%;">
                                <span style="top: 2px;position: relative;margin-left: 0;"
                                      class="tooltip2 ico-question"
                                      data-tooltip-content="#tooltip_carrier_additional_expenses"></span>
                            </div>
                            <?= $form->field($model, 'carrier_amount', array_merge($config, [
                                'options' => [
                                    'class' => '',
                                ],
                                'labelOptions' => [
                                    'class' => 'col-md-2 control-label',
                                ],
                                'inputOptions' => [
                                    'class' => 'form-control is_own-disabled',
                                    'style' => 'width: 100%;',
                                    'disabled' => (bool)$model->is_own,
                                ],
                                'wrapperOptions' => [
                                    'class' => 'col-md-2 p-l-0 p-r-0',
                                    'style' => 'width: 19%;',
                                ],
                            ]))->label('Сумма:'); ?>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 30px;">
                    <div class="col-md-12">
                        <table class="table table-striped table-bordered table-hover dataTable" role="grid">
                            <thead>
                            <tr class="heading">
                                <th width="4%">Вид</th>
                                <th width="10%">Дата</th>
                                <th width="9%">Время</th>
                                <th width="10%">Город</th>
                                <th width="30%">Адрес</th>
                                <th width="10%">Контактное лицо</th>
                                <th width="17%">Телефон</th>
                                <th width="10%">Способ Погрузки/Разгрузки</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <?php foreach ($logisticsRequestLoading->attributeLabels() as $attribute => $label): ?>
                                    <?php if (in_array($attribute, ['id', 'logistics_request_id'])) continue; ?>
                                    <td>
                                        <?php if ($attribute == 'type'): ?>
                                            <?= LogisticsRequestLoadingAndUnloading::$types[$logisticsRequestLoading->$attribute]; ?>
                                        <?php elseif ($attribute == 'time'): ?>
                                            <?php $hours = $logisticsRequestLoading->time ? DateHelper::format($logisticsRequestLoading->time, 'G', 'H:i:s') : null;
                                            $minutes = $logisticsRequestLoading->time ? DateHelper::format($logisticsRequestLoading->time, 'i', 'H:i:s') : null;
                                            $logisticsRequestLoading->time = $logisticsRequestLoading->time ? DateHelper::format($logisticsRequestLoading->time, DateHelper::FORMAT_USER_DATE . ' H:i', 'H:i:s') : null; ?>
                                            <?= $form->field($logisticsRequestLoading, $attribute, [
                                                'options' => [
                                                    'class' => '',
                                                ],
                                                'inputOptions' => [
                                                    'style' => 'width: 100%;',
                                                ],
                                                'template' => "{input}\n{error}\n{hint}",
                                            ])->widget(DateTimePicker::className(), [
                                                'language' => 'ru',
                                                'size' => 'ms',
                                                'template' => '{input}',
                                                'pickButtonIcon' => 'glyphicon glyphicon-time',
                                                'inline' => false,
                                                'options' => [
                                                    'name' => 'LogisticsRequestLoadingAndUnloading[' . $logisticsRequestLoading->type . '][' . $attribute . ']',
                                                    'class' => 'logisticsrequestLoadingandunloading-time',
                                                    'id' => 'logisticsrequestLoadingandunloading-0-time',
                                                    'data-hour' => $hours,
                                                    'data-minute' => $minutes,
                                                ],
                                                'clientOptions' => [
                                                    'startView' => 1,
                                                    'minView' => 0,
                                                    'maxView' => 1,
                                                    'autoclose' => true,
                                                    'minuteStep' => 15,
                                                    'format' => 'hh:ii',
                                                    'pickerPosition' => 'top-left',
                                                    'useCurrent' => true,
                                                ],
                                            ])->label(false); ?>
                                        <?php elseif ($attribute == 'contact_person_phone'): ?>
                                            <?= $form->field($logisticsRequestLoading, $attribute, [
                                                'options' => [
                                                    'class' => '',
                                                ],
                                                'template' => "{input}\n{error}\n{hint}",
                                            ])->widget(\yii\widgets\MaskedInput::className(), [
                                                'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                                                'options' => [
                                                    'class' => 'form-control',
                                                    'style' => 'width: 100%;',
                                                    'name' => 'LogisticsRequestLoadingAndUnloading[' . $logisticsRequestLoading->type . '][' . $attribute . ']',
                                                    'id' => 'logisticsrequestloadingandunloading-' . $logisticsRequestLoading->type . '-contact_person_phone',
                                                    'placeholder' => '+7(XXX) XXX-XX-XX',
                                                ],
                                            ]); ?>
                                        <?php else: ?>
                                            <?= $form->field($logisticsRequestLoading, $attribute, [
                                                'options' => [
                                                    'class' => '',
                                                ],
                                                'inputOptions' => [
                                                    'class' => 'form-control' . ($attribute == 'date' ? ' date-picker min_wid_picker' : null),
                                                    'style' => 'width: 100%;',
                                                    'name' => 'LogisticsRequestLoadingAndUnloading[' . $logisticsRequestLoading->type . '][' . $attribute . ']',
                                                    'value' => $attribute == 'date' ?
                                                        DateHelper::format($logisticsRequestLoading->$attribute, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) :
                                                        $logisticsRequestLoading->$attribute,
                                                ],
                                                'template' => "{input}\n{error}\n{hint}",
                                            ])->label(false); ?>
                                        <?php endif; ?>
                                    </td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <?php foreach ($logisticsRequestUnloading->attributeLabels() as $attribute => $label): ?>
                                    <?php if (in_array($attribute, ['id', 'logistics_request_id'])) continue; ?>
                                    <td>
                                        <?php if ($attribute == 'type'): ?>
                                            <?= LogisticsRequestLoadingAndUnloading::$types[$logisticsRequestUnloading->$attribute]; ?>
                                        <?php elseif ($attribute == 'time'): ?>
                                            <?php $hours = $logisticsRequestUnloading->time ? DateHelper::format($logisticsRequestUnloading->time, 'G', 'H:i:s') : null;
                                            $minutes = $logisticsRequestUnloading->time ? DateHelper::format($logisticsRequestUnloading->time, 'i', 'H:i:s') : null;
                                            $logisticsRequestUnloading->time = $logisticsRequestUnloading->time ? DateHelper::format($logisticsRequestUnloading->time, DateHelper::FORMAT_USER_DATE . ' H:i', 'H:i:s') : null; ?>
                                            <?= $form->field($logisticsRequestUnloading, $attribute, [
                                                'options' => [
                                                    'class' => '',
                                                ],
                                                'inputOptions' => [
                                                    'style' => 'width: 100%;',
                                                ],
                                                'template' => "{input}\n{error}\n{hint}",
                                            ])->widget(DateTimePicker::className(), [
                                                'language' => 'ru',
                                                'size' => 'ms',
                                                'template' => '{input}',
                                                'pickButtonIcon' => 'glyphicon glyphicon-time',
                                                'inline' => false,
                                                'options' => [
                                                    'name' => 'LogisticsRequestLoadingAndUnloading[' . $logisticsRequestUnloading->type . '][' . $attribute . ']',
                                                    'class' => 'logisticsrequestLoadingandunloading-time',
                                                    'id' => 'logisticsrequestLoadingandunloading-1-time',
                                                    'data-hour' => $hours,
                                                    'data-minute' => $minutes,
                                                ],
                                                'clientOptions' => [
                                                    'startView' => 1,
                                                    'minView' => 0,
                                                    'maxView' => 1,
                                                    'autoclose' => true,
                                                    'minuteStep' => 15,
                                                    'format' => 'hh:ii',
                                                    'pickerPosition' => 'top-left',
                                                    'useCurrent' => true,
                                                ],
                                            ])->label(false); ?>
                                        <?php elseif ($attribute == 'contact_person_phone'): ?>
                                            <?= $form->field($logisticsRequestUnloading, $attribute, [
                                                'options' => [
                                                    'class' => '',
                                                ],
                                                'template' => "{input}\n{error}\n{hint}",
                                            ])->widget(\yii\widgets\MaskedInput::className(), [
                                                'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                                                'options' => [
                                                    'class' => 'form-control',
                                                    'style' => 'width: 100%;',
                                                    'name' => 'LogisticsRequestLoadingAndUnloading[' . $logisticsRequestUnloading->type . '][' . $attribute . ']',
                                                    'id' => 'logisticsrequestloadingandunloading-' . $logisticsRequestUnloading->type . '-contact_person_phone',
                                                    'placeholder' => '+7(XXX) XXX-XX-XX',
                                                ],
                                            ]); ?>
                                        <?php else: ?>
                                            <?= $form->field($logisticsRequestUnloading, $attribute, [
                                                'options' => [
                                                    'class' => '',
                                                ],
                                                'inputOptions' => [
                                                    'class' => 'form-control' . ($attribute == 'date' ? ' date-picker min_wid_picker' : null),
                                                    'style' => 'width: 100%;',
                                                    'name' => 'LogisticsRequestLoadingAndUnloading[' . $logisticsRequestUnloading->type . '][' . $attribute . ']',
                                                    'value' => $attribute == 'date' ?
                                                        DateHelper::format($logisticsRequestUnloading->$attribute, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) :
                                                        $logisticsRequestUnloading->$attribute,
                                                ],
                                                'template' => "{input}\n{error}\n{hint}",
                                            ])->label(false); ?>
                                        <?php endif; ?>
                                    </td>
                                <?php endforeach; ?>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-12 row" style="margin-top: 15px;">
                    <?= $form->field($model, 'goods_name', array_merge($config, [
                        'labelOptions' => [
                            'class' => 'col-md-3 control-label',
                            'style' => 'max-width: 140px;',
                        ],
                        'wrapperOptions' => [
                            'class' => 'col-md-6',
                        ],
                    ]))->label('Груз:'); ?>

                    <div class="form-group">
                        <div class="col-md-3">
                            <?= $form->field($model, 'goods_weight', array_merge($config, [
                                'options' => [
                                    'class' => '',
                                ],
                                'labelOptions' => [
                                    'class' => 'col-md-3 control-label p-l-0 p-r-0',
                                    'style' => 'width: 140px;',
                                ],
                                'wrapperOptions' => [
                                    'class' => 'col-md-4 p-l-0 p-r-0',
                                ],
                            ]))->label('Вес (тонн):'); ?>
                        </div>
                        <div class="col-md-2 p-l-0" style="width: 11%;">
                            <label class="control-label" for="vehicle-length">
                                Габариты (м)
                            </label>
                        </div>
                        <div class="col-md-1 p-l-0 p-r-0" style="width: 10%;">
                            <?= $form->field($model, 'goods_length', [
                                'options' => [
                                    'class' => '',
                                ],
                                'labelOptions' => [
                                    'class' => '',
                                    'style' => 'font-weight: bold;',
                                ],
                                'template' => "{label}\n{input}\n{error}\n{hint}",
                            ])->textInput([
                                'type' => 'number',
                                'class' => 'form-control',
                                'style' => 'width: 78%;display: inline-block;',
                                'min' => 0,
                                'max' => 10000000,
                                'step' => 'any',
                            ])->label('Д:'); ?>
                        </div>
                        <div class="col-md-1 p-l-0 p-r-0" style="width: 10%;">
                            <?= $form->field($model, 'goods_width', [
                                'options' => [
                                    'class' => '',
                                ],
                                'labelOptions' => [
                                    'class' => '',
                                    'style' => 'font-weight: bold;',
                                ],
                                'template' => "{label}\n{input}\n{error}\n{hint}",
                            ])->textInput([
                                'type' => 'number',
                                'class' => 'form-control',
                                'style' => 'width: 78%;display: inline-block;',
                                'min' => 0,
                                'max' => 10000000,
                                'step' => 'any',
                            ])->label('Ш:'); ?>
                        </div>
                        <div class="col-md-1 p-l-0 p-r-0" style="width: 10%;">
                            <?= $form->field($model, 'goods_height', [
                                'options' => [
                                    'class' => '',
                                ],
                                'labelOptions' => [
                                    'class' => '',
                                    'style' => 'font-weight: bold;',
                                ],
                                'template' => "{label}\n{input}\n{error}\n{hint}",
                            ])->textInput([
                                'type' => 'number',
                                'class' => 'form-control',
                                'style' => 'width: 78%;display: inline-block;',
                                'min' => 0,
                                'max' => 10000000,
                                'step' => 'any',
                            ])->label('В:'); ?>
                        </div>
                    </div>
                    <?= $form->field($model, 'goods_count', array_merge($config, [
                        'labelOptions' => [
                            'class' => 'col-md-3 control-label',
                            'style' => 'max-width: 140px;',
                        ],
                        'inputOptions' => [
                            'type' => 'number',
                            'class' => 'form-control',
                            'min' => 1,
                            'max' => 10000000,
                            'step' => 'any',
                            'style' => 'width: 100%;',
                        ],
                        'wrapperOptions' => [
                            'class' => 'col-md-2',
                            'style' => 'width: 10%;',
                        ],
                    ]))->label('Кол-во (шт.):'); ?>
                </div>
                <div class="row" style="margin-top: 15px;">
                    <div class="col-md-6">
                        <div class="has_customer_agreement-hide"
                             style="display: <?= $model->customerAgreement ? 'none' : 'block'; ?>;">
                            <div class="field-logisticsrequest_customer_request_essence">
                                <label class="text-bold">
                                    Предмет договора с закачиком
                                </label>
                                <div style="margin-bottom: 15px;">
                                    <?= Html::activeTextarea($model, 'customer_request_essence', [
                                        'class' => 'form-control',
                                        'rows' => 6,
                                        'style' => 'width: 100%;',
                                        'value' => $logisticsRequestContractEssenceCustomerAll->is_checked && $model->isNewRecord ?
                                            $logisticsRequestContractEssenceCustomerAll->text : $model->customer_request_essence,
                                    ]); ?>
                                </div>
                            </div>
                            <div>
                                <label class="text-bold">
                                    Сохранить текст для всех договоров-заявок
                                </label>
                            </div>
                            <div class="field-logisticsrequest_customer_request_essence-save_with_current">
                                <div class="row">
                                    <div class="col-xs-1" style="width: 3%;">
                                    <span style="position: relative;">
                                        <?= Html::activeCheckbox($logisticsRequestContractEssenceCustomerForContractor, 'is_checked', [
                                            'class' => 'form-control',
                                            'name' => "LogisticsRequestContractEssence[customer][contractor][is_checked]",
                                            'id' => 'logisticsrequestcontractessence-customer-contractor-is_checked',
                                            'label' => false,
                                        ]); ?>
                                    </span>
                                    </div>
                                    <div class="col-xs-11">
                                        <label for="logisticsrequestcontractessence-customer-contractor-is_checked"
                                               class="control-label"
                                               style="padding-top: 0px !important;">
                                            С данным заказчиком
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="field-logisticsrequest_customer_request_essence-save_with_all">
                                <div class="row">
                                    <div class="col-xs-1" style="width: 3%;">
                                    <span style="position: relative;">
                                        <?= Html::activeCheckbox($logisticsRequestContractEssenceCustomerAll, 'is_checked', [
                                            'class' => 'form-control',
                                            'name' => "LogisticsRequestContractEssence[customer][all][is_checked]",
                                            'id' => 'logisticsrequestcontractessence-customer-all-is_checked',
                                            'label' => false,
                                        ]); ?>
                                    </span>
                                    </div>
                                    <div class="col-xs-11">
                                        <label for="logisticsrequestcontractessence-customer-all-is_checked"
                                               class="control-label"
                                               style="padding-top: 0px !important;">
                                            Со всеми заказчиками
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="is_own-hide has_carrier_agreement-hide"
                             style="display: <?= (bool)$model->is_own || $model->carrierAgreement ? 'none' : 'block'; ?>;">
                            <div class="field-logisticsrequest_carrier_request_essence">
                                <label class="text-bold">
                                    Предмет договора с перевозчиком
                                </label>
                                <div style="margin-bottom: 15px;">
                                    <?= Html::activeTextarea($model, 'carrier_request_essence', [
                                        'class' => 'form-control',
                                        'rows' => 6,
                                        'style' => 'width: 100%;',
                                        'value' => $logisticsRequestContractEssenceCarrierAll->is_checked && $model->isNewRecord ?
                                            $logisticsRequestContractEssenceCarrierAll->text : $model->carrier_request_essence,
                                    ]); ?>
                                </div>
                            </div>
                            <div>
                                <label class="text-bold">
                                    Сохранить текст для всех договоров-заявок
                                </label>
                            </div>
                            <div class="field-logisticsrequest_carrier_request_essence-save_with_current">
                                <div class="row">
                                    <div class="col-xs-1" style="width: 3%;">
                                    <span style="position: relative;">
                                        <?= Html::activeCheckbox($logisticsRequestContractEssenceCarrierForContractor, 'is_checked', [
                                            'class' => 'form-control',
                                            'name' => "LogisticsRequestContractEssence[carrier][contractor][is_checked]",
                                            'id' => 'logisticsrequestcontractessence-carrier-contractor-is_checked',
                                            'label' => false,
                                        ]); ?>
                                    </span>
                                    </div>
                                    <div class="col-xs-11">
                                        <label for="logisticsrequestcontractessence-carrier-contractor-is_checked"
                                               class="control-label"
                                               style="padding-top: 0px !important;">
                                            С данным заказчиком
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="field-logisticsrequest_carrier_request_essence-save_with_all">
                                <div class="row">
                                    <div class="col-xs-1" style="width: 3%;">
                                    <span style="position: relative;">
                                        <?= Html::activeCheckbox($logisticsRequestContractEssenceCarrierAll, 'is_checked', [
                                            'class' => 'form-control',
                                            'name' => "LogisticsRequestContractEssence[carrier][all][is_checked]",
                                            'id' => 'logisticsrequestcontractessence-carrier-all-is_checked',
                                            'label' => false,
                                        ]); ?>
                                    </span>
                                    </div>
                                    <div class="col-xs-11">
                                        <label for="logisticsrequestcontractessence-carrier-all-is_checked"
                                               class="control-label"
                                               style="padding-top: 0px !important;">
                                            Со всеми заказчиками
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row action-buttons">
                <div class="spinner-button col-sm-1 col-xs-1">
                    <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs create-invoice mt-ladda-btn ladda-button',
                        'data-style' => 'expand-right',
                    ]); ?>
                    <?= Html::submitButton('<span class="ico-Save-smart-pls fs"></span>', [
                        'class' => 'btn darkblue widthe-100 hidden-lg create-invoice',
                        'title' => 'Сохранить',
                    ]); ?>
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="spinner-button col-sm-1 col-xs-1">
                    <?= Html::a('Отменить', Yii::$app->request->referrer ? Yii::$app->request->referrer : Url::to(['index']), [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                        'data-style' => 'expand-right',
                    ]); ?>
                    <?= Html::a('<span class="ico-Cancel-smart-pls fs"></span>', Yii::$app->request->referrer ? Yii::$app->request->referrer : Url::to(['index']), [
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                        'title' => 'Отменить',
                    ]); ?>
                </div>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
        </div>
    </div>
    <div class="modal fade t-p-f modal_scroll_center mobile-modal" id="add-new" tabindex="-1" role="modal"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
                <div class="modal-body" id="block-modal-new-product-form">

                </div>
            </div>
        </div>
    </div>
<?php Modal::begin([
    'id' => 'agreement-modal-container',
    'header' => '<h3 id="agreement-modal-header">Добавить договор</h3>',
    'options' => ['style' => 'max-width: 600px;']
]);
Pjax::begin([
    'id' => 'agreement-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]);

Pjax::end();
Modal::end(); ?>
    <div class="tooltip_templates" style="display: none;">
        <span id="tooltip_customer_request" style="display: inline-block; text-align: center;">
            Если у Заказчика свой номер и дата заявки, то укажите их тут
        </span>
        <span id="tooltip_carrier_request" style="display: inline-block; text-align: center;">
            Если у Перевозчика свой номер и дата заявки, то укажите их тут
        </span>
        <span id="tooltip_carrier_delay" style="display: inline-block; text-align: center;">
            Количество дней от условия
        </span>
        <span id="tooltip_carrier_additional_expenses" style="display: inline-block; text-align: center;">
            Прибыль будет уменьшаться на сумму доп. расходов
        </span>
        <span id="tooltip_customer_delay" style="display: inline-block; text-align: center;">
            Количество дней от условия
        </span>
        <span id="tooltip_customer_additional_expenses" style="display: inline-block; text-align: center;">
            Прибыль будет уменьшаться на сумму доп. расходов
        </span>
    </div>
<?php ActiveForm::end(); ?>
<?php $this->registerJs('
    $(document).on("change", "#logisticsrequest-customer_id, #logisticsrequest-carrier_id", function() {
        var $customerContractEssence = $(".has_customer_agreement-hide");
        var $carrierContractEssence = $(".has_carrier_agreement-hide");

        if ($(this).attr("id") == "logisticsrequest-customer_id") {
            $pjaxContainer = "#customerAgreement-pjax-container";
            $customerContractEssence.show();
            $type = 0;
        } else {
            $pjaxContainer = "#carrierAgreement-pjax-container";
            $carrierContractEssence.show();
            $type = 1;
        }
        $.post("contractor-essence", {
                contractor_id: $(this).val(),
                type: $type
            }, function (data) {
                if (data.result == true) {
                    if ($type == 0) {
                        $("#logisticsrequest-customer_request_essence").val(data.essence);
                        $("#logisticsrequestcontractessence-customer-contractor-is_checked:not(:checked)").click();
                    } else {
                        $("#logisticsrequest-carrier_request_essence").val(data.essence);
                        $("#logisticsrequestcontractessence-carrier-contractor-is_checked:not(:checked)").click();
                    }
                }
        });
        if ($($pjaxContainer).length) {
            $.pjax({
                url: $($pjaxContainer).data("url"),
                data: {
                    contractorId: $(this).val(),
                },
                container: $pjaxContainer,
                push: false,
                timeout: 10000,
            });
        }
    });
    
    $(document).on("change", "#logisticsrequest-customeragreement, #logisticsrequest-carrieragreement", function(e) {
        var value = $(this).val() || $(this).text();
        
        if ($(this).attr("id") == "logisticsrequest-customeragreement") {
            $contractorID = $("#logisticsrequest-customer_id").val();
            $type = 2;
        } else {
            $contractorID = $("#logisticsrequest-carrier_id").val();
            $type = 1;
        }
        if (value == "add-modal-agreement") {
            e.preventDefault();
    
            $.pjax({
                url: "/documents/agreement/create?contractor_id=" + $contractorID + "&type=" + $type + "&returnTo=request",
                container: "#agreement-form-container",
                push: false,
                timeout: 5000,
            });
    
            $(document).on("pjax:success", function() {
                $("#agreement-modal-header").html($("[data-header]").data("header"));
                $(".date-picker").datepicker({format: "dd.mm.yyyy", language:"ru", autoclose: true}).on("change.dp", dateChanged);
    
                function dateChanged(ev) {
                    if (ev.bubbles == undefined) {
                        var $input = $("[name=\'" + ev.currentTarget.name +"\']");
                        if (ev.currentTarget.value == "") {
                            if ($input.data("last-value") == null) {
                                $input.data("last-value", ev.currentTarget.defaultValue);
                            }
                            var $lastDate = $input.data("last-value");
                            $input.datepicker("setDate", $lastDate);
                        } else {
                            $input.data("last-value", ev.currentTarget.value);
                        }
                    }
                };
            });
            $("#agreement-modal-container").modal("show");
            $(this).val("").trigger("change");
        }
    });
    $("#customerAgreement-pjax-container").on("pjax:complete", function() {
        if (window.AgreementValue) {
            $("#logisticsrequest-customeragreement").val(window.AgreementValue).trigger("change");
        }
    });
    $("#carrierAgreement-pjax-container").on("pjax:complete", function() {
        if (window.AgreementValue) {
            $("#logisticsrequest-carrieragreement").val(window.AgreementValue).trigger("change");
        }
    });
    $(document).on("change", "#logisticsrequest-driver_id", function (data) {
        loadDriverPhone($(this).val());
    });
    
    $(document).ready(function () {
        loadDriverPhone($("#logisticsrequest-driver_id").val());
    });
    function loadDriverPhone(driverID) {
        $.post("/logistics/driver/phone", {driver_id: driverID}, function (data) {
            if (data.result) {
                $(".driver-phone").text(data.phone);
            }
        });
    }
    
    $(document).on("change", "#logisticsrequest-carrier_id", function () {
        var $isOwnDisabled = $(".is_own-disabled");
        var $isOwnHide = $(".is_own-hide");
        
        if ($(this).val() == "is_own") {
            $("#logisticsrequest-is_own").val(1);
            $isOwnDisabled.val("").trigger("change").attr("disabled", true);
            $isOwnHide.find("textarea").val("");
            $isOwnHide.find("input:checkbox:checked").click();
            $isOwnHide.hide();       
        } else {
            $("#logisticsrequest-is_own").val(0);
            $isOwnDisabled.removeAttr("disabled");
            $isOwnHide.show();
        }
    });
    
    $(document).on("change", "#logisticsrequest-is_own", function (e) {
        var $isOwnDisabled = $(".is_own-disabled");
        var $isOwnHide = $(".is_own-hide");
        
        if ($(this).is(":checked")) {
            $isOwnDisabled.val("").trigger("change").attr("disabled", true);
            $isOwnHide.find("textarea").val("");
            $isOwnHide.find("input:checkbox:checked").click();
            $isOwnHide.hide();
        } else {
            $isOwnDisabled.removeAttr("disabled");
            $isOwnHide.show();
        }
    });
    
    $(document).on("change", "#logisticsrequest-customeragreement", function (data) {
    console.log($(this).val());
        var $customerContractEssence = $(".has_customer_agreement-hide");
        
        if ($(this).val() == "" || $(this).val() == null) {
            $customerContractEssence.show();
        } else {
            $customerContractEssence.hide();
        }
    });
    
    $(document).on("change", "#logisticsrequest-carrieragreement", function (data) {
        var $carrierContractEssence = $(".has_carrier_agreement-hide");
        
        if ($(this).val() == "" || $(this).val() == null) {
            $carrierContractEssence.show();
        } else {
            $carrierContractEssence.hide();
        }
    });
    
    var $pickers = $(".datetimepicker .datetimepicker-minutes, .datetimepicker .datetimepicker-hours");
    $pickers.find("thead").remove();
    $pickers.find("tbody tr td").css("width", "185px");
    $("input.logisticsrequestLoadingandunloading-time").each(function () {
        if ($(this).data("hour") !== undefined && $(this).data("minute") !== undefined) {
            $(this).datetimepicker("show");
            $(".datetimepicker-hours:visible").find("tbody span.hour:contains(" + $(this).data("hour") +")").click();
            $(".datetimepicker-minutes:visible").find("tbody span.minute:contains(" + $(this).data("minute") +")").click();
        }
    });
');