<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 06.12.2018
 * Time: 15:01
 */

use common\components\helpers\Html;
use yii\helpers\Url;
use frontend\modules\documents\widgets\DocumentLogWidget;
use yii\bootstrap\Tabs;
use \common\models\logisticsRequest\LogisticsRequest;
use common\components\image\EasyThumbnailImage;
use common\models\file\widgets\FileUpload;
use common\components\date\DateHelper;
use common\models\vehicle\VehicleType;
use frontend\widgets\ConfirmModalWidget;
use yii\bootstrap\Dropdown;
use common\models\logisticsRequest\LogisticsRequestStatus;

/* @var $this yii\web\View
 * @var $model \common\models\logisticsRequest\LogisticsRequest
 * @var $activeTab integer
 */

$logoLink = !$model->company->logo_link ? null :
    EasyThumbnailImage::thumbnailSrc($model->company->getImage('logoImage'), 150, 90, EasyThumbnailImage::THUMBNAIL_INSET);

$this->context->layoutWrapperCssClass = 'out-document';
$this->title = $model->getTitle($activeTab);
$createInvoiceText = $activeTab == LogisticsRequest::TYPE_CUSTOMER ? 'Счет' : 'Загрузить счет';
$tabItems = [];
if (!$model->is_own) {
    $tabItems[] = [
        'label' => 'С перевозчиком',
        'url' => Url::to(['view', 'id' => $model->id, 'activeTab' => LogisticsRequest::TYPE_CARRIER]),
        'active' => $activeTab == LogisticsRequest::TYPE_CARRIER,
        'linkOptions' => [
            'style' => 'border-top: 0;border-right: 0;font-size: 14px;',
        ],
    ];
}
$tabItems[] = [
    'label' => 'С заказчиком',
    'url' => Url::to(['view', 'id' => $model->id, 'activeTab' => LogisticsRequest::TYPE_CUSTOMER]),
    'active' => $activeTab == LogisticsRequest::TYPE_CUSTOMER,
    'linkOptions' => [
        'style' => 'border-top: 0;font-size: 14px;',
    ],
];
?>
    <div class="page-content-in logistics-request-view" style="padding-bottom: 30px;">
        <div style="col-xs-12 pad3">
            <?= Html::a('Назад к списку', Url::to(['index']), [
                'class' => 'back-to-customers',
            ]); ?>
        </div>
        <div class="col-xs-12 pad0">
            <div class="col-xs-12 col-lg-7 pad0">
                <div class="page-content-in m-size-div container-first-account-table no_min_h pad0 logistics-request-main_info"
                     style="min-width: 520px; max-width: 735px; border: 1px solid rgb(66, 118, 164); margin-top: 3px; min-height: 229px;">
                    <div class="actions pull-left">
                        <?= DocumentLogWidget::widget([
                            'model' => $model,
                        ]); ?>
                        <?= Html::a('<i class="icon-pencil"></i>', Url::to(['update', 'id' => $model->id]), [
                            'class' => 'btn darkblue btn-sm',
                            'title' => 'Редактировать',
                        ]); ?>
                    </div>
                    <div class="profile-form-tabs logistics-request-tabs col-md-9 pull-right">
                        <?= Tabs::widget([
                            'options' => ['class' => 'nav-form-tabs row'],
                            'headerOptions' => [
                                'class' => 'text-center',
                                'style' => 'padding-left: 10px;width: 127px;',
                            ],
                            'renderTabContent' => false,
                            'items' => $tabItems,
                        ]); ?>
                    </div>
                    <div class="col-xs-12 pad5 pre-view-table">
                        <div class="col-xs-12 pad3"
                             style="height: <?= $activeTab == LogisticsRequest::TYPE_CARRIER ? '80px' : '40px'; ?>;">
                            <div class="col-xs-8 pad0 font-bold" style="height: inherit">
                                <div style="padding-top: 12px !important;">
                                    <p style="font-size: 17px">
                                        <?= $activeTab == LogisticsRequest::TYPE_CARRIER ?
                                            $model->company->getTitle(true) : $model->customer->getNameWithType(); ?>
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-4 pad3 v_middle" style="height: inherit; text-align: right;">
                                <?php if ($activeTab == LogisticsRequest::TYPE_CARRIER && !$model->is_own): ?>
                                    <div>
                                        <?php if ($logoLink): ?>
                                            <img style="max-height: 70px;max-width: 170px;" src="<?= $logoLink ?>"
                                                 alt="">
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="tab-content">
                            <?php if (!$model->is_own): ?>
                                <div id="carrier-tab"
                                     class="tab-pane <?= $activeTab == LogisticsRequest::TYPE_CARRIER ? 'active' : null; ?>">
                                    <?= $this->render('view/main_info', [
                                        'model' => $model,
                                        'type' => $activeTab,
                                    ]); ?>
                                </div>
                            <?php endif; ?>
                            <div id="customer-tab"
                                 class="tab-pane <?= $activeTab == LogisticsRequest::TYPE_CUSTOMER ? 'active' : null; ?>">
                                <?= $this->render('view/main_info', [
                                    'model' => $model,
                                    'type' => $activeTab,
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-lg-5 pad0 pull-right" style="padding-bottom: 5px !important; max-width: 480px;">
                <div class="col-xs-12" style="padding-right:0px !important;">
                    <div class="control-panel col-xs-12 pad0 pull-right">
                        <div class="status-panel col-xs-12 pad0">
                            <div class="col-xs-12 col-sm-3 pad3">
                                <div class="btn full_w marg <?= $model->getStatusColor(); ?>"
                                     style="padding-left:0; padding-right:0;text-align: center;"
                                     title="Дата изменения статуса">
                                    <?= date("d.m.Y", $model->status_updated_at); ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-9 pad0">
                                <div class="col-xs-7 pad3">
                                    <div class="btn full_w marg <?= $model->getStatusColor(); ?>" title="Статус">
                                        <span class="icon pull-left <?= $model->getStatusIcon(); ?>"></span>
                                        <?= $model->status->name; ?>
                                    </div>
                                </div>
                                <div class="col-xs-5 pad3">
                                    <div class="btn full_w marg <?= $model->getStatusColor(); ?>"
                                         style="padding-left:0; padding-right:0;text-align: center;">
                                        <i class="fa fa-rub"></i>
                                        <?= $activeTab == LogisticsRequest::TYPE_CARRIER ?
                                            $model->carrier_rate : $model->customer_rate; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 pad0" style="padding-bottom: 5px !important;">
                            <div class="col-xs-3"></div>
                            <div class="col-xs-9 pad3" style="padding-top: 0!important; margin-top: -3px; ">
                                <div class="document-panel btn-group tooltip2"
                                     style="width: 99.9% !important; margin-bottom: 0;"
                                     data-tooltip-content="#status-block-tooltip">
                                    <div class="clearfix">
                                        <?= Html::a('<i class="pull-left icon fa fa-plus-circle"></i> ' . $createInvoiceText, '#', [
                                            'class' => 'btn yellow full_w',
                                            'title' => 'Добавить Счет',
                                            'style' => 'height: 34px;margin-left: 0; text-transform: uppercase;',
                                        ]); ?>
                                    </div>
                                    <div class="clearfix">
                                        <?= Html::a('<i class="pull-left icon fa fa-plus-circle"></i> Акт', '#', [
                                            'class' => 'btn yellow full_w disabled',
                                            'title' => 'Добавить Акт',
                                            'disabled' => true,
                                            'style' => 'height: 34px;margin-left: 0; text-transform: uppercase;background-color: #a2a2a2;',
                                        ]); ?>
                                    </div>
                                    <div class="clearfix">
                                        <?= Html::a('<i class="pull-left icon fa fa-plus-circle"></i> Упд', '#', [
                                            'class' => 'btn yellow full_w disabled',
                                            'title' => 'Добавить УПД',
                                            'disabled' => true,
                                            'style' => 'height: 34px;margin-left: 0; text-transform: uppercase;background-color: #a2a2a2;',
                                        ]); ?>
                                    </div>
                                    <?php if ($model->company->companyTaxationType->osno): ?>
                                        <div class="clearfix">
                                            <?= Html::a('<i class="pull-left icon fa fa-plus-circle"></i> Счет-фактура', '#', [
                                                'class' => 'btn yellow full_w disabled',
                                                'title' => 'Добавить СФ',
                                                'disabled' => true,
                                                'style' => 'height: 34px;margin-left: 0; text-transform: uppercase;background-color: #a2a2a2;',
                                            ]); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-9 pull-right pad3" style="max-width: 720px;">
                            <div class="portlet">
                                <div class="customer-info" style="min-height:auto !important">
                                    <div class="portlet-body no_mrg_bottom main_inf_no-bord">
                                        <table class="table no_mrg_bottom">
                                            <tr>
                                                <td>
                                            <span class="customer-characteristic" style="color: #000;">
                                                Заказчик:
                                            </span>
                                                    <span>
                                                <?= Html::a($model->customer->getNameWithType(),
                                                    Url::to(['/contractor/view', 'id' => $model->customer_id, 'type' => $model->customer->type])); ?>
                                            </span>
                                                </td>
                                            </tr>
                                            <?php if ($model->customerAgreement): ?>
                                                <tr>
                                                    <td>
                                            <span class="customer-characteristic" style="color: #000;">
                                                Договор с заказчиком №:
                                            </span>
                                                        <span style="color: #000;">
                                                <?= $model->customer_agreement_document_number; ?> от
                                                            <?= DateHelper::format($model->customer_agreement_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                            </span>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>
                                            <?php if (!$model->is_own): ?>
                                                <tr>
                                                    <td>
                                                    <span class="customer-characteristic" style="color: #000;">
                                                        Перевозчик:
                                                    </span>
                                                        <span>
                                                        <?= Html::a($model->carrier->getNameWithType(),
                                                            Url::to(['/contractor/view', 'id' => $model->carrier_id, 'type' => $model->carrier->type])); ?>
                                                    </span>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>
                                            <?php if ($model->carrierAgreement && !$model->is_own): ?>
                                                <tr>
                                                    <td>
                                                        <span class="customer-characteristic" style="color: #000;">
                                                            Договор с перевозчиком №:
                                                        </span>
                                                        <span style="color: #000;">
                                                            <?= $model->carrier_agreement_document_number; ?> от
                                                            <?= DateHelper::format($model->carrier_agreement_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                                        </span>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>
                                            <tr>
                                                <td>
                                                    <span class="customer-characteristic" style="color: #000;">
                                                        ТС:
                                                    </span>
                                                    <span style="color: #000;">
                                                        <?= Html::a($model->vehicle->model . ', ' . $model->vehicle->state_number,
                                                            Url::to(['/logistics/vehicle/view', 'id' => $model->vehicle_id])); ?>
                                                        ,
                                                        <?php if ($model->vehicle->vehicle_type_id == VehicleType::TYPE_TRACTOR && $model->vehicle->semitrailerType): ?>
                                                            <?= Html::a($model->vehicle->semitrailerType->state_number,
                                                                Url::to(['/logistics/vehicle/view', 'id' => $model->vehicle->semitrailerType->id])); ?>
                                                        <?php elseif ($model->vehicle->vehicle_type_id == VehicleType::TYPE_WAGON && $model->vehicle->trailerType): ?>
                                                            <?= Html::a($model->vehicle->trailerType->state_number,
                                                                Url::to(['/logistics/vehicle/view', 'id' => $model->vehicle->trailerType->id])); ?>
                                                        <?php endif; ?>
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                            <span class="customer-characteristic" style="color: #000;">
                                                Водитель:
                                            </span>
                                                    <span>
                                                <?= Html::a($model->driver->getFio(), Url::to(['/logistics/driver/view', 'id' => $model->driver_id,])); ?>
                                            </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                            <span class="customer-characteristic" style="color: #000;">
                                                Ответственный:
                                            </span>
                                                    <span style="color: #000;">
                                                <?= $model->author->getFio(); ?>
                                            </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div style="margin-bottom: 5px;color: #000;">
                                                        <?= FileUpload::widget([
                                                            'uploadUrl' => Url::to(['file-upload', 'id' => $model->id,]),
                                                            'deleteUrl' => Url::to(['file-delete', 'id' => $model->id,]),
                                                            'listUrl' => Url::to(['file-list', 'id' => $model->id,]),
                                                        ]); ?>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?= $this->render('view/action_buttons', [
            'model' => $model,
            'type' => $activeTab,
        ]); ?>
    </div>
<?= $this->render('view/modal_change_status', [
    'model' => $model,
]); ?>
<?= $this->render('@frontend/modules/documents/views/invoice/view/_send_message', [
    'model' => $model,
    'useContractor' => null,
    'requestType' => $activeTab,
]); ?>
<?php $this->registerJs('
    $(document).on("click", ".dropdown-menu_request li", function () {
        var $submitStatus = $("#change-status.modal .change-status_submit");
        $("#change-status.modal .status-name").text($(this).find("a").text().trim());
        $submitStatus.attr("data-url", $submitStatus.attr("data-url") + $(this).find("a").data("id"));
    });
    
    $(document).on("click", "#change-status.modal .change-status_submit", function (e) {
        $.post($(this).data("url"), {date: $("#change-status.modal .modal-document-date").val()}, function (data) {
        });
    });
'); ?>