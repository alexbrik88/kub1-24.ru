<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.12.2018
 * Time: 20:15
 */

use common\components\helpers\Html;
use yii\helpers\Url;
use common\components\grid\GridView;
use common\components\grid\DataColumn;
use common\models\logisticsRequest\LogisticsRequest;
use common\components\date\DateHelper;
use common\components\grid\DropDownSearchDataColumn;
use yii\widgets\ActiveForm;
use yii\bootstrap\Dropdown;
use frontend\widgets\TableConfigWidget;

/* @var $this yii\web\View
 * @var $searchModel \frontend\modules\logistics\models\RequestSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $company \common\models\Company
 */

$userConfig = Yii::$app->user->identity->config;
$this->title = 'Заявки';
?>
<div class="portlet box">
    <div class="btn-group pull-right title-buttons">
        <?= Html::a('<i class="fa fa-plus"></i> ДОБАВИТЬ', Url::to(['create']), [
            'class' => 'btn yellow',
        ]); ?>
    </div>
    <h3 class="page-title"><?= $this->title; ?></h3>
</div>
<div class="row" id="widgets">
    <div class="col-md-12 col-sm-12">
        <div class="table-icons">
            <?= TableConfigWidget::widget([
                'items' => [
                    [
                        'attribute' => 'logistics_request_unloading_date',
                    ],
                    [
                        'attribute' => 'logistics_request_goods',
                    ],
                    [
                        'attribute' => 'logistics_request_customer_debt',
                    ],
                    [
                        'attribute' => 'logistics_request_carrier_debt',
                    ],
                    [
                        'attribute' => 'logistics_request_ttn',
                    ],
                    [
                        'attribute' => 'logistics_request_author_id',
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Список заявок: <?= $dataProvider->totalCount; ?>
        </div>
        <div class="tools search-tools tools_button col-sm-4">
            <div class="form-body">
                <?php $form = ActiveForm::begin([
                    'method' => 'GET',
                    'action' => Url::current([$searchModel->formName() => null]),
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'options' => [
                            'class' => '',
                        ],
                    ],
                ]); ?>
                <div class="search_cont">
                    <div class="wimax_input" style="width: 100%!important;padding-right: 20px;float: right;">
                        <?= $form->field($searchModel, 'search')->textInput([
                            'placeholder' => 'Поиск...',
                        ]); ?>
                    </div>
                    <div class="wimax_button">
                        <?= Html::submitButton('НАЙТИ', [
                            'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                        ]) ?>
                    </div>
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
        <div class="actions joint-operations col-sm-4" style="display:none;">
            <div class="dropdown">
                <?= Html::a('Еще  <span class="caret"></span>', null, [
                    'class' => 'btn btn-default btn-sm dropdown-toggle',
                    'id' => 'dropdownMenu1',
                    'data-toggle' => 'dropdown',
                    'aria-expanded' => true,
                    'style' => 'height: 28px;',
                ]); ?>
                <?= Dropdown::widget([
                    'items' => [
                        [
                            'label' => 'Выставить Счет',
                            'url' => '#many-create-invoice',
                            'linkOptions' => [
                                'data-toggle' => 'modal',
                            ],
                        ],
                        [
                            'label' => 'Создать ТТН',
                            'url' => '#many-create-ttn',
                            'linkOptions' => [
                                'data-toggle' => 'modal',
                            ],
                        ],
                    ],
                    'options' => [
                        'style' => 'right: -30px; left: auto; top: 28px;',
                        'aria-labelledby' => 'dropdownMenu1',
                    ],
                ]); ?>
            </div>
            <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                'class' => 'btn btn-default btn-sm',
                'data-toggle' => 'modal',
            ]); ?>
            <div id="many-delete" class="confirm-modal fade modal" role="dialog"
                 tabindex="-1" aria-hidden="true"
                 style="display: none; margin-top: -51.5px;">
                <div class="modal-dialog ">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="form-body">
                                <div class="row">Вы уверены, что хотите удалить выбранные заявки?
                                </div>
                            </div>
                            <div class="form-actions row">
                                <div class="col-xs-6">
                                    <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
                                        'class' => 'btn darkblue pull-right modal-many-delete ladda-button',
                                        'data-url' => Url::to(['many-delete',]),
                                        'data-style' => 'expand-right',
                                    ]); ?>
                                </div>
                                <div class="col-xs-6">
                                    <button type="button" class="btn darkblue" data-dismiss="modal">
                                        НЕТ
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?= Html::a('Статус', '#status', [
                'class' => 'btn btn-default btn-sm',
                'data-toggle' => 'modal',
            ]); ?>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-container clearfix" style="overflow-y: auto;">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'dataColumnClass' => DataColumn::className(),
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'columns' => [
                    [
                        'header' => Html::checkbox('', false, [
                            'class' => 'joint-operation-main-checkbox',
                        ]),
                        'headerOptions' => [
                            'class' => 'text-center pad0',
                            'width' => '5%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-center pad0-l pad0-r',
                        ],
                        'format' => 'raw',
                        'value' => function (LogisticsRequest $model) {
                            return Html::checkbox('LogisticsRequest[' . $model->id . '][checked]', false, [
                                'class' => 'joint-operation-checkbox',
                            ]);
                        },
                    ],
                    [
                        'attribute' => 'document_number',
                        'label' => '№№',
                        'contentOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (LogisticsRequest $model) {
                            return Html::a($model->document_number, ['view', 'id' => $model->id]);
                        },
                    ],
                    [
                        'attribute' => 'document_date',
                        'label' => 'Дата',
                        'contentOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (LogisticsRequest $model) {
                            return DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                        },
                    ],
                    [
                        'attribute' => 'loading.date',
                        'label' => 'Дата погрузки',
                        'contentOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (LogisticsRequest $model) {
                            return $model->loading->date ?
                                DateHelper::format($model->loading->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) :
                                '';
                        },
                    ],
                    [
                        'attribute' => 'unloading.date',
                        'label' => 'Дата разгрузки',
                        'headerOptions' => [
                            'class' => 'col_logistics_request_unloading_date sorting' . ($userConfig->logistics_request_unloading_date ? '' : ' hidden'),
                        ],
                        'contentOptions' => [
                            'class' => 'col_logistics_request_unloading_date' . ($userConfig->logistics_request_unloading_date ? '' : ' hidden'),
                        ],
                        'format' => 'raw',
                        'value' => function (LogisticsRequest $model) {
                            return $model->unloading->date ?
                                DateHelper::format($model->unloading->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) :
                                '';
                        },
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'status_id',
                        'label' => 'Статус',
                        'contentOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getStatusFilter(),
                        'value' => function (LogisticsRequest $model) {
                            return $model->status ? $model->status->name : '';
                        },
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'customer_id',
                        'label' => 'Заказчик',
                        'contentOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getCustomerFilter(),
                        'value' => function (LogisticsRequest $model) {
                            return $model->customer ?
                                Html::a($model->customer->getNameWithType(), Url::to(['/contractor/view', 'id' => $model->customer_id, 'type' => $model->customer->type])) :
                                '';
                        },
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'carrier_id',
                        'label' => 'Перевозчик',
                        'contentOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getCarrierFilter(),
                        'value' => function (LogisticsRequest $model) {
                            return $model->carrier ?
                                Html::a($model->carrier->getNameWithType(), Url::to(['/contractor/view', 'id' => $model->carrier_id, 'type' => $model->carrier->type])) :
                                '';
                        },
                    ],
                    [
                        'attribute' => 'route',
                        'label' => 'Маршрут',
                        'contentOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (LogisticsRequest $model) {
                            return $model->loading->city . ' - ' . $model->unloading->city;
                        },
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'driver_id',
                        'label' => 'Водитель',
                        'contentOptions' => [
                            'class' => 'dropdown-filter',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getDriverFilter(),
                        'value' => function (LogisticsRequest $model) {
                            return $model->driver ?
                                Html::a($model->driver->getFio(), Url::to(['/logistics/driver/view', 'id' => $model->driver_id,])) :
                                '';
                        },
                    ],
                    [
                        'attribute' => 'goods_name',
                        'label' => 'Груз',
                        'headerOptions' => [
                            'class' => 'sorting col_logistics_request_goods' . ($userConfig->logistics_request_goods ? '' : ' hidden'),
                        ],
                        'contentOptions' => [
                            'class' => 'col_logistics_request_goods' . ($userConfig->logistics_request_goods ? '' : ' hidden'),
                        ],
                        'format' => 'raw',
                        'value' => function (LogisticsRequest $model) {
                            return $model->goods_name ?: '';
                        },
                    ],
                    [
                        'attribute' => 'customerDolg',
                        'label' => 'Долг заказчику',
                        'headerOptions' => [
                            'class' => 'col_logistics_request_customer_debt' . ($userConfig->logistics_request_customer_debt ? '' : ' hidden'),
                        ],
                        'contentOptions' => [
                            'class' => 'col_logistics_request_customer_debt' . ($userConfig->logistics_request_customer_debt ? '' : ' hidden'),
                        ],
                        'format' => 'raw',
                        'value' => function (LogisticsRequest $model) {
                            return '???';
                        },
                    ],
                    [
                        'attribute' => 'carrierDolg',
                        'label' => 'Долг перевозчику',
                        'headerOptions' => [
                            'class' => 'col_logistics_request_carrier_debt' . ($userConfig->logistics_request_carrier_debt ? '' : ' hidden'),
                        ],
                        'contentOptions' => [
                            'class' => 'col_logistics_request_carrier_debt' . ($userConfig->logistics_request_carrier_debt ? '' : ' hidden'),
                        ],
                        'format' => 'raw',
                        'value' => function (LogisticsRequest $model) {
                            return '???';
                        },
                    ],
                    [
                        'attribute' => 'ttn',
                        'label' => 'ТТН',
                        'headerOptions' => [
                            'class' => 'col_logistics_request_ttn' . ($userConfig->logistics_request_ttn ? '' : ' hidden'),
                        ],
                        'contentOptions' => [
                            'class' => 'col_logistics_request_ttn' . ($userConfig->logistics_request_ttn ? '' : ' hidden'),
                        ],
                        'format' => 'raw',
                        'value' => function (LogisticsRequest $model) {
                            return '???';
                        },
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'author_id',
                        'label' => 'Ответственный',
                        'headerOptions' => [
                            'class' => 'col_logistics_request_author_id' . ($userConfig->logistics_request_author_id ? '' : ' hidden'),
                        ],
                        'contentOptions' => [
                            'class' => 'col_logistics_request_author_id dropdown-filter' . ($userConfig->logistics_request_author_id ? '' : ' hidden'),
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getAuthorFilter(),
                        'value' => function (LogisticsRequest $model) {
                            return $model->author->getFio();
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
