<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 10.12.2018
 * Time: 16:13
 */

use common\components\image\EasyThumbnailImage;
use common\models\logisticsRequest\LogisticsRequest;
use common\components\date\DateHelper;
use common\models\vehicle\VehicleType;
use common\components\TextHelper;

/* @var $this yii\web\View */
/* @var $model LogisticsRequest */
/* @var $type integer */

$this->title = $model->getPrintTitle();
$this->context->layoutWrapperCssClass = 'out-invoice out-document';

$logoLink = !$model->company->logo_link ? null :
    EasyThumbnailImage::thumbnailSrc($model->company->getImage('logoImage'), 150, 90, EasyThumbnailImage::THUMBNAIL_INSET);

$printLink = !$model->company->print_link ? null :
    EasyThumbnailImage::thumbnailSrc($model->company->getImage('printImage'), 130, 130, EasyThumbnailImage::THUMBNAIL_INSET);

if ($type == LogisticsRequest::TYPE_CARRIER) {
    $contractor = $model->carrier;
    $agreement = 'carrierAgreement';
    $prefix = 'carrier';
    $leftPrintLink = '195px';
} else {
    $contractor = $model->customer;
    $agreement = 'customerAgreement';
    $prefix = 'customer';
    $leftPrintLink = '530px';
}
$printLinkStyle = $printLink ? " background: url($printLink); background-repeat: no-repeat; background-position: {$leftPrintLink} 0px;" : '';
$contactPersonAttribute = $prefix . '_contact_person';
$formID = $prefix . '_form_id';
$form = $prefix . 'Form';
$conditionID = $prefix . '_condition_id';
$condition = $prefix . 'Condition';
$delay = $prefix . '_delay';
$rate = $prefix . '_rate';
$essence = $prefix . '_request_essence';

$directorName = null;
if (!empty($contractor->director_name) && $contractor->director_name != 'ФИО Руководителя') {
    $directorName = TextHelper::nameShort($contractor->director_name);
}
?>
<style type="text/css">
    <?php if (is_file($file = \Yii::getAlias('@frontend/web/css/print/common.css'))) {
        echo $this->renderFile($file);
    }
    if (is_file($file = \Yii::getAlias('@frontend/web/css/print/documents-invoice.css'))) {
        echo $this->renderFile($file);
    } ?>
</style>
<div class="page-content-in p-center pad-pdf-p" style="box-sizing: content-box;">
    <table class="no-b m-t" style="width: 100%; margin-top: 0;">
        <tr>
            <td class="font-bold print-title" style="padding-left: 0;padding-bottom: 12px;">
                <?php if ($type == LogisticsRequest::TYPE_CARRIER): ?>
                    <?= $model->company->getTitle(true); ?>
                <?php else: ?>
                    <?= $model->customer->getNameWithType(); ?>
                <?php endif; ?>
            </td>
            <td style="width: 325px; text-align: right">
                <?php if ($logoLink && $type == LogisticsRequest::TYPE_CARRIER) : ?>
                    <img class="pull-right" src="<?= $logoLink ?>" alt="">
                <?php endif; ?>
            </td>
        </tr>
    </table>
    <h3 style="font-size: 12pt;">
        <?= $model->getTitle($type); ?>
    </h3>
    <table class="t-p m-t-m" style="width: 100%; border: none">
        <tr>
            <td width="50%" style="border-bottom: none">
                <b>Заказчик</b>
            </td>
            <td width="50%" style="border-bottom: none;">
                <b>Исполнитель</b>
            </td>
        </tr>
        <tr>
            <td width="50%" style="border-bottom: none;border-top: none;">
                <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    $model->company->getTitle(true) :
                    $contractor->getNameWithType(); ?>
            </td>
            <td width="50%" style="border-bottom: none;border-top: none;">
                <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    $contractor->getNameWithType() :
                    $model->company->getTitle(true); ?>
            </td>
        </tr>
        <tr>
            <td width="50%" style="border-bottom: none;border-top: none;">
                тел: <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    $model->company->phone :
                    $contractor->getRealContactPhone(); ?>
            </td>
            <td width="50%" style="border-bottom: none;border-top: none;">
                тел: <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    $contractor->getRealContactPhone() :
                    $model->company->phone; ?>
            </td>
        </tr>
        <tr>
            <td width="50%" style="border-bottom: none;border-top: none;">
                ФИО: <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    $model->company->getChiefFio(true) :
                    $contractor->getDirectorFio(); ?>
            </td>
            <td width="50%" style="border-bottom: none;border-top: none;">
                ФИО: <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    $contractor->getDirectorFio() :
                    $model->company->getChiefFio(true); ?>
            </td>
        </tr>
    </table>
    <table class="t-p m-t-m" style="width: 100%; border: none">
        <tr>
            <td width="25%" style="border-bottom: none;">
                <b>Маршрут</b>
            </td>
            <td width="75%" style="border-bottom: none;">
                <?= $model->loading->city . ' - ' . $model->unloading->city; ?>
            </td>
        </tr>
    </table>
    <table class="t-p m-t-m" style="width: 100%; border: none">
        <tr>
            <td width="25%" style="border-bottom: none;">
                <b>Погрузка</b>
            </td>
            <td width="75%" style="border-bottom: none;">
                <?= ($model->loading->date ?
                    DateHelper::format($model->loading->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) :
                    null) . ' ' .
                ($model->loading->time ?
                    DateHelper::format($model->loading->time, 'H:i', 'H:i:s') : null); ?>
            </td>
        </tr>
    </table>
    <table class="t-p m-t-m" style="width: 100%; border: none">
        <tr>
            <td width="25%" style="border-bottom: none;">
                Адрес
            </td>
            <td width="75%" style="border-bottom: none;">
                <?= $model->loading->city . ' ' . $model->loading->address; ?>
            </td>
        </tr>
    </table>
    <table class="t-p m-t-m" style="width: 100%; border: none">
        <tr>
            <td width="25%" style="border-bottom: none;">
                Контактное лицо
            </td>
            <td width="25%" style="border-bottom: none;">
                <?= $model->loading->contact_person; ?>
            </td>
            <td width="25%" style="border-bottom: none;">
                Телефон
            </td>
            <td width="25%" style="border-bottom: none;">
                <?= $model->loading->contact_person_phone; ?>
            </td>
        </tr>
    </table>
    <table class="t-p m-t-m" style="width: 100%; border: none">
        <tr>
            <td width="25%" style="border-bottom: none;">
                <b>Разгрузка</b>
            </td>
            <td width="75%" style="border-bottom: none;">
                <?= ($model->unloading->date ?
                    DateHelper::format($model->unloading->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) :
                    null) . ' ' .
                ($model->unloading->time ?
                    DateHelper::format($model->unloading->time, 'H:i', 'H:i:s') : null); ?>
            </td>
        </tr>
    </table>
    <table class="t-p m-t-m" style="width: 100%; border: none">
        <tr>
            <td width="25%" style="border-bottom: none;">
                Адрес
            </td>
            <td width="75%" style="border-bottom: none;">
                <?= $model->unloading->city . ' ' . $model->unloading->address; ?>
            </td>
        </tr>
    </table>
    <table class="t-p m-t-m" style="width: 100%; border: none">
        <tr>
            <td width="25%" style="border-bottom: none;">
                Контактное лицо
            </td>
            <td width="25%" style="border-bottom: none;">
                <?= $model->unloading->contact_person; ?>
            </td>
            <td width="25%" style="border-bottom: none;">
                Телефон
            </td>
            <td width="25%" style="border-bottom: none;">
                <?= $model->unloading->contact_person_phone; ?>
            </td>
        </tr>
    </table>
    <table class="t-p m-t-m" style="width: 100%; border: none">
        <tr>
            <td width="25%" rowspan="2" style="border-bottom: none;">
                <b>Груз</b>
            </td>
            <td width="75%" colspan="3" style="border-bottom: none;">
                <?= $model->goods_name; ?>
            </td>
        </tr>
        <tr>
            <td width="25%" style="border-bottom: none;">
                <b>Вес (тонн):</b> <?= $model->goods_weight; ?>
            </td>
            <td width="25%" style="border-bottom: none;">
                <b>Габариты:</b> <?= "{$model->goods_length}x{$model->goods_width}x{$model->goods_height}=" .
                $model->goods_length * $model->goods_width * $model->goods_height . " м3"; ?>
            </td>
            <td width="25%" style="border-bottom: none;">
                <b>Кол-во мест:</b> <?= $model->goods_count; ?>
            </td>
        </tr>
    </table>
    <table class="t-p m-t-m" style="width: 100%; border: none">
        <tr>
            <td width="25%" style="border-bottom: none;">
                Способ погрузки
            </td>
            <td width="25%" style="border-bottom: none;">
                <?= $model->loading->method; ?>
            </td>
            <td width="25%" style="border-bottom: none;">
                Способ разгрузки
            </td>
            <td width="25%" style="border-bottom: none;">
                <?= $model->unloading->method; ?>
            </td>
        </tr>
    </table>
    <table class="t-p m-t-m" style="width: 100%; border: none">
        <tr>
            <td width="25%" rowspan="2" style="border-bottom: none;">
                <b>Стоимость услуги</b>
            </td>
            <td width="75%" colspan="3" style="border-bottom: none;">
                <b><?= $model->$rate; ?></b>
            </td>
        </tr>
        <tr>
            <td width="25%" style="border-bottom: none;">
                <b><?= $model->$formID ? $model->$form->name : null; ?></b>
            </td>
            <td width="25%" style="border-bottom: none;">
                <b><?= $model->$conditionID ? $model->$condition->name : null; ?></b>
            </td>
            <td width="25%" style="border-bottom: none;">
                <b>
                    <?php if ($model->$delay): ?>
                        Через <?= $model->$delay; ?>
                        <?php if ($model->$delay == 1): ?>
                            банковский день
                        <?php elseif ($model->$delay > 1 && $model->$delay < 5): ?>
                            банковских дня
                        <?php else: ?>
                            банковских дней
                        <?php endif; ?>
                    <?php endif; ?>
                </b>
            </td>
        </tr>
    </table>
    <?php $hasTrailer = ($model->vehicle->vehicle_type_id == VehicleType::TYPE_TRACTOR && $model->vehicle->semitrailerType) ||
        ($model->vehicle->vehicle_type_id == VehicleType::TYPE_WAGON && $model->vehicle->trailerType); ?>
    <table class="t-p m-t-m" style="width: 100%; border: none">
        <tr>
            <td width="25%" rowspan="<?= $hasTrailer ? 3 : 2; ?>" style="border-bottom: none;">
                <b>Транспортное средство</b>
            </td>
            <td width="25%" style="border-bottom: none;">
                <?= $model->vehicle->vehicleType->name; ?>
            </td>
            <td width="25%" style="border-bottom: none;">
                <?= $model->vehicle->model; ?>
            </td>
            <td width="25%" style="border-bottom: none;">
                <?= $model->vehicle->state_number; ?>
            </td>
        </tr>
        <?php if ($model->vehicle->vehicle_type_id == VehicleType::TYPE_TRACTOR && $model->vehicle->semitrailerType): ?>
            <tr>
                <td width="25%" style="border-bottom: none;">
                    <?= $model->vehicle->semitrailerType->vehicleType->name; ?>
                </td>
                <td width="25%" style="border-bottom: none;">
                    <?= $model->vehicle->semitrailerType->model; ?>
                </td>
                <td width="25%" style="border-bottom: none;">
                    <?= $model->vehicle->semitrailerType->state_number; ?>
                </td>
            </tr>
        <?php elseif ($model->vehicle->vehicle_type_id == VehicleType::TYPE_WAGON && $model->vehicle->trailerType): ?>
            <tr>
                <td width="25%" style="border-bottom: none;">
                    <?= $model->vehicle->trailerType->vehicleType->name; ?>
                </td>
                <td width="25%" style="border-bottom: none;">
                    <?= $model->vehicle->trailerType->model; ?>
                </td>
                <td width="25%" style="border-bottom: none;">
                    <?= $model->vehicle->trailerType->state_number; ?>
                </td>
            </tr>
        <?php endif; ?>
        <tr>
            <td width="25%" style="border-bottom: none;">
                <b>Тип кузова:</b> <?= $model->vehicle->bodyType ? $model->vehicle->bodyType->name : null; ?>
            </td>
            <td width="25%" style="border-bottom: none;">
                <b>Грузоподъемность:</b> <?= $model->vehicle->getTonnageFull(); ?> т.
            </td>
            <td width="25%" style="border-bottom: none;">
                <b>Объем:</b> <?= $model->vehicle->getVolumeFull(); ?> м3
            </td>
        </tr>
    </table>
    <table class="t-p m-t-m" style="width: 100%; border: none">
        <tr>
            <td width="25%" rowspan="2" style="border-bottom: none;">
                <b>Водитель</b>
            </td>
            <td width="50%" style="border-bottom: none;">
                <?= $model->driver->getFio(); ?>
            </td>
            <td width="25%" style="border-bottom: none;">
                <?= $model->driver->mainDriverPhone ? $model->driver->mainDriverPhone->phone : null; ?>
            </td>
        </tr>
        <tr>
            <td width="100%" colspan="2" style="border-bottom: none;">
                <?php $identificationDate = DateHelper::format($model->driver->identification_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                <b><?= $model->driver->identificationType->name ?>:</b>
                <?= "{$model->driver->identification_series}, 
                    {$model->driver->identification_number}, 
                    {$model->driver->identification_issued_by}, 
                    {$identificationDate}"; ?>
            </td>
        </tr>
    </table>
    <table class="t-p m-t-m" style="width: 100%; border: none">
        <tr>
            <td style="border-bottom: none;">
                <?= $model->$essence; ?>
            </td>
        </tr>
    </table>
    <table class="t-p m-t-m" style="width: 100%; border: none">
        <tr>
            <td width="50%" style="border-bottom: none">
                <b>Заказчик</b>
            </td>
            <td width="50%" style="border-bottom: none;">
                <b>Исполнитель</b>
            </td>
        </tr>
        <tr>
            <td width="50%" style="border-bottom: none;border-top: none;">
                <b><?= $type == LogisticsRequest::TYPE_CARRIER ?
                        $model->company->getTitle(true) :
                        $contractor->getNameWithType(); ?></b>
            </td>
            <td width="50%" style="border-bottom: none;border-top: none;">
                <b><?= $type == LogisticsRequest::TYPE_CARRIER ?
                        $contractor->getNameWithType() :
                        $model->company->getTitle(true); ?></b>
            </td>
        </tr>
        <tr>
            <td width="50%" style="border-bottom: none;border-top: none;">
                <b>Юр. адрес:</b>
                <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    $model->company->getAddressActualFull() :
                    $contractor->legal_address; ?>
            </td>
            <td width="50%" style="border-bottom: none;border-top: none;">
                <b>Юр. адрес:</b>
                <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    $contractor->legal_address :
                    $model->company->getAddressActualFull(); ?>
            </td>
        </tr>
        <tr>
            <td width="50%" style="border-bottom: none;border-top: none;">
                <b>Почт. адрес:</b>
                <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    $model->company->getAddressLegalFull() :
                    $contractor->actual_address; ?>
            </td>
            <td width="50%" style="border-bottom: none;border-top: none;">
                <b>Почт. адрес:</b>
                <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    $contractor->actual_address :
                    $model->company->getAddressLegalFull(); ?>
            </td>
        </tr>
        <tr>
            <td width="50%" style="border-bottom: none;border-top: none;">
                <b>ИНН / КПП:</b>
                <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    ($model->company->inn . ' / ' . $model->company->kpp) :
                    ($contractor->ITN . ' / ' . $contractor->PPC); ?>
            </td>
            <td width="50%" style="border-bottom: none;border-top: none;">
                <b>ИНН / КПП:</b>
                <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    ($contractor->ITN . ' / ' . $contractor->PPC) :
                    ($model->company->inn . ' / ' . $model->company->kpp); ?>
            </td>
        </tr>
        <tr>
            <td width="50%" style="border-bottom: none;border-top: none;">
                <b>ОГРН:</b>
                <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    $model->company->ogrn :
                    $contractor->BIN; ?>
            </td>
            <td width="50%" style="border-bottom: none;border-top: none;">
                <b>ОГРН:</b>
                <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    $contractor->BIN :
                    $model->company->ogrn; ?>
            </td>
        </tr>
        <tr>
            <td width="50%" style="border-bottom: none;border-top: none;">
                <b>Р/с:</b>
                <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    ($model->company->mainCheckingAccountant ?
                        ($model->company->mainCheckingAccountant->rs . ' в ' . $model->company->mainCheckingAccountant->bank_name) : null) :
                    ($contractor->current_account ?
                        ($contractor->current_account . ' в ' . $contractor->bank_name) : null); ?>
            </td>
            <td width="50%" style="border-bottom: none;border-top: none;">
                <b>Р/с:</b>
                <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    ($contractor->current_account ?
                        ($contractor->current_account . ' в ' . $contractor->bank_name) : null) :
                    ($model->company->mainCheckingAccountant ?
                        ($model->company->mainCheckingAccountant->rs . ' в ' . $model->company->mainCheckingAccountant->bank_name) : null); ?>
            </td>
        </tr>
        <tr>
            <td width="50%" style="border-bottom: none;border-top: none;">
                <b>Кор/с:</b>
                <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    $model->company->mainCheckingAccountant->ks :
                    $contractor->corresp_account; ?>
            </td>
            <td width="50%" style="border-bottom: none;border-top: none;">
                <b>Кор/с:</b>
                <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    $contractor->corresp_account :
                    $model->company->mainCheckingAccountant->ks; ?>
            </td>
        </tr>
        <tr>
            <td width="50%" style="border-bottom: none;border-top: none;">
                <b>БИК:</b>
                <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    $model->company->mainCheckingAccountant->bik :
                    $contractor->BIC; ?>
            </td>
            <td width="50%" style="border-bottom: none;border-top: none;">
                <b>БИК:</b>
                <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    $contractor->BIC :
                    $model->company->mainCheckingAccountant->bik; ?>
            </td>
        </tr>
        <tr>
            <td width="50%" style="border-bottom: none;border-top: none;">
                <b>Тел:</b>
                <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    $model->company->phone :
                    $contractor->getRealContactPhone(); ?>
            </td>
            <td width="50%" style="border-bottom: none;border-top: none;">
                <b>Тел:</b>
                <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    $contractor->getRealContactPhone() :
                    $model->company->phone; ?>
            </td>
        </tr>
        <tr>
            <td width="50%" style="border-bottom: none;border-top: none;">
                <b>Email:</b>
                <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    $model->company->email :
                    $contractor->getRealContactEmail(); ?>
            </td>
            <td width="50%" style="border-bottom: none;border-top: none;">
                <b>Email:</b>
                <?= $type == LogisticsRequest::TYPE_CARRIER ?
                    $contractor->getRealContactEmail() :
                    $model->company->email; ?>
            </td>
        </tr>
    </table>
    <table class="t-p m-t-m" style="width: 100%; border: none">
        <tr>
            <td width="50%" style="border-bottom: none">
                <b>Генеральный директор</b>
            </td>
            <td width="50%" style="border-bottom: none;">
                <b>Генеральный директор</b>
            </td>
        </tr>
    </table>
    <div style="<?= $printLinkStyle; ?>height: 150px;">
        <table class="t-p m-t-m" style="width: 100%; border: none;border-left: 1px solid; border-right: 1px solid;">
            <tr>
                <td width="3%" style="border: none;height: 73px;"></td>
                <td width="27%"
                    style="height: 34px;border: none;border-bottom: 1px solid #000;text-align: right;vertical-align: bottom;">
                </td>
                <td width="20%"
                    style="height: 34px; border: none;border-right: 1px solid #000;text-align: left; vertical-align: bottom;">
                    <?= $type == LogisticsRequest::TYPE_CARRIER ?
                        ('/ ' . $model->company->getChiefFio(true) . ' /') :
                        (!empty($directorName) ? '/ ' . $directorName . ' /' : ''); ?>
                </td>
                <td width="3%" style="border: none;"></td>
                <td width="27%"
                    style="height: 34px;border: none;border-bottom: 1px solid #000;text-align: right;vertical-align: bottom;">
                </td>
                <td width="20%"
                    style="height: 34px; border: none;text-align: left; vertical-align: bottom;">
                    <?= $type == LogisticsRequest::TYPE_CARRIER ?
                        (!empty($directorName) ? '/ ' . $directorName . ' /' : '') :
                        ('/ ' . $model->company->getChiefFio(true) . ' /'); ?>
                </td>
            </tr>
        </table>
        <table class="t-p m-t-m" style="width: 100%; border: none;border-left: 1px solid; border-right: 1px solid;border-bottom: 1px solid;">
            <tr>
                <td width="50%" style="border: none;border-right: 1px solid #000;    height: 70px;">
                </td>
                <td style="border: none;"></td>
            </tr>
        </table>
    </div>
</div>


