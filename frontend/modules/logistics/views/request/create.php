<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.12.2018
 * Time: 23:04
 */

/* @var $this yii\web\View */
/* @var $model \common\models\logisticsRequest\LogisticsRequest */
/* @var $company \common\models\Company */
/* @var $logisticsRequestLoading \common\models\logisticsRequest\LogisticsRequestLoadingAndUnloading */
/* @var $logisticsRequestUnloading \common\models\logisticsRequest\LogisticsRequestLoadingAndUnloading */
/* @var $logisticsRequestContractEssenceCustomerAll \common\models\logisticsRequest\LogisticsRequestContractEssence */
/* @var $logisticsRequestContractEssenceCustomerForContractor \common\models\logisticsRequest\LogisticsRequestContractEssence */
/* @var $logisticsRequestContractEssenceCarrierAll \common\models\logisticsRequest\LogisticsRequestContractEssence */
/* @var $logisticsRequestContractEssenceCarrierForContractor \common\models\logisticsRequest\LogisticsRequestContractEssence */

$this->title = 'Добавить заявку';
?>
<div class="logistcis_request-create">
    <?= $this->render('_form', [
        'model' => $model,
        'company' => $company,
        'logisticsRequestLoading' => $logisticsRequestLoading,
        'logisticsRequestUnloading' => $logisticsRequestUnloading,
        'logisticsRequestContractEssenceCustomerAll' => $logisticsRequestContractEssenceCustomerAll,
        'logisticsRequestContractEssenceCustomerForContractor' => $logisticsRequestContractEssenceCustomerForContractor,
        'logisticsRequestContractEssenceCarrierAll' => $logisticsRequestContractEssenceCarrierAll,
        'logisticsRequestContractEssenceCarrierForContractor' => $logisticsRequestContractEssenceCarrierForContractor,
    ]); ?>
</div>
