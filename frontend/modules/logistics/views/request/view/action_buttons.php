<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 07.12.2018
 * Time: 0:54
 */

use common\components\helpers\Html;
use frontend\widgets\ConfirmModalWidget;
use yii\helpers\Url;
use yii\bootstrap\Dropdown;
use common\models\logisticsRequest\LogisticsRequest;
use common\models\logisticsRequest\LogisticsRequestStatus;

/* @var $model LogisticsRequest */
/* @var $status LogisticsRequestStatus */
/* @var $type integer */

$statusItems = [];
foreach (LogisticsRequestStatus::find()->all() as $status) {
    $statusItems[] = [
        'label' => $status->name,
        'encode' => false,
        'url' => $status->id == LogisticsRequestStatus::STATUS_REJECTED ?
            Url::to(['update-status', 'id' => $model->id, 'status' => $status->id]) :
            '#change-status',
        'linkOptions' => [
            'class' => 'change-status-trigger',
            'data-toggle' => $status->id == LogisticsRequestStatus::STATUS_REJECTED ? null : 'modal',
            'data-id' => $status->id,
            'style' => 'text-align: left; padding: 8px 5px;',
        ],
    ];
}
?>
<div id="buttons-bar-fixed">
    <div class="row action-buttons margin-no-icon">
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?= Html::button('Отправить', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs send-message-panel-trigger',
            ]); ?>
            <?= Html::button('<span class="ico-Send-smart-pls fs"></span>', [
                'class' => 'btn darkblue widthe-100 hidden-lg send-message-panel-trigger',
                'title' => 'Отправить',
            ]); ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?= Html::a('Печать', Url::to([
                'document-print',
                'actionType' => 'print',
                'id' => $model->id,
                'type' => $type,
                'filename' => $model->getPrintTitle(),
            ]), [
                'target' => '_blank',
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
            ]); ?>
            <?= Html::a('<i class="fa fa-print fa-2x"></i>', Url::to([
                'document-print',
                'actionType' => 'print',
                'id' => $model->id,
                'type' => $type,
                'filename' => $model->getPrintTitle(),
            ]), [
                'target' => '_blank',
                'title' => 'Печать',
                'class' => 'btn darkblue widthe-100 hidden-lg',
            ]); ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <span class="dropup">
            <?= Html::a('Скачать', '#', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs dropdown-toggle',
                'data-toggle' => 'dropdown'
            ]); ?>
            <?= Html::a('<i class="glyphicon glyphicon-download" style="font-size: 17px;"></i>', '#', [
                'class' => 'btn darkblue widthe-100 hidden-lg dropdown-toggle',
                'data-toggle' => 'dropdown'
            ]); ?>
            <?= Dropdown::widget([
                'options' => [
                    'style' => '',
                    'class' => 'dropdown-menu-mini'
                ],
                'items' => [
                    [
                        'label' => '<span style="display: inline-block;">PDF</span> файл',
                        'encode' => false,
                        'url' => Url::to([
                            'document-print',
                            'actionType' => 'pdf',
                            'id' => $model->id,
                            'type' => $type,
                            'filename' => $model->getPrintTitle(),
                        ]),
                        'linkOptions' => [
                            'target' => '_blank',
                        ]
                    ],
                    [
                        'label' => '<span style="display: inline-block;">Word</span> файл',
                        'encode' => false,
                        'url' => ['docx', 'id' => $model->id, 'type' => $type,],
                        'linkOptions' => [
                            'target' => '_blank',
                        ]
                    ],
                ],
            ]); ?>
        </span>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => 'Копировать',
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ],
                'confirmUrl' => Url::to(['copy', 'id' => $model->id]),
                'message' => 'Вы уверены, что хотите скопировать эту заявку?',
            ]); ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => '<i class="fa fa-files-o fa-2x"></i>',
                    'title' => 'Копировать',
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                ],
                'confirmUrl' => Url::to(['copy', 'id' => $model->id]),
                'message' => 'Вы уверены, что хотите скопировать эту заявку?',
            ]);
            ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <style>
                .dropdown-menu-mini {
                    width: 100%;
                    min-width: 98px;
                    border-color: #4276a4 !important;
                }

                .dropdown-menu-mini a {
                    padding: 7px 0px;
                    text-align: center;
                }
            </style>
            <span class="dropup">
                    <?= Html::a('Статус', '#', [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs dropdown-toggle',
                        'data-toggle' => 'dropdown'
                    ]); ?>
                    <?= Html::a('<i class="glyphicon glyphicon-download" style="font-size: 17px;"></i>', '#', [
                        'class' => 'btn darkblue widthe-100 hidden-lg dropdown-toggle',
                        'data-toggle' => 'dropdown'
                    ]); ?>
                    <?= Dropdown::widget([
                        'options' => [
                            'style' => '',
                            'class' => 'dropdown-menu-mini dropdown-menu_request'
                        ],
                        'items' => $statusItems,
                    ]); ?>
                </span>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?= Html::button('Удалить', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                'data-toggle' => 'modal',
                'href' => '#delete-confirm',
            ]); ?>
            <?= Html::button('<i class="fa fa-trash-o fa-2x"></i>', [
                'class' => 'btn darkblue widthe-100 hidden-lg',
                'data-toggle' => 'modal',
                'href' => '#delete-confirm',
            ]); ?>
            <?= ConfirmModalWidget::widget([
                'options' => [
                    'id' => 'delete-confirm',
                ],
                'toggleButton' => false,
                'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id]),
                'confirmParams' => [],
                'message' => "Вы уверены, что хотите удалить заявку?",
            ]); ?>
        </div>
    </div>
</div>
