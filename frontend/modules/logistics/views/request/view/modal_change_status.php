<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 07.12.2018
 * Time: 0:52
 */

use common\components\helpers\Html;
use yii\helpers\Url;
use common\components\date\DateHelper;

?>
<div id="change-status" class="confirm-modal fade modal in" role="dialog" tabindex="-1" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body">
                    <div class="row">
                        <div class="form-group">
                            <label for="under-date" class="col-md-3 col-sm-3 control-label label-acts-establish1">
                                <span class="status-name"></span> дата:
                            </label>
                            <div class="col-md-3 col-sm-3 inp_one_line_min cal-acts-establish">
                                <div class="input-icon">
                                    <i class="fa fa-calendar"></i>
                                    <?= Html::textInput('date', date(DateHelper::FORMAT_USER_DATE), [
                                        'class' => 'form-control date-picker modal-document-date',
                                        'id' => 'under-date',
                                        'data-date-viewmode' => 'years',
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-3">
                        <?= Html::a('Сохранить', 'javascript:;', [
                            'class' => 'btn darkblue pull-right change-status_submit',
                            'data-url' => Url::to(['update-status', 'id' => $model->id, 'status' => '']),
                            'style' => 'width: 100px;',
                        ]); ?>
                    </div>
                    <div class="col-xs-3"></div>
                    <div class="col-xs-3"></div>
                    <div class="col-xs-3">
                        <?= Html::button('Отменить', [
                            'class' => 'btn darkblue',
                            'data-dismiss' => 'modal',
                            'style' => 'width: 100px;',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
