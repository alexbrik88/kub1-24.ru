<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 05.12.2018
 * Time: 23:28
 */

use \common\models\logisticsRequest\LogisticsRequest;

/* @var $this yii\web\View */
/* @var $model LogisticsRequest */
/* @var $company \common\models\Company */
/* @var $logisticsRequestLoading \common\models\logisticsRequest\LogisticsRequestLoadingAndUnloading */
/* @var $logisticsRequestUnloading \common\models\logisticsRequest\LogisticsRequestLoadingAndUnloading */
/* @var $logisticsRequestContractEssenceCustomerAll \common\models\logisticsRequest\LogisticsRequestContractEssence */
/* @var $logisticsRequestContractEssenceCustomerForContractor \common\models\logisticsRequest\LogisticsRequestContractEssence */
/* @var $logisticsRequestContractEssenceCarrierAll \common\models\logisticsRequest\LogisticsRequestContractEssence */
/* @var $logisticsRequestContractEssenceCarrierForContractor \common\models\logisticsRequest\LogisticsRequestContractEssence */

$this->title = $model->getTitle(LogisticsRequest::TYPE_CUSTOMER);
?>
<div class="logistcis_request-update">
    <?= $this->render('_form', [
        'model' => $model,
        'company' => $company,
        'logisticsRequestLoading' => $logisticsRequestLoading,
        'logisticsRequestUnloading' => $logisticsRequestUnloading,
        'logisticsRequestContractEssenceCustomerAll' => $logisticsRequestContractEssenceCustomerAll,
        'logisticsRequestContractEssenceCustomerForContractor' => $logisticsRequestContractEssenceCustomerForContractor,
        'logisticsRequestContractEssenceCarrierAll' => $logisticsRequestContractEssenceCarrierAll,
        'logisticsRequestContractEssenceCarrierForContractor' => $logisticsRequestContractEssenceCarrierForContractor,
    ]); ?>
</div>