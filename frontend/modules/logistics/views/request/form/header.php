<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.12.2018
 * Time: 23:19
 */

use common\components\helpers\Html;
use common\components\date\DateHelper;

/* @var $this yii\web\View */
/* @var $model \common\models\logisticsRequest\LogisticsRequest */
?>
<div class="portlet-title">
    <div class="caption" style="width: 100%">
        <table cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td valign="middle" style="width:1%; white-space:nowrap;">
                    <span>
                        Договор-Заявка №
                        <?= Html::activeTextInput($model, 'document_number', [
                            'id' => 'account-number',
                            'data-required' => 1,
                            'class' => 'form-control',
                        ]); ?>

                        <br class="box-br">
                    </span>
                    <span class="box-margin-top-t">от</span>
                    <div class="input-icon box-input-icon-top"
                         style="display: inline-block; vertical-align: top; margin-left: 6px; margin-right: 8px;">
                        <i class="fa fa-calendar"></i>
                        <?= Html::activeTextInput($model, 'document_date', [
                            'id' => 'under-date',
                            'class' => 'form-control date-picker',
                            'size' => 16,
                            'data-date' => '12-02-2012',
                            'data-date-viewmode' => 'years',
                            'value' => DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
