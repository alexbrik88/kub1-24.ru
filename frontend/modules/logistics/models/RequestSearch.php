<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.12.2018
 * Time: 19:42
 */

namespace frontend\modules\logistics\models;


use common\components\helpers\ArrayHelper;
use common\models\Contractor;
use common\models\logisticsRequest\LogisticsRequest;
use common\models\logisticsRequest\LogisticsRequestLoadingAndUnloading;
use yii\data\ActiveDataProvider;
use Yii;
use yii\db\ActiveQuery;

/**
 * Class RequestSearch
 * @package frontend\modules\logistics\models
 */
class RequestSearch extends LogisticsRequest
{
    /**
     * @var ActiveQuery
     */
    public $query;

    /**
     * @var
     */
    public $search;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['status_id', 'customer_id', 'carrier_id', 'driver_id', 'author_id',], 'integer'],
            [['search'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /* @var $company \common\models\Company */
        $company = Yii::$app->user->identity->company;
        $this->query = $query = static::find()
            ->joinWith('customer customer')
            ->joinWith('carrier carrier')
            ->leftJoin([
                'loading' => LogisticsRequestLoadingAndUnloading::find()
                    ->andWhere(['type' => LogisticsRequestLoadingAndUnloading::TYPE_LOADING])
            ], '{{logistics_request}}.[[id]] = {{loading}}.[[logistics_request_id]]')
            ->leftJoin([
                'unloading' => LogisticsRequestLoadingAndUnloading::find()
                    ->andWhere(['type' => LogisticsRequestLoadingAndUnloading::TYPE_UNLOADING])
            ], '{{logistics_request}}.[[id]] = {{unloading}}.[[logistics_request_id]]')
            ->andWhere([static::tableName() . '.company_id' => $company->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'document_number',
                    'document_date',
                    'loading.date',
                    'unloading.date',
                    'goods_name',
                    'route' => [
                        'asc' => [
                            'loading.city' => SORT_ASC,
                            'unloading.city' => SORT_ASC,
                        ],
                        'desc' => [
                            'loading.city' => SORT_DESC,
                            'unloading.city' => SORT_ASC,
                        ],
                    ],
                ],
                'defaultOrder' => [
                    'document_number' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        $query->andFilterWhere([static::tableName() . '.status_id' => $this->status_id]);

        $query->andFilterWhere([static::tableName() . '.customer_id' => $this->customer_id]);

        $query->andFilterWhere([static::tableName() . '.carrier_id' => $this->carrier_id]);

        $query->andFilterWhere([static::tableName() . '.driver_id' => $this->driver_id]);

        $query->andFilterWhere([static::tableName() . '.author_id' => $this->author_id]);

        $query->andFilterWhere(['or',
            ['like', 'customer.name', $this->search],
            ['like', 'carrier.name', $this->search],
        ]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getStatusFilter()
    {
        $query = clone $this->query;

        return [null => 'Все'] + ArrayHelper::map($query->groupBy(static::tableName() . '.status_id')->all(), 'status_id', 'status.name');
    }

    /**
     * @return array
     */
    public function getCustomerFilter()
    {
        $query = clone $this->query;
        $idArray = $query->select(static::tableName() . '.customer_id')->column();
        $contractorArray = Contractor::getSorted()->andWhere(['contractor.id' => $idArray])->all();
        $customerFilter = ArrayHelper::map($contractorArray, 'id', 'nameWithType');

        return [null => 'Все'] + $customerFilter;
    }

    /**
     * @return array
     */
    public function getCarrierFilter()
    {
        $query = clone $this->query;
        $idArray = $query->select(static::tableName() . '.carrier_id')->column();
        $contractorArray = Contractor::getSorted()->andWhere(['contractor.id' => $idArray])->all();
        $carrierFilter = ArrayHelper::map($contractorArray, 'id', 'nameWithType');

        return [null => 'Все'] + $carrierFilter;
    }

    /**
     * @return array
     */
    public function getDriverFilter()
    {
        $query = clone $this->query;

        return [null => 'Все'] + ArrayHelper::map($query->groupBy(static::tableName() . '.driver_id')->all(), 'driver_id', 'driver.fio');
    }

    /**
     * @return array
     */
    public function getAuthorFilter()
    {
        $query = clone $this->query;

        return [null => 'Все'] + ArrayHelper::map($query->groupBy(static::tableName() . '.author_id')->all(), 'author_id', 'author.fio');

    }
}