<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 24.11.2018
 * Time: 0:46
 */

namespace frontend\modules\logistics\models;


use common\components\helpers\ArrayHelper;
use common\models\Contractor;
use common\models\vehicle\BodyType;
use common\models\vehicle\Vehicle;
use common\models\vehicle\VehicleType;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * Class VehicleSearch
 * @package frontend\modules\logistics\models
 */
class VehicleSearch extends Vehicle
{
    const MY_VEHICLE = 'my';
    const TYPE_TRACTOR_PLUS_SEMITRAILER = 't_st';
    const TYPE_WAGON_PLUS_TRAILER = 'w_t';

    /**
     * @var
     */
    public $search;
    /**
     * @var
     */
    public $owner;

    /**
     * @var ActiveQuery
     */
    private $query;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['vehicle_type_id', 'body_type_id'], 'integer'],
            [['search', 'owner', 'vehicle_type_id'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /* @var $company \common\models\Company */
        $company = Yii::$app->user->identity->company;
        $this->query = $query = static::find()
            ->select([
                static::tableName() . '.*',
                Vehicle::tableName() . '.tonnage + IFNULL(semiTrailer.tonnage, 0) + IFNULL(trailer.tonnage, 0) as totalTonnage',
                Vehicle::tableName() . '.volume + IFNULL(semiTrailer.volume, 0) + IFNULL(trailer.volume, 0) as totalVolume',
            ])
            ->joinWith('semitrailerType semiTrailer')
            ->joinWith('trailerType trailer')
            ->joinWith('vehicleType')
            ->andWhere([static::tableName() . '.company_id' => $company->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'number',
                    'model',
                    'state_number',
                    'totalTonnage',
                    'totalVolume',
                ],
            ],
        ]);

        $this->load($params);

        $query->andFilterWhere(['like', static::tableName() . '.state_number', $this->search]);

        if ($this->vehicle_type_id == self::TYPE_TRACTOR_PLUS_SEMITRAILER) {
            $query->andWhere(['and',
                [static::tableName() . '.vehicle_type_id' => VehicleType::TYPE_TRACTOR],
                ['not', [static::tableName() . '.semitrailer_type_id' => null]],
                ['not', [static::tableName() . '.semitrailer_type_id' => '']],
            ]);
        } elseif ($this->vehicle_type_id == self::TYPE_WAGON_PLUS_TRAILER) {
            $query->andWhere(['and',
                [static::tableName() . '.vehicle_type_id' => VehicleType::TYPE_WAGON],
                ['not', [static::tableName() . '.trailer_type_id' => null]],
                ['not', [static::tableName() . '.trailer_type_id' => '']],
            ]);
        } else {
            $query->andFilterWhere([static::tableName() . '.vehicle_type_id' => $this->vehicle_type_id]);
        }

        $query->andFilterWhere([static::tableName() . '.body_type_id' => $this->body_type_id]);

        if ($this->owner == self::MY_VEHICLE) {
            $query->andWhere([static::tableName() . '.is_own' => true]);
        } elseif ($this->owner) {
            $query
                ->andWhere([static::tableName() . '.contractor_id' => $this->owner])
                ->andWhere([static::tableName() . '.is_own' => false]);
        }

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getVehicleTypeFilter()
    {
        $query = clone $this->query;

        $vehicleTypeFilter = ArrayHelper::map($query->all(), 'vehicleType.id', 'vehicleType.name');
        if ($query->andWhere([static::tableName() . '.vehicle_type_id' => VehicleType::TYPE_TRACTOR])
            ->andWhere(['and',
                ['not', [static::tableName() . '.semitrailer_type_id' => null]],
                ['not', [static::tableName() . '.semitrailer_type_id' => '']],
            ])->exists()) {
            $vehicleTypeFilter = [self::TYPE_TRACTOR_PLUS_SEMITRAILER => 'Тягач + полуприцеп'] + $vehicleTypeFilter;
        }

        $query = clone $this->query;
        if ($query->andWhere([static::tableName() . '.vehicle_type_id' => VehicleType::TYPE_WAGON])
            ->andWhere(['and',
                ['not', [static::tableName() . '.trailer_type_id' => null]],
                ['not', [static::tableName() . '.trailer_type_id' => '']],
            ])->exists()) {
            $vehicleTypeFilter = [self::TYPE_WAGON_PLUS_TRAILER => 'Фургон + прицеп'] + $vehicleTypeFilter;
        }
        return [null => 'Все'] + $vehicleTypeFilter;
    }

    /**
     * @return array
     */
    public function getBodyTypeFilter()
    {
        $query = clone $this->query;

        return [null => 'Все'] + ArrayHelper::map($query->joinWith('bodyType')->all(), 'bodyType.id', 'bodyType.name');
    }

    /**
     * @return array
     */
    public function getOwnerFilter()
    {
        $query = clone $this->query;
        $idArray = $query->select(static::tableName() . '.contractor_id')->column();
        $contractorArray = Contractor::getSorted()->andWhere(['contractor.id' => $idArray])->all();
        $ownerFilter = ArrayHelper::map($contractorArray, 'id', 'nameWithType');
        if ($query->andWhere([static::tableName() . '.is_own' => true])->exists()) {
            $ownerFilter = [self::MY_VEHICLE => 'Собственное ТС'] + $ownerFilter;
        }

        return ['' => 'Все'] + $ownerFilter;
    }
}