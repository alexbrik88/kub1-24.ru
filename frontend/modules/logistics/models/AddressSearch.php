<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 27.11.2018
 * Time: 14:05
 */

namespace frontend\modules\logistics\models;


use common\components\helpers\ArrayHelper;
use common\models\Contractor;
use common\models\loadingAndUnloadingAddress\LoadingAndUnloadingAddress;
use yii\db\ActiveQuery;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class AddressSearch
 * @package frontend\modules\logistics\models
 */
class AddressSearch extends LoadingAndUnloadingAddress
{
    /**
     * @var
     */
    public $search;

    /**
     * @var
     */
    public $mainContactPerson;

    /**
     * @var ActiveQuery
     */
    private $query;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['search'], 'string'],
            [['address_type', 'contractor_id', 'status'], 'integer'],
            [['mainContactPerson'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /* @var $company \common\models\Company */
        $company = Yii::$app->user->identity->company;
        $this->query = $query = static::find()
            ->joinWith('mainLoadingAndUnloadingAddressContactPerson mainContactPerson')
            ->andWhere([static::tableName() . '.company_id' => $company->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'address',
                    'mainContactPersonPhone' => [
                        'asc' => [
                            'mainContactPerson.phone' => SORT_ASC,
                        ],
                        'desc' => [
                            'mainContactPerson.phone' => SORT_DESC,
                        ],
                    ],
                ],
            ],
        ]);

        $this->load($params);

        $query->andFilterWhere(['like', 'address', $this->search]);

        $query->andFilterWhere(['address_type' => $this->address_type]);

        $query->andFilterWhere(['contractor_id' => $this->contractor_id]);

        $query->andFilterWhere(['status' => $this->status]);

        $query->andFilterWhere(['mainContactPerson.contact_person' => $this->mainContactPerson]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getContractorFilter()
    {
        $query = clone $this->query;
        $idArray = $query->select(static::tableName() . '.contractor_id')->column();
        $contractorArray = Contractor::getSorted()->andWhere(['contractor.id' => $idArray])->all();
        $ownerFilter = ArrayHelper::map($contractorArray, 'id', 'nameWithType');

        return [null => 'Все'] + $ownerFilter;
    }

    /**
     * @return array
     */
    public function getContactPersonFilter()
    {
        $query = clone $this->query;

        return [null => 'Все'] + ArrayHelper::map($query->groupBy('mainContactPerson.contact_person')->all(), 'mainLoadingAndUnloadingAddressContactPerson.contact_person', 'mainLoadingAndUnloadingAddressContactPerson.contact_person');
    }
}