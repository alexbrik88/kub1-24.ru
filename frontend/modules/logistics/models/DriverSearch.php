<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 20.11.2018
 * Time: 21:37
 */

namespace frontend\modules\logistics\models;


use common\components\helpers\ArrayHelper;
use common\models\Contractor;
use common\models\driver\Driver;
use common\models\driver\DriverPhone;
use common\models\vehicle\Vehicle;
use common\models\vehicle\VehicleType;
use yii\data\ActiveDataProvider;
use Yii;
use yii\db\ActiveQuery;

/**
 * Class DriverSearch
 * @package frontend\modules\logistics\models
 */
class DriverSearch extends Driver
{
    public $search;

    /**
     * @var ActiveQuery
     */
    private $query;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['search'], 'string'],
            [['contractor_id'], 'integer'],
            [['vehicle_id'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /* @var $company \common\models\Company */
        $company = Yii::$app->user->identity->company;
        $this->query = $query = static::find()
            ->joinWith('mainDriverPhone')
            ->joinWith('vehicle')
            ->joinWith('vehicle.vehicleType')
            ->andWhere([static::tableName() . '.company_id' => $company->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'fio' => [
                        'asc' => [
                            'last_name' => SORT_ASC,
                            'first_name' => SORT_ASC,
                            'patronymic' => SORT_ASC,
                        ],
                        'desc' => [
                            'last_name' => SORT_DESC,
                            'first_name' => SORT_DESC,
                            'patronymic' => SORT_DESC,
                        ],
                    ],
                    'phone' => [
                        'asc' => [
                            DriverPhone::tableName() . '.phone' => SORT_ASC,
                        ],
                        'desc' => [
                            DriverPhone::tableName() . '.phone' => SORT_DESC,
                        ],
                    ],
                    'home_town',
                ],
            ],
        ]);

        $this->load($params);

        $query->andFilterWhere([static::tableName() . '.contractor_id' => $this->contractor_id]);

        $query->andFilterWhere(['LIKE', 'CONCAT(last_name, " ", first_name, " ", patronymic)', $this->search]);

        if ($this->vehicle_id == VehicleSearch::TYPE_TRACTOR_PLUS_SEMITRAILER) {
            $query->andWhere(['and',
                [Vehicle::tableName() . '.vehicle_type_id' => VehicleType::TYPE_TRACTOR],
                ['not', [Vehicle::tableName() . '.semitrailer_type_id' => null]],
                ['not', [Vehicle::tableName() . '.semitrailer_type_id' => '']],
            ]);
        } elseif ($this->vehicle_id == VehicleSearch::TYPE_WAGON_PLUS_TRAILER) {
            $query->andWhere(['and',
                [Vehicle::tableName() . '.vehicle_type_id' => VehicleType::TYPE_WAGON],
                ['not', [Vehicle::tableName() . '.trailer_type_id' => null]],
                ['not', [Vehicle::tableName() . '.trailer_type_id' => '']],
            ]);
        } else {
            $query->andFilterWhere([Vehicle::tableName() . '.vehicle_type_id' => $this->vehicle_id]);
        }

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getVehicleFilter()
    {
        $query = clone $this->query;

        $vehicleTypeFilter = ArrayHelper::map($query->all(), 'vehicle.vehicleType.id', 'vehicle.vehicleType.name');
        if ($query->andWhere(['vehicle.vehicle_type_id' => VehicleType::TYPE_TRACTOR])
            ->andWhere(['and',
                ['not', ['vehicle.semitrailer_type_id' => null]],
                ['not', ['vehicle.semitrailer_type_id' => '']],
            ])->exists()) {
            $vehicleTypeFilter = [VehicleSearch::TYPE_TRACTOR_PLUS_SEMITRAILER => 'Тягач + полуприцеп'] + $vehicleTypeFilter;
        }

        $query = clone $this->query;
        if ($query->andWhere(['vehicle.vehicle_type_id' => VehicleType::TYPE_WAGON])
            ->andWhere(['and',
                ['not', ['vehicle.trailer_type_id' => null]],
                ['not', ['vehicle.trailer_type_id' => '']],
            ])->exists()) {
            $vehicleTypeFilter = [VehicleSearch::TYPE_WAGON_PLUS_TRAILER => 'Фургон + прицеп'] + $vehicleTypeFilter;
        }

        return [null => 'Все'] + $vehicleTypeFilter;
    }

    /**
     * @return array
     */
    public function getContractorFilter()
    {
        $query = clone $this->query;
        $idArray = $query->select(static::tableName() . '.contractor_id')->column();
        $contractorArray = Contractor::getSorted()->andWhere(['contractor.id' => $idArray])->all();
        $ownerFilter = ArrayHelper::map($contractorArray, 'id', 'nameWithType');

        return ['' => 'Все'] + $ownerFilter;
    }
}