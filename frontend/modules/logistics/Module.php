<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 20.11.2018
 * Time: 11:18
 */

namespace frontend\modules\logistics;


/**
 * Class Module
 * @package frontend\modules\logistics
 */
class Module extends \yii\base\Module
{
    /**
     * @var string
     */
    public $controllerNamespace = 'frontend\modules\logistics\controllers';

    /**
     *
     */
    public function init()
    {
        parent::init();
    }
}