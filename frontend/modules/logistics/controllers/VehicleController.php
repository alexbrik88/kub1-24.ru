<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 22.11.2018
 * Time: 10:45
 */

namespace frontend\modules\logistics\controllers;


use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\models\Contractor;
use common\models\vehicle\Vehicle;
use common\models\vehicle\VehicleType;
use frontend\components\FrontendController;
use frontend\models\Documents;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\modules\logistics\models\VehicleSearch;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use common\models\file;

/**
 * Class VehicleController
 * @package frontend\modules\logistics\controllers
 */
class VehicleController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // files
                    'file-list' => ['get'],
                    'file-upload' => ['post'],
                    'file-delete    ' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        $actions = array_merge($actions, [
            'file-get' => [
                'class' => file\actions\GetFileAction::className(),
                'model' => file\File::className(),
                'idParam' => 'file-id',
                'fileNameField' => 'filename_full',
                'folderPath' => function ($model) {
                    return $model->getUploadPath();
                },
            ],

            'file-list' => [
                'class' => file\actions\FileListAction::className(),
                'model' => Vehicle::className(),
                'fileLinkCallback' => function (file\File $file, Vehicle $ownerModel) {
                    return Url::to(['file-get', 'type' => Documents::IO_TYPE_OUT, 'id' => $ownerModel->id, 'file-id' => $file->id,]);
                },
            ],
            'file-delete' => [
                'class' => file\actions\FileDeleteAction::className(),
                'model' => Vehicle::className(),
            ],
            'file-upload' => [
                'class' => file\actions\FileUploadAction::className(),
                'model' => Vehicle::className(),
                'noLimitCountFile' => true,
                'folderPath' => 'documents' . DIRECTORY_SEPARATOR . Vehicle::$uploadDirectory,
            ],
            'add-modal-contractor' => [
                'class' => 'frontend\components\AddModalContractorAction',
            ],
        ]);

        return $actions;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = 'reference';

        $searchModel = new VehicleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => Yii::$app->user->identity,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @return mixed|string|\yii\web\Response
     */
    public function actionCreate()
    {
        /* @var $user \common\models\employee\Employee */
        $user = Yii::$app->user->identity;
        /* @var $company \common\models\Company */
        $company = $user->company;

        $model = new Vehicle();
        $model->vehicle_type_id = VehicleType::TYPE_TRACTOR;
        $model->company_id = $company->id;
        $model->author_id = $user->id;
        $model->number = Vehicle::getNextDocumentNumber($company);
        $model->date = date(DateHelper::FORMAT_DATE);
        $model->is_state_number_russian = true;

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->_save()) {
            Yii::$app->session->setFlash('success', 'Транспортное средство добавлено.');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'company' => $company,
        ]);
    }

    /**
     * @param $id
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->_save()) {
            Yii::$app->session->setFlash('success', 'Транспортное средство обновлено.');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'company' => $model->company,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionCopy($id)
    {
        $cloneModel = $this->cloneVehicle($id);
        if ($cloneModel->_save()) {
            Yii::$app->session->setFlash('success', 'Траспортное средство скопировано.');

            return $this->redirect(['view', 'id' => $cloneModel->id,]);
        } else {
            return $this->redirect(['view', 'id' => $id,]);
        }
    }

    /**
     * @param $id
     * @return Vehicle
     * @throws NotFoundHttpException
     */
    public function cloneVehicle($id)
    {
        $model = $this->findModel($id);
        $clone = clone $model;
        unset($clone->id);
        $clone->isNewRecord = true;
        $clone->date = date(DateHelper::FORMAT_DATE);
        $clone->number = (string)$model->getNextDocumentNumber(Yii::$app->user->identity->company);
        $clone->driver_id = null;

        return $clone;
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionComment($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);
        $model->comment = Yii::$app->request->post('comment', '');
        $model->save(true, ['comment']);

        return ['value' => $model->comment];
    }

    /**
     * @return Response
     * @throws \Throwable
     */
    public function actionManyDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $vehicles = Yii::$app->request->post('Vehicle');
        $noDeletedVehicles = [];
        $deletedVehicles = [];
        foreach ($vehicles as $id => $vehicle) {
            if ($vehicle['checked']) {
                $model = Vehicle::findOne([
                    'id' => $id,
                    'company_id' => Yii::$app->user->identity->company->id,
                ]);
                if ($model) {
                    $deleted = $model->_delete();
                    if ($deleted) {
                        $deletedVehicles[] = '№ ' . $model->number . ' дата ' . DateHelper::format($model->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    } else {
                        $noDeletedVehicles[] = '№ ' . $model->number . ' дата ' . DateHelper::format($model->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    }
                }
            }
        }

        if (count($noDeletedVehicles) > 0) {
            Yii::$app->session->setFlash('error', 'ТС не могут быть удалены:<br>' . implode('<br>', $noDeletedVehicles));
        }
        if (count($deletedVehicles) > 0) {
            Yii::$app->session->setFlash('success', 'ТС успешно удалены:<br>' . implode('<br>', $deletedVehicles));
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return array|Response
     * @throws NotFoundHttpException
     * @throws \MpdfException
     * @throws \yii\base\Exception
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionSend($id)
    {
        $model = $this->findModel($id);

        $sendForm = new InvoiceSendForm(Yii::$app->user->identity->currentEmployeeCompany, [
            'model' => $model,
        ]);
        if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($sendForm);
        }

        if ($sendForm->load(Yii::$app->request->post()) && $sendForm->validate()) {
            $saved = $sendForm->send();
            if (is_array($saved)) {
                Yii::$app->session->setFlash('success', 'Отправлено ' . $saved[0] . ' из ' . $saved[1] . ' писем.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке писем.');
            }
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->_delete();

        Yii::$app->session->setFlash('success', 'Траспортное средство удалено.');

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pixel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Vehicle the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $model Vehicle */
        if (($model = Vehicle::find()
                ->andWhere(['and',
                    ['id' => $id],
                    ['company_id' => Yii::$app->user->identity->company_id],
                ])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}