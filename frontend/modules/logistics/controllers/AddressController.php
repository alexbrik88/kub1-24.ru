<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 27.11.2018
 * Time: 13:57
 */

namespace frontend\modules\logistics\controllers;


use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\models\Contractor;
use common\models\loadingAndUnloadingAddress\LoadingAndUnloadingAddress;
use common\models\loadingAndUnloadingAddress\LoadingAndUnloadingAddressContactPerson;
use frontend\components\FrontendController;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\modules\logistics\models\AddressSearch;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class AddressController
 * @package frontend\modules\logistics\controllers
 */
class AddressController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return array_merge(parent::actions(), [
            'add-modal-contractor' => [
                'class' => 'frontend\components\AddModalContractorAction',
            ],
        ]);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = 'reference';

        $searchModel = new AddressSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => Yii::$app->user->identity,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @return mixed|string|\yii\web\Response
     * @throws \Throwable
     */
    public function actionCreate()
    {
        /* @var $user \common\models\employee\Employee */
        $user = Yii::$app->user->identity;
        /* @var $company \common\models\Company */
        $company = $user->company;

        $model = new LoadingAndUnloadingAddress();
        $model->address_type = LoadingAndUnloadingAddress::TYPE_LOADING;
        $model->company_id = $company->id;
        $model->author_id = $user->id;
        $model->number = LoadingAndUnloadingAddress::getNextDocumentNumber($company);
        $model->date = date(DateHelper::FORMAT_DATE);
        $newAddressContactPerson = new LoadingAndUnloadingAddressContactPerson();

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->_save()) {
            Yii::$app->session->setFlash('success', 'Адрес добавлен.');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'company' => $company,
            'newAddressContactPerson' => $newAddressContactPerson,
        ]);
    }

    /**
     * @param $id
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $newAddressContactPerson = new LoadingAndUnloadingAddressContactPerson();
        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->_save()) {
            Yii::$app->session->setFlash('success', 'Адрес обновлен.');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'company' => $model->company,
            'newAddressContactPerson' => $newAddressContactPerson,
        ]);
    }

    /**
     * @return Response
     * @throws \Throwable
     */
    public function actionManyDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $addresses = Yii::$app->request->post('LoadingAndUnloadingAddress');
        $noDeletedAddresses = [];
        $deletedAddresses = [];
        foreach ($addresses as $id => $address) {
            if ($address['checked']) {
                $model = LoadingAndUnloadingAddress::findOne([
                    'id' => $id,
                    'company_id' => Yii::$app->user->identity->company->id,
                ]);
                if ($model) {
                    $deleted = $model->_delete();
                    if ($deleted) {
                        $deletedAddresses[] = '№ ' . $model->number . ' дата ' . DateHelper::format($model->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    } else {
                        $noDeletedAddresses[] = '№ ' . $model->number . ' дата ' . DateHelper::format($model->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    }
                }
            }
        }

        if (count($noDeletedAddresses) > 0) {
            Yii::$app->session->setFlash('error', 'Адреса не могут быть удалены:<br>' . implode('<br>', $noDeletedAddresses));
        }
        if (count($deletedAddresses) > 0) {
            Yii::$app->session->setFlash('success', 'Адреса успешно удалены:<br>' . implode('<br>', $deletedAddresses));
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->_delete();

        Yii::$app->session->setFlash('success', 'Адрес удален.');

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pixel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return LoadingAndUnloadingAddress the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $model LoadingAndUnloadingAddress */
        if (($model = LoadingAndUnloadingAddress::find()
                ->andWhere(['and',
                    ['id' => $id],
                    ['company_id' => Yii::$app->user->identity->company_id],
                ])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}