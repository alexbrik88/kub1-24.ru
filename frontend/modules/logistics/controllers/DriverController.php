<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 20.11.2018
 * Time: 11:25
 */

namespace frontend\modules\logistics\controllers;


use common\components\filters\AccessControl;
use common\models\Contractor;
use common\models\driver\Driver;
use common\models\driver\DriverPhone;
use common\models\driver\IdentificationType;
use common\models\employee\Employee;
use frontend\components\FrontendController;
use frontend\models\Documents;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\modules\logistics\models\DriverSearch;
use yii\helpers\Url;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use common\models\file;
use yii\widgets\ActiveForm;

/**
 * Class DriverController
 * @package frontend\modules\logistics\controllers
 */
class DriverController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // files
                    'file-list' => ['get'],
                    'file-upload' => ['post'],
                    'file-delete    ' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        $actions = array_merge($actions, [
            'file-get' => [
                'class' => file\actions\GetFileAction::className(),
                'model' => file\File::className(),
                'idParam' => 'file-id',
                'fileNameField' => 'filename_full',
                'folderPath' => function ($model) {
                    return $model->getUploadPath();
                },
            ],

            'file-list' => [
                'class' => file\actions\FileListAction::className(),
                'model' => Driver::className(),
                'fileLinkCallback' => function (file\File $file, Driver $ownerModel) {
                    return Url::to(['file-get', 'type' => Documents::IO_TYPE_OUT, 'id' => $ownerModel->id, 'file-id' => $file->id,]);
                },
            ],
            'file-delete' => [
                'class' => file\actions\FileDeleteAction::className(),
                'model' => Driver::className(),
            ],
            'file-upload' => [
                'class' => file\actions\FileUploadAction::className(),
                'model' => Driver::className(),
                'noLimitCountFile' => true,
                'folderPath' => 'documents' . DIRECTORY_SEPARATOR . Driver::$uploadDirectory,
            ],
            'add-modal-contractor' => [
                'class' => 'frontend\components\AddModalContractorAction',
            ],
        ]);

        return $actions;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = 'reference';

        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $searchModel = new DriverSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'company' => $company,
            'user' => $user,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @return mixed|string|\yii\web\Response
     * @throws \Throwable
     */
    public function actionCreate()
    {
        /* @var $user \common\models\employee\Employee */
        $user = Yii::$app->user->identity;
        /* @var $company \common\models\Company */
        $company = $user->company;

        $model = new Driver();
        $model->identification_type_id = IdentificationType::TYPE_PASSPORT;
        $model->company_id = $company->id;
        $model->author_id = $user->id;
        $newDriverPhone = new DriverPhone();

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->_save()) {
            Yii::$app->session->setFlash('success', 'Водитель добавлен.');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'company' => $company,
            'newDriverPhone' => $newDriverPhone,
        ]);
    }

    /**
     * @param $id
     * @return mixed|string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $newDriverPhone = new DriverPhone();
        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->_save()) {
            Yii::$app->session->setFlash('success', 'Водитель обновлен.');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'company' => $model->company,
            'newDriverPhone' => $newDriverPhone,
        ]);
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionComment($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);
        $model->comment = Yii::$app->request->post('comment', '');
        $model->save(true, ['comment']);

        return ['value' => $model->comment];
    }

    /**
     * @param $id
     * @return array|Response
     * @throws NotFoundHttpException
     * @throws \MpdfException
     * @throws \yii\base\Exception
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionSend($id)
    {
        $model = $this->findModel($id);

        $sendForm = new InvoiceSendForm(Yii::$app->user->identity->currentEmployeeCompany, [
            'model' => $model,
        ]);
        if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($sendForm);
        }

        if ($sendForm->load(Yii::$app->request->post()) && $sendForm->validate()) {
            $saved = $sendForm->send();
            if (is_array($saved)) {
                Yii::$app->session->setFlash('success', 'Отправлено ' . $saved[0] . ' из ' . $saved[1] . ' писем.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке писем.');
            }
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->_delete();

        Yii::$app->session->setFlash('success', 'Водитель удален.');

        return $this->redirect(['index']);
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionPhone()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel(Yii::$app->request->post('driver_id'));

        if ($model->mainDriverPhone) {
            return [
                'result' => true,
                'phone' => $model->mainDriverPhone->phoneType->name . ': ' . $model->mainDriverPhone->phone,
            ];
        }

        return [
            'result' => false,
        ];
    }

    /**
     * Finds the Pixel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Driver the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $model Driver */
        if (($model = Driver::find()
                ->andWhere(['and',
                    ['id' => $id],
                    ['company_id' => Yii::$app->user->identity->company_id],
                ])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}