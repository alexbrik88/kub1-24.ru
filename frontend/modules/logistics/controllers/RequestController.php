<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.12.2018
 * Time: 19:33
 */

namespace frontend\modules\logistics\controllers;


use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\components\pdf\PdfRenderer;
use common\components\pdf\Printable;
use common\components\phpWord\PhpWordLogisticsRequest;
use common\models\Contractor;
use common\models\employee\Employee;
use common\models\logisticsRequest\LogisticsRequest;
use common\models\logisticsRequest\LogisticsRequestAdditionalExpenses;
use common\models\logisticsRequest\LogisticsRequestContractEssence;
use common\models\logisticsRequest\LogisticsRequestLoadingAndUnloading;
use common\models\logisticsRequest\LogisticsRequestStatus;
use frontend\components\FrontendController;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\documents\assets\InvoicePrintAsset;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\modules\logistics\models\RequestSearch;
use Yii;
use yii\base\Exception;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use common\models\file;
use yii\widgets\ActiveForm;

/**
 * Class RequestController
 * @package frontend\modules\logistics\controllers
 */
class RequestController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    // files
                    'file-list' => ['get'],
                    'file-upload' => ['post'],
                    'file-delete    ' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        $actions = array_merge($actions, [
            'file-get' => [
                'class' => file\actions\GetFileAction::class,
                'model' => file\File::class,
                'idParam' => 'file-id',
                'fileNameField' => 'filename_full',
                'folderPath' => function ($model) {
                    return $model->getUploadPath();
                },
            ],

            'file-list' => [
                'class' => file\actions\FileListAction::class,
                'model' => LogisticsRequest::class,
                'fileLinkCallback' => function (file\File $file, LogisticsRequest $ownerModel) {
                    return Url::to(['file-get', 'type' => Documents::IO_TYPE_OUT, 'id' => $ownerModel->id, 'file-id' => $file->id,]);
                },
            ],
            'file-delete' => [
                'class' => file\actions\FileDeleteAction::class,
                'model' => LogisticsRequest::class,
            ],
            'file-upload' => [
                'class' => file\actions\FileUploadAction::class,
                'model' => LogisticsRequest::class,
                'noLimitCountFile' => true,
                'folderPath' => 'documents' . DIRECTORY_SEPARATOR . LogisticsRequest::$uploadDirectory,
            ],
            'add-modal-contractor' => [
                'class' => 'frontend\components\AddModalContractorAction',
            ],
        ]);

        return $actions;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        /* @var $company \common\models\Company */
        $company = Yii::$app->user->identity->company;
        $searchModel = new RequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'company' => $company,
        ]);
    }

    /**
     * @param $id
     * @param int $activeTab
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id, $activeTab = LogisticsRequest::TYPE_CUSTOMER)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
            'activeTab' => $activeTab,
        ]);
    }

    /**
     * @return string|Response
     * @throws \Throwable
     */
    public function actionCreate()
    {
        /* @var $user \common\models\employee\Employee */
        $user = Yii::$app->user->identity;
        $company = $user->company;

        $model = new LogisticsRequest();
        $model->company_id = $company->id;
        $model->author_id = $user->id;
        $model->status_id = LogisticsRequestStatus::STATUS_ON_LOADING;
        $model->document_date = date(DateHelper::FORMAT_DATE);
        $model->document_number = LogisticsRequest::getNextDocumentNumber($company);

        $logisticsRequestLoading = new LogisticsRequestLoadingAndUnloading();
        $logisticsRequestLoading->type = LogisticsRequestLoadingAndUnloading::TYPE_LOADING;

        $logisticsRequestUnloading = new LogisticsRequestLoadingAndUnloading();
        $logisticsRequestUnloading->type = LogisticsRequestLoadingAndUnloading::TYPE_UNLOADING;

        if ($company->logisticsRequestContractEssenceCustomerAll) {
            $logisticsRequestContractEssenceCustomerAll = $company->logisticsRequestContractEssenceCustomerAll;
        } else {
            $logisticsRequestContractEssenceCustomerAll = new LogisticsRequestContractEssence();
            $logisticsRequestContractEssenceCustomerAll->company_id = $company->id;
            $logisticsRequestContractEssenceCustomerAll->type = LogisticsRequestContractEssence::TYPE_CUSTOMER;
            $logisticsRequestContractEssenceCustomerAll->for_contractor = 0;
        }

        $logisticsRequestContractEssenceCustomerForContractor = new LogisticsRequestContractEssence();
        $logisticsRequestContractEssenceCustomerForContractor->company_id = $company->id;
        $logisticsRequestContractEssenceCustomerForContractor->type = LogisticsRequestContractEssence::TYPE_CUSTOMER;
        $logisticsRequestContractEssenceCustomerForContractor->for_contractor = 1;

        if ($company->logisticsRequestContractEssenceCarrierAll) {
            $logisticsRequestContractEssenceCarrierAll = $company->logisticsRequestContractEssenceCarrierAll;
        } else {
            $logisticsRequestContractEssenceCarrierAll = new LogisticsRequestContractEssence();
            $logisticsRequestContractEssenceCarrierAll->company_id = $company->id;
            $logisticsRequestContractEssenceCarrierAll->type = LogisticsRequestContractEssence::TYPE_CARRIER;
            $logisticsRequestContractEssenceCarrierAll->for_contractor = 0;
        }

        $logisticsRequestContractEssenceCarrierForContractor = new LogisticsRequestContractEssence();
        $logisticsRequestContractEssenceCarrierForContractor->company_id = $company->id;
        $logisticsRequestContractEssenceCarrierForContractor->type = LogisticsRequestContractEssence::TYPE_CARRIER;
        $logisticsRequestContractEssenceCarrierForContractor->for_contractor = 1;

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        $loaded = false;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->carrier_id == 'is_own') {
                $model->carrier_id = null;
                $model->is_own = true;
            }
            $loaded = true;
        }
        $logisticsRequestLoading->load(Yii::$app->request->post('LogisticsRequestLoadingAndUnloading'), 0);
        $logisticsRequestUnloading->load(Yii::$app->request->post('LogisticsRequestLoadingAndUnloading'), 1);
        $logisticsRequestContractEssenceCustomerAll->load(Yii::$app->request->post('LogisticsRequestContractEssence')['customer'] ?? [], 'all');
        $logisticsRequestContractEssenceCustomerForContractor->load(Yii::$app->request->post('LogisticsRequestContractEssence')['customer'] ?? [], 'contractor');
        $logisticsRequestContractEssenceCarrierAll->load(Yii::$app->request->post('LogisticsRequestContractEssence')['carrier'] ?? [], 'all');
        $logisticsRequestContractEssenceCarrierForContractor->load(Yii::$app->request->post('LogisticsRequestContractEssence')['carrier'] ?? [], 'contractor');

        if ($loaded) {
            $result = function ($db) use (
                $logisticsRequestLoading, $logisticsRequestUnloading,
                $logisticsRequestContractEssenceCustomerAll, $logisticsRequestContractEssenceCustomerForContractor,
                $logisticsRequestContractEssenceCarrierAll, $logisticsRequestContractEssenceCarrierForContractor,
                $model, $company
            ) {
                if ($model->save()) {
                    $logisticsRequestLoading->logistics_request_id = $model->id;
                    if (!$logisticsRequestLoading->save()) {

                        return false;
                    }

                    $logisticsRequestUnloading->logistics_request_id = $model->id;
                    if (!$logisticsRequestUnloading->save()) {

                        return false;
                    }

                    if (($logisticsRequestContractEssenceCustomerAll->is_checked && $model->customer_request_essence) ||
                        !$logisticsRequestContractEssenceCustomerAll->is_checked) {
                        $logisticsRequestContractEssenceCustomerAll->text = $model->customer_request_essence;
                        if (!$logisticsRequestContractEssenceCustomerAll->save()) {

                            return false;
                        }
                    }

                    if (($logisticsRequestContractEssenceCarrierAll->is_checked && $model->carrier_request_essence) ||
                        !$logisticsRequestContractEssenceCarrierAll->is_checked) {
                        $logisticsRequestContractEssenceCarrierAll->text = $model->carrier_request_essence;
                        if (!$logisticsRequestContractEssenceCarrierAll->save()) {

                            return false;
                        }
                    }

                    if (($logisticsRequestContractEssenceCustomerForContractor->is_checked && $model->customer_request_essence) ||
                        !$logisticsRequestContractEssenceCustomerForContractor->is_checked) {

                        if (($newLogisticsRequestContractEssenceCustomerForContractor = $company->getLogisticsRequestContractEssenceCustomerForContractor()
                                ->andWhere(['contractor_id' => $model->customer_id])
                                ->one()) == null) {
                            $newLogisticsRequestContractEssenceCustomerForContractor = new LogisticsRequestContractEssence();
                            $newLogisticsRequestContractEssenceCustomerForContractor->company_id = $company->id;
                            $newLogisticsRequestContractEssenceCustomerForContractor->type = LogisticsRequestContractEssence::TYPE_CUSTOMER;
                            $newLogisticsRequestContractEssenceCustomerForContractor->for_contractor = 1;
                            $newLogisticsRequestContractEssenceCustomerForContractor->contractor_id = $model->customer_id;
                        }
                        $newLogisticsRequestContractEssenceCustomerForContractor->is_checked = $logisticsRequestContractEssenceCustomerForContractor->is_checked;
                        $newLogisticsRequestContractEssenceCustomerForContractor->text = $model->customer_request_essence;
                        if (!$newLogisticsRequestContractEssenceCustomerForContractor->save()) {

                            return false;
                        }
                    }

                    if (($logisticsRequestContractEssenceCarrierForContractor->is_checked && $model->carrier_request_essence) ||
                        !$logisticsRequestContractEssenceCarrierForContractor->is_checked) {

                        if (($newLogisticsRequestContractEssenceCarrierForContractor = $company->getLogisticsRequestContractEssenceCarrierForContractor()
                                ->andWhere(['contractor_id' => $model->carrier_id])
                                ->one()) == null) {
                            $newLogisticsRequestContractEssenceCarrierForContractor = new LogisticsRequestContractEssence();
                            $newLogisticsRequestContractEssenceCarrierForContractor->company_id = $company->id;
                            $newLogisticsRequestContractEssenceCarrierForContractor->type = LogisticsRequestContractEssence::TYPE_CARRIER;
                            $newLogisticsRequestContractEssenceCarrierForContractor->for_contractor = 1;
                            $newLogisticsRequestContractEssenceCarrierForContractor->contractor_id = $model->carrier_id;
                        }
                        $newLogisticsRequestContractEssenceCarrierForContractor->is_checked = $logisticsRequestContractEssenceCarrierForContractor->is_checked;
                        $newLogisticsRequestContractEssenceCarrierForContractor->text = $model->carrier_request_essence;
                        if (!$newLogisticsRequestContractEssenceCarrierForContractor->save()) {

                            return false;
                        }
                    }

                    return true;
                }

                return false;
            };
            $isSaved = LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, $result);
            if ($isSaved) {
                Yii::$app->session->setFlash('success', 'Заявка создана.');

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        if ($model->is_own) {
            $model->carrier_id = 'is_own';
        }

        return $this->render('create', [
            'model' => $model,
            'company' => $company,
            'logisticsRequestLoading' => $logisticsRequestLoading,
            'logisticsRequestUnloading' => $logisticsRequestUnloading,
            'logisticsRequestContractEssenceCustomerAll' => $logisticsRequestContractEssenceCustomerAll,
            'logisticsRequestContractEssenceCustomerForContractor' => $logisticsRequestContractEssenceCustomerForContractor,
            'logisticsRequestContractEssenceCarrierAll' => $logisticsRequestContractEssenceCarrierAll,
            'logisticsRequestContractEssenceCarrierForContractor' => $logisticsRequestContractEssenceCarrierForContractor,
        ]);
    }

    /**
     * @param $id
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        /* @var $user \common\models\employee\Employee */
        $user = Yii::$app->user->identity;
        $company = $user->company;

        $logisticsRequestLoading = $model->loading;
        $logisticsRequestUnloading = $model->unloading;

        if ($company->logisticsRequestContractEssenceCustomerAll) {
            $logisticsRequestContractEssenceCustomerAll = $company->logisticsRequestContractEssenceCustomerAll;
        } else {
            $logisticsRequestContractEssenceCustomerAll = new LogisticsRequestContractEssence();
            $logisticsRequestContractEssenceCustomerAll->company_id = $company->id;
            $logisticsRequestContractEssenceCustomerAll->type = LogisticsRequestContractEssence::TYPE_CUSTOMER;
            $logisticsRequestContractEssenceCustomerAll->for_contractor = 0;
        }

        $logisticsRequestContractEssenceCustomerForContractor = new LogisticsRequestContractEssence();
        $logisticsRequestContractEssenceCustomerForContractor->company_id = $company->id;
        $logisticsRequestContractEssenceCustomerForContractor->type = LogisticsRequestContractEssence::TYPE_CUSTOMER;
        $logisticsRequestContractEssenceCustomerForContractor->for_contractor = 1;

        if ($company->logisticsRequestContractEssenceCarrierAll) {
            $logisticsRequestContractEssenceCarrierAll = $company->logisticsRequestContractEssenceCarrierAll;
        } else {
            $logisticsRequestContractEssenceCarrierAll = new LogisticsRequestContractEssence();
            $logisticsRequestContractEssenceCarrierAll->company_id = $company->id;
            $logisticsRequestContractEssenceCarrierAll->type = LogisticsRequestContractEssence::TYPE_CARRIER;
            $logisticsRequestContractEssenceCarrierAll->for_contractor = 0;
        }

        $logisticsRequestContractEssenceCarrierForContractor = new LogisticsRequestContractEssence();
        $logisticsRequestContractEssenceCarrierForContractor->company_id = $company->id;
        $logisticsRequestContractEssenceCarrierForContractor->type = LogisticsRequestContractEssence::TYPE_CARRIER;
        $logisticsRequestContractEssenceCarrierForContractor->for_contractor = 1;

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        $loaded = false;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->carrier_id == 'is_own') {
                $model->carrier_id = null;
                $model->is_own = true;
            }
            $loaded = true;
        }
        $logisticsRequestLoading->load(Yii::$app->request->post('LogisticsRequestLoadingAndUnloading'), 0);
        $logisticsRequestUnloading->load(Yii::$app->request->post('LogisticsRequestLoadingAndUnloading'), 1);
        $logisticsRequestContractEssenceCustomerAll->load(Yii::$app->request->post('LogisticsRequestContractEssence')['customer'] ?? [], 'all');
        $logisticsRequestContractEssenceCustomerForContractor->load(Yii::$app->request->post('LogisticsRequestContractEssence')['customer'] ?? [], 'contractor');
        $logisticsRequestContractEssenceCarrierAll->load(Yii::$app->request->post('LogisticsRequestContractEssence')['carrier'] ?? [], 'all');
        $logisticsRequestContractEssenceCarrierForContractor->load(Yii::$app->request->post('LogisticsRequestContractEssence')['carrier'] ?? [], 'contractor');


        if ($loaded) {
            $result = function ($db) use (
                $logisticsRequestLoading, $logisticsRequestUnloading,
                $logisticsRequestContractEssenceCustomerAll, $logisticsRequestContractEssenceCustomerForContractor,
                $logisticsRequestContractEssenceCarrierAll, $logisticsRequestContractEssenceCarrierForContractor,
                $model, $company
            ) {
                if ($model->save()) {
                    if (!$logisticsRequestLoading->save()) {

                        return false;
                    }

                    if (!$logisticsRequestUnloading->save()) {

                        return false;
                    }

                    if (($logisticsRequestContractEssenceCustomerAll->is_checked && $model->customer_request_essence) ||
                        !$logisticsRequestContractEssenceCustomerAll->is_checked) {
                        $logisticsRequestContractEssenceCustomerAll->text = $model->customer_request_essence;
                        if (!$logisticsRequestContractEssenceCustomerAll->save()) {

                            return false;
                        }
                    }

                    if (($logisticsRequestContractEssenceCarrierAll->is_checked && $model->carrier_request_essence) ||
                        !$logisticsRequestContractEssenceCarrierAll->is_checked) {
                        $logisticsRequestContractEssenceCarrierAll->text = $model->carrier_request_essence;
                        if (!$logisticsRequestContractEssenceCarrierAll->save()) {

                            return false;
                        }
                    }

                    if (($logisticsRequestContractEssenceCustomerForContractor->is_checked && $model->customer_request_essence) ||
                        !$logisticsRequestContractEssenceCustomerForContractor->is_checked) {

                        if (($newLogisticsRequestContractEssenceCustomerForContractor = $company->getLogisticsRequestContractEssenceCustomerForContractor()
                                ->andWhere(['contractor_id' => $model->customer_id])
                                ->one()) == null) {
                            $newLogisticsRequestContractEssenceCustomerForContractor = new LogisticsRequestContractEssence();
                            $newLogisticsRequestContractEssenceCustomerForContractor->company_id = $company->id;
                            $newLogisticsRequestContractEssenceCustomerForContractor->type = LogisticsRequestContractEssence::TYPE_CUSTOMER;
                            $newLogisticsRequestContractEssenceCustomerForContractor->for_contractor = 1;
                            $newLogisticsRequestContractEssenceCustomerForContractor->contractor_id = $model->customer_id;
                        }
                        $newLogisticsRequestContractEssenceCustomerForContractor->is_checked = $logisticsRequestContractEssenceCustomerForContractor->is_checked;
                        $newLogisticsRequestContractEssenceCustomerForContractor->text = $model->customer_request_essence;
                        if (!$newLogisticsRequestContractEssenceCustomerForContractor->save()) {

                            return false;
                        }
                    }

                    if (($logisticsRequestContractEssenceCarrierForContractor->is_checked && $model->carrier_request_essence) ||
                        !$logisticsRequestContractEssenceCarrierForContractor->is_checked) {

                        if (($newLogisticsRequestContractEssenceCarrierForContractor = $company->getLogisticsRequestContractEssenceCarrierForContractor()
                                ->andWhere(['contractor_id' => $model->carrier_id])
                                ->one()) == null) {
                            $newLogisticsRequestContractEssenceCarrierForContractor = new LogisticsRequestContractEssence();
                            $newLogisticsRequestContractEssenceCarrierForContractor->company_id = $company->id;
                            $newLogisticsRequestContractEssenceCarrierForContractor->type = LogisticsRequestContractEssence::TYPE_CARRIER;
                            $newLogisticsRequestContractEssenceCarrierForContractor->for_contractor = 1;
                            $newLogisticsRequestContractEssenceCarrierForContractor->contractor_id = $model->carrier_id;
                        }
                        $newLogisticsRequestContractEssenceCarrierForContractor->is_checked = $logisticsRequestContractEssenceCarrierForContractor->is_checked;
                        $newLogisticsRequestContractEssenceCarrierForContractor->text = $model->carrier_request_essence;
                        if (!$newLogisticsRequestContractEssenceCarrierForContractor->save()) {

                            return false;
                        }
                    }

                    return true;
                }

                return false;
            };
            $isSaved = LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE, $result);
            if ($isSaved) {
                Yii::$app->session->setFlash('success', 'Заявка обновлена.');

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        if ($model->is_own) {
            $model->carrier_id = 'is_own';
        }

        return $this->render('update', [
            'model' => $model,
            'company' => $company,
            'logisticsRequestLoading' => $logisticsRequestLoading,
            'logisticsRequestUnloading' => $logisticsRequestUnloading,
            'logisticsRequestContractEssenceCustomerAll' => $logisticsRequestContractEssenceCustomerAll,
            'logisticsRequestContractEssenceCustomerForContractor' => $logisticsRequestContractEssenceCustomerForContractor,
            'logisticsRequestContractEssenceCarrierAll' => $logisticsRequestContractEssenceCarrierAll,
            'logisticsRequestContractEssenceCarrierForContractor' => $logisticsRequestContractEssenceCarrierForContractor,
        ]);
    }

    /**
     * @param $id
     * @param $status
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionUpdateStatus($id, $status)
    {
        $model = $this->findModel($id);

        $status = LogisticsRequestStatus::findOne($status);
        $date = Yii::$app->request->post('date');

        if ($status !== null && $status->id != $model->status_id) {
            $model->status_id = $status->id;
            $model->status_updated_at = $date ? strtotime($date) : null;
            $model->changeStatusDate = $date ? strtotime($date) : null;
            LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function (LogisticsRequest $model) {
                if ($model->save(true, ['status_id', 'status_updated_at',])) {
                    return true;
                }

                return false;
            });
        }

        return $this->redirect(['view', 'id' => $model->id,]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionCopy($id)
    {
        $cloneID = $this->cloneRequest($id);
        if ($cloneID) {
            Yii::$app->session->setFlash('success', 'Заявка скопирована.');

            return $this->redirect(['view', 'id' => $cloneID,]);
        } else {
            return $this->redirect(['view', 'id' => $id,]);
        }
    }

    /**
     * @param $id
     * @return bool
     * @throws NotFoundHttpException
     */
    public function cloneRequest($id)
    {
        $model = $this->findModel($id);
        $clone = clone $model;
        unset($clone->id);
        $clone->isNewRecord = true;
        $clone->document_date = date(DateHelper::FORMAT_DATE);
        $clone->document_number = (string)$model->getNextDocumentNumber(Yii::$app->user->identity->company);

        if ($clone->save()) {
            $logisticsRequestLoading = clone $model->loading;
            $logisticsRequestUnloading = clone $model->unloading;
            unset($logisticsRequestLoading->id);
            unset($logisticsRequestUnloading->id);
            $logisticsRequestLoading->isNewRecord = true;
            $logisticsRequestUnloading->isNewRecord = true;

            $logisticsRequestLoading->logistics_request_id = $clone->id;
            $logisticsRequestUnloading->logistics_request_id = $clone->id;
            if ($logisticsRequestLoading->save() && $logisticsRequestUnloading->save()) {
                return $clone->id;
            }
        }

        return false;
    }

    /**
     * @param $actionType
     * @param $id
     * @param null $type
     * @param $filename
     * @return string|\yii\console\Response|Response
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws \MpdfException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDocumentPrint($actionType, $id, $type = LogisticsRequest::TYPE_CUSTOMER, $filename)
    {
        $model = $this->findModel($id);

        return $this->_documentPrint($model, InvoicePrintAsset::class, $actionType, 'pdf-view', [
            'addStamp' => ($actionType === 'pdf') ? $model->company->pdf_signed : 0,
            'type' => $type,
        ]);
    }

    /**
     * @param $uid
     * @param int $type
     * @param string $view
     * @return string|\yii\console\Response|Response
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws \MpdfException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionOutView($uid, $type = LogisticsRequest::TYPE_CUSTOMER, $view = 'print')
    {
        /* @var LogisticsRequest $model */
        $model = LogisticsRequest::find()
            ->andWhere(['id' => $uid])
            ->one();

        if ($model === null) {
            throw new NotFoundHttpException();
        }

        return $this->_documentPrint($model, InvoicePrintAsset::class, $view, 'pdf-view', [
            'isOutView' => true,
            'addStamp' => true,
            'type' => $type,
        ]);
    }

    /**
     * @param $id
     * @param $requestType
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function actionDocx($id, $type)
    {
        $model = $this->findModel($id);
        $word = new PhpWordLogisticsRequest($model, $type);

        return $word->getFile();
    }

    /**
     * @return string
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionBasisDocument()
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException();
        }
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $contractor = Contractor::findOne(['id' => Yii::$app->request->get('contractorId'), 'company_id' => $company->id]);
        if ($contractor && $contractor->type == Contractor::TYPE_CUSTOMER) {
            $attribute = 'customer_id';
            $agreementAttribute = 'customerAgreement';
            $relation = 'customer';
        } elseif ($contractor && $contractor->type == Contractor::TYPE_SELLER) {
            $attribute = 'carrier_id';
            $agreementAttribute = 'carrierAgreement';
            $relation = 'carrier';
        } else {
            throw new BadRequestHttpException();
        }
        $model = new LogisticsRequest();
        $model->company_id = $company->id;
        $model->author_id = $user->id;
        $model->status_id = LogisticsRequestStatus::STATUS_ON_LOADING;
        $model->document_date = date(DateHelper::FORMAT_DATE);
        $model->document_number = LogisticsRequest::getNextDocumentNumber($company);

        if ($contractor) {
            $model->$attribute = $contractor->id;
            $model->$agreementAttribute = $contractor->last_basis_document;
            $model->populateRelation($relation, $contractor);
        }

        return $this->renderPartial('form/_basis_document', [
            'model' => $model,
            'contractor' => $contractor,
            'attribute' => $agreementAttribute,
            'options' => [
                'class' => 'is_own-disabled',
            ],
        ]);
    }

    /**
     * @return array
     */
    public function actionExpensesCreate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $companyID = Yii::$app->user->identity->company->id;
        $itemName = Yii::$app->request->post('name');

        $model = new LogisticsRequestAdditionalExpenses([
            'company_id' => $companyID,
            'name' => $itemName,
        ]);

        if ($model->save()) {
            return ['itemId' => $model->id, 'itemName' => $model->name];
        }

        return [];
    }

    /**
     * @return array
     */
    public function actionExpensesEdit()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = LogisticsRequestAdditionalExpenses::findOne([
            'id' => Yii::$app->request->post('id'),
            'company_id' => Yii::$app->user->identity->company->id,
        ]);
        if ($model === null) {
            return [
                'success' => false,
                'message' => 'Доп. расход не найден.',
            ];
        }
        if ($model->load(Yii::$app->request->post(), '') && $model->save()) {
            return [
                'success' => true,
                'name' => $model->name,
            ];
        }
        return [
            'success' => false,
            'message' => $model->getFirstError('name'),
        ];
    }

    /**
     * @return array
     * @throws \yii\db\StaleObjectException
     */
    public function actionExpensesDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = LogisticsRequestAdditionalExpenses::findOne([
            'id' => Yii::$app->request->post('id'),
            'company_id' => Yii::$app->user->identity->company->id,
        ]);
        if ($model === null) {
            return [
                'success' => false,
                'message' => 'Доп. расход не найден.',
            ];
        }
        if ($model->getCustomerLogisticsRequests()->exists() || $model->getCarrierLogisticsRequests()->exists()) {
            return [
                'success' => false,
                'message' => 'Доп. расход удалить нельзя, так как к нему привязаны заявки.',
            ];
        }
        if ($model->delete()) {
            return [
                'success' => true,
                'message' => 'Доп. расход успешно удален.',
            ];
        }

        return [
            'success' => false,
            'message' => 'Доп. расход не удалось удалить.',
        ];
    }

    /**
     * @return array
     */
    public function actionContractorEssence()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* @var $company \common\models\Company */
        $company = Yii::$app->user->identity->company;
        $contractorID = Yii::$app->request->post('contractor_id');
        $type = Yii::$app->request->post('type');
        $contractor = $company->getContractors()->andWhere(['id' => $contractorID])->one();

        if ($contractor) {
            $essence = null;
            /* @var $essence LogisticsRequestContractEssence */
            if ($type == LogisticsRequestContractEssence::TYPE_CARRIER) {
                $essence = $company->getLogisticsRequestContractEssenceCarrierForContractor()
                    ->andWhere(['contractor_id' => $contractorID])
                    ->andWhere(['is_checked' => true])
                    ->one();
            } elseif ($type == LogisticsRequestContractEssence::TYPE_CUSTOMER) {
                $essence = $company->getLogisticsRequestContractEssenceCustomerForContractor()
                    ->andWhere(['contractor_id' => $contractorID])
                    ->andWhere(['is_checked' => true])
                    ->one();
            }
            if ($essence) {
                return [
                    'result' => true,
                    'essence' => $essence->text,
                ];
            }
        }

        return ['result' => false];
    }

    /**
     * @param $id
     * @return array|Response
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws \MpdfException
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionSend($id)
    {
        $model = $this->findModel($id);

        $sendForm = new InvoiceSendForm(Yii::$app->user->identity->currentEmployeeCompany, [
            'model' => $model,
        ]);
        if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($sendForm);
        }

        if ($sendForm->load(Yii::$app->request->post()) && $sendForm->validate()) {
            $saved = $sendForm->send();
            if (is_array($saved)) {
                Yii::$app->session->setFlash('success', 'Отправлено ' . $saved[0] . ' из ' . $saved[1] . ' писем.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке писем.');
            }
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->_delete();

        Yii::$app->session->setFlash('success', 'Заявка успешно удалена.');

        return $this->redirect(['index']);
    }

    /**
     * @return Response
     * @throws \Throwable
     */
    public function actionManyDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $requests = Yii::$app->request->post('LogisticsRequest');
        $noDeletedRequested = [];
        $deletedRequested = [];
        foreach ($requests as $id => $request) {
            if ($request['checked']) {
                $model = LogisticsRequest::findOne([
                    'id' => $id,
                    'company_id' => Yii::$app->user->identity->company->id,
                ]);
                if ($model) {
                    $deleted = $model->_delete();
                    if ($deleted) {
                        $deletedRequested[] = '№ ' . $model->document_number . ' от ' . DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    } else {
                        $noDeletedRequested[] = '№ ' . $model->document_number . ' от ' . DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    }
                }
            }
        }

        if (count($noDeletedRequested) > 0) {
            Yii::$app->session->setFlash('error', 'Заявки не могут быть удалены:<br>' . implode('<br>', $noDeletedRequested));
        }
        if (count($deletedRequested) > 0) {
            Yii::$app->session->setFlash('success', 'Заявки успешно удалены:<br>' . implode('<br>', $deletedRequested));
        }

        return $this->redirect(['index']);
    }

    /**
     * @param LogisticsRequest $model
     * @param $asset
     * @param $actionType
     * @param $view
     * @param array $params
     * @return string|\yii\console\Response|Response
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws \MpdfException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    protected function _documentPrint(LogisticsRequest $model, $asset, $actionType, $view, $params = [])
    {
        if (!($model instanceof Printable)) {
            throw new Exception('Model must be instance of Printable.');
        }
        $renderer = new PdfRenderer([
            'view' => $view,
            'params' => array_merge([
                'model' => $model,
                'message' => null,
            ], $params),
            'destination' => PdfRenderer::DESTINATION_BROWSER,
            'filename' => $model->getPrintTitle() . '.pdf',
            'displayMode' => ArrayHelper::getValue($params, 'displayMode', PdfRenderer::DISPLAY_MODE_FULLPAGE),
            'format' => ArrayHelper::getValue($params, 'documentFormat', 'A4-P'),
        ]);
        $this->view->params['asset'] = $asset;
        switch ($actionType) {
            case 'pdf':
                return $renderer->output();
            case 'print':
            default:
                if ($this->action->id != 'out-view') {
                    Yii::$app->view->registerJs('window.print();');
                }

                return $renderer->renderHtml();
        }
    }

    /**
     * Finds the Pixel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return LogisticsRequest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $model LogisticsRequest */
        if (($model = LogisticsRequest::find()
                ->andWhere(['and',
                    ['id' => $id],
                    ['company_id' => Yii::$app->user->identity->company_id],
                ])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}