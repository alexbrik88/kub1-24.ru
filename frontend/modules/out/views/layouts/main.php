<?php
use common\components\image\EasyThumbnailImage;
use common\models\Company;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$company = Yii::$app->params['outCompany'];
$logoWidth = Company::$imageDataArray['logoImage']['width'];
$logoHeight = Company::$imageDataArray['logoImage']['height'];
$ratio = $logoHeight / $logoWidth * 100;
$showHeader = empty($this->params['showHeader']) ? false : true;
$hasError = Yii::$app->params['hasError'];
$isDemo = Yii::$app->params['isDemo'];

if ($company && is_file($path = $company->getImage('logoImage'))) {
    $logoSrc = EasyThumbnailImage::thumbnailSrc(
        $path,
        $logoWidth,
        $logoHeight,
        EasyThumbnailImage::THUMBNAIL_INSET
    );
    $this->registerMetaTag([
        'property' => 'og:image',
        'content' => Url::to($logoSrc, true),
    ], 'og_image');
}
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="icon" href="/img/fav.svg?i=2" type="image/x-icon">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:regular,italic,bold,bolditalic" rel="stylesheet" type="text/css"/>
</head>
<body style="display: block; background-color: #fff;">
    <style type="text/css">
    .form-control {
        width: 100%;
    }
    .darkblue {
        color: #fff;
    }
    .action-battons .btn {
        min-width: 120px;
        text-align: center;
    }
    .btn.go-back-btn {
        color: #fff;
        background-color: #83a6c2;
    }
    .btn.go-back-btn:hover {
        color: #fff;
        background-color: #4276a4;
    }
    <?php if ($isDemo): ?>
        .demo-out-invoice {
            color: red;
            font-size: 88px;
            font-weight: bold;
            opacity: .4;
            position: absolute;
            top: 100px;
            text-align: center;
            margin-left: auto;
            margin-right: auto;
            left: 0;
            right: 0;
        }
        .rotatable{
            -webkit-transform: rotate(-45deg); //Chrome, Safari
            -moz-transform: rotate(-45deg); //Firefox
            -o-transform: rotate(-45deg); //Opera
            -ms-transform: rotate(-45deg); //IE
            transform: rotate(-45deg); //браузеры без префексов
        }
        #produc-table,
        #produc-table thead,
        #produc-table tbody,
        #produc-table tr,
        #produc-table td,
        .form-control {
            background-color:rgba(255, 255, 255, 0);
        }
    <?php endif; ?>
    </style>
    <div style="text-align: center;">
        <div style="display: inline-block; margin: 20px; text-align: left; background-image:">
            <div style="display: inline-block; padding: 20px; border: 1px solid #bebebe; border-radius: 8px !important; position: relative;">
                <?= Alert::widget(); ?>
                <div>

                    <?php if ($isDemo): ?>
                        <div class="demo-out-invoice rotatable">Образец</div>
                    <?php endif; ?>

                    <?php if (($hasError || $showHeader) && $company !== null) : ?>
                        <table>
                            <tr>
                                <?php if (is_file($path)) : ?>
                                    <td style="padding: 0; border-width: 0;">
                                        <div style="padding: 0 15px 0 0; margin: 0 10px 0 0; border-right: 1px solid #ddd;">
                                        <?= EasyThumbnailImage::thumbnailImg(
                                            $path,
                                            $logoWidth,
                                            $logoHeight,
                                            EasyThumbnailImage::THUMBNAIL_INSET,
                                            [
                                                'style' => 'height: 36px;'
                                            ]
                                        ); ?>
                                        </div>
                                    </td>
                                <?php endif ?>
                                <td style="padding: 0; border-width: 0;">
                                    Выставление счета на оплату
                                    <br>
                                    в <?= Yii::$app->params['outCompany']->getTitle(true) ?>
                                </td>
                            </tr>
                        </table>
                    <?php endif ?>
                </div>

                <?php echo $content ?>
            </div>
            <?php if (!$company->hide_widget_footer) : ?>
                <div style="margin-top: 20px;">
                    2015-<?= date('Y') ?> <?= Yii::$app->kubCompany->getTitle(true) ?>
                    <span class="pull-right">
                        <?= Html::a(
                            'О сервисе КУБ',
                            trim(Yii::$app->params['serviceSite'], '/') . '/modul-b2b-sales/?utm_source=lk_modul_b2b',
                            ['target' => '_blank']
                        ) ?>
                    </span>
                </div>
            <?php endif ?>
        </div>
    </div>

<?php $this->endBody(); ?>

<script>
    jQuery(document).ready(function () {
        Metronic.init(); // init metronic core components
    });
</script>

</body>
</html>
<?php $this->endPage(); ?>
