<?php

use common\models\product\PriceList;
use frontend\modules\out\models\OutInvoiceForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\helpers\ArrayHelper;
use common\models\product\Product;

/* @var $this yii\web\View */
/* @var $model frontend\modules\out\models\OutOrderDocumentForm */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $priceListOrders \common\models\product\PriceListOrder[] */

$allowedProduct = $model->getPriceList()->getPriceListOrders()->indexBy('product_id')->all();
$totalPrice = 0;
$isArticle = false;

// for modal
$isDescription = $model->priceList->include_description_column;
$isImage = $model->priceList->include_image_column;

$sortAttr = $model->priceList->getSortAttributeName();
$priceListOrders = $model->priceList->getPriceListOrders()
    ->joinWith('productGroup')
    ->orderBy(['production_type' => SORT_DESC, $sortAttr => SORT_ASC])
    ->where(['product_id' => array_keys($model->product)])
    ->all();

?>

<table class="table table-bordered table-hover" style="margin-bottom: 20px;">
    <tr>
        <?php if ($isArticle) : ?>
            <th>Артикул</th>
        <?php endif ?>
        <th>Наименование</th>
        <th style="width: 110px; text-align: center;">Цена (руб)</th>
        <th style="width: 100px; text-align: center;">Количество</th>
        <th style="width: 110px; text-align: center;">Стоимость</th>
    </tr>
    <?php foreach ($priceListOrders as $key => $priceListOrder) : ?>
        <?php $pid = $priceListOrder->product_id; ?>
        <?php $count = ArrayHelper::getValue($model->product, $pid, 0); ?>
        <?php if ($count > 0 && isset($allowedProduct[$pid])) : ?>
            <?php
            //$productListOrder = \common\models\product\Product::findOne($pid);
            //$price = isset($model->price[$pid]) ? $model->price[$pid] * 100 : $product->price_for_sell_with_nds;
            $price = $priceListOrder->price_for_sell;
            $rowPrice = bcmul($price, $count, 2);
            $totalPrice = bcadd($totalPrice, $rowPrice, 2);
            ?>
            <tr class="<?= ($isDescription || $isImage) ? "show-pricelist-modal" : '' ?>" data-key="<?= $key ?>">
                <?php if ($isArticle) : ?>
                    <td><?= $priceListOrder->article ?></td>
                <?php endif ?>
                <td>
                    <?= $priceListOrder->name ?>
                </td>
                <td style="text-align: right;">
                    <?= number_format(($price/100), 2, ',', '&nbsp;') ?>
                </td>
                <td style="text-align: center;">
                    <?= $count ?>
                </td>
                <td style="text-align: right;">
                    <?= number_format(($rowPrice/100), 2, ',', '&nbsp;') ?>
                </td>
            </tr>
        <?php endif; ?>
    <?php endforeach; ?>
    <tr style="font-weight: bold;">
        <td colspan="<?= $isArticle ? 3 : 2; ?>" style="text-align: right;">Итого</td>
        <td style="text-align: center;">
            <?= array_sum($model->product) ?>
        </td>
        <td style="text-align: right;">
            <?= number_format(($totalPrice/100), 2, ',', '&nbsp;') ?>
        </td>
    </tr>
</table>

<?= $this->render('_orders_form', [
    'form' => $form,
    'model' => $model,
]) ?>

<?= $this->render('@frontend/views/price-list/_clickable_product_modal', [
    'priceList' => $model->priceList,
    'priceListOrders' => $priceListOrders,
    'showModalBtn' => '.show-pricelist-modal'
]) ?>