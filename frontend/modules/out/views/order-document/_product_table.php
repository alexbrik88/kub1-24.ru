<?php

use common\components\TextHelper;
use common\models\product\Product;
use frontend\modules\out\models\OutInvoiceForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model frontend\modules\out\models\OutOrderDocumentForm */

$isProduct = $model->priceList->production_type == Product::PRODUCTION_TYPE_GOODS || $model->priceList->production_type == Product::PRODUCTION_TYPE_ALL;

// changing columns
$isName = $model->priceList->include_name_column;
$isArticle = $model->priceList->include_article_column;
$isGroup = $model->priceList->include_product_group_column;
$isReminder = $model->priceList->include_reminder_column;
$isUnit = $model->priceList->include_product_unit_column;
$isPrice = $model->priceList->include_price_column;

// for modal
$isDescription = $model->priceList->include_description_column;
$isImage = $model->priceList->include_image_column;

$jsColumns = [
    'name' => $isName ? '{"orderable": true, "searchable": true},' : '',
    'article' => $isArticle ? 'null,' : '',
    'group' => $isGroup ? 'null,' : '',
    'reminder' => $isReminder ? 'null,' : '',
    'unit' => $isUnit ? '{"orderable": false, "searchable": false},' : '',
    //'description' => $isDescription ? '{"orderable": false, "searchable": false},' : '',
    'price' => $isPrice ? '{"type": "formatted-num", "searchable": false},' : ''
];

$paging = count($model->priceList->priceListOrders) > 10 ? 'true' : 'false';

$productArray = $model->priceList->priceListOrders;

$jsColumn = $isArticle ? 'null,' : '';
$js = <<<JS

// per page
window.productsPerPage = localStorage.getItem("productsPerPage") || 10;
$(document).on('change', '#produc-table_length select', function() {
    localStorage.setItem("productsPerPage", $(this).val());
});
console.log(window.productsPerPage)

var priceValueFormat = function (value) {
    return new Intl.NumberFormat('ru-RU', {minimumFractionDigits: 2, maximumFractionDigits: 2}).format(value);
}
var checkTotalCount = function() {
    var totalCount = 0;
    var totalSum = 0;
    $('#outinvoice-form .product-quantity-input').each(function(){
        var prodCount = Math.max(0, $(this).val() * 1);
        var prodSum = Math.round($(this).data('price') * prodCount);
        //console.log(this);
        totalCount += prodCount;
        totalSum += prodSum;
        $('#outinvoice-form .prod-sum-'+$(this).data('product'))
            .text(priceValueFormat(prodSum/100));
    });
    $('#outinvoice-form .total-count').text(totalCount);
    $('#outinvoice-form .total-sum').text(priceValueFormat(totalSum/100));
    if (totalCount > 0) {
        $('#outinvoice-submit').text('Продолжить');
        $('#outinvoice-submit').prop('disabled', false);
    } else {
        $('#outinvoice-submit').text('Выберите количество');
        $('#outinvoice-submit').prop('disabled', true);
    }
}

$(document).on('click', '.quantity-btn', function(e){
    e.preventDefault();
    var type      = $(this).attr('data-type');
    var input     = $(this).closest('.quantity-box').find('.quantity-input');
    var val = parseInt(input.val());
    if(type == 'minus') {
        val--;
    } else if(type == 'plus') {
        val++;
    }

    input.val(Math.min(input.attr('max'), Math.max(input.attr('min'), val))).change();
});

$(document).on('change', '.quantity-input', function(){
    var box = $(this).closest('.quantity-box');
    var minus = box.find(".quantity-btn.type-minus");
    var plus = box.find(".quantity-btn.type-plus");
    var minVal =  parseInt($(this).attr('min'));
    var maxVal =  parseInt($(this).attr('max'));
    var val = Math.min(maxVal, Math.max(minVal, parseFloat($(this).val().replace(',', '.'))));

    minus.prop('disabled', val == minVal);
    plus.prop('disabled', val == maxVal);

    if ($(this).val() != String(val)) {
        $(this).val(val);
    }
    $($(this).data('target')).val(val);

    checkTotalCount();
});

$(document).on('keydown', '.quantity-input', function(){
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
         // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
         // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

checkTotalCount();

$.extend( $.fn.dataTableExt.oSort, {
    "formatted-num-pre": function ( a ) {
        a = (a === "-" || a === "") ? 0 : a.replace( /[^\d,]/g, "" );
        return parseFloat( a );
    },
    "formatted-num-asc": function ( a, b ) {
        return a - b;
    },
    "formatted-num-desc": function ( a, b ) {
        return b - a;
    }
} );
var prodTable = $("#produc-table").DataTable({
    "paging": $paging,
    "info": false,
    "dom": "t<'row'<'col-sm-12'pl>>",
    "order": [[ 1, "asc" ]],
    "columns": [
        { "orderable": false, "searchable": false },
        {$jsColumns['name']}
        {$jsColumns['article']}
        {$jsColumns['group']}
        {$jsColumns['reminder']}
        {$jsColumns['unit']}
        //{jsColumns['description']}
        {$jsColumns['price']}
        { "orderable": false, "searchable": false },
        { "orderable": false, "searchable": false }
    ],
    "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "Все"]],
    "language": {
        "lengthMenu": "Выводить по _MENU_",
        "decimal": ",",
        "thousands": "&nbsp;",
    },
    "pageLength": window.productsPerPage
});
prodTable.on('order.dt', function () {
    prodTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
}).draw();
$(document).on('keyup paste', '#product-title-search', function() {
    prodTable.search(this.value).draw();
})
JS;

$this->registerJs($js);

$totalWidth = 875;
$width = [
    'num' => 40,
    'name' => 300,
    'article' => 85,
    'group' => 115,
    'reminder' => 85,
    'units' => 85,
    //'description' => 100,
    'price' => 125,
    'quantity' => 150,
    'cost' => 125
];
foreach ($width as $k=>$v) {
    $width[$k] = round($v * 100 / $totalWidth) . '%';
}

?>
<style type="text/css">
/* MAIN WIDTH */
.out-order-document-form {
    width: 975px;
}

table#produc-table {
    width: 100%;
    position: relative;
    margin-bottom: 5px;
    table-layout: fixed;
    border-bottom: 1px solid #ddd;
}
table#produc-table th {
    padding: 10px;
    vertical-align: middle;
    text-align: center;
}
table#produc-table td {
    vertical-align: middle;
    box-sizing: border-box;
}
table#produc-table tfoot td {
    border-left: none;
    border-bottom: none;
    border-top: 1px solid #ddd;
    padding: 8px 10px;
}
table.quantity-box td {
    vertical-align: middle;
    text-align: center;
}
table.quantity-box button.quantity-btn,
table.quantity-box button.quantity-btn:hover {
    margin: 0;
    padding: 6px 10px;
    border-radius: 4px !important;
    background-color: #e3e8ee;
    color: #333333;
}
.dataTables_wrapper .dataTables_paginate .paginate_button,
.dataTables_wrapper .dataTables_paginate .paginate_button:hover,
.dataTables_wrapper .dataTables_paginate .paginate_button.disabled,
.dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover {
    padding: 0;
    border-width: 0;
}
</style>

<div class="row">
    <div class="col-sm-12 col-md-6">
        <div style="line-height:1; padding-top:8px;padding-bottom:5px; font-size: 15px">Данные на <?= date('d.m.Y', $model->priceList->updated_at) ?></div>
    </div>
    <div class="col-sm-12 col-md-6">
        <?php if (count($productArray) > 10) : ?>
            <?= Html::textInput('search', '', [
                'id' => 'product-title-search',
                'class' => 'form-control width-100',
                'placeholder' => 'Поиск по названию' . ($isArticle ? ' и артикулу' : ''),
            ]) ?>
        <?php endif; ?>
    </div>
</div>

<table id="produc-table" class="table table-bordered table-hover" style="table-layout: fixed;">
    <col width="<?= $width['num'] ?>">
    <?php if ($isName): ?><col width="<?= $width['name'] ?>"><?php endif; ?>
    <?php if ($isArticle): ?><col width="<?= $width['article'] ?>"><?php endif; ?>
    <?php if ($isGroup): ?><col width="<?= $width['group'] ?>"><?php endif; ?>
    <?php if ($isReminder): ?><col width="<?= $width['reminder'] ?>"><?php endif; ?>
    <?php if ($isUnit): ?><col width="<?= $width['units'] ?>"><?php endif; ?>
    <?php /* if ($isDescription): ?><col width="<?= $width['description'] ?>"><?php endif; */ ?>
    <?php if ($isPrice): ?><col width="<?= $width['price'] ?>"><?php endif; ?>
    <col width="<?= $width['quantity'] ?>">
    <col width="<?= $width['cost'] ?>">
    <thead>
    <tr>
        <th>№</th>
        <?php if ($isName): ?>
            <th>Наимено&shy;вание</th>
        <?php endif; ?>
        <?php if ($isProduct && $isArticle): ?>
            <th>Артикул</th>
        <?php endif; ?>
        <?php if ($isGroup): ?>
            <th>Группа <?= $isProduct ? 'товаров' : 'услуг'; ?></th>
        <?php endif; ?>
        <?php if ($isProduct && $isReminder): ?>
            <th>Остаток</th>
        <?php endif; ?>
        <?php if ($isUnit): ?>
            <th>Ед. измер-я</th>
        <?php endif; ?>
        <?php /* if ($isProduct && $isDescription): ?>
            <th>Описание</th>
        <?php endif; */ ?>
        <?php if ($isPrice): ?>
            <th>Цена</th>
        <?php endif; ?>
        <th>Количество</th>
        <th>Стоимость</th>
    </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        foreach ($productArray as $key=>$prd) : ?>
            <tr class="produc-table-row <?= ($isDescription || $isImage) ? "show-pricelist-modal" : '' ?>" data-key="<?= $key ?>">
                <td style="text-align: right;">
                    <?= $i++; ?>
                </td>
                <?php if ($isName): ?>
                    <td class="column-product-search">
                        <?= $prd->name; ?>
                    </td>
                <?php endif; ?>
                <?php if ($isArticle) : ?>
                    <td class="column-product-search" style="overflow: hidden;">
                        <?= $prd->article ?>
                    </td>
                <?php endif ?>
                <?php if ($isGroup): ?>
                    <td>
                        <?= $prd->productGroup ? $prd->productGroup->title : '---'; ?>
                    </td>
                <?php endif; ?>
                <?php if ($isProduct && $isReminder): ?>
                    <td style="text-align: right;">
                        <?= ($prd->quantity > 0) ? $prd->quantity : 0; ?>
                    </td>
                <?php endif; ?>
                <?php if ($isUnit): ?>
                    <td style="text-align: right;">
                        <?= $prd->productUnit ? $prd->productUnit->name : '---'; ?>
                    </td>
                <?php endif; ?>
                <?php /* if ($isDescription): ?>
                    <td>
                        <?= $prd->description; ?>
                    </td>
                <?php endif; */ ?>
                <?php if ($isPrice): ?>
                    <td style="text-align: right;">
                        <?php
                        //$price = isset($model->price[$product->id]) ?
                        //         $model->price[$product->id] * 100 :
                        //         $product->price_for_sell_with_nds;
                        $price = $prd->price_for_sell;
                        echo number_format(($price / 100), 2, ',', '&nbsp;');
                        ?>
                    </td>
                <?php endif; ?>
                <td style="padding: 0;" class="td-no-clickable">
                    <?php $inputName = "product[{$prd->product_id}]"; ?>
                    <?php $inputValue = max(0, ArrayHelper::getValue($model->product, $prd->product_id, 0)); ?>
                    <div style="margin: 5px;">
                        <table class="input-group quantity-box" style="width: 100%; margin: 0;">
                            <tr>
                                <td style="padding: 0;">
                                    <?= Html::button('<span class="glyphicon glyphicon-minus"></span>', [
                                        'class' => 'btn btn-default quantity-btn type-minus',
                                        'disabled' => ($inputValue == 0) ? true : false,
                                        'data-type' => 'minus',
                                        'type' => 'button',
                                    ]) ?>
                                </td>
                                <td style="padding: 0 3px;">
                                    <?= Html::textInput("prod[{$prd->product_id}]", $inputValue, [
                                        'class' => 'form-control input-number quantity-input',
                                        'style' => 'text-align: center; border-radius: 4px !important; padding: 6px;',
                                        'min' => 0,
                                        'max' => OutInvoiceForm::MAX_VALUE,
                                        'data-target' => "#data-product-{$prd->product_id}",
                                    ]) ?>
                                </td>
                                <td style="padding: 0;">
                                    <?= Html::button('<span class="glyphicon glyphicon-plus"></span>', [
                                        'class' => 'btn btn-default quantity-btn type-plus',
                                        'disabled' => false,
                                        'data-type' => 'plus',
                                        'type' => 'button',
                                    ]) ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td class="prod-sum-<?=$prd->product_id?>" style="text-align: right;">
                    0
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr class="text-bold">
            <td colspan="<?= 1 + $isName + $isArticle + $isGroup + $isReminder + $isUnit + /* $isDescription + */ $isPrice; ?>">Итого</td>
            <td class="total-count text-center">0</td>
            <td class="total-sum text-right">0</td>
        </tr>
    </tfoot>
</table>

<?php
foreach ($productArray as $prd) {
    $inputValue = max(0, ArrayHelper::getValue($model->product, $prd->product_id, 0));
    $price = $prd->price_for_sell;
    echo Html::activeHiddenInput($model, "product[{$prd->product_id}]", [
        'value' => $inputValue,
        'class' => 'product-quantity-input',
        'data-product' => $prd->product_id,
        'data-price' => $price,
    ]);
}
?>

<?= $this->render('@frontend/views/price-list/_clickable_product_modal', [
    'priceList' => $model->priceList,
    'priceListOrders' => $productArray,
    'showModalBtn' => '.show-pricelist-modal'
]) ?>