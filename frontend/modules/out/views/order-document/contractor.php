<?php

use common\models\company\CompanyType;
use common\models\Contractor;
use frontend\modules\out\models\OutInvoiceForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\out\models\OutOrderDocumentForm */
/* @var $form yii\bootstrap\ActiveForm */

$this->title = 'Реквизиты покупателя';
$this->params['showHeader'] = true;

// Fields params
$is_autocomplete = true;
$is_bik_autocomplete = true;

$serviceSiteUrl = trim(Yii::$app->params['serviceSite'], '/');

$this->registerJs(<<<JS
$(document).on('keyup paste', '#data-legal_inn', function() {
    if (this.value.length >= 10) {
        $('.legal_data_collapse').collapse('show');
    }
})
JS
);

$autocompleteJs = <<<JS
$('#data-legal_inn').suggestions({
    serviceUrl: 'https://dadata.ru/api/v2',
    token: '78497656dfc90c2b00308d616feb9df60c503f51',
    type: 'PARTY',
    count: 10,
    onSelect: function(suggestion) {
        var data = suggestion.data;
        var opf = data.opf.short;
        var type = $("#data-legal_type option:contains('"+opf+"')").val();
        $('#data-legal_inn').val(data.inn);
        $('#data-legal_type').val(type);
        $('#data-legal_kpp').val(data.kpp);
        var address = '';
        if (data.address.data !== null) {
            if (data.address.data.postal_code) {
                address += data.address.data.postal_code;
                address += ', ';
            }
        }
        address += data.address.value;
        $('#data-legal_address').val(address);
        if (opf == 'ИП') {
            $('#data-legal_name').val(data.name ? data.name.full : '');
            $('#data-chief_name').val(data.name ? data.name.full : '');
        } else {
            $('#data-legal_name').val(data.name ? data.name.short : '');
            $('#data-chief_name').val(data.management ? data.management.name : '');
        }
        $('#data-chief_position').val(data.management ? data.management.post : '');
        $('.legal_data_collapse').collapse('show');
    }
});
JS;

$this->registerJs($autocompleteJs);

?>

<div class="out-invoice-form out-order-document-form">

    <?php $form = ActiveForm::begin([
        'id' => 'outinvoice-form',
        'action' => [
            'create',
            'id' => $model->priceList->id,
            'uid' => $model->uid,
            'hash' => $model->hash,
        ],
        'enableClientValidation' => false,
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-8',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?= Html::activeHiddenInput($model, 'view') ?>

    <?= $this->render('_orders_form', [
        'form' => $form,
        'model' => $model,
    ]) ?>

    <?= Html::activeHiddenInput($model, 'email'); ?>
    <?= Html::activeHiddenInput($model, 'phone'); ?>
    <?= Html::activeHiddenInput($model, 'comment'); ?>
    <?= Html::activeHiddenInput($model, 'face_type'); ?>

    <div class="legal-prop-box">
        <?= $form->field($model, 'legal_inn')->textInput([
            'maxlength' => true,
            'placeholder' => ($is_autocomplete) ? 'Автоматическое заполнение по ИНН' : null,
        ])->label('ИНН вашей компании'); ?>
        <div class="legal_data_collapse collapse <?= $model->legal_inn ? 'in' : ''; ?>">
            <?= $form->field($model, 'legal_type')->dropDownList($model->getTypeArray()); ?>
            <?= $form->field($model, 'legal_name')->textInput([
                'maxlength' => true,
                'placeholder' => 'Без ООО / ИП',
            ]); ?>
            <?= $form->field($model, 'legal_kpp')->textInput(['maxlength' => true]); ?>
            <?= $form->field($model, 'legal_address')->textInput(['maxlength' => true]); ?>
            <?= $form->field($model, 'chief_position')->textInput(['maxlength' => true]); ?>
            <?= $form->field($model, 'chief_name')->textInput(['maxlength' => true]); ?>
        </div>
        <?= $form->field($model, 'legal_rs')->textInput([
            'maxlength' => true,
            'placeholder' => 'Для Актов и Товарных накладных',
        ]); ?>

        <?php if ($is_bik_autocomplete) : ?>
            <?= $form->field($model, 'legal_bik')->widget(\common\components\widgets\BikTypeahead::classname(), [
                'remoteUrl' => Url::to(['/dictionary/bik']),
                'related' => [],
            ])->textInput(['placeHolder' => 'Для Актов и Товарных накладных']); ?>
        <?php else : ?>
            <?= $form->field($model, 'legal_bik')->textInput([
                'maxlength' => true,
                'placeholder' => 'Для Актов и Товарных накладных',
            ]); ?>
        <?php endif; ?>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            <i>
                Нажимая на кнопку “Оформить заказ”, вы даете согласие на обработку
                <a target="_blank" href="<?=$serviceSiteUrl;?>/SecurityPolicy/Security_Policy.pdf">
                    персональных данных
                </a>.
            </i>
        </div>
    </div>

    <div class="form-group row action-battons">
        <div class="col-xs-6">
            <?= Html::submitButton('Назад', [
                'class' => 'btn go-back-btn',
                'name' => 'back',
                'value' => 'back',
            ]) ?>
        </div>
        <div class="col-xs-6">
            <?= Html::submitButton('Оформить заказ', [
                'class' => 'btn darkblue pull-right',
                'style' => 'color: #fff; min-width: 120px; text-align: center',
            ]) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
