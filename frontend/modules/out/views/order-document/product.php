<?php

use frontend\modules\out\models\OutInvoiceForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\out\models\OutOrderDocumentForm */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Выберите товар/услугу';
$this->params['showHeader'] = true;

$totalCount = array_sum($model->product);
?>

<div class="out-invoice-form out-order-document-form">

    <?php if ($model->priceList->is_deleted || $model->priceList->is_archive): ?>

        <div class="text-center price-list_deleted">
            Данный прайс-лист не действителен. <br>
            Запросите новый.
        </div>

    <?php else: ?>

        <?php $form = ActiveForm::begin([
            'id' => 'outinvoice-form',
            'action' => [
                'create',
                'id' => $model->priceList->id,
                'uid' => $model->uid,
                'hash' => $model->hash,
            ],
            'enableClientValidation' => false,
        ]); ?>

        <?= Html::activeHiddenInput($model, 'view') ?>

        <?= $form->field($model, 'product', [
            'parts' => [
                '{input}' => $this->render('_product_table', ['model' => $model]),
            ]
        ])->label(false)->render(); ?>

        <?= Html::activeHiddenInput($model, 'email') ?>
        <?= Html::activeHiddenInput($model, 'phone') ?>
        <?= Html::activeHiddenInput($model, 'comment') ?>
        <?= Html::activeHiddenInput($model, 'legal_type') ?>
        <?= Html::activeHiddenInput($model, 'legal_name') ?>
        <?= Html::activeHiddenInput($model, 'legal_inn') ?>
        <?= Html::activeHiddenInput($model, 'legal_kpp') ?>
        <?= Html::activeHiddenInput($model, 'legal_address') ?>
        <?= Html::activeHiddenInput($model, 'legal_rs') ?>
        <?= Html::activeHiddenInput($model, 'legal_bik') ?>
        <?= Html::activeHiddenInput($model, 'chief_position') ?>
        <?= Html::activeHiddenInput($model, 'chief_name') ?>

        <div class="form-group row">
            <div class="col-xs-12">

            </div>
        </div>

        <div class="form-group row action-battons">
            <div class="col-xs-6">
                <?= Html::a('Назад', ['/price-list/out-view', 'uid' => $model->priceList->uid], [
                    'class' => 'btn go-back-btn',
                    'name' => 'back',
                    'value' => 'back',
                ]) ?>
            </div>
            <div class="col-xs-6">
                <?= Html::submitButton($totalCount > 0 ? 'Продолжить' : 'Выберите количество', [
                    'id' => 'outinvoice-submit',
                    'class' => 'btn darkblue pull-right',
                    'style' => 'color: #fff; min-width: 120px; text-align: center',
                    'disabled' => $totalCount > 0 ? false : true,
                ]) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    <?php endif; ?>

</div>
