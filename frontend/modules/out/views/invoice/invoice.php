<?php

use frontend\modules\out\models\OutInvoiceForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\out\models\OutInvoiceForm */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Просмотр счета';
$this->params['showHeader'] = false;
?>

<div class="out-invoice-form" style="width: 675px; overflow: hidden;">

    <div style="border: 0 solid #bbb; margin: -50px -60px 20px;">
    <?= $model->renderInvoice() ?>
    </div>

    <div style="font-family: 'Open Sans', sans-serif; font-weight: normal;">
        <h3 style="margin-bottom: 20px; text-align: left; font-family: 'Open Sans', sans-serif; font-weight: normal;">
            Счет отправлен вам на почту <?= Html::mailto($model->email) ?>
        </h3>

        <?php if ($model->outInvoice->add_comment) : ?>
            <div style="margin-bottom: 20px;">
                <?= Html::encode($model->outInvoice->add_comment) ?>
            </div>
        <?php endif; ?>

        <div class="form-group row">
            <div class="col-xs-12" style="text-align: center;">
                <?= Html::a('Вернуться на сайт', $model->outInvoice->return_url, [
                    'class' => 'btn darkblue',
                    'style' => 'color: #fff; min-width: 120px; text-align: center',
                ]) ?>
            </div>
        </div>
    </div>
</div>
