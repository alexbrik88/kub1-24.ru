<?php

use frontend\modules\out\models\OutInvoiceForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\out\models\OutInvoiceForm */
/* @var $form yii\bootstrap\ActiveForm */
?>

<?php foreach ((array) $model->getOrderList() as $key => $order) : ?>
    <?php foreach ($order->getAttributes() as $name => $value) : ?>
        <?= Html::activeHiddenInput($model, "order_list[{$key}][{$name}]", ['value' => $value]); ?>
    <?php endforeach ?>
<?php endforeach ?>
