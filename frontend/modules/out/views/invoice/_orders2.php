<?php

use frontend\modules\out\models\OutInvoiceForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\out\models\OutInvoiceForm */
/* @var $form yii\bootstrap\ActiveForm */

$allowedProduct = $model->getOutInvoice()->getProducts()->indexBy('id')->all();
$totalPrice = 0;

$isArticle = (boolean) $model->outInvoice->show_article;
?>

<table class="table table-bordered table-hover" style="margin-bottom: 20px;">
    <tr>
        <?php if ($isArticle) : ?>
            <th>Артикул</th>
        <?php endif ?>
        <th>Наименование</th>
        <th style="width: 110px; text-align: center;">Цена (руб)</th>
        <th style="width: 100px; text-align: center;">Количество</th>
        <th style="width: 110px; text-align: center;">Стоимость</th>
    </tr>
    <?php foreach ($model->product as $pid => $count) : ?>
        <?php if ($count > 0 && isset($allowedProduct[$pid])) : ?>
            <?php
            $product = $allowedProduct[$pid];
            $price = isset($model->price[$pid]) ? $model->price[$pid] * 100 : $product->price_for_sell_with_nds;
            $rowPrice = $price * $count;
            $totalPrice += $rowPrice;
            ?>
            <tr>
                <?php if ($isArticle) : ?>
                    <td><?= $product->article ?></td>
                <?php endif ?>
                <td>
                    <?= $product->title ?>
                </td>
                <td style="text-align: right;">
                    <?= number_format(($price/100), 2, ',', '&nbsp;') ?>
                </td>
                <td style="text-align: center;">
                    <?= $count ?>
                </td>
                <td style="text-align: right;">
                    <?= number_format($rowPrice/100, 2, ',', '&nbsp;') ?>
                </td>
            </tr>
        <?php endif; ?>
    <?php endforeach; ?>
    <tr style="font-weight: bold;">
        <td colspan="<?= $isArticle ? 3 : 2; ?>" style="text-align: right;">Итого</td>
        <td style="text-align: center;">
            <?= array_sum($model->product) ?>
        </td>
        <td style="text-align: right;">
            <?= number_format(($totalPrice/100), 2, ',', '&nbsp;') ?>
        </td>
    </tr>
</table>

<?= $this->render('_orders_form2', [
    'form' => $form,
    'model' => $model,
]) ?>
