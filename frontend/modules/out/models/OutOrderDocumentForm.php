<?php

namespace frontend\modules\out\models;

use common\components\DadataClient;
use common\components\date\DateHelper;
use common\components\TextHelper;
use common\components\validators\PhoneValidator;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\dictionary\bik\BikDictionary;
use common\models\document\Invoice;
use common\models\document\OrderDocument;
use common\models\document\status\OrderDocumentStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\product\PriceList;
use common\models\product\Product;
use common\models\product\ProductStore;
use frontend\models\Documents;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\modules\documents\components\OrderDocumentHelper;
use Yii;
use yii\db\Connection;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\validators\UrlValidator;
use yii\web\BadRequestHttpException;

/**
 * Class OutOrderDocumentForm
 * @property PriceList $priceList
 * @package frontend\modules\out\models
 */
class OutOrderDocumentForm extends \yii\base\Model
{
    /**
     * params
     */
    public $send_with_stamp = false;

    const VIEW_PRODUCT = 'product';
    const VIEW_CONTACT = 'contact';
    const VIEW_CONTRACTOR = 'contractor';
    const VIEW_INVOICE = 'invoice';
    const VIEW_DEFAULT = self::VIEW_PRODUCT;

    const MAX_VALUE = 99999;

    const SCENARIO_LOADDATA = 'loaddata';
    const SCENARIO_FIXPRODUCT = 'fixproduct';
    const SCENARIO_FORMDATA = 'formdata';

    public static $queue = [
        1 => self::VIEW_PRODUCT,
        2 => self::VIEW_CONTACT,
        3 => self::VIEW_CONTRACTOR,
        4 => self::VIEW_INVOICE,
    ];

    /**
     * attributes
     */
    public $uid;
    public $hash;
    public $valid_until;
    public $email_content;
    public $attach = [];
    public $email;
    public $phone;
    public $comment;
    public $face_type = Contractor::TYPE_LEGAL_PERSON;
    public $legal_type;
    public $legal_name;
    public $legal_inn;
    public $legal_kpp;
    public $legal_address;
    public $legal_rs;
    public $legal_bik;
    public $chief_position;
    public $chief_name;
    public $product = [];
    public $price = [];
    public $physical_lastname;
    public $physical_firstname;
    public $physical_patronymic;
    public $physical_no_patronymic;
    public $physical_address;

    public $dataErrorStatus = false;
    public $dataErrorText = 'Во время проверки данных произошла ошибка. Попробуйте позже еще раз.';

    protected $_redirect;
    protected $_priceList;
    protected $_view = self::VIEW_DEFAULT;

    public static $attributes = [
        self::VIEW_PRODUCT => [
            'product',
        ],
        self::VIEW_CONTACT => [
            'email',
            'phone',
            'comment',
        ],
        self::VIEW_CONTRACTOR => [
            'legal_type',
            'legal_name',
            'legal_inn',
            'legal_kpp',
            'legal_address',
            'legal_rs',
            'legal_bik',
            'chief_position',
            'chief_name',
            'physical_lastname',
            'physical_firstname',
            'physical_patronymic',
            'physical_no_patronymic',
            'physical_address',
        ],
        self::VIEW_INVOICE => null,
    ];

    /**
     * @return array
     */
    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_LOADDATA => [
                'product',
                'price',
                'valid_until',
                'email_content',
                'attach',
                'email',
                'phone',
                'comment',
                'chief_position',
                'chief_name',
                'legal_type',
                'legal_name',
                'legal_inn',
                'legal_address',
                'legal_kpp',
                'legal_rs',
                'legal_bik',
            ],
            self::SCENARIO_FIXPRODUCT => [
                'view',
                'email',
                'phone',
                'comment',
                'chief_position',
                'chief_name',
                'legal_type',
                'legal_name',
                'legal_inn',
                'legal_address',
                'legal_kpp',
                'legal_rs',
                'legal_bik',
            ],
            self::SCENARIO_FORMDATA => [
                'view',
                'product',
                'price',
                'email',
                'phone',
                'comment',
                'chief_position',
                'chief_name',
                'legal_type',
                'legal_name',
                'legal_inn',
                'legal_address',
                'legal_kpp',
                'legal_rs',
                'legal_bik',
                'physical_lastname',
                'physical_firstname',
                'physical_patronymic',
                'physical_no_patronymic',
                'physical_address',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['view', 'product', 'price', 'valid_until', 'attach'], 'safe'],
            // self::VIEW_PRODUCT
            [['product'], 'filter', 'filter' => function ($value) {
                array_walk($value, function ($val, $key) {
                    $val = in_array($key, $this->priceList->productId) ?
                        min(self::MAX_VALUE, max(0, $val)) : null;
                });

                return array_filter($value);
            }],
            [['price'], 'filter', 'filter' => function ($value) {
                array_walk($value, function ($val, $key) {
                    $val = in_array($key, $this->priceList->productId) ?
                        round(max(0, $val) * 100) / 100 : null;
                });

                return array_filter($value);
            }],
            [['product'], 'validateProduct'],
            //[['price'], 'validatePrice'],
            // self::VIEW_CONTACT
            [['email', 'phone'], 'required'],
            [['email'], 'email'],
            [['email_content'], 'string'],
            [['comment'], 'string', 'max' => 255],
            [['phone'], PhoneValidator::class],
            // self::VIEW_CONTRACTOR
            [['chief_position'], 'string', 'max' => 255],
            [['chief_name'], 'string', 'max' => 45],
            [
                ['chief_name', 'legal_type', 'legal_name', 'legal_inn', 'legal_address'],
                'required', 'when' => function ($model) {
                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON;
                },
            ],
            [['legal_inn', 'legal_kpp', 'legal_rs', 'legal_bik'], 'integer', 'min' => 0],
            [['legal_kpp', 'legal_bik'], 'string',  'length' => 9, 'max' => 9],
            [['legal_rs'], 'string', 'length' => 20, 'max' => 20],
            [['legal_kpp'], 'required', 'when' => function ($model) {
                return ($model->legal_type != CompanyType::TYPE_IP) && ($model->face_type == Contractor::TYPE_LEGAL_PERSON);
            },],
            [['legal_inn'], 'string', 'length' => 10, 'max' => 12, 'when' => function ($model) {
                return ($model->legal_type != CompanyType::TYPE_IP) && ($model->face_type == Contractor::TYPE_LEGAL_PERSON);
            },],
            [['legal_inn'], 'string', 'length' => 12, 'max' => 12, 'when' => function ($model) {
                return ($model->legal_type == CompanyType::TYPE_IP) && ($model->face_type == Contractor::TYPE_LEGAL_PERSON);
            },],

            // self::VIEW_CONTRACTOR_PHYSICAL
            [['physical_lastname', 'physical_firstname'], 'string', 'max' => 45],
            [['physical_address'], 'string', 'max' => 255],
            [
                ['physical_lastname', 'physical_firstname'],
                'required', 'when' => function ($model) {
                    return $model->face_type == Contractor::TYPE_PHYSICAL_PERSON;
                },
            ],
            [
                ['physical_patronymic'],
                'required',
                'when' => function ($model) {
                    return ($model->face_type == Contractor::TYPE_PHYSICAL_PERSON && !$model->physical_no_patronymic);
                },
            ],
            [
                ['physical_no_patronymic'],
                'boolean',
                'when' => function ($model) {
                    return $model->face_type == Contractor::TYPE_PHYSICAL_PERSON;
                },
            ],
        ];
    }

    public function __construct(PriceList $priceList, $config = [])
    {
        $this->_priceList = $priceList;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product' => 'Выберите',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'comment' => 'Комментарии',
            'face_type' => 'Тип плательщика',
            'legal_type' => 'Форма собственности',
            'legal_name' => 'Название',
            'legal_inn' => 'ИНН',
            'legal_kpp' => 'КПП',
            'legal_address' => 'Юридический адрес',
            'legal_rs' => 'Расчетный счет',
            'legal_bik' => 'БИК банка',
            'chief_position' => 'Должность руководителя',
            'chief_name' => 'ФИО руководителя',
            'physical_firstname' => 'Имя',
            'physical_lastname' => 'Фамилия',
            'physical_patronymic' => 'Отчество',
            'physical_no_patronymic' => 'Нет отчества',
            'physical_address' => 'Адрес доставки',
        ];
    }

    /**
     * @inheritdoc
     */
    public function validateProduct($attribute, $params)
    {
        $prodArray = [];
        foreach ($this->$attribute as $key => $value) {
            if (!in_array($key, $this->_priceList->productId)) {
                unset($this->$attribute[$key]);
            } else {
                $prodArray[$key] = min(static::MAX_VALUE, max(0, $value));
            }
        }
        $this->$attribute = $prodArray;

        if (array_sum($this->$attribute) == 0) {
            $this->addError($attribute, 'Необходимо выбрать товар/услугу.');
        }
    }

    /**
     * @inheritdoc
     */
    public function getPriceList()
    {
        return $this->_priceList;
    }

    /**
     * @inheritdoc
     */
    public function setView($value)
    {
        $this->_view = in_array($value, static::$queue) ? $value : static::VIEW_DEFAULT;
    }

    /**
     * @inheritdoc
     */
    public function getView()
    {
        return $this->_view;
    }

    /**
     * @inheritdoc
     */
    public function getPrevView()
    {
        $viewKey = 0;
        foreach (static::$queue as $key => $value) {
            if ($value == $this->view) {
                $viewKey = $key - 1;
                break;
            }
        }

        return isset(static::$queue[$viewKey]) ? static::$queue[$viewKey] : $this->view;
    }

    /**
     * @inheritdoc
     */
    public function getNextView()
    {
        $viewKey = 0;
        foreach (static::$queue as $key => $value) {
            if ($value == $this->view) {
                $viewKey = $key + 1;
                break;
            }
        }

        return isset(static::$queue[$viewKey]) ? static::$queue[$viewKey] : $this->view;
    }

    /**
     * @inheritdoc
     */
    public function getRedirect()
    {
        return $this->_redirect;
    }

    /**
     * @inheritdoc
     */
    public function validate($attributeNames = null, $clearErrors = true)
    {
        foreach (static::$queue as $key => $value) {
            if (parent::validate(static::$attributes[$value])) {
                if ($value == $this->view) {
                    break;
                }
            } else {
                $this->view = $value;

                return false;
            }
        }

        return true;
    }

    /**
     * @return array
     */
    public function getTypeArray()
    {
        $typeArray = CompanyType::find()->inContractor()
            ->select(['name_short', 'id'])
            ->orderBy([
                'IF([[id]] < 5, [[id]], 5)' => SORT_ASC,
                'name_short' => SORT_ASC,
            ])
            ->indexBy('id')
            ->column();

        return ['' => '---'] + $typeArray;
    }

    /**
     * @return string
     */
    public function renderInvoice()
    {
        $model = $this->getInvoice();
        $view = '@frontend/modules/documents/views/invoice/pdf-view';

        return Yii::$app->view->render($view, [
            'model' => $model,
            'ioType' => Documents::IO_TYPE_OUT,
            'addStamp' => $this->send_with_stamp,
        ]);
    }

    /**
     * @return Invoice
     */
    public function getInvoice($saveNew = false)
    {
        $company = $this->priceList->company;
        $contractor = $this->getContractor($saveNew);
        $payDate = $this->valid_until ? date_create($this->valid_until) : null;

        $model = new Invoice([
            'scenario' => 'insert',
            'type' => Documents::IO_TYPE_OUT,
            'document_date' => date(DateHelper::FORMAT_DATE),
            'isAutoinvoice' => false,
            'company_id' => $company->id,
            'contractor_id' => $contractor->id,
            'payment_limit_date' => $payDate ? $payDate->format('Y-m-d') :
                (new \DateTime("+{$contractor->customer_payment_delay} days"))->format('Y-m-d'),
            'agreement' => $contractor->last_basis_document,
            'comment' => null,
            'comment_internal' => 'Покупатель: ' . $this->comment,
            'is_by_out_link' => true,
            'by_out_link_id' => null,
            'document_additional_number' => null,
            'is_additional_number_before' => false,
            'price_precision' => 2,
            'show_article' => $this->priceList->include_article_column,
            'nds_view_type_id' => $company->nds_view_type_id,
            'from_out_invoice' => true,
        ]);

        $model->document_number = (string) Invoice::getNextDocumentNumber($company->id, Documents::IO_TYPE_OUT, null, $model->document_date);

        $model->populateRelation('company', $company);
        $model->populateRelation('contractor', $contractor);

        $orderArray = [];
        $allowedProduct = $this->getPriceList()->getPriceListOrders()->indexBy('product_id')->all();
        foreach ($this->product as $pid => $count) {
            if ($count > 0 && isset($allowedProduct[$pid])) {
                $product = Product::findOne($pid);
                $price = isset($this->price[$pid]) ? $this->price[$pid] : $product->price_for_sell_with_nds / 100;
                $orderArray[] = [
                    'order_id' => null,
                    'product_id' => $product->id,
                    'title' => $product->title,
                    'count' => $count,
                    'price' => $price,
                ];
            }
        }

        $params = [
            'Invoice' => [
                'type' => Documents::IO_TYPE_OUT,
            ],
            'orderArray' => $orderArray,
        ];

        InvoiceHelper::load($model, $params);

        $model->setContractor($contractor);

        if ($saveNew) {
            InvoiceHelper::save($model);
        } else {
            $model->beforeValidate();
            $model->beforeSave(true);
        }

        return $model;
    }

    /**
     * @return OrderDocument
     */
    public function getOrderDocument($saveNew = false)
    {
        $company = $this->priceList->company;
        $contractor = $this->getContractor($saveNew);

        $model = new OrderDocument();
        $model->type = Documents::IO_TYPE_OUT;
        $model->price_list_id = $this->priceList->id;
        $model->company_id = $company->id;
        $model->contractor_id = $contractor->id;
        $model->document_date = date(DateHelper::FORMAT_DATE);
        $model->has_nds = $company->companyTaxationType->osno ?
            OrderDocument::HAS_NDS :
            OrderDocument::HAS_NO_NDS;
        $model->status_id = OrderDocumentStatus::STATUS_NEW;
        $model->document_number = (string)OrderDocument::getNextDocumentNumber($company, null, $model->document_date);
        $model->ship_up_to_date = date(DateHelper::FORMAT_DATE, time() + 1 * 24 * 3600);
        $model->comment_internal = 'Покупатель: ' . $this->comment;
        $model->populateRelation('company', $company);
        $model->populateRelation('contractor', $contractor);

        $store_id = null;
        $orderArray = [];
        $allowedProduct = $this->getPriceList()->getPriceListOrders()->indexBy('product_id')->all();

        $sortAttr = $this->priceList->getSortAttributeName();
        $priceListOrders = $this->priceList->getPriceListOrders()
            ->joinWith('productGroup')
            ->orderBy(['production_type' => SORT_DESC, $sortAttr => SORT_ASC])
            ->where(['product_id' => array_keys($this->product)])
            ->all();

        // discount/markup
        $discountPercent = $markupPercent = 0;
        if ($this->priceList->has_discount_from_sum || $this->priceList->has_markup_from_sum) {
            $totalSum = 0;
            foreach ($priceListOrders as $priceListOrder) {
                $pid = $priceListOrder->product_id;
                $count = ArrayHelper::getValue($this->product, $pid, 0);
                if ($count > 0 && isset($allowedProduct[$pid])) {
                    $totalSum += $priceListOrder->price_for_sell * $count;
                }
            }
            if ($totalSum >= $model->priceList->discount_from_sum && $this->priceList->has_discount_from_sum) {
                $model->has_discount = 1;
                $discountPercent = $model->priceList->discount_from_sum_percent;
            }
            if ($totalSum >= $model->priceList->markup_from_sum && $this->priceList->has_markup_from_sum) {
                $model->has_markup = 1;
                $markupPercent = $model->priceList->markup_from_sum_percent;
            }
        }

        foreach ($priceListOrders as $priceListOrder) {
            $pid = $priceListOrder->product_id;
            $count = ArrayHelper::getValue($this->product, $pid, 0);
            if ($count > 0 && isset($allowedProduct[$pid])) {

                $price = $priceListOrder->price_for_sell / 100;

                $orderArray[] = [
                    'title' => $priceListOrder->name,
                    'model' => 'product',
                    'id' => $priceListOrder->product_id,
                    'count' => $count,
                    'price' => $price,
                    'discount' => $discountPercent,
                    'markup' => $markupPercent,
                    'priceOneWithVat' => $price - ($discountPercent / 100 * $price) + ($markupPercent / 100 * $price)
                ];

                $store_id = 1;

                if (!$store_id) {
                    $productStore = ProductStore::findOne(['product_id' => $priceListOrder->product_id]);
                    $store_id = $productStore->store_id;
                }
            }
        }

        $params = [
            'OrderDocument' => ['store_id' => $store_id],
            'orderArray' => $orderArray,
        ];

        OrderDocumentHelper::load($model, $params);
        $model->setContractor($contractor);

        if ($saveNew) {
            $orderDocumentSaved = Yii::$app->db->transaction(function (Connection $db) use ($model, $params, $discountPercent, $markupPercent) {
                if (OrderDocumentHelper::save($model)) {
                    return true;
                }
                \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);

                $db->transaction->rollBack();

                return false;
            });
        } else {
            $model->beforeValidate();
            $model->beforeSave(true);
        }

        return $model;
    }

    /**
     * @return Contractor
     */
    public function getContractor($saveNew = false)
    {
        if ($this->face_type == Contractor::TYPE_PHYSICAL_PERSON) {
            $model = $this->physical_lastname ? $this->priceList->company->getContractors()
                ->andWhere([
                    'type' => Contractor::TYPE_CUSTOMER,
                    'status' => Contractor::ACTIVE,
                    'is_deleted' => Contractor::NOT_DELETED,
                    'physical_lastname' => $this->physical_lastname,
                    'physical_firstname' => $this->physical_firstname,
                    'physical_patronymic' => $this->physical_patronymic,
                ])
                ->one() : null;
        } else {
            $model = $this->legal_inn ? $this->priceList->company->getContractors()
                ->andWhere([
                    'type' => Contractor::TYPE_CUSTOMER,
                    'status' => Contractor::ACTIVE,
                    'is_deleted' => Contractor::NOT_DELETED,
                    'ITN' => $this->legal_inn,
                ])
                ->andFilterWhere([
                    'PPC' => $this->legal_kpp,
                ])
                ->one() : null;
        }

        if ($model === null) {
            $bank = $this->legal_bik ? BikDictionary::findOne([
                'bik' => $this->legal_bik,
                'is_active' => true,
            ]) : null;
            $model = new Contractor([
                'type' => Contractor::TYPE_CUSTOMER,
                'status' => Contractor::ACTIVE,
                'company_id' => $this->priceList->company->id,
                'payment_delay' => 10,
                'face_type' => $this->face_type,
                'company_type_id' => $this->legal_type ? : null,
                'name' => $this->face_type == Contractor::TYPE_LEGAL_PERSON ? $this->legal_name : '',
                'legal_address' => $this->face_type == Contractor::TYPE_LEGAL_PERSON ? $this->legal_address : '',
                'ITN' => $this->face_type == Contractor::TYPE_LEGAL_PERSON ? $this->legal_inn : '',
                'PPC' => $this->face_type == Contractor::TYPE_LEGAL_PERSON ? $this->legal_kpp : '',
                'director_post_name' => (string) $this->face_type == Contractor::TYPE_LEGAL_PERSON ? $this->chief_position : '',
                'director_name' => $this->face_type == Contractor::TYPE_LEGAL_PERSON ? $this->chief_name : '',
                'current_account' => $this->face_type == Contractor::TYPE_LEGAL_PERSON ? $this->legal_rs : '',
                'BIC' => $bank !== null ? $bank->bik : '',
                'bank_name' => $bank !== null ? $bank->name : '',
                'bank_city' => $bank !== null ? $bank->city : '',
                'corresp_account' => $bank !== null ? $bank->ks : '',
                'director_email' => $this->email,
                'director_phone' => $this->phone,
                'chief_accountant_is_director' => true,
                'contact_is_director' => true,
                'physical_lastname' => $this->face_type == Contractor::TYPE_PHYSICAL_PERSON ? $this->physical_lastname : '',
                'physical_firstname' => $this->face_type == Contractor::TYPE_PHYSICAL_PERSON ? $this->physical_firstname : '',
                'physical_patronymic' => $this->face_type == Contractor::TYPE_PHYSICAL_PERSON ? $this->physical_patronymic : '',
                'physical_no_patronymic' => $this->face_type == Contractor::TYPE_PHYSICAL_PERSON ? $this->physical_no_patronymic : '',
                'physical_address' => $this->face_type == Contractor::TYPE_PHYSICAL_PERSON ? $this->physical_address : '',
            ]);

            if ($saveNew) {
                $model->save();
            } else {
                $model->beforeSave(true);
            }
        }

        return $model;
    }

    /**
     * @return EmployeeCompany
     */
    public function getSender()
    {
        $query = $this->priceList->company->getEmployeeCompanies()
            ->joinWith('employee', false)
            ->andWhere([
                'employee.is_active' => Employee::ACTIVE,
                'employee.is_deleted' => Employee::NOT_DELETED,
                'employee_company.is_working' => true,
                'employee_company.employee_role_id' => EmployeeRole::ROLE_CHIEF,
            ])
            ->orderBy(['employee_company.created_at' => SORT_ASC]);

        return $query->one();
    }

    /**
     * @return boolean
     */
    public function sendOrderDocument()
    {
        /** @var OrderDocument $orderDocument */
        $orderDocument = $this->getOrderDocument(true);
        if (!$orderDocument->isNewRecord) {
            $this->_redirect = [
                '/documents/order-document/out-view',
                'uid' => $orderDocument->uid,
            ];
            $sender = $this->getSender();
            $addHtml = $this->email_content;

            if ($orderDocument->sendAsEmail($sender, $this->email, null, null, $this->send_with_stamp, $addHtml /*, $attach*/)) {

                return true;
            }
        }

        return false;
    }

    /**
     * @return boolean
     */
    public function sendInvoice()
    {
        $invoice = $this->getInvoice(true);
        if (!$invoice->isNewRecord) {
            $this->_redirect = [
                '/documents/invoice/out-view',
                'uid' => $invoice->uid,
            ];
            $sender = $this->getSender();
            $addHtml = $this->email_content;
            $attach = [];
            if ($this->attach) {
                foreach ((array) $this->attach as $file) {
                    if (!empty($file['file_name']) && is_string($file['file_name']) && !empty($file['file_url'])) {
                        $fileName = $file['file_name'];
                        $validator = new UrlValidator();
                        if ($validator->validate($file['file_url'])) {
                            $content = file_get_contents($file['file_url']);
                            $finfo = new \finfo(FILEINFO_MIME_TYPE);
                            if ($content && ($contentType = $finfo->buffer($content))) {
                                $attach[] = [
                                    'content' => $content,
                                    'fileName' => $fileName,
                                    'contentType' => $contentType,
                                ];
                            }
                        }
                    }
                }
            }

            if ($invoice->sendAsEmail($sender, $this->email, null, null, $this->send_with_stamp, $addHtml, $attach)) {

                return true;
            }
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function checkOrderData()
    {
        $request = Yii::$app->getRequest();
        $data = array_merge($request->get(), $request->post());
        unset($data['id'], $data['uid'], $data['hash']);
        if ($data && !isset($data[$request->csrfParam])) {
            $this->scenario = static::SCENARIO_LOADDATA;
            $this->load($data, '');
            $this->load($data);
            $this->validate();
            if ($this->product && $this->price && array_keys($this->product) == array_keys($this->price)) {
                $this->view = static::VIEW_CONTACT;
                $this->scenario = static::SCENARIO_FIXPRODUCT;
            }
        }

        if ($this->dataErrorStatus) {
            throw new BadRequestHttpException($this->dataErrorText);
        }

        if ($this->scenario != static::SCENARIO_FIXPRODUCT) {
            $this->scenario = static::SCENARIO_FORMDATA;
        }

        $this->clearErrors();
    }

    /**
     * @inheritdoc
     */
    public function loadContractorData()
    {
        if ($this->legal_inn && $this->getPriceList()->is_autocomplete) {
            $data = null;
            $dadata = new DadataClient;
            $suggestions = (array) ArrayHelper::getValue($dadata->getCompany($this->legal_inn, $this->legal_kpp), 'suggestions');
            if ($suggestions) {
                foreach ($suggestions as $item) {
                    if (isset($item['value'], $item['data']['state']['status']) && $item['data']['state']['status'] == 'ACTIVE') {
                        if (empty($kpp) || (isset($item['data']['kpp']) && $kpp == $item['data']['kpp'])) {
                            $data = $item['data'];
                            break;
                        }
                    }
                }
            }
            if ($data !== null) {
                $management = (array) ArrayHelper::getValue($data, 'management');
                $opf = (array) ArrayHelper::getValue($data, 'opf');
                $typeId = CompanyType::find()->select('id')->where([
                    'name_short' => ArrayHelper::getValue($opf, 'short', ''),
                ])->scalar();
                $isIp = $typeId == CompanyType::TYPE_IP;
                $name = (array) ArrayHelper::getValue($data, 'name');

                $chiefName = $isIp ? ArrayHelper::getValue($name, 'full') : ArrayHelper::getValue($management, 'name');

                $address = (array) ArrayHelper::getValue($data, 'address');
                $addressData = (array) ArrayHelper::getValue($address, 'data');
                $adresItems = [
                    ArrayHelper::getValue($addressData, 'postal_code'),
                    ArrayHelper::getValue($address, 'value'),
                ];
                $addresString = implode(', ', array_filter($adresItems));

                $this->legal_type = $typeId;
                $this->legal_kpp = ArrayHelper::getValue($data, 'kpp');
                $this->legal_name = $isIp ? TextHelper::nameShort($chiefName) : ArrayHelper::getValue($name, 'short');
                $this->legal_address = implode(', ', array_filter($adresItems));
                $this->chief_position = $isIp ? '' : ArrayHelper::getValue($management, 'post');
                $this->chief_name = $chiefName;
            }
        }
    }

    /**
     * Returns the form name that this model class should use.
     *
     * @return string the form name of this model class.
     */
    public function formName()
    {
        return 'data';
    }
}
