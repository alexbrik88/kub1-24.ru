<?php

namespace frontend\modules\out\models;

use common\models\address\Country;
use common\models\product\Product;
use common\models\product\ProductUnit;
use common\models\document\Order;
use common\models\TaxRate;
use Yii;

/**
 * Class OutInvoiceForm
 * @package frontend\modules\out\models
 */
class OrderModel extends \yii\base\Model
{
    const TYPE_PRODUCT = 'PRODUCT';
    const TYPE_SERVICE = 'SERVICE';
    const TYPE_DEFAULT = self::TYPE_PRODUCT;

    const UNIT_CODE_DEFAULT = 796;

    /**
     * attributes
     */
    public $number;
    public $type = self::TYPE_DEFAULT;
    public $article;
    public $title;
    public $unit_code = self::UNIT_CODE_DEFAULT;
    public $quantity;
    public $price;
    public $nds;

    protected $_outInvoiceForm;
    protected $_product;
    protected $_unit_id;

    /**
     * Returns the form name that this model class should use.
     *
     * @return string the form name of this model class.
     */
    public function formName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'trim'],
            [['price', 'quantity'], 'filter', 'filter' => function ($value) {
                return str_replace(",", ".", $value);
            },],
            [['type'], 'filter', 'filter' => function ($value) {
                return strtoupper($value);
            },],
            [['type', 'title', 'unit_code', 'quantity', 'price'], 'required'],
            [['type'], 'in', 'range' => [self::TYPE_PRODUCT, self::TYPE_SERVICE]],
            [['title'], 'string'],
            [
                ['unit_code'], 'exist', 'skipOnError' => true,
                'targetClass' => ProductUnit::class,
                'targetAttribute' => ['unit_code' => 'code_okei'],
            ],
            [
                ['nds'], 'safe',
            ],
            [
                ['price'], 'number',
                'numberPattern' => '/^\d{1,20}(\.\d{0,2})?$/',
                'min' => Order::MIN_PRICE,
                'max' => Order::MAX_PRICE,
            ],
            [
                ['quantity'], 'number',
                'numberPattern' => '/^\d{1,10}(\.\d{0,10})?$/',
                'min' => Order::MIN_QUANTITY,
                'max' => Order::MAX_QUANTITY,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type' => 'Тип (товар/услуга)',
            'title' => 'Наименование',
            'unit_id' => 'Единица измерения',
            'quantity' => 'Количество',
            'price' => 'Цена за единицу',
        ];
    }

    /**
     * @inheritdoc
     */
    public function setOutInvoiceForm(OutInvoiceForm $value)
    {
        return $this->_outInvoiceForm = $value;
    }

    /**
     * @inheritdoc
     */
    public function getOutInvoiceForm()
    {
        return $this->_outInvoiceForm;
    }

    /**
     * @inheritdoc
     */
    public function getCompany()
    {
        return $this->getOutInvoiceForm()->getOutInvoice()->company;
    }

    /**
     * @inheritdoc
     */
    public function getNdsId()
    {
        if ($this->nds !== null || $this->nds !== '') {
            $nds = $this->nds / 100;
            if ($rate = TaxRate::findOne(['rate' => $nds])) {
                return $rate->id;
            }
        }

        return $this->getCompany()->companyTaxationType->osno ? (date('Y') > 2018 ? TaxRate::RATE_20 : TaxRate::RATE_18) : TaxRate::RATE_WITHOUT;
    }

    /**
     * @inheritdoc
     */
    public function getProductionType()
    {
        return $this->type == self::TYPE_SERVICE ? Product::PRODUCTION_TYPE_SERVICE : Product::PRODUCTION_TYPE_GOODS;
    }

    /**
     * @inheritdoc
     */
    public function getProduct()
    {
        if ($this->_product === null) {
            $this->_product = Product::find()->andWhere([
                'company_id' => $this->getCompany()->id,
                'production_type' => $this->getProductionType(),
                'product_unit_id' => $this->getUnitId(),
                'title' => $this->title,
                'status' => Product::ACTIVE,
                'is_deleted' => false,
            ])->one();
            if ($this->_product === null) {
                $this->_product = new Product([
                    'creator_id' => $this->getOutInvoiceForm()->getOutInvoice()->created_by,
                    'company_id' => $this->getCompany()->id,
                    'production_type' => $this->getProductionType(),
                    'product_unit_id' => $this->getUnitId(),
                    'title' => $this->title,
                    'code' => Product::DEFAULT_VALUE,
                    'box_type' => Product::DEFAULT_VALUE,
                    'count_in_place' => Product::DEFAULT_VALUE,
                    'place_count' => Product::DEFAULT_VALUE,
                    'mass_gross' => Product::DEFAULT_VALUE,
                    'has_excise' => 0,
                    'price_for_sell_nds_id' => $this->getNdsId(),
                    'price_for_sell_with_nds' => bcmul($this->price, 100),
                    'country_origin_id' => Country::COUNTRY_WITHOUT,
                    'customs_declaration_number' => Product::DEFAULT_VALUE,
                ]);
            }
        }

        return $this->_product;
    }

    /**
     * @inheritdoc
     */
    public function getUnitId()
    {
        if ($this->_unit_id === null) {
            $query = ProductUnit::find()->select('id')->where([
                'code_okei' => $this->unit_code,
            ]);
            $this->_unit_id = ($unit = $query->one()) ? $unit->id : ProductUnit::UNIT_COUNT;
        }

        return $this->_unit_id;
    }

    /**
     * @inheritdoc
     */
    public function getOrderData()
    {
        return [
            'number' => $this->number,
            'product_id' => $this->getProduct()->id,
            'unit_id' => $this->getUnitId(),
            'quantity' => $this->quantity,
            'price' => $this->price,
        ];
    }
}
