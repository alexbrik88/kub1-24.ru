<?php

namespace frontend\modules\out\models;

use common\components\date\DateHelper;
use common\components\validators\PhoneValidator;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\Invoice;
use frontend\models\Documents;
use frontend\modules\documents\components\InvoiceHelper;
use Yii;

/**
 * Class OutInvoiceForm2
 * @package frontend\modules\out\models
 */
class OutInvoiceForm2 extends OutInvoiceForm
{
    protected $_view = self::VIEW_PRODUCT;

    /**
     * attributes
     */
    public $product = [];
    public $price = [];

    public static $queue = [
        1 => self::VIEW_PRODUCT,
        2 => self::VIEW_CONTACT,
        3 => self::VIEW_CONTRACTOR,
        4 => self::VIEW_INVOICE,
    ];

    public static $attributes = [
        self::VIEW_PRODUCT => [
            'product',
        ],
        self::VIEW_CONTACT => [
            'email',
            'phone',
            'comment',
        ],
        self::VIEW_CONTRACTOR => [
            'legal_type',
            'legal_name',
            'legal_inn',
            'legal_kpp',
            'legal_address',
            'legal_rs',
            'legal_bik',
            'chief_position',
            'chief_name',
        ],
        self::VIEW_INVOICE => null,
    ];

    /**
     * @return array
     */
    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_LOADDATA => [
                'product',
                'price',
                'valid_until',
                'email_content',
                'attach',
                'email',
                'phone',
                'comment',
                'chief_position',
                'chief_name',
                'legal_type',
                'legal_name',
                'legal_inn',
                'legal_address',
                'legal_kpp',
                'legal_rs',
                'legal_bik',
            ],
            self::SCENARIO_FIXPRODUCT => [
                'view',
                'email',
                'phone',
                'comment',
                'chief_position',
                'chief_name',
                'legal_type',
                'legal_name',
                'legal_inn',
                'legal_address',
                'legal_kpp',
                'legal_rs',
                'legal_bik',
            ],
            self::SCENARIO_FORMDATA => [
                'view',
                'product',
                'price',
                'email',
                'phone',
                'comment',
                'chief_position',
                'chief_name',
                'legal_type',
                'legal_name',
                'legal_inn',
                'legal_address',
                'legal_kpp',
                'legal_rs',
                'legal_bik',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'trim'],
            [['view', 'product', 'price', 'valid_until', 'attach'], 'safe'],
            // self::VIEW_PRODUCT
            [['product'], 'filter', 'filter' => function ($value) {
                array_walk($value, function ($val, $key) {
                    $val = in_array($key, $this->outInvoice->productId) ?
                            min(self::MAX_VALUE, max(0, $val)) : null;
                });

                return array_filter($value);
            }],
            [['price'], 'filter', 'filter' => function ($value) {
                array_walk($value, function ($val, $key) {
                    $val = in_array($key, $this->outInvoice->productId) ?
                            round(max(0, $val) * 100) / 100 : null;
                });

                return array_filter($value);
            }],
            [['product'], 'validateProduct'],
            //[['price'], 'validatePrice'],
            // self::VIEW_CONTACT
            [['email', 'phone'], 'required'],
            [['email'], 'email'],
            [['email_content'], 'string'],
            [['comment'], 'string', 'max' => 255],
            [['phone'], PhoneValidator::class],
            // self::VIEW_CONTRACTOR
            [['chief_position'], 'string', 'max' => 255],
            [['chief_name'], 'string', 'max' => 45],
            [
                ['chief_name', 'legal_type', 'legal_name', 'legal_inn', 'legal_address'],
                'required', 'when' => function ($model) {
                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON;
                },
            ],
            [['legal_inn', 'legal_kpp', 'legal_rs', 'legal_bik'], 'integer', 'min' => 0],
            [['legal_kpp', 'legal_bik'], 'string',  'length' => 9, 'max' => 9],
            [['legal_rs'], 'string', 'length' => 20, 'max' => 20],
            [['legal_kpp'], 'required', 'when' => function ($model) {
                return ($model->legal_type != CompanyType::TYPE_IP) && ($model->face_type == Contractor::TYPE_LEGAL_PERSON);
            },],
            [['legal_inn'], 'string', 'length' => 10, 'max' => 12, 'when' => function ($model) {
                return ($model->legal_type != CompanyType::TYPE_IP) && ($model->face_type == Contractor::TYPE_LEGAL_PERSON);
            },],
            [['legal_inn'], 'string', 'length' => 12, 'max' => 12, 'when' => function ($model) {
                return ($model->legal_type == CompanyType::TYPE_IP) && ($model->face_type == Contractor::TYPE_LEGAL_PERSON);
            },],
        ];
    }

    /**
     * @inheritdoc
     */
    public function validateProduct($attribute, $params)
    {
        $productId = (array) $this->outInvoice->productId;
        $prodArray = [];
        foreach ($this->$attribute as $key => $value) {
            if (!in_array($key, $productId)) {
                unset($this->$attribute[$key]);
            } else {
                $prodArray[$key] = min(self::MAX_VALUE, max(0, $value));
            }
        }
        $this->$attribute = $prodArray;

        if (array_sum($this->$attribute) == 0) {
            $this->addError($attribute, 'Необходимо выбрать товар/услугу.');
        }
    }

    /**
     * @return Invoice
     */
    public function getInvoice($saveNew = false)
    {
        $company = $this->outInvoice->company;
        $contractor = $this->getContractor($saveNew);
        $payDate = $this->valid_until ? date_create($this->valid_until) : null;

        $model = new Invoice([
            'scenario' => 'insert',
            'type' => Documents::IO_TYPE_OUT,
            'document_date' => date(DateHelper::FORMAT_DATE),
            'isAutoinvoice' => false,
            'company_id' => $company->id,
            'contractor_id' => $contractor->id,
            'payment_limit_date' => $payDate ? $payDate->format('Y-m-d') :
                                    (new \DateTime("+{$contractor->customer_payment_delay} days"))->format('Y-m-d'),
            'agreement' => $contractor->last_basis_document,
            'comment' => $this->outInvoice->invoice_comment,
            'comment_internal' => 'Покупатель: ' . $this->comment,
            'is_by_out_link' => true,
            'by_out_link_id' => $this->outInvoice->id,
            'document_additional_number' => $this->outInvoice->additional_number,
            'is_additional_number_before' => $this->outInvoice->is_additional_number_before,
            'price_precision' => 2,
            'show_article' => $this->outInvoice->show_article,
            'nds_view_type_id' => $company->nds_view_type_id,
            'from_out_invoice' => true,
            'from_demo_out_invoice' => $this->isDemo
        ]);

        $model->document_number = (string) Invoice::getNextDocumentNumber($company->id, Documents::IO_TYPE_OUT, null, $model->document_date);

        if ($this->isDemo) {
            $model->document_additional_number .= ' ДЕМО ';
        }

        $model->populateRelation('company', $company);
        $model->populateRelation('contractor', $contractor);

        $orderArray = [];
        $allowedProduct = $this->outInvoice->getProducts()->indexBy('id')->all();
        foreach ($this->product as $pid => $count) {
            if ($count > 0 && isset($allowedProduct[$pid])) {
                $product = $allowedProduct[$pid];
                $price = isset($this->price[$pid]) ? $this->price[$pid] : $product->price_for_sell_with_nds / 100;
                $orderArray[] = [
                    'order_id' => null,
                    'product_id' => $product->id,
                    'title' => $product->title,
                    'count' => $count,
                    'price' => $price,
                ];
            }
        }

        $params = [
            'Invoice' => [
                'type' => Documents::IO_TYPE_OUT,
            ],
            'orderArray' => $orderArray,
        ];

        InvoiceHelper::load($model, $params);

        $model->setContractor($contractor);

        if ($saveNew) {
            if (!InvoiceHelper::save($model)) {
                \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);
            }
        } else {
            $model->beforeValidate();
            $model->beforeSave(true);
        }

        return $model;
    }
}
