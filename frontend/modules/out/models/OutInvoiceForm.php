<?php

namespace frontend\modules\out\models;

use common\components\DadataClient;
use common\components\TextHelper;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\dictionary\bik\BikDictionary;
use common\models\document\Invoice;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\out\OutInvoice;
use frontend\models\Documents;
use common\components\curl\Curl;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\validators\UrlValidator;
use yii\web\BadRequestHttpException;

/**
 * Class OutInvoiceForm
 * @package frontend\modules\out\models
 */
class OutInvoiceForm extends \yii\base\Model
{
    public $isDemo;

    const VIEW_PRODUCT = 'product';
    const VIEW_CONTACT = 'contact';
    const VIEW_CONTRACTOR = 'contractor';
    const VIEW_INVOICE = 'invoice';
    const VIEW_DEFAULT = self::VIEW_PRODUCT;

    const MAX_VALUE = 99999;

    const SCENARIO_LOADDATA = 'loaddata';
    const SCENARIO_FIXPRODUCT = 'fixproduct';
    const SCENARIO_FORMDATA = 'formdata';

    /**
     * attributes
     */
    public $uid;
    public $hash;
    public $valid_until;
    public $email_content;
    public $attach = [];
    public $email;
    public $phone;
    public $comment;
    public $face_type = Contractor::TYPE_LEGAL_PERSON;
    public $legal_type;
    public $legal_name;
    public $legal_inn;
    public $legal_kpp;
    public $legal_address;
    public $legal_rs;
    public $legal_bik;
    public $chief_position;
    public $chief_name;
    public $currency;

    public $dataErrorStatus = false;
    public $dataErrorText = 'Во время проверки данных произошла ошибка. Попробуйте позже еще раз.';

    protected $_redirect;
    protected $_outInvoice;
    protected $_view = self::VIEW_DEFAULT;

    public function __construct(OutInvoice $outInvoice, $config = [])
    {
        $this->_outInvoice = $outInvoice;

        parent::__construct($config);
    }
    /**
     * Returns the form name that this model class should use.
     *
     * @return string the form name of this model class.
     */
    public function formName()
    {
        return 'data';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product' => 'Выберите',
            'email' => 'E-mail для отправки счета',
            'phone' => 'Телефон',
            'comment' => 'Комментарии',
            'face_type' => 'Тип плательщика',
            'legal_type' => 'Форма собственности',
            'legal_name' => 'Название',
            'legal_inn' => 'ИНН',
            'legal_kpp' => 'КПП',
            'legal_address' => 'Юридический адрес',
            'legal_rs' => 'Расчетный счет',
            'legal_bik' => 'БИК банка',
            'chief_position' => 'Должность руководителя',
            'chief_name' => 'ФИО руководителя',
        ];
    }

    /**
     * @inheritdoc
     */
    public function validateProduct($attribute, $params)
    {
        $productId = (array) $this->outInvoice->productId;
        $prodArray = [];
        foreach ($this->$attribute as $key => $value) {
            if (!in_array($key, $productId)) {
                unset($this->$attribute[$key]);
            } else {
                $prodArray[$key] = min(static::MAX_VALUE, max(0, $value));
            }
        }
        $this->$attribute = $prodArray;

        if (array_sum($this->$attribute) == 0) {
            $this->addError($attribute, 'Необходимо выбрать товар/услугу.');
        }
    }

    /**
     * @inheritdoc
     */
    public function getOutInvoice()
    {
        return $this->_outInvoice;
    }

    /**
     * @inheritdoc
     */
    public function setView($value)
    {
        $this->_view = in_array($value, static::$queue) ? $value : static::VIEW_DEFAULT;
    }

    /**
     * @inheritdoc
     */
    public function getView()
    {
        return $this->_view;
    }

    /**
     * @inheritdoc
     */
    public function getPrevView()
    {
        $viewKey = 0;
        foreach (static::$queue as $key => $value) {
            if ($value == $this->view) {
                $viewKey = $key - 1;
                break;
            }
        }

        return isset(static::$queue[$viewKey]) ? static::$queue[$viewKey] : $this->view;
    }

    /**
     * @inheritdoc
     */
    public function getNextView()
    {
        $viewKey = 0;
        foreach (static::$queue as $key => $value) {
            if ($value == $this->view) {
                $viewKey = $key + 1;
                break;
            }
        }

        return isset(static::$queue[$viewKey]) ? static::$queue[$viewKey] : $this->view;
    }

    /**
     * @inheritdoc
     */
    public function getRedirect()
    {
        return $this->_redirect;
    }

    /**
     * @inheritdoc
     */
    public function validate($attributeNames = null, $clearErrors = true)
    {
        foreach (static::$queue as $key => $value) {
            if (parent::validate(static::$attributes[$value])) {
                if ($value == $this->view) {
                    break;
                }
            } else {
                file_put_contents(
                    Yii::getAlias('@runtime/logs/OutInvoiceForm-lastalidationErrors.log'),
                    var_export($this->errors, true)
                );
                $this->view = $value;

                return false;
            }
        }

        return true;
    }

    /**
     * @return array
     */
    public function getTypeArray()
    {
        $typeArray = CompanyType::find()->inContractor()
            ->select(['name_short', 'id'])
            ->orderBy([
                'IF([[id]] < 5, [[id]], 5)' => SORT_ASC,
                'name_short' => SORT_ASC,
            ])
            ->indexBy('id')
            ->column();

        return ['' => '---'] + $typeArray;
    }

    /**
     * @return string
     */
    public function renderInvoice()
    {
        $model = $this->getInvoice();
        $view = '@frontend/modules/documents/views/invoice/pdf-view';

        return Yii::$app->view->render($view, [
            'model' => $model,
            'ioType' => Documents::IO_TYPE_OUT,
            'addStamp' => $this->outInvoice->send_with_stamp,
        ]);
    }

    /**
     * @return Invoice
     */
    public function getInvoice($saveNew = false)
    {
        return new Invoice;
    }

    /**
     * @return Contractor
     */
    public function getContractor($saveNew = false)
    {
        $model = $this->legal_inn ? $this->outInvoice->company->getContractors()
            ->andWhere([
                'type' => Contractor::TYPE_CUSTOMER,
                'status' => Contractor::ACTIVE,
                'is_deleted' => Contractor::NOT_DELETED,
                'ITN' => $this->legal_inn,
            ])
            ->andFilterWhere([
                'PPC' => $this->legal_kpp,
            ])
            ->one() : null;

        if ($model === null) {
            $bank = $this->legal_bik ? BikDictionary::findOne([
                'bik' => $this->legal_bik,
                'is_active' => true,
            ]) : null;
            $model = new Contractor([
                'type' => Contractor::TYPE_CUSTOMER,
                'status' => Contractor::ACTIVE,
                'company_id' => $this->outInvoice->company->id,
                'payment_delay' => 10,
                'face_type' => $this->face_type,
                'company_type_id' => $this->legal_type ? : null,
                'name' => $this->face_type == Contractor::TYPE_LEGAL_PERSON ? $this->legal_name : '',
                'legal_address' => $this->face_type == Contractor::TYPE_LEGAL_PERSON ? $this->legal_address : '',
                'ITN' => $this->face_type == Contractor::TYPE_LEGAL_PERSON ? $this->legal_inn : '',
                'PPC' => $this->face_type == Contractor::TYPE_LEGAL_PERSON ? $this->legal_kpp : '',
                'director_post_name' => (string) $this->face_type == Contractor::TYPE_LEGAL_PERSON ? $this->chief_position : '',
                'director_name' => $this->face_type == Contractor::TYPE_LEGAL_PERSON ? $this->chief_name : '',
                'current_account' => $this->face_type == Contractor::TYPE_LEGAL_PERSON ? $this->legal_rs : '',
                'BIC' => $bank !== null ? $bank->bik : '',
                'bank_name' => $bank !== null ? $bank->name : '',
                'bank_city' => $bank !== null ? $bank->city : '',
                'corresp_account' => $bank !== null ? $bank->ks : '',
                'director_email' => $this->email,
                'director_phone' => $this->phone,
                'chief_accountant_is_director' => true,
                'contact_is_director' => true,
                'physical_lastname' => $this->face_type == Contractor::TYPE_PHYSICAL_PERSON ? $this->physical_lastname : '',
                'physical_firstname' => $this->face_type == Contractor::TYPE_PHYSICAL_PERSON ? $this->physical_firstname : '',
                'physical_patronymic' => $this->face_type == Contractor::TYPE_PHYSICAL_PERSON ? $this->physical_patronymic : '',
                'physical_address' => $this->face_type == Contractor::TYPE_PHYSICAL_PERSON ? $this->physical_address : '',
            ]);

            if ($saveNew) {
                $model->save();
            } else {
                $model->beforeSave(true);
            }
        }

        return $model;
    }

    /**
     * @return Employee
     */
    public function getSender()
    {
        $query = $this->outInvoice->company->getEmployeeCompanies()
            ->joinWith('employee', false)
            ->andWhere([
                'employee.is_active' => Employee::ACTIVE,
                'employee.is_deleted' => Employee::NOT_DELETED,
                'employee_company.is_working' => true,
                'employee_company.employee_role_id' => EmployeeRole::ROLE_CHIEF,
            ])
            ->orderBy(['employee_company.created_at' => SORT_ASC]);

        return $query->one();
    }

    /**
     * @return boolean
     */
    public function sendInvoice()
    {
        $invoice = $this->getInvoice(true);
        if (!$invoice->isNewRecord) {
            $this->_redirect = [
                '/documents/invoice/out-view',
                'uid' => $invoice->uid,
            ];
            $sender = $this->getSender();
            $addHtml = $this->email_content;
            $attach = [];
            if ($this->attach) {
                foreach ((array) $this->attach as $file) {
                    if (!empty($file['file_name']) && is_string($file['file_name']) && !empty($file['file_url'])) {
                        $fileName = $file['file_name'];
                        $validator = new UrlValidator();
                        if ($validator->validate($file['file_url'])) {
                            $content = file_get_contents($file['file_url']);
                            $finfo = new \finfo(FILEINFO_MIME_TYPE);
                            if ($content && ($contentType = $finfo->buffer($content))) {
                                $attach[] = [
                                    'content' => $content,
                                    'fileName' => $fileName,
                                    'contentType' => $contentType,
                                ];
                            }
                        }
                    }
                }
            }

            if ($invoice->sendAsEmail($sender, $this->email, null, null, $this->outInvoice->send_with_stamp, $addHtml, $attach)) {
                $this->sendResult($invoice);

                return true;
            }
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function checkOrderData()
    {
        if ($this->outInvoice->is_outer_shopcart) {
            return;
        }
        if ($this->outInvoice->data_url) {
            if ($this->outInvoice->strict_data) {
                $this->dataErrorStatus = true;
            }
            if ($this->uid && $this->hash) {
                $requestHeader = [
                    'Content-Type: application/x-www-form-urlencoded',
                    'Accept: application/json',
                ];

                $requestData = [
                    'uid' => $this->uid,
                    'hash' => $this->hash,
                ];

                $curl = new Curl();
                /** @response 'Content-Type: application/json' */
                $response = $curl->setOptions([
                    CURLOPT_HTTPHEADER => $requestHeader,
                    CURLOPT_POSTFIELDS => http_build_query($requestData),
                ])->post($this->outInvoice->data_url);

                if (!$curl->errorCode) {
                    $requestResult = json_decode($response, true);
                    if (isset($requestResult['data']['product'], $requestResult['data']['price']) &&
                        array_keys($requestResult['data']['product']) == array_keys($requestResult['data']['price'])
                    ) {
                        $this->dataErrorStatus = false;
                    }
                    $this->scenario = static::SCENARIO_LOADDATA;
                    $this->load($requestResult);
                    $this->validate();
                    if ($this->product) {
                        $this->view = static::VIEW_CONTACT;
                        $this->scenario = static::SCENARIO_FIXPRODUCT;
                    }
                    if (isset($requestResult['error'])) {
                        $this->dataErrorStatus = true;
                        $this->dataErrorText = Html::encode($requestResult['error']);
                    }
                }
                if ($curl->errorCode || $this->dataErrorStatus) {
                    Yii::error(__METHOD__ . "\n" . $curl->getDump() . "\n", 'validation');
                }
            } else {
                Yii::error(__METHOD__ . "\n" . "Value of \"uid\" or \"hash\" is empty\n", 'validation');
            }
        } else {
            $request = Yii::$app->getRequest();
            $data = array_merge($request->get(), $request->post());
            unset($data['id'], $data['uid'], $data['hash']);
            if ($data && !isset($data[$request->csrfParam])) {
                $this->scenario = static::SCENARIO_LOADDATA;
                $this->load($data, '');
                $this->load($data);
                $this->validate();
                if ($this->product && $this->price && array_keys($this->product) == array_keys($this->price)) {
                    $this->view = static::VIEW_CONTACT;
                    $this->scenario = static::SCENARIO_FIXPRODUCT;
                }
            }
        }

        if ($this->dataErrorStatus) {
            throw new BadRequestHttpException($this->dataErrorText);
        }

        if ($this->scenario != static::SCENARIO_FIXPRODUCT) {
            $this->scenario = static::SCENARIO_FORMDATA;
        }

        $this->clearErrors();
    }

    /**
     * @inheritdoc
     */
    public function sendResult(Invoice $model)
    {
        if ($this->outInvoice->result_url) {
            $ndsExclude = $model->company->isNdsExclude;
            $requestHeader = [
                'Content-Type: application/x-www-form-urlencoded',
                'Accept: application/json',
            ];

            $items = [];
            foreach ($model->orders as $order) {
                $items[] = [
                    'id' => $order->product_id,
                    'name' => $order->product_title,
                    'count' => $order->quantity * 1,
                    'unit_name' => $order->unit ? $order->unit->name : '',
                    'price' => ($ndsExclude ?  $order->selling_price_no_vat : $order->selling_price_with_vat) / 100,
                    'amount' => ($ndsExclude ? $order->amount_sales_no_vat : $order->amount_sales_with_vat) / 100,
                ];
            }
            $data = [
                'uid' => $this->uid,
                'hash' => $this->hash,
                'invoice' => [
                    'number' => $model->fullNumber,
                    'date' => $model->document_date,
                    'items_count' => count($items),
                    'sum' => $model->total_amount_no_nds / 100,
                    'nds' => $model->total_amount_nds / 100,
                    'sum_with_nds' => $model->total_amount_with_nds / 100,
                    'provider_name' => $model->company_name_short,
                    'provider_address' => $model->company_address_legal_full,
                    'provider_inn' => $model->company_inn,
                    'provider_kpp' => $model->company_kpp,
                    'provider_bank_bik' => $model->company_bik,
                    'provider_bank_ks' => $model->company_ks,
                    'provider_bank_rs' => $model->company_rs,
                    'customer_name' => $model->contractor_name_short,
                    'customer_address' => $model->contractor_address_legal_full,
                    'customer_inn' => $model->contractor_inn,
                    'customer_kpp' => $model->contractor_kpp,
                    'customer_bank_bik' => $model->contractor_bik,
                    'customer_bank_ks' => $model->contractor_ks,
                    'customer_bank_rs' => $model->contractor_rs,
                    'items' => $items,
                ]
            ];

            $curl = new Curl();
            /** @response 'Content-Type: application/json' */
            $curl->setOptions([
                CURLOPT_HTTPHEADER => $requestHeader,
                CURLOPT_POSTFIELDS => http_build_query($data),
            ])->post($this->outInvoice->result_url);

            if ($curl->errorCode) {
                Yii::error(__METHOD__ . "\n" . $curl->getDump() . "\n", 'validation');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function loadContractorData()
    {
        if ($this->legal_inn && $this->getOutInvoice()->is_autocomplete) {
            $data = null;
            $dadata = new DadataClient;
            $suggestions = (array) ArrayHelper::getValue($dadata->getCompany($this->legal_inn, $this->legal_kpp), 'suggestions');
            if ($suggestions) {
                foreach ($suggestions as $item) {
                    if (isset($item['value'], $item['data']['state']['status']) && $item['data']['state']['status'] == 'ACTIVE') {
                        if (empty($kpp) || (isset($item['data']['kpp']) && $kpp == $item['data']['kpp'])) {
                            $data = $item['data'];
                            break;
                        }
                    }
                }
            }
            if ($data !== null) {
                $management = (array) ArrayHelper::getValue($data, 'management');
                $opf = (array) ArrayHelper::getValue($data, 'opf');
                $typeId = CompanyType::find()->select('id')->where([
                    'name_short' => ArrayHelper::getValue($opf, 'short', ''),
                ])->scalar();
                $isIp = $typeId == CompanyType::TYPE_IP;
                $name = (array) ArrayHelper::getValue($data, 'name');

                $chiefName = $isIp ? ArrayHelper::getValue($name, 'full') : ArrayHelper::getValue($management, 'name');

                $address = (array) ArrayHelper::getValue($data, 'address');
                $addressData = (array) ArrayHelper::getValue($address, 'data');
                $adresItems = [
                    ArrayHelper::getValue($addressData, 'postal_code'),
                    ArrayHelper::getValue($address, 'value'),
                ];
                $addresString = implode(', ', array_filter($adresItems));

                $this->legal_type = $typeId;
                $this->legal_kpp = ArrayHelper::getValue($data, 'kpp');
                $this->legal_name = $isIp ? TextHelper::nameShort($chiefName) : ArrayHelper::getValue($name, 'short');
                $this->legal_address = implode(', ', array_filter($adresItems));
                $this->chief_position = $isIp ? '' : ArrayHelper::getValue($management, 'post');
                $this->chief_name = $chiefName;
            }
        }
    }
}
