<?php

namespace frontend\modules\out\models;

use common\components\date\DateHelper;
use common\components\validators\PhoneValidator;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\currency\CurrencyRate;
use common\models\document\Invoice;
use common\models\document\OrderHelper;
use frontend\models\Documents;
use frontend\modules\documents\components\InvoiceHelper;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;

/**
 * Class OutInvoiceForm1
 * @package frontend\modules\out\models
 */
class OutInvoiceForm1 extends OutInvoiceForm
{
    protected $_view = self::VIEW_CONTACT;

    /**
     * attributes
     */
    public $order_list;

    public static $queue = [
        1 => self::VIEW_CONTACT,
        2 => self::VIEW_CONTRACTOR,
        3 => self::VIEW_INVOICE,
    ];

    public static $attributes = [
        self::VIEW_CONTACT => [
            'order_list',
            'email',
            'phone',
            'comment',
        ],
        self::VIEW_CONTRACTOR => [
            'legal_type',
            'legal_name',
            'legal_inn',
            'legal_kpp',
            'legal_address',
            'legal_rs',
            'legal_bik',
            'chief_position',
            'chief_name',
        ],
        self::VIEW_INVOICE => null,
    ];

    protected $_orderList = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'trim'],
            [['order_list', 'view', 'valid_until', 'attach', 'currency'], 'safe'],
            // self::VIEW_CONTACT
            [['order_list', 'email'], 'required'],
            ['order_list', function ($attribute, $params) {
                if (!is_array($this->$attribute) || ($count = count($this->$attribute)) == 0 || $count != count($this->getOrderList())) {
                    $this->addError($attribute, 'Не верные данные заказа.');
                }
            }],
            [['email'], 'email'],
            [['email_content'], 'string'],
            [['comment'], 'string', 'max' => 255],
            [['phone'], PhoneValidator::class],
            // self::VIEW_CONTRACTOR
            [['chief_position'], 'string', 'max' => 255],
            [['chief_name'], 'string', 'max' => 45],
            [
                ['chief_name', 'legal_type', 'legal_name', 'legal_inn', 'legal_address'],
                'required', 'when' => function ($model) {
                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON;
                },
            ],
            [['legal_inn', 'legal_kpp', 'legal_rs', 'legal_bik'], 'integer', 'min' => 0],
            [['legal_kpp', 'legal_bik'], 'string',  'length' => 9, 'max' => 9],
            [['legal_rs'], 'string', 'length' => 20, 'max' => 20],
            [['legal_kpp'], 'required', 'when' => function ($model) {
                return ($model->legal_type != CompanyType::TYPE_IP) && ($model->face_type == Contractor::TYPE_LEGAL_PERSON);
            },],
            [['legal_inn'], 'string', 'length' => 10, 'max' => 12, 'when' => function ($model) {
                return ($model->legal_type != CompanyType::TYPE_IP) && ($model->face_type == Contractor::TYPE_LEGAL_PERSON);
            },],
            [['legal_inn'], 'string', 'length' => 12, 'max' => 12, 'when' => function ($model) {
                return ($model->legal_type == CompanyType::TYPE_IP) && ($model->face_type == Contractor::TYPE_LEGAL_PERSON);
            },],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getOrderList()
    {
        if ($this->_orderList === false) {
            $this->_orderList = [];
            if (is_array($this->order_list) && count($this->order_list) > 0) {
                $i = 0;
                foreach ($this->order_list as $key => $orderData) {
                    $order =  new OrderModel();
                    $order->setOutInvoiceForm($this);
                    $order->load($orderData, '');
                    $order->number = ++$i;
                    if ($order->validate()) {
                        $this->_orderList[$key] = $order;
                    } else {
                        throw new BadRequestHttpException(
                            'Что то пошло не так:( Обратитесь к менеджерам интернет магазина ' .
                            $this->getOutInvoice()->company->getTitle(true)
                        );
                    }
                }
            } else {
                throw new BadRequestHttpException(
                    'В вашей корзине 0 товаров. Добавьте товар в корзину и повторите попытку.'
                );
            }
        }

        return $this->_orderList;
    }

    /**
     * @return Invoice
     */
    public function getInvoice($saveNew = false)
    {
        $company = $this->outInvoice->company;
        $contractor = $this->getContractor($saveNew);
        $payDate = $this->valid_until ? date_create($this->valid_until) : null;

        $model = new Invoice([
            'scenario' => 'insert',
            'type' => Documents::IO_TYPE_OUT,
            'document_date' => date(DateHelper::FORMAT_DATE),
            'isAutoinvoice' => false,
            'company_id' => $company->id,
            'contractor_id' => $contractor->id,
            'payment_limit_date' => $payDate ? $payDate->format('Y-m-d') :
                                    (new \DateTime("+{$contractor->customer_payment_delay} days"))->format('Y-m-d'),
            'agreement' => $contractor->last_basis_document,
            'comment' => $this->outInvoice->invoice_comment,
            'comment_internal' => 'Покупатель: ' . $this->comment,
            'is_by_out_link' => true,
            'by_out_link_id' => $this->outInvoice->id,
            'document_additional_number' => $this->outInvoice->additional_number,
            'is_additional_number_before' => $this->outInvoice->is_additional_number_before,
            'price_precision' => 2,
            'show_article' => $this->outInvoice->show_article,
            'nds_view_type_id' => $company->nds_view_type_id,
            'from_out_invoice' => true,
            'from_demo_out_invoice' => $this->isDemo,
        ]);

        $currency_name = ArrayHelper::getValue(Currency::findOne(['name' => $this->currency]), 'name', Currency::DEFAULT_NAME);
        if ($currency_name != Currency::DEFAULT_NAME) {
            $rateData = CurrencyRate::getCurrencyRate($currency_name);
            if ($rateData) {
                $model->currency_name = $currency_name;
                $model->currency_amount = $model->currency_rate_amount = $rateData['amount'];
                $model->currency_rate = $model->currency_rate_value = $rateData['value'];
                $model->currency_rate_date = $rateData['date'];
                $model->currency_rate_type = Currency::RATE_PAYMENT_DATE;
                $model->comment .= "\nОплата счета производится в российских рублях по курсу ЦБ РФ на день оплаты.";
            }
        }

        if ($this->isDemo) {
            $model->document_additional_number .= ' ДЕМО ';
        }

        $model->document_number = (string) Invoice::getNextDocumentNumber($company->id, Documents::IO_TYPE_OUT, null, $model->document_date);

        $model->populateRelation('company', $company);
        $model->populateRelation('contractor', $contractor);

        $orderArray = [];
        foreach ($this->getOrderList() as $orderModel) {
            $order = OrderHelper::createOrderByProduct($orderModel->getProduct(), $model, null);
            $order->load($orderModel->getOrderData(), '');
            $orderArray[] = $order;
        }
        $model->populateRelation('orders', $orderArray);

        $model->total_mass_gross = (string)array_sum(ArrayHelper::getColumn($model->orders, 'mass_gross'));
        $model->total_place_count = (string)array_sum(ArrayHelper::getColumn($model->orders, 'place_count'));

        $model->setContractor($contractor);

        if ($saveNew) {
            InvoiceHelper::$save_product = true;
            if (!InvoiceHelper::save($model)) {
                \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);
            }
        } else {
            $model->beforeValidate();
            $model->beforeSave(true);
        }

        return $model;
    }
}
