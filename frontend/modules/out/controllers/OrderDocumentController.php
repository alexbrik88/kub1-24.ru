<?php

namespace frontend\modules\out\controllers;

use common\models\Contractor;
use common\models\product\PriceList;
use frontend\modules\out\models\OutOrderDocumentForm;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

/**
 * OrderDocumentController
 */
class OrderDocumentController extends Controller
{
    public $layoutWrapperCssClass;
    public $layout = 'order-document';

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionCreate($uid)
    {
        $priceList = $this->findModel($uid);

        Yii::$app->params['outCompany'] = $priceList->company;
        Yii::$app->params['hasError'] = false;
        Yii::$app->params['isDemo'] = false;

        $model = new OutOrderDocumentForm($priceList);
        $model->face_type = $priceList->is_contractor_physical ?
            Contractor::TYPE_PHYSICAL_PERSON : Contractor::TYPE_LEGAL_PERSON;

        $request = Yii::$app->getRequest();
        $model->load($request->get(), '');
        $model->load($request->post(), '');
        $model->checkOrderData();

        if ($request->get('legal_inn') || $request->post('legal_inn')) {
            $model->loadContractorData();
        }

        if ($request->validateCsrfToken() && $model->load($request->post())) {
            if ($request->post('back')) {
                $model->setView($model->getPrevView());
            } elseif ($model->validate()) { // validate all pages
                if ($model->view != OutOrderDocumentForm::VIEW_CONTRACTOR || $model->sendOrderDocument()) {
                    $model->setView($model->getNextView());
                }
            }
        }

        if ($redirect = $model->getRedirect()) {
            return $this->redirect($redirect);
        }

        Yii::$app->params['view'] = $model->view;

        return $this->render($model->view, [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return PriceList
     * @throws NotFoundHttpException
     */
    protected function findModel($uid)
    {
        /* @var PriceList $model */
        $model = PriceList::find()
            //->andWhere(['company_id' => Yii::$app->user->identity->company->id])
            ->andWhere(['uid' => $uid])
            ->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Страница не найдена.');
        }
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        Yii::$app->controller->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }
}
