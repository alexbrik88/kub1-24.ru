<?php

namespace frontend\modules\out\controllers;

use common\models\out\OutInvoice;
use common\models\service\SubscribeTariffGroup;
use frontend\models\Documents;
use frontend\modules\out\models\OutInvoiceForm;
use frontend\modules\out\models\OutInvoiceForm1;
use frontend\modules\out\models\OutInvoiceForm2;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

/**
 * InvoiceController
 */
class InvoiceController extends Controller
{
    public $layoutWrapperCssClass;
    public $layout = 'main';

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionCreate($id)
    {
        $outInvoice = $this->findModel($id);

        $isDemo = ($outInvoice->demo_id == $id);

        Yii::$app->params['isDemo'] = $isDemo;
        Yii::$app->params['outCompany'] = $outInvoice->company;
        Yii::$app->params['hasError'] = false;

        if ($outInvoice->status != OutInvoice::ACTIVE && !$isDemo) {
            $message = 'Страница была удалена.';
            $message .= "\r\n";
            $message .= 'За подробной информацией обратитесь к ';
            $message .= $outInvoice->company->getTitle(true) . '.';
            Yii::$app->params['hasError'] = true;

            throw new NotFoundHttpException($message);
        }
        //if (!$outInvoice->company->createInvoiceAllowed(Documents::IO_TYPE_OUT)) {
        //    $message = 'Количество счетов достигло лимита.';
        //    $message .= "\r\n";
        //    $message .= 'За подробной информацией обратитесь к ';
        //    $message .= $outInvoice->company->getTitle(true) . '.';
        //    Yii::$app->params['hasError'] = true;
        //
        //    throw new ForbiddenHttpException($message);
        //}

        if ($outInvoice->is_outer_shopcart) {
            $model = new OutInvoiceForm1($outInvoice);
        } else {
            $model = new OutInvoiceForm2($outInvoice);
        }

        $model->isDemo = $isDemo;

        $request = Yii::$app->getRequest();
        $model->uid = $request->post('uid', $request->get('uid'));
        $model->hash = $request->post('hash', $request->get('hash'));
        $model->load($request->get(), '');
        $model->load($request->post(), '');
        $model->checkOrderData();

        if ($request->get('legal_inn') || $request->post('legal_inn')) {
            $model->loadContractorData();
        }

        //if ($request->validateCsrfToken() && $model->load($request->post())) {  // cookie don't work in iframe!
        if ($model->load($request->post())) {
            if ($request->post('back')) {
                $model->setView($model->getPrevView());
            } elseif ($model->validate()) {
                if ($model->view != OutInvoiceForm::VIEW_CONTRACTOR || $model->sendInvoice()) {
                    $model->setView($model->getNextView());
                }
            }
        }

        if ($redirect = $model->getRedirect()) {
            return $this->redirect($redirect);
        }

        return $this->render($model->view, [
            'model' => $model,
            'isDemo' => $isDemo
        ]);
    }

    /**
     * Finds the OutInvoice model based on its primary key value or DEMO key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return OutInvoice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = OutInvoice::find()->joinWith('company')->andWhere([
            'out_invoice.id' => $id,
            'company.blocked' => false,
        ])->one();

        if ($model !== null) {

            return $model;

        } elseif (($demoModel = OutInvoice::findOne(['demo_id' => $id])) !== null) {

            return $demoModel;

        } else {

            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        Yii::$app->controller->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    public function getIsPaid($company)
    {
        if (ArrayHelper::getValue(Yii::$app->params, 'b2b-paid', false)) {
            return true;
        }

        return $company->getHasActualSubscription(SubscribeTariffGroup::B2B_PAYMENT);
    }
}
