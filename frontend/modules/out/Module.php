<?php

namespace frontend\modules\out;

use common\models\CreateType;
use Yii;

/**
 * out module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\out\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        Yii::$app->params['create_type_id'] = CreateType::TYPE_B2B;

        Yii::$app->errorHandler->errorAction = '/out/default/error';
        Yii::$app->params['outCompany'] = null;
    }
}
