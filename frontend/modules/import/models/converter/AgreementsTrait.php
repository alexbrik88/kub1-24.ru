<?php

namespace frontend\modules\import\models\converter;

use common\models\Agreement;
use common\models\document\Invoice;
use frontend\models\Documents;
use frontend\modules\import\models\OneSConverter;
use Yii;

trait AgreementsTrait {

    /**
     * @return mixed
     * @throws \Throwable
     */
    public function insertAgreements()
    {
        $model = new Agreement;

        foreach ($this->data[OneSConverter::AGREEMENT] as $uid => $arr) {

            $this->updateProgressBar();

            /** @var Invoice $presentInDB */
            $presentInDB = Agreement::find()->where([
                'company_id' => $this->_company->id,
                'document_date' => $arr['document_date'],
                'object_guid' => $uid,
                'is_created' => true
            ])->one();

            if ($presentInDB) {
                switch ($presentInDB->type) {
                    case Documents::IO_TYPE_IN:
                        $this->dbIds[self::IN_AGREEMENT][$uid] = $presentInDB->id;
                        break;
                    case Documents::IO_TYPE_OUT:
                        $this->dbIds[self::OUT_AGREEMENT][$uid] = $presentInDB->id;
                        break;
                }

                continue;
            }

            foreach ([self::CONTRACTOR_SELLER, self::CONTRACTOR_CUSTOMER] as $CONTRACTOR_IO_KEY) {
                /** @var Agreement $agreement */
                $agreement = $this->array2ContractorAgreement($model, $uid);

                if (!isset($this->dbIds[$CONTRACTOR_IO_KEY][$arr['contractor_uid']]))
                    continue;

                $AGREEMENT_IO_KEY = ($CONTRACTOR_IO_KEY == self::CONTRACTOR_SELLER) ? self::IN_AGREEMENT : self::OUT_AGREEMENT;
                $agreement->type = ($AGREEMENT_IO_KEY == self::IN_AGREEMENT) ? Documents::IO_TYPE_IN : Documents::IO_TYPE_OUT;
                $agreement->contractor_id = $this->dbIds[$CONTRACTOR_IO_KEY][$arr['contractor_uid']] ?? null;

                if ($agreement->validate()) {
                    $agreement->save(false);
                    $this->dbIds[$AGREEMENT_IO_KEY][$uid] = $agreement->primaryKey;
                    $this->stat[OneSConverter::AGREEMENT] += 1;

                } else {

                    $this->dbIds[$AGREEMENT_IO_KEY][$uid] = null;
                    $this->addError(OneSConverter::AGREEMENT, $agreement->getFullNumber(), $agreement->getErrors());
                }
            }
        }

        return $this->stat[OneSConverter::AGREEMENT];
    }

}