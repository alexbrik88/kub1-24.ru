<?php

namespace frontend\modules\import\models\converter;

use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\import\models\OneSConverter;
use Yii;

trait InvoiceFacturesTrait {

    /**
     * @return mixed
     * @throws \Throwable
     */
    public function insertInvoiceFactures($IO_KEY)
    {
        $model = new InvoiceFacture;

        $INVOICE_IO_KEY = ($IO_KEY == self::IN_INVOICE_FACTURE) ? self::IN_INVOICE : self::OUT_INVOICE;
        $UPD_IO_KEY = ($IO_KEY == self::IN_INVOICE_FACTURE) ? self::IN_UPD : self::OUT_UPD;

        foreach ($this->data[$IO_KEY] as $uid => $arr) {

            $this->updateProgressBar();

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            file_put_contents(\Yii::getAlias('@runtime/logs/debug_fresh.log'), $uid.' '.date('c')."\n", FILE_APPEND);
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /** @var InvoiceFacture $presentInDB */
            $presentInDB = InvoiceFacture::find()->joinWith('invoice')->where([
                'invoice.company_id' => $this->_company->id,
                'invoice_facture.document_date' => $arr['document_date'],
                'invoice_facture.object_guid' => $uid,
            ])->one();

            if ($presentInDB) {
                $this->dbIds[$IO_KEY][$uid] = $presentInDB->id;
                $this->doubles[$IO_KEY] += 1;
                continue;
            }

            /** @var Invoice $invoice */
            $baseDocArr = $this->data[$UPD_IO_KEY][$arr['base_doc_uid']] ?? null;
            $invoiceUid = ($baseDocArr) ? $baseDocArr['invoice_uid'] : null;
            $invoiceId  = ($invoiceUid) ? $this->dbIds[$INVOICE_IO_KEY][$invoiceUid] : null;
            $invoice = Invoice::findOne($invoiceId);

            if (!$invoice) {
                $this->addError($IO_KEY, $arr['document_number'], [['Счёт не найден']]);
                continue;
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            file_put_contents(\Yii::getAlias('@runtime/logs/debug_fresh.log'), $invoice->object_guid.' '.date('c')."\n", FILE_APPEND);
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /** @var InvoiceFacture $invoiceFacture */
            $invoiceFacture = $this->_array2InvoiceFacture($model, $invoice, $uid, $IO_KEY);

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            file_put_contents(\Yii::getAlias('@runtime/logs/debug_fresh.log'), 'formed new InvoiceFacture'.date('c')."\n", FILE_APPEND);
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////

            if ($invoiceFacture->validate()) {

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////
                file_put_contents(\Yii::getAlias('@runtime/logs/debug_fresh.log'), 'validate new InvoiceFacture'.date('c')."\n", FILE_APPEND);
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////

                $saveFunction = function ($invoiceFacture) {
                    /** @var InvoiceFacture $invoiceFacture */
                    if ($invoiceFacture->ownOrders && $invoiceFacture->save(false)) {
                        foreach ($invoiceFacture->ownOrders as $order) {
                            $order->invoice_facture_id = $invoiceFacture->primaryKey;
                            $order->quantity = number_format($order->quantity, 10, '.', '');
                            if (!$order->save()) {
                                $invoiceFacture->addErrors($order->errors);

                                return false;
                            }
                        }
                        //foreach ($invoiceFacture->paymentDocuments as $paymentDocument) {
                        //    $paymentDocument->invoice_facture_id = $invoiceFacture->id;
                        //    if (!$paymentDocument->save()) {
                        //        return false;
                        //    }
                        //}

                        return true;
                    }

                    return false;
                };

                $isSaved = LogHelper::save($invoiceFacture, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, $saveFunction);

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////
                file_put_contents(\Yii::getAlias('@runtime/logs/debug_fresh.log'), 'saved new InvoiceFacture'.date('c')."\n", FILE_APPEND);
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////

                if ($isSaved) {
                    $this->dbIds[$IO_KEY][$uid] = $invoiceFacture->primaryKey;
                    $this->stat[$IO_KEY] += 1;

                    $invoice->populateRelation('invoiceFacture', $invoiceFacture);
                    $invoice->updateAttributes([
                        'has_invoice_facture' => true,
                        'can_add_invoice_facture' => false,
                    ]);

                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    file_put_contents(\Yii::getAlias('@runtime/logs/debug_fresh.log'), 'invoice has_facture updated'.date('c')."\n\n", FILE_APPEND);
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////

                } else {

                    $this->dbIds[$IO_KEY][$uid] = null;
                    $this->addError($IO_KEY, $invoiceFacture->getFullNumber(), $invoiceFacture->getErrors());
                }

            } else {

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////
                file_put_contents(\Yii::getAlias('@runtime/logs/debug_fresh.log'), 'errors '.date('c').'  ' . json_encode($invoiceFacture->getErrors()) ."\n\n", FILE_APPEND);
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////

                $this->dbIds[$IO_KEY][$uid] = null;
                $this->addError($IO_KEY, $invoiceFacture->getFullNumber(), $invoiceFacture->getErrors());
            }
        }

        return $this->stat[$IO_KEY];
    }

}