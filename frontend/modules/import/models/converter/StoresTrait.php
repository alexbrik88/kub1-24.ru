<?php

namespace frontend\modules\import\models\converter;

use common\components\date\DateHelper;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductInitialBalance;
use common\models\product\Store;
use frontend\modules\import\models\OneSConverter;

trait StoresTrait {

    /**
     * @return int
     */
    public function insertStores()
    {
        $model = new Store();

        foreach ($this->data[OneSConverter::STORE] as $uid => $arr) {

            $this->updateProgressBar();

            /** @var Product $presentInDB */
            $presentInDB = Store::find()
                ->andWhere(['company_id' => $this->_company->id])
                ->andWhere(['or',
                    ['object_guid' => $uid],
                    ['name' => $arr['name']]
                ])->one();

            if ($presentInDB) {
                $this->dbIds[OneSConverter::STORE][$uid] = $presentInDB->id;
                $this->doubles[OneSConverter::STORE] += 1;

                // set uid by 1C
                if ($uid != $presentInDB->object_guid) {
                    $presentInDB->updateAttributes(['object_guid' => $uid]);
                }

                continue;
            }

            /** @var Store $store */
            $store = $this->array2Store($model, $uid);

            if ($store->validate()) {

                $store->save(false);
                $this->dbIds[OneSConverter::STORE][$uid] = $store->primaryKey;
                $this->stat[OneSConverter::STORE] += 1;

            } else {

                $this->dbIds[OneSConverter::STORE][$uid] = null;
                $this->addError(OneSConverter::STORE, $store->name, $store->getErrors());
            }
        }

        return $this->stat[OneSConverter::STORE];
    }
}