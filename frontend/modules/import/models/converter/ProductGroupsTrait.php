<?php

namespace frontend\modules\import\models\converter;

use common\components\date\DateHelper;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductInitialBalance;
use frontend\modules\import\models\OneSConverter;

trait ProductGroupsTrait {

    /**
     * @return int
     */
    public function insertProductGroups()
    {
        $model = new ProductGroup();

        foreach ($this->data[OneSConverter::NOMENCLATURE_GROUP] as $uid => $arr) {

            $this->updateProgressBar();

            /** @var Product $presentInDB */
            $presentInDB = ProductGroup::find()
                ->andWhere(['company_id' => $this->_company->id])
                ->andWhere(['or',
                    ['object_guid' => $uid],
                    ['title' => $arr['title']]
                ])->one();

            if ($presentInDB) {
                $this->dbIds[OneSConverter::NOMENCLATURE_GROUP][$uid] = $presentInDB->id;
                $this->doubles[OneSConverter::NOMENCLATURE_GROUP] += 1;

                // set uid by 1C
                if ($uid != $presentInDB->object_guid) {
                    $presentInDB->updateAttributes(['object_guid' => $uid]);
                }

                continue;
            }

            /** @var ProductGroup $productGroup */
            $productGroup = $this->array2NomenclatureGroup($model, $uid);

            if ($productGroup->validate()) {

                $productGroup->save(false);
                $this->dbIds[OneSConverter::NOMENCLATURE_GROUP][$uid] = $productGroup->primaryKey;
                $this->stat[OneSConverter::NOMENCLATURE_GROUP] += 1;

            } else {

                $this->dbIds[OneSConverter::NOMENCLATURE_GROUP][$uid] = null;
                $this->addError(OneSConverter::NOMENCLATURE_GROUP, $productGroup->title, $productGroup->getErrors());
            }
        }

        return $this->stat[OneSConverter::NOMENCLATURE_GROUP];
    }
}