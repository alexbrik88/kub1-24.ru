<?php

namespace frontend\modules\import\models\converter;

use common\models\document\Invoice;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\modules\import\models\OneSConverter;
use Yii;

trait InvoicesTrait {

    /**
     * @return mixed
     * @throws \Throwable
     */
    public function insertInvoices($IO_KEY)
    {
        $model = new Invoice([
            'company_id' => $this->_company->id
        ]);

        $model->setCompany($this->_company);

        foreach ($this->data[$IO_KEY] as $uid => $arr) {

            $this->updateProgressBar();

            /** @var Invoice $presentInDB */
            $presentInDB = Invoice::find()->where([
                'company_id' => $this->_company->id,
                'document_date' => $arr['document_date'],
                'object_guid' => $uid,
                'is_deleted' => false
            ])->one();

            if ($presentInDB) {
                $this->dbIds[$IO_KEY][$uid] = $presentInDB->id;
                $this->doubles[$IO_KEY] += 1;
                continue;
            }

            /** @var Invoice $invoice */
            $invoice = $this->_array2Invoice($model, $uid, $IO_KEY);

            // add prefix
            if (!$invoice->validate(['document_number'])) {
                if (self::_isGeneratedInvoice($uid)) {
                    $invoice->document_additional_number = ' - к УПД';
                    $invoice->is_additional_number_before = 0;
                } else {
                    $invoice->document_additional_number = ' - из 1С';
                    $invoice->is_additional_number_before = 0;
                }
            }

            if ($invoice->validate()) {

                $isSaved = InvoiceHelper::save($invoice, false, true);

                if ($isSaved) {
                    $this->dbIds[$IO_KEY][$uid] = $invoice->primaryKey;
                    $this->stat[$IO_KEY] += 1;
                } else {
                    $this->addError($IO_KEY, $invoice->getFullNumber(), [['Невозможно создать счет']]); // todo: show orders errors
                }

            } else {

                $this->dbIds[$IO_KEY][$uid] = null;
                $this->addError($IO_KEY, $invoice->getFullNumber(), $invoice->getErrors());
            }
        }

        return $this->stat[$IO_KEY];
    }

}