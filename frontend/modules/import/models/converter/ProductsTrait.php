<?php

namespace frontend\modules\import\models\converter;

use common\components\date\DateHelper;
use common\models\product\Product;
use common\models\product\ProductInitialBalance;
use frontend\modules\import\models\OneSConverter;

trait ProductsTrait {

    /**
     * @return int
     */
    public function insertProducts()
    {
        $model = new Product;

        foreach ($this->data[OneSConverter::NOMENCLATURE] as $uid => $arr) {

            $this->updateProgressBar();

            /** @var Product $presentInDB */
            $presentInDB = Product::find()->andWhere(['and',
                ['company_id' => $this->_company->id],
                ['object_guid' => $uid],
                ['is_deleted' => false],
            ])->one();

            if ($presentInDB) {
                $this->updateProductInitialBalance($presentInDB);
                $this->dbIds[OneSConverter::NOMENCLATURE][$uid] = $presentInDB->id;
                $this->doubles[OneSConverter::NOMENCLATURE] += 1;
                continue;
            }

            /** @var Product $product */
            $product = $this->array2Nomenclature($model, $uid);

            if ($product->validate()) {

                $product->initQuantity = 0;
                $product->irreducibleQuantity = 0;
                $product->save(false);
                $this->updateProductInitialBalance($product);
                $this->dbIds[OneSConverter::NOMENCLATURE][$uid] = $product->primaryKey;
                $this->stat[OneSConverter::NOMENCLATURE] += 1;

            } else {

                $this->dbIds[OneSConverter::NOMENCLATURE][$uid] = null;
                $this->addError(OneSConverter::NOMENCLATURE, $product->title, $product->getErrors());
            }
        }

        return $this->stat[OneSConverter::NOMENCLATURE];
    }

    public function updateProductInitialBalance(Product $product)
    {
        $uid = $product->object_guid;
        $array = &$this->data[self::NOMENCLATURE][$uid];

        if (!empty($array['start_quantity_arr']) && $this->importPeriodStart) {

            $modelInitialBalance = $this->_getModelInitialBalance($product);
            $importStartDate = DateHelper::format($this->importPeriodStart, 'Y-m-d', 'd.m.Y');

            if ($importStartDate && ($importStartDate <= $modelInitialBalance->date)) {

                // save initial balance date and first price
                $modelInitialBalance->date = $importStartDate;
                $modelInitialBalance->price = $this->_getProductFirstPriceForBuy($uid);
                $modelInitialBalance->save(false);

                // save start quantity by stores
                foreach ($array['start_quantity_arr'] as $startQuantity) {
                    if ($modelProductStore = $this->_getModelProductStore($startQuantity['store_uid'], $product)) {
                        $modelProductStore->initial_quantity = $startQuantity['quantity'];
                        if ($modelProductStore->validate(['initial_quantity'])) {
                            $modelProductStore->updateAttributes(['initial_quantity']);
                        }
                    }
                }
            }
        }
    }

    private function _getModelInitialBalance(Product $product)
    {
        if (!$product || !$product->initialBalance) {
            $pb = new ProductInitialBalance([
                'product_id' => $product->id,
                'product_unit_id' => $product->product_unit_id,
                'price' => 0,
                'date' => date('Y-m-d')
            ]);
        } else {
            $pb = $product->initialBalance;
        }

        return $pb;
    }

    private function _getModelProductStore($storeUID, Product $product)
    {
        $query = $this->_company
            ->getStores()
            ->select('id')
            ->orderBy(['is_main' => SORT_DESC]);

        if ($storeUID) {
            $query->where(['object_guid' => $storeUID]);
        }

        $storeId = $query->scalar();

        return $product->getProductStoreByStore($storeId);
    }

    private function _getProductFirstPriceForBuy($productUid)
    {
        $firstDate = date('Y-m-d');
        $firstPrice = 0;
        foreach  ($this->data[OneSConverter::IN_INVOICE] as $uid => $invoice) {
            foreach ($invoice['products'] as $product) {
                if ($product['uid'] == $productUid) {
                    if ($invoice['document_date'] < $firstDate) {
                        $firstDate = $invoice['document_date'];
                        $firstPrice = $product['price'];
                    }
                }
            }
        }

        return $firstPrice;
    }
}