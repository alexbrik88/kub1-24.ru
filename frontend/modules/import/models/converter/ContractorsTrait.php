<?php

namespace frontend\modules\import\models\converter;

use common\models\Contractor;
use common\models\ContractorAccount;
use frontend\modules\import\models\OneSConverter;
use yii\helpers\ArrayHelper;

trait ContractorsTrait {

    /**
     * @return int
     */
    public function insertContractors()
    {
        $model = new Contractor;
        $accountModel = new ContractorAccount();

        //foreach ($this->data[OneSConverter::CONTRACTOR] as $uid => $arr) {
        foreach ($this->_contractorType as $IO_KEY => $contractorsUids)
        foreach ($contractorsUids as $uid => $val) {

            $arr = &$this->data[OneSConverter::CONTRACTOR][$uid];
            $contractorType = ($IO_KEY == self::CONTRACTOR_SELLER) ? Contractor::TYPE_SELLER : Contractor::TYPE_CUSTOMER;

            $this->updateProgressBar();

            /** @var Contractor $presentInDB */
            $presentInDB = Contractor::find()->andWhere(['and',
                    ['company_id' => $this->_company->id],
                    ['is_deleted' => false],
                    ['type' => $contractorType],
                    ['or',
                        ['object_guid' => $uid],
                        ['and', ['face_type' => Contractor::TYPE_LEGAL_PERSON], ['ITN' => $arr['ITN']]],
                    ]
                ])->andFilterWhere(['PPC' => $arr['PPC']])->one();

            if ($presentInDB) {
                $this->dbIds[$IO_KEY][$uid] = $presentInDB->id;
                $this->doubles[OneSConverter::CONTRACTOR] += 1;
                continue;
            }

            $accountUid = $this->_getUidByUid(OneSConverter::BANK_ACCOUNT, 'contractor_uid', $uid);

            /** @var Contractor $contractor */
            $contractor = $this->array2Contractor($model, $uid);
            $contractor->type = $contractorType;

            if ($contractor->director_phone && !$contractor->validate(['director_phone']))
                $contractor->director_phone = null;
            if ($contractor->director_email && !$contractor->validate(['director_email']))
                $contractor->director_email = null;

            if ($contractor->validate()) {

                $contractor->save(false);
                $this->dbIds[$IO_KEY][$uid] = $contractor->primaryKey;
                $this->stat[OneSConverter::CONTRACTOR] += 1;

                if ($accountUid) {
                    /** @var ContractorAccount $account */
                    if ($account = $this->array2ContractorAccount($accountModel, $accountUid)) {
                        $account->contractor_id = $contractor->primaryKey;
                        if ($account->validate()) {
                            $account->save(false);
                        } else {

                            $this->addError(OneSConverter::BANK_ACCOUNT, $contractor->name, $account->getErrors());
                        }
                    }
                }

            } else {

                $this->dbIds[$IO_KEY][$uid] = null;
                $this->addError(OneSConverter::CONTRACTOR, $contractor->name, $contractor->getErrors());
            }
        }

        return $this->stat[OneSConverter::CONTRACTOR];
    }

}