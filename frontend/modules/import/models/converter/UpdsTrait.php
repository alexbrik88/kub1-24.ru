<?php

namespace frontend\modules\import\models\converter;

use common\components\helpers\ArrayHelper;
use common\models\document\Invoice;
use common\models\document\Upd;
use common\models\product\Product;
use common\models\product\Store;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\import\models\OneSConverter;
use Yii;

trait UpdsTrait {

    /**
     * @return mixed
     * @throws \Throwable
     */
    public function insertUpds($IO_KEY)
    {
        $model = new Upd;

        $INVOICE_IO_KEY = ($IO_KEY == self::IN_UPD) ? self::IN_INVOICE : self::OUT_INVOICE;

        $mainStoreId = Store::find()->where(['company_id' => $this->_company->id, 'is_main' => 1])->select('id')->scalar();

        foreach ($this->data[$IO_KEY] as $uid => $arr) {

            $this->updateProgressBar();

            /** @var Upd $presentInDB */
            $presentInDB = Upd::find()->joinWith('invoice')->where([
                'invoice.company_id' => $this->_company->id,
                'upd.document_date' => $arr['document_date'],
                'upd.object_guid' => $uid,
            ])->one();

            if ($presentInDB) {
                $this->dbIds[$IO_KEY][$uid] = $presentInDB->id;
                $this->doubles[$IO_KEY] += 1;
                continue;
            }

            /** @var Invoice $invoice */
            $invoiceId  = $this->dbIds[$INVOICE_IO_KEY][$arr['invoice_uid']] ?? null;
            $invoice = Invoice::findOne($invoiceId);

            if (!$invoice) {
                $this->addError($IO_KEY, $arr['document_number'], [['Счёт не найден']]);
                continue;
            } else {
                $storeId = ArrayHelper::getValue($this->dbIds[self::STORE], $arr['store_uid'] ?? null);
                if ($storeId && $storeId != $mainStoreId && $storeId != $invoice->store_id) {
                    $invoice->updateAttributes(['store_id' => $storeId]);
                }
            }

            /** @var Upd $upd */
            $upd = $this->_array2Upd($model, $invoice, $uid, $IO_KEY);

            if ($upd->validate()) {

                $saveFunction = function ($upd) use ($invoice) {
                    /** @var Upd $upd */
                    if ($upd->ownOrders) {
                        $upd->orders_sum = $upd->getTotalAmountWithNds();
                        $upd->order_nds = $upd->getTotalNds();
                        if ($upd->save(false)) {
                            foreach ($upd->ownOrders as $order) {
                                $order->upd_id = $upd->primaryKey;
                                $order->quantity = number_format($order->quantity, 10, '.', '');
                                if (!$order->save()) {
                                    $upd->addErrors($order->errors);

                                    return false;
                                }
                            }

                            $invoice->updateAttributes([
                                'has_upd' => true,
                                'can_add_upd' => false,
                                'can_add_act' => false
                            ]);

                            //foreach ($upd->paymentDocuments as $paymentDocument) {
                            //    $paymentDocument->upd_id = $upd->primaryKey;
                            //    if (!$paymentDocument->save()) {
                            //        $upd->addErrors($paymentDocument->errors);
                            //
                            //        return false;
                            //    }
                            //}

                            return true;
                        }
                    }

                    return false;
                };

                $isSaved = LogHelper::save($upd, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, $saveFunction);

                if ($isSaved) {
                    $this->dbIds[$IO_KEY][$uid] = $upd->primaryKey;
                    $this->stat[$IO_KEY] += 1;

                    $invoice->populateRelation('upd', $upd);

                    if ($invoice->payment_partial_amount > 0) {
                        $invoice->updateDocumentsPaid();
                    }

                    Product::updateGrowingPriceForBuy($invoice);

                } else {

                    $this->dbIds[$IO_KEY][$uid] = null;
                    $this->addError($IO_KEY, $upd->getFullNumber(), $upd->getErrors());
                }

            } else {

                $this->dbIds[$IO_KEY][$uid] = null;
                $this->addError($IO_KEY, $upd->getFullNumber(), $upd->getErrors());
            }
        }

        return $this->stat[$IO_KEY];
    }

}