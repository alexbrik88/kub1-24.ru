<?php
namespace frontend\modules\import\models;

class OneS {

    public $importPeriodStart;
    public $importPeriodEnd;

    public static $dbConst = [
        'tax_rate' => [
            'НДС18'  => 1,
            'НДС10'  => 2,
            'НДС0'   => 3,
            'БезНДС' => 4,
            'НДС20'  => 5,
        ],
        'currency' => [
            '643' => 'RUB',
            '398' => 'KZT',
            '840' => 'USD',
            '933' => 'BYN',
            '978' => 'EUR',
            '980' => 'UAH'
        ]
    ];

    // dict
    const CONTRACTOR = 'contractor';
    const AGREEMENT = 'agreement';
    const BANK = 'bank';
    const BANK_ACCOUNT = 'bank_account';
    const NOMENCLATURE = 'nomenclature';
    const NOMENCLATURE_GROUP = 'nomenclature_group';
    const NOMENCLATURE_NDS = 'nomenclature_nds';
    const STORE = 'store';
    // doc
    const IN_INVOICE = 'in_invoice';
    const IN_INVOICE_FACTURE = 'in_invoice_facture';
    const IN_UPD = 'in_upd';
    const IN_ACT = 'in_act';
    const IN_PACKING_LIST = 'in_packing_list';
    // doc
    const OUT_INVOICE = 'out_invoice';
    const OUT_INVOICE_FACTURE = 'out_invoice_facture';
    const OUT_UPD = 'out_upd';
    const OUT_ACT = 'out_act';
    const OUT_PACKING_LIST = 'out_packing_list';

    // split contractors|agreements in converter
    const CONTRACTOR_SELLER = 'contractor_seller';
    const CONTRACTOR_CUSTOMER = 'contractor_customer';
    const IN_AGREEMENT = 'in_agreement';
    const OUT_AGREEMENT = 'out_agreement';

    /**
     * @return string
     */
    public static function getUploadsDir()
    {
        return \Yii::getAlias('@common/uploads') . DIRECTORY_SEPARATOR . 'one_s_import';
    }
}