<?php

namespace frontend\modules\import\models;

use common\models\Company;
use common\models\employee\Employee;
use Yii;

/**
 * This is the model class for table "import_1c".
 *
 * @property int $id
 * @property int $created_at
 * @property int $company_id
 * @property int $employee_id
 * @property string|null $period
 * @property int|null $is_completed
 * @property int|null $is_autoload
 * @property int|null $total_contractors
 * @property int|null $total_products
 * @property int|null $total_in_invoices
 * @property int|null $total_in_upds
 * @property int|null $total_in_invoice_factures
 * @property int|null $total_out_invoices
 * @property int|null $total_out_upds
 * @property int|null $total_out_invoice_factures
 * @property string|null $errors_messages
 * @property integer $source_id
 * @property string|null $filename
 *
 * @property Company $company
 * @property Employee $employee
 */
class Import1c extends \yii\db\ActiveRecord
{
    const SOURCE_MANUAL = 1;
    const SOURCE_1C_FRESH = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'import_1c';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'company_id', 'employee_id'], 'required'],
            [['created_at', 'company_id', 'employee_id', 'is_completed', 'is_autoload'], 'integer'],
            [['total_contractors', 'total_products'], 'integer'],
            [['total_in_invoices', 'total_in_upds', 'total_in_invoice_factures'], 'integer'],
            [['total_out_invoices', 'total_out_upds', 'total_out_invoice_factures'], 'integer'],
            [['period'], 'string', 'max' => 255],
            [['errors_messages'], 'string'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
            [['source_id'], 'default', 'value' => self::SOURCE_MANUAL],
            [['source_id'], 'integer'],
            [['filename'], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'company_id' => 'Company ID',
            'employee_id' => 'Employee ID',
            'period' => 'Период загрузки',
            'is_completed' => 'Статус',
            'total_contractors' => 'Контрагентов',
            'total_products' => 'Товаров/Услуг',
            'total_in_invoices' => 'Вх. Счетов',
            'total_in_upds' => 'Вх. УПД',
            'total_in_invoice_factures' => 'Вх. Счетов-Фактур',
            'total_out_invoices' => 'Исх. Счетов',
            'total_out_upds' => 'Исх. УПД',
            'total_out_invoice_factures' => 'Исх. Счетов-Фактур',
            'errors_messages' => 'Ошибки загрузки',
            'status' => 'Статус',
            'source_id' => 'Источник'
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * Gets query for [[Employee]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }
}
