<?php

namespace frontend\modules\import\models;

use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use yii\data\ActiveDataProvider;

/**
 * ImportSearch 
 */
class Import1cSearch extends Import1c
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee_id'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Import1c::find()
            ->joinWith('employee')
            ->andWhere([
                Import1c::tableName() . '.company_id' => \Yii::$app->user->identity->company->id,
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'created_at',
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->employee_id) {
            if ($this->employee_id == 'autoload') {
                $query->andWhere(['is_autoload' => true]);
            } else {
                $query->Where([
                    'employee_id' => $this->employee_id,
                    'is_autoload' => false
                ]);
            }
        }

        $dataProvider->setSort([
            'attributes' => [
                'created_at',
            ],
            'defaultOrder' => [
                'created_at' => SORT_DESC,
            ],
        ]);

        return $dataProvider;
    }

    /**
     * @param $companyId
     * @return array
     */
    public function getEmployeeByCompanyArray($companyId, $emailModel = null)
    {
        $employee[null] = 'Все';

        $query = EmployeeCompany::find()->where([
            'company_id' => $companyId,
            'employee_role_id' => [
                EmployeeRole::ROLE_CHIEF,
                EmployeeRole::ROLE_ACCOUNTANT,
            ],
            'is_working' => true,
        ]);
        foreach ($query->all() as $employeeCompany) {
            $employee[$employeeCompany->employee_id] = $employeeCompany->getFio(true);
        }

        if ($emailModel) {
            $employee['autoload'] = $emailModel->email;
        }

        return $employee;
    }
}
