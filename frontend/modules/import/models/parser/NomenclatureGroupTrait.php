<?php

namespace frontend\modules\import\models\parser;

use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductUnit;
use frontend\modules\import\helpers\XmlHelper;

trait NomenclatureGroupTrait {

    public function xml2NomenclatureGroup(\SimpleXMLElement $xmlObject)
    {
        $props = XmlHelper::getProperties($xmlObject);
        $linkProps = XmlHelper::getProperties($xmlObject->{'Ссылка'});

        $uid = $linkProps['{УникальныйИдентификатор}'] ?? null;

        $entity = [
            'uid' => $uid,
            'title' => mb_substr($props['Наименование'] ?? '', 0, 60)
        ];

        if ($uid) {
            $this->data[self::NOMENCLATURE_GROUP][$uid] = $entity;
        }
    }

    public function array2NomenclatureGroup(ProductGroup &$model, $uid)
    {
        $array = &$this->data[self::NOMENCLATURE_GROUP][$uid];

        ///////////////////////////
        $model->id = null;
        $model->isNewRecord = true;
        $model->object_guid = $uid;
        ///////////////////////////

        $model->company_id = $this->_company->id;
        $model->production_type = 1;
        $model->title = $array['title'];

        return $model;
    }
}