<?php

namespace frontend\modules\import\models\parser;

use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductUnit;
use frontend\modules\import\helpers\XmlHelper;

trait NomenclatureTrait {

    public function xml2Nomenclature(\SimpleXMLElement $xmlObject)
    {
        $unitObj = XmlHelper::getPropertyByName($xmlObject, 'ЕдиницаИзмерения');
        $groupObj = XmlHelper::getPropertyByName($xmlObject, 'НоменклатурнаяГруппа');
        $startQuantityObj = XmlHelper::getTagByName($xmlObject, 'ЗначениеПараметра', 'ОстатокТовара');

        $props = XmlHelper::getProperties($xmlObject);
        $linkProps = XmlHelper::getProperties($xmlObject->{'Ссылка'});
        $unitProps = ($unitObj) ? XmlHelper::getProperties($unitObj->{'Ссылка'}) : [];
        $groupProps = ($groupObj) ? XmlHelper::getProperties($groupObj->{'Ссылка'}) : [];

        $uid = $linkProps['{УникальныйИдентификатор}'] ?? null;

        $startQuantityValue = self::__parseStartQuantity($startQuantityObj);

        $entity = [
            'uid' => $uid,
            'group_uid' => $groupProps['{УникальныйИдентификатор}'] ?? null,
            'code' => $linkProps['Код'] ?? null,
            'title' => $linkProps['Наименование'] ?? null,
            'code_okei' => trim($unitProps['Код'] ?? ""),
            'article' => trim($props['Артикул'] ?? ""),
            'production_type' => (isset($props['Услуга']) && $props['Услуга'] === "true") ?
                Product::PRODUCTION_TYPE_SERVICE : Product::PRODUCTION_TYPE_GOODS,
            'start_quantity_arr' => $startQuantityValue
        ];

        if ($uid) {
            if ((isset($linkProps['ЭтоГруппа']) && $linkProps['ЭтоГруппа'] === "true")) {
                // product hierarchy not used
            } else {
                $this->data[self::NOMENCLATURE][$uid] = $entity;
            }
        }
    }

    public function array2Nomenclature(Product &$model, $uid)
    {
        $array = &$this->data[self::NOMENCLATURE][$uid];
        $groupId  = $this->dbIds[self::NOMENCLATURE_GROUP][$array['group_uid']] ?? ProductGroup::WITHOUT;

        ///////////////////////////
        $model->id = null;
        $model->isNewRecord = true;
        $model->object_guid = $uid;
        $model->is_import_1c = 1;
        $model->one_c_imported = 1;
        ///////////////////////////

        $model->company_id = $this->_company->id;
        $model->production_type = $array['production_type'];
        $model->title = $array['title'];
        $model->code = $array['code'];
        $model->article = $array['article'];
        $model->country_origin_id = 1; // todo
        $model->has_excise = 0;
        $model->product_unit_id = ($unitModel = ProductUnit::findOne(['code_okei' => $array['code_okei']])) ?
            $unitModel->id : ProductUnit::UNIT_COUNT;
        $model->group_id = $groupId;

        return $model;
    }

    private static function __parseStartQuantity($obj)
    {
        $result = [];
        if ($obj) {
            $raw = trim((string)$obj->{'Значение'});
            if (mb_substr($raw, 0, 5) == 'Склад') {
                $arr = explode(';', $raw);
                foreach ($arr as $a) {
                    $data = explode(',', $a);
                    if (mb_strpos($data[0], 'Склад') !== false && mb_strpos($data[1], 'Остаток') !== false) {

                        $result[] = [
                            'store_uid' => trim(str_replace('Склад:', '', $data[0])),
                            'quantity' => trim(str_replace('Остаток:', '', $data[1])),
                        ];
                    }
                }
            } elseif (is_numeric($raw)) {

                $result[] = [
                    'store_uid' => null,
                    'quantity' => $raw
                ];
            }
        }

        return $result;
    }
}