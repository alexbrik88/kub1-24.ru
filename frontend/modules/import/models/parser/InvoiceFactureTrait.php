<?php

namespace frontend\modules\import\models\parser;

use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\Order;
use common\models\document\OrderInvoiceFacture;
use common\models\document\Upd;
use frontend\models\Documents;
use frontend\modules\import\helpers\XmlHelper;

trait InvoiceFactureTrait {

    public function xml2InInvoiceFacture(\SimpleXMLElement $xmlObject) { $this->_xml2InvoiceFacture($xmlObject, self::IN_INVOICE_FACTURE); }
    public function xml2OutInvoiceFacture(\SimpleXMLElement $xmlObject) { $this->_xml2InvoiceFacture($xmlObject, self::OUT_INVOICE_FACTURE); }

    private function _xml2InvoiceFacture(\SimpleXMLElement $xmlObject, $IO_KEY)
    {
        $contractorObj = XmlHelper::getPropertyByName($xmlObject, 'Контрагент');
        $agreementObj = XmlHelper::getPropertyByName($xmlObject, 'ДоговорКонтрагента');
        $baseDocObj = XmlHelper::getPropertyByName($xmlObject, 'ДокументОснование');

        $props = XmlHelper::getProperties($xmlObject);
        $linkProps = XmlHelper::getProperties($xmlObject->{'Ссылка'});
        $contractorProps = ($contractorObj) ? XmlHelper::getProperties($contractorObj->{'Ссылка'}) : [];
        $agreementProps = ($agreementObj) ? XmlHelper::getProperties($agreementObj->{'Ссылка'}) : [];
        $baseDocProps = ($baseDocObj) ? XmlHelper::getProperties($baseDocObj->{'Ссылка'}) : [];

        $uid = $linkProps['{УникальныйИдентификатор}'] ?? null;

        if ($IO_KEY == self::IN_INVOICE_FACTURE) {
            $documentDate = ($linkProps['ДатаВходящегоДокумента'] ?? null) ?: ($linkProps['Дата'] ?? null);
            $documentNumber = ($linkProps['НомерВходящегоДокумента'] ?? null) ?: (($linkProps['Номер'] ?? null) ?: 'б/н');
        }
        else if ($IO_KEY == self::OUT_INVOICE_FACTURE) {
            $_outNumberObj = XmlHelper::getTagByName($xmlObject, 'ЗначениеПараметра', 'НомерИсходящегоДокумента');
            $documentDate = $linkProps['Дата'] ?? null;
            $documentNumber = ($_outNumberObj) ? (string)$_outNumberObj->{'Значение'} : null;
        } else {
            $documentDate = $documentNumber = null;
        }

        $entity = [
            'uid' => $uid,
            'contractor_uid' => $contractorProps['{УникальныйИдентификатор}'] ?? null,
            'agreement_uid' => $agreementProps['{УникальныйИдентификатор}'] ?? null,
            'base_doc_uid' => $baseDocProps['{УникальныйИдентификатор}'] ?? null,
            'document_date' => substr($documentDate, 0, 10),
            'document_number' => $documentNumber,
            //'raw' => [
            //    'Комментарий' => $props['Комментарий'] ?? null,
            //    'СуммаДокумента' =>  $props['СуммаДокумента'] ?? null,
            //    'СуммаНДСДокумента' =>  $props['СуммаНДСДокумента'] ?? null,
            //]
        ];

        if ($uid)
            $this->data[$IO_KEY][$uid] = $entity;
    }

    private function _array2InvoiceFacture(InvoiceFacture &$model, Invoice $invoice, $uid, $IO_KEY)
    {
        $array = &$this->data[$IO_KEY][$uid];
        $numberArray = self::_getDocumentNumberArr($array['document_number'], $IO_KEY == self::OUT_INVOICE_FACTURE);

        $BASE_DOC_IO_KEY = ($IO_KEY == self::IN_INVOICE_FACTURE) ? self::IN_UPD : self::OUT_UPD;

        /** @var Upd $baseDocId */
        $baseDocId = $this->dbIds[$BASE_DOC_IO_KEY][$array['base_doc_uid']] ?? null;
        $baseDoc   = Upd::findOne($baseDocId);

        //////////////////////////////
        $model->id = null;
        $model->isNewRecord = true;
        $model->object_guid = $uid;
        $model->one_c_imported = 1;
        //////////////////////////////

        $model->type = ($IO_KEY == self::IN_INVOICE_FACTURE) ? Documents::IO_TYPE_IN : Documents::IO_TYPE_OUT;
        $model->invoice_id = $invoice->id;
        $model->document_date = $array['document_date'];
        $model->document_number = $numberArray['document_number'];
        $model->document_additional_number = $numberArray['document_additional_number'];
        $model->auto_created = false;
        $model->document_author_id = $this->_employee->id;
        $model->status_out_author_id = $this->_employee->id;
        $model->status_out_updated_at = time();

        // orders
        $ownOrderArray = [];
        if ($invoice && $baseDoc) {
            foreach ($baseDoc->orders as $order) {
                $ownOrderArray[] = self::_createOrderInvoiceFacture($order);
            }
        }

        $model->populateRelation('invoice', $invoice);
        $model->populateRelation('ownOrders', $ownOrderArray);

        return $model;
    }
    
    private function _createOrderInvoiceFacture(Order $order)
    {
        return new OrderInvoiceFacture([
            'invoice_facture_id' => null,
            'order_id' => $order->id,
            'quantity' => $order->quantity,
            'country_id' => $order->country_id,
            'custom_declaration_number' => $order->custom_declaration_number,
        ]);
    }

}