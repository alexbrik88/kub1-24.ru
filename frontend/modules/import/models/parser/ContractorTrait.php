<?php

namespace frontend\modules\import\models\parser;

use common\components\helpers\ArrayHelper;
use common\models\company\CompanyType;
use common\models\Contractor;
use frontend\modules\import\helpers\XmlHelper;

trait ContractorTrait {

    public function xml2Contractor(\SimpleXMLElement $xmlObject)
    {
        $props = XmlHelper::getProperties($xmlObject);
        $linkProps = XmlHelper::getProperties($xmlObject->{'Ссылка'});
        $tableInfoObj = XmlHelper::getTagByName($xmlObject, 'ТабличнаяЧасть', 'КонтактнаяИнформация');

        if ($tableInfoObj && $tableInfoObj->children()->count()) {
            for ($i = 0; $i < $tableInfoObj->children()->count(); $i++) {
                if ($recordObj = $tableInfoObj->children()->{'Запись'}[$i]) {
                    $recordProps = XmlHelper::getProperties($recordObj);
                    $key = ArrayHelper::getValue($recordProps, 'Тип');
                    $value = ArrayHelper::getValue($recordProps, 'Представление');

                    switch ($key) {
                        case 'Телефон':
                            $raw = filter_var(str_replace(['+', '-'], '', $value), FILTER_SANITIZE_NUMBER_INT);
                            if ($raw) {
                                $phone = '+'.substr($raw, 0, 1).'('.substr($raw, 1, 3).') '.substr($raw, 4, 3).'-'.substr($raw, 7, 2).'-'.substr($raw, 9, 2);
                            }
                            break;
                        case 'АдресЭлектроннойПочты':
                            $email = filter_var($value, FILTER_SANITIZE_EMAIL);
                            break;
                        case 'Адрес':
                            $addressObj = XmlHelper::getPropertyByName($recordObj, 'Вид');
                            $addressProps = ($addressObj) ? XmlHelper::getProperties($addressObj->{'Ссылка'}) : [];
                            if ($addressProps) {
                                switch ($addressProps['Наименование']) {
                                    case 'Юридический адрес':
                                        $legalAddress = $value;
                                        break;
                                    case 'Фактический адрес':
                                        $actualAddress = $value;
                                        break;
                                    case 'Почтовый адрес':
                                        $postalAddress = $value;
                                        break;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        $uid = $linkProps['{УникальныйИдентификатор}'] ?? null;

        $entity = [
            'uid' => $uid,
            'name' => $linkProps['Наименование'] ?? null,
            'ITN' => $linkProps['ИНН'] ?? null,
            'PPC' => $linkProps['КПП'] ?? null,
            'BIN' => $props['РегистрационныйНомер'] ?? null,
            'phone' => $phone ?? null,
            'email' => $email ?? null,
            'legal_address' => $legalAddress ?? null,
            'actual_address' => $actualAddress ?? null,
            'postal_address' => $postalAddress ?? null
        ];

        if ($uid)
            $this->data[self::CONTRACTOR][$uid] = $entity;
    }

    public function array2Contractor(Contractor &$model, $uid)
    {
        $array = &$this->data[self::CONTRACTOR][$uid];

        ///////////////////////////
        $model->id = null;
        $model->isNewRecord = true;
        $model->object_guid = $uid;
        $model->type = null; // set later by doc type
        $model->one_c_imported = 1;
        ///////////////////////////

        $model->company_id = $this->_company->id;
        $model->employee_id = $this->_employee->id;
        $model->status = Contractor::ACTIVE;
        $model->contact_is_director = 1;
        $model->chief_accountant_is_director = 1;

        $model->document_guid = $array['uid'];
        $model->name = $array['name'];
        $model->ITN = $array['ITN'];
        $model->PPC = $array['PPC'];
        $model->BIN = $array['BIN'];
        $model->director_phone = $array['phone'];
        $model->director_email = $array['email'];
        $model->legal_address = $array['legal_address'];
        $model->actual_address = $array['actual_address'];
        $model->postal_address = $array['postal_address'];

        // todo: load from dadata

        if (strlen($array['ITN']) == 12) {
            $model->face_type = Contractor::TYPE_LEGAL_PERSON;
            $model->company_type_id = CompanyType::TYPE_IP;
        } elseif (strlen($array['ITN']) == 10) {
            $model->face_type = Contractor::TYPE_LEGAL_PERSON;
            $model->company_type_id = CompanyType::TYPE_OOO;
        } else {
            $model->face_type = Contractor::TYPE_PHYSICAL_PERSON;
            $model->physical_fio = $model->name;
            $model->company_type_id = null;
            $model->physical_no_patronymic = 1;
        }

        return $model;
    }
}