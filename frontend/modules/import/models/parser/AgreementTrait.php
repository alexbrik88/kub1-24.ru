<?php

namespace frontend\modules\import\models\parser;

use common\models\Agreement;
use common\models\document\status\AgreementStatus;
use frontend\models\Documents;
use frontend\modules\import\helpers\XmlHelper;

trait AgreementTrait {

    public function xml2Agreement(\SimpleXMLElement $xmlObject)
    {
        $contractorObj = XmlHelper::getPropertyByName($xmlObject->{'Ссылка'}, 'Владелец');

        $props = XmlHelper::getProperties($xmlObject);
        $linkProps = XmlHelper::getProperties($xmlObject->{'Ссылка'});
        $contractorProps = ($contractorObj) ? XmlHelper::getProperties($contractorObj->{'Ссылка'}) : [];

        $uid = $linkProps['{УникальныйИдентификатор}'] ?? null;

        $documentDate = $linkProps['Дата'] ?? null;
        $documentNumber = ($props['Номер'] ?? null) ?: 'б/н';

        $entity = [
            'uid' => $uid,
            'contractor_uid' => $contractorProps['{УникальныйИдентификатор}'] ?? null,
            'document_date' => ($documentDate) ? substr((string)$documentDate, 0, 10) : null,
            'document_number' => $documentNumber,
            'name' => $props['Наименование'] ?? null,
        ];

        if ($uid)
            $this->data[self::AGREEMENT][$uid] = $entity;
    }

    public function array2ContractorAgreement(Agreement &$model, $uid)
    {
        $array = &$this->data[self::AGREEMENT][$uid];

        ///////////////////////////
        $model->id = null;
        $model->isNewRecord = true;
        $model->object_guid = $uid;
        $model->one_c_imported = 1;
        ///////////////////////////

        $model->company_id = $this->_company->id;
        $model->contractor_id = null;
        $model->type = Documents::IO_TYPE_IN;
        $model->status_id = AgreementStatus::STATUS_CREATED;
        $model->title_template_id = 1;
        $model->document_type_id = 1;
        $model->is_created = true;
        $model->status_author_id = $this->_employee->id;
        $model->document_author_id = $this->_employee->id;
        $model->created_by = $this->_employee->id;
        $model->created_at = time();
        $model->document_date_input = ($array['document_date']) ? date('d.m.Y', strtotime($array['document_date'])) : null;
        $model->document_number = $array['document_number'];
        $model->document_name = $array['name'];

        return $model;
    }
}