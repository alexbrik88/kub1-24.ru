<?php

namespace frontend\modules\import\models\parser;

use frontend\modules\import\helpers\XmlHelper;

trait BankTrait {

    public function xml2Bank(\SimpleXMLElement $xmlObject)
    {
        $props = XmlHelper::getProperties($xmlObject);
        $linkProps = XmlHelper::getProperties($xmlObject->{'Ссылка'});

        $uid = $linkProps['{УникальныйИдентификатор}'] ?? null;

        $entity = [
            'uid' => $uid,
            'bik' => $linkProps['Код'] ?? null,
            'bank_name' => $props['Наименование' ?? null],
            'bank_city' => $props['Город'] ?? null,
            'ks' => $props['КоррСчет'] ?? null
        ];

        if ($uid)
            $this->data[self::BANK][$uid] = $entity;
    }

}