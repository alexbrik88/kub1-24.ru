<?php

namespace frontend\modules\import\models\parser;

use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductUnit;
use common\models\product\Store;
use frontend\modules\import\helpers\XmlHelper;

trait StoreTrait {

    public function xml2Store(\SimpleXMLElement $xmlObject)
    {
        $props = XmlHelper::getProperties($xmlObject);
        $linkProps = XmlHelper::getProperties($xmlObject->{'Ссылка'});

        $uid = $linkProps['{УникальныйИдентификатор}'] ?? null;

        $entity = [
            'uid' => $uid,
            'name' => mb_substr($props['Наименование'] ?? ($linkProps['Наименование'] ?? ''), 0, 60),
            'is_main' => 0 // todo
        ];

        if ($uid) {
            $this->data[self::STORE][$uid] = $entity;
        }
    }

    public function array2Store(Store &$model, $uid)
    {
        $array = &$this->data[self::STORE][$uid];

        ///////////////////////////
        $model->id = null;
        $model->isNewRecord = true;
        $model->object_guid = $uid;
        ///////////////////////////

        $model->company_id = $this->_company->id;
        $model->responsible_employee_id = 'all';
        $model->is_main = $array['is_main'];
        $model->is_closed = 0;
        $model->name = $array['name'];

        return $model;
    }
}