<?php

namespace frontend\modules\import\models\parser;

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\models\Agreement;
use common\models\AgreementType;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\document\OrderHelper;
use common\models\document\status\InvoiceStatus;
use common\models\TaxRate;
use frontend\models\Documents;
use frontend\modules\import\helpers\XmlHelper;
use common\models\product\Product;

trait InvoiceTrait {

    public function xml2InInvoice(\SimpleXMLElement $xmlObject) { $this->_xml2Invoice($xmlObject, self::IN_INVOICE); }
    public function xml2OutInvoice(\SimpleXMLElement $xmlObject) { $this->_xml2Invoice($xmlObject, self::OUT_INVOICE); }

    private function _xml2Invoice($xmlObject, $IO_KEY)
    {
        $contractorObj = XmlHelper::getPropertyByName($xmlObject, 'Контрагент');
        $agreementObj = XmlHelper::getPropertyByName($xmlObject, 'ДоговорКонтрагента');
        $currencyObj = XmlHelper::getPropertyByName($xmlObject, 'ВалютаДокумента');
        $tableProductObj = XmlHelper::getTagByName($xmlObject, 'ТабличнаяЧасть', 'Товары');
        $tableServiceObj = XmlHelper::getTagByName($xmlObject, 'ТабличнаяЧасть', 'Услуги');
        $tableEquipmentObj = XmlHelper::getTagByName($xmlObject, 'ТабличнаяЧасть', 'Оборудование');

        $props = XmlHelper::getProperties($xmlObject);
        $linkProps = XmlHelper::getProperties($xmlObject->{'Ссылка'});
        $currencyProps = ($currencyObj) ? XmlHelper::getProperties($currencyObj->{'Ссылка'}) : [];
        $contractorProps = ($contractorObj) ? XmlHelper::getProperties($contractorObj->{'Ссылка'}) : [];
        $agreementProps = ($agreementObj) ? XmlHelper::getProperties($agreementObj->{'Ссылка'}) : [];

        $uid = $linkProps['{УникальныйИдентификатор}'] ?? null;
        $hasNds = false;

        $products = [];
        foreach ([$tableProductObj, $tableEquipmentObj] as $productObj)
            if ($productObj && $productObj->children()->count()) {
                for ($i = 0; $i < $productObj->children()->count(); $i++) {
                    if ($recordObj = $productObj->children()->{'Запись'}[$i]) {
                        $recordProps = XmlHelper::getProperties($recordObj);
                        $nomenclatureObj = XmlHelper::getPropertyByName($recordObj, 'Номенклатура');
                        $nomenclatureProps = ($nomenclatureObj) ? XmlHelper::getProperties($nomenclatureObj->{'Ссылка'}) : [];
                        $products[] = [
                            'uid' => $nomenclatureProps['{УникальныйИдентификатор}'] ?? null,
                            'title' => $nomenclatureProps['Наименование'] ?? null,
                            'count' => ($recordProps['Количество'] ?? null) ?: 1,
                            'ndsKey' => $recordProps['СтавкаНДС'] ?? null,
                            'summ' => bcmul(100, $recordProps['Сумма'] ?? 0),
                            'ndsSumm' => bcmul(100, $recordProps['СуммаНДС'] ?? 0),
                            'price' =>  bcmul(100, $recordProps['Цена'] ?? 0),
                            'production_type' => Product::PRODUCTION_TYPE_GOODS
                        ];
    
                        // to set products nds id
                        if ($recordProps['СуммаНДС'] > 0) {
                            $hasNds = true;
                            $taxRateId = ArrayHelper::getValue(self::$dbConst['tax_rate'], $recordProps['СтавкаНДС'], TaxRate::RATE_WITHOUT);
                            if ($IO_KEY == self::OUT_INVOICE) {
                                $this->data[self::NOMENCLATURE_NDS][$nomenclatureProps['{УникальныйИдентификатор}']]['price_for_sell_nds_id'] = $taxRateId;
                            } elseif ($IO_KEY == self::IN_INVOICE) {
                                $this->data[self::NOMENCLATURE_NDS][$nomenclatureProps['{УникальныйИдентификатор}']]['price_for_buy_nds_id'] = $taxRateId;
                            }
                        }
                    }
                }
            }

        $services = [];
        foreach ([$tableServiceObj] as $serviceObj)
            if ($serviceObj && $serviceObj->children()->count()) {
                for ($i = 0; $i < $serviceObj->children()->count(); $i++) {
                    if ($recordObj = $serviceObj->children()->{'Запись'}[$i]) {
                        $recordProps = XmlHelper::getProperties($recordObj);
                        $nomenclatureObj = XmlHelper::getPropertyByName($recordObj, 'Номенклатура');
                        $nomenclatureProps = ($nomenclatureObj) ? XmlHelper::getProperties($nomenclatureObj->{'Ссылка'}) : [];
                        $services[] = [
                            'uid' => $nomenclatureProps['{УникальныйИдентификатор}'] ?? null,
                            'title' => $nomenclatureProps['Наименование'] ?? null,
                            'count' => ($recordProps['Количество'] ?? null) ?: 1,
                            'ndsKey' => $recordProps['СтавкаНДС'] ?? null,
                            'summ' => bcmul(100, $recordProps['Сумма'] ?? 0),
                            'ndsSumm' => bcmul(100, $recordProps['СуммаНДС'] ?? 0),
                            'price' =>  bcmul(100, $recordProps['Цена'] ?? 0),
                            'production_type' => Product::PRODUCTION_TYPE_SERVICE
                        ];

                        // to set products nds id
                        if ($recordProps['СуммаНДС'] > 0) {
                            $hasNds = true;
                            $taxRateId = ArrayHelper::getValue(self::$dbConst['tax_rate'], $recordProps['СтавкаНДС'], TaxRate::RATE_WITHOUT);
                            if ($IO_KEY == self::OUT_INVOICE) {
                                $this->data[self::NOMENCLATURE_NDS][$nomenclatureProps['{УникальныйИдентификатор}']]['price_for_sell_nds_id'] = $taxRateId;
                            } elseif ($IO_KEY == self::IN_INVOICE) {
                                $this->data[self::NOMENCLATURE_NDS][$nomenclatureProps['{УникальныйИдентификатор}']]['price_for_buy_nds_id'] = $taxRateId;
                            }
                        }
                    }
                }
            }

        if ($IO_KEY == self::IN_INVOICE) {
            $documentDate = ($linkProps['ДатаВходящегоДокумента'] ?? null) ?: ($linkProps['Дата'] ?? null);
            $documentNumber = ($linkProps['НомерВходящегоДокумента'] ?? null) ?: (($linkProps['Номер'] ?? null) ?: 'б/н');
        }
        else if ($IO_KEY == self::OUT_INVOICE) {
            $_outNumberObj = XmlHelper::getTagByName($xmlObject, 'ЗначениеПараметра', 'НомерИсходящегоДокумента');
            $documentDate = $linkProps['Дата'] ?? null;
            $documentNumber = ($_outNumberObj) ? (string)$_outNumberObj->{'Значение'} : null;
        } else {
            $documentDate = $documentNumber = null;
        }

        if (!isset($currencyProps['Код']) || $currencyProps['Код'] == '643') {
            $CURRENCY = self::$dbConst['currency']['643'];
        } else {
            $CURRENCY = self::$dbConst['currency'][$currencyProps['Код']] ?? null;
        }

        $entity = [
            'uid' => $uid,
            'contractor_uid' => $contractorProps['{УникальныйИдентификатор}'] ?? null,
            'agreement_uid' => $agreementProps['{УникальныйИдентификатор}'] ?? null,
            'document_date' => substr($documentDate, 0, 10),
            'document_number' => $documentNumber,
            'hasNds' => $hasNds,
            'isNdsExcluded' => (isset($props['СуммаВключаетНДС']) && $props['СуммаВключаетНДС'] === "false"),
            'products' => $products,
            'services' => $services,
            //'raw' => [
            //    'Комментарий' => $props['Комментарий'] ?? null,
            //    'СуммаДокумента' =>  $props['СуммаДокумента'] ?? null,
            //]
        ];

        if ($CURRENCY != 'RUB') {
            $entity = array_merge($entity,
            [
                'currency_name' => $CURRENCY,
                'currency_amount' => $props['КратностьВзаиморасчетов'] ?? Currency::DEFAULT_AMOUNT,
                'currency_rate' => $props['КурсВзаиморасчетов'] ?? Currency::DEFAULT_RATE
            ]);
        }

        if ($uid)
            $this->data[$IO_KEY][$uid] = $entity;
    }

    private function _array2Invoice(Invoice &$model, $uid, $IO_KEY)
    {
        $array = &$this->data[$IO_KEY][$uid];
        $numberArray = self::_getDocumentNumberArr($array['document_number'], $IO_KEY == self::OUT_INVOICE);

        $AGREEMENT_IO_KEY = ($IO_KEY == self::IN_INVOICE) ? self::IN_AGREEMENT : self::OUT_AGREEMENT;
        $CONTRACTOR_IO_KEY = ($IO_KEY == self::IN_INVOICE) ? self::CONTRACTOR_SELLER : self::CONTRACTOR_CUSTOMER;

        $agreementId  = $this->dbIds[$AGREEMENT_IO_KEY][$array['agreement_uid']] ?? null;
        $contractorId = $this->dbIds[$CONTRACTOR_IO_KEY][$array['contractor_uid']] ?? null;
        $productionType = ($array['products'] && $array['services']) ? '0, 1' : ($array['services'] ? '0' : '1');

        ///////////////////////////
        $model->id = null;
        $model->isNewRecord = true;
        $model->object_guid = $uid;
        $model->one_c_imported = 1;
        ///////////////////////////
        $createdAt = ($d = date_create($array['document_date'])) ? $d->getTimestamp() : null;
        if ($createdAt) {
            $model->detachBehavior('documentCreatedAtBehavior');
            $model->created_at = $createdAt;
            $model->updated_at = $createdAt;
        }

        $model->type = ($IO_KEY == self::IN_INVOICE) ? Documents::IO_TYPE_IN : Documents::IO_TYPE_OUT;
        $model->invoice_status_id = InvoiceStatus::STATUS_CREATED;
        $model->invoice_status_author_id = $this->_employee->id;
        $model->document_author_id = $this->_employee->id;
        $model->document_date = $array['document_date'];
        $model->document_number = $numberArray['document_number'];
        $model->document_additional_number = $numberArray['document_additional_number'];
        $model->is_additional_number_before = $numberArray['is_additional_number_before'];
        $model->production_type = $productionType;
        $model->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime('+10 days', strtotime($model->document_date)));
        $model->isAutoinvoice = 0;
        $model->company_checking_accountant_id = $this->_company->mainCheckingAccountant->id;
        $model->nds_view_type_id = ($array['hasNds']) ? ($array['isNdsExcluded'] ? Invoice::NDS_VIEW_OUT : Invoice::NDS_VIEW_IN) : Invoice::NDS_VIEW_WITHOUT;
        $model->price_precision = 2;

        // agreement
        if ($agreementId && ($agreement = Agreement::findOne($agreementId))) {
            // Договор&БП-000100&2020-05-16&1&172
            $model->agreement = implode('&', [
                AgreementType::TYPE_AGREEMENT_NAME,
                $agreement->document_number,
                $agreement->document_date,
                AgreementType::TYPE_AGREEMENT,
                $agreement->id
            ]);
        }


        // contractor
        if ($contractor = Contractor::findOne($contractorId)) {
            $model->contractor_id = $contractor->id;
            $model->contractor_name_short = $contractor->getTitle(true);
            $model->contractor_name_full = $contractor->getTitle(false);
            $model->contractor_address_legal_full = ($contractor->face_type == Contractor::TYPE_LEGAL_PERSON ? $contractor->legal_address : $contractor->physical_address);
            $model->contractor_bank_name = $contractor->bank_name;
            $model->contractor_bank_city = $contractor->bank_city;
            $model->contractor_bik = $contractor->BIC;
            $model->contractor_inn = $contractor->ITN;
            $model->contractor_kpp = $contractor->PPC;
            $model->contractor_ks = $contractor->corresp_account;
            $model->contractor_rs = $contractor->current_account;
            $contractorPaymentDelay = ($IO_KEY == self::IN_INVOICE)
                ? $contractor->seller_payment_delay
                : $contractor->customer_payment_delay;

            // contractor payment limit date
            if (strlen($contractorPaymentDelay))
                $model->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime("+{$contractorPaymentDelay} days", strtotime($model->document_date)));
        }

        // article
        if ($IO_KEY == self::IN_INVOICE) {
            $model->invoice_income_item_id = null;
            $model->invoice_expenditure_item_id = ($contractor)
                ? $contractor->invoice_expenditure_item_id
                : InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT;
        } else {
            $model->invoice_expenditure_item_id = null;
            $model->invoice_income_item_id = ($contractor)
                ? $contractor->invoice_income_item_id
                : InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER;
        }

        // currency
        if (isset($array['currency_name'])) {
            $model->currency_name = $array['currency_name'];
            $model->currency_rate_type = Currency::RATE_CUSTOM;
            $model->currencyAmountCustom = $array['currency_amount'];
            $model->currencyRateCustom = $array['currency_rate'];
            $model->currency_amount = $array['currency_amount'];
            $model->currency_rate_amount = $array['currency_amount'];
            $model->currency_rate = $array['currency_rate'];
            $model->currency_rate_value = $array['currency_rate'];
        } else {
            $model->currency_name = 'RUB';
            $model->currency_rate_type = Currency::RATE_SERTAIN_DATE;
            $model->currencyAmountCustom = 1;
            $model->currencyRateCustom = 1;
            $model->currency_amount = 1;
            $model->currency_rate_amount = 1;
            $model->currency_rate = 1;
            $model->currency_rate_value = 1;
        }

        // orders
        $orderArray = [];
        foreach ($array['products'] as $arrayProduct) {
            $kubProductId = $this->dbIds[self::NOMENCLATURE][$arrayProduct['uid']] ?? null;
            if ($kubProductId && ($product = Product::findOne(['id' => $kubProductId]))) {

                // nds
                if ($arrayProduct['ndsKey']) {
                    $taxRateId = ArrayHelper::getValue(self::$dbConst['tax_rate'], $arrayProduct['ndsKey'], TaxRate::RATE_WITHOUT);
                    if ($IO_KEY == self::OUT_INVOICE) {
                        $product->price_for_sell_nds_id = $taxRateId;
                    } elseif ($IO_KEY == self::IN_INVOICE) {
                        $product->price_for_buy_nds_id = $taxRateId;
                    }
                }

                $order = OrderHelper::createOrderByProduct($product, $model, $arrayProduct['count'], $arrayProduct['price']);
                $orderArray[] = $order;
            }
        }
        foreach ($array['services'] as $arrayService) {
            $kubServiceId = $this->dbIds[self::NOMENCLATURE][$arrayService['uid']] ?? null;
            if ($kubServiceId && ($service = Product::findOne(['id' => $kubServiceId]))) {

                // nds
                if ($arrayService['ndsKey']) {
                    $taxRateId = ArrayHelper::getValue(self::$dbConst['tax_rate'], $arrayService['ndsKey'], TaxRate::RATE_WITHOUT);
                    if ($IO_KEY == self::OUT_INVOICE) {
                        $service->price_for_sell_nds_id = $taxRateId;
                    } elseif ($IO_KEY == self::IN_INVOICE) {
                        $service->price_for_buy_nds_id = $taxRateId;
                    }
                }

                $order = OrderHelper::createOrderByProduct($service, $model, $arrayService['count'], $arrayService['price']);
                $orderArray[] = $order;
            }
        }

        $model->populateRelation('orders', $orderArray);

        array_walk($orderArray, function ($order, $key) { $order->number = $key + 1; });

        return $model;
    }
}