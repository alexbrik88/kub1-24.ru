<?php

namespace frontend\modules\import\models\parser;

use common\components\helpers\ArrayHelper;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\OrderUpd;
use common\models\document\Upd;
use common\models\product\Product;
use common\models\TaxRate;
use frontend\models\Documents;
use frontend\modules\import\helpers\XmlHelper;

trait UpdTrait {

    public function xml2InUpd(\SimpleXMLElement $xmlObject) { $this->_xml2Upd($xmlObject, self::IN_UPD); }
    public function xml2OutUpd(\SimpleXMLElement $xmlObject) { $this->_xml2Upd($xmlObject, self::OUT_UPD); }

    private function _xml2Upd($xmlObject, $IO_KEY)
    {
        $contractorObj = XmlHelper::getPropertyByName($xmlObject, 'Контрагент');
        $agreementObj = XmlHelper::getPropertyByName($xmlObject, 'ДоговорКонтрагента');
        $currencyObj = XmlHelper::getPropertyByName($xmlObject, 'ВалютаДокумента');
        $invoiceObj = ($IO_KEY == self::IN_UPD) ?
            XmlHelper::getPropertyByName($xmlObject, 'СчетНаОплатуПоставщика') :
            XmlHelper::getPropertyByName($xmlObject, 'СчетНаОплатуПокупателю');
        $tableProductObj = XmlHelper::getTagByName($xmlObject, 'ТабличнаяЧасть', 'Товары');
        $tableServiceObj = XmlHelper::getTagByName($xmlObject, 'ТабличнаяЧасть', 'Услуги');
        $tableEquipmentObj = XmlHelper::getTagByName($xmlObject, 'ТабличнаяЧасть', 'Оборудование');
        $storeObj = XmlHelper::getPropertyByName($xmlObject, 'Склад');

        $props = XmlHelper::getProperties($xmlObject);
        $linkProps = XmlHelper::getProperties($xmlObject->{'Ссылка'});
        $currencyProps = ($currencyObj) ? XmlHelper::getProperties($currencyObj->{'Ссылка'}) : [];
        $contractorProps = ($contractorObj) ? XmlHelper::getProperties($contractorObj->{'Ссылка'}) : [];
        $agreementProps = ($agreementObj) ? XmlHelper::getProperties($agreementObj->{'Ссылка'}) : [];
        $invoiceProps = ($invoiceObj) ? XmlHelper::getProperties($invoiceObj->{'Ссылка'}) : [];
        $storeProps = ($storeObj) ? XmlHelper::getProperties($storeObj->{'Ссылка'}) : [];

        $uid = $linkProps['{УникальныйИдентификатор}'] ?? null;
        $hasNds = false;

        $products = [];
        foreach ([$tableProductObj, $tableEquipmentObj] as $productObj)
            if ($productObj && $productObj->children()->count()) {
                for ($i = 0; $i < $productObj->children()->count(); $i++) {
                    if ($recordObj = $productObj->children()->{'Запись'}[$i]) {
                        $recordProps = XmlHelper::getProperties($recordObj);
                        $nomenclatureObj = XmlHelper::getPropertyByName($recordObj, 'Номенклатура');
                        $nomenclatureProps = ($nomenclatureObj) ? XmlHelper::getProperties($nomenclatureObj->{'Ссылка'}) : [];
                        $products[] = [
                            'uid' => $nomenclatureProps['{УникальныйИдентификатор}'] ?? null,
                            'title' => $nomenclatureProps['Наименование'] ?? null,
                            'count' => ($recordProps['Количество'] ?? null) ?: 1,
                            'ndsKey' => $recordProps['СтавкаНДС'] ?? null,
                            'summ' => bcmul(100, $recordProps['Сумма'] ?? 0),
                            'ndsSumm' => bcmul(100, $recordProps['СуммаНДС'] ?? 0),
                            'price' =>  bcmul(100, $recordProps['Цена'] ?? 0),
                            'production_type' => Product::PRODUCTION_TYPE_GOODS
                        ];

                        // to set products nds id
                        if ($recordProps['СуммаНДС'] > 0) {
                            $hasNds = true;
                            $taxRateId = ArrayHelper::getValue(self::$dbConst['tax_rate'], $recordProps['СтавкаНДС'], TaxRate::RATE_WITHOUT);
                            if ($IO_KEY == self::OUT_UPD) {
                                $this->data[self::NOMENCLATURE_NDS][$nomenclatureProps['{УникальныйИдентификатор}']]['price_for_sell_nds_id'] = $taxRateId;
                            } elseif ($IO_KEY == self::IN_UPD) {
                                $this->data[self::NOMENCLATURE_NDS][$nomenclatureProps['{УникальныйИдентификатор}']]['price_for_buy_nds_id'] = $taxRateId;
                            }
                        }
                    }
                }
            }

        $services = [];
        foreach ([$tableServiceObj] as $serviceObj)
            if ($serviceObj && $serviceObj->children()->count()) {
                for ($i = 0; $i < $serviceObj->children()->count(); $i++) {
                    if ($recordObj = $serviceObj->children()->{'Запись'}[$i]) {
                        $recordProps = XmlHelper::getProperties($recordObj);
                        $nomenclatureObj = XmlHelper::getPropertyByName($recordObj, 'Номенклатура');
                        $nomenclatureProps = ($nomenclatureObj) ? XmlHelper::getProperties($nomenclatureObj->{'Ссылка'}) : [];

                        // todo
                        if (empty($nomenclatureProps['{УникальныйИдентификатор}']))
                            continue;

                        $services[] = [
                            'uid' => $nomenclatureProps['{УникальныйИдентификатор}'] ?? null,
                            'title' => $nomenclatureProps['Наименование'] ?? null,
                            'count' => ($recordProps['Количество'] ?? null) ?: 1,
                            'ndsKey' => $recordProps['СтавкаНДС'] ?? null,
                            'summ' => bcmul(100, $recordProps['Сумма'] ?? 0),
                            'ndsSumm' => bcmul(100, $recordProps['СуммаНДС'] ?? 0),
                            'price' =>  bcmul(100, $recordProps['Цена'] ?? 0),
                            'production_type' => Product::PRODUCTION_TYPE_SERVICE
                        ];

                        // to set products nds id
                        if ($recordProps['СуммаНДС'] > 0) {
                            $hasNds = true;
                            $taxRateId = ArrayHelper::getValue(self::$dbConst['tax_rate'], $recordProps['СтавкаНДС'], TaxRate::RATE_WITHOUT);
                            if ($IO_KEY == self::OUT_UPD) {
                                $this->data[self::NOMENCLATURE_NDS][$nomenclatureProps['{УникальныйИдентификатор}']]['price_for_sell_nds_id'] = $taxRateId;
                            } elseif ($IO_KEY == self::IN_UPD) {
                                $this->data[self::NOMENCLATURE_NDS][$nomenclatureProps['{УникальныйИдентификатор}']]['price_for_buy_nds_id'] = $taxRateId;
                            }
                        }
                    }
                }
            }

        if ($IO_KEY == self::IN_UPD) {
            $documentDate = ($linkProps['ДатаВходящегоДокумента'] ?? null) ?: ($linkProps['Дата'] ?? null);
            $documentNumber = ($linkProps['НомерВходящегоДокумента'] ?? null) ?: (($linkProps['Номер'] ?? null) ?: 'б/н');
        }
        else if ($IO_KEY == self::OUT_UPD) {
            $_outNumberObj = XmlHelper::getTagByName($xmlObject, 'ЗначениеПараметра', 'НомерИсходящегоДокумента');
            $documentDate = $linkProps['Дата'] ?? null;
            $documentNumber = ($_outNumberObj) ? (string)$_outNumberObj->{'Значение'} : null;
        } else {
            $documentDate = $documentNumber = null;
        }

        if (!isset($currencyProps['Код']) || $currencyProps['Код'] == '643') {
            $CURRENCY = self::$dbConst['currency']['643'];
        } else {
            $CURRENCY = self::$dbConst['currency'][$currencyProps['Код']] ?? null;
        }

        $entity = [
            'uid' => $uid,
            'contractor_uid' => $contractorProps['{УникальныйИдентификатор}'] ?? null,
            'agreement_uid' => $agreementProps['{УникальныйИдентификатор}'] ?? null,
            'invoice_uid' => $invoiceProps['{УникальныйИдентификатор}'] ?? null,
            'store_uid' => $storeProps['{УникальныйИдентификатор}'] ?? null,
            'document_date' => substr($documentDate, 0, 10),
            'document_number' => $documentNumber,
            'hasNds' => $hasNds,
            'isNdsExcluded' => (isset($props['СуммаВключаетНДС']) && $props['СуммаВключаетНДС'] === "false"),
            'products' => $products,
            'services' => $services,
            //'raw' => [
            //    'НДСВключенВСтоимость' => (isset($props['НДСВключенВСтоимость']) && $props['НДСВключенВСтоимость'] === "true") ? 1 : 0,
            //    'Комментарий' => $props['Комментарий'] ?? null,
            //    'СуммаДокумента' =>  $props['СуммаДокумента'] ?? null,
            //]
        ];

        if ($CURRENCY != 'RUB') {
            $entity = array_merge($entity,
            [
                'currency_name' => $CURRENCY,
                'currency_amount' => $props['КратностьВзаиморасчетов'] ?? Currency::DEFAULT_AMOUNT,
                'currency_rate' => $props['КурсВзаиморасчетов'] ?? Currency::DEFAULT_RATE
            ]);
        }

        if ($uid)
            $this->data[$IO_KEY][$uid] = $entity;
    }

    private function _array2Upd(Upd &$model, Invoice $invoice, $uid, $IO_KEY)
    {
        $array = &$this->data[$IO_KEY][$uid];
        $numberArray = self::_getDocumentNumberArr($array['document_number'], $IO_KEY == self::OUT_UPD);

        ///////////////////////////
        $model->id = null;
        $model->isNewRecord = true;
        $model->object_guid = $uid;
        $model->one_c_imported = 1;
        $model->company_id = $invoice->company_id;
        $model->contractor_id = $invoice->contractor_id;
        ///////////////////////////

        $model->type = ($IO_KEY == self::IN_UPD) ? Documents::IO_TYPE_IN : Documents::IO_TYPE_OUT;
        $model->invoice_id = $invoice->id;
        $model->documentDate = ($array['document_date']) ? date('d.m.Y', strtotime($array['document_date'])) : null;
        $model->document_date = $array['document_date'];
        $model->document_number = $numberArray['document_number'];
        $model->document_additional_number = $numberArray['document_additional_number'];
        $model->document_author_id = $this->_employee->id;
        $model->status_out_author_id = $this->_employee->id;
        $model->status_out_updated_at = time();

        // orders
        $ownOrderArray = [];
        $ordersSum = 0;
        $orderNds = 0;
        if ($invoice) {

            foreach ($array['products'] as $arrayProduct) {
                $productId = $this->dbIds[self::NOMENCLATURE][$arrayProduct['uid']];
                foreach ($invoice->orders as $order) {
                    if ($productId == $order->product_id && !array_key_exists($order->id, $ownOrderArray)) {
                        $ownOrderArray[$order->id] = self::_createOrderUpd($order, $arrayProduct['count']);
                        $ordersSum += $arrayProduct['summ'];
                        $orderNds += $arrayProduct['ndsSumm'];
                        continue 2;
                    }
                }
            }

            foreach ($array['services'] as $arrayService) {
                $serviceId = $this->dbIds[self::NOMENCLATURE][$arrayService['uid']];
                foreach ($invoice->orders as $order) {
                    if ($serviceId == $order->product_id && !array_key_exists($order->id, $ownOrderArray)) {
                        $ownOrderArray[$order->id] = self::_createOrderUpd($order, $arrayService['count']);
                        $ordersSum += $arrayService['summ'];
                        $orderNds += $arrayService['ndsSumm'];
                        continue 2;
                    }
                }
            }

            $model->orders_sum = $ordersSum;
            $model->order_nds = $orderNds;
            $model->remaining_amount = $ordersSum;
        }

        $model->populateRelation('invoice', $invoice);
        $model->populateRelation('ownOrders', $ownOrderArray);

        return $model;
    }
    
    private function _createOrderUpd(Order $order, $count) 
    {
        return new OrderUpd([
            'upd_id' => null,
            'order_id' => $order->id,
            'product_id' => $order->product_id,
            'quantity' => $count,
            'country_id' => $order->country_id,
            'custom_declaration_number' => $order->custom_declaration_number,
        ]);
    }
}