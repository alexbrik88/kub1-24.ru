<?php

namespace frontend\modules\import\models\parser;

use common\models\ContractorAccount;
use frontend\modules\import\helpers\XmlHelper;

trait BankAccountTrait {

    public function xml2BankAccount(\SimpleXMLElement $xmlObject)
    {
        $bankObj = XmlHelper::getPropertyByName($xmlObject, 'Банк');
        $contractorObj = XmlHelper::getPropertyByName($xmlObject->{'Ссылка'}, 'Владелец');

        $props = XmlHelper::getProperties($xmlObject);
        $linkProps = XmlHelper::getProperties($xmlObject->{'Ссылка'});
        $bankProps = ($bankObj) ? XmlHelper::getProperties($bankObj->{'Ссылка'}) : [];
        $contractorProps = ($contractorObj) ? XmlHelper::getProperties($contractorObj->{'Ссылка'}) : [];

        $uid = $linkProps['{УникальныйИдентификатор}'] ?? null;

        $entity = [
            'uid' => $uid,
            'bank_uid' => $bankProps['{УникальныйИдентификатор}'] ?? null,
            'contractor_uid' => $contractorProps['{УникальныйИдентификатор}'] ?? null,
            'rs' => $props['НомерСчета'] ?? null,
        ];

        if ($uid)
            $this->data[self::BANK_ACCOUNT][$uid] = $entity;
    }

    public function array2ContractorAccount(ContractorAccount $model, $uid)
    {
        if ($uid && isset($this->data[self::BANK_ACCOUNT][$uid])) {

            $array = &$this->data[self::BANK_ACCOUNT][$uid];

            if ($array['bank_uid'] && isset($this->data[self::BANK][$array['bank_uid']])) {

                $bankArray = $this->data[self::BANK][$array['bank_uid']];

                ///////////////////////////
                $model->id = null;
                $model->isNewRecord = true;
                ///////////////////////////

                $model->contractor_id = null;
                $model->bik = $bankArray['bik'];
                $model->rs = $array['rs'];
                $model->ks = $bankArray['ks'];
                $model->bank_name = $bankArray['bank_name'];
                $model->bank_city = $bankArray['bank_city'];

                return $model;
            }
        }

        return null;
    }

}