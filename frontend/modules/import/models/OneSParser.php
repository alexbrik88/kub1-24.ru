<?php
namespace frontend\modules\import\models;

use common\models\Company;
use common\models\employee\Employee;
use yii\base\Exception;
use frontend\modules\import\models\parser\AgreementTrait;
use frontend\modules\import\models\parser\BankAccountTrait;
use frontend\modules\import\models\parser\BankTrait;
use frontend\modules\import\models\parser\ContractorTrait;
use frontend\modules\import\models\parser\InvoiceFactureTrait;
use frontend\modules\import\models\parser\InvoiceTrait;
use frontend\modules\import\models\parser\UpdTrait;
use frontend\modules\import\models\parser\NomenclatureTrait;
use frontend\modules\import\models\parser\NomenclatureGroupTrait;
use frontend\modules\import\models\parser\StoreTrait;

class OneSParser extends OneS {

    use AgreementTrait;
    use BankAccountTrait;
    use BankTrait;
    use ContractorTrait;
    use InvoiceTrait;
    use InvoiceFactureTrait;
    use UpdTrait;
    use NomenclatureTrait;
    use NomenclatureGroupTrait;
    use StoreTrait;

    public static $SOURCE = [
        // dict
        self::CONTRACTOR => 'СправочникСсылка.Контрагенты',
        self::AGREEMENT => 'СправочникСсылка.ДоговорыКонтрагентов',
        self::BANK => 'СправочникСсылка.Банки',
        self::BANK_ACCOUNT => 'СправочникСсылка.БанковскиеСчета',
        self::NOMENCLATURE => 'СправочникСсылка.Номенклатура',
        self::NOMENCLATURE_GROUP => 'СправочникСсылка.НоменклатурныеГруппы',
        self::STORE => 'СправочникСсылка.Склады',
        // doc
        self::IN_INVOICE => 'ДокументСсылка.СчетНаОплатуПоставщика',
        self::IN_UPD => 'ДокументСсылка.ПоступлениеТоваровУслуг',
        // self::IN_INVOICE_FACTURE => 'ДокументСсылка.СчетФактураПолученный',
        // doc
        self::OUT_INVOICE => 'ДокументСсылка.СчетНаОплатуПокупателю',
        self::OUT_UPD => 'ДокументСсылка.РеализацияТоваровУслуг',
        // self::OUT_INVOICE_FACTURE => 'ДокументСсылка.СчетФактураВыданный',
    ];

    public static $GETTER = [
        // dict
        self::CONTRACTOR => 'xml2Contractor',
        self::AGREEMENT => 'xml2Agreement',
        self::BANK => 'xml2Bank',
        self::BANK_ACCOUNT => 'xml2BankAccount',
        self::NOMENCLATURE => 'xml2Nomenclature',
        self::NOMENCLATURE_GROUP => 'xml2NomenclatureGroup',
        self::STORE => 'xml2Store',
        // doc
        self::IN_INVOICE => 'xml2InInvoice',
        self::IN_UPD => 'xml2InUpd',
        self::IN_INVOICE_FACTURE => 'xml2InInvoiceFacture',
        // doc
        self::OUT_INVOICE => 'xml2OutInvoice',
        self::OUT_UPD => 'xml2OutUpd',
        self::OUT_INVOICE_FACTURE => 'xml2OutInvoiceFacture',
    ];

    public $data = [
        // dict
        self::CONTRACTOR => [],
        self::AGREEMENT => [],
        self::BANK => [],
        self::BANK_ACCOUNT => [],
        self::NOMENCLATURE => [],
        self::NOMENCLATURE_GROUP => [],
        self::NOMENCLATURE_NDS => [],
        self::STORE => [],
        // doc
        self::IN_INVOICE => [],
        self::IN_UPD => [],
        self::IN_INVOICE_FACTURE => [],
        // doc
        self::OUT_INVOICE => [],
        self::OUT_UPD => [],
        self::OUT_INVOICE_FACTURE => []
    ];

    private $_fileDescriptor;
    private $_fileStringNum = 0;

    private $_company;
    private $_employee;

    public $importPeriod;

    /**
     * OneSParser constructor.
     * @var Employee $employee
     * @var Company|null $company
     */
    public function __construct(Employee $employee, Company $company = null)
    {
        $this->_employee = $employee;
        $this->_company = $company ?: $employee->currentEmployeeCompany->company;
    }

    /**
     * @param $fileName
     * @throws Exception
     */
    public function open($fileName)
    {
        if (is_file($fileName))
            $this->_fileDescriptor = fopen($fileName, 'r');
        else
            throw new Exception('File not found');
    }

    /**
     * @throws Exception
     */
    public function close()
    {
        if ($this->_fileDescriptor)
            fclose($this->_fileDescriptor);
        else
            throw new Exception('File not found');
    }

    /**
     * @throws Exception
     */
    public function parse()
    {
        if (!$this->_fileDescriptor) {
            throw new Exception('File not found');
        }

        $this->_skipTag('ПравилаОбмена');

        while ($xmlObject = $this->_getTag('Объект')) {

            $tagType = (string)$xmlObject->attributes()->{'Тип'};

            if (in_array($tagType, self::$SOURCE)) {
                $tagId = array_search($tagType, self::$SOURCE);
                $tagGetter = self::$GETTER[$tagId] ?? null;
                if (method_exists($this, $tagGetter)) {
                    call_user_func_array([$this, $tagGetter], [$xmlObject]);
                }
            }
        }
    }

    private function _skipTag($TAG)
    {
        $tagOpen = $tagClosed = 0;
        while (($line = fgets($this->_fileDescriptor)) !== false) {

            $this->_fileStringNum++;

            if (strpos($line, "<{$TAG}>") !== false) {
                $tagOpen++;
            }
            elseif (strpos($line, "</{$TAG}>") !== false) {
                $tagClosed++;
            }

            if ($tagOpen == 0 && $tagClosed == 0 && strpos($line, "ФайлОбмена") !== false) {
                preg_match('/НачалоПериодаВыгрузки="([0-9]{4}-[0-9]{2}-[0-9]{2})/', $line, $mStart);
                preg_match('/ОкончаниеПериодаВыгрузки="([0-9]{4}-[0-9]{2}-[0-9]{2})/', $line, $mEnd);
                if (isset($mStart[1]) && isset($mEnd[1])) {
                    $this->importPeriodStart = date('d.m.Y', strtotime($mStart[1]));
                    $this->importPeriodEnd = date('d.m.Y', strtotime($mEnd[1]));
                }
            }
            if ($tagOpen === 1 && $tagClosed == 1) {

                return true;
            }
            elseif ($tagOpen > 1 || $tagClosed > 1) {
                throw new Exception('Parsing Error #2 ('.$this->_fileStringNum.')');
            }
        }

        if (!$tagOpen || !$tagClosed) {
            throw new Exception('Parsing Error #1 ('.$this->_fileStringNum.')');
        }

        return false;
    }

    private function _getTag($TAG)
    {
        $tagOpen = $tagClosed = 0;
        $object = '';
        while (($line = fgets($this->_fileDescriptor)) !== false) {

            $this->_fileStringNum++;

            if (strpos($line, "<{$TAG}") !== false) {
                $tagOpen++;
            }
            elseif (strpos($line, "</{$TAG}>") !== false) {
                $tagClosed++;
            }

            if ($tagOpen === 1) {
                $object .= $line;
            }
            if ($tagClosed === 1) {
                if ($xml = simplexml_load_string($object, "\SimpleXMLElement", LIBXML_NOCDATA)) {
                    return $xml;
                } else {
                    throw new Exception('Parsing Error #3 ('.$this->_fileStringNum.')');
                }
            }
            elseif ($tagOpen > 1 || $tagClosed > 1) {
                throw new Exception('Parsing Error #2 ('.$this->_fileStringNum.')');
            }
        }

        if (($tagOpen && !$tagClosed) || (!$tagOpen && $tagClosed)) {
            throw new Exception('Parsing Error #1 ('.$this->_fileStringNum.')');
        }

        return null;
    }
}