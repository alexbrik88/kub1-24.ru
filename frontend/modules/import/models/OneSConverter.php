<?php
namespace frontend\modules\import\models;

use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\document\OrderHelper;
use common\models\employee\Employee;
use common\models\product\Product;
use common\models\product\ProductStore;
use common\models\TaxRate;
use frontend\modules\import\models\converter\AgreementsTrait;
use frontend\modules\import\models\converter\ContractorsTrait;
use frontend\modules\import\models\converter\InvoiceFacturesTrait;
use frontend\modules\import\models\converter\InvoicesTrait;
use frontend\modules\import\models\converter\ProductGroupsTrait;
use frontend\modules\import\models\converter\StoresTrait;
use frontend\modules\import\models\converter\UpdsTrait;
use frontend\modules\import\models\converter\ProductsTrait;
use frontend\modules\import\models\parser\AgreementTrait;
use frontend\modules\import\models\parser\BankAccountTrait;
use frontend\modules\import\models\parser\BankTrait;
use frontend\modules\import\models\parser\ContractorTrait;
use frontend\modules\import\models\parser\InvoiceFactureTrait;
use frontend\modules\import\models\parser\InvoiceTrait;
use frontend\modules\import\models\parser\NomenclatureGroupTrait;
use frontend\modules\import\models\parser\StoreTrait;
use frontend\modules\import\models\parser\UpdTrait;
use frontend\modules\import\models\parser\NomenclatureTrait;
use common\models\Contractor;

class OneSConverter extends OneS {

    // parser
    use AgreementTrait;
    use BankAccountTrait;
    use BankTrait;
    use ContractorTrait;
    use InvoiceTrait;
    use InvoiceFactureTrait;
    use UpdTrait;
    use NomenclatureTrait;
    use NomenclatureGroupTrait;
    use StoreTrait;

    // converter
    use AgreementsTrait;
    use ContractorsTrait;
    use ProductsTrait;
    use ProductGroupsTrait;
    use InvoicesTrait;
    use UpdsTrait;
    use InvoiceFacturesTrait;
    use StoresTrait;

    public static $NAMES = [
        // dict
        self::CONTRACTOR => 'Контрагенты',
        self::AGREEMENT => 'ДоговорыКонтрагентов',
        self::BANK => 'Банки',
        self::BANK_ACCOUNT => 'БанковскиеСчета',
        self::NOMENCLATURE => 'Номенклатура',
        self::NOMENCLATURE_GROUP => 'НоменклатурныеГруппы',
        self::STORE => 'Склады',
        // doc
        self::IN_INVOICE => 'СчетНаОплатуПоставщика',
        self::IN_UPD => 'ПоступлениеТоваровУслуг',
        self::IN_INVOICE_FACTURE => 'СчетФактураПолученный',
        // doc
        self::OUT_INVOICE => 'СчетНаОплатуПокупателю',
        self::OUT_UPD => 'РеализацияТоваровУслуг',
        self::OUT_INVOICE_FACTURE => 'СчетФактураВыданный',
    ];

    public $dbIds = [
        // dict
        self::CONTRACTOR_SELLER => [],
        self::CONTRACTOR_CUSTOMER => [],
        self::AGREEMENT => [],
        self::BANK => [],
        self::BANK_ACCOUNT => [],
        self::NOMENCLATURE => [],
        self::STORE => [],
        // doc
        self::IN_INVOICE => [],
        self::IN_UPD => [],
        self::IN_INVOICE_FACTURE => []
    ];

    public $stat = [
        // dict
        self::CONTRACTOR => 0,
        self::AGREEMENT => 0,
        self::BANK => 0,
        self::BANK_ACCOUNT => 0,
        self::NOMENCLATURE => 0,
        self::NOMENCLATURE_GROUP => 0,
        self::STORE => 0,
        // doc
        self::IN_INVOICE => 0,
        self::IN_UPD => 0,
        self::IN_INVOICE_FACTURE => 0,
        self::OUT_INVOICE => 0,
        self::OUT_UPD => 0,
        self::OUT_INVOICE_FACTURE => 0,

    ];

    public $doubles = [
        // dict
        self::CONTRACTOR => 0,
        self::AGREEMENT => 0,
        self::BANK => 0,
        self::BANK_ACCOUNT => 0,
        self::NOMENCLATURE => 0,
        self::NOMENCLATURE_GROUP => 0,
        self::STORE => 0,
        // doc
        self::IN_INVOICE => 0,
        self::IN_UPD => 0,
        self::IN_INVOICE_FACTURE => 0,
        self::OUT_INVOICE => 0,
        self::OUT_UPD => 0,
        self::OUT_INVOICE_FACTURE => 0,
    ];

    public $data = [];

    /** @var Company $_company */
    private $_company;
    /** @var Employee $_employee */
    private $_employee;
    /** @var array $_errors */
    private $_errors;
    /** @var int $_progressBar */
    private $_progressBar = -1;

    private $_contractorType = [
        self::CONTRACTOR_SELLER => [],
        self::CONTRACTOR_CUSTOMER => []
    ];

    /**
     * OneSConverter constructor.
     * @var Employee $employee
     * @var Company|null $company
     */
    public function __construct(Employee $employee, Company $company = null)
    {
        $this->_employee = $employee;
        $this->_company = $company ?: $employee->currentEmployeeCompany->company;
    }

    /**
     * @param $key
     * @param $name
     * @param $errors
     */
    public function addError($key, $name, $errors)
    {
        $msg = '';
        $msg .= self::$NAMES[$key] ?? null;
        $msg .= ' ' . $name . ': ';
        foreach ($errors as $error)
            $msg .= implode(', ', $error);

        $this->_errors[] = $msg;
    }

    public function generateInvoicesFromUpds($INVOICES_KEY, $UPDS_KEY)
    {
        foreach ($this->data[$UPDS_KEY] as $uid => $arr) {
            $invoiceUid = $arr['invoice_uid'] ?? null;
            if (!$invoiceUid) {
                $generatedInvoiceUid = self::_generateInvoiceUid($uid);
                $this->data[$UPDS_KEY][$uid]['invoice_uid'] = $generatedInvoiceUid;
                $this->data[$INVOICES_KEY][$generatedInvoiceUid] = $arr;
            }
        }
    }

    public function splitContractorsByType()
    {
        $SEARCH_IN_DOCS = [
            self::CONTRACTOR_SELLER => [self::IN_UPD, self::IN_INVOICE],
            self::CONTRACTOR_CUSTOMER => [self::OUT_UPD, self::OUT_INVOICE]
        ];

        foreach ($SEARCH_IN_DOCS as $TYPE => $IO_KEYS)
            foreach ($IO_KEYS as $IO_KEY)
                foreach ($this->data[$IO_KEY] as $uid => $arr)
                    if ($contractorUid = $arr['contractor_uid'])
                        $this->_contractorType[$TYPE][$contractorUid] = 1;
    }

    public function getContractorTypes()
    {
        return $this->_contractorType;
    }

    public function updateProductsPrices()
    {
        foreach ($this->dbIds[OneSConverter::NOMENCLATURE] as $uid => $productId) {

            if ($product = Product::findOne(['company_id' => $this->_company->id, 'id' => $productId])) {

                $updateAttrs = [];
                $_priceForBuy = $product->getGrowingPriceForBuy();
                $_nds = $this->data[self::NOMENCLATURE_NDS][$product->object_guid] ?? null;

                if ($_priceForBuy != $product->price_for_buy_with_nds) {
                    $updateAttrs = ['price_for_buy_with_nds' => $_priceForBuy];
                }
                if ($_nds) {
                    if (isset($_nds['price_for_buy_nds_id']) && $_nds['price_for_buy_nds_id'] != $product->price_for_buy_nds_id) {
                        $updateAttrs['price_for_buy_nds_id'] = $_nds['price_for_buy_nds_id'];
                    }
                    if (isset($_nds['price_for_sell_nds_id']) && $_nds['price_for_sell_nds_id'] != $product->price_for_sell_nds_id) {
                        $updateAttrs['price_for_sell_nds_id'] = $_nds['price_for_sell_nds_id'];
                    }
                }

                if ($updateAttrs) {
                    $product->updateAttributes($updateAttrs);
                }
            }
        }

        return true;
    }

    // public function preloadNomenclatureFromInUpd()
    // {
    //     foreach ($this->data[self::IN_UPD] as $uid => $arr) {
    //         foreach ($arr['products'] as $arrProduct) {
    //             $productUid = $arrProduct['uid'];
    //             if (!isset($this->data[self::NOMENCLATURE][$productUid]))
    //                 $this->data[self::NOMENCLATURE][$productUid] = $arrProduct;
    //         }
    //         foreach ($arr['services'] as $arrService) {
    //             $serviceUid = $arrService['uid'];
    //             if (!isset($this->data[self::NOMENCLATURE][$serviceUid]))
    //                 $this->data[self::NOMENCLATURE][$serviceUid] = $arrService;
    //         }
    //     }
    // }

    /**
     * @return array
     */
    public function getErrors()
    {
        return (array)$this->_errors;
    }

    /**
     * @param $attr
     * @return string
     */
    public function getFormattedReport($attr)
    {
        $ret = '<ul>';
        foreach ($this->{$attr} as $key => $cnt) {

            if (in_array($key, [self::AGREEMENT, self::BANK, self::BANK_ACCOUNT]))
                continue;

            $ret .= '<li>' . self::$NAMES[$key] . ': ' . $cnt . '</li>';
        }
        $ret .= '</ul>';

        return $ret;
    }

    public function updateProgressBar($completed = false)
    {
        if (++$this->_progressBar % 20 === 0 || $completed) {
            $file = @fopen(sys_get_temp_dir() . "/import1c_progress_c{$this->_company->id}e{$this->_employee->id}.tmp", "w");
            @fwrite($file, json_encode([
                'completed' => (int)$completed,
                'uploadedItems' => $this->getFormattedReport('stat'),
                'doubleItems' => $this->getFormattedReport('doubles')
            ]));
            @fclose($file);
        }
    }

    public function getProgressBar() {
        return @file_get_contents(sys_get_temp_dir() . "/import1c_progress_c{$this->_company->id}e{$this->_employee->id}.tmp");
    }

    private function _getUidByUid($tagType, $searchField, $searchUID)
    {
        foreach ($this->data[$tagType] as $key => $val) {
            if (isset($val[$searchField]) && $val[$searchField] == $searchUID)
                return (string)$key;
        }

        return null;
    }

    private static function _generateInvoiceUid($updUID)
    {
        return 'UPD-' . str_replace('-', '', $updUID);
    }

    private static function _isGeneratedInvoice($UID)
    {
        return 'UPD-' === substr($UID, 0, 4);
    }

    private static function _getDocumentNumberArr($number1C, $isOut)
    {
        if ($isOut) {
            preg_match_all('/[\d]+/', $number1C, $m);
            if (count($m[0]) == 0) {
                $num = $additional_num = null;
            }
            elseif (count($m[0]) == 1) {
                $num = array_pop($m[0]);
                $additional_num = str_replace($num, '', $number1C);
            }
            else {
                $maxLen = 0;
                $num = '';
                foreach ($m[0] as $mm) {
                    if (strlen($mm) > $maxLen) {
                        $num = $mm;
                        $maxLen = strlen($num);
                    }
                }
                $additional_num = str_replace($num, '', $number1C);
            }
            $is_additional_number_before = ($num && strpos($number1C, (string)$num) > 0);
        } else {
            $num = $number1C;
            $additional_num = null;
            $is_additional_number_before = false;
        }

        return [
            'document_number' => $num,
            'document_additional_number' => $additional_num,
            'is_additional_number_before' => $is_additional_number_before
        ];
    }
}