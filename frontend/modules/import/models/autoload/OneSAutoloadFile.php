<?php

namespace frontend\modules\import\models\autoload;

use common\components\helpers\ModelHelper;
use Yii;
use common\models\Company;

/**
 * This is the model class for table "one_s_autoload_file".
 *
 * @property int $id
 * @property int $company_id
 * @property int $email_id
 * @property string $filename
 * @property int $created_at
 * @property int|null $uploaded_at
 *
 * @property Company $company
 * @property OneSAutoloadFile $email
 */
class OneSAutoloadFile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'one_s_autoload_file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'email_id', 'filename'], 'required'],
            [['company_id', 'email_id', 'created_at', 'uploaded_at'], 'integer'],
            [['filename'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['email_id'], 'exist', 'skipOnError' => true, 'targetClass' => OneSAutoloadEmail::className(), 'targetAttribute' => ['email_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'email_id' => 'Email ID',
            'filename' => 'Filename',
            'created_at' => 'Created At',
            'uploaded_at' => 'Uploaded At',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            ModelHelper::HtmlEntitiesModelAttributes($this);

            if ($insert) {
                $this->created_at = time();
            }

            return true;
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmail()
    {
        return $this->hasOne(OneSAutoloadEmail::className(), ['id' => 'email_id']);
    }
}
