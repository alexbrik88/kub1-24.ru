<?php

/* NOT USED (03.09.2020) */

namespace frontend\modules\import\models\autoload;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * OneSAutoloadHelper
 */
class OneSAutoloadHelper {

    static protected $inbox;
    static $lastErrorCode;
    //static $deleteAfterLoad = true;

    /**
     * @param OneSAutoloadEmail $model
     * @return bool
     */
    static public function createMailbox(OneSAutoloadEmail $model) {

        if (!$model->password) {
            $model->password = self::randomPassword();
            $model->updateAttributes(['password']);
        }

        $vst_hostname = ArrayHelper::getValue(Yii::$app->params['vesta'], 'hostname');
        $vst_username = ArrayHelper::getValue(Yii::$app->params['vesta'], 'username');
        $vst_password = ArrayHelper::getValue(Yii::$app->params['vesta'], 'password');
        $vst_returncode = 'yes';
        $vst_command = 'v-add-mail-account';
        $vst_mail_user = ArrayHelper::getValue(Yii::$app->params['vesta'], 'mail_user');
        $vst_mail_domain = ArrayHelper::getValue(Yii::$app->params['vesta'], 'one_s_mail_domain');

        // New Email
        $account = current(explode('@', $model->email));
        $password = $model->password;

        // Prepare POST query
        $postvars = [
            'user' => $vst_username,
            'password' => $vst_password,
            'returncode' => $vst_returncode,
            'cmd' => $vst_command,
            'arg1' => $vst_mail_user,
            'arg2' => $vst_mail_domain,
            'arg3' => $account,
            'arg4' => $password,
        ];

        // Send POST query via cURL
        $postdata = http_build_query($postvars);

        $curl = new \common\components\curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postdata,
        ])->post('https://' . $vst_hostname . ':8083/api/');

        $postvars['username'] = $postvars['password'] = '******';

        // Check result
        if($response == "0") {
            return true;
        } else {
            self::$lastErrorCode = $response;
        }

        return false;
    }

    /**
     * @param int $length
     * @return bool|string
     */
    static private function randomPassword($length = 16)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $length = rand(10, 16);
        $password = substr( str_shuffle(sha1(rand() . time()) . $chars ), 0, $length);

        return $password;
    }

    static public function getFiles() {

        if (!self::$inbox)
            return false;

        /* grab emails */
        $messages = imap_search(self::$inbox, 'ALL');

        $NEW_FILES = [];

        /* if emails are returned, cycle through each... */
        if($messages) {

            /* put the newest emails on top */
            rsort($messages);

            foreach($messages as $msg_number) {

                /* get information specific to this email */
                $header = imap_header(self::$inbox, $msg_number);
                $structure = imap_fetchstructure(self::$inbox,$msg_number);
                preg_match("/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,10})/", $header->fromaddress, $matches);
                $fromEmail = ($matches) ? $matches[0] : 'unknown';

                $attachments = array();
                if(isset($structure->parts) && count($structure->parts)) {
                    for($i = 0; $i < count($structure->parts); $i++) {
                        $attachments[$i] = array(
                            'is_attachment' => false,
                            'filename' => '',
                            'name' => '',
                            'attachment' => '');

                        if($structure->parts[$i]->ifdparameters) {
                            foreach($structure->parts[$i]->dparameters as $object) {

                                if(strtolower($object->attribute) == 'filename') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['filename'] = self::decodeFilename($object->value);
                                }
                            }
                        }

                        if($structure->parts[$i]->ifparameters) {
                            foreach($structure->parts[$i]->parameters as $object) {
                                if(strtolower($object->attribute) == 'name') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['name'] = $object->value;
                                }
                            }
                        }

                        if($attachments[$i]['is_attachment']) {
                            $attachments[$i]['attachment'] = imap_fetchbody(self::$inbox, $msg_number, $i+1);
                            if($structure->parts[$i]->encoding == 3) { // 3 = BASE64
                                $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                            }
                            elseif($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
                                $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                            }
                        }
                    } // for($i = 0; $i < count($structure->parts); $i++)
                } // if(isset($structure->parts) && count($structure->parts))

                if(count($attachments) != 0) {
                    foreach($attachments as $at) {
                        if($at['is_attachment'] == 1) {

                            $tmpFilename = tempnam(sys_get_temp_dir(), 'scan');
                            file_put_contents($tmpFilename, $at['attachment']);

                            $tmpFile = [
                                'from_email' => $fromEmail,
                                'filename' => $at['filename'],
                                'filesize' => strlen($at['attachment']),
                                'type' => mime_content_type($tmpFilename),
                                'tmp_name' => $tmpFilename,
                                'msg_number' => $msg_number
                            ];
                            $NEW_FILES[] = $tmpFile;

                        }
                    }
                }

                imap_delete(self::$inbox, $msg_number);

                BREAK; // only 1 message
            }
        }

        return $NEW_FILES;
    }

    static public function expungeMessages()
    {
        imap_expunge(self::$inbox);
    }

    static public function connect(OneSAutoloadEmail $model)
    {
        $imap_host = ArrayHelper::getValue(Yii::$app->params['vesta'], 'imap_host');
        $imap_port = ArrayHelper::getValue(Yii::$app->params['vesta'], 'imap_port');
        $hostname = '{'.$imap_host.':'.$imap_port.'/novalidate-cert/tls}INBOX';
        $username = $model->email;
        $password = $model->password;

        /* try to connect */
        if (!(self::$inbox = @imap_open($hostname,$username,$password,0,1))) {
            Yii::warning(__METHOD__ . ' ' . imap_last_error() . ' ' . $model->email . "\n");
            return false;
        }

        return true;
    }

    static public function closeConnection()
    {
        imap_close(self::$inbox);
    }

    static protected function decodeFilename($filename, $encoding = 'UTF-8')
    {
        // No encoding
        if (strpos($filename, '=?') === false)
            return $filename;

        $ret = '';
        $parts = explode('?=', $filename);

        if (count($parts)) {

            foreach ($parts as $str) {
                //Get parts of the string
                $arrStr = explode('?', $str);

                //second part of array should be an encoding name
                if (isset($arrStr[1]) && in_array($arrStr[1], mb_list_encodings())) {

                    switch ($arrStr[2]) {

                        case 'B': //base64 encoded
                            $str = base64_decode($arrStr[3]);
                            break;

                        case 'Q': //quoted printable encoded
                            $str = quoted_printable_decode($arrStr[3]);
                            break;
                    }

                    //convert it to UTF-8
                    $ret .= mb_convert_encoding($str, $encoding, $arrStr[1]);
                }
            }
        }

        return $ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
}