<?php
namespace frontend\modules\import\models\autoload;

use common\models\Company;
use common\models\employee\Employee;
use frontend\modules\import\models\Import1c;
use frontend\modules\import\models\OneS;
use Yii;
use yii\base\Exception;
use frontend\modules\import\models\OneSConverter;
use frontend\modules\import\models\OneSParser;

class OneSImportHelper {

    /**
     * DEBUG
     */
    const DEBUG_MODE = true;

    /**
     * 
     */
    const ERROR_MESSAGE = 'Что-то пошло не так. Повторите действие позже или обратитесь в службу поддержки.';

    /**
     * @param string $file
     * @param Company $company
     * @param Employee $employee
     * @return array
     */
    static public function importFile(string $file, Company $company, Employee $employee)
    {
        set_error_handler(function($errno, $errstr, $errfile, $errline, $errcontext) {
            // error was suppressed with the @-operator
            if (0 === error_reporting()) {
                return false;
            }

            throw new Exception($errstr, 0);
        }, E_WARNING);

        if (is_file($file)) {

            $start = microtime(true);

            $parser = new OneSParser($employee);
            $converter = new OneSConverter($employee);
            // rel
            $converter->data = &$parser->data;
            $converter->importPeriodStart = &$parser->importPeriodStart;
            $converter->importPeriodEnd = &$parser->importPeriodEnd;

            try {
                // parse XML
                $parser->open($file);
                $parser->parse();
                $parser->close();

                // DEBUG
                // return $parser->data;

            } catch (Exception $e) {

                //$converter->updateProgressBar(true);
                self::_logParseError($company, $employee, $parser, $e->getMessage());
                //self::_removeTmpFile($file);

                $ret = [
                    'result' => false,
                    'message' => 'PARSER ERROR',
                    'errorMessage' => $e->getMessage()
                ];

                // LOG & CLEAR MEMORY
                self::debugLog($ret);
                unset($converter);
                unset($parser);

                return $ret;
            }
            // preload
            $converter->generateInvoicesFromUpds(OneSConverter::IN_INVOICE, OneSConverter::IN_UPD);
            $converter->generateInvoicesFromUpds(OneSConverter::OUT_INVOICE, OneSConverter::OUT_UPD);
            $converter->splitContractorsByType();

            try {

                Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS=0;')->execute();

                // save dicts
                $converter->insertContractors();
                $converter->insertAgreements();
                $converter->insertStores();
                $converter->insertProductGroups();
                $converter->insertProducts();
                // in docs
                $converter->insertInvoices(OneSConverter::IN_INVOICE);
                $converter->insertUpds(OneSConverter::IN_UPD);
                $converter->insertInvoiceFactures(OneSConverter::IN_INVOICE_FACTURE);
                // out docs
                $converter->insertInvoices(OneSConverter::OUT_INVOICE);
                $converter->insertUpds(OneSConverter::OUT_UPD);
                $converter->insertInvoiceFactures(OneSConverter::OUT_INVOICE_FACTURE);
                // products prices
                $converter->updateProductsPrices();

                Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS=1;')->execute();

            } catch (\Throwable $e) {

                //$converter->updateProgressBar(true);
                self::_logConvertError($company, $employee, $converter, $e->getMessage());
                //self::_removeTmpFile($file);

                $ret = [
                    'result' => false,
                    'message' => 'CONVERTER ERROR',
                    'errorMessage' => $e->getMessage()
                ];

                // LOG & CLEAR MEMORY
                self::debugLog($ret);
                unset($converter);
                unset($parser);

                return $ret;
            }

            //$converter->updateProgressBar(true);
            self::_logSuccess($company, $employee, $parser, $converter);
            //self::_removeTmpFile($file);

            $ret = [
                'result' => true,
                'message' => 'SUCCESS',
                'uploadedItems' => $converter->stat,
                'doubleItems' => $converter->doubles,
                'timeUsage' => 'Time usage: '.round(microtime(true) - $start, 4).'s',
                'memoryUsage' => 'Memory: ' . round(memory_get_usage() / 1024 / 1024, 4) . 'Mb'
            ];

            // LOG & CLEAR MEMORY
            self::debugLog($ret);
            unset($converter);
            unset($parser);

            return $ret;
        }

        $ret = [
            'result' => false,
            'message' => 'FILE NOT FOUND',
            'errorMessage' => 'filepath: ' . $file
        ];

        // LOG & CLEAR MEMORY
        self::debugLog($ret);
        unset($converter);
        unset($parser);

        return $ret;
    }

    /**
     * @param $file
     */
    private static function _removeTmpFile($file)
    {
        @unlink($file);
    }

    private static function _logSuccess(Company $company, Employee $employee, OneSParser $parser, OneSConverter $converter)
    {
        $log = new Import1c();
        $log->company_id = $company->id;
        $log->employee_id = $employee->id;
        $log->created_at = time();
        $log->period = $parser->importPeriodStart .' - '. $parser->importPeriodEnd;
        $log->total_contractors = $converter->stat[OneSConverter::CONTRACTOR];
        $log->total_products = $converter->stat[OneSConverter::NOMENCLATURE];
        $log->total_in_invoices = $converter->stat[OneSConverter::IN_INVOICE];
        $log->total_in_upds = $converter->stat[OneSConverter::IN_UPD];
        $log->total_in_invoice_factures = $converter->stat[OneSConverter::IN_INVOICE_FACTURE];
        $log->total_out_invoices = $converter->stat[OneSConverter::OUT_INVOICE];
        $log->total_out_upds = $converter->stat[OneSConverter::OUT_UPD];
        $log->total_out_invoice_factures = $converter->stat[OneSConverter::OUT_INVOICE_FACTURE];
        $log->errors_messages = mb_substr(implode("\n", $converter->getErrors()), 0, 2E4, 'UTF-8');
        $log->is_completed = 1;
        $log->is_autoload = 1;
        $log->save(false);
    }

    private static function _logParseError(Company $company, Employee $employee, OneSParser $parser, $errorMessage = '')
    {
        $log = new Import1c();
        $log->company_id = $company->id;
        $log->employee_id = $employee->id;
        $log->created_at = time();
        $log->period = $parser->importPeriodStart .' - '. $parser->importPeriodEnd;
        $log->errors_messages = $errorMessage;
        $log->is_completed = 0;
        $log->is_autoload = 1;
        $log->save(false);
    }

    private static function _logConvertError(Company $company, Employee $employee, OneSConverter $converter, $errorMessage = '')
    {
        $log = new Import1c();
        $log->company_id = $company->id;
        $log->employee_id = $employee->id;
        $log->created_at = time();
        $log->period = $converter->importPeriodStart .' - '. $converter->importPeriodEnd;
        $log->total_contractors = $converter->stat[OneSConverter::CONTRACTOR];
        $log->total_products = $converter->stat[OneSConverter::NOMENCLATURE];
        $log->total_in_invoices = $converter->stat[OneSConverter::IN_INVOICE];
        $log->total_in_upds = $converter->stat[OneSConverter::IN_UPD];
        $log->total_in_invoice_factures = $converter->stat[OneSConverter::IN_INVOICE_FACTURE];
        $log->total_out_invoices = $converter->stat[OneSConverter::OUT_INVOICE];
        $log->total_out_upds = $converter->stat[OneSConverter::OUT_UPD];
        $log->total_out_invoice_factures = $converter->stat[OneSConverter::OUT_INVOICE_FACTURE];
        $log->errors_messages = $errorMessage;
        $log->is_completed = 0;
        $log->is_autoload = 1;
        $log->save(false);
    }

    // DEBUG LOG
    /**
     * @inheritdoc
     */
    private static function debugLog($msg = null)
    {
        if (self::DEBUG_MODE) {
            $logFile = Yii::getAlias('@runtime/logs/debug_import_1c_cron.log');
            file_put_contents($logFile, self::getLogMsg($msg), FILE_APPEND);
        }
    }

    private static function getLogMsg($msg = null)
    {
        $time = date('c');

        $msg = "=========" . $time . "====================================\n" .
            json_encode($msg) . "\n" .
            "======================================================================\n\n";

        return $msg;
    }
}