<?php
namespace frontend\modules\import\controllers;

use common\components\filters\AccessControl;
use common\models\Company;
use frontend\components\FrontendController;
use frontend\modules\import\models\autoload\OneSAutoloadEmail;
use frontend\modules\import\models\Import1c;
use frontend\modules\import\models\Import1cSearch;
use frontend\rbac\UserRole;
use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;

/**
 * Class OneSController
 * @package frontend\modules\import\controllers
 */
class OneSController extends FrontendController
{
    public $layout = '//one_s';

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF, UserRole::ROLE_ACCOUNTANT],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;

        $model = new Import1c();
        $searchModel = new Import1cSearch();
        $emailModel = OneSAutoloadEmail::findOne(['company_id' => $company->id]);

        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('index', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'emailModel' => $emailModel
        ]);
    }

    public function actionGetAutoloadModal()
    {
        $this->layout = '@frontend/views/layouts/empty';

        /* @var $company Company */
        $company = Yii::$app->user->identity->company;

        $model = OneSAutoloadEmail::findOne([
            'company_id' => $company->id,
        ]);

        // First create
        if (!$model) {
            $model = new OneSAutoloadEmail();
            $model->enabled = false;
            $model->company_id = $company->id;
            do {
                $model->email = OneSAutoloadEmail::generateEmail($company->getShortName());
                if (!filter_var($model->email, FILTER_VALIDATE_EMAIL)) {
                    throw new Exception('Can\'t generate email, please check domain config');
                }
            } while (!$model->validate(['email']));

            $model->save(false);
        }

        $oldEmail = $model->email;
        if ($model->load(Yii::$app->request->post())) {
            $model->email = $model->customEmailPart .'.'. $model->basisEmailPart;
            if ($model->validate() && $model->validateBasisPart($oldEmail)) {
                $model->enabled = true;
                $model->save();
                Yii::$app->session->setFlash('success', 'Email включен.');

                return $this->redirect(Yii::$app->request->referrer ?: '/import/one-s/index');
            }
        }

        return $this->renderAjax('_viewAuto/_modal_content', [
            'model' => $model,
            'oldEmail' => $oldEmail
        ]);
    }
}