<?php

namespace frontend\modules\import\controllers;

use common\models\Company;
use common\models\employee\Employee;
use Exception;
use frontend\modules\import\models\Import1c;
use frontend\modules\import\models\OneSConverter;
use frontend\modules\import\models\OneSParser;
use Yii;
use common\components\filters\AccessControl;
use frontend\components\FrontendController;
use frontend\components\XlsHelper;
use frontend\rbac\UserRole;
use himiklab\thumbnail\FileNotFoundException;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Class OneSAjaxController
 * @package frontend\controllers
 */
class OneSAjaxController extends FrontendController
{
    const ERROR_MESSAGE = 'Что-то пошло не так. Повторите действие позже или обратитесь в службу поддержки.';
    const PROCESS_MESSAGE = 'Дождитесь окончания импорта предыдущего файла';

    /**
     * @var bool
     */
    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF, UserRole::ROLE_ACCOUNTANT],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return array|bool
     */
    public function actionUpload()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;
        /** @var Company $company */
        $company = $employee->company;

        $file = UploadedFile::getInstanceByName('uploadfile');

        if (Yii::$app->session->get('import1c_saved_file')) {
            return [
                'result' => false,
                'message' => self::PROCESS_MESSAGE,
            ];
        }

        if ($file && is_file($file->tempName)) {

            Yii::$app->session->set('import1c_saved_file', $this->_saveTmpFile($file->tempName, $employee, $company));

            /** @var Employee $employee */
            $employee = Yii::$app->user->identity;
            $converter = new OneSConverter($employee);
            $converter->updateProgressBar();

            return [
                'result' => true,
                'message' => 'Файл успешно загружен',
                'uploadedItems' => $converter->getFormattedReport('stat'),
                'doubleItems' => $converter->getFormattedReport('doubles'),
            ];
        }

        return [
            'result' => false,
            'message' => self::ERROR_MESSAGE,
            '_info' => 'File not uploaded successfully'
        ];
    }

    /**
     * @return array|bool
     */
    public function actionParse()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;
        /** @var Company $company */
        $company  = $employee->company;

        ini_set('max_execution_time', 7200);
        ini_set('memory_limit', '4096M');

        $file = Yii::$app->session->remove('import1c_saved_file');

        if ($file && is_file($file)) {

            $start = microtime(true);

            $parser = new OneSParser($employee);
            $converter = new OneSConverter($employee);
            // rel
            $converter->data = &$parser->data;
            $converter->importPeriodStart = &$parser->importPeriodStart;
            $converter->importPeriodEnd = &$parser->importPeriodEnd;

            try {
                // parse XML
                $parser->open($file);
                $parser->parse();
                $parser->close();

                // DEBUG
                // return $parser->data;

            } catch (Exception $e) {

                $converter->updateProgressBar(true);

                $msg = (YII_ENV_DEV || $employee->id === 2) ?
                    $e->getFile().' '.$e->getLine().' '.$e->getMessage() :
                    $e->getMessage();

                $this->_logParseError($company, $employee, $parser, $msg);
                // $this->_removeTmpFile($file);

                return [
                    'result' => false,
                    'message' => self::ERROR_MESSAGE,
                    'errorMessage' => $e->getMessage()
                ];
            }
            // preload
            $converter->generateInvoicesFromUpds(OneSConverter::IN_INVOICE, OneSConverter::IN_UPD);
            $converter->generateInvoicesFromUpds(OneSConverter::OUT_INVOICE, OneSConverter::OUT_UPD);
            $converter->splitContractorsByType();

            try {

                Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS = 0;')->execute();

                // save dicts
                $converter->insertContractors();
                $converter->insertAgreements();
                $converter->insertStores();
                $converter->insertProductGroups();
                $converter->insertProducts();
                // in docs
                $converter->insertInvoices(OneSConverter::IN_INVOICE);
                $converter->insertUpds(OneSConverter::IN_UPD);
                $converter->insertInvoiceFactures(OneSConverter::IN_INVOICE_FACTURE);
                // out docs
                $converter->insertInvoices(OneSConverter::OUT_INVOICE);
                $converter->insertUpds(OneSConverter::OUT_UPD);
                $converter->insertInvoiceFactures(OneSConverter::OUT_INVOICE_FACTURE);
                // products prices
                $converter->updateProductsPrices();

                Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS = 1;')->execute();

            } catch (\Throwable $e) {

                $converter->updateProgressBar(true);

                $msg = (YII_ENV_DEV || $employee->id === 2) ?
                    $e->getFile().' '.$e->getLine().' '.$e->getMessage() :
                    $e->getMessage();

                $this->_logConvertError($company, $employee, $converter, $msg);
                // $this->_removeTmpFile($file);

                return [
                    'result' => false,
                    'message' => self::ERROR_MESSAGE,
                    'errorMessage' => $e->getMessage()
                ];
            }

            $converter->updateProgressBar(true);
            $this->_logSuccess($company, $employee, $parser, $converter);
            $this->_removeTmpFile($file);

            return [
                'result' => true,
                'message' => 'Файл успешно загружен',
                'uploadedItems' => $converter->getFormattedReport('stat'),
                'doubleItems' => $converter->getFormattedReport('doubles'),
                'timeUsage' => 'Время выполнения скрипта: '.round(microtime(true) - $start, 4).' сек.',
                'memoryUsage' => 'Память: ' . round(memory_get_usage() / 1024 / 1024, 4) . 'Mb'
            ];
        }

        return [
            'result' => false,
            'message' => self::ERROR_MESSAGE,
            'errorMessage' => 'File not found'
        ];
    }

    private function _saveTmpFile($path, $employee, $company) {
        $newPath = sys_get_temp_dir() . "/import1c_c{$company->id}e{$employee->id}.tmp";
        if (move_uploaded_file($path, $newPath))
            return $newPath;

        return "";
    }

    private function _removeTmpFile($file) {
        @unlink($file);
    }

    private function _logSuccess(Company $company, Employee $employee, OneSParser $parser, OneSConverter $converter)
    {
        $log = new Import1c();
        $log->company_id = $company->id;
        $log->employee_id = $employee->id;
        $log->created_at = time();
        $log->period = $parser->importPeriodStart .' - '. $parser->importPeriodEnd;
        $log->total_contractors = $converter->stat[OneSConverter::CONTRACTOR];
        $log->total_products = $converter->stat[OneSConverter::NOMENCLATURE];
        $log->total_in_invoices = $converter->stat[OneSConverter::IN_INVOICE];
        $log->total_in_upds = $converter->stat[OneSConverter::IN_UPD];
        $log->total_in_invoice_factures = $converter->stat[OneSConverter::IN_INVOICE_FACTURE];
        $log->total_out_invoices = $converter->stat[OneSConverter::OUT_INVOICE];
        $log->total_out_upds = $converter->stat[OneSConverter::OUT_UPD];
        $log->total_out_invoice_factures = $converter->stat[OneSConverter::OUT_INVOICE_FACTURE];
        $log->errors_messages = mb_substr(implode("\n", $converter->getErrors()), 0, 2E4, 'UTF-8');
        $log->is_completed = 1;
        $log->save(false);
    }

    private function _logParseError(Company $company, Employee $employee, OneSParser $parser, $errorMessage = '')
    {
        $log = new Import1c();
        $log->company_id = $company->id;
        $log->employee_id = $employee->id;
        $log->created_at = time();
        $log->period = $parser->importPeriodStart .' - '. $parser->importPeriodEnd;
        $log->errors_messages = $errorMessage;
        $log->is_completed = 0;
        $log->save(false);
    }

    private function _logConvertError(Company $company, Employee $employee, OneSConverter $converter, $errorMessage = '')
    {
        $log = new Import1c();
        $log->company_id = $company->id;
        $log->employee_id = $employee->id;
        $log->created_at = time();
        $log->period = $converter->importPeriodStart .' - '. $converter->importPeriodEnd;
        $log->total_contractors = $converter->stat[OneSConverter::CONTRACTOR];
        $log->total_products = $converter->stat[OneSConverter::NOMENCLATURE];
        $log->total_in_invoices = $converter->stat[OneSConverter::IN_INVOICE];
        $log->total_in_upds = $converter->stat[OneSConverter::IN_UPD];
        $log->total_in_invoice_factures = $converter->stat[OneSConverter::IN_INVOICE_FACTURE];
        $log->total_out_invoices = $converter->stat[OneSConverter::OUT_INVOICE];
        $log->total_out_upds = $converter->stat[OneSConverter::OUT_UPD];
        $log->total_out_invoice_factures = $converter->stat[OneSConverter::OUT_INVOICE_FACTURE];
        $log->errors_messages = $errorMessage;
        $log->is_completed = 0;
        $log->save(false);
    }
}
