<?php

namespace frontend\modules\import\controllers;

use common\models\employee\Employee;
use frontend\modules\import\models\OneSConverter;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class ProgressController
 * @package frontend\modules\export\controllers
 */
class ProgressController extends Controller
{
    /**
     * @var bool
     */
    public $enableCsrfValidation = false;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionOneS()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;

        $converter = new OneSConverter($employee);
        $progress  = $converter->getProgressBar();

        return json_decode($progress, true);
    }
}
