<?php

namespace frontend\modules\import\controllers;

use common\models\Company;
use frontend\modules\import\models\autoload\OneSAutoloadEmail;
use frontend\modules\import\models\autoload\OneSAutoloadFile;
use frontend\modules\import\models\OneS;
use Yii;
use common\components\filters\AccessControl;
use yii\base\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Class OneSImportFileController
 * @package frontend\controllers
 */
class OneSImportFileController extends Controller
{
    /**
     * DEBUG
     */
    const DEBUG_MODE = 1;

    /**
     * OPTIONS 
     */
    const MAX_FILE_SIZE = 64 * 1024 * 1024;
    const MAX_UNFINISHED_IMPORTS = 1;
    
    /**
     * MESSAGES
     */
    const MESSAGE_ERROR = 'Что-то пошло не так. Повторите действие позже или обратитесь в службу поддержки.';
    const MESSAGE_EMAIL_NOT_FOUND = 'Емайл не найден';
    //const MESSAGE_IN_PROCESS = 'Дождитесь окончания предыдущего файла';
    const MESSAGE_FILE_NOT_FOUND = 'Файл не найден';
    const MESSAGE_FILE_TOO_BIG = 'Файл слищком большой, макс. размер: ' . (self::MAX_FILE_SIZE / 1024 / 1024) . 'Мб';
    const MESSAGE_SUCCESS = 'Файл успешно загружен';
    const MESSAGE_FILE_EXTENSION = 'Возможна загрузка только файлов с расширением xml';
    const MESSAGE_INTERNAL_ERROR = 'Внутренняя ошибка сервера. Повторите действие позже или обратитесь в службу поддержки.';
    const MESSAGE_CANT_MOVE_FILE = 'Внутренняя ошибка сервера. Файл не может быть перемещен.';
    const MESSAGE_MAX_IMPORTED = 'Ваши файлы еще загружаются. Повторите попытку позднее.';

    /**
     * ERROR CODES
     */
    const ERROR_EMAIL_NOT_FOUND = 1;
    const ERROR_FILE_NOT_FOUND = 2;
    const ERROR_FILE_NOT_UPLOADED = 3;
    const ERROR_FILE_TOO_BIG = 4;
    const ERROR_FILE_EXTENSION = 5;
    const ERROR_MAX_IMPORTED = 6;
    const ERROR_INTERNAL = 7;

    /**
     * @var Company
     */
    private $company;

    /**
     * @var bool
     */
    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return array|\yii\db\ActiveRecord|null
     */
    private function findAutoloadEmail()
    {
        if ($email = base64_decode(Yii::$app->request->headers->get('Kub-Email')))
        {
            if (filter_var($email, FILTER_VALIDATE_EMAIL))
                return OneSAutoloadEmail::find()->where(['email' => $email, 'enabled' => 1])->one();
        }

        return null;
    }

    /**
     * @param $errorCode
     * @param $errorMessage
     * @return array
     */
    public static function getError($errorCode, $errorMessage)
    {
        $response = [
            'success' => 0,
            'error_code' => $errorCode,
            'message' => $errorMessage,
        ];

        self::debugLog($response);

        return $response;
    }

    /**
     * @return array
     * @throws \yii\base\Exception
     */
    public function actionIndex()
    {
        self::debugLog();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        /** @var OneSAutoloadEmail $model */
        $model = $this->findAutoloadEmail();

        if (!$model)
            return self::getError(self::ERROR_EMAIL_NOT_FOUND, self::MESSAGE_EMAIL_NOT_FOUND);
        if (!$this->checkMaxUnfinishedImports($model->company))
            return self::getError(self::ERROR_MAX_IMPORTED, self::MESSAGE_MAX_IMPORTED);
        //if (!$file)
        //    return self::getError(self::ERROR_FILE_NOT_FOUND, self::MESSAGE_FILE_NOT_FOUND);
        //if (!is_file($file->tempName))
        //    return self::getError(self::ERROR_FILE_NOT_UPLOADED, self::MESSAGE_FILE_NOT_FOUND);
        //if ($file->size > self::MAX_FILE_SIZE)
        //    return self::getError(self::ERROR_FILE_TOO_BIG, self::MESSAGE_FILE_TOO_BIG);
        //if ($file->extension != 'xml')
        //    return self::getError(self::ERROR_FILE_EXTENSION, self::MESSAGE_FILE_EXTENSION);

        /** @var Company $company */
        $company = $model->company;

        $newFilename = $this->_saveTmpFile($company->id);

        if ($newFilename) {

            $fileModel = new OneSAutoloadFile([
                'company_id' => $company->id,
                'email_id' => $model->id,
                'filename' => $newFilename
            ]);

            if ($fileModel->save()) {

                $success = [
                    'success' => 1,
                    'error_code' => null,
                    'message' => 'Файл успешно загружен',
                ];

                self::debugLog($success);

                return $success;
            } else {
                return self::getError(self::ERROR_INTERNAL, serialize($fileModel->getErrors()));
            }
        }

        return self::getError(self::ERROR_FILE_NOT_FOUND, self::MESSAGE_FILE_NOT_FOUND);
    }

    private function checkMaxUnfinishedImports($company)
    {
        return (OneSAutoloadFile::find()
                ->where(['company_id' => $company->id])
                ->andWhere(['uploaded_at' => null])
                ->count() < self::MAX_UNFINISHED_IMPORTS);
    }

    private function _saveTmpFile($companyId) {
        $now = time();
        $fileName = "import1c_auto_c{$companyId}_{$now}.tmp";
        $uploadsDir = OneS::getUploadsDir();
        if (!is_dir($uploadsDir))
            FileHelper::createDirectory($uploadsDir);

        $newPath = $uploadsDir . DIRECTORY_SEPARATOR . $fileName;

        $fp = fopen($newPath,"w");

        if (fwrite($fp, file_get_contents('php://input')))
            return $fileName;

        return "";
    }

    private function _removeTmpFile($file) {
        @unlink($file);
    }

    /**
     * @inheritdoc
     */
    private static function debugLog($msg = null)
    {
        if (self::DEBUG_MODE) {
            $logFile = Yii::getAlias('@runtime/logs/debug_import_1c.log');
            file_put_contents($logFile, self::getLogMsg($msg), FILE_APPEND);
        }
    }

    private static function getLogMsg($msg = null)
    {
        $time = date('c');

        if (!$msg) {

            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }

            $msg = "=========" . $time . "====================================\n" .
                "---------Request------------------------------------------------------\n" .
                'HEADERS: ' . json_encode(getallheaders()) . "\n" .
                'GET: ' . json_encode($_GET) . "\n" .
                'POST: ' . json_encode($_POST) . "\n" .
                'FILES:' . json_encode($_FILES) . "\n" .
                'IP: ' . $ip . "\n";
                //'BODY:' . file_get_contents('php://input') . "\n";
        } else {
            $msg = "---------Response-----------------------------------------------------\n" .
                json_encode($msg) . "\n" .
                "======================================================================\n\n";
        }

        return $msg;
    }
}