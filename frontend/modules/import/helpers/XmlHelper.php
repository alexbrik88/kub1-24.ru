<?php
namespace frontend\modules\import\helpers;

class XmlHelper {

    /**
     * @param $array
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    public static function getValue($array, $key, $default = null)
    {
        if (is_array($array) && (isset($array[$key]) || array_key_exists($key, $array))) {
            return $array[$key];
        }

        if (($pos = strrpos($key, '.')) !== false) {
            $array = static::getValue($array, substr($key, 0, $pos), $default);
            $key = substr($key, $pos + 1);
        }

        if (is_object($array)) {
            return $array->$key;
        } elseif (is_array($array)) {
            return (isset($array[$key]) || array_key_exists($key, $array)) ? $array[$key] : $default;
        }

        return $default;
    }

    /**
     * @param \SimpleXMLElement $element
     * @param $tag
     * @return array
     */
    public static function getTags(\SimpleXMLElement $element, $tag)
    {
        $result = [];

        if ($element->count()) {
            for ($i = 0; $i < $element->children()->count(); $i++) {
                if ($property = $element->children()->{$tag}[$i]) {
                    $key = (string)$property->attributes()->{'Имя'};
                    $val = (string)$property->{'Значение'};
                    $result[$key] = $val;
                }
            }
        }

        return $result;
    }

    /**
     * @param \SimpleXMLElement $element
     * @param $tag
     * @param $tagName
     * @return \SimpleXMLElement|null
     */
    public static function getTagByName(\SimpleXMLElement $element, $tag, $tagName)
    {
        if ($element->count()) {
            for ($i = 0; $i < $element->children()->count(); $i++) {
                if ($property = $element->children()->{$tag}[$i]) {
                    $key = (string)$property->attributes()->{'Имя'};
                    if ($key === $tagName)
                        return $property;
                }
            }
        }

        return null;
    }

    /**
     * @param \SimpleXMLElement $element
     * @return array
     */
    public static function getProperties(\SimpleXMLElement $element)
    {
        return self::getTags($element, 'Свойство');
    }

    /**
     * @param \SimpleXMLElement $element
     * @param $tagName
     * @return \SimpleXMLElement|null
     */
    public static function getPropertyByName(\SimpleXMLElement $element, $tagName)
    {
        return self::getTagByName($element, 'Свойство', $tagName);
    }
}