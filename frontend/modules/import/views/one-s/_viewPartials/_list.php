<?php
use common\components\grid\DropDownDataColumn;
use common\models\employee\Employee;
use frontend\modules\import\models\Import1c;
use common\components\date\DateHelper;
use common\components\TextHelper;

?>
<div class="table-container" style="">
    <div class="dataTables_wrapper dataTables_extended_wrapper">
        <?= common\components\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => [
                'class' => 'table table-style table-count-list',
                'aria-describedby' => 'datatable_ajax_info',
                'role' => 'grid',
            ],

            'headerRowOptions' => [
                'class' => 'heading',
            ],
            //'options' => [
            //    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
            //],
            'pager' => [
                'options' => [
                    'class' => 'nav-pagination list-clr',
                ],
            ],
            'layout' => $this->render('//layouts/grid/layout_no_scroll', ['totalCount' => $dataProvider->totalCount]),
            'columns' => [
                [
                    'attribute' => 'created_at',
                    'label' => 'Дата загрузки',
                    'headerOptions' => ['width' => '10%'],
                    'format' => 'raw',
                    'value' => function (Import1c $model) {
                        return date('d.m.Y', $model->created_at);
                    }
                ],
                [
                    'attribute' => 'employee_id',
                    'label' => 'Сотрудник',
                    'headerOptions' => ['width' => '15%'],
                    'filter' => $searchModel->getEmployeeByCompanyArray(\Yii::$app->user->identity->company->id),
                    'format' => 'raw',
                    'value' => function (Import1c $model) {
                        return $model->employee instanceof Employee ? $model->employee->getFio(true) : '-';
                    },
                    's2width' => '200px',
                ],
                [
                    'attribute' => 'period',
                    'label' => 'Период загрузки',
                    'headerOptions' => ['width' => '15%'],
                    'format' => 'raw',
                    'value' => function (Import1c $model) {
                        return $model->period;
                    },
                ],
                [
                    'attribute' => 'total_contractors',
                    'label' => 'Контрагентов',
                    'headerOptions' => ['width' => '5%'],
                    'format' => 'raw',
                    'value' => function (Import1c $model) {
                        return TextHelper::numberFormat($model->total_contractors, 0);
                    },
                ],
                [
                    'attribute' => 'total_contractors',
                    'label' => 'Товаров / Услуг',
                    'headerOptions' => ['width' => '5%'],
                    'format' => 'raw',
                    'value' => function (Import1c $model) {
                        return TextHelper::numberFormat($model->total_products, 0);
                    },
                ],
                [
                    'attribute' => 'total_contractors',
                    'label' => 'Счетов (Вх/Исх)',
                    'headerOptions' => ['width' => '5%'],
                    'format' => 'raw',
                    'value' => function (Import1c $model) {
                        return TextHelper::numberFormat($model->total_in_invoices, 0) . ' / ' .
                            TextHelper::numberFormat($model->total_out_invoices, 0);
                    },
                ],
                [
                    'attribute' => 'total_contractors',
                    'label' => 'УПД (Вх/Исх)',
                    'headerOptions' => ['width' => '5%'],
                    'format' => 'raw',
                    'value' => function (Import1c $model) {
                        return TextHelper::numberFormat($model->total_in_upds, 0) . ' / ' .
                            TextHelper::numberFormat($model->total_out_upds, 0);
                    },
                ],
                [
                    'attribute' => 'total_contractors',
                    'label' => 'Счетов-Фактур (Вх/Исх)',
                    'headerOptions' => ['width' => '5%'],
                    'format' => 'raw',
                    'value' => function (Import1c $model) {
                        return TextHelper::numberFormat($model->total_in_invoice_factures, 0) . ' / ' .
                            TextHelper::numberFormat($model->total_out_invoice_factures, 0);
                    },
                ],
                [
                    'attribute' => 'errors_messages',
                    'label' => 'Ошибки загрузки',
                    'headerOptions' => ['width' => '25%'],
                    'contentOptions' => ['style' => 'font-size: 12px'],
                    'format' => 'raw',
                    'value' => function (Import1c $model) {
                        return '<div style="max-height:99px; overflow-y:auto">'.nl2br($model->errors_messages).'</div>';
                    },
                ],
            ],
        ]); ?>
    </div>
</div>