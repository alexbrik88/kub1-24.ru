<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 21.12.2016
 * Time: 8:09
 */

use frontend\components\XlsHelper;
use yii\helpers\Json;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $header string
 * @var $text string
 * @var $formData string
 * @var $uploadXlsTemplateUrl string
 */

?>
<div class="modal fade" id="import-xml" tabindex="-1" role="modal" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">Загрузить данные из 1С</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">
                <div class="row">
                    <div class="col-6">
                        <?= \yii\helpers\Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']) . '<span>Выбрать файл</span>', 'javascript:;', [
                            'class' => 'button-regular button-width button-hover-content-red button-clr add-xml-file',
                            'style' => 'width:160px'
                        ]); ?>
                    </div>
                    <div class="col-6 upload-xml-button ta-r">
                        <?= \yii\bootstrap\Html::a('<span>Загрузить</span>', 'javascript:;', [
                            'class' => 'button-regular button-width button-hover-content-red button-clr disabled',
                            'data-tooltip-content' => '#upload-xml-tooltip',
                            'data-tooltipster' => Json::encode([
                                'contentAsHTML' => true,
                            ]),
                            'style' => 'width:160px'
                        ]); ?>
                        <div class="hidden">
                            <div id="upload-xml-tooltip">
                                Сначала добавьте файл для загрузки
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12" style="margin-top: 20px;">
                        <div class="xml-error"></div>
                        <div class="row">
                            <?php ActiveForm::begin([
                                'action' => '#',
                                'method' => 'post',
                                'id' => 'xml-ajax-form',
                                'options' => [
                                    'enctype' => 'multipart/form-data',
                                ],
                            ]); ?>
                            <?= Html::hiddenInput('createdModels', null, [
                                'class' => 'created-models',
                            ]); ?>
                            <div class="file-list col-12">
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                    <div class="col-12">
                        <div id="progressBox"></div>
                    </div>
                </div>
                <div class="row xls-title">
                    <div class="gray-alert col-12 mt-2">
                        Для загрузки данных из 1С, скачайте обработку для вашей 1С.<br/>
                        Откройте её в 1С и дальше действуйте по инструкции.<br/>
                        Выгруженный файл загрузите по кнопке "Выбрать файл".
                    </div>
                </div>
                <div class="row">
                    <div class="col-12" style="margin-top: 20px;">
                        <?= Html::a('Скачать обработку для 1С Бухгалтерия', Url::to(['/xls/download-epf', 'type' => 'import1C']), [
                            'class' => 'upload-xls-template-url',
                        ]); ?>
                    </div>
                    <?php /*
                    <div class="column mr-auto xml-action-buttons mt-4">
                        <?= Html::button('Закрыть', [
                            'class' => 'button-regular button-width button-clr modal-close-2',
                            'data-dismiss' => 'modal',
                        ]); ?>
                    </div>
                    */ ?>
                    <div class="col-12">
                        <div id="progressBox"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
