<?php

namespace frontend\modules\retail\models;

use common\models\ofd\OfdKkt;
use common\models\ofd\OfdOperationType;
use common\models\ofd\OfdOperator;
use common\models\ofd\OfdReceipt;
use common\models\ofd\OfdReceiptItem;
use common\models\ofd\OfdStore;
use frontend\components\PageSize;
use frontend\modules\retail\components\RetailPeriodTrait;
use yii\base\Model;
use yii\data\SqlDataProvider;

/**
 * OfdReportSearch represents the model behind the search form of `common\models\ofd\OfdReceipt`.
 */
class OfdReportSearch extends OfdReceiptItem
{
    use RetailPeriodTrait;

    public $store_id;
    public $kkt_id;
    public $operator_id;
    public $return;
    public $payment_type_id;
    public $product_type;

    protected $_period;
    protected $minDate;
    protected $periodDateRange;
    protected $_storeDropdownList;
    protected $_operatorDropdownList;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'store_id', 'kkt_id', 'operator_id', 'return', 'payment_type_id', 'product_type'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function setPeriod($period)
    {
        $this->_period['from'] = ($f = date_create($period['from'] ?? null)) ? $f->format('Y-m-d 00:00:00') : date('Y-m-d 00:00:00');
        $this->_period['to'] = ($f = date_create($period['to'] ?? null)) ? $f->format('Y-m-d 23:59:59') : date('Y-m-d 23:59:59');
    }

    /**
     * {@inheritdoc}
     */
    public function getPeriod()
    {
        if (!isset($this->_period)) {
            $this->_period = [
                'from' => date('Y-01-01 00:00:00'),
                'to' => date('Y-12-31 23:59:59'),
            ];
        }

        return $this->_period;
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название товара и услуги',
            'count' => 'Кол-во',
            'average_price' => 'Средняя цена',
            'amount' => 'Сумма продаж',
            'part' => 'Доля %',
            'store_id' => 'Точка продаж',
            'kkt_id' => 'Название ККТ',
            'operator_id' => 'Кассир',
            'return' => 'Возврат',
            'payment_type_id' => 'Тип оплаты',
            'product_type' => 'Товар или услуга',
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $year = (int) $this->year;

        $queryReport = OfdReceiptItem::find()->joinWith('receipt', false)->where([
            'and',
            ['ofd_receipt_item.company_id' => $this->company_id],
            ['between', 'ofd_receipt_item.date_time', $this->period['from'], $this->period['to']],
        ])->andFilterWhere([
            'store_id' => $this->store_id,
            'kkt_id' => $this->kkt_id,
            'operator_id' => $this->operator_id,
            'payment_type_id' => $this->payment_type_id,
        ])->andFilterWhere(['like', 'name', $this->name]);

        if (!empty($this->product_type)) {
            if ($this->product_type == 1) {
                $queryReport->andWhere(['product_type' => OfdReceiptItem::TYPE_PRODUCT]);
            } else {
                $queryReport->andWhere(['not', ['product_type' => OfdReceiptItem::TYPE_PRODUCT]]);
            }
        }
        if (!empty($this->return)) {
            $queryReport->andWhere(['not', ['operation_type_id' => OfdOperationType::INCOME_RETUSN]]);
        }

        $querySum = clone $queryReport;
        $queryCount = clone $queryReport;

        $queryReport->select([
            'name',
            'count' => 'SUM([[quantity]])',
            'amount' => 'SUM([[sum]])',
        ])->groupBy('name');
        $queryCount->select([
            'name',
        ])->groupBy('name');
        $querySum->select([
            's' => 'SUM([[sum]])',
        ]);

        $query = (new \yii\db\Query)->select([
            'name',
            'count',
            'amount',
            'average_price' => '({{t1}}.[[amount]]/{{t1}}.[[count]])',
            'part' => '({{t1}}.[[amount]] * 100 / {{t2}}.[[s]])',
        ])->from([
            't1' => $queryReport,
        ])->join('CROSS JOIN', [
            't2' => $querySum,
        ]);

        $dataProvider = new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'totalCount' => $queryCount->count(),
            'pagination' => ['pageSize' => PageSize::get()],
            'sort' => [
                'attributes' => [
                    'name',
                    'count',
                    'amount',
                    'average_price',
                    'part',
                ],
                'defaultOrder' => [
                    'part' => SORT_DESC
                ]
            ],
        ]);


        return $dataProvider;
    }

    /**
     * @inheritdoc
     */
    public function operatorDropdownList() : array
    {
        if ($this->_operatorDropdownList === null) {
            $this->_operatorDropdownList = ['' => 'Все'] + OfdOperator::find()->select([
                'name',
                'id',
            ])->andWhere([
                'company_id' => $this->company_id,
            ])->orderBy([
                'name' => SORT_ASC,
            ])->indexBy('id')->column();
        }

        return $this->_operatorDropdownList;
    }

    /**
     * @inheritdoc
     */
    public function storeDropdownList() : array
    {
        if ($this->_storeDropdownList === null) {
            $this->_storeDropdownList = ['' => 'Все'] + OfdStore::find()->select([
                'local_name',
                'id',
            ])->andWhere([
                'company_id' => $this->company_id,
            ])->orderBy([
                'local_name' => SORT_ASC,
            ])->indexBy('id')->column();
        }

        return $this->_storeDropdownList;
    }

    /**
     * @inheritdoc
     */
    public function kktDropdownList() : array
    {
        $request = OfdKkt::find()->select([
            'ofd_kkt.name',
            'ofd_kkt.id',
        ])->leftJoin(
            OfdStore::tableName(),
            '{{ofd_store}}.[[uid]] = {{ofd_kkt}}.[[store_uid]]'.
            ' AND {{ofd_store}}.[[company_id]] = {{ofd_kkt}}.[[company_id]]'.
            ' AND {{ofd_store}}.[[ofd_id]] = {{ofd_kkt}}.[[ofd_id]]'
        )->andWhere([
            'ofd_kkt.company_id' => $this->company_id,
            'ofd_store.id' => $this->store_id,
        ]);

        return ['' => 'Все'] + $request->orderBy([
            'ofd_kkt.name' => SORT_ASC,
        ])->indexBy('id')->column();
    }

    /**
     * @inheritdoc
     */
    public function returnDropdownList() : array
    {
        return [
            '' => 'Учитывать',
            1 => 'Не учитывать',
        ];
    }

    /**
     * @inheritdoc
     */
    public function paymentTypeDropdownList() : array
    {
        return ['' => 'Все'] + OfdReceipt::$payTypeList;
    }

    /**
     * @inheritdoc
     */
    public function productTypeDropdownList() : array
    {
        return [
            '' => 'Все',
            1 => 'Товар',
            2 => 'Услуга',
        ];
    }
}
