<?php

namespace frontend\modules\retail\models;

use Yii;
use common\models\Company;
use common\models\cash\Cashbox;
use common\models\ofd\OfdImportConfig;
use common\models\ofd\OfdImportGeneralCashbox;
use common\models\ofd\OfdImportKktCashbox;
use common\models\ofd\OfdImportStoreCashbox;

/**
 * This is the model class for table "ofd_import_config".
 *
 * @property int $company_id
 * @property int $no_cashbox
 * @property int $store_cashbox
 * @property int $kkt_cashbox
 * @property int $general_cashbox
 */
class ImportConfigForm extends \yii\base\Model
{
    private Company $company;
    private OfdImportConfig $ofdImportConfig;
    private OfdImportGeneralCashbox $ofdImportGeneralCashbox;
    private Cashbox $cashbox;

    public $import_config_id;
    public $general_cashbox_name;

    public static $items = [
        OfdImportConfig::NO_CASHBOX => [
            'label' => 'Не передавать данные из ОФД в Кассы в КУБ24.',
            'hint' => [
                'В данном варианте, данные по приходу денег не попадут в отчеты по кассе и не отобразятся в отчетах.',
            ],
        ],
        OfdImportConfig::STORE_CASHBOX => [
            'label' => 'Создать под каждую Точку Продаж отдельную Кассу в КУБ24',
            'hint' => [
                'и зачислять приходы туда.',
                'Название Касс в КУБ24 = Название Точки продаж.',
                'После создания кассы в КУБ24 вы сможете переименовать название кассы.',
            ],
        ],
        OfdImportConfig::KKT_CASHBOX => [
            'label' => 'Создать под каждый кассовый аппарат отдельную Кассу в КУБ24',
            'hint' => [
                'и зачислять приходы туда.',
                'Название Касс в КУБ24 = Название Точки продаж + Кассовый аппарат.',
                'После создания кассы в КУБ24 вы сможете переименовать название кассы.',
            ],
        ],
        OfdImportConfig::GENERAL_CASHBOX => [
            'label' => 'Создать 1 общую Кассу в КУБ24 для всех Точек Продаж',
            'hint' => [
                'и зачислять все приходы туда.',
            ],
        ],
    ];

    /**
     * {@inheritdoc}
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
        $this->ofdImportConfig = $company->ofdImportConfig ?? new OfdImportConfig([
            'company_id' => $company->id,
            'import_config_id' => count($company->ofdStores) > 6 ? OfdImportConfig::NO_CASHBOX : OfdImportConfig::STORE_CASHBOX,
        ]);

        $this->ofdImportGeneralCashbox = $company->ofdImportGeneralCashbox ?? new OfdImportGeneralCashbox([
            'company_id' => $company->id,
        ]);

        $this->cashbox = $this->ofdImportGeneralCashbox->cashbox ?? $this->newCashbox();

        $this->import_config_id = $this->ofdImportConfig->import_config_id;
        $this->general_cashbox_name = $this->cashbox->name;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['import_config_id'], 'required'],
            [['import_config_id'], 'in', 'range' => array_keys(self::$items)],
            [['general_cashbox_name'], 'trim'],
            [['general_cashbox_name'], 'required', 'when' => function ($model) {
                return $model->import_config_id == OfdImportConfig::GENERAL_CASHBOX;
            }],
            [['general_cashbox_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'import_config_id' => 'Настройка загрузки данных из ОФД в раздел кассы',
            'general_cashbox_name' => 'Название Кассы в КУБ24',
        ];
    }

    /**
     * @return bool
     */
    public function newCashbox($name = null)
    {
        return new Cashbox([
            'company_id' => $this->company->id,
            'is_accounting' => true,
            'name' => $name,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function _save()
    {
        if ($this->ofdImportConfig->save()) {
            $method = sprintf('_save_%s', $this->import_config_id);

            return $this->{$method}();
        }

        return false;
    }

    /**
     * @return bool
     */
    public function _save_1()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function _save_2()
    {
        foreach ($this->company->ofdStores as $store) {
            $ofdImportStoreCashbox = $store->ofdImportStoreCashbox ?: new OfdImportStoreCashbox([
                'company_id' => $this->company->id,
                'ofd_store_id' => $store->id,
            ]);

            $cashbox = $ofdImportStoreCashbox->cashbox ?: $this->newCashbox($store->name);

            if ($cashbox->getIsNewRecord()) {
                if (!$cashbox->save()) {
                    \common\components\helpers\ModelHelper::logErrors($cashbox, __METHOD__);

                    return false;
                }
                $ofdImportStoreCashbox->cashbox_id = $cashbox->id;
                if (!$ofdImportStoreCashbox->save()) {
                    \common\components\helpers\ModelHelper::logErrors($ofdImportStoreCashbox, __METHOD__);

                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    public function _save_3()
    {
        foreach ($this->company->ofdKkts as $kkt) {
            $ofdImportKktCashbox = $kkt->ofdImportKktCashbox ?: new OfdImportKktCashbox([
                'company_id' => $this->company->id,
                'ofd_kkt_id' => $kkt->id,
            ]);

            $cashbox = $ofdImportKktCashbox->cashbox ?: $this->newCashbox(($kkt->store->name ?? '').'. '. $kkt->name);

            if ($cashbox->getIsNewRecord()) {
                if (!$cashbox->save()) {
                    \common\components\helpers\ModelHelper::logErrors($cashbox, __METHOD__);

                    return false;
                }
                $ofdImportKktCashbox->cashbox_id = $cashbox->id;
                if (!$ofdImportKktCashbox->save()) {
                    \common\components\helpers\ModelHelper::logErrors($ofdImportKktCashbox, __METHOD__);

                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    public function _save_4()
    {
        if ($this->cashbox->save()) {
            if ($this->ofdImportGeneralCashbox->cashbox_id != $this->cashbox->id) {
                $this->ofdImportGeneralCashbox->cashbox_id = $this->cashbox->id;
                if ($this->ofdImportGeneralCashbox->save()) {
                    return true;
                } else {
                    \common\components\helpers\ModelHelper::logErrors($this->ofdImportGeneralCashbox, __METHOD__);

                    return false;
                }
            }

            return true;
        }
        \common\components\helpers\ModelHelper::logErrors($this->cashbox, __METHOD__);

        return false;
    }

    /**
     * @return bool
     */
    public function save()
    {
        if (!$this->validate()) {
            \common\components\helpers\ModelHelper::logErrors($this, __METHOD__);
            return false;
        }

        $this->ofdImportConfig->import_config_id = $this->import_config_id;
        if ($this->import_config_id == OfdImportConfig::GENERAL_CASHBOX) {
            $this->cashbox->name = $this->general_cashbox_name;
        }

        return Yii::$app->db->transaction(function ($db) {
            if ($this->_save()) {
                return true;
            }
            if ($db->transaction) {
                $db->transaction->rollBack();
            }

            return false;
        });
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
