<?php

namespace frontend\modules\retail\models;

use common\components\helpers\ArrayHelper;
use common\components\helpers\Month;
use common\models\ofd\OfdKkt;
use common\models\ofd\OfdOperationType;
use common\models\ofd\OfdReceipt;
use common\models\ofd\OfdReceiptItem;
use common\models\ofd\OfdStore;
use frontend\modules\retail\components\RetailPeriodTrait;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * OfdRetailSearch represents the model behind the search form of `common\models\ofd\OfdReceipt`.
 */
class OfdRetailSearch extends OfdReceipt
{
    use RetailPeriodTrait;

    const JANUARY = '01';
    const FEBRUARY = '02';
    const MARCH = '03';
    const APRIL = '04';
    const MAY = '05';
    const JUNE = '06';
    const JULY = '07';
    const AUGUST = '08';
    const SEPTEMBER = '09';
    const OCTOBER = '10';
    const NOVEMBER = '11';
    const DECEMBER = '12';

    const DAY_1 = '1';
    const DAY_2 = '2';
    const DAY_3 = '3';
    const DAY_4 = '4';
    const DAY_5 = '5';
    const DAY_6 = '6';
    const DAY_7 = '7';

    const SORT_BY_NAME = 'sort_by_name';
    const SORT_BY_PLACE = 'sort_by_place';

    /**
     * @var array
     */
    public static $month = [
        self::JANUARY => 'Январь',
        self::FEBRUARY => 'Февраль',
        self::MARCH => 'Март',
        self::APRIL => 'Апрель',
        self::MAY => 'Май',
        self::JUNE => 'Июнь',
        self::JULY => 'Июль',
        self::AUGUST => 'Август',
        self::SEPTEMBER => 'Сентябрь',
        self::OCTOBER => 'Октябрь',
        self::NOVEMBER => 'Ноябрь',
        self::DECEMBER => 'Декабрь',
    ];

    /**
     * @var array
     */
    public static $day = [
        self::DAY_1 => 'пн',
        self::DAY_2 => 'вт',
        self::DAY_3 => 'ср',
        self::DAY_4 => 'чт',
        self::DAY_5 => 'пт',
        self::DAY_6 => 'сб',
        self::DAY_7 => 'вс',
    ];

    /**
     * @var array
     */
    protected $data = [];

    protected $kktWithSales = [];

    protected $totalByStore = [];
    protected $totalByKkt = [];
    protected $totalByYear = 0;

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @return array
     */
    public function search()
    {
        $year = (int) $this->year;
        $query = OfdReceipt::find();

        $query->rightJoin(
            OfdKkt::tableName(),
            '{{ofd_kkt}}.[[id]] = {{ofd_receipt}}.[[kkt_id]]'
        )->rightJoin(
            OfdStore::tableName(),
            '{{ofd_store}}.[[company_id]]={{ofd_kkt}}.[[company_id]]'.
            ' AND {{ofd_store}}.[[ofd_id]] = {{ofd_kkt}}.[[ofd_id]]'.
            ' AND {{ofd_store}}.[[id]] = {{ofd_kkt}}.[[store_id]]'
        )->andWhere([
            'ofd_receipt.company_id' => $this->company_id,
        ])->andWhere([
            'between', 'date_time', "{$year}-01-01 00:00", "{$year}-12-31 23:59"
        ]);

        $query->select([
            'month' => 'MONTH({{ofd_receipt}}.[[date_time]])',
            'store' => 'ofd_store.id',
            'date' => 'DATE({{ofd_receipt}}.[[date_time]])',
            'kkt' => 'ofd_receipt.kkt_id',
            'amount' => 'SUM([[total_sum]])',
        ])->groupBy(new \yii\db\Expression('[[month]], [[date]], [[store]], [[kkt]] WITH ROLLUP'));

        $result = $query->asArray()->all();
        $data = [];

        foreach ($result as $row) {
            if ($row['month'] === null) {
                $data['total'] = $row['amount'];
            } else {
                if ($row['date'] === null) {
                    $data[$row['month']]['summary']['total'] = $row['amount'];
                } else {
                    if ($row['store'] === null) {
                        $data[$row['month']][$row['date']]['total'] = $row['amount'];
                    } else {
                        if ($row['kkt'] === null) {
                            $data[$row['month']][$row['date']][$row['store']]['total'] = $row['amount'];
                            $data[$row['month']]['summary'][$row['store']]['total'] = (
                                $data[$row['month']]['summary'][$row['store']]['total'] ?? 0
                            ) + $row['amount'];
                        } else {
                            $data[$row['month']][$row['date']][$row['store']][$row['kkt']] = $row['amount'];
                            $data[$row['month']]['summary'][$row['store']][$row['kkt']] = (
                                $data[$row['month']]['summary'][$row['store']][$row['kkt']] ?? 0
                            ) + $row['amount'];

                            // not empty kkts
                            if (!in_array($row['kkt'], $this->kktWithSales)) {
                                $this->kktWithSales[] = $row['kkt'];
                            }

                            // total by store
                            if (!isset($this->totalByStore[$row['store']])) {
                                $this->totalByStore[$row['store']] = 0;
                            }
                            $this->totalByStore[$row['store']] += $row['amount'];

                            // total by kkt
                            if (!isset($this->totalByKkt[$row['kkt']])) {
                                $this->totalByKkt[$row['kkt']] = 0;
                            }
                            $this->totalByKkt[$row['kkt']] += $row['amount'];
                        }
                    }
                }
            }
        }

        return $data;
    }

    /**
     * @return array
     */
    public function excelData()
    {
        $year = (int) $this->year;
        $query = OfdReceipt::find();

        $query->rightJoin(
            OfdKkt::tableName(),
            '{{ofd_kkt}}.[[id]] = {{ofd_receipt}}.[[kkt_id]]'
        )->andWhere([
            'ofd_receipt.company_id' => $this->company_id,
        ])->andWhere([
            'between', 'date_time', "{$year}-01-01 00:00", "{$year}-12-31 23:59"
        ]);

        $query->select([
            'date' => 'DATE({{ofd_receipt}}.[[date_time]])',
            'kkt' => 'ofd_receipt.kkt_id',
            'amount' => 'SUM([[total_sum]])',
        ])->groupBy(new \yii\db\Expression('[[date]], [[kkt]] WITH ROLLUP'));

        $result = $query->asArray()->all();
        $data = [];

        foreach ($result as $row) {
            if ($row['date'] !== null) {
                if ($row['kkt'] === null) {
                    $data[$row['date']]['total'] = $row['amount'];
                } else {
                    $data[$row['date']][$row['kkt']] = $row['amount'];
                }
            }
        }

        $kktArray = OfdKkt::find()->joinWith('store')->where([
            'ofd_kkt.company_id' => $this->company_id,
        ])->orderBy([
            'ofd_store.local_name' => SORT_ASC,
            'ofd_kkt.name' => SORT_ASC,
        ])->all();

        $excelData = [];
        $excelColumns = ['date'];
        $excelHeaders = ['date' => 'Период'];
        foreach ($kktArray as $kkt) {
            $excelColumns[] = "kkt_{$kkt->id}:money";
            $excelHeaders["kkt_{$kkt->id}"] = $kkt->name;
        }
        $excelColumns[] = 'total:money';
        $excelHeaders['total'] = 'Итого';

        foreach ($this->getPeriodDateRange() as $date) {
            $row = ['date' => $date->format('d.m.Y')];
            foreach ($kktArray as $kkt) {
                $row["kkt_{$kkt->id}"] = $data[$date->format('Y-m-d')][$kkt->id] ?? 0;
            }
            $row['total'] = $data[$date->format('Y-m-d')]['total'] ?? 0;
            $excelData[] = $row;
        }

        return [
            'data' => $excelData,
            'columns' => $excelColumns,
            'headers' => $excelHeaders,
        ];
    }

    public function getNotEmptyKkts()
    {
        return $this->kktWithSales;
    }

    public function getTotalByStore($storeId)
    {
        return ArrayHelper::getValue($this->totalByStore, $storeId);
    }

    public function getTotalByKkt($kktId)
    {
        return ArrayHelper::getValue($this->totalByKkt, $kktId);
    }
}
