<?php

namespace frontend\modules\retail\models;

use common\components\helpers\Month;
use common\models\ofd\OfdKkt;
use common\models\ofd\OfdReceipt;
use common\models\ofd\OfdStore;
use frontend\modules\retail\components\RetailPeriodTrait;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ChartReceiptSearch
 */
class ChartReceiptSearch extends Model
{
    use RetailPeriodTrait;

    /**
     * @var integer
     */
    public $company_id;

    /**
     * @param array $params
     *
     * @return array
     */
    public function search($params = [])
    {
        $data = [
            'month' => [],
            'categories' => [],
            'sum' => [],
            'count' => [],
        ];

        $year = (int) $this->year;
        $query = OfdReceipt::find();

        $query->andWhere([
            'company_id' => $this->company_id,
        ])->andWhere([
            'between', 'date_time', "{$year}-01-01 00:00", "{$year}-12-31 23:59"
        ]);

        $query->select([
            'month' => 'MONTH({{ofd_receipt}}.[[date_time]])',
            'sum' => 'SUM([[total_sum]])',
            'count' => 'COUNT(*)',
        ])->groupBy('month');

        $result = [];
        foreach ($query->asArray()->all() as $row) {
            $result[$row['month']] = [
                'sum' => $row['count'] > 0 ? round($row['sum']/$row['count']) : 0,
                'count' => $row['count'],
            ];
        }

        $period = $this->getPeriodDateRange();
        array_reverse($period);

        foreach (range(1, 12) as $month) {
            $data['month'][] = $month;
            $data['categories'][] = mb_strtoupper(mb_substr(Month::$monthFullRU[$month], 0, 3));
            $data['sum'][] = $result[$month]['sum'] ?? 0;
            $data['count'][] = $result[$month]['count'] ?? 0;
        }

        return $data;
    }
}
