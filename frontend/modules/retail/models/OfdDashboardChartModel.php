<?php

namespace frontend\modules\retail\models;

use common\models\ofd\OfdOperationType;
use frontend\components\StatisticPeriod;
use Yii;
use common\models\ofd\OfdKkt;
use common\models\ofd\OfdReceipt;
use common\models\ofd\OfdReceiptItem;
use common\models\ofd\OfdStore;
use common\models\product\Product;
use frontend\modules\retail\components\RetailPeriodTrait;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * OfdDashboardChartModel
 */
class OfdDashboardChartModel extends Model
{
    const TOTAL_SUM = 'sum';
    const TOTAL_QUANTITY = 'quantity';

    /**
     * @var yii\db\Connection
     */
    public $sourceDB;

    /**
     * @var integer
     */
    public $company_id;

    /**
     * OfdStore id
     * @var integer
     */
    public $store;

    /**
     * Custom period
     * @var array
     */
    public $period;

    /**
     * Custom period totals
     * @var array
     */
    public $periodTotals;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [[['store'], 'safe']];
    }

    public function init()
    {
        $this->sourceDB = Yii::$app->db2;
    }

    private function _filterByStore(ActiveQuery $query)
    {
        return $query->rightJoin(
            OfdReceipt::tableName(),
            '{{ofd_receipt}}.[[id]] = {{ofd_receipt_item}}.[[receipt_id]]'
        )->rightJoin(
            OfdKkt::tableName(),
            '{{ofd_kkt}}.[[id]] = {{ofd_receipt}}.[[kkt_id]]'
        )->rightJoin(
            OfdStore::tableName(),
            '{{ofd_store}}.[[company_id]]={{ofd_kkt}}.[[company_id]]'.
            ' AND {{ofd_store}}.[[ofd_id]] = {{ofd_kkt}}.[[ofd_id]]'.
            ' AND {{ofd_store}}.[[uid]] = {{ofd_kkt}}.[[store_uid]]'
        )->andFilterWhere([
            'ofd_store.id' => $this->store
        ]);
    }

    /**
     * @param $periods
     * @return array
     */
    public function getSalesToday($periods)
    {
        $result = array_fill(0, count($periods), 0);

        $timeFrom = date('Y-m-d H:00:00', $periods[0]['from']);
        $timeTill = date('Y-m-d H:59:59', $periods[count($periods)-1]['to']);

        $query = OfdReceiptItem::find()
            ->select(new Expression('
                SUM(`sum`) AS amount, 
                DATE_FORMAT(ofd_receipt_item.date_time, "%Y-%m-%d %H") AS step,
                UNIX_TIMESTAMP(ofd_receipt_item.date_time) AS created_at'))
            ->where(['ofd_receipt_item.company_id' => $this->company_id])
            ->andWhere(['between', 'ofd_receipt_item.date_time', $timeFrom, $timeTill])
            ->groupBy('step');

        if ($this->store) {
            $query = $this->_filterByStore($query);
        }

        foreach ($query->asArray()->all($this->sourceDB) as $sell) {
            foreach ($periods as $i => $p) {
                if ($sell['created_at'] >= $p['from'] && $sell['created_at'] <= $p['to']) {
                    $result[$i] += $sell['amount'] / 100;
                    continue 2;
                }
            }
        }

        return $result;
    }

    /**
     * @param $periods
     * @param $prevPeriods
     * @return array
     */
    public function getSalesByDays($periods, $prevPeriods)
    {
        $result = [
            'curr' => array_fill(0, count($periods), 0),
            'prev' => array_fill(0, count($periods), 0),
        ];

        $dateFrom = $prevPeriods[0]['from'] . ' 00:00:00';
        $dateTill = $periods[count($periods)-1]['to'] . ' 23:59:59';

        $query = OfdReceiptItem::find()
            ->select(new Expression('
                SUM(`sum`) AS amount, 
                DATE_FORMAT(ofd_receipt_item.date_time, "%Y-%m-%d") AS step,
                DATE_FORMAT(ofd_receipt_item.date_time, "%Y-%m-%d") AS document_date'))
            ->where(['ofd_receipt_item.company_id' => $this->company_id])
            ->andWhere(['between', 'ofd_receipt_item.date_time', $dateFrom, $dateTill])
            ->groupBy('step');

        if ($this->store) {
            $query = $this->_filterByStore($query);
        }

        foreach ($query->asArray()->all($this->sourceDB) as $sell)
        {
            if ($sell['document_date'] < $periods[0]['from']) {
                foreach ($prevPeriods as $i => $p) {
                    if ($sell['document_date'] == $p['from']) {
                        $result['prev'][$i] += $sell['amount'] / 100;
                        continue 2;
                    }
                }
            } else {
                foreach ($periods as $i => $p) {
                    if ($sell['document_date'] == $p['from']) {
                        $result['curr'][$i] += $sell['amount'] / 100;
                        continue 2;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param $periods
     * @param $prevPeriods
     * @return array
     */
    public function getAverageCheckByDays($periods, $prevPeriods)
    {
        $result = [
            'curr' => array_fill(0, count($periods), 0),
            'prev' => array_fill(0, count($periods), 0),
        ];

        $dateFrom = $prevPeriods[0]['from'] . ' 00:00:00';
        $dateTill = $periods[count($periods)-1]['to'] . ' 23:59:59';

        $query = OfdReceiptItem::find()
            ->select(new Expression('
                AVG(`sum`) AS amount, 
                DATE_FORMAT(ofd_receipt_item.date_time, "%Y-%m-%d") AS step,
                DATE_FORMAT(ofd_receipt_item.date_time, "%Y-%m-%d") AS document_date'))
            ->where(['ofd_receipt_item.company_id' => $this->company_id])
            ->andWhere(['between', 'ofd_receipt_item.date_time', $dateFrom, $dateTill])
            ->groupBy('step');

        if ($this->store) {
            $query = $this->_filterByStore($query);
        }

        foreach ($query->asArray()->all($this->sourceDB) as $sell)
        {
            if ($sell['document_date'] < $periods[0]['from']) {
                foreach ($prevPeriods as $i => $p) {
                    if ($sell['document_date'] == $p['from']) {
                        $result['prev'][$i] += $sell['amount'] / 100;
                        continue 2;
                    }
                }
            } else {
                foreach ($periods as $i => $p) {
                    if ($sell['document_date'] == $p['from']) {
                        $result['curr'][$i] += $sell['amount'] / 100;
                        continue 2;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param int $limit
     * @param string $orderBy
     * @return array
     */
    public function getTopProductData($limit = 10, $orderBy = 'sum')
    {
        $period = $this->getPeriod();

        $query = OfdReceiptItem::find()
            ->select(['product_id', 'sum' => 'SUM([[sum]])', 'quantity' => 'SUM([[quantity]])'])
            ->where([
                'and',
                ['ofd_receipt_item.company_id' => $this->company_id],
                ['between', 'ofd_receipt_item.date_time', $period['from'], $period['till']],
                //['product_type' => OfdReceiptItem::TYPE_PRODUCT],
                //['operation_type_id' => OfdOperationType::INCOME],
            ])
            ->groupBy('product_id')
            ->orderBy([$orderBy => SORT_DESC])
            ->limit($limit);

        if ($this->store) {
            $query = $this->_filterByStore($query);
        }

        $data = [
            'id' => [],
            'sum' => [],
            'quantity' => [],
            'categories' => [],
        ];

        foreach ($query->asArray()->all($this->sourceDB) as $row) {
            $data['id'][] = $row['product_id'];
            $data['sum'][] = $row['sum'] / 100;
            $data['quantity'][] = $row['quantity'] / 1;
            $data['categories'][] = ($p = Product::findOne($row['product_id'])) ? $p->title : $row['product_id'];
        }

        return $data;
    }

    /**
     * @param int $limit
     * @return array
     */
    public function getTopStoreData($limit = 10)
    {
        $period = $this->getPeriod();

        $query = OfdReceipt::find()
            ->rightJoin(
                OfdKkt::tableName(),
                '{{ofd_kkt}}.[[id]] = {{ofd_receipt}}.[[kkt_id]]'
            )->rightJoin(
                OfdStore::tableName(),
                '{{ofd_store}}.[[company_id]]={{ofd_kkt}}.[[company_id]]'.
                ' AND {{ofd_store}}.[[ofd_id]] = {{ofd_kkt}}.[[ofd_id]]'.
                ' AND {{ofd_store}}.[[uid]] = {{ofd_kkt}}.[[store_uid]]'
            )
            ->where([
                'and',
                ['ofd_receipt.company_id' => $this->company_id],
                ['between', 'ofd_receipt.date_time', $period['from'], $period['till']],
            ]);

        $query->select([
            'ofd_store.id',
            'ofd_store.local_name',
            'sum' => 'SUM({{ofd_receipt}}.[[total_sum]])',
            'sum_cash' => 'SUM({{ofd_receipt}}.[[cash_sum]])',
            'sum_cashless' => 'SUM({{ofd_receipt}}.[[cashless_sum]])',
        ])
            ->groupBy('ofd_store.id')
            ->orderBy(['sum' => SORT_DESC])
            ->limit($limit);

        if ($this->store) {
            $query->andFilterWhere(['ofd_store.id' => $this->store]);
        }

        $data = [
            'id' => [],
            'data' => [],
            'categories' => [],
            'percent_cash' => [],
            'percent_cashless' => []
        ];

        foreach ($query->asArray()->all($this->sourceDB) as $row) {
            $data['id'][] = $row['id'];
            $data['data'][] = $row['sum'] / 100;
            $data['categories'][] = $row['local_name'];
            $data['percent_cash'][] = 100 * $row['sum_cash'] / ($row['sum'] ?: 9E9);
            $data['percent_cashless'][] = 100 * $row['sum_cashless'] / ($row['sum'] ?: 9E9);
        }

        return $data;
    }

    public function getPeriod()
    {
        $curFrom = StatisticPeriod::dateRange('from');
        $curTill = StatisticPeriod::dateRange('to');

        return [
            'from' => $curFrom->format('Y-m-d 00:00:00'),
            'till' => $curTill->format('Y-m-d 23:59:59')
        ];
    }

    /**
     * @param string $key
     * @return array|float
     */
    public function getTotalByPeriod($key = null)
    {
        $period = $this->getPeriod();

        if ($this->periodTotals === null) {
            $this->periodTotals = OfdReceiptItem::find()->where([
                    'and',
                    ['company_id' => $this->company_id],
                    ['between', 'date_time', $period['from'], $period['till']],
                ])->select([
                    'SUM([[ofd_receipt_item.sum]] / 100) AS ' . self::TOTAL_SUM,
                    'SUM([[ofd_receipt_item.quantity]]) AS ' . self::TOTAL_QUANTITY,
                ])
                ->asArray()
                ->one();
        }

        return ($key) ? ArrayHelper::getValue($this->periodTotals, $key, 0) : $this->periodTotals;
    }
}
