<?php

namespace frontend\modules\retail\models;

use frontend\components\PageSize;
use Yii;
use common\models\ofd\OfdKkt;
use common\models\ofd\OfdReceipt;
use common\models\ofd\OfdStore;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * OfdReceiptSearch represents the model behind the search form of `common\models\ofd\OfdReceipt`.
 */
class OfdReceiptSearch extends OfdReceipt
{
    protected $_period;

    protected $_query;

    public $income_sum;
    public $return_income_sum;
    public $expense_sum;
    public $return_expense_sum;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['payment_type_id', 'ofd_id', 'store_id', 'kkt_id', 'shift_number'], 'safe'],
            [['document_number'], 'integer']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function setPeriod($period)
    {
        $this->_period['from'] = ($f = date_create($period['from'] ?? null)) ? $f->format('Y-m-d 00:00:00') : date('Y-m-d 00:00:00');
        $this->_period['to'] = ($f = date_create($period['to'] ?? null)) ? $f->format('Y-m-d 23:59:59') : date('Y-m-d 23:59:59');
    }

    /**
     * {@inheritdoc}
     */
    public function getPeriod()
    {
        if (!isset($this->_period)) {
            $this->_period = [
                'from' => date('Y-01-01 00:00:00'),
                'to' => date('Y-12-31 23:59:59'),
            ];
        }

        return $this->_period;
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()->alias('ofd_receipt')->addSelect([
            '*',
            'income_sum' => 'IF({{ofd_receipt}}.[[operation_type_id]] = 1, {{ofd_receipt}}.[[total_sum]], 0)',
            'return_income_sum' => 'IF({{ofd_receipt}}.[[operation_type_id]] = 2, {{ofd_receipt}}.[[total_sum]], 0)',
            'expense_sum' => 'IF({{ofd_receipt}}.[[operation_type_id]] = 3, {{ofd_receipt}}.[[total_sum]], 0)',
            'return_expense_sum' => 'IF({{ofd_receipt}}.[[operation_type_id]] = 4, {{ofd_receipt}}.[[total_sum]], 0)',
        ]);

        // add conditions that should always apply here
        $query->andWhere([
            'and',
            ['ofd_receipt.company_id' => $this->company_id],
            ['between', 'ofd_receipt.date_time', $this->period['from'], $this->period['to']],
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'date_time',
                    'document_number',
                    'number',
                    'income_sum' => [
                        'asc' => [
                            'IF({{ofd_receipt}}.[[operation_type_id]] = 1, {{ofd_receipt}}.[[total_sum]], 0)' => SORT_ASC,
                        ],
                        'desc' => [
                            'IF({{ofd_receipt}}.[[operation_type_id]] = 1, {{ofd_receipt}}.[[total_sum]], 0)' => SORT_DESC,
                        ],
                    ],
                    'return_income_sum' => [
                        'asc' => [
                            'IF({{ofd_receipt}}.[[operation_type_id]] = 2, {{ofd_receipt}}.[[total_sum]], 0)' => SORT_ASC,
                        ],
                        'desc' => [
                            'IF({{ofd_receipt}}.[[operation_type_id]] = 2, {{ofd_receipt}}.[[total_sum]], 0)' => SORT_DESC,
                        ],
                    ],
                    'expense_sum' => [
                        'asc' => [
                            'IF({{ofd_receipt}}.[[operation_type_id]] = 3, {{ofd_receipt}}.[[total_sum]], 0)' => SORT_ASC,
                        ],
                        'desc' => [
                            'IF({{ofd_receipt}}.[[operation_type_id]] = 3, {{ofd_receipt}}.[[total_sum]], 0)' => SORT_DESC,
                        ],
                    ],
                    'return_expense_sum' => [
                        'asc' => [
                            'IF({{ofd_receipt}}.[[operation_type_id]] = 4, {{ofd_receipt}}.[[total_sum]], 0)' => SORT_ASC,
                        ],
                        'desc' => [
                            'IF({{ofd_receipt}}.[[operation_type_id]] = 4, {{ofd_receipt}}.[[total_sum]], 0)' => SORT_DESC,
                        ],
                    ],
                ],
                'defaultOrder' => [
                    'date_time' => SORT_DESC,
                ],
            ],
            'pagination' => ['pageSize' => PageSize::get()],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ofd_receipt.store_id' => $this->store_id,
            'ofd_receipt.kkt_id' => $this->kkt_id,
            'ofd_receipt.ofd_id' => $this->ofd_id,
            'ofd_receipt.payment_type_id' => $this->payment_type_id,
            'ofd_receipt.shift_number' => $this->shift_number,
        ]);

        if ($this->document_number) {
            $query->andWhere(new Expression('ofd_receipt.document_number LIKE "'.str_replace('"', '', $this->document_number).'%"'));
        }

        $this->_query = clone $query;

        return $dataProvider;
    }

    /**
     * {@inheritdoc}
     */
    public function getPayTypeFilter() : array
    {
        return ['' => 'Все'] + OfdReceipt::$payTypeList;
    }

    /**
     * {@inheritdoc}
     */
    public function getStoreFilter() : array
    {
        return ['' => 'Все'] + OfdStore::find()->select([
            'local_name',
            'id',
        ])->andWhere([
            'company_id' => $this->company_id,
        ])->orderBy([
            'local_name' => SORT_ASC,
        ])->indexBy('id')->column();
    }

    /**
     * {@inheritdoc}
     */
    public function getKktFilter() : array
    {
        return ['' => 'Все'] + OfdKkt::find()->select([
            'name',
            'id',
        ])->andWhere([
            'company_id' => $this->company_id,
        ])->andFilterWhere([
            'store_id' => $this->store_id,
        ])->orderBy([
            'name' => SORT_ASC,
        ])->indexBy('id')->column();
    }

    public function getShiftNumberFilter()
    {
        $query = clone $this->_query;
        return ['' => 'Все'] + $query
            ->select('shift_number')
            ->orderBy('shift_number')
            ->groupBy('shift_number')
            ->indexBy('shift_number')
            ->column();
    }
}
