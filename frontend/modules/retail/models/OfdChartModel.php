<?php

namespace frontend\modules\retail\models;

use common\components\helpers\Month;
use common\models\ofd\OfdKkt;
use common\models\ofd\OfdOperationType;
use common\models\ofd\OfdReceipt;
use common\models\ofd\OfdReceiptItem;
use common\models\ofd\OfdStore;
use common\models\product\Product;
use frontend\modules\retail\components\RetailPeriodTrait;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * OfdChartModel
 */
class OfdChartModel extends Model
{
    use RetailPeriodTrait;

    const STEP_MONTH = 'month';
    const STEP_DAY = 'day';

    public static $steps = [
        self::STEP_MONTH => 'months',
        self::STEP_DAY => 'days',
    ];

    public static $stepLabels = [
        self::STEP_MONTH => 'Месяц',
        self::STEP_DAY => 'День',
    ];

    const JANUARY = 1;
    const FEBRUARY = 2;
    const MARCH = 3;
    const APRIL = 4;
    const MAY = 5;
    const JUNE = 6;
    const JULY = 7;
    const AUGUST = 8;
    const SEPTEMBER = 9;
    const OCTOBER = 10;
    const NOVEMBER = 11;
    const DECEMBER = 12;

    /**
     * @var array
     */
    public static $month = [
        self::JANUARY => 'Январь',
        self::FEBRUARY => 'Февраль',
        self::MARCH => 'Март',
        self::APRIL => 'Апрель',
        self::MAY => 'Май',
        self::JUNE => 'Июнь',
        self::JULY => 'Июль',
        self::AUGUST => 'Август',
        self::SEPTEMBER => 'Сентябрь',
        self::OCTOBER => 'Октябрь',
        self::NOVEMBER => 'Ноябрь',
        self::DECEMBER => 'Декабрь',
    ];

    /**
     * @var array
     */
    public static $monthShort = [
        self::JANUARY => 'Янв',
        self::FEBRUARY => 'Фев',
        self::MARCH => 'Мар',
        self::APRIL => 'Апр',
        self::MAY => 'Май',
        self::JUNE => 'Июн',
        self::JULY => 'Июл',
        self::AUGUST => 'Авг',
        self::SEPTEMBER => 'Сен',
        self::OCTOBER => 'Окт',
        self::NOVEMBER => 'Ноя',
        self::DECEMBER => 'Дек',
    ];

    /**
     * @var array
     */
    public static $monthInflected = [
        self::JANUARY => 'января',
        self::FEBRUARY => 'февраля',
        self::MARCH => 'марта',
        self::APRIL => 'апреля',
        self::MAY => 'мая',
        self::JUNE => 'июня',
        self::JULY => 'июля',
        self::AUGUST => 'августа',
        self::SEPTEMBER => 'сентября',
        self::OCTOBER => 'октября',
        self::NOVEMBER => 'ноября',
        self::DECEMBER => 'декабря',
    ];

    /**
     * @var integer
     */
    public $company_id;

    /**
     * OfdStore id
     * @var integer
     */
    public $store;

    /**
     * OfdReceiptItem name
     * @var string
     */
    public $name;

    /**
     * Product id
     * @var integer
     */
    public $product;

    /**
     * @var
     */
    private $_totalByYear;
    private $_totalByMonth;

    /**
     * Period offset from default
     * @var [type]
     */
    protected $_offset = 0;

    /**
     * Period step month or day
     * @var integer
     */
    protected $_step = self::STEP_MONTH;

    protected $_from;
    protected $_till;
    protected $_range;
    protected $_rangeInfo;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [[['step', 'offset', 'store', 'product', 'name', 'customMonth'], 'safe']];
    }

    public function init()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function setOffset($value)
    {
        $this->_offset = intval($value);
    }

    /**
     * {@inheritdoc}
     */
    public function getOffset()
    {
        return $this->_offset;
    }

    /**
     * {@inheritdoc}
     */
    public function setStep($value)
    {
        $this->_step = (in_array($value, [self::STEP_MONTH, self::STEP_DAY])) ? $value : self::STEP_MONTH;
    }

    /**
     * {@inheritdoc}
     */
    public function getStep()
    {
        return $this->_step;
    }

    /**
     * {@inheritdoc}
     */
    public function getFrom()
    {
        if ($this->_from === null) {
            $date = $this->getPeriodDateFrom()->modify('today');
            if ($this->step == SELF::STEP_DAY && ($m = date('n')) > 1) {
                $m--;
                $date->modify("+{$m} month");
            }

            if ($this->offset != 0) {
                $offset = sprintf("%+d", $this->offset);
                $date->modify("{$offset} {$this->step}");
            }

            $this->_from = $date;
        }

        return clone $this->_from;
    }

    /**
     * {@inheritdoc}
     */
    public function getTill()
    {
        if ($this->_till === null) {
            $date = $this->getFrom();
            if ($this->step == SELF::STEP_DAY) {
                $date->modify("+12 day -1 second");
            } else {
                $date->modify("+1 year -1 second");
            }

            $this->_till = $date;
        }

        return clone $this->_till;
    }

    /**
     * {@inheritdoc}
     */
    public function getRange()
    {
        if ($this->_range === null) {
            $date = $this->from;
            $till = $this->till;
            $step = $this->step;
            $range = [];
            $rangeInfo = [
                'labelsX' => [],
                'currDayPos' => [],
                'wrapPointPos' => [],
                'freeDayPos' => [],
            ];
            $datePrev = $date;
            while ($date < $till) {
                $range[] = (int) $date->format($step == self::STEP_DAY ? 'd' : 'm');

                $rangeInfo['labelsX'][] = $this->getDateInfo('labelsX', $date, $datePrev);
                $rangeInfo['currDayPos'][] = $this->getDateInfo('currDayPos', $date, $datePrev);
                $rangeInfo['freeDayPos'][] = $this->getDateInfo('freeDayPos', $date, $datePrev);
                $rangeInfo['wrapPointPos'][] = $this->getDateInfo('wrapPointPos', $date, $datePrev);

                $datePrev = clone $date;
                $date->modify("+1 {$step}");
            }
            $this->_range = $range;
            $this->_rangeInfo = $rangeInfo;
        }

        return $this->_range;
    }

    public function getDateInfo($param, $date, $datePrev)
    {
        $byDays = ($this->step == self::STEP_DAY);

        if ($param == 'labelsX') {
            return $byDays
                ? ($date->format('d') .' '. self::$monthInflected[$date->format('n')] .' '. $date->format('Y'))
                : (self::$month[$date->format('n')] .' '. $date->format('Y'));
        }

        if ($param == 'currDayPos') {
            $stepPrecision = $byDays ? 'Ymd' : 'Ym';
                return ($date->format($stepPrecision) == date($stepPrecision)) ? 1 : null;
        }

        if ($param == 'freeDayPos' && $byDays) {
            return (in_array($date->format('w'), [0, 6]));
        }

        if ($param == 'wrapPointPos') {
            $stepWrap = $byDays ? 'm' : 'Y';

            if ($date->format($stepWrap) != $datePrev->format($stepWrap)) {

                // emulate align right
                if ($byDays) {
                switch ($date->format('m')) {
                    case 1: $leftMargin = '-350%'; break;
                    case 2: $leftMargin = '-300%'; break;
                    case 3: $leftMargin = '-375%'; break;
                    case 4: $leftMargin = '-180%'; break; // march
                    case 5: $leftMargin = '-285%'; break;
                    case 6: $leftMargin = '-125%'; break;
                    case 7: $leftMargin = '-190%'; break;
                    case 8: $leftMargin = '-190%'; break;
                    case 9: $leftMargin = '-250%'; break;
                    case 10: $leftMargin = '-420%'; break;
                    case 11: $leftMargin = '-350%'; break;
                    case 12: $leftMargin = '-290%'; break;
                }
                } else {
                    switch ($date->format('Y') - 1) {
                        case 2015: case 2016: case 2017: case 2018: case 2019: case 2021: case 2031: case 2041:
                        $leftMargin = '-30%'; break;
                        default:
                            $leftMargin = '-28%'; break;
                    }
                }

                return [
                    'prev' => $byDays ? (self::$month[$datePrev->format('n')]) : $datePrev->format($stepWrap),
                    'next' => $byDays ? (self::$month[$date->format('n')]) : $date->format($stepWrap),
                    'leftMargin' => $leftMargin
                ];
            }
        }

        return null;
    }

    /**
     * @return array
     */
    public function receiptDataSlow()
    {
        $step = $this->step;
        $query = OfdReceiptItem::find();

        $query->rightJoin(
            OfdReceipt::tableName(),
        '{{ofd_receipt}}.[[id]] = {{ofd_receipt_item}}.[[receipt_id]]'
            )->rightJoin(
            OfdKkt::tableName(),
            '{{ofd_kkt}}.[[id]] = {{ofd_receipt}}.[[kkt_id]]'
        )->rightJoin(
            OfdStore::tableName(),
            '{{ofd_store}}.[[company_id]]={{ofd_kkt}}.[[company_id]]'.
            ' AND {{ofd_store}}.[[ofd_id]] = {{ofd_kkt}}.[[ofd_id]]'.
            ' AND {{ofd_store}}.[[uid]] = {{ofd_kkt}}.[[store_uid]]'
        )->andWhere([
            'ofd_receipt_item.company_id' => $this->company_id,
        ])->andWhere([
            'between', 'ofd_receipt_item.date_time', $this->from->format("Y-m-d H:i:s"), $this->till->format("Y-m-d H:i:s")
        ])->andFilterWhere([
            'ofd_store.id' => $this->store,
        ])->andFilterWhere([
            'ofd_receipt_item.name' => $this->name,
        ]);

        $sqlStep = $step == self::STEP_DAY ? 'DAY({{ofd_receipt}}.[[date_time]])' : 'MONTH({{ofd_receipt}}.[[date_time]])';
        $query->select([
            'step' => $sqlStep,
            'sum' => 'SUM({{ofd_receipt_item}}.[[sum]])',
            'count' => 'COUNT(DISTINCT {{ofd_receipt}}.[[id]])',
            'sum_cash' => 'SUM(IF({{ofd_receipt}}.{{cash_sum}} > 0 AND {{ofd_receipt}}.{{cashless_sum}} = 0, {{ofd_receipt_item}}.[[sum]], 0))',
            'sum_cashless' => 'SUM(IF({{ofd_receipt}}.{{cashless_sum}} > 0 AND {{ofd_receipt}}.{{cash_sum}} = 0, {{ofd_receipt_item}}.[[sum]], 0))',
        ])->groupBy('step');

        $result = [];
        foreach ($query->asArray()->all() as $row) {
            $result[$row['step']] = [
                'sum' => $row['sum'] / 100,
                'count' => $row['count'],
                'avg' => ($row['count'] > 0) ? round($row['sum'] / 100 / $row['count']) : 0,
                'sum_cash' => $row['sum_cash'] / 100,
                'sum_cashless' => $row['sum_cashless'] / 100,
            ];
        }

        $data = [
            'categories' => [],
            'sum' => [],
            'count' => [],
            'avg' => [],
            'sum_cash' => [],
            'sum_cashless' => [],
        ];

        foreach ($this->getRange() as $i) {
            $data['categories'][] = $step == self::STEP_DAY ? $i : mb_strtoupper(self::$monthShort[$i]);
            $data['sum'][] = $result[$i]['sum'] ?? 0;
            $data['count'][] = $result[$i]['count'] ?? 0;
            $data['avg'][] = $result[$i]['avg'] ?? 0;
            $data['sum_cash'][] = $result[$i]['sum_cash'] ?? 0;
            $data['sum_cashless'][] = $result[$i]['sum_cashless'] ?? 0;
        }

        return $data + ['rangeInfo' => $this->_rangeInfo];
    }

    public function receiptData()
    {
        $step = $this->step;
        $sqlStep = $step == self::STEP_DAY ? 'DAY({{ofd_receipt}}.[[date_time]])' : 'MONTH({{ofd_receipt}}.[[date_time]])';

        $query = OfdReceipt::find()
        ->rightJoin(
            OfdKkt::tableName(),
            '{{ofd_kkt}}.[[id]] = {{ofd_receipt}}.[[kkt_id]]'
        )->rightJoin(
            OfdStore::tableName(),
            '{{ofd_store}}.[[company_id]]={{ofd_kkt}}.[[company_id]]'.
            ' AND {{ofd_store}}.[[ofd_id]] = {{ofd_kkt}}.[[ofd_id]]'.
            ' AND {{ofd_store}}.[[uid]] = {{ofd_kkt}}.[[store_uid]]'
        )->andWhere([
            'ofd_receipt.company_id' => $this->company_id,
        ])->andWhere([
            'between', 'ofd_receipt.date_time', $this->from->format("Y-m-d H:i:s"), $this->till->format("Y-m-d H:i:s")
        ])->andFilterWhere([
            'ofd_store.id' => $this->store,
        ]);

        if ($this->product) {
            // slow calc by ofd_receipt_item (single product)
            $query->leftJoin(
                OfdReceiptItem::tableName(),
                '{{ofd_receipt_item}}.[[receipt_id]] = {{ofd_receipt}}.[[id]]'
            )->andFilterWhere(['ofd_receipt_item.product_id' => $this->product])
            ->select([
                'step' => $sqlStep,
                'sum' => 'SUM({{ofd_receipt_item}}.[[sum]])',
                'count' => 'COUNT({{ofd_receipt}}.[[id]])',
                'sum_cash' => 'SUM(IF({{ofd_receipt}}.[[cash_sum]] > 0, {{ofd_receipt_item}}.[[sum]], 0))',
                'sum_cashless' => 'SUM(IF({{ofd_receipt}}.[[cashless_sum]] > 0, {{ofd_receipt_item}}.[[sum]], 0))',
            ]);
        } else {
            // fast calc by ofd_receipt
            $query->select([
                'step' => $sqlStep,
                'sum' => 'SUM({{ofd_receipt}}.[[total_sum]])',
                'count' => 'COUNT({{ofd_receipt}}.[[id]])',
                'sum_cash' => 'SUM({{ofd_receipt}}.[[cash_sum]])',
                'sum_cashless' => 'SUM({{ofd_receipt}}.[[cashless_sum]])',
            ]);
        }

        $query->groupBy('step');

        $result = [];
        foreach ($query->asArray()->all() as $row) {
            $result[$row['step']] = [
                'sum' => $row['sum'] / 100,
                'count' => (int)$row['count'],
                'avg' => ($row['count'] > 0) ? round($row['sum'] / 100 / $row['count']) : 0,
                'sum_cash' => $row['sum_cash'] / 100,
                'sum_cashless' => $row['sum_cashless'] / 100,
            ];
        }

        $data = [
            'categories' => [],
            'sum' => [],
            'count' => [],
            'avg' => [],
            'sum_cash' => [],
            'sum_cashless' => [],
        ];

        foreach ($this->getRange() as $i) {
            $data['categories'][] = $step == self::STEP_DAY ? $i : mb_strtoupper(self::$monthShort[$i]);
            $data['sum'][] = $result[$i]['sum'] ?? 0;
            $data['count'][] = $result[$i]['count'] ?? 0;
            $data['avg'][] = $result[$i]['avg'] ?? 0;
            $data['sum_cash'][] = $result[$i]['sum_cash'] ?? 0;
            $data['sum_cashless'][] = $result[$i]['sum_cashless'] ?? 0;
        }

        return $data + ['rangeInfo' => $this->_rangeInfo];
    }

/*
    public function receiptData()
    {
        $step = $this->step;
        $query = OfdReceipt::find();

        $query->rightJoin(
            OfdKkt::tableName(),
            '{{ofd_kkt}}.[[id]] = {{ofd_receipt}}.[[kkt_id]]'
        )->rightJoin(
            OfdStore::tableName(),
            '{{ofd_store}}.[[company_id]]={{ofd_kkt}}.[[company_id]]'.
            ' AND {{ofd_store}}.[[ofd_id]] = {{ofd_kkt}}.[[ofd_id]]'.
            ' AND {{ofd_store}}.[[uid]] = {{ofd_kkt}}.[[store_uid]]'
        )->andWhere([
            'ofd_receipt.company_id' => $this->company_id,
        ])->andWhere([
            'between', 'ofd_receipt.date_time', $this->from->format("Y-m-d H:i:s"), $this->till->format("Y-m-d H:i:s")
        ])->andFilterWhere([
            'ofd_store.id' => $this->store,
        ]);

        $sqlStep = $step == self::STEP_DAY ? 'DAY' : 'MONTH';
        $query->select([
            'month' => $sqlStep.'({{ofd_receipt}}.[[date_time]])',
            'sum' => 'SUM([[total_sum]])',
            'count' => 'COUNT(*)',
        ])->groupBy('month');

        $result = [];
        foreach ($query->asArray()->all() as $row) {
            $result[$row['month']] = [
                'sum' => $row['count'] > 0 ? round($row['sum']/$row['count']) : 0,
                'count' => $row['count'],
            ];
        }

        $data = [
            'categories' => [],
            'sum' => [],
            'count' => [],
        ];

        foreach ($this->getRange() as $i) {
            $data['categories'][] = $step == self::STEP_DAY ? $i : mb_strtoupper(self::$monthShort[$i]);
            $data['sum'][] = ($result[$i]['sum'] ?? 0)/100;
            $data['count'][] = $result[$i]['count'] ?? 0;
        }

        return $data + ['rangeInfo' => $this->_rangeInfo];
    }
*/
/*
    public function cashTypeData()
    {
        $step = $this->step;
        $query = OfdReceipt::find();

        $query->rightJoin(
            OfdKkt::tableName(),
            '{{ofd_kkt}}.[[id]] = {{ofd_receipt}}.[[kkt_id]]'
        )->rightJoin(
            OfdStore::tableName(),
            '{{ofd_store}}.[[company_id]]={{ofd_kkt}}.[[company_id]]'.
            ' AND {{ofd_store}}.[[ofd_id]] = {{ofd_kkt}}.[[ofd_id]]'.
            ' AND {{ofd_store}}.[[uid]] = {{ofd_kkt}}.[[store_uid]]'
        )->andWhere([
            'ofd_receipt.company_id' => $this->company_id,
        ])->andWhere([
            'between', 'ofd_receipt.date_time', $this->from->format("Y-m-d H:i:s"), $this->till->format("Y-m-d H:i:s")
        ])->andFilterWhere([
            'ofd_store.id' => $this->store,
        ]);

        $sqlStep = $step == self::STEP_DAY ? 'DAY({{ofd_receipt}}.[[date_time]])' : 'MONTH({{ofd_receipt}}.[[date_time]])';
        $query->select([
            'step' => $sqlStep,
            'cash' => 'SUM([[cash_sum]])',
            'cashless' => 'SUM([[cashless_sum]])',
        ])->groupBy('step');

        $result = [];
        foreach ($query->asArray()->all() as $row) {
            $result[$row['step']] = [
                'cash' => $row['cash'],
                'cashless' => $row['cashless'],
            ];
        }

        $data = [
            'categories' => [],
            'cash' => [],
            'cashless' => [],
        ];

        foreach ($this->getRange() as $i) {
            $data['categories'][] = $step == self::STEP_DAY ? $i : mb_strtoupper(self::$monthShort[$i]);
            $data['cash'][] = ($result[$i]['cash'] ?? 0)/100;
            $data['cashless'][] = ($result[$i]['cashless'] ?? 0)/100;
        }

        return $data + ['rangeInfo' => $this->_rangeInfo];
    }
*/
/*
    public function compareData()
    {
        $data = [
            'day' => [
                'this' => 0,
                'last' => 0,
            ],
            'week' => [
                'this' => 0,
                'last' => 0,
            ],
            'month' => [
                'this' => 0,
                'last' => 0,
            ],
        ];

        $dateFrom = date_create('today first day of previous month');
        $today = date_create('today');
        $yesterday = date_create('yesterday');

        $lastDay = date_create('today -1 days');
        $thisWeek = date_create('today monday this week');
        $lastWeek = date_create('today monday previous week');
        $thisMonth = date_create('today first day of this month');

        $query = OfdReceipt::find()->joinWith('items')->select([
            'sum' => 'SUM({{ofd_receipt_item}}.[[sum]])',
            'date' => 'DATE({{ofd_receipt}}.[[date_time]])',
        ])->andWhere([
            'ofd_receipt.company_id' => $this->company_id,
        ])->andWhere([
            'between', 'ofd_receipt.date_time', $dateFrom->format('Y-m-d 00:00:00'), $today->format('Y-m-d 23:59:59')
        ])->groupBy('date');

        $result = $query->asArray()->indexBy('date')->column();

        foreach ($result as $key => $value) {
            $value = $value / 100;
            $date = date_create_from_format('Y-m-d|', $key);
            if ($date > $thisMonth) {
                $data['month']['this'] += $value;
                if ($date > $lastWeek) {
                    if ($date > $thisWeek) {
                        $data['week']['this'] += $value;
                        if ($date = $lastDay) {
                            $data['day']['last'] += $value;
                        }
                        if ($date > $lastDay) {
                            $data['day']['this'] += $value;
                        }
                    } else {
                        $data['week']['last'] += $value;
                    }
                }
            } else {
                $data['month']['last'] += $value;
            }
        }

        return [
            'data' => $data,
            'max' => ($result ? max($result) : 0) / 100,
        ];
    }
*/

    /**
     * @return array
     */
    public function topStoreData()
    {
        $query = OfdReceipt::find()
            ->rightJoin(
                OfdKkt::tableName(),
                '{{ofd_kkt}}.[[id]] = {{ofd_receipt}}.[[kkt_id]]'
            )->rightJoin(
                OfdStore::tableName(),
                '{{ofd_store}}.[[company_id]]={{ofd_kkt}}.[[company_id]]'.
                ' AND {{ofd_store}}.[[ofd_id]] = {{ofd_kkt}}.[[ofd_id]]'.
                ' AND {{ofd_store}}.[[uid]] = {{ofd_kkt}}.[[store_uid]]'
            )
            ->where([
            'and',
            ['ofd_receipt.company_id' => $this->company_id],
            ['YEAR(ofd_receipt.date_time)' => $this->_year],
            ['MONTH(ofd_receipt.date_time)' => $this->_month]
            //['between', 'ofd_receipt.date_time', date("Y-m-d 00:00:00"), date("Y-m-d 23:59:59")],
        ]);

        $query->select([
            'ofd_store.id',
            'ofd_store.local_name',
            'sum' => 'SUM({{ofd_receipt}}.[[total_sum]])',
            'sum_cash' => 'SUM({{ofd_receipt}}.[[cash_sum]])',
            'sum_cashless' => 'SUM({{ofd_receipt}}.[[cashless_sum]])',
        ])
        ->groupBy('ofd_store.id')
        ->orderBy(['sum' => SORT_DESC])
        ->limit(5);

        $data = [
            'id' => [],
            'data' => [],
            'categories' => [],
            'percent_cash' => [],
            'percent_cashless' => []
        ];

        foreach ($query->asArray()->all() as $row) {
            $data['id'][] = $row['id'];
            $data['data'][] = $row['sum'] / 100;
            $data['categories'][] = $row['local_name'];
            $data['percent_cash'][] = 100 * $row['sum_cash'] / ($row['sum'] ?: 9E9);
            $data['percent_cashless'][] = 100 * $row['sum_cashless'] / ($row['sum'] ?: 9E9);
        }

        return $data;
    }

    /**
     * @return float
     */
    public function getTotalByYear()
    {
        if ($this->_totalByYear === null) {
            $this->_totalByYear = OfdReceipt::find()->where([
                    'and',
                    ['ofd_receipt.company_id' => $this->company_id],
                    ['YEAR(ofd_receipt.date_time)' => $this->_year],
                ])->sum('ofd_receipt.total_sum') / 100;
        }

        return $this->_totalByYear;
    }

    /**
     * @return float
     */
    public function getTotalByMonth()
    {
        if ($this->_totalByMonth === null) {
            $this->_totalByMonth = OfdReceipt::find()->where([
                    'and',
                    ['ofd_receipt.company_id' => $this->company_id],
                    ['YEAR(ofd_receipt.date_time)' => $this->_year],
                    ['MONTH(ofd_receipt.date_time)' => $this->_month],
                ])->sum('ofd_receipt.total_sum') / 100;
        }

        return $this->_totalByMonth;
    }

    /**
     * @return array
     */
    public function topProductData()
    {
        $query = OfdReceiptItem::find()
            ->select(['product_id', 'sum' => 'SUM([[sum]])'])
            ->where([
                'and',
                ['ofd_receipt_item.company_id' => $this->company_id],
                ['YEAR(ofd_receipt_item.date_time)' => $this->_year],
                ['MONTH(ofd_receipt_item.date_time)' => $this->_month]
                //['product_type' => OfdReceiptItem::TYPE_PRODUCT],
                //['operation_type_id' => OfdOperationType::INCOME],
                //['between', 'ofd_receipt_item.date_time', date("Y-m-d 00:00:00"), date("Y-m-d 23:59:59")],
            ])
            ->groupBy('product_id')
            ->orderBy(['sum' => SORT_DESC])
            ->limit(16);

        $data = [
            'id' => [],
            'data' => [],
            'categories' => [],
        ];

        foreach ($query->all() as $row) {
            $data['id'][] = $row['product_id'];
            $data['data'][] = $row['sum'] / 100;
            $data['categories'][] = ($p = Product::findOne($row['product_id'])) ? $p->title : $row['product_id'];
        }

        return $data;
    }

    public function getByDays()
    {
        return $this->step == self::STEP_DAY;
    }
}
