<?php

namespace frontend\modules\retail\models;

use common\models\ofd\OfdKkt;
use common\models\ofd\OfdReceipt;
use common\models\ofd\OfdStore;
use frontend\modules\retail\components\RetailPeriodTrait;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ChartCompareSearch
 */
class ChartCompareSearch extends Model
{
    static $labels =[
        'day' => [
            'this' => 'Сегодня',
            'last' => 'Вчера',
        ],
        'week' => [
            'this' => 'Неделя',
            'last' => 'Пред. неделя',
        ],
        'month' => [
            'this' => 'Месяц',
            'last' => 'Пред. месяц',
        ],
    ];

    /**
     * @var integer
     */
    public $company_id;

    /**
     * @param array $params
     *
     * @return array
     */
    public function search($params = [])
    {
        $data = [
            'day' => [
                'this' => 0,
                'last' => 0,
            ],
            'week' => [
                'this' => 0,
                'last' => 0,
            ],
            'month' => [
                'this' => 0,
                'last' => 0,
            ],
        ];

        $dateFrom = date_create('today -2 months +1 days');
        $dateTill = date_create('today');

        $lastDay = date_create('today -1 days');
        $thisWeek = date_create('today -7 days');
        $lastWeek = date_create('today -14 days');
        $thisMonth = date_create('today -1 months');

        $this->load($params);

        $query = OfdReceipt::find()->select([
            'sum' => 'SUM([[total_sum]])',
            'date' => 'DATE({{ofd_receipt}}.[[date_time]])',
        ])->andWhere([
            'company_id' => $this->company_id,
        ])->andWhere([
            'between', 'date_time', $dateFrom->format('Y-m-d 00:00'), $dateTill->format('Y-m-d 23:59')
        ])->groupBy('date');

        $result = $query->asArray()->indexBy('date')->column();

        foreach ($result as $key => $value) {
            $date = date_create_from_format('Y-m-d|', $key);
            if ($date > $thisMonth) {
                $data['month']['this'] += $value;
                if ($date > $lastWeek) {
                    if ($date > $thisWeek) {
                        $data['week']['this'] += $value;
                        if ($date = $lastDay) {
                            $data['day']['last'] += $value;
                        }
                        if ($date > $lastDay) {
                            $data['day']['this'] += $value;
                        }
                    } else {
                        $data['week']['last'] += $value;
                    }
                }
            } else {
                $data['month']['last'] += $value;
            }
        }

        return [
            'data' => $data,
            'max' => max($result) ?? 0,
        ];
    }
}
