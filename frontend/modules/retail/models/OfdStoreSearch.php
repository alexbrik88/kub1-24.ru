<?php

namespace frontend\modules\retail\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ofd\OfdStore;

/**
 * OfdStoreSearch represents the model behind the search form of `common\models\ofd\OfdStore`.
 */
class OfdStoreSearch extends OfdStore
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ofd_id'], 'integer'],
            [['uid', 'name', 'address'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OfdStore::find();

        // add conditions that should always apply here
        $query->andWhere([
            'company_id' => $this->company_id,
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ofd_id' => $this->ofd_id,
        ]);

        $query->andFilterWhere(['like', 'uid', $this->uid])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'address', $this->address]);

        return $dataProvider;
    }
}
