<?php

namespace frontend\modules\retail\models;

use Yii;
use common\components\date\DateHelper;
use common\models\employee\EmployeeRole;
use common\models\ofd\OfdKkt;
use common\models\ofd\OfdReceipt;
use common\models\ofd\OfdReceiptItem;
use common\models\ofd\OfdStore;
use frontend\modules\reports\models\FlowOfFundsReportSearch;

class RetailEmployeeRating extends \yii\base\Component
{
    const TYPE_AMOUNT = 'amount';
    const TYPE_COUNT = 'count';

    protected $_store;
    protected $_type;
    protected $_date;
    protected $_currentData;
    protected $_previousData;

    public static $typeArray = [
        self::TYPE_AMOUNT,
        self::TYPE_COUNT,
    ];

    public static $selectArray = [
        self::TYPE_AMOUNT => 'SUM({{ofd_receipt}}.[[total_sum]])',
        self::TYPE_COUNT => 'COUNT({{ofd_receipt}}.[[id]])',
    ];

    public static $unitArray = [
        self::TYPE_AMOUNT => '<i class="fa fa-rub"></i>',
        self::TYPE_COUNT => 'шт.',
    ];

    public static $colors = [
        self::TYPE_AMOUNT => [1 => '#57B8AE', 2 => '#92CDDC'],
        self::TYPE_COUNT => [1 => '#57B8AE', 2 => '#92CDDC'],
    ];

    public static $roleArray = [
        EmployeeRole::ROLE_CHIEF,
        EmployeeRole::ROLE_SUPERVISOR,
        EmployeeRole::ROLE_SUPERVISOR_VIEWER,
        EmployeeRole::ROLE_DEMO,
    ];

    /**
     * @return array
     */
    public function setType($value)
    {
        $this->_type = in_array($value, self::$typeArray) ? $value : reset(self::$typeArray);
    }

    /**
     * @return array
     */
    public function setDate($value)
    {
        $this->_date = date_create($value) ?: date_create();
        $this->_date->modify('first day of this month');
    }

    /**
     * @return array
     */
    public function getType()
    {
        if ($this->_type === null) {
            $this->_type = reset(self::$typeArray);
        }

        return $this->_type;
    }

    public function setStore($value)
    {
        $this->_store = ($value > 0) ? (int)$value : null;
    }

    public function getStore()
    {
        return $this->_store;
    }

    /**
     * @return DateTime
     */
    public function getDate()
    {
        if ($this->_date === null) {
            $this->_date = date_create()->modify('first day of this month');
        }

        return clone $this->_date;
    }

    /**
     * @return DateTime
     */
    public function getNextDate()
    {
        $date = clone $this->getDate();

        return $date->modify('first day of +1 month');
    }

    /**
     * @return DateTime
     */
    public function getPrevDate()
    {
        $date = clone $this->getDate();

        return $date->modify('first day of -1 month');
    }

    /**
     * @return array
     */
    public function getEmployee()
    {
        return Yii::$app->user->identity;
    }

    /**
     * @return array
     */
    public function getCompany()
    {
        return Yii::$app->user->identity->company;
    }

    /**
     * @return array
     */
    public function getUnit()
    {
        return self::$unitArray[$this->type];
    }

    /**
     * @return array
     */
    public function getCanAll()
    {
        $role = $this->employee->currentEmployeeCompany->employee_role_id;

        return in_array($role, self::$roleArray);
    }

    /**
     * @return false|string
     */
    public function getStartDate()
    {
        $firstDate = $this->getChecksQuery()->min('ofd_receipt.date_time');
        $firstDate = $firstDate ? DateHelper::format($firstDate, 'Y-m-01', 'Y-m-d H:i:s') : date('Y-m-01');

        return $firstDate;
    }

    /**
     * @return array
     */
    public function getDateItems()
    {
        $items = [];
        $date = $this->getStartDate();
        $currentDate = date('Y-m-01');

        do {
            $items[$date] = FlowOfFundsReportSearch::$month[DateHelper::format($date, 'm', 'Y-m-01')] . ' ' .
                DateHelper::format($date, 'Y', 'Y-m-01');
            $date = date('Y-m-01', strtotime('+1 month', strtotime($date)));
        } while ($date <= $currentDate);

        return $items;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChecksQuery()
    {
        $query = OfdReceipt::find()
            ->rightJoin(
                OfdKkt::tableName(),
                '{{ofd_kkt}}.[[id]] = {{ofd_receipt}}.[[kkt_id]]'
            )->rightJoin(
                OfdStore::tableName(),
                '{{ofd_store}}.[[company_id]]={{ofd_kkt}}.[[company_id]]'.
                ' AND {{ofd_store}}.[[ofd_id]] = {{ofd_kkt}}.[[ofd_id]]'.
                ' AND {{ofd_store}}.[[uid]] = {{ofd_kkt}}.[[store_uid]]'
            )->andWhere(['ofd_receipt.company_id' => $this->company->id])
            ->andFilterWhere(['ofd_store.id' => $this->store]);

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBaseQuery()
    {
        $query = $this->getChecksQuery()
            ->select([
                'value' => self::$selectArray[$this->type],
                'operator_id',
            ])
            ->orderBy(['value' => SORT_DESC])
            ->groupBy('operator_id')
            ->indexBy('operator_id')
            ->limit(10);

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrentQuery()
    {
        $query = $this->getBaseQuery()
            ->andWhere([
                'between',
                'ofd_receipt.date_time',
                $this->date->format('Y-m-d 00:00:00'),
                $this->date->modify('last day of this month')->format('Y-m-d 23:59:59'),
            ]);

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreviousQuery()
    {
        $query = $this->getBaseQuery()
            ->andWhere([
                'between',
                'ofd_receipt.date_time',
                $this->prevDate->format('Y-m-d 00:00:00'),
                $this->prevDate->modify('last day of this month')->format('Y-m-d 23:59:59'),
            ]);

        return $query;
    }

    /**
     * @return array
     */
    public function getCurrentData()
    {
        if ($this->_currentData === null) {

            $this->_currentData = $this->getCurrentQuery()->column();
        }

        return $this->_currentData;
    }

    /**
     * @return array
     */
    public function getPreviousData()
    {
        if ($this->_previousData === null) {

            $this->_previousData = $this->getPreviousQuery()->column();
        }

        return $this->_previousData;
    }

    /**
     * @return array
     */
    public function getRatingData()
    {
        $curr = $this->currentData;
        $prev = $this->previousData;

        return [
            'current' => $curr,
            'previous' => $prev,
            'max' => max($curr ? max($curr) : 0, $prev ? max($prev) : 0),
        ];
    }

    /**
     * @return string
     */
    public function getFio($id)
    {
        $receipt = OfdReceipt::findOne([
            'operator_id' => $id,
            'company_id' => $this->company->id,
        ]);

        return $receipt ? $receipt->operator_name : '&nbsp;';
    }

    /**
     * @return string
     */
    public function format($value)
    {
        if ($this->type == self::TYPE_COUNT) {
            return number_format($value, 0, ',', ' ') . '&nbsp;' . $this->unit;
        } else {
            return number_format($value / 100, 2, ',', ' ') . '&nbsp;' . $this->unit;
        }
    }

    /**
     * @return string
     */
    public function getColor1()
    {
        return self::$colors[$this->getType()][1];
    }

    /**
     * @return string
     */
    public function getColor2()
    {
        return self::$colors[$this->getType()][2];
    }
}
