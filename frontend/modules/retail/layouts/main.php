<?php

use common\models\Company;
use frontend\rbac\permissions;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\helpers\Url;


$this->beginContent('@frontend/views/layouts/main.php');

$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;
$module = Yii::$app->controller->module->id;

$employee = Yii::$app->user->identity;
$company = $employee->company;

$ofdUserArray = $employee->getOfdUsers()->andWhere([
    'company_id' => $company->id,
])->all();

if (count($ofdUserArray) === 1) {
    $uploadUrl = [
        "/ofd/{$ofdUserArray[0]->ofdProvider->alias}/default/receipt",
    ];
} else {
    $uploadUrl = [
        '/ofd/default/select',
    ];
}
?>
<div class="debt-report-content container-fluid" style="padding: 0; margin-top: -10px;">
    <div class="d-flex my-3">
        <h3 class="m-0">
            Розничные продажи
        </h3>
        <?= Html::a('<i class="fa fa-plus mr-2"></i> Загрузить чеки', $uploadUrl, [
            'class' => 'button-regular button-hover-transparent ml-auto ofd_module_open_link',
        ]) ?>
    </div>
    <?php NavBar::begin([
        'options' => [
            'class' => 'navbar-report navbar-default',
        ],
        'brandOptions' => [
            'style' => 'margin-left: 0;'
        ],
        'containerOptions' => [
            'style' => 'padding: 0;'
        ],
        'innerContainerOptions' => [
            'class' => 'container-fluid',
            'style' => 'padding: 0;'
        ],
    ]) ?>

    <?= Nav::widget([
        'id' => 'debt-report-menu',
        'items' => [
            [
                'label' => 'Продажи',
                'url' => ['/ofd/sales/index',],
            ],
            [
                'label' => 'Отчеты',
                'url' => ['/ofd/report/index',],
            ],
            [
                'label' => 'Чеки',
                'url' => ['/ofd/receipt/index',],
            ],
            [
                'label' => 'Точки продаж',
                'url' => ['/ofd/store/index',],
            ],
            [
                'label' => 'ОФД',
                'url' => ['/ofd/default/index',],
            ],
        ],
        'options' => ['class' => 'navbar-nav'],
    ]) ?>

    <?php NavBar::end(); ?>

    <?= $content; ?>

    <?= frontend\modules\retail\widgets\OfdModalWidget::widget() ?>
</div>

<?php $this->endContent(); ?>
