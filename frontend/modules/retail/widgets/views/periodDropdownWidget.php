<?php

use yii\bootstrap\Html;
use yii\bootstrap\Dropdown;

/** $this yii\web\View */
/** $year integer */
/** $items array */

?>

<div class="dropdown">
    <?= Html::button($year.'<span class="wropdown_arrow"></span>', [
        'class' => 'button-regular button-hover-transparent dropdown-toggle min-w-130',
        'data-toggle' => 'dropdown',
    ]) ?>
    <?= Dropdown::widget([
        'items' => $items,
        'options' => [
            'class' => 'min-w-130',
        ],
    ]) ?>
</div>
