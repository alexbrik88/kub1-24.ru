<?php

namespace frontend\modules\retail\widgets;

class PeriodDropdownWidget extends \yii\base\Widget
{
    public $searchModel;

    public function run()
    {
        $items = [];
        foreach ($this->searchModel->getYearFilter() as $key => $value) {
            $items[] = [
                'label' => $value,
                'url' => ['index', 'year' => $key],
            ];
        }

        return $this->render('periodDropdownWidget', [
            'items' => $items,
            'year' => $this->searchModel->year,
        ]);
    }
}
