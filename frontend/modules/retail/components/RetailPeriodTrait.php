<?php

namespace frontend\modules\retail\components;

use common\models\ofd\OfdReceipt;

trait RetailPeriodTrait
{
    protected $_year;
    protected $_month;
    protected $minDate;
    protected $periodDateRange;

    /**
     * {@inheritdoc}
     */
    public function setYear($value)
    {
        $this->_year = min($value, date('Y'));
    }

    /**
     * {@inheritdoc}
     */
    public function getYear()
    {
        if ($this->_year === null) {
            $this->_year = date('Y');
        }

        return $this->_year;
    }

    /**
     * {@inheritdoc}
     */
    public function setMonth($value)
    {
        $this->_month = ($value >= 1 && $value <= 12)
            ? str_pad($value, 2, "0", STR_PAD_LEFT)
            : date('m');
    }

    /**
     * {@inheritdoc}
     */
    public function getMonth()
    {
        if ($this->_month === null) {
            $this->_month = date('m');
        }

        return $this->_month;
    }

    /**
     * @return array
     */
    public function getReceiptMinDate()
    {
        if ($this->minDate === null) {
            $minDate = OfdReceipt::find()->andWhere([
                'company_id' => $this->company_id,
            ])->min('date_time');

            $this->minDate = date_create($minDate ?: null);
        }

        return $this->minDate;
    }

    /**
     * @return \DateTime|false
     */
    public function getPeriodDateFrom()
    {
        return date_create("{$this->year}-01-01 00:00:00");
    }

    /**
     * @return mixed
     */
    public function getPeriodDateTill()
    {
        return min(date_create(), date_create("{$this->year}-12-31 23:59:59"));
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        $range = [];
        $minYear = $this->getReceiptMinDate()->format('Y');
        $curYear = date('Y');
        foreach (range($minYear, $curYear) as $value) {
            $range[$value] = $value;
        }
        arsort($range);

        return $range;
    }

    /**
     * @return array
     */
    public function getPeriodDateRange()
    {
        if ($this->periodDateRange === null) {
            $this->periodDateRange = [];
            $date = $this->getPeriodDateTill();
            do {
                $this->periodDateRange[] = clone $date;
                $date->modify('-1 day');
            } while ($date->format('z') > 0);
        }

        return $this->periodDateRange;
    }
}
