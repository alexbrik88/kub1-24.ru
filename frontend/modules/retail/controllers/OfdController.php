<?php

namespace frontend\modules\retail\controllers;

use common\models\ofd\Ofd;
use frontend\modules\retail\models\OfdSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * Ofd controller for the `ofd` module
 */
class OfdController extends Controller
{
    public static $selectActions = [
        'receipt',
    ];

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\UserRole::ROLE_CHIEF,
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $employee = Yii::$app->user->identity;
        $searchModel = new OfdSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'employee' => $employee,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ofd model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionSelect($action = 'receipt')
    {
        $this->layout = 'modal';

        if (!in_array($action, self::$selectActions)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        $ofdUserArray = $employee->getOfdUsers()->andWhere([
            'company_id' => $company->id,
        ])->all();

        return $this->render('select', [
            'employee' => $employee,
            'company' => $company,
            'ofdUserArray' => $ofdUserArray,
            'action' => $action,
        ]);
    }

    /**
     * Finds the Ofd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ofd the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ofd::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
