<?php

namespace frontend\modules\retail\controllers;

use frontend\modules\retail\models\ImportConfigForm;
use Yii;
use yii\web\Controller;

/**
 * ImportConfig controller for the `ofd` module
 */
class ImportConfigController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\UserRole::ROLE_CHIEF,
                        ],
                    ],
                ],
            ],
            'ajax' => [
                'class' => 'yii\filters\AjaxFilter',
                'only' => [
                    'update',
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionUpdate()
    {
        $saved  = false;
        $model = new ImportConfigForm(Yii::$app->user->identity->company);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $saved  = true;
        }

        return $this->renderAjax('update', [
            'model' => $model,
            'saved' => $saved,
        ]);
    }
}
