<?php

namespace frontend\modules\retail\controllers;

use Yii;
use common\models\ofd\OfdReceipt;
use frontend\components\StatisticPeriod;
use frontend\modules\retail\models\OfdReceiptSearch;
use yii\db\Connection;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ReceiptController implements the CRUD actions for OfdReceipt model.
 */
class ReceiptController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\UserRole::ROLE_CHIEF,
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all OfdReceipt models.
     * @return mixed
     */
    public function actionIndex()
    {
        $user = Yii::$app->user->identity;
        $company = $user->company;

        $searchModel = new OfdReceiptSearch([
            'company_id' => $company->id,
            'period' => StatisticPeriod::getSessionPeriod(),
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'user' => $user,
            'company' => $company,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OfdReceipt model.
     * @param string $id
     * @param string $date_time
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $date_time = null)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id, $date_time),
        ]);
    }

    public function actionManyDelete()
    {
        $idArray = Yii::$app->request->post('receiptId', []);
        $isDeleted = true;

        foreach ($idArray as $id) {

            if ($model = $this->findModel($id, null, true)) {

                $isDeleted = Yii::$app->db->transaction(function (Connection $db) use ($model) {
                    foreach ($model->items as $item) {
                        if (!$item->delete()) {
                            $db->transaction->rollBack();
                            return false;
                        }
                    }
                    if (!$model->delete()) {
                        $db->transaction->rollBack();
                        return false;
                    }

                    return true;
                });

                if (!$isDeleted) {
                    Yii::$app->session->setFlash('error', 'Не удалось удалить чеки.');
                    break;
                }
            }
        }

        if ($isDeleted) {
            Yii::$app->session->setFlash('success', 'Чеки удалены.');
        }

        return $this->redirect(Yii::$app->request->referrer ?: ['index']);
    }

    /**
     * Finds the OfdReceipt model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @param string $date_time
     * @return OfdReceipt|null the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $date_time = null, $skipEmpty = false)
    {
        $model = OfdReceipt::find()->where([
            'id' => $id,
            'company_id' => Yii::$app->user->identity->company->id,
        ])->andFilterWhere(['date_time' => $date_time])->one();

        if ($model !== null) {
            return $model;
        }

        if ($skipEmpty)
            return null;

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
