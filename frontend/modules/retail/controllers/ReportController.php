<?php

namespace frontend\modules\retail\controllers;

use Yii;
use common\components\excel\Excel;
use common\models\ofd\OfdReceipt;
use frontend\components\StatisticPeriod;
use frontend\modules\retail\models\OfdReportSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ReportController implements the CRUD actions for OfdReceipt model.
 */
class ReportController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\UserRole::ROLE_CHIEF,
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $company = Yii::$app->user->identity->company;

        $searchModel = new OfdReportSearch([
            'company_id' => $company->id,
            'period' => StatisticPeriod::getSessionPeriod(),
        ]);

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionGetXls($year = null)
    {
        $company = Yii::$app->user->identity->company;

        $searchModel = new OfdReportSearch([
            'company_id' => $company->id,
            'period' => StatisticPeriod::getSessionPeriod(),
        ]);

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $excel = new Excel();

        $fileName = 'Отчет о розничных продажах';

        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $dataProvider->models,
            'title' => $fileName,
            'rangeHeader' => range('A', 'E'),
            'columns' => [
                'name',
                'count',
                'amount:money',
                'average_price:money',
                'part:decimal',
            ],
            'headers' => [
                'name' => 'Название товара и услуги',
                'count' => 'Кол-во',
                'amount' => 'Сумма продаж',
                'average_price' => 'Средняя цена',
                'part' => 'Доля %',
            ],
            'format' => 'Xlsx',
            'fileName' => $fileName,
        ]);
    }

    /**
     * Lists all OfdReceipt models.
     * @return mixed
     */
    public function actionKktFilter($year = null)
    {
        $company = Yii::$app->user->identity->company;
        $searchModel = new OfdReportSearch([
            'company_id' => $company->id,
            'year' => $year,
        ]);
        $yearFilter = $searchModel->getYearFilter();
        if (!in_array($searchModel->year, $yearFilter)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $searchModel->load(Yii::$app->request->post());

        return $this->render('_search', [
            'model' => $searchModel,
        ]);
    }

    /**
     * Finds the OfdReceipt model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @param string $date_time
     * @return OfdReceipt the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $date_time)
    {
        if (($model = OfdReceipt::findOne(['id' => $id, 'date_time' => $date_time])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
