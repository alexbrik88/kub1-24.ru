<?php

namespace frontend\modules\retail\controllers;

use frontend\modules\retail\models\OfdDashboardChartModel;
use frontend\modules\retail\models\RetailEmployeeRating;
use Yii;
use yii\web\Controller;

/**
 * Dashboard controller for the `ofd` module
 */
class DashboardController extends Controller
{
    public static $chartView = [
        'chart_11' =>'chart_11_amount_by_hour',
        'chart_12' =>'chart_12_amount_by_day',
        'chart_13' =>'chart_13_average_by_day',
    ];

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\UserRole::ROLE_CHIEF,
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $storeArray = $company->getOfdStores()->select('local_name')->orderBy(['local_name' => SORT_ASC,])->indexBy('id')->column();
        $searchModel = new OfdDashboardChartModel(['company_id' => $company->id]);
        $searchModel->load(Yii::$app->request->get(), '');

        return $this->render('index', [
            'employee' => $employee,
            'company' => $company,
            'storeArray' => $storeArray,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * @param string $chart
     * @return string
     */
    public function actionGetDataAjax($chart)
    {
        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $searchModel = new OfdDashboardChartModel(['company_id' => $company->id]);
        $searchModel->load(Yii::$app->request->post(), '');

        if (isset(self::$chartView[$chart])) {

            // params
            $customOffset = Yii::$app->request->post('offset', 0);

            return $this->renderPartial('partial/' . self::$chartView[$chart], [
                'id' => $chart,
                'isAjax' => true,
                'searchModel' => $searchModel,
                'customOffset' => $customOffset,
            ]);
        }

        return 'Unknown Chart Id.';
    }

    /**
     * @return string
     */
    public function actionEmployeeRating($type, $date, $store = null)
    {
        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        $ratingModel = new RetailEmployeeRating([
            'store' => $store,
            'type' => $type,
            'date' => $date,
        ]);

        return $this->renderAjax('partial/chart_31_employee_rating', [
            'rating' => $ratingModel,
        ]);
    }
}
