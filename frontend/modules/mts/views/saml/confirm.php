<?php

use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Подтвердите свою учетную запись';

?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="row">
    <div class="col-lg-5">
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <?= $form->field($model, 'username')->textInput([
            'readonly' => true,
        ]) ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <div style="color:#999;margin:1em 0">
            Если Вы забыли пароль, то Вы можете <?= Html::a('сбросить его', ['site/request-password-reset']) ?>.
        </div>
        <div class="form-group">
            <?= Html::submitButton('Подтвердить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
