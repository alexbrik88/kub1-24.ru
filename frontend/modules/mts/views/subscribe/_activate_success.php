<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\mts\models\MtsSubscribeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mts-subscribe-activate-success">

    <h2>
        Подписка успешно активирована
    </h2>

    <div class="text-center">
        <?= Html::button('Ok', ['class' => 'btn darkblue text-white pull-right', 'data-dismiss' => 'modal']) ?>
    </div>
</div>
