<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\modules\mts\models\MtsPaymentForm */
/* @var $success boolean */

?>
<div class="mts-subscribe-activate">
    <?php Pjax::begin([
        'id' => 'mts-subscribe-activate-pjax',
        'formSelector' => '#mts-subscribe-activate-form',
        'enablePushState' => false,
        'timeout' => 10000,
    ]); ?>
    <?php if ($success) : ?>
        <?= $this->render('_activate_success', [
            'model' => $model,
        ]) ?>
    <?php else : ?>
        <?= $this->render('_activate_form', [
            'model' => $model,
        ]) ?>
    <?php endif ?>
    <?php Pjax::end(); ?>
</div>
