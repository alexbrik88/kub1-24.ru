<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model frontend\modules\mts\models\MtsPaymentForm */
/* @var $form yii\widgets\ActiveForm */


?>

<div class="mts-subscribe-form">
    <?php $form = ActiveForm::begin([
        'id' => 'mts-subscribe-activate-form',
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'options' => [
            'data-pjax' => true,
        ],
    ]); ?>

    <?= $form->field($model, 'companyList')->checkboxList(ArrayHelper::map($model->allCompanies, 'id', 'shortTitle')) ?>

    <?= $form->field($model, 'companyId')->widget(Select2::classname(), [
        'data' => ArrayHelper::map($model->okCompanies, 'id', 'shortTitle'),
        'options' => [
            'placeholder' => '',
        ],
    ]); ?>

    <div>
        <?= Html::button('Отмена', ['class' => 'btn darkblue text-white pull-right', 'data-dismiss' => 'modal']) ?>
        <?= Html::submitButton('Применить', ['class' => 'btn darkblue text-white']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<script type="text/javascript">
$('.mts-subscribe-form input[type=checkbox]').uniform();
</script>