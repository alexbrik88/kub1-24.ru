<?php

use common\models\service\SubscribeHelper;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use common\components\grid\GridView;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $company common\models\Company */
/* @var $tariffGroupArray common\models\service\SubscribeTariffGroup[] */
/* @var $mtsSubscribeArray [] */
/* @var $searchModel frontend\modules\mts\models\MtsSubscribeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Оплата сервиса'.(Yii::$app->user->isMtsUser ? '' : ' через МТС');

$expiresArray = [];
foreach ($tariffGroupArray as $item) {
    $subscribes = SubscribeHelper::getPayedSubscriptions($company->id, $item->id);
    if ($subscribes) {
        $expiresArray[] = [
            'name' => $item->name,
            'date' => $date = SubscribeHelper::getExpireDate($subscribes),
            'days' => Yii::t('yii', '{n, plural, =0{# дня} 1{# день} one{# день} few{# дня} other{# дней}}', [
                'n' => SubscribeHelper::getExpireLeftDays($date),
            ]),
            'is_trial' => $item->id == SubscribeTariffGroup::STANDART && end($subscribes)->tariff_id == SubscribeTariff::TARIFF_TRIAL,
        ];
    }
}
if (count($expiresArray) > 1 && $expiresArray[0]['is_trial']) {
    $exp = array_shift($expiresArray);
    $expiresArray[] = $exp;
}

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-pay',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

$this->registerJs(
<<<JS
$(document).on('hide.bs.modal', '#ajax-modal-box', function () {
    $.pjax.reload('#mts-subscribe-index-pjax', {'timeout':10000});
});
JS
);
?>

<?= $this->render('_index_style'); ?>

<h1 class="page-title">
    <?= $this->title ?>
</h1>

<div class="mts-subscribe-index">
    <?php if ($expiresArray && ($expire = array_shift($expiresArray))) : ?>
        <h2 class="page-title">
            Текущая подписка "<?= $expire['name'] ?>" активна до
            <?= date('d.m.Y', $expire['date']); ?>
            (осталось: <?= $expire['days'] ?>)
        </h2>
    <?php else : ?>
        <h2 class="page-title">
            Текущая подписка БЕСПЛАТНО
        </h2>
    <?php endif; ?>

    <?php if ($expiresArray) : ?>
        <div class="mar-b-20">
        <?php foreach ($expiresArray as $expire) : ?>
            <div>
                "<?= $expire['name'] ?>"
                до <?= date(DateHelper::FORMAT_USER_DATE, $expire['date']); ?>
                (осталось: <?= \Yii::t('yii', '{n, plural, =0{# дня} 1{# день} one{# день} few{# дня} other{# дней}}', [
                    'n' => $expire['days'],
                ]); ?>)
            </div>
        <?php endforeach; ?>
        </div>
    <?php endif; ?>

    <div class="portlet box darkblue">
        <div class="portlet-title">
            <div class="caption">
                Оплатить подписку
            </div>
        </div>
        <div class="portlet-body tariff_block-list">
            <div class="row" style="margin-left: -5px; margin-right: -5px;">
                <?php foreach ($tariffGroupArray as $item) : ?>
                    <?php if (isset($mtsSubscribeArray[$item->id])) : ?>
                        <?= $this->render('_tariff_block', [
                            'company' => $company,
                            'group' => $item,
                            'url' => $mtsSubscribeArray[$item->id],
                        ]) ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

    <div class="portlet box darkblue">
        <div class="portlet-title">
            <div class="caption">
                Оплаченные подписки, ожидающие активации
            </div>
        </div>
        <div class="portlet-body">
            <?php Pjax::begin([
                'id' => 'mts-subscribe-index-pjax',
                'timeout' => 10000,
            ]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover documents_table status_nowrap',
                    'id' => 'mts-subscribe-table',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],

                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => "{items}\n{pager}",
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'subscription_id',
                    [
                        'attribute' => 'tariff_id',
                        'value' => 'tariff.tariffName',
                        'value' => function ($model) {
                            return "{$model->tariff->tariffGroup->name} {$model->tariff->tariffName}";
                        }
                    ],
                    'company_number',
                    'created_at:datetime',

                    [
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a('Активировать', [
                                'activate',
                                'id' => $model->id,
                            ], [
                                'class' => 'ajax-modal-btn',
                                'title' => 'Активировать подписку',
                                'data-pjax' => '0',
                            ]);
                        }
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
