<style>
    .row-5ths {
        width:100%;
        padding-left:10px;
        padding-right:10px;
    }
    .col-xs-5ths,
    .col-sm-5ths,
    .col-md-5ths,
    .col-lg-5ths {
        position: relative;
        min-height: 1px;
        padding-right: 5px;
        padding-left: 5px;
    }

    .col-xs-5ths {
        width: 20%;
        float: left;
    }

    @media (min-width: 768px) {
        .col-sm-5ths {
            width: 20%;
            float: left;
        }
    }

    @media (min-width: 992px) {
        .col-md-5ths {
            width: 20%;
            float: left;
        }
    }

    @media (min-width: 1200px) {
        .col-lg-5ths {
            width: 20%;
            float: left;
        }
    }

    .subscribe-tariff .tariff-icon {
        width:60px;
        height:60px;
        background-color:#4276a4;
        border-radius:30px!important;
    }
    .subscribe-tariff .tariff-icon > img {
        padding:10px;
    }
    .subscribe-tariff .tariff-group-name {
        text-transform: uppercase;
        font-weight: bold;
        font-size: 15px;
        margin-top: 20px;
        min-height: 45px;
    }
    .subscribe-tariff .bordered {
        border-left:1px solid #d7d7d7;
        border-right:1px solid #d7d7d7;
        border-top:1px solid #d7d7d7;
        padding:15px;
    }
    .subscribe-tariff .bordered:last-child {
        border-bottom:1px solid #d7d7d7;
    }

    .tariff-rules {
        min-height: 55px;
    }
    .tariff-options {

    }
    .tariff-price {
        font-size:44px;
    }
    .tariff-discount-proposition {
        margin-top:10px;
        color:#f3565d;
    }
    .tariff-description {
        overflow-y: hidden;
        height: 150px;
        position:relative;
    }
    .text-opacity {
        position:absolute;
        height: 100px;
        width:100%;
        bottom:0;
        left:0;
        z-index: 2;
        background-image: linear-gradient(to bottom, rgba(255,255,255,0), rgba(255,255,255,1));
    }
    .panel-promo-code {
        background-color: #eee;
        border: 1px solid  #d7d7d7;
        margin-bottom: 30px;
    }
    .subscribe-tariff .yellow.btn {
        color: white;
        background-color: #ffb848;
        padding: 10px 25px;
        margin-top:20px;
        width:100%;
    }
    .subscribe-tariff .yellow.btn:hover,
    .subscribe-tariff .yellow.btn:focus,
    .subscribe-tariff .yellow.btn:active {
        color: white;
        background-color: #ffaa24;
    }
    .tariff-read-more {
        color:#3175af;
        border-bottom: 1px dashed #3175af;
        width:70px;
        margin: 0 auto;
        cursor:pointer;
    }
    .pay-tariff-wrap {
        display: block;
        font-size: 14px;
        margin-bottom: 5px;
        text-align: left;
    }
    .bold-italic {
        font-weight:bold;
        font-style:italic;
    }
    .bold-italic.red {
        color: #f3565d;
    }
    #mtspaymentform-companylist label {
        display: block;
    }
    h2.page-title {
        font-size: 24px;
    }
</style>