<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var \yii\web\View $this */
/* @var \common\models\Company $company */
/* @var \common\models\service\SubscribeTariffGroup $group */

$tariffArray = $group->actualTariffs;
?>

<?php if ($tariffArray && $group->activeByCompany($company)) : ?>
    <?php
    $tariff = reset($tariffArray);
    $discount = $company->getDiscount($tariff->id);
    $price = round($tariff->price - $tariff->price * $discount / 100);
    $tariffDropdown = [];
    foreach ($tariffArray as $t) {
        $tariffDropdown[$t->id] = [
            'label' => $t->getTariffName(),
            'url' => "#tarif_price_{$t->id}",
            'linkOptions' => [
                'class' => 'choose-tariff',
                'data' => [
                    'toggle' => 'tab',
                ],
            ],
        ];
    }
    ?>

    <div class="col-md-5ths">
        <div class="subscribe-tariff">
            <div class="bordered">
                <div class="tariff-icon">
                    <img src="/img/icons/tariff_<?= $group->id ?>.png"/>
                </div>
                <div class="tariff-group-name">
                    <?= $group->name ?>
                    <?php if ($group->id == 1): ?>
                        <div style="font-size: 13px; text-transform: none">
                            (Выставление счетов)
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="bordered">
                <div class="tariff-rules">
                    <?php if ($group->is_base): ?>
                        <?php $items = [
                            [
                                'label' => '300 счетов',
                                'url' => ["#"],
                                'linkOptions' => ['class' => 'active choose-invoices-count', 'data' => ['tariff-invoices' => 300]],
                            ],
                        ]; ?>
                        <div style="display: inline-block;">До </div>
                        <div style="display: inline-block; margin: 0 5px;">
                            <div class="dropdown">
                                <?= Html::tag('div', $items[0]['label'], [
                                    'class' => 'dropdown-toggle',
                                    'data-toggle' => 'dropdown',
                                    'style' => 'display: inline-block; border-bottom: 1px dashed #000; cursor: pointer;',
                                ])?>
                                <?= \yii\bootstrap\Dropdown::widget([
                                    'id' => 'invoices-count-dropdown',
                                    'items' => $items,
                                ])?>
                            </div>
                        </div>
                        <div style="display: inline-block;"> в месяц</div>
                    <?php elseif ($group->has_base): ?>
                        Тариф "Выставление счетов"<br/>
                        <strong>ВКЛЮЧЕН</strong> в стоимость<br/>
                        этого тарифа
                    <?php else: ?>
                        Тариф "Выставление счетов"<br/>
                        <strong>НЕ включен</strong> в стоимость<br/>
                        этого тарифа
                    <?php endif; ?>
                </div>
            </div>
            <div class="bordered">
                <div class="tab-content">
                    <?php $i = 0; ?>
                    <?php foreach ($tariffArray as $t) : ?>
                        <div role="tabpanel" class="tab-pane <?= $i == 0 ? 'active' : '' ?>" id="tarif_price_<?= $t->id ?>">
                            <div class="tariff-options">
                                <div style="display: inline-block;">Оплата за </div>
                                <div style="display: inline-block; width: 100px;">
                                    <div class="dropdown">
                                        <?= Html::tag('div', $tariffDropdown[$t->id]['label'], [
                                            'class' => 'dropdown-toggle',
                                            'data-toggle' => 'dropdown',
                                            'style' => 'display: inline-block; border-bottom: 1px dashed #000; cursor: pointer;',
                                        ])?>
                                        <?= \yii\bootstrap\Dropdown::widget([
                                            'id' => 'employee-rating-dropdown',
                                            'encodeLabels' => false,
                                            'items' => $tariffDropdown,
                                        ])?>
                                    </div>
                                </div>
                            </div>
                            <div class="tariff-price" data-tariff-price="<?= $price ?>" data-tariff-id="<?= $tariff->id ?>">
                                <span class="current-tariff-price">
                                    <?= $t->price ?>
                                </span>
                                ₽
                            </div>
                            <div class="tariff-price-per-month">
                                <span class="current-tariff-price-per-month"><?= round($t->price / $t->duration_month) ?></span>
                                ₽ / месяц
                            </div>
                            <div class="tariff-discount-proposition" style="min-height: 36px;">
                                <?= $t->proposition ?>
                            </div>
                        </div>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="bordered">
                <div class="hidden">
                    <div id="tariff_description_<?= $group->id ?>">
                        <?php foreach (explode('|', $group->description) as $string) {
                            echo Html::tag('p', $string);
                        } ?>
                    </div>
                </div>
                <div style="text-align: center">
                    <?= Html::tag('div', 'Подробнее', [
                        'class' => 'tariff-read-more tooltip2-pay',
                        'data-tooltip-content' => '#tariff_description_'.$group->id,
                    ]) ?>
                    <?= Html::a('Оплатить', $url, [
                        'class' => 'tariff btn yellow but_weight',
                        'target' => '_blank',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>
