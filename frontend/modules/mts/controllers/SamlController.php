<?php

namespace frontend\modules\mts\controllers;

use common\models\service\SubscribeTariffGroup;
use common\models\employee\Employee;
use common\models\mts\MtsUser;
use frontend\models\Documents;
use frontend\models\LoginForm;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Saml controller for the `mts` module
 */
class SamlController extends Controller
{
    // Remove CSRF protection
    public $enableCsrfValidation = false;

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'login' => [
                'class' => 'asasmoyo\yii2saml\actions\LoginAction'
            ],
            'acs' => [
                'class' => 'asasmoyo\yii2saml\actions\AcsAction',
                'successCallback' => [$this, 'acsSuccess'],
                'successUrl' => Url::to(['/site/index']),
            ],
            'metadata' => [
                'class' => 'asasmoyo\yii2saml\actions\MetadataAction'
            ],
            'logout' => [
                'class' => 'asasmoyo\yii2saml\actions\LogoutAction',
                'returnTo' => Url::to('site/bye'),
            ],
            'sls' => [
                'class' => 'asasmoyo\yii2saml\actions\SlsAction',
                'successUrl' => Url::to('site/bye'),
            ],
        ];
    }

    /**
     * @param frontend\modules\mts\components\RpcAdd[] $pp
     * @return array
     */
    public function actionConfirm()
    {
        $user = MtsUser::findOne(Yii::$app->session->get('_mts_user_id'));

        if ($user === null) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }

        $model = new LoginForm([
            'username' => $user->email,
            'rememberMe' => 1,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            Yii::$app->session->remove('_mts_user_id');
            if (Yii::$app->user->identity->email == $user->email) {
                $user->updateAttributes(['employee_id' => Yii::$app->user->id]);
            }
            if (($company = Yii::$app->user->identity->company) && !$company->getInvoices()->exists()) {
                return $this->redirect(['/documents/invoice/create', 'type' => Documents::IO_TYPE_OUT]);
            }

            return $this->goBack(['/site/index']);
        }

        return $this->render('confirm', [
            'model' => $model,
        ]);
    }

    /**
     * @param  array $data
     * @return MtsUser
     */
    public function acsSuccess($data)
    {
        $user = $this->getMtsUser($data);

        if ($employee = $user->employee) {
            if (Yii::$app->user->login($employee)) {
                return $this->goBack(['/site/index']);
            }
        }

        if ($employee = Employee::findIdentityByLogin($user->email)) {
            Yii::$app->session->set('_mts_user_id', $user->id);

            return $this->redirect(['confirm']);
        }

        if ($employee = $user->createRegisteredEmployee()) {
            if (Yii::$app->user->login($employee)) {
                $firstSubscribe = $user->getMtsSubscribes()->orderBy(['id' => SORT_ASC])->one();
                $groupId = ArrayHelper::getValue($firstSubscribe, 'tariff.tariff_group_id');
                switch ($groupId) {
                    case SubscribeTariffGroup::B2B_PAYMENT:
                        $redirectUrl = ['/b2b/company'];
                        break;
                    case SubscribeTariffGroup::TAX_DECLAR_IP_USN_6:
                    case SubscribeTariffGroup::TAX_IP_USN_6:
                        $redirectUrl = ['/tax/robot/company'];
                        break;
                    default:
                        $redirectUrl = ['documents/invoice/first-create', 'type' => Documents::IO_TYPE_OUT];
                        break;
                }
                return $this->redirect($redirectUrl);
            }
        }

        return $this->redirect(ArrayHelper::getValue(Yii::$app->params, 'mts.purchaseUrl', ['/site/login']));
    }

    /**
     * Find or create MtsUser
     * @param  array $data
     * @return MtsUser
     */
    protected function getMtsUser($data)
    {
        if ($id = ArrayHelper::getValue($data, ['uid', 0])) {
            $user = MtsUser::findOne($id);
            if ($user === null) {
                $user = new MtsUser([
                    'id' => $id,
                    'employee_id' => null,
                    'firstname' => ArrayHelper::getValue($data, ['firstName', 0]),
                    'lastname' => ArrayHelper::getValue($data, ['lastName', 0]),
                    'email' => ArrayHelper::getValue($data, ['email', 0]),
                    'phone' => ArrayHelper::getValue($data, ['mobilePhone', 0]),
                    'groups' => implode(',', ArrayHelper::getValue($data, 'groups')),
                ]);

                if (!$user->save(false)) {
                    return null;
                }
            }

            return $user;
        }

        return null;
    }
}
