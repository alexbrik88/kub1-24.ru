<?php

namespace frontend\modules\mts\controllers;

use common\models\mts\MtsSubscribe;
use common\models\mts\MtsUser;
use common\models\service\Subscribe;
use JsonRpc2\Controller;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Response;

/**
 * Rpc controller for the `mts` module
 */
class RpcController extends Controller
{
    /**
     * @var string|array the configuration for creating the serializer that formats the response data.
     */
    public $serializer = 'yii\rest\Serializer';
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    '*' => ['post'],
                ],
            ],
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBasicAuth',
                'user' => Yii::$app->mts,
                'auth' => function ($username, $password) {
                    if ($username && $password) {
                        if (ArrayHelper::getValue(\Yii::$app->params, 'mts.rpc.username') === $username &&
                            ArrayHelper::getValue(\Yii::$app->params, 'mts.rpc.password') === $password
                        ) {
                            return new \frontend\modules\mts\models\MtsUser;
                        }
                    }

                    return null;
                },
            ],
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'user' => 'mts',
                'only' => ['point'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return []
     */
    public function actionAdd($productCode, $priceCode, $userId, $subscriptionId, $data)
    {
        $tariffId = Subscribe::find()->select('id')->where([
            'id' => ArrayHelper::getValue(explode('_', $priceCode), 1),
        ])->scalar();

        $model = new MtsSubscribe([
            'status' => MtsSubscribe::STATUS_ACTIVE,
            'mts_user_id' => $userId,
            'tariff_id' => $tariffId,
            'purchase_inn' => ArrayHelper::getValue($data, 'parameters.purchaseINN'),
            'company_number' => ArrayHelper::getValue($data, 'parameters.companyNumber'),
            'product_code' => $productCode,
            'price_code' => $priceCode,
            'subscription_id' => $subscriptionId,
            'data' => Json::encode($data),
            'request_id' => $this->requestObject->id,
        ]);

        if ($model->save()) {
            $companyArray = (array) ArrayHelper::getValue($model, 'mtsUser.employee.companiesByChief');
            $inn = ArrayHelper::getValue($model, 'paymentData.parameters.purchaseINN');
            $company = null;
            foreach ($companyArray as $item) {
                if (!$item->strict_mode && $item->inn == $inn) {
                    $company = $item;
                    break;
                }
            }
            if ($company !== null && count($companyArray) == $model->company_number) {
                $model->createPayment($company, ArrayHelper::getColumn($companyArray, 'id'));
            }

            return [
                'status' => MtsSubscribe::STATUS_ACTIVE,
            ];
        } else {
            \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);

            return [
                'status' => MtsSubscribe::STATUS_ERROR,
            ];
        }
    }

    /**
     * @inheritdoc
     */
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);
        return $this->serializeData($result);
    }

    /**
     * Serializes the specified data.
     * The default implementation will create a serializer based on the configuration given by [[serializer]].
     * It then uses the serializer to serialize the given data.
     * @param mixed $data the data to be serialized
     * @return mixed the serialized data.
     */
    protected function serializeData($data)
    {
        return Yii::createObject($this->serializer)->serialize($data);
    }
}
