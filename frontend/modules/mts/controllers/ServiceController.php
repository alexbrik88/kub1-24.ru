<?php

namespace frontend\modules\mts\controllers;

use common\models\company\RegistrationPageType;
use frontend\models\Documents;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Service controller for the `mts` module
 */
class ServiceController extends Controller
{
    public static $urlMap = [
        'invoice' => ['/documents/invoice/first-create', 'type' => Documents::IO_TYPE_OUT],
        'ip6' => ['/tax/robot/index'],
        'b2b' => ['/b2b'],
    ];
    public static $pageMap = [
        'invoice' => RegistrationPageType::PAGE_TYPE_MTS_INVOICE,
        'ip6' => RegistrationPageType::PAGE_TYPE_MTS_BUH_IP6,
        'b2b' => RegistrationPageType::PAGE_TYPE_MTS_B2B,
    ];

    /**
     * @param frontend\modules\mts\components\RpcAdd[] $pp
     * @return array
     */
    public function actionIndex($id)
    {
        $id = strtolower($id);
        $url = ArrayHelper::getValue(self::$urlMap, $id);

        if ($url === null) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }

        if (!Yii::$app->user->isGuest) {
            return $this->redirect($url);
        }

        Yii::$app->user->setReturnUrl($url);
        Yii::$app->session->set('__registration_page_type_id', ArrayHelper::getValue(self::$pageMap, $id));

        return $this->redirect(['/auth/login', 'authclient' => 'mts']);
    }
}
