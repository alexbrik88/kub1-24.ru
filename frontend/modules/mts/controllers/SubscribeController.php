<?php

namespace frontend\modules\mts\controllers;

use Yii;
use common\models\mts\MtsSubscribe;
use common\models\service\SubscribeTariffGroup;
use frontend\components\FrontendController;
use frontend\modules\mts\models\MtsPaymentForm;
use frontend\modules\mts\models\MtsSubscribeSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * SubscribeController implements the CRUD actions for MtsSubscribe model.
 */
class SubscribeController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->mtsUser !== null;
                        },
                    ],
                ],
            ],
            'ajax' => [
                'class' => 'common\components\filters\AjaxFilter',
                'only' => ['activate']
            ],
        ];
    }

    /**
     * Lists all MtsSubscribe models.
     * @return mixed
     */
    public function actionIndex()
    {
        $company = Yii::$app->user->identity->company;
        $tariffGroupArray = SubscribeTariffGroup::find()->where(['is_active' => 1])->all();
        $mtsSubscribeArray = (array) ArrayHelper::getValue(\Yii::$app->params, 'mts.tariffList');
        $searchModel = new MtsSubscribeSearch([
            'is_applied' => 0,
            'mts_user_id' => Yii::$app->user->identity->mtsUser->id,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'company' => $company,
            'tariffGroupArray' => $tariffGroupArray,
            'mtsSubscribeArray' => $mtsSubscribeArray,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new MtsSubscribe model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionActivate($id)
    {
        $mtsSubscribe = $this->findModel($id);
        $model = new MtsPaymentForm($mtsSubscribe);

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->renderAjax('activate', [
                'model' => $model,
                'success' => true,
            ]);
        } else {
            return $this->renderAjax('activate', [
                'model' => $model,
                'success' => false,
            ]);
        }
    }

    /**
     * Finds the MtsSubscribe model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MtsSubscribe the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = MtsSubscribe::findOne([
            'id' => $id,
            'is_applied' => 0,
            'mts_user_id' => Yii::$app->user->identity->mtsUser->id,
        ]);

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
