<?php

namespace frontend\modules\mts;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * mts module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\mts\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (Yii::$app->request->pathInfo == 'mts/auth') {
            $queryParams = Yii::$app->request->getQueryParams();
            $queryParams['authclient'] = 'mts';
            Yii::$app->request->setQueryParams($queryParams);
        }

        if (Yii::$app->request->pathInfo == 'mts/rpc') {
            Yii::$app->set('mts', [
                'class' => 'yii\web\User',
                'identityClass' => 'frontend\modules\mts\models\MtsUser',
                'enableSession' => false,
                'loginUrl' => null,
            ]);

            if (ArrayHelper::getValue(\Yii::$app->params, 'mts.rpc.debug')) {
                $logText = date('c') . "\n";
                foreach (getallheaders() as $key => $value) {
                    $logText .= "{$key}: {$value}\n";
                }
                $logText .= "\n".file_get_contents('php://input')."\n\n\n";

                file_put_contents(Yii::getAlias("@runtime/logs/mts-rpc.log"), $logText, FILE_APPEND);
            }
        }

        \Yii::$app->view->theme = \Yii::createObject([
            'class' => 'yii\base\Theme',
            'basePath' => '@frontend/themes/mts',
            'baseUrl' => '@web/themes/mts',
            'pathMap' => [
                '@frontend/views' => '@frontend/themes/mts/views',
                '@frontend/modules' => '@frontend/themes/mts/modules',
                '@frontend/widgets' => '@frontend/themes/mts/widgets',
            ],
        ]);

        \Yii::$app->set('saml', [
            'class' => 'asasmoyo\yii2saml\Saml',
            'configFileName' => '@frontend/modules/mts/config/saml.php',
        ]);
    }
}
