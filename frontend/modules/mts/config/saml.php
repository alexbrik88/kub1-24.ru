<?php

use yii\helpers\ArrayHelper;

$urlManager = \Yii::$app->urlManager;
$spBaseUrl = $urlManager->getHostInfo() . $urlManager->getBaseUrl();

$identifier = ArrayHelper::getValue(\Yii::$app->params, 'mts.saml.identifier', '');

return [
    'sp' => [
        'entityId' => 'service-provider',
        'assertionConsumerService' => [
            'url' => $spBaseUrl.'/mts/saml/acs',
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
        ],
        'singleLogoutService' => [
            'url' => $spBaseUrl.'/mts/saml/sls',
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ],
        //'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified',
    ],
    'idp' => [
        'entityId' => 'identity-provider',
        'singleSignOnService' => [
            'url' => 'https://b2b.mts.ru/websso/idp/login/'.$identifier,
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ],
        'singleLogoutService' => [
            'url' => 'https://b2b.mts.ru/websso/idp/logout/'.$identifier,
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ],
        //'x509cert' => '',
        'certFingerprint' => ArrayHelper::getValue(\Yii::$app->params, 'mts.saml.fingerprint', ''),
        'certFingerprintAlgorithm' => ArrayHelper::getValue(\Yii::$app->params, 'mts.saml.algorithm', ''),
    ],
    'debug' => ArrayHelper::getValue(\Yii::$app->params, 'mts.saml.debug', false),
];
