<?php

namespace frontend\modules\mts\models;

use common\models\Company;
use common\models\mts\MtsSubscribe;
use common\models\service\Payment;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class MtsPaymentForm
 * @package frontend\modules\mts\models
 */
class MtsPaymentForm extends Model
{
    /**
     * Form attributes
     */
    public $companyList;
    public $companyId;

    protected $_mtsSubscribe;
    protected $_payment;
    protected $_allCompanyes;
    protected $_okCompanyes;

    /**
     * @param Company $company
     * @param array $config
     * @throws Exception
     */
    public function __construct(MtsSubscribe $mtsSubscribe, $config = [])
    {
        $this->_mtsSubscribe = $mtsSubscribe;
        $allCompanyes = (array) ArrayHelper::getValue($mtsSubscribe, 'mtsUser.employee.companiesByChief');
        $okCompanyes = array_filter($allCompanyes, function ($item) {
            return $item->strict_mode == 0;
        });
        $this->_allCompanyes = ArrayHelper::index($allCompanyes, 'id');
        $this->_okCompanyes = ArrayHelper::index($okCompanyes, 'id');

        $inn = ArrayHelper::getValue($mtsSubscribe, 'paymentData.parameters.purchaseINN');
        foreach ($okCompanyes as $item) {
            if ($item->inn == $inn) {
                $this->companyId = $item->id;
                break;
            }
        }

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (empty($this->_okCompanyes)) {
            $this->addError('companyId', 'Не найдено ни одной компании с заполненными реквизитами.');
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['companyList'], 'filter', 'filter' => function ($value) {
                return is_array($value) ? array_unique($value) : [];
            }],
            [
                ['companyList', 'companyId'], 'required',
                'message' => 'Необходимо выбрать «{attribute}».'
            ],
            [['companyList'], 'each',
                'rule' => [
                    'in',
                    'range' => array_keys($this->_allCompanyes),
                ],
            ],
            ['companyList', function ($attribute, $params) {
                if (count($this->$attribute) != $this->_mtsSubscribe->company_number) {
                    $this->addError($attribute, "Количество выбранных компаний должно соответствовать количеству оплаченных компаний. Количество оплаченных компаний: {$this->_mtsSubscribe->company_number} шт.");
                }
            }],
            [
                ['companyId'],'in',
                'range' => array_keys($this->_okCompanyes),
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'companyList' => 'Оплачиваемые компании',
            'companyId' => 'От какой компании оплата',
        ];
    }

    /**
     * @return Company[]
     */
    public function getAllCompanies()
    {
        return $this->_allCompanyes;
    }

    /**
     * @return Company[]
     */
    public function getOkCompanies()
    {
        return $this->_okCompanyes;
    }

    /**
     * @return MtsSubscribe
     */
    public function getSubscribe()
    {
        return $this->_mtsSubscribe;
    }

    /**
     * @return Payment
     */
    public function getPayment()
    {
        return $this->_mtsSubscribe->payment;
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            if (empty($this->_okCompanyes)) {
                $this->addError('companyId', 'Не найдено ни одной компании с заполненными реквизитами.');

                return false;
            }

            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    public function save($validate = true)
    {
        if ($validate && !$this->validate()) {
            return false;
        }

        $company = $this->_okCompanyes[$this->companyId];

        return $this->_mtsSubscribe->createPayment($company, $this->companyList);
    }
}
