<?php

namespace frontend\modules\mts\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\mts\MtsSubscribe;

/**
 * MtsSubscribeSearch represents the model behind the search form about `common\models\mts\MtsSubscribe`.
 */
class MtsSubscribeSearch extends MtsSubscribe
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'company_number', 'subscription_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MtsSubscribe::find();

        // add conditions that should always apply here
        $query->andWhere([
            'mts_user_id' => $this->mts_user_id,
            'is_applied' => $this->is_applied,
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tariff_id' => $this->tariff_id,
            'company_number' => $this->company_number,
            'subscription_id' => $this->subscription_id,
        ]);

        return $dataProvider;
    }
}
