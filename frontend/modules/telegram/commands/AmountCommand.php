<?php

namespace frontend\modules\telegram\commands;

use common\models\cash\Cashbox;
use common\models\cash\CashOrderFlows;
use frontend\modules\telegram\factories\ApiFactory;
use frontend\modules\telegram\models\Identity;
use frontend\modules\telegram\models\OrderFactory;
use frontend\modules\telegram\models\State;
use TgBotApi\BotApiBase\Method\SendMessageMethod;

class AmountCommand extends AbstractCommand
{
    /** @var int */
    private const TYPE_LIST = [
        CashOrderFlows::FLOW_TYPE_INCOME => 'Покупатель',
        CashOrderFlows::FLOW_TYPE_EXPENSE => 'Поставщик',
    ];

    /**
     * @var
     */
    public $company_id;

    /**
     * @var int
     */
    public $cashbox_id;

    /**
     * @var int
     */
    public $contractor_id;

    /**
     * @inheritDoc
     * @throws
     */
    public function execute(State $state): bool
    {
        $state->resetAll();
        $state->setValue('command', $this);
        $this->exportParams($state, ['company_id', 'cashbox_id', 'contractor_id']);
        $api = ApiFactory::createBot();

        if (preg_match('/^([0-9]+)([.,][0-9]{2})*$/', $state->getMessage()->text, $matches)) {
            $identity = Identity::findOne([
                'company_id' => $this->company_id,
                'telegram_id' => $state->getTelegramId(),
            ]);

            if (empty($identity)) {
                return false;
            }

            $order = (new OrderFactory)->createOrder(
                $identity,
                $this->cashbox_id,
                $this->contractor_id,
                (int) (str_replace(',', '.', $matches[0]) * 100)
            );
            $order->save(false);
            $api->sendMessage(SendMessageMethod::create($state->getTelegramId(), $this->buildText($order)));
            $state->resetAll();

            return MenuCommand::createInstance(['menu' => 'main'])->execute($state);
        } else {
            $api->sendMessage(SendMessageMethod::create($state->getTelegramId(), 'Неверный формат суммы'));
        }

        return true;
    }

    /**
     * @param CashOrderFlows $order
     * @return string
     */
    private function buildText(CashOrderFlows $order): string
    {
        /** @var Cashbox[] $cashboxes */
        $cashboxes = $order->company->getCashboxes()->indexBy('id')->all();
        $box = '';

        if (count($cashboxes) > 1) {
            $box = sprintf("В кассу: %s\n", $cashboxes[$order->cashbox_id]->name);
        }

        return sprintf(
            "Операция добавлена в компанию: %s\n%s: %s\n%sСумма: %s руб.",
            $order->company->getShortTitle(),
            self::TYPE_LIST[$order->flow_type],
            $order->contractor->getNameWithType(),
            $box,
            number_format($order->amount / 100, 2, '.', ' '),
        );
    }
}
