<?php

namespace frontend\modules\telegram\commands;

use frontend\modules\telegram\models\State;

class CompanyCommand extends AbstractCommand
{
    /**
     * @var int
     */
    public $company_id;

    /**
     * @inheritDoc
     */
    public function execute(State $state): bool
    {
        $state->resetAll();
        $this->exportParams($state, ['company_id']);

        return SwitchCommand::createInstance(['company_id' => $this->company_id])->execute($state);
    }
}
