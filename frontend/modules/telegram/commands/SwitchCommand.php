<?php

namespace frontend\modules\telegram\commands;

use frontend\modules\telegram\models\CashboxRepository;
use frontend\modules\telegram\models\CompanyRepository;
use frontend\modules\telegram\models\RepositoryFactory;
use frontend\modules\telegram\models\State;
use yii\db\ActiveRecord;

class SwitchCommand extends AbstractCommand
{
    /**
     * @var int|null
     */
    public $company_id;

    /**
     * @var int|null
     */
    public $cashbox_id;

    /**
     * @param State $state
     * @return bool
     */
    public function execute(State $state): bool
    {
        if (empty($this->company_id)) {
            $this->company_id = $this->getId(CompanyRepository::class, [
                'telegram_id' => $state->getTelegramId(),
            ]);
        }

        if ($this->company_id && empty($this->cashbox_id)) {
            $this->cashbox_id = $this->getId(CashboxRepository::class, [
                'company_id' => $this->company_id,
            ]);
        }

        $state->resetAll();
        $this->exportParams($state, ['company_id', 'cashbox_id']);

        return $this->getCommand()->execute($state);
    }

    /**
     * @return CommandInterface
     */
    private function getCommand(): CommandInterface
    {
        if ($this->company_id && $this->cashbox_id) {
            return MenuCommand::createInstance(['menu' => 'operation']);
        }

        if ($this->company_id) {
            return MenuCommand::createInstance(['menu' => 'cashbox']);
        }

        return MenuCommand::createInstance(['menu' => 'company']);
    }

    /**
     * @param string $repositoryClass
     * @param array $attributes
     * @return int|null
     */
    private function getId(string $repositoryClass, array $attributes): ?int
    {
        $provider = (new RepositoryFactory)->createRepository($repositoryClass, $attributes)->getProvider();
        /** @var ActiveRecord[] $models */
        $models = $provider->getModels();

        if (count($models) == 1) {
            $model = array_shift($models);

            return $model->getAttribute('id');
        }

        return null;
    }
}
