<?php

namespace frontend\modules\telegram\commands;

use frontend\modules\telegram\models\State;

class PageCommand extends AbstractCommand
{
    /**
     * @var int
     */
    public $page;

    /**
     * @var string
     */
    public $menu;

    /**
     * @param State $state
     * @return bool
     */
    public function execute(State $state): bool
    {
        $state->setValue('page', $this->page);

        return MenuCommand::createInstance(['menu' => $this->menu])->execute($state);
    }
}
