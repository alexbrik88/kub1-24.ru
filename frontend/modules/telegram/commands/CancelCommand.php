<?php

namespace frontend\modules\telegram\commands;

use frontend\modules\telegram\models\State;

class CancelCommand extends AbstractCommand
{
    /**
     * @inheritDoc
     */
    public function execute(State $state): bool
    {
        $state->resetAll();

        return MenuCommand::createInstance(['menu' => 'main'])->execute($state);
    }

    /**
     * @inheritDoc
     */
    public function canAccess(State $state): bool
    {
        return true;
    }
}
