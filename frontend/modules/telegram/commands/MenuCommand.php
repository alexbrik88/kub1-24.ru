<?php

namespace frontend\modules\telegram\commands;

use frontend\modules\telegram\factories\ApiFactory;
use frontend\modules\telegram\menus\MenuFactory;
use frontend\modules\telegram\models\State;
use RuntimeException;
use TgBotApi\BotApiBase\Method\EditMessageReplyMarkupMethod;
use TgBotApi\BotApiBase\Method\EditMessageTextMethod;
use TgBotApi\BotApiBase\Method\SendMessageMethod;

/**
 * Показать меню
 */
class MenuCommand extends AbstractCommand
{
    /**
     * @var string
     */
    public $menu;

    /**
     * @param State $state
     * @return bool
     * @throws
     */
    public function execute(State $state): bool
    {
        try {
            $api = ApiFactory::createBot();
            $menu = (new MenuFactory)->createMenu($this->menu, $state);
            $messageId = $state->getValue('message_id');

            if ($state->getMessage()->messageId == $messageId) {
                $api->editMessageText(EditMessageTextMethod::create(
                    $state->getTelegramId(),
                    $messageId,
                    $menu->getText(),
                    ['replyMarkup' => $menu->getKeyboard()]
                ));

                $state->setValue('commands', $menu->getCommands());
            } else {
                $message = $api->sendMessage(SendMessageMethod::create(
                    $state->getTelegramId(),
                    $menu->getText(),
                    ['replyMarkup' => $menu->getKeyboard()]
                ));

                $state->setValue('commands', $menu->getCommands());
                $state->setValue('message_id', $message->messageId);

                if ($messageId) {
                    $api->editMessageReplyMarkup(EditMessageReplyMarkupMethod::create(
                        $state->getTelegramId(),
                        $messageId,
                    ));
                }
            }

            return true;
        } catch (RuntimeException $exception) {
            return false;
        }
    }

    /**
     * @param State $state
     * @return bool
     */
    public function canAccess(State $state): bool
    {
        return true;
    }
}
