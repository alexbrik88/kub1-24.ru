<?php

namespace frontend\modules\telegram\commands;

use RuntimeException;

class CommandFactory implements CommandFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function createCommand(string $name): CommandInterface
    {
        $class = __NAMESPACE__ . '\\' . ucfirst($name) . 'Command';

        if (!is_a($class, CommandInterface::class, true)) {
            throw new RuntimeException('Class not found.');
        }

        return new $class();
    }
}
