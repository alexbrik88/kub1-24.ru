<?php

namespace frontend\modules\telegram\commands;

use frontend\modules\telegram\models\State;

class OperationCommand extends AbstractCommand
{
    /**
     * @var int $company_id
     */
    public $company_id;

    /**
     * @var int
     */
    public $cashbox_id;

    /**
     * @var
     */
    public $contractor_type;

    /**
     * @inheritDoc
     */
    public function execute(State $state): bool
    {
        $state->resetAll();
        $this->exportParams($state, ['company_id', 'cashbox_id', 'contractor_type']);

        return SearchCommand::createInstance([
            'company_id' => $this->company_id,
            'cashbox_id' => $this->cashbox_id,
            'contractor_type' => $this->contractor_type,
        ])->execute($state);
    }
}
