<?php

namespace frontend\modules\telegram\commands;

use frontend\modules\telegram\models\State;

abstract class AbstractCommand implements CommandInterface
{
    /**
     * @param array $attributes
     */
    private function __construct(array $attributes = [])
    {
        array_walk($attributes, function ($value, string $name): void {
            $this->$name = $value;
        });
    }

    /**
     * @param array $attributes
     * @return static
     */
    public static function createInstance(array $attributes = []): CommandInterface
    {
        return new static($attributes);
    }

    /**
     * @param State $state
     * @param array $names
     * @return void
     */
    protected function exportParams(State $state, array $names): void
    {
        array_walk($names, function (string $name) use ($state): void {
            $state->setValue($name, $this->$name);
        });
    }

    /**
     * @param State $state
     * @return bool
     */
    public function canAccess(State $state): bool
    {
        return $state->getIdentity() ? true : false;
    }
}
