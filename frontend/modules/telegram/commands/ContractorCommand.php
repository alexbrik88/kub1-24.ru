<?php

namespace frontend\modules\telegram\commands;

use frontend\modules\telegram\models\State;

class ContractorCommand extends AbstractCommand
{
    /**
     * @var
     */
    public $company_id;

    /**
     * @var int
     */
    public $cashbox_id;

    /**
     * @var int
     */
    public $contractor_id;

    /**
     * @param State $state
     * @return bool
     * @throws
     */
    public function execute(State $state): bool
    {
        $state->resetAll();
        $state->setValue('command', AmountCommand::createInstance([
            'company_id' => $this->company_id,
            'cashbox_id' => $this->cashbox_id,
            'contractor_id' => $this->contractor_id,
        ]));
        $this->exportParams($state, ['company_id', 'cashbox_id', 'contractor_id']);

        return MenuCommand::createInstance(['menu' => 'amount'])->execute($state);
    }
}
