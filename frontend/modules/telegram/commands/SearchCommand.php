<?php

namespace frontend\modules\telegram\commands;

use frontend\modules\telegram\models\State;

class SearchCommand extends AbstractCommand
{
    /**
     * @var int
     */
    public $company_id;

    /**
     * @var int
     */
    public $cashbox_id;

    /**
     * @var
     */
    public $contractor_type;

    /**
     * @var string
     */
    public $search;

    /**
     * @inheritDoc
     */
    public function execute(State $state): bool
    {
        $state->resetAll();
        $state->setValue('command', $this);
        $message = $state->getMessage();

        if (!$message->from->isBot) {
            $this->search = $message->text;
            $state->setValue('search', $this->search);
        }

        $this->exportParams($state, ['company_id', 'cashbox_id', 'contractor_type', 'search']);

        return MenuCommand::createInstance(['menu' => 'contractor'])->execute($state);
    }
}
