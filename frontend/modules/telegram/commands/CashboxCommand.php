<?php

namespace frontend\modules\telegram\commands;

use frontend\modules\telegram\models\State;

class CashboxCommand extends AbstractCommand
{
    /**
     * @var int
     */
    public $cashbox_id;

    /**
     * @var int
     */
    public $company_id;

    /**
     * @inheritDoc
     */
    public function execute(State $state): bool
    {
        $state->resetAll();
        $this->exportParams($state, ['cashbox_id', 'company_id']);

        return MenuCommand::createInstance(['menu' => 'operation'])->execute($state);
    }
}
