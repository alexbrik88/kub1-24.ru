<?php

namespace frontend\modules\telegram\commands;

use common\models\Company;
use frontend\modules\telegram\factories\ApiFactory;
use frontend\modules\telegram\models\Code;
use frontend\modules\telegram\models\Identity;
use frontend\modules\telegram\models\State;
use TgBotApi\BotApiBase\Method\SendMessageMethod;

class CodeCommand extends AbstractCommand
{
    /**
     * @param State $state
     * @return bool
     * @throws
     */
    public function execute(State $state): bool
    {
        $state->resetAll();
        $state->setValue('command', $this);

        Code::deleteExpired();

        if (!$state->getMessage()->from->isBot && $this->handleCode($state)) {
            return true;
        }

        return MenuCommand::createInstance(['menu' => 'code'])->execute($state);
    }

    /**
     * @param State $state
     * @return bool
     */
    public function canAccess(State $state): bool
    {
        return true;
    }

    /**
     * @param State $state
     * @return bool
     * @throws
     */
    private function handleCode(State $state): bool
    {
        $api = ApiFactory::createBot();
        $message = $state->getMessage();

        if (preg_match('/^([0-9]+)$/', $message->text, $matches)) {
            $code = Code::findOne($matches[1]);

            if ($code) {
                Identity::createInstance($state->getTelegramId(), $code->employee_id, $code->company_id);
                $company = Company::findOne($code->company_id);
                $code->delete();
                $api->sendMessage(SendMessageMethod::create($state->getTelegramId(), $this->buildText($company)));

                return MenuCommand::createInstance(['menu' => 'main'])->execute(State::createInstance($message));
            }
        }

        $api->sendMessage(SendMessageMethod::create($state->getTelegramId(), 'Неверный код'));

        return false;
    }

    /**
     * @param Company $company
     * @return string
     */
    private function buildText(Company $company): string
    {
        $format = implode(' ', [
            "Компания %s подключена.\n",
            "Теперь Вы можете вносить операции по кассе и получать уведомления о новом заказе",
            "или счете сделанном через Онлайн Прайс-лист."
        ]);

        return sprintf($format, $company->getShortTitle());
    }
}
