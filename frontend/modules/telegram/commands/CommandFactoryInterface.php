<?php

namespace frontend\modules\telegram\commands;

interface CommandFactoryInterface
{
    /**
     * @param string $name
     * @return CommandInterface
     */
    public function createCommand(string $name): CommandInterface;
}
