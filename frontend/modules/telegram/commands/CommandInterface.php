<?php

namespace frontend\modules\telegram\commands;

use frontend\modules\telegram\models\State;

interface CommandInterface
{
    /**
     * @param State $state
     * @return bool
     */
    public function execute(State $state): bool;

    /**
     * @param mixed[] $params
     * @return mixed
     */
    public static function createInstance(array $params = []);

    /**
     * @param State $state
     * @return bool
     */
    public function canAccess(State $state): bool;
}
