<?php

namespace frontend\modules\telegram\actions;

use TgBotApi\BotApiBase\Type\MessageType;

class CompositeAction implements ActionInterface
{
    /**
     * @var string[]
     */
    private $actions = [
        'command',
        'chat',
        'menu',
    ];

    /**
     * @inheritDoc
     */
    public function execute(MessageType $message, string $data): bool
    {
        $factory = new ActionFactory();

        foreach ($this->actions as $name) {
            $action = $factory->createAction($name);

            if ($action->execute($message, $data)) {
                return true;
            }
        }

        return false;
    }
}
