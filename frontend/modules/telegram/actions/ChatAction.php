<?php

namespace frontend\modules\telegram\actions;

use frontend\modules\telegram\commands\CommandInterface;
use frontend\modules\telegram\models\State;
use TgBotApi\BotApiBase\Type\MessageType;

class ChatAction implements ActionInterface
{
    /**
     * @param MessageType $message
     * @param string $data
     * @return bool
     */
    public function execute(MessageType $message, string $data): bool
    {
        $state = State::createInstance($message);
        $command = $state->getValue('command');

        if ($command instanceof CommandInterface && $command->canAccess($state)) {
            return $command->execute($state);
        }

        return false;
    }
}
