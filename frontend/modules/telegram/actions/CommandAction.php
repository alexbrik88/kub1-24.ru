<?php

namespace frontend\modules\telegram\actions;

use frontend\modules\telegram\commands\CommandInterface;
use frontend\modules\telegram\factories\ApiFactory;
use frontend\modules\telegram\models\State;
use TgBotApi\BotApiBase\Method\EditMessageReplyMarkupMethod;
use TgBotApi\BotApiBase\Type\MessageType;

/**
 * Обрабатывает нажатие кнопки меню
 */
class CommandAction implements ActionInterface
{
    /**
     * @inheritDoc
     * @throws
     */
    public function execute(MessageType $message, string $data): bool
    {
        $state = State::createInstance($message);
        $commands = $state->getValue('commands');

        if (!strlen($data)) {
            return false;
        }

        if (is_array($commands) && isset($commands[$data])) {
            $command = $commands[$data];

            if ($command instanceof CommandInterface && $command->canAccess($state)) {
                return $command->execute($state);
            }
        }

        ApiFactory::createBot()->editMessageReplyMarkup(EditMessageReplyMarkupMethod::create(
            $state->getTelegramId(),
            $message->messageId,
        ));

        return false;
    }
}
