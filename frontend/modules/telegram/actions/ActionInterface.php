<?php

namespace frontend\modules\telegram\actions;

use TgBotApi\BotApiBase\Type\MessageType;

interface ActionInterface
{
    /**
     * @param MessageType $message
     * @param string|null $data
     * @return bool
     */
    public function execute(MessageType $message, string $data): bool;
}
