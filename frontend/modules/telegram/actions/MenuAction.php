<?php

namespace frontend\modules\telegram\actions;

use frontend\modules\telegram\commands\MenuCommand;
use frontend\modules\telegram\models\State;
use TgBotApi\BotApiBase\Type\MessageType;

class MenuAction implements ActionInterface
{
    /** @var string */
    private const DEFAULT_MENU = 'main';

    /**
     * @inheritDoc
     * @throws
     */
    public function execute(MessageType $message, string $data): bool
    {
        $state = State::createInstance($message);
        $command = MenuCommand::createInstance(['menu' => self::DEFAULT_MENU]);

        if ($command->canAccess($state)) {
            $command->execute($state);
        }

        return false;
    }
}
