<?php

namespace frontend\modules\telegram\actions;

interface ActionFactoryInterface
{
    /**
     * @param string $name
     * @return ActionInterface
     */
    public function createAction(string $name): ActionInterface;
}
