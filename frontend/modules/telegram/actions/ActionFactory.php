<?php

namespace frontend\modules\telegram\actions;

use RuntimeException;

class ActionFactory implements ActionFactoryInterface
{
    /**
     * @param string $name
     * @return ActionInterface
     */
    public function createAction(string $name): ActionInterface
    {
        /** @var ActionInterface $class */
        $class = __NAMESPACE__ . '\\' . ucfirst($name) . 'Action';

        if (is_a($class, ActionInterface::class, true)) {
            return new $class();
        }

        throw new RuntimeException('Class not found.');
    }
}
