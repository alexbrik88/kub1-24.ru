<?php

namespace frontend\modules\telegram\factories;

use Http\Adapter\Guzzle6\Client;
use Http\Factory\Guzzle\RequestFactory;
use Http\Factory\Guzzle\StreamFactory;
use TgBotApi\BotApiBase\ApiClient;
use TgBotApi\BotApiBase\BotApiComplete;
use TgBotApi\BotApiBase\BotApiInterface;
use TgBotApi\BotApiBase\BotApiNormalizer;
use Yii;

class ApiFactory
{
    /**
     * @return BotApiComplete
     */
    public static function createBot(): BotApiInterface
    {
        $apiClient = new ApiClient(new RequestFactory, new StreamFactory, new Client);
        $token = Yii::$app->params['telegram']['bot_token'];

        return new BotApiComplete($token, $apiClient, new BotApiNormalizer);
    }
}
