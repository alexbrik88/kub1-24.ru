(function(w) {
    "use strict";

    var timer = {
        interval: null,

        calculateTime: function (value) {
            var seconds = Math.floor(value % 60);
            var minutes = Math.floor(value / 60);

            seconds = (seconds < 10) ? '0' + seconds : seconds;
            minutes = (minutes < 10) ? '0' + minutes : minutes;

            return minutes + ':' + seconds;
        },

        create: function(callback) {
            this.destroy();
            this.interval = w.setInterval(callback, 1000);
        },

        destroy: function() {
            if (this.interval !== null) {
                w.clearInterval(this.interval);
                this.interval = null;
            }
        }
    };
    
    var activation = {
        _isActivated: false,
        _subscribers: {},

        check: function () {
            if (this._isActivated) {
                activation.broadcast();
            } else {
                var ajax = $.ajax('/telegram/activation/check');

                ajax.timeout = 4 * 1000;
                ajax.done(function(data) {
                    if (data === true) {
                        activation._isActivated = true;
                    }

                    activation.broadcast();
                });
            }
        },

        subscribe: function (name, callable) {
            this.unsubscribe(name);
            this._subscribers[name] = callable;
        },

        unsubscribe: function (name) {
            delete this._subscribers[name];
        },

        one: function(name, callable) {
            activation.subscribe(name, function (isActivated) {
                activation.unsubscribe(name);
                callable(isActivated);
            });
        },

        broadcast: function () {
            $.each(this._subscribers, function (name, callable) {
                callable(activation._isActivated);
            });
        }
    };

    w.Telegram = {};

    w.Telegram.Modal = {
        _showCode: function(modal) {
            var ajax = $.ajax('/telegram/activation/code');

            ajax.done(function(data) {
                var time = data.expires_in;

                modal.find('#telegram-code-value').text(data.code);
                modal.find('#telegram-code-timer').text(timer.calculateTime(time));
                modal.modal('show');

                timer.create(function () {
                    time--;

                    if ((time % 5) === 0) {
                        activation.check();
                    }

                    if (time < 0) {
                        timer.destroy();
                        w.Telegram.Modal._showCode(modal);
                    } else {
                        $('#telegram-code-timer').text(timer.calculateTime(time));
                    }
                });
            });
        },

        _showActivated: function(modal) {
            modal.find('.telegram-code').addClass('d-none');
            modal.find('.telegram-activated').removeClass('d-none');
        },

        open: function (hideCode) {
            var modal = $('#telegram-code-modal');

            modal.one('hidden.bs.modal', function () {
                timer.destroy();
            });

            activation.subscribe(modal, function (isActivated) {
                if (isActivated) {
                    timer.destroy();
                    w.Telegram.Modal._showActivated(modal);
                }
            });

            if (hideCode) {
                this._showActivated(modal);
            } else {
                this._showCode(modal);
            }
        },

        close: function () {
            var modal = $('#telegram-code-modal');

            modal.modal('hide');
            timer.destroy();
        }
    };
    
    w.Telegram.CheckBox = {
        onchange: function (element) {
            var checkBox = $(element);

            activation.subscribe(element, function (isActivated) {
                if (isActivated) {
                    checkBox.prop('checked', true);
                    checkBox.parent().addClass('checked');
                }
            });

            if (checkBox.prop('checked')) {
                activation.one(this, function (isActivated) {
                    if (!isActivated) {
                        checkBox.prop('checked', false);
                        checkBox.parent().removeClass('checked');
                        w.Telegram.Modal.open(isActivated);
                    }
                });

                activation.check();
            }

            return true;
        }
    };

    w.Telegram.Button = {
        click: function (element) {
            var button = $(element);

            activation.subscribe(element, function (isActivated) {
                if (isActivated) {
                    button.addClass(button.data('disabled'));
                } else {
                    button.removeClass(button.data('disabled'));
                }
            });

            activation.one(this, function (isActivated) {
                if (!isActivated) {
                    w.Telegram.Modal.open(isActivated);
                }
            });

            activation.check();

            return false;
        }
    };
})(window);
