<?php

namespace frontend\modules\telegram\assets;

use yii\web\AssetBundle;

class CodeAsset extends AssetBundle
{
    /**
     * @inheritDoc
     */
    public $js = [
        'code.js',
    ];

    /**
     * @inheritDoc
     */
    public $sourcePath = __DIR__ . '/dist';

    /**
     * @inheritDoc
     */
    public $depends = [
        'yii\bootstrap4\BootstrapPluginAsset',
    ];
}
