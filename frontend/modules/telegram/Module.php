<?php

namespace frontend\modules\telegram;

use yii\base\Module as BaseModule;

class Module extends BaseModule
{
    /**
     * @inheritDoc
     */
    public $controllerNamespace = 'frontend\modules\telegram\controllers';
}
