<?php

namespace frontend\modules\telegram\models;

use common\models\Company;
use common\models\employee\EmployeeRole;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

class CompanyRepository extends Model implements RepositoryInterface
{
    use RepositoryAttributesTrait;

    /** @var int */
    private const PAGE_SIZE = 10;

    /** @var int */
    public $telegram_id;

    /**
     * @var mixed[] Фильтр получателей
     */
    public $employeeFilter = [
        'is_working' => true,
        'employee_role_id' => EmployeeRole::ROLE_CHIEF,
    ];

    /**
     * @inheritDoc
     */
    public function getQuery(): ActiveQuery
    {
        $company_id = Identity::find()
            ->select([Identity::tableName() . '.company_id'])
            ->innerJoinWith(['employeeCompany' => function (ActiveQuery $query) {
                $query->andWhere($this->employeeFilter);
            }], false)
            ->andWhere(['telegram_id' => $this->telegram_id])
            ->groupBy([Identity::tableName() . '.company_id'])
            ->column();

        return Company::find()
            ->andWhere(['id' => $company_id])
            ->orderBy(['name_short' => SORT_ASC]);
    }

    /**
     * @inheritDoc
     */
    public function getProvider(): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $this->getQuery(),
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);
    }
}
