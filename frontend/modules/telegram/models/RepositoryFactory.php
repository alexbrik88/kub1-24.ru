<?php

namespace frontend\modules\telegram\models;

use RuntimeException;

class RepositoryFactory implements RepositoryFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function createRepository(string $class, array $attributes): RepositoryInterface
    {
        if (!is_a($class, RepositoryInterface::class, true)) {
            throw new RuntimeException('Class not found.');
        }

        /** @var RepositoryInterface $repository */
        $repository = new $class();

        array_walk($attributes, function ($value, string $name) use ($repository) {
            $repository->setAttribute($name, $value);
        });

        return $repository;
    }
}
