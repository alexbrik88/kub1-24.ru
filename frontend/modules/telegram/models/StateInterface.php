<?php

namespace frontend\modules\telegram\models;

use TgBotApi\BotApiBase\Type\MessageType;

interface StateInterface
{
    /**
     * @param string $key
     * @return mixed|null
     */
    public function getValue(string $key);

    /**
     * @return void
     */
    public function resetAll(): void;

    /**
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function setValue(string $key, $value): void;

    /**
     * @param MessageType $message
     * @return StateInterface
     */
    public static function createInstance(MessageType $message): self;

    /**
     * Получить идентификатор пользователя telegram
     *
     * @return int
     */
    public function getTelegramId(): int;

    /**
     * @return Identity|null
     */
    public function getIdentity(): ?Identity;

    /**
     * @return MessageType
     */
    public function getMessage(): MessageType;
}
