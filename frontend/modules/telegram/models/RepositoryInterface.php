<?php

namespace frontend\modules\telegram\models;

use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

interface RepositoryInterface
{
    /**
     * @param string $name
     * @return mixed
     */
    public function getAttribute(string $name);

    /**
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function setAttribute(string $name, $value): void;

    /**
     * @return ActiveDataProvider
     */
    public function getProvider(): ActiveDataProvider;

    /**
     * @return ActiveQuery
     */
    public function getQuery(): ActiveQuery;
}
