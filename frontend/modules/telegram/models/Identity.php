<?php

namespace frontend\modules\telegram\models;

use common\models\Company;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use Yii;

/**
 * @property-read int $telegram_id Идентификатор пользователя телеграм
 * @property-read int $employee_id Идентификатор сотрудника
 * @property-read int $company_id Идентификатор компании
 * @property int $notification_count Счётчик уведомлений
 * @property-read Company $company
 * @property-read EmployeeCompany $employeeCompany
 */
class Identity extends ActiveRecord
{
    /**
     * Создать новую привязку телеграм-аккаунта
     *
     * @param int $telegram_id Идентификатор пользователя телеграм
     * @param int $employee_id Идентификатор сотрудника
     * @param int $company_id Идентификатор компании
     * @return Identity
     */
    public static function createInstance(int $telegram_id, int $employee_id, int $company_id): self
    {
        $params = compact('telegram_id', 'employee_id', 'company_id');
        $model = static::findOne($params);

        if (!$model) {
            $model = new static($params);
            $model->save();
        }

        return $model;
    }

    /**
     * Получить привязку телеграм-аккаунта для текущего пользователя
     *
     * @return static|null
     */
    public static function getCurrent(): ?self
    {
        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;

        return static::findOne(['employee_id' => $employee->id, 'company_id' => $employee->company->id]);
    }

    /**
     * @return ActiveQuery
     */
    public function getCompany(): ActiveQuery
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEmployeeCompany(): ActiveQuery
    {
        return $this->hasOne(EmployeeCompany::class, ['employee_id' => 'employee_id', 'company_id' => 'company_id']);
    }

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%telegram_identity}}';
    }
}
