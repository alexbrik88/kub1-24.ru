<?php

namespace frontend\modules\telegram\models;

trait RepositoryAttributesTrait
{
    /**
     * @inheritDoc
     */
    public function getAttribute(string $name)
    {
        return $this->getAttributes([$name])[$name];
    }

    /**
     * @inheritDoc
     */
    public function setAttribute(string $name, $value): void
    {
        $this->setAttributes([$name => $value], false);
    }
}
