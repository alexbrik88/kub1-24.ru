<?php

namespace frontend\modules\telegram\models;

use common\models\cash\Cashbox;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

class CashboxRepository extends Model implements RepositoryInterface
{
    use RepositoryAttributesTrait;

    /**
     * @var int
     */
    public $company_id;

    /**
     * @var int
     */
    public $cashbox_id;

    /**
     * @inheritDoc
     */
    public function getQuery(): ActiveQuery
    {
        $query = Cashbox::find()->andFilterWhere([
            'id' => $this->cashbox_id,
            'company_id' => $this->company_id,
        ]);

        return $query;
    }

    /**
     * @inheritDoc
     */
    public function getProvider(): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $this->getQuery(),
            'pagination' => false,
        ]);
    }
}
