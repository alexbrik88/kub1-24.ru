<?php

namespace frontend\modules\telegram\models;

use TgBotApi\BotApiBase\Type\MessageType;
use Yii;
use yii\caching\CacheInterface;

class State implements StateInterface
{
    /**
     * @var MessageType
     */
    private $message;

    /**
     * @var mixed[]
     */
    private $data;

    /**
     * @var string
     */
    private $cacheKey;

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var Identity|null
     */
    private $identity;

    /**
     * @param MessageType $message
     */
    private function __construct(MessageType $message)
    {
        $this->message = $message;
        $this->cache = Yii::$app->cache;
        $this->cacheKey = 'telegram_state_' . $this->getTelegramId();

        if ($this->cache->exists($this->cacheKey)) {
            $this->data = $this->cache->get($this->cacheKey);
        } else {
            $this->resetAll();
        }

        $this->identity = Identity::findOne(['telegram_id' => $this->getTelegramId()]);
    }

    /**
     * @inheritDoc
     */
    public function getValue(string $key)
    {
        return array_key_exists($key, $this->data) ? $this->data[$key] : null;
    }

    /**
     * @inheritDoc
     */
    public function setValue(string $key, $value): void
    {
        $this->data[$key] = $value;
        $this->cache->set($this->cacheKey, $this->data);
    }

    /**
     * @inheritDoc
     */
    public function resetAll(): void
    {
        $message_id= $this->data['message_id'] ?? null;
        $this->data = compact('message_id');
        $this->cache->set($this->cacheKey, $this->data);
    }

    /**
     * @inheritDoc
     * @return static
     */
    public static function createInstance(MessageType $message): StateInterface
    {
        return new static($message);
    }

    /**
     * @return int
     */
    public function getTelegramId(): int
    {
        return $this->message->chat->id;
    }

    /**
     * @inheritDoc
     */
    public function getIdentity(): ?Identity
    {
        return $this->identity;
    }

    /**
     * @inheritDoc
     */
    public function getMessage(): MessageType
    {
        return $this->message;
    }
}
