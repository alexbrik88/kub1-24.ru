<?php

namespace frontend\modules\telegram\models;

use common\models\Contractor;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

class ContractorRepository extends Model implements RepositoryInterface
{
    use RepositoryAttributesTrait;

    /** @var int */
    private const PAGE_SIZE = 10;

    /**
     * @var int|null
     */
    public $company_id;

    /**
     * @var int|null
     */
    public $contractor_type;

    /**
     * @var int|null
     */
    public $contractor_id;

    /**
     * @var string
     */
    public $search;

    /**
     * @inheritDoc
     */
    public function getQuery(): ActiveQuery
    {
        $query = Contractor::find()->andFilterWhere([
            'company_id' => $this->company_id,
            'type' => $this->contractor_type,
            'id' => $this->contractor_id,
        ]);

        if (strlen($this->search)) {
            $query->andWhere(['like', 'name', $this->search]);
        }

        return $query;
    }

    /**
     * @inheritDoc
     */
    public function getProvider(): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $this->getQuery(),
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);
    }
}
