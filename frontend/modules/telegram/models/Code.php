<?php

namespace frontend\modules\telegram\models;

use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * @property-read string $code Значение кода
 * @property-read int $employee_id Идентификатор сотрудника
 * @property-read int $company_id Идентификатор компании
 * @property-read string $expired_at Дата истечения кода
 * @property-read integer $expiresIn
 */
class Code extends ActiveRecord
{
    /** @var int Длина кода */
    public const CODE_LENGTH = 4;

    /** @var int Время жизни в секундах */
    public const LIFE_TIME = 1200;

    /**
     * @inheritDoc
     */
    public function fields()
    {
        $fields = parent::fields();
        $fields['expires_in'] = 'expiresIn';

        return $fields;
    }

    /**
     * Создать новый экземпляр для текщего пользователя
     *
     * @param int $employee_id
     * @param int $company_id
     * @return static
     */
    public static function createInstance(int $employee_id, int $company_id): self
    {
        self::deleteExpired();
        $model = static::findOne(compact('employee_id', 'company_id'));

        if (!$model) {
            $code = self::generateCode();
            $expired_at = new Expression(sprintf('DATE_ADD(NOW(), INTERVAL %s SECOND)', self::LIFE_TIME));
            $model = new static(compact('code', 'employee_id', 'company_id', 'expired_at'));
            $model->save();
            $model->refresh();
        }

        return $model;
    }

    /**
     * Удалить просроченные коды
     *
     * @return void
     */
    public static function deleteExpired(): void
    {
        static::deleteAll(['<', 'expired_at', new Expression('NOW()')]);
    }

    /**
     * @return int
     */
    public function getExpiresIn(): int
    {
        return strtotime($this->expired_at) - time();
    }

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%telegram_code}}';
    }

    /**
     * Генерировать код
     *
     * @return string
     */
    private static function generateCode(): string
    {
        $random = rand(1, pow(10, self::CODE_LENGTH) - 1);
        $code = str_pad($random, self::CODE_LENGTH, '0', STR_PAD_LEFT);

        if (static::findOne($code)) { // Код уже занят
            return self::generateCode();
        }

        return $code;
    }
}
