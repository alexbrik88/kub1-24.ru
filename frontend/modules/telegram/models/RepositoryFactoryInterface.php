<?php

namespace frontend\modules\telegram\models;

interface RepositoryFactoryInterface
{
    /**
     * @param string $class
     * @param mixed[] $attributes
     * @return RepositoryInterface
     */
    public function createRepository(string $class, array $attributes): RepositoryInterface;
}
