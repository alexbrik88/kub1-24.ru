<?php

namespace frontend\modules\telegram\models;

use common\components\date\DateHelper;
use common\models\cash\Cashbox;
use common\models\cash\CashOrderFlows;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;

class OrderFactory
{
    /** @var int */
    private const TYPE_LIST = [
        Contractor::TYPE_CUSTOMER => CashOrderFlows::FLOW_TYPE_INCOME,
        Contractor::TYPE_SELLER => CashOrderFlows::FLOW_TYPE_EXPENSE,
    ];

    /**
     * @param Identity $identity
     * @param int $cashbox_id
     * @param int $contractor_id
     * @param int $amount
     * @return CashOrderFlows
     */
    public function createOrder(Identity $identity, int $cashbox_id, int $contractor_id, int $amount): CashOrderFlows
    {
        $cashbox = $this->getCashbox($cashbox_id, $identity);
        $contractor = $this->getContractor($contractor_id, $identity);
        $contractor_type = $contractor->getAttribute('type');
        $incomeItem = $this->getIncomeItem($contractor_type);
        $expenditureItem = $this->getExpenditureItem($contractor_type);

        return new CashOrderFlows([
            'date' => date(DateHelper::FORMAT_DATE),
            'author_id' => $identity->employee_id,
            'company_id' => $identity->company_id,
            'cashbox_id' => $cashbox->getAttribute('id'),
            'number' => CashOrderFlows::getNextNumber($identity->company_id, self::TYPE_LIST[$contractor_type]),
            'flow_type' => self::TYPE_LIST[$contractor_type],
            'is_accounting' => $cashbox->getAttribute('is_accounting'),
            'contractor_id' => $contractor_id,
            'amount' => $amount,
            'income_item_id' => $incomeItem ? $incomeItem->getAttribute('id') : null,
            'expenditure_item_id' => $expenditureItem ? $expenditureItem->getAttribute('id') : null,
            'is_taxable' => true,
            'description' => $incomeItem ? $incomeItem->getAttribute('name') : $expenditureItem->getAttribute('name'),
        ]);
    }

    /**
     * @param int $contractor_type
     * @return InvoiceIncomeItem|null
     */
    private function getIncomeItem(int $contractor_type): ?InvoiceIncomeItem
    {
        if ($contractor_type == Contractor::TYPE_CUSTOMER) {
            return InvoiceIncomeItem::findOne(['id' => InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER]);
        }

        return null;
    }

    /**
     * @param int $contractor_type
     * @return InvoiceExpenditureItem|null
     */
    private function getExpenditureItem(int $contractor_type): ?InvoiceExpenditureItem
    {
        if ($contractor_type == Contractor::TYPE_SELLER) {
            return InvoiceExpenditureItem::findOne(['id' => InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT]);
        }

        return null;
    }

    /**
     * @param int $cashbox_id
     * @param Identity $identity
     * @return Cashbox
     */
    private function getCashbox(int $cashbox_id, Identity $identity): Cashbox
    {
        $repository = (new RepositoryFactory)->createRepository(CashboxRepository::class, [
            'company_id' => $identity->company_id,
            'cashbox_id' => $cashbox_id,
        ]);

        /** @var Cashbox $cashbox */
        $cashbox = $repository->getQuery()->one();

        return $cashbox;
    }

    /**
     * @param int $contractor_id
     * @param Identity $identity
     * @return Contractor
     */
    private function getContractor(int $contractor_id, Identity $identity): Contractor
    {
        $repository = (new RepositoryFactory)->createRepository(ContractorRepository::class, [
            'company_id' => $identity->company_id,
            'contractor_id' => $contractor_id,
        ]);

        /** @var Contractor $contractor */
        $contractor = $repository->getQuery()->one();

        return $contractor;
    }
}
