<?php

namespace frontend\modules\telegram\components;

use frontend\modules\telegram\models\Identity;

interface NotificationInterface
{
    /**
     * Уведомление включено
     *
     * @return bool
     */
    public function isEnabled(): bool;

    /**
     * Получить идентификатор компании
     *
     * @return int
     */
    public function getCompanyId() : int;

    /**
     * Получить сообщение уведомления
     *
     * @return string
     */
    public function getMessage() : string;

    /**
     * @param Identity $identity Получатель
     * @return bool
     */
    public function onDispatch(Identity $identity): bool;
}
