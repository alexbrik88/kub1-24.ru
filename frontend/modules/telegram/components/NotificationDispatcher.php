<?php

namespace frontend\modules\telegram\components;

use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use frontend\modules\telegram\jobs\SendNotificationJob;
use frontend\modules\telegram\models\Identity;
use yii\base\Component;

class NotificationDispatcher extends Component implements NotificationDispatcherInterface
{
    /**
     * @var mixed[] Фильтр получателей
     */
    public $employeeFilter = [
        'is_working' => true,
        'employee_role_id' => EmployeeRole::ROLE_CHIEF,
    ];

    /**
     * @param NotificationInterface $notification
     * @param int $pushDelay
     * @return void
     * @inheritDoc
     */
    public function sendNotification(NotificationInterface $notification, int $pushDelay = 0): void
    {
        if ($notification->isEnabled()) {
            $text = $notification->getMessage();
            $identities = $this->getIdentities($notification->getCompanyId());

            array_walk($identities, function (Identity $identity) use ($text, $notification, $pushDelay) {
                $identity->notification_count++;

                if ($notification->onDispatch($identity)) {
                    SendNotificationJob::dispatchJob($identity, $text, $pushDelay);
                    $identity->save();
                }
            });
        }
    }

    /**
     * Получить список получателей
     *
     * @param int $company_id Идентификатор компании
     * @return Identity[]
     */
    private function getIdentities(int $company_id): array
    {
        $filter = ['company_id' => $company_id] + $this->employeeFilter;
        $employee_id = EmployeeCompany::find()->where($filter)->select('employee_id')->column();

        if (empty($employee_id)) {
            return [];
        }

        return Identity::find()
            ->where(['company_id' => $company_id, 'employee_id' => $employee_id])
            ->with(['company'])
            ->groupBy(['telegram_id'])
            ->all();
    }
}
