<?php

namespace frontend\modules\telegram\components;

interface NotificationDispatcherInterface
{
    /**
     * Отправить уведомление
     *
     * @param NotificationInterface $notification
     * @param int $pushDelay
     * @param void
     */
    public function sendNotification(NotificationInterface $notification, int $pushDelay = 0): void;
}
