<?php

namespace frontend\modules\telegram\behaviors;

use frontend\modules\telegram\components\NotificationDispatcher;
use frontend\modules\telegram\components\NotificationDispatcherInterface;
use yii\base\Behavior;
use yii\base\InvalidConfigException;
use Yii;

/**
 * @property-read NotificationFactoryInterface $owner
 */
class NotificationBehavior extends Behavior
{
    /**
     * @var string[] Список событий
     */
    public $events = [];

    /**
     * @var mixed[] Настройки диспетчера уведомлений
     */
    public $dispatcherOptions = ['class' => NotificationDispatcher::class];

    /**
     * @var NotificationDispatcherInterface
     */
    private $dispatcher;

    /**
     * @inheritDoc
     */
    public function events()
    {
        return array_fill_keys($this->events, [$this, 'handleEvent']);
    }

    /**
     * Обработать событие
     *
     * @throws InvalidConfigException
     */
    public function handleEvent(): void
    {
        if (!($this->owner instanceof NotificationFactoryInterface)) {
            throw new InvalidConfigException();
        }

        $this->getDispatcher()->sendNotification($this->owner->createNotification());
    }

    /**
     * @throws InvalidConfigException
     * @return NotificationDispatcherInterface
     */
    private function getDispatcher(): NotificationDispatcherInterface
    {
        if ($this->dispatcher === null) {
            $this->dispatcher = Yii::createObject($this->dispatcherOptions);
        }

        return $this->dispatcher;
    }
}
