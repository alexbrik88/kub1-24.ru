<?php

namespace frontend\modules\telegram\behaviors;

use frontend\modules\telegram\components\NotificationInterface;

interface NotificationFactoryInterface
{
    /**
     * Создать уведомление
     *
     * @return NotificationInterface
     */
    public function createNotification(): NotificationInterface;
}
