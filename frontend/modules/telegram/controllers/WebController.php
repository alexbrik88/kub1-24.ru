<?php

namespace frontend\modules\telegram\controllers;

use frontend\modules\telegram\actions\CompositeAction;
use TgBotApi\BotApiBase\BotApiNormalizer;
use TgBotApi\BotApiBase\Exception\BadRequestException;
use TgBotApi\BotApiBase\Type\CallbackQueryType;
use TgBotApi\BotApiBase\Type\MessageType;
use TgBotApi\BotApiBase\WebhookFetcher;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Request;
use yii\web\UnauthorizedHttpException;

class WebController extends Controller
{
    /**
     * @inheritDoc
     */
    public $enableCsrfValidation = false;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var mixed[]
     */
    private $params;

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'verbFilter' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'hook' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->request = Yii::$app->request;
        $this->params = Yii::$app->params['telegram'];
    }

    /**
     * Получение сообытий от Telegram-серверов
     *
     * @see https://core.telegram.org/bots/webhooks
     * @return void
     * @throws UnauthorizedHttpException
     * @throws BadRequestException
     */
    public function actionHook(): void
    {
        if ($this->request->get('secret') !== $this->params['web_hook_secret']) {
            throw new UnauthorizedHttpException();
        }

        if (empty($this->request->getRawBody())) {
            return;
        }

        $fetcher = new WebhookFetcher(new BotApiNormalizer);
        $update = $fetcher->fetch($this->request->getRawBody());

        if ($update->callbackQuery instanceof CallbackQueryType) {
            $this->handleMessage($update->callbackQuery->message, $update->callbackQuery->data);

            return;
        }

        if ($update->message instanceof MessageType) {
            $this->handleMessage($update->message, '');
        }
    }

    /**
     * Обработать сообщение
     *
     * @param MessageType $message Экземпляр сообщения
     * @param string $data
     * @return void
     */
    private static function handleMessage(MessageType $message, string $data): void
    {
        $action = new CompositeAction();
        $action->execute($message, $data);
    }
}
