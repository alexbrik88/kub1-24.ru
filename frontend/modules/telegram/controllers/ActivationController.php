<?php

namespace frontend\modules\telegram\controllers;

use common\components\filters\AccessControl;
use common\models\employee\Employee;
use frontend\modules\telegram\models\Code;
use frontend\modules\telegram\models\Identity;
use frontend\rbac\UserRole;
use Yii;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;

class ActivationController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['accessControl'] = [
            'class' => AccessControl::class,
            'denyCallback' => [$this, 'throwHttpException'],
            'rules' => [
                [
                    'allow' => true,
                    'roles' => [
                        UserRole::ROLE_CHIEF,
                    ],
                ],
            ],
        ];

        return $behaviors;
    }

    /**
     * Генерировать пароль
     *
     * @return string[]
     * @throws ForbiddenHttpException
     */
    public function actionCode(): array
    {
        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;

        if (Identity::getCurrent()) {
            $this->throwHttpException();
        }

        return Code::createInstance($employee->id, $employee->company->id)->toArray();
    }

    /**
     * Проверить статус активация
     *
     * @return bool
     */
    public function actionCheck(): bool
    {
        return Identity::getCurrent() ? true : false;
    }

    /**
     * @throws ForbiddenHttpException
     */
    public function throwHttpException(): void
    {
        throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
    }
}
