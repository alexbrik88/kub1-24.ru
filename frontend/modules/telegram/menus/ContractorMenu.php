<?php

namespace frontend\modules\telegram\menus;

use common\models\Contractor;
use frontend\modules\telegram\commands\ContractorCommand;
use frontend\modules\telegram\commands\PageCommand;
use frontend\modules\telegram\models\ContractorRepository;
use frontend\modules\telegram\models\RepositoryFactory;
use frontend\modules\telegram\models\State;
use TgBotApi\BotApiBase\Type\InlineKeyboardMarkupType;
use yii\data\ActiveDataProvider;

class ContractorMenu extends AbstractMenu
{
    use PageMenuTrait;

    /**
     * @var ActiveDataProvider
     */
    private $provider;

    /**
     * @var string
     */
    private $search;

    /**
     * @param State $state
     */
    public function __construct(State $state)
    {
        parent::__construct($state);

        $this->search = $this->getState()->getValue('search');
        $repository = (new RepositoryFactory)->createRepository(ContractorRepository::class, [
            'company_id' => $this->getState()->getValue('company_id'),
            'contractor_type' => $this->getState()->getValue('contractor_type'),
            'search' => $this->search,
        ]);
        $this->provider = $repository->getProvider();
        $this->provider->pagination->page = $this->getState()->getValue('page') ?: 0;
        $this->provider->prepare();
    }

    /**
     * @inheritDoc
     */
    public function getKeyboard(): InlineKeyboardMarkupType
    {
        if (strlen($this->search) && $this->getProvider()->getTotalCount() > 0) {
            $this->addResultButtons();
        } else {
            $this->builder->addCancelButton();
        }

        return $this->builder->buildKeyboard();
    }

    /**
     * @return void
     */
    private function addResultButtons(): void
    {
        /** @var Contractor $contractor */
        foreach ($this->getProvider()->getModels() as $i => $contractor) {
            $row = floor($i / 2);
            $this->builder->addButton($row, $contractor->getNameWithType(), ContractorCommand::createInstance([
                'company_id' => $this->getState()->getValue('company_id'),
                'cashbox_id' => $this->getState()->getValue('cashbox_id'),
                'contractor_id' => $contractor->getAttribute('id'),
            ]));
        }

        $this->builder->addButton(PHP_INT_MAX, '«', PageCommand::createInstance([
            'menu' => 'contractor',
            'page' => $this->getPrevPage(),
        ]));

        $this->builder->addCancelButton(PHP_INT_MAX);

        $this->builder->addButton(PHP_INT_MAX, '»', PageCommand::createInstance([
            'menu' => 'contractor',
            'page' => $this->getNextPage(),
        ]));
    }

    /**
     * @inheritDoc
     */
    public function getText(): string
    {
        if (strlen($this->search) < 1) {
            return ($this->getState()->getValue('contractor_type') == Contractor::TYPE_SELLER)
                ? 'Введите название поставщика'
                : 'Введите название покупателя';
        }

        if (strlen($this->search) && $this->getProvider()->getTotalCount() > 0) {
            return ($this->getState()->getValue('contractor_type') == Contractor::TYPE_SELLER)
                ? 'Выберите поставщика из списка'
                : 'Выберите покупателя из списка';
        }

        return "По запросу «{$this->search}»\nничего не найдено";
    }

    /**
     * @return ActiveDataProvider
     */
    public function getProvider(): ActiveDataProvider
    {
        return $this->provider;
    }
}
