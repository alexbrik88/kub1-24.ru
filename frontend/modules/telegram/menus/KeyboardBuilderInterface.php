<?php

namespace frontend\modules\telegram\menus;

use frontend\modules\telegram\commands\CommandInterface;
use TgBotApi\BotApiBase\Type\InlineKeyboardMarkupType;

interface KeyboardBuilderInterface
{
    /**
     * @param int $row
     * @param string $text
     * @param CommandInterface $command
     */
    public function addButton(int $row, string $text, CommandInterface $command): void;

    /**
     * @return InlineKeyboardMarkupType
     */
    public function buildKeyboard(): InlineKeyboardMarkupType;

    /**
     * @return CommandInterface[]
     */
    public function getCommands(): array;
}
