<?php

namespace frontend\modules\telegram\menus;

use common\models\Company;
use frontend\modules\telegram\commands\CompanyCommand;
use frontend\modules\telegram\commands\PageCommand;
use frontend\modules\telegram\models\CompanyRepository;
use frontend\modules\telegram\models\RepositoryFactory;
use frontend\modules\telegram\models\State;
use TgBotApi\BotApiBase\Type\InlineKeyboardMarkupType;
use yii\data\ActiveDataProvider;

class CompanyMenu extends AbstractMenu
{
    use PageMenuTrait;

    /**
     * @var ActiveDataProvider
     */
    private $provider;

    /**
     * @param State $state
     */
    public function __construct(State $state)
    {
        parent::__construct($state);

        $repository = (new RepositoryFactory)->createRepository(CompanyRepository::class, [
            'telegram_id' => $this->getState()->getTelegramId(),
        ]);

        $this->provider = $repository->getProvider();
        $this->provider->pagination->page = $this->getState()->getValue('page') ?: 0;
        $this->provider->prepare();
    }

    /**
     * @inheritDoc
     */
    public function getKeyboard(): InlineKeyboardMarkupType
    {
        /** @var Company $company */
        foreach ($this->provider->getModels() as $i => $company) {
            $row = floor($i / 2);
            $this->builder->addButton($row, $company->getShortName(), CompanyCommand::createInstance([
                'company_id' => $company->id,
            ]));
        }

        $this->builder->addButton(PHP_INT_MAX, '«', PageCommand::createInstance([
            'menu' => 'company',
            'page' => $this->getPrevPage(),
        ]));

        $this->builder->addCancelButton(PHP_INT_MAX);

        $this->builder->addButton(PHP_INT_MAX, '»', PageCommand::createInstance([
            'menu' => 'company',
            'page' => $this->getNextPage(),
        ]));

        return $this->builder->buildKeyboard();
    }

    /**
     * @inheritDoc
     */
    public function getText(): string
    {
        return 'Выберите компанию, в которую записать операцию:';
    }

    /**
     * @return ActiveDataProvider
     */
    public function getProvider(): ActiveDataProvider
    {
        return $this->provider;
    }
}
