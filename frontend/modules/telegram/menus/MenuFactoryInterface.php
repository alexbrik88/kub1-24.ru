<?php

namespace frontend\modules\telegram\menus;

use frontend\modules\telegram\models\State;
use RuntimeException;

interface MenuFactoryInterface
{
    /**
     * @param string $name
     * @param State $state
     * @return MenuInterface
     * @throws RuntimeException
     */
    public function createMenu(string $name, State $state): MenuInterface;
}
