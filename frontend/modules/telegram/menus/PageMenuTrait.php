<?php

namespace frontend\modules\telegram\menus;

use yii\data\ActiveDataProvider;

trait PageMenuTrait
{
    /**
     * @return int
     */
    protected function getPrevPage(): int
    {
        $page = $this->getProvider()->getPagination()->getPage() - 1;

        if ($page < 0) {
            return 0;
        }

        return $page;
    }

    /**
     * @return int
     */
    protected function getNextPage(): int
    {
        $page = $this->getProvider()->getPagination()->getPage() + 1;
        $max = $this->getProvider()->getPagination()->getPageCount() - 1;

        if ($page > $max) {
            $page = $max;
        }

        return $page;
    }

    /**
     * @return ActiveDataProvider
     */
    abstract public function getProvider(): ActiveDataProvider;
}
