<?php

namespace frontend\modules\telegram\menus;

use TgBotApi\BotApiBase\Type\InlineKeyboardMarkupType;

class CodeMenu extends AbstractMenu
{
    /**
     * @inheritDoc
     */
    public function getKeyboard(): InlineKeyboardMarkupType
    {
        $this->builder->addCancelButton();

        return $this->builder->buildKeyboard();
    }

    /**
     * @inheritDoc
     */
    public function getText(): string
    {
        return 'Введите КОД полученный в личной кабинете КУБ-24:';
    }
}
