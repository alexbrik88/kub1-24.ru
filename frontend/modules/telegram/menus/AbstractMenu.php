<?php

namespace frontend\modules\telegram\menus;

use frontend\modules\telegram\models\Identity;
use frontend\modules\telegram\models\State;

abstract class AbstractMenu implements MenuInterface
{
    /** @var int */
    protected const DEFAULT_ROW = 0;

    /**
     * @var State
     */
    private $state;

    /**
     * @var KeyboardBuilder
     */
    protected $builder;

    /**
     * @param State $state
     */
    public function __construct(State $state)
    {
        $this->state = $state;
        $this->builder = new KeyboardBuilder();
    }

    /**
     * @return State
     */
    public function getState(): State
    {
        return $this->state;
    }

    /**
     * @inheritDoc
     */
    public function getCommands(): array
    {
        return $this->builder->getCommands();
    }
}
