<?php

namespace frontend\modules\telegram\menus;

use TgBotApi\BotApiBase\Type\InlineKeyboardMarkupType;

class AmountMenu extends AbstractMenu
{
    /**
     * @inheritDoc
     */
    public function getKeyboard(): InlineKeyboardMarkupType
    {
        $this->builder->addCancelButton();

        return $this->builder->buildKeyboard();
    }

    /**
     * @inheritDoc
     */
    public function getText(): string
    {
        return 'Введите сумму';
    }
}
