<?php

namespace frontend\modules\telegram\menus;

use frontend\modules\telegram\models\State;
use TgBotApi\BotApiBase\Exception\BadArgumentException;
use TgBotApi\BotApiBase\Type\InlineKeyboardMarkupType;

interface MenuInterface
{
    /**
     * @return string
     */
    public function getText(): string;

    /**
     * @return InlineKeyboardMarkupType
     * @throws BadArgumentException
     */
    public function getKeyboard(): InlineKeyboardMarkupType;

    /**
     * @return string[]
     */
    public function getCommands(): array;

    /**
     * @return State
     */
    public function getState(): State;
}
