<?php

namespace frontend\modules\telegram\menus;

use frontend\modules\telegram\commands\CodeCommand;
use frontend\modules\telegram\commands\SwitchCommand;
use TgBotApi\BotApiBase\Type\InlineKeyboardMarkupType;

class MainMenu extends AbstractMenu
{
    /**
     * @inheritDoc
     */
    public function getKeyboard(): InlineKeyboardMarkupType
    {
        if ($this->getState()->getIdentity()) {
            $this->builder->addButton(self::DEFAULT_ROW, 'Операция по кассе', SwitchCommand::createInstance());
        }

        $this->builder->addButton(self::DEFAULT_ROW, 'Ввести код', CodeCommand::createInstance());

        return $this->builder->buildKeyboard();
    }

    /**
     * @inheritDoc
     */
    public function getText(): string
    {
        if ($this->getState()->getIdentity()) {
            return implode(' ', [
                "Чтобы записать наличные по кассе, нажмите по кнопке «Операция по кассе».",
                "Добавленные операции мгновенно появятся в разделе Деньги-Касса в Вашем личном кабинете КУБ-24.\n",
                "Если вы хотите подключить еще одну компанию к Telegram, нажмите по кнопке «Ввести код».",
            ]);
        }

        return 'Чтобы подключить компанию к Telegram, нажмите по кнопке «Ввести код».';
    }
}
