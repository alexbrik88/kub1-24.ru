<?php

namespace frontend\modules\telegram\menus;

use frontend\modules\telegram\models\State;
use RuntimeException;

class MenuFactory implements MenuFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function createMenu(string $name, State $state): MenuInterface
    {
        $class = __NAMESPACE__ . '\\' . ucfirst($name) . 'Menu';

        if (is_a($class, MenuInterface::class, true)) {
            return new $class($state);
        }

        throw new RuntimeException('Class not found.');
    }
}
