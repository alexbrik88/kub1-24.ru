<?php

namespace frontend\modules\telegram\menus;

use common\models\cash\Cashbox;
use frontend\modules\telegram\commands\CashboxCommand;
use frontend\modules\telegram\models\CashboxRepository;
use frontend\modules\telegram\models\RepositoryFactory;
use TgBotApi\BotApiBase\Type\InlineKeyboardMarkupType;

class CashboxMenu extends AbstractMenu
{
    /**
     * @inheritDoc
     */
    public function getKeyboard(): InlineKeyboardMarkupType
    {
        $company_id = $this->getState()->getValue('company_id');
        $provider = (new RepositoryFactory)
            ->createRepository(CashboxRepository::class, ['company_id' => $company_id])
            ->getProvider();

        /**
         * @var Cashbox $cashbox
         */
        foreach ($provider->getModels() as $i => $cashbox) {
            $this->builder->addButton($i, $cashbox->name, CashboxCommand::createInstance([
                'company_id' => $company_id,
                'cashbox_id' => $cashbox->id,
            ]));
        }

        $this->builder->addCancelButton();

        return $this->builder->buildKeyboard();
    }

    /**
     * @inheritDoc
     */
    public function getText(): string
    {
        return 'Выберите кассу, в которую записать операцию';
    }
}
