<?php

namespace frontend\modules\telegram\menus;

use frontend\modules\telegram\commands\CancelCommand;
use frontend\modules\telegram\commands\CommandInterface;
use TgBotApi\BotApiBase\Type\InlineKeyboardButtonType;
use TgBotApi\BotApiBase\Type\InlineKeyboardMarkupType;

class KeyboardBuilder implements KeyboardBuilderInterface
{
    /** @var string */
    public const CANCEL_TEXT = 'Отмена';

    /**
     * @var InlineKeyboardButtonType[][]
     */
    private $buttons = [];

    /**
     * @var CommandInterface[]
     */
    private $commands = [];

    /**
     * @inheritDoc
     * @throws
     */
    public function addButton(int $row, string $text, CommandInterface $command): void
    {
        usleep(100);
        $id = (string) microtime(true);

        if (!isset($this->buttons[$row])) {
            $this->buttons[$row] = [];
        }

        $this->commands[$id] = $command;
        $this->buttons[$row][] = InlineKeyboardButtonType::create($text, ['callbackData' => $id]);
    }

    /**
     * @param int $row
     * @param string $text
     * @return void
     */
    public function addCancelButton(int $row = PHP_INT_MAX, string $text = self::CANCEL_TEXT): void
    {
        $this->addButton($row, $text, CancelCommand::createInstance());
    }

    /**
     * @inheritDoc
     */
    public function buildKeyboard(): InlineKeyboardMarkupType
    {
        ksort($this->buttons);
        $buttons = array_values($this->buttons);

        return InlineKeyboardMarkupType::create($buttons);
    }

    /**
     * @inheritDoc
     */
    public function getCommands(): array
    {
        return $this->commands;
    }
}
