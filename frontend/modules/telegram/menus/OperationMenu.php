<?php

namespace frontend\modules\telegram\menus;

use common\models\Contractor;
use frontend\modules\telegram\commands\OperationCommand;
use TgBotApi\BotApiBase\Type\InlineKeyboardMarkupType;

class OperationMenu extends AbstractMenu
{
    /**
     * @inheritDoc
     */
    public function getKeyboard(): InlineKeyboardMarkupType
    {
        $this->builder->addButton(self::DEFAULT_ROW, 'Приход', OperationCommand::createInstance([
            'company_id' => $this->getState()->getValue('company_id'),
            'cashbox_id' => $this->getState()->getValue('cashbox_id'),
            'contractor_type' => Contractor::TYPE_CUSTOMER,
        ]));

        $this->builder->addButton(self::DEFAULT_ROW, 'Расход', OperationCommand::createInstance([
            'company_id' => $this->getState()->getValue('company_id'),
            'cashbox_id' => $this->getState()->getValue('cashbox_id'),
            'contractor_type' =>  Contractor::TYPE_SELLER,
        ]));

        $this->builder->addCancelButton();

        return $this->builder->buildKeyboard();
    }

    /**
     * @inheritDoc
     */
    public function getText(): string
    {
        return 'Укажите тип операции. Приход или Расход?';
    }
}
