<?php

namespace frontend\modules\telegram\jobs;

use frontend\modules\telegram\factories\ApiFactory;
use frontend\modules\telegram\models\Identity;
use TgBotApi\BotApiBase\Method\SendMessageMethod;
use yii\base\BaseObject;
use yii\queue\JobInterface;
use yii\queue\Queue;
use Yii;

class SendNotificationJob extends BaseObject implements JobInterface
{
    /**
     * @var int Идентификатор чата
     */
    public $chatId;

    /**
     * @var string Текст сообщения
     */
    public $text;

    /**
     * Добавить задачу в очередь
     *
     * @param Identity $identity Телеграм аккаунт
     * @param string $text Текст сообщения
     * @param int $delay Задержка отправки, секунд
     * @return int
     */
    public static function dispatchJob(Identity $identity, string $text, int $delay = 0): int
    {
        $chatId = $identity->telegram_id;

        return Yii::$app->queue->delay($delay)->push(new self(compact('chatId', 'text')));
    }

    /**
     * Запустить задачу
     *
     * @param Queue $queue
     * @return void
     * @throws
     */
    public function execute($queue)
    {
        ApiFactory::createBot()->sendMessage(SendMessageMethod::create($this->chatId, $this->text));
    }
}
