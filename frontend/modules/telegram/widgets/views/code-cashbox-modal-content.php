<?php

namespace frontend\modules\telegram\views\widgets;

use common\models\employee\Employee;
use Yii;
use yii\bootstrap4\Html;

/** @var Employee $employee */
$employee = Yii::$app->user->identity;
$link = Yii::$app->params['telegram']['bot_link'];

?>
<div class="row">
    <div class="col-sm-7 col-md-8">
        <p>Вносите операции по кассе через Telegram в вашем мобильном.
            <br/>
            Данные моментально попадают в кассу КУБ24.
            <br/>
            Особенно удобно, когда вы даете деньги подотчет сотруднику.
            Заводите на него кассу, например «Касса Петров» и по QR коду он у себя запускает наш Telegram бот. Теперь после каждой траты, он вводит данные в Telegram бот и данные отражаются у вас в кассе и во всех отчетах.
            <br/>
            Вы автоматизировали свою работу - вам не нужно ждать от него инфы по расходам и вносить её в кассу!
        </p>

        <p><b>Отсканируйте qr-код, <a href="<?= Html::encode($link) ?>" target="_blank">перейдите в чат</a>
                и введите код доступа.</b></p>

        <div class="telegram-code">
            <h4>Ваш код: <span id="telegram-code-value"></span></h4>
            <p>Код активен: <span id="telegram-code-timer"></span></p>
        </div>

        <div class="telegram-activated text-center mt-5 d-none">
            <h5>Уведомления для компании <?= Html::encode($employee->company->name_short) ?> подключены</h5>

            <button type="button" class="button-clr button-regular button-regular_red w-50" data-dismiss="modal">
                Продолжить
            </button>
        </div>
    </div>
    <div class="col-sm-5 col-md-4 mt-4 mt-sm-0">
        <?= Html::img('/img/telegram/qr-code.gif', ['class' => 'border img-fluid']) ?>
    </div>
</div>
