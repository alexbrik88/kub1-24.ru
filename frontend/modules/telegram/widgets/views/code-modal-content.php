<?php

namespace frontend\modules\telegram\views\widgets;

use common\models\employee\Employee;
use Yii;
use yii\bootstrap4\Html;

/** @var Employee $employee */
$employee = Yii::$app->user->identity;
$link = Yii::$app->params['telegram']['bot_link'];

?>
<div class="row">
    <div class="col-sm-7 col-md-8">
        <p>Телеграмм, используется для получения уведомлений из сервиса КУБ24. Подключите интеграцию и Вы сможете
            оперативно получать необходимую информацию о заказах, продажах, финансах и т.д.</p>

        <p><b>Отсканируйте qr-код, <a href="<?= Html::encode($link) ?>" target="_blank">перейдите в чат</a>
                и введите код доступа.</b></p>

        <div class="telegram-code">
            <h4>Ваш код: <span id="telegram-code-value"></span></h4>
            <p>Код активен: <span id="telegram-code-timer"></span></p>
        </div>

        <div class="telegram-activated text-center mt-5 d-none">
            <h5>Уведомления для компании <?= Html::encode($employee->company->name_short) ?> подключены</h5>

            <button type="button" class="button-clr button-regular button-regular_red w-50" data-dismiss="modal">
                Продолжить
            </button>
        </div>
    </div>
    <div class="col-sm-5 col-md-4 mt-4 mt-sm-0">
        <?= Html::img('/img/telegram/qr-code.gif', ['class' => 'border img-fluid']) ?>
    </div>
</div>
