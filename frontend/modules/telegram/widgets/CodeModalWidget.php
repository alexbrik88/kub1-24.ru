<?php

namespace frontend\modules\telegram\widgets;

use frontend\modules\telegram\assets\CodeAsset;
use yii\bootstrap4\Modal;

class CodeModalWidget extends Modal
{
    /**
     * @inheritDoc
     */
    public $title = 'Подключить уведомления';

    /**
     * @inheritDoc
     */
    public $options = ['id' => 'telegram-code-modal'];
    public $viewFile = 'code-modal-content.php';

    /**
     * @inheritDoc
     */
    public $closeButton = [
        'class' => 'modal-close close',
        'data' => 'data-dismiss',
        'label' => '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#close"></svg>',
    ];

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();
        CodeAsset::register($this->view);
        echo $this->render($this->viewFile);
    }
}
