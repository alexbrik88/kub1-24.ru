<?php

namespace frontend\modules\telegram\widgets;

use frontend\modules\telegram\assets\CodeAsset;
use frontend\modules\telegram\models\Identity;
use yii\bootstrap4\Html;
use yii\bootstrap4\Widget;
use yii\helpers\ArrayHelper;

class CodeButtonWidget extends Widget
{
    /**
     * @var string[]
     */
    public $options = [];
    public $defaults = [
        'id' => 'telegram-modal-button',
        'type' => 'button',
        'class' => 'button-clr button-regular button-hover-transparent pr-3 pl-3',
        'onclick' => "return Telegram.Button.click(this);",
        'data-disabled' => 'button-disabled',
        'style' => '',
        'icon' => '',
    ];

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();
        CodeAsset::register($this->view);
    }

    /**
     * @inheritDoc
     */
    public function run()
    {
        $options = array_merge($this->defaults, $this->options);
        $tag = ArrayHelper::remove($options, 'tag', 'button');
        $label = ArrayHelper::remove($options, 'label', 'Подключить');
        $icon = ArrayHelper::remove($options, 'icon');


        if (!self::isActive()) {
            Html::addCssClass($options, $options['data-disabled']);
            unset($options['onclick']);
        }

        echo Html::tag($tag, self::getImg($icon) . $label, $options);
    }

    /**
     * Виджет активен
     *
     * @return bool
     */
    public static function isActive(): bool
    {
        if (!Identity::getCurrent()) {
            return true;
        }

        return false;
    }

    /**
     * @param $icon
     * @return string
     */
    public static function getImg($icon): string
    {
        if ($icon) {
            return Html::img($icon, ['style' => 'width:22px; padding-right:5px;']);
        }

        return '';
    }
}
