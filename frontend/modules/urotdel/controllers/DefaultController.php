<?php

namespace frontend\modules\urotdel\controllers;

use common\models\UrotdelVisit;
use frontend\components\FrontendController;
use frontend\modules\urotdel\models\LaborDocumentsFrom;
use frontend\modules\urotdel\models\VisitForm;
use frontend\rbac\UserRole;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

final class DefaultController extends FrontendController
{
    public $layout = 'documentoved';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            UserRole::ROLE_CHIEF,
                        ]
                    ],
                ],
            ],
        ]);
    }

    public function actionIndex(): string {
        return $this->render('index');
    }

    public function actionLaborDocuments() {
        $form = new LaborDocumentsFrom();
        if ($form->load(Yii::$app->request->post()) && $form->send(Yii::$app->user->identity)) {
            Yii::$app->session->setFlash('success', 'Ваши пожелания успешно отправлены.');

            return $this->redirect('labor-documents');
        }

        return $this->render('labor-documents', [
            'model' => $form,
        ]);
    }

    public function actionVisit(): int {
        $visitId = Yii::$app->request->post('visitId');
        $visit = null;
        if ($visitId !== null) {
            $visit = $this->findVisitModel((int)$visitId);
        }

        $form = new VisitForm();
        $form->duration = Yii::$app->request->post('duration');
        $visit = $form->save(Yii::$app->user->identity->company, $visit);
        if ($visit !== null) {
            return $visit->id;
        }

        return 0;
    }

    private function findVisitModel(int $visitId): ?UrotdelVisit {
        return UrotdelVisit::findOne(['id' => $visitId]);
    }

}
