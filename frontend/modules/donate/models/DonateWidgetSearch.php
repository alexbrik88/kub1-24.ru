<?php

namespace frontend\modules\donate\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\company\DonateWidget;

/**
 * DonateWidgetSearch represents the model behind the search form about `common\models\company\DonateWidget`.
 */
class DonateWidgetSearch extends DonateWidget
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'return_url', 'additional_number', 'result_url', 'email_comment', 'invoice_comment'], 'safe'],
            [['status', 'company_id', 'product_id', 'is_autocomplete', 'is_additional_number_before', 'send_with_stamp', 'send_result', 'is_bik_autocomplete', 'notify_to_email', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DonateWidget::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'status' => $this->status,
            'company_id' => $this->company_id,
            'product_id' => $this->product_id,
            'is_autocomplete' => $this->is_autocomplete,
            'is_additional_number_before' => $this->is_additional_number_before,
            'send_with_stamp' => $this->send_with_stamp,
            'send_result' => $this->send_result,
            'is_bik_autocomplete' => $this->is_bik_autocomplete,
            'notify_to_email' => $this->notify_to_email,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'return_url', $this->return_url])
            ->andFilterWhere(['like', 'additional_number', $this->additional_number])
            ->andFilterWhere(['like', 'result_url', $this->result_url])
            ->andFilterWhere(['like', 'email_comment', $this->email_comment])
            ->andFilterWhere(['like', 'invoice_comment', $this->invoice_comment]);

        return $dataProvider;
    }
}
