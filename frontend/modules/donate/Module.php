<?php

namespace frontend\modules\donate;

use Yii;

/**
 * donate module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\donate\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
        Yii::$app->errorHandler->errorAction = '/out/default/error';
        Yii::$app->params['outCompany'] = null;
    }
}
