<?php

use common\models\Company;
use common\models\company\DonateWidget;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\company\DonateWidget */
/* @var $form yii\widgets\ActiveForm */
/* @var $company Company */

$this->registerJs('
    $("#add_num").on("change", function() {
        if ($(this).prop("checked")) {
            $("#add_num_collapse").collapse("show");
        } else {
            $("#add_num_collapse").collapse("hide");
            $("#donatewidget-additional_number").val("");
            $("#donatewidget-is_additional_number_before input[value=0]").prop("checked", true);
            $("#donatewidget-is_additional_number_before input[type=radio]:not(.md-radiobtn)").uniform("refresh");
        }
    });
    $("#invoice_comment").on("change", function() {
        if ($(this).prop("checked")) {
            $("#invoice_comment_collapse").collapse("show");
        } else {
            $("#invoice_comment_collapse").collapse("hide");
            $("#donatewidget-invoice_comment").val("");
        }
    });
    $("#email_comment").on("change", function() {
        if ($(this).prop("checked")) {
            $("#email_comment_collapse").collapse("show");
        } else {
            $("#email_comment_collapse").collapse("hide");
            $("#donatewidget-email_comment").val("");
        }
    });
    $("#donatewidget-need_contract").on("change", function() {
        if ($(this).prop("checked")) {
            $("#need_contract_collapse").collapse("show");
        } else {
            $("#need_contract_collapse").collapse("hide");
            $("#donatewidget-need_contract_sum").val("");
        }
    });
    $(".check-collapse").on("change", function() {
        if ($(this).prop("checked")) {
            $($(this).data("target")).collapse("show");
        } else {
            $($(this).data("target")).collapse("hide");
            $("input", $($(this).data("target"))).val("");
        }
    });
    $(".tooltip-hint").tooltipster({
        "theme": ["tooltipster-kub"],
        "trigger": "click",
        "contentAsHTML": true,
    });
');

$checkboxTemplate = "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}";
$dataHint = Html::tag('span', '', [
    'class' => 'tooltip-hint ico-question',
    'data-tooltip-content' => '#tooltip_strict_data',
    'style' => 'margin: 0; cursor: pointer;'
]);
$resultHint = Html::tag('span', '', [
    'class' => 'tooltip-hint ico-question',
    'data-tooltip-content' => '#tooltip_send_result',
    'style' => 'margin: 0; cursor: pointer;'
]);
$hintOptions = [
    'style' => 'position: absolute; top: -5px; left: 40px;'
];
$company = Yii::$app->user->identity->company;

$code = '<?php
$orderList = [
    [
        "type" => "Укажите ТИП товара \'PRODUCT\' или \'SERVICE\'",
        "title" => "Укажите НАЗВАНИЕ ТОВАРА",
        "unit_code" => "Укажите код ОКЕИ ЕДИНИЦЫ ИЗМЕРЕНИЯ (796)",
        "quantity" => "Укажите КОЛИЧЕСТВО",
        "price" => "Укажите ЦЕНУ ЗА ЕДИНИЦУ в рублях",
    ],
];
?>
<form action="'.$model->outUrl.'" method="post">
    <?php foreach ($orderList as $key => $orderParams) : ?>
        <?php foreach ($orderParams as $name => $value) : ?>
            <input type="hidden" name="order_list[<?= $key ?>][<?= $name ?>]" value="<?= $value ?>">
        <?php endforeach ?>
    <?php endforeach ?>
    <input type="submit" name="form" value="Выставить счет" style="
        cursor: pointer;
        background: #4276a4;
        color: #fff;
        border-width: 0;
        padding: 7px 14px;
        font-size: 14px;">
</form>';
?>

<style type="text/css">
    #out-invoice-form label {
        color: #333333;
    }

    #out-invoice-form select,
    #out-invoice-form input[type="text"] {
        width: 100%;
    }
</style>

<?php $form = ActiveForm::begin([
    'id' => 'donate-widget-form',
    'layout' => 'horizontal',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => '',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
]); ?>

<div class="out-invoice-form" style="max-width: 600px;">

    <?= $form->errorSummary($model); ?>

    <?= Html::hiddenInput('out_id', $model->id) ?>

    <?= $form->field($model, 'status')->dropDownList(DonateWidget::$statusAvailable, [
        'disabled' => !$company->canDonateWidget() && $model->status == DonateWidget::INACTIVE,
    ]); ?>

    <?= $form->field($model, 'note')->textInput() ?>

    <?= $form->field($model, 'return_url')->textInput([
        'placeholder' => 'http://name.com',
    ]) ?>

    <?= $form->field($model, 'productId', [
        'parts' => [
            '{input}' => $this->render('_form_product_table', ['model' => $model]),
        ],
        'horizontalCssClasses' => [
            'label' => 'col-sm-12',
            'offset' => '',
            'wrapper' => 'col-sm-12',
            'error' => '',
            'hint' => '',
        ],
        'labelOptions' => [
            'style' => 'margin-bottom: 5px;',
        ]
    ])->render(); ?>
</div>

<div class="out-invoice-form" style="max-width: 600px;">
    <div style="margin: 0 0 15px;">
        <?= Html::A('Дополнительные настройки <span class="caret"></span>', '#additionalSettings', [
            'class' => 'collapse-toggle-link',
            'data-toggle' => 'collapse',
            'aria-expanded' => 'false',
            'aria-controls' => 'additionalSettings',
        ]) ?>
    </div>
    <div class="collapse" id="additionalSettings">
        <div class="">
            <?= $form->field($model, 'is_autocomplete', [
                'labelOptions' => [
                    'style' => 'padding-top: 0;',
                ]
            ])->checkbox([], false); ?>
            <?= $form->field($model, 'is_bik_autocomplete', [
                'labelOptions' => [
                    'style' => 'padding-top: 0;',
                ]
            ])->checkbox([], false); ?>
            <?= $form->field($model, 'send_with_stamp', [
                'labelOptions' => [
                    'style' => 'padding-top: 0;',
                ]
            ])->checkbox([], false); ?>

            <div class="form-group field-donatewidget-additional_number" style="overflow: hidden;">
                <label class="control-label col-sm-4" style="padding-top: 0;" for="add_num">
                    К номеру счета Доп номер
                </label>

                <div class="col-sm-8" style="margin-bottom: -20px;">
                    <?= Html::checkbox('add_num', (boolean)$model->additional_number, [
                        'id' => 'add_num',
                    ]) ?>
                    <div id="add_num_collapse" class="collapse <?= $model->additional_number ? 'in' : ''; ?>"
                         style="margin-top: 10px;">
                        <?= $form->field($model, 'is_additional_number_before', [
                            'horizontalCssClasses' => [
                                'label' => 'col-sm-12',
                                'offset' => '',
                                'wrapper' => 'col-sm-12',
                                'error' => 'col-sm-12',
                                'hint' => 'col-sm-12',
                            ],
                        ])->radioList(Company::$addNumPositions, [
                            'item' => function ($index, $label, $name, $checked, $value) {
                                return Html::tag('label', Html::radio($name, $checked, ['value' => $value]) . $label, [
                                    'style' => 'margin: 0 !important;'
                                ]);
                            },
                        ])->label(false)->error(false)->hint($form->field($model, 'additional_number', [
                            'labelOptions' => [
                                'style' => 'width: 95px; padding-right: 0;'
                            ],
                            'inputOptions' => [
                                'style' => 'width: 150px;'
                            ],
                        ])); ?>
                    </div>
                </div>
            </div>

            <div class="form-group field-donatewidget-invoice_comment" style="overflow: hidden;">
                <?= Html::activeLabel($model, 'invoice_comment', [
                    'class' => 'control-label col-sm-4',
                    'style' => 'padding-top: 0;',
                    'for' => 'invoice_comment',
                ]) ?>

                <div class="col-sm-8">
                    <?= Html::checkbox('invoice_comment', (boolean)$model->invoice_comment, [
                        'id' => 'invoice_comment',
                    ]) ?>
                    <div id="invoice_comment_collapse" class="collapse <?= $model->invoice_comment ? 'in' : ''; ?>"
                         style="margin-top: 10px;">
                        <?= Html::activeTextarea($model, 'invoice_comment', [
                            'maxlength' => true,
                            'style' => 'width: 100%;',
                        ]); ?>
                    </div>
                </div>
            </div>

            <div class="form-group field-donatewidget-email_comment" style="overflow: hidden;">
                <?= Html::activeLabel($model, 'email_comment', [
                    'class' => 'control-label col-sm-4',
                    'style' => 'padding-top: 0;',
                    'for' => 'email_comment',
                ]) ?>

                <div class="col-sm-8">
                    <?= Html::checkbox('email_comment', (boolean)$model->email_comment, [
                        'id' => 'email_comment',
                    ]) ?>
                    <div id="email_comment_collapse" class="collapse <?= $model->email_comment ? 'in' : ''; ?>"
                         style="margin-top: 10px;">
                        <?= Html::activeTextarea($model, 'email_comment', [
                            'maxlength' => true,
                            'style' => 'width: 100%;',
                        ]); ?>
                    </div>
                </div>
            </div>

            <?= $form->field($model, 'notify_to_email', [
                'labelOptions' => [
                    'style' => 'padding-top: 0;',
                ]
            ])->checkbox([], false); ?>

            <div class="form-group field-donatewidget-need_contract" style="overflow: hidden;">
                <?= Html::activeLabel($model, 'need_contract', [
                    'class' => 'control-label col-sm-4',
                    'style' => 'padding-top: 0;',
                ]) ?>

                <div class="col-sm-8">
                    <?= Html::activeCheckbox($model, 'need_contract', ['label' => false]) ?>
                    <div id="need_contract_collapse" class="collapse <?= $model->need_contract ? 'in' : ''; ?>"
                         style="margin-top: 10px;">
                        <?= $form->field($model, 'need_contract_sum', [
                            'options' => ['class' => 'row'],
                            'labelOptions' => ['class' => 'col-sm-6'],
                            'wrapperOptions' => ['class' => 'col-sm-6'],
                        ])->textInput([
                            'maxlength' => true,
                            'style' => 'width: 100%;',
                        ]); ?>
                    </div>
                </div>
            </div>

            <span style="font-weight: 600;font-size: 14px;">Для интеграции по API (информация для программиста):</span>

            <?= $form->field($model, 'send_result', [
                'template' => $checkboxTemplate,
                'hintOptions' => $hintOptions,
                'wrapperOptions' => [
                    'style' => 'padding-top: 7px;'
                ]
            ])->hint($resultHint)->checkbox([
                'class' => 'check-collapse',
                'data-target' => '#result_url_collapse'
            ], false) ?>
            <div id="result_url_collapse" class="collapse <?= $model->send_result ? 'in' : ''; ?>">
                <?= $form->field($model, 'result_url')->textInput([
                    'placeholder' => 'http://name.com/result',
                ]) ?>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-12">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span>', [
                'class' => 'btn darkblue ladda-button',
                'data-style' => 'expand-right',
                'style' => 'color: #fff; width: 140px;'
            ]) ?>
            <?= Html::a('Отменить', ['index'], [
                'class' => 'btn darkblue pull-right',
                'style' => 'color: #fff; width: 140px;',
            ]) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>

<div style="display: none;">
    <span id="tooltip_send_result">
        Выберите и укажите "URL результата",
        <br>
        чтобы отправить на свой сайт
        <br>
        данные созданного счета.
        <br>
        Остались вопросы? Пишите нам
        <br>
        на support@kub-24.ru
    </span>
</div>

<?= $this->render('@frontend/modules/documents/views/invoice/form/_form_product_new'); ?>

<?= $this->render('@frontend/modules/documents/views/invoice/form/_form_product_existed', [
    'contentCssClass' => 'to-outInvoice-content',
]); ?>
