<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\company\DonateWidget */
/* @var $product common\models\product\Product */
?>

<tr class="selected-product-row">
    <td style="text-align: center;">
        <span class="selected-product-delete">
            <i class="icon-close" style="font-size: 20px; cursor: pointer;"></i>
        </span>
    </td>
    <td style="text-align: right;"><?= $product->id ?></td>
    <td style="text-align: right;"><?= $product->article ?></td>
    <td>
        <?= $product->title ?>
        <?= Html::activeHiddenInput($model, 'productId[]', [
            'value' => $product->id,
            'class' => 'selected-product-id'
        ]) ?>
    </td>
    <td style="text-align: right;">
        <?= number_format(($product->price_for_sell_with_nds/100), 2, ',', '&nbsp;') ?>
    </td>
</tr>
