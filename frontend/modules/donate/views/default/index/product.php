<?php

use frontend\modules\donate\models\DonateForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\donate\models\DonateForm */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Введите сумму';
$this->params['showHeader'] = true;

$totalCount = array_sum($model->product);
?>

<div class="out-invoice-form" style="width: 600px; padding-top: 25px;">

    <?php $form = ActiveForm::begin([
        'id' => 'donatewidget-form',
        'action' => [
            'index',
            'id' => $model->donateWidget->id,
            'uid' => $model->uid,
            'hash' => $model->hash,
        ],
        'enableClientValidation' => false,
    ]); ?>

    <?= Html::activeHiddenInput($model, 'view') ?>

    <?= $form->field($model, 'product', [
        'parts' => [
            '{input}' => $this->render('_product_table', ['model' => $model]),
        ]
    ])->label(false)->render(); ?>

    <?php if ($model->donateWidget->need_contract) : ?>
        <div class="donate-sum-message collapse" data-toggle="false">
            <div style="padding-bottom: 15px;">
                При пожертвовании более <?= $model->donateWidget->need_contract_sum ?> р. необходим договор пожертвования.<br>
                Договор пожертвования Вам пришлет менеджер фонда после того, как Вы сделаете оплату.
            </div>
        </div>
    <?php endif ?>

    <?= Html::activeHiddenInput($model, 'email') ?>
    <?= Html::activeHiddenInput($model, 'phone') ?>
    <?= Html::activeHiddenInput($model, 'comment') ?>
    <?= Html::activeHiddenInput($model, 'legal_type') ?>
    <?= Html::activeHiddenInput($model, 'legal_name') ?>
    <?= Html::activeHiddenInput($model, 'legal_inn') ?>
    <?= Html::activeHiddenInput($model, 'legal_kpp') ?>
    <?= Html::activeHiddenInput($model, 'legal_address') ?>
    <?= Html::activeHiddenInput($model, 'legal_rs') ?>
    <?= Html::activeHiddenInput($model, 'legal_bik') ?>
    <?= Html::activeHiddenInput($model, 'chief_position') ?>
    <?= Html::activeHiddenInput($model, 'chief_name') ?>

    <div class="form-group row">
        <div class="col-xs-12">
            <?= Html::submitButton($totalCount > 0 ? 'Продолжить' : 'Введите сумму', [
                'id' => 'donatewidget-submit',
                'class' => 'btn darkblue pull-right',
                'style' => 'color: #fff; min-width: 120px; text-align: center',
                'disabled' => $totalCount > 0 ? false : true,
            ]) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
