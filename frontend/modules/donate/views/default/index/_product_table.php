<?php

use frontend\modules\donate\models\DonateForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model frontend\modules\donate\models\DonateForm */

$paging = count($model->donateWidget->selectedProducts) > 10 ? 'true' : 'false';

$productArray = $model->donateWidget->selectedProducts;
$need_contract = (int) $model->donateWidget->need_contract;
$need_contract_sum = (int) $model->donateWidget->need_contract_sum;

$js = <<<JS
var needContract = {$need_contract};
var needContractSum = {$need_contract_sum};
var checkTotalSum = function() {
    var totalSum = 0;
    $('#donatewidget-form .quantity-input').each(function(){
        var prodSum = parseInt($(this).val()) || 0;
        totalSum += Math.max(0, prodSum);
    });
    if (totalSum > 0) {
        $('#donatewidget-submit').text('Продолжить');
        $('#donatewidget-submit').prop('disabled', false);
    } else {
        $('#donatewidget-submit').text('Введите сумму');
        $('#donatewidget-submit').prop('disabled', true);
    }
    if (needContract == 1) {
        if (totalSum > needContractSum) {
            $('.donate-sum-message').collapse('show');
        } else {
            $('.donate-sum-message').collapse('hide');
        }
    }
}

$(document).on('click', '.quantity-btn', function(e){
    e.preventDefault();
    var type      = $(this).attr('data-type');
    var input     = $(this).closest('.quantity-box').find('.quantity-input');
    var val = parseInt(input.val());
    if(type == 'minus') {
        val -= 1000;
    } else if(type == 'plus') {
        val += 1000;
    }
    input.val(Math.min(input.attr('max'), Math.max(input.attr('min'), val))).change();
});

$(document).on('paste keyup change', '.quantity-input', function(){
    if ($(this).val() != '') {
        var box = $(this).closest('.quantity-box');
        var minus = box.find(".quantity-btn.type-minus");
        var plus = box.find(".quantity-btn.type-plus");
        var minVal =  parseInt($(this).attr('min'));
        var maxVal =  parseInt($(this).attr('max'));
        var val = Math.min(maxVal, Math.max(minVal, parseInt($(this).val()) || 0));

        minus.prop('disabled', val <= minVal);
        plus.prop('disabled', val >= maxVal);

        if ($(this).val() != String(val)) {
            $(this).val(val);
        }
        $($(this).data('target')).val(val);
    }

    checkTotalSum();
});

$(document).on('keydown', '.quantity-input', function(e){
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
         // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
         // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

checkTotalSum();

JS;

$this->registerJs($js);
?>
<style type="text/css">
table#produc-table {
    width: 100%;
    position: relative;
    margin-bottom: 5px;
    table-layout: fixed;
    border-bottom: 1px solid #ddd;
}
table#produc-table th {
    padding: 10px;
}
table#produc-table td {
    vertical-align: middle;
    box-sizing: border-box;
}
table.quantity-box td {
    vertical-align: middle;
    text-align: center;
}
table.quantity-box button.quantity-btn,
table.quantity-box button.quantity-btn:hover {
    margin: 0;
    padding: 6px 10px;
    border-radius: 4px !important;
    background-color: #e3e8ee;
    color: #333333;
}
.dataTables_wrapper .dataTables_paginate .paginate_button,
.dataTables_wrapper .dataTables_paginate .paginate_button:hover,
.dataTables_wrapper .dataTables_paginate .paginate_button.disabled,
.dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover {
    padding: 0;
    border-width: 0;
}
</style>

<table id="produc-table" class="table table-bordered table-hover" style="table-layout: fixed;">
    <col width="40">
    <col width="340">
    <col width="220">
    <thead>
        <tr class="produc-table-row">
            <th style="">##</th>
            <th style="">Наименование</th>
            <th style="text-align: center;">Сумма пожертвования руб.</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        foreach ($productArray as $product) : ?>
            <tr class="produc-table-row">
                <td style="text-align: right;">
                    <?= $i++; ?>
                </td>
                <td class="column-product-search">
                    <?= $product->title ?>
                </td>
                <td style="padding: 0;">
                    <?php $inputName = "product[{$product->id}]"; ?>
                    <?php $inputValue = round(max(0, ArrayHelper::getValue($model->product, $product->id, 1000))); ?>
                    <div style="margin: 5px;">
                        <table class="input-group quantity-box" style="width: 100%; margin: 0;">
                            <tr>
                                <td style="padding: 0;">
                                    <?= Html::button('<span class="glyphicon glyphicon-minus"></span>', [
                                        'class' => 'btn btn-default quantity-btn type-minus',
                                        'disabled' => ($inputValue == 0) ? true : false,
                                        'data-type' => 'minus',
                                        'type' => 'button',
                                    ]) ?>
                                </td>
                                <td style="padding: 0 3px;">
                                    <?= Html::activeTextInput($model, "product[{$product->id}]", [
                                        'value' => $inputValue,
                                        'class' => 'form-control input-number quantity-input price-input',
                                        'style' => 'text-align: center; border-radius: 4px !important; padding: 6px;',
                                        'data-value' => $inputValue,
                                        'min' => 1,
                                        'max' => DonateForm::MAX_VALUE,
                                        'data-target' => "#data-product-{$product->id}",
                                    ]) ?>
                                </td>
                                <td style="padding: 0;">
                                    <?= Html::button('<span class="glyphicon glyphicon-plus"></span>', [
                                        'class' => 'btn btn-default quantity-btn type-plus',
                                        'disabled' => false,
                                        'data-type' => 'plus',
                                        'type' => 'button',
                                    ]) ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
