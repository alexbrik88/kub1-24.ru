<?php

use yii\bootstrap\Nav;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $invoice common\models\document\Invoice */

$this->title = 'Платежное поручение';

$this->registerCssFile('@web/css/print/common.css', [
    'depends' => [
        'frontend\assets\AppAsset',
        'frontend\modules\documents\assets\DocumentPrintAsset',
    ],
]);

$this->params['uid'] = $invoice->uid;

$script = <<<JS
var documentPayOpen = function() {
    $("#out-view-shading").removeClass("hidden");
    $("#document-pay-wrapper").animate({right: 0}, 200);
}
var documentPayClose = function() {
    $("#document-pay-wrapper").animate({right: "-400px"}, 200, function() {
        $("#out-view-shading").addClass("hidden");
    });
}
$(document).on("click", "#document-pay-link", function (e) {
    e.preventDefault();
    documentPayOpen();
});
$(document).on("click", "#document-pay-close, .close-panel", function (e) {
    e.preventDefault();
    documentPayClose();
});
$(document).on("click", "#document-pay-content .pay-block.pay-link",function () {
    if ($(this).hasClass('target')) {
        window.open($(this).data('url'));
    } else {
        location.href = $(this).data('url');
    }
});
JS;

$this->registerJs($script);
?>

<style type="text/css">
.view-payment-order * {
    -webkit-box-sizing: content-box;
    -moz-box-sizing: content-box;
    box-sizing: content-box;
}
 td {
    border: 1px solid #000000;
    padding: 2px 4px;
    font-size: 9pt;
}
.table > tbody > tr > td {
    border: 0;
    padding: 2px 4px;
    font-size: 9pt;
    line-height: normal;
}
</style>

<div class="view-payment-order" style="padding-top: 50px;">
    <?= $this->render('@documents/views/payment-order/pdf-view', ['model' => $model]); ?>
</div>

<div id="out-view-shading" class="hidden"></div>

<div id="document-pay-wrapper">
    <div id="document-pay-header" class="border-bottom-e4">
        <button id="document-pay-close" type="button" class="close" aria-hidden="true">×&times;</button>
        Оплатить счет
    </div>
    <div id="document-pay-content">
        <?= $this->render('view/pay', [
            'invoice' => $invoice,
        ]); ?>
    </div>
</div>
