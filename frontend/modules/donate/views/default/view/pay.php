<?php

use common\components\ImageHelper;
use common\models\bank\Bank;
use common\models\Contractor;
use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $invoice common\models\document\Invoice */

$apiBank = null;
$apiBankUrl = null;
$userBank = $invoice->contractor_bik ? Bank::findOne([
    'bik' => $invoice->contractor_bik,
    'is_blocked' => false,
]) : null;

if ($invoice->contractor_bik && ($apiBank = Banking::getBankByBik($invoice->contractor_bik)) !== null) {
    if ($userBank === null) {
        $userBank = $apiBank;
    }
    $alias = Banking::aliasByBik($invoice->contractor_bik);
    $apiBankUrl = Url::to([
        "/cash/banking/{$alias}/default/pay-bill",
        'uid' => $invoice->uid,
    ]);
}

$payUrl = Url::to(['download', 'type' =>'1c', 'uid' => $invoice->uid]);
$isPhysicalPerson = $invoice->contractor->face_type == Contractor::TYPE_PHYSICAL_PERSON;
?>

<?php if ($apiBank !== null && $apiBankUrl !== null) : ?>
    <div class="pay-block pay-link target border-bottom-e4" data-url="<?= $apiBankUrl; ?>">
        <?= ImageHelper::getThumb($apiBank->uploadDirectory . $apiBank->logo_link, [150, 90]); ?>
    </div>
    <div class="pay-block border-bottom-e4" style="padding: 10px 15px">
        <table>
            <tr>
                <td style="width: 100%; padding: 5px 0;">
                    <div class="main-text" style="margin-bottom: 5px;">
                        Платежка будет создана автоматически в вашем клиент-банке,
                        после нажатия на логотип банка.
                    </div>
                    <div class="additional-text">
                        Платежка создается за счет интеграции Банка и
                        <br>
                        Сервиса выставления счетов
                        <?= Html::a('КУБ', Yii::$app->params['serviceSite'], [
                            'target' => '_blank',
                        ]); ?>
                    </div>
                </td>
            </tr>
        </table>
    </div>
<?php else : ?>
    <?php if ($userBank !== null) : ?>
        <div class="pay-block pay-link target border-bottom-e4" data-url="<?= $userBank->url; ?>">
            <table>
                <tr>
                    <td>
                        <?= ImageHelper::getThumb($userBank->uploadDirectory . $userBank->logo_link, [150, 90]); ?>
                    </td>
                    <td>
                        <span class="main-text hover-text">Переход в банк-клиент</span>
                    </td>
                </tr>
            </table>
        </div>
    <?php endif; ?>
    <?php if ($isPhysicalPerson) : ?>
        <?php if (YII_ENV == 'dev' || in_array($invoice->company_id, [1, 486])) : ?>
            <div style="padding: 15px;">
                <div class="form-group">
                    Для оплаты счета № <?= $invoice->getFullNumber() ?>
                    от <?= date_format(date_create($invoice->document_date), 'd.m.Y') ?>г,<br>
                    на сумму <?= number_format($invoice->total_amount_with_nds/100, 2, ',', ' ') ?> руб.,
                    укажите сумму счета<br>
                    и нажмите "Оплатить".
                </div>
                <div class="form-group">
                    Оплата будет произведена через платежную<br>
                    систему <span style="color: #F48723;">QIWI</span> на счет платежного агента ООО «КУБ»<br>
                    с последующим зачислением ООО «Мегаполис»
                </div>

                <iframe width="300" height="300" src="https://widget.qiwi.com/widgets/middle-widget-300x300?publicKey=2tbp1WQvsgQeziGY9vTLe9vDZNg7tmCymb4Lh6STQokqKrpCC6qrUUKEDZAJ7orpdMagCqapdmb9DjbQkStALmcU2egNERsreZZYTavVTgndXJAbLyFYyAyH4bNmv" allowtransparency="true" scrolling="no" frameborder="0"></iframe>
            </div>
        <?php endif ?>
    <?php else : ?>
        <div class="pay-block pay-link border-bottom-e4" data-url="<?= $payUrl; ?>">
            <table>
                <tr>
                    <td style="width: 80px; padding: 15px 0;">
                        <?= ImageHelper::getThumb('img/icons/download-ico.png', [54, 54]); ?>
                    </td>
                    <td style="padding: 15px 0;">
                        <div class="main-text">Скачать платежку</div>
                        <div class="additional-text hover-text">Платежка в формате 1С,<br>для загрузки в клиент-банк</div>
                    </td>
                </tr>
            </table>
        </div>
    <?php endif ?>
<?php endif; ?>
