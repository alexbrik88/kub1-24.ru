<?php

use frontend\assets\AppAsset;
use frontend\modules\cash\modules\banking\components\Banking;
use lavrentiev\widgets\toastr\NotificationFlash;
use yii\bootstrap\Modal;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

//AppAsset::register($this);
$this->registerCssFile('@web/css/out-view.css',  [
    'depends' => array_merge(['frontend\assets\AppAsset'], ArrayHelper::getValue($this->params, 'assets', [])),
]);

$uid = ArrayHelper::getValue($this->params, 'uid', false);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
    <link rel="icon" href="/img/fav.svg?i=2" type="image/x-icon">
</head>
<body class="out-view-page">
<?php $this->beginBody() ?>

<?php NavBar::begin([
    'brandLabel' => 'Сервис выставления счетов',
    'brandUrl' => Yii::$app->params['serviceSite'],
    'brandOptions' => [
        'target' => '_blank',
    ],
    'options' => [
        'class' => 'navbar-default navbar-fixed-top',
    ],
    'innerContainerOptions' => [
        'class' => 'container-fluid',
    ],
]); ?>

<?= Nav::widget([
    'options' => [
        'id' => 'out-view-navbar',
        'class' => 'navbar-nav',
    ],
    'encodeLabels' => false,
    'items' => [
        [
            'label' => '<i class="fa fa-file-pdf-o" aria-hidden="true"></i><span> Скачать в PDF</span>',
            'url' => ['download', 'type' =>'pdf', 'uid' => $uid],
            'options' => [
                'class' => 'out-view-item',
            ],
            'linkOptions' => [
                'target' => '_blank',
                'style' => 'padding-top: 8px;',
            ],
        ],
        [
            'label' => '<i class="fa fa-file-text-o" aria-hidden="true"></i><span> Скачать в формате 1С</span>',
            'url' => ['download', 'type' =>'1c', 'uid' => $uid],
            'options' => [
                'class' => 'out-view-item',
            ],
            'linkOptions' => [
                'target' => '_blank',
                'style' => 'padding-top: 8px;',
            ],
        ],
        [
            'label' => 'Оплатить',
            'url' => '#',
            'options' => [
                'class' => 'out-view-item-save',
            ],
            'linkOptions' => [
                'id' => 'document-pay-link',
                'class' => 'btn darkblue text-white document-pay-link',
                'style' => 'margin-top: 8px;',
            ],
        ],
    ],
]); ?>

<?php NavBar::end(); ?>

<?= NotificationFlash::widget([
    'options' => [
        'closeButton' => true,
        'showDuration' => 1000,
        'hideDuration' => 1000,
        'timeOut' => 5000,
        'extendedTimeOut' => 1000,
        'positionClass' => NotificationFlash::POSITION_TOP_RIGHT,
    ],
]); ?>

<div class="page-content">
    <?= $content ?>
</div>

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
