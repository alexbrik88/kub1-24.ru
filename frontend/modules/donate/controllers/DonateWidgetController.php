<?php

namespace frontend\modules\donate\controllers;

use Yii;
use common\models\company\DonateWidget;
use common\models\Contractor;
use frontend\components\FrontendController;
use frontend\modules\donate\models\DonateWidgetSearch;
use frontend\rbac\UserRole;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * DonateWidgetController implements the CRUD actions for DonateWidget model.
 */
class DonateWidgetController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->company->canDonateWidget();
                        }
                    ],
                ],
            ],
        ]);
    }

    /**
     * Lists all DonateWidget models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->redirect(['/contractor/sale-increase', 'activeTab' => Contractor::TAB_INVOICE_LINKS]);

        $company = Yii::$app->user->identity->company;
        $searchModel = new DonateWidgetSearch([
            'company_id' => $company->id,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'company' => $company,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DonateWidget model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DonateWidget model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $company = Yii::$app->user->identity->company;
        $model = new DonateWidget([
            'company_id' => $company->id,
        ]);

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DonateWidget model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DonateWidget model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @return mixed
     */
    public function actionProductsTable()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $company = Yii::$app->user->identity->company;
        $model = new DonateWidget(['company_id' => $company->id]);
        $model->populateRelation('company', $company);

        $query = $company->getProducts()->alias('product')->select([
            'product.id',
            'product.title',
            'product.price_for_sell_with_nds',
        ])->andWhere([
            'product.production_type' => Yii::$app->request->post('productType'),
            'product.is_deleted' => false,
            'product.not_for_sale' => false,
        ])->andWhere([
            'not',
            ['product.id' => Yii::$app->request->post('exists')]
        ]);

        if (Yii::$app->request->post('add_all') == 1) {
            $query->andFilterWhere(['like', 'product.title', Yii::$app->request->post('title')]);
        } else {
            $query->andWhere([
                'product.id' => Yii::$app->request->post('in_order'),
            ]);
        }

        $productArray = $query->all();
        $result = '';

        foreach ($productArray as $product) {
            $result .= $this->renderPartial('_form_product_row', [
                'model' => $model,
                'product' => $product,
            ]);
        }

        return ['result' => $result];
    }

    /**
     * Finds the DonateWidget model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return DonateWidget the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $company = Yii::$app->user->identity->company;
        if (($model = DonateWidget::findOne(['id' => $id, 'company_id' => $company->id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
