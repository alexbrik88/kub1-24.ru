<?php

namespace frontend\modules\donate\controllers;

use common\components\pdf\PdfRenderer;
use common\models\company\DonateWidget;
use common\models\document\Invoice;
use common\models\document\PaymentOrder;
use frontend\models\Documents;
use frontend\modules\documents\controllers\InvoiceController;
use frontend\modules\donate\models\DonateForm;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

/**
 * Default controller for the `donate` module
 */
class DefaultController extends Controller
{
    public $layoutWrapperCssClass;
    public $layout = 'main';

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => \yii\web\ErrorAction::className(),
            ],
        ];
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws BadRequestHttpException
     */
    public function actionIndex($id)
    {
        $donate = $this->findModel($id);
        if ($donate->status != DonateWidget::ACTIVE) {
            $message = 'Страница была удалена.';
            $message .= "\r\n";
            $message .= 'За подробной информацией обратитесь к ';
            $message .= $donate->company->getTitle(true) . '.';
            Yii::$app->params['hasError'] = true;

            throw new NotFoundHttpException($message);
        }
        if (!$donate->company->createInvoiceAllowed(Documents::IO_TYPE_OUT)) {
            $message = 'Количество счетов достигло лимита.';
            $message .= "\r\n";
            $message .= 'За подробной информацией обратитесь к ';
            $message .= $donate->company->getTitle(true) . '.';
            Yii::$app->params['hasError'] = true;

            throw new ForbiddenHttpException($message);
        }

        Yii::$app->params['outCompany'] = $donate->company;
        Yii::$app->params['hasError'] = false;

        $model = new DonateForm($donate);

        $request = Yii::$app->getRequest();

        if ($request->getIsGet()) {
            $model->legal_inn = $request->get('inn');
            $model->legal_kpp = $request->get('kpp');
            $model->legal_bik = $request->get('bik');
            $model->legal_rs = $request->get('rs');
            $model->loadContractorData();
        }

        if ($model->load($request->post())) {
            if ($request->post('back')) {
                $model->setView($model->getPrevView());
            } elseif ($model->validate()) {
                if ($model->view != DonateForm::VIEW_CONTRACTOR || $model->sendInvoice()) {
                    $model->setView($model->getNextView());
                }
            }
        }

        if ($redirect = $model->getRedirect()) {
            return $this->redirect($redirect);
        }

        return $this->render("index/{$model->view}", [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws BadRequestHttpException
     */
    public function actionView($uid)
    {
        $this->layout = 'view';

        /* @var $invoice Invoice */
        $invoice = InvoiceController::getInvoiceByUid($uid);

        return $this->render('view', [
            'model' => PaymentOrder::createFromOutInvoice($invoice),
            'invoice' => $invoice,
        ]);
    }

    /**
     * Get an invoice file by uid for unauthorized users
     * @param $type file type
     * @param $uid invoice uid
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDownload($type, $uid)
    {
        /* @var $invoice Invoice */
        $invoice = InvoiceController::getInvoiceByUid($uid);

        /* @var $model PaymentOrder */
        $model = PaymentOrder::createFromOutInvoice($invoice);

        switch ($type) {
            case 'pdf':
                $renderer = new PdfRenderer([
                    'view' => '@documents/views/payment-order/pdf-view',
                    'params' => [
                        'model' => $model,
                    ],
                    'destination' => PdfRenderer::DESTINATION_STRING,
                    'filename' => $model->printTitle . '.pdf',
                    'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
                    'format' => 'A4-P',
                ]);

                return $renderer->output();
                break;
            case '1c':
                $content = Yii::$app->view->render('@frontend/modules/documents/views/invoice/1C_payment_order.php', [
                    'model' => $invoice,
                ]);
                $fileName = 'Платежное_поручение_для_счета_№' .
                            mb_ereg_replace("([^\w\s\d\-_])", '', $invoice->fullNumber) . '.txt';
                $options = [
                    'mimeType' => 'text/plain',
                ];
                return Yii::$app->response->sendContentAsFile($content, $fileName, $options);
                break;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the DonateWidget model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return DonateWidget the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DonateWidget::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
