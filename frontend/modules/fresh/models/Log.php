<?php
namespace frontend\modules\fresh\models;

use Yii;
use yii\helpers\Json;

class Log {

    private static $file = '@runtime/logs/debug_fresh.log';

    public static function add($message, $method)
    {
        if (is_array($message)) {
            $message = Json::encode($message);
        }

        $dump = "=========" . date('c') . "====================================" . "\n";
        $dump .= $method . "\n";
        $dump .= $message . "\n";
        if ($_GET)
            $dump .= 'GET: ' . json_encode($_GET) . "\n";
        if ($_POST)
            $dump .= 'POST: ' . json_encode($_POST) . "\n";
        $dump .= 'IP: ' . (Yii::$app->request->userIP ?? 'localhost') . "\n";
        $dump .= "======================================================================\n\n";

        $logFile = \Yii::getAlias(self::$file);
        file_put_contents($logFile, $dump, FILE_APPEND);
    }
}