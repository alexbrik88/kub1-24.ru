<?php
namespace frontend\modules\fresh\models;

class Fresh {

    const TMP_FILE_LIFETIME = 3600 * 24 * 14;

    /**
     * @return bool|string
     */
    public static function getWorkingPath()
    {
        return \Yii::getAlias('@frontend/runtime/fresh');
    }

    /**
     * @param ExportForm|ImportForm $form
     */
    public static function clearWorkingPath($form)
    {
        $companyID = $form->getCompany();

        switch (get_class($form)) {
            case ImportForm::class:
                $filesMask = "import_{$companyID}*";
                break;
            case ExportForm::class:
                $filesMask = "export_{$companyID}*";
                break;
            default:
                $filesMask = null;
                break;
        }

        if ($filesMask) {
            $dir = self::getWorkingPath();
            $limit = time() - self::TMP_FILE_LIFETIME;
            foreach (glob($dir . DIRECTORY_SEPARATOR . $filesMask) as $file) {
                if (filemtime($file) < $limit) {
                    @unlink($file);
                }
            }
        }
    }
}