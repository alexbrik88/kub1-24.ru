<?php
namespace frontend\modules\fresh\models;

use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use frontend\models\Documents;
use frontend\modules\export\models\export\Export;
use Yii;
use common\models\Company;
use frontend\modules\fresh\jobs\ExportDocumentsJob;

/**
 * Class ExportForm
 * @package frontend\modules\fresh\models
 *
 * @var $inn
 * @var $date_from
 * @var $date_to
 *
 */
class ExportForm extends \yii\base\Model {

    public static $allowedRoles = [
        EmployeeRole::ROLE_CHIEF,
        EmployeeRole::ROLE_ACCOUNTANT,
        EmployeeRole::ROLE_FOUNDER
    ];

    /**
     * client attributes
     */
    public $inn;
    public $date_from;
    public $date_to;

    /**
     * server attributes
     */
    private $user_id;
    private $company_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inn', 'date_from', 'date_to'], 'required'],
            [['inn'], 'string', 'min' => 10, 'max' => 12],
            [['date_from', 'date_to'], 'date', 'format' => 'php:Y-m-d'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inn' => 'ИНН',
            'date_from' => 'Дата начала',
            'date_to' => 'Дата окончания',
            'company_id' => 'Компания',
        ];
    }

    /**
     * @param $employeeId
     * @return bool
     */
    public function setCompany($employeeId)
    {
        $allowedCompaniesIds = EmployeeCompany::find()
            ->where([
                'employee_id' => $employeeId,
                'employee_role_id' => self::$allowedRoles
            ])
            ->select(['company_id'])
            ->column();

        /** @var $company Company */
        $company = Company::find()->where([
            'id' => $allowedCompaniesIds,
            'inn' => $this->inn,
        ])->one();

        if (!$company) {
            $this->addError(null, 'Компания не найдена');
            return false;
        }

        if ($company->blocked) {
            $this->addError(null, 'Компания заблокирована');
            return false;
        }

        $this->user_id = $employeeId;
        $this->company_id = $company->id;

        return true;
    }

    /**
     * @return int|null
     */
    public function getCompany()
    {
        return $this->company_id;
    }

    /**
     * @param $taskID
     * @return ExportDocumentsJob
     */
    public function newJob($taskID)
    {
        return (new ExportDocumentsJob(['task_id' => $taskID]));
    }

    /**
     * @return bool|int
     * @throws \Throwable
     */
    public function createTask()
    {
        $task = new Export([
            'user_id' => $this->user_id,
            'company_id' => $this->company_id,
            'source_id' => Export::SOURCE_1C_FRESH,
            'period_start_date' => $this->date_from,
            'period_end_date' => $this->date_to,
            // export options
            'io_type_in' => 1,
            'io_type_out' => 1,
            'contractor' => 0,
            'product_and_service' => 0,
            'io_type_in_items' => [
                Documents::DOCUMENT_ACT,
                Documents::DOCUMENT_INVOICE,
                Documents::DOCUMENT_PACKING_LIST,
                Documents::DOCUMENT_INVOICE_FACTURE,
                Documents::DOCUMENT_UPD,
            ],
            'io_type_out_items' => [
                Documents::DOCUMENT_ACT,
                Documents::DOCUMENT_INVOICE,
                Documents::DOCUMENT_PACKING_LIST,
                Documents::DOCUMENT_INVOICE_FACTURE,
                Documents::DOCUMENT_UPD,
            ],
            'contractor_items' => ""
        ]);

        if (!$task->save()) {
            $this->addError(null, 'Не удалось создать задание ' . serialize($task->getErrors()));

            return false;
        }

        if (!Yii::$app->queue->push($this->newJob($task->id))) {
            $task->delete();
            $this->addError(null, 'Не удалось добавить задание в очередь');
            return false;
        }

        return $task->id;
    }
}