<?php
namespace frontend\modules\fresh\models;

use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use Yii;
use common\models\Company;
use common\components\fileUpload\FileHelper;
use frontend\modules\import\models\Import1c;
use frontend\modules\fresh\jobs\ImportDocumentsJob;

/**
 * Class ImportForm
 * @package frontend\modules\fresh\models
 *
 * @var $inn
 * @var $date_from
 * @var $date_to
 *
 */
class ImportForm extends \yii\base\Model {

    public static $allowedRoles = [
        EmployeeRole::ROLE_CHIEF,
        EmployeeRole::ROLE_ACCOUNTANT,
        EmployeeRole::ROLE_FOUNDER
    ];

    /**
     * client attributes
     */
    public $inn;

    /**
     * server attributes
     */
    private $user_id;
    private $company_id;
    private $filename;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inn'], 'required'],
            [['inn'], 'string', 'min' => 10, 'max' => 12],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inn' => 'ИНН',
            'company_id' => 'Компания',
        ];
    }

    /**
     * @param $employeeId
     * @return bool
     */
    public function setCompany($employeeId)
    {
        $allowedCompaniesIds = EmployeeCompany::find()
            ->where([
                'employee_id' => $employeeId,
                'employee_role_id' => self::$allowedRoles
            ])
            ->select(['company_id'])
            ->column();

        /** @var $company Company */
        $company = Company::find()->where([
            'id' => $allowedCompaniesIds,
            'inn' => $this->inn,
        ])->one();

        if (!$company) {
            $this->addError(null, 'Компания не найдена');
            return false;
        }

        if ($company->blocked) {
            $this->addError(null, 'Компания заблокирована');
            return false;
        }

        $this->user_id = $employeeId;
        $this->company_id = $company->id; 
        
        return true;
    }

    /**
     * @return int|null
     */
    public function getCompany()
    {
        return $this->company_id;
    }

    /**
     * @param $maxCount
     * @return bool
     */
    public function checkUnfinishedImports($maxCount)
    {
        $unfinishedCount = Import1c::find()->where([
            'company_id' => $this->company_id,
            'is_completed' => 0,
            'source_id' => Import1c::SOURCE_1C_FRESH
        ])->count();

        if ($unfinishedCount >= $maxCount) {
            $this->addError(null, 'Ваши файлы еще загружаются. Повторите попытку позднее.');
            return false;
        }
        
        return true;
    }

    /**
     * @param $taskID
     * @return ImportDocumentsJob
     */
    public function newJob($taskID)
    {
        return (new ImportDocumentsJob(['task_id' => $taskID]));
    }

    /**
     * @return bool|int
     * @throws \Throwable
     */
    public function createTask()
    {
        $task = new Import1c([
            'employee_id' => $this->user_id,
            'company_id' => $this->company_id,
            'source_id' => Import1C::SOURCE_1C_FRESH,
            'filename' => $this->filename,
            'created_at' => time(),
            'period' => 'Идет загрузка...',
            'total_contractors' => 0,
            'total_products' => 0,
            'total_in_invoices' => 0,
            'total_in_upds' => 0,
            'total_in_invoice_factures' => 0,
            'total_out_invoices' => 0,
            'total_out_upds' => 0,
            'total_out_invoice_factures' => 0,
            'errors_messages' => '',
            'is_completed' => 0,
            'is_autoload' => 0,
        ]);
        
        if (!$task->save()) {
            $this->addError(null, 'Не удалось создать задание ' . serialize($task->getErrors()));

            return false;
        }

        if (!Yii::$app->queue->push($this->newJob($task->id))) {
            $task->delete();
            $this->addError(null, 'Не удалось добавить задание в очередь');
            return false;
        }

        return $task->id;
    }

    /**
     * @return string|null
     */
    public function moveTmpFile()
    {
        $now = time();
        $filename = "import_{$this->company_id}_{$now}.xml";
        $filepath = Fresh::getWorkingPath();

        try {
            if (!is_dir($filepath)) {
                FileHelper::createDirectory($filepath);
            }
            $fp = fopen($filepath . DIRECTORY_SEPARATOR . $filename, "w");
            fwrite($fp, file_get_contents('php://input'));
            $this->filename = $filename;

            return true;

        } catch (\Exception $e) {
            $this->addError(null, 'Не удалось сохранить файл');
        }

        return false;
    }
}