<?php

namespace frontend\modules\fresh;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\fresh\controllers';

    public function init()
    {
        parent::init();
    }
}
