<?php
namespace frontend\modules\fresh\controllers;

use Yii;
use common\models\User;
use yii\web\HttpException;
use frontend\rbac\UserRole;
use frontend\modules\fresh\models\Log;
use frontend\modules\import\models\Import1c;
use frontend\modules\fresh\models\ImportForm;
use frontend\modules\fresh\models\Fresh;

class ImportController extends \yii\rest\Controller {

    const MAX_UNFINISHED_IMPORTS = 3;

    /**
     * @var User
     */
    protected $user;

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpHeaderAuth',
            ],
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            UserRole::ROLE_CHIEF,
                            UserRole::ROLE_ACCOUNTANT,
                            UserRole::ROLE_FOUNDER,
                        ],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @param $action
     * @return bool
     * @throws HttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        parent::beforeAction($action);

        $this->user = Yii::$app->user;

        if (!$this->user || $this->user->isGuest)
            throw new HttpException(403);

        return true;
    }

    public function actionFile($inn)
    {
        $form = new ImportForm([
            'inn' => $inn
        ]);

        if ($form->validate()) {
            if ($form->setCompany($this->user->id)) {
                if ($form->checkUnfinishedImports(self::MAX_UNFINISHED_IMPORTS)) {
                    if ($form->moveTmpFile()) {

                        if ($taskID = $form->createTask()) {

                            Log::add($taskID, __METHOD__);
                            Fresh::clearWorkingPath($form);

                            return [
                                'success' => 1,
                                'task_id' => $taskID
                            ];
                        }
                    }
                }
            }
        }

        return $this->throwValidationError($form);
    }

    /**
     * @param $task_id
     * @return array
     * @throws HttpException
     */
    public function actionStatus($task_id)
    {
        $task = Import1c::findOne(['id' => $task_id, 'employee_id' => $this->user->id]);

        if ($task) {
            return (!$task->is_completed) ?
                ['success' => 0, 'status' => 'Выполняется'] :
                ['success' => 1, 'status' => 'Завершено'];
        }

        return $this->throwError(404, 'ID не найден');
    }




    /**
     * @param ImportForm $form
     * @throws HttpException
     */
    public function throwValidationError(ImportForm $form)
    {
        $_validationErrors = array_values(
            $form->getFirstErrors()
        );

        Log::add(reset($_validationErrors), __METHOD__);

        throw new HttpException(400, reset($_validationErrors));
    }

    /**
     * @param $status
     * @param $message
     * @throws HttpException
     */
    public function throwError($status, $message)
    {
        Log::add($message, __METHOD__);

        throw new HttpException($status, $message);
    }
}