<?php
namespace frontend\modules\fresh\controllers;

use Yii;
use common\models\User;
use yii\web\HttpException;
use frontend\rbac\UserRole;
use frontend\modules\fresh\models\Log;
use frontend\modules\fresh\models\Fresh;
use frontend\modules\fresh\models\ExportForm;
use frontend\modules\export\models\export\Export;

class ExportController extends \yii\rest\Controller {

    /**
     * @var User
     */
    protected $user;

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpHeaderAuth',
            ],
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            UserRole::ROLE_CHIEF,
                            UserRole::ROLE_ACCOUNTANT,
                            UserRole::ROLE_FOUNDER,
                        ],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @param $action
     * @return bool
     * @throws HttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        parent::beforeAction($action);

        $this->user = Yii::$app->user;

        if (!$this->user || $this->user->isGuest)
            throw new HttpException(403);

        return true;
    }

    /**
     * @param string $inn
     * @param string $date_from
     * @param string $date_to
     * @return array
     * @throws HttpException
     * @throws \Throwable
     */
    public function actionRequest($inn, $date_from, $date_to)
    {
        $form = new ExportForm([
            'inn' => $inn,
            'date_from' => $date_from,
            'date_to' => $date_to,
        ]);

        if ($form->validate()) {
            if ($form->setCompany($this->user->id)) {
                if ($taskID = $form->createTask()) {

                    Log::add($taskID, __METHOD__);
                    Fresh::clearWorkingPath($form);

                    return [
                        'success' => 1,
                        'task_id' => $taskID
                    ];
                }
            }
        }

        return $this->throwValidationError($form);
    }

    /**
     * @param $task_id
     * @return array
     * @throws HttpException
     */
    public function actionStatus($task_id)
    {
        $task = Export::findOne(['id' => $task_id, 'user_id' => $this->user->id]);

        if ($task) {
            return ($task->status == Export::STATUS_IN_PROGRESS) ?
                ['success' => 0, 'status' => 'Выполняется'] :
                ['success' => 1, 'status' => 'Завершено'];
        }

        return $this->throwError(404, 'ID не найден');
    }

    /**
     * @param $task_id
     * @return string|void
     * @throws HttpException
     */
    public function actionResult($task_id)
    {
        $task = Export::findOne(['id' => $task_id, 'user_id' => $this->user->id]);

        if ($task) {
            if ($task->status == Export::STATUS_COMPLETED) {

                $file = Fresh::getWorkingPath() . DIRECTORY_SEPARATOR . $task->filename;

                if (file_exists($file)) {

                    Log::add('Ok', __METHOD__);
                    return \Yii::$app->response->sendFile($file, $task->filename, ['mimeType' => 'text/xml'])->send();

                } else {

                    return $this->throwError(404, 'Файл не найден');
                }
            } else {

                return $this->throwError(404, 'Файл в обработке');
            }
        }

        return $this->throwError(404, 'ID не найден');
    }

    /**
     * @param ExportForm $form
     * @throws HttpException
     */
    public function throwValidationError(ExportForm $form)
    {
        $_validationErrors = array_values(
            $form->getFirstErrors()
        );

        Log::add(reset($_validationErrors), __METHOD__);

        throw new HttpException(400, reset($_validationErrors));
    }

    /**
     * @param $status
     * @param $message
     * @throws HttpException
     */
    public function throwError($status, $message)
    {
        Log::add($message, __METHOD__);

        throw new HttpException($status, $message);
    }
}