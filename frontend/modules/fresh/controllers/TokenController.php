<?php

namespace frontend\modules\fresh\controllers;

use frontend\modules\fresh\models\Log;
use Yii;
use yii\helpers\ArrayHelper;
use common\models\employee\Employee;

/**
 * Class TokenController
 */
class TokenController extends \yii\rest\Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBasicAuth',
                'auth' => function ($username, $password) {
                    if ($username && $password) {
                        $user = Employee::findIdentityByLogin($username);
                        if ($user !== null && $user->validatePassword($password)) {
                            return $user;
                        }
                    }

                    return null;
                },
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'index' => ['GET'],
        ];
    }

    /**
     * @return array
     */
    public function actionIndex()
    {
        Log::add('Ok', __METHOD__);

        return [
            'access_token' => Yii::$app->user->identity->getAccessToken()
        ];
    }
}