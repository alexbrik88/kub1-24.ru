<?php

namespace frontend\modules\fresh\jobs;

use Yii;
use yii\queue\Queue;
use yii\base\Exception;
use yii\base\BaseObject;
use yii\queue\JobInterface;
use frontend\modules\fresh\models\Fresh;
use frontend\modules\fresh\models\Log;
use frontend\modules\import\models\Import1c;
use frontend\modules\import\models\OneSConverter;
use frontend\modules\import\models\OneSParser;

class ImportDocumentsJob extends BaseObject implements JobInterface
{
    public $task_id;

    /**
     * @param Queue $queue
     * @return mixed|void
     * @throws \Exception
     */
    public function execute($queue)
    {
        Yii::$app->urlManager->baseUrl = '/';

        if (YII_ENV_PROD) {
            ini_set('memory_limit', '4096M');
        }

        /** @var Import1C $task */
        $task = Import1c::findOne($this->task_id);

        if (!$task) {
            $message = 'Task: ' . $task->id . ', задание не найдено';
            Log::add($message, __METHOD__);
            return;
        }

        $filepath = Fresh::getWorkingPath();
        $filename = $task->filename;
        $file = $filepath . DIRECTORY_SEPARATOR . $filename;

        if (is_file($file)) {

            $parser = new OneSParser($task->employee, $task->company);
            $converter = new OneSConverter($task->employee, $task->company);
            $converter->data = &$parser->data;
            $converter->importPeriodStart = &$parser->importPeriodStart;
            $converter->importPeriodEnd = &$parser->importPeriodEnd;

            try {
                // parse XML
                $parser->open($file);
                $parser->parse();
                $parser->close();
            } catch (Exception $e) {

                $task->updateAttributes([
                    'is_completed' => 1,
                    'errors_messages' => $e->getMessage(),
                    'period' => $parser->importPeriodStart .' - '. $parser->importPeriodEnd,
                ]);

                Log::add('Task: ' . $task->id . ', ошибки парсинга: ' . $e->getMessage(), __METHOD__);

                return;
            }

            // preload
            $converter->generateInvoicesFromUpds(OneSConverter::IN_INVOICE, OneSConverter::IN_UPD);
            $converter->generateInvoicesFromUpds(OneSConverter::OUT_INVOICE, OneSConverter::OUT_UPD);
            $converter->splitContractorsByType();

            try {

                Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS=0;')->execute();

                // save dicts
                $converter->insertContractors();
                $converter->insertAgreements();
                $converter->insertStores();
                $converter->insertProductGroups();
                $converter->insertProducts();
                // in docs
                $converter->insertInvoices(OneSConverter::IN_INVOICE);
                $converter->insertUpds(OneSConverter::IN_UPD);
                $converter->insertInvoiceFactures(OneSConverter::IN_INVOICE_FACTURE);
                // out docs
                $converter->insertInvoices(OneSConverter::OUT_INVOICE);
                $converter->insertUpds(OneSConverter::OUT_UPD);
                $converter->insertInvoiceFactures(OneSConverter::OUT_INVOICE_FACTURE);
                // products prices
                $converter->updateProductsPrices();

                Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS=1;')->execute();

            } catch (\Throwable $e) {

                $task->updateAttributes([
                    'is_completed' => 1,
                    'errors_messages' => $e->getMessage(),
                    'period' => $parser->importPeriodStart .' - '. $parser->importPeriodEnd,
                ]);

                Log::add('Task: ' . $task->id . ', ошибки конвертации: ' . $e->getMessage(), __METHOD__);

                return;
            }

            // SUCCESS

            $successErrors = implode("\n", $converter->getErrors());

            $task->updateAttributes([
                'is_completed' => 1,
                'errors_messages' => mb_substr($successErrors, 0, 2E4, 'UTF-8'),
                'period' => $parser->importPeriodStart .' - '. $parser->importPeriodEnd,
                'total_contractors' => $converter->stat[OneSConverter::CONTRACTOR],
                'total_products' => $converter->stat[OneSConverter::NOMENCLATURE],
                'total_in_invoices' => $converter->stat[OneSConverter::IN_INVOICE],
                'total_in_upds' => $converter->stat[OneSConverter::IN_UPD],
                'total_in_invoice_factures' => $converter->stat[OneSConverter::IN_INVOICE_FACTURE],
                'total_out_invoices' => $converter->stat[OneSConverter::OUT_INVOICE],
                'total_out_upds' => $converter->stat[OneSConverter::OUT_UPD],
                'total_out_invoice_factures' => $converter->stat[OneSConverter::OUT_INVOICE_FACTURE],
            ]);

            Log::add('Task: ' . $task->id . ', загружено: ' . $successErrors, __METHOD__);

        } else {
            $task->updateAttributes([
                'is_completed' => 1,
                'errors_messages' => 'Файл не найден'
            ]);

            Log::add('Task: ' . $task->id . ', файл не найден', __METHOD__);
        }
    }
}