<?php

namespace frontend\modules\fresh\jobs;

use frontend\modules\export\models\export\Export;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\modules\fresh\models\Fresh;
use frontend\modules\fresh\models\Log;
use yii\queue\Queue;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class ExportDocumentsJob extends BaseObject implements JobInterface
{
    public $task_id;

    /**
     * @param Queue $queue
     * @return mixed|void
     * @throws \Exception
     */
    public function execute($queue)
    {
        if (YII_ENV_PROD) {
            ini_set('memory_limit', '4096M');
        }

        /** @var Export $task */
        $task = Export::findOne($this->task_id);

        if (!$task) {
            $message = 'Task: ' . $task->id . ', задание не найдено';
            Log::add($message, __METHOD__);
            return;
        }

        $oneCExport = new OneCExport($task);
        $filepath = Fresh::getWorkingPath();
        $filename = "export_{$task->company_id}_{$this->task_id}.xml";

        if ($oneCExport->findDataObjects()) {
            if ($oneCExport->createTemplate($filepath, $filename)) {

                $task->updateAttributes([
                    'status' => 1,
                    'filename' => $filename
                ]);

                Log::add('Task: ' . $task->id . ', файл создан: ' . $filename, __METHOD__);
            } else {
                Log::add('Task: ' . $task->id . ', не удалось создать файл', __METHOD__);
            }
        } else {
            Log::add('Task: ' . $task->id . ', нет объектов в выбранном периоде', __METHOD__);
        }
    }
}