<?php

namespace frontend\modules\edm;

use common\models\employee\Employee;
use Yii;
use yii\base\Module as BaseModule;
use yii\filters\AccessControl;

class Module extends BaseModule
{
    /**
     * @inheritDoc
     */
    public $controllerNamespace = 'frontend\modules\edm\controllers';
}
