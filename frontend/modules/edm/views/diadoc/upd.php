<?php

namespace frontend\modules\edm\views;

use common\models\document\Upd;
use frontend\modules\edm\models\DocumentBuilder;
use frontend\modules\edm\models\DocumentDecorator;

/**
 * @var Upd $model
 */

$builder = DocumentBuilder::createInstance(DocumentDecorator::createInstance($model), 'СЧФДОП');

echo $builder->buildDocument();
