<?php

namespace frontend\modules\edm\views;

use common\models\document\InvoiceFacture;
use frontend\modules\edm\models\DocumentBuilder;
use frontend\modules\edm\models\DocumentDecorator;

/**
 * @var InvoiceFacture $model
 */

$builder = DocumentBuilder::createInstance(DocumentDecorator::createInstance($model), 'СЧФ');

echo $builder->buildDocument();
