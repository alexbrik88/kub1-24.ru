<?php

namespace frontend\modules\edm\views;

use common\models\document\Act;
use frontend\modules\edm\models\DocumentBuilder;
use frontend\modules\edm\models\DocumentDecorator;

/**
 * @var Act $model
 */

$builder = DocumentBuilder::createInstance(DocumentDecorator::createInstance($model), 'ДОП');

echo $builder->buildDocument();
