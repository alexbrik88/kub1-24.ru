<?php

namespace frontend\modules\edm\models;

interface RepositoryFactoryInterface
{
    /**
     * @param string $class
     * @param mixed[] $attributes
     * @return RepositoryInterface
     */
    public function createRepository(string $class, array $attributes = []): RepositoryInterface;
}
