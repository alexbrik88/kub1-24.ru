<?php

namespace frontend\modules\edm\models;

use common\models\company\CompanyType;
use common\models\Contractor;
use yii\base\InvalidConfigException;
use yii\base\Model;

class ContractorNames extends Model
{
    /**
     * @var Contractor
     */
    public $contractor;

    /**
     * @var string
     */
    public $lastName = '';

    /**
     * @var string
     */
    public $firstName = '';

    /**
     * @var string
     */
    public $patronymicName = '';

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->contractor)) {
            throw new InvalidConfigException();
        }

        $this->fetchNames();
    }

    /**
     * @param Contractor $contractor
     * @return static
     */
    public static function createInstance(Contractor $contractor): self
    {
        return new static(compact('contractor'));
    }

    /**
     * @return void
     */
    private function fetchNames(): void
    {
        if ($this->contractor->face_type == Contractor::TYPE_PHYSICAL_PERSON) {
            $this->lastName = $this->contractor->physical_lastname;
            $this->firstName = $this->contractor->physical_firstname;
            $this->patronymicName = $this->contractor->physical_patronymic;
        } elseif ($this->contractor->face_type == Contractor::TYPE_LEGAL_PERSON) {
            if ($this->contractor->company_type_id == CompanyType::TYPE_IP) {
                $names = explode(' ', $this->contractor->name);
                $this->lastName = $names[0] ?? '';
                $this->firstName = $names[1] ?? '';
                $this->patronymicName = $names[2] ?? '';
            }
        }
    }
}
