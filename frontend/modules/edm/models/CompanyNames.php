<?php

namespace frontend\modules\edm\models;

use common\models\company\CompanyType;
use common\models\Company;
use yii\base\InvalidConfigException;
use yii\base\Model;

class CompanyNames extends Model
{
    /**
     * @var Company
     */
    public $company;

    /**
     * @var string
     */
    public $lastName = '';

    /**
     * @var string
     */
    public $firstName = '';

    /**
     * @var string
     */
    public $patronymicName = '';

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->company)) {
            throw new InvalidConfigException();
        }

        $this->fetchNames();
    }

    /**
     * @param Company $company
     * @return static
     */
    public static function createInstance(Company $company): self
    {
        return new static(compact('company'));
    }

    /**
     * @return void
     */
    private function fetchNames(): void
    {
        if ($this->company->company_type_id == CompanyType::TYPE_IP) {
            $this->lastName = $this->company->ip_lastname;
            $this->firstName = $this->company->ip_firstname;
            $this->patronymicName = $this->company->ip_patronymic;
        }
    }
}
