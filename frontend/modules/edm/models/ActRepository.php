<?php

namespace frontend\modules\edm\models;

use common\models\document\Act;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

class ActRepository extends Model implements RepositoryInterface
{
    /**
     * @var int
     */
    public $company_id;

    /**
     * @inheritDoc
     */
    public function getById(string $id): ?ActiveRecord
    {
        return $this->getQuery()->andWhere([Act::tableName() . '.id' => $id])->one();
    }

    /**
     * @inheritDoc
     */
    public function getQuery(): ActiveQuery
    {
        return Act::find()
            ->joinWith('invoice')
            ->andWhere(['invoice.company_id' => $this->company_id]);
    }
}
