<?php

namespace frontend\modules\edm\models;

use DOMDocument;
use DOMElement;
use DOMNode;

class ElementFactory implements ElementFactoryInterface
{
    /**
     * @var DOMDocument
     */
    private $document;

    /**
     * @param DOMDocument $document
     */
    public function __construct(DOMDocument $document)
    {
        $this->document = $document;
        $this->document->formatOutput = true;
    }

    /**
     * @inheritDoc
     */
    public function createElement(DOMNode $parent, string $name, array $attributes = [], bool $noEmpty = true): DOMElement
    {
        $element = $this->document->createElement($name);

        if ($noEmpty) {
            $attributes = array_filter($attributes);
        }

        foreach ($attributes as $key => $value) {
            $element->setAttribute($key, $value);
        }

        $parent->appendChild($element);

        return $element;
    }
}
