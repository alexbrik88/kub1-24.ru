<?php

namespace frontend\modules\edm\models;

use common\models\document\InvoiceFacture;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

class InvoiceFactureRepository extends Model implements RepositoryInterface
{
    /**
     * @var int
     */
    public $company_id;

    /**
     * @inheritDoc
     */
    public function getById(string $id): ?ActiveRecord
    {
        return $this->getQuery()->andWhere([InvoiceFacture::tableName() . '.id' => $id])->one();
    }

    /**
     * @inheritDoc
     */
    public function getQuery(): ActiveQuery
    {
        return InvoiceFacture::find()
            ->joinWith('invoice')
            ->andWhere(['invoice.company_id' => $this->company_id]);
    }
}
