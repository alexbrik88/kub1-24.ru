<?php

namespace frontend\modules\edm\models;

use common\models\Contractor;
use common\models\document\AbstractDocument;
use common\models\document\AbstractOrder;
use common\models\document\BasisDocumentInterface;
use common\models\document\Invoice;
use common\models\employee\Employee;
use common\models\product\Product;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\db\ActiveRecord;

class DocumentDecorator extends Model
{
    /**
     * @var AbstractDocument
     */
    public $document;

    /**
     * @var Invoice
     */
    public $invoice;

    /**
     * @param AbstractDocument $document
     * @return static
     */
    public static function createInstance(AbstractDocument $document): self
    {
        return new static(compact('document'));
    }

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!($this->document instanceof AbstractDocument)) {
            throw new InvalidConfigException();
        }

        if ($this->hasDocumentProperty('invoice')) {
            $this->invoice = $this->getDocumentProperty('invoice');
        }

        if (!($this->invoice instanceof Invoice)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @return bool
     */
    public function hasWaybill(): bool
    {
        if ($this->document->hasAttribute('waybill_number') && $this->document->hasAttribute('waybill_date')) {
            if ($this->document->getAttribute('waybill_number') && $this->document->getAttribute('waybill_date')) {
                return true;
            }
        }

        return true;
    }

    /**
     * @return string
     */
    public function getWaybillDate(): string
    {
        return (string) $this->document->getAttribute('waybill_date');
    }

    /**
     * @return string
     */
    public function getWaybillNumber(): string
    {
        return (string) $this->document->getAttribute('waybill_number');
    }

    /**
     * @return AbstractOrder[]
     */
    public function getOrders(): array
    {
        /** @var AbstractOrder[] $orders */
        $orders = $this->getDocumentProperty('ownOrders');

        return $orders;
    }

    /**
     * @return bool
     */
    public function hasGoods(): bool
    {
        foreach ($this->getOrders() as $index => $order) {
            $type = $order->order->product->production_type;

            if (in_array($type, [Product::PRODUCTION_TYPE_ALL, Product::PRODUCTION_TYPE_GOODS])) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function hasServices(): bool
    {
        foreach ($this->getOrders() as $index => $order) {
            $type = $order->order->product->production_type;

            if (in_array($type, [Product::PRODUCTION_TYPE_ALL, Product::PRODUCTION_TYPE_SERVICE])) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return BasisDocumentInterface|null
     */
    public function getBasicDocument(): ?BasisDocumentInterface
    {
        if ($this->document instanceof BasisDocumentInterface && $this->document->hasBasisDocumentParams()) {
            return $this->document;
        }

        if ($this->invoice->hasBasisDocumentParams()) {
            return $this->invoice;
        }

        return null;
    }

    /**
     * @return ActiveRecord[]
     */
    public function getPaymentDocuments(): array
    {
        if ($this->hasDocumentProperty('paymentDocuments')) {
            /** @var ActiveRecord[] $documents */
            $documents = $this->getDocumentProperty('paymentDocuments');

            return $documents;
        }

        return [];
    }

    /**
     * @return string
     */
    public function getObjectGuid(): string
    {
        if ($this->hasDocumentProperty('object_guid') && $this->getDocumentProperty('object_guid')) {
            return $this->getDocumentProperty('object_guid');
        }

        if ($this->invoice->object_guid) {
            return $this->invoice->object_guid;
        }

        return '';
    }

    /**
     * @return float
     */
    public function getTotalNds(): float
    {
        return $this->getDocumentProperty('totalNds') / 100;
    }

    /**
     * @return Contractor|null
     */
    public function getConsignor(): ?Contractor
    {
        if ($this->hasDocumentProperty('consignor')) {
            return $this->getDocumentProperty('consignor');
        }

        return null;
    }

    /**
     * @return Contractor
     */
    public function getConsignee(): Contractor
    {
        if ($this->hasDocumentProperty('consignee') && $this->getDocumentProperty('consignee')) {
            return $this->getDocumentProperty('consignee');
        }

        return $this->invoice->contractor;
    }

    /**
     * @return string|null
     */
    public function getStateContractNumber(): ?string
    {
        if ($this->hasDocumentProperty('state_contract')) {
            return $this->getDocumentProperty('state_contract');
        }

        return null;
    }

    /**
     * @return Employee|null
     */
    public function getSigner(): ?Employee
    {
        if ($this->hasProperty('signedByEmployee')) {
            return $this->getDocumentProperty('signedByEmployee');
        }

        if ($this->invoice->employeeSignature) {
            return $this->invoice->employeeSignature->employee;
        }

        return null;
    }

    /**
     * @param string $name
     * @return mixed
     */
    private function getDocumentProperty(string $name)
    {
        return $this->document->$name;
    }

    /**
     * @param string $name
     * @return bool
     */
    private function hasDocumentProperty(string $name): bool
    {
        return ($this->document->hasAttribute($name) || $this->document->canGetProperty($name));
    }
}
