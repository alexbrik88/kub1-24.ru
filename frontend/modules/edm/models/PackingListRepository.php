<?php

namespace frontend\modules\edm\models;

use common\models\document\PackingList;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

class PackingListRepository extends Model implements RepositoryInterface
{
    /**
     * @var int
     */
    public $company_id;

    /**
     * @inheritDoc
     */
    public function getById(string $id): ?ActiveRecord
    {
        return $this->getQuery()->andWhere([PackingList::tableName() . '.id' => $id])->one();
    }

    /**
     * @inheritDoc
     */
    public function getQuery(): ActiveQuery
    {
        return PackingList::find()
            ->joinWith('invoice')
            ->andWhere(['invoice.company_id' => $this->company_id]);
    }
}
