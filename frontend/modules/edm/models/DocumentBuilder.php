<?php

namespace frontend\modules\edm\models;

use common\components\date\DateHelper;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\TaxRate;
use DOMDocument;
use DOMElement;
use DOMNode;
use yii\base\InvalidConfigException;
use yii\base\Model;

class DocumentBuilder extends Model
{
    /** @var string */
    private const WITHOUT_VAT_TEXT = 'без НДС';

    /**
     * @var DocumentDecorator
     */
    public $model;

    /**
     * @var string
     */
    public $function;

    /**
     * @var DOMDocument
     */
    private $dom;

    /**
     * @var ElementFactory
     */
    private $factory;

    /**
     * @param DocumentDecorator $model
     * @param string $function
     * @return static
     */
    public static function createInstance(DocumentDecorator $model, string $function): self
    {
        return new static(compact('model', 'function'));
    }

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        $this->dom = new DOMDocument('1.0', 'windows-1251');
        $this->factory = new ElementFactory($this->dom);

        if (!($this->model instanceof DocumentDecorator) || !is_string($this->function)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @return string
     */
    public function buildDocument(): string
    {
        $root = $this->addRoot();
        $body = $this->addBody($root);
        $invoice = $this->addInvoice($body);

        $this->addSeller($invoice);
        $this->addConsignor($invoice);
        $this->addConsignee($invoice);
        $this->addPaymentDocuments($invoice);
        $this->addCustomer($invoice);
        $this->addContract($invoice);

        $this->addTable($body);
        $this->addReason($body);
        $this->addSigner($body);

        return $this->dom->saveXML();
    }

    /**
     * @return DOMElement
     */
    private function addRoot(): DOMElement
    {
        $root = $this->factory->createElement($this->dom,  'Файл', [
            'ИдФайл' => $this->model->getObjectGuid(),
            'ВерсФорм' =>'5.01',
            'ВерсПрог' => 'kub24',
        ]);

        $element = $this->factory->createElement($root, 'СвУчДокОбор', ['ИдОтпр' => '', 'ИдПол' => ''], false);
        $this->factory->createElement($element, 'СвОЭДОтпр', [
            'НаимОрг' => 'АО "ПФ "СКБ Контур"',
            'ИННЮЛ' => '5904303287',
            'ИдЭДО' => '2BM',
        ]);

        return $root;
    }

    /**
     * @param DOMNode $parent
     * @return DOMElement
     */
    private function addBody(DOMNode $parent): DOMElement
    {
        $body = $this->factory->createElement($parent, 'Документ', [
            'КНД' => '1115131',
            'Функция' => $this->function,
            'ДатаИнфПр' => DateHelper::format($this->model->document->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'ВремИнфПр' => date('H.i.s', $this->model->document->created_at),
            'НаимЭконСубСост' => sprintf(
                '%s, ИНН/КПП, %s/%s',
                $this->model->invoice->company_name_short,
                $this->model->invoice->company_inn,
                $this->model->invoice->company_kpp
            ),
        ]);

        return $body;
    }

    /**
     * @param DOMNode $parent
     * @return DOMElement
     */
    private function addInvoice(DOMNode $parent): DOMElement
    {
        $invoice = $this->factory->createElement($parent, 'СвСчФакт', [
            'НомерСчФ' => $this->model->document->fullNumber,
            'ДатаСчФ' => DateHelper::format($this->model->document->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'КодОКВ' => '643', // TODO
        ], false);

        return $invoice;
    }

    /**
     * @param DOMNode $parent
     * @return void
     */
    private function addSeller(DOMNode $parent): void
    {
        $factory = $this->factory;
        $invoice = $this->model->invoice;
        $company = $this->model->invoice->company;
        $info = $factory->createElement($parent, 'СвПрод', ['ОКПО' => $company->okpo]);
        $element = $factory->createElement($info, 'ИдСв');

        if ($company->company_type_id == CompanyType::TYPE_IP) {
            $element = $factory->createElement($element, 'СвИП', ['ИННФЛ' => $company->inn]);

            $factory->createElement($element, 'ФИО', [
                'Фамилия' => $company->ip_lastname,
                'Имя' => $company->ip_firstname,
                'Отчество' => $company->ip_patronymic,
            ]);
        } else {
            $factory->createElement($element, 'СвЮЛУч', [
                'НаимОрг' => $company->getTitle(true),
                'ИННЮЛ' => $company->inn,
                'КПП' => $company->kpp,
            ]);
        }

        $factory->createElement($factory->createElement($info, 'Адрес'), 'АдрИнф', [
            'КодСтр' => '643', // TODO
            'АдрТекст' => $company->address_legal,
        ]);

        $element = $factory->createElement($info, 'БанкРекв', ['НомерСчета' => $invoice->company_rs]);
        $factory->createElement($element, 'СвБанк', [
            'НаимБанк' => $invoice->company_bank_name,
            'БИК' => $invoice->company_bik,
            'КорСчет' => $invoice->company_ks,
        ]);
    }

    /**
     * @param DOMNode $parent
     * @return void
     */
    private function addCustomer(DOMNode $parent): void
    {
        $factory = $this->factory;
        $invoice = $this->model->invoice;
        $customer = $invoice->contractor;
        $info = $factory->createElement($parent, 'СвПокуп', ['ОКПО' => $customer->okpo]);
        $this->addContractor($info, $customer);
        $element = $factory->createElement($info, 'БанкРекв', ['НомерСчета' => $invoice->contractor_rs]);
        $factory->createElement($element, 'СвБанк', [
            'НаимБанк' => $invoice->contractor_bank_name,
            'БИК' => $invoice->contractor_bik,
            'КорСчет' => $invoice->contractor_ks,
        ]);
    }

    /**
     * @param DOMNode $parent
     * @return void
     */
    private function addConsignor(DOMNode $parent): void
    {
        $factory = $this->factory;
        $info = $factory->createElement($parent, 'ГрузОт');
        $consignor = $this->model->getConsignor();

        if ($consignor) {
            $this->addContractor($factory->createElement($info, 'ГрузОтпр', ['ОКПО' => $consignor->okpo]), $consignor);
        }  else {
            $factory->createElement($info, 'ОнЖе')->textContent = 'он же';
        }
    }

    /**
     * @param DOMNode $parent
     * @return void
     */
    private function addConsignee(DOMNode $parent): void
    {
        $factory = $this->factory;
        $consignee = $this->model->getConsignee();
        $info = $factory->createElement($parent, 'ГрузПолуч', ['ОКПО' => $consignee->okpo]);
        $this->addContractor($info, $consignee);
    }

    /**
     * @param DOMNode $parent
     * @return void
     */
    private function addContract(DOMNode $parent): void
    {
        $number = $this->model->getStateContractNumber();

        if ($number) {
            $this->factory->createElement($parent, 'ДопСвФХЖ1', [
                'ИдГосКон' => $number,
                'НаимОКВ' => 'Российский рубль',
            ]);
        }
    }

    /**
     * @param DOMNode $parent
     * @return void
     */
    private function addTable(DOMNode $parent): void
    {
        $factory = $this->factory;
        $document = $this->model->document;
        $table = $factory->createElement($parent, 'ТаблСчФакт');

        foreach ($this->model->getOrders() as $index => $order) {
            $row = $factory->createElement($table, 'СведТов', [
                'НомСтр' => $index + 1,
                'НалСт' => self::getVAT($order->order->saleTaxRate),
                'ОКЕИ_Тов' => $order->order->unit ? $order->order->unit->code_okei : '',
                'НаимТов' => $order->order->product_title,
                'ЦенаТов' => $order->priceNoNds / 100,
                'СтТовБезНДС' => $document->getPrintOrderAmount($order->order_id, true) / 100,
                'СтТовУчНал' => $document->getPrintOrderAmount($order->order_id, false) / 100,
                'КолТов' => $order->quantity ?: 1,
            ]);

            $factory->createElement($factory->createElement($row, 'Акциз'), 'БезАкциз')->textContent = 'без акциза';
            $tax = $factory->createElement($row, 'СумНал');

            if ($order->order->saleTaxRate->id == TaxRate::RATE_WITHOUT) {
                $factory->createElement($tax, 'БезНДС')->textContent = self::getVAT($order->order->saleTaxRate);
            } else {
                $factory->createElement($tax, 'СумНал')->textContent = $order->order->sale_tax / 100;
            }

            if ($order->order->unit) {
                $factory->createElement($row, 'ДопСведТов', ['НаимЕдИзм' => $order->order->unit->name]);
            }
        }

        $total = $factory->createElement($factory->createElement($table, 'ВсегоОпл', [
            'СтТовБезНДСВсего' => $document->getPrintAmountNoNds() / 100,
            'СтТовУчНалВсего' => $document->getPrintAmountWithNds() / 100,
        ]), 'СумНалВсего');

        if ($this->model->getTotalNds() > 0) {
            $factory->createElement($total, 'СумНал')->textContent = $this->model->getTotalNds();
        } else {
            $factory->createElement($total, 'БезНДС')->textContent = self::WITHOUT_VAT_TEXT;
        }
    }

    /**
     * @param DOMNode $parent
     * @return void
     */
    private function addReason(DOMNode $parent): void
    {
        $factory = $this->factory;
        $company = $this->model->invoice->company;
        $details = '';

        if ($this->model->hasGoods()) {
            $details .= 'Товары переданы';
        }

        if ($this->model->hasServices()) {
            $details .= $details ? ', услуга оказана' : 'Услуга оказана';
        }

        $reason = $factory->createElement($factory->createElement($parent, 'СвПродПер'), 'СвПер', ['СодОпер' => $details]);
        $document = $this->model->getBasicDocument();
        $signer = $this->model->getSigner();

        if ($document) {
            $factory->createElement($reason, 'ОснПер', [
                'НаимОсн' => $document->getBasisDocumentName(),
                'НомОсн' => $document->getBasisDocumentNumber(),
                'ДатаОсн' => DateHelper::format(
                    $document->getBasisDocumentDate(),
                    DateHelper::FORMAT_USER_DATE,
                    DateHelper::FORMAT_DATE
                ),
            ]);
        }

        if ($signer) {
            $element = $factory->createElement($factory->createElement($reason, 'СвЛицПер'), 'РабОргПрод', [
                'Должность' => $signer->position,
            ]);

            $factory->createElement($element, 'ФИО', [
                'Фамилия' => $signer->lastname,
                'Имя' => $signer->firstname,
                'Отчество' => $signer->patronymic,
            ]);
        } else {
            $element = $factory->createElement($factory->createElement($reason, 'СвЛицПер'), 'РабОргПрод', [
                'Должность' => $company->chief_post_name,
            ]);

            $factory->createElement($element, 'ФИО', [
                'Фамилия' => $company->chief_lastname,
                'Имя' => $company->chief_firstname,
                'Отчество' => $company->chief_patronymic,
            ]);
        }

        if ($this->model->hasWaybill()) {
            $this->addWaybill($reason);
        }
    }

    /**
     * @param DOMNode $parent
     * @return void
     */
    private function addSigner(DOMNode $parent): void
    {
        $factory = $this->factory;
        $company = $this->model->invoice->company;
        $element = $factory->createElement($parent, 'Подписант', [
            'ОблПолн' => '1', // TODO
            'Статус' => '1', // TODO
            'ОснПолн' => 'Должностные обязанности', // TODO
        ]);

        if ($company->company_type_id == CompanyType::TYPE_IP) {
            $element = $factory->createElement($element, 'ИП', [
                'Должн' => $company->chief_post_name,
                'ИННФЛ' => $company->inn,
            ]);

            $factory->createElement($element, 'ФИО', [
                'Фамилия' => $company->ip_lastname,
                'Имя' => $company->ip_firstname,
                'Отчество' => $company->ip_patronymic,
            ]);
        } else {
            $element = $factory->createElement($element, 'ЮЛ', [
                'Должн' => $company->chief_post_name,
                'ИННЮЛ' => $company->inn,
            ]);

            $factory->createElement($element, 'ФИО', [
                'Фамилия' => $company->chief_accountant_lastname,
                'Имя' => $company->chief_accountant_firstname,
                'Отчество' => $company->chief_accountant_patronymic,
            ]);
        }
    }

    /**
     * @param DOMNode $parent
     * @return void
     */
    private function addPaymentDocuments(DOMNode $parent): void
    {
        foreach ($this->model->getPaymentDocuments() as $doc) {
            $this->factory->createElement($parent, 'СвПРД', [
                'НомерПРД' => $doc->getAttribute('payment_document_number'),
                'ДатаПРД' => DateHelper::format(
                    $doc->getAttribute('payment_document_date'),
                    DateHelper::FORMAT_USER_DATE,
                    DateHelper::FORMAT_DATE
                ),
            ]);
        }
    }

    /**
     * @param TaxRate $taxRate
     * @return string
     */
    private static function getVAT(TaxRate $taxRate): string
    {
        if ($taxRate->id == TaxRate::RATE_WITHOUT) {
            return self::WITHOUT_VAT_TEXT;
        }

        return $taxRate->name;
    }

    /**
     * @param DOMNode $parent
     * @param Contractor $contractor
     * @return void
     */
    private function addContractor(DOMNode $parent, Contractor $contractor): void
    {
        $factory = $this->factory;
        $info = $factory->createElement($parent, 'ИдСв');
        $contractorNames = ContractorNames::createInstance($contractor);

        if ($contractor->face_type == Contractor::TYPE_PHYSICAL_PERSON) {
            $element = $factory->createElement($info, 'СвФЛУчастФХЖ', ['ИННФЛ' => $contractor->ITN]);

            $factory->createElement($element, 'ФИО', [
                'Фамилия' => $contractorNames->lastName,
                'Имя' => $contractorNames->firstName,
                'Отчество' => $contractorNames->patronymicName,
            ]);
        } elseif ($contractor->face_type == Contractor::TYPE_LEGAL_PERSON) {
            if ($contractor->company_type_id == CompanyType::TYPE_IP) {
                $element = $factory->createElement($info, 'СвИП', ['ИННФЛ' => $contractor->ITN]);

                $factory->createElement($element, 'ФИО', [
                    'Фамилия' => $contractorNames->lastName,
                    'Имя' => $contractorNames->firstName,
                    'Отчество' => $contractorNames->patronymicName,
                ]);
            } else {
                $factory->createElement($info, 'СвЮЛУч', [
                    'НаимОрг' => $contractor->getTitle(true),
                    'ИННЮЛ' => $contractor->ITN,
                    'КПП' => $contractor->PPC,
                ]);
            }
        }

        $factory->createElement($factory->createElement($parent, 'Адрес'), 'АдрИнф', [
            'КодСтр' => '643', // TODO
            'АдрТекст' => $contractor->legal_address,
        ]);
    }

    /**
     * @param DOMNode $parent
     * @return void
     */
    private function addWaybill(DOMNode $parent): void
    {
        $factory = $this->factory;
        $date = DateHelper::format($this->model->getWaybillDate(), DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $info = $factory->createElement($parent, 'ТранГруз', [
            'СвТранГруз' => sprintf('Транспортная накладная № %s от %s', $this->model->getWaybillDate(), $date),
        ]);
        $factory->createElement($info, 'ТранНакл', [
            'НомТранНакл' => $this->model->getWaybillNumber(),
            'ДатаТранНакл' => $date,
        ]);
    }
}
