<?php

namespace frontend\modules\edm\models;

use yii\base\Configurable;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

interface RepositoryInterface extends Configurable
{
    /**
     * @param string $id
     * @return ActiveRecord|null
     */
    public function getById(string $id): ?ActiveRecord;

    /**
     * @return ActiveQuery
     */
    public function getQuery(): ActiveQuery;
}
