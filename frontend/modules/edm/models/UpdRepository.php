<?php

namespace frontend\modules\edm\models;

use common\models\document\Upd;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

class UpdRepository extends Model implements RepositoryInterface
{
    /**
     * @var int
     */
    public $company_id;

    /**
     * @inheritDoc
     */
    public function getById(string $id): ?ActiveRecord
    {
        return $this->getQuery()->andWhere([Upd::tableName() . '.id' => $id])->one();
    }

    /**
     * @inheritDoc
     */
    public function getQuery(): ActiveQuery
    {
        return Upd::find()
            ->joinWith('invoice')
            ->andWhere(['invoice.company_id' => $this->company_id]);
    }
}
