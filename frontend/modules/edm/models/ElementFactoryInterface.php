<?php

namespace frontend\modules\edm\models;

use DOMElement;
use DOMNode;

interface ElementFactoryInterface
{
    /**
     * @param DOMNode $parent
     * @param string $name
     * @param array $attributes
     * @param bool $noEmpty
     * @return DOMElement
     */
    public function createElement(DOMNode $parent, string $name, array $attributes = [], bool $noEmpty = false): DOMElement;
}
