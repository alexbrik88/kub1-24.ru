<?php

namespace frontend\modules\edm\models;

use common\models\employee\Employee;
use RuntimeException;
use Yii;

class RepositoryFactory implements RepositoryFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function createRepository(string $class, array $attributes = []): RepositoryInterface
    {
        if (!is_a($class, RepositoryInterface::class, true)) {
            throw new RuntimeException('Invalid class.');
        }

        if (property_exists($class, 'company_id')) {
            /** @var Employee $employee */
            $employee = Yii::$app->user->identity;
            $attributes['company_id'] = $employee->company->id;
        }

        /** @var RepositoryInterface $repository */
        $repository = new $class($attributes);

        return $repository;
    }
}
