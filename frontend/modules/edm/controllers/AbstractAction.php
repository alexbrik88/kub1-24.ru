<?php

namespace frontend\modules\edm\controllers;

use Yii;
use yii\base\Action;
use yii\web\Request;
use yii\web\Response;

abstract class AbstractAction extends Action
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var callable a PHP callable that will be called when running an action to determine
     * if the current user has the permission to execute the action. If not set, the access
     * check will not be performed. The signature of the callable should be as follows,
     *
     * ```php
     * function ($action, $model = null) {
     *     // $model is the requested model instance.
     * }
     * ```
     */
    public $checkAccess;

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->request = Yii::$app->request;
        $this->response = Yii::$app->response;
    }
}
