<?php

namespace frontend\modules\edm\controllers;

use frontend\modules\edm\models\ActRepository;
use frontend\modules\edm\models\RepositoryFactory;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ActAction extends AbstractAction
{
    /**
     * @param string $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function run(string $id): Response
    {
        $model = (new RepositoryFactory)
            ->createRepository(ActRepository::class)
            ->getById($id);

        if (!$model) {
            throw new NotFoundHttpException();
        }

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        $this->response->content = $this->controller->renderPartial('act', compact('model'));
        $this->response->format = Response::FORMAT_XML;
        $this->response->setDownloadHeaders('act.xml');

        return $this->response;
    }
}
