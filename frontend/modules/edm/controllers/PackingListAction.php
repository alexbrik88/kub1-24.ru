<?php

namespace frontend\modules\edm\controllers;

use frontend\modules\edm\models\PackingListRepository;
use frontend\modules\edm\models\RepositoryFactory;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class PackingListAction extends AbstractAction
{
    /**
     * @param string $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function run(string $id): Response
    {
        $model = (new RepositoryFactory)
            ->createRepository(PackingListRepository::class)
            ->getById($id);

        if (!$model) {
            throw new NotFoundHttpException();
        }

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        $this->response->content = $this->controller->renderPartial('packing-list', compact('model'));
        $this->response->format = Response::FORMAT_XML;
        $this->response->setDownloadHeaders('packing-list.xml');

        return $this->response;
    }
}
