<?php

namespace frontend\modules\edm\controllers;

use frontend\modules\edm\models\UpdRepository;
use frontend\modules\edm\models\RepositoryFactory;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class UpdAction extends AbstractAction
{
    /**
     * @param string $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function run(string $id): Response
    {
        $model = (new RepositoryFactory)
            ->createRepository(UpdRepository::class)
            ->getById($id);

        if (!$model) {
            throw new NotFoundHttpException();
        }

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        $this->response->content = $this->controller->renderPartial('upd', compact('model'));
        $this->response->format = Response::FORMAT_XML;
        $this->response->setDownloadHeaders('upd.xml');

        return $this->response;
    }
}
