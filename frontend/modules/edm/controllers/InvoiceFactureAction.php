<?php

namespace frontend\modules\edm\controllers;

use frontend\modules\edm\models\InvoiceFactureRepository;
use frontend\modules\edm\models\RepositoryFactory;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class InvoiceFactureAction extends AbstractAction
{
    /**
     * @param string $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function run(string $id): Response
    {
        $model = (new RepositoryFactory)
            ->createRepository(InvoiceFactureRepository::class)
            ->getById($id);

        if (!$model) {
            throw new NotFoundHttpException();
        }

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        $this->response->content = $this->controller->renderPartial('invoice-facture', compact('model'));
        $this->response->format = Response::FORMAT_XML;
        $this->response->setDownloadHeaders('invoice-facture.xml');

        return $this->response;
    }
}
