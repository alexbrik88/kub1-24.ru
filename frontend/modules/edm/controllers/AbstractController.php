<?php

namespace frontend\modules\edm\controllers;

use Yii;
use yii\web\Controller;

abstract class AbstractController extends \frontend\components\FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function actions()
    {
        return [
            'act' => [
                'class' => ActAction::class,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'invoice-facture' => [
                'class' => InvoiceFactureAction::class,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'packing-list' => [
                'class' => PackingListAction::class,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'upd' => [
                'class' => UpdAction::class,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

    /**
     * Checks the privilege of the current user.
     *
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string $action the ID of the action to be executed
     * @param object $model the model to be accessed. If null, it means no specific model is being accessed.
     * @param array $params additional parameters
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case 'act':
            case 'invoice-facture':
            case 'packing-list':
            case 'upd':
                $can = Yii::$app->getUser()->can(\frontend\rbac\permissions\document\Document::VIEW, [
                    'model' => $model,
                ]);
                break;
            default:
                $can = false;
                break;
        }

        if (empty($can)) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }
}
