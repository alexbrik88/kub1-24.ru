<?php

namespace frontend\modules\export\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "export_guid".
 *
 *  @property integer id
 *  @property string class_id
 *  @property integer object_id
 *  @property string guid
 */
class ExportGUID extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'export_guid';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['object_id', 'integer'],
            [['class_id', 'guid'], 'string'],
        ];
    }
}