<?php

namespace frontend\modules\export\models\export;

use common\components\pdf\PdfRenderer;
use common\models\Company;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\OrderDocument;
use common\models\document\PackingList;
use common\models\document\PaymentOrder;
use common\models\document\Upd;
use common\models\employee\Employee;
use common\models\file\File;
use frontend\models\Documents;
use frontend\modules\documents\models\SpecificDocument;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use ZipArchive;

/**
 * This is the model class for table "export".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $user_id
 * @property integer $period_start_date
 * @property integer $period_end_date
 * @property integer $only_new
 * @property integer $io_type_in
 * @property string $io_type_in_items
 * @property integer $io_type_out
 * @property string $io_type_out_items
 * @property integer $io_type_specific
 * @property string $io_type_specific_items
 * @property integer $product_and_service
 * @property integer $contractor
 * @property string $contractor_items
 * @property string $filename
 * @property string $total_objects
 * @property string $objects_completed
 * @property integer $status
 */
class ExportFiles extends ActiveRecord
{
    const ONLY_NEW = 1;
    const STATUS_IN_PROGRESS = 0;
    const STATUS_COMPLETED = 1;

    public static $archiveDir = '@frontend/runtime/export/export_files/';

    /**
     * @var array
     */
    public static $documentTypes = [
        Documents::DOCUMENT_ACT => 'Акты',
        Documents::DOCUMENT_INVOICE => 'Счета',
        Documents::DOCUMENT_PACKING_LIST => 'Товарные накладные',
        Documents::DOCUMENT_INVOICE_FACTURE => 'Счета-фактуры',
        Documents::DOCUMENT_UPD => 'УПД',
        Documents::DOCUMENT_ORDER => 'Заказы',
        Documents::DOCUMENT_PAYMENT_ORDER => 'Платежные поручения',
        Documents::DOCUMENT_SPECIFIC => 'Мои документы',
    ];
    /**
     * @var array
     */
    public static $functionName = [
        Documents::DOCUMENT_ACT => 'getActIds',
        Documents::DOCUMENT_INVOICE => 'getInvoiceIds',
        Documents::DOCUMENT_PACKING_LIST => 'getPackingListIds',
        Documents::DOCUMENT_INVOICE_FACTURE => 'getInvoiceFactureIds',
        Documents::DOCUMENT_UPD => 'getUpdIds',
        Documents::DOCUMENT_ORDER => 'getOrderDocumentIds',
        Documents::DOCUMENT_PAYMENT_ORDER => 'getPaymentOrderIds',
        Documents::DOCUMENT_SPECIFIC => 'getSpecificDocumentIds',
    ];
    /**
     * @var array
     */
    public static $archiveStructure = [
        Documents::IO_TYPE_IN_URL => [
            'ioType' => Documents::IO_TYPE_IN,
            'path' => 'Входящие/',
        ],
        Documents::IO_TYPE_OUT_URL => [
            'ioType' => Documents::IO_TYPE_OUT,
            'path' => 'Исходящие/',
        ],
        Documents::TYPE_SPECIFIC_URL => [
            'ioType' => null,
            'path' => '',
        ],
    ];
    /**
     * @var array
     */
    public static $documentInKeys = [
        Documents::DOCUMENT_ACT,
        Documents::DOCUMENT_INVOICE,
        Documents::DOCUMENT_PACKING_LIST,
        Documents::DOCUMENT_INVOICE_FACTURE,
        Documents::DOCUMENT_UPD,
        Documents::DOCUMENT_PAYMENT_ORDER,
    ];
    /**
     * @var array
     */
    public static $documentOutKeys = [
        Documents::DOCUMENT_ACT,
        Documents::DOCUMENT_INVOICE,
        Documents::DOCUMENT_PACKING_LIST,
        Documents::DOCUMENT_INVOICE_FACTURE,
        Documents::DOCUMENT_UPD,
        Documents::DOCUMENT_ORDER,
    ];

    /**
     * @var array
     */
    public static $contractorTypes = [
        Contractor::TYPE_CUSTOMER => 'Покупатель',
        Contractor::TYPE_SELLER => 'Поставщик',
    ];

    /**
     * @var
     */
    public $lastExportFiles;

    protected $_lastExportFiles = false;
    protected $_documentsData = false;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @return array
     */
    public static function documentClass()
    {
        return [
            Documents::DOCUMENT_ACT => Act::className(),
            Documents::DOCUMENT_INVOICE => Invoice::className(),
            Documents::DOCUMENT_PACKING_LIST => PackingList::className(),
            Documents::DOCUMENT_INVOICE_FACTURE => InvoiceFacture::className(),
            Documents::DOCUMENT_UPD => Upd::className(),
            Documents::DOCUMENT_ORDER => OrderDocument::className(),
            Documents::DOCUMENT_PAYMENT_ORDER => PaymentOrder::className(),
            Documents::DOCUMENT_SPECIFIC => SpecificDocument::className(),
        ];
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'export_files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['io_type_in', 'io_type_out', 'io_type_specific', 'only_new'], 'boolean'],
            [['inItems'], 'in', 'range' => self::$documentInKeys, 'allowArray' => true],
            [['outItems'], 'in', 'range' => self::$documentOutKeys, 'allowArray' => true],
            [['specificItems'], 'in', 'range' => [Documents::DOCUMENT_SPECIFIC], 'allowArray' => true],
            [['period_start_date', 'period_end_date'], 'date', 'format' => 'php:Y-m-d'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'io_type_in' => 'Входящие документы',
            'inItems' => 'Входящие документы',
            'io_type_out' => 'Исходящие документы',
            'outItems' => 'Исходящие документы',
            'io_type_specific' => 'Мои документы',
            'specificItems' => 'Мои документы',
            'product_and_service' => 'Товары и услуги',
            'contractor' => 'Контрагенты',
            'only_new' => 'Выгружать только новые объекты',
        ];
    }

    /**
     * @param array $value
     */
    public function setInItems($value)
    {
        $this->io_type_in_items = (is_array($value) && count($value) > 0) ? serialize($value) : '';
    }

    /**
     * @return array
     */
    public function getInItems()
    {
        return $this->io_type_in_items ? unserialize($this->io_type_in_items) : [];
    }

    /**
     * @param array $value
     */
    public function setOutItems($value)
    {
        $this->io_type_out_items = (is_array($value) && count($value) > 0) ? serialize($value) : '';
    }

    /**
     * @return array
     */
    public function getOutItems()
    {
        return $this->io_type_out_items ? unserialize($this->io_type_out_items) : [];
    }

    /**
     * @param array $value
     */
    public function setSpecificItems($value)
    {
        $this->io_type_specific_items = (is_array($value) && count($value) > 0) ? serialize($value) : '';
    }

    /**
     * @return array
     */
    public function getSpecificItems()
    {
        return $this->io_type_specific_items ? unserialize($this->io_type_specific_items) : [];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'user_id']);
    }

    /**
     * @return bool|string
     */
    public function getCreatedAt()
    {
        return $this->getFormattedDate($this->created_at);
    }

    /**
     * @return bool|string
     */
    public function getPeriodStartDate()
    {
        return $this->getFormattedDate($this->period_start_date);
    }

    /**
     * @return bool|string
     */
    public function getPeriodEndDate()
    {
        return $this->getFormattedDate($this->period_end_date);
    }

    /**
     * @return mixed|null
     */
    public function getContractorItems()
    {
        return !empty($this->contractor_items) ? unserialize($this->contractor_items) : null;
    }

    /**
     * @param $type
     * @return array
     */
    public function getIODocumentsByType($type)
    {
        $inOutDocuments = [];

        if (!empty($this->io_type_in_items)) {
            $typeInDocuments = unserialize($this->io_type_in_items);

            if (in_array($type, $typeInDocuments)) {
                $inOutDocuments[] = Documents::IO_TYPE_IN;
            }
        }

        if (!empty($this->io_type_out_items)) {
            $typeOutDocuments = unserialize($this->io_type_out_items);

            if (in_array($type, $typeOutDocuments)) {
                $inOutDocuments[] = Documents::IO_TYPE_OUT;
            }
        }

        if (!empty($this->io_type_specific_items)) {
            $typeSpecificDocuments = unserialize($this->io_type_specific_items);

            if (in_array($type, $typeSpecificDocuments)) {
                $inOutDocuments[] = Documents::TYPE_SPECIFIC;
            }
        }

        return $inOutDocuments;
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->setSpecificItems($this->io_type_specific ? [Documents::DOCUMENT_SPECIFIC] : []);

        return parent::beforeValidate();
    }

    /**
     * @param  bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->status = self::STATUS_IN_PROGRESS;
                $this->total_objects = $this->getDocumentsData()['count'];
                $this->filename = $this->getArchiveName();
            }

            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        $this->deleteFile();

        parent::afterDelete();
    }

    /**
     * @param $date
     * @return bool|string
     */
    protected function getFormattedDate($date)
    {
        return date('d.m.Y', !is_integer($date) ? strtotime($date) : $date);
    }

    /**
     * Delete export file
     */
    private function deleteFile()
    {
        $filePath = \Yii::getAlias('@frontend/runtime/export') . DIRECTORY_SEPARATOR . $this->filename;

        if (is_file($filePath)) {
            unlink($filePath);
        }
    }

    /**
     * @return bool
     */
    public function getHasFiles()
    {
        return $this->getDocumentsData()['count'] > 0;
    }

    /**
     * @return bool
     */
    public function getDocumentsData()
    {
        if ($this->_documentsData === false) {
            $classArray = $this->documentClass();
            $this->_documentsData = [
                'count' => 0,
                'data' => [],
            ];
            foreach (self::$archiveStructure as $type => $data) {
                foreach ($this->{$type . 'Items'} as $key) {
                    $class = $classArray[$key];
                    $documentIds = $this->{self::$functionName[$key]}($data['ioType']);
                    if ($documentIds) {
                        $count = 0;
                        if ($data['ioType'] == Documents::IO_TYPE_OUT) {
                            $count += count($documentIds);
                        }
                        $fileIds = File::find()->select('id')->andWhere([
                            'owner_model' => $class::className(),
                            'owner_id' => $documentIds,
                        ])->column();
                        if ($fileIds) {
                            $count += count($fileIds);
                        }
                        if ($count > 0) {
                            $this->_documentsData['count'] += $count;
                            $this->_documentsData['data'][] = [
                                'inerPath' => $data['path'] . self::$documentTypes[$key],
                                'ioType' => $data['ioType'],
                                'documentClass' => $classArray[$key],
                                'documentIds' => $data['ioType'] == Documents::IO_TYPE_OUT ? $documentIds : null,
                                'fileIds' => $fileIds,
                            ];
                        }
                    }
                }
            }
        }

        return $this->_documentsData;
    }

    /**
     * @param $type
     * @return array
     */
    public function getActIds($type)
    {
        $query = Act::find()
            ->select(Act::tableName() . '.id')
            ->joinWith('invoice')
            ->andWhere([Invoice::tableName() . '.company_id' => $this->company_id])
            ->andWhere([Act::tableName() . '.type' => $type])
            ->andWhere(['between', Act::tableName() . '.document_date', $this->period_start_date, $this->period_end_date]);

        if ($this->only_new && ($lastExportFiles = $this->getLastExportFiles())) {
            $query->andWhere(['>', Act::tableName() . '.created_at', $lastExportFiles->created_at]);
        }

        return $query->column();
    }

    /**
     * @param $type
     * @return array
     */
    public function getInvoiceIds($type)
    {
        $query = Invoice::find()
            ->select(Invoice::tableName() . '.id')
            ->andWhere(['company_id' => $this->company_id])
            ->andWhere(['type' => $type])
            ->andWhere(['between', Invoice::tableName() . '.document_date', $this->period_start_date, $this->period_end_date])
            ->andWhere(['is_deleted' => false]);

        if ($this->only_new && ($lastExportFiles = $this->getLastExportFiles())) {
            $query->andWhere(['>', Invoice::tableName() . '.created_at', $lastExportFiles->created_at]);
        }

        return $query->column();
    }

    /**
     * @param $type
     * @return array
     */
    public function getInvoiceFactureIds($type)
    {
        $query = InvoiceFacture::find()
            ->select(InvoiceFacture::tableName() . '.id')
            ->joinWith('invoice')
            ->andWhere([
                Invoice::tableName() . '.company_id' => $this->company_id,
                InvoiceFacture::tableName() . '.type' => $type,
                Invoice::tableName() . '.is_deleted' => false,
            ])
            ->andWhere(['between', InvoiceFacture::tableName() . '.document_date', $this->period_start_date, $this->period_end_date]);

        if ($this->only_new && ($lastExportFiles = $this->getLastExportFiles())) {
            $query->andWhere(['>', InvoiceFacture::tableName() . '.created_at', $lastExportFiles->created_at]);
        }

        return $query->column();
    }

    /**
     * @param $type
     * @return array
     */
    public function getPackingListIds($type)
    {
        $query = PackingList::find()
            ->select(PackingList::tableName() . '.id')
            ->joinWith('invoice')
            ->andWhere([Invoice::tableName() . '.company_id' => $this->company_id])
            ->andWhere([PackingList::tableName() . '.type' => $type])
            ->andWhere(['between', PackingList::tableName() . '.document_date', $this->period_start_date, $this->period_end_date]);

        if ($this->only_new && ($lastExportFiles = $this->getLastExportFiles())) {
            $query->andWhere(['>', PackingList::tableName() . '.created_at', $lastExportFiles->created_at]);
        }

        return $query->column();
    }

    /**
     * @param $type
     * @return array
     */
    public function getUpdIds($type)
    {
        $query = Upd::find($type)
            ->select(Upd::tableName() . '.id')
            ->joinWith('invoice')
            ->andWhere([Invoice::tableName() . '.company_id' => $this->company_id])
            ->andWhere([Upd::tableName() . '.type' => $type])
            ->andWhere(['between', Upd::tableName() . '.document_date', $this->period_start_date, $this->period_end_date]);

        if ($this->only_new && ($lastExportFiles = $this->getLastExportFiles())) {
            $query->andWhere(['>', Upd::tableName() . '.created_at', $lastExportFiles->created_at]);
        }

        return $query->column();
    }

    /**
     * @param $type
     * @return array
     */
    public function getPaymentOrderIds($type)
    {
        $query = PaymentOrder::find()
            ->select(PaymentOrder::tableName() . '.id')
            ->joinWith('invoice')
            ->andWhere([Invoice::tableName() . '.company_id' => $this->company_id])
            ->andWhere(['between', PaymentOrder::tableName() . '.document_date', $this->period_start_date, $this->period_end_date]);

        if ($this->only_new && ($lastExportFiles = $this->getLastExportFiles())) {
            $query->andWhere(['>', PaymentOrder::tableName() . '.created_at', $lastExportFiles->created_at]);
        }

        return $query->column();
    }

    /**
     * @param $type
     * @return array
     */
    public function getOrderDocumentIds($type)
    {
        $query = OrderDocument::find()
            ->select(OrderDocument::tableName() . '.id')
            ->andWhere([OrderDocument::tableName() . '.company_id' => $this->company_id])
            ->andWhere(['between', OrderDocument::tableName() . '.document_date', $this->period_start_date, $this->period_end_date]);

        if ($this->only_new && ($lastExportFiles = $this->getLastExportFiles())) {
            $query->andWhere(['>', OrderDocument::tableName() . '.created_at', $lastExportFiles->created_at]);
        }

        return $query->column();
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getSpecificDocumentIds()
    {
        $query = SpecificDocument::find()
            ->select('id')
            ->andWhere([
                'or',
                ['document_author_id' => $this->user_id],
                ['company_id' => $this->company_id],
            ])
            ->andWhere(['between', 'document_date', $this->period_start_date, $this->period_end_date]);

        if ($this->only_new && ($lastExportFiles = $this->getLastExportFiles())) {
            $query->andWhere(['>', 'created_at', $lastExportFiles->created_at]);
        }

        return $query->column();
    }

    /**
     * @return array|null|ActiveRecord
     */
    public function getLastExportFiles()
    {
        if ($this->_lastExportFiles === false) {
            $this->_lastExportFiles = ExportFiles::find()
                ->andWhere(['user_id' => $this->user_id])
                ->andWhere(['company_id' => $this->company_id])
                ->andWhere(['status' => ExportFiles::STATUS_COMPLETED])
                ->orderBy(['created_at' => SORT_DESC])
                ->one();
        }

        return $this->_lastExportFiles;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Документы_с_' . $this->period_start_date . '_по_' . $this->period_end_date;
    }

    /**
     * @return string
     */
    public function getArchiveName()
    {
        return $this->getName() . '.zip';
    }

    /**
     * @return string
     */
    public function getArchiveDir()
    {
        return Yii::getAlias(self::$archiveDir);
    }

    /**
     * @return string
     */
    public function getArchivePath()
    {
        return $this->getArchiveDir() . DIRECTORY_SEPARATOR . $this->id;
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    public function createArchive()
    {
        $dir = $this->getArchiveDir();
        $path = $this->getArchivePath();

        if (!is_dir($dir)) {
            FileHelper::createDirectory($dir);
        }

        $zip = new ZipArchive;
        $res = $zip->open($path, ZipArchive::CREATE);
        if ($res === true) {
            $name = $this->getName();
            $companyFileDir = Company::fileUploadPath($this->company_id);
            $addStamp = $this->company->pdf_signed;
            $totalCount = (int)$this->total_objects + 1;
            $processedCount = 0;
            foreach ($this->getDocumentsData()['data'] as $data) {
                $inerPath = $name . '/' . $data['inerPath'] . '/';
                if ($data['documentIds'] !== null && method_exists($data['documentClass'], 'getRenderer')) {
                    foreach ($data['documentIds'] as $id) {
                        $model = $data['documentClass']::findOne($id);
                        $content = $data['documentClass']::getRenderer(null, $model, PdfRenderer::DESTINATION_STRING, $addStamp)->output(false);
                        //$zip->addFromString(iconv("UTF-8","CP866", $inerPath . $model->pdfFileName), $content);
                        $zip->addFromString($inerPath . $model->pdfFileName, $content);
                        $processedCount++;

                        if ($processedCount % (1 + round($totalCount / 100)) == 0) {
                            Yii::$app->session->set('export_files_progress', floor($processedCount * 100 / $totalCount));
                            Yii::$app->session->close();
                        }
                    }
                }
                $fileDataArray = $data['fileIds'] ? File::find()->select([
                    'id',
                    'filename_full',
                    'filepath',
                ])->andWhere([
                    'id' => $data['fileIds'],
                ])->asArray()->all() : [];
                if ($fileDataArray) {
                    foreach ($fileDataArray as $fileData) {
                        $filePath = $companyFileDir . DIRECTORY_SEPARATOR .
                                    $fileData['filepath'] . DIRECTORY_SEPARATOR .
                                    $fileData['id'];
                        if (is_file($filePath)) {
                            //$zip->addFile($filePath, iconv("UTF-8","CP866", $inerPath . $fileData['filename_full']));
                            $zip->addFile($filePath, $inerPath . $fileData['filename_full']);
                        }
                        $processedCount++;
                        Yii::$app->session->set('export_files_progress', floor($processedCount * 100 / $totalCount));
                        Yii::$app->session->close();
                    }
                }
            }
            if ($zip->close()) {
                Yii::$app->session->remove('export_files_progress');
                Yii::$app->session->close();
                $this->updateAttributes(['status' => self::STATUS_COMPLETED]);
                @header_remove('Set-Cookie');

                return true;
            }
        }

        return false;
    }
}
