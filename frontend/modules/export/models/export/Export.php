<?php

namespace frontend\modules\export\models\export;

use common\models\Company;
use common\models\Contractor;
use common\models\employee\Employee;
use frontend\models\Documents;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "export".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $user_id
 * @property integer $company_id
 * @property integer $period_start_date
 * @property integer $period_end_date
 * @property integer $only_new
 * @property integer $io_type_in
 * @property string $io_type_in_items
 * @property integer $io_type_out
 * @property string $io_type_out_items
 * @property integer $io_type_specific
 * @property string $io_type_specific_items
 * @property integer $product_and_service
 * @property integer $contractor
 * @property string $contractor_items
 * @property string $filename
 * @property string $total_objects
 * @property string $objects_completed
 * @property integer $status
 * @property integer $source_id
 *
 * @property Employee $employee
 */
class Export extends ActiveRecord
{
    const SOURCE_MANUAL = 1;
    const SOURCE_1C_FRESH = 2;

    const ONLY_NEW = 1;

    const STATUS_IN_PROGRESS = 0;
    const STATUS_COMPLETED = 1;
    const DOCUMENT_ACT_AND_PACKING_LIST = 5;

    public static $documentTypes = [
        Documents::DOCUMENT_ACT => 'Акт',
        Documents::DOCUMENT_INVOICE => 'Счет',
        Documents::DOCUMENT_PACKING_LIST => 'Товарные накладные',
        Documents::DOCUMENT_INVOICE_FACTURE => 'Счета-фактуры',
        self::DOCUMENT_ACT_AND_PACKING_LIST => 'Акты+ТН (счета с товарами и услугами)',
        Documents::DOCUMENT_UPD => 'УПД',
    ];

    public static $contractorTypes = [
        Contractor::TYPE_CUSTOMER => 'Покупатель',
        Contractor::TYPE_SELLER => 'Поставщик',
    ];

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'export';
    }

    /**
     * @param $documentTypeIds
     * @return array
     */
    public static function getExportDocumentTypeList($documentTypeIds = [])
    {
        $temp = [];

        if (is_array($documentTypeIds) && sizeof($documentTypeIds) > 0) {
            foreach ($documentTypeIds as $documentTypeId) {
                if (isset(self::$documentTypes[$documentTypeId])) {
                    $temp[] = self::$documentTypes[$documentTypeId];
                }
            }
        }

        return $temp;
    }

    /**
     * @param $contractorTypeIds
     * @return array
     */
    public static function getExportContractorTypes($contractorTypeIds = [])
    {
        $temp = [];

        if (is_array($contractorTypeIds) && sizeof($contractorTypeIds) > 0) {
            foreach ($contractorTypeIds as $contractorTypeId) {
                if (isset(self::$contractorTypes[$contractorTypeId])) {
                    $temp[] = self::$contractorTypes[$contractorTypeId];
                }
            }
        }

        return $temp;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'user_id']);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'io_type_in' => 'Входящие документы',
            'io_type_out' => 'Исходящие документы',
            'product_and_service' => 'Товары и услуги',
            'contractor' => 'Контрагенты',
            'only_new' => 'Выгружать только новые объекты',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        foreach (['io_type_in_items', 'io_type_out_items'] as $serializeField) {
            if (!empty($this->$serializeField)) {
                $this->$serializeField = unserialize($this->$serializeField);
            }
        }
    }

    /**
     * @return bool|string
     */
    public function getCreatedAt()
    {
        return $this->getFormattedDate($this->created_at);
    }

    /**
     * @return bool|string
     */
    public function getPeriodStartDate()
    {
        return $this->getFormattedDate($this->period_start_date);
    }

    /**
     * @return bool|string
     */
    public function getPeriodEndDate()
    {
        return $this->getFormattedDate($this->period_end_date);
    }

    /**
     * @return mixed|null
     */
    public function getContractorItems()
    {
        return !empty($this->contractor_items) ? unserialize($this->contractor_items) : null;
    }

    /**
     * @param $type
     * @return array
     */
    public function getIODocumentsByType($type)
    {
        $inOutDocuments = [];

        if (!empty($this->io_type_in_items)) {
            $typeInDocuments = (is_array($this->io_type_in_items))
                ? $this->io_type_in_items
                : unserialize($this->io_type_in_items);

            if (in_array($type, $typeInDocuments)) {
                $inOutDocuments[] = Documents::IO_TYPE_IN;
            }
        }

        if (!empty($this->io_type_out_items)) {
            $typeOutDocuments = is_array($this->io_type_out_items)
                ? $this->io_type_out_items
                : unserialize($this->io_type_out_items);

            if (in_array($type, $typeOutDocuments)) {
                $inOutDocuments[] = Documents::IO_TYPE_OUT;
            }
        }

        return $inOutDocuments;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['io_type_in', 'io_type_out', 'product_and_service', 'contractor', 'only_new'], 'boolean'],
            [['io_type_in_items', 'io_type_out_items', 'contractor_items'], 'each', 'rule' => ['integer']],
            [['user_id', 'source_id', 'status'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_IN_PROGRESS],
            [['period_start_date', 'period_end_date'], 'date', 'format' => 'Y-m-d'],
            ['filename', 'string'],
            [['source_id'], 'default', 'value' => self::SOURCE_MANUAL],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $result = false;

        if (parent::beforeSave($insert)) {
            if ($insert) {
                foreach (['io_type_in_items', 'io_type_out_items', 'contractor_items'] as $listField) {
                    if (is_array($this->$listField) && sizeof($this->$listField) > 0) {
                        $this->$listField = serialize($this->$listField);
                    }
                }
                if (empty($this->user_id)) {
                    $this->user_id = Yii::$app->user->identity->id;
                }
                if (empty($this->company_id)) {
                    $this->company_id = Yii::$app->user->identity->company->id;
                }
            }

            $result = true;
        }

        return $result;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        $this->deleteFile();

        parent::afterDelete();
    }

    /**
     * @param $date
     * @return bool|string
     */
    protected function getFormattedDate($date)
    {
        return date('d.m.Y', !is_integer($date) ? strtotime($date) : $date);
    }

    /**
     * Delete export file
     */
    private function deleteFile()
    {
        $filePath = \Yii::getAlias('@frontend/runtime/export') . DIRECTORY_SEPARATOR . $this->filename;

        if (is_file($filePath)) {
            unlink($filePath);
        }
    }
}