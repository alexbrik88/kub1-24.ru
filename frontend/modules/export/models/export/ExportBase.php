<?php

namespace frontend\modules\export\models\export;

use common\models\Contractor;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\document\SalesInvoice;
use common\models\document\Upd;
use common\models\employee\Employee;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\modules\export\models\one_c\OneCExport;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class ExportBase
 * @package frontend\modules\export\models\export
 */
abstract class ExportBase
{
    const EXPORT_CONTRACTOR = 'contractor';
    const EXPORT_DOCUMENT_ACT = 'act';
    const EXPORT_DOCUMENT_INVOICE = 'invoice';
    const EXPORT_DOCUMENT_PACKING_LIST = 'packingList';
    const EXPORT_DOCUMENT_SALES_INVOICE = 'salesInvoice';
    const EXPORT_DOCUMENT_INVOICE_FACTURE = 'invoiceFacture';
    const EXPORT_PRODUCT_AND_SERVICE = 'product';
    const EXPORT_INVOICE_WITH_ACT_AND_PACKING_LIST = 'invoice_with_act_and_packing_list';
    const EXPORT_UPD = 'upd';

    /**
     * @var Export
     */
    protected $exportModel = null;

    /**
     * @var string
     */
    protected $outputFile = '';

    /**
     * @var null
     */
    protected $lastExport = null;

    /**
     * @return mixed
     */
    abstract public function createExportFile();

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getContractors()
    {
        $out = [];

        if ($contractorTypes = $this->exportModel->getContractorItems()) {
            $query = Contractor::find()
                ->byCompany($this->exportModel->company_id)
                ->byStatus(Contractor::ACTIVE)
                ->andWhere(['in', Contractor::tableName() . '.type', $contractorTypes])
                ->byDeleted();
            if ((bool) $this->exportModel->only_new) {
                if ($lastExport = $this->getLastExport()) {
                    $query->andWhere(Contractor::tableName() . '.created_at > :created_at', [':created_at' => $lastExport->created_at]);
                }
            } else {
                $query->andWhere(['between', Contractor::tableName() . '.created_at', strtotime($this->exportModel->period_start_date), strtotime($this->exportModel->period_end_date)]);
            }

            if ($this instanceof OneCExport) {
                $query->andWhere([Contractor::tableName() . '.not_accounting' => false]);
            }

            $objects = $query->all();

            $out = [
                'count' => sizeof($objects),
                'data' => $objects,
            ];
        }

        return $out;
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getDocumentAct()
    {
        $out = [];

        if ($documentActTypes = $this->exportModel->getIODocumentsByType(Documents::DOCUMENT_ACT)) {
            $query = Act::find()->joinWith('invoice')
                ->byCompany($this->exportModel->company_id)
                ->andWhere(['in', Act::tableName() . '.type', $documentActTypes])
                ->andWhere([
                    Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED,
                ]);

            if ((bool) $this->exportModel->only_new) {
                if ($lastExport = $this->getLastExport()) {
                    $query->andWhere(Act::tableName() . '.created_at > :created_at', [':created_at' => $lastExport->created_at]);
                };
            } else {
                if (count($documentActTypes) == 2) {
                    $query->andWhere(['or',
                        ['and',
                            [Act::tableName() . '.type' => Documents::IO_TYPE_OUT],
                            ['between', Act::tableName() . '.document_date', $this->exportModel->period_start_date, $this->exportModel->period_end_date],
                        ],
                        ['and',
                            [Act::tableName() . '.type' => Documents::IO_TYPE_IN],
                            ['between', Act::tableName() . '.document_date', $this->exportModel->period_start_date, $this->exportModel->period_end_date],
                        ],
                    ]);
                } elseif (in_array(Documents::IO_TYPE_IN, $documentActTypes)) {
                    $query->andWhere(['and',
                        [Act::tableName() . '.type' => Documents::IO_TYPE_IN],
                        ['between', Act::tableName() . '.document_date', $this->exportModel->period_start_date, $this->exportModel->period_end_date],
                    ]);
                } else {
                    $query->andWhere(['and',
                        [Act::tableName() . '.type' => Documents::IO_TYPE_OUT],
                        ['between', Act::tableName() . '.document_date', $this->exportModel->period_start_date, $this->exportModel->period_end_date],
                    ]);
                }
            }

            if ($this instanceof OneCExport) {
                $query
                    ->leftJoin(Contractor::tableName(),
                        '{{' . Contractor::tableName() . '}}.[[id]] = {{' . Invoice::tableName() . '}}.[[contractor_id]]'
                    )
                    ->leftJoin([
                        'not_buh' => $this->getNotAccountinQuery()
                    ], '{{not_buh}}.[[invoice_id]] = {{' . Invoice::tableName() . '}}.[[id]]')
                    ->andWhere(['not_buh.not_accounting' => null])
                    ->andWhere([Contractor::tableName() . '.not_accounting' => false]);
            }

            $temp = $query->all();

            $out['count'] = sizeof($temp);

            if (is_array($temp) && sizeof($temp) > 0) {
                foreach ($temp as $actRow) {
                    $out['data'][$actRow->type][] = $actRow;
                }
            }
        }

        return $out;
    }

    /**
     * @return array
     */
    public function getDocumentInvoiceFacture()
    {
        $out = [];

        if ($documentInvoiceFactureTypes = $this->exportModel->getIODocumentsByType(Documents::DOCUMENT_INVOICE_FACTURE)) {
            $query = InvoiceFacture::find()->joinWith('invoice')
                ->byCompany($this->exportModel->company_id)
                ->andWhere(['in', InvoiceFacture::tableName() . '.type', $documentInvoiceFactureTypes])
                ->andWhere([
                    Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED,
                ]);

            if ((bool) $this->exportModel->only_new) {
                if ($lastExport = $this->getLastExport()) {
                    $query->andWhere(InvoiceFacture::tableName() . '.created_at > :created_at', [':created_at' => $lastExport->created_at]);
                };
            } else {
                if (count($documentInvoiceFactureTypes) == 2) {
                    $query->andWhere(['or',
                        ['and',
                            [InvoiceFacture::tableName() . '.type' => Documents::IO_TYPE_OUT],
                            ['between', InvoiceFacture::tableName() . '.document_date', $this->exportModel->period_start_date, $this->exportModel->period_end_date],
                        ],
                        ['and',
                            [InvoiceFacture::tableName() . '.type' => Documents::IO_TYPE_IN],
                            ['between', InvoiceFacture::tableName() . '.document_date', $this->exportModel->period_start_date, $this->exportModel->period_end_date],
                        ],
                    ]);
                } elseif (in_array(Documents::IO_TYPE_IN, $documentInvoiceFactureTypes)) {
                    $query->andWhere(['and',
                        [InvoiceFacture::tableName() . '.type' => Documents::IO_TYPE_IN],
                        ['between', InvoiceFacture::tableName() . '.document_date', $this->exportModel->period_start_date, $this->exportModel->period_end_date],
                    ]);
                } else {
                    $query->andWhere(['and',
                        [InvoiceFacture::tableName() . '.type' => Documents::IO_TYPE_OUT],
                        ['between', InvoiceFacture::tableName() . '.document_date', $this->exportModel->period_start_date, $this->exportModel->period_end_date],
                    ]);
                }
            }

            if ($this instanceof OneCExport) {
                $query
                    ->leftJoin(Contractor::tableName(),
                        '{{' . Contractor::tableName() . '}}.[[id]] = {{' . Invoice::tableName() . '}}.[[contractor_id]]'
                    )
                    ->leftJoin([
                        'not_buh' => $this->getNotAccountinQuery()
                    ], '{{not_buh}}.[[invoice_id]] = {{' . Invoice::tableName() . '}}.[[id]]')
                    ->andWhere(['not_buh.not_accounting' => null])
                    ->andWhere([Contractor::tableName() . '.not_accounting' => false]);
            }

            $temp = $query->all();

            $out['count'] = sizeof($temp);
            if (is_array($temp) && sizeof($temp) > 0) {
                foreach ($temp as $invoiceFacture) {
                    //    if ($invoiceFacture->invoice->actOrPackingList == null) {
                    //        if ($invoiceFacture->invoice->production_type == Product::PRODUCTION_TYPE_GOODS) {
                    //            $invoiceFacture->invoice->createPackingList();
                    //        } else {
                    //            $invoiceFacture->invoice->createAct();
                    //        }
                    //    }
                    $out['data'][$invoiceFacture->type][] = $invoiceFacture;
                }
            }
        }

        return $out;
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getProductAndService()
    {
        $out = [];

        if ($this->exportModel->product_and_service) {
            $query = Product::find()->with(['countryOrigin', 'priceForSellNds', 'countryOrigin'])
                ->byCompany($this->exportModel->company_id)
                ->byUser();

            if ((bool) $this->exportModel->only_new) {
                if ($lastExport = $this->getLastExport()) {
                    $query->andWhere(Product::tableName() . '.created_at > :created_at', [':created_at' => $lastExport->created_at]);
                }
            } else {
                $query->andWhere(['between', Product::tableName() . '.created_at', strtotime($this->exportModel->period_start_date), strtotime($this->exportModel->period_end_date)]);
            }

            $out['data'] = $query->all();
            $out['count'] = sizeof($out['data']);
        }

        return $out;
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getInvoices()
    {
        $out = [];

        if ($invoiceTypes = $this->exportModel->getIODocumentsByType(Documents::DOCUMENT_INVOICE)) {
            $query = Invoice::find()
                ->byCompany($this->exportModel->company_id)
                ->andWhere(['in', Invoice::tableName() . '.type', $invoiceTypes])
                ->andWhere([
                    Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED,
                ]);

            if ((bool) $this->exportModel->only_new) {
                if ($lastExport = $this->getLastExport()) {
                    $query->andWhere(Invoice::tableName() . '.created_at > :created_at', [':created_at' => $lastExport->created_at]);
                };
            } else {
                $query->andWhere(['between', Invoice::tableName() . '.document_date', $this->exportModel->period_start_date, $this->exportModel->period_end_date]);
            }

            if ($this instanceof OneCExport) {
                $query
                    ->leftJoin(Contractor::tableName(),
                        '{{' . Contractor::tableName() . '}}.[[id]] = {{' . Invoice::tableName() . '}}.[[contractor_id]]'
                    )
                    ->leftJoin([
                        'not_buh' => $this->getNotAccountinQuery()
                    ], '{{not_buh}}.[[invoice_id]] = {{' . Invoice::tableName() . '}}.[[id]]')
                    ->andWhere(['not_buh.not_accounting' => null])
                    ->andWhere([Contractor::tableName() . '.not_accounting' => false]);
            }

            $temp = $query->all();

            $out['count'] = sizeof($temp);

            foreach ($temp as $invoice) {

                if (substr($invoice->object_guid, 0, 3) === 'UPD') {
                    $out['count']--;
                    continue;
                }

                $out['data'][$invoice->type][] = $invoice;
            }
        }

        return $out;
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getPackingList()
    {
        $out = [];

        if ($packingListTypes = $this->exportModel->getIODocumentsByType(Documents::DOCUMENT_PACKING_LIST)) {
            $query = PackingList::find()
                ->byCompany($this->exportModel->company_id)
                ->andWhere(['in', PackingList::tableName() . '.type', $packingListTypes])
                ->andWhere([
                    Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED,
                ]);

            if ((bool) $this->exportModel->only_new) {
                if ($lastExport = $this->getLastExport()) {
                    $query->andWhere(PackingList::tableName() . '.created_at > :created_at', [':created_at' => $lastExport->created_at]);
                };
            } else {
                if (count($packingListTypes) == 2) {
                    $query->andWhere(['or',
                        ['and',
                            [PackingList::tableName() . '.type' => Documents::IO_TYPE_OUT],
                            ['between', PackingList::tableName() . '.document_date', $this->exportModel->period_start_date, $this->exportModel->period_end_date],
                        ],
                        ['and',
                            [PackingList::tableName() . '.type' => Documents::IO_TYPE_IN],
                            ['between', PackingList::tableName() . '.document_date', $this->exportModel->period_start_date, $this->exportModel->period_end_date],
                        ],
                    ]);
                } elseif (in_array(Documents::IO_TYPE_IN, $packingListTypes)) {
                    $query->andWhere(['and',
                        [PackingList::tableName() . '.type' => Documents::IO_TYPE_IN],
                        ['between', PackingList::tableName() . '.document_date', $this->exportModel->period_start_date, $this->exportModel->period_end_date],
                    ]);
                } else {
                    $query->andWhere(['and',
                        [PackingList::tableName() . '.type' => Documents::IO_TYPE_OUT],
                        ['between', PackingList::tableName() . '.document_date', $this->exportModel->period_start_date, $this->exportModel->period_end_date],
                    ]);
                }
            }

            if ($this instanceof OneCExport) {
                $query
                    ->leftJoin(Contractor::tableName(),
                        '{{' . Contractor::tableName() . '}}.[[id]] = {{' . Invoice::tableName() . '}}.[[contractor_id]]'
                    )
                    ->leftJoin([
                        'not_buh' => $this->getNotAccountinQuery()
                    ], '{{not_buh}}.[[invoice_id]] = {{' . Invoice::tableName() . '}}.[[id]]')
                    ->andWhere(['not_buh.not_accounting' => null])
                    ->andWhere([Contractor::tableName() . '.not_accounting' => false]);
            }

            $temp = $query->all();
            $out['count'] = sizeof($temp);

            if (is_array($temp) && sizeof($temp) > 0) {
                foreach ($temp as $packingListRow) {
                    $out['data'][$packingListRow->type][] = $packingListRow;
                }
            }
        }

        return $out;
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getSalesInvoice()
    {
        $out = [];

        if ($salesInvoiceTypes = $this->exportModel->getIODocumentsByType(Documents::DOCUMENT_PACKING_LIST)) {
            $query = SalesInvoice::find()
                ->byCompany($this->exportModel->company_id)
                ->andWhere(['in', SalesInvoice::tableName() . '.type', $salesInvoiceTypes])
                ->andWhere([
                    Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED,
                ]);

            if ((bool) $this->exportModel->only_new) {
                if ($lastExport = $this->getLastExport()) {
                    $query->andWhere(SalesInvoice::tableName() . '.created_at > :created_at', [':created_at' => $lastExport->created_at]);
                };
            } else {
                if (count($salesInvoiceTypes) == 2) {
                    $query->andWhere(['or',
                        ['and',
                            [SalesInvoice::tableName() . '.type' => Documents::IO_TYPE_OUT],
                            ['between', SalesInvoice::tableName() . '.document_date', $this->exportModel->period_start_date, $this->exportModel->period_end_date],
                        ],
                        ['and',
                            [SalesInvoice::tableName() . '.type' => Documents::IO_TYPE_IN],
                            ['between', SalesInvoice::tableName() . '.document_date', $this->exportModel->period_start_date, $this->exportModel->period_end_date],
                        ],
                    ]);
                } elseif (in_array(Documents::IO_TYPE_IN, $salesInvoiceTypes)) {
                    $query->andWhere(['and',
                        [SalesInvoice::tableName() . '.type' => Documents::IO_TYPE_IN],
                        ['between', SalesInvoice::tableName() . '.document_date', $this->exportModel->period_start_date, $this->exportModel->period_end_date],
                    ]);
                } else {
                    $query->andWhere(['and',
                        [SalesInvoice::tableName() . '.type' => Documents::IO_TYPE_OUT],
                        ['between', SalesInvoice::tableName() . '.document_date', $this->exportModel->period_start_date, $this->exportModel->period_end_date],
                    ]);
                }
            }

            if ($this instanceof OneCExport) {
                $query
                    ->leftJoin(Contractor::tableName(),
                        '{{' . Contractor::tableName() . '}}.[[id]] = {{' . Invoice::tableName() . '}}.[[contractor_id]]'
                    )
                    ->leftJoin([
                        'not_buh' => $this->getNotAccountinQuery()
                    ], '{{not_buh}}.[[invoice_id]] = {{' . Invoice::tableName() . '}}.[[id]]')
                    ->andWhere(['not_buh.not_accounting' => null])
                    ->andWhere([Contractor::tableName() . '.not_accounting' => false]);
            }

            $temp = $query->all();
            $out['count'] = sizeof($temp);

            if (is_array($temp) && sizeof($temp) > 0) {
                foreach ($temp as $salesInvoiceRow) {
                    $out['data'][$salesInvoiceRow->type][] = $salesInvoiceRow;
                }
            }
        }

        return $out;
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getDocumentUpd()
    {
        $out = [];

        if ($documentUpdTypes = $this->exportModel->getIODocumentsByType(Documents::DOCUMENT_UPD)) {
            $query = Upd::find()->joinWith('invoice')
                ->byCompany($this->exportModel->company_id)
                ->andWhere(['in', Upd::tableName() . '.type', $documentUpdTypes])
                ->andWhere([
                    Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED,
                ]);

            if ((bool) $this->exportModel->only_new) {
                if ($lastExport = $this->getLastExport()) {
                    $query->andWhere(Upd::tableName() . '.created_at > :created_at', [':created_at' => $lastExport->created_at]);
                };
            } else {
                $query->andWhere(['between', Upd::tableName() . '.document_date', $this->exportModel->period_start_date, $this->exportModel->period_end_date]);
            }

            if ($this instanceof OneCExport) {
                $query->leftJoin(Contractor::tableName(),
                        '{{' . Contractor::tableName() . '}}.[[id]] = {{' . Invoice::tableName() . '}}.[[contractor_id]]'
                    )
                    ->leftJoin([
                        'not_buh' => $this->getNotAccountinQuery()
                    ], '{{not_buh}}.[[invoice_id]] = {{' . Invoice::tableName() . '}}.[[id]]')
                    ->andWhere(['not_buh.not_accounting' => null])
                    ->andWhere([Contractor::tableName() . '.not_accounting' => false]);
            }

            $temp = $query->all();

            $out['count'] = sizeof($temp);

            if (is_array($temp) && sizeof($temp) > 0) {
                foreach ($temp as $actRow) {
                    $out['data'][$actRow->type][] = $actRow;
                }
            }
        }

        return $out;
    }

    /**
     * @return array
     */
    public function getInvoiceWithTypeActAndPackingList()
    {
        $out = [];

        if ($invoiceWithPackingListType = $this->exportModel->getIODocumentsByType(Export::DOCUMENT_ACT_AND_PACKING_LIST)) {
            $query = Invoice::find()
                ->byCompany($this->exportModel->company_id)
                ->andWhere([Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED,])
                ->andWhere(['in', Invoice::tableName() . '.type', $invoiceWithPackingListType])
                ->andWhere([Invoice::tableName() . '.production_type' => Product::PRODUCTION_TYPE_SERVICE . ', ' . Product::PRODUCTION_TYPE_GOODS]);

            if ((bool) $this->exportModel->only_new) {
                if ($lastExport = $this->getLastExport()) {
                    $query->andWhere(Invoice::tableName() . '.created_at > :created_at', [':created_at' => $lastExport->created_at]);
                };
            } else {
                $query->andWhere(['between', Invoice::tableName() . '.document_date', $this->exportModel->period_start_date, $this->exportModel->period_end_date]);
            }

            if ($this instanceof OneCExport) {
                $query
                    ->leftJoin(Contractor::tableName(),
                        '{{' . Contractor::tableName() . '}}.[[id]] = {{' . Invoice::tableName() . '}}.[[contractor_id]]'
                    )
                    ->leftJoin([
                        'not_buh' => $this->getNotAccountinQuery()
                    ], '{{not_buh}}.[[invoice_id]] = {{' . Invoice::tableName() . '}}.[[id]]')
                    ->andWhere(['not_buh.not_accounting' => null])
                    ->andWhere([Contractor::tableName() . '.not_accounting' => false]);
            }

            $temp = $query->all();

            $out['count'] = sizeof($temp);

            foreach ($temp as $invoice) {

                if (substr($invoice->object_guid, 0, 3) === 'UPD') {
                    $out['count']--;
                    continue;
                }

                $out['data'][$invoice->type][] = $invoice;
            }
        }

        return $out;
    }

    /**
     * @return $this
     */
    protected function getLastExport()
    {
        if (is_null($this->lastExport)) {
            $this->lastExport = Export::find()->joinWith('employee')->andWhere([
                Employee::tableName() . '.company_id' => $this->exportModel->company_id,
            ])->andWhere(['status' => Export::STATUS_COMPLETED])->orderBy('created_at DESC')->one();
        }

        return $this->lastExport;
    }

    /**
     * @return Query
     */
    protected function getNotAccountinQuery()
    {
        $query = new Query;
        $query1 = $this->exportModel->company->getCashEmoneyFlows()
            ->alias('flow')
            ->select('{{link}}.[[invoice_id]], 1 AS [[not_accounting]]')
            ->joinWith('flowInvoices link', false, 'INNER JOIN')
            ->andWhere(['flow.is_accounting' => false]);
        $query2 = $this->exportModel->company->getCashOrderFlows()
            ->alias('flow')
            ->select('{{link}}.[[invoice_id]], 1 AS [[not_accounting]]')
            ->joinWith('flowInvoices link', false, 'INNER JOIN')
            ->andWhere(['flow.is_accounting' => false]);

        return $query1->union($query2);
    }

    public function setDocsIsExportedFlag(&$objectsByType)
    {
        foreach ($objectsByType as $type => $objectsByFlowType)
            foreach ($objectsByFlowType as $flowType => $objects) {

            $ids = array_filter(ArrayHelper::getColumn($objects, 'id'));

            if (empty($ids))
                continue;

            $idsStr = implode(',', $ids);

            if (self::EXPORT_DOCUMENT_INVOICE == $type) {
                $table = Invoice::tableName();
                $relTables = [];
            } elseif (self::EXPORT_DOCUMENT_ACT == $type) {
                $table = Act::tableName();
                $relTables = [];
            } elseif (self::EXPORT_DOCUMENT_PACKING_LIST == $type) {
                $table = PackingList::tableName();
                $relTables = [];
            } elseif (self::EXPORT_DOCUMENT_SALES_INVOICE == $type) {
                $table = SalesInvoice::tableName();
                $relTables = [];
            } elseif (self::EXPORT_DOCUMENT_INVOICE_FACTURE == $type) {
                $table = InvoiceFacture::tableName();
                $relTables = [];
            } elseif (self::EXPORT_UPD == $type) {
                $table = Upd::tableName();
                $relTables = [];
            } elseif (self::EXPORT_INVOICE_WITH_ACT_AND_PACKING_LIST == $type) {
                $table = Invoice::tableName();
                $relTables = [
                    Act::tableName(),
                    PackingList::tableName()
                ];
            } else {
                $table = null;
            }

            if ($table) {
                $query = "UPDATE `{$table}` SET `one_c_exported` = 1 WHERE `id` IN ({$idsStr}) AND `one_c_exported` = 0";
                \Yii::$app->db->createCommand(new Expression($query))->execute();

                if ($relTables) {
                    foreach ($relTables as $relTable)
                    {
                        if ($relTable == Act::tableName()) {
                            $query2 = "UPDATE `{$relTable}` SET `one_c_exported` = 1 WHERE `id` IN (SELECT `act_id` FROM `invoice_act` WHERE `invoice_id` IN ({$idsStr})) AND `one_c_exported` = 0";
                        }
                        elseif ($relTable == PackingList::tableName()) {
                            $query2 = "UPDATE `{$relTable}` SET `one_c_exported` = 1 WHERE `invoice_id` IN ({$idsStr}) AND `one_c_exported` = 0";
                        } else {
                            continue;
                        }

                        \Yii::$app->db->createCommand(new Expression($query2))->execute();
                    }
                }
            }
        }
    }

    public function setOtherObjectsIsExportedFlag(&$objectsByType)
    {
        foreach ($objectsByType as $type => $objects) {

            $ids = array_filter(ArrayHelper::getColumn($objects, 'id'));

            if (empty($ids))
                continue;

            $idsStr = implode(',', $ids);

            if (self::EXPORT_CONTRACTOR == $type) {
                $table = Contractor::tableName();
            } elseif (self::EXPORT_PRODUCT_AND_SERVICE == $type) {
                $table = Product::tableName();
            } else {
                $table = null;
            }

            if ($table) {
                $query = "UPDATE `{$table}` SET `one_c_exported` = 1 WHERE `id` IN ({$idsStr}) AND `one_c_exported` = 0";
                \Yii::$app->db->createCommand(new Expression($query))->execute();
            }
        }
    }
}
