<?php

namespace frontend\modules\export\models\export;

use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * ExportSearch represents the model behind the search form about `frontend\modules|export|models\Export`.
 */
class ExportFilesSearch extends Export
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ExportFiles::find()
            ->joinWith('employee')
            ->andWhere([
                ExportFiles::tableName() . '.company_id' => \Yii::$app->user->identity->company->id,
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'created_at',
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            ExportFiles::tableName() . '.user_id' => $this->user_id,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'created_at',
            ],
            'defaultOrder' => [
                'created_at' => SORT_DESC,
            ],
        ]);

        return $dataProvider;
    }

    /**
     * @param $companyId
     * @return array
     */
    public function getEmployeeByCompanyArray($companyId)
    {
        $employee[null] = 'Все';
        $employeeList = Employee::find()->byCompany($companyId)
            ->andWhere(['in', 'employee_role_id', [
                EmployeeRole::ROLE_CHIEF,
                EmployeeRole::ROLE_ACCOUNTANT,
            ]])->all();

        if (is_array($employeeList) && sizeof($employeeList) > 0) {
            foreach ($employeeList as $employeeRow) {
                $employee[$employeeRow->id] = $employeeRow->getFio(true);
            }
        }

        return $employee;
    }
}
