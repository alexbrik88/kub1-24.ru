<?php
namespace frontend\modules\export\models\one_c;

use frontend\modules\export\models\export\ExportBase;

/**
 * Class OneCFactory
 * @package frontend\modules\export\models\one_c
 */
class OneCFactory
{
    /**
     * @param $type
     * @param $existsContractor
     * @param $currency
     * @param $individualCurrency
     * @param $existNomenclature
     * @param $existCompany
     * @param $existBankInvoice
     * @param $existBank
     * @param $existCountry
     * @param $storage
     * @param $nomenclatureGroupProduct
     * @param $nomenclatureGroupService
     * @param $nomenclatureGroupMain
     * @param $receiptGoodsServiceAct
     * @param $saleGoodsServiceAct
     * @param $receiptGoodsServiceProduct
     * @param $saleGoodsServiceProduct
     * @return ActOneC|ContractorOneC|InvoiceOneC|InvoiceFacture|PackingListOneC|ProductAndService|null
     * @throws NotFoundHttpException
     */
    public static function factory($type, $existsContractor, $currency, $individualCurrency, $existNomenclature
        , $existCompany, $existBankInvoice, $existBank, $existCountry, $storage, $nomenclatureGroupProduct, $nomenclatureGroupService, $nomenclatureGroupMain
        , $receiptGoodsServiceAct, $saleGoodsServiceAct, $receiptGoodsServiceProduct, $saleGoodsServiceProduct)
    {
        $className = null;

        switch ($type) {
            case ExportBase::EXPORT_CONTRACTOR:
                $className = new ContractorOneC();
                break;
            case ExportBase::EXPORT_DOCUMENT_ACT:
                $className = new ActOneC();
                break;
            case ExportBase::EXPORT_DOCUMENT_INVOICE_FACTURE:
                $className = new InvoiceFactureOneC();
                break;
            case ExportBase::EXPORT_PRODUCT_AND_SERVICE:
                $className = new ProductAndService();
                break;
            case ExportBase::EXPORT_DOCUMENT_INVOICE:
                $className = new InvoiceOneC();
                break;
            case ExportBase::EXPORT_DOCUMENT_PACKING_LIST:
                $className = new PackingListOneC();
                break;
            case ExportBase::EXPORT_DOCUMENT_SALES_INVOICE:
                $className = new SalesInvoiceOneC();
                break;
            case ExportBase::EXPORT_INVOICE_WITH_ACT_AND_PACKING_LIST:
                $className = new InvoiceWithActAndPackingListOneC();
                break;
            case ExportBase::EXPORT_UPD:
                $className = new UpdOneC();
                break;
            default:
                throw new NotFoundHttpException();
        }

        if ($className !== null) {
            $className->existsContractor = $existsContractor;
            $className->currency = $currency;
            $className->individualCurrency = $individualCurrency;
            $className->existNomenclature = $existNomenclature;
            $className->existCompany = $existCompany;
            $className->existBankInvoice = $existBankInvoice;
            $className->existBank = $existBank;
            $className->existCountry = $existCountry;
            $className->storage = $storage;
            $className->nomenclatureGroupProduct = $nomenclatureGroupProduct;
            $className->nomenclatureGroupService = $nomenclatureGroupService;
            $className->nomenclatureGroupMain = $nomenclatureGroupMain;
            $className->receiptGoodsServiceAct = $receiptGoodsServiceAct;
            $className->saleGoodsServiceAct = $saleGoodsServiceAct;
            $className->receiptGoodsServiceProduct = $receiptGoodsServiceProduct;
            $className->saleGoodsServiceProduct = $saleGoodsServiceProduct;
        }

        return $className;
    }
}