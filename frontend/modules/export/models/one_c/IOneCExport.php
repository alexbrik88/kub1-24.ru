<?php

namespace frontend\modules\export\models\one_c;

/**
 * Interface IOneSExport
 *
 * @package frontend\modules\export\models\one_c
 */
interface IOneCExport
{
    /**
     * @param array $data
     * @param null $counter
     * @param null $exportModel
     * @return mixed
     */
    public function export($data = [], &$counter = null, &$exportModel = null);

    /**
     * Get object export rules
     *
     * @return array
     */
    public static function exportRules();
}