<?php

namespace frontend\modules\export\models\one_c;

use common\models\address\Country as ModelCountry;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\document\Act;
use common\models\document\DocumentHelper;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\document\SalesInvoice;
use common\models\document\Upd;
use common\models\product\ProductType;
use common\models\product\ProductUnit;
use common\models\product\Store;
use frontend\models\Documents;
use frontend\modules\export\models\export\Export;
use frontend\modules\export\models\export\ExportBase;
use frontend\modules\export\models\one_c\Country as ExportCountry;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Class OneCExport
 * @package frontend\modules\export\models\one_c
 */
class OneCExport extends ExportBase
{
    /**
     *
     */
    const FILE_FORMAT_VERSION = '2.0';
    /**
     *
     */
    const RULE_FORMAT_VERSION = '2.01';

    /**
     *
     */
    const XML_DATETIME_FORMAT = 'Y-m-d\TH:i:s';

    /** ИмяКонфигурацииПриемника */
    const CONFIG_NAME_IN = 'БухгалтерияПредприятия';

    /** ИмяКонфигурацииИсточника */
    const CONFIG_NAME_OUT = 'БухгалтерияПредприятия';

    /**
     *  Комментарий
     */
    const UNLOADING_OF_SERVICE_KUB = 'Выгрузка из сервиса КУБ';

    /**
     *
     */
    const NAME = 'Бухгалтерия предприятия, редакция 3.0 (3.0.43.101) --> Бухгалтерия предприятия, редакция 3.0 (3.0.43.101)';

    /**
     * xmlns
     */
    const NAMESPACE_XML = 'http://kub-24.ru/upload';

    /**
     * Инструкция по установке (docx)
     */
    const INSTRUCTION_FILENAME = 'instruction.pdf';
    /**
     * Обработка (epf)
     */
    const IC_FILENAME = '1c_v1.0.6.0.cfe';

    /**
     * @var null
     */
    protected $dom = null;

    /**
     * @var null
     */
    protected $file = null;

    /**
     * @var int
     */
    protected $totalObjects = 0;

    /**
     * @var array
     */
    protected $objects = [];

    /**
     * @var array
     */
    protected $exportData = [];

    /**
     * @var int
     */
    protected $counter = 0;

    /**
     * @var array
     */
    protected $rules = [];

    /**
     * @var \XmlWriter
     */
    protected $xmlCreator = null;

    /**
     * @return string
     */
    public static function generateGUID()
    {
        mt_srand((double)microtime() * 10000);

        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);
        $uuid = substr($charid, 0, 8) . $hyphen
            . substr($charid, 8, 4) . $hyphen
            . substr($charid, 12, 4) . $hyphen
            . substr($charid, 16, 4) . $hyphen
            . substr($charid, 20, 12);

        return $uuid;
    }

    /**
     *  Названия Складов
     * @var bool
     */
    public static $storageNames;
    public static $mainStorageName = 'Основной';

    public static function getInvoiceStorageName(Invoice $invoice)
    {
        $storage_id = $invoice->store_id;
        if (count(self::$storageNames) <= 1)
            return 'Основной';

        return ArrayHelper::getValue(self::$storageNames, $storage_id, self::$mainStorageName);
    }

    /**
     * @param Export $model
     */
    public function __construct(Export $model)
    {
        $this->exportModel = $model;

        $storages = Store::find()->where(['company_id' => $model->company_id])->select(['id', 'name', 'is_main'])->asArray()->all();
        foreach($storages as $storage) {
            if ($storage['is_main'])
                self::$mainStorageName = $storage['name'];
        }
        self::$storageNames = ArrayHelper::map($storages, 'id', 'name');
    }

    /**
     * @return array
     */
    public function getObjectsRules()
    {
        return $this->rules;
    }

    /**
     * Add export object rules
     *
     * @param array $rules
     */
    public function addRules($rules = [])
    {
        if (is_array($rules) && sizeof($rules) > 0) {
            foreach ($rules as $ruleName => $rule) {
                if (!isset($this->rules[$ruleName])) {
                    $this->rules[$ruleName] = $rule;
                }
            }
        }
    }

    /**
     * @param $invoiceID
     * @param $ioType
     * @return bool
     * @throws \yii\base\Exception
     */
    public function findDataObjectInvoice($invoiceID, $ioType)
    {
        $invoice = Invoice::findOne($invoiceID);
        if ($ioType !== $invoice->type) {
            $contractor = DocumentHelper::getContractorByCompany($invoice->company);
            $company = DocumentHelper::getCompanyByContractor($invoice->contractor);
            $invoice->populateRelation('contractor', $contractor);
            $invoice->populateRelation('company', $company);
            $invoice->setCompany($company);
            $invoice->setContractor($contractor);
            $invoice->type = $ioType;
        }

        $this->objects[ExportBase::EXPORT_DOCUMENT_INVOICE] = [
            $ioType => [
                $invoice,
            ],
        ];
        $this->totalObjects += 1;
        $this->addRules(InvoiceOneC::exportRules());

        $this->exportModel->updateAttributes(['total_objects' => $this->totalObjects]);

        return $this->totalObjects > 0;
    }

    /**
     * @param $actID
     * @param $ioType
     * @return bool
     * @throws \yii\base\Exception
     */
    public function findDataObjectAct($actID, $ioType)
    {
        $act = Act::findOne($actID);
        if ($ioType !== $act->type) {
            $invoice = $act->invoice;
            $contractor = DocumentHelper::getContractorByCompany($invoice->company);
            $company = DocumentHelper::getCompanyByContractor($invoice->contractor);
            $invoice->populateRelation('contractor', $contractor);
            $invoice->populateRelation('company', $company);
            $invoice->setCompany($company);
            $invoice->setContractor($contractor);
            $invoice->type = $ioType;
            $act->type = $ioType;
            $act->populateRelation('invoice', $invoice);
        }
        $this->objects[ExportBase::EXPORT_DOCUMENT_ACT] = [
            $ioType => [
                $act,
            ],
        ];
        $this->totalObjects += 1;
        $this->addRules(ActOneC::exportRules());

        $this->exportModel->updateAttributes(['total_objects' => $this->totalObjects]);

        return true;
    }

    /**
     * @param $packingListID
     * @param $ioType
     * @return bool
     * @throws \yii\base\Exception
     */
    public function findDataObjectPackingList($packingListID, $ioType)
    {
        $packingList = PackingList::findOne($packingListID);
        if ($ioType !== $packingList->type) {
            $invoice = $packingList->invoice;
            $contractor = DocumentHelper::getContractorByCompany($invoice->company);
            $company = DocumentHelper::getCompanyByContractor($invoice->contractor);
            $invoice->populateRelation('contractor', $contractor);
            $invoice->populateRelation('company', $company);
            $invoice->setCompany($company);
            $invoice->setContractor($contractor);
            $invoice->type = $ioType;
            $packingList->type = $ioType;
            $packingList->populateRelation('invoice', $invoice);
        }
        $this->objects[ExportBase::EXPORT_DOCUMENT_PACKING_LIST] = [
            $ioType => [
                $packingList,
            ],
        ];
        $this->totalObjects += 1;
        $this->addRules(PackingListOneC::exportRules());

        $this->exportModel->updateAttributes(['total_objects' => $this->totalObjects]);

        return true;
    }

    /**
     * @param $salesInvoiceID
     * @param $ioType
     * @return bool
     * @throws \yii\base\Exception
     */
    public function findDataObjectSalesINvoice($salesInvoiceID, $ioType)
    {
        $salesInvoice = SalesInvoice::findOne($salesInvoiceID);
        if ($ioType !== $salesInvoice->type) {
            $invoice = $salesInvoice->invoice;
            $contractor = DocumentHelper::getContractorByCompany($invoice->company);
            $company = DocumentHelper::getCompanyByContractor($invoice->contractor);
            $invoice->populateRelation('contractor', $contractor);
            $invoice->populateRelation('company', $company);
            $invoice->setCompany($company);
            $invoice->setContractor($contractor);
            $invoice->type = $ioType;
            $salesInvoice->type = $ioType;
            $salesInvoice->populateRelation('invoice', $invoice);
        }
        $this->objects[ExportBase::EXPORT_DOCUMENT_SALES_INVOICE] = [
            $ioType => [
                $salesInvoice,
            ],
        ];
        $this->totalObjects += 1;
        $this->addRules(SalesInvoiceOneC::exportRules());

        $this->exportModel->updateAttributes(['total_objects' => $this->totalObjects]);

        return true;
    }

    /**
     * @param $invoiceID
     * @return bool
     */
    public function findDataObjectInvoiceWithActAndPackingList($invoiceID)
    {
        $this->objects[ExportBase::EXPORT_INVOICE_WITH_ACT_AND_PACKING_LIST] = [
            Documents::IO_TYPE_OUT => [
                Invoice::findOne($invoiceID),
            ],
        ];
        $this->totalObjects += 1;
        $this->addRules(InvoiceWithActAndPackingListOneC::exportRules());

        $this->exportModel->updateAttributes(['total_objects' => $this->totalObjects]);

        return true;
    }

    /**
     * @param $invoiceFactureID
     * @param $ioType
     * @return bool
     * @throws \yii\base\Exception
     */
    public function findDataObjectInvoiceFacture($invoiceFactureID, $ioType)
    {
        $invoiceFacture = InvoiceFacture::findOne($invoiceFactureID);
        if ($ioType !== $invoiceFacture->type) {
            $invoice = $invoiceFacture->invoice;
            $contractor = DocumentHelper::getContractorByCompany($invoice->company);
            $company = DocumentHelper::getCompanyByContractor($invoice->contractor);
            $invoice->populateRelation('contractor', $contractor);
            $invoice->populateRelation('company', $company);
            $invoice->setCompany($company);
            $invoice->setContractor($contractor);
            $invoice->type = $ioType;
            $invoiceFacture->type = $ioType;
            $invoiceFacture->populateRelation('invoice', $invoice);
        }
        $this->objects[ExportBase::EXPORT_DOCUMENT_INVOICE_FACTURE] = [
            $ioType => [
                $invoiceFacture,
            ],
        ];
        $this->totalObjects += 1;
        $this->addRules(InvoiceFactureOneC::exportRules());

        $this->exportModel->updateAttributes(['total_objects' => $this->totalObjects]);

        return true;
    }

    /**
     * @param $updID
     * @return bool
     */
    public function findDataObjectUpd($updID)
    {
        $this->objects[ExportBase::EXPORT_UPD] = [
            Documents::IO_TYPE_OUT => [
                Upd::findOne($updID),
            ],
        ];
        $this->totalObjects += 1;
        $this->addRules(UpdOneC::exportRules());

        $this->exportModel->updateAttributes(['total_objects' => $this->totalObjects]);

        return true;
    }

    /**
     * @return bool
     */
    public function findDataObjects()
    {
        //Контрагенты
        if ($contractors = $this->getContractors()) {
            if (isset($contractors['data'])) {
                $this->objects[ExportBase::EXPORT_CONTRACTOR] = $contractors['data'];
                $this->totalObjects += $contractors['count'];
                $this->addRules(ContractorOneC::exportRules());
            }
        }

        // Товары и услуги
        if ($productAndService = $this->getProductAndService()) {
            if (isset($productAndService['data'])) {
                $this->objects[ExportBase::EXPORT_PRODUCT_AND_SERVICE] = $productAndService['data'];
                $this->totalObjects += $productAndService['count'];
                $this->addRules(ProductAndService::exportRules());
            }
        }

        // Счета
        if ($invoices = $this->getInvoices()) {
            if (isset($invoices['data'])) {
                $this->objects[ExportBase::EXPORT_DOCUMENT_INVOICE] = $invoices['data'];
                $this->totalObjects += $invoices['count'];
                $this->addRules(InvoiceOneC::exportRules());
            }
        }

        // Акты
        if ($documentActs = $this->getDocumentAct()) {
            if (isset($documentActs['data'])) {
                $this->objects[ExportBase::EXPORT_DOCUMENT_ACT] = $documentActs['data'];
                $this->totalObjects += $documentActs['count'];
                $this->addRules(ActOneC::exportRules());
            }
        }

        // Товарные накладные
        if ($packingList = $this->getPackingList()) {
            if (isset($packingList['data'])) {
                $this->objects[ExportBase::EXPORT_DOCUMENT_PACKING_LIST] = $packingList['data'];
                $this->totalObjects += $packingList['count'];
                $this->addRules(PackingListOneC::exportRules());
            }
        }

        // Счет фактуры
        if ($documentInvoiceFacture = $this->getDocumentInvoiceFacture()) {
            if (isset($documentInvoiceFacture['data'])) {
                $this->objects[ExportBase::EXPORT_DOCUMENT_INVOICE_FACTURE] = $documentInvoiceFacture['data'];
                $this->totalObjects += $documentInvoiceFacture['count'];
                $this->addRules(InvoiceFactureOneC::exportRules());
            }
        }

        // Счета с актами и товарными накладными
        if ($actAndPackingList = $this->getInvoiceWithTypeActAndPackingList()) {
            if (isset($actAndPackingList['data'])) {
                $this->objects[ExportBase::EXPORT_INVOICE_WITH_ACT_AND_PACKING_LIST] = $actAndPackingList['data'];
                $this->totalObjects += $actAndPackingList['count'];
                $this->addRules(InvoiceWithActAndPackingListOneC::exportRules());
            }
        }

        // УПД
        if ($upd = $this->getDocumentUpd()) {
            if (isset($upd['data'])) {
                $this->objects[ExportBase::EXPORT_UPD] = $upd['data'];
                $this->totalObjects += $upd['count'];
                $this->addRules(UpdOneC::exportRules());
            }
        }

        // Записываем количество экпортируемых объектов
        $this->exportModel->updateAttributes(['total_objects' => $this->totalObjects]);

        // Обновляем флаг one_c_exporded во всех объектах
        $this->setDocsIsExportedFlag($this->objects);
        $this->setOtherObjectsIsExportedFlag($this->objects);

        return $this->totalObjects > 0;
    }

    /**
     * @param null $userID
     * @return mixed|null
     */
    public function createExportFile($userID = null)
    {
        $this->createTemplate();

        $out = null;

        $fileName = $this->xmlCreator->getFilePath();
        unset($this->xmlCreator);

        $dir = \Yii::getAlias('@frontend/runtime/export');
        $zipFilename = str_replace('.xml', '.zip', $fileName);
        $zip = new \ZipArchive();
        $res = $zip->open($dir . DIRECTORY_SEPARATOR . $zipFilename, \ZipArchive::CREATE|\ZipArchive::OVERWRITE);

        if ($res) {
            $zip->addFile($dir . DIRECTORY_SEPARATOR . $fileName, $fileName);
            if (self::INSTRUCTION_FILENAME)
                $zip->addFile(\Yii::getAlias('@frontend/modules/export/docs') . DIRECTORY_SEPARATOR . self::INSTRUCTION_FILENAME, 'Инструкция по добавлению расширений_v1.0.6.0.pdf');
            if (self::IC_FILENAME)
                $zip->addFile(\Yii::getAlias('@frontend/modules/export/docs') . DIRECTORY_SEPARATOR . self::IC_FILENAME, 'КУБ24_Бух_1.0.6.0_22022022_КассовыеДокументы_Ответственные.cfe');

            $zip->close();
            if (is_file($dir . DIRECTORY_SEPARATOR . $fileName)) {
                unlink($dir . DIRECTORY_SEPARATOR . $fileName);
            }
            // return zip except xml
            $fileName = $zipFilename;
        }

        /** @var $this ->dom DOMDocument */
        $this->exportModel->setAttribute('filename', $fileName);
        $this->exportModel->setAttribute('status', Export::STATUS_COMPLETED);
        if ($userID) {
            $this->exportModel->setAttribute('user_id', $userID);
        }

        if ($this->exportModel->save(false)) {
            $out = $fileName;
        }

        return $out;
    }

    /**
     * @return string
     */
    public function generateFileName()
    {
        if ($this->exportModel->employee && $this->exportModel->employee->company) {
            $company = $this->exportModel->employee->company;
            $companyName = ($company->company_type_id == CompanyType::TYPE_IP) ?
                $company->chief_lastname :
                $company->getShortName();
            $beginOfDay = strtotime("midnight", $this->exportModel->created_at);
            $endOfDay = strtotime("tomorrow", $this->exportModel->created_at) - 1;
            $todayExportsCount = Export::find()
                ->where(['company_id' => $company->id])
                ->andWhere(['between', 'created_at', $beginOfDay, $endOfDay])
                ->count();
            $filename = $companyName . '_' . date('Y.m.d', $this->exportModel->created_at);
            if ($todayExportsCount > 1)
                $filename .= '_' . $todayExportsCount;

            return $this->translit(sprintf('1c_%s.xml', $filename));
        }

        return sprintf('1c_%s.xml', md5($this->exportModel->user_id . $this->exportModel->created_at));
    }

    /**
     * @param $filename
     * @return string
     */
    public function getSavePath($filename)
    {
        $path = \Yii::getAlias('@frontend/runtime/export');

        if (!file_exists($path)) {
            mkdir($path);
        }

        return $path . DIRECTORY_SEPARATOR . $filename;
    }

    /**
     * Create xml template
     * @param null $filepath
     * @param null $filename
     * @return bool
     * @throws \Exception
     */
    public function createTemplate($filepath = null, $filename = null)
    {
        $filepath = $filepath ?: \Yii::getAlias('@frontend/runtime/export');
        $filename = $filename ?: $this->generateFileName();

        $this->xmlCreator = new XmlCreator($filepath, $filename);
        $this->xmlCreator->startDocument('1.0', 'utf-8');

        $fileGUID = OneCExport::generateGUID();

        $this->xmlCreator->startElement('ФайлОбмена');
        $this->xmlCreator->writeAttribute('xmlns', self::NAMESPACE_XML);
        $this->xmlCreator->writeAttribute('ВерсияФормата', self::FILE_FORMAT_VERSION);
        $this->xmlCreator->writeAttribute('ДатаВыгрузки', date(self::XML_DATETIME_FORMAT));
        $this->xmlCreator->writeAttribute('НачалоПериодаВыгрузки', date(self::XML_DATETIME_FORMAT, strtotime($this->exportModel->period_start_date)));
        $this->xmlCreator->writeAttribute('ОкончаниеПериодаВыгрузки', date(self::XML_DATETIME_FORMAT, strtotime($this->exportModel->period_end_date)));
        $this->xmlCreator->writeAttribute('ИмяКонфигурацииИсточника', self::CONFIG_NAME_OUT);
        $this->xmlCreator->writeAttribute('ИмяКонфигурацииПриемника', self::CONFIG_NAME_IN);
        $this->xmlCreator->writeAttribute('ИдПравилКонвертации', $fileGUID);
        $this->xmlCreator->writeAttribute('Комментарий', self::UNLOADING_OF_SERVICE_KUB);

        $this->xmlCreator->startElement('ПравилаОбмена');
        $this->xmlCreator->writeElement('ВерсияФормата', self::RULE_FORMAT_VERSION);
        $this->xmlCreator->writeElement('Ид', $fileGUID);
        $this->xmlCreator->writeElement('Наименование', self::NAME);
        $this->xmlCreator->writeElement('ДатаВремяСоздания', date(self::XML_DATETIME_FORMAT));
        $this->xmlCreator->writeElement('Источник', self::CONFIG_NAME_OUT);
        $this->xmlCreator->writeElement('Приемник', self::CONFIG_NAME_IN);
        $this->xmlCreator->writeElement('Параметры');
        $this->xmlCreator->writeElement('Обработки');
        $this->xmlCreator->startElement('ПравилаКонвертацииОбъектов');

        if ($rules = $this->getObjectsRules()) {
            foreach ($rules as $ruleName => $rule) {
                $this->xmlCreator->startElement('Правило');

                foreach ($rule as $rowName => $rowValue) {
                    if ($rowName == 'Свойства') {
                        $this->xmlCreator->startElement('Свойства');

                        foreach ($rowValue as $newRow => $newValue) {
                            $this->xmlCreator->startElement('Свойство');
                            if (array_key_exists('Атрибуты', $newValue)) {

                                foreach ($newValue['Атрибуты'] as $attrRow => $attrValue) {
                                    $this->xmlCreator->writeAttribute($attrRow, $attrValue);
                                }
                            }

                            foreach ($newValue as $row => $value) {
                                if (in_array($row, ['Источник', 'Приемник'])) {
                                    $this->xmlCreator->startElement($row);

                                    foreach ($value['Атрибуты'] as $attrRow => $attrValue) {
                                        $this->xmlCreator->writeAttribute($attrRow, $attrValue);
                                    }
                                    $this->xmlCreator->endElement();
                                } else {
                                    $this->xmlCreator->writeElement($row, $value);
                                }

                            }
                            $this->xmlCreator->endElement();
                        }
                        $this->xmlCreator->endElement();
                    } else {
                        $this->xmlCreator->writeElement($rowName, $rowValue);
                    }
                }

                $this->xmlCreator->endElement(); // !Правило
            }
        }

        $this->xmlCreator->endElement(); // !ПравилаКонвертацииОбъектов

        $this->xmlCreator->writeElement('ПравилаОчисткиДанных');
        $this->xmlCreator->writeElement('Алгоритмы');
        $this->xmlCreator->writeElement('Запросы');

        $this->xmlCreator->endElement(); // !ПравилаОбмена

        // Экспортируем
        if (is_array($this->objects) && sizeof($this->objects) > 0) {
            $done = false;
            $country = false;

            $existsContractor = [];
            $currency = false;
            $individualCurrency = null;
            $existNomenclature = [];
            $existCompany = [];
            $existBankInvoice = [];
            $existBank = [];
            $existCountry = [];
            $storage = false;
            $nomenclatureGroupProduct = false;
            $nomenclatureGroupService = false;
            $nomenclatureGroupMain = false;
            $receiptGoodsServiceAct = [];
            $saleGoodsServiceAct = [];
            $receiptGoodsServiceProduct = [];
            $saleGoodsServiceProduct = [];

            foreach ($this->objects as $className => $objectList) {
                if ($exportModel = OneCFactory::factory($className, $existsContractor, $currency, $individualCurrency, $existNomenclature
                    , $existCompany, $existBankInvoice, $existBank, $existCountry, $storage, $nomenclatureGroupProduct, $nomenclatureGroupService, $nomenclatureGroupMain
                    , $receiptGoodsServiceAct, $saleGoodsServiceAct, $receiptGoodsServiceProduct, $saleGoodsServiceProduct)
                ) {

                    if (in_array($className, [
                            ExportBase::EXPORT_DOCUMENT_ACT,
                            ExportBase::EXPORT_DOCUMENT_INVOICE,
                            ExportBase::EXPORT_DOCUMENT_PACKING_LIST,
                            ExportBase::EXPORT_DOCUMENT_PACKING_LIST,
                            ExportBase::EXPORT_DOCUMENT_INVOICE_FACTURE,
                            ExportBase::EXPORT_INVOICE_WITH_ACT_AND_PACKING_LIST,
                            ExportBase::EXPORT_UPD,
                        ]) && !$done
                    ) {
                        $this->generateXmlTree($this->addProductUnitAndType());
                        $countryArray[] = ExportCountry::render(ModelCountry::findOne(ModelCountry::COUNTRY_RUSSIA), $this->counter);
                        $this->generateXmlTree($countryArray);
                        $this->counter++;
                        $done = true;
                        $country = true;
                    } elseif ($className == ExportBase::EXPORT_CONTRACTOR && !$country) {
                        $countryArray[] = ExportCountry::render(ModelCountry::findOne(ModelCountry::COUNTRY_RUSSIA), $this->counter);
                        $this->generateXmlTree($countryArray);
                        $country = true;
                    } elseif ($className == ExportBase::EXPORT_PRODUCT_AND_SERVICE && !$done) {
                        $this->generateXmlTree($this->addProductUnitAndType());
                        $done = true;
                    }

                    if ($data = $exportModel->export($objectList, $this->counter, $this->exportModel)) {
                        $this->generateXmlTree($data);
                        unset($data);

                        $existsContractor = $exportModel->existsContractor;
                        $currency = $exportModel->currency;
                        $individualCurrency = $exportModel->individualCurrency;
                        $existNomenclature = $exportModel->existNomenclature;
                        $existCompany = $exportModel->existCompany;
                        $existBankInvoice = $exportModel->existBankInvoice;
                        $existBank = $exportModel->existBank;
                        $existCountry = $exportModel->existCountry;
                        $storage = $exportModel->storage;
                        $nomenclatureGroupProduct = $exportModel->nomenclatureGroupProduct;
                        $nomenclatureGroupService = $exportModel->nomenclatureGroupService;
                        $nomenclatureGroupMain = $exportModel->nomenclatureGroupMain;
                        $receiptGoodsServiceAct = $exportModel->receiptGoodsServiceAct;
                        $saleGoodsServiceAct = $exportModel->saleGoodsServiceAct;
                        $receiptGoodsServiceProduct = $exportModel->receiptGoodsServiceProduct;
                        $saleGoodsServiceProduct = $exportModel->saleGoodsServiceProduct;
                    }
                };
            }
        }
        $this->xmlCreator->endElement(); // !ФайлОбмена

        return true;
    }

    /**
     * Generate xml tree
     *
     * @param $exportData
     */
    protected function generateXmlTree($exportData)
    {
        if (is_array($exportData) && sizeof($exportData) > 0) {
            foreach ($exportData as $row) {
                if (isset($row['@name']) && !empty($row['@name'])) {
                    $this->xmlCreator->startElement($row['@name']);

                    if (isset($row['@attr']) && sizeof($row['@attr']) > 0) {
                        foreach ($row['@attr'] as $key => $value) {
                            $this->xmlCreator->writeAttribute($key, $value);
                        }
                    }

                    $this->appendChild($row);
                    $this->xmlCreator->endElement(); //! $row['@name']
                }
            }
        }
        $this->xmlCreator->flush();
    }

    /**
     * @param $name
     * @return $this
     * @throws NotFoundHttpException
     */
    protected function sendFile($name)
    {
        $filepath = $this->getSavePath($name);

        if (file_exists($filepath)) {
            return \Yii::$app->response->sendFile($filepath, $name, ['mimeType' => 'text/xml']);
        } else {
            throw new NotFoundHttpException('Такого файла не существует');
        }
    }

    /**
     * @param $item
     * @param $parent
     */
    private function appendChild($item, $parent = null)
    {
        if (isset($item['@child']) && is_array($item['@child'])) {
            foreach ($item['@child'] as $row) {
                if (isset($row['@name']) && !empty($row['@name'])) {
                    $value = isset($row['@value']) && !empty($row['@value']) ? $row['@value'] : null;

                    if ($row['@name'] == 'Значение' && empty($row['@value'])) {
                        $this->xmlCreator->startElement('Пусто');
                    } else {
                        $this->xmlCreator->startElement($row['@name']);

                        if (isset($row['@attr']) && sizeof($row['@attr']) > 0) {
                            foreach ($row['@attr'] as $key => $val) {
                                $this->xmlCreator->writeAttribute($key, $val);
                            }
                        }

                        $this->xmlCreator->text($value);
                    }

                    $this->appendChild($row);
                    $this->xmlCreator->endElement(); //
                }
            }
        }
    }

    /**
     * Add ProductUnit objects and ProductType objects
     */
    public function addProductUnitAndType()
    {
        $this->counter++;

        $productionUnit = ProductUnit::find()->all();

        $abc = [];
        foreach ($productionUnit as $unit) {
            $abc[] = [
                '@name' => 'Объект',
                '@attr' => [
                    'Нпп' => $this->counter,
                    'Тип' => 'СправочникСсылка.КлассификаторЕдиницИзмерения',
                    'ИмяПравила' => 'ЕдиницаИзмерения',
                ],
                '@child' => [
                    [
                        '@name' => 'Ссылка',
                        '@attr' => [
                            'Нпп' => $this->counter,
                        ],
                        '@child' => [
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Код',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $unit->code_okei,
                                    ],
                                ],
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'Наименование',
                            'Тип' => 'Строка',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => $unit->name,
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'НаименованиеПолное',
                            'Тип' => 'Строка',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => $unit->name,
                            ],
                        ],
                    ],
                ],
            ];
            $this->counter++;
        }

        $productionType = ProductType::find()->all();

        foreach ($productionType as $type) {
            $abc[] = [
                '@name' => 'Объект',
                '@attr' => [
                    'Нпп' => $this->counter,
                    'Имя' => 'ВидНоменклатуры',
                    'Тип' => 'СправочникСсылка.ВидыНоменклатуры',
                ],
                '@child' => [
                    [
                        '@name' => 'Ссылка',
                        '@attr' => [
                            'Нпп' => $this->counter,
                        ],
                        '@child' => [
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Наименование',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $type->name,
                                    ],
                                ],
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'Услуга',
                            'Тип' => 'Булево',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => $type->id == 1 ? 'true' : 'false',
                            ],
                        ],
                    ],
                ],
            ];
            $this->counter++;
        }

        return $abc;
    }

    public function translit($s) {
        $s = (string) $s;
        $s = strip_tags($s);
        $s = str_replace(array("\n", "\r"), " ", $s);
        $s = preg_replace("/\s+/", ' ', $s);
        $s = trim($s);
        $s = mb_strtolower($s);
        $s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
        $s = preg_replace("/[^0-9a-z-_. ]/i", "", $s);
        $s = str_replace(" ", "-", $s);
        return $s;
    }
}