<?php

namespace frontend\modules\export\models\one_c;

use common\components\date\DateHelper;
use common\models\address\Country;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\dictionary\bik\BikDictionary;

/**
 * Class Contractor
 * @package frontend\modules\export\models\one_c
 */
class ContractorOneC extends OneCObject implements IOneCExport
{
    /**
     * @inheritdoc
     */
    public static function exportRules()
    {
        return [
            'Контрагенты' => [
                'Код' => 'Контрагенты',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'Источник' => 'СправочникСсылка.Контрагенты',
                'Приемник' => 'СправочникСсылка.Контрагенты',
                'НеЗамещать' => 'true',
            ],
            'Банки' => [
                'Код' => 'Банки',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.Банки',
                'Приемник' => 'СправочникСсылка.Банки',
            ],
            'БанковскиеСчета' => [
                'Код' => 'БанковскиеСчета',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.БанковскиеСчета',
                'Приемник' => 'СправочникСсылка.БанковскиеСчета',
            ],
            'ВалютаВзаиморасчетов' => [
                'Код' => 'ВалютаВзаиморасчетов',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.Валюты',
                'Приемник' => 'СправочникСсылка.Валюты',
            ],
            'СтраныМира' => [
                'Код' => 'СтраныМира',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.СтраныМира',
                'Приемник' => 'СправочникСсылка.СтраныМира',
            ],
            'ЮридическоеФизическоеЛицо' => [
                'Код' => 'ЮридическоеФизическоеЛицо',
                'Источник' => 'ПеречислениеСсылка.ЮридическоеФизическоеЛицо',
                'Приемник' => 'ПеречислениеСсылка.ЮридическоеФизическоеЛицо',
            ],
            'Организации' => [
                'Код' => 'Организации',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.Организации',
                'Приемник' => 'СправочникСсылка.Организации',
                'НеЗамещать' => 'true',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function export($data = [], &$counter = null, &$exportModel = null)
    {
        $out = [];

        if ($this->individualCurrency == null) {
            $this->individualCurrency = OneCExport::generateGUID();
        }

        /**
         * @var $contractor \common\models\Contractor
         */
        foreach ($data as $contractor) {
            $counter++;

            if (empty($contractor->object_guid)) {
                $contractor->object_guid = OneCExport::generateGUID();
                $contractor->save(false);
            }

            $type = '';
            switch ($contractor->type) {
                case \common\models\Contractor::TYPE_SELLER:
                    $type = 'СПоставщиком';
                    break;
                case \common\models\Contractor::TYPE_CUSTOMER:
                    $type = 'СПокупателем';
                    break;
                case \common\models\Contractor::TYPE_FOUNDER:
                    $type = 'Прочее';
                    break;
            }


            if ($contractor->bank instanceof BikDictionary) {
                if (!in_array($contractor->bank->bik, $this->existBank)) {
                    array_push($this->existBank, $contractor->bank->bik);
                    $out[] = [
                        '@name' => 'Объект',
                        '@attr' => [
                            'Нпп' => $counter,
                            'Тип' => 'СправочникСсылка.Банки',
                            'ИмяПравила' => 'Банки',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Ссылка',
                                '@attr' => [
                                    'Нпп' => $counter,
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'Код',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $contractor->bank->bik,
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            $contractor->bank->address ?
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Адрес',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $contractor->bank->address,
                                        ]
                                    ],
                                ] : [],
                            $contractor->bank->city ?
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Город',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $contractor->bank->city,
                                        ]
                                    ],
                                ] : [],
                            $contractor->bank->ks ?
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'КоррСчет',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $contractor->bank->ks,
                                        ]
                                    ],
                                ] : [],
                            $contractor->bank->name ?
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Наименование',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $contractor->bank->name,
                                        ]
                                    ],
                                ] : [],
                            $contractor->bank->phone ?
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Телефоны',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $contractor->bank->phone,
                                        ]
                                    ],
                                ] : [],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ЭтоГруппа',
                                    'Тип' => 'Булево',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => 'false',
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Страна',
                                    'Тип' => 'СправочникСсылка.СтраныМира',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Ссылка',
                                        '@attr' => [
                                            'Нпп' => '',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'Код',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => Country::findOne(Country::COUNTRY_RUSSIA)->code,
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ];
                    $counter++;
                }
            }

            if (!$this->currency) {

                $this->currency = true;
                $out[] = [
                    '@name' => 'Объект',
                    '@attr' => [
                        'Нпп' => $counter,
                        'Тип' => 'СправочникСсылка.Валюты',
                        'ИмяПравила' => 'ВалютаВзаиморасчетов',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Ссылка',
                            '@attr' => [
                                'Нпп' => $counter,
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Код',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => '643',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Наименование',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'руб.',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'НаименованиеПолное',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'Российский рубль',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'СпособУстановкиКурса',
                                'Тип' => 'ПеречислениеСсылка.СпособыУстановкиКурсаВалюты',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'РучнойВвод',
                                ],
                            ],
                        ],
                    ],
                ];
                $counter++;
            }

            if ($contractor->company !== null) {
                $company = $contractor->company;


                if (!in_array($company->id, $this->existCompany)) {
                    array_push($this->existCompany, $company->id);
                    if ($company->company_type_id == CompanyType::TYPE_IP) {
                        $out[] = [
                            '@name' => 'Объект',
                            '@attr' => [
                                'Нпп' => $counter,
                                'Тип' => 'СправочникСсылка.ФизическиеЛица',
                                'ИмяПравила' => 'ФизическиеЛица',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => $counter,
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'ИНН',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $company->inn,
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'ФИО',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $company->getIpFio(),
                                        ],
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Наименование',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $company->getIpFio(),
                                        ],
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Код',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                        ],
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'ЭтоГруппа',
                                        'Тип' => 'Булево',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => 'false',
                                        ],
                                    ],
                                ],
                            ],
                        ];
                        $counter++;
                    }

                    $out[] = [
                        '@name' => 'Объект',
                        '@attr' => [
                            'Нпп' => $counter,
                            'Тип' => 'СправочникСсылка.Организации',
                            'ИмяПравила' => 'Организации',
                            'НеЗамещать' => 'true',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Ссылка',
                                '@attr' => [
                                    'Нпп' => $counter,
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'ИНН',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $company->inn,
                                            ],
                                        ],
                                    ],
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'КПП',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $company->kpp,
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ЮридическоеФизическоеЛицо',
                                    'Тип' => 'ПеречислениеСсылка.ЮридическоеФизическоеЛицо',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $company->company_type_id == CompanyType::TYPE_IP
                                            ? 'ФизическоеЛицо'
                                            : 'ЮридическоеЛицо',
                                    ],
                                ],
                            ],
                            $company->company_type_id == CompanyType::TYPE_IP ?
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'ИндивидуальныйПредприниматель',
                                        'Тип' => 'СправочникСсылка.ФизическиеЛица',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Ссылка',
                                            '@attr' => [
                                                'Нпп' => '',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Свойство',
                                                    '@attr' => [
                                                        'Имя' => 'ИНН',
                                                        'Тип' => 'Строка',
                                                    ],
                                                    '@child' => [
                                                        [
                                                            '@name' => 'Значение',
                                                            '@value' => $company->inn,
                                                        ],
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ] : [],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Наименование',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $company->name_full,
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'НаименованиеПолное',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $company->name_full,
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'НаименованиеСокращенное',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $company->name_short,
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ОГРН',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $company->ogrn,
                                    ],
                                ],
                            ],
                        ],
                    ];
                    $counter++;
                }
            }

            if (array_key_exists($contractor->ITN, $this->existsContractor)) {
                if (!in_array($contractor->PPC, $this->existsContractor[$contractor->ITN])) {
                    $this->existsContractor[$contractor->ITN][] = $contractor->PPC;
                    $out[] = [
                        '@name' => 'Объект',
                        '@attr' => [
                            'Нпп' => $counter,
                            'Тип' => 'СправочникСсылка.Контрагенты',
                            'ИмяПравила' => 'Контрагенты',
                            'НеЗамещать' => 'true',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Ссылка',
                                '@attr' => [
                                    'Нпп' => $counter,
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => '{УникальныйИдентификатор}',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $contractor->object_guid,
                                            ],
                                        ],
                                    ],
                                    $contractor->face_type === Contractor::TYPE_PHYSICAL_PERSON ? [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'Наименование',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => str_replace('"', '', $contractor->name),
                                            ],
                                        ],
                                    ] : [],
                                    !empty($contractor->ITN) ? [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'ИНН',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $contractor->ITN,
                                            ],
                                        ],
                                    ] : [],
                                    !empty($contractor->PPC) ?
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'КПП',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $contractor->PPC,
                                                ],
                                            ],
                                        ] : [],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ЮридическоеФизическоеЛицо',
                                    'Тип' => 'ПеречислениеСсылка.ЮридическоеФизическоеЛицо',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $contractor->company_type_id == CompanyType::TYPE_IP ||
                                        $contractor->face_type == Contractor::TYPE_PHYSICAL_PERSON
                                            ? 'ФизическоеЛицо'
                                            : 'ЮридическоеЛицо',
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ЭтоГруппа',
                                    'Тип' => 'Булево',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => 'false',
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'СтранаРегистрации',
                                    'Тип' => 'СправочникСсылка.СтраныМира',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Ссылка',
                                        '@attr' => [
                                            'Нпп' => '',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'Код',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => Country::findOne(Country::COUNTRY_RUSSIA)->code,
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Наименование',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => str_replace('"', '', $contractor->name),
                                    ],
                                ]
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ПометкаУдаления',
                                    'Тип' => 'Булево',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => 'false',
                                    ],
                                ],
                            ],
                            $contractor->BIN ?
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'ОГРН',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $contractor->BIN,
                                        ],
                                    ],
                                ] : [],
                            $contractor->legal_address ?
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'ЮридическийАдрес',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $contractor->legal_address,
                                        ],
                                    ],
                                ] : [],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ФормаСобственностиСокращенно',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => ($contractor->companyType instanceof CompanyType) ? $contractor->companyType->name_short : '',
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ФормаСобственностиПолностью',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => ($contractor->companyType instanceof CompanyType) ? $contractor->companyType->name_full : '',
                                    ],
                                ],
                            ],
                            $contractor->bank instanceof BikDictionary ?
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'РасчетныйСчет',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $contractor->current_account,
                                        ],
                                    ],
                                ] : [],
                            $contractor->bank instanceof BikDictionary ?
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'БанкБИК',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $contractor->bank->bik,
                                        ],
                                    ],
                                ] : [],
                        ],
                    ];
                    $counter++;
                    $this->existsContractor[$contractor->ITN][] = $type;
                    if (empty($contractor->document_guid)) {
                        $contractor->document_guid = OneCExport::generateGUID();
                        $contractor->save(false);
                    }
                }
            } else {
                $this->existsContractor[$contractor->ITN][] = $contractor->PPC;
                $out[] = [
                    '@name' => 'Объект',
                    '@attr' => [
                        'Нпп' => $counter,
                        'Тип' => 'СправочникСсылка.Контрагенты',
                        'ИмяПравила' => 'Контрагенты',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Ссылка',
                            '@attr' => [
                                'Нпп' => $counter,
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => '{УникальныйИдентификатор}',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $contractor->object_guid,
                                        ],
                                    ],
                                ],
                                $contractor->face_type === Contractor::TYPE_PHYSICAL_PERSON ? [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Наименование',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => str_replace('"', '', $contractor->name),
                                        ],
                                    ],
                                ] : [],
                                !empty($contractor->ITN) ? [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'ИНН',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $contractor->ITN,
                                        ],
                                    ],
                                ] : [],
                                !empty($contractor->PPC) ?
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'КПП',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $contractor->PPC,
                                            ],
                                        ],
                                    ] : [],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ЮридическоеФизическоеЛицо',
                                'Тип' => 'ПеречислениеСсылка.ЮридическоеФизическоеЛицо',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $contractor->company_type_id == CompanyType::TYPE_IP ||
                                    $contractor->face_type == Contractor::TYPE_PHYSICAL_PERSON
                                        ? 'ФизическоеЛицо'
                                        : 'ЮридическоеЛицо',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ЭтоГруппа',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'СтранаРегистрации',
                                'Тип' => 'СправочникСсылка.СтраныМира',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'Код',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => Country::findOne(Country::COUNTRY_RUSSIA)->code,
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Наименование',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => str_replace('"', '', $contractor->name),
                                ],
                            ]
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ПометкаУдаления',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ],
                            ],
                        ],
                        $contractor->BIN ?
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ОГРН',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $contractor->BIN,
                                    ],
                                ],
                            ] : [],
                        $contractor->legal_address ?
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ЮридическийАдрес',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $contractor->legal_address,
                                    ],
                                ],
                            ] : [],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ФормаСобственностиСокращенно',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => ($contractor->companyType instanceof CompanyType) ? $contractor->companyType->name_short : '',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ФормаСобственностиПолностью',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => ($contractor->companyType instanceof CompanyType) ? $contractor->companyType->name_full : '',
                                ],
                            ],
                        ],
                        $contractor->bank instanceof BikDictionary ?
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'РасчетныйСчет',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $contractor->current_account,
                                    ],
                                ],
                            ] : [],
                        $contractor->bank instanceof BikDictionary ?
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'БанкБИК',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $contractor->bank->bik,
                                    ],
                                ],
                            ] : [],
                    ],
                ];
                $counter++;
            }
            if ($contractor->bank instanceof BikDictionary) {
                if (!in_array($contractor->current_account, $this->existBankInvoice)) {
                    array_push($this->existBankInvoice, $contractor->current_account);
                    if (empty($contractor->current_account_guid)) {
                        $contractor->current_account_guid = OneCExport::generateGUID();
                        $contractor->save(false);
                    }
                    $out[] = [
                        '@name' => 'Объект',
                        '@attr' => [
                            'Нпп' => $counter,
                            'Тип' => 'СправочникСсылка.БанковскиеСчета',
                            'ИмяПравила' => 'БанковскиеСчета',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Ссылка',
                                '@attr' => [
                                    'Нпп' => $counter,
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => '{УникальныйИдентификатор}',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $contractor->current_account_guid,
                                            ],
                                        ],
                                    ],
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'НомерСчета',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $contractor->current_account,
                                            ]
                                        ],
                                    ],
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'Владелец',
                                            'Тип' => 'СправочникСсылка.Контрагенты',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Ссылка',
                                                '@attr' => [
                                                    'Нпп' => '',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Свойство',
                                                        '@attr' => [
                                                            'Имя' => '{УникальныйИдентификатор}',
                                                            'Тип' => 'Строка',
                                                        ],
                                                        '@child' => [
                                                            [
                                                                '@name' => 'Значение',
                                                                '@value' => $contractor->object_guid,
                                                            ],
                                                        ],
                                                    ],
                                                    $contractor->face_type === Contractor::TYPE_PHYSICAL_PERSON ? [
                                                        '@name' => 'Свойство',
                                                        '@attr' => [
                                                            'Имя' => 'Наименование',
                                                            'Тип' => 'Строка',
                                                        ],
                                                        '@child' => [
                                                            [
                                                                '@name' => 'Значение',
                                                                '@value' => str_replace('"', '', $contractor->name),
                                                            ],
                                                        ],
                                                    ] : [],
                                                    !empty($contractor->ITN) ? [
                                                        '@name' => 'Свойство',
                                                        '@attr' => [
                                                            'Имя' => 'ИНН',
                                                            'Тип' => 'Строка',
                                                        ],
                                                        '@child' => [
                                                            [
                                                                '@name' => 'Значение',
                                                                '@value' => $contractor->ITN,
                                                            ],
                                                        ],
                                                    ] : [],
                                                    !empty($contractor->PPC) ?
                                                        [
                                                            '@name' => 'Свойство',
                                                            '@attr' => [
                                                                'Имя' => 'КПП',
                                                                'Тип' => 'Строка',
                                                            ],
                                                            '@child' => [
                                                                [
                                                                    '@name' => 'Значение',
                                                                    '@value' => $contractor->PPC,
                                                                ],
                                                            ],
                                                        ] : [],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Банк',
                                    'Тип' => 'СправочникСсылка.Банки',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Ссылка',
                                        '@attr' => [
                                            'Нпп' => '',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'Код',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => $contractor->bank->bik,
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ВалютаДенежныхСредств',
                                    'Тип' => 'СправочникСсылка.Валюты',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Ссылка',
                                        '@attr' => [
                                            'Нпп' => '',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'Код',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => '643',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Валютный',
                                    'Тип' => 'Булево',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => 'false',
                                    ]
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ВидСчета',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => 'Расчетный',
                                    ]
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ВсегдаУказыватьКПП',
                                    'Тип' => 'Булево',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => 'false',
                                    ]
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'МесяцПрописью',
                                    'Тип' => 'Булево',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => 'false',
                                    ]
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Наименование',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $contractor->bank->name,
                                    ]
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ПометкаУдаления',
                                    'Тип' => 'Булево',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => 'false',
                                    ]
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'СуммаБезКопеек',
                                    'Тип' => 'Булево',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => 'false',
                                    ]
                                ],
                            ],
                        ],
                    ];
                }
            }

            $exportModel->updateCounters(['objects_completed' => 1]);
        }

        return $out;
    }
}
