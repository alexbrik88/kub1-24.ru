<?php

namespace frontend\modules\export\models\one_c;

use common\models\address\Country;
use common\models\Company;
use common\models\product\Product;
use common\models\product\ProductUnit;

/**
 * @var $row Product
 */

/**
 * Class ProductAndService
 * @package frontend\modules\export\models\one_c
 */
class ProductAndService extends OneCObject implements IOneCExport
{
    /**
     * @inheritdoc
     */
    public static function exportRules()
    {
        return [
            'СтраныМира' => [
                'Код' => 'СтраныМира',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.СтраныМира',
                'Приемник' => 'СправочникСсылка.СтраныМира',
            ],
            'Номенклатура' => [
                'Код' => 'Номенклатура',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.Номенклатура',
                'Приемник' => 'СправочникСсылка.Номенклатура',
            ],
            'СтавкиНДС' => [
                'Код' => 'СтавкиНДС',
                'Источник' => 'ПеречислениеСсылка.СтавкиНДС',
                'Приемник' => 'ПеречислениеСсылка.СтавкиНДС',
            ],
            'ЕдиницаИзмерения' => [
                'Код' => 'ЕдиницаИзмерения',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.КлассификаторЕдиницИзмерения',
                'Приемник' => 'СправочникСсылка.КлассификаторЕдиницИзмерения',
            ],
            'НоменклатурныеГруппы' => [
                'Код' => 'НоменклатурныеГруппы',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.НоменклатурныеГруппы',
                'Приемник' => 'СправочникСсылка.НоменклатурныеГруппы',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function export($data = [], &$counter = null, &$exportModel = null)
    {
        $products = [];

        if (isset($data) && is_array($data)) {
            foreach ($data as $index => $row) {
                if (empty($row->object_guid)) {
                    $row->setAttribute('object_guid', OneCExport::generateGUID());
                    $row->save(false);
                }
                $counter++;

                if ($row->countryOrigin instanceof Country && $row->countryOrigin->id > 1) {

                    $productCountry = [
                        '@name' => 'Ссылка',
                        '@attr' => [
                            'Нпп' => '',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Код',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $row->countryOrigin->code,
                                    ],
                                ],
                            ],
                        ],
                    ];

                    if (!in_array($row->countryOrigin->code, $this->existCountry)) {
                        array_push($this->existCountry, $row->countryOrigin->code);

                        if ($row->countryOrigin->id !== Country::COUNTRY_RUSSIA && $row->countryOrigin->id !== Country::COUNTRY_WITHOUT) {
                            $products[] = \frontend\modules\export\models\one_c\Country::render($row->countryOrigin, $counter++);
                        }
                    }
                } else {
                    $productCountry = [];
                }

                if (!$this->nomenclatureGroupService) {
                    $this->nomenclatureGroupService = true;
                    $products[] = [
                        '@name' => 'Объект',
                        '@attr' => [
                            'Нпп' => $counter,
                            'Тип' => 'СправочникСсылка.НоменклатурныеГруппы',
                            'ИмяПравила' => 'НоменклатурныеГруппы',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Ссылка',
                                '@attr' => [
                                    'Нпп' => $counter,
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'Наименование',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => 'Услуги',
                                            ],
                                        ],
                                    ],
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'ЭтоГруппа',
                                            'Тип' => 'Булево',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => 'false',
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Родитель',
                                    'Тип' => 'СправочникСсылка.НоменклатурныеГруппы',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                    ],
                                ],
                            ],
                        ],
                    ];
                    $counter++;
                }

                if (!$this->nomenclatureGroupProduct) {
                    $this->nomenclatureGroupProduct = true;
                    $products[] = [
                        '@name' => 'Объект',
                        '@attr' => [
                            'Нпп' => $counter,
                            'Тип' => 'СправочникСсылка.НоменклатурныеГруппы',
                            'ИмяПравила' => 'НоменклатурныеГруппы',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Ссылка',
                                '@attr' => [
                                    'Нпп' => $counter,
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'Наименование',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => 'Товары',
                                            ],
                                        ],
                                    ],
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'ЭтоГруппа',
                                            'Тип' => 'Булево',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => 'false',
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Родитель',
                                    'Тип' => 'СправочникСсылка.НоменклатурныеГруппы',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                    ],
                                ],
                            ],
                        ],
                    ];
                    $counter++;
                }

                if (!$this->nomenclatureGroupMain) {
                    $this->nomenclatureGroupMain = true;
                    $products[] = self::getMainNomenclatureGroup($counter);
                    $counter++;
                }

                if (!in_array($row->id, $this->existNomenclature)) {
                    array_push($this->existNomenclature, $row->id);
                    if (empty($row->object_guid)) {
                        $row->object_guid = OneCExport::generateGUID();
                        $row->save(false);
                    }

                    $products[] = [
                        '@name' => 'Объект',
                        '@attr' => [
                            'Нпп' => $counter,
                            'Тип' => 'СправочникСсылка.Номенклатура',
                            'ИмяПравила' => 'Номенклатура',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Ссылка',
                                '@attr' => [
                                    'Нпп' => $counter,
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => '{УникальныйИдентификатор}',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $row->object_guid,
                                            ],
                                        ],
                                    ],
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'ВидНоменклатуры',
                                            'Тип' => 'СправочникСсылка.ВидыНоменклатуры',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Ссылка',
                                                '@attr' => [
                                                    'Нпп' => '',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Свойство',
                                                        '@attr' => [
                                                            'Имя' => 'Наименование',
                                                            'Тип' => 'Строка',
                                                        ],
                                                        '@child' => [
                                                            [
                                                                '@name' => 'Значение',
                                                                '@value' => $row->getType()->name,
                                                            ],
                                                        ],
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'Наименование',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => str_replace(PHP_EOL, '', preg_replace('/\s{2,}/', ' ', mb_substr($row->title, 0, 100, 'UTF-8'))),
                                            ],
                                        ],
                                    ],
                                    $row->production_type ?
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'ЕдиницаИзмерения',
                                                'Тип' => 'СправочникСсылка.КлассификаторЕдиницИзмерения',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Ссылка',
                                                    '@attr' => [
                                                        'Нпп' => '',
                                                    ],
                                                    '@child' => [
                                                        [
                                                            '@name' => 'Свойство',
                                                            '@attr' => [
                                                                'Имя' => 'Код',
                                                                'Тип' => 'Строка',
                                                            ],
                                                            '@child' => [
                                                                [
                                                                    '@name' => 'Значение',
                                                                    '@value' => ProductUnit::findOne($row->product_unit_id)->code_okei,
                                                                ],
                                                            ],
                                                        ],
                                                    ],
                                                ],
                                            ],
                                        ] : [],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Услуга',
                                    'Тип' => 'Булево',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $row->production_type ? 'false' : 'true',
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'НоменклатурнаяГруппа',
                                    'Тип' => 'СправочникСсылка.НоменклатурныеГруппы',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Ссылка',
                                        '@attr' => [
                                            'Нпп' => '',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'Наименование',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        //'@value' => $row->production_type ? 'Товары' : 'Услуги',
                                                        '@value' => 'Основная номенклатурная группа',
                                                    ],
                                                ],
                                            ],
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'ЭтоГруппа',
                                                    'Тип' => 'Булево',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => 'false',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Код',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $row->code,
                                    ],
                                ],
                            ],
                            $row->production_type && $row->country_origin_id !== Country::COUNTRY_WITHOUT ?
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'СтранаПроисхождения',
                                        'Тип' => 'СправочникСсылка.СтраныМира',
                                    ],
                                    '@child' => [
                                        $productCountry,
                                    ],
                                ] : [],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ЭтоГруппа',
                                    'Тип' => 'Булево',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => 'false',
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Родитель',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $row->production_type ? 'Товары' : 'Услуги',
                                    ],
                                ],
                            ],
                        ],
                    ];
                    $counter++;
                }

                $exportModel->updateCounters(['objects_completed' => 1]);
            }
        }

        return $products;
    }
}