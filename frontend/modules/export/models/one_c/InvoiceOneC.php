<?php

namespace frontend\modules\export\models\one_c;

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\address\Country as CountryService;
use common\models\Agreement;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\dictionary\bik\BikDictionary;
use common\models\document\Order;
use common\models\document\status\InvoiceStatus;
use common\models\product\ProductUnit;
use common\models\TaxRate;
use frontend\models\Documents;

/**
 * Class Invoice
 * @package frontend\modules\export\models\one_c
 */
class InvoiceOneC extends OneCObject implements IOneCExport
{
    /**
     * @var array
     */
    protected $out = [];

    /**
     * @var int
     */
    protected $counter = 0;

    /**
     * @var null
     */
    protected $exportModel = null;

    /**
     * @inheritdoc
     */
    public static function exportRules()
    {
        return [
            'СчетНаОплатуПоставщика' => [
                'Код' => 'СчетНаОплатуПоставщика',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'true',
                'Источник' => 'ДокументСсылка.СчетНаОплатуПоставщика',
                'Приемник' => 'ДокументСсылка.СчетНаОплатуПоставщика',
            ],
            'СчетНаОплатуПокупателю' => [
                'Код' => 'СчетНаОплатуПокупателю',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'true',
                'Источник' => 'ДокументСсылка.СчетНаОплатуПокупателю',
                'Приемник' => 'ДокументСсылка.СчетНаОплатуПокупателю',
            ],
            'Номенклатура' => [
                'Код' => 'Номенклатура',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.Номенклатура',
                'Приемник' => 'СправочникСсылка.Номенклатура',
            ],
            'Организации' => [
                'Код' => 'Организации',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.Организации',
                'Приемник' => 'СправочникСсылка.Организации',
                'НеЗамещать' => 'true',
            ],
            'СтавкиНДС' => [
                'Код' => 'СтавкиНДС',
                'Источник' => 'ПеречислениеСсылка.СтавкиНДС',
                'Приемник' => 'ПеречислениеСсылка.СтавкиНДС',
            ],
            'ЮридическоеФизическоеЛицо' => [
                'Код' => 'ЮридическоеФизическоеЛицо',
                'Источник' => 'ПеречислениеСсылка.ЮридическоеФизическоеЛицо',
                'Приемник' => 'ПеречислениеСсылка.ЮридическоеФизическоеЛицо',
            ],
            'СтатусДокумента' => [
                'Код' => 'СтатусДокумента',
                'Источник' => 'ПеречислениеСсылка.СтатусОплатыСчета',
                'Приемник' => 'ПеречислениеСсылка.СтатусОплатыСчета',
            ],
            'ФизическиеЛица' => [
                'Код' => 'ФизическиеЛица',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.ФизическиеЛица',
                'Приемник' => 'СправочникСсылка.ФизическиеЛица',
            ],
            'ЕдиницаИзмерения' => [
                'Код' => 'ЕдиницаИзмерения',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.КлассификаторЕдиницИзмерения',
                'Приемник' => 'СправочникСсылка.КлассификаторЕдиницИзмерения',
            ],
            'СтраныМира' => [
                'Код' => 'СтраныМира',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.СтраныМира',
                'Приемник' => 'СправочникСсылка.СтраныМира',
            ],
            'Контрагенты' => [
                'Код' => 'Контрагенты',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'Источник' => 'СправочникСсылка.Контрагенты',
                'Приемник' => 'СправочникСсылка.Контрагенты',
                'НеЗамещать' => 'true',
            ],
            'ДоговорыКонтрагентов' => [
                'Код' => 'ДоговорыКонтрагентов',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.ДоговорыКонтрагентов',
                'Приемник' => 'СправочникСсылка.ДоговорыКонтрагентов',
            ],
            'ВидДоговора' => [
                'Код' => 'ВидыДоговоровКонтрагентов',
                'Источник' => 'ПеречислениеСсылка.ВидыДоговоровКонтрагентов',
                'Приемник' => 'ПеречислениеСсылка.ВидыДоговоровКонтрагентов',
            ],
            'Банки' => [
                'Код' => 'Банки',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.Банки',
                'Приемник' => 'СправочникСсылка.Банки',
            ],
            'БанковскиеСчета' => [
                'Код' => 'БанковскиеСчета',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.БанковскиеСчета',
                'Приемник' => 'СправочникСсылка.БанковскиеСчета',
            ],
            'НоменклатурныеГруппы' => [
                'Код' => 'НоменклатурныеГруппы',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.НоменклатурныеГруппы',
                'Приемник' => 'СправочникСсылка.НоменклатурныеГруппы',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function export($data = [], &$counter = null, &$exportModel = null)
    {
        $this->counter = $counter;
        $this->exportModel = $exportModel;

        if (isset($data[Documents::IO_TYPE_IN])) {
            $this->exportInDocuments($data[Documents::IO_TYPE_IN]);
        }

        if (isset($data[Documents::IO_TYPE_OUT])) {
            $this->exportOutDocuments($data[Documents::IO_TYPE_OUT]);
        }

        $counter = $this->counter;

        return $this->out;
    }

    /**
     * @param $data
     */
    protected function exportInDocuments($data)
    {
        foreach ($data as $invoice) {

            if (empty($invoice->object_guid)) {
                $invoice->object_guid = OneCExport::generateGUID();
                $invoice->save(false, ['object_guid']);
            } elseif (substr($invoice->object_guid, 0, 3) === 'UPD') {
                continue;
            }

            if (empty($invoice->contractor->object_guid)) {
                $invoice->contractor->object_guid = OneCExport::generateGUID();
                if (!$invoice->contractor->isNewRecord) {
                    $invoice->contractor->save(false, ['object_guid']);
                }
            }
            $this->counter++;

            $goodsOrders = $serviceOrders = [];

            $type = '';
            switch ($invoice->contractor->type) {
                case Contractor::TYPE_SELLER:
                    $type = 'СПоставщиком';
                    break;
                case Contractor::TYPE_CUSTOMER:
                    $type = 'СПокупателем';
                    break;
                case Contractor::TYPE_FOUNDER:
                    $type = 'Прочее';
                    break;
            }

            /* @var Order[] $invoiceOrders */
            if ($invoiceOrders = $invoice->orders) {
                if (is_array($invoiceOrders) && sizeof($invoiceOrders) > 0) {
                    foreach ($invoiceOrders as $invoiceOrder) {
                        if ($invoiceOrder->purchase_tax > 0 && $invoiceOrder->purchaseTaxRate instanceof TaxRate && $invoiceOrder->purchaseTaxRate->rate > 0) {
                            $nds = [
                                '@name' => 'Значение',
                                '@value' => 'НДС' . round($invoiceOrder->purchaseTaxRate->rate * 100),
                            ];
                        } else {
                            $nds = [
                                '@name' => 'Значение',
                                '@value' => 'БезНДС',
                            ];
                        }
                        if (empty($invoiceOrder->product->object_guid)) {
                            $invoiceOrder->product->object_guid = OneCExport::generateGUID();
                            $invoiceOrder->product->save(false);
                        }

                        $_order = [
                            '@name' => 'Запись',
                            '@child' => [
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Количество',
                                        'Тип' => 'Число',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $invoiceOrder->quantity,
                                        ]
                                    ]
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Номенклатура',
                                        'Тип' => 'СправочникСсылка.Номенклатура',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Ссылка',
                                            '@attr' => [
                                                'Нпп' => '',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Свойство',
                                                    '@attr' => [
                                                        'Имя' => '{УникальныйИдентификатор}',
                                                        'Тип' => 'Строка',
                                                    ],
                                                    '@child' => [
                                                        [
                                                            '@name' => 'Значение',
                                                            '@value' => $invoiceOrder->product->object_guid,
                                                        ],
                                                    ],
                                                ],
                                                [
                                                    '@name' => 'Свойство',
                                                    '@attr' => [
                                                        'Имя' => 'ВидНоменклатуры',
                                                        'Тип' => 'СправочникСсылка.ВидыНоменклатуры',
                                                    ],
                                                    '@child' => [
                                                        [
                                                            '@name' => 'Ссылка',
                                                            '@attr' => [
                                                                'Нпп' => '',
                                                            ],
                                                            '@child' => [
                                                                [
                                                                    '@name' => 'Свойство',
                                                                    '@attr' => [
                                                                        'Имя' => 'Наименование',
                                                                        'Тип' => 'Строка',
                                                                    ],
                                                                    '@child' => [
                                                                        [
                                                                            '@name' => 'Значение',
                                                                            '@value' => $invoiceOrder->product->getType()->name,
                                                                        ],
                                                                    ],
                                                                ],
                                                            ],
                                                        ],
                                                    ],
                                                ],
                                                [
                                                    '@name' => 'Свойство',
                                                    '@attr' => [
                                                        'Имя' => 'Наименование',
                                                        'Тип' => 'Строка',
                                                    ],
                                                    '@child' => [
                                                        [
                                                            '@name' => 'Значение',
                                                            '@value' => str_replace(PHP_EOL, '', preg_replace('/\s{2,}/', ' ', mb_substr($invoiceOrder->product_title, 0, 255, 'UTF-8'))),
                                                        ],
                                                    ],
                                                ],
                                                $invoiceOrder->product->production_type ?
                                                    [
                                                        '@name' => 'Свойство',
                                                        '@attr' => [
                                                            'Имя' => 'ЕдиницаИзмерения',
                                                            'Тип' => 'СправочникСсылка.КлассификаторЕдиницИзмерения',
                                                        ],
                                                        '@child' => [
                                                            [
                                                                '@name' => 'Ссылка',
                                                                '@attr' => [
                                                                    'Нпп' => '',
                                                                ],
                                                                '@child' => [
                                                                    [
                                                                        '@name' => 'Свойство',
                                                                        '@attr' => [
                                                                            'Имя' => 'Код',
                                                                            'Тип' => 'Строка',
                                                                        ],
                                                                        '@child' => [
                                                                            [
                                                                                '@name' => 'Значение',
                                                                                '@value' => ProductUnit::findOne($invoiceOrder->product->product_unit_id)->code_okei,
                                                                            ],
                                                                        ],
                                                                    ],
                                                                ],
                                                            ],
                                                        ],
                                                    ] : [],
                                            ],
                                        ],
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Содержание',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $invoiceOrder->product_title,
                                        ],
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'СтавкаНДС',
                                        'Тип' => 'ПеречислениеСсылка.СтавкиНДС',
                                    ],
                                    '@child' => [
                                        $nds,
                                    ]
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Сумма',
                                        'Тип' => 'Число',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => TextHelper::moneyFormatFromIntToFloat($invoiceOrder->amount_purchase_with_vat),
                                        ],
                                    ]
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'СуммаНДС',
                                        'Тип' => 'Число',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => TextHelper::moneyFormatFromIntToFloat($invoiceOrder->purchase_tax),
                                        ],
                                    ]
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Цена',
                                        'Тип' => 'Число',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => TextHelper::moneyFormatFromIntToFloat($invoiceOrder->amount_purchase_with_vat),
                                        ],
                                    ]
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'ЦенаБезНДС',
                                        'Тип' => 'Число',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => TextHelper::moneyFormatFromIntToFloat($invoiceOrder->amount_purchase_no_vat),
                                        ],
                                    ]
                                ],
                            ],
                        ];

                        if ($invoiceOrder->product->production_type) {
                            $goodsOrders[] = $_order;
                        } else {
                            $serviceOrders[] = $_order;
                        }
                    }
                }
            }

            if (!$this->nomenclatureGroupService) {
                $this->nomenclatureGroupService = true;
                $this->out[] = [
                    '@name' => 'Объект',
                    '@attr' => [
                        'Нпп' => $this->counter,
                        'Тип' => 'СправочникСсылка.НоменклатурныеГруппы',
                        'ИмяПравила' => 'НоменклатурныеГруппы',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Ссылка',
                            '@attr' => [
                                'Нпп' => $this->counter,
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Наименование',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => 'Услуги',
                                        ],
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'ЭтоГруппа',
                                        'Тип' => 'Булево',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => 'false',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Родитель',
                                'Тип' => 'СправочникСсылка.НоменклатурныеГруппы',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                ],
                            ],
                        ],
                    ],
                ];
                $this->counter++;
            }

            if (!$this->nomenclatureGroupProduct) {
                $this->nomenclatureGroupProduct = true;
                $this->out[] = [
                    '@name' => 'Объект',
                    '@attr' => [
                        'Нпп' => $this->counter,
                        'Тип' => 'СправочникСсылка.НоменклатурныеГруппы',
                        'ИмяПравила' => 'НоменклатурныеГруппы',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Ссылка',
                            '@attr' => [
                                'Нпп' => $this->counter,
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Наименование',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => 'Товары',
                                        ],
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'ЭтоГруппа',
                                        'Тип' => 'Булево',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => 'false',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Родитель',
                                'Тип' => 'СправочникСсылка.НоменклатурныеГруппы',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                ],
                            ],
                        ],
                    ],
                ];
                $this->counter++;
            }

            if (!$this->nomenclatureGroupMain) {
                $this->nomenclatureGroupMain = true;
                $this->out[] = self::getMainNomenclatureGroup($this->counter);
                $this->counter++;
            }

            foreach ($invoiceOrders as $invoiceOrder) {

                if (!in_array($invoiceOrder->product->countryOrigin->code, $this->existCountry)) {
                    array_push($this->existCountry, $invoiceOrder->product->countryOrigin->code);

                    if ($invoiceOrder->product->countryOrigin->id !== CountryService::COUNTRY_RUSSIA && $invoiceOrder->product->countryOrigin->id !== CountryService::COUNTRY_WITHOUT) {
                        $this->out[] = Country::render($invoiceOrder->product->countryOrigin, $this->counter);
                        $this->counter++;
                    }
                }

                if (!in_array($invoiceOrder->product->id, $this->existNomenclature)) {
                    array_push($this->existNomenclature, $invoiceOrder->product->id);
                    if (empty($invoiceOrder->product->object_guid)) {
                        $invoiceOrder->product->object_guid = OneCExport::generateGUID();
                        $invoiceOrder->product->save(false);
                    }
                    $this->out[] = [
                        '@name' => 'Объект',
                        '@attr' => [
                            'Нпп' => $this->counter,
                            'Тип' => 'СправочникСсылка.Номенклатура',
                            'ИмяПравила' => 'Номенклатура',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Ссылка',
                                '@attr' => [
                                    'Нпп' => $this->counter,
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => '{УникальныйИдентификатор}',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoiceOrder->product->object_guid,
                                            ],
                                        ],
                                    ],
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'ВидНоменклатуры',
                                            'Тип' => 'СправочникСсылка.ВидыНоменклатуры',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Ссылка',
                                                '@attr' => [
                                                    'Нпп' => '',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Свойство',
                                                        '@attr' => [
                                                            'Имя' => 'Наименование',
                                                            'Тип' => 'Строка',
                                                        ],
                                                        '@child' => [
                                                            [
                                                                '@name' => 'Значение',
                                                                '@value' => $invoiceOrder->product->getType()->name,
                                                            ],
                                                        ],
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'Наименование',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => str_replace(PHP_EOL, '', preg_replace('/\s{2,}/', ' ', mb_substr($invoiceOrder->product_title, 0, 255, 'UTF-8'))),
                                            ],
                                        ],
                                    ],
                                    $invoiceOrder->product->production_type ?
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'ЕдиницаИзмерения',
                                                'Тип' => 'СправочникСсылка.КлассификаторЕдиницИзмерения',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Ссылка',
                                                    '@attr' => [
                                                        'Нпп' => '',
                                                    ],
                                                    '@child' => [
                                                        [
                                                            '@name' => 'Свойство',
                                                            '@attr' => [
                                                                'Имя' => 'Код',
                                                                'Тип' => 'Строка',
                                                            ],
                                                            '@child' => [
                                                                [
                                                                    '@name' => 'Значение',
                                                                    '@value' => ProductUnit::findOne($invoiceOrder->product->product_unit_id)->code_okei,
                                                                ],
                                                            ],
                                                        ],
                                                    ],
                                                ],
                                            ],
                                        ] : [],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Услуга',
                                    'Тип' => 'Булево',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $invoiceOrder->product->production_type ? 'false' : 'true',
                                    ],
                                ],
                            ],
                            $invoiceOrder->product->production_type && $invoiceOrder->product->country_origin_id !== CountryService::COUNTRY_WITHOUT ?
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'СтранаПроисхождения',
                                        'Тип' => 'СправочникСсылка.СтраныМира',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Ссылка',
                                            '@attr' => [
                                                'Нпп' => '',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Свойство',
                                                    '@attr' => [
                                                        'Имя' => 'Код',
                                                        'Тип' => 'Строка',
                                                    ],
                                                    '@child' => [
                                                        [
                                                            '@name' => 'Значение',
                                                            '@value' => $invoiceOrder->product->countryOrigin->code,
                                                        ],
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ] : [],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'НоменклатурнаяГруппа',
                                    'Тип' => 'СправочникСсылка.НоменклатурныеГруппы',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Ссылка',
                                        '@attr' => [
                                            'Нпп' => '',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'Наименование',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        //'@value' => $invoiceOrder->product->production_type ? 'Товары' : 'Услуги',
                                                        '@value' => 'Основная номенклатурная группа',
                                                    ],
                                                ],
                                            ],
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'ЭтоГруппа',
                                                    'Тип' => 'Булево',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => 'false',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Код',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $invoiceOrder->product->code,
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ЭтоГруппа',
                                    'Тип' => 'Булево',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => 'false',
                                    ],
                                ],
                            ],
                        ],
                    ];
                    $this->counter++;
                }
            }

            if (!in_array($invoice->company->id, $this->existCompany)) {
                array_push($this->existCompany, $invoice->company->id);
                if ($invoice->company->company_type_id == CompanyType::TYPE_IP) {
                    $this->out[] = [
                        '@name' => 'Объект',
                        '@attr' => [
                            'Нпп' => $this->counter,
                            'Тип' => 'СправочникСсылка.ФизическиеЛица',
                            'ИмяПравила' => 'ФизическиеЛица',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Ссылка',
                                '@attr' => [
                                    'Нпп' => $this->counter,
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'ИНН',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->company->inn,
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ФИО',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $invoice->company->getIpFio(),
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Наименование',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $invoice->company->getIpFio(),
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Код',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ЭтоГруппа',
                                    'Тип' => 'Булево',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => 'false',
                                    ],
                                ],
                            ],
                        ],
                    ];
                    $this->counter++;
                }

                $this->out[] = [
                    '@name' => 'Объект',
                    '@attr' => [
                        'Нпп' => $this->counter,
                        'Тип' => 'СправочникСсылка.Организации',
                        'ИмяПравила' => 'Организации',
                        'НеЗамещать' => 'true',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Ссылка',
                            '@attr' => [
                                'Нпп' => $this->counter,
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'ИНН',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $invoice->company->inn,
                                        ],
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'КПП',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $invoice->company->kpp,
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ЮридическоеФизическоеЛицо',
                                'Тип' => 'ПеречислениеСсылка.ЮридическоеФизическоеЛицо',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $invoice->company->company_type_id == CompanyType::TYPE_IP
                                        ? 'ФизическоеЛицо'
                                        : 'ЮридическоеЛицо',
                                ],
                            ],
                        ],
                        $invoice->company->company_type_id == CompanyType::TYPE_IP ?
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ИндивидуальныйПредприниматель',
                                    'Тип' => 'СправочникСсылка.ФизическиеЛица',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Ссылка',
                                        '@attr' => [
                                            'Нпп' => '',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'ИНН',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => $invoice->company->inn,
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ] : [],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Наименование',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $invoice->company->name_full,
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'НаименованиеПолное',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $invoice->company->name_full,
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'НаименованиеСокращенное',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $invoice->company->name_short,
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ОГРН',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $invoice->company->ogrn,
                                ],
                            ],
                        ],
                    ],
                ];
                $this->counter++;


                if ($invoice->company->bank instanceof BikDictionary) {
                    if (!in_array($invoice->company->bank->bik, $this->existBank)) {
                        array_push($this->existBank, $invoice->company->bank->bik);
                        $this->out[] = [
                            '@name' => 'Объект',
                            '@attr' => [
                                'Нпп' => $this->counter,
                                'Тип' => 'СправочникСсылка.Банки',
                                'ИмяПравила' => 'Банки',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => $this->counter,
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'Код',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $invoice->company->bank->bik,
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                                $invoice->company->bank->address ?
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'Адрес',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->company->bank->address,
                                            ]
                                        ],
                                    ] : [],
                                $invoice->company->bank->city ?
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'Город',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->company->bank->city,
                                            ]
                                        ],
                                    ] : [],
                                $invoice->company->bank->ks ?
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'КоррСчет',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->company->bank->ks,
                                            ]
                                        ],
                                    ] : [],
                                $invoice->company->bank->name ?
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'Наименование',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->company->bank->name,
                                            ]
                                        ],
                                    ] : [],
                                $invoice->company->bank->phone ?
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'Телефоны',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->company->bank->phone,
                                            ]
                                        ],
                                    ] : [],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'ЭтоГруппа',
                                        'Тип' => 'Булево',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => 'false',
                                        ],
                                    ],
                                ],
                            ],
                        ];
                        $this->counter++;
                    }
                }
            }

            if (!$this->currency) {

                $this->currency = true;
                $out[] = [
                    '@name' => 'Объект',
                    '@attr' => [
                        'Нпп' => $this->counter,
                        'Тип' => 'СправочникСсылка.Валюты',
                        'ИмяПравила' => 'ВалютаВзаиморасчетов',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Ссылка',
                            '@attr' => [
                                'Нпп' => $this->counter,
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Код',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => '643',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Наименование',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'руб.',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'НаименованиеПолное',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'Российский рубль',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'СпособУстановкиКурса',
                                'Тип' => 'ПеречислениеСсылка.СпособыУстановкиКурсаВалюты',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'РучнойВвод',
                                ],
                            ],
                        ],
                    ],
                ];
                $this->counter++;
            }

            if (array_key_exists($invoice->contractor->ITN, $this->existsContractor)) {
                if (!in_array($invoice->contractor->PPC, $this->existsContractor[$invoice->contractor->ITN])) {
                    $this->existsContractor[$invoice->contractor->ITN][] = $invoice->contractor->PPC;
                    $this->addContractor($invoice->contractor);
                    $this->counter++;
                }
            } else {
                $this->existsContractor[$invoice->contractor->ITN][] = $invoice->contractor->PPC;
                $this->addContractor($invoice->contractor);
                $this->counter++;
            }
            $agreement = Agreement::getAgreementObjectByDocument($invoice, $this->counter, $type);
            if (!in_array($agreement['guid'], $this->existsAgreement)) {
                $this->out[] = $agreement['object'];
                $this->existsAgreement[] = $agreement['guid'];
                $this->counter++;
            }

            if (!in_array($invoice->company->mainCheckingAccountant->rs, $this->existBankInvoice)) {
                array_push($this->existBankInvoice, $invoice->company->mainCheckingAccountant->rs);
                if (empty($invoice->company->current_account_guid)) {
                    $invoice->company->current_account_guid = OneCExport::generateGUID();
                    $invoice->company->save(false);
                }

                $this->out[] = [
                    '@name' => 'Объект',
                    '@attr' => [
                        'Нпп' => $this->counter,
                        'Тип' => 'СправочникСсылка.БанковскиеСчета',
                        'ИмяПравила' => 'БанковскиеСчета',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Ссылка',
                            '@attr' => [
                                'Нпп' => $this->counter,
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => '{УникальныйИдентификатор}',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $invoice->company->current_account_guid,
                                        ],
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'НомерСчета',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $invoice->company->mainCheckingAccountant->rs,
                                        ]
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Владелец',
                                        'Тип' => 'СправочникСсылка.Организации',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Ссылка',
                                            '@attr' => [
                                                'Нпп' => '',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Свойство',
                                                    '@attr' => [
                                                        'Имя' => 'ИНН',
                                                        'Тип' => 'Строка',
                                                    ],
                                                    '@child' => [
                                                        [
                                                            '@name' => 'Значение',
                                                            '@value' => $invoice->company->inn,
                                                        ],
                                                    ],
                                                ],
                                                [
                                                    '@name' => 'Свойство',
                                                    '@attr' => [
                                                        'Имя' => 'КПП',
                                                        'Тип' => 'Строка',
                                                    ],
                                                    '@child' => [
                                                        [
                                                            '@name' => 'Значение',
                                                            '@value' => $invoice->company->kpp,
                                                        ],
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Банк',
                                'Тип' => 'СправочникСсылка.Банки',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'Код',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $invoice->company->bank ? $invoice->company->bank->bik : null,
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ВалютаДенежныхСредств',
                                'Тип' => 'СправочникСсылка.Валюты',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'Код',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => '643',
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Валютный',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ]
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ВидСчета',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'Расчетный',
                                ]
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ВсегдаУказыватьКПП',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ]
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'МесяцПрописью',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ]
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Наименование',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $invoice->company->bank ? $invoice->company->bank->name : null,
                                ]
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ПометкаУдаления',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ]
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'СуммаБезКопеек',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ]
                            ],
                        ],
                    ],
                ];
                $this->counter++;
            }

            $this->out[] = [
                '@name' => 'Объект',
                '@attr' => [
                    'Нпп' => $this->counter,
                    'Тип' => 'ДокументСсылка.СчетНаОплатуПоставщика',
                    'ИмяПравила' => 'СчетНаОплатуПоставщика',
                ],
                '@child' => [
                    [
                        '@name' => 'Ссылка',
                        '@attr' => [
                            'Нпп' => $this->counter,
                        ],
                        '@child' => [
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => '{УникальныйИдентификатор}',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $invoice->object_guid,
                                    ]
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Дата',
                                    'Тип' => 'Дата',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => date(OneCExport::XML_DATETIME_FORMAT, strtotime($invoice->document_date)),
                                    ]
                                ],
                            ],
                        ],
                    ],
                    [
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ДатаОплатитьДо',
                                'Тип' => 'Дата',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => date(OneCExport::XML_DATETIME_FORMAT, strtotime($invoice->payment_limit_date)),
                                ],
                            ],
                        ],
                    ],
                    [
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Статус',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => in_array($invoice->invoice_status_id, [
                                        InvoiceStatus::STATUS_CREATED,
                                        InvoiceStatus::STATUS_SEND,
                                        InvoiceStatus::STATUS_APPROVED,
                                        InvoiceStatus::STATUS_OVERDUE,
                                    ]) ? 'Не оплачен' : $invoice->invoiceStatus->name,
                                ],
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'Номер',
                            'Тип' => 'Строка',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => $invoice->getDocumentNumberOneC(),
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'Комментарий',
                            'Тип' => 'Строка',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => 'Выгрузка из сервиса КУБ',
                            ],
                        ],
                    ],
                    Agreement::getAgreementPropertyByDocument($invoice, $type),
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'ВалютаДокумента',
                            'Тип' => 'СправочникСсылка.Валюты',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Ссылка',
                                '@attr' => [
                                    'Нпп' => '',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'Код',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => '643',
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'ДатаВходящегоДокумента',
                            'Тип' => 'Дата',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => date(OneCExport::XML_DATETIME_FORMAT, strtotime($invoice->document_date)),
                            ]
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'Контрагент',
                            'Тип' => 'СправочникСсылка.Контрагенты',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Ссылка',
                                '@attr' => [
                                    'Нпп' => '',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => '{УникальныйИдентификатор}',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->contractor->object_guid,
                                            ],
                                        ],
                                    ],
                                    $invoice->contractor->face_type === Contractor::TYPE_PHYSICAL_PERSON ? [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'Наименование',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => str_replace('"', '', $invoice->contractor->name),
                                            ],
                                        ],
                                    ] : [],
                                    !empty($invoice->contractor->ITN) ? [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'ИНН',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->contractor->ITN,
                                            ],
                                        ],
                                    ] : [],
                                    !empty($invoice->contractor->PPC) ?
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'КПП',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $invoice->contractor->PPC,
                                                ],
                                            ],
                                        ] : [],
                                ],
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'КратностьВзаиморасчетов',
                            'Тип' => 'Число',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => 1,
                            ]
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'КурсВзаиморасчетов',
                            'Тип' => 'Число',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => 1,
                            ]
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'НомерВходящегоДокумента',
                            'Тип' => 'Строка',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => $invoice->getDocumentNumberOneC(),
                            ]
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'Организация',
                            'Тип' => 'СправочникСсылка.Организации',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Ссылка',
                                '@attr' => [
                                    'Нпп' => '',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'ИНН',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->company->inn,
                                            ],
                                        ],
                                    ],
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'КПП',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->company->kpp,
                                            ],
                                        ],
                                    ],
                                ]
                            ]
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'ПометкаУдаления',
                            'Тип' => 'Булево',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => 'false',
                            ]
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'Проведен',
                            'Тип' => 'Булево',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => 'false',
                            ]
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'СуммаВключаетНДС',
                            'Тип' => 'Булево',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => ($invoice->nds_view_type_id == 0 ? 'true' : ($invoice->nds_view_type_id == 1 ? 'false' : ''))
                            ]
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'СуммаДокумента',
                            'Тип' => 'Число',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => TextHelper::moneyFormatFromIntToFloat($invoice->total_amount_with_nds),
                            ]
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'ТипЦен',
                            'Тип' => 'СправочникСсылка.ТипыЦенНоменклатуры',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => TextHelper::moneyFormatFromIntToFloat($invoice->total_amount_with_nds),
                            ]
                        ],
                    ],
                    [
                        '@name' => 'ТабличнаяЧасть',
                        '@attr' => [
                            'Имя' => 'ВозвратнаяТара',
                        ],
                    ],
                    [
                        '@name' => 'ТабличнаяЧасть',
                        '@attr' => [
                            'Имя' => 'Товары',
                        ],
                        '@child' => $goodsOrders,
                    ],
                    [
                        '@name' => 'ТабличнаяЧасть',
                        '@attr' => [
                            'Имя' => 'Услуги',
                        ],
                        '@child' => $serviceOrders,
                    ],
                ],
            ];

            $this->exportModel->updateCounters(['objects_completed' => 1]);
        }
    }

    /**
     * @param $data
     */
    protected function exportOutDocuments($data)
    {
        foreach ($data as $invoice) {

            if (empty($invoice->object_guid)) {
                $invoice->object_guid = OneCExport::generateGUID();
                $invoice->save(false, ['object_guid']);
            } elseif (substr($invoice->object_guid, 0, 3) === 'UPD') {
                continue;
            }

            if (empty($invoice->contractor->object_guid)) {
                $invoice->contractor->object_guid = OneCExport::generateGUID();
                if (!$invoice->contractor->isNewRecord) {
                    $invoice->contractor->save(false, ['object_guid']);
                }
            }
            $this->counter++;

            $goodsOrders = $serviceOrders = [];

            $type = '';
            switch ($invoice->contractor->type) {
                case Contractor::TYPE_SELLER:
                    $type = 'СПоставщиком';
                    break;
                case Contractor::TYPE_CUSTOMER:
                    $type = 'СПокупателем';
                    break;
                case Contractor::TYPE_FOUNDER:
                    $type = 'Прочее';
                    break;
            }

            /* @var Order[] $invoiceOrders */
            if ($invoiceOrders = $invoice->orders) {
                if (is_array($invoiceOrders) && sizeof($invoiceOrders) > 0) {

                    foreach ($invoiceOrders as $invoiceOrder) {
                        if ($invoiceOrder->sale_tax > 0 && $invoiceOrder->saleTaxRate instanceof TaxRate && $invoiceOrder->saleTaxRate->rate > 0) {
                            $nds = [
                                '@name' => 'Значение',
                                '@value' => 'НДС' . round($invoiceOrder->saleTaxRate->rate * 100),
                            ];
                        } else {
                            $nds = [
                                '@name' => 'Значение',
                                '@value' => 'БезНДС',
                            ];
                        }
                        if (empty($invoiceOrder->product->object_guid)) {
                            $invoiceOrder->product->object_guid = OneCExport::generateGUID();
                            $invoiceOrder->product->save(false);
                        }
                        $_order = [
                            '@name' => 'Запись',
                            '@child' => [
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Количество',
                                        'Тип' => 'Число',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $invoiceOrder->quantity,
                                        ]
                                    ]
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Номенклатура',
                                        'Тип' => 'СправочникСсылка.Номенклатура',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Ссылка',
                                            '@attr' => [
                                                'Нпп' => '',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Свойство',
                                                    '@attr' => [
                                                        'Имя' => '{УникальныйИдентификатор}',
                                                        'Тип' => 'Строка',
                                                    ],
                                                    '@child' => [
                                                        [
                                                            '@name' => 'Значение',
                                                            '@value' => $invoiceOrder->product->object_guid,
                                                        ],
                                                    ],
                                                ],
                                                [
                                                    '@name' => 'Свойство',
                                                    '@attr' => [
                                                        'Имя' => 'ВидНоменклатуры',
                                                        'Тип' => 'СправочникСсылка.ВидыНоменклатуры',
                                                    ],
                                                    '@child' => [
                                                        [
                                                            '@name' => 'Ссылка',
                                                            '@attr' => [
                                                                'Нпп' => '',
                                                            ],
                                                            '@child' => [
                                                                [
                                                                    '@name' => 'Свойство',
                                                                    '@attr' => [
                                                                        'Имя' => 'Наименование',
                                                                        'Тип' => 'Строка',
                                                                    ],
                                                                    '@child' => [
                                                                        [
                                                                            '@name' => 'Значение',
                                                                            '@value' => $invoiceOrder->product->getType()->name,
                                                                        ],
                                                                    ],
                                                                ],
                                                            ],
                                                        ],
                                                    ],
                                                ],
                                                [
                                                    '@name' => 'Свойство',
                                                    '@attr' => [
                                                        'Имя' => 'Наименование',
                                                        'Тип' => 'Строка',
                                                    ],
                                                    '@child' => [
                                                        [
                                                            '@name' => 'Значение',
                                                            '@value' => str_replace(PHP_EOL, '', preg_replace('/\s{2,}/', ' ', mb_substr($invoiceOrder->product_title, 0, 255, 'UTF-8'))),
                                                        ],
                                                    ],
                                                ],
                                                $invoiceOrder->product->production_type ?
                                                    [
                                                        '@name' => 'Свойство',
                                                        '@attr' => [
                                                            'Имя' => 'ЕдиницаИзмерения',
                                                            'Тип' => 'СправочникСсылка.КлассификаторЕдиницИзмерения',
                                                        ],
                                                        '@child' => [
                                                            [
                                                                '@name' => 'Ссылка',
                                                                '@attr' => [
                                                                    'Нпп' => '',
                                                                ],
                                                                '@child' => [
                                                                    [
                                                                        '@name' => 'Свойство',
                                                                        '@attr' => [
                                                                            'Имя' => 'Код',
                                                                            'Тип' => 'Строка',
                                                                        ],
                                                                        '@child' => [
                                                                            [
                                                                                '@name' => 'Значение',
                                                                                '@value' => ProductUnit::findOne($invoiceOrder->product->product_unit_id)->code_okei,
                                                                            ],
                                                                        ],
                                                                    ],
                                                                ],
                                                            ],
                                                        ],
                                                    ] : [],
                                            ],
                                        ],
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Содержание',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $invoiceOrder->product_title,
                                        ],
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'СтавкаНДС',
                                        'Тип' => 'ПеречислениеСсылка.СтавкиНДС',
                                    ],
                                    '@child' => [
                                        $nds,
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Сумма',
                                        'Тип' => 'Число',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => TextHelper::moneyFormatFromIntToFloat($invoiceOrder->amount_sales_with_vat),
                                        ],
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'СуммаНДС',
                                        'Тип' => 'Число',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => TextHelper::moneyFormatFromIntToFloat($invoiceOrder->sale_tax),
                                        ],
                                    ]
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Цена',
                                        'Тип' => 'Число',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => TextHelper::moneyFormatFromIntToFloat($invoiceOrder->selling_price_with_vat),
                                        ],
                                    ]
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'ЦенаБезНДС',
                                        'Тип' => 'Число',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => TextHelper::moneyFormatFromIntToFloat($invoiceOrder->selling_price_no_vat),
                                        ],
                                    ]
                                ],
                            ],
                        ];

                        if ($invoiceOrder->product->production_type) {
                            $goodsOrders[] = $_order;
                        } else {
                            $serviceOrders[] = $_order;
                        }
                    }

                }
            }

            if (!$this->nomenclatureGroupService) {
                $this->nomenclatureGroupService = true;
                $this->out[] = [
                    '@name' => 'Объект',
                    '@attr' => [
                        'Нпп' => $this->counter,
                        'Тип' => 'СправочникСсылка.НоменклатурныеГруппы',
                        'ИмяПравила' => 'НоменклатурныеГруппы',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Ссылка',
                            '@attr' => [
                                'Нпп' => $this->counter,
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Наименование',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => 'Услуги',
                                        ],
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'ЭтоГруппа',
                                        'Тип' => 'Булево',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => 'false',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Родитель',
                                'Тип' => 'СправочникСсылка.НоменклатурныеГруппы',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                ],
                            ],
                        ],
                    ],
                ];
                $this->counter++;
            }

            if (!$this->nomenclatureGroupProduct) {
                $this->nomenclatureGroupProduct = true;
                $this->out[] = [
                    '@name' => 'Объект',
                    '@attr' => [
                        'Нпп' => $this->counter,
                        'Тип' => 'СправочникСсылка.НоменклатурныеГруппы',
                        'ИмяПравила' => 'НоменклатурныеГруппы',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Ссылка',
                            '@attr' => [
                                'Нпп' => $this->counter,
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Наименование',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => 'Товары',
                                        ],
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'ЭтоГруппа',
                                        'Тип' => 'Булево',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => 'false',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Родитель',
                                'Тип' => 'СправочникСсылка.НоменклатурныеГруппы',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                ],
                            ],
                        ],
                    ],
                ];
                $this->counter++;
            }

            if (!$this->nomenclatureGroupMain) {
                $this->nomenclatureGroupMain = true;
                $this->out[] = self::getMainNomenclatureGroup($this->counter);
                $this->counter++;
            }

            foreach ($invoiceOrders as $invoiceOrder) {

                if (!in_array($invoiceOrder->product->countryOrigin->code, $this->existCountry)) {
                    array_push($this->existCountry, $invoiceOrder->product->countryOrigin->code);

                    if ($invoiceOrder->product->countryOrigin->id !== CountryService::COUNTRY_RUSSIA && $invoiceOrder->product->countryOrigin->id !== CountryService::COUNTRY_WITHOUT) {
                        $this->out[] = Country::render($invoiceOrder->product->countryOrigin, $this->counter);
                        $this->counter++;
                    }
                }

                if (!in_array($invoiceOrder->product->id, $this->existNomenclature)) {
                    array_push($this->existNomenclature, $invoiceOrder->product->id);
                    if (empty($invoiceOrder->product->object_guid)) {
                        $invoiceOrder->product->object_guid = OneCExport::generateGUID();
                        $invoiceOrder->product->save(false);
                    }
                    $this->out[] = [
                        '@name' => 'Объект',
                        '@attr' => [
                            'Нпп' => $this->counter,
                            'Тип' => 'СправочникСсылка.Номенклатура',
                            'ИмяПравила' => 'Номенклатура',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Ссылка',
                                '@attr' => [
                                    'Нпп' => $this->counter,
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => '{УникальныйИдентификатор}',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoiceOrder->product->object_guid,
                                            ],
                                        ],
                                    ],
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'ВидНоменклатуры',
                                            'Тип' => 'СправочникСсылка.ВидыНоменклатуры',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Ссылка',
                                                '@attr' => [
                                                    'Нпп' => '',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Свойство',
                                                        '@attr' => [
                                                            'Имя' => 'Наименование',
                                                            'Тип' => 'Строка',
                                                        ],
                                                        '@child' => [
                                                            [
                                                                '@name' => 'Значение',
                                                                '@value' => $invoiceOrder->product->getType()->name,
                                                            ],
                                                        ],
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'Наименование',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => str_replace(PHP_EOL, '', preg_replace('/\s{2,}/', ' ', mb_substr($invoiceOrder->product_title, 0, 255, 'UTF-8'))),
                                            ],
                                        ],
                                    ],
                                    $invoiceOrder->product->production_type ?
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'ЕдиницаИзмерения',
                                                'Тип' => 'СправочникСсылка.КлассификаторЕдиницИзмерения',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Ссылка',
                                                    '@attr' => [
                                                        'Нпп' => '',
                                                    ],
                                                    '@child' => [
                                                        [
                                                            '@name' => 'Свойство',
                                                            '@attr' => [
                                                                'Имя' => 'Код',
                                                                'Тип' => 'Строка',
                                                            ],
                                                            '@child' => [
                                                                [
                                                                    '@name' => 'Значение',
                                                                    '@value' => ProductUnit::findOne($invoiceOrder->product->product_unit_id)->code_okei,
                                                                ],
                                                            ],
                                                        ],
                                                    ],
                                                ],
                                            ],
                                        ] : [],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Услуга',
                                    'Тип' => 'Булево',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $invoiceOrder->product->production_type ? 'false' : 'true',
                                    ],
                                ],
                            ],
                            $invoiceOrder->product->production_type && $invoiceOrder->product->country_origin_id !== CountryService::COUNTRY_WITHOUT ?
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'СтранаПроисхождения',
                                        'Тип' => 'СправочникСсылка.СтраныМира',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Ссылка',
                                            '@attr' => [
                                                'Нпп' => '',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Свойство',
                                                    '@attr' => [
                                                        'Имя' => 'Код',
                                                        'Тип' => 'Строка',
                                                    ],
                                                    '@child' => [
                                                        [
                                                            '@name' => 'Значение',
                                                            '@value' => $invoiceOrder->product->countryOrigin->code,
                                                        ],
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ] : [],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'НоменклатурнаяГруппа',
                                    'Тип' => 'СправочникСсылка.НоменклатурныеГруппы',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Ссылка',
                                        '@attr' => [
                                            'Нпп' => '',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'Наименование',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        //'@value' => $invoiceOrder->product->production_type ? 'Товары' : 'Услуги',
                                                        '@value' => 'Основная номенклатурная группа',
                                                    ],
                                                ],
                                            ],
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'ЭтоГруппа',
                                                    'Тип' => 'Булево',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => 'false',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Код',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $invoiceOrder->product->code,
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ЭтоГруппа',
                                    'Тип' => 'Булево',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => 'false',
                                    ],
                                ],
                            ],
                        ],
                    ];
                    $this->counter++;
                }
            }

            if (!in_array($invoice->company->id, $this->existCompany)) {
                array_push($this->existCompany, $invoice->company->id);
                if ($invoice->company->company_type_id == CompanyType::TYPE_IP) {
                    $this->out[] = [
                        '@name' => 'Объект',
                        '@attr' => [
                            'Нпп' => $this->counter,
                            'Тип' => 'СправочникСсылка.ФизическиеЛица',
                            'ИмяПравила' => 'ФизическиеЛица',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Ссылка',
                                '@attr' => [
                                    'Нпп' => $this->counter,
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'ИНН',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->company->inn,
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ФИО',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $invoice->company->getIpFio(),
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Наименование',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $invoice->company->getIpFio(),
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Код',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ЭтоГруппа',
                                    'Тип' => 'Булево',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => 'false',
                                    ],
                                ],
                            ],
                        ],
                    ];
                    $this->counter++;
                }

                $this->out[] = [
                    '@name' => 'Объект',
                    '@attr' => [
                        'Нпп' => $this->counter,
                        'Тип' => 'СправочникСсылка.Организации',
                        'ИмяПравила' => 'Организации',
                        'НеЗамещать' => 'true',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Ссылка',
                            '@attr' => [
                                'Нпп' => $this->counter,
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'ИНН',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $invoice->company->inn,
                                        ],
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'КПП',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $invoice->company->kpp,
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ЮридическоеФизическоеЛицо',
                                'Тип' => 'ПеречислениеСсылка.ЮридическоеФизическоеЛицо',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $invoice->company->company_type_id == CompanyType::TYPE_IP
                                        ? 'ФизическоеЛицо'
                                        : 'ЮридическоеЛицо',
                                ],
                            ],
                        ],
                        $invoice->company->company_type_id == CompanyType::TYPE_IP ?
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ИндивидуальныйПредприниматель',
                                    'Тип' => 'СправочникСсылка.ФизическиеЛица',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Ссылка',
                                        '@attr' => [
                                            'Нпп' => '',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'ИНН',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => $invoice->company->inn,
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ] : [],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Наименование',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $invoice->company->name_full,
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'НаименованиеПолное',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $invoice->company->name_full,
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'НаименованиеСокращенное',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $invoice->company->name_short,
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ОГРН',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $invoice->company->ogrn,
                                ],
                            ],
                        ],
                    ],
                ];
                $this->counter++;


                if ($invoice->company->bank instanceof BikDictionary) {
                    if (!in_array($invoice->company->bank->bik, $this->existBank)) {
                        array_push($this->existBank, $invoice->company->bank->bik);
                        $this->out[] = [
                            '@name' => 'Объект',
                            '@attr' => [
                                'Нпп' => $this->counter,
                                'Тип' => 'СправочникСсылка.Банки',
                                'ИмяПравила' => 'Банки',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => $this->counter,
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'Код',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $invoice->company->bank->bik,
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                                $invoice->company->bank->address ?
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'Адрес',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->company->bank->address,
                                            ]
                                        ],
                                    ] : [],
                                $invoice->company->bank->city ?
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'Город',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->company->bank->city,
                                            ]
                                        ],
                                    ] : [],
                                $invoice->company->bank->ks ?
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'КоррСчет',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->company->bank->ks,
                                            ]
                                        ],
                                    ] : [],
                                $invoice->company->bank->name ?
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'Наименование',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->company->bank->name,
                                            ]
                                        ],
                                    ] : [],
                                $invoice->company->bank->phone ?
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'Телефоны',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->company->bank->phone,
                                            ]
                                        ],
                                    ] : [],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'ЭтоГруппа',
                                        'Тип' => 'Булево',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => 'false',
                                        ],
                                    ],
                                ],
                            ],
                        ];
                        $this->counter++;
                    }
                }
            }

            if (!$this->currency) {

                $this->currency = true;
                $out[] = [
                    '@name' => 'Объект',
                    '@attr' => [
                        'Нпп' => $this->counter,
                        'Тип' => 'СправочникСсылка.Валюты',
                        'ИмяПравила' => 'ВалютаВзаиморасчетов',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Ссылка',
                            '@attr' => [
                                'Нпп' => $this->counter,
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Код',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => '643',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Наименование',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'руб.',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'НаименованиеПолное',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'Российский рубль',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'СпособУстановкиКурса',
                                'Тип' => 'ПеречислениеСсылка.СпособыУстановкиКурсаВалюты',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'РучнойВвод',
                                ],
                            ],
                        ],
                    ],
                ];
                $this->counter++;
            }

            if (array_key_exists($invoice->contractor->ITN, $this->existsContractor)) {
                if (!in_array($invoice->contractor->PPC, $this->existsContractor[$invoice->contractor->ITN])) {
                    $this->existsContractor[$invoice->contractor->ITN][] = $invoice->contractor->PPC;
                    $this->addContractor($invoice->contractor);
                    $this->counter++;
                }
            } else {
                $this->existsContractor[$invoice->contractor->ITN][] = $invoice->contractor->PPC;
                $this->addContractor($invoice->contractor);
                $this->counter++;
            }
            $agreement = Agreement::getAgreementObjectByDocument($invoice, $this->counter, $type);
            if (!in_array($agreement['guid'], $this->existsAgreement)) {
                $this->out[] = $agreement['object'];
                $this->existsAgreement[] = $agreement['guid'];
                $this->counter++;
            }

            if (!in_array($invoice->company->mainCheckingAccountant->rs, $this->existBankInvoice)) {
                array_push($this->existBankInvoice, $invoice->company->mainCheckingAccountant->rs);
                if (empty($invoice->company->current_account_guid)) {
                    $invoice->company->current_account_guid = OneCExport::generateGUID();
                    $invoice->company->save(false);
                }
                $this->out[] = [
                    '@name' => 'Объект',
                    '@attr' => [
                        'Нпп' => $this->counter,
                        'Тип' => 'СправочникСсылка.БанковскиеСчета',
                        'ИмяПравила' => 'БанковскиеСчета',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Ссылка',
                            '@attr' => [
                                'Нпп' => $this->counter,
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => '{УникальныйИдентификатор}',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $invoice->company->current_account_guid,
                                        ],
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'НомерСчета',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $invoice->company->mainCheckingAccountant->rs,
                                        ]
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Владелец',
                                        'Тип' => 'СправочникСсылка.Организации',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Ссылка',
                                            '@attr' => [
                                                'Нпп' => '',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Свойство',
                                                    '@attr' => [
                                                        'Имя' => 'ИНН',
                                                        'Тип' => 'Строка',
                                                    ],
                                                    '@child' => [
                                                        [
                                                            '@name' => 'Значение',
                                                            '@value' => $invoice->company->inn,
                                                        ],
                                                    ],
                                                ],
                                                [
                                                    '@name' => 'Свойство',
                                                    '@attr' => [
                                                        'Имя' => 'КПП',
                                                        'Тип' => 'Строка',
                                                    ],
                                                    '@child' => [
                                                        [
                                                            '@name' => 'Значение',
                                                            '@value' => $invoice->company->kpp,
                                                        ],
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Банк',
                                'Тип' => 'СправочникСсылка.Банки',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'Код',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $invoice->company->bank ? $invoice->company->bank->bik : null,
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ВалютаДенежныхСредств',
                                'Тип' => 'СправочникСсылка.Валюты',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'Код',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => '643',
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Валютный',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ]
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ВидСчета',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'Расчетный',
                                ]
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ВсегдаУказыватьКПП',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ]
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'МесяцПрописью',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ]
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Наименование',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $invoice->company->bank ? $invoice->company->bank->name : null,
                                ]
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ПометкаУдаления',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ]
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'СуммаБезКопеек',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ]
                            ],
                        ],
                    ],
                ];
                $this->counter++;
            }

            if (empty($invoice->company->current_account_guid)) {
                $invoice->company->current_account_guid = OneCExport::generateGUID();
                $invoice->company->save(false);
            }
            $this->out[] = [
                '@name' => 'Объект',
                '@attr' => [
                    'Нпп' => $this->counter,
                    'Тип' => 'ДокументСсылка.СчетНаОплатуПокупателю',
                    'ИмяПравила' => 'СчетНаОплатуПокупателю',
                ],
                '@child' => [
                    [
                        '@name' => 'Ссылка',
                        '@attr' => [
                            'Нпп' => $this->counter,
                        ],
                        '@child' => [
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => '{УникальныйИдентификатор}',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $invoice->object_guid,
                                    ]
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Дата',
                                    'Тип' => 'Дата',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => date(OneCExport::XML_DATETIME_FORMAT, strtotime($invoice->document_date)),
                                    ]
                                ],
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'ДатаОплатитьДо',
                            'Тип' => 'Дата',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => date(OneCExport::XML_DATETIME_FORMAT, strtotime($invoice->payment_limit_date)),
                            ]
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'Статус',
                            'Тип' => 'Строка',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => in_array($invoice->invoice_status_id, [
                                    InvoiceStatus::STATUS_CREATED,
                                    InvoiceStatus::STATUS_SEND,
                                    InvoiceStatus::STATUS_APPROVED,
                                    InvoiceStatus::STATUS_OVERDUE,
                                ]) ? 'Не оплачен' : $invoice->invoiceStatus->name,
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'Номер',
                            'Тип' => 'Строка',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => $invoice->getDocumentNumberOneC(),
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'Комментарий',
                            'Тип' => 'Строка',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => 'Выгрузка из сервиса КУБ',
                            ],
                        ],
                    ],
                    Agreement::getAgreementPropertyByDocument($invoice, $type),
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'ВалютаДокумента',
                            'Тип' => 'СправочникСсылка.Валюты',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Ссылка',
                                '@attr' => [
                                    'Нпп' => '',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'Код',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => '643',
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'ОрганизацияПолучатель',
                            'Тип' => 'СправочникСсылка.Организации',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Ссылка',
                                '@attr' => [
                                    'Нпп' => '',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'ИНН',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->company->inn,
                                            ],
                                        ],
                                    ],
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'КПП',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->company->kpp,
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'ДокументБезНДС',
                            'Тип' => 'Булево',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => $invoice->hasNds ? 'false' : 'true',
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'Контрагент',
                            'Тип' => 'СправочникСсылка.Контрагенты',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Ссылка',
                                '@attr' => [
                                    'Нпп' => '',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => '{УникальныйИдентификатор}',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->contractor->object_guid,
                                            ],
                                        ],
                                    ],
                                    $invoice->contractor->face_type === Contractor::TYPE_PHYSICAL_PERSON ? [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'Наименование',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => str_replace('"', '', $invoice->contractor->name),
                                            ],
                                        ],
                                    ] : [],
                                    !empty($invoice->contractor->ITN) ? [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'ИНН',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->contractor->ITN,
                                            ],
                                        ],
                                    ] : [],
                                    !empty($invoice->contractor->PPC) ?
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'КПП',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $invoice->contractor->PPC,
                                                ],
                                            ],
                                        ] : [],
                                ],
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'КратностьВзаиморасчетов',
                            'Тип' => 'Число',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => 1,
                            ]
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'КурсВзаиморасчетов',
                            'Тип' => 'Число',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => 1,
                            ]
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'Организация',
                            'Тип' => 'СправочникСсылка.Организации',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Ссылка',
                                '@attr' => [
                                    'Нпп' => '',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'ИНН',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->company->inn,
                                            ],
                                        ],
                                    ],
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'КПП',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->company->kpp,
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'ПометкаУдаления',
                            'Тип' => 'Булево',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => 'false',
                            ]
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'Проведен',
                            'Тип' => 'Булево',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => 'false',
                            ]
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'СтруктурнаяЕдиница',
                            'Тип' => 'СправочникСсылка.БанковскиеСчета',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Ссылка',
                                '@attr' => [
                                    'Нпп' => '',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'НомерСчета',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $invoice->company_rs,
                                            ]
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'СуммаВключаетНДС',
                            'Тип' => 'Булево',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => ($invoice->nds_view_type_id == 0 ? 'true' : ($invoice->nds_view_type_id == 1 ? 'false' : ''))
                            ]
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'СуммаДокумента',
                            'Тип' => 'Число',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => TextHelper::moneyFormatFromIntToFloat($invoice->total_amount_with_nds),
                            ]
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'УдалитьУчитыватьНДС',
                            'Тип' => 'Булево',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => 'false',
                            ]
                        ],
                    ],
                    [
                        '@name' => 'ТабличнаяЧасть',
                        '@attr' => [
                            'Имя' => 'ВозвратнаяТара',
                        ],
                    ],
                    [
                        '@name' => 'ТабличнаяЧасть',
                        '@attr' => [
                            'Имя' => 'Товары',
                        ],
                        '@child' => $goodsOrders,
                    ],
                    [
                        '@name' => 'ТабличнаяЧасть',
                        '@attr' => [
                            'Имя' => 'Услуги',
                        ],
                        '@child' => $serviceOrders,
                    ],
                ],
            ];

            $this->exportModel->updateCounters(['objects_completed' => 1]);
        }
    }
}