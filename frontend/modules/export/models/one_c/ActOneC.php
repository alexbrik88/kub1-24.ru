<?php

namespace frontend\modules\export\models\one_c;

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\address\Country;
use common\models\Agreement;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\Act;
use common\models\product\Product;
use common\models\TaxRate;
use frontend\models\Documents;

/**
 * Class Act
 *
 * @package frontend\modules\export\models\one_c
 */
class ActOneC extends OneCObject implements IOneCExport
{
    /**
     * @var array
     */
    protected $out = [];

    /**
     * @var int
     */
    protected $counter = 1;

    /**
     * @var null
     */
    protected $exportModel = null;

    /**
     * @var null
     */
    protected $individualGuid = null;

    /**
     * @var array
     */
    protected $existsCountry = false;

    /**
     * @inheritdoc
     */
    public static function exportRules()
    {
        return [
            'ПоступлениеТоваровУслуг' => [
                'Код' => 'ПоступлениеТоваровУслуг',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'true',
                'Источник' => 'ДокументСсылка.ПоступлениеТоваровУслуг',
                'Приемник' => 'ДокументСсылка.ПоступлениеТоваровУслуг',
            ],
            'РеализацияТоваровУслуг' => [
                'Код' => 'РеализацияТоваровУслуг',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'true',
                'Источник' => 'ДокументСсылка.РеализацияТоваровУслуг',
                'Приемник' => 'ДокументСсылка.РеализацияТоваровУслуг',
            ],
            'Номенклатура' => [
                'Код' => 'Номенклатура',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.Номенклатура',
                'Приемник' => 'СправочникСсылка.Номенклатура',
            ],
            'СтавкиНДС' => [
                'Код' => 'СтавкиНДС',
                'Источник' => 'ПеречислениеСсылка.СтавкиНДС',
                'Приемник' => 'ПеречислениеСсылка.СтавкиНДС',
            ],
            'ВидНоменклатуры' => [
                'Код' => 'ВидНоменклатуры',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.ВидыНоменклатуры',
                'Приемник' => 'СправочникСсылка.ВидыНоменклатуры',
            ],
            'ЕдиницаИзмерения' => [
                'Код' => 'ЕдиницаИзмерения',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.КлассификаторЕдиницИзмерения',
                'Приемник' => 'СправочникСсылка.КлассификаторЕдиницИзмерения',
            ],
            'Организации' => [
                'Код' => 'Организации',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.Организации',
                'Приемник' => 'СправочникСсылка.Организации',
                'НеЗамещать' => 'true',
            ],
            'ЮридическоеФизическоеЛицо' => [
                'Код' => 'ЮридическоеФизическоеЛицо',
                'Источник' => 'ПеречислениеСсылка.ЮридическоеФизическоеЛицо',
                'Приемник' => 'ПеречислениеСсылка.ЮридическоеФизическоеЛицо',
            ],
            'ФизическиеЛица' => [
                'Код' => 'ФизическиеЛица',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.ФизическиеЛица',
                'Приемник' => 'СправочникСсылка.ФизическиеЛица',
            ],
            'Контрагенты' => [
                'Код' => 'Контрагенты',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'Источник' => 'СправочникСсылка.Контрагенты',
                'Приемник' => 'СправочникСсылка.Контрагенты',
                'НеЗамещать' => 'true',
            ],
            'СтраныМира' => [
                'Код' => 'СтраныМира',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.СтраныМира',
                'Приемник' => 'СправочникСсылка.СтраныМира',
            ],
            'ДоговорыКонтрагентов' => [
                'Код' => 'ДоговорыКонтрагентов',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.ДоговорыКонтрагентов',
                'Приемник' => 'СправочникСсылка.ДоговорыКонтрагентов',
            ],
            'ВалютаВзаиморасчетов' => [
                'Код' => 'ВалютаВзаиморасчетов',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.Валюты',
                'Приемник' => 'СправочникСсылка.Валюты',
            ],
            'СпособУстановкиКурса' => [
                'Код' => 'СпособыУстановкиКурсаВалюты',
                'Источник' => 'ПеречислениеСсылка.СпособыУстановкиКурсаВалюты',
                'Приемник' => 'ПеречислениеСсылка.СпособыУстановкиКурсаВалюты',
            ],
            'ВидДоговора' => [
                'Код' => 'ВидыДоговоровКонтрагентов',
                'Источник' => 'ПеречислениеСсылка.ВидыДоговоровКонтрагентов',
                'Приемник' => 'ПеречислениеСсылка.ВидыДоговоровКонтрагентов',
            ],
            'НоменклатурныеГруппы' => [
                'Код' => 'НоменклатурныеГруппы',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.НоменклатурныеГруппы',
                'Приемник' => 'СправочникСсылка.НоменклатурныеГруппы',
            ],
            'Склады' => [
                'Код' => 'Склады',
                'ГенерироватьНовыйНомерИлиКодЕслиНеУказан' => 'true',
                'СинхронизироватьПоИдентификатору' => 'false',
                'Источник' => 'СправочникСсылка.Склады',
                'Приемник' => 'СправочникСсылка.Склады',
            ],
            'ТипыСкладов' => [
                'Код' => 'ТипыСкладов',
                'Источник' => 'ПеречислениеСсылка.ТипыСкладов',
                'Приемник' => 'ПеречислениеСсылка.ТипыСкладов',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function export($data = [], &$counter = null, &$exportModel = null)
    {
        $this->counter = $counter + 1;
        $this->exportModel = $exportModel;
        $this->individualGuid = OneCExport::generateGUID();
        if ($this->individualCurrency == null) {
            $this->individualCurrency = OneCExport::generateGUID();
        }

        if (isset($data[Documents::IO_TYPE_IN])) {
            $this->exportInDocuments($data[Documents::IO_TYPE_IN]);
        }

        if (isset($data[Documents::IO_TYPE_OUT])) {
            $this->exportOutDocuments($data[Documents::IO_TYPE_OUT]);
        }

        $counter = $this->counter;

        return $this->out;
    }

    /**
     * @param $data
     * @return array
     */
    protected function exportInDocuments($data)
    {
        /* @var $documentAct Act */
        foreach ($data as $documentAct) {
            if (empty($documentAct->object_guid)) {
                $documentAct->object_guid = OneCExport::generateGUID();
                $documentAct->save(false, ['object_guid']);
            }
            if (empty($documentAct->invoice->contractor->object_guid)) {
                $documentAct->invoice->contractor->object_guid = OneCExport::generateGUID();
                if (!$documentAct->invoice->contractor->isNewRecord) {
                    $documentAct->invoice->contractor->save(false, ['object_guid']);
                }
            }

            $invoiceOrders = [];
            $type = '';
            switch ($documentAct->invoice->contractor->type) {
                case Contractor::TYPE_SELLER:
                    $type = 'СПоставщиком';
                    break;
                case Contractor::TYPE_CUSTOMER:
                    $type = 'СПокупателем';
                    break;
                case Contractor::TYPE_FOUNDER:
                    $type = 'Прочее';
                    break;
            }

            if (is_array($documentAct->orders) && sizeof($documentAct->orders) > 0) {
                foreach ($documentAct->orderActs as $orderAct) {
                    $order = $orderAct->order;
                    if ($order->purchase_tax > 0 && $order->purchaseTaxRate instanceof TaxRate && $order->purchaseTaxRate->rate > 0) {
                        $nds = [
                            '@name' => 'Значение',
                            '@value' => 'НДС' . round($order->purchaseTaxRate->rate * 100),
                        ];
                    } else {
                        $nds = [
                            '@name' => 'Значение',
                            '@value' => 'БезНДС',
                        ];
                    }
                    if (empty($order->product->object_guid)) {
                        $order->product->object_guid = OneCExport::generateGUID();
                        $order->product->save(false);
                    }

                    $invoiceOrders[] = [
                        '@name' => 'Запись',
                        '@child' => [
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Количество',
                                    'Тип' => 'Число',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $orderAct->quantity,
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Номенклатура',
                                    'Тип' => 'СправочникСсылка.Номенклатура',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Ссылка',
                                        '@attr' => [
                                            'Нпп' => '',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => '{УникальныйИдентификатор}',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => $order->product->object_guid,
                                                    ],
                                                ],
                                            ],
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'ВидНоменклатуры',
                                                    'Тип' => 'СправочникСсылка.ВидыНоменклатуры',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Ссылка',
                                                        '@attr' => [
                                                            'Нпп' => '',
                                                        ],
                                                        '@child' => [
                                                            [
                                                                '@name' => 'Свойство',
                                                                '@attr' => [
                                                                    'Имя' => 'Наименование',
                                                                    'Тип' => 'Строка',
                                                                ],
                                                                '@child' => [
                                                                    [
                                                                        '@name' => 'Значение',
                                                                        '@value' => $order->product->getType()->name,
                                                                    ],
                                                                ],
                                                            ],
                                                        ],
                                                    ],
                                                ],
                                            ],
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'Наименование',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => str_replace(['\r\n', '\r', '\n'], '', preg_replace('/\s{2,}/', ' ', mb_substr($order->product_title, 0, 100, 'UTF-8'))),
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ОтражениеВУСН',
                                    'Тип' => 'ПеречислениеСсылка.ОтражениеВУСН',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => 'Принимаются',
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Содержание',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $order->product_title,
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'СпособУчетаНДС',
                                    'Тип' => 'ПеречислениеСсылка.СпособыУчетаНДС',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => 'ПринимаетсяКВычету',
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'СтавкаНДС',
                                    'Тип' => 'ПеречислениеСсылка.СтавкиНДС',
                                ],
                                '@child' => [
                                    $nds,
                                ],
                            ],
                            $documentAct->invoice->company->company_type_id !== CompanyType::TYPE_IP ?
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'СчетЗатратНУ',
                                        'Тип' => 'ПланСчетовСсылка.Хозрасчетный',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Ссылка',
                                            '@attr' => [
                                                'Нпп' => '',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Свойство',
                                                    '@attr' => [
                                                        'Имя' => 'Код',
                                                        'Тип' => 'Строка',
                                                    ],
                                                    '@child' => [
                                                        [
                                                            '@name' => 'Значение',
                                                            '@value' => '26',
                                                        ],
                                                    ],
                                                ],
                                            ],
                                        ]
                                    ],
                                ] : [],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'СчетЗатрат',
                                    'Тип' => 'ПланСчетовСсылка.Хозрасчетный',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Ссылка',
                                        '@attr' => [
                                            'Нпп' => '',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'Код',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => '26',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'СчетУчетаНДС',
                                    'Тип' => 'ПланСчетовСсылка.Хозрасчетный',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Ссылка',
                                        '@attr' => [
                                            'Нпп' => '',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'Код',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => '19.04',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Сумма',
                                    'Тип' => 'Число',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => TextHelper::moneyFormatFromIntToFloat($order->amount_purchase_with_vat),
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'СуммаНДС',
                                    'Тип' => 'Число',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => TextHelper::moneyFormatFromIntToFloat($order->purchase_tax),
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Цена',
                                    'Тип' => 'Число',
                                ],
                                '@child' => [
                                    [

                                        '@name' => 'Значение',
                                        '@value' => TextHelper::moneyFormatFromIntToFloat($order->purchase_price_with_vat),
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ЦенаБезНДС',
                                    'Тип' => 'Число',
                                ],
                                '@child' => [
                                    [

                                        '@name' => 'Значение',
                                        '@value' => TextHelper::moneyFormatFromIntToFloat($order->purchase_price_no_vat),
                                    ],
                                ],
                            ],
                        ],
                    ];
                }
            }

            $this->addNomenclatureCompanyAndContractor($documentAct, $type);

            if (!in_array($documentAct->id, $this->receiptGoodsServiceAct)) {
                array_push($this->receiptGoodsServiceAct, $documentAct->id);

                $this->out[] = [
                    '@name' => 'Объект',
                    '@attr' => [
                        'Нпп' => $this->counter,
                        'Тип' => 'ДокументСсылка.ПоступлениеТоваровУслуг',
                        'ИмяПравила' => 'ПоступлениеТоваровУслуг',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Ссылка',
                            '@attr' => [
                                'Нпп' => $this->counter,
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => '{УникальныйИдентификатор}',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $documentAct->object_guid,
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Дата',
                                'Тип' => 'Дата',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => date(OneCExport::XML_DATETIME_FORMAT, strtotime($documentAct->document_date)),
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ВалютаДокумента',
                                'Тип' => 'СправочникСсылка.Валюты',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'Код',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => '643',
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Номер',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $documentAct->getDocumentNumberOneC(),
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Комментарий',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'Выгрузка из сервиса КУБ',
                                ],
                            ],
                        ],
                        Agreement::getAgreementPropertyByDocument($documentAct, $type),
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ВидОперации',
                                'Тип' => 'ПеречислениеСсылка.ВидыОперацийПоступлениеТоваровУслуг',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'Услуги',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'СчетУчетаРасчетовСКонтрагентом',
                                'Тип' => 'ПланСчетовСсылка.Хозрасчетный',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'Код',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => '60.01',
                                                ],
                                            ],
                                        ],
                                    ],
                                ]
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'СчетУчетаРасчетовПоАвансам',
                                'Тип' => 'ПланСчетовСсылка.Хозрасчетный',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'Код',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => '60.02',
                                                ],
                                            ],
                                        ],
                                    ],
                                ]
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ДатаВходящегоДокумента',
                                'Тип' => 'Дата',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => date(OneCExport::XML_DATETIME_FORMAT, strtotime($documentAct->document_date)),
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Контрагент',
                                'Тип' => 'СправочникСсылка.Контрагенты',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => '{УникальныйИдентификатор}',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $documentAct->invoice->contractor->object_guid,
                                                ],
                                            ],
                                        ],
                                        $documentAct->invoice->contractor->face_type === Contractor::TYPE_PHYSICAL_PERSON ? [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'Наименование',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => str_replace('"', '', $documentAct->invoice->contractor->name),
                                                ],
                                            ],
                                        ] : [],
                                        !empty($documentAct->invoice->contractor->ITN) ? [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'ИНН',
                                                'Тип' => 'Строка'
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $documentAct->invoice->contractor->ITN,
                                                ],
                                            ],
                                        ] : [],
                                        !empty($documentAct->invoice->contractor->PPC) ?
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'КПП',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => $documentAct->invoice->contractor->PPC,
                                                    ],
                                                ],
                                            ] : [],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'КратностьВзаиморасчетов',
                                'Тип' => 'Число',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 1,
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'КурсВзаиморасчетов',
                                'Тип' => 'Число',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 1,
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'НДСВключенВСтоимость',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'true',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'НДСНеВыделять',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'НомерВходящегоДокумента',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $documentAct->getDocumentNumberOneC() ? $documentAct->getDocumentNumberOneC() : $documentAct->invoice->getDocumentNumberOneC(),
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Организация',
                                'Тип' => 'СправочникСсылка.Организации',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'ИНН',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $documentAct->invoice->company->inn,
                                                ],
                                            ],
                                        ],
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'КПП',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $documentAct->invoice->company->kpp,
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ПометкаУдаления',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Проведен',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'РучнаяКорректировка',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Проведен',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'РучнаяКорректировка',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'СпособЗачетаАвансов',
                                'Тип' => 'ПеречислениеСсылка.СпособыЗачетаАвансов',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'Автоматически'
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'СуммаВключаетНДС',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => ($documentAct->invoice->nds_view_type_id == 0 ? 'true' : ($documentAct->invoice->nds_view_type_id == 1 ? 'false' : ''))
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'СуммаДокумента',
                                'Тип' => 'Число',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => TextHelper::moneyFormatFromIntToFloat($documentAct->invoice->total_amount_with_nds),
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'УдалитьНДСПредъявленКВычету',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'УдалитьПредъявленСчетФактура',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'УдалитьУчитыватьНДС',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'ТабличнаяЧасть',
                            '@attr' => [
                                'Имя' => 'АгентскиеУслуги',
                            ],
                        ],
                        [
                            '@name' => 'ТабличнаяЧасть',
                            '@attr' => [
                                'Имя' => 'ВозвратнаяТара',
                            ],
                        ],
                        [
                            '@name' => 'ТабличнаяЧасть',
                            '@attr' => [
                                'Имя' => 'ЗачетАвансов',
                            ],
                        ],
                        [
                            '@name' => 'ТабличнаяЧасть',
                            '@attr' => [
                                'Имя' => 'Оборудование',
                            ],
                        ],
                        [
                            '@name' => 'ТабличнаяЧасть',
                            '@attr' => [
                                'Имя' => 'ОбъектыСтроительства',
                            ],
                        ],
                        [
                            '@name' => 'ТабличнаяЧасть',
                            '@attr' => [
                                'Имя' => 'Товары',
                            ],
                        ],
                        [
                            '@name' => 'ТабличнаяЧасть',
                            '@attr' => [
                                'Имя' => 'Услуги',
                            ],
                            '@child' => $invoiceOrders,
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'УидСчета',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $documentAct->invoice->object_guid,
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Счет',
                                'Тип' => 'ДокументСсылка.СчетНаОплатуПоставщика',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'Номер',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $documentAct->invoice->getDocumentNumberOneC(),
                                                ],
                                            ],
                                        ],
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'Дата',
                                                'Тип' => 'Дата'
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => date(OneCExport::XML_DATETIME_FORMAT, strtotime($documentAct->invoice->document_date)),
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],

                        ($documentAct->invoice->has_goods) ?
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Склад',
                                    'Тип' => 'СправочникСсылка.Склады',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Ссылка',
                                        '@attr' => [
                                            'Нпп' => '',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'Наименование',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => OneCExport::getInvoiceStorageName($documentAct->invoice),
                                                    ],
                                                ],
                                            ],
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'ТипСклада',
                                                    'Тип' => 'ПеречислениеСсылка.ТипыСкладов',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => 'ОптовыйСклад',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ] : []

                    ],
                ];
                $this->counter++;
                $this->exportModel->updateCounters(['objects_completed' => 1]);
            }
        }
    }

    /**
     * @param $data
     */
    protected function exportOutDocuments($data)
    {
        /* @var $documentAct Act */
        foreach ($data as $documentAct) {
            if (empty($documentAct->object_guid)) {
                $documentAct->object_guid = OneCExport::generateGUID();
                $documentAct->save(false, ['object_guid']);
            }
            if (empty($documentAct->invoice->contractor->object_guid)) {
                $documentAct->invoice->contractor->object_guid = OneCExport::generateGUID();
                if (!$documentAct->invoice->contractor->isNewRecord) {
                    $documentAct->invoice->contractor->save(false, ['object_guid']);
                }
            }
            $this->counter++;

            $invoiceOrders = [];
            $type = '';
            switch ($documentAct->invoice->contractor->type) {
                case Contractor::TYPE_SELLER:
                    $type = 'СПоставщиком';
                    break;
                case Contractor::TYPE_CUSTOMER:
                    $type = 'СПокупателем';
                    break;
                case Contractor::TYPE_FOUNDER:
                    $type = 'Прочее';
                    break;
            }

            if (is_array($documentAct->orders) && sizeof($documentAct->orders) > 0) {
                foreach ($documentAct->orderActs as $orderAct) {
                    $order = $orderAct->order;
                    if (!$order->amount_purchase_with_vat) {
                        $order->amount_purchase_with_vat = $order->amount_sales_with_vat;
                        $order->purchase_tax = $order->sale_tax;
                        $order->purchase_price_with_vat = $order->selling_price_with_vat;
                    }
                    if ($order->sale_tax > 0 && $order->saleTaxRate instanceof TaxRate && $order->saleTaxRate->rate > 0) {
                        $nds = [
                            '@name' => 'Значение',
                            '@value' => 'НДС' . round($order->saleTaxRate->rate * 100),
                        ];
                    } else {
                        $nds = [
                            '@name' => 'Значение',
                            '@value' => 'БезНДС',
                        ];
                    }
                    if (empty($order->product->object_guid)) {
                        $order->product->object_guid = OneCExport::generateGUID();
                        $order->product->save(false);
                    }

                    $invoiceOrders[] = [
                        '@name' => 'Запись',
                        '@child' => [
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Количество',
                                    'Тип' => 'Число',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $orderAct->quantity,
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Номенклатура',
                                    'Тип' => 'СправочникСсылка.Номенклатура',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Ссылка',
                                        '@attr' => [
                                            'Нпп' => '',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => '{УникальныйИдентификатор}',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => $order->product->object_guid,
                                                    ],
                                                ],
                                            ],
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'ВидНоменклатуры',
                                                    'Тип' => 'СправочникСсылка.ВидыНоменклатуры',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Ссылка',
                                                        '@attr' => [
                                                            'Нпп' => '',
                                                        ],
                                                        '@child' => [
                                                            [
                                                                '@name' => 'Свойство',
                                                                '@attr' => [
                                                                    'Имя' => 'Наименование',
                                                                    'Тип' => 'Строка',
                                                                ],
                                                                '@child' => [
                                                                    [
                                                                        '@name' => 'Значение',
                                                                        '@value' => $order->product->getType()->name,
                                                                    ],
                                                                ],
                                                            ],
                                                        ],
                                                    ],
                                                ],
                                            ],
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'Наименование',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => str_replace(['\r\n', '\r', '\n'], '', preg_replace('/\s{2,}/', ' ', mb_substr($order->product_title, 0, 100, 'UTF-8'))),
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Содержание',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $order->product_title,
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'СтавкаНДС',
                                    'Тип' => 'ПеречислениеСсылка.СтавкиНДС',
                                ],
                                '@child' => [
                                    $nds,
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'СчетДоходов',
                                    'Тип' => 'ПланСчетовСсылка.Хозрасчетный',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Ссылка',
                                        '@attr' => [
                                            'Нпп' => '',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'Код',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => '90.01.1',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'СчетРасходов',
                                    'Тип' => 'ПланСчетовСсылка.Хозрасчетный',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Ссылка',
                                        '@attr' => [
                                            'Нпп' => '',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'Код',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => '90.02.1',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'СчетУчетаНДСПоРеализации',
                                    'Тип' => 'ПланСчетовСсылка.Хозрасчетный',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Ссылка',
                                        '@attr' => [
                                            'Нпп' => '',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'Код',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => '90.03',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Субконто',
                                    'Тип' => 'СправочникСсылка.НоменклатурныеГруппы',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Ссылка',
                                        '@attr' => [
                                            'Нпп' => '',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'Наименование',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => 'Услуги',
                                                    ],
                                                ],
                                            ],
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'ЭтоГруппа',
                                                    'Тип' => 'Булево',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => 'false',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Сумма',
                                    'Тип' => 'Число',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => TextHelper::moneyFormatFromIntToFloat($order->amount_sales_with_vat),
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'СуммаНДС',
                                    'Тип' => 'Число',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => TextHelper::moneyFormatFromIntToFloat($order->sale_tax),
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Цена',
                                    'Тип' => 'Число',
                                ],
                                '@child' => [
                                    [

                                        '@name' => 'Значение',
                                        '@value' => TextHelper::moneyFormatFromIntToFloat($order->selling_price_with_vat),
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ЦенаБезНДС',
                                    'Тип' => 'Число',
                                ],
                                '@child' => [
                                    [

                                        '@name' => 'Значение',
                                        '@value' => TextHelper::moneyFormatFromIntToFloat($order->selling_price_no_vat),
                                    ],
                                ],
                            ],
                        ],
                    ];
                }
            }

            $this->addNomenclatureCompanyAndContractor($documentAct, $type);

            if (!in_array($documentAct->id, $this->saleGoodsServiceAct)) {
                array_push($this->saleGoodsServiceAct, $documentAct->id);

                $this->out[] = [
                    '@name' => 'Объект',
                    '@attr' => [
                        'Нпп' => $this->counter,
                        'Тип' => 'ДокументСсылка.РеализацияТоваровУслуг',
                        'ИмяПравила' => 'РеализацияТоваровУслуг',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Ссылка',
                            '@attr' => [
                                'Нпп' => $this->counter,
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => '{УникальныйИдентификатор}',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $documentAct->object_guid,
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Номер',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $documentAct->getDocumentNumberOneC(),
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Дата',
                                'Тип' => 'Дата',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => date(OneCExport::XML_DATETIME_FORMAT, strtotime($documentAct->document_date)),
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ВалютаДокумента',
                                'Тип' => 'СправочникСсылка.Валюты',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'Код',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => '643',
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Комментарий',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'Выгрузка из сервиса КУБ',
                                ],
                            ],
                        ],
                        Agreement::getAgreementPropertyByDocument($documentAct, $type),
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'СтруктурнаяЕдиница',
                                'Тип' => 'СправочникСсылка.БанковскиеСчета',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'НомерСчета',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $documentAct->invoice->company_rs,
                                                ]
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ВидОперации',
                                'Тип' => 'ПеречислениеСсылка.ВидыОперацийРеализацияТоваров',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'Услуги',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ВидЭлектронногоДокумента',
                                'Тип' => 'ПеречислениеСсылка.ВидыЭД',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'АктИсполнитель',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ДеятельностьНаПатенте',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ДеятельностьНаТорговомСборе',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ДокументБезНДС',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $documentAct->invoice->hasNds ? 'false' : 'true',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Контрагент',
                                'Тип' => 'СправочникСсылка.Контрагенты',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => '{УникальныйИдентификатор}',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $documentAct->invoice->contractor->object_guid,
                                                ],
                                            ],
                                        ],
                                        $documentAct->invoice->contractor->face_type === Contractor::TYPE_PHYSICAL_PERSON ? [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'Наименование',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => str_replace('"', '', $documentAct->invoice->contractor->name),
                                                ],
                                            ],
                                        ] : [],
                                        !empty($documentAct->invoice->contractor->ITN) ? [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'ИНН',
                                                'Тип' => 'Строка'
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $documentAct->invoice->contractor->ITN,
                                                ],
                                            ],
                                        ] : [],
                                        !empty($documentAct->invoice->contractor->PPC) ?
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'КПП',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => $documentAct->invoice->contractor->PPC,
                                                    ],
                                                ],
                                            ] : [],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'КратностьВзаиморасчетов',
                                'Тип' => 'Число',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 1,
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'КурсВзаиморасчетов',
                                'Тип' => 'Число',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 1,
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Организация',
                                'Тип' => 'СправочникСсылка.Организации',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'ИНН',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $documentAct->invoice->company->inn,
                                                ],
                                            ],
                                        ],
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'КПП',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $documentAct->invoice->company->kpp,
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ПометкаУдаления',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'СчетУчетаРасчетовСКонтрагентом',
                                'Тип' => 'ПланСчетовСсылка.Хозрасчетный',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'Код',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => '62.01',
                                                ],
                                            ],
                                        ],
                                    ],
                                ]
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'СчетУчетаРасчетовПоАвансам',
                                'Тип' => 'ПланСчетовСсылка.Хозрасчетный',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'Код',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => '62.02',
                                                ],
                                            ],
                                        ],
                                    ],
                                ]
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Проведен',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'РучнаяКорректировка',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'СпособЗачетаАвансов',
                                'Тип' => 'ПеречислениеСсылка.СпособыЗачетаАвансов',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'Автоматически'
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'СуммаВключаетНДС',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => ($documentAct->invoice->nds_view_type_id == 0 ? 'true' : ($documentAct->invoice->nds_view_type_id == 1 ? 'false' : ''))
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'СуммаДокумента',
                                'Тип' => 'Число',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => TextHelper::moneyFormatFromIntToFloat($documentAct->invoice->total_amount_with_nds),
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'УдалитьУчитыватьНДС',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'ТабличнаяЧасть',
                            '@attr' => [
                                'Имя' => 'АгентскиеУслуги',
                            ],
                        ],
                        [
                            '@name' => 'ТабличнаяЧасть',
                            '@attr' => [
                                'Имя' => 'ВозвратнаяТара',
                            ],
                        ],
                        [
                            '@name' => 'ТабличнаяЧасть',
                            '@attr' => [
                                'Имя' => 'ЗачетАвансов',
                            ],
                        ],
                        [
                            '@name' => 'ТабличнаяЧасть',
                            '@attr' => [
                                'Имя' => 'Товары',
                            ],
                        ],
                        [
                            '@name' => 'ТабличнаяЧасть',
                            '@attr' => [
                                'Имя' => 'Услуги',
                            ],
                            '@child' => $invoiceOrders,
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'УидСчета',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $documentAct->invoice->object_guid,
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Счет',
                                'Тип' => 'ДокументСсылка.СчетНаОплатуПокупателю',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'Номер',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $documentAct->invoice->getDocumentNumberOneC(),
                                                ],
                                            ],
                                        ],
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'Дата',
                                                'Тип' => 'Дата'
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => date(OneCExport::XML_DATETIME_FORMAT, strtotime($documentAct->invoice->document_date)),
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],

                        ($documentAct->invoice->has_goods) ?
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Склад',
                                    'Тип' => 'СправочникСсылка.Склады',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Ссылка',
                                        '@attr' => [
                                            'Нпп' => '',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'Наименование',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => OneCExport::getInvoiceStorageName($documentAct->invoice),
                                                    ],
                                                ],
                                            ],
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'ТипСклада',
                                                    'Тип' => 'ПеречислениеСсылка.ТипыСкладов',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => 'ОптовыйСклад',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ] : []
                    ],
                ];

                $this->exportModel->updateCounters(['objects_completed' => 1]);
            }
        }
    }

    /**
     * @param Act $documentAct
     * @param $type
     */
    public function addNomenclatureCompanyAndContractor($documentAct, $type)
    {
        if (!$this->nomenclatureGroupService) {
            $this->nomenclatureGroupService = true;
            $this->out[] = [
                '@name' => 'Объект',
                '@attr' => [
                    'Нпп' => $this->counter,
                    'Тип' => 'СправочникСсылка.НоменклатурныеГруппы',
                    'ИмяПравила' => 'НоменклатурныеГруппы',
                ],
                '@child' => [
                    [
                        '@name' => 'Ссылка',
                        '@attr' => [
                            'Нпп' => $this->counter,
                        ],
                        '@child' => [
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Наименование',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => 'Услуги',
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ЭтоГруппа',
                                    'Тип' => 'Булево',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => 'false',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'Родитель',
                            'Тип' => 'СправочникСсылка.НоменклатурныеГруппы',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                            ],
                        ],
                    ],
                ],
            ];
            $this->counter++;
        }

        if (!$this->nomenclatureGroupMain) {
            $this->nomenclatureGroupMain = true;
            $this->out[] = self::getMainNomenclatureGroup($this->counter);
            $this->counter++;
        }

        if (!$this->currency) {

            $this->currency = true;
            $this->out[] = [
                '@name' => 'Объект',
                '@attr' => [
                    'Нпп' => $this->counter,
                    'Тип' => 'СправочникСсылка.Валюты',
                    'ИмяПравила' => 'ВалютаВзаиморасчетов',
                ],
                '@child' => [
                    [
                        '@name' => 'Ссылка',
                        '@attr' => [
                            'Нпп' => $this->counter,
                        ],
                        '@child' => [
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Код',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => '643',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'Наименование',
                            'Тип' => 'Строка',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => 'руб.',
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'НаименованиеПолное',
                            'Тип' => 'Строка',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => 'Российский рубль',
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'СпособУстановкиКурса',
                            'Тип' => 'ПеречислениеСсылка.СпособыУстановкиКурсаВалюты',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => 'РучнойВвод',
                            ],
                        ],
                    ],
                ],
            ];
            $this->counter++;
        }


        if (!in_array($documentAct->invoice->company->id, $this->existCompany)) {
            array_push($this->existCompany, $documentAct->invoice->company->id);
            if ($documentAct->invoice->company->company_type_id == CompanyType::TYPE_IP) {
                $this->out[] = [
                    '@name' => 'Объект',
                    '@attr' => [
                        'Нпп' => $this->counter,
                        'Тип' => 'СправочникСсылка.ФизическиеЛица',
                        'ИмяПравила' => 'ФизическиеЛица',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Ссылка',
                            '@attr' => [
                                'Нпп' => $this->counter,
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'ИНН',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $documentAct->invoice->company->inn,
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ФИО',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $documentAct->invoice->company->getIpFio(),
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Наименование',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $documentAct->invoice->company->getIpFio(),
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Код',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ЭтоГруппа',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ],
                            ],
                        ],
                    ],
                ];
                $this->counter++;
            }

            $this->out[] = [
                '@name' => 'Объект',
                '@attr' => [
                    'Нпп' => $this->counter,
                    'Тип' => 'СправочникСсылка.Организации',
                    'ИмяПравила' => 'Организации',
                    'НеЗамещать' => 'true',
                ],
                '@child' => [
                    [
                        '@name' => 'Ссылка',
                        '@attr' => [
                            'Нпп' => $this->counter,
                        ],
                        '@child' => [
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ИНН',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $documentAct->invoice->company->inn,
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'КПП',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $documentAct->invoice->company->kpp,
                                    ],
                                ],
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'ЮридическоеФизическоеЛицо',
                            'Тип' => 'ПеречислениеСсылка.ЮридическоеФизическоеЛицо',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => $documentAct->invoice->company->company_type_id == CompanyType::TYPE_IP
                                    ? 'ФизическоеЛицо'
                                    : 'ЮридическоеЛицо',
                            ],
                        ],
                    ],
                    $documentAct->invoice->company->company_type_id == CompanyType::TYPE_IP ?
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ИндивидуальныйПредприниматель',
                                'Тип' => 'СправочникСсылка.ФизическиеЛица',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'ИНН',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $documentAct->invoice->company->inn,
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ] : [],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'Наименование',
                            'Тип' => 'Строка',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => $documentAct->invoice->company->name_full,
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'НаименованиеПолное',
                            'Тип' => 'Строка',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => $documentAct->invoice->company->name_full,
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'НаименованиеСокращенное',
                            'Тип' => 'Строка',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => $documentAct->invoice->company->name_short,
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'ОГРН',
                            'Тип' => 'Строка',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => $documentAct->invoice->company->ogrn,
                            ],
                        ],
                    ],
                ],
            ];
            $this->counter++;
        }

        if (array_key_exists($documentAct->invoice->contractor->ITN, $this->existsContractor)) {
            if (!in_array($documentAct->invoice->contractor->PPC, $this->existsContractor[$documentAct->invoice->contractor->ITN])) {
                $this->existsContractor[$documentAct->invoice->contractor->ITN][] = $documentAct->invoice->contractor->PPC;
                $this->addContractor($documentAct->invoice->contractor);
                $this->counter++;
            }
        } else {
            $this->existsContractor[$documentAct->invoice->contractor->ITN][] = $documentAct->invoice->contractor->PPC;
            $this->addContractor($documentAct->invoice->contractor);
            $this->counter++;
        }
        $agreement = Agreement::getAgreementObjectByDocument($documentAct, $this->counter, $type);
        if (!in_array($agreement['guid'], $this->existsAgreement)) {
            $this->out[] = $agreement['object'];
            $this->existsAgreement[] = $agreement['guid'];
            $this->counter++;
        }

        if (is_array($documentAct->orders) && sizeof($documentAct->orders) > 0) {
            foreach ($documentAct->orderActs as $orderAct) {
                $order = $orderAct->order;
                if (!in_array($order->product->id, $this->existNomenclature)) {
                    array_push($this->existNomenclature, $order->product->id);
                    if (empty($order->product->object_guid)) {
                        $order->product->object_guid = OneCExport::generateGUID();
                        $order->product->save(false);
                    }

                    $this->out[] = [
                        '@name' => 'Объект',
                        '@attr' => [
                            'Нпп' => $this->counter,
                            'Тип' => 'СправочникСсылка.Номенклатура',
                            'ИмяПравила' => 'Номенклатура',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Ссылка',
                                '@attr' => [
                                    'Нпп' => $this->counter,
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => '{УникальныйИдентификатор}',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => $order->product->object_guid,
                                            ],
                                        ],
                                    ],
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'ВидНоменклатуры',
                                            'Тип' => 'СправочникСсылка.ВидыНоменклатуры',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Ссылка',
                                                '@attr' => [
                                                    'Нпп' => '',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Свойство',
                                                        '@attr' => [
                                                            'Имя' => 'Наименование',
                                                            'Тип' => 'Строка',
                                                        ],
                                                        '@child' => [
                                                            [
                                                                '@name' => 'Значение',
                                                                '@value' => $order->product->getType()->name,
                                                            ],
                                                        ],
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'Наименование',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => str_replace(['\r\n', '\r', '\n'], '', preg_replace('/\s{2,}/', ' ', mb_substr($order->product_title, 0, 100, 'UTF-8'))),
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Услуга',
                                    'Тип' => 'Булево',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $order->product->production_type ? 'false' : 'true',
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Код',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $order->product->code,
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'НоменклатурнаяГруппа',
                                    'Тип' => 'СправочникСсылка.НоменклатурныеГруппы',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Ссылка',
                                        '@attr' => [
                                            'Нпп' => '',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'Наименование',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        //'@value' => 'Услуги',
                                                        '@value' => 'Основная номенклатурная группа',
                                                    ],
                                                ],
                                            ],
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'ЭтоГруппа',
                                                    'Тип' => 'Булево',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => 'false',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ЭтоГруппа',
                                    'Тип' => 'Булево',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => 'false',
                                    ],
                                ],
                            ],
                        ],
                    ];
                    $this->counter++;
                }
            }
        }
    }
}

