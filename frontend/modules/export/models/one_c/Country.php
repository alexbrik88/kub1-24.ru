<?php

namespace frontend\modules\export\models\one_c;

/**
 * Class Country
 * @package frontend\modules\export\models\one_c
 */
class Country
{
    public static function render($model, $counter)
    {
        return $productCountry = [
            '@name' => 'Объект',
            '@attr' => [
                'Нпп' => $counter,
                'Тип' => 'СправочникСсылка.СтраныМира',
                'ИмяПравила' => 'СтраныМира',
            ],
            '@child' => [
                [
                    '@name' => 'Ссылка',
                    '@attr' => [
                        'Нпп' => $counter,
                    ],
                    '@child' => [
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Код',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $model->code,
                                ],
                            ],
                        ],
                    ],
                ],
                [
                    '@name' => 'Свойство',
                    '@attr' => [
                        'Имя' => 'Наименование',
                        'Тип' => 'Строка',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Значение',
                            '@value' => $model->name_short,
                        ],
                    ],
                ],
                [
                    '@name' => 'Свойство',
                    '@attr' => [
                        'Имя' => 'НаименованиеПолное',
                        'Тип' => 'Строка',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Значение',
                            '@value' => $model->name_short,
                        ],
                    ],
                ],
                [
                    '@name' => 'Свойство',
                    '@attr' => [
                        'Имя' => 'ПометкаУдаления',
                        'Тип' => 'Булево',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Значение',
                            '@value' => 'false',
                        ],
                    ],
                ],
            ],
        ];
    }
}