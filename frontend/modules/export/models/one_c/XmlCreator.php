<?php
namespace frontend\modules\export\models\one_c;

use yii\helpers\FileHelper;
/**
 * Class XmlCreator
 *
 * @package frontend\modules\export\models\one_c
 */
class XmlCreator
{
    /**
     * XmlWriter object
     *
     * @var \XMLWriter
     */
    private $xmlWriter;

    /**
     * Path to temp file
     *
     * @var string
     */
    private $tempFile = '';

    /**
     * @var string
     */
    private $filename = '';

    /**
     * Construct
     *
     * @param $folder
     * @param $file
     *
     * @throws \Exception
     */
    public function __construct($folder, $file)
    {
        $this->xmlWriter = new \XMLWriter();

        $this->filename = $file;
        $this->tempFile = $folder . DIRECTORY_SEPARATOR . $file;
        if (!file_exists($folder)) {
            FileHelper::createDirectory($folder);
        }
        if (false === $this->tempFile || false === $this->xmlWriter->openUri($this->tempFile)) {
            throw new \Exception('Error. file does not exist.');
        }

        $this->xmlWriter->setIndent(false);
        $this->xmlWriter->setIndentString('');
    }

    /**
     * Destruct
     */
    public function __destruct()
    {
        unset($this->xmlWriter);
    }

    /**
     * Call func
     *
     * @param $function
     * @param $args
     *
     * @throws \Exception
     */
    public function __call($function, $args)
    {
        if (method_exists($this->xmlWriter, $function) === false) {
            throw new \Exception("Method '{$function}' does not exists.");
        }

        try {
            @call_user_func_array([$this->xmlWriter, $function], $args);
        } catch (\Exception $ex) {}
    }

    /**
     * Get path to file
     *
     * @return string
     */
    public function getFilePath()
    {
        $this->xmlWriter->flush();

        return $this->filename;
    }

//    public function writeElementBlock($element, $attributes, $value = null)
//    {
//        $this->xmlWriter->startElement($element);
//        if (!is_array($attributes)) {
//            $attributes = [$attributes => $value];
//        }
//        foreach ($attributes as $attribute => $value) {
//            $this->xmlWriter->writeAttribute($attribute, $value);
//        }
//        $this->xmlWriter->endElement();
//    }
//
//
//    public function writeElementIf($condition, $element, $attribute = null, $value = null)
//    {
//        if ($condition == true) {
//            if (is_null($attribute)) {
//                $this->xmlWriter->writeElement($element, $value);
//            } else {
//                $this->xmlWriter->startElement($element);
//                $this->xmlWriter->writeAttribute($attribute, $value);
//                $this->xmlWriter->endElement();
//            }
//        }
//    }
//
//    public function writeAttributeIf($condition, $attribute, $value)
//    {
//        if ($condition == true) {
//            $this->xmlWriter->writeAttribute($attribute, $value);
//        }
//    }
}
