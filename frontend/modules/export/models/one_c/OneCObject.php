<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.04.2016
 * Time: 4:25
 */

namespace frontend\modules\export\models\one_c;


use common\models\address\Country as CountryService;
use common\models\address\Country;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\dictionary\bik\BikDictionary;
use yii\base\BaseObject;

/**
 * Class OneCObject
 * @package frontend\modules\export\models\one_c
 */
class OneCObject extends BaseObject
{
    /**
     *  Исключаем дублирование Контрагентов
     * @var array
     */
    public $existsContractor = [];
    /**
     *  Исключаем думалирование Валют
     * @var bool
     */
    public $currency = false;
    /**
     *  УИД Валюты
     * @var null
     */
    public $individualCurrency = null;
    /**
     *  Исключаем дублирование Номенклатуры
     * @var array
     */
    public $existNomenclature = [];
    /**
     *  Исключаем дублирование Организаций
     * @var array
     */
    public $existCompany = [];
    /**
     *  Исключаем дублирование БанковскихСчетов
     * @var array
     */
    public $existBankInvoice = [];
    /**
     *  Исключаем дублирование Банков
     * @var array
     */
    public $existBank = [];
    /**
     * Исключаем дублирование Стран
     * @var array
     */
    public $existCountry = [];
    /**
     *  Исключаем дублирование Складов
     * @var bool
     */
    public $storage = false;
    /**
     *  Исключаем дублирование НоменклатурныхГруппТоваров
     * @var bool
     */
    public $nomenclatureGroupProduct = false;
    /**
     *  Исключаем дублирование НоменклатурныхГруппУслуг
     * @var bool
     */
    public $nomenclatureGroupService = false;
    /**
     *  Исключаем дублирование НоменклатурныхГруппОсновных
     * @var bool
     */
    public $nomenclatureGroupMain = false;
    /**
     *  Исключаем дублирование ПоступленияТоваровУслуг в актах
     * @var array
     */
    public $receiptGoodsServiceAct = [];
    /**
     *  Исключаем дублирование РеализацииТоваровУслуг в актах
     * @var array
     */
    public $saleGoodsServiceAct = [];
    /**
     *  Исключаем дублирование ПоступленияТоваровУслуг в товарах
     * @var array
     */
    public $receiptGoodsServiceProduct = [];
    /**
     *  Исключаем дублирование РеализацииТоваровУслуг в товарах
     * @var array
     */
    public $saleGoodsServiceProduct = [];
    /**
     *  Исключаем дублирование РеализацииТоваровУслуг в счетах с товарами и услугами
     * @var array
     */
    public $saleInvoiceWithActAndPackingList = [];
    /**
     * Исключаем дублирование ПоступленияТоваровУслуг в счетах с товарами и услугами
     * @var array
     */
    public $receiptInvoiceWithActAndPackingList = [];
    /**
     * Исключаем дублирование Договоров
     * @var array
     */
    public $existsAgreement = [];

    /**
     * @param $counter
     * @return array
     */
    public static function getMainNomenclatureGroup($counter) {
        return [
            '@name' => 'Объект',
            '@attr' => [
                'Нпп' => $counter,
                'Тип' => 'СправочникСсылка.НоменклатурныеГруппы',
                'ИмяПравила' => 'НоменклатурныеГруппы',
            ],
            '@child' => [
                [
                    '@name' => 'Ссылка',
                    '@attr' => [
                        'Нпп' => $counter,
                    ],
                    '@child' => [
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Наименование',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'Основная номенклатурная группа',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ЭтоГруппа',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ],
                            ],
                        ],
                    ],
                ],
                [
                    '@name' => 'Свойство',
                    '@attr' => [
                        'Имя' => 'Родитель',
                        'Тип' => 'СправочникСсылка.НоменклатурныеГруппы',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Значение',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     *
     */
    public function addStorage()
    {
        if (!$this->storage) {
            $this->storage = true;

            $storageNames = (count(OneCExport::$storageNames) <= 1) ? [0 => 'Основной'] : OneCExport::$storageNames;

            foreach ($storageNames as $storageName) {
                $this->out[] = [
                    '@name' => 'Объект',
                    '@attr' => [
                        'Нпп' => $this->counter,
                        'Тип' => 'СправочникСсылка.Склады',
                        'ИмяПравила' => 'Склады',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Ссылка',
                            '@attr' => [
                                'Нпп' => $this->counter,
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Наименование',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $storageName,
                                        ],
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'ТипСклада',
                                        'Тип' => 'ПеречислениеСсылка.ТипыСкладов',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => 'ОптовыйСклад',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ];
                $this->counter++;
            }
        }
    }
    
    //////////////
    // CONTRACTOR
    /////////////

    /**
     * @param $contractor
     */
    public function addContractor(Contractor $contractor)
    {
        if (isset($contractor->one_c_exported) && !$contractor->one_c_exported) {
            $contractor->updateAttributes(['one_c_exported' => 1]);
        }

        // СправочникСсылка.Контрагенты
        $this->out[] = [
            '@name' => 'Объект',
            '@attr' => [
                'Нпп' => $this->counter,
                'Тип' => 'СправочникСсылка.Контрагенты',
                'ИмяПравила' => 'Контрагенты',
                'НеЗамещать' => 'true',
            ],
            '@child' => [
                [
                    '@name' => 'Ссылка',
                    '@attr' => [
                        'Нпп' => $this->counter,
                    ],
                    '@child' => [
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => '{УникальныйИдентификатор}',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $contractor->object_guid,
                                ],
                            ],
                        ],
                        $contractor->face_type === Contractor::TYPE_PHYSICAL_PERSON ? [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Наименование',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => str_replace('"', '', $contractor->name),
                                ],
                            ],
                        ] : [],
                        !empty($contractor->ITN) ? [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ИНН',
                                'Тип' => 'Строка'
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $contractor->ITN,
                                ],
                            ],
                        ] : [],
                        !empty($contractor->PPC) ?
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'КПП',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $contractor->PPC,
                                    ],
                                ],
                            ] : [],

                    ],
                ],
                [
                    '@name' => 'Свойство',
                    '@attr' => [
                        'Имя' => 'ЮридическоеФизическоеЛицо',
                        'Тип' => 'ПеречислениеСсылка.ЮридическоеФизическоеЛицо',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Значение',
                            '@value' => $contractor->company_type_id == CompanyType::TYPE_IP
                                ? 'ФизическоеЛицо'
                                : 'ЮридическоеЛицо',
                        ],
                    ],
                ],
                [
                    '@name' => 'Свойство',
                    '@attr' => [
                        'Имя' => 'Наименование',
                        'Тип' => 'Строка',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Значение',
                            '@value' => str_replace('"', '', $contractor->name),
                        ],
                    ],
                ],
                [
                    '@name' => 'Свойство',
                    '@attr' => [
                        'Имя' => 'СтранаРегистрации',
                        'Тип' => 'СправочникСсылка.СтраныМира',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Ссылка',
                            '@attr' => [
                                'Нпп' => '',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Код',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => CountryService::findOne(CountryService::COUNTRY_RUSSIA)->code,
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
                $contractor->BIN ?
                [
                    '@name' => 'Свойство',
                    '@attr' => [
                        'Имя' => 'ОГРН',
                        'Тип' => 'Строка',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Значение',
                            '@value' => $contractor->BIN,
                        ],
                    ],
                ] : [],
                $contractor->legal_address ?
                [
                    '@name' => 'Свойство',
                    '@attr' => [
                        'Имя' => 'ЮридическийАдрес',
                        'Тип' => 'Строка',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Значение',
                            '@value' => $contractor->legal_address,
                        ],
                    ],
                ] : [],
                [
                    '@name' => 'Свойство',
                    '@attr' => [
                        'Имя' => 'ФормаСобственностиСокращенно',
                        'Тип' => 'Строка',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Значение',
                            '@value' => ($contractor->companyType instanceof CompanyType) ? $contractor->companyType->name_short : '',
                        ],
                    ],
                ],
                [
                    '@name' => 'Свойство',
                    '@attr' => [
                        'Имя' => 'ФормаСобственностиПолностью',
                        'Тип' => 'Строка',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Значение',
                            '@value' => ($contractor->companyType instanceof CompanyType) ? $contractor->companyType->name_full : '',
                        ],
                    ],
                ],
                $contractor->bank instanceof BikDictionary ?
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'РасчетныйСчет',
                            'Тип' => 'Строка',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => $contractor->current_account,
                            ],
                        ],
                    ] : [],
                $contractor->bank instanceof BikDictionary ?
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'БанкБИК',
                            'Тип' => 'Строка',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Значение',
                                '@value' => $contractor->bank->bik,
                            ],
                        ],
                    ] : [],
            ],
        ];
        $this->counter++;

        if ($contractor->bank instanceof BikDictionary) {

            if (!in_array($contractor->bank->bik, $this->existBank)) {

                array_push($this->existBank, $contractor->bank->bik);
                // СправочникСсылка.Банки
                $this->out[] = [
                    '@name' => 'Объект',
                    '@attr' => [
                        'Нпп' => $this->counter,
                        'Тип' => 'СправочникСсылка.Банки',
                        'ИмяПравила' => 'Банки',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Ссылка',
                            '@attr' => [
                                'Нпп' => $this->counter,
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Код',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $contractor->bank->bik,
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        $contractor->bank->address ?
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Адрес',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $contractor->bank->address,
                                    ]
                                ],
                            ] : [],
                        $contractor->bank->city ?
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Город',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $contractor->bank->city,
                                    ]
                                ],
                            ] : [],
                        $contractor->bank->ks ?
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'КоррСчет',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $contractor->bank->ks,
                                    ]
                                ],
                            ] : [],
                        $contractor->bank->name ?
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Наименование',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $contractor->bank->name,
                                    ]
                                ],
                            ] : [],
                        $contractor->bank->phone ?
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Телефоны',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $contractor->bank->phone,
                                    ]
                                ],
                            ] : [],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ЭтоГруппа',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Страна',
                                'Тип' => 'СправочникСсылка.СтраныМира',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'Код',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => Country::findOne(Country::COUNTRY_RUSSIA)->code,
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ];
                $this->counter++;
            }
        }

        if ($contractor->bank instanceof BikDictionary) {
            if (!in_array($contractor->current_account, $this->existBankInvoice)) {
                if (empty($contractor->current_account_guid)) {
                    $contractor->current_account_guid = OneCExport::generateGUID();
                    $contractor->save(false);
                }

                array_push($this->existBankInvoice, $contractor->current_account);
                // СправочникСсылка.БанковскиеСчета
                $this->out[] = [
                    '@name' => 'Объект',
                    '@attr' => [
                        'Нпп' => $this->counter,
                        'Тип' => 'СправочникСсылка.БанковскиеСчета',
                        'ИмяПравила' => 'БанковскиеСчета',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Ссылка',
                            '@attr' => [
                                'Нпп' => $this->counter,
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => '{УникальныйИдентификатор}',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $contractor->current_account_guid,
                                        ],
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'НомерСчета',
                                        'Тип' => 'Строка',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Значение',
                                            '@value' => $contractor->current_account,
                                        ]
                                    ],
                                ],
                                [
                                    '@name' => 'Свойство',
                                    '@attr' => [
                                        'Имя' => 'Владелец',
                                        'Тип' => 'СправочникСсылка.Контрагенты',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Ссылка',
                                            '@attr' => [
                                                'Нпп' => '',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Свойство',
                                                    '@attr' => [
                                                        'Имя' => '{УникальныйИдентификатор}',
                                                        'Тип' => 'Строка',
                                                    ],
                                                    '@child' => [
                                                        [
                                                            '@name' => 'Значение',
                                                            '@value' => $contractor->object_guid,
                                                        ],
                                                    ],
                                                ],
                                                [
                                                    '@name' => 'Свойство',
                                                    '@attr' => [
                                                        'Имя' => 'ИНН',
                                                        'Тип' => 'Строка',
                                                    ],
                                                    '@child' => [
                                                        [
                                                            '@name' => 'Значение',
                                                            '@value' => $contractor->ITN,
                                                        ],
                                                    ],
                                                ],
                                                !empty($contractor->PPC) ?
                                                    [
                                                        '@name' => 'Свойство',
                                                        '@attr' => [
                                                            'Имя' => 'КПП',
                                                            'Тип' => 'Строка',
                                                        ],
                                                        '@child' => [
                                                            [
                                                                '@name' => 'Значение',
                                                                '@value' => $contractor->PPC,
                                                            ],
                                                        ],
                                                    ] : [],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Банк',
                                'Тип' => 'СправочникСсылка.Банки',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'Код',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $contractor->bank->bik,
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ВалютаДенежныхСредств',
                                'Тип' => 'СправочникСсылка.Валюты',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'Код',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => '643',
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Валютный',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ]
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ВидСчета',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'Расчетный',
                                ]
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ВсегдаУказыватьКПП',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ]
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'МесяцПрописью',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ]
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Наименование',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $contractor->bank->name,
                                ]
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ПометкаУдаления',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ]
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'СуммаБезКопеек',
                                'Тип' => 'Булево',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => 'false',
                                ]
                            ],
                        ],
                    ],
                ];
                $this->counter++;
            }
        }
    }
}