<?php

namespace frontend\modules\export;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\export\controllers';

    public function init()
    {
        parent::init();
    }
}
