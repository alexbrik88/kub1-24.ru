<?php
namespace frontend\modules\export\controllers;

use common\components\CommonController;
use frontend\modules\export\models\export\Export;
use frontend\modules\export\models\export\ExportFiles;
use frontend\rbac\permissions;
use Yii;
use yii\web\Response;
use yii\web\NotFoundHttpException;

/**
 * Class FileController
 * @package frontend\modules\export\controllers
 */
class FileController extends CommonController
{
    /**
     * @return mixed
     */
    public function actionProgress()
    {
        if (\Yii::$app->request->isAjax) {
            if ($userId = \Yii::$app->request->post('userId')) {
                /** @var $exportModel Export */
                $exportModel = Export::find()->where('user_id = :user_id AND status = :status', [':user_id' => $userId, ':status' => Export::STATUS_IN_PROGRESS])->orderBy('id DESC')->one();

                $out['progress'] = 0;

                if ($exportModel instanceof Export) {
                    if ($exportModel->total_objects == $exportModel->objects_completed && $exportModel->total_objects > 0) {
                        $out['progress'] = 100;
                    } else if ($exportModel->total_objects == 0 || $exportModel->objects_completed == 0) {
                        $out['progress'] = 0;
                    } else if ($exportModel->total_objects > 0 && $exportModel->objects_completed > 0) {
                        if ($exportModel->total_objects == $exportModel->objects_completed) {
                            $out['progress'] = 100;
                        } else {
                            $out['progress'] = ceil($exportModel->objects_completed * 100 / $exportModel->total_objects);
                        }
                    }
                }

                \Yii::$app->response->format = Response::FORMAT_JSON;

                return $out;
            }
        }
    }

    /**
     * @return mixed
     */
    public function actionProgressFiles()
    {
        if (\Yii::$app->request->isAjax) {
            if ($userId = \Yii::$app->request->post('userId')) {
                /** @var $exportModel Export */
                $exportModel = ExportFiles::find()->where('user_id = :user_id AND status = :status', [':user_id' => $userId, ':status' => ExportFiles::STATUS_IN_PROGRESS])->orderBy('id DESC')->one();

                $out['progress'] = 0;

                if ($exportModel instanceof ExportFiles) {
                    if ($exportModel->total_objects == $exportModel->objects_completed && $exportModel->total_objects > 0) {
                        $out['progress'] = 100;
                    } else if ($exportModel->total_objects == 0 || $exportModel->objects_completed == 0) {
                        $out['progress'] = 0;
                    } else if ($exportModel->total_objects > 0 && $exportModel->objects_completed > 0) {
                        if ($exportModel->total_objects == $exportModel->objects_completed) {
                            $out['progress'] = 100;
                        } else {
                            $out['progress'] = ceil($exportModel->objects_completed * 100 / $exportModel->total_objects);
                        }
                    }
                }

                \Yii::$app->response->format = Response::FORMAT_JSON;

                return $out;
            }
        }
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionDownload()
    {
        if ($filename = \Yii::$app->request->get('f')) {
            $userId = \Yii::$app->user->id;

            $exportModel = Export::find()->where('filename = :filename AND user_id = :user_id', [':filename' => $filename, ':user_id' => $userId])->one();

            if ($exportModel instanceof Export) {
                $filepath = \Yii::getAlias('@frontend/runtime/export') . DIRECTORY_SEPARATOR . $exportModel->filename;

                if (is_file($filepath)) {

                    return \Yii::$app->response->sendFile($filepath, $exportModel->filename, [
                            'mimeType' => (substr($exportModel->filename, -4) == '.zip') ? 'application/zip' : 'text/xml'
                    ])->on(Response::EVENT_AFTER_SEND, function($event) {
                        unlink($event->data);
                    }, $filepath);
                } else throw new NotFoundHttpException('Файл не найден');
            } else throw new NotFoundHttpException('Такого файла не существует');
        }
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionDownloadArchive()
    {
        if ($filename = \Yii::$app->request->get('f')) {
            $userId = \Yii::$app->user->id;

            $exportModel = ExportFiles::find()
                ->andWhere('filename = :filename AND user_id = :user_id', [':filename' => $filename, ':user_id' => $userId])->orderBy(['created_at' => SORT_DESC])->one();

            if ($exportModel !== null) {
                $filepath = \Yii::getAlias('@frontend/runtime/export/export_files/')
                    . $exportModel->id . '/' . $exportModel->filename;
                if (is_file($filepath)) {
                    return \Yii::$app->response->sendFile($filepath, $exportModel->filename, [
                        'mimeType' => 'application/zip',
                    ])->on(Response::EVENT_AFTER_SEND, function($event) {
                        unlink($event->data);
                    }, $filepath);
                } else throw new NotFoundHttpException('Файл не найден');
            } else throw new NotFoundHttpException('Такого файла не существует');
        }
    }
}
