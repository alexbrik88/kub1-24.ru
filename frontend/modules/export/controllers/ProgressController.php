<?php

namespace frontend\modules\export\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class ProgressController
 * @package frontend\modules\export\controllers
 */
class ProgressController extends Controller
{
    /**
     * @return array
     */

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionFiles()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'progress' => Yii::$app->session->get('export_files_progress', 0),
        ];
    }
}
