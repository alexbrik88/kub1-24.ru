<?php

namespace frontend\modules\export\controllers;

use common\components\filters\AccessControl;
use common\components\pdf\PdfRenderer;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\document\PaymentOrder;
use common\models\document\Upd;
use common\models\file\File;
use common\models\Company;
use frontend\components\FrontendController;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\modules\export\models\export\ExportFiles;
use frontend\modules\export\models\export\ExportFilesSearch;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use ZipArchive;

/**
 * Class OneSController
 * @package frontend\modules\export\controllers
 */
class FilesController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF, UserRole::ROLE_ACCOUNTANT],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = new ExportFiles();
        $searchModel = new ExportFilesSearch();
        $period = StatisticPeriod::getSessionPeriod();

        $model->period_start_date = $period['from'];
        $model->period_end_date = $period['to'];

        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('index', [
            'exportFilesModel' => $model,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionCreate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->session->set('export_files_progress', "0");
        Yii::$app->session->close();

        $user = Yii::$app->user->identity;
        $company = $user->company;

        if ($company->isFreeTariff) {
            return [
                'success' => false,
                'error' => 'Выгрузить архив документов можно только на платном тарифе. ' .
                            Html::a('Оплатить сервис', ['/subscribe/default/index']),
            ];
        }

        ini_set('max_execution_time', 1800);
        ini_set('memory_limit', '512M');

        $model = new ExportFiles([
            'user_id' => $user->id,
            'company_id' => $company->id,
        ]);
        $model->populateRelation('employee', $user);
        $model->populateRelation('company', $company);

        $out = [
            'success' => false,
            'error' => 'Не удалось создать архив. Попробуйте позже еще раз.',
        ];
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->getDocumentsData()['count'] > 0) {
                if ($archive = $model->createArchive()) {
                    $out = [
                        'success' => true,
                        'url' => \Yii::$app->urlManager->createAbsoluteUrl(['/export/files/download', 'id' => $model->id]),
                    ];
                }
            } else {
                $out = [
                    'success' => false,
                    'error' => 'По данным критериям объекты не найдены.',
                ];
            }
        } else {
            $out = [
                'success' => false,
                'error' => implode('<br>', $model->getFirstErrors()),
            ];
        }
        Yii::$app->session->remove('export_files_progress');

        return $out;
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionDownload($id)
    {
        if ($model = ExportFiles::findOne(['id' => $id, 'company_id' => Yii::$app->user->identity->company->id])) {
            $file = $model->getArchivePath();

            if (is_file($file)) {
                return \Yii::$app->response->sendFile($file, $model->filename, [
                    'mimeType' => 'application/zip',
                ])->on(Response::EVENT_AFTER_SEND, function ($event) {
                    unlink($event->data);
                }, $file);
            } else {
                throw new NotFoundHttpException('Файл не найден');
            }
        }

        throw new NotFoundHttpException('Файл не найден');
    }
}
