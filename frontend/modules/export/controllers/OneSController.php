<?php
namespace frontend\modules\export\controllers;

use common\components\filters\AccessControl;
use frontend\components\FrontendController;
use frontend\components\StatisticPeriod;
use frontend\modules\export\models\export\Export;
use frontend\modules\export\models\export\ExportSearch;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Response;

/**
 * Class OneSController
 * @package frontend\modules\export\controllers
 */
class OneSController extends FrontendController
{
    public $layout = '//one_s';

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF, UserRole::ROLE_ACCOUNTANT],
                    ],
                ],
            ],
        ]);
    }


    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $exportModel = new Export();
        $searchModel = new ExportSearch();
        $period = StatisticPeriod::getSessionPeriod();

        $exportModel->period_start_date = $period['from'];
        $exportModel->period_end_date = $period['to'];

        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('index', [
            'exportModel' => $exportModel,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function actionCreate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $company = Yii::$app->user->identity->company;

        if (!$this->isAllowed($company)) {
            return [
                'success' => false,
                'error' => 'Выгрузить данные в 1С можно только на платном тарифе. ' .
                            Html::a('Оплатить с ервис', ['/subscribe/default/index']),
            ];
        }

        if (YII_ENV_PROD) {
            ini_set('memory_limit', '8192M');
            ini_set('max_execution_time', 7200);
        }

        $exportModel = new Export(['company_id' => $company->id,]);

        if ($exportModel->load(Yii::$app->request->post())) {
            if ($exportModel->save(false)) {
                $oneCExport = new OneCExport($exportModel);

                $out = [];

                if ($oneCExport->findDataObjects()) {
                    if ($filePath = $oneCExport->createExportFile()) {
                        $out = [
                            'success' => true,
                            'url' => \Yii::$app->urlManager->createAbsoluteUrl(['export/file/download', 'f' => $filePath]),
                        ];
                    } else {
                        $out = [
                            'success' => false,
                            'error' => 'Не удалось создать файл экспорта.',
                        ];
                    };
                } else {
                    $out = [
                        'success' => false,
                        'error' => 'По данным критериям объекты не найдены.',
                    ];
                    $exportModel->delete();
                }

                return $out;
            }
        }
    }

    protected function isAllowed($company)
    {
        if (!$company->isFreeTariff || ArrayHelper::getValue(Yii::$app->params, '1c-export-enabled'))
            return true;

        return false;
    }
}