<?php

use frontend\modules\export\models\export\export;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\models\Documents;

$outDocumentsType = Export::$documentTypes;
$inDocumentsType = Export::$documentTypes;
unset($inDocumentsType[Documents::DOCUMENT_UPD]);

$form = \yii\widgets\ActiveForm::begin([
    'id' => 'one-s-export-form',
    'action' => ['create'],
    'method' => 'post',
//    'enableClientValidation' => true,
//    'enableAjaxValidation' => true,
//    'validateOnSubmit' => true,
    'validateOnBlur' => false,
    'fieldConfig' => [
        'options' => [
            'class' => '',
        ],
    ],
    'options' => [
        'data' => [
            'progress-url' => Url::to(['/export/file/progress']),
        ],
    ],
]); ?>
    <div class="portlet box darkblue blk_wth_srch export-form-wrap">
        <div class="portlet-title row-fluid">
            <div class="caption list_recip col-md-3 col-sm-3">
                Параметры выгрузки
            </div>
            <div class="col-md-5 col-sm-5 check_posit">
                <?= $form->field($exportModel, 'only_new')->checkbox(); ?>
            </div>
            <div class="tools tools_button col-md-3 col-sm-3" style="padding-top: 4px!important;">

                <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row m-r',]); ?>
            </div>
        </div>
        <div class="portlet-body accounts-list check_group_posit">
            <?= $form->field($exportModel, 'io_type_out')->checkbox(['id' => 'export-type-out']); ?>
            <?= $form->field($exportModel, 'io_type_out_items')->label(false)->checkboxList($outDocumentsType, ['class' => 'export-type-out']); ?>
            <?= $form->field($exportModel, 'io_type_in')->checkbox(['id' => 'export-type-in']); ?>
            <?= $form->field($exportModel, 'io_type_in_items')->label(false)->checkboxList($inDocumentsType, ['class' => 'export-type-in']); ?>
            <?= $form->field($exportModel, 'product_and_service')->checkbox(); ?>
            <?= $form->field($exportModel, 'contractor')->checkbox(['id' => 'export-contractor']); ?>
            <?= $form->field($exportModel, 'contractor_items')->label(false)->checkboxList(Export::$contractorTypes, ['class' => 'export-contractor']); ?>
            <?= $form->field($exportModel, 'period_start_date')->label(false)->hiddenInput(); ?>
            <?= $form->field($exportModel, 'period_end_date')->label(false)->hiddenInput(); ?>
            <?= Html::hiddenInput('user_id', \Yii::$app->user->id, ['id' => 'user_id']); ?>
            <div class="load_button">
                <?= Html::submitButton('Сформировать файл для 1С'); ?>
                <div class="progress-status m-t-n-10" style="display: none;">
                    <label><span class="export-progress-action"
                                 data-in-progress="идет формирование файла... "
                                 data-completed="файл сформирован"></span><span
                            class="progress-value"></span></label>

                    <div
                        class="progress progress-striped active export-progressbar">
                        <div class="progress-bar" role="progressbar"
                             aria-valuenow="0" aria-valuemin="0"
                             aria-valuemax="100" style="width:0%;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-left contragent-label">
                Если у вас возникли трудности при выгрузке в 1С, обращайтесь в
                службу
                поддержки: <?= Html::a(Yii::$app->params['emailList']['support'], 'mailto:' . Yii::$app->params['emailList']['support']); ?>
            </div>
        </div>
    </div>
<?php $form->end(); ?>