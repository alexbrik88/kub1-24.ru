<?php
/* @var $this yii\web\View */
/* @var $templates common\models\template\Template[] */

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = 'Выгрузка в 1С';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portlet box">
    <h3 class="page-title">
        <?= $this->title; ?>
        <img src="/img/open-book.svg" class="odds-panel-trigger" style="margin-left:5px; margin-top:-3px;">
    </h3>
</div>
<?= $this->render('_viewPartials/_form', [
    'exportModel' => $exportModel,
]); ?>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            История выгрузок
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <?php Pjax::begin([
            'id' => 'export-list',
        ]) ?>
            <?= $this->render('_viewPartials/_list', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]); ?>
        <?php Pjax::end() ?>
    </div>
</div>
<div id="visible-right-menu" style="display: none;">
    <div id="visible-right-menu-wrapper"></div>
</div>

<?= $this->render('_viewPartials/export-help-panel'); ?>

<?php $this->registerJs('
$(document).ready(function(){
    $(".odds-panel-trigger").click(function(){
        $(".odds-panel").toggle("fast");
        $(this).toggleClass("active");
        $("#visible-right-menu").show();
        $("html").attr("style", "overflow: hidden;");
        return false;
    });
    $("#visible-right-menu").click(function () {
        $(".odds-panel").toggle("fast");
        $(".odds-panel-trigger").toggleClass("active");
        $(this).hide();
        $("html").removeAttr("style");
        $(".odds-panel-trigger").pulsate({
           color: "#f00",
           reach: 20,
           repeat: 3
        });
    });
    $(".odds-panel .side-panel-close").click(function () {
        $(".odds-panel").toggle("fast");
        $(".odds-panel-trigger").toggleClass("active");
        $("#visible-right-menu").hide();
        $("html").removeAttr("style");
        $(".odds-panel-trigger").pulsate({
           color: "#f00",
           reach: 20,
           repeat: 3
        });        
    });
    
    $(".odds-panel-trigger").click();
});
'); ?>