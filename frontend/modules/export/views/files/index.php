<?php
/* @var $this yii\web\View */
/* @var $templates common\models\template\Template[] */

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = 'Выгрузить прикрепленные документы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portlet box">
    <h3 class="page-title"><?= $this->title; ?></h3>
    <?= Html::a('Как это работает?', Url::to(['/help-article/view', 'id' => 38])); ?>
</div>
<?= $this->render('_viewPartials/_form', [
    'exportFilesModel' => $exportFilesModel,
]); ?>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            История выгрузок
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <?php Pjax::begin([
            'id' => 'export-list',
        ]) ?>
            <?= $this->render('_viewPartials/_list', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]); ?>
        <?php Pjax::end() ?>
    </div>
</div>
