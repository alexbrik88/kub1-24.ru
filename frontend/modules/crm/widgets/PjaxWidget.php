<?php

namespace frontend\modules\crm\widgets;

use yii\widgets\Pjax;

final class PjaxWidget extends Pjax
{
    /**
     * @inheritDoc
     */
    public function run()
    {
        if ($this->requiresPjax()) {
            $this->view->assetBundles = [];
        }

        return parent::run();
    }
}
