<?php

namespace frontend\modules\crm\widgets;

use frontend\components\WebUser;
use frontend\themes\kub\widgets\SpriteIconWidget;
use yii\bootstrap4\Html;
use yii\bootstrap4\Widget;
use yii\web\User;

final class GridButtonWidget extends Widget
{
    /**
     * @var string
     */
    public string $accessName = '';

    /**
     * @var array
     */
    public array $accessParams = [];

    /**
     * @var array
     */
    public array $fields = [];

    /**
     * @var string|null
     */
    public string $icon = '';

    /**
     * @var string
     */
    public string $label = '';

    /**
     * @var string
     */
    public string $url = '';

    /**
     * @var bool
     */
    public bool $visible = true;

    /**
     * @var User|WebUser
     */
    private User $user;

    /**
     * @param User $user
     * @param array $config
     */
    public function __construct(User $user, $config = [])
    {
        $this->user = $user;

        parent::__construct($config);
    }

    /**
     * @inheritDoc
     */
    public function run(): void
    {
        $options = $this->options;
        $content = '';

        if ($this->icon) {
            $content .= SpriteIconWidget::widget(['icon' => $this->icon]);
        }

        if ($this->label) {
            $content .= $this->icon ? Html::tag('span', $this->label, ['class' => 'ml-1']) : $this->label;
        }

        if ($this->fields) {
            $options['data-fields'] = json_encode($this->fields);
        }

        if (!$this->canAccess() && !$this->visible) {
            Html::addCssClass($options, 'invisible');
        }

        if ($this->url) {
            echo Html::a($content, $this->url, $options);
        } else {
            echo Html::button($content, $options);
        }
    }

    /**
     * @return bool
     */
    private function canAccess(): bool
    {
        if (empty($this->accessName)) {
            return true;
        }

        return $this->user->can($this->accessName, $this->accessParams);
    }
}
