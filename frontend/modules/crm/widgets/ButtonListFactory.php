<?php

namespace frontend\modules\crm\widgets;

use common\models\Contractor;
use common\models\ListInterface;
use frontend\components\WebUser;
use frontend\modules\crm\assets\AddItemAsset;
use frontend\modules\crm\models\CommonList;
use frontend\modules\crm\models\Contact;
use frontend\modules\crm\models\ContactForm;
use frontend\modules\crm\models\ListFactory;
use frontend\modules\crm\models\WebUserFactory;
use frontend\rbac\permissions\crm\ClientCheckAccess;
use frontend\rbac\permissions\crm\ClientPositionAccess;
use frontend\rbac\permissions\crm\ClientReasonAccess;
use frontend\rbac\permissions\crm\ClientTypeAccess;
use frontend\rbac\permissions\crm\ContactAccess;
use frontend\rbac\permissions\crm\ContractorActivityAccess;
use frontend\rbac\permissions\crm\ContractorCampaignAccess;
use frontend\themes\kub\helpers\Icon;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\User;
use yii\web\View;

final class ButtonListFactory // TODO: объект мешает данные и верстку
{
    /**
     * @var User|WebUser
     */
    private User $user;

    /**
     * @var ListFactory
     */
    private ListFactory $listFactory;

    /**
     * @param View $view
     * @throws
     */
    public function __construct(View $view)
    {
        $this->user = (new WebUserFactory)->createUser();
        $this->listFactory = new ListFactory();
        $view->registerAssetBundle(AddItemAsset::class);
    }

    /**
     * @return ListInterface
     */
    public function createCheckList(): ListInterface
    {
        return $this->extendList(
            $this->listFactory->createCheckList(),
            ClientCheckAccess::CREATE,
            $this->createButton('Добавить чек', ['/crm/client-check/create', 'redirect' => 0]),
        );
    }

    /**
     * @return ListInterface
     */
    public function createPositionList(): ListInterface
    {
        return $this->extendList(
            $this->listFactory->createPositionList(),
            ClientPositionAccess::CREATE,
            $this->createButton('Добавить должность', ['/crm/client-position/create', 'redirect' => 0]),
        );
    }

    /**
     * @return ListInterface
     */
    public function createReasonList(): ListInterface
    {
        return $this->extendList(
            $this->listFactory->createReasonList(),
            ClientReasonAccess::CREATE,
            $this->createButton('Добавить тип обращения', ['/crm/client-reason/create', 'redirect' => 0]),
        );
    }

    /**
     * @return ListInterface
     */
    public function createCampaignList(): ListInterface
    {
        return $this->extendList(
            $this->listFactory->createCampaignList(),
            ContractorCampaignAccess::CREATE,
            $this->createButton('Добавить источник', ['/crm/contractor-campaign/create', 'redirect' => 0]),
        );
    }

    /**
     * @return ListInterface
     */
    public function createActivityList(): ListInterface
    {
        return $this->extendList(
            $this->listFactory->createActivityList(),
            ContractorActivityAccess::CREATE,
            $this->createButton('Добавить вид деятельности', ['/crm/contractor-activity/create', 'redirect' => 0]),
        );
    }

    /**
     * @return ListInterface
     */
    public function createClientTypeList(): ListInterface
    {
        return $this->extendList(
            $this->listFactory->createClientTypeList(),
            ClientTypeAccess::CREATE,
            $this->createButton('Добавить тип отношений', ['/crm/client-type/create', 'redirect' => 0]),
        );
    }

    /**
     * @param Contractor $contractor
     * @return ListInterface
     */
    public function createContactList(Contractor $contractor): ListInterface
    {
        $items = array_map(fn (Contact $contact): string => $contact->contact_name, $this->getAllContacts($contractor));
        $count = count($items); // Получить количество
        $items = array_filter($items, 'strlen'); // Отфильтровать пустые

        if (!$this->user->can(ContactAccess::EDIT, ['contractor' => $contractor])) {
            return new CommonList($items);
        }

        if ($count >= ContactForm::CONTACT_COUNT + 1) {
            return new CommonList($items);
        }

        if ($count > 1 && $contractor->face_type == Contractor::TYPE_PHYSICAL_PERSON) {
            return new CommonList($items);
        }

        $button = $this->createButton('Добавить контакт', ['/crm/contact/create', 'contractor_id' => $contractor->id]);

        return new CommonList(['add-item-button' => $button] + $items);
    }

    /**
     * @param Contractor $contractor
     * @return Contact[]
     */
    private function getAllContacts(Contractor $contractor): array
    {
        /** @var Contact[] $contacts */
        $contacts = $contractor
            ->getContacts()
            ->orderBy(['contact_default' => SORT_DESC, 'contact_name' => SORT_ASC])
            ->indexBy('contact_id')
            ->all();

        foreach ($contacts as $contact) {
            if ($contact->contact_default) {
                // Добавить контакт в конец списка
                return $contacts + [Contact::DEFAULT_CONTACT_ID => Contact::createDefaultContact($contractor)];
            }
        }
        // Добавить контакт в начало списка
        return [Contact::DEFAULT_CONTACT_ID => Contact::createDefaultContact($contractor)] + $contacts;
    }

    /**
     * @param ListInterface $list
     * @param string $permissionName
     * @param string $button
     * @return ListInterface
     */
    private function extendList(ListInterface $list, string $permissionName, string $button): ListInterface
    {
        if (!$this->user->can($permissionName)) {
            return $list;
        }

        $items = ['add-item-button' => $button] + $list->getItems();

        return new CommonList($items);
    }

    /**
     * @param string $label
     * @param array $route
     * @return string
     */
    private function createButton(string $label, array $route): string
    {
        $icon = Html::tag('span', Icon::PLUS, ['class' => 'mr-1']);

        return Html::tag('span', sprintf('%s %s', $icon, $label), [
            'class' => 'font-weight-bold add-item-button',
            'data-url' => Url::to($route),
        ]);
    }
}
