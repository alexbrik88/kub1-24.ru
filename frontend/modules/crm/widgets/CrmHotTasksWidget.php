<?php

namespace frontend\modules\crm\widgets;

use common\models\Company;
use common\models\employee\Employee;
use frontend\modules\crm\models\Task;
use frontend\modules\crm\models\TaskFilter;
use frontend\rbac\permissions\crm\TaskAccess;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\web\User;

final class CrmHotTasksWidget extends \yii\base\Widget
{
    public User $user;
    public ?Employee $employee = null;
    public ?Company $company = null;

    private static $data = [];

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();

        $this->user = \Yii::$app->user;
        $this->employee = $this->user->identity;
        $this->company = $this->employee ? $this->employee->company : null;
    }

    public function run() : string
    {
        if (!isset($this->company, $this->employee)) {
            return '';
        }

        return $this->render('srm_hot_tasks', [
            'dataProvider' => $this->dataProvider(),
        ]);
    }

    private function dataProvider() : ActiveDataProvider
    {
        if (!isset(self::$data['dataProvider'])) {
            $query = $this->query()->andWhere([
                'not',
                ['status' => Task::STATUS_COMPLETED],
            ])->andWhere([
                '<',
                'datetime',
                date_create('tomorrow')->format('Y-m-d H:i:s'),
            ]);

            self::$data['dataProvider'] = new ActiveDataProvider([
                'query' => $query,
            ]);
        }

        return self::$data['dataProvider'];
    }

    private function query() : ActiveQuery
    {
        $owner = $this->user->can(TaskAccess::VIEW_ALL) ? null : $this->employee;
        $filter = new TaskFilter($this->company, $owner);

        return $filter->createQuery();
    }
}
