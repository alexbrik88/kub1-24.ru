<?php

use frontend\components\Icon;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $data array */
/* @var $canViewAll bool */
/* @var $taskEmployeeId int */
/* @var $employeeFilterItems array */

$this->registerCss(<<<CSS
.task_statistics_wrap {
    color: #9198a0;
    font-size: 14px;
    font-weight: 700;
}
.task_item_wrap {
    border: 1px solid #dee2e6 !important;
    border-radius: 8px;
    padding: 10px;
}
.task_item_wrap hr {
    border-top: 2px solid;
    margin: 10px 0;
}
.task_item_wrap hr.bc-r {
    border-color: #e30611;
}
.task_item_wrap hr.bc-g {
    border-color: #00c19b;
}
.task_item_wrap hr.bc-y {
    border-color: #fac031;
}
.task_item_wrap .txt-r {
    color: #e30611;
}
.task_item_wrap .txt-g {
    color: #00c19b;
}
.task_item_wrap .txt-y {
    color: #fac031;
}
.task_item_wrap .txt-b {
    color: #5bb9ef;
}
.mt-5p {
    margin-top: 5px;
}
.task_list_wrap .task_item_wrap {
    cursor: pointer;
}
.task_list_wrap .task_item_wrap:hover {
    background-color: #f2f3f7;
}
.mt-12p {
    margin-top: 12px;
}
CSS
);
?>

<?php if ($canViewAll) : ?>
    <div class="row">
        <div class="col-3 offset-9" style="margin-top: -54px;">
            <?= Select2::widget([
                'id' => 'tasks_dashboard_employee',
                'name' => 'responsible_employee_id',
                'value' => $taskEmployeeId,
                'data' => $employeeFilterItems,
                'hideSearch' => true,
                'pluginOptions' => [
                    'width' => '100%',
                ],
                'options' => [
                    'onchange' => '$.pjax.reload({container: "#pjaxContent2", data: {task_employee_id: this.value}});',
                ],
            ]) ?>
        </div>
    </div>
<?php endif ?>

<div class="wrap wrap_count" style="margin-bottom: 10px;">
    <div class="row task_statistics_wrap">
        <?php foreach ($data as $col => $colData) : ?>
            <div class="col-4">
                <div class="task_item_wrap">
                    <div class="">
                        <?= $colData['label'] ?>
                    </div>
                    <hr class="<?= $colData['lineClass'] ?>">
                    <div class="">
                        Количество задач:
                        <?= count($colData['items']) ?>
                    </div>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>

<div class="wrap wrap_count">
    <div class="row task_list_wrap">
        <?php foreach ($data as $col => $colData) : ?>
            <div class="col-4">
                <?php foreach ($colData['items'] as $key => $item) : ?>
                    <?= Html::beginTag('div', [
                        'class' => 'task_item_wrap ajax-form-button'.($key ? ' mt-12p' : ''),
                        'data-url' => Url::to(['task/update', 'task_id' => $item['task']->task_id]),
                        'data-title' => 'Редактировать задачу № ' . $item['task']->task_number,
                    ]) ?>
                        <div class="">
                            <span class="<?= $colData['dateClass'] ?>">
                                <?= $item['date'] ?>
                            </span>
                            <?= Html::encode($item['fio']) ?>
                        </div>
                        <div class="mt-5p d-flex flex-wrap">
                            <?php if ($item['contact']) : ?>
                                <div class="mr-2"><?= Html::encode($item['contact']) ?>,</div>
                            <?php endif ?>
                            <div><?= Html::encode($item['client']) ?></div>
                        </div>
                        <div class="mt-5p d-flex align-items-center">
                            <?php if ($item['icon']) : ?>
                                <div class="mr-1 txt-b">
                                    <?= Icon::get($item['icon']) ?>
                                </div>
                            <?php endif ?>
                            <div class="font-weight-bold mr-1 text-nowrap">
                                <?= Html::encode($item['event']) ?>:
                            </div>
                            <div class="text-truncate">
                                <?= Html::encode($item['task']->description) ?>
                            </div>
                        </div>
                    <?= Html::endTag('div') ?>
                <?php endforeach ?>
            </div>
        <?php endforeach ?>
    </div>
</div>
