<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$count = $dataProvider->getTotalCount();

if ($count > 0) {
    echo Html::tag('span', $count, [
        'class' => 'btn',
        'style' => '
            display: inline-block;
            height: 19px;
            min-width: 19px;
            padding: 1px 0 0;
            border: 3px solid #4679AE;
            border-radius: 5px;
            background-color: #4679AE;
            font-weight: 300;
            font-size: 11px !important;
            text-shadow: none !important;
            text-align: center;
            white-space: nowrap;
            color: #fff;
            line-height: 1;
        ',
    ]);
}
