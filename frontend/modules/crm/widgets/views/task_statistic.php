<?php

use frontend\components\Icon;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $data array */
/* @var $canViewAll bool */
/* @var $taskEmployeeId int */
/* @var $employeeFilterItems array */

$this->registerCss(<<<CSS
.task_statistics_wrap {
    color: #9198a0;
    font-size: 14px;
    font-weight: 700;
}
.task_item_wrap {
    border: 1px solid #dee2e6 !important;
    border-radius: 8px;
    padding: 10px;
}
.task_item_wrap hr {
    border-top: 2px solid;
    margin: 10px 0;
}
.task_item_wrap hr.bc-r {
    border-color: #e30611;
}
.task_item_wrap hr.bc-g {
    border-color: #00c19b;
}
.task_item_wrap hr.bc-y {
    border-color: #fac031;
}
.task_item_wrap .txt-r {
    color: #e30611;
}
.task_item_wrap .txt-g {
    color: #00c19b;
}
.task_item_wrap .txt-y {
    color: #fac031;
}
.task_item_wrap .txt-b {
    color: #5bb9ef;
}
.mt-5p {
    margin-top: 5px;
}
.task_list_wrap .task_item_wrap {
    cursor: pointer;
}
.task_list_wrap .task_item_wrap:hover {
    background-color: #f2f3f7;
}
.mt-12p {
    margin-top: 12px;
}
CSS
);
?>

<div class="wrap wrap_count" style="margin-bottom: 10px;">
    <div class="row task_statistics_wrap">
        <?php foreach ($data as $col => $colData) : ?>
            <div class="col-4">
                <div class="task_item_wrap">
                    <div class="">
                        <?= $colData['label'] ?>
                    </div>
                    <hr class="<?= $colData['lineClass'] ?>">
                    <div class="">
                        Количество задач:
                        <?= $colData['count'] ?>
                    </div>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>
