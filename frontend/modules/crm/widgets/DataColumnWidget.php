<?php

namespace frontend\modules\crm\widgets;

use common\components\grid\KubDataColumn;
use Closure;
use frontend\components\WebUser;
use yii\base\InvalidConfigException;
use yii\bootstrap4\Html;
use yii\web\User;

final class DataColumnWidget extends KubDataColumn
{
    /**
     * @var string
     */
    public $configAttribute;

    /**
     * @var string
     */
    public $hiddenClass = 'hidden';

    /**
     * @var string
     */
    private $addClass = '';

    /**
     * @var WebUser
     */
    private $user;

    /**
     * @param WebUser $user
     * @param array $config
     */
    public function __construct(User $user, $config = [])
    {
        $this->user = $user;

        parent::__construct($config);
    }

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!$this->user->identity) {
            throw new InvalidConfigException();
        }

        if ($this->configAttribute) {
            $this->addClass = $this->user->identity->config->getAttribute($this->configAttribute)
                ? 'col_' . $this->configAttribute
                : 'col_' . $this->configAttribute . ' ' . $this->hiddenClass;

            Html::addCssClass($this->headerOptions, $this->addClass);
        }
    }

    /**
     * @inheritDoc
     */
    public function renderDataCell($model, $key, $index)
    {
        if ($this->contentOptions instanceof Closure) {
            $options = call_user_func($this->contentOptions, $model, $key, $index, $this);
        } else {
            $options = $this->contentOptions;
        }

        Html::addCssClass($options, $this->addClass);

        if (empty($options['title'])) {
            unset($options['title']);
        }

        return Html::tag('td', $this->renderDataCellContent($model, $key, $index), $options);
    }
}
