<?php

namespace frontend\modules\crm\widgets;

use Yii;
use common\models\Company;
use common\models\employee\Employee;
use frontend\modules\crm\models\Task;
use frontend\modules\crm\models\TaskEventList;
use frontend\modules\crm\models\TaskFilter;
use frontend\rbac\permissions\crm\TaskAccess;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\web\User;

final class TasksStatisticWidget extends Widget
{
    /**
     * @var yii\db\ActiveQuery
     */
    private $_query;

    private $_data;

    /**
     * @inheritDoc
     */
    public function run()
    {
        $data = [
            'col1' => [
                'expired' => true,
                'lineClass' => 'bc-r',
                'dateClass' => 'txt-r',
                'label' => 'ПРОСРОЧЕННЫЕ ЗАДАЧИ',
                'count' => $this->data[Task::STATUS_EXPIRED] ?? 0,
            ],
            'col2' => [
                'expired' => false,
                'lineClass' => 'bc-g',
                'dateClass' => 'txt-g',
                'label' => 'ЗАДАЧИ В РАБОТЕ',
                'count' => $this->data[Task::STATUS_IN_PROGRESS] ?? 0,
            ],
            'col3' => [
                'expired' => false,
                'lineClass' => 'bc-y',
                'dateClass' => '',
                'label' => 'ЗАВЕРШЕННЫЕ ЗАДАЧИ',
                'count' => $this->data[Task::STATUS_COMPLETED] ?? 0,
            ],
        ];

        return $this->render('task_statistic', [
            'data' => $data,
        ]);
    }

    public function setQuery($query)
    {
        $this->_query = $query;
    }

    public function getQuery()
    {
        return clone $this->_query;
    }

    public function getData()
    {
        if (!$this->_data) {
            $this->_data = $this->query->select([
                'count' => 'COUNT(*)',
                'status',
            ])->groupBy('status')->indexBy('status')->column();
        }
        return $this->_data;
    }
}
