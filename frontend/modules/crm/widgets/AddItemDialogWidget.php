<?php

namespace frontend\modules\crm\widgets;

use frontend\modules\crm\assets\AddItemAsset;
use frontend\themes\kub\widgets\SpriteIconWidget;
use yii\base\InvalidConfigException;
use yii\bootstrap4\Modal;
use yii\widgets\Pjax;

final class AddItemDialogWidget extends Modal
{
    /**
     * @inheritDoc
     */
    public $options = [
        'id' => 'add-item-widget',
    ];

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        $this->view->registerAssetBundle(AddItemAsset::class);

        if ($this->closeButton === []) {
            $this->closeButton = [
                'label' => SpriteIconWidget::widget(['icon' => 'close']),
                'class' => 'modal-close close',
            ];
        }

        parent::init();

        Pjax::begin(['id' => 'add-item-pjax', 'enablePushState' => false, 'linkSelector' => false]);
        Pjax::end();
    }
}
