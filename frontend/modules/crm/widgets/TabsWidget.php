<?php

namespace frontend\modules\crm\widgets;

use yii\bootstrap4\Tabs;

/**
 * @deprecated
 */
final class TabsWidget extends Tabs
{
    /**
     * @var bool
     */
    public $renderTabContent = false;

    /**
     * @inheritDoc
     */
    protected function hasActiveTab()
    {
        return true;
    }
}
