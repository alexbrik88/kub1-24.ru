<?php

namespace frontend\modules\crm\widgets;

use frontend\modules\crm\assets\AddClientAsset;
use frontend\themes\kub\widgets\SpriteIconWidget;
use yii\base\InvalidConfigException;
use yii\bootstrap4\Modal;
use yii\widgets\Pjax;

final class AddClientDialogWidget extends Modal
{
    /**
     * @inheritDoc
     */
    public $options = [
        'id' => 'add-client-widget',
        'style' => 'z-index: 99990',
    ];

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        $this->view->registerAssetBundle(AddClientAsset::class);

        if ($this->closeButton === []) {
            $this->closeButton = [
                'label' => SpriteIconWidget::widget(['icon' => 'close']),
                'class' => 'modal-close close',
            ];
        }

        parent::init();

        Pjax::begin(['id' => 'add-client-pjax', 'enablePushState' => false, 'linkSelector' => false]);
        Pjax::end();
    }
}
