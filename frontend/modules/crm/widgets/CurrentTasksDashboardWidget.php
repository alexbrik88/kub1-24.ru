<?php

namespace frontend\modules\crm\widgets;

use Yii;
use common\models\Company;
use common\models\employee\Employee;
use frontend\modules\crm\models\Task;
use frontend\modules\crm\models\TaskEventList;
use frontend\modules\crm\models\TaskFilter;
use frontend\rbac\permissions\crm\TaskAccess;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\web\User;

final class CurrentTasksDashboardWidget extends Widget
{
    public User $user;
    public Company $company;
    public Employee $employee;

    private $canViewAll = false;
    private $taskEmployeeId;

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();

        $this->user = \Yii::$app->user;
        $this->employee = $this->user->identity;
        $this->company = $this->employee->company;

        $this->canViewAll = $this->user->can(TaskAccess::VIEW_ALL);

        if ($this->canViewAll) {
            $this->taskEmployeeId = Yii::$app->request->get('task_employee_id');
            if (isset($this->taskEmployeeId)) {
                $lastId = (int) $this->taskEmployeeId;
                $this->employee->config->updateAttributes([
                    'crm_task_last_employee_id' => max(0, min(2147483647, $lastId)) ?: null,
                ]);
            } else {
                $lastId = $this->employee->config->crm_task_last_employee_id;
                $isExists = $this->company->getEmployeeCompanies()->andWhere([
                    'employee_id' => $lastId,
                    'is_working' => true,
                ])->exists();

                $this->taskEmployeeId = $isExists ? $lastId : null;
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function run()
    {
        $data = [
            'col1' => [
                'expired' => true,
                'lineClass' => 'bc-r',
                'dateClass' => 'txt-r',
                'label' => 'ПРОСРОЧЕННЫЕ ЗАДАЧИ',
                'items' => $this->expiredItems(),
            ],
            'col2' => [
                'expired' => false,
                'lineClass' => 'bc-g',
                'dateClass' => 'txt-g',
                'label' => 'ЗАДАЧИ на СЕГОДНЯ',
                'items' => $this->todayItems(),
            ],
            'col3' => [
                'expired' => false,
                'lineClass' => 'bc-y',
                'dateClass' => '',
                'label' => 'ЗАДАЧИ на ЗАВТРА',
                'items' => $this->tomorrowItems(),
            ],
        ];

        if (empty($data['col1']['items'])) {
            $data['col1']['expired'] = false;
            $data['col1']['dateClass'] = '';
            $data['col1']['label'] = 'Завершенные ЗАДАЧИ';
            $data['col1']['items'] = $this->completedItems();
        }

        return $this->render('current_tasks_dashboard', [
            'data' => $data,
            'canViewAll' => $this->canViewAll,
            'taskEmployeeId' => $this->taskEmployeeId,
            'employeeFilterItems' => $this->canViewAll ? $this->employeeFilterItems() : [],
        ]);
    }

    private function query()
    {
        $owner = $this->canViewAll ? null : $this->employee;
        $filter = new TaskFilter($this->company, $owner);
        $query = $filter->createQuery();
        if ($this->canViewAll) {
            $query->andFilterWhere([
                'employee_id' => $this->taskEmployeeId,
            ]);
        }

        return $query;
    }

    private function employeeFilterItems() : array
    {
        $employeeId = array_unique(
            array_filter(
                array_merge(
                    $this->query()->select('employee_id')->distinct()->column(),
                    (array) $this->taskEmployeeId
                )
            )
        );

        $employeeCompanyArray = $this->company->getEmployeeCompanies()->andWhere([
            'employee_id' => $employeeId,
        ])->orderBy([
            'lastname' => SORT_ASC,
            'firstname' => SORT_ASC,
            'patronymic' => SORT_ASC,
        ])->all();

        return ['' => 'Все'] + ArrayHelper::map($employeeCompanyArray, 'employee_id', 'shortFio');
    }

    private function expiredItems()
    {
        $taskArray = $this->query()->andWhere([
            'not',
            ['status' => Task::STATUS_COMPLETED],
        ])->andWhere([
            '<',
            'datetime',
            date('Y-m-d H:i:s'),
        ])->orderBy([
            'datetime' => SORT_DESC,
        ])->all();

        return $this->parseItems($taskArray, function ($model) {
            return ($d = date_create($model->datetime)) ? $d->format('d.m.Y H:i') : '';
        });
    }

    private function completedItems()
    {
        $taskArray = $this->query()->andWhere([
            'status' => Task::STATUS_COMPLETED,
        ])->andWhere([
            'between',
            'completed_at',
            date('Y-m-d 00:00:00'),
            date('Y-m-d 23:59:59'),
        ])->orderBy([
            'completed_at' => SORT_DESC,
        ])->all();

        return $this->parseItems($taskArray, function ($model) {
            return ($d = date_create($model->completed_at)) ? $d->format('d.m.Y H:i') : '';
        });
    }

    private function todayItems()
    {
        $taskArray = $this->query()->andWhere([
            'status' => Task::STATUS_IN_PROGRESS,
        ])->andWhere([
            'between',
            'datetime',
            date('Y-m-d H:i:s'),
            date('Y-m-d 23:59:59'),
        ])->orderBy([
            'datetime' => SORT_ASC,
        ])->all();

        return $this->parseItems($taskArray, function ($model) {
            return 'Сегодня, '.(($d = date_create($model->datetime)) ? $d->format('H:i') : '');
        });
    }

    private function tomorrowItems()
    {
        $date = date_create('+1 day');
        $taskArray = $this->query()->andWhere([
            'status' => Task::STATUS_IN_PROGRESS,
        ])->andWhere([
            'between',
            'datetime',
            $date->format('Y-m-d 00:00:00'),
            $date->format('Y-m-d 23:59:59'),
        ])->orderBy([
            'datetime' => SORT_ASC,
        ])->all();

        return $this->parseItems($taskArray, function ($model) {
            return 'Завтра, '.(($d = date_create($model->datetime)) ? $d->format('d.m.Y H:i') : '');
        });
    }

    private function parseItems($taskArray, $date) : array
    {
        $items = [];
        foreach ($taskArray as $key => $task) {
            $items[] = [
                'date' => call_user_func($date, $task),
                'fio' => $task->employeeCompany->getShortFio(),
                'contact' => $task->contactData['name'] ?? null,
                'client' => $task->contractor->getShortTitle(),
                'event' => (new TaskEventList)->getItems()[$task->event_type] ?? '---',
                'icon' => TaskEventList::getIcons()[$task->event_type] ?? null,
                'task' => $task,
            ];
        }

        return $items;
    }
}
