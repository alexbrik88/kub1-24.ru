<?php

namespace frontend\modules\crm\widgets;

use frontend\modules\crm\assets\AjaxFormAsset;
use frontend\themes\kub\widgets\SpriteIconWidget;
use yii\bootstrap4\Modal;

/**
 * @deprecated
 */
final class AjaxFormDialog extends Modal
{
    /** @var string */
    private const WIDGET_ID = 'ajaxFormDialog';

    /**
     * @var bool
     */
    private static $executed = false;

    /**
     * @var string
     */
    public $title = 'Undefined';

    /**
     * @var string[]
     */
    public $options = ['style' => 'z-index: 99990'];

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->options['id'] = self::WIDGET_ID;

        if ($this->closeButton === []) {
            $this->closeButton = [
                'label' => SpriteIconWidget::widget(['icon' => 'close']),
                'class' => 'modal-close close',
            ];
        }

        AjaxFormAsset::register($this->view);
        parent::init();
    }

    /**
     * @inheritDoc
     */
    public static function widget($config = [])
    {
        if (self::$executed) {
            return ''; // Может существовать на странице в единственном экземпляре
        }

        self::$executed = true;

        return parent::widget($config);
    }
}
