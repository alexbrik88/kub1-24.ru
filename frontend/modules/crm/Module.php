<?php

namespace frontend\modules\crm;

use common\models\employee\Employee;
use frontend\modules\crm\behaviors\InstallTypesBehavior;
use Yii;
use yii\base\Module as BaseModule;
use yii\filters\AccessControl;

final class Module extends BaseModule
{
    /**
     * @inheritDoc
     */
    public $controllerNamespace = 'frontend\modules\crm\controllers';

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'accessControl' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => [self::class, 'isEnabled'],
                    ],
                ],
            ],
            'installTypes' => [
                'class' => InstallTypesBehavior::class,
                'events' => [self::EVENT_BEFORE_ACTION],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        \common\components\SyncTimeZone::sync();
    }

    /**
     * @return bool
     */
    public static function isEnabled(): bool
    {
        $params = Yii::$app->params;

        if (!isset($params['crm']['allowCompanies']) || empty($params['crm']['allowCompanies'])) {
            return true;
        }

        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;

        if ($employee && in_array($employee->company->id, $params['crm']['allowCompanies'])) {
            return true;
        }

        return false;
    }
}
