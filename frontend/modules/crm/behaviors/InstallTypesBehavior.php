<?php

namespace frontend\modules\crm\behaviors;

use frontend\modules\crm\models\ContractorActivity;
use frontend\modules\crm\models\ContractorCampaign;
use frontend\modules\crm\models\ClientCheck;
use frontend\modules\crm\models\ClientPosition;
use frontend\modules\crm\models\ClientReason;
use frontend\modules\crm\models\ClientStatus;
use frontend\modules\crm\models\ClientType;
use frontend\modules\crm\models\AbstractType;
use frontend\modules\crm\models\WebUserFactory;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\Exception;

final class InstallTypesBehavior extends Behavior
{
    /** @var string[] */
    private const TYPES_LIST = [
        ContractorActivity::class,
        ContractorCampaign::class,
        ClientCheck::class,
        ClientPosition::class,
        ClientReason::class,
        ClientStatus::class,
        ClientType::class,
    ];

    /**
     * @var string[]
     */
    public $events;

    /**
     * @inheritDoc
     */
    public function events()
    {
        return array_fill_keys($this->events,  [$this, 'installTypes']);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function installTypes(): void
    {
        $company = (new WebUserFactory)->createUser()->identity->company;

        if (ClientStatus::find()->where(['company_id' => $company->id])->exists()) {
            return;
        }

        /** @var AbstractType|ActiveRecord $modelClass */
        foreach (self::TYPES_LIST as $modelClass) {
            $rows = $modelClass::getDefaultRows($company->id);

            if ($rows) {
                $db = $modelClass::getDb();
                $sql = $db->createCommand()
                    ->batchInsert($modelClass::tableName(), array_keys($rows[0]), $rows)
                    ->getRawSql();

                $db->createCommand(str_replace('INSERT ', 'INSERT IGNORE ', $sql))->execute();
            }
        }
    }
}
