<?php

namespace frontend\modules\crm\behaviors;

use frontend\modules\crm\models\Deal;
use yii\base\Behavior;
use yii\db\Expression;

/**
 * @property-read Deal $owner
 */
final class DealStagedAtBehavior extends Behavior
{
    /**
     * @inheritDoc
     */
    public function events(): array
    {
        return [
            Deal::EVENT_BEFORE_INSERT => [$this, 'beforeSave'],
            Deal::EVENT_BEFORE_UPDATE => [$this, 'beforeSave'],
        ];
    }

    /**
     * @return void
     */
    public function beforeSave(): void
    {
        $newValue = (string) $this->owner->getAttribute('deal_stage_id');
        $oldValue = (string) $this->owner->getOldAttribute('deal_stage_id');

        if ($newValue !== $oldValue) {
            $this->owner->staged_at = new Expression('NOW()');
            $this->owner->markAttributeDirty('staged_at');
        }
    }
}
