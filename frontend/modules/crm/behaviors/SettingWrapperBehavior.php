<?php

namespace frontend\modules\crm\behaviors;

use frontend\controllers\WebTrait;
use yii\base\ActionEvent;
use yii\base\Behavior;
use yii\web\Controller;
use yii\web\Response;

/**
 * @property-read Controller $owner
 */
final class SettingWrapperBehavior extends Behavior
{
    use WebTrait;

    /**
     * @var array
     */
    public array $actions = ['index'];

    /**
     * @var string
     */
    public string $viewPath = '@frontend/modules/crm/views/settings/widgets/wrapper';

    /**
     * @inheritDoc
     */
    public function events(): array
    {
        return [
            Controller::EVENT_BEFORE_ACTION => [$this, 'beforeAction'],
            Controller::EVENT_AFTER_ACTION => [$this, 'afterAction'],
        ];
    }

    /**
     * @param ActionEvent $event
     * @return void
     */
    public function beforeAction(ActionEvent $event): void
    {
        if (empty($this->actions) || in_array($event->action->id, $this->actions)) {
            $this->owner->layout = false;
        }
    }

    /**
     * @param ActionEvent $event
     * @return void
     */
    public function afterAction(ActionEvent $event): void
    {
        if (empty($this->actions) || in_array($event->action->id, $this->actions)) {
            /** @var Response $response */
            $response = $event->result;
            $this->owner->layout = null;
            $response->content = $this->owner->render($this->viewPath, [
                'content' => $response->content,
                'user' => $this->user,
                'activeTab' => $this->owner->id,
            ]);
        }
    }
}
