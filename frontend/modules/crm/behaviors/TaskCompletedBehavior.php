<?php

namespace frontend\modules\crm\behaviors;

use frontend\modules\crm\models\Task;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * @property-read Task $owner
 */
final class TaskCompletedBehavior extends Behavior
{
    /**
     * @inheritDoc
     */
    public function events(): array
    {
        return [
            ActiveRecord::EVENT_BEFORE_UPDATE => [$this, 'beforeUpdate'],
        ];
    }

    /**
     * @return void
     */
    public function beforeUpdate($event): void
    {
        $model = $event->sender;
        if ($model->isAttributeChanged('status')) {
            $model->completed_at = $model->status == Task::STATUS_COMPLETED ? new Expression('NOW()') : null;
        }
    }
}
