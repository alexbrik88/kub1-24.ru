<?php

namespace frontend\modules\crm\behaviors;

use common\models\Contractor;
use common\models\document\Invoice;
use yii\base\Behavior;

/**
 * Потенциальный клиент (из ЦРМ) становится покупателем после создания счёта
 *
 * @property-read Invoice $owner
 */
final class PotentialClientBehavior extends Behavior
{
    /**
     * @inheritDoc
     */
    public function events(): array
    {
        return [
            Invoice::EVENT_AFTER_INSERT => [$this, 'afterInsert'],
        ];
    }

    /**
     * @return void
     */
    public function afterInsert(): void
    {
        $contractor = $this->owner->contractor;

        if ($contractor && $contractor->type == Contractor::TYPE_POTENTIAL_CLIENT && $contractor->client) {
            $contractor->type = Contractor::TYPE_CUSTOMER;
            $contractor->is_customer = true;
            $contractor->save(false);
        }
    }
}
