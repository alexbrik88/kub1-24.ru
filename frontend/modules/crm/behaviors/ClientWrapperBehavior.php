<?php

namespace frontend\modules\crm\behaviors;

use Yii;
use common\models\Contractor;
use frontend\components\PageSize;
use frontend\controllers\WebTrait;
use frontend\modules\crm\models\ClientForm;
use frontend\modules\crm\models\ContractorFilter;
use frontend\modules\crm\models\ContractorSelectForm;
use frontend\rbac\permissions\crm\ClientAccess;
use yii\base\ActionEvent;
use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\web\Controller;
use yii\web\Response;

/**
 * @property-read Controller $owner
 */
final class ClientWrapperBehavior extends Behavior
{
    use WebTrait;

    /**
     * @var Contractor|null
     */
    public ?Contractor $contractor = null;

    /**
     * @var string
     */
    public string $viewPath = '@frontend/modules/crm/views/client/widgets/wrapper';

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        if ($this->contractor === null) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function events(): array
    {
        return [
            Controller::EVENT_BEFORE_ACTION => [$this, 'beforeAction'],
            Controller::EVENT_AFTER_ACTION => [$this, 'afterAction'],
        ];
    }

    /**
     * @param ActionEvent $event
     * @return void
     */
    public function beforeAction(ActionEvent $event): void
    {
        $user = Yii::$app->user;
        $employee = $user->identity;
        $company = $employee->company;

        $this->owner->layout = false;
        $clientForm = new ClientForm($this->contractor);

        $filter = new ContractorFilter(
            $company,
            $user->can(ClientAccess::VIEW_ALL) ? null : $employee,
            null,
            null,
            PageSize::get(),
        );

        $form = new ContractorSelectForm($company, clone $filter, $user->can(ClientAccess::EDIT_ALL));
        $form->contractor_id = (array) $this->request->get('contractor_id');

        if ($clientForm->load($this->request->post())) {
            if ($clientForm->validate()) {
                $clientForm->saveModel();
            }

            $this->response->refresh();
            $event->isValid = false;
        }
        if ($form->load($this->request->post())) {
            if ($form->validate()) {
                $form->updateModels();
            }

            $this->response->refresh();
            $event->isValid = false;
        }
    }

    /**
     * @param ActionEvent $event
     * @return void
     */
    public function afterAction(ActionEvent $event): void
    {
        $user = Yii::$app->user;
        $employee = $user->identity;
        $company = $employee->company;

        $filter = new ContractorFilter(
            $company,
            $user->can(ClientAccess::VIEW_ALL) ? null : $employee,
            null,
            null,
            PageSize::get(),
        );

        /** @var Response $response */
        $response = $event->result;
        $this->owner->layout = null;
        $response->content = $this->owner->render($this->viewPath, [
            'user' => $this->user,
            'content' => $response->content,
            'contractor' => $this->contractor,
            'clientForm' => new ClientForm($this->contractor),
            'form' => new ContractorSelectForm($company, clone $filter, $user->can(ClientAccess::EDIT_ALL)),
            'isContractor' => (bool) $this->request->get('contractor'),
        ]);
    }
}
