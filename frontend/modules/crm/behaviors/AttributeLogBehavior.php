<?php

namespace frontend\modules\crm\behaviors;

use common\models\employee\Employee;
use frontend\modules\crm\models\Log;
use frontend\modules\crm\models\Task;
use frontend\modules\crm\models\WebUserFactory;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\AfterSaveEvent;
use yii\helpers\ArrayHelper;

/**
 * @property-read ActiveRecord $owner
 */
final class AttributeLogBehavior extends Behavior
{
    /**
     * @var Employee|null
     */
    private ?Employee $employee = null;

    /**
     * @var string[]
     */
    public array $exceptAttributes = [
        'updated_at',
        'created_at',
    ];

    /**
     * @inheritDoc
     */
    public function init(): void
    {
        if ('app-frontend' == \Yii::$app->id)
            $this->employee = \Yii::$app->user->identity;
    }

    /**
     * @inheritDoc
     */
    public function events(): array
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => [$this, 'afterInsert'],
            ActiveRecord::EVENT_AFTER_UPDATE => [$this, 'afterUpdate'],
            ActiveRecord::EVENT_AFTER_DELETE => [$this, 'afterDelete'],
        ];
    }

    /**
     * @return void
     */
    public function afterInsert(): void
    {
        if ($this->owner instanceof Task) return;

        $this->createLog(Log::LOG_TYPE_MODEL_CREATE);
    }

    /**
     * @param AfterSaveEvent $event
     * @return void
     */
    public function afterUpdate(AfterSaveEvent $event): void
    {
        $changedAttributes = $event->changedAttributes;
        if ($this->owner instanceof Task) {
            if (isset($changedAttributes['status']) && $changedAttributes['status'] == Task::STATUS_EMPTY) {
                $this->createLog(Log::LOG_TYPE_MODEL_CREATE);
                return;
            }
        }
        array_walk($changedAttributes, function (?string $oldAttributeValue, string $attributeName): void {
            $newAttributeValue = (string) $this->owner->getAttribute($attributeName);

            if (strval($oldAttributeValue) !== $newAttributeValue && !in_array($attributeName, $this->exceptAttributes)) {
                $this->createLog(Log::LOG_TYPE_MODEL_UPDATE, $attributeName, $newAttributeValue, $oldAttributeValue);
            }
        });
    }

    /**
     * @return void
     */
    public function afterDelete(): void
    {
        $this->createLog(Log::LOG_TYPE_MODEL_DELETE);
    }

    /**
     * @param int $logType
     * @return void
     * @param string|null $attributeName
     * @param mixed|null $newAttributeValue
     * @param mixed|null $oldAttributeValue
     * @return void
     */
    private function createLog(
        int $logType,
        ?string $attributeName = null,
        ?string $newAttributeValue = null,
        ?string $oldAttributeValue = null
    ): void {
        if ($logType == Log::LOG_TYPE_MODEL_CREATE || $this->employee) {
            $employeeCompanyId = ArrayHelper::getValue($this->employee, ['company', 'id']);
            if ($employeeCompanyId == $this->owner->company_id) {
                $employeeId = $this->employee->id;
            } else {
                $employeeId = null;
            }
            $log = new Log([
                'log_type' => $logType,
                'company_id' => $this->owner->company_id,
                'employee_id' => $employeeId,
                'model_id' => $this->owner->getAttribute($this->owner::primaryKey()[0]),
                'model_class' => get_class($this->owner),
                'attribute_name' => $attributeName,
                'new_attribute_value' => $newAttributeValue,
                'old_attribute_value' => $oldAttributeValue,
            ]);

            $log->save(false);
        }
    }
}
