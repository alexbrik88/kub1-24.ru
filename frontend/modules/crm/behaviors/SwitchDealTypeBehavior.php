<?php

namespace frontend\modules\crm\behaviors;

use frontend\controllers\WebTrait;
use frontend\modules\crm\models\DealType;
use yii\base\ActionEvent;
use yii\base\Behavior;
use yii\web\Controller;

/**
 * Это поведение редиректит пользователя на последний тип сделки, используемый пользователем
 * Это поведение сохраняет текущий тип сделки в настройках у пользователя
 *
 * @property-read Controller $owner
 */
final class SwitchDealTypeBehavior extends Behavior
{
    use WebTrait;

    /**
     * @var array
     */
    public array $actions = ['index'];

    /**
     * @inheritDoc
     */
    public function events(): array
    {
        return [
            Controller::EVENT_BEFORE_ACTION => [$this, 'beforeAction'],
        ];
    }

    /**
     * @param ActionEvent $event
     * @return void
     */
    public function beforeAction(ActionEvent $event): void
    {
        if ($this->isActive($event)) {
            $dealType = null;
            $employeeCompany = $this->user->identity->currentEmployeeCompany;
            $id = $this->request->get('deal_type_id');

            if ($dealType === null && strlen($id)) { // Вытянуть из запроса
                $dealType = DealType::findById($this->user->identity->company, $id);

                if (!$dealType) {
                    return; // Это 404
                }
            }

            if ($dealType === null && $employeeCompany->crm_deal_type_id) { // Вытянуть ранее сохраненный
                $dealType = DealType::findById($this->user->identity->company, $employeeCompany->crm_deal_type_id);
                $this->setRedirect($event, $dealType);
            }

            if ($dealType === null) {
                $dealType = DealType::findFirst($this->user->identity->company); // Первый активный по алфавиту
                $this->setRedirect($event, $dealType);
            }

            if ($dealType) {
                $employeeCompany->updateAttributes(['crm_deal_type_id' => $dealType->deal_type_id]);
            }
        }
    }

    /**
     * @param ActionEvent $event
     * @return bool
     */
    private function isActive(ActionEvent $event): bool
    {
        return empty($this->actions) || in_array($event->action->id, $this->actions);
    }

    /**
     * @param ActionEvent $event
     * @param DealType|null $dealType
     * @return void
     */
    private function setRedirect(ActionEvent $event, ?DealType $dealType): void
    {
        if ($dealType) {
            $event->handled = true;
            $event->result = $this->owner->redirect([
                $event->action->id,
                'deal_type_id' => $dealType->deal_type_id
            ]);
        }
    }
}
