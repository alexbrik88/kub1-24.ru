<?php

namespace frontend\modules\crm\behaviors;

use frontend\modules\crm\models\AbstractType;
use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;

/**
 * @property-read AbstractType $owner
 */
final class CanDeleteBehavior extends Behavior
{
    /**
     * @var array|null
     */
    private static array $cache = [];

    /**
     * @var string|ActiveRecord
     */
    public string $modelClass = '';

    /**
     * @var string
     */
    public string $modelAttribute = '';

    /**
     * @var bool
     */
    public bool $hasCompany = true;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        if (!is_a($this->modelClass, ActiveRecord::class, true) || empty($this->modelAttribute)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function events(): array
    {
        return [
            AbstractType::EVENT_AFTER_FIND => [$this, 'afterFind'],
        ];
    }

    /**
     * @return void
     */
    public function afterFind(): void
    {
        $this->owner->canDelete = ($this->owner->canDelete && $this->getCanDelete());
    }

    /**
     * @return bool
     */
    private function getCanDelete(): bool
    {
        $id = $this->owner->getAttribute($this->owner::getIdKey());
        $typeClass = get_class($this->owner);

        if (!isset(self::$cache[$this->modelClass])) {
            self::$cache[$this->modelClass] = [];
        }

        if (!array_key_exists($typeClass, self::$cache[$this->modelClass])) {
            $query = $this->modelClass::find()
                ->select($this->modelAttribute)
                ->groupBy($this->modelAttribute)
                ->andWhere(['not', [$this->modelAttribute => null]]);

            if ($this->hasCompany) {
                $query->andWhere(['company_id' => $this->owner->company_id]);
            }

            self::$cache[$this->modelClass][$typeClass] = $query->column();
        }

        if (in_array($id, self::$cache[$this->modelClass][$typeClass])) {
            return false;
        }

        return true;
    }
}
