<?php

namespace frontend\modules\crm\behaviors;

use frontend\modules\crm\models\Task;
use yii\base\ActionEvent;
use yii\base\Behavior;
use yii\web\Controller;

/**
 * Обновляет статусы задач перед действием контроллера
 *
 * @property-read Controller $controller
 */
final class TaskStatusBehavior extends Behavior
{
    /**
     * @var array
     */
    public array $actions = []; // По умолчанию все действия

    /**
     * @param ActionEvent $event
     * @return void
     */
    public function beforeAction(ActionEvent $event): void
    {
        if (empty($this->actions) || in_array($event->action->id, $this->actions)) {
            Task::updateStatuses();
        }
    }

    /**
     * @inheritDoc
     */
    public function events(): array
    {
        return [
            Controller::EVENT_BEFORE_ACTION => [$this, 'beforeAction'],
        ];
    }
}
