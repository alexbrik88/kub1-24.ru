<?php

namespace frontend\modules\crm\behaviors;

use common\models\Contractor;
use frontend\modules\crm\models\Client;
use frontend\modules\crm\models\Task;
use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * @property-read Task $owner
 */
final class ActualTaskBehavior extends Behavior
{
    /**
     * @inheritDoc
     */
    public function events(): array
    {
        return [
            ActiveRecord::EVENT_AFTER_UPDATE => [$this, 'afterSave'],
            ActiveRecord::EVENT_AFTER_INSERT => [$this, 'afterSave'],
        ];
    }

    /**
     * @return void
     */
    public function afterSave(): void
    {
        if ($this->owner instanceof Task && $this->owner->status == Task::STATUS_EMPTY) {
            return;
        }
        $contractor = $this->getContractor();
        $client = $contractor->client ?: Client::createInstance($contractor);
        $task = Task::findActual($contractor);
        $client->updateAttributes([
            'task_id' => $task ? $task->task_id : null,
            'event_type' => $task ? $task->event_type : null,
        ]);
    }

    /**
     * @return Contractor
     */
    private function getContractor(): Contractor
    {
        return $this->owner->contractor;
    }
}
