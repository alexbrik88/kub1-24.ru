<?php

namespace frontend\modules\crm\controllers;

use common\models\Contractor;
use frontend\controllers\WebTrait;
use yii\web\NotFoundHttpException;

trait ContractorTrait
{
    use WebTrait;

    /**
     * @var Contractor
     */
    private $_contractor;

    /**
     * @return Contractor
     * @throws NotFoundHttpException
     */
    protected function getContractor(): Contractor
    {
        if ($this->_contractor === null) {
            $this->_contractor = Contractor::findById($this->user->identity->company, $this->request->get('contractor_id'));
        }

        if ($this->_contractor === null) {
            throw new NotFoundHttpException('Клиент не найден.');
        }

        return $this->_contractor;
    }
}
