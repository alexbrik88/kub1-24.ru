<?php

namespace frontend\modules\crm\controllers;

use common\models\Contractor;
use frontend\components\PageSize;
use frontend\controllers\WebTrait;
use frontend\modules\crm\models\ContractorFilter;
use frontend\modules\crm\models\ContractorFormMutator;
use frontend\modules\crm\models\ContractorSelectForm;
use frontend\modules\crm\models\ClientForm;
use frontend\modules\crm\models\ClientImportForm;
use frontend\modules\crm\models\ContactForm;
use frontend\modules\crm\models\CreateContractorForm;
use frontend\modules\crm\models\Deal;
use frontend\modules\crm\models\DealType;
use frontend\modules\crm\services\Client\ImportFromExcelService;
use frontend\rbac\permissions\Contractor as ContractorAccess;
use frontend\rbac\permissions\crm\ClientAccess;
use frontend\rbac\permissions\crm\ContactAccess;
use yii\filters\AccessControl;
use yii\filters\AjaxFilter;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

final class ClientsController extends Controller
{
    use WebTrait;
    use ContractorTrait;

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                            'contacts',
                        ],
                        'roles' => [ClientAccess::INDEX],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'create',
                            'import',
                        ],
                        'roles' => [ClientAccess::EDIT],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update'],
                        'roles' => [ClientAccess::EDIT],
                        'roleParams' => function(): array {
                            return ['contractor' => $this->getContractor()];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => [ContractorAccess::DELETE],
                        'roleParams' => function(): array {
                            return ['model' => $this->getContractor()];
                        },
                    ],
                ],
            ],
            'verb' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['POST', 'GET'],
                    'create' => ['POST', 'GET'],
                    'update' => ['POST', 'GET'],
                    'delete' => ['POST'],
                ],
            ],
            'ajax' => [
                'class' => AjaxFilter::class,
                'only' => [
                    'create',
                ],
            ],
        ];
    }

    /**
     * @return Response
     * @throws FiltrationErrorException
     */
    public function actionIndex(): Response
    {
        \yii\helpers\Url::remember(null, 'crm.client.card');
        $filter = new ContractorFilter(
            $this->user->identity->company,
            $this->user->can(ClientAccess::VIEW_ALL) ? null : $this->user->identity,
            null, // StatisticPeriod::getDateFrom(),
            null, // StatisticPeriod::getDateTo(),
            PageSize::get(),
        );

        $form = new ContractorSelectForm($this->user->identity->company, clone $filter, $this->user->can(ClientAccess::EDIT_ALL));

        if ($form->load($this->request->post())) {
            if ($form->validate()) {
                $form->updateModels();
            }

            return $this->refresh();
        }

        if ($filter->load($this->request->get()) && !$filter->validate()) {
            throw new FiltrationErrorException($filter);
        }

        $this->response->content = $this->render('index', [
            'user' => $this->user,
            'form' => $form,
            'filter' => $filter,
        ]);

        return $this->response;
    }

    /**
     * @return Response
     */
    public function actionCreate(): Response
    {
        $post = $this->request->post();
        $dealType = DealType::findById($this->user->identity->company, $this->request->get('deal_type_id'));
        $legalForm = new CreateContractorForm($this->user->identity, Contractor::TYPE_LEGAL_PERSON, $dealType);
        $physicalForm = new CreateContractorForm($this->user->identity, Contractor::TYPE_PHYSICAL_PERSON, $dealType);
        $isRedirect = (bool) $this->request->get('redirect', true);
        $contractor = null;

        if ($legalForm->load($post) && $legalForm->validate() && $legalForm->createPerson()) {
            $contractor = $legalForm->contractorForm;
        }

        if ($physicalForm->load($post) && $physicalForm->validate() && $physicalForm->createPerson()) {
            $contractor = $physicalForm->contractorForm;
        }

        if ($contractor && $isRedirect) {
            return $this->redirect(['client/index', 'contractor_id' => $contractor->id]);
        }

        if ($contractor && !$isRedirect) {
            $this->response->content = $this->renderAjax('create-response', [
                'contractor' => $contractor,
            ]);

            return $this->response;
        }

        $this->response->content = $this->renderAjax('widgets/client-form', [
            'legalForm' => $legalForm,
            'physicalForm' => $physicalForm,
        ]);

        return $this->response;
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate(): Response
    {
        $post = $this->request->post();
        $contractor = $this->getContractor();

        if ($contractor->company_id === null) {
            throw new ForbiddenHttpException("Контрагент {$model->shortTitle} не может быть изменен.");
        }

        $contractor->setScenario('insert');

        if (!$contractor->is_seller && !$contractor->is_customer) {
            $mutator = new ContractorFormMutator($this->contractor);
            $mutator->mutateRequiredValidator($this->contractor->face_type);
            $mutator->mutateWhenClient();
        }

        $clientForm = new ClientForm($contractor);
        $contactForms = $this->user->can(ContactAccess::EDIT, ['contractor' => $contractor])
            ? ContactForm::createMultiple($contractor)
            : null;

        if ($this->request->post('ajax')) {
            $contractor->load($post);
            $this->response->format = Response::FORMAT_JSON;
            $this->response->data = ActiveForm::validate($contractor);

            return $this->response;
        }

        $isLoad = [ // Вызвать загрузку всех форм
            $contractor->load($post) && $contractor->loadAgent($post), // loadAgent() всегда TRUE?
            $clientForm->load($post),
            $contactForms && ContactForm::loadMultiple($contactForms, $post),
        ];

        if (array_sum($isLoad) > 0) {
            $isValid = [ // Вызвать проверку всех форм
                $contractor->validate(),
                $contractor->validateAgent(),
                $clientForm->validate(),
                !$contactForms || ContactForm::validateMultiple($contactForms),
            ];

            if (array_sum($isValid) == count($isValid)) {
                $contractor->save();
                $contractor->saveAgent();
                $clientForm->saveModel();

                if ($contactForms) {
                    array_walk($contactForms, function (ContactForm $contactForm) {
                        $contactForm->updateContact();
                    });
                }

                return $this->redirect(['/crm/client', 'contractor_id' => $contractor->id]);
            }
        }

        $this->response->content = $this->render('update', [
            'contractor' => $contractor,
            'clientForm' => $clientForm,
            'contactForms' => $contactForms,
        ]);

        return $this->response;
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDelete(): Response
    {
        $contractor = $this->getContractor();
        $clientForm = new ClientForm($contractor);

        if ($clientForm->removeClient()) {
            $this->session->setFlash('success', 'Клиент удалён');
        } else {
            $this->session->setFlash('error', 'Произошла ошибка при удалении клиента');
        }

        return $this->redirect(['index']);
    }

    /**
     * @return Response
     */
    public function actionImport(): Response
    {
        $model = new ClientImportForm(new ImportFromExcelService($this->user->identity));

        if ($this->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');
            $model->import();
        }

        $this->response->data = $this->renderAjax('import', [
            'model' => $model,
        ]);

        return $this->response;
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionContacts(): Response
    {
        $contractor = $this->getContractor();

        $this->response->format = Response::FORMAT_JSON;
        $this->response->data = array_values($contractor->allContactsData);

        return $this->response;
    }
}
