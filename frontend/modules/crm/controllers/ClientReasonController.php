<?php

namespace frontend\modules\crm\controllers;

use frontend\modules\crm\behaviors\SettingWrapperBehavior;
use frontend\modules\crm\models\ClientReason;
use frontend\rbac\permissions\crm\ClientReasonAccess;
use yii\filters\AccessControl;
use yii\filters\AjaxFilter;
use yii\web\Controller;

final class ClientReasonController extends Controller implements SettingControllerInterface
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                        ],
                        'roles' => [ClientReasonAccess::VIEW],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'create',
                        ],
                        'roles' => [ClientReasonAccess::CREATE],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'update',
                            'delete',
                        ],
                        'roles' => [ClientReasonAccess::EDIT],
                    ],
                ],
            ],
            'ajaxFilter' => [
                'class' => AjaxFilter::class,
                'only' => [
                    'create',
                    'update',
                    'delete',
                ],
            ],
            'settingWrapper' => [
                'class' => SettingWrapperBehavior::class,
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function actions(): array
    {
        return [
            'index' => [
                'class' => SettingIndexAction::class,
                'modelClass' => ClientReason::class,
                'createAccess' => ClientReasonAccess::CREATE,
                'editAccess' => ClientReasonAccess::EDIT,
            ],
            'create' => [
                'class' => SettingCreateAction::class,
                'modelClass' => ClientReason::class,
            ],
            'update' => [
                'class' => SettingUpdateAction::class,
                'modelClass' => ClientReason::class,
            ],
            'delete' => [
                'class' => SettingDeleteAction::class,
                'modelClass' => ClientReason::class,
            ],
        ];
    }
}
