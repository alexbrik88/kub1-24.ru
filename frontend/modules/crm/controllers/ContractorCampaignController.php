<?php

namespace frontend\modules\crm\controllers;

use frontend\modules\crm\behaviors\SettingWrapperBehavior;
use frontend\modules\crm\models\ContractorCampaign;
use frontend\rbac\permissions\crm\ContractorCampaignAccess;
use yii\filters\AccessControl;
use yii\filters\AjaxFilter;
use yii\web\Controller;

final class ContractorCampaignController extends Controller implements SettingControllerInterface
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                        ],
                        'roles' => [ContractorCampaignAccess::VIEW],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'create',
                        ],
                        'roles' => [ContractorCampaignAccess::CREATE],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'update',
                            'delete',
                        ],
                        'roles' => [ContractorCampaignAccess::EDIT],
                    ],
                ],
            ],
            'ajaxFilter' => [
                'class' => AjaxFilter::class,
                'only' => [
                    'create',
                    'update',
                    'delete',
                ],
            ],
            'settingWrapper' => [
                'class' => SettingWrapperBehavior::class,
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function actions(): array
    {
        return [
            'index' => [
                'class' => SettingIndexAction::class,
                'modelClass' => ContractorCampaign::class,
                'createAccess' => ContractorCampaignAccess::CREATE,
                'editAccess' => ContractorCampaignAccess::EDIT,
            ],
            'create' => [
                'class' => SettingCreateAction::class,
                'modelClass' => ContractorCampaign::class,
            ],
            'update' => [
                'class' => SettingUpdateAction::class,
                'modelClass' => ContractorCampaign::class,
            ],
            'delete' => [
                'class' => SettingDeleteAction::class,
                'modelClass' => ContractorCampaign::class,
            ],
        ];
    }
}
