<?php

namespace frontend\modules\crm\controllers;

use Yii;
use common\models\Contractor;
use frontend\modules\crm\models\ClientWish;
use frontend\modules\crm\models\ClientWishForm;
use frontend\modules\crm\models\ClientWishSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AjaxFilter;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * ClientWishController implements the CRUD actions for ClientWish model.
 */
class ClientWishController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'ajaxFilter' => [
                'class' => AjaxFilter::class,
                'only' => [
                    'view',
                    'create',
                    'update',
                ],
            ],
        ];
    }

    /**
     * Displays a single ClientWish model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($contractor_id, $id)
    {
        $contractor = $this->findContractor($contractor_id);

        return $this->renderAjax('view', [
            'model' => $this->findModel($contractor, $id),
        ]);
    }

    /**
     * Creates a new ClientWish model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($contractor_id)
    {
        $contractor = $this->findContractor($contractor_id);
        $wish = new ClientWish([
            'responsible_employee_id' => Yii::$app->user->id,
        ]);
        $model = new ClientWishForm($contractor, $wish, [
            'scenario' => ClientWishForm::SCENARIO_CREATE,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Yii::$app->request->referrer ?: ['client/wish', 'contractor_id' => $contractor->id]);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ClientWish model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($contractor_id, $id)
    {
        $contractor = $this->findContractor($contractor_id);
        $wish = $this->findModel($contractor, $id);
        $model = new ClientWishForm($contractor, $wish, [
            'responsible_employee_id' => Yii::$app->user->id,
            'scenario' => ClientWishForm::SCENARIO_UPDATE,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Yii::$app->request->referrer ?: ['client/wish', 'contractor_id' => $contractor->id]);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ClientWish model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionStatus($contractor_id, $id, $status_id)
    {
        $contractor = $this->findContractor($contractor_id);
        $wish = $this->findModel($contractor, $id);
        $model = new ClientWishForm($contractor, $wish, [
            'responsible_employee_id' => Yii::$app->iser->id,
            'status_id' => $status_id,
            'scenario' => ClientWishForm::SCENARIO_STATUS,
        ]);

        if ($model->save()) {
            return $this->redirect(Yii::$app->request->referrer ?: ['client/wish', 'contractor_id' => $contractor->id]);
        }

        return $this->renderAjax('status', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ClientWish model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($contractor_id, $id)
    {
        $contractor = $this->findContractor($contractor_id);
        $model = $this->findModel($contractor, $id);
        $model->delete();

        return $this->redirect(Yii::$app->request->referrer ?: ['client/wish', 'contractor_id' => $model->contractor_id]);
    }

    /**
     * Finds the Contractor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contractor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findContractor($id)
    {
        $companyId = ArrayHelper::getValue(Yii::$app, ['user', 'identity', 'company', 'id']);
        if ($companyId && ($model = Contractor::findOne(['id' => $id, 'company_id' => $companyId])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the ClientWish model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ClientWish the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(Contractor $comtractor, $id)
    {
        if (($model = ClientWish::findOne(['id' => $id, 'contractor_id' => $comtractor->id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
