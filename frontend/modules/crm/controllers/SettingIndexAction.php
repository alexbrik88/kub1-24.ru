<?php

namespace frontend\modules\crm\controllers;

use frontend\controllers\ActionInterface;
use frontend\controllers\WebTrait;
use frontend\modules\crm\models\TypeFilter;
use frontend\modules\crm\models\AbstractType;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\web\Response;

final class SettingIndexAction extends Action implements ActionInterface
{
    use WebTrait;

    /**
     * @var string|AbstractType
     */
    public string $modelClass = '';

    /**
     * @var string
     */
    public string $createAccess = '';

    /**
     * @var string
     */
    public string $editAccess = '';

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        if (!is_a($this->modelClass, AbstractType::class, true)) {
            throw new InvalidConfigException();
        }

        if (empty($this->createAccess) || empty($this->editAccess)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @return Response
     * @throws FiltrationErrorException
     */
    public function run(): Response
    {
        $filter = new TypeFilter($this->user->identity->company, $this->modelClass);

        if ($filter->load($this->request->get()) && !$filter->validate()) {
            throw new FiltrationErrorException($filter);
        }

        $this->response->content = $this->controller->render('index', [
            'filter' => $filter,
            'user' => $this->user,
            'createAccess' => $this->createAccess,
            'editAccess' => $this->editAccess,
        ]);

        return $this->response;
    }
}
