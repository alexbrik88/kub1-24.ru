<?php

namespace frontend\modules\crm\controllers;

use Yii;
use frontend\modules\crm\behaviors\SettingWrapperBehavior;
use frontend\modules\crm\models\TaskResult;
use frontend\rbac\permissions\crm\TaskAccess;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\AjaxFilter;

/**
 * TaskResultController implements the CRUD actions for TaskResult model.
 */
class TaskResultController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                        ],
                        'roles' => [TaskAccess::VIEW],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'create',
                        ],
                        'roles' => [TaskAccess::CREATE],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'update',
                            'delete',
                        ],
                        'roles' => [TaskAccess::EDIT],
                    ],
                ],
            ],
            'ajaxFilter' => [
                'class' => AjaxFilter::class,
                'only' => [
                    'create',
                    'update',
                    'delete',
                ],
            ],
            'settingWrapper' => [
                'class' => SettingWrapperBehavior::class,
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function actions(): array
    {
        return [
            'index' => [
                'class' => SettingIndexAction::class,
                'modelClass' => TaskResult::class,
                'createAccess' => TaskAccess::CREATE,
                'editAccess' => TaskAccess::EDIT,
            ],
            'create' => [
                'class' => SettingCreateAction::class,
                'modelClass' => TaskResult::class,
            ],
            'update' => [
                'class' => SettingUpdateAction::class,
                'modelClass' => TaskResult::class,
            ],
            'delete' => [
                'class' => SettingDeleteAction::class,
                'modelClass' => TaskResult::class,
            ],
        ];
    }
}
