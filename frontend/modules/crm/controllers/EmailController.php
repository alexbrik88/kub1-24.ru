<?php

namespace frontend\modules\crm\controllers;

use frontend\controllers\WebTrait;
use frontend\modules\crm\models\Client;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\rbac\permissions\crm\ClientAccess;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

final class EmailController extends Controller
{
    use WebTrait;
    use ContractorTrait;

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [], // all
                        'roles' => [ClientAccess::VIEW],
                        'roleParams' => function(): array {
                            return ['contractor' => $this->getContractor()];
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * @return Response
     * @throws
     */
    public function actionSend(): Response
    {
        $contractor = $this->getContractor();
        $client = $contractor->client ?: Client::createInstance($contractor);
        $sendForm = new InvoiceSendForm($this->user->identity->currentEmployeeCompany, [
            'model' => $client,
            'textRequired' => true,
        ]);

        if ($this->request->isAjax && $sendForm->load($this->request->post())) {
            $this->response->format = Response::FORMAT_JSON;
            $this->response->data = ActiveForm::validate($sendForm);

            return $this->response;
        }

        if ($sendForm->load($this->request->post()) && $sendForm->validate()) {
            $saved = $sendForm->send();

            if (is_array($saved)) {
                $this->session->setFlash('success', 'Отправлено ' . $saved[0] . ' из ' . $saved[1] . ' писем.');
            }
        }

        return $this->redirect(['client/index', 'contractor_id' => $contractor->id]);
    }
}
