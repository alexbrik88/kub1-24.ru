<?php

namespace frontend\modules\crm\controllers;

use frontend\modules\crm\behaviors\SettingWrapperBehavior;
use frontend\modules\crm\models\ClientCheck;
use frontend\rbac\permissions\crm\DealTypeAccess;
use yii\filters\AccessControl;
use yii\filters\AjaxFilter;
use yii\web\Controller;

final class ClientCheckController extends Controller implements SettingControllerInterface
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                        ],
                        'roles' => [DealTypeAccess::VIEW],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'create',
                        ],
                        'roles' => [DealTypeAccess::CREATE],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'update',
                            'delete',
                        ],
                        'roles' => [DealTypeAccess::EDIT],
                    ],
                ],
            ],
            'ajaxFilter' => [
                'class' => AjaxFilter::class,
                'only' => [
                    'create',
                    'update',
                    'delete',
                ],
            ],
            'settingWrapper' => [
                'class' => SettingWrapperBehavior::class,
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function actions(): array
    {
        return [
            'index' => [
                'class' => SettingIndexAction::class,
                'modelClass' => ClientCheck::class,
                'createAccess' => DealTypeAccess::CREATE,
                'editAccess' => DealTypeAccess::EDIT,
            ],
            'create' => [
                'class' => SettingCreateAction::class,
                'modelClass' => ClientCheck::class,
            ],
            'update' => [
                'class' => SettingUpdateAction::class,
                'modelClass' => ClientCheck::class,
            ],
            'delete' => [
                'class' => SettingDeleteAction::class,
                'modelClass' => ClientCheck::class,
            ],
        ];
    }
}
