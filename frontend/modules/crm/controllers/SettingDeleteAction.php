<?php

namespace frontend\modules\crm\controllers;

use frontend\controllers\ActionInterface;
use frontend\controllers\RenderResponseTrait;
use frontend\controllers\WebTrait;
use frontend\modules\crm\models\TypeForm;
use frontend\modules\crm\models\AbstractTypeForm;
use frontend\modules\crm\models\AbstractType;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use Throwable;

final class SettingDeleteAction extends Action implements ActionInterface
{
    use WebTrait;
    use RenderResponseTrait;

    /**
     * @var string|AbstractType
     */
    public string $modelClass = '';

    /**
     * @var string|AbstractTypeForm
     */
    public string $formClass = TypeForm::class;

    /**
     * @var string
     */
    public string $errorMessage = 'Невозможно удалить объект, пока он используется';

    /**
     * @var string
     */
    public string $viewPath = '@frontend/modules/crm/views/settings/delete-form';

    /**
     * @var string
     */
    public string $errorPath = '@frontend/modules/crm/views/settings/delete-error';

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        if (!is_a($this->modelClass, AbstractType::class, true)) {
            throw new InvalidConfigException();
        }

        if (!is_a($this->formClass, AbstractTypeForm::class, true)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws Throwable
     */
    public function run(): Response
    {
        $type = $this->modelClass::findById($this->user->identity->company, $this->request->get('id'));

        if (!$type) {
            throw new NotFoundHttpException();
        }

        if (!$type->canDelete) {
            return $this->renderResponse($this->errorPath, [
                'errorMessage' => $this->errorMessage,
            ]);
        }

        $form = $this->formClass::createTypeForm($type, $this->user->identity, AbstractTypeForm::SCENARIO_DELETE);

        if ($this->request->isPost && $form->validate() && $form->deleteModel()) {
            return $this->controller->redirect(['index']);
        }

        return $this->renderResponse($this->viewPath, [
            'form' => $form,
        ]);
    }
}
