<?php

namespace frontend\modules\crm\controllers;

use common\models\FilterInterface;
use yii\base\Model;
use yii\web\BadRequestHttpException;

final class FiltrationErrorException extends BadRequestHttpException
{
    /**
     * @param FilterInterface|Model $filter
     */
    public function __construct(Model $filter)
    {
        $message = implode(' ', $filter->getErrorSummary(true));

        parent::__construct($message);
    }
}
