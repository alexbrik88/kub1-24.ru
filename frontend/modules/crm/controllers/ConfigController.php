<?php

namespace frontend\modules\crm\controllers;

use frontend\controllers\WebTrait;
use frontend\modules\crm\behaviors\SettingWrapperBehavior;
use frontend\modules\crm\models\Config;
use frontend\modules\crm\models\ConfigForm;
use frontend\rbac\permissions\crm\ConfigAccess;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

final class ConfigController extends Controller implements SettingControllerInterface
{
    use WebTrait;

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                        ],
                        'roles' => [ConfigAccess::VIEW],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'create',
                            'update',
                            'delete',
                        ],
                        'roles' => [ConfigAccess::EDIT],
                    ],
                ],
            ],
            'settingWrapper' => [
                'class' => SettingWrapperBehavior::class,
                'actions' => [
                    'index',
                    'update',
                ],
            ],
        ];
    }

    /**
     * @return Response
     */
    public function actionIndex(): Response
    {
        $config = $this->getConfig();
        $this->response->content = $this->render('index', [
            'config' => $config,
        ]);

        return $this->response;
    }

    /**
     * @return Response
     */
    public function actionUpdate(): Response
    {
        $config = $this->getConfig();
        $form = new ConfigForm($config);

        if ($form->load($this->request->post()) && $form->validate() && $form->updateConfig()) {
            return $this->redirect(['index']);
        }

        $this->response->content = $this->render('update', [
            'form' => $form,
        ]);

        return $this->response;
    }

    /**
     * @return Config
     */
    private function getConfig(): Config
    {
        $company = $this->user->identity->company;

        return Config::getInstance($company) ?: Config::createInstance($company);
    }
}
