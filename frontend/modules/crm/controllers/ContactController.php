<?php

namespace frontend\modules\crm\controllers;

use common\models\Contractor;
use frontend\controllers\WebTrait;
use frontend\modules\crm\models\Contact;
use frontend\modules\crm\models\ContactForm;
use frontend\modules\crm\widgets\ButtonListFactory;
use frontend\rbac\permissions\crm\ContactAccess;
use yii\filters\AccessControl;
use yii\filters\AjaxFilter;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

final class ContactController extends Controller
{
    use WebTrait;
    use ContractorTrait;

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['dropdown'],
                        'roles' => [ContactAccess::VIEW],
                        'roleParams' => function(): array {
                            return ['contractor' => $this->getContractor()];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update'],
                        'roles' => [ContactAccess::EDIT],
                        'roleParams' => function(): array {
                            return ['contractor' => $this->getContractor()];
                        },
                    ],
                ],
            ],
            'ajaxFilter' => [
                'class' => AjaxFilter::class,
            ],
        ];
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionCreate(): Response
    {
        $contractor = $this->getContractor();
        $count = count(Contact::findByContractor($contractor));
        $contact = new Contact();
        $form = new ContactForm($contractor, $contact, ['contact_name']);

        if ($contractor->face_type == Contractor::TYPE_PHYSICAL_PERSON && $count > 0) {
            throw new ForbiddenHttpException('Достигнут лимит контактов.');
        }

        if ($contractor->face_type != Contractor::TYPE_PHYSICAL_PERSON && $count >= ContactForm::CONTACT_COUNT) {
            throw new ForbiddenHttpException('Достигнут лимит контактов.');
        }

        if ($form->load($this->request->post()) && $form->validate() && $form->updateContact()) {
            $this->response->content = $this->renderAjax('edit-response', [
                'contact' => $contact,
            ]);
        } else {
            $this->response->content = $this->renderAjax('widgets/contact-form', [
                'form' => $form,
                'contractor' => $contractor,
            ]);
        }

        return $this->response;
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate(): Response
    {
        $contractor = $this->getContractor();
        $contact = Contact::findById($contractor, $this->request->get('contact_id'));

        if ($contact === null) {
            throw new NotFoundHttpException();
        }

        return $this->response;
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDropdown(): Response
    {
        $contractor = $this->getContractor();
        $results = [];
        $items = (new ButtonListFactory($this->view))->createContactList($contractor)->getItems();

        foreach ($items as $key => $value) {
            $results[] = ['id' => $key, 'text' => $value];
        }

        $this->response->format = Response::FORMAT_JSON;
        $this->response->data = $results;

        return $this->response;
    }
}
