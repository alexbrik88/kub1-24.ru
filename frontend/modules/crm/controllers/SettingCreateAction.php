<?php

namespace frontend\modules\crm\controllers;

use frontend\controllers\ActionInterface;
use frontend\controllers\RenderResponseTrait;
use frontend\controllers\WebTrait;
use frontend\modules\crm\models\TypeForm;
use frontend\modules\crm\models\AbstractTypeForm;
use frontend\modules\crm\models\AbstractType;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\web\Response;

final class SettingCreateAction extends Action implements ActionInterface
{
    use WebTrait;
    use RenderResponseTrait;

    /**
     * @var string|AbstractType
     */
    public string $modelClass = '';

    /**
     * @var string|AbstractTypeForm
     */
    public string $formClass = TypeForm::class;

    /**
     * @var array
     */
    public array $redirectRoute = ['index'];

    /**
     * @var string
     */
    public string $viewPath = '@frontend/modules/crm/views/settings/edit-form';

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        if (!is_a($this->modelClass, AbstractType::class, true)) {
            throw new InvalidConfigException();
        }

        if (!is_a($this->formClass, AbstractTypeForm::class, true)) {
            throw new InvalidConfigException();
        }

        if (empty($this->redirectRoute)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @return Response
     */
    public function run(): Response
    {
        /** @var AbstractType $type */
        $type = new $this->modelClass();
        $form = $this->formClass::createTypeForm($type, $this->user->identity, AbstractTypeForm::SCENARIO_CREATE);

        if ($form->load($this->request->post()) && $form->validate()) {
            $form->saveModel();

            if (array_key_exists('id', $this->redirectRoute)) {
                $this->redirectRoute['id'] = $type->getAttribute($type::getIdKey());
            }

            $isRedirect = (bool) $this->request->get('redirect', true);

            if ($isRedirect) {
                return $this->controller->redirect($this->redirectRoute);
            }

            return $this->renderResponse('@frontend/modules/crm/views/settings/edit-response', [
                'type' => $type,
            ]);
        }

        return $this->renderResponse($this->viewPath, [
            'form' => $form,
            'statusDisabled' => true,
        ]);
    }
}
