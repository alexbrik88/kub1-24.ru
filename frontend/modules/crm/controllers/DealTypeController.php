<?php

namespace frontend\modules\crm\controllers;

use frontend\controllers\WebTrait;
use frontend\modules\crm\behaviors\SettingWrapperBehavior;
use frontend\modules\crm\models\DealType;
use frontend\modules\crm\models\DealTypeForm;
use frontend\rbac\permissions\crm\DealTypeAccess;
use yii\filters\AccessControl;
use yii\filters\AjaxFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

final class DealTypeController extends Controller implements SettingControllerInterface
{
    use WebTrait;

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                            'view',
                        ],
                        'roles' => [DealTypeAccess::VIEW],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'create',
                        ],
                        'roles' => [DealTypeAccess::CREATE],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'update',
                            'delete',
                        ],
                        'roles' => [DealTypeAccess::EDIT],
                    ],
                ],
            ],
            'ajaxFilter' => [
                'class' => AjaxFilter::class,
                'only' => [
                    'delete',
                ],
            ],
            'settingWrapper' => [
                'class' => SettingWrapperBehavior::class,
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function actions(): array
    {
        return [
            'index' => [
                'class' => SettingIndexAction::class,
                'modelClass' => DealType::class,
                'createAccess' => DealTypeAccess::CREATE,
                'editAccess' => DealTypeAccess::EDIT,
            ],
            'create' => [
                'class' => SettingCreateAction::class,
                'modelClass' => DealType::class,
                'formClass' => DealTypeForm::class,
                'redirectRoute' => ['view', 'id' => null],
                'viewPath' => 'edit',
            ],
            'update' => [
                'class' => SettingUpdateAction::class,
                'modelClass' => DealType::class,
                'formClass' => DealTypeForm::class,
                'redirectRoute' => ['view', 'id' => null],
                'viewPath' => 'edit',
            ],
            'delete' => [
                'class' => SettingDeleteAction::class,
                'errorMessage' => 'Удалить нельзя, т.к. есть сделки в данной воронке',
                'modelClass' => DealType::class,
                'formClass' => DealTypeForm::class,
            ],
        ];
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionView(): Response
    {
        $deal = DealType::findById($this->user->identity->company, $this->request->get('id'));

        if (!$deal) {
            throw new NotFoundHttpException();
        }

        $this->response->content = $this->render('view', [
            'user' => $this->user,
            'deal' => $deal,
        ]);

        return $this->response;
    }
}
