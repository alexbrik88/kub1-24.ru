<?php

namespace frontend\modules\crm\controllers;
use common\models\Agreement;
use common\models\Contractor;
use frontend\controllers\WebTrait;
use frontend\models\Documents;
use frontend\modules\crm\models\AgreementFactory;
use frontend\modules\crm\models\AgreementForm;
use frontend\rbac\permissions\crm\ClientAccess;
use frontend\rbac\permissions\document\Document;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

final class AgreementController extends Controller
{
    use WebTrait;
    use ContractorTrait;

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [],
                        'roles' => [ClientAccess::EDIT],
                        'roleParams' => function(): array {
                            return ['contractor' => $this->getContractor()];
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * @return Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionCreate(): Response
    {
        $contractor = $this->getContractor();
        $agreement = (new AgreementFactory)->createAgreement($this->user->identity, $contractor);
        $form = new AgreementForm($agreement, $contractor);
        $this->checkAccess(Document::CREATE);

        if ($form->load($this->request->post()) && $form->validate() && $form->saveModel()) {
            return $this->response;
        }

        $this->response->content = $this->renderAjax('widgets/agreement-form', [
            'form' => $form,
        ]);

        return $this->response;
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionDelete(): Response
    {
        $contractor = $this->getContractor();
        $agreement = $this->getAgreement($contractor);
        $this->checkAccess(Document::DELETE, $agreement);
        $form = new AgreementForm($agreement, $contractor);

        if ($this->request->isPost && $form->removeModel()) {
            return $this->response;
        }

        $this->response->content = $this->renderAjax('widgets/agreement-delete', [
            'form' => $form,
        ]);

        return $this->response;
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionUpdate(): Response
    {
        $contractor = $this->getContractor();
        $agreement = $this->getAgreement($contractor);
        $this->checkAccess(Document::UPDATE, $agreement);
        $form = new AgreementForm($agreement, $contractor);

        if ($form->load($this->request->post()) && $form->validate() && $form->saveModel()) {
            return $this->response;
        }

        $this->response->content = $this->renderAjax('widgets/agreement-form', [
            'form' => $form,
        ]);

        return $this->response;
    }

    /**
     * @param string $permission
     * @param Agreement|null $agreement
     * @throws ForbiddenHttpException
     */
    private function checkAccess(string $permission, Agreement $agreement = null): void
    {
        if (!$this->user->can($permission, ['model' => $agreement, 'ioType' => Documents::DOCUMENT_AGREEMENT])) {
            throw new ForbiddenHttpException();
        }
    }

    /**
     * @param Contractor $contractor
     * @return Agreement
     * @throws NotFoundHttpException
     */
    private function getAgreement(Contractor $contractor): Agreement
    {
        $agreement = Agreement::findById($contractor, $this->request->get('agreement_id'));

        if (!$agreement) {
            throw new NotFoundHttpException('Договор не найден.');
        }

        return $agreement;
    }
}
