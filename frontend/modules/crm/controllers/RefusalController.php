<?php

namespace frontend\modules\crm\controllers;

use frontend\modules\crm\behaviors\SettingWrapperBehavior;
use frontend\modules\crm\models\Refusal;
use frontend\rbac\permissions\crm\DealTypeAccess;
use yii\filters\AccessControl;
use yii\filters\AjaxFilter;
use yii\web\Controller;

final class RefusalController extends Controller implements SettingControllerInterface
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                        ],
                        'roles' => [DealTypeAccess::VIEW],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'create',
                        ],
                        'roles' => [DealTypeAccess::CREATE],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'update',
                            'delete',
                        ],
                        'roles' => [DealTypeAccess::EDIT],
                    ],
                ],
            ],
            'ajaxFilter' => [
                'class' => AjaxFilter::class,
                'only' => [
                    'create',
                    'update',
                    'delete',
                ],
            ],
            'settingWrapper' => [
                'class' => SettingWrapperBehavior::class,
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function actions(): array
    {
        return [
            'index' => [
                'class' => SettingIndexAction::class,
                'modelClass' => Refusal::class,
                'createAccess' => DealTypeAccess::CREATE,
                'editAccess' => DealTypeAccess::EDIT,
            ],
            'create' => [
                'class' => SettingCreateAction::class,
                'modelClass' => Refusal::class,
            ],
            'update' => [
                'class' => SettingUpdateAction::class,
                'modelClass' => Refusal::class,
            ],
            'delete' => [
                'class' => SettingDeleteAction::class,
                'modelClass' => Refusal::class,
            ],
        ];
    }
}
