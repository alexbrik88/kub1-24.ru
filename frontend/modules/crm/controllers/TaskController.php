<?php

namespace frontend\modules\crm\controllers;

use Yii;
use common\models\Contractor;
use frontend\controllers\WebTrait;
use frontend\modules\crm\behaviors\TaskStatusBehavior;
use frontend\modules\crm\models\LogFilter;
use frontend\modules\crm\models\Task;
use frontend\modules\crm\models\TaskComment;
use frontend\modules\crm\models\TaskCommentForm;
use frontend\modules\crm\models\TaskEventList;
use frontend\modules\crm\models\TaskForm;
use frontend\rbac\permissions\crm\TaskAccess;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

final class TaskController extends Controller
{
    use WebTrait;

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => [TaskAccess::INDEX],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => [TaskAccess::CREATE],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'update',
                            'comment',
                            'more-comments',
                        ],
                        'roles' => [TaskAccess::EDIT],
                        'roleParams' => function(): array {
                            return ['task' => $this->getTask()];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'delete',
                            'many-delete',
                        ],
                        'roles' => [TaskAccess::DELETE],
                    ],
                ],
            ],
            'taskStatus' => [
                'class' => TaskStatusBehavior::class,
            ],
            'ajaxFilter' => [
                'class' => 'yii\filters\AjaxFilter',
                'only' => [
                    'comment',
                ],
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function actions(): array
    {
        return [
            'index' => [
                'class' => TaskIndexAction::class,
                'viewPath' => 'index',
                'canDelete' => Yii::$app->user->can(TaskAccess::DELETE),
            ],
        ];
    }

    /**
     * @return Response
     */
    public function actionCreate(): Response
    {
        $contractor = Contractor::findById($this->user->identity->company, $this->request->get('contractor_id'));
        $task = Task::createNewEmpty($this->user->identity->company, $contractor);
        $task->status = Task::STATUS_IN_PROGRESS;
        $canChangeEmployee = true;
        $form = new TaskForm($this->user->identity, $task, $contractor, $canChangeEmployee, TaskForm::SCENARIO_CREATE);
        $form->event_type = TaskEventList::EVENT_TYPE_CALL;
        $taskCommentForm = new TaskCommentForm($this->user->identity->currentEmployeeCompany, $task, new TaskComment);

        if ($this->request->post('ajax')) {
            $form->load($this->request->post());
            $this->response->data = ActiveForm::validate($form);
            $this->response->format = Response::FORMAT_JSON;

            return $this->response;
        }

        if ($form->load($this->request->post()) && $form->validate()) {
            if ($form->updateTask()) {
                if ($taskCommentForm->load($this->request->post())) {
                    $taskCommentForm->save();
                }

                return $this->response;
            }
        }

        $this->response->content = $this->renderAjax('widgets/task-form', [
            'user' => $this->user,
            'form' => $form,
            'task' => $task,
            'taskCommentForm' => $taskCommentForm,
            'filter' => null,
            'canDelete' => Yii::$app->user->can(TaskAccess::DELETE),
        ]);

        return $this->response;
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate(): Response
    {
        $task = $this->getTask();
        $scenario = $task->status == Task::STATUS_EMPTY ? TaskForm::SCENARIO_CREATE : TaskForm::SCENARIO_UPDATE;
        $canChangeEmployee = true; // $this->user->can(TaskAccess::EDIT_ALL);
        $form = new TaskForm($this->user->identity, $task, $task->contractor, $canChangeEmployee, $scenario);
        $taskCommentForm = new TaskCommentForm($this->user->identity->currentEmployeeCompany, $task, new TaskComment);

        if ($this->request->post('ajax')) {
            $form->load($this->request->post());
            $this->response->data = ActiveForm::validate($form);
            $this->response->format = Response::FORMAT_JSON;

            return $this->response;
        }

        if ($form->load($this->request->post()) && $form->validate() && $form->updateTask()) {
            if ($taskCommentForm->load($this->request->post())) {
                if ($taskCommentForm->save()) {
                    $taskCommentForm = new TaskCommentForm($this->user->identity->currentEmployeeCompany, $task, new TaskComment);
                }
            }
            if ($r = \Yii::$app->request->post('redirect')) {
                return $this->redirect($r);
            }
            if ($form->completed == TaskForm::COMPLETED_CHANGE_ADDITIONAL) {
                $contractor = $form->task->contractor;
                $contact_id = $form->contact_id;
                $task = Task::createNewEmpty($this->user->identity->company, $contractor);
                $task->status = Task::STATUS_IN_PROGRESS;
                $form = new TaskForm($this->user->identity, $task, $contractor, $canChangeEmployee, TaskForm::SCENARIO_CREATE);
                $form->event_type = TaskEventList::EVENT_TYPE_CALL;
                $form->contact_id = $contact_id;
                $taskCommentForm = new TaskCommentForm($this->user->identity->currentEmployeeCompany, $task, new TaskComment);
                $this->view->registerJs('
                    $.pjax.reload({container: "#pjaxContent", async:false});
                    if ($("#pjaxContent2").lrngth > 0) {
                        $.pjax.reload({container: "#pjaxContent2", async:false});
                    }
                ');
            } else {
                return $this->response;
            }
        }

        $logFilter = new LogFilter($this->user->identity->company, $task);
        $this->response->content = $this->renderAjax('widgets/task-form', [
            'user' => $this->user,
            'form' => $form,
            'task' => $task,
            'taskCommentForm' => $taskCommentForm,
            'filter' => $logFilter,
            'canDelete' => Yii::$app->user->can(TaskAccess::DELETE),
        ]);

        return $this->response;
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDelete(): Response
    {
        if ($this->getTask()->delete()) {
            Yii::$app->session->setFlash('success', 'Задача успешно удалено');
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось удалить задачу');
        }

        return $this->redirect($this->request->referrer ?? ['index']);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionManyDelete(): Response
    {
        $company = $this->user->identity->company;
        $count = 0;
        $task_id = ArrayHelper::getValue($this->request->post(), ['TaskSelectForm', 'task_id']);
        foreach ((array) $task_id as $task_id) {
            if ($task = Task::findById($company, $task_id)) {
                if (Yii::$app->user->can(TaskAccess::EDIT, ['task' => $task]) && $task->delete()) {
                    $count++;
                }
            }
        }

        if ($count > 0) {
            Yii::$app->session->setFlash('success', "Задачи успешно удалены ({$count})");
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось удалить задачи');
        }

        return $this->redirect($this->request->referrer ?? ['index']);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionComment(): Response
    {
        $task = $this->getTask();
        $taskCommentForm = new TaskCommentForm($this->user->identity->currentEmployeeCompany, $task, new TaskComment);

        if ($taskCommentForm->load($this->request->post()) && $taskCommentForm->save()) {
            $taskCommentForm = new TaskCommentForm($this->user->identity->currentEmployeeCompany, $task, new TaskComment);
        }

        $this->response->content = $this->renderAjax('widgets/_task-comment', [
            'taskCommentForm' => $taskCommentForm,
        ]);

        return $this->response;
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionMoreComments($last_id): Response
    {
        $task = $this->getTask();
        $taskCommentForm = new TaskCommentForm($this->user->identity->currentEmployeeCompany, $task, new TaskComment);

        $this->response->format = Response::FORMAT_JSON;
        $this->response->data = [
            'result' => $this->renderAjax('widgets/_task-comment-list', [
                'taskCommentForm' => $taskCommentForm,
                'last_id' => $last_id,
            ]),
        ];

        return $this->response;
    }

    /**
     * @return Task
     * @throws NotFoundHttpException
     */
    private function getTask(): Task
    {
        $task = Task::findById($this->user->identity->company, $this->request->get('task_id'));

        if (!$task) {
            throw new NotFoundHttpException('Задача не найдена.');
        }

        return $task;
    }
}
