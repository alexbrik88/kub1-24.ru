<?php

namespace frontend\modules\crm\controllers;

use common\models\Contractor;
use frontend\components\PageSize;
use frontend\controllers\ActionInterface;
use frontend\controllers\WebTrait;
use frontend\modules\crm\models\TaskFilter;
use frontend\modules\crm\models\TaskSelectForm;
use frontend\rbac\permissions\crm\TaskAccess;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\web\Response;

final class TaskIndexAction extends Action implements ActionInterface
{
    use WebTrait;

    /**
     * @var Contractor|null
     */
    public ?Contractor $contractor = null;

    /**
     * @var callable
     */
    public $contractorCallback;

    /**
     * @var string
     */
    public string $viewPath = '';

    /**
     * @var bool
     */
    public bool $canDelete = false;

    /**
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        if (empty($this->viewPath)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @return Response
     * @throws FiltrationErrorException
     */
    public function run(): Response
    {
        $contractor = $this->getContractor();
        $employee = $this->user->identity;
        $owner = $this->user->can(TaskAccess::VIEW_ALL) ? null : $employee;
        $filter = new TaskFilter($employee->company, $owner, $contractor, PageSize::get());
        if ($employee->config->crm_task_hide_completed) {
            $filter->status = TaskFilter::STATUS_ACTIVE;
        }
        $form = new TaskSelectForm($employee->company, clone $filter, $this->user->can(TaskAccess::EDIT_ALL));

        if ($form->load($this->request->post())) {
            if ($form->validate()) {
                $form->updateModels();
            }

            return $this->response->refresh();
        }

        if ($filter->load($this->request->get()) && !$filter->validate()) {
            throw new FiltrationErrorException($filter);
        }

        $this->response->content = $this->controller->render($this->viewPath, [
            'filter' => $filter,
            'form' => $form,
            'user' => $this->user,
            'canDelete' => $this->canDelete,
            'contractor' => $contractor,
            'isContractor' => (bool) $this->request->get('contractor'),
        ]);

        return $this->response;
    }

    /**
     * @return bool whether the rule should be applied
     */
    protected function getContractor() : ?Contractor
    {
        if (empty($this->contractor) && !empty($this->contractorCallback)) {
            $this->contractor = call_user_func($this->contractorCallback, $this);
        }

        return $this->contractor;
    }
}
