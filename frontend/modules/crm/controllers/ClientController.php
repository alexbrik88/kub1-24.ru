<?php

namespace frontend\modules\crm\controllers;

use Yii;
use frontend\components\PageSize;
use frontend\controllers\WebTrait;
use frontend\modules\crm\behaviors\ClientWrapperBehavior;
use frontend\modules\crm\behaviors\TaskStatusBehavior;
use frontend\modules\crm\models\AgreementFilter;
use frontend\modules\crm\models\ContactForm;
use frontend\modules\crm\models\ClientRefusalSearch;
use frontend\modules\crm\models\ClientStatus;
use frontend\modules\crm\models\ClientWishSearch;
use frontend\rbac\permissions\crm\ClientAccess;
use frontend\rbac\permissions\crm\ContactAccess;
use frontend\rbac\permissions\crm\TaskAccess;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

final class ClientController extends Controller
{
    use WebTrait;
    use ContractorTrait;

    /**
     * @var string
     */
    public $layoutWrapperCssClass = '';

    /**
     * @inheritDoc
     * @throws NotFoundHttpException
     */
    public function behaviors()
    {
        return [
            'accessControl' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                        ],
                        'roles' => [ClientAccess::INDEX],
                    ],
                    [
                        'allow' => true,
                        'roles' => [ClientAccess::VIEW],
                        'roleParams' => function(): array {
                            return ['contractor' => $this->getContractor()];
                        },
                    ],
                ],
            ],
            'taskStatus' => [
                'class' => TaskStatusBehavior::class,
                'actions' => ['tasks'],
            ],
            'clientHeader' => [
                'class' => ClientWrapperBehavior::class,
                'contractor' => $this->getContractor(),
            ],
        ];
    }

    /**
     * @inheritDoc
     * @throws NotFoundHttpException
     */
    public function actions(): array
    {
        return [
            'tasks' => [
                'class' => TaskIndexAction::class,
                'viewPath' => 'tasks',
                'canDelete' => Yii::$app->user->can(TaskAccess::DELETE),
                'contractorCallback' => function ($action) {
                    return $this->getContractor();
                },
            ],
        ];
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @deprecated
     */
    public function actionIndex(): Response
    {
        return $this->redirect(['tasks', 'contractor_id' => $this->getContractor()->id]);
    }

    /**
     * @return Response
     * @throws FiltrationErrorException
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function actionWish(): Response
    {
        $contractor = $this->getContractor();
        $searchModel = new ClientWishSearch($contractor);
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $dataProvider->pagination->pageSize = PageSize::get();

        $this->response->content = $this->render('wish', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'contractor' => $contractor,
            'isContractor' => (bool) $this->request->get('contractor'),
        ]);

        return $this->response;
    }

    /**
     * @return Response
     * @throws FiltrationErrorException
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function actionAgreements(): Response
    {
        $contractor = $this->getContractor();
        $filter = new AgreementFilter($contractor, PageSize::get());

        if ($filter->load($this->request->get()) && !$filter->validate()) {
            throw new FiltrationErrorException($filter);
        }

        $this->response->content = $this->render('agreements', [
            'filter' => $filter,
            'contractor' => $contractor,
            'isContractor' => (bool) $this->request->get('contractor'),
        ]);

        return $this->response;
    }

    /**
     * @return Response
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function actionHistory(): Response
    {
        $contractor = $this->getContractor();

        $this->response->content = $this->render('history', [
            'contractor' => $contractor,
            'isContractor' => (bool) $this->request->get('contractor'),
        ]);

        return $this->response;
    }

    /**
     * @return Response
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function actionCard(): Response
    {
        $contractor = $this->getContractor();
        $contacts = $this->user->can(ContactAccess::VIEW, ['contractor' => $contractor])
            ? ArrayHelper::getColumn(ContactForm::createMultiple($contractor), 'contact')
            : null;

        $this->response->content = $this->render('card', [
            'company' => $this->user->identity->company,
            'contacts' => $contacts,
            'contractor' => $contractor,
            'isContractor' => (bool) $this->request->get('contractor'),
        ]);

        return $this->response;
    }

    /**
     * @return Response
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function actionRefusal(): Response
    {
        $contractor = $this->getContractor();
        if (!$contractor->client || $contractor->client->client_status_id != ClientStatus::STATUS_REFUSED) {
            return $this->redirect(['tasks', 'contractor_id' => $contractor->id]);
        }
        $searchModel = new ClientRefusalSearch([
            'company_id' => $contractor->company_id,
            'contractor_id' => $contractor->id,
            'scenario' => ClientRefusalSearch::SCENARIO_CLIENT,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $dataProvider->pagination->pageSize = PageSize::get();

        $this->response->content = $this->render('refusal', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'contractor' => $contractor,
            'isContractor' => (bool) $this->request->get('contractor'),
        ]);

        return $this->response;
    }

    public function getBackUrl() : ?string
    {
        if ($referrer = \Yii::$app->request->referrer) {
            if (strpos($referrer, '/crm/clients/index') !== false) {
                \yii\helpers\Url::remember($referrer, 'crm.client.card');
            }
        }

        return \yii\helpers\Url::previous('crm.client.card');
    }
}
