<?php

namespace frontend\modules\crm\controllers;

use frontend\modules\crm\behaviors\SettingWrapperBehavior;
use frontend\modules\crm\models\ContractorActivity;
use frontend\rbac\permissions\crm\ContractorActivityAccess;
use yii\filters\AccessControl;
use yii\filters\AjaxFilter;
use yii\web\Controller;

final class ContractorActivityController extends Controller implements SettingControllerInterface
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                        ],
                        'roles' => [ContractorActivityAccess::VIEW],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'create',
                        ],
                        'roles' => [ContractorActivityAccess::CREATE],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'update',
                            'delete',
                        ],
                        'roles' => [ContractorActivityAccess::EDIT],
                    ],
                ],
            ],
            'ajaxFilter' => [
                'class' => AjaxFilter::class,
                'only' => [
                    'create',
                    'update',
                    'delete',
                ],
            ],
            'settingWrapper' => [
                'class' => SettingWrapperBehavior::class,
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function actions(): array
    {
        return [
            'index' => [
                'class' => SettingIndexAction::class,
                'modelClass' => ContractorActivity::class,
                'createAccess' => ContractorActivityAccess::CREATE,
                'editAccess' => ContractorActivityAccess::EDIT,
            ],
            'create' => [
                'class' => SettingCreateAction::class,
                'modelClass' => ContractorActivity::class,
            ],
            'update' => [
                'class' => SettingUpdateAction::class,
                'modelClass' => ContractorActivity::class,
            ],
            'delete' => [
                'class' => SettingDeleteAction::class,
                'modelClass' => ContractorActivity::class,
            ],
        ];
    }
}
