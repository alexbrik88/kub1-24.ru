<?php

namespace frontend\modules\crm\controllers;

use Yii;
use frontend\components\PageSize;
use frontend\components\StatisticPeriod;
use frontend\modules\crm\models\ClientRefusalSearch;
use yii\filters\AccessControl;
use yii\web\Controller;

final class ReportsController extends Controller
{
    public $layout = 'crm-reports';

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->rdirect(['refusal']);
    }

    /**
     * Lists all ClientRefusal models.
     *
     * @return string
     */
    public function actionRefusal()
    {
        $searchModel = new ClientRefusalSearch([
            'company_id' => Yii::$app->user->identity->company->id,
            'period' => StatisticPeriod::getSessionPeriod(),
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = PageSize::get();

        return $this->render('refusal', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
