<?php

namespace frontend\modules\crm\controllers;

use frontend\components\PageSize;
use frontend\controllers\WebTrait;
use frontend\modules\crm\behaviors\SwitchDealTypeBehavior;
use frontend\modules\crm\models\Deal;
use frontend\modules\crm\models\DealFilter;
use frontend\modules\crm\models\DealForm;
use frontend\modules\crm\models\DealSelectForm;
use frontend\modules\crm\models\DealType;
use frontend\rbac\permissions\crm\DealAccess;
use yii\filters\AccessControl;
use yii\filters\AjaxFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

final class DealController extends Controller
{
    use WebTrait;

    /**
     * @var Deal|null
     */
    private ?Deal $_deal = null;

    /**
     * @var DealType|null
     */
    private ?DealType $_dealType = null;

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => [DealAccess::INDEX],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => [DealAccess::CREATE],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'delete'],
                        'roles' => [DealAccess::EDIT],
                        'roleParams' => function(): array {
                            return ['deal' => $this->getDeal()];
                        },
                    ],
                ],
            ],
            'ajaxFilter' => [
                'class' => AjaxFilter::class,
                'only' => [
                    'create',
                    'update',
                    'delete',
                ],
            ],
            'switchDealType' => [
                'class' => SwitchDealTypeBehavior::class,
            ],
        ];
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws FiltrationErrorException
     */
    public function actionIndex(): Response
    {
        $dealType = ($this->request->get('deal_type_id') !== null) ? $this->getDealType() : new DealType();
        $owner = $this->user->can(DealAccess::VIEW_ALL) ? null : $this->user->identity;
        $filter = new DealFilter($this->user->identity->company, $dealType, PageSize::get(), $owner);
        $form = new DealSelectForm($dealType, clone $filter, $this->user);

        if ($form->load($this->request->post()) && $form->validate() && $form->updateDeals()) {
            return $this->refresh();
        }

        if ($filter->load($this->request->get()) && !$filter->validate()) {
            throw new FiltrationErrorException($filter);
        }

        $this->response->content = $this->render('index', [
            'form' => $form,
            'user' => $this->user,
            'dealType' => $dealType,
            'canCreate' => $this->user->can(DealAccess::CREATE),
            'canEdit' => $this->user->can(DealAccess::EDIT_ALL),
            'filter' => $filter,
        ]);

        return $this->response;
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionCreate(): Response
    {
        $dealType = $this->getDealType();
        $form = new DealForm(new Deal(), $dealType, $this->user);

        if ($form->load($this->request->post()) && $form->validate() && $form->updateDeal()) {
            return $this->redirect(['index', 'deal_type_id' => $dealType->deal_type_id]);
        }

        $this->response->content = $this->renderAjax('widgets/deal-form', [
            'form' => $form,
        ]);

        return $this->response;
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate(): Response
    {
        $deal = $this->getDeal();
        $form = new DealForm($deal, $deal->type, $this->user);

        if ($form->load($this->request->post()) && $form->validate() && $form->updateDeal()) {
            return $this->redirect(['index', 'deal_type_id' => $deal->type->deal_type_id]);
        }

        $this->response->content = $this->renderAjax('widgets/deal-form', [
            'form' => $form,
        ]);

        return $this->response;
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDelete(): Response
    {
        $deal = $this->getDeal();
        $dealType = $deal->type;

        if ($this->request->isPost) {
            $deal->delete();

            return $this->response->redirect(['index', 'deal_type_id' => $dealType->deal_type_id]);
        }

        $this->response->content = $this->renderAjax('widgets/deal-delete', [
            'deal' => $deal,
        ]);

        return $this->response;
    }

    /**
     * @return Deal
     * @throws NotFoundHttpException
     */
    private function getDeal(): Deal
    {
        if ($this->_deal === null) {
            $this->_deal = Deal::findById($this->user->identity->company, $this->request->get('deal_id'));
        }

        if ($this->_deal === null) {
            throw new NotFoundHttpException('Сделка не найдена.');
        }

        return $this->_deal;
    }

    /**
     * @return DealType
     * @throws NotFoundHttpException
     */
    private function getDealType(): DealType
    {
        if ($this->_dealType === null) {
            $this->_dealType = DealType::findById($this->user->identity->company, $this->request->get('deal_type_id'));
        }

        if ($this->_dealType === null) {
            throw new NotFoundHttpException('Тип сделки не найден.');
        }

        return $this->_dealType;
    }
}
