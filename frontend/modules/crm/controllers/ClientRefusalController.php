<?php

namespace frontend\modules\crm\controllers;

use Yii;
use common\models\Contractor;
use frontend\controllers\WebTrait;
use frontend\modules\crm\models\AbstractType;
use frontend\modules\crm\models\Client;
use frontend\modules\crm\models\ClientRefusal;
use frontend\modules\crm\models\ClientStatus;
use frontend\modules\crm\models\Refusal;
use frontend\rbac\permissions\crm\ClientAccess;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * ClientRefusalController implements the CRUD actions for ClientRefusal model.
 */
class ClientRefusalController extends Controller
{
    use ContractorTrait;
    use WebTrait;

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => [ClientAccess::EDIT],
                            'roleParams' => function($rule) {
                                return [
                                    'contractor' => $this->findContractor($this->request->get('contractor_id')),
                                ];
                            }
                        ],
                    ],
                ],
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Creates a new ClientRefusal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate($contractor_id)
    {
        $client = $this->findClient($contractor_id);

        $model = new ClientRefusal([
            'company_id' => $client->company_id,
            'contractor_id' => $client->contractor_id,
            'employee_id' => Yii::$app->user->id,
        ]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                if ($client->isNewRecord || $client->client_status_id != ClientStatus::STATUS_REFUSED) {
                    $client->client_status_id = ClientStatus::STATUS_REFUSED;
                    $client->save(false);
                }

                return $this->redirect(Yii::$app->request->referrer);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->renderAjax('create', [
            'model' => $model,
            'refusalArray' => $this->refusalArray(),
        ]);
    }

    /**
     * Updates an existing ClientRefusal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->renderAjax('update', [
            'model' => $model,
            'refusalArray' => $this->refusalArray(),
        ]);
    }

    /**
     * Deletes an existing ClientRefusal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the ClientRefusal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return ClientRefusal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = ClientRefusal::find()->andWhere([
            'id' => $id,
            'company_id' => Yii::$app->user->identity->company->id,
        ])->one();

        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the ClientRefusal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return ClientRefusal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findContractor($id)
    {
        $model = Contractor::findOne([
            'company_id' => Yii::$app->user->identity->company->id,
            'id' => $id,
        ]);

        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the ClientRefusal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return ClientRefusal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findClient($id)
    {
        $contractor = $this->findContractor($id);
        $model = $contractor->client ?? Client::createInstance($contractor);

        return $model;
    }

    /**
     * Finds the ClientRefusal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return ClientRefusal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function refusalArray()
    {
        return Refusal::find()->andWhere([
            'company_id' => Yii::$app->user->identity->company->id,
            'status' => AbstractType::TYPE_STATUS_ACTIVE,
        ])->orderBy([
            'name' => SORT_ASC,
        ])->all();
    }
}
