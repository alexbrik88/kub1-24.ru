<?php

namespace frontend\modules\crm\services\Client;

use Yii;
use common\models\Contractor;
use common\models\employee\Employee;
use common\services\TransactionManager;
use frontend\modules\crm\models\ClientImportLegal;
use frontend\modules\crm\models\ClientImportPhysical;
use frontend\modules\crm\models\ClientType;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class ImportFromExcelService
{
    private Employee $employee;
    private array $report = [];

    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
        $this->transaction = \Yii::createObject(TransactionManager::class);
    }

    public function import(string $filename): bool
    {


        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($filename);
        $spreadsheet->setActiveSheetIndex(0);
        $worksheet = $spreadsheet->getActiveSheet();
        $importData = $worksheet->toArray(null, false, false, true);

        $faceType = $this->faceType($importData[1]);

        if ($faceType === null) {
            $this->report = [
                'status' => 0,
                'message' => 'Не верный формат файла. Скачайте нужный шаблон, заполните его и попытайтесь загрузить еще раз.',
            ];
        } else {
            $this->importInternal($faceType, $importData);
        }

        return ($this->report['status'] ?? 0) === 1;
    }

    private function importInternal(int $faceType, array $importData) : void
    {
        $modelClass = $faceType == Contractor::TYPE_PHYSICAL_PERSON ? ClientImportPhysical::class : ClientImportLegal::class;
        $model = new $modelClass($this->employee, $this->transaction);
        $sheetCount = count($importData);
        $defaultClientType = ClientType::find()->select('client_type_id')->where([
            'company_id' => $this->employee->company->id,
            'value' => ClientType::TYPE_CLIENT,
        ])->scalar();

        $this->report = [
            'status' => 0,
            'message' => [
                'Данные клиентов в файле не найдены.',
                'Заполните файл и загрузите еще раз.'
            ],
        ];

        if ($sheetCount < 2) {
            return;
        }

        $loadKeys = array_keys($model->attributeLabels());
        $loadKeysCount = count($loadKeys);

        $successCount = 0;
        $failCount = 0;
        $errors = [];

        for ($i = 2; $i <= $sheetCount; $i++) {
            $clientData = array_slice($importData[$i], 0, $loadKeysCount);
            if (empty(array_filter($clientData))) {
                break;
            }
            $loadData = array_combine($loadKeys, $clientData);
            $model = new $modelClass($this->employee, $this->transaction, $defaultClientType);

            if ($model->load($loadData, '') && $model->import()) {
                $successCount++;
            } else {
                $failCount++;
                $errors[$i] = ($e = $model->getFirstErrors()) ? implode(' ', $e) : "Неизвестная ошибка.";
            }
        }
        if ($successCount > 0) {
            $this->report['status'] = 1;
        }
        $this->report['message'] = [];
        $this->report['message'][] = "Добавлено новых позиций: {$successCount} шт";
        $this->report['message'][] = "Не загрузилось: {$failCount} шт";
        if (!empty($errors)) {
            $this->report['message'][] = "Необходимо исправить следующие ошибки:";
            foreach ($errors as $key => $value) {
                $this->report['message'][] = "{$key} - {$value}";
            }
            $this->report['message'][] = "Исправьте файл и загрузите заново.";
        }
    }

    private function faceType(array $tableHeader) : ?int
    {
        $tableHeader = array_filter($tableHeader);
        foreach ($tableHeader as $key => $value) {
            $tableHeader[$key] = trim($value, " \*\n\r\t\v\0");
        }

        $legalForm = new ClientImportLegal($this->employee, $this->transaction);
        $physicalForm = new ClientImportPhysical($this->employee, $this->transaction);

        switch (true) {
            case empty(array_diff($legalForm->attributeLabels(), $tableHeader)):
                return Contractor::TYPE_LEGAL_PERSON;
                break;

            case empty(array_diff($physicalForm->attributeLabels(), $tableHeader)):
                return Contractor::TYPE_PHYSICAL_PERSON;
                break;

            default:
                return null;
                break;
        }
    }

    public function importReport() : array
    {
        return (array) $this->report;
    }
}
