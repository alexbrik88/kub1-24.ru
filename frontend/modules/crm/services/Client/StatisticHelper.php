<?php

namespace frontend\modules\crm\services\Client;

use common\models\ContractorQuery;
use frontend\modules\crm\models\Task;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;

class StatisticHelper
{
    private ContractorQuery $_query;

    private array $data = [];

    public function __construct(ContractorQuery $query)
    {
        $this->_query = $query;
    }

    public function totalCount() : int
    {
        if (!isset($this->data['totalCount'])) {
            $this->data['totalCount'] = $this->query()->limit(-1)->offset(-1)->orderBy([])->count('*');
        }

        return $this->data['totalCount'];
    }

    public function withOverdueTasksCount() : int
    {
        if (!isset($this->data['withOverdueTasksCount'])) {
            $query = $this->query();
            $alias = $query->getTableAlias();
            $query->leftJoin([
                'overdue' => Task::tableName()
            ], "{{{$alias}}}.[[id]] = {{overdue}}.[[contractor_id]]")->andWhere([
                'overdue.status' => Task::STATUS_EXPIRED,
            ])->groupBy("$alias.id")->limit(-1)->offset(-1)->orderBy([]);

            $this->data['withOverdueTasksCount'] = (new Query)->from(['t' => $query])->count('*');
        }

        return $this->data['withOverdueTasksCount'];
    }

    public function noContactsCount() : int
    {
        if (!isset($this->data['noContactsCount'])) {
            $query = $this->query();
            $alias = $query->getTableAlias();
            $datetime = date_create('-30 days')->format('Y-m-d 00:00:00');
            $query->leftJoin([
                'noContacts' => Task::tableName()
            ], "{{{$alias}}}.[[id]] = {{noContacts}}.[[contractor_id]] AND {{noContacts}}.[[datetime]] < \"{$datetime}\"")->andWhere([
                'noContacts.task_id' => null,
            ])->groupBy("$alias.id")->limit(-1)->offset(-1)->orderBy([]);

            $this->data['noContactsCount'] = (new Query)->from(['t' => $query])->count('*');
        }

        return $this->data['noContactsCount'];
    }

    public function query() : ActiveQuery
    {
        return clone $this->_query;
    }
}
