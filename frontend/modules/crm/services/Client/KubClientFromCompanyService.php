<?php

namespace frontend\modules\crm\services\Client;

use Yii;
use common\models\Company;
use common\models\Contractor;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\services\TransactionManager;
use common\services\Contractor\KubContractorFromCompany;
use frontend\modules\crm\models\Client;
use frontend\modules\crm\models\ClientPosition;
use frontend\modules\crm\models\Contact;
use frontend\modules\crm\models\ContactForm;
use yii\helpers\ArrayHelper;

/**
 * Создание CRM клиента для компании КУБ из зарегистрированной в сервисе компании
 */
class KubClientFromCompanyService
{
    private Company $kubCompany;
    private Company $company;

    public function __construct(Company $company)
    {
        $this->company = $company;
        $this->kubCompany = Yii::$app->kubCompany;
    }

    public function create(): ?Contractor
    {
        $contractorService = new KubContractorFromCompany($this->company);
        $contractor = $contractorService->create(false);
        if ($contractor) {
            if (empty($contractor->name)) {
                $contractor->name = 'ID '.$this->company->id;
            }

            $responsibleEmployeeId = \common\models\CrmKubResponsible::getEmployeeId();
            if ($responsibleEmployeeId) {
                $contractor->responsible_employee_id = $responsibleEmployeeId;
            }

            $needContacts = false;
            if ($contractor->isNewRecord) {
                $needContacts = true;
                $contractor->is_customer = 0;
                $contractor->type = Contractor::TYPE_POTENTIAL_CLIENT;
            }
            if ($contractor->save(false)) {
                if ($contractor->client) {
                    $client = $contractor->client;
                } else {
                    $client = Client::createInstance($contractor);
                    if (!$client->save()) {
                        \common\components\helpers\ModelHelper::logErrors($client, __METHOD__);
                    }
                }
                if (!$client->isNewRecord) {
                    $this->contractor = $contractor;
                    $this->client = $client;
                    $contractor->populateRelation('client', $client);
                    if ($needContacts) {
                        $this->createContacts();
                        if ($responsibleEmployeeId) {
                            \common\models\CrmKubResponsible::touch($responsibleEmployeeId);
                        }
                    }

                    return $contractor;
                }
            } else {
                \common\components\helpers\ModelHelper::logErrors($contractor, __METHOD__);
            }
        }

        return null;
    }

    private function createContacts() : void
    {
        $employeeCompanies = $this->company->getEmployeeCompanies()->andWhere([
            'is_working' => true,
        ])->orderBy('[[employee_role_id]] = :chief desc, [[created_at]] asc')->addParams([
            ':chief' => EmployeeRole::ROLE_CHIEF
        ])->all();

        foreach ($employeeCompanies as $key => $employeeCompany) {
            if ($employeeCompany->is_working) {
                $this->createContactFromEmployeeCompany($employeeCompany, $key === 0);
            }
        }
    }

    private function createContactFromEmployeeCompany(EmployeeCompany $employeeCompany, bool $isDefault) : void
    {
        $contact = new ContactForm($this->contractor, new Contact);
        $contact->contact_default = $isDefault;
        $contact->contact_name = ArrayHelper::getValue($employeeCompany, ['fio'], '---');
        $contact->client_position_id = $this->getPositionIdByName($employeeCompany->position);
        $contact->primary_phone_number = ArrayHelper::getValue($employeeCompany, ['phone'], '---');
        $contact->email_address = ArrayHelper::getValue($employeeCompany, ['employee', 'email'], '---');
        if ($contact->validate()) {
            $contact->updateContact();
        } else {
            \common\components\helpers\ModelHelper::logErrors($contact, __METHOD__);
        }
    }

    private function getPositionIdByName(?string $positionName) : ?int
    {
        if (empty($positionName)) {
            return null;
        }

        $position = ClientPosition::find()->andWhere([
            'company_id' => $this->kubCompany->id,
            'name' => $positionName,
        ])->orderBy([
            'status' => SORT_DESC,
        ])->one();

        if ($position === null) {
            $position = $this->createPosition($positionName);
        } elseif ($position->status == ClientPosition::TYPE_STATUS_INACTIVE) {
            $position->updateAttributes([
                'status' => ClientPosition::TYPE_STATUS_ACTIVE,
            ]);
        }

        return $position->client_position_id;
    }

    private function createPosition(string $positionName) : ClientPosition
    {
        $position = new ClientPosition([
            'company_id' => $this->kubCompany->id,
            'name' => $positionName,
        ]);

        $position->save();

        return $position;
    }
}
