$(document).ready(function () {
    $(document).on('change', '.checker input[type="checkbox"]', function() {
        var checkBox = $(this);

        if (checkBox.prop('checked')) {
            checkBox.parent().addClass('checked');
        } else {
            checkBox.parent().removeClass('checked');
        }
    });

    $(document).on('change', '#clientSelectForm input[type="checkbox"]', function() {
        var form = $('#clientSelectForm');
        var summary = $('#clientSelectSummary');
        var count = form.find('td input[type="checkbox"]:checked').length;

        if (count) {
            summary.addClass('visible').addClass('check-true');
        } else {
            summary.removeClass('visible').removeClass('check-true');
        }

        summary.find('.total-count').text(count);
    });
    
    $(document).on('hidden.bs.modal', '#changeEmployeeDialog, #changeStatusDialog', function () {
        $(this).find('[name]').each(function () {
            $(this).val('');
            $(this).trigger('change');
        });
    });
});
