$(document).ready(function () {
    $(document).on('click', '.add-client-dialog', function() {
        const url = $(this).data('url');

        $.pjax({
            url: url,
            container: '#add-client-pjax',
            push: false,
            scrollTo: false
        });

        return false;
    });

    $(document).on("pjax:success", "#add-client-pjax", function () {
        $("#add-client-widget").modal();
    });

    $(document).on('select2:selecting', 'select', function (event) {
        if (event.params.args.data.id === 'add-client-button') {
            const select = $(this);
            const url = $('body .select2-container.select2-container--open [data-url]').data('url');

            select.select2('close');

            $(document).off('client:created');
            $(document).on('client:created', function (event, item) {
                let option = new Option(item.label, item.id, false, false);

                option.setAttribute('data-face_type', item.face_type);
                select.append(option);
                select.val(item.id);
                select.trigger('change');
                select.trigger('client:added', item);
            });

            $.pjax({
                url: url,
                container: '#add-client-pjax',
                push: false,
                scrollTo: false
            });

            return false;
        }

        return true;
    });
});
