$(document).ready(function() {
    var updateDialog = function(data) {
        var dialog = $('#ajaxFormDialog');

        dialog.find('.modal-body').html(data);
        dialog.find('.date-picker').datepicker(kubDatepickerConfig);
        dialog.find('input[type="checkbox"]').uniform('refresh');
        tooltipstering();
        initDatepicker();
    };

    window.openAjaxDialog = function (title, url) {
        var ajax = $.get(url);
        var dialog = $('#ajaxFormDialog');

        dialog.find('.modal-body').html('');

        ajax.done(function(data) {
            dialog.modal('show');
            dialog.find('.modal-title').html(title);
            updateDialog(data);
        });

        return false;
    };

    $(document).on('click', '.ajax-form-button', function () {
        var button = $(this);
        var dialog = $('#ajaxFormDialog');
        var form = dialog.find('form');
        var url = button.data('url') || this.href;

        if (form.length && form.attr('action') === url) {
            dialog.modal('show');

            return false;
        }

        window.openAjaxDialog(button.data('title'), url);

        return false;
    });

    $(document).on('submit', '#ajaxFormDialog form', function () {
        var form = $(this);
        var dialog = $('#ajaxFormDialog');
        var ajax = $.post(form.attr('action'), form.serialize());

        ajax.done(function(data) {
            dialog.find('.modal-body').html('');

            if (data.length) {
                updateDialog(data);
            } else {
                dialog.modal('hide');

                $.pjax.reload({container: '#pjaxContent', async:false});
                if ($('#pjaxContent2').length) {
                    $.pjax.reload({container: '#pjaxContent2', async:false});
                }
            }
        });

        return false;
    });

    $(document).on('hidden.bs.modal', '#ajaxFormDialog', function (e) {
        if (e.target.id == 'ajaxFormDialog') {
            $('.modal-dialog', this).toggleClass('modal-xl', false);
            $('.modal-body', this).html('');
        }
    });
});
