!function($) {
    var taskAddComment = function (input) {
        let val = input.value.trim();
        if (val !== '') {
            $.ajax({
                url: $(input).data('url'),
                method: 'post',
                data: $(input.form).serialize(),
                success: function(data) {
                    console.log(data);
                    $('#task-comment-wrapper').replaceWith(data);
                }
            });
            input.value = '';
        }
    };
    $(document).on('keyup keypress', '#taskcommentform-text', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            taskAddComment(this);
            return false;
        }
    });
    $(document).on('click', '#task-comment-add', function (e) {
        e.preventDefault();
        let input = $(this).parent().find('input').get(0);
        if (input) {
            taskAddComment(input);
        }
    });
    $(document).on('click', '.show_more_task_comments', function (e) {
        e.preventDefault();
        var btn = this;
        $.ajax({
            url: $(btn).data('url'),
            success: function (data) {
                if (data.result) {
                    var result = $('<div>'+data.result+'</div>');
                    var items = $('#task-comment-list', result).html();
                    console.log(items);
                    $(btn).replaceWith(items);
                }
            }
        });
    });
}(window.jQuery);