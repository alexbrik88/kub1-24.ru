$(document).ready(function() {
    $(document).on('change', '#clientForm select', function() {
        if (this.id == 'client_status_id' && this.value == '2') {
            var btn = $(this).data('refusal-btn');
            if (btn) {
                $(btn).trigger('click');
                return;
            }
        }
        $(this).submit();
    });
    $(document).on('hide.bs.modal', '#client-refusal-ajax-modal-box', function() {
        document.location.reload();
    });
});
