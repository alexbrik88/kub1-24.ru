$(document).ready(function () {
    $(document).on("pjax:success", "#add-item-pjax", function () {
        $("#add-item-widget").modal();
    });

    $(document).on('select2:selecting', 'select', function (event) {
        if (event.params.args.data.id === 'add-item-button') {
            const select = $(this);
            const url = $('body .select2-container.select2-container--open .add-item-button').data('url');

            select.select2('close');

            if (url) {
                $(document).off('type:created');
                $(document).on('type:created', function (event, item) {
                    let option = new Option(item.label, item.id, false, false);

                    select.append(option);
                    select.val(item.id);
                    select.trigger('change');
                    select.trigger('type:added', item);
                });
            }

            if (url) {
                $.pjax({
                    url: url,
                    container: '#add-item-pjax',
                    push: false,
                    scrollTo: false
                });
            }

            return false;
        }

        return true;
    });
});
