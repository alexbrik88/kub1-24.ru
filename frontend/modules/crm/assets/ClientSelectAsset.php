<?php

namespace frontend\modules\crm\assets;

use yii\web\AssetBundle;

final class ClientSelectAsset extends AssetBundle
{
    /**
     * @inheritDoc
     */
    public $js = [
        'client-select.js',
    ];

    /**
     * @inheritDoc
     */
    public $sourcePath = __DIR__ . '/dist';

    /**
     * @inheritDoc
     */
    public $depends = [
        'yii\bootstrap4\BootstrapPluginAsset',
    ];
}
