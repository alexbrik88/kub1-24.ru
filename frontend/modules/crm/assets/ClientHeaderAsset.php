<?php

namespace frontend\modules\crm\assets;

use yii\web\AssetBundle;

final class ClientHeaderAsset extends AssetBundle
{
    /**
     * @inheritDoc
     */
    public $css = [
        'client-header.css',
    ];

    /**
     * @inheritDoc
     */
    public $js = [
        'client-header.js',
    ];

    /**
     * @inheritDoc
     */
    public $sourcePath = __DIR__ . '/dist';

    /**
     * @inheritDoc
     */
    public $depends = [
        'yii\bootstrap4\BootstrapPluginAsset',
        'frontend\modules\crm\assets\TaskFormAsset',
    ];
}
