<?php

namespace frontend\modules\crm\assets;

use yii\web\AssetBundle;

final class BootstrapTimepickerAsset extends AssetBundle
{
    /**
     * @inheritDoc
     */
    public $sourcePath = '@common/assets/web/plugins/bootstrap-timepicker';

    /**
     * @inheritDoc
     */
    public $css = [
        'css/bootstrap-timepicker.min.css',
    ];

    /**
     * @inheritDoc
     */
    public $js = [
        'js/bootstrap-timepicker.min.js',
    ];

    /**
     * @inheritDoc
     */
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}
