<?php

namespace frontend\modules\crm\assets;

use yii\web\AssetBundle;

final class TaskFormAsset extends AssetBundle
{
    /**
     * @inheritDoc
     */
    public $sourcePath = __DIR__ . '/dist';

    /**
     * @inheritDoc
     */
    public $css = [
        'timepicker.css',
    ];

    /**
     * @inheritDoc
     */
    public $js = [
        'task-form.js',
    ];

    /**
     * @inheritDoc
     */
    public $depends = [
        'yii\bootstrap4\BootstrapPluginAsset',
        'frontend\modules\crm\assets\BootstrapTimepickerAsset',
    ];
}
