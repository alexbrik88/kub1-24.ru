<?php

namespace frontend\modules\crm\assets;

use yii\web\AssetBundle;

final class RowSelectAsset extends AssetBundle
{
    /**
     * @inheritDoc
     */
    public $js = [
        'row-select.js',
    ];

    /**
     * @inheritDoc
     */
    public $sourcePath = __DIR__ . '/dist';

    /**
     * @inheritDoc
     */
    public $depends = [
        'yii\bootstrap4\BootstrapPluginAsset',
    ];
}
