<?php

namespace frontend\modules\crm\assets;

use yii\web\AssetBundle;

final class AddItemAsset extends AssetBundle
{
    /**
     * @inheritDoc
     */
    public $js = [
        'add-item.js',
    ];

    /**
     * @inheritDoc
     */
    public $sourcePath = __DIR__ . '/dist';

    /**
     * @inheritDoc
     */
    public $depends = [
        'yii\bootstrap4\BootstrapPluginAsset',
        'frontend\themes\kub\assets\KubCommonAsset',
        'kartik\select2\Select2Asset',
    ];
}
