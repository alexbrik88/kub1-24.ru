<?php

namespace frontend\modules\crm\views;

use frontend\components\WebUser;
use frontend\modules\crm\models\TypeFilter;
use yii\web\View;

/**
 * @var View $this
 * @var WebUser $user
 * @var TypeFilter $filter
 * @var string $createAccess
 * @var string $editAccess
 */

$this->title = 'Вариант отказа';

?>

<?= $this->render('@frontend/modules/crm/views/settings/type-grid', [
    'filter' => $filter,
    'user' => $user,
    'createAccess' => $createAccess,
    'editAccess' => $editAccess,
    'deletionError' => 'Удалить нельзя, так как этот вариант отказа задействован. Вы можете нажать на редактировать и поменять "Используется" на "Архив"',
]) ?>
