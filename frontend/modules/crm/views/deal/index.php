<?php

namespace frontend\modules\crm\views;

use frontend\modules\crm\models\DealFilter;
use frontend\modules\crm\models\DealSelectForm;
use frontend\modules\crm\models\DealType;
use frontend\modules\crm\widgets\AddClientDialogWidget;
use frontend\modules\crm\widgets\GridButtonWidget;
use frontend\modules\crm\widgets\PjaxWidget;
use frontend\modules\crm\widgets\AddItemDialogWidget;
use frontend\rbac\permissions\crm\DealAccess;
use frontend\themes\kub\widgets\ImportDialogWidget;
use yii\helpers\Url;
use yii\web\User;
use yii\web\View;

/**
 * @var View $this
 * @var DealType $dealType
 * @var DealFilter $filter
 * @var DealSelectForm $form
 * @var User $user
 */

$this->title = $dealType->name ?: 'Сделки';

?>

<?= AddItemDialogWidget::widget() ?>
<?= AddClientDialogWidget::widget() ?>
<?= ImportDialogWidget::widget() ?>

<?php PjaxWidget::begin(['id' => 'pjaxContent']) ?>

<div class="stop-zone-for-fixed-elems cash-bank-flows-index">
    <div class="page-head d-flex flex-wrap align-items-center">
        <?= $this->render('widgets/change-type', [
            'user' => $user,
            'dealType' => $dealType,
        ]) ?>

        <?= GridButtonWidget::widget([
            'accessName' => DealAccess::CREATE,
            'visible' => !$dealType->isNewRecord,
            'label' => 'Добавить сделку',
            'icon' => 'add-icon',
            'options' => [
                'id' => 'dealAddButton',
                'class' => 'button-regular button-regular_red pl-3 pr-3 ml-auto import-dialog',
                'data-url' => Url::to(['deal/create', 'deal_type_id' => $dealType->deal_type_id]),
            ],
        ]) ?>
    </div>
</div>

<?= $this->render('widgets/deal-stages', [
    'dealType' => $dealType,
    'filter' => $filter,
]) ?>

<?= $this->render('widgets/deal-filter', [
    'filter' => $filter,
    'placeholder' => 'Поиск по названию...',
]) ?>

<?= $this->render('widgets/deal-grid', [
    'dealType' => $dealType,
    'filter' => $filter,
    'form' => $form,
    'user' => $user,
]) ?>

<?php PjaxWidget::end(); ?>
