<?php

namespace frontend\modules\crm\views;

use frontend\modules\crm\assets\RowSelectAsset;
use frontend\modules\crm\models\DealSelectForm;
use frontend\modules\crm\models\DealType;
use frontend\modules\crm\models\ListFactory;
use frontend\modules\crm\widgets\GridButtonWidget;
use frontend\rbac\permissions\crm\DealAccess;
use kartik\select2\Select2;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\web\View;

/**
 * @var View $this
 * @var string $content
 * @var DealSelectForm $form
 * @var DealType $dealType
 */

RowSelectAsset::register($this);

$text = 'Для выбранных сделок изменить на:';
$dialogs = [
    [
        'id' => 'changeDealDialog',
        'label' => $form->getAttributeLabel('deal_stage_id'),
        'text' => $text,
        'enabled' => true,
        'button' => 'Этап',
        'title' => 'Изменить этап',
        'input' => Select2::widget([
            'model' => $form,
            'attribute' => 'deal_stage_id',
            'data' => (new ListFactory)->createDealStageList($dealType)->getItems(),
            'pluginOptions' => [
                'placeholder' => '',
                'width' => '100%',
            ],
            'hideSearch' => true,
        ]),
    ],
    [
        'id' => 'changeEmployeeDialog',
        'label' => $form->getAttributeLabel('employee_id'),
        'text' => $text,
        'enabled' => $form->getUser()->can(DealAccess::EDIT_ALL),
        'button' => 'Ответственного',
        'title' => 'Изменить ответственного',
        'attribute' => 'employee_id',
        'input' => Select2::widget([
            'model' => $form,
            'attribute' => 'employee_id',
            'data' => (new ListFactory)->createEmployeeList()->getItems(),
            'pluginOptions' => [
                'placeholder' => '',
                'width' => '100%',
            ],
            'hideSearch' => true,
        ]),
    ],
];

?>

<?= Html::beginForm(null, 'post', ['id' => 'rowSelectForm']); ?>

<?= $content ?>

<div id="rowSelectSummary" class="wrap wrap_btns check-condition">
    <div class="row align-items-center justify-content-end">
        <div class="column flex-shrink-0">
            <span class="checkbox-txt total-txt-foot ml-3" style="padding-left: 6px">
                Выбрано сделок: <strong class="pl-2 total-count">0</strong>
            </span>
        </div>

        <div class="column flex-shrink-0">
            <span class="checkbox-txt total-txt-foot ml-3">
                Сумма сделок: <strong class="pl-2 total-sum">0</strong>
            </span>
        </div>

        <div class="column ml-auto"></div>
        <?php if ($form->getUser()->can(DealAccess::EDIT)): ?>
        <div class="column">
            <div class="dropup">
                <?= Html::button('<span class="pr-2">Изменить</span>  '.$this->render('//svg-sprite', ['ico' => 'shevron']), [
                    'class' => 'button-regular button-regular-more button-hover-transparent button-clr dropdown-toggle',
                    'data-toggle' => 'dropdown',
                ]) ?>
                <?= Dropdown::widget([
                    'items' => array_map(function (array $dialog) {
                        return [
                            'label' => $dialog['button'],
                            'url' => "#{$dialog['id']}",
                            'linkOptions' => ['data-toggle' => 'modal'],
                            'visible' => $dialog['enabled'],
                        ];
                    }, $dialogs),
                    'options' => ['class' => 'form-filter-list list-clr dropdown-menu-right'],
                ]) ?>
            </div>
        </div>
        <div class="column">
            <?= GridButtonWidget::widget([
                'label' => 'Удалить',
                'icon' => 'garbage',
                'options' => [
                    'class' => 'button-clr button-regular button-width button-hover-transparent',
                    'data-toggle' => 'modal',
                    'data-target' => '#deleteDialog',
                ],
            ]) ?>
        </div>
        <?php endif; ?>
    </div>
</div>

<?php Modal::begin([
    'options' => ['id' => 'deleteDialog'],
    'headerOptions' => ['class' => 'text-center'],
    'closeButton' => false,
]); ?>


<h4 class="modal-title text-center">
    Вы действительно хотите удалить сделки?
</h4>

<div class="form-group text-center mb-0">
    <?= Html::submitButton('Да', [
        'name' => Html::getInputName($form, 'delete'),
        'value' => 1,
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2',
    ]) ?>

    <?= Html::button('Нет', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-2',
        'data-dismiss' => 'modal',
    ]) ?>
</div>

<?php Modal::end(); ?>

<?php foreach ($dialogs as $dialog): ?>
    <?= $this->render('@frontend/modules/crm/views/widgets/row-select-dialog', $dialog) ?>
<?php endforeach; ?>

<?= Html::endForm() ?>

<?php

$this->registerJs(<<<JS
    (function () {
       $(document).on('change', '#rowSelectForm [type="checkbox"]', function () {
            let sum = 0;
    
            $('#rowSelectForm [type="checkbox"][data-amount]:checked').each(function () {
                var checkBox = $(this);
    
                sum += parseFloat($(this).data('amount'));
            });

            $('#rowSelectSummary .total-sum').text(number_format(sum, 2, ',', ' '));
        });
    })();
JS);

?>
