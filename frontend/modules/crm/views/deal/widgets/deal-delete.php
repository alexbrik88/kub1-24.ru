<?php

namespace frontend\modules\crm\views;

use frontend\modules\crm\models\Deal;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\web\View;

/**
 * @var View $this
 * @var Deal $deal
 */

$this->title = sprintf('Вы действительно хотите удалить сделку «%s»?', $deal->name);

?>

<h4 class="modal-title text-center"><?= Html::encode($this->title) ?></h4>

<?php ActiveForm::begin([
    'id' => 'typeForm',
    'fieldConfig' => [
        'labelOptions' => ['class' => 'label'],
        'options' => ['class' => 'form-group col-12'],
    ],
    'options' => [
        'data-pjax' => true,
    ],
]); ?>

<div class="form-group text-center mb-0">
    <?= Html::submitButton('Да', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2',
    ]) ?>

    <?= Html::button('Нет', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-2',
        'data-dismiss' => 'modal',
    ]) ?>
</div>

<?php ActiveForm::end(); ?>
