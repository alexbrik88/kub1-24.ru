<?php

namespace frontend\modules\crm\views;

use common\models\Contractor;
use frontend\modules\crm\models\DealForm;
use frontend\modules\crm\models\ListFactory;
use frontend\modules\crm\widgets\ButtonListFactory;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\ContractorDropdown;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/**
 * @var View $this
 * @var DealForm $form
 */

$this->title = $form->getDeal()->isNewRecord
    ? 'Добавить сделку № ' . $form->nextNumber
    : 'Редактировать сделку ' . $form->getDeal()->name;

$dataUrl = Url::to(['clients/create', 'redirect' => 0, 'deal_type_id' => $form->getDealType()->deal_type_id]);

?>

<h4 class="modal-title"><?= Html::encode($this->title) ?></h4>

<?php $widget = ActiveForm::begin([
    'id' => 'dealForm',
    'action' => $form->getDeal()->isNewRecord
        ? ['deal/create', 'deal_type_id' => $form->getDealType()->deal_type_id]
        : ['deal/update', 'deal_id' => $form->getDeal()->deal_id],
    'fieldConfig' => [
        'labelOptions' => ['class' => 'label'],
        'options' => ['class' => 'form-group col-6'],
    ],
    'options' => [
        'data-pjax' => true,
    ],
]); ?>

<div class="row">
    <?= $widget->field($form, 'name')->textInput() ?>

    <?= $widget->field($form, 'employee_id')->widget(Select2::class, [
        'readonly' => !$form->canChangeEmployee,
        'disabled' => !$form->canChangeEmployee,
        'data' => (new ListFactory)->createEmployeeList()->getItems(),
        'pluginOptions' => ['width' => '100%'],
    ]) ?>

    <?= $widget->field($form, 'deal_stage_id')->widget(Select2::class, [
        'data' => (new ListFactory)->createDealStageList($form->getDealType())->getItems(),
        'pluginOptions' => [
            'width' => '100%',
        ],
    ]) ?>

    <?= $widget->field($form, 'amount', ['options' => ['class' => 'form-group col-4']])->textInput() ?>

    <?= $widget->field($form, 'currency_id', ['options' => ['class' => 'form-group col-2']])->widget(Select2::class, [
        'readonly' => true,
        'disabled' => true,
        'data' => (new ListFactory)->createCurrencyList()->getItems(),
        'pluginOptions' => [
            'width' => '100%',
        ],
    ]) ?>

    <?= $widget->field($form, 'contractor_id')->widget(ContractorDropdown::class, [
        'staticData' => [
            'add-client-button' => Html::tag('b', Icon::PLUS . ' Добавить клиента ', ['data-url' => $dataUrl]),
        ],
        'addRequestParams' => ['_type' => Contractor::TYPE_POTENTIAL_CLIENT],
        'contractorType' => Contractor::TYPE_CUSTOMER,
        'options' => ['placeholder' => '', 'class' => 'js-contractor-select'],
        'pluginOptions' => [
            'width' => '100%',
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        ],
    ]) ?>

    <?= $widget->field($form, 'contact_id')->widget(Select2::class, [
        'data' => $form->getDeal()->isNewRecord
            ? []
            : (new ButtonListFactory($this))->createContactList($form->getDeal()->contractor)->getItems(),
        'options' => ['placeholder' => '', 'class' => 'js-contact-select'],
        'hideSearch' => true,
        'pluginOptions' => [
            'width' => '100%',
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'language' => [
                'noResults' => new JsExpression('function () { return "Сначало нужно выбрать клиента"; }'),
            ],
        ],
    ]) ?>

    <?= $widget->field($form, 'comment', ['options' => ['class' => 'form-group col-12']])->textarea(['rows' => 3]) ?>
</div>

<?= $this->render('@frontend/modules/crm/views/widgets/dialog-buttons') ?>

<?php ActiveForm::end(); ?>

<?php

// Загрузить список контактов для выбранного в списке контрагента, и установить выбранным первый контакт в списке

$this->registerJs(<<<JS
    (function() {
        $('.js-contractor-select').on('change', function () {
            const contractor_id = $(this).val();
            const select = $('.js-contact-select');
    
            select.find('option:not([value=""])').remove();
            select.val(null).trigger('change');
    
            if (contractor_id.length > 0) {
                $.ajax({
                    url: '/crm/contact/dropdown?contractor_id=' + contractor_id
                }).done(function(data) {
                    $.each(data, function(index, item) {
                        let option = new Option(item.text, item.id, false, false);
                        select.append(option);
                    });
                    
                    const value = select.find('option:not([value=""]):not([value="add-item-button"])').first().val();
                    
                    if (value) {
                        select.val(value).trigger('change');
                    }
                });
            }
    
            return true;
        });
    })();
JS);

// Удалить из списка кнопку "Довавить контакт"

$this->registerJs(<<<JS
    (function() {
        const TYPE_PHYSICAL_PERSON = 1;
        const CONTACT_COUNT = 3;
    
        $('.js-contact-select').on('type:added', function () {
            const faceType = $('.js-contractor-select option:selected').data('face_type');
            const countContact = $(this).find('option:not([value=""]):not([value="add-item-button"])').length;
            
            if (faceType !== undefined) {
                if (parseInt(faceType) === TYPE_PHYSICAL_PERSON && countContact > 0) {
                    $(this).find('option[value="add-item-button"]').remove();
                    $(this).trigger('change');
                }

                if (parseInt(faceType) !== TYPE_PHYSICAL_PERSON && countContact >= CONTACT_COUNT) {
                    $(this).find('option[value="add-item-button"]').remove();
                    $(this).trigger('change');
                }
            }
        });
    })();
JS);

?>

<?php

$this->registerCss(<<<CSS
    .select2-container--krajee-bs4 .select2-results > #select2-dealform-deal_stage_id-results.select2-results__options {
        max-height: 270px;
        overflow-y: auto;
    }
CSS);

?>
