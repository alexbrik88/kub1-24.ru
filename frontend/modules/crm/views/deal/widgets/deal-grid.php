<?php

namespace frontend\modules\crm\views;

use common\components\date\DateHelper;
use common\components\grid\GridView;
use frontend\components\WebUser;
use frontend\modules\crm\models\Contact;
use frontend\modules\crm\models\Deal;
use frontend\modules\crm\models\DealFilter;
use frontend\modules\crm\models\DealSelectForm;
use frontend\modules\crm\models\DealType;
use frontend\modules\crm\widgets\DataColumnWidget;
use frontend\rbac\permissions\crm\DealAccess;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\i18n\Formatter;
use yii\web\View;
use yii\widgets\ContentDecorator;

/**
 * @var View $this
 * @var WebUser $user
 * @var DealType|null $dealType
 * @var DealFilter $filter
 * @var DealSelectForm $form
 */

$dataProvider = $filter->getDataProvider();
$canCreate = $user->can(DealAccess::EDIT);
$emptyText = 'По заданным фильтрам сделок не найдено. ' . Html::a(
    'Очистить',
    ['index', 'deal_type_id' => $dealType->deal_type_id],
);

$count = $filter->getTotalCount();

if ($count == 0) {
    $emptyText = 'Вы ещё не добавили ни одной сделки. ';

    if ($canCreate) {
        $emptyText .= Html::a('Добавить сделку', '#', [
            'class' => 'import-dialog',
            'data-url' => Url::to(['create', 'deal_type_id' => $dealType->deal_type_id]),
        ]);
    }
}

?>

<?php ContentDecorator::begin([
    'viewFile' => __DIR__ . '/../decorators/deal-select.php',
    'params' => [
        'form' => $form,
        'dealType' => $dealType,
    ],
]); ?>

<div class="table-container products-table clearfix">
    <?= GridView::widget([
        'id' => 'product-grid',
        'emptyText' => $emptyText,
        'filterModel' => $filter,
        'dataProvider' => $dataProvider,
        'dataColumnClass' => DataColumnWidget::class,
        'configAttribute' => 'crm_deal',
        'formatter' => ['class' => Formatter::class, 'nullDisplay' => ''],
        'tableOptions' => [
            'class' => 'table table-style table-count-list',
            'id' => 'datatable_ajax',
            'aria-describedby' => 'datatable_ajax_info',
            'role' => 'grid',
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->getTotalCount()]),
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'name' => 'DealSelectForm[deal_id]',
                'visible' => $canCreate,
                'headerOptions' => [
                    'style' => 'width: 20px;',
                ],
                'checkboxOptions' => function (Deal $deal) {
                    return [
                        'value' => $deal->deal_id,
                        'data-amount' => (float) $deal->amount / 100,
                    ];
                },
            ],
            [
                'label' => 'Дата',
                'attribute' => 'created_at',
                'value' => 'created_at',
                'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
                'headerOptions' => [
                    'style' => 'width: 1%;',
                ],
            ],
            [
                'label' => 'Название сделки',
                'attribute' => 'name',
                'contentOptions' => function (Deal $deal): array {
                    return [
                        'class' => 'contact-cell',
                        'title' => $deal->name,
                    ];
                },
                'format' => 'raw',
                'value' => function (Deal $deal) use ($user): string {
                    $name = Html::encode($deal->name);

                    if ($user->can(DealAccess::EDIT, ['contractor' => $deal->contractor])) {
                        return Html::a($name, '#', [
                            'class' => 'text-decoration-none import-dialog',
                            'data-url' => Url::to(['update', 'deal_id' => $deal->deal_id]),
                            'data-pjax' => 0,
                        ]);
                    }

                    return $name;
                }
            ],
            [
                'label' => 'ФИО контакта',
                'attribute' => 'contact_name',
                'contentOptions' => function (Deal $deal): array {
                    $contact = $deal->contact ?: Contact::createDefaultContact($deal->contractor);

                    return [
                        'class' => 'contact-cell',
                        'title' => $contact->contact_name,
                    ];
                },
                'value' => function (Deal $deal): string {
                    $contact = $deal->contact ?: Contact::createDefaultContact($deal->contractor);

                    return $contact->contact_name;
                },
                'filter' => ['' => 'Все'] + $filter->getListFactory()->createContactList()->getItems(),
                's2width' => '200px',
                'enableSorting' => false,
                'hideSearch' => false,
            ],
            [
                'label' => 'Компания',
                'attribute' => 'contractor_id',
                'format' => 'raw',
                's2width' => '200px',
                'enableSorting' => false,
                'hideSearch' => false,
                'filter' => ['' => 'Все'] + $filter->getListFactory()->createContractorList()->getItems(),
                'value' => function (Deal $deal): string {
                    return Html::a(
                        $deal->contractor->getNameWithType(),
                        ['client/tasks', 'contractor_id' => $deal->contractor->id],
                        ['data-pjax' => 0],
                    );
                }
            ],
            [
                'label' => 'Этап сделки',
                'attribute' => 'deal_stage_id',
                'value' => 'stage.name',
                'contentOptions' => function (Deal $deal): array {
                    return [
                        'class' => 'contact-cell',
                        'title' => $deal->stage->name,
                    ];
                },
                'headerOptions' => [
                    'style' => 'width: 1%;',
                ],
                'filter' => ['' => 'Все'] + $filter->getListFactory()->createDealStageList()->getItems(),
                's2width' => '200px',
                'enableSorting' => false,
                'hideSearch' => false,
            ],
            [
                'label' => 'Дней на этапе',
                'attribute' => 'staged_at',
                'value' => 'stageDayCount',
                'headerOptions' => [
                    'style' => 'min-width: 100px; width: 1%;',
                ],
            ],
            [
                'label' => 'Сумма',
                'attribute' => 'amount',
                'value' => 'amountWithCurrency',
                'contentOptions' => [
                    'class' => 'nowrap',
                ],
                'headerOptions' => [
                    'style' => 'width: 1%;',
                ],
            ],
            [
                'label' => 'Ответственный',
                'attribute' => 'employee_id',
                'value' => 'employeeCompany.shortFio',
                'filter' => ['' => 'Все'] + $filter->getListFactory()->createEmployeeList()->getItems(),
                'headerOptions' => [
                    'style' => 'width: 1%;',
                ],
                's2width' => '200px',
                'enableSorting' => false,
                'hideSearch' => false,
            ],
        ],
    ]) ?>
</div>

<?php ContentDecorator::end() ?>
