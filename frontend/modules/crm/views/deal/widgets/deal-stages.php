<?php

namespace frontend\modules\crm\views;

use common\components\helpers\Html;
use common\components\helpers\Url;
use frontend\modules\cash\widgets\Statistic;
use frontend\modules\crm\models\DealFilter;
use frontend\modules\crm\models\DealType;
use frontend\modules\crm\models\ListFactory;
use Yii;
use yii\web\View;

/**
 * @var View $this
 * @var DealFilter $filter
 * @var DealType $dealType
 */

$params = Yii::$app->getRequest()->getQueryParams();
$params[0] = 'index';
$items = [];
$classes = ['count-card_yellow', 'count-card_turquoise', 'count-card_red'];
$textClasses = ['_yellow_statistic', '_green_statistic', '_red_statistic'];
$summary = $filter->getSummary();
$index = 0;

foreach ((new ListFactory)->createDealStageList($dealType)->getItems() as $stageId => $stageName) {
    $params[$filter->formName()]['deal_stage_id'] = $stageId;
    $items[] = [
        'class' => $classes[$index % 3],
        'text' => $stageName,
        'amount' => $summary->getAmount($stageId),
        'statistic_text' => sprintf('Кол-во сделок: %s', $summary->getCount($stageId)),
        'statistic_text_class' => $textClasses[$index % 3],
        'dataValue' => Url::to($params),
    ];

    $index++;
}

?>

<?php if ($dealType->isNewRecord): ?>
<div class="wrap">
    <div class="row">
        <div class="col-12">
            <div class="text">
                Сначала необходимо <?= Html::a('добавить', ['deal-type/create'], ['data-pjax' => 0]) ?> хотя бы один тип сделки
            </div>
        </div>
    </div>
</div>
<?php else: ?>
<div class="wrap wrap_count testimonial-group">
    <div class="row flex-nowrap">
        <?= Statistic::widget(['items' => $items]) ?>
    </div>
</div>
<?php endif; ?>

<?php $this->registerJs(<<<JS
    (function () {
        $('.count-card[data-value]').on('click', function () {
            const value = $(this).data('value');

            window.location.assign(value);
            
            return false;
        });
    })();
JS) ?>

<?php
// https://coderoad.ru/47733537/Bootstrap-4-горизонтальный-скроллер-div#47793060
$this->registerCss(<<<CSS

.testimonial-group > .row {
    overflow-x: auto;
    white-space: nowrap;
}

.testimonial-group > .row > .col-6 {
    display: inline-block;
    float: none;
}

.count-card-column > .count-card[data-value] {
    cursor: pointer;
}

CSS);

?>
