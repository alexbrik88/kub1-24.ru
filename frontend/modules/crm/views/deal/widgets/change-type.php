<?php

namespace frontend\modules\crm\views;

use frontend\modules\crm\models\DealType;
use frontend\modules\crm\models\ListFactory;
use frontend\rbac\permissions\crm\DealTypeAccess;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\User;
use yii\web\View;

/**
 * @var View $this
 * @var User $user
 * @var DealType $dealType
 */

$items = (new ListFactory)->createDealList()->getItems();

?>

<div class="h4">
    <div class="dropdown popup-dropdown popup-dropdown_storage">
        <a class="link link_title" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <span class="acquiring-label"><?= Html::encode($this->title) ?></span>
            <svg class="svg-icon">
                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
            </svg>
        </a>
        <div class="dropdown-menu" aria-labelledby="cardProductTitle">
            <div class="popup-dropdown-in" id="user-bank-dropdown">
                <?php foreach ($items as $deal_type_id => $name): ?>
                    <div class="form-edit form-edit_alternative" data-id="<?= $deal_type_id ?>">
                        <div class="form-edit-result hide show" data-id="form-edit">
                            <div class="weight-400">
                                <a href="<?= Url::to(['index', 'deal_type_id' => $deal_type_id]) ?>" class="goto-cashbox black-link" data-pjax="0">
                                    <?= Html::encode($name) ?>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                <hr class="popup-devider">
                <div class="popup-content pt-3 pb-3">
                    <?php if ($user->can(DealTypeAccess::CREATE)) : ?>
                        <a href="<?= Url::to(['deal-type/create']) ?>" class="black-link link_bold link_font-14 link_decoration_none nowrap button-clr" data-pjax="0" style="cursor: pointer;">
                            <svg class="add-button-icon svg-icon">
                                <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                            </svg>
                            <span class="ml-2">Добавить тип сделки</span>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
