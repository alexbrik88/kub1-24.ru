<?php

namespace frontend\modules\crm\views;

use frontend\modules\crm\models\DealFilter;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View $this
 * @var DealFilter $filter
 * @var string $placeholder
 */

?>

<?php $widget = ActiveForm::begin([
    'id' => 'searchFilterForm',
    'method' => 'GET',
    'action' => Url::current([$filter->formName() => null]),
    'options' => [
        'data-pjax' => true,
    ],
    'fieldConfig' => [
        'labelOptions' => ['class' => 'label'],
        'options' => ['class' => 'form-group col-6 mb-3'],
    ],
]) ?>

<div class="table-settings row row_indents_s">
    <div class="col-6">
        <div class="row align-items-center">
            <div class="column flex-grow-1 d-flex flex-wrap justify-content-end">
                <?= TableViewWidget::widget(['attribute' => 'crm_deal']) ?>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="d-flex flex-nowrap align-items-center">
            <?= $widget->field($filter, 'search', [
                'options' => ['class' => 'form-group flex-grow-1 mr-2'],
            ])->textInput([
                'placeholder' => $placeholder,
                'type' => 'search',
            ])->label(false) ?>

            <div class="form-group">
                <?= Html::submitButton('Найти', [
                    'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                ]) ?>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end() ?>
