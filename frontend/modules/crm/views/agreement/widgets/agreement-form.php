<?php

namespace frontend\modules\crm\views;

use frontend\modules\crm\models\AgreementForm;
use frontend\modules\crm\models\ListFactory;
use frontend\themes\kub\widgets\file\FileUpload;
use kartik\select2\Select2;
use Yii;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var AgreementForm $form
 */

$datePickerTemplate = <<<HTML
{label}
<div class="date-picker-wrap">
    {input}
    <svg class="date-picker-icon svg-icon input-toggle"><use xlink:href="/img/svg/svgSprite.svg#calendar"></use></svg>
    {error}
</div>
HTML;

$onTemplateOptions = ['options' => ['class' => 'form-group col-6 on-template']];

?>

<?php $widget = ActiveForm::begin([
    'id' => 'agreementForm',
    'action' => ($form->agreement->is_created)
        ? ['agreement/update', 'contractor_id' => $form->contractor->id, 'agreement_id' => $form->agreement->id]
        : ['agreement/create', 'contractor_id' => $form->contractor->id],
    'fieldConfig' => [
        'labelOptions' => ['class' => 'label'],
        'options' => ['class' => 'form-group col-6'],
    ],
    'options' => [
        'data-pjax' => true,
    ],
]); ?>

<div class="row">
    <?= $widget->field($form, 'create_agreement_from', [
        'checkTemplate' => "{label}\n<div>{input}</div>",
    ])->checkbox([
        'labelOptions' => ['class' => 'label'],
        'class' => '', // Иначе чекбокс не меняет состояние
    ]) ?>

    <?= $widget->field($form, 'agreement_template_id', $onTemplateOptions)->widget(Select2::class, [
        'data' => (new ListFactory)->createAgreementTemplateList()->getItems(),
        'pluginOptions' => [
            'width' => '100%',
            'placeholder' => '',
        ],
    ]) ?>
</div>

<div class="row">
    <?= $widget->field($form, 'document_type_id')->widget(Select2::class, [
        'data' => (new ListFactory)->createAgreementTypeList()->getItems(),
        'pluginOptions' => [
            'width' => '100%',
            'placeholder' => '',
        ],
    ]) ?>

    <?= $widget->field($form, 'document_name')->textInput() ?>
</div>

<div class="row">
    <?= $widget->field($form, 'document_number')->textInput() ?>

    <?= $widget->field($form, 'document_date', ['template' => $datePickerTemplate])->textInput([
        'class' => 'form-control date-picker',
        'data-date-viewmode' => 'years',
    ]) ?>
</div>

<div class="row on-template">
    <?= $widget->field($form, 'checking_accountant_id')->widget(Select2::class, [
        'data' => (new ListFactory)->createCheckingAccountantList()->getItems(),
        'pluginOptions' => ['width' => '100%'],
    ]) ?>

    <?= $widget->field($form, 'payment_limit_date', ['template' => $datePickerTemplate])->textInput([
        'class' => 'form-control date-picker',
        'data-date-viewmode' => 'years',
    ]) ?>
</div>

<div class="row">
    <div class="form-group col-6">
        <label class="label" for="file">Загрузить скан/файл документа</label>
        <div id="agreement_upload_file" style="position:relative">
            <?= FileUpload::widget([
                'uploadUrl' => Url::to(['/documents/agreement/agreement-file-upload', 'id' => $form->agreement->id]),
                'deleteUrl' => Url::to(['/documents/agreement/agreement-file-delete', 'id' => $form->agreement->id]),
                'listUrl' => Url::to(['/documents/agreement/agreement-file-list', 'id' => $form->agreement->id]),
            ]); ?>
        </div>
    </div>
</div>

<div class="row mt-4">
    <div class="form-group col-6 text-left mb-0">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-width button-regular_red button-clr',
            'data-style' => 'expand-right',
        ]) ?>
    </div>

    <div class="form-group col-6 text-right mb-0">
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'data-dismiss' => 'modal'
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php $this->registerJs($this->renderFile(Yii::getAlias('@common/assets/web/scripts/upload.js'))); ?>

<?php $this->registerJs(<<<JS
$(document).ready(function () {
    $(document).on('change', '#agreementForm [name$="[create_agreement_from]"]', function () {
        var form = $(this).closest('form').first();
        var checked = $(this).prop('checked');

        if (checked) {
            form.find('.on-template').removeClass('d-none');
        } else {
            form.find('.on-template').addClass('d-none');
        }
    });

    $('#agreementForm [name$="[create_agreement_from]"]').trigger('change');
});
JS) ?>

<?php $this->assetBundles = []; ?>
