<?php

use yii\bootstrap4\Nav;

$this->beginContent('@frontend/views/layouts/main.php');

?>

<div class="page-head d-flex flex-wrap align-items-center">
    <h4>Отчеты</h4>
</div>

<div class="crm-report-content">
    <div class="nav-tabs-row mb-2">
        <?= Nav::widget([
            'id' => 'debt-report-menu',
            'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
            'items' => [
                [
                    'label' => 'Отказы',
                    'url' => ['/crm/reports/refusal'],
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
            ],
        ]);?>
    </div>
    <div class="">
        <?= $content; ?>
    </div>
</div>
<?php $this->endContent(); ?>