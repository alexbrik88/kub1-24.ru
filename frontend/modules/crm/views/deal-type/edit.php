<?php

namespace frontend\modules\crm\views;

use frontend\modules\crm\models\DealTypeForm;
use frontend\modules\crm\widgets\PjaxWidget;
use yii\web\View;

/**
 * @var View $this
 * @var DealTypeForm $form
 * @var bool $statusDisabled
 */

$this->title = sprintf(
    '%s %s',
    $form->scenario == $form::SCENARIO_CREATE ? 'Добавить' : 'Редактировать',
    'тип сделки (воронку продаж)',
);

?>

<?php PjaxWidget::begin(['id' => 'pjaxContent']) ?>

<?= $this->render('widgets/edit-form', [
    'form' => $form,
    'statusDisabled' => $statusDisabled,
]) ?>

<?php PjaxWidget::end(); ?>
