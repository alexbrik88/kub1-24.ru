<?php

namespace frontend\modules\crm\views;

use common\components\date\DateHelper;
use common\components\grid\GridView;
use frontend\modules\crm\models\DealType;
use frontend\modules\crm\models\TypeFilter;
use frontend\modules\crm\models\AbstractType;
use frontend\modules\crm\widgets\GridButtonWidget;
use frontend\themes\kub\widgets\SpriteIconWidget;
use yii\bootstrap4\Html;
use yii\grid\ActionColumn;
use yii\grid\SerialColumn;
use yii\helpers\Url;
use yii\i18n\Formatter;
use yii\web\User;
use yii\web\View;

/**
 * @var View $this
 * @var TypeFilter $filter
 * @var User $user
 * @var string $createAccess
 * @var string $editAccess
 */

$this->title = 'Типы сделок (воронок)';

?>

<div class="page-head d-flex flex-wrap align-items-center mb-2">
    <h4><?= Html::encode($this->title); ?></h4>

    <?= GridButtonWidget::widget([
        'accessName' => $createAccess,
        'url' => Url::to(['create']),
        'label' => 'Добавить',
        'icon' => 'add-icon',
        'options' => [
            'class' => 'button-regular button-regular_red pl-3 pr-3 ml-auto',
            'data-pjax' => '0',
        ],
    ]); ?>
</div>

<div class="table-container products-table clearfix">
<?= GridView::widget([
    'id' => 'product-grid',
    'dataProvider' => $filter->getDataProvider(),
    'filterModel' => $filter,
    'formatter' => ['class' => Formatter::class, 'nullDisplay' => ''],
    'tableOptions' => [
        'class' => 'table table-style table-count-list',
        'id' => 'grid',
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],
    'headerRowOptions' => [
        'class' => 'heading',
    ],
    'options' => [
        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
    ],
    'rowOptions' => [
        'role' => 'row',
    ],
    'pager' => [
        'options' => [
            'class' => 'pagination pull-right',
        ],
    ],
    'layout' => <<<HTML
        <div class="wrap wrap_padding_none">
            <div class="custom-scroll-table">
                <div class="table-wrap">
                    {items}
                </div>
            </div>
        </div>
    HTML,
    'columns' => [
        [
            'class' => SerialColumn::class,
            'contentOptions' => ['class' => 'serial-column'],
            'headerOptions' => [
                'style' => 'width: 1px;',
            ],
        ],
        [
            'attribute' => 'created_at',
            'headerOptions' => [
                'style' => 'width: 1px;',
            ],
            'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
            'label' => 'Дата создания',
        ],
        [
            'headerOptions' => [
                'class' => 'max-width',
            ],
            'attribute' => 'name',
            'label' => 'Название',
            'format' => 'raw',
            'value' => function(DealType $deal): string {
                return Html::a($deal->name, ['view', 'id' => $deal->deal_type_id], ['data-pjax' => 0]);
            },
        ],
        [
            'headerOptions' => [
                'style' => 'width: 1px;',
            ],
            'attribute' => '#',
            'label' => 'Количество этапов',
            'value' => function (DealType $deal): ?int {
                return $deal->getStagesCount();
            },
        ],
        [
            'headerOptions' => [
                'style' => 'width: 1px;',
            ],
            'attribute' => 'status',
            'label' => 'Статус',
            'format' => function (int $status) {
                return AbstractType::TYPE_STATUS_LIST[$status];
            },
            'filter' => ['' => 'Все'] + $filter->getListFactory()->createStatusList()->getItems(),
            's2width' => '200px',
        ],
        [
            'contentOptions' => [
                'class' => 'text-nowrap',
            ],
            'headerOptions' => [
                'style' => 'min-width: 20%;',
            ],
            'attribute' => 'employee_id',
            'value' => 'employeeCompany.shortFio',
            'label' => 'Ответственный',
            'filter' => ['' => 'Все'] + $filter->getListFactory()->createEmployeeList()->getItems(),
            's2width' => '200px',
            'hideSearch' => false,
        ],
        [
            'class' => ActionColumn::class,
            'template' => '{update}{delete}',
            'contentOptions' => [
                'class' => 'text-nowrap',
            ],
            'headerOptions' => [
                'style' => 'width: 1px;',
            ],
            'visible' => $user->can($editAccess),
            'buttons' => [
                'update' => function (string $url, AbstractType $model, int $id) use ($editAccess): string {
                    return GridButtonWidget::widget([
                        'accessName' => $editAccess,
                        'url' => Url::to(['update', 'id' => $id]),
                        'icon' => 'pencil',
                        'options' => [
                            'title' => 'Редактировать',
                            'class' => 'button-clr link mr-1',
                            'data-pjax' => '0',
                        ],
                    ]);
                },
                'delete' => function (string $url, AbstractType $type, int $id) use ($editAccess): string {
                    if (!$type->canDelete) {
                        return Html::button(
                            SpriteIconWidget::widget([
                                'icon' => 'garbage',
                                'options' => ['class' => 'svg-icon icon-disabled'],
                            ]),
                            [
                                'class' => 'button-clr link ml-1',
                                'title' => 'Удалить нельзя, так как этот тип сделки задействован. Вы можете нажать на редактировать и поменять "Используется" на "Архив"',
                                'disabled' => true,
                            ]
                        );
                    }

                    return GridButtonWidget::widget([
                        'accessName' => $editAccess,
                        'icon' => 'garbage',
                        'options' => [
                            'title' => 'Удалить',
                            'class' => 'button-clr link ml-1 import-dialog',
                            'data-url' => Url::to(['delete', 'id' => $id]),
                        ],
                    ]);
                },
            ],
        ],
    ],
]) ?>
</div>
