<?php

namespace frontend\modules\crm\views;

use common\components\date\DateHelper;
use frontend\components\WebUser;
use frontend\modules\crm\models\DealType;
use frontend\modules\crm\widgets\GridButtonWidget;
use frontend\rbac\permissions\crm\DealTypeAccess;
use frontend\themes\kub\components\Icon;
use frontend\themes\kub\widgets\SpriteIconWidget;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\web\View;

/**
 * @var View $this
 * @var WebUser $user
 * @var DealType $deal
 */

$canEdit = $user->can(DealTypeAccess::EDIT);
$stageNumber = 0;

?>

<?= Html::a('Назад к списку', ['index'], ['class' => 'link mb-2', 'data-pjax' => 0]) ?>

<div class="wrap wrap_padding_small pl-4 pr-3 pb-3">
    <div class="pl-1 pb-1">
        <div class="page-in row">
            <div class="col-9 column pr-4">
                <div class="pr-2">
                    <div class="row align-items-center justify-content-between mb-3">
                        <h4 class="column mb-2" style="max-width: 550px;"><?= Html::encode($deal->name); ?></h4>
                        <div class="column" style="margin-bottom: auto;">
                            <?= GridButtonWidget::widget([
                                'icon' => 'info',
                                'options' => [
                                    'title' => 'Последние действия',
                                    'class' => 'button-regular button-regular_red button-clr w-44 mb-2 mr-2',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#lastActions',
                                ],
                            ]) ?>

                            <?php if ($canEdit): ?>
                                <?= Html::a(
                                    SpriteIconWidget::widget(['icon' => 'pencil']),
                                    ['update', 'id' => $deal->deal_type_id],
                                    [
                                        'title' => 'Редактировать',
                                        'class' => 'button-regular button-regular_red button-clr w-44 mb-2 mr-2',
                                        'data-pjax' => 0,
                                    ]
                                ) ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="column">
                            <ul class="sidebar-menu list-clr">
<?php foreach ($deal->stages as $stage): ?>
                                <li class="sideabar-menu-item active">
                                    <span class="sidebar-menu-icon svg-icon text-center deal-stage-number">
                                        <?= (++$stageNumber) ?>
                                    </span>
                                    <span class="sidebar-menu-btn-txt"><?= Html::encode($stage->name) ?></span>
                                </li>
<?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-3 column pl-0">
                <div class="pb-2 mb-1">
                    <?php if ($deal->getStatus() == $deal::TYPE_STATUS_ACTIVE): ?>
                    <button
                        type="button"
                        onclick="return false;"
                        class="button-regular button-hover-content-red button-clr w-100 deal-status-active">
                        <?= SpriteIconWidget::widget(['icon' => 'check-2']) ?>
                        <span class="ml-1"><?= Html::encode($deal->getStatusLabel()) ?></span>
                    </button>
                    <?php endif; ?>

                    <?php if ($deal->getStatus() == $deal::TYPE_STATUS_INACTIVE): ?>
                    <button
                        type="button"
                        onclick="return false;"
                        class="button-regular button-hover-content-red button-clr w-100 deal-status-inactive">
                        <?= SpriteIconWidget::widget(['icon' => 'close']) ?>
                        <span class="ml-1"><?= Html::encode($deal->getStatusLabel()) ?></span>
                    </button>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    'id' => 'lastActions',
    'title' => 'Последние действия',
    'options' => [
        'class' => 'doc-history-modal fade',
    ],
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
    'footer' => Html::button('OK', [
        'class' => 'button-regular button-regular_red button-clr w-44',
        'data-dismiss' => 'modal',
    ]),
]) ?>

<div class="created-by">
    <?= DateHelper::format($deal->created_at, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATETIME) ?>
    Создал
    <?= $deal->employeeCompany ? $deal->employeeCompany->getFio() : ''; ?>
</div>

<?php Modal::end(); ?>

<?php

$this->registerCss(<<<CSS

.deal-stage-number {
    color: #4679AE;
    font-weight: 400;
    font-size: 14px;
    line-height: 1.6em;
    border: 2px solid #4679AE;
    border-radius: 1em;
    vertical-align: middle;
    width: 2em;
    height: 2em;
}

.deal-status-active *, .deal-status-inactive * {
    color: #ffffff !important;
}

.deal-status-active {
    background-color: #26cd58;
    border-color: #26cd58;
    color: #ffffff;
}

.deal-status-inactive {
    background-color: #e30611;
    border-color: #e30611;
    color: #ffffff;
}

CSS);

$this->registerJs(<<<JS
    $(document).ready(function() {
        initMain();
    });
JS);
