<?php

namespace frontend\modules\crm\views;

use frontend\modules\crm\models\DealStageForm;
use frontend\themes\kub\widgets\SpriteIconWidget;
use yii\bootstrap4\ActiveForm;
use yii\web\View;

/**
 * @var View $this
 * @var DealStageForm $form
 * @var ActiveForm $widget
 * @var int $index
 */

?>

<tr>
    <td class="text-center">
        <button class="button-clr link js-stage-up">
            <?= SpriteIconWidget::widget(['icon' => 'arrow', 'options' => ['class' => 'svg-icon rotate-180']]) ?>
        </button>
        <br>
        <button class="button-clr link js-stage-down">
            <?= SpriteIconWidget::widget(['icon' => 'arrow']) ?>
        </button>
    </td>
    <td>
        <?= $widget->field($form, "[{$index}]id", ['template' => "{input}"])->hiddenInput([
            'id' => "stage-id-{$index}",
            'value' => $form->id,
        ]) ?>
        <?= $widget->field($form, "[{$index}]name", ['template' => "{input}"])->textInput([
            'id' => "stage-name-{$index}",
            'value' => $form->name,
        ]) ?>
    </td>
    <td>
        <?php if ($form->type->isNewRecord || $form->type->canDelete): ?>
        <button class="button-clr link js-stage-delete" title="Удалить этап">
            <?= SpriteIconWidget::widget(['icon' => 'garbage']) ?>
        </button>
        <?php else: ?>
        <button disabled class="button-clr link" title="Удалить этот этап нельзя, так как он задействован">
            <?= SpriteIconWidget::widget(['icon' => 'garbage', 'options' => ['class' => 'svg-icon icon-disabled']]) ?>
        </button>
        <?php endif; ?>
    </td>
</tr>
