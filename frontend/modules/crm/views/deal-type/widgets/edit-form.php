<?php

namespace frontend\modules\crm\views;

use frontend\modules\crm\models\DealTypeForm;
use frontend\modules\crm\models\AbstractType;
use frontend\themes\kub\widgets\SpriteIconWidget;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\web\View;

/**
 * @var View $this
 * @var DealTypeForm $form
 * @var bool $statusDisabled
 */

$redirectUrl = ($form->scenario == $form::SCENARIO_CREATE)
    ? ['index']
    : ['view', 'id' => $form->type->deal_type_id];

?>

<?php $widget = ActiveForm::begin([
    'id' => 'clientDialForm',
    'enableClientValidation' => false,
    'fieldConfig' => [
        'labelOptions' => ['class' => 'label'],
        'options' => ['class' => 'form-group m-0'],
    ],
    'options' => [
        'data-pjax' => true,
    ],
]); ?>

<div class="wrap">
    <div class="row">
        <?= $widget
            ->field($form, 'name', ['options' => ['class' => 'form-group col-9']])
            ->textInput()
            ->label('Название типа сделки') ?>

        <?= $widget
            ->field($form, 'status', ['options' => ['class' => 'form-group col-3']])
            ->widget(Select2::class, [
                'data' => AbstractType::TYPE_STATUS_LIST,
                'hideSearch' => true,
                'options' => [
                    'placeholder' => '',
                    'id' => 'clientDialFormStatus',
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'readonly' => $statusDisabled,
                    'disabled' => $statusDisabled,
                ],
            ]) ?>
    </div>
</div>

<div class="wrap">
<div class="row">
<div class="col-9">

<div class="table-wrap">
    <table class="table table-style table-count-list">
        <thead>
        <tr class="heading">
            <th style="width: 1px;">Порядок этапов</th>
            <th>Название этапов</th>
            <th style="width: 1px;"></th>
        </tr>
        </thead>
        <tbody class="js-stage-wrapper">
<?php foreach ($form->stageForms as $index => $stageForm): ?>
            <?= $this->render('stage-form', [
                'widget' => $widget,
                'index' => $index,
                'form' => $stageForm,
            ]) ?>
<?php endforeach; ?>
        </tbody>
    </table>
</div>

<div>
    <button class="button-regular button-hover-content-red button-clr pl-3 pr-3 js-stage-add" title="Добавить этап">
        <?= SpriteIconWidget::widget(['icon' => 'add-icon']) ?>
        <span class="mr-1">Добавить</span>
    </button>
</div>
</div>
</div>
</div>

<div class="wrap wrap_btns check-condition visible mb-0 mt-0">
    <div class="row align-items-center justify-content-between">
        <div class="column">
            <?= Html::submitButton('Сохранить', [
                'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
                'data-style' => 'expand-right',
            ]) ?>
        </div>
        <div class="column">
            <?= Html::a('Отменить', $redirectUrl, [
                'class' => 'button-clr button-width button-regular button-hover-transparent',
                'data-pjax' => 0,
            ]) ?>
        </div>
    </div>
</div>

<?php

$template = json_encode($this->render('stage-form', [
    'form' => $form->createStageForm(),
    'widget' => $widget,
    'index' => '{index}',
]));

$this->registerJs(<<<JS
    $(document).off('click', '.js-stage-add');
    $(document).on('click', '.js-stage-add', function () {
        const template = {$template};

        $('.js-stage-wrapper').append($(template.split('{index}').join(Date.now())));
        
        window.scrollTo(0, document.body.scrollHeight);

        return false;
    });

    $(document).off('click', '.js-stage-delete');
    $(document).on('click', '.js-stage-delete', function () {
        $(this).closest('tr').first().remove();
        
        return false;
    });

    $(document).off('click', '.js-stage-up');
    $(document).on('click', '.js-stage-up', function () {
        const prev = $(this).closest('tr').prev('tr');
        
        if (prev.length) {
            $(this).closest('tr').insertBefore(prev);
        }

        return false;
    });

    $(document).off('click', '.js-stage-down');
    $(document).on('click', '.js-stage-down', function () {
        const next = $(this).closest('tr').next('tr');
        
        if (next.length) {
            $(this).closest('tr').insertAfter(next);
        }

        return false;
    });
JS);

?>

<?php ActiveForm::end(); ?>
