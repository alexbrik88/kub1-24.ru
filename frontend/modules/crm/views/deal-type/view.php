<?php

namespace frontend\modules\crm\views;

use frontend\components\WebUser;
use frontend\modules\crm\models\DealType;
use frontend\modules\crm\widgets\GridButtonWidget;
use frontend\modules\crm\widgets\PjaxWidget;
use frontend\rbac\permissions\crm\DealTypeAccess;
use frontend\themes\kub\widgets\ImportDialogWidget;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var WebUser $user
 * @var DealType $deal
 */

$this->title = $deal->name;
$canEdit = $user->can(DealTypeAccess::EDIT);

?>

<?= $this->render('widgets/deal-header', [
    'deal' => $deal,
    'user' => $user,
]) ?>

<?php PjaxWidget::begin(['id' => 'pjaxContent']) ?>

<?php PjaxWidget::end(); ?>

<?php if ($canEdit): ?>
<div class="wrap wrap_btns check-condition visible mb-0 mt-0">
    <div class="row align-items-center justify-content-between">
        <div class="column"></div>
        <div class="column">
            <?= GridButtonWidget::widget([
                'icon' => 'garbage',
                'label' => 'Удалить',
                'options' => [
                    'title' => 'Удалить',
                    'class' => 'button-clr button-width button-regular button-hover-transparent import-dialog',
                    'data-url' => Url::to(['delete', 'id' => $deal->deal_type_id]),
                ],
            ]) ?>
        </div>
    </div>
</div>

<?php endif; ?>

<?= ImportDialogWidget::widget() ?>
