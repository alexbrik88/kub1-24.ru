<?php

namespace frontend\modules\crm\views;

use frontend\modules\crm\models\ContractorFilter;
use frontend\modules\crm\models\ListFactory;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use frontend\components\Icon;
use yii\helpers\Url;

/**
 * @var ContractorFilter $filter
 * @var ActiveForm $widget
 */

$isFilters = [
    'client_check_ids' => (bool) $filter->client_check_ids,
    'campaign_ids' => (bool) $filter->campaign_ids,
    'activity_ids' => (bool) $filter->activity_ids,
    'client_status_id' => (bool) $filter->client_status_id,
];

$hasFilters = array_sum($isFilters);
$statusItems = (new ListFactory)->createStatusList()->getItems();

$getActivityLabel = function () use ($filter) {
    $items = (new ListFactory)->createActivityList()->getItems();
    $id = null;

    if (is_array($filter->activity_ids) && !empty($filter->activity_ids)) {
        if (count($filter->activity_ids) > 1) {
            return sprintf('Виды деятельности %d', count($filter->activity_ids));
        }

        $id = reset($filter->activity_ids);
    }

    return $items[$id] ?? 'Все виды';
};

$getCampaignLabel = function () use ($filter) {
    $items = (new ListFactory)->createCampaignList()->getItems();
    $id = null;

    if (is_array($filter->campaign_ids) && !empty($filter->campaign_ids)) {
        if (count($filter->campaign_ids) > 1) {
            return sprintf('Источники %d', count($filter->campaign_ids));
        }

        $id = reset($filter->campaign_ids);
    }

    return $items[$id] ?? 'Все источники';
};

$getCheckLabel = function () use ($filter) {
    $items = (new ListFactory)->createCheckList()->getItems();
    $id = null;

    if (is_array($filter->client_check_ids) && !empty($filter->client_check_ids)) {
        if (count($filter->client_check_ids) > 1) {
            return sprintf('Чеки %d', count($filter->client_check_ids));
        }

        $id = reset($filter->client_check_ids);
    }

    return $items[$id] ?? 'Все чеки';
};

?>

<?php if ($hasFilters): ?>
    <div class="table-settings row row_indents_s">
        <div class="col-5"></div>
        <div class="col-7">
            <div class="top-filter-block-wrapper">
                <div class="row row_indents_s">
                    <?php if ($isFilters['client_status_id']): ?>
                        <div class="col-3" data-filter="client_status_id">
                            <div class="top-filter-block">
                                <a class="close" href="<?= Url::current([$filter->formName() => ['client_status_id' => null]]) ?>"><?= Icon::get('close') ?></a>
                                <span><?= $statusItems[$filter->client_status_id] ?? 'Все' ?></span>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($isFilters['activity_ids']): ?>
                        <div class="col-3" data-filter="activity_ids">
                            <div class="top-filter-block">
                                <a class="close" href="<?= Url::current([$filter->formName() => ['activity_ids' => null]]) ?>"><?= Icon::get('close') ?></a>
                                <span><?= $getActivityLabel() ?></span>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($isFilters['campaign_ids']): ?>
                        <div class="col-3" data-filter="campaign_ids">
                            <div class="top-filter-block">
                                <a class="close" href="<?= Url::current([$filter->formName() => ['campaign_ids' => null]]) ?>"><?= Icon::get('close') ?></a>
                                <span><?= $getCampaignLabel() ?></span>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($isFilters['client_check_ids']): ?>
                        <div class="col-3" data-filter="client_check_ids">
                            <div class="top-filter-block">
                                <a class="close" href="<?= Url::current([$filter->formName() => ['client_check_ids' => null]]) ?>"><?= Icon::get('close') ?></a>
                                <span><?= $getCheckLabel() ?></span>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="table-settings row row_indents_s">
    <div class="col-5">
        <div class="row align-items-center">
            <div class="column flex-grow-1 d-flex flex-wrap justify-content-end">
                <?= TableConfigWidget::widget([
                    'items' => [
                        ['attribute' => 'crm_client_created', 'label' => 'Дата добавления'],
                        ['attribute' => 'crm_client_type', 'label' => 'Тип отношений'],
                        ['attribute' => 'crm_contact_name', 'label' => 'ФИО контакта'],
                        ['attribute' => 'crm_client_position', 'label' => 'Должность'],
                        ['attribute' => 'crm_contact_email', 'label' => 'E-mail'],
                        ['attribute' => 'contractor_campaign', 'label' => 'Источник'],
                        ['attribute' => 'contractor_activity', 'label' => 'Вид деятельности'],
                        ['attribute' => 'crm_client_comment'],
                        ['attribute' => 'crm_client_check', 'label' => 'Чек'],
                        ['attribute' => 'crm_client_reason', 'label' => 'Тип обращения'],
                    ],
                    'buttonClass' => 'button-regular button-regular_red button-clr w-44 mr-2',
                ]); ?>
                <?= TableViewWidget::widget(['attribute' => 'crm_client']) ?>
            </div>
        </div>
    </div>
    <div class="col-7">
        <?php $form = ActiveForm::begin([
            'id' => 'clientFilterForm',
            'method' => 'GET',
            'action' => Url::current([$filter->formName() => null]),
            'options' => [
                //'data-pjax' => true,
            ],
            /*'fieldConfig' => [
                'labelOptions' => ['class' => 'label'],
                'options' => ['class' => 'form-group col-6 mb-3'],
            ],*/
            'enableClientValidation' => false,
            'enableAjaxValidation' => false,
        ]); ?>
        <div class="d-flex flex-nowrap align-items-center">
            <div class="form-group mr-2 pr-1">
                <div class="dropdown popup-dropdown popup-dropdown_filter popup-dropdown_filter cash-filter <?= $hasFilters ? 'itemsSelected' : '' ?>" data-check-items="dropdown" style="z-index: 1001">
                    <span class="button-regular button-regular-more button-hover-transparent button-clr" id="filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="button-txt">Фильтр</span>
                        <svg class="svg-icon">
                            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                        </svg>
                    </span>
                    <div class="dropdown-menu" aria-labelledby="filter">
                        <div class="popup-dropdown-in p-3">
                            <div class="p-1">
                                <div class="row">
                                    <div class="col-6" data-filter="client_status_id">
                                        <div class="form-group mb-3">
                                            <div class="dropdown-drop" data-id="dropdown1">
                                                <div class="label">Статус</div>
                                                <a class="a-filter button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#"
                                                   data-target="dropdown1">
                                                        <span>
                                                            <?= $statusItems[$filter->client_status_id] ?? 'Все' ?>
                                                        </span>
                                                    <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                        <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                                    </svg>
                                                </a>
                                                <div class="dropdown-drop-menu" data-id="dropdown1">
                                                    <div class="dropdown-drop-in">
                                                        <?= $form->field($filter, 'client_status_id', [
                                                            'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                                        ])->label(false)->radioList(['' => 'Все'] + $statusItems, [
                                                            'item' => function ($index, $label, $name, $checked, $value) use ($filter) {
                                                                if ($index == 0) {
                                                                    $checked = !$filter->client_status_id;
                                                                }

                                                                return Html::radio($name, $checked, [
                                                                    'value' => $value,
                                                                    'label' => $label,
                                                                    'labelOptions' => [
                                                                        'class' => 'radio-label p-2 no-border',
                                                                    ],
                                                                    'data-default' => ($checked) ? 1 : 0,
                                                                ]);
                                                            },
                                                        ]); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6" data-filter="activity_ids">
                                        <div class="form-group mb-3">
                                            <div class="dropdown-drop" data-id="dropdown3">
                                                <div class="label">Вид деятельности</div>
                                                <a class="a-filter button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-target="dropdown3">
                                                    <span><?= ($filter->activity_ids) ? (count($filter->activity_ids) . ' шт.') : 'Все' ?></span>
                                                    <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                        <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                                    </svg>
                                                </a>
                                                <div class="dropdown-drop-menu" data-id="dropdown3">
                                                    <div class="dropdown-drop-in">
                                                        <?= $form->field($filter, 'activity_ids', [
                                                            'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                                        ])->label(false)->checkboxList(['' => 'Все'] + $filter->getListFactory()->createActivityList()->getItems(), [
                                                            'item' => function ($index, $label, $name, $checked, $value) use ($filter) {
                                                                if ($index == 0) {
                                                                    $checked = !$filter->activity_ids;
                                                                }
                                                                return Html::checkbox($name, $checked, [
                                                                    'value' => $value,
                                                                    'label' => $label,
                                                                    'class' => ($index === 0) ? 'checkbox-main' : '',
                                                                    'labelOptions' => [
                                                                        'class' => 'checkbox-label p-2 no-border',
                                                                    ],
                                                                    'data-default' => ($checked) ? 1 : 0,
                                                                ]);
                                                            },
                                                        ]) ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6" data-filter="campaign_ids">
                                        <div class="form-group mb-3">
                                            <div class="dropdown-drop" data-id="dropdown2">
                                                <div class="label">Источник</div>
                                                <a class="a-filter button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-target="dropdown2">
                                                    <span><?= ($filter->campaign_ids) ? (count($filter->campaign_ids) . ' шт.') : 'Все' ?></span>
                                                    <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                        <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                                    </svg>
                                                </a>
                                                <div class="dropdown-drop-menu" data-id="dropdown2">
                                                    <div class="dropdown-drop-in">
                                                        <?= $form->field($filter, 'campaign_ids', [
                                                            'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                                        ])->label(false)->checkboxList(['' => 'Все'] + $filter->getListFactory()->createCampaignList()->getItems(), [
                                                            'item' => function ($index, $label, $name, $checked, $value) use ($filter) {
                                                                if ($index == 0) {
                                                                    $checked = !$filter->campaign_ids;
                                                                }

                                                                return Html::checkbox($name, $checked, [
                                                                    'value' => $value,
                                                                    'label' => $label,
                                                                    'class' => ($index === 0) ? 'checkbox-main' : '',
                                                                    'labelOptions' => [
                                                                        'class' => 'checkbox-label p-2 no-border',
                                                                    ],
                                                                    'data-default' => ($checked) ? 1:0
                                                                ]);
                                                            },
                                                        ]) ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6" data-filter="client_check_ids">
                                        <div class="form-group mb-3">
                                            <div class="dropdown-drop" data-id="dropdown4">
                                                <div class="label">Чеки</div>
                                                <a class="a-filter button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-target="dropdown4">
                                                    <span><?= ($filter->client_check_ids) ? (count($filter->client_check_ids) . ' шт.') : 'Все' ?></span>
                                                    <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                        <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                                    </svg>
                                                </a>
                                                <div class="dropdown-drop-menu" data-id="dropdown4">
                                                    <div class="dropdown-drop-in">
                                                        <?= $form->field($filter, 'client_check_ids', [
                                                            'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                                        ])->label(false)->checkboxList(['' => 'Все'] + $filter->getListFactory()->createCheckList()->getItems(), [
                                                            'item' => function ($index, $label, $name, $checked, $value) use ($filter) {
                                                                if ($index == 0) {
                                                                    $checked = !$filter;
                                                                }

                                                                return Html::checkbox($name, $checked, [
                                                                    'value' => $value,
                                                                    'label' => $label,
                                                                    'class' => ($index === 0) ? 'checkbox-main' : '',
                                                                    'labelOptions' => [
                                                                        'class' => 'checkbox-label p-2 no-border',
                                                                    ],
                                                                    'data-default' => ($checked) ? 1 : 0
                                                                ]);
                                                            },
                                                        ]) ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-between">
                                    <div class="col-6 pr-0">
                                        <button class="button-regular button-hover-content-red button-width-medium button-clr" type="submit">
                                            Применить
                                        </button>
                                    </div>
                                    <div class="col-6 pl-0 text-right">
                                        <button class="cash_filters_reset button-regular button-hover-content-red button-width-medium button-clr" type="button">
                                            Сбросить
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group flex-grow-1 mr-2" data-filter="search">
                <?= $form->field($filter, 'search')->label(false)->textInput(['type' => 'search', 'class' => 'form-control', 'placeholder' => 'Поиск...']); ?>
            </div>
            <div class="form-group" data-filter="search">
                <?= Html::submitButton('Найти', ['class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>

<script>

    CashFilter = {
        selector: '.cash-filter',
        init: function() {
            this.bindEvents();
        },
        bindEvents: function() {
            const that = this;
            // sub-dropdown
            $(that.selector + ' .popup-dropdown-in').on('click', function(e) {
                e.stopPropagation();

                const el = that.normalizeElement(e.target);

                if (that.detectElement.isFilter(el)) {
                    that.toggleDropdown(el);
                    return false;
                }
                else if (that.detectElement.isCheckboxMain(el)) {
                    that.toggleCheckboxes(el, true);
                    that.setDropdownValue(el);
                    return true;
                }
                else if (that.detectElement.isCheckbox(el)) {
                    that.toggleCheckboxes(el, false);
                    that.setDropdownValue(el);
                    return true;
                }
                else if (that.detectElement.isRadio(el)) {
                    that.setDropdownValue(el);
                    return true;
                }
                else if (that.detectElement.isButton(el)) {
                    return true;
                }
                else if (that.detectElement.isSelfBody(el)) {
                    return false;
                }
                else {
                    that.closeAllDropdowns();
                    return false;
                }
            });
            // reset filter
            $(that.selector + ' .cash_filters_reset').on('click', function() {
                that.resetFilter();
                that.refreshSubmitColor();
            });
            // change submit color
            $(that.selector + ' input').on('change', function(e) {
                e.preventDefault();
                that.refreshSubmitColor();
            });
        },
        toggleCheckboxes: function(el, isMain) {
            const wrapper = $(el).closest('.dropdown-drop-in');
            const main = wrapper.find('input.checkbox-main');
            const plain = $(el).find('input:checkbox');
            const others = wrapper.find('input:checkbox').not('.checkbox-main');
            if (isMain) {
                if (main.prop('checked'))
                    others.prop('checked', false).uniform('refresh');
                else
                    others.first().prop('checked', true).uniform('refresh');
            } else {
                if (plain.prop('checked'))
                    main.prop('checked', false).uniform('refresh');
                else if (!others.filter(':checked').length)
                    main.prop('checked', true).uniform('refresh');
            }
        },
        normalizeElement: function(el) {
            if ($(el).attr('type') === 'checkbox')
                return $(el).closest('.checkbox-label')[0];
            if ($(el).attr('type') === 'radio')
                return $(el).closest('.radio-label')[0];

            return el;
        },
        detectElement: {
            isFilter: function(el) {
                return $(el).hasClass('a-filter');
            },
            isCheckboxMain: function(el) {
                return $(el).hasClass('checkbox-label') && $(el).find('input:checkbox').hasClass('checkbox-main');
            },
            isCheckbox: function(el) {
                return $(el).hasClass('checkbox-label');
            },
            isRadio: function(el) {
                return $(el).hasClass('radio-label');
            },
            isButton: function(el) {
                return $(el).is('button');
            },
            isSelfBody: function(el) {
                return $(el).hasClass('dropdown-drop-in');
            }
        },
        toggleDropdown: function(el) {
            const that = this;
            const target = $(el).data('target');
            $(that.selector).find(' [data-id="'+target+'"]').toggleClass('visible show');
            $(that.selector).find('.dropdown-drop-menu.show, .dropdown-drop.show').not('[data-id="'+target+'"]').removeClass('visible show');
        },
        closeDropdown: function(el) {
            $(el).parents('.dropdown-drop-menu, .dropdown-drop').removeClass('visible show');
        },
        closeAllDropdowns: function() {
            const that = this;
            $(that.selector).find('.dropdown-drop-menu.show, .dropdown-drop.show').removeClass('visible show');
        },
        setDropdownValue: function(el) {
            const wrapper = $(el).closest('.dropdown-drop-in');
            const checkedInputs = $(wrapper).find('input:checked');
            const value = (checkedInputs.length > 1) ? (checkedInputs.length + ' шт.') : $(checkedInputs).closest('label').text();
            $(el).parents('.dropdown-drop').find('a > span').html(value);
        },
        resetFilter: function() {
            const that = this;
            $(that.selector).find('.dropdown-drop-in').each(function(i, el) {
                const radio = $(this).find('input:radio');
                const checkbox = $(this).find('input:checkbox');
                if (radio.length) {
                    radio.prop('checked', false).uniform().first().prop('checked', true).uniform();
                }
                if (checkbox.length) {
                    checkbox.prop('checked', false).uniform().first().prop('checked', true).uniform();
                }
                that.setDropdownValue(el);
            });
        },
        refreshSubmitColor: function() {
            const that = this;
            let filter_on = false;
            $(that.selector).find('.form-filter-list').each(function() {
                let a_val = $(this).find('input').filter('[data-default=1]').val();
                let i_val = $(this).find('input:checked').val();
                if (i_val === undefined) {
                    i_val = $(this).find('label:first-child').find('input').val();
                }
                if (a_val === undefined) {
                    a_val = $(this).find('label:first-child').find('input').val();
                }
                if (a_val !== i_val) {
                    filter_on = true;
                    return false;
                }
            });

            $(that.selector).find('[type=submit]')
                .addClass(filter_on ? 'button-regular_red' : 'button-hover-content-red')
                .removeClass(filter_on ? 'button-hover-content-red' : 'button-regular_red');
        }
    };

    //////////////////
    CashFilter.init();
    //////////////////

</script>
