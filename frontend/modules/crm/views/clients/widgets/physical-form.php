<?php

namespace frontend\modules\crm\views;

use common\components\date\DateHelper;
use common\models\Contractor;
use Closure;
use frontend\modules\crm\models\CreateContractorForm;
use frontend\modules\crm\models\MessengerTypeList;
use frontend\modules\crm\models\ListFactory;
use frontend\modules\crm\widgets\ButtonListFactory;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\MaskedInput;

/**
 * @var View $his
 * @var CreateContractorForm $form
 * @var Closure $tooltipTemplate
 */

?>

<?php $widget = ActiveForm::begin([
    'id' => 'physicalForm',
    'fieldConfig' => [
        'labelOptions' => ['class' => 'label'],
        'options' => ['class' => 'form-group col-6'],
    ],
    'options' => [
        'data-pjax' => true,
        'class' => $form->isActive() ? '' : 'd-none',
    ],
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
]); ?>

<div class="row">

<?= $widget->field($form->contractorForm, 'face_type', [
    'inline' => true,
    'labelOptions' => ['class' => 'label mb-3'],
    'radioOptions' => [
        'class' => '',
        'labelOptions' => ['class' => 'radio-label radio-txt-bold'],
    ],
])->radioList([
    Contractor::TYPE_LEGAL_PERSON => 'Юридическое лицо',
    Contractor::TYPE_PHYSICAL_PERSON => 'Физическое лицо',
])->label('Тип покупателя') ?>

<?= $widget->field($form->contractorForm, 'ITN', [
    'options' => [
        'class' => 'form-group col-6 invisible',
        'id' => 'physicalITN',
    ],
])->textInput() ?>

<?= $widget->field($form->contractorForm, 'physical_lastname')->textInput()->label('Фамилия') ?>

<?= $widget->field($form->contractorForm, 'physical_firstname')->textInput()->label('Имя') ?>

<?= $widget->field($form->contractorForm, 'physical_patronymic')->textInput()->label('Отчество') ?>

<div class="form-group col-6">
    <div class="checkbox checkbox_left" style="padding-top: 34px;">
        <label class="checkbox-label label" for="contractor-physical_no_patronymic">
            <?= Html::activeCheckbox($form->contractorForm, 'physical_no_patronymic', [
                'id' => 'contractor-physical_no_patronymic',
                'class' => 'checkbox-input input-hidden',
                'label' => false,
            ]) ?>
            <span>Нет отчества</span>
        </label>
    </div>
</div>

<?= $widget->field($form->contactForm, 'primary_phone_number', ['options' => ['class' => 'form-group col-3']])->textInput() ?>

<?= $widget->field(
    $form->contactForm,
    'primary_messenger_type',
    ['options' => ['class' => 'form-group col-3'], 'template' => $tooltipTemplate('#primaryMessengerTypeTooltip')]
)->widget(Select2::class, [
    'data' => (new MessengerTypeList)->getItems(),
    'hideSearch' => true,
    'options' => [
        'id' => 'physicalPrimaryMessengerType',
    ],
    'pluginOptions' => [
        'placeholder' => '',
        'width' => '100%',
    ],
]) ?>

<?= $widget->field($form->contactForm, 'secondary_phone_number', ['options' => ['class' => 'form-group col-3']])->textInput() ?>

<?= $widget->field(
    $form->contactForm,
    'secondary_messenger_type',
    ['options' => ['class' => 'form-group col-3'], 'template' => $tooltipTemplate('#secondaryMessengerTypeTooltip')]
)->widget(Select2::class, [
    'data' => (new MessengerTypeList)->getItems(),
    'hideSearch' => true,
    'options' => [
        'id' => 'physicalSecondaryMessengerType',
    ],
    'pluginOptions' => [
        'placeholder' => '',
        'width' => '100%',
    ],
]) ?>

<?= $widget->field($form->contactForm, 'email_address')->textInput() ?>

<?= $widget->field(
    $form->contactForm,
    'skype_account',
    ['template' => $tooltipTemplate('#skypeTooltip')]
)->textInput() ?>

<?= $widget->field(
    $form->clientForm,
    'deal_type_id',
    ['template' => $tooltipTemplate('#dealTypeTooltip')]
)->widget(Select2::class, [
    'data' => (new ListFactory)->createDealList()->getItems(),
    'options' => [
        'id' => 'physicalDealTypeId',
    ],
    'readonly' => $form->clientForm->getDealType() !== null,
    'disabled' => $form->clientForm->getDealType() !== null,
    'pluginOptions' => [
        'placeholder' => '',
        'width' => '100%',
    ],
]) ?>

<?= $widget->field(
    $form->contractorForm,
    'campaign_id',
    ['template' => $tooltipTemplate('#campaignTooltip')]
)->widget(Select2::class, [
    'data' => (new ButtonListFactory($this))->createCampaignList()->getItems(),
    'options' => [
        'id' => 'physicalCampaignId',
    ],
    'pluginOptions' => [
        'placeholder' => '',
        'width' => '100%',
        'escapeMarkup' => new JsExpression("function(value) { return value; }"),
    ],
]) ?>

<?= $widget->field(
    $form->contractorForm,
    'activity_id',
    ['template' => $tooltipTemplate('#activityTooltip')]
)->widget(Select2::class, [
    'data' => (new ButtonListFactory($this))->createActivityList()->getItems(),
    'options' => [
        'id' => 'physicalActivityId',
    ],
    'pluginOptions' => [
        'placeholder' => '',
        'width' => '100%',
        'escapeMarkup' => new JsExpression("function(value) { return value; }"),
    ],
]) ?>

<?= $widget->field(
    $form->clientForm,
    'client_type_id',
    ['template' => $tooltipTemplate('#clientTypeTooltip')]
)->widget(Select2::class, [
    'options' => [
        'id' => 'client_type_id_physical'
    ],
    'data' => (new ButtonListFactory($this))->createClientTypeList()->getItems(),
    'options' => [
        'id' => 'clientTypeId',
    ],
    'pluginOptions' => [
        'placeholder' => '',
        'width' => '100%',
        'escapeMarkup' => new JsExpression("function(value) { return value; }"),
    ],
]) ?>

<?= $widget->field($form->contractorForm, 'comment', ['options' => ['class' => 'form-group col-12']])->textarea() ?>

    <div class="form-group col-12 m-0">
        <?= Html::a('<span class="link-txt">Реквизиты клиента</span><svg class="link-shevron svg-icon"><use xlink:href="/img/svg/svgSprite.svg#shevron"></use></svg>', '#collapsePhysical', [
            'class' => 'link link_collapse link_bold button-clr mb-4 collapsed',
            'data-toggle' => 'collapse',
            'role' => 'button',
            'aria-expanded' => 'false',
            'aria-controls' => 'collapsePhysical',
        ]) ?>
    </div>
</div>

<div class="row collapse" id="collapsePhysical">

<?= $widget->field($form->contractorForm, 'physical_address')->textInput()->label('Адрес регистрация') ?>

<?= $widget->field($form->contractorForm, 'physical_passport_series')->widget(MaskedInput::class, [
    'mask' => '9{2} 9{2}',
    'options' => ['class' => 'form-control', 'placeholder' => 'XX XX'],
])->label('Серия паспорта') ?>

<?= $widget->field($form->contractorForm, 'physical_passport_number')->widget(MaskedInput::class, [
    'mask' => '9{6}',
    'options' => ['class' => 'form-control', 'placeholder' => 'XXXXXX'],
])->label('Номер паспорта') ?>

    <div class="form-group col-6">
        <label class="label">Дата выдачи</label>
        <div class="date-picker-wrap">
            <?= Html::activeTextInput($form->contractorForm, 'physical_passport_date_output', [
                'class' => 'form-control date-picker',
                'data-date-viewmode' => 'years',
                'value' => DateHelper::format($form->contractorForm->physical_passport_date_output, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ]); ?>
            <svg class="date-picker-icon svg-icon input-toggle">
                <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
            </svg>
        </div>
    </div>
</div>

<div class="row mt-4">
    <div class="form-group col-6 text-left mb-0">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-width button-regular_red button-clr',
            'data-style' => 'expand-right',
        ]) ?>
    </div>

    <div class="form-group col-6 text-right mb-0">
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'data-dismiss' => 'modal'
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
