<?php

namespace frontend\modules\crm\views;

use frontend\modules\crm\models\CreateContractorForm;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\Html;
use yii\web\View;

/**
 * @var View $this
 * @var CreateContractorForm $legalForm
 * @var CreateContractorForm $physicalForm
 */

$this->title = 'Добавить клиента';

$tooltipTemplate = function (string $selector): string {
    $icon = Html::tag(
        'span',
        '<svg class="tooltip-question-icon svg-icon mx-1"><use xlink:href="/img/svg/svgSprite.svg#question"></use></svg>',
        [
            'class' => 'tooltip2',
            'style' => '',
            'data-tooltip-content' => $selector,
        ]
    );

    return "{label}\n{$icon}\n{input}\n{hint}\n{error}";
};

?>

<?= yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub', 'tooltipster-in-modal'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'contentCloning' => true,
    ],
]);

?>

<h4 class="modal-title"><?= Html::encode($this->title) ?></h4>

<div class="hidden">
    <div id="skypeTooltip">
        Укажите Skype клиента для связи
    </div>
    <div id="primaryMessengerTypeTooltip">
        Укажите мессенджер, в котором клиент предпочитает переписываться
    </div>
    <div id="secondaryMessengerTypeTooltip">
        Укажите мессенджер, в котором клиент предпочитает переписываться
    </div>
    <div id="campaignTooltip">
        Укажите источник, из которого клиент пришел к вам
    </div>
    <div id="activityTooltip">
        Укажите вид деятельности клиента, это поможет<br>группировать клиентов для сегментирования
    </div>
    <div id="dealTypeTooltip">
        Укажите тип сделки (воронку продаж),<br>по которой вы ведете клиента к продаже
    </div>
    <div id="clientTypeTooltip">
        В этом поле укажите тип отношений с клиентом.<br>Например: Клиент или Партнер
    </div>
</div>

<?= $this->render('legal-form', [
    'form' => $legalForm,
    'tooltipTemplate' => $tooltipTemplate,
]) ?>

<?= $this->render('physical-form', [
    'form' => $physicalForm,
    'tooltipTemplate' => $tooltipTemplate,
]) ?>

<?php

$this->registerJs(<<<JS
    (function () {
        $('[name="Contractor[face_type]"], [name="Contractor[face_type]"]').on('change', function () {
            var value = $(this).val();
    
            var legalForm = $('#legalForm');
            var physicalForm = $('#physicalForm');
    
            legalForm.addClass('d-none');
            physicalForm.addClass('d-none');
    
            if (value > 0) {
                physicalForm.removeClass('d-none');
                physicalForm.find('[name="Contractor[face_type]"][value="1"]').trigger('click');
            } else {
                legalForm.removeClass('d-none');
                legalForm.find('[name="Contractor[face_type]"][value="0"]').trigger('click');
            }
        });
    })();
JS);

$this->registerCss(<<<CSS
    #legalForm .tt-dataset {
        overflow-y: auto;
        max-height: 10em;
    }
CSS);

?>
