<?php

namespace frontend\modules\crm\views;

use common\components\widgets\BikTypeahead;
use common\models\Contractor;
use Closure;
use frontend\modules\crm\models\CreateContractorForm;
use frontend\modules\crm\models\ListFactory;
use frontend\modules\crm\models\MessengerTypeList;
use frontend\modules\crm\widgets\ButtonListFactory;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/**
 * @var View $this
 * @var CreateContractorForm $form
 * @var Closure $tooltipTemplate
 */

$companyTypeList = (new ListFactory)->createCompanyTypeList()->getItems();
$companyTypeJs = json_encode(array_flip($companyTypeList));
?>

<?php $widget = ActiveForm::begin([
    'id' => 'legalForm',
    'fieldConfig' => [
        'labelOptions' => ['class' => 'label'],
        'options' => ['class' => 'form-group col-6'],
    ],
    'options' => [
        'data-pjax' => true,
        'class' => $form->isActive() ? '' : 'd-none'
    ],
    'enableClientValidation' => true,
    //'enableAjaxValidation' => true,
]); ?>

<div class="row">

<?= $widget->field($form->contractorForm, 'face_type', [
    'inline' => true,
    'labelOptions' => ['class' => 'label mb-3'],
    'radioOptions' => [
        'class' => '',
        'labelOptions' => ['class' => 'radio-label radio-txt-bold'],
    ],
])->radioList([
    Contractor::TYPE_LEGAL_PERSON => 'Юридическое лицо',
    Contractor::TYPE_PHYSICAL_PERSON => 'Физическое лицо',
])->label('Тип покупателя') ?>

<?= $widget->field($form->contractorForm, 'ITN')->textInput([
    'class' => 'form-control form-control_placeholder_red',
    'placeholder' => 'Автозаполнение по ИНН',
])->label('ИНН клиента') ?>

<?= $widget->field($form->contractorForm, 'name')->textInput()->label('Название')->hint(false) ?>

<?= $widget->field($form->contractorForm, 'company_type_id')->widget(Select2::class, [
    'data' => $companyTypeList,
    'pluginOptions' => [
        'placeholder' => '',
        'width' => '100%',
    ],
])->label('Форма организации') ?>

<?= $widget->field($form->contactForm, 'contact_name')->textInput()->label('ФИО контакта') ?>

<?= $widget->field($form->contactForm, 'client_position_id')->widget(Select2::class, [
    'data' => (new ButtonListFactory($this))->createPositionList()->getItems(),
    'pluginOptions' => [
        'placeholder' => '',
        'width' => '100%',
        'escapeMarkup' => new JsExpression("function(value) { return value; }"),
    ],
]) ?>

<?= $widget->field($form->contactForm, 'primary_phone_number', ['options' => ['class' => 'form-group col-3']])->textInput() ?>

<?= $widget->field(
    $form->contactForm,
    'primary_messenger_type',
    ['options' => ['class' => 'form-group col-3'], 'template' => $tooltipTemplate('#primaryMessengerTypeTooltip')]
)->widget(Select2::class, [
    'data' => (new MessengerTypeList)->getItems(),
    'hideSearch' => true,
    'pluginOptions' => [
        'placeholder' => '',
        'width' => '100%',
    ],
]) ?>

<?= $widget->field($form->contactForm, 'secondary_phone_number', ['options' => ['class' => 'form-group col-3']])->textInput() ?>

<?= $widget->field(
        $form->contactForm,
        'secondary_messenger_type',
        ['options' => ['class' => 'form-group col-3'], 'template' => $tooltipTemplate('#secondaryMessengerTypeTooltip')]
)->widget(Select2::class, [
    'data' => (new MessengerTypeList)->getItems(),
    'hideSearch' => true,
    'pluginOptions' => [
        'placeholder' => '',
        'width' => '100%',
    ],
]) ?>

<?= $widget->field($form->contactForm, 'email_address')->textInput() ?>

<?= $widget->field(
    $form->contactForm,
    'skype_account',
    ['template' => $tooltipTemplate('#skypeTooltip')]
)->textInput() ?>

<?= $widget->field(
    $form->clientForm,
    'deal_type_id',
    ['template' => $tooltipTemplate('#dealTypeTooltip')]
)->widget(Select2::class, [
    'data' => (new ListFactory)->createDealList()->getItems(),
    'readonly' => $form->clientForm->getDealType() !== null,
    'disabled' => $form->clientForm->getDealType() !== null,
    'pluginOptions' => [
        'placeholder' => '',
        'width' => '100%',
    ],
]) ?>

<?= $widget->field(
    $form->contractorForm,
    'campaign_id',
    ['template' => $tooltipTemplate('#campaignTooltip')]
)->widget(Select2::class, [
    'data' => (new ButtonListFactory($this))->createCampaignList()->getItems(),
    'pluginOptions' => [
        'placeholder' => '',
        'width' => '100%',
        'escapeMarkup' => new JsExpression("function(value) { return value; }"),
    ],
]) ?>

<?= $widget->field(
    $form->contractorForm,
    'activity_id',
    ['template' => $tooltipTemplate('#activityTooltip')]
)->widget(Select2::class, [
    'data' => (new ButtonListFactory($this))->createActivityList()->getItems(),
    'pluginOptions' => [
        'placeholder' => '',
        'width' => '100%',
        'escapeMarkup' => new JsExpression("function(value) { return value; }"),
    ],
]) ?>

<?= $widget->field(
    $form->clientForm,
    'client_type_id',
    ['template' => $tooltipTemplate('#clientTypeTooltip')]
)->widget(Select2::class, [
    'options' => [
        'id' => 'client_type_id_legal'
    ],
    'data' => (new ButtonListFactory($this))->createClientTypeList()->getItems(),
    'pluginOptions' => [
        'placeholder' => '',
        'width' => '100%',
        'escapeMarkup' => new JsExpression("function(value) { return value; }"),
    ],
]) ?>

<?= $widget->field($form->contractorForm, 'comment', ['options' => ['class' => 'form-group col-12']])->textarea() ?>

    <div class="form-group col-12 m-0">
        <?= Html::a('<span class="link-txt">Реквизиты клиента</span><svg class="link-shevron svg-icon"><use xlink:href="/img/svg/svgSprite.svg#shevron"></use></svg>', '#collapseLegal', [
            'class' => 'link link_collapse link_bold button-clr mb-4 collapsed',
            'data-toggle' => 'collapse',
            'role' => 'button',
            'aria-expanded' => 'false',
            'aria-controls' => 'collapseLegal',
        ]) ?>
    </div>

</div>

<div class="collapse row" id="collapseLegal">

<?= $widget->field($form->contractorForm, 'legal_address', ['options' => ['class' => 'form-group col-12']])->textInput() ?>

<?= $widget->field($form->contractorForm, 'director_post_name')->textInput()->label('Должность руководителя') ?>

<?= $widget->field($form->contractorForm, 'director_name')->textInput()->label('ФИО руководителя') ?>

<?= $widget->field($form->contractorForm, 'PPC')->textInput(['maxlength' => 9]) ?>

<?= $widget->field($form->contractorForm, 'current_account')->textInput(['maxlength' => 20]) ?>

<?= $widget->field($form->contractorForm, 'BIC')->widget(BikTypeahead::class, [
    'remoteUrl' => Url::to(['/dictionary/bik']),
    'related' => [
        '#' . Html::getInputId($form->contractorForm, 'bank_name') => 'name',
        '#' . Html::getInputId($form->contractorForm, 'bank_city') => 'city',
        '#' . Html::getInputId($form->contractorForm, 'corresp_account') => 'ks',
    ],
])->textInput(); ?>

<?= $widget->field($form->contractorForm, 'bank_name')->textInput(['readonly' => true]) ?>

<?= $widget->field($form->contractorForm, 'bank_city')->textInput(['readonly' => true]) ?>

<?= $widget->field($form->contractorForm, 'corresp_account')->textInput(['readonly' => true]) ?>

</div>

<div class="row mt-4">
    <div class="form-group col-6 text-left mb-0">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-width button-regular_red button-clr',
            'data-style' => 'expand-right',
        ]) ?>
    </div>

    <div class="form-group col-6 text-right mb-0">
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'data-dismiss' => 'modal'
        ]) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php $this->registerJs(<<<JS
$(document).ready(function() {
    var form = $('#legalForm');
    var type = form.find('[name="Contractor[company_type_id]"]');
    var companyType = $companyTypeJs;

    form.find('[name="Contractor[ITN]"]').suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: '78497656dfc90c2b00308d616feb9df60c503f51',
        type: 'PARTY',
        count: 10,

        onSelect: function(suggestion) {
            var companyTypeId = '-1';
            if (!empty(suggestion.data.opf) && !empty(companyType[suggestion.data.opf.short])) {
                companyTypeId = companyType[suggestion.data.opf.short.toUpperCase()];
            }
            form.find('[name="Contractor[ITN]"]').val(suggestion.data.inn);
            form.find('[name="Contractor[PPC]"]').val(suggestion.data.kpp);
            form.find('[name="Contractor[name]"]').val(suggestion.data.name.full);
            form.find('[name="Contractor[legal_address]"]').val(suggestion.data.address.value);

            if (suggestion.data.management) {
                form.find('[name="Contractor[director_name]"]').val(suggestion.data.management.name);
                form.find('[name="Contractor[director_post_name]"]').val(suggestion.data.management.post);
            }
            
            if (suggestion.data.opf && suggestion.data.opf.short) {
                if (suggestion.data.opf.short === 'ИП') {
                    form.find('[name="Contractor[director_name]"]').val(suggestion.data.name.full);
                }
                
                var option = type.find('option:contains("' + suggestion.data.opf.short + '")');
                
                if (option.length) {
                    type.val(option.val());
                    type.change();
                }
            }

            $('[name="Contractor[company_type_id]"]', form).val(companyTypeId).trigger('change');

            return false;
        }
    });
});
JS);
