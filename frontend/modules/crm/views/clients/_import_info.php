<div class="file_upload_info mt-3">
    <div>
        Для загрузки списка клиентов из Excel,
    </div>
    <div>
        заполните шаблон таблицы и загрузите ее тут.
    </div>
    <div class="mt-3">
        <a href="/files/crm/ImportClients.xlsx" target="_blank">Скачать шаблон таблицы для юрлиц</a>
    </div>
    <div>
        <a href="/files/crm/ImportClientsPhysical.xlsx" target="_blank">Скачать шаблон таблицы для физлиц</a>
    </div>
</div>