<?php

namespace frontend\modules\crm\views;

use common\models\Contractor;
use frontend\modules\crm\models\ClientForm;
use frontend\modules\crm\models\ContactForm;
use yii\web\View;

/**
 * @var View $this
 * @var Contractor $contractor
 * @var ClientForm $clientForm
 * @var ContactForm[] $contactForms
 */

echo $this->render('@frontend/views/contractor/form/_form', [
    'model' => $contractor,
    'clientForm' => $clientForm,
    'contactForms' => $contactForms,
    'isCrmView' => true,
    'face_type_opt' => 1,
    'showErrorSummary' => 1
]);

$this->title = 'Обновить клиента: ' . $contractor->name;
