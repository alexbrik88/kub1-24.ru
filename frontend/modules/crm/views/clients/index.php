<?php

namespace frontend\modules\crm\views;

use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\models\Contractor;
use frontend\components\StatisticPeriod;
use frontend\components\WebUser;
use frontend\modules\crm\models\ContractorSelectForm;
use frontend\modules\crm\models\ContractorFilter;
use frontend\modules\crm\models\Task;
use frontend\modules\crm\services\Client\StatisticHelper;
use frontend\modules\crm\widgets\AddClientDialogWidget;
use frontend\modules\crm\widgets\AddItemDialogWidget;
use frontend\modules\crm\widgets\DataColumnWidget;
use frontend\modules\crm\widgets\GridButtonWidget;
use frontend\rbac\permissions\crm\ClientAccess;
use frontend\rbac\permissions\crm\TaskAccess;
use frontend\themes\kub\widgets\ImportDialogWidget;
use frontend\widgets\RangeButtonWidget;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\i18n\Formatter;
use yii\web\View;
use yii\widgets\ContentDecorator;

/**
 * @var View $this
 * @var WebUser $user
 * @var ContractorFilter $filter
 * @var ContractorSelectForm $form
 */

$this->title = 'Клиенты';
$dataProvider = $filter->getDataProvider();
$countHelper = new StatisticHelper($dataProvider->query);
$canEdit = $user->can(ClientAccess::EDIT);
$canTaskViewAll = $user->can(TaskAccess::VIEW_ALL);
$emptyText = sprintf(
    'В выбранном периоде («%s») нет добавленных клиентов. Измените период, чтобы увидеть добавленных клиентов',
    StatisticPeriod::getSessionName()
);

$count = $filter->getTotalCount();

if ($count == 0) {
    $emptyText = 'Вы ещё не добавили ни одного клиента. ';

    if ($canEdit) {
        $emptyText .= Html::a('Добавить клиента', '#', [
            'class' => 'ajax-form-button',
            'data-url' => Url::to(['clients/create']),
            'data-title' => 'Добавить клиента',
        ]);
    }
}

$contentOptions = function (Contractor $contractor) use ($canTaskViewAll, $user) : array {
    $task = ArrayHelper::getValue($contractor, ['client', 'task']);
    if ($task && $task->status == Task::STATUS_EXPIRED && ($canTaskViewAll || $task->employee_id == $user->id)) {
        return ['class' => 'nowrap red-column'];
    }

    return ['class' => 'nowrap'];
};

?>
<div class="page-head d-flex flex-wrap align-items-center mb-2">
    <h4><?= Html::encode($this->title); ?></h4>

    <?= GridButtonWidget::widget([
        'accessName' => ClientAccess::EDIT,
        'label' => 'Добавить',
        'icon' => 'add-icon',
        'options' => [
            'class' => 'button-regular button-regular_red pl-3 pr-3 ml-auto add-client-dialog',
            'data-url' => Url::to(['clients/create']),
        ],
    ]) ?>
</div>

<?= AddClientDialogWidget::widget() ?>
<?= AddItemDialogWidget::widget() ?>
<?= ImportDialogWidget::widget() ?>

<div class="wrap wrap_count">
    <div class="row">
        <div class="col-6 col-xl-3">
            <div class="count-card count-card wrap">
                <div class="count-card-main"><?= $countHelper->totalCount() ?></div>
                <div class="count-card-title">Клиентов</div>
                <hr>
                <div class="count-card-foot">Всего</div>
            </div>
        </div>
        <div class="col-6 col-xl-3">
            <div class="count-card count-card_red wrap">
                <div class="count-card-main"><?= $countHelper->withOverdueTasksCount() ?></div>
                <div class="count-card-title">Клиентов</div>
                <hr>
                <div class="count-card-foot">С просроченными задачами</div>
            </div>
        </div>
        <div class="col-6 col-xl-3">
            <div class="count-card count-card_green wrap">
                <div class="count-card-main"><?= $countHelper->noContactsCount() ?></div>
                <div class="count-card-title">Клиентов</div>
                <hr>
                <div class="count-card-foot">Не было контактов > 30 дн.</div>
            </div>
        </div>
        <div class="col-6 col-xl-3 d-flex flex-column justify-content-between">
            <?= RangeButtonWidget::widget() ?>
            <?= Html::button('Загрузить из Excel', [
                'class' => 'button-regular w-100 button-hover-content-red ajax-modal-btn',
                'data-title' => 'Загрузка клиентов в CRM из Excel',
                'data-url' => Url::to(['import']),
            ]) ?>
        </div>
    </div>
</div>

<?= $this->render('widgets/clients-filter', ['filter' => $filter]) ?>

<?php ContentDecorator::begin([
    'viewFile' => __DIR__ . '/decorators/client-select.php',
    'params' => [
        'form' => $form,
    ],
]); ?>

<div class="table-container products-table clearfix grid-view">
<?= GridView::widget([
    'id' => 'product-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $filter,
    'emptyText' => $emptyText,
    'dataColumnClass' => DataColumnWidget::class,
    'formatter' => ['class' => Formatter::class, 'nullDisplay' => ''],
    'tableOptions' => [
        'class' => 'table table-style table-count-list ' . $user->identity->config->getTableViewClass('crm_client'),
        'id' => 'datatable_ajax',
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],
    'headerRowOptions' => [
        'class' => 'heading',
    ],
    'options' => [
        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
    ],
    'rowOptions' => [
        'role' => 'row',
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->getTotalCount()]),
    'columns' => [
        [
            'class' => 'yii\grid\CheckboxColumn',
            'name' => 'ContractorSelectForm[contractor_id]',
            'visible' => $canEdit,
            'headerOptions' => [
                'style' => 'width: 20px;',
            ],
            'checkboxOptions' => function (Contractor $contractor) {
                return ['value' => $contractor->id];
            },
        ],
        [
            'label' => 'Дата добавления',
            'attribute' => 'created_at',
            'value' => 'created_at',
            'configAttribute' => 'crm_client_created',
            'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
            'headerOptions' => [
                'style' => 'width: 1%',
            ],
        ],
        [
            'label' => 'Клиент',
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function (Contractor $contractor): string {
                return Html::a(
                    $contractor->getNameWithType(),
                    ['client/tasks', 'contractor_id' => $contractor->id],
                    ['data-pjax' => 0]
                );
            }
        ],
        [
            'label' => 'Тип отношений',
            'attribute' => 'client_type_id',
            'value' => 'client.type.name',
            'configAttribute' => 'crm_client_type',
            'contentOptions' => [
                'class' => 'nowrap',
            ],
            'filter' => ['' => 'Все'] + $filter->getListFactory()->createClientTypeList()->getItems(),
            's2width' => '200px',
            'enableSorting' => false,
            'hideSearch' => false,
        ],
        [
            'label' => 'ФИО контакта',
            'attribute' => 'contact_name',
            'value' => 'defaultContact.contact_name',
            'configAttribute' => 'crm_contact_name',
            'contentOptions' => function (Contractor $contractor): array {
                return [
                    'class' => 'contact-cell',
                    'title' => $contractor->getActualContact()->contact_name ?? '',
                ];
            },
            'filter' => ['' => 'Все'] + $filter->getListFactory()->createContactNameList()->getItems(),
            's2width' => '200px',
            'enableSorting' => false,
            'hideSearch' => false,
        ],
        [
            'label' => 'Должность',
            'attribute' => 'client_position_id',
            'configAttribute' => 'crm_client_position',
            'value' => 'defaultContact.position.name',
            'contentOptions' => function (Contractor $contractor): array {
                return [
                    'class' => 'contact-cell',
                    'title' => $contractor->getActualContact()->position->name ?? '',
                ];
            },
            'filter' => ['' => 'Все'] + $filter->getListFactory()->createPositionList()->getItems(),
            's2width' => '200px',
            'enableSorting' => false,
            'hideSearch' => false,
        ],
        [
            'label' => 'Телефон',
            'attribute' => 'contact_phone',
            'value' => 'defaultContact.primary_phone_number',
            'headerOptions' => [
                'style' => 'min-width: 100px;',
            ],
            'filter' => ['' => 'Все'] + $filter->getListFactory()->createContactPhoneList()->getItems(),
            's2width' => '200px',
            'enableSorting' => false,
            'hideSearch' => false,
        ],
        [
            'label' => 'E-mail',
            'attribute' => 'contact_email',
            'value' => 'defaultContact.email_address',
            'configAttribute' => 'crm_contact_email',
            'filter' => ['' => 'Все'] + $filter->getListFactory()->createContactEmailList()->getItems(),
            's2width' => '200px',
            'enableSorting' => false,
            'hideSearch' => false,
        ],
        [
            'label' => 'Событие',
            'attribute' => 'event_type',
            'value' => 'client.task.eventName',
            'contentOptions' => $contentOptions,
            'filter' => ['' => 'Все'] + $filter->getListFactory()->createTaskEventList()->getItems(),
            's2width' => '200px',
            'enableSorting' => false,
            'hideSearch' => false,
        ],
        [
            'label' => 'Статус',
            'attribute' => 'client_status_id',
            'value' => 'client.status.name',
            'contentOptions' => [
                'class' => 'nowrap',
            ],
            'filter' => ['' => 'Все'] + $filter->getListFactory()->createStatusList()->getItems(),
            's2width' => '200px',
            'enableSorting' => false,
            'hideSearch' => false,
        ],
        [
            'label' => 'Источник',
            'attribute' => 'campaign_id',
            'value' => 'campaign.name',
            'configAttribute' => 'contractor_campaign',
            'filter' => ['' => 'Все'] + $filter->getListFactory()->createCampaignList()->getItems(),
            's2width' => '200px',
            'enableSorting' => false,
            'hideSearch' => false,
        ],
        [
            'label' => 'Вид деятельности',
            'attribute' => 'activity_id',
            'value' => 'activity.name',
            'configAttribute' => 'contractor_activity',
            'contentOptions' => function (Contractor $contractor): array {
                return [
                    'class' => 'contact-cell',
                    'title' => $contractor->activity->name ?? '',
                ];
            },
            'filter' => ['' => 'Все'] + $filter->getListFactory()->createActivityList()->getItems(),
            's2width' => '200px',
            'enableSorting' => false,
            'hideSearch' => false,
        ],
        [
            'attribute' => 'comment',
            'configAttribute' => 'crm_client_comment',
            'headerOptions' => [
                'class' => 'w-130',
            ],
            'contentOptions' => [
                'class' => 'w-130',
            ],
            'format' => 'html',
            'value' => function (Contractor $contractor): string {
                return Html::tag('div', Html::encode($contractor->comment), [
                    'class' => 'text-truncate w-130',
                    'title' => $contractor->comment,
                ]);
            },
        ],
        [
            'label' => 'Чек',
            'attribute' => 'client_check_id',
            'value' => 'client.check.name',
            'configAttribute' => 'crm_client_check',
            'filter' => ['' => 'Все'] + $filter->getListFactory()->createCheckList()->getItems(),
            's2width' => '200px',
            'enableSorting' => false,
            'hideSearch' => false,
        ],
        [
            'label' => 'Тип обращения',
            'attribute' => 'client_reason_id',
            'value' => 'client.reason.name',
            'configAttribute' => 'crm_client_reason',
            'filter' => ['' => 'Все'] + $filter->getListFactory()->createReasonList()->getItems(),
            's2width' => '200px',
            'enableSorting' => false,
            'hideSearch' => false,
        ],
        [
            'label' => 'Ответственный',
            'attribute' => 'responsible_employee_id',
            'value' => 'responsibleEmployeeCompany.shortFio',
            'filter' => ['' => 'Все'] + $filter->getListFactory()->createEmployeeList()->getItems(),
            's2width' => '200px',
            'enableSorting' => false,
            'hideSearch' => false,
        ],
    ],
]) ?>
</div>

<?php ContentDecorator::end() ?>
