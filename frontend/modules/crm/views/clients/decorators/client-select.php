<?php

namespace frontend\modules\crm\views;

use frontend\modules\crm\assets\ClientSelectAsset;
use frontend\modules\crm\models\ContractorSelectForm;
use frontend\modules\crm\models\ListFactory;
use kartik\select2\Select2;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Html;
use yii\web\View;

/**
 * @var View $this
 * @var ContractorSelectForm $form
 * @var string $content
 */

ClientSelectAsset::register($this);

$text = 'Для выбранных клиентов изменить на:';
$dialogs = [
    [
        'id' => 'changeStatusDialog',
        'label' => $form->getAttributeLabel('client_status_id'),
        'text' => $text,
        'enabled' => true,
        'button' => 'Статус',
        'title' => 'Изменить статус',
        'input' => Select2::widget([
            'model' => $form,
            'attribute' => 'client_status_id',
            'data' => (new ListFactory)->createStatusList()->getItems(),
            'pluginOptions' => [
                'placeholder' => '',
                'width' => '100%',
            ],
            'hideSearch' => true,
        ]),
    ],
    [
        'id' => 'changeActivityDialog',
        'label' => $form->getAttributeLabel('activity_id'),
        'text' => $text,
        'enabled' => true,
        'button' => 'Вид деятельности',
        'title' => 'Изменить вид деятельности',
        'attribute' => 'activity_id',
        'input' => Select2::widget([
            'model' => $form,
            'attribute' => 'activity_id',
            'data' => (new ListFactory)->createActivityList()->getItems(),
            'pluginOptions' => [
                'placeholder' => '',
                'width' => '100%',
            ],
            'hideSearch' => true,
        ]),
    ],
    [
        'id' => 'changeCampaignDialog',
        'label' => $form->getAttributeLabel('campaign_id'),
        'text' => $text,
        'enabled' => true,
        'button' => 'Источник',
        'title' => 'Изменить источник',
        'input' => Select2::widget([
            'model' => $form,
            'attribute' => 'campaign_id',
            'data' => (new ListFactory)->createCampaignList()->getItems(),
            'pluginOptions' => [
                'placeholder' => '',
                'width' => '100%',
            ],
            'hideSearch' => true,
        ]),
    ],
    [
        'id' => 'changeEmployeeDialog',
        'label' => $form->getAttributeLabel('responsible_employee_id'),
        'text' => $text,
        'enabled' => $form->canChangeEmployee,
        'button' => 'Ответственного',
        'title' => 'Изменить ответственного',
        'attribute' => 'responsible_employee_id',
        'input' => Select2::widget([
            'model' => $form,
            'attribute' => 'responsible_employee_id',
            'data' => (new ListFactory)->createEmployeeList()->getItems(),
            'pluginOptions' => [
                'placeholder' => '',
                'width' => '100%',
            ],
            'hideSearch' => true,
        ]),
        'checkbox' => Html::activeCheckbox($form, 'responsible_employee_task', [
            'label' => 'Изменить ответственного по задачам',
        ])
    ],
];

?>

<?= Html::beginForm(null, 'post', ['id' => 'clientSelectForm']); ?>

<?= $content ?>

<div id="clientSelectSummary" class="wrap wrap_btns check-condition">
    <div class="row align-items-center justify-content-end">
        <div class="column flex-shrink-0 mr-3">
            <span class="checkbox-txt total-txt-foot ml-3" style="padding-left: 6px">
                Выбрано клиентов: <strong class="pl-2 total-count">0</strong>
            </span>
        </div>
        <div class="column ml-auto"></div>
        <div class="column">
            <div class="dropup">
                <?= Html::button('<span class="pr-2">Изменить</span>  '.$this->render('//svg-sprite', ['ico' => 'shevron']), [
                    'class' => 'button-regular button-regular-more button-hover-transparent button-clr dropdown-toggle',
                    'data-toggle' => 'dropdown',
                ]) ?>
                <?= Dropdown::widget([
                    'items' => array_map(function (array $dialog) {
                        return [
                            'label' => $dialog['button'],
                            'url' => "#{$dialog['id']}",
                            'linkOptions' => ['data-toggle' => 'modal'],
                            'visible' => $dialog['enabled'],
                        ];
                    }, $dialogs),
                    'options' => ['class' => 'form-filter-list list-clr dropdown-menu-right'],
                ]) ?>
            </div>
        </div>
    </div>
</div>

<?php foreach ($dialogs as $dialog): ?>
    <?= $this->render('@frontend/modules/crm/views/widgets/row-select-dialog', $dialog) ?>
<?php endforeach; ?>

<?= Html::endForm() ?>
