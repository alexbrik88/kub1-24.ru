<?php

use frontend\components\Icon;
use kartik\file\FileInput;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\widgets\Pjax;

/**
 * @var View $this
 * @var ClientImportForm $model
 */

$result = '';
foreach ((array) ($model->getImportReport()['message'] ?? []) as $message) {
    $result .= Html::tag('div', $message);
}
?>

<style type="text/css">
.field-clientimportform-file.has_xls_import_result .file_upload_caption,
.field-clientimportform-file > .file-input.file-input-new .file_upload_caption {
    display: none;
}
.field-clientimportform-file.has_xls_import_result .file_upload_info,
.field-clientimportform-file > .file-input:not(.file-input-new) .file_upload_info {
    display: none;
}
.field-clientimportform-file:not(.has_xls_import_result) .xls_import_result {
    display: none;
}
.field-clientimportform-file.has_xls_import_result .xls_import_result {
    background-color: #ff8795;
    border-radius: 5px !important;
    margin: 0 auto;
    padding: 13px;
}
</style>

<?php Pjax::begin([
    'id' => 'client-import-pjax',
    'formSelector' => '#client-import-form',
    'linkSelector' => false,
    'enablePushState' => false,
]) ?>

<?php $form = ActiveForm::begin([
    'id' => 'client-import-form',
    'options' => [
        'data-pjax' => true,
        'enctype' => 'multipart/form-data',
    ],
]); ?>

<?= $form->field($model, 'file', ['options' => ['class' => $result ? 'has_xls_import_result' : '']])->widget(FileInput::class, [
    'options' => ['class' => ''],
    'pluginOptions' => [
        'showCaption' => false,
        'browseClass' => 'button-regular width-160 button-hover-content-red',
        'browseIcon' => Icon::get('add-icon', ['class' => 'mr-2']),
        'browseLabel' => 'Добавить файл',
        'uploadClass' => 'button-regular width-160 button-hover-content-red',
        'uploadIcon' => '',
        'removeClass' => 'link border-0 bg-white',
        'removeIcon' => '<i class="fa fa-times-circle"></i>',
        'removeLabel' => '',
        'layoutTemplates' => [
            'icon' => '',
            'main2' => '<div class="d-flex justify-content-between mb-4">{browse}{upload}</div>'.
                '<div class="kv-upload-progress hide"></div>'.
                '<div class="file_upload_caption"><div class="d-flex">{caption} {remove}</div></div>'.
                $this->render('_import_info'),
            'caption' => '<div class="file-caption {class}">'.
                '<span class="file-caption-ellipsis">&hellip;</span>'.
                '<div class="file-caption-name"></div>'.
                '</div>',
        ]
    ],
])->label(false)->error(false)->hint($result ?: false, [
    'tag' => 'div',
    'class' => 'xls_import_result',
]) ?>

<?php ActiveForm::end(); ?>

<?php Pjax::end() ?>

<script type="text/javascript">
    $(document).on('fileselect', '#clientimportform-file', function(event, numFiles, label) {
        $(this).closest('.has_xls_import_result').toggleClass('has_xls_import_result', false);
    });
</script>
