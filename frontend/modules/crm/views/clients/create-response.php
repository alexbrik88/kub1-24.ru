<?php

namespace frontend\modules\crm\views;

use common\models\Contractor;
use yii\web\View;

/**
 * @var View $this
 * @var Contractor $contractor
 */

$data = json_encode([
    'id' => $contractor->id,
    'face_type' => $contractor->face_type,
    'label' => addslashes($contractor->getNameWithType()),
]);

?>

<?php $this->registerJs(<<<JS
    (function () {
        $('#add-client-widget').first().modal('hide');
        $(document).trigger('client:created', JSON.parse('{$data}'));
    })();
JS) ?>
