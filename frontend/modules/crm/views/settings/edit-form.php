<?php

namespace frontend\modules\crm\views;

use frontend\modules\crm\models\TypeForm;
use frontend\modules\crm\models\AbstractType;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\web\View;

/**
 * @var View $this
 * @var TypeForm $form
 * @var bool $statusDisabled
 */

$this->title = sprintf(
    '%s %s',
    $form->scenario == $form::SCENARIO_CREATE ? 'Добавить' : 'Редактировать',
    mb_strtolower($form->type->getTypeName(), 'UTF-8')
);

?>

<h4 class="modal-title"><?= Html::encode($this->title) ?></h4>

<?php $widget = ActiveForm::begin([
    'id' => 'typeForm',
    'fieldConfig' => [
        'labelOptions' => ['class' => 'label'],
        'options' => ['class' => 'form-group col-12'],
    ],
    'options' => [
        'data-pjax' => true,
    ],
]); ?>

<div class="row">

<?= $widget->field($form, 'name')->textInput()->label($form->type->getAttributeLabel('name')) ?>

<?= $widget->field($form, 'status')->widget(Select2::class, [
    'data' => AbstractType::TYPE_STATUS_LIST,
    'options' => [
        'placeholder' => '',
        'id' => 'typeFormStatus',
    ],
    'pluginOptions' => [
        'width' => '100%',
        'readonly' => $statusDisabled,
        'disabled' => $statusDisabled,
    ],
]) ?>

</div>

<?= $this->render('@frontend/modules/crm/views/widgets/dialog-buttons') ?>

<?php ActiveForm::end(); ?>
