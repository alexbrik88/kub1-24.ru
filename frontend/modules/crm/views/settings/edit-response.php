<?php

namespace frontend\modules\crm\views;

use frontend\modules\crm\models\AbstractType;
use yii\web\View;

/**
 * @var View $this
 * @var AbstractType $type
 */

$data = json_encode([
    'id' => $type->getAttribute($type::getIdKey()),
    'label' => addslashes($type->name),
]);

?>

<?php $this->registerJs(<<<JS
    (function () {
        $('#add-item-widget').first().modal('hide');
        $(document).trigger('type:created', JSON.parse('{$data}'));
    })();
JS) ?>
