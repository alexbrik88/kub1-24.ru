<?php

namespace frontend\modules\crm\views;

use frontend\modules\crm\widgets\PjaxWidget;
use frontend\rbac\permissions\crm\ConfigAccess;
use frontend\themes\kub\widgets\ImportDialogWidget;
use yii\bootstrap4\Tabs;
use yii\web\User;
use yii\web\View;

/**
 * @var View $this
 * @var string $content
 * @var User $user
 * @var string $activeTab
 */

?>

<?php PjaxWidget::begin(['id' => 'pjaxContent']) ?>

<div class="d-flex flex-wrap align-items-center">
    <h4>Настройки</h4>
</div>

<?= Tabs::widget([
    'renderTabContent' => false,
    'options' => [
        'class' => 'nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mb-4',
    ],
    'items' => [
        [
            'label' => 'Сделки',
            'url' => ['deal-type/index'],
            'active' => ($activeTab == 'deal-type'),
        ],
        [
            'label' => 'Источник',
            'url' => ['contractor-campaign/index'],
            'active' => ($activeTab == 'contractor-campaign'),
        ],
        [
            'label' => 'Вид деятельности',
            'url' => ['contractor-activity/index'],
            'active' => ($activeTab == 'contractor-activity'),
        ],
        [
            'label' => 'Должность',
            'url' => ['client-position/index'],
            'active' => ($activeTab == 'client-position'),
        ],
        [
            'label' => 'Тип',
            'url' => ['client-type/index'],
            'active' => ($activeTab == 'client-type'),
        ],
        [
            'label' => 'Отказы',
            'url' => ['refusal/index'],
            'active' => ($activeTab == 'refusal'),
        ],
        [
            'label' => 'Общие',
            'url' => ['config/index'],
            'visible' => $user->can(ConfigAccess::VIEW),
            'active' => ($activeTab == 'config'),
        ],
        [
            'label' => 'Дополнительно',
            'visible' => $user->can(ConfigAccess::VIEW),
            'linkOptions' => [
                'aria-expanded' => 'false',
            ],
            'items' => [
                [
                    'label' => 'Чек',
                    'url' => ['client-check/index'],
                    'active' => ($activeTab == 'client-check'),
                ],
                [
                    'label' => 'Тип обращения',
                    'url' => ['client-reason/index'],
                    'active' => ($activeTab == 'client-reason'),
                ],
                [
                    'label' => 'Статус клиента',
                    'url' => ['client-status/index'],
                    'active' => ($activeTab == 'client-status'),
                ],
                [
                    'label' => 'Результат по задаче',
                    'url' => ['task-result/index'],
                    'active' => ($activeTab == 'task-result'),
                ],
            ],
        ],
    ],
]) ?>

<div>
    <?= $content ?>
</div>
<?php PjaxWidget::end(); ?>

<?= ImportDialogWidget::widget() ?>
