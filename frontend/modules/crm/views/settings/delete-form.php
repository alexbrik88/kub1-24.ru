<?php

namespace frontend\modules\crm\views;

use frontend\modules\crm\models\TypeForm;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\web\View;

/**
 * @var View $this
 * @var TypeForm $form
 */

$this->title = 'Вы действительно хотите удалить запись?'

?>

<h4 class="modal-title text-center"><?= Html::encode($this->title) ?></h4>

<?php ActiveForm::begin([
    'id' => 'typeForm',
    'fieldConfig' => [
        'labelOptions' => ['class' => 'label'],
        'options' => ['class' => 'form-group col-12'],
    ],
    'options' => [
        'data-pjax' => true,
    ],
]); ?>

<div class="form-group text-center mb-0">
    <?= Html::submitButton('Да', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2',
    ]) ?>

    <?= Html::button('Нет', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-2',
        'data-dismiss' => 'modal',
    ]) ?>
</div>

<?php ActiveForm::end(); ?>
