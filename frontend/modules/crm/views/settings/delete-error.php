<?php

namespace frontend\modules\crm\views;

use yii\bootstrap4\Html;
use yii\web\View;

/**
 * @var View $this
 * @var string $errorMessage
 */

$this->title = $errorMessage;

?>

<h4 class="modal-title text-center"><?= Html::encode($this->title) ?></h4>

<div class="form-group text-center mb-0">
    <?= Html::button('Закрыть', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-2',
        'data-dismiss' => 'modal',
    ]) ?>
</div>
