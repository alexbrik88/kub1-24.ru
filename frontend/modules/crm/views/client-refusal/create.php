<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\crm\models\ClientRefusal */

?>
<div class="client-refusal-create">

    <?= $this->render('_form', [
        'model' => $model,
        'refusalArray' => $refusalArray,
    ]) ?>

</div>
