<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\crm\models\ClientRefusal */
/* @var $refusalArray frontend\modules\crm\models\Refusal[] */
/* @var $form yii\bootstrap4\ActiveForm */

$refusalData = ArrayHelper::map($refusalArray, 'refusal_id', 'name');
?>

<div class="client-refusal-form">

    <?php $form = ActiveForm::begin([
        'id' => 'client-refusal-form',
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    ]); ?>

    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'refusal_id')->widget(Select2::class, [
                'data' => $refusalData,
                'options' => [
                    'placeholder' => '',
                ],
                'pluginOptions' => [
                    'width' => '100%',
                ],
            ]) ?>
        </div>
    </div>

    <?= $form->field($model, 'details')->textarea(['rows' => 3]) ?>

    <div class="d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-width button-regular button-regular_red ladda-button',
        ]) ?>
        <?= Html::submitButton('Отменить', [
            'class' => 'button-width button-regular button-hover-transparent',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
