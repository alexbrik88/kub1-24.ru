<?php

use common\components\grid\GridView;
use common\models\Contractor;
use frontend\modules\crm\widgets\GridButtonWidget;
use frontend\rbac\permissions\crm\ClientAccess;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\i18n\Formatter;
use yii\web\View;

/** @var $this yii\web\View */
/** @var $isContractor bool */

$this->title = 'Причина отказа';

?>

<div class="wrap p-2 mb-2">
    <div class="d-flex align-items-center">
        <div class="mr-auto pl-2">
            <h4 class="mb-1"><?= Html::encode($this->title); ?></h4>
        </div>
        <div class="" style="width: 260px;">
            <?= \frontend\widgets\RangeButtonWidget::widget(); ?>
        </div>
    </div>
</div>

<div class="table-container products-table clearfix">
<?= GridView::widget([
    'id' => 'product-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'formatter' => ['class' => Formatter::class, 'nullDisplay' => ''],
    'tableOptions' => [
        'class' => 'table table-style table-count-list',
        'id' => 'datatable_ajax',
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],
    'headerRowOptions' => [
        'class' => 'heading',
    ],
    'options' => [
        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
    ],
    'rowOptions' => [
        'role' => 'row',
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->getTotalCount()]),
    'columns' => [
        [
            'attribute' => 'created_at',
            'label' => 'Дата отказа',
            'value' => function($model) {
                return ($d = date_create($model->created_at)) ? $d->format('d.m.Y') : '-';
            },
        ],
        [
            'attribute' => 'contractor_id',
            'label' => 'Клиент',
            'filter' => $searchModel->getContractorItems(),
            'format' => 'html',
            'value' => function($model) {
                return Html::a(Html::encode($model->contractor->shortTitle), [
                    '/crm/client/tasks',
                    'contractor_id' => $model->contractor_id,
                ]);
            },
            's2width' => '300px',
        ],
        [
            'attribute' => 'refusal_id',
            'label' => 'Вариант отказа',
            'filter' => $searchModel->getRefusalItems(),
            'value' => 'refusal.name',
            's2width' => '200px',
        ],
        [
            'attribute' => 'details',
            'label' => 'Описание причины отказа',
        ],
        [
            'attribute' => 'employee_id',
            'label' => 'Ответственный',
            'filter' => $searchModel->getEmployeeItems(),
            'value' => 'employeeCompany.shortFio',
            's2width' => '200px',
        ],
    ],
]) ?>
</div>
