<?php

namespace frontend\modules\crm\views;

use common\components\grid\GridView;
use common\models\Contractor;
use frontend\modules\crm\widgets\GridButtonWidget;
use frontend\rbac\permissions\crm\ClientAccess;
use Yii;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\i18n\Formatter;
use yii\web\View;

/** @var $this yii\web\View */
/** @var $contractor common\models\Contractor */
/** @var $isContractor bool */

$this->title = 'Причина отказа';

$canEdit = (Yii::$app->user->can(ClientAccess::EDIT, ['contractor' => $contractor]));

?>

<div class="page-head d-flex flex-wrap align-items-center mb-2">
    <h4><?= Html::encode($this->title); ?></h4>

    <?= GridButtonWidget::widget([
        'label' => 'Добавить',
        'icon' => 'add-icon',
        'options' => [
            'class' => 'button-regular button-regular_red pl-3 pr-3 ml-auto ajax-form-button',
            'data-url' => Url::to(['client-refusal/create', 'contractor_id' => $contractor->id, 'contractor' => $isContractor]),
            'data-title' => 'Добавить отказ клиента',
        ],
    ]) ?>
</div>

<div class="table-container products-table clearfix">
<?= GridView::widget([
    'id' => 'product-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'formatter' => ['class' => Formatter::class, 'nullDisplay' => ''],
    'tableOptions' => [
        'class' => 'table table-style table-count-list',
        'id' => 'datatable_ajax',
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],
    'headerRowOptions' => [
        'class' => 'heading',
    ],
    'options' => [
        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
    ],
    'rowOptions' => [
        'role' => 'row',
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->getTotalCount()]),
    'columns' => [
        [
            'attribute' => 'created_at',
            'label' => 'Дата отказа',
            'value' => function($model) {
                return ($d = date_create($model->created_at)) ? $d->format('d.m.Y') : '-';
            },
        ],
        [
            'attribute' => 'refusal_id',
            'label' => 'Вариант отказа',
            'filter' => $searchModel->getRefusalItems(),
            'value' => 'refusal.name',
            's2width' => '200px',
        ],
        [
            'attribute' => 'details',
            'label' => 'Подробное описание',
        ],
        [
            'attribute' => 'employee_id',
            'label' => 'Ответственный',
            'filter' => $searchModel->getEmployeeItems(),
            'value' => 'employeeCompany.shortFio',
            's2width' => '200px',
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => '/crm/client-refusal',
            'template' => '{update} {delete}',
            'urlCreator' => function ($action, $model, $key, $index, $column) {
                $params = [
                    'contractor_id' => $model->contractor_id,
                    'id' => (string) $key,
                ];
                $params[0] = $column->controller . '/' . $action;

                return Url::toRoute($params);
            },
            'buttons' => [
                'update' => function ($url, $model, $key) {
                    $options = [
                        'data-pjax' => '0',
                        'class' => 'ajax-form-button',
                        'title' => 'Редактировать',
                        'data-url' => $url,
                        'data-title' => 'Изменить отказ клиента',
                    ];
                    $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-pencil"]);

                    return Html::a($icon, '#', $options);
                },
                'delete' => function ($url, $model, $key) {
                    return \frontend\themes\kub\widgets\ConfirmModalWidget::widget([
                        'toggleButton' => [
                            'tag' => 'span',
                            'label' => '',
                            'title' => 'Удалить',
                            'class' => 'glyphicon glyphicon-trash link',
                        ],
                        'confirmUrl' => $url,
                        'confirmParams' => [],
                        'message' => 'Вы уверены, что хотите удалить этот отказ?',
                    ]);
                },
            ],
        ],
    ],
]) ?>
</div>
