<?php

use frontend\components\Icon;
use frontend\modules\crm\assets\RowSelectAsset;
use frontend\modules\crm\models\ListFactory;
use frontend\themes\kub\widgets\SpriteIconWidget;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\web\View;

/**
 * @var $this
 * @var $model
 * @var $contractor
 */

$model->responsible_employee_id = $contractor->responsible_employee_id;
?>

<?php Modal::begin([
    'id' => 'client_responsible_modal',
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
    'title' => 'Изменить ответственного',
]); ?>

    <?php $form = ActiveForm::begin([
        'id' => 'client_responsible_form',
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    ]) ?>

        <?= Html::activeHiddenInput($model, 'contractor_id[]', [
            'value' => $contractor->id,
        ]) ?>

        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'responsible_employee_id')->widget(Select2::classname(), [
                    'data' => (new ListFactory)->createEmployeeList()->getItems(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ]
                ]) ?>
            </div>
            <div class="col-6">
                <label class="label">&nbsp;</label>
                <div class="pt-2">
                    <?= $form->field($model, 'responsible_employee_task')->checkbox([
                        'label' => 'Изменить ответственного по задачам',
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-6 text-left mb-0">
                <?= Html::submitButton('Сохранить', [
                    'class' => 'button-regular button-width button-regular_red button-clr',
                    'data-style' => 'expand-right',
                ]) ?>
            </div>

            <div class="col-6 text-right mb-0">
                <?= Html::button('Отменить', [
                    'class' => 'button-clr button-width button-regular button-hover-transparent',
                    'onclick' => "$(this).closest('.modal').first().modal('hide'); return false;",
                ]) ?>
            </div>
        </div>

    <?php ActiveForm::end() ?>

<?php Modal::end(); ?>

<?php $this->registerJs(<<<JS
    (function () {
        $('#client_responsible_modal').on('hidden.bs.modal', function () {
            $(this).find('select').val('');
            $(this).find('select').trigger('change');
        });
    })();
JS); ?>
