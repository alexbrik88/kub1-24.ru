<?php

namespace frontend\modules\crm\views;

use common\components\date\DateHelper;
use common\models\Contractor;
use frontend\components\WebUser;
use frontend\models\Documents;
use frontend\modules\crm\assets\ClientHeaderAsset;
use frontend\modules\crm\models\Client;
use frontend\modules\crm\models\ClientForm;
use frontend\modules\crm\models\Contact;
use frontend\modules\crm\models\Task;
use frontend\modules\crm\models\ListFactory;
use frontend\modules\crm\widgets\AjaxFormDialog;
use frontend\modules\crm\widgets\GridButtonWidget;
use frontend\rbac\permissions\crm\ClientAccess;
use frontend\themes\kub\components\Icon;
use frontend\themes\kub\widgets\SpriteIconWidget;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var WebUser $user
 * @var Contractor $contractor
 * @var ClientForm $clientForm
 * @var bool $isContractor
 */

ClientHeaderAsset::register($this);

$contact = $contractor->getActualContact();
$client = $contractor->client ?: Client::createInstance($contractor);
$canEdit = $user->can(ClientAccess::EDIT, ['contractor' => $contractor]);

if ($contractor->face_type == Contractor::TYPE_PHYSICAL_PERSON) {
    $contact->contact_name = $contractor->getRealContactName();
}

$backUrl = $this->context->backUrl ?? null;
if (!$backUrl) {
    $backUrl = $isContractor ? ['/contractor/index', 'type' => $contractor->type] : ['clients/index'];
}
?>

<?= AjaxFormDialog::widget() ?>
<div id="client-header">

<?= Html::a('Назад к списку', $backUrl, [
    'class' => 'link mb-2',
    'data-pjax' => 0,
]) ?>

<?php ActiveForm::begin(['options' => ['id' => 'clientForm']]); ?>

<div class="wrap wrap_padding_small pl-4 pr-3 pb-2 mb-2">
    <div class="pl-1 pb-1">
        <div class="page-in row">
            <div class="col-9 column pr-4">
                <div class="pr-2">
                    <div class="row align-items-center justify-content-between mb-3">
                        <h4 class="column mb-2" style="max-width: 550px;">
                            <?= Html::encode($contractor->getNameWithType()); ?>
                        </h4>
                        <div class="column" style="margin-bottom: auto;">

                            <?= GridButtonWidget::widget([
                                'icon' => 'info',
                                'options' => [
                                    'title' => 'Последние действия',
                                    'class' => 'button-regular button-regular_red button-clr w-44 mb-2 mr-2',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#lastActions',
                                ],
                            ]) ?>

                        <?php if ($canEdit): ?>
                            <?= Html::a(
                                SpriteIconWidget::widget(['icon' => 'pencil']),
                                ['clients/update', 'contractor_id' => $contractor->id],
                                [
                                    'title' => 'Редактировать',
                                    'class' => 'button-regular button-regular_red button-clr w-44 mb-2 ml-1',
                                    'data-pjax' => 0,
                                ]
                            ) ?>
                        <?php endif; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col mb-4 pb-2">
                            <div class="label weight-700 mb-3">Контакт</div>
                            <div><?= Html::encode($contact->contact_name) ?: '—' ?></div>
                            <div class="label weight-700 mb-3 mt-4">Должность</div>
                            <div><?= $contact->position ? Html::encode($contact->position->name) : '—' ?></div>
                            <div class="label weight-700 mb-3 mt-4">Статус клиента</div>
                            <div>
                                <?php if ($canEdit): ?>
                                    <?= Select2::widget([
                                        'model' => $form,
                                        'attribute' => 'client_status_id',
                                        'options' => [
                                            'id' => 'client_status_id',
                                            'value' => $client->client_status_id,
                                            'data-refusal-btn' => '#client-refusal-create-btn-'.$client->contractor_id,
                                        ],
                                        'data' => (new ListFactory)->createStatusList()->getItems(),
                                        'pluginOptions' => [
                                            'width' => '150px',
                                        ],
                                        'hideSearch' => true,
                                        'pluginLoading' => false,
                                    ]) ?>
                                <?php else: ?>
                                    <?= $client->status ? Html::encode($client->status->name) : '—' ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col mb-4 pb-2">
                            <div class="label weight-700 mb-3">Телефон</div>
                            <div><?= Html::encode($contact->primary_phone_number) ?: '—' ?></div>
                            <div class="label weight-700 mb-3 mt-4">Чек</div>
                            <div><?= $client->check ? Html::encode($client->check->name) : '—' ?></div>
                            <div class="label weight-700 mb-3 mt-4">Источник</div>
                            <div><?= $contractor->campaign ? Html::encode($contractor->campaign->name) : '—' ?></div>
                        </div>
                        <div class="col mb-4 pb-2">
                            <div class="label weight-700 mb-3">E-mail</div>
                            <div>
                                <?php if ($contact->email_address): ?>
                                    <a class="link" href="mailto:<?= $contact->email_address ?>">
                                        <?= Html::encode($contact->email_address) ?>
                                    </a>
                                <?php else: ?>
                                    —
                                <?php endif; ?>
                            </div>
                            <div class="label weight-700 mb-3 mt-4">
                                <?= Html::encode($clientForm->getAttributeLabel('deal_stage_id')) ?>
                            </div>
                            <div>
                            <?php if ($canEdit && $client->dealStage): ?>
                                <?= Select2::widget([
                                    'model' => $clientForm,
                                    'attribute' => 'deal_stage_id',
                                    'options' => ['id' => 'deal_stage_id'],
                                    'data' => (new ListFactory)->createDealStageList($client->deal)->getItems(),
                                    'pluginOptions' => [
                                        'width' => '150px',
                                    ],
                                    'hideSearch' => true,
                                    'pluginLoading' => false,
                                ]) ?>
                            <?php else: ?>
                                <?= $client->dealStage ? Html::encode($client->dealStage->name) : '—' ?>
                            <?php endif; ?>
                            </div>
                            <div class="label weight-700 mb-3 mt-4">Ответственный</div>
                            <div>
                                <span class="link" data-toggle="modal" data-target="#client_responsible_modal">
                                    <?php if ($contractor->responsibleEmployeeCompany) : ?>
                                        <?= Html::encode($contractor->responsibleEmployeeCompany->getShortFio()) ?>
                                    <?php else : ?> : '—' ?>
                                        —
                                    <?php endif ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-3 column pl-0">
                <?php if ($canEdit): ?>
                <div class="pb-2 mb-1">
                    <?= GridButtonWidget::widget([
                        'label' => 'Задача',
                        'icon' => 'add-icon',
                        'options' => [
                            'id' => 'taskFormButton',
                            'title' => 'Добавить задачу',
                            'class' => 'button-regular button-clr button-regular_red w-100 ajax-form-button',
                            'data-url' => Url::to(['task/create', 'contractor_id' => $contractor->id]),
                            'data-title' => 'Добавить задачу № ' . Task::getNextNumber($user->identity->company),
                        ],
                    ]) ?>
                </div>
                <?php endif; ?>

                <div class="pb-2 mb-1">
                    <?= GridButtonWidget::widget([
                        'label' => 'Написать на e-mail',
                        'icon' => 'envelope',
                        'options' => [
                            'class' => 'button-regular button-hover-content-red button-clr w-100 open-send-to',
                            'data-toggle' => 'toggleVisible',
                            'data-target' => 'invoice',
                        ],
                    ]) ?>
                </div>

                <?php if ($canEdit): ?>
                <div class="pb-2 mb-1">
                    <?= GridButtonWidget::widget([
                        'label' => 'Договор',
                        'icon' => 'add-icon',
                        'options' => [
                            'title' => 'Добавить договор',
                            'class' => 'button-regular button-hover-content-red button-clr w-100 ajax-form-button',
                            'data-url' => Url::to([
                                'agreement/create',
                                'contractor_id' => $contractor->id,
                            ]),
                            'data-title' => 'Добавить договор',
                        ],
                    ]) ?>
                </div>
                <?php endif; ?>

                <?php if ($canEdit): ?>
                <div class="pb-2 mb-1">
                    <?= Html::a(
                        SpriteIconWidget::widget(['icon' => 'add-icon']) . Html::tag('span', 'Счёт', ['class' => 'ml-1']),
                        ['/documents/invoice/create', 'type' => Documents::IO_TYPE_OUT, 'contractorId' => $contractor->id],
                        ['class' => 'button-regular button-hover-content-red button-clr w-100',]
                    ) ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
</div>

<?= $this->render('responsible', [
    'model' => $form,
    'contractor' => $contractor,
]) ?>

<?php if ($canEdit): ?>
    <?= \frontend\widgets\AjaxModalWidget::widget([
        'id' => 'client-refusal-ajax-modal-box',
        'linkSelector' => '.client-refusal-create-btn',
    ]) ?>
    <?= Html::a('', [
        '/crm/client-refusal/create',
        'contractor_id' => $client->contractor_id,
    ], [
        'id' => 'client-refusal-create-btn-'.$client->contractor_id,
        'class' => 'client-refusal-create-btn hidden',
        'data-title' => 'Отказ',
    ]) ?>
<?php endif; ?>

<?php Modal::begin([
    'id' => 'lastActions',
    'title' => 'Последние действия',
    'options' => [
        'class' => 'doc-history-modal fade',
    ],
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
    'footer' => Html::button('OK', [
        'class' => 'button-regular button-regular_red button-clr w-44',
        'data-dismiss' => 'modal',
    ]),
]) ?>

    <div class="created-by">
        <?= date(DateHelper::FORMAT_USER_DATE, $contractor->created_at) ?>
        <?php if ($contractor->employee) : ?>
            Создал <?= $contractor->employee->fio ?>
        <?php else : ?>
            Автосоздание
        <?php endif ?>
    </div>

<?php Modal::end(); ?>

<?= $this->render('@frontend/views/contractor/view-customer', [
    'model' => $contractor,
    'user' => $user,
    'type' => Contractor::TYPE_CUSTOMER,
    'ioType' => Contractor::TYPE_CUSTOMER,
    'tabData' => [],
    'tab' => 'crm',
]) ?>

<?= $this->render('@frontend/modules/documents/views/invoice/view/_send_message', [
    'model' => $client,
    'header' => 'Написать клиенту',
    'useContractor' => true,
    'showSendPopup' => false,
]) ?>

<?php

$this->registerJs(<<<JS
    $(document).ready(function() {
        initMain();
    });
JS);

$display = $contractor->is_customer ? 'block' : 'none';

$this->registerCss(<<<CSS
    #contractor-header {
        display: none;
    }

    #contractor-tabs {
        display: {$display};
    }
CSS);

?>
