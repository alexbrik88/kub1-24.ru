<?php

namespace frontend\modules\crm\views;

use common\models\Contractor;
use frontend\components\WebUser;
use frontend\modules\crm\models\ClientForm;
use frontend\modules\crm\models\ClientStatus;
use frontend\modules\crm\widgets\PjaxWidget;
use frontend\modules\crm\widgets\TabsWidget;
use yii\helpers\ArrayHelper;
use yii\web\View;

/**
 * @var View $this
 * @var WebUser $user
 * @var Contractor $contractor
 * @var ClientForm $clientForm
 * @var bool $isContractor
 * @var string $content
 */

$createUrl = function (string $action) use ($contractor, $isContractor): array {
    $url = ["client/{$action}", 'contractor_id' => $contractor->id];

    if ($isContractor) {
        $url['contractor'] = $isContractor;
    }

    return $url;
};

$this->title = 'Карточка';

?>

<?= $this->render('header', [
    'user' => $user,
    'form' => $form,
    'contractor' => $contractor,
    'clientForm' => $clientForm,
    'isContractor' => $isContractor,
]) ?>

<?php PjaxWidget::begin(['id' => 'pjaxContent']) ?>

<?= TabsWidget::widget([
    'options' => [
        'class' => 'nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mb-4',
    ],
    'items' => [
        ['label' => 'Задачи', 'url' => $createUrl('tasks')],
        ['label' => 'Пожелания', 'url' => $createUrl('wish')],
        ['label' => 'Договоры', 'url' => $createUrl('agreements')],
        ['label' => 'История', 'url' => $createUrl('history')],
        ['label' => 'Карточка', 'url' => $createUrl('card'), 'visible' => !$contractor->is_customer],
        [
            'label' => 'Отказ',
            'url' => $createUrl('refusal'),
            'visible' => ArrayHelper::getValue($contractor->client, ['status', 'value']) == ClientStatus::STATUS_REFUSED,
        ],
    ],
]) ?>

<div>
    <?= $content ?>
</div>

<?php PjaxWidget::end(); ?>
