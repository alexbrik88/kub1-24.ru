<?php

namespace frontend\modules\crm\views;

use common\components\grid\GridView;
use common\models\Agreement;
use common\models\Contractor;
use frontend\modules\crm\models\AgreementFactory;
use frontend\modules\crm\models\AgreementFilter;
use frontend\modules\crm\widgets\GridButtonWidget;
use frontend\rbac\permissions\crm\ClientAccess;
use frontend\rbac\permissions\document\Document;
use frontend\themes\kub\widgets\SpriteIconWidget;
use Yii;
use yii\bootstrap4\Html;
use yii\grid\ActionColumn;
use yii\helpers\Url;
use yii\i18n\Formatter;
use yii\web\View;

/**
 * @var View $this
 * @var AgreementFilter $filter
 * @var Contractor $contractor
 * @var bool $isContractor
 */

$agreement = (new AgreementFactory)->createAgreement(Yii::$app->user->identity, $contractor);
$canEdit = (Yii::$app->user->can(ClientAccess::EDIT, ['contractor' => $contractor]));
$canEdit = ($canEdit && Yii::$app->user->can(Document::UPDATE, ['model' => $agreement, 'ioType' => $agreement->type]));
$dataProvider = $filter->getDataProvider();
$count = $filter->getTotalCount();
$emptyText = implode(' ', [
    'По заданным фильтрам задач не найдено.',
    Html::a('Очистить', ['client/agreements', 'contractor_id' => $contractor->id, 'contractor' => $isContractor]),
]);

if ($count == 0) {
    $emptyText = 'Вы ещё не добавили ни одного договора. ';

    if ($canEdit) {
        $emptyText .= Html::a('Добавить договор', '#', [
            'class' => 'ajax-form-button',
            'data-url' => Url::to(['agreement/create', 'contractor_id' => $contractor->id, 'contractor' => $isContractor]),
            'data-title' => 'Добавить договор',
        ]);
    }
}

?>

<?= $this->render('@frontend/modules/crm/views/widgets/search-filter', [
    'placeholder' => 'Поиск по названию...',
    'filter' => $filter,
    'contractor' => $contractor,
]) ?>

<div class="table-container products-table clearfix">
<?= GridView::widget([
    'id' => 'product-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $filter,
    'emptyText' => $emptyText,
    'formatter' => ['class' => Formatter::class, 'nullDisplay' => ''],
    'tableOptions' => [
        'class' => 'table table-style table-count-list',
        'id' => 'datatable_ajax',
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],
    'headerRowOptions' => [
        'class' => 'heading',
    ],
    'options' => [
        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
    ],
    'rowOptions' => [
        'role' => 'row',
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->getTotalCount()]),
    'columns' => [
        [
            'label' => 'Номер',
            'attribute' => 'document_number',
            'format' => 'raw',
            'headerOptions' => [
                'style' => 'width: 1%',
            ],
            'value' => function (Agreement $agreement) {
                $value = $agreement->document_additional_number
                    ? $agreement->document_additional_number . '-' . $agreement->document_number
                    : $agreement->document_number;

                if (Yii::$app->user->can(Document::VIEW, ['model' => $agreement]) && $agreement->template) {
                    return Html::a($value, [
                        '/documents/agreement/view',
                        'id' => $agreement->id,
                        'contractor' => $agreement->contractor_id,
                    ], ['data-pjax' => 0]);
                }

                return $value;
            },
        ],
        [
            'label' => 'Дата',
            'attribute' => 'document_date',
            'headerOptions' => [
                'style' => 'width: 1%',
            ],
        ],
        [
            'label' => 'Название',
            'attribute' => 'document_name',
        ],
        [
            'label' => 'Тип документа',
            'attribute' => 'document_type_id',
            'format' => 'raw',
            's2width' => '200px',
            'enableSorting' => false,
            'filter' => ['' => 'Все'] + $filter->getListFactory()->createAgreementTypeList()->getItems(),
            'value' => 'agreementType.name',
        ],
        [
            'label' => 'Шаблон',
            'attribute' => 'agreement_template_id',
            'format' => 'raw',
            's2width' => '200px',
            'enableSorting' => false,
            'filter' => ['' => 'Все'] + $filter->getListFactory()->createAgreementTemplateList()->getItems(),
            'value' => 'template.fullName',
        ],
        [
            'label' => 'Скан',
            'attribute' => 'has_file',
            'format' => 'raw',
            'enableSorting' => false,
            'headerOptions' => [
                'style' => 'width: 1%',
            ],
            'value' => function (Agreement $agreement): string {
                if (!isset($agreement->files[0])) {
                    return '';
                }

                return Html::a(
                    SpriteIconWidget::widget(['icon' => 'clip']),
                    [
                        '/contractor/agreement-file-get',
                        'id' => $agreement->id,
                        'file-id' => $agreement->files[0]->id,
                    ],
                    [
                        'class' => 'file-link',
                        'target' => '_blank',
                        'download' => '',
                        'data-pjax' => 0,
                    ]
                );
            },
        ],
        [
            'label' => 'Ответственный',
            'attribute' => 'created_by',
            'value' => 'createdBy.shortFio',
            'filter' => ['' => 'Все'] + $filter->getListFactory()->createEmployeeList()->getItems(),
            's2width' => '200px',
            'enableSorting' => false,
            'hideSearch' => false,
        ],
        [
            'class' => ActionColumn::class,
            'template' => '{update}{delete}',
            'headerOptions' => ['style' => 'width: 1%',],
            'contentOptions' => ['class' => 'text-nowrap'],
            'visible' => $canEdit,
            'buttons' => [
                'update' => function (string $url, Agreement $agreement) use ($contractor): string {
                    return GridButtonWidget::widget([
                        'icon' => 'pencil',
                        'options' => [
                            'title' => 'Изменить',
                            'class' => 'button-clr link ajax-form-button mr-2',
                            'data-url' => Url::to([
                                'agreement/update',
                                'contractor_id' => $contractor->id,
                                'agreement_id' => $agreement->id,
                            ]),
                            'data-title' => 'Редактировать договор'
                        ],
                    ]);
                },
                'delete' => function (string $url, Agreement $agreement) use ($contractor): string {
                    return GridButtonWidget::widget([
                        'icon' => 'garbage',
                        'options' => [
                            'title' => 'Удалить',
                            'class' => 'button-clr link ajax-form-button',
                            'data-url' => Url::to([
                                'agreement/delete',
                                'contractor_id' => $contractor->id,
                                'agreement_id' => $agreement->id,
                            ]),
                            'data-title' => '<span class="text-center d-block">Вы действительно хотите удалить запись?</span>',
                        ],
                    ]);
                },
            ],
        ],
    ],
]) ?>
</div>
