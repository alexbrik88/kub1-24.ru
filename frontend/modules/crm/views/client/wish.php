<?php

namespace frontend\modules\crm\views;

use common\components\grid\GridView;
use common\models\Contractor;
use frontend\modules\crm\models\Wishes;
use frontend\modules\crm\models\WishesFactory;
use frontend\modules\crm\models\WishesFilter;
use frontend\modules\crm\widgets\GridButtonWidget;
use frontend\rbac\permissions\crm\ClientAccess;
use frontend\rbac\permissions\document\Document;
use frontend\themes\kub\widgets\SpriteIconWidget;
use Yii;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\i18n\Formatter;
use yii\web\View;

/**
 * @var View $this
 * @var WishesFilter $filter
 * @var Contractor $contractor
 * @var bool $isContractor
 */

$this->title = 'Пожелания клиента';
$count = $dataProvider->getTotalCount();
$emptyText = implode(' ', [
    'По заданным фильтрам пожеланий не найдено.',
    Html::a('Очистить', ['client/wishes', 'contractor_id' => $contractor->id, 'contractor' => $isContractor]),
]);
$canEdit = (Yii::$app->user->can(ClientAccess::EDIT, ['contractor' => $contractor]));

if ($count == 0) {
    $emptyText = 'Ещё не добавлено ни одного пожелания. ';

    if ($canEdit) {
        $emptyText .= Html::a('Добавить пожелание клиента', '#', [
            'class' => 'ajax-form-button',
            'data-url' => Url::to(['client-wish/create', 'contractor_id' => $contractor->id, 'contractor' => $isContractor]),
            'data-title' => 'Добавить пожелание клиента',
        ]);
    }
}

?>

<div class="page-head d-flex flex-wrap align-items-center mb-2">
    <h4><?= Html::encode($this->title); ?></h4>

    <?= GridButtonWidget::widget([
        'label' => 'Добавить',
        'icon' => 'add-icon',
        'options' => [
            'class' => 'button-regular button-regular_red pl-3 pr-3 ml-auto ajax-form-button',
            'data-url' => Url::to(['client-wish/create', 'contractor_id' => $contractor->id, 'contractor' => $isContractor]),
            'data-title' => 'Добавить пожелание клиента',
        ],
    ]) ?>
</div>

<div class="table-container products-table clearfix">
<?= GridView::widget([
    'id' => 'product-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'emptyText' => $emptyText,
    'formatter' => ['class' => Formatter::class, 'nullDisplay' => ''],
    'tableOptions' => [
        'class' => 'table table-style table-count-list',
        'id' => 'datatable_ajax',
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],
    'headerRowOptions' => [
        'class' => 'heading',
    ],
    'options' => [
        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
    ],
    'rowOptions' => [
        'role' => 'row',
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->getTotalCount()]),
    'columns' => [
        ['class' => 'yii\grid\CheckboxColumn'],

        [
            'attribute' => 'date',
            'format' => ['date', 'MM.dd.yyyy'],
        ],
        'number',
        'text:ntext',
        [
            'attribute' => 'status_id',
            'value' => 'statusName',
            'filter' => $searchModel->statusFilterItems(),
            's2width' => '140px',
        ],
        [
            'attribute' => 'responsible_employee_id',
            'value' => 'employeeCompany.shortFio',
            'filter' => $searchModel->responsibleFilterItems(),
            's2width' => '200px',
        ],

        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => '/crm/client-wish',
            'template' => '{update} {delete}',
            'urlCreator' => function ($action, $model, $key, $index, $column) {
                $params = [
                    'contractor_id' => $model->contractor_id,
                    'id' => (string) $key,
                ];
                $params[0] = $column->controller . '/' . $action;

                return Url::toRoute($params);
            },
            'buttons' => [
                'update' => function ($url, $model, $key) {
                    $options = [
                        'data-pjax' => '0',
                        'class' => 'ajax-form-button',
                        'title' => 'Редактировать',
                        'data-url' => $url,
                        'data-title' => 'Изменить пожелание клиента',
                    ];
                    $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-pencil"]);

                    return Html::a($icon, '#', $options);
                },
                'delete' => function ($url, $model, $key) {
                    return \frontend\themes\kub\widgets\ConfirmModalWidget::widget([
                        'toggleButton' => [
                            'tag' => 'span',
                            'label' => '',
                            'title' => 'Удалить',
                            'class' => 'glyphicon glyphicon-trash link',
                        ],
                        'confirmUrl' => $url,
                        'confirmParams' => [],
                        'message' => 'Вы уверены, что хотите удалить это пожелание?',
                    ]);
                },
            ],
        ],
    ],
]) ?>
</div>
