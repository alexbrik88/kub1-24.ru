<?php

namespace frontend\modules\crm\views;

use common\models\Contractor;
use frontend\modules\crm\models\Contact;
use yii\web\View;

/**
 * @var View $this
 * @var Contractor $contractor
 * @var Contact[]|null $contacts
 */

$this->title = 'Карточка';

?>

<?= $this->render('@frontend/views/contractor/view/tab_customer_info', [
    'company' => $company,
    'model' => $contractor,
    'contacts' => $contacts,
    'activeTab' => 'crm',
    'isLegal' => ($contractor->face_type == Contractor::TYPE_LEGAL_PERSON),
    'isPhysical' => ($contractor->face_type == Contractor::TYPE_PHYSICAL_PERSON),
    'isForeign' => ($contractor->face_type == Contractor::TYPE_FOREIGN_LEGAL_PERSON),
    'is_edit' => null,
]) ?>
