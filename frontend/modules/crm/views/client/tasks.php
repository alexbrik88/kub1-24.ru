<?php

namespace frontend\modules\crm\views;

use common\models\Contractor;
use frontend\components\WebUser;
use frontend\modules\crm\models\TaskFilter;
use frontend\modules\crm\models\TaskSelectForm;
use yii\web\View;

/**
 * @var View $this
 * @var WebUser $user
 * @var TaskSelectForm $form
 * @var Contractor $contractor
 * @var TaskFilter $filter
 * @var bool $isContractor
 */

$this->title = 'Список задач';

?>

<?= $this->render('../task/widgets/task-filter', [
    'filter' => $filter,
    'placeholder' => 'Поиск по описанию...',
]) ?>

<?= $this->render('../task/widgets/task-grid', [
    'user' => $user,
    'form' => $form,
    'filter' => $filter,
    'contractor' => $contractor,
    'isContractor' => $isContractor,
    'canDelete' => $canDelete,
]) ?>
