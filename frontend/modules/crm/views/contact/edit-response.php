<?php

namespace frontend\modules\crm\views;

use frontend\modules\crm\models\Contact;
use yii\web\View;

/**
 * @var View $this
 * @var Contact $contact
 */

$data = json_encode([
    'id' => $contact->contact_id,
    'label' => addslashes($contact->contact_name),
]);

?>

<?php $this->registerJs(<<<JS
    (function () {
        $('#add-item-widget').first().modal('hide');
        $(document).trigger('type:created', JSON.parse('{$data}'));
    })();
JS) ?>
