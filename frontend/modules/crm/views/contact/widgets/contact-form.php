<?php

namespace frontend\views\contractor;

use common\models\Contractor;
use frontend\modules\crm\models\ContactForm;
use frontend\modules\crm\models\ListFactory;
use frontend\modules\crm\models\MessengerTypeList;
use kartik\widgets\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\web\View;

/**
 * @var View $this
 * @var ContactForm $form
 * @var Contractor $contractor
 */

$tooltipTemplate = function (string $selector): string {
    $icon = Html::tag(
        'span',
        '<svg class="tooltip-question-icon svg-icon mx-1"><use xlink:href="/img/svg/svgSprite.svg#question"></use></svg>',
        [
            'class' => 'tooltip2',
            'style' => '',
            'data-tooltip-content' => $selector,
        ]
    );

    return "{label}\n{$icon}\n{input}\n{hint}\n{error}";
};

yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub', 'tooltipster-in-modal'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'contentCloning' => true,
    ],
]);

$this->title = $form->contact->isNewRecord
    ? 'Добавить контакт в ' . $contractor->getNameWithType()
    : 'Редактировать контакт в ' . $contractor->getNameWithType();

?>

<h4 class="modal-title"><?= Html::encode($this->title) ?></h4>

<div class="hidden">
    <div id="skypeTooltip">
        Укажите Skype клиента для связи
    </div>
    <div id="primaryMessengerTypeTooltip">
        Укажите мессенджер, в котором клиент предпочитает переписываться
    </div>
    <div id="secondaryMessengerTypeTooltip">
        Укажите мессенджер, в котором клиент предпочитает переписываться
    </div>
</div>

<?php $widget = ActiveForm::begin([
    'id' => 'contactForm',
    'fieldConfig' => [
        'labelOptions' => ['class' => 'label'],
        'options' => ['class' => 'form-group col-6'],
    ],
    'options' => [
        'data-pjax' => true,
    ],
]); ?>

<div class="row">

<?= $widget->field($form, 'contact_name')->textInput()->label('ФИО контакта') ?>

<?php if ($contractor->face_type == Contractor::TYPE_PHYSICAL_PERSON): ?>
    <div class="form-group col-3"></div>
<?php else: ?>
<?= $widget->field($form, 'client_position_id')->widget(Select2::class, [
    'data' => (new ListFactory)->createPositionList()->getItems(),
    'pluginOptions' => [
        'placeholder' => '',
        'width' => '100%',
    ],
]) ?>
<?php endif; ?>

<?= $widget->field($form, 'primary_phone_number', ['options' => ['class' => 'form-group col-3']])->textInput() ?>

<?= $widget->field(
    $form,
    'primary_messenger_type',
    ['options' => ['class' => 'form-group col-3'], 'template' => $tooltipTemplate('#primaryMessengerTypeTooltip')]
)->widget(Select2::class, [
    'data' => (new MessengerTypeList)->getItems(),
    'pluginOptions' => [
        'placeholder' => '',
        'width' => '100%',
    ],
]) ?>

<?= $widget->field($form, 'secondary_phone_number', ['options' => ['class' => 'form-group col-3']])->textInput() ?>

<?= $widget->field(
    $form,
    "secondary_messenger_type",
    ['options' => ['class' => 'form-group col-3'], 'template' => $tooltipTemplate('#secondaryMessengerTypeTooltip')]
)->widget(Select2::class, [
    'data' => (new MessengerTypeList)->getItems(),
    'pluginOptions' => [
        'placeholder' => '',
        'width' => '100%',
    ],
]) ?>

<?= $widget->field($form, 'email_address')->textInput() ?>

<?= $widget->field($form, 'skype_account', ['template' => $tooltipTemplate('#skypeTooltip')])->textInput() ?>

<?= $widget->field($form, 'social_account')->textInput() ?>

</div>

<?= $this->render('@frontend/modules/crm/views/widgets/dialog-buttons') ?>

<?php ActiveForm::end(); ?>
