<?php

use frontend\components\Icon;
use frontend\modules\crm\models\Task;
use frontend\modules\crm\models\TaskCommentForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\Tabs;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ListView;

/** @var $this yii\web\View */
/** @var $taskCommentForm frontend\modules\crm\models\TaskCommentForm */

$task = $taskCommentForm->task;
?>
<div id="task-comment-wrapper" class="d-flex flex-column h-100">
    <div class="flex-grow-1" style="position: relative;">
        <div style="position: absolute; top: 0; bottom: 5px; left: 0; right: 0; overflow: hidden;">
            <div style="height: 100%; overflow-y: auto;">
                <?= $this->render('_task-comment-list', [
                    'taskCommentForm' => $taskCommentForm,
                ]) ?>
            </div>
        </div>
    </div>
    <div class="d-flex">
        <?= Html::activeTextInput($taskCommentForm, 'text', [
            'class' => 'form-control flex-grow-1',
            'autocomplete' => 'off',
            'maxlength' => true,
            'rows' => 3,
            'placeholder' => 'введите сообщение',
            'data-url' => Url::to([
                '/crm/task/comment',
                'task_id' => $task->task_id,
            ]),
        ]) ?>
        <span id="task-comment-add" class="button-list button-hover-transparent">
            <?= Icon::get('check-2') ?>
        </span>
    </div>
</div>
