<?php

namespace frontend\modules\crm\views;

use common\models\Contractor;
use frontend\components\WebUser;
use frontend\modules\crm\assets\TaskFormAsset;
use frontend\modules\crm\models\ListFactory;
use frontend\modules\crm\models\LogFilter;
use frontend\modules\crm\models\Task;
use frontend\modules\crm\models\TaskEventList;
use frontend\modules\crm\models\TaskForm;
use frontend\themes\kub\helpers\Icon;
use frontend\themes\kub\widgets\SpriteIconWidget;
use frontend\widgets\ContractorDropdown;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/**
 * @var View $this
 * @var WebUser $user
 * @var Task $task
 * @var TaskForm $form
 * @var LogFilter|null $filter
 * @var bool $canDelete
 */

//TaskFormAsset::register($this);

$now = date('Y-m-d H:i:00');

$upIcon = SpriteIconWidget::widget(['icon' => 'arrow', 'options' => ['class' => 'svg-icon rotate-180']]);
$downIcon = SpriteIconWidget::widget(['icon' => 'arrow', 'options' => ['class' => 'svg-icon']]);
$dataUrl = Url::to(['clients/create', 'redirect' => 0]);
$delModalId = 'task_delete_modal_'.$task->task_id;
?>

<?php $widget = ActiveForm::begin([
    'id' => 'taskForm',
    'action' => $task->task_id
        ? ['task/update', 'task_id' => $task->task_id]
        : ['task/create', 'contractor_id' => $form->contractor_id],
    'fieldConfig' => [
        'labelOptions' => ['class' => 'label'],
        'options' => ['class' => 'form-group col-6'],
    ],
    'options' => [
        'data-pjax' => true,
    ],
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
]); ?>

<div class="row">
    <div class="col-6">
        <div class="row">
            <?= $widget->field($form, 'event_type')->widget(Select2::class, [
                'data' => (new TaskEventList)->getItems(),
                'hideSearch' => true,
                'pluginOptions' => [
                    'placeholder' => '',
                    'width' => '100%',
                ],
            ]) ?>

            <div class="col-6 d-flex">
                <?= $widget->field($form, 'date', ['options' => ['class' => 'form-group']])->textInput([
                    'id' => 'taskDate',
                    'class' => 'form-control date-picker ico js-task-date w-130',
                    'data-date-viewmode' => 'years',
                ]) ?>

                <?= $widget->field($form, 'time', ['options' => ['class' => 'form-group ml-2', 'style' => 'margin-left: 15px !important;']])->textInput([
                    'class' => 'form-control js-task-time timepicker bootstrap-timepicker input-group-addon',
                ]) ?>
            </div>

            <?php if ($form->contractor): ?>
                <?php
                $route = [
                    '/crm/client/tasks',
                    'contractor_id' => $form->contractor_id,
                ];
                $btnLabel = Html::a(Html::encode($form->contractor->getNameWithType()), $route, [
                    'class' => 'd-inline-block text-truncate w-100 client_new_tab_link',
                ]);
                ?>
                <div class="form-group col-6">
                    <label class="label">Клиент</label>
                    <?= Html::submitButton($btnLabel, [
                        'name' => 'redirect',
                        'title' => 'При клике переход в карточку клиента.<br>Данные в задаче сохранятся.',
                        'class' => 'button-regular button-hover-transparent d-inline-block w-100 text-left form-control',
                        'title-as-html' => 1,
                        'value' => Url::to($route),
                        'style' => [
                            'background-color' => '#e9ecef',
                            'border-color' => '#e2e5eb',
                            'padding-left' => '15px',
                        ],
                    ]) ?>
                </div>
                <?php if ($form->scenario == TaskForm::SCENARIO_CREATE) : ?>
                    <?= Html::activeHiddenInput($form, 'contractor_id') ?>
                <?php endif ?>
            <?php else: ?>
                <?= $widget->field($form, 'contractor_id')->widget(ContractorDropdown::class, [
                    'company' => $user->identity->company,
                    'staticData' => [
                        'add-client-button' => Html::tag('b', Icon::PLUS . ' Добавить клиента ', ['data-url' => $dataUrl]),
                    ],
                    'addRequestParams' => ['_type' => Contractor::TYPE_POTENTIAL_CLIENT],
                    'contractorType' => Contractor::TYPE_CUSTOMER,
                    'options' => [
                        'placeholder' => '',
                        'data-contacts-url' => Url::to(['clients/contacts']),
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                    ],
                ]) ?>
            <?php endif; ?>

            <?= $widget->field($form, 'employee_id')->widget(Select2::class, [
                'readonly' => !$form->canChangeEmployee,
                'disabled' => !$form->canChangeEmployee,
                'data' => (new ListFactory)->createEmployeeList()->getItems(),
                'pluginOptions' => ['width' => '100%'],
            ]) ?>
        </div>

        <div class="row">
            <?= $widget->field($form, 'contact_id')->widget(Select2::class, [
                'data' => $form->contactDropdownData()['data'],
                'options' => [
                    'placeholder' => '',
                    'onchange' => '$(".contact-phone-view", this.form).val($(":selected", this).data("phone"));'.
                                  '$(".contact-email-view", this.form).val($(":selected", this).data("email"));',
                    'options' => $form->contactDropdownData()['options'],
                ],
                'pluginOptions' => [
                    'width' => '100%',
                ],
            ])->label('ФИО контакта') ?>

            <div class="form-group col-6 field-taskform-employee_id">
                <div class="d-flex justify-content-between">
                    <label class="label toggle_contact_items_view cursor-pointer" data-hide=".contact-email-view" data-show=".contact-phone-view">Телефон</label>
                    <label class="label toggle_contact_items_view cursor-pointer" data-hide=".contact-phone-view" data-show=".contact-email-view">E-mail</label>
                </div>
                <?= Html::textInput(null, null, [
                    'class' => 'contact-phone-view form-control',
                    'style' => 'background-color: #fff;',
                    'readonly' => true,
                ]) ?>
                <?= Html::textInput(null, null, [
                    'class' => 'contact-email-view form-control hidden',
                    'style' => 'background-color: #fff;',
                    'readonly' => true,
                ]) ?>
            </div>
        </div>

        <div class="row">
            <?= $widget->field($form, 'description', ['options' => ['class' => 'form-group col-12']])->textarea([
                'rows' => 3,
                'style' => 'resize: vertical;',
                'oninput' => 'this.style.height = "";this.style.height = this.scrollHeight + "px";',
                'onfocus' => 'this.style.height = "";this.style.height = this.scrollHeight + "px";',
            ]) ?>

            <div class="col-12">
                <?= $this->render('_task-form-checklist', ['form' => $form]) ?>
            </div>

            <?= $widget->field($form, 'result', ['options' => ['class' => 'form-group col-12']])->textarea([
                'rows' => 3,
                'style' => 'resize: vertical;',
                'oninput' => 'this.style.height = "";this.style.height = this.scrollHeight + "px";',
                'onfocus' => 'this.style.height = "";this.style.height = this.scrollHeight + "px";',
            ])->label($this->render('_task-result-label', [
                'widget' => $widget,
                'form' => $form,
            ]), ['class' => 'label w-100']) ?>

            <?= $widget
                    ->field($form, 'completed', ['options' => ['class' => '']])
                    ->hiddenInput(['class' => 'js-completed'])
                    ->label(false)
            ?>
        </div>
    </div>
    <div class="col-6" style="margin-top: -18px;">
        <?= $this->render('_task-right-part', [
            'filter' => $filter,
            'taskCommentForm' => $taskCommentForm,
        ]) ?>
    </div>
</div>

<?= $this->render('_task-form-buttons', [
    'task' => $task,
    'form' => $form,
    'delModalId' => $delModalId,
    'canDelete' => $canDelete,
]) ?>

<?php ActiveForm::end(); ?>

<?php if ($canDelete) : ?>
    <?php Modal::begin([
        'id' => $delModalId,
        'title' => 'Вы уверены, что хотите удалить эту задачу?',
        'toggleButton' => false,
        'closeButton' => false,
    ]) ?>
        <?= Html::beginForm(['task/delete', 'task_id' => $task->task_id]) ?>
            <div class="text-center">
                <?= Html::submitButton('Да', [
                    'class' => 'button-regular button-hover-transparent button-width-medium ladda-button mr-2',
                ]) ?>
                <?= Html::button('Нет', [
                    'class' => 'button-regular button-hover-transparent button-width-medium',
                    'onclick' => "$('#{$delModalId}').modal('hide')",
                ]) ?>
            </div>
        <?= Html::endForm() ?>
    <?php Modal::end() ?>
<?php endif ?>

<?php $this->registerJs(<<<JS
    (function () {
        const STATUS_IN_PROGRESS = 1;
        const STATUS_EXPIRED = 2;
        const STATUS_COMPLETED = 3;
        const now = new Date('{$now}');

        /*$('.js-task-date, .js-task-time').on('change', function () {
            const form = $(this).closest('form').first();
            const input = form.find('.js-task-status');
            const date = form.find('.js-task-date').val();
            const time = form.find('.js-task-time').val();
            const status = input.val();

            if (date.length && time.length && status.length) {
                const dateParts = date.match(/^([0-9]{1,2})[.]([0-9]{1,2})[.]([0-9]{4})$/);
                const timeParts = time.match(/^([0-9]{1,2})[:]([0-9]{1,2})$/);

                if (dateParts && timeParts) {
                    let selected = new Date(dateParts[3] + '-' +  dateParts[2] + '-' + dateParts[1] + ' ' + timeParts[1] + ':' + timeParts[2] + ':00');

                    if (parseInt(status) !== STATUS_COMPLETED) {
                        const select2 = input.closest('.form-group').first().find('.select2-container');

                        if (now.getTime() > selected.getTime()) {
                            select2.addClass('select2-value-red');
                            input.val(STATUS_EXPIRED);
                        } else {
                            select2.removeClass('select2-value-red');
                            input.val(STATUS_IN_PROGRESS);
                        }

                        input.trigger('change');
                    }
                }
            }

            form.data('form-initialised', true);
        });*/

        $('.js-complete-button').on('click', function () {
            const form = $(this).closest('form').first();
            const value = $(this).data('completed-value');

            form.find('.js-completed').val(value);
        });
        $('label.toggle_contact_items_view').on('click', function () {
            var form = $(this).closest('form');
            $($(this).data('hide'), form).toggleClass('hidden', true);
            $($(this).data('show'), form).toggleClass('hidden', false);
        });
        $('#taskform-contractor_id').on('change', function () { cursor-pointer
            let val = this.value;
            if (val) {
                let form = this.form;
                let url = $(this).data('contacts-url');
                $.ajax({
                    url: url,
                    data: {contractor_id: val},
                    success: function (data) {
                        console.log(data);

                        $('#taskform-contact_id', form).empty();
                        data.forEach(function (item) {
                            let option = new Option(item.name, item.id);
                            $(option).data('phone', item.phone || '---');
                            $(option).data('email', item.email || '---');
                            $('#taskform-contact_id', form).append($(option));
                        });
                        $('#taskform-contact_id', form).trigger('change');
                    }
                });
            }
        });
        $('#taskform-contact_id').trigger('change');
    })();
JS); ?>

<?php $this->registerJs(<<<JS
    (function () {
        $('.timepicker').timepicker({
            showMeridian: false,
            minuteStep: 15,
            showInputs: false,
            orientation: {
                x: 'right',
                y: 'top'
            },
            icons: {
                up: 'timepicker-button-up link',
                down: 'timepicker-button-down link'
            }
        }).on('show.timepicker', function () {
            $('.bootstrap-timepicker-widget .timepicker-button-up').html('{$upIcon}');
            $('.bootstrap-timepicker-widget .timepicker-button-down').html('{$downIcon}');
            // Всё наоборот
            $('.bootstrap-timepicker-widget a[data-action="incrementHour"]').data('action', 'decrementHour');
            $('.bootstrap-timepicker-widget a[data-action="incrementMinute"]').data('action', 'decrementMinute');
            $('.bootstrap-timepicker-widget a[data-action="decrementHour"]').data('action', 'incrementHour');
            $('.bootstrap-timepicker-widget a[data-action="decrementMinute"]').data('action', 'incrementMinute');
        }).closest('form').first().on('submit', function() {
            $('.timepicker').timepicker('hideWidget');
        });
    })();
JS); ?>

<?php $this->assetBundles = []; ?>
