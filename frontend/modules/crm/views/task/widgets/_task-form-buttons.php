<?php

use frontend\modules\crm\models\Task;
use frontend\modules\crm\models\TaskForm;
use frontend\themes\kub\helpers\Icon;
use yii\bootstrap4\Html;
use yii\web\View;

/**
 * @var View $this
 * @var Task $task
 * @var TaskForm $form
 * @var bool $canDelete
 */

?>

<div class="d-flex">
    <?= Html::submitButton('Сохранить', [
        'form' => 'taskForm',
        'class' => 'button-regular button-width button-regular_red button-clr js-complete-button',
        'data-style' => 'expand-right',
        'data-completed-value' => TaskForm::COMPLETED_NO_CHANGE,
    ]) ?>

    <?php if ($form->scenario == TaskForm::SCENARIO_UPDATE && $task->status == Task::STATUS_COMPLETED): ?>
        <?= Html::submitButton('Сохранить + Вернуть в работу', [
            'form' => 'taskForm',
            'class' => 'button-clr button-regular button-hover-transparent js-complete-button ml-2',
            'data-style' => 'expand-right',
            'data-completed-value' => TaskForm::COMPLETED_RESET,
        ]) ?>
    <?php endif; ?>

    <?php if ($task->status != Task::STATUS_COMPLETED): ?>
        <?= Html::submitButton('Завершить', [
            'form' => 'taskForm',
            'class' => 'button-clr button-width button-regular button-hover-transparent js-complete-button ml-2',
            'data-style' => 'expand-right',
            'data-completed-value' => TaskForm::COMPLETED_CHANGE,
        ]) ?>

        <?= Html::submitButton('Завершить + Добавить новую', [
            'form' => 'taskForm',
            'class' => 'button-clr button-regular button-hover-transparent js-complete-button ml-2',
            'data-style' => 'expand-right',
            'data-completed-value' => TaskForm::COMPLETED_CHANGE_ADDITIONAL,
        ]) ?>
    <?php endif; ?>

    <div class="ml-auto">
        <?php if ($canDelete) : ?>
            <?= Html::button('Удалить', [
                'class' => 'button-clr button-width button-regular button-hover-transparent ml-2',
                'data-toggle' => 'modal',
                'data-target' => '#'.$delModalId,
            ]) ?>
        <?php endif ?>
    </div>

    <?= Html::button('Отменить', [
        'class' => 'button-clr button-width button-regular button-hover-transparent ml-2',
        'data-dismiss' => 'modal'
    ]) ?>
</div>
