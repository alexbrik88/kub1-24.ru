<?php

namespace frontend\modules\crm\views;

use frontend\components\Icon;
use frontend\modules\crm\models\TaskFilter;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View $this
 * @var TaskFilter $filter
 * @var string $placeholder
 */

$viewToggle = (bool) \Yii::$app->user->identity->config->crm_task_index_view;
?>

<?php $widget = ActiveForm::begin([
    'id' => 'searchFilterForm',
    'method' => 'GET',
    'action' => Url::current([$filter->formName() => null]),
    'options' => [
        'data-pjax' => true,
    ],
    'fieldConfig' => [
        'labelOptions' => ['class' => 'label'],
        'options' => ['class' => 'form-group col-6 mb-3'],
    ],
]) ?>

<div class="table-settings row row_indents_s">
    <div class="col-6">
        <div class="row align-items-center">
            <div class="column flex-grow-1 d-flex flex-wrap">
                <?= TableConfigWidget::widget([
                    'items' => [
                        ['attribute' => 'crm_task_number', 'label' => 'Номер задачи'],
                        ['attribute' => 'crm_task_time', 'label' => 'Время'],
                        ['attribute' => 'crm_task_contractor', 'label' => 'Клиент'],
                        ['attribute' => 'crm_task_contact', 'label' => 'ФИО контакт'],
                        ['attribute' => 'crm_task_description', 'label' => 'Описание задачи'],
                        ['attribute' => 'crm_task_result', 'label' => 'Результат по задаче'],
                    ],
                    'hideItems' => [
                        ['attribute' => 'crm_task_hide_completed', 'label' => 'Завершенные', 'refresh-page' => true],
                    ],
                    'buttonClass' => 'button-regular button-regular_red button-clr w-44 mr-2 table_conf_btn',
                ]) ?>

                <?= TableViewWidget::widget([
                    'attribute' => 'crm_task',
                    'buttonClass' => 'button-regular button-regular_red button-clr w-44 mr-2 table_conf_btn'
                ]) ?>

                <?= \frontend\widgets\ToggleViewWidget::widget([
                    'isActive' => $viewToggle,
                    'options' => [
                        'tag' => 'span',
                        'class' => 'button-regular button-regular_red button-clr w-44 mr-auto crm_task_index_view_toggle',
                        'label' => Icon::get('grid', [
                            'class' => 'task-index-grid'.($viewToggle ? ' hidden' : ''),
                        ]).Icon::get('3cols', [
                            'class' => 'current-tasks-dashboard'.($viewToggle ? '' : ' hidden'),
                        ]),
                        'data-url' => Url::to(['/site/config']),
                    ],
                    'disableTarget' => '.table_conf_btn',
                    'hideTarget' => '.task-index-grid',
                    'showTarget' => '.current-tasks-dashboard',
                ]) ?>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="d-flex flex-nowrap align-items-center task-index-grid <?= $viewToggle ? 'hidden' : '' ?>">
            <?= $widget->field($filter, 'search', [
                'options' => ['class' => 'form-group flex-grow-1 mr-2'],
            ])->textInput([
                'placeholder' => $placeholder,
                'type' => 'search',
            ])->label(false) ?>

            <div class="form-group">
                <?= Html::submitButton('Найти', [
                    'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                ]) ?>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end() ?>

<?php $this->registerJs(<<<JS
    $(document).on('toggleView.change', '.crm_task_index_view_toggle', function () {
        $.post($(this).data('url'), {'Config[crm_task_index_view]': $(this).hasClass('active') ? '1' : '0'});
    });
JS) ?>
