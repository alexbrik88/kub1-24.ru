<?php

namespace frontend\modules\crm\views;

use common\models\Contractor;
use frontend\components\WebUser;
use frontend\modules\crm\assets\TaskFormAsset;
use frontend\modules\crm\models\ListFactory;
use frontend\modules\crm\models\LogFilter;
use frontend\modules\crm\models\Task;
use frontend\modules\crm\models\TaskEventList;
use frontend\modules\crm\models\TaskForm;
use frontend\themes\kub\helpers\Icon;
use frontend\themes\kub\widgets\SpriteIconWidget;
use frontend\widgets\ContractorDropdown;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/**
 * @var View $this
 * @var WebUser $user
 * @var Task $task
 * @var TaskForm $form
 * @var LogFilter|null $filter
 * @var bool $canDelete
 */

TaskFormAsset::register($this);

$this->title = ($form->scenario == TaskForm::SCENARIO_UPDATE)
    ? 'Редактировать задачу № ' . $task->task_number
    : 'Добавить задачу № ' . Task::getNextNumber($user->identity->company);

$title = Html::tag('div', $this->title);
$status = $task->statusName ? Html::textInput(null, $task->statusName, [
    'class' => 'form-control text-center ml-auto w-130'.($task->status == Task::STATUS_EXPIRED ? ' text-danger' : ''),
    'readonly' => true,
]) : '';
$modalTitle = Html::tag('div', $title . $status, [
    'class' => 'd-flex align-items-center justify-content-between',
]);
?>

<?= $this->render('_task-form', [
    'user' => $user,
    'task' => $task,
    'form' => $form,
    'filter' => $filter,
    'taskCommentForm' => $taskCommentForm,
    'canDelete' => $canDelete,
]) ?>

<script type="text/javascript">
    $('#ajaxFormDialog #ajaxFormDialog-label.modal-title').html('<?= $modalTitle ?>');
    $('#ajaxFormDialog > .modal-dialog').toggleClass('modal-xl', true);
</script>

<?php $this->assetBundles = []; ?>
