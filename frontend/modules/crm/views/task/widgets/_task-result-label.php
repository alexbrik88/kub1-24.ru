<?php

use frontend\components\Icon;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @var View $this
 * @var ActiveForm $widget
 * @var TaskForm $form
 */

$data = ArrayHelper::map($form->getResultItems(), 'task_result_id', 'name');
$items = [];
foreach ($data as $key => $value) {
    $items[] = [
        'label' => $value,
        'url' => 'javascript:void(0);',
        'active' => $key == $form->task_result_id,
        'linkOptions' => [
            'data-id' => $key,
            'data-value' => $value,
            'onclick' => '$(this).closest("form").find("#taskform-result").val($(this).data("value"));'.
                '$(this).closest("form").find("#taskform-task_result_id").val($(this).data("id"));',
        ],
    ];
}
?>

<?= Html::activeHiddenInput($form, 'task_result_id') ?>

<?php if ($data) : ?>
    <div class="d-flex justify-content-between">
        <div>
            <?= $form->getAttributeLabel('result') ?>
        </div>
        <div class="dropdown">
            <?= Html::tag('div', 'Варианты '.Icon::get('shevron', ['class' => 'ml-1']), [
                'data-toggle' => 'dropdown',
            ]) ?>
            <?= Dropdown::widget([
               'options' => [
                    'class' => 'dropdown-menu-right',
                ],
               'items' => $items,
            ]); ?>
        </div>
    </div>
<?php else : ?>
    <?= $form->getAttributeLabel('result') ?>
<?php endif ?>