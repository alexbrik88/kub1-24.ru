<?php

use frontend\modules\crm\models\TaskComment;
use frontend\modules\crm\models\TaskCommentForm;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var TaskComment $model
 * @var TaskCommentForm $taskCommentForm
 * @var bool $isFirst
 */

?>

<div class="mt-2 p-2 border rounded <?= $isFirst ? 'mb-auto' : '' ?>">
    <div class="text-muted">
        <?= date_create($model->created_at)->format('d.m.Y H:i') ?>
        <?php if ($model->employeeCompany) : ?>
            <?= Html::encode($model->employeeCompany->getShortFio()) ?>
        <?php else : ?>
            ---
        <?php endif ?>
    </div>
    <div>
        <?= Html::encode($model->text) ?>
    </div>
</div>