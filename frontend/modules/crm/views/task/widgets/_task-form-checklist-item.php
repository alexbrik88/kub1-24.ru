<?php

use frontend\components\Icon;
use yii\helpers\Html;

/**
 * @var View $this
 * @var TaskForm $form
 * @var int $key
 * @var bool $isTemplate
 */

$checkOtions = [
    'id' => null,
    'label' => false,
    'class' => 'task-checklist-item-check',
    'onchange' => 'TaskChecklist.check(this)',
];
$textOtions = [
    'id' => null,
    'class' => 'form-control task-checklist-item-text',
    'onchange' => 'TaskChecklist.validate(this)',
];
if ($isTemplate) {
    $checkOtions['disabled'] = true;
    $checkOtions['checked'] = false;
    $textOtions['disabled'] = true;
    $textOtions['value'] = '';
} else {
    $textOtions['readonly'] = (bool) ($form->checklist[$key]['checked'] ?? 0);
}
?>

<tr class="<?= $isTemplate ? 'task-checklist-template' : 'task-checklist-item'; ?>">
    <td style="width: 50px !important;">
        <span class="d-flex" style="width: 40px;">
            <?= Html::activeCheckbox($form, "checklist[{$key}][checked]", $checkOtions) ?>
            <span class="task-checklist-item-num ml-auto">
                <?= $key + 1 ?>
            </span>
        </span>
    </td>
    <td>
        <?= Html::activeTextInput($form, "checklist[{$key}][text]", $textOtions) ?>
    </td>
    <td style="width: 50px !important;">
        <span class="d-flex" style="width: 40px;">
            <?= Icon::get('menu-small', ['class' => 'sortable-row-icon mr-auto']) ?>
            <?= Icon::get('circle-close', [
                'class' => 'table-count-icon task-checklist-items-remove',
                'onclick' => 'TaskChecklist.remove(this)',
            ]) ?>
        </span>
    </td>
</tr>