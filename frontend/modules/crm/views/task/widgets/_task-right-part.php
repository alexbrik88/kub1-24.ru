<?php

use frontend\modules\crm\models\LogFilter;
use frontend\modules\crm\models\TaskCommentForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\Tabs;
use yii\web\View;

/**
 * @var View $this
 * @var LogFilter|null $filter
 * @var TaskCommentForm $taskCommentForm
 */

?>

<div class="d-flex flex-column h-100" style="max-height: 100%; overflow: hidden;">
    <?= Tabs::widget([
        'options' => [
            'class' => 'av-tabs_border_transparent nav-tabs_indents_alternative w-100 justify-content-end',
        ],
        'tabContentOptions' => [
            'class' => 'flex-grow-1 d-flex flex-column',
        ],
        'itemOptions' => [
            'class' => 'flex-grow-1 form-group',
        ],
        'items' => [
            [
                'label' => 'Комментарии',
                'content' => $this->render('_task-comment', ['taskCommentForm' => $taskCommentForm]),
            ],
            [
                'label' => 'История',
                'content' => $this->render('_task-history', ['filter' => $filter]),
                'visible' => $filter !== null,
            ],
        ],
    ]); ?>
</div>