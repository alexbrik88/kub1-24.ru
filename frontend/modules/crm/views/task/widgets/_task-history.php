<?php

use frontend\modules\crm\models\LogFilter;
use yii\web\View;

/**
 * @var View $this
 * @var LogFilter|null $filter
 */

$items = $filter ? $filter->getListFactory()->createMessageList()->getItems() : [];
?>

<?php foreach ($items as $key => $item) : ?>
    <div class="mt-2 p-2 border rounded">
        <?= $item ?>
    </div>
<?php endforeach ?>