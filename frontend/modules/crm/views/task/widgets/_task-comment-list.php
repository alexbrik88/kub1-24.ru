<?php

use frontend\modules\crm\models\TaskCommentForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

/** @var $this yii\web\View */
/** @var $taskCommentForm frontend\modules\crm\models\TaskCommentForm */

$dataProvider = $taskCommentForm->search($last_id ?? null);
$task = $taskCommentForm->task;
$pageSize = TaskCommentForm::PAGE_SIZE;
$currentPageSize = $dataProvider->pagination->pageSize;
$totalCount = $dataProvider->totalCount;
?>

<?= ListView::widget([
    'id' => 'task-comment-list',
    'dataProvider' => $dataProvider,
    'itemView' => '_task-comment-item',
    'options' => [
        'class' => 'list-view d-flex flex-column-reverse justify-content-end',
        'style' => 'line-height: 20px;',
    ],
    'viewParams' => [
        'taskCommentForm' => $taskCommentForm,
        'isFirst' => $isFirst ?? false,
    ],
    'layout' => '{items}',
    'emptyText' => false,
    'afterItem' => function ($model, $key, $index, $widget) use ($task, $pageSize, $currentPageSize, $totalCount) {
        if ($totalCount > $currentPageSize && $currentPageSize == ($index + 1)) {
            $left = min($pageSize, $totalCount - $currentPageSize);
            $btn = Html::tag('span', sprintf("Еще %s комментариев", $left), [
                'class' => 'text-muted',
                'style' => 'border-bottom: 1px dashed #6c757d;',
            ]);

            return Html::tag('div', $btn, [
                'class' => 'show_more_task_comments',
                'style' => 'cursor: pointer;',
                'data-url' => Url::to(['task/more-comments', 'task_id' => $task->task_id, 'last_id' => $model->id]),
            ]);
        }
    },
]) ?>
