<?php

namespace frontend\modules\crm\views;

use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\models\Contractor;
use frontend\components\WebUser;
use frontend\modules\crm\models\Task;
use frontend\modules\crm\models\TaskFilter;
use frontend\modules\crm\models\TaskSelectForm;
use frontend\modules\crm\widgets\DataColumnWidget;
use frontend\modules\crm\widgets\GridButtonWidget;
use frontend\rbac\permissions\crm\TaskAccess;
use yii\bootstrap4\Html;
use yii\grid\ActionColumn;
use yii\helpers\Url;
use yii\i18n\Formatter;
use yii\web\View;
use yii\widgets\ContentDecorator;

/**
 * @var View $this
 * @var WebUser $user
 * @var TaskFilter $filter
 * @var TaskSelectForm $form
 * @var Contractor $contractor
 * @var bool $isContractor
 */

$dataProvider = $filter->getDataProvider();
$canEditAll = $user->can(TaskAccess::EDIT_ALL);
$canViewAll = $user->can(TaskAccess::VIEW_ALL);
$emptyText = 'По заданным фильтрам задач не найдено. ' . Html::a(
    'Очистить',
    $contractor
        ? ['client/tasks', 'contractor_id' => $contractor->id, 'contractor' => $isContractor]
        : ['task/index', 'contractor' => $isContractor]
);

$count = $filter->getTotalCount();

if ($count == 0) {
    $emptyText = 'Вы ещё не добавили ни одной задачи. ';

    $emptyText .= Html::a('Добавить задачу', '#', [
        'class' => 'ajax-form-button',
        'data-url' => Url::to($contractor ? ['task/create', 'contractor_id' => $contractor->id] : ['task/create']),
        'data-title' => 'Добавить задачу № ' . Task::getNextNumber($user->identity->company),
    ]);
}

$contentOptions = function (Task $task) use ($canViewAll, $user): array {
    return ($task->status == Task::STATUS_EXPIRED && ($canViewAll || $task->employee_id == $user->id))
        ? ['class' => 'nowrap red-column']
        : ['class' => 'nowrap'];
};

?>

<?php ContentDecorator::begin([
    'viewFile' => __DIR__ . '/../decorators/task-select.php',
    'params' => [
        'form' => $form,
        'canDelete' => $canDelete,
    ],
]); ?>

<div class="table-container products-table clearfix">

<?= GridView::widget([
    'id' => 'product-grid',
    'emptyText' => $emptyText,
    'filterModel' => $filter,
    'dataProvider' => $dataProvider,
    'dataColumnClass' => DataColumnWidget::class,
    'configAttribute' => 'crm_task',
    'formatter' => ['class' => Formatter::class, 'nullDisplay' => ''],
    'tableOptions' => [
        'class' => 'table table-style table-count-list',
        'id' => 'datatable_ajax',
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->getTotalCount()]),
    'columns' => [
        [
            'class' => 'yii\grid\CheckboxColumn',
            'name' => 'TaskSelectForm[task_id]',
            'headerOptions' => [
                'style' => 'width: 20px;',
            ],
            'checkboxOptions' => function (Task $task) use ($user, $canEditAll) {
                $canEdit = $canEditAll || $user->can(TaskAccess::EDIT, ['task' => $task]);
                return [
                    'value' => $task->task_id,
                    'disabled' => !$canEdit,
                ];
            },
        ],
        [
            'label' => 'Номер задачи',
            'attribute' => 'task_number',
            'configAttribute' => 'crm_task_number',
            'headerOptions' => [
                'style' => 'width: 1%;',
            ],
        ],
        [
            'label' => 'Событие',
            'attribute' => 'event_type',
            'format' => 'raw',
            'filter' => ['' => 'Все'] + $filter->getListFactory()->createTaskEventList()->getItems(),
            's2width' => '200px',
            'enableSorting' => false,
            'value' => function (Task $task) use ($user, $canEditAll): string {
                $canEdit = $canEditAll || $user->can(TaskAccess::EDIT, ['task' => $task]);
                if ($canEdit) {
                    return Html::a(Html::encode($task->eventName), '#', [
                        'class' => 'ajax-form-button text-decoration-none',
                        'data-url' => Url::to(['task/update', 'task_id' => $task->task_id]),
                        'data-title' => 'Редактировать задачу № ' . $task->task_number,
                    ]);
                }

                return Html::tag('span', $task->eventName);
            }
        ],
        [
            'label' => 'Дата',
            'attribute' => 'date',
            'value' => function ($model) {
                return ($d = date_create($model->datetime)) !== false ? $d->format('d.m.Y') : '';
            },
            'contentOptions' => $contentOptions,
            'headerOptions' => [
                'style' => 'width: 1%;',
            ],
        ],
        [
            'label' => 'Время',
            'attribute' => 'time',
            'value' => function ($model) {
                return ($d = date_create($model->datetime)) !== false ? $d->format('H:i') : '';
            },
            'configAttribute' => 'crm_task_time',
            'contentOptions' => $contentOptions,
            'headerOptions' => [
                'style' => 'width: 1%;',
            ],
        ],
        [
            'label' => 'Клиент',
            'attribute' => 'contractor_id',
            'configAttribute' => 'crm_task_contractor',
            'format' => 'raw',
            's2width' => '200px',
            'enableSorting' => false,
            'hideSearch' => false,
            'visible' => ($contractor === null),
            'filter' => ['' => 'Все'] + $filter->getListFactory()->createClientList()->getItems(),
            'value' => function (Task $task): string {
                return Html::a(
                    Html::encode($task->contractor->nameWithType),
                    ['client/index', 'contractor_id' => $task->contractor_id],
                    ['data-pjax' => 0],
                );
            },
        ],
        [
            'label' => 'ФИО контакт',
            'attribute' => 'contact_id',
            'configAttribute' => 'crm_task_contact',
            'filter' => ['' => 'Все'] + $filter->getListFactory()->createContactList(clone $dataProvider->query)->getItems(),
            's2width' => '200px',
            'value' => 'contactData.name',
        ],
        [
            'label' => 'Описание задачи',
            'attribute' => 'description',
            'configAttribute' => 'crm_task_description',
            'contentOptions' => function ($model, $key, $index, $column) {
                return [
                    'class' => 'text-truncate',
                    'title' => $model->description,
                    'style' => 'max-width: 200px;',
                ];
            }
        ],
        [
            'label' => 'Результат по задаче',
            'attribute' => 'result',
            'configAttribute' => 'crm_task_result',
            'contentOptions' => function ($model, $key, $index, $column) {
                return [
                    'class' => 'text-truncate',
                    'title' => $model->result,
                    'style' => 'max-width: 150px;',
                ];
            }
        ],
        [
            'label' => 'Статус задачи',
            'attribute' => 'status',
            'headerOptions' => [
                'style' => 'width: 1px;'
            ],
            'contentOptions' => $contentOptions,
            'filter' => ['' => 'Все'] + $filter->getListFactory()->createStatusList()->getItems(),
            'value' => 'statusName',
            's2width' => '200px',
            'enableSorting' => false,
        ],
        [
            'label' => 'Ответственный',
            'attribute' => 'employee_id',
            'value' => 'employeeCompany.shortFio',
            'filter' => ['' => 'Все'] + $filter->getListFactory()->createEmployeeList()->getItems(),
            's2width' => '200px',
            'enableSorting' => false,
            'hideSearch' => false,
        ],
    ],
]) ?>

</div>

<?php ContentDecorator::end() ?>
