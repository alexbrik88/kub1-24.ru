<?php

use frontend\modules\crm\models\Task;
use frontend\modules\crm\models\TaskForm;
use frontend\themes\kub\helpers\Icon;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\web\View;

/**
 * @var View $this
 * @var TaskForm $form
 */

$totalCount = count($form->checklist);
$doneCount = array_sum(ArrayHelper::getColumn($form->checklist, 'checked'));
?>
<style type="text/css">
    .task-checklist-header .task-checklist-label .svg-icon {
        font-size: 12px;
    }
    .task-checklist-header:not(.collapsed) .task-checklist-label .svg-icon {
        transform: rotate(0.5turn);
    }
</style>
<div class="task-checklist-wrapper form-group">
    <?= Html::beginTag('div', [
        'class' => 'task-checklist-header d-flex collapsed',
        'data-toggle' => 'collapse',
        'data-target' => '#task-checklist-collapse',
        'aria-expanded' => false,
        'aria-controls' => 'task-checklist-collapse',
        'role' => 'button',
    ]) ?>
        <div class="task-checklist-label link">
            <span class="mr-1">
                Чек-лист
            </span>
            <?= Icon::get('shevron') ?>
        </div>
        <div class="task-checklist-summary ml-4 mr-auto text-nowrap <?= $totalCount == 0 ? 'hidden' : ''; ?>">
            Выполнено
            <span class="task-checklist-summary-done"><?= $doneCount ?></span>
            из
            <span class="task-checklist-summary-total"><?= $totalCount ?></span>
        </div>
    <?= Html::endTag('div') ?>
    <div id="task-checklist-collapse" class="collapse">
        <table class="task-checklist-table table table-style table-count mt-2 mb-0">
            <thead class="hidden" style="display: none;">
                <?= $this->render('_task-form-checklist-item', [
                    'form' => $form,
                    'key' => 0,
                    'isTemplate' => true,
                ]) ?>
            </thead>
            <tbody class="task-checklist-items">
                <?php if ($totalCount > 0) : ?>
                    <?php foreach ($form->checklist as $key => $item) : ?>
                        <?= $this->render('_task-form-checklist-item', [
                            'form' => $form,
                            'key' => $key,
                            'isTemplate' => false,
                        ]) ?>
                    <?php endforeach ?>
                <?php else : ?>
                    <?= $this->render('_task-form-checklist-item', [
                        'form' => $form,
                        'key' => 0,
                        'isTemplate' => false,
                    ]) ?>
                <?php endif ?>
            </tbody>
        </table>
        <div class="mt-3">
            <?= Html::tag('span', Icon::get('add-icon').Html::tag('span', 'Добавить', ['class' => 'ml-2']), [
                'class' => 'task-checklist-items-add button-regular button-hover-content-red',
                'onclick' => 'TaskChecklist.add(this)',
            ]) ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.TaskChecklist = {
        filter: function (container) {
            $('tr.task-checklist-item', container).each(function (i, el) {
                if ($('input.task-checklist-item-text', this).val().trim() == '') {
                    $(this).remove();
                }
            });
        },
        indexing: function (container) {
            $('tr.task-checklist-item', container).each(function (key, el) {
                $('.task-checklist-item-num', el).text(key+1);
                $('input', el).each(function () {
                    this.name = this.name.replace(/(\w+\[checklist\]\[)(\d+)(\]\[\w+\])/, '$1'+key+'$3');
                });
            });
        },
        add: function (btn) {
            let table = $(btn).closest('.task-checklist-wrapper').find('table.task-checklist-table');
            let items = $('tbody.task-checklist-items', table);
            TaskChecklist.filter(items);
            let item = $('tr.task-checklist-template', table).clone();
            item.toggleClass('task-checklist-template', false).toggleClass('task-checklist-item', true);
            $('input', item).prop('disabled', false);
            $('input[type=checkbox]', item).each(function () {
                let checker = $(this).closest('.checker');
                if (checker.length) {
                    checker.replaceWith(this);
                }
            });
            items.append(item);
            $('input[type=checkbox]', item).uniform('refresh');
            TaskChecklist.indexing(items);
        },
        validate: function (input) {
            let row = $(input).closest('tr');
            let check = $('.task-checklist-item-check', row);
            let text = $('.task-checklist-item-text', row);
            if (text.val().trim() == '') {
                check.prop('checked', false).uniform('refresh');
            }
            TaskChecklist.summary(row);
        },
        check: function (input) {
            let row = $(input).closest('tr');
            let check = $('.task-checklist-item-check', row);
            let text = $('.task-checklist-item-text', row);
            if (check.is(':checked')) {
                if (text.val().trim() == '') {
                    check.prop('checked', false).uniform('refresh');
                } else {
                    text.prop('readonly', true);
                }
            } else {
                text.prop('readonly', false);
            }
            TaskChecklist.summary(row);
        },
        remove: function (btn) {
            let item = $(btn).closest('tr.task-checklist-item');
            let items = item.closest('tbody.task-checklist-items');
            item.remove();
            TaskChecklist.indexing(items);
            TaskChecklist.summary(items);
        },
        summary: function (el) {
            let container = $(el).closest('.task-checklist-wrapper');
            let items = $('tbody.task-checklist-items', container);
            let summary = $('.task-checklist-summary', container);
            let total = 0;
            let done = 0;
            $('tr.task-checklist-item', items).each(function (i, item) {
                console.log($('input.task-checklist-item-text', item).val().trim());
                if ($('input.task-checklist-item-text', item).val().trim() != '') {
                    total++;
                    if ($('input.task-checklist-item-check', item).is(':checked')) {
                        done++;
                    }
                }
            });
            summary.toggleClass('hidden', total == 0);
            $('.task-checklist-summary-done', summary).text(done);
            $('.task-checklist-summary-total', summary).text(total);
        },
    };
    $(function () {
        $(".task-checklist-table").sortable({
            items: '.task-checklist-items > tr',
            cursor: 'pointer',
            axis: 'y',
            dropOnEmpty: false,
            start: function (e, ui) {
                ui.item.addClass("selected");
            },
            stop: function (e, ui) {
                ui.item.removeClass("selected");
                let items = ui.item.closest('tbody.task-checklist-items');
                TaskChecklist.indexing(items);
            }
        });
    });
</script>