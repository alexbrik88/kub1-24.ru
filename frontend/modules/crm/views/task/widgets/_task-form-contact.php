<?php

use common\models\Contractor;
use frontend\components\WebUser;
use frontend\modules\crm\assets\TaskFormAsset;
use frontend\modules\crm\models\ListFactory;
use frontend\modules\crm\models\LogFilter;
use frontend\modules\crm\models\Task;
use frontend\modules\crm\models\TaskEventList;
use frontend\modules\crm\models\TaskForm;
use frontend\themes\kub\helpers\Icon;
use frontend\themes\kub\widgets\SpriteIconWidget;
use frontend\widgets\ContractorDropdown;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/**
 * @var View $this
 * @var TaskForm $form
 * @var ActiveForm $widget
 */

$data = $form->contactDropdownData();
$phone = $data['options'][$form->contact_id]['data-phone'] ?? null;
?>

<div class="row">
    <?= $widget->field($form, 'contact_id', [
        'labelOptions' => ['class' => 'label'],
        'options' => ['class' => 'form-group col-6'],
    ])->widget(Select2::class, [
        'data' => $data['data'],
        'options' => [
            'placeholder' => '',
            'onchange' => '$(".contact-phone-view", this.form).val($(":selected", this).data("phone"))',
            'options' => $data['options'],
        ],
        'pluginOptions' => [
            'width' => '100%',
        ],
    ])->label('ФИО контакта') ?>

    <div class="form-group col-6 field-taskform-employee_id">
        <label class="label">Телефон</label>
        <?= Html::textInput(null, $phone, [
            'class' => 'contact-phone-view form-control',
            'style' => 'background-color: #fff;',
            'readonly' => true,
        ]) ?>
    </div>
</div>
