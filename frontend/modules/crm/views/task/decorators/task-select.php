<?php

namespace frontend\modules\crm\views;

use frontend\components\Icon;
use frontend\modules\crm\assets\RowSelectAsset;
use frontend\modules\crm\models\ListFactory;
use frontend\modules\crm\models\Task;
use frontend\modules\crm\models\TaskSelectForm;
use kartik\select2\Select2;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var string $content
 * @var TaskSelectForm $form
 */

RowSelectAsset::register($this);

$text = 'Для выбранных задач изменить на:';
$dialogs = [
    [
        'id' => 'changeStatusDialog',
        'label' => $form->getAttributeLabel('status'),
        'text' => $text,
        'enabled' => true,
        'button' => 'Статус',
        'title' => 'Изменить статус',
        'input' => Select2::widget([
            'model' => $form,
            'attribute' => 'status',
            'data' => array_filter(
                Task::STATUS_LIST,
                fn (int $status): bool => $status != Task::STATUS_EXPIRED,
                ARRAY_FILTER_USE_KEY
            ),
            'pluginOptions' => [
                'placeholder' => '',
                'width' => '100%',
            ],
            'hideSearch' => true,
        ]),
    ],
    [
        'id' => 'changeEmployeeDialog',
        'label' => $form->getAttributeLabel('employee_id'),
        'text' => $text,
        'enabled' => $form->canChangeEmployee,
        'button' => 'Ответственного',
        'title' => 'Изменить ответственного',
        'attribute' => 'employee_id',
        'input' => Select2::widget([
            'model' => $form,
            'attribute' => 'employee_id',
            'data' => (new ListFactory)->createEmployeeList()->getItems(),
            'pluginOptions' => [
                'placeholder' => '',
                'width' => '100%',
            ],
            'hideSearch' => true,
        ]),
    ],
];

?>

<?= Html::beginForm(null, 'post', ['id' => 'rowSelectForm']); ?>

<?= $content ?>

<div id="rowSelectSummary" class="wrap wrap_btns check-condition">
    <div class="row align-items-center justify-content-end">
        <div class="column flex-shrink-0 mr-3">
            <span class="checkbox-txt total-txt-foot ml-3" style="padding-left: 6px">
                Выбрано задач: <strong class="pl-2 total-count">0</strong>
            </span>
        </div>
        <div class="column ml-auto">
            <?= $canDelete ? Html::button($this->render('//svg-sprite', ['ico' => 'garbage']) . ' <span>Удалить</span>', [
                'class' => 'button-regular button-width button-hover-transparent',
                'data-toggle' => 'modal',
                'data-target' => '#task_many_delete',
            ]) : ''; ?>
        </div>
        <div class="column">
            <div class="dropup">
                <?= Html::button('<span class="pr-2">Изменить</span>  '.$this->render('//svg-sprite', ['ico' => 'shevron']), [
                    'class' => 'button-regular button-regular-more button-hover-transparent dropdown-toggle',
                    'data-toggle' => 'dropdown',
                ]) ?>
                <?= Dropdown::widget([
                    'items' => array_map(function (array $dialog) {
                        return [
                            'label' => $dialog['button'],
                            'url' => "#{$dialog['id']}",
                            'linkOptions' => ['data-toggle' => 'modal'],
                            'visible' => $dialog['enabled'],
                        ];
                    }, $dialogs),
                    'options' => ['class' => 'form-filter-list list-clr dropdown-menu-right'],
                ]) ?>
            </div>
        </div>
    </div>
</div>

<?php foreach ($dialogs as $dialog): ?>
    <?= $this->render('@frontend/modules/crm/views/widgets/row-select-dialog', $dialog) ?>
<?php endforeach; ?>

<?= Html::endForm() ?>

<?php if ($canDelete) : ?>
    <?php Modal::begin([
        'id' => 'task_many_delete',
        'title' => 'Вы уверены, что хотите удалить выбранные задачи?',
        'toggleButton' => false,
        'closeButton' => false,
    ]) ?>
        <div class="text-center">
            <?= Html::button('Да', [
                'class' => 'button-regular button-hover-transparent button-width-medium mr-2 task_many_delete_submit',
                'data-url' => Url::to(['many-delete']),
            ]) ?>
            <?= Html::button('Нет', [
                'class' => 'button-regular button-hover-transparent button-width-medium',
                'onclick' => "$('#task_many_delete').modal('hide')",
            ]) ?>
        </div>
    <?php Modal::end() ?>
<?php endif ?>
