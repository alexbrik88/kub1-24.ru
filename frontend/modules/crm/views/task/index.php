<?php

namespace frontend\modules\crm\views;

use frontend\modules\crm\models\Task;
use frontend\modules\crm\models\TaskFilter;
use frontend\modules\crm\assets\TaskFormAsset;
use frontend\modules\crm\models\TaskSelectForm;
use frontend\modules\crm\widgets\AddClientDialogWidget;
use frontend\modules\crm\widgets\AddItemDialogWidget;
use frontend\modules\crm\widgets\AjaxFormDialog;
use frontend\modules\crm\widgets\CurrentTasksDashboardWidget;
use frontend\modules\crm\widgets\GridButtonWidget;
use frontend\modules\crm\widgets\PjaxWidget;
use frontend\modules\crm\widgets\TasksStatisticWidget;
use frontend\rbac\permissions\crm\TaskAccess;
use frontend\themes\kub\widgets\ImportDialogWidget;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\User;
use yii\web\View;

/**
 * @var View $this
 * @var User $user
 * @var TaskFilter $filter
 * @var TaskSelectForm $form
 * @var bool $canDelete
 */

$this->title = 'Задачи';

TaskFormAsset::register($this);

$viewToggle = (bool) \Yii::$app->user->identity->config->crm_task_index_view;
?>

<?= ImportDialogWidget::widget() ?>
<?= AjaxFormDialog::widget() ?>
<?= AddClientDialogWidget::widget() ?>
<?= AddItemDialogWidget::widget() ?>

<div class="page-head d-flex flex-wrap align-items-center mb-3">
    <h4><?= Html::encode($this->title); ?></h4>

    <?= GridButtonWidget::widget([
        'accessName' => TaskAccess::EDIT,
        'label' => 'Добавить',
        'icon' => 'add-icon',
        'options' => [
            'class' => 'button-regular button-regular_red pl-3 pr-3 ml-auto ajax-form-button',
            'data-url' => Url::to(['task/create']),
            'data-title' => 'Добавить задачу № ' . Task::getNextNumber($user->identity->company),
        ],
    ]) ?>
</div>

<?= $this->render('widgets/task-filter', [
    'filter' => $filter,
    'viewToggle' => $viewToggle,
    'placeholder' => 'Поиск по описанию...',
]) ?>

<div class="task-index-grid <?= $viewToggle ? 'hidden' : '' ?>">
    <?php PjaxWidget::begin(['id' => 'pjaxContent']) ?>

    <?= TasksStatisticWidget::widget(['query' => $filter->getDataProvider()->query]) ?>

    <?= $this->render('widgets/task-grid', [
        'filter' => $filter,
        'form' => $form,
        'user' => $user,
        'canDelete' => $canDelete,
        'contractor' => null,
        'isContractor' => false,
    ]) ?>

    <?php PjaxWidget::end(); ?>
</div>

<div class="current-tasks-dashboard <?= $viewToggle ? '' : 'hidden' ?>">
    <?php PjaxWidget::begin(['id' => 'pjaxContent2']) ?>

    <?= CurrentTasksDashboardWidget::widget() ?>

    <?php PjaxWidget::end(); ?>
</div>

<?php $this->registerJs(<<<JS
    $('.crm_task_index_view_toggle').togleViewButton();
JS) ?>
