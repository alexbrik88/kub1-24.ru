<?php

namespace frontend\modules\crm\views;

use frontend\modules\crm\models\AbstractType;
use frontend\modules\crm\models\ConfigForm;
use frontend\modules\crm\widgets\GridButtonWidget;
use frontend\rbac\permissions\crm\ConfigAccess;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/**
 * @var ConfigForm $form
 */

$this->title = 'Общие настройки CRM';

?>
<div class="page-head d-flex flex-wrap align-items-center mb-2">
    <h4><?= Html::encode($this->title); ?></h4>

    <?= GridButtonWidget::widget([
        'accessName' => ConfigAccess::EDIT,
        'url' => Url::to(['update']),
        'icon' => 'pencil',
        'options' => [
            'title' => 'Редактировать',
            'class' => 'button-regular button-regular_red button-clr w-44 ml-auto invisible',
            'data-pjax' => '0',
        ],
    ]) ?>
</div>

<?php $widget = ActiveForm::begin([
    'id' => $form->formName(),
    'fieldConfig' => [
        'labelOptions' => ['class' => 'label'],
        'options' => ['class' => 'form-group col-4'],
    ],
    'options' => [
        'data-pjax' => false,
    ],
]); ?>

<div class="wrap">
    <div class="row">
        <?= $widget->field($form, 'show_customers')->widget(Select2::class, [
            'data' => [1 => 'Да', 0 => 'Нет'],
            'options' => [
                'value' => (int) $form->show_customers,
            ],
            'pluginOptions' => [
                'width' => '100%',
            ],
        ]) ?>
    </div>
</div>

<div class="wrap wrap_btns check-condition visible mb-0 mt-0">
    <div class="row align-items-center justify-content-between">
        <div class="column">
            <?= Html::submitButton('Сохранить', [
                'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
                'data-style' => 'expand-right',
            ]) ?>
        </div>
        <div class="column">
            <?= Html::a('Отменить', ['index'], [
                'class' => 'button-clr button-width button-regular button-hover-transparent',
                'data-pjax' => 0,
            ]) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
