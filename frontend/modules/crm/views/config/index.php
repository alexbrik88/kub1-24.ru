<?php

namespace frontend\modules\crm\views;

use frontend\modules\crm\models\Config;
use frontend\modules\crm\widgets\GridButtonWidget;
use frontend\rbac\permissions\crm\ConfigAccess;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/**
 * @var Config $config
 */

$this->title = 'Общие настройки CRM';

?>
<div class="page-head d-flex flex-wrap align-items-center mb-2">
    <h4><?= Html::encode($this->title); ?></h4>

    <?= GridButtonWidget::widget([
        'accessName' => ConfigAccess::EDIT,
        'url' => Url::to(['update']),
        'icon' => 'pencil',
        'options' => [
            'title' => 'Редактировать',
            'class' => 'button-regular button-regular_red button-clr w-44 ml-auto',
            'data-pjax' => '0',
        ],
    ]) ?>
</div>

<div class="wrap wrap_padding_small pl-4 pr-3 pb-2 mb-2">
    <div class="pl-1 pb-1">
        <div class="page-in row">
            <div class="col-9 column pr-4">
                <div class="pr-2">
                    <div class="row">
                        <div class="col mb-4 pb-2">
                            <div class="label weight-700 mb-3 mt-4">Выводить покупателей в CRM</div>
                            <div><?= $config->show_customers ? 'Да' : 'Нет' ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-3 column pl-0">

            </div>
        </div>
    </div>
</div>
