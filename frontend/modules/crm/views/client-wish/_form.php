<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\crm\models\ClientWishes */
/* @var $form yii\bootstrap4\ActiveForm */

$attrArray = $model->safeAttributes();
?>

<div class="client-wishes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if (in_array('text', $attrArray)) : ?>
        <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>
    <?php endif ?>

    <?php if (in_array('status_id', $attrArray)) : ?>
        <?= $form->field($model, 'status_id')->widget(Select2::class, [
            'data' => $model->statusDropdownData(),
            'options' => [
                'placeholder' => '',
            ],
            'pluginOptions' => [
                'width' => '100%',
            ],
        ]) ?>
    <?php endif ?>

    <?php if (in_array('responsible_employee_id', $attrArray)) : ?>
        <?= $form->field($model, 'responsible_employee_id')->widget(Select2::class, [
            'data' => $model->responsibleDropdownData(),
            'options' => [
                'placeholder' => '',
            ],
            'pluginOptions' => [
                'width' => '100%',
            ],
        ]) ?>
    <?php endif ?>

    <div class="d-flex">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-width button-regular_red',
        ]) ?>
        <?= Html::button('Отменить', [
            'class' => 'button-width button-regular button-hover-transparent ml-auto',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
