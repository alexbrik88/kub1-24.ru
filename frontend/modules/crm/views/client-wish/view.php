<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\crm\models\ClientWishes */

?>

<div class="client-wishes-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'contractor_id',
            'number',
            'responsible_employee_id',
            'status_id',
            'date',
            'text:ntext',
        ],
    ]) ?>

</div>
