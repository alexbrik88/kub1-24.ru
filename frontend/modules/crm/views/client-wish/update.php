<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\crm\models\ClientWishForm */

?>
<div class="client-wishes-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
