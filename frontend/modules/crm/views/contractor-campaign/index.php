<?php

namespace frontend\modules\crm\views;

use frontend\components\WebUser;
use frontend\modules\crm\models\TypeFilter;
use yii\web\View;

/**
 * @var View $this
 * @var WebUser $user
 * @var TypeFilter $filter
 * @var string $createAccess
 * @var string $editAccess
 */

$this->title = 'Источник';

?>

<?= $this->render('@frontend/modules/crm/views/settings/type-grid', [
    'user' => $user,
    'filter' => $filter,
    'createAccess' => $createAccess,
    'editAccess' => $editAccess,
    'deletionError' => 'Удалить нельзя, так как этот источник задействован. Вы можете нажать на редактировать и поменять "Используется" на "Архив"',
]) ?>
