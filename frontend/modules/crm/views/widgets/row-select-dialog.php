<?php

namespace frontend\modules\crm\views;

use frontend\modules\crm\assets\RowSelectAsset;
use frontend\themes\kub\widgets\SpriteIconWidget;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\web\View;

/**
 * @var View $this
 * @var string $label
 * @var string $input
 * @var string $title
 * @var string $id
 * @var string $text
 */

RowSelectAsset::register($this);

?>

<?php Modal::begin([
    'options' => ['id' => $id],
    'closeButton' => [
        'label' => SpriteIconWidget::widget(['icon' => 'close']),
        'class' => 'modal-close close',
    ],
    'title' => $title,
]); ?>

<div class="row">
    <?php if ($text): ?>
    <div class="form-group col-12">
        <b><?= $text ?></b>
    </div>
    <?php endif; ?>

    <div class="form-group col-6">
        <label class="label"><?= Html::encode($label) ?></label>
        <?= $input ?>
    </div>
    <?php if (isset($checkbox)) : ?>
        <div class="form-group col-6">
            <label class="label">&nbsp;</label>
            <div class="pt-2">
                <?= $checkbox ?>
            </div>
        </div>
    <?php endif ?>
</div>

<?= $this->render('@frontend/modules/crm/views/widgets/dialog-buttons') ?>

<?php Modal::end(); ?>

<?php $this->registerJs(<<<JS
    (function () {
        $('{$id}').on('hidden.bs.modal', function () {
            $(this).find('select').val('');
            $(this).find('select').trigger('change');
        });
    })();
JS); ?>
