<?php

namespace frontend\modules\crm\views;

use yii\bootstrap4\Html;

?>

<div class="row mt-4">
    <div class="form-group col-6 text-left mb-0">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-width button-regular_red button-clr',
            'data-style' => 'expand-right',
        ]) ?>
    </div>

    <div class="form-group col-6 text-right mb-0">
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'onclick' => <<<JS
                $(this).closest('.modal').first().modal('hide'); return false;
JS
        ]) ?>
    </div>
</div>
