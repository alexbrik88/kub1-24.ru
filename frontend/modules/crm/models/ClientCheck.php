<?php

namespace frontend\modules\crm\models;

use frontend\modules\crm\behaviors\CanDeleteBehavior;

/**
 * Тип чека клиента
 *
 * @property-read int $client_check_id
 */
final class ClientCheck extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['canDelete'] = [
            'class' => CanDeleteBehavior::class,
            'modelClass' => Client::class,
            'modelAttribute' => 'client_check_id',
        ];

        return $behaviors;
    }

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%crm_client_check}}';
    }

    /**
     * @inheritDoc
     */
    public static function getDefaultRows(int $company_id): array
    {
        return [
            ['company_id' => $company_id, 'name' => 'Низкий'],
            ['company_id' => $company_id, 'name' => 'Средний'],
            ['company_id' => $company_id, 'name' => 'Высокий'],
            ['company_id' => $company_id, 'name' => 'Крупный'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'Чек';
    }
}
