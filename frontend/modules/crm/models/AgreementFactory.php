<?php

namespace frontend\modules\crm\models;

use common\components\date\DateHelper;
use common\models\Agreement;
use common\models\Contractor;
use common\models\employee\Employee;

final class AgreementFactory
{
    /**
     * @param Employee $employee
     * @param Contractor $contractor
     * @return Agreement
     */
    public function createAgreement(Employee $employee, Contractor $contractor): Agreement
    {
        $attributes = [
            'created_by' => $employee->id,
            'company_id' => $employee->company->id,
            'contractor_id' => $contractor->id,
            'is_created' => false,
        ];

        $agreement = Agreement::findOne($attributes) ?: new Agreement($attributes);
        $agreement->setAttributes([
            'document_date' => date(DateHelper::FORMAT_DATE),
            'document_number' => '',
            'document_name' => '',
            'created_at' => time(),
        ], false);

        return $agreement;
    }
}
