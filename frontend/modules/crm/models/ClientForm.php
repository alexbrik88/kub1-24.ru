<?php

namespace frontend\modules\crm\models;

use common\models\Contractor;
use yii\base\Model;

final class ClientForm extends Model
{
    use ValidateTypeTrait;

    /**
     * @var int
     */
    public $client_check_id;

    /**
     * @var int
     */
    public $client_reason_id;

    /**
     * @var int
     */
    public $client_status_id;

    /**
     * @var int
     */
    public $client_type_id;

    /**
     * @var int
     */
    public $deal_type_id;

    /**
     * @var int
     */
    public $deal_stage_id;

    /**
     * @var string
     */
    public $site_url;

    /**
     * @var Contractor
     */
    private Contractor $contractor;

    /**
     * @var Client
     */
    private Client $client;

    /**
     * @var DealType|null
     */
    private ?DealType $dealType;

    /**
     * @param Contractor $contractor
     * @param DealType|null $dealType
     */
    public function __construct(Contractor $contractor, ?DealType $dealType = null)
    {
        $this->contractor = $contractor;
        $this->client = $this->contractor->client ?? Client::createInstance($this->contractor);
        $this->dealType = $dealType;

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function init()
    {
        $attributes = $this->client->getAttributes($this->activeAttributes());

        if ($this->dealType) {
            $attributes['deal_type_id'] = $this->dealType->deal_type_id;
        }

        $this->setAttributes($attributes, false);
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels(): array
    {
        return [
            'client_check_id' => 'Чек',
            'client_reason_id' => 'Тип обращения',
            'client_status_id' => 'Статус',
            'client_type_id' => 'Тип',
            'deal_type_id' => 'Тип сделки',
            'deal_stage_id' => 'Этап сделки',
            'site_url' => 'Сайт',
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [
                [
                    'client_status_id',
                    'client_type_id',
                ],
                'required',
            ],

            [['client_check_id'], 'integer'],
            [['client_check_id'], 'validateType', 'params' => ['modelClass' => ClientCheck::class]],

            [['client_reason_id'], 'integer'],
            [['client_reason_id'], 'validateType', 'params' => ['modelClass' => ClientReason::class]],

            [['client_status_id'], 'integer'],
            [['client_status_id'], 'validateType', 'params' => ['modelClass' => ClientStatus::class]],

            [['client_type_id'], 'integer'],
            [['client_type_id'], 'validateType', 'params' => ['modelClass' => ClientType::class]],

            [['deal_type_id'], 'integer'],
            [['deal_type_id'], 'validateType', 'params' => ['modelClass' => DealType::class]],

            [['deal_stage_id'], 'integer'],
            [['deal_stage_id'], 'validateType', 'params' => ['modelClass' => DealStage::class]],

            [['site_url'], 'trim'],
            [['site_url'], 'url', 'defaultScheme' => 'http'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function scenarios(): array
    {
        return [
            self::SCENARIO_DEFAULT => [
                'client_check_id',
                'client_reason_id',
                'client_status_id',
                'client_type_id',
                'deal_type_id',
                'deal_stage_id',
                'site_url',
            ],
        ];
    }

    /**
     * @return bool
     */
    public function saveModel(): bool
    {
        $this->contractor->refresh();
        $dealTypeId = $this->dealType ? $this->dealType->deal_type_id : $this->deal_type_id;
        $attributes = $this->getAttributes($this->activeAttributes());
        $attributes['deal_type_id'] = $dealTypeId;
        $attributes['deal_stage_id'] = $this->getDealStage($dealTypeId)->deal_stage_id ?? null;
        $attributes['contractor_id'] = $this->contractor->id;
        $this->client->setAttributes($attributes, false);

        return $this->client->save(false);
    }

    /**
     * @return bool
     */
    public function removeClient(): bool
    {
        return $this->contractor->updateAttributes(['is_deleted' => true]);
    }

    /**
     * @param int $dealTypeId
     * @return DealStage|null
     */
    private function getDealStage($dealTypeId): ?DealStage
    {
        $stage = DealStage::findOne([
            'deal_type_id' => $dealTypeId,
            'deal_stage_id' => $this->deal_stage_id,
        ]);

        if ($stage) {
            return $stage;
        }

        $deal = DealType::findById($this->contractor->company, $dealTypeId);

        if ($deal && $deal->getStagesCount() > 0) {
            return $deal->getFirstStage();
        }

        return null;
    }

    /**
     * @return DealType|null
     */
    public function getDealType(): ?DealType
    {
        return $this->dealType;
    }
}
