<?php

namespace frontend\modules\crm\models;

use common\components\date\DateHelper;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\ListInterface;
use DateTimeImmutable;

final class LogListFactory
{
    /**
     * @var $filter
     */
    private $filter;

    /**
     * @param LogFilter $filter
     */
    public function __construct(LogFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return ListInterface
     */
    public function createMessageList(): ListInterface
    {
        $logs = $this->filter->getDataProvider()->getModels();
        $messages = array_map(fn (Log $log) => $this->createMessage($log), $logs);
        $items = array_filter($messages);

        return new CommonList($items);
    }

    /**
     * @param Log $log
     * @return string|null
     */
    private function createMessage(Log $log): ?string // TODO: check model class
    {
        $created = DateHelper::format($log->created_at, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATETIME);
        $creator = $log->employeeCompany ? $log->employeeCompany->getShortFio() : '';

        switch ($log->attribute_name) {
            case null:
                return ($log->log_type == Log::LOG_TYPE_MODEL_CREATE)
                    ? ($creator ? sprintf('%s Создано %s', $created, $creator) : sprintf('%s Автосоздание', $created))
                    : null;
            case 'status':
                $status = Task::STATUS_LIST[$log->new_attribute_value];

                return sprintf('%s Изменён статус на %s. %s', $created, $status, $creator);
            case 'datetime':
                return $this->createDateTimeMessage($log, $created, $creator);
            case 'description':
                return sprintf('%s Изменено описание. %s', $created, $creator);
            case 'event_type':
                $eventName = (new TaskEventList)->getItems()[$log->new_attribute_value];

                return sprintf('%s Событие на %s. %s', $created, $eventName, $creator);
            case 'employee_id':
                $employeeCompany = EmployeeCompany::findOne([
                    'employee_id' => $log->new_attribute_value,
                    'company_id' => $log->company_id,
                ]);

                return $employeeCompany
                    ? sprintf('%s Изменен ответственный на %s. %s', $created, $employeeCompany->getShortFio(), $creator)
                    : null;
        }

        return null;
    }

    /**
     * @param Log $log
     * @param string $created
     * @param string $creator
     * @return string
     */
    private function createDateTimeMessage(Log $log, string $created, string $creator): string
    {
        $oldDate = DateTimeImmutable::createFromFormat(DateHelper::FORMAT_DATETIME, $log->old_attribute_value);
        $newDate = DateTimeImmutable::createFromFormat(DateHelper::FORMAT_DATETIME, $log->new_attribute_value);
        $dateChanged = ($newDate->format(DateHelper::FORMAT_DATE) != $oldDate->format(DateHelper::FORMAT_DATE));
        $timeChanged = ($newDate->format(DateHelper::FORMAT_TIME) != $oldDate->format(DateHelper::FORMAT_TIME));
        $date = $newDate->format(DateHelper::FORMAT_USER_DATE);
        $time = $newDate->format(DateHelper::FORMAT_USER_TIME);

        if ($dateChanged && $timeChanged) {
            return sprintf('%s Перенос даты на %s и времени на %s. %s', $created, $date, $time, $creator);
        } elseif ($dateChanged) {
            return sprintf('%s Перенос даты на %s. %s', $created, $date, $creator);
        } else {
            return sprintf('%s Перенос времени на %s. %s', $created, $time, $creator);
        }
    }
}
