<?php

namespace frontend\modules\crm\models;

use Yii;
use common\models\Company;
use common\models\Contractor;
use common\models\EmployeeCompany;
use common\models\employee\Employee;

/**
 * This is the model class for table "crm_client_refusal".
 *
 * @property int $id
 * @property int $company_id
 * @property int $contractor_id
 * @property int|null $refusal_id
 * @property int $employee_id
 * @property string $created_at
 * @property string $updated_at
 * @property string|null $details
 *
 * @property Contractor $contractor
 * @property Employee $employee
 * @property CrmRefusal $refusal
 */
class ClientRefusal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_client_refusal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['refusal_id'], 'integer'],
            [['details'], 'trim'],
            [['details'], 'required'],
            [['details'], 'string'],
            [
                ['refusal_id'], 'exist',
                'skipOnError' => true,
                'targetClass' => Refusal::className(),
                'targetAttribute' => ['refusal_id' => 'refusal_id'],
                'filter' => function ($query) {
                    $query->andWhere([
                        'company_id' => $this->company_id,
                    ]);
                }
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'contractor_id' => 'Contractor ID',
            'refusal_id' => 'Вариант отказа',
            'details' => 'Подробное описание причины отказа',
            'created_at' => 'Дата',
            'employee_id' => 'Ответственный',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * Gets query for [[Contractor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * Gets query for [[Client]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'contractor_id']);
    }

    /**
     * Gets query for [[Employee]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * Gets query for [[EmployeeCompany]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeCompany()
    {
        return $this->hasOne(EmployeeCompany::className(), [
            'employee_id' => 'employee_id',
            'company_id' => 'company_id',
        ]);
    }

    /**
     * Gets query for [[Refusal]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRefusal()
    {
        return $this->hasOne(Refusal::className(), ['refusal_id' => 'refusal_id']);
    }
}
