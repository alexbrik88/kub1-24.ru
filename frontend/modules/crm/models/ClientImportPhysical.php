<?php

namespace frontend\modules\crm\models;

use common\models\Contractor;
use common\models\company\CompanyType;

final class ClientImportPhysical extends AbstractClientImport
{
    const FACE_TYPE = Contractor::TYPE_PHYSICAL_PERSON;

    public $date;
    public $physical_lastname;
    public $physical_firstname;
    public $physical_patronymic;
    public $contact_phone;
    public $contact_email;
    public $contact_skype;
    public $deal_type;
    public $campaign;
    public $activity;
    public $client_type;
    public $client_check;
    public $client_reason;
    public $comment;
    public $physical_address;
    public $physical_passport_series;
    public $physical_passport_number;
    public $physical_passport_date_output;

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [
                [
                    'date',
                    'physical_lastname',
                    'physical_firstname',
                    'physical_patronymic',
                    'contact_phone',
                    'contact_email',
                    'contact_skype',
                    'deal_type',
                    'campaign',
                    'activity',
                    'client_type',
                    'client_check',
                    'client_reason',
                    'comment',
                    'physical_address',
                    'physical_passport_series',
                    'physical_passport_number',
                    'physical_passport_date_output',
                ],
                'trim',
            ],
            [
                [
                    'physical_lastname',
                    'physical_firstname',
                ],
                'required',
            ],
            [['contact_email'], 'email'],
            [
                ['physical_lastname'],
                'unique',
                'targetClass' => Contractor::class,
                'filter' => function ($query) {
                    $query->andWhere([
                        'company_id' => $this->company_id,
                        'physical_firstname' => $this->physical_firstname,
                        'is_deleted' => false,
                    ]);
                    if (empty($this->physical_patronymic)) {
                        $query->andWhere([
                            'physical_patronymic' => $this->physical_patronymic,
                        ]);
                    } else {
                        $query->andWhere([
                            'or',
                            'physical_patronymic' => null,
                            'physical_patronymic' => '',
                        ]);
                    }
                },
                'message' => 'Такой клиент уже есть.',
            ],
        ];
    }

    public function attributeLabels() : array
    {
        return [
            'date' => 'Дата добавления',
            'physical_lastname' => 'Фамилия',
            'physical_firstname' => 'Имя',
            'physical_patronymic' => 'Отчество',
            'contact_phone' => 'Телефон',
            'contact_email' => 'E-mail',
            'contact_skype' => 'Skype',
            'deal_type' => 'Тип сделки',
            'campaign' => 'Источник',
            'activity' => 'Вид деятельности',
            'client_type' => 'Тип отношений',
            'client_check' => 'Чек',
            'client_reason' => 'Тип обращения',
            'comment' => 'Комментарий',
            'physical_address' => 'Адрес регистрации',
            'physical_passport_series' => 'Серия паспорта',
            'physical_passport_number' => 'Номер паспорта',
            'physical_passport_date_output' => 'Дата выдачи',
        ];
    }

    public function createContractorFormData() : array
    {
        return [
            'Contractor' => [
                'face_type' => Contractor::TYPE_PHYSICAL_PERSON,
                'physical_lastname' => (string) $this->physical_lastname,
                'physical_firstname' => (string) $this->physical_firstname,
                'physical_patronymic' => (string) $this->physical_patronymic,
                'physical_no_patronymic' => empty($this->physical_patronymic) ? '1' : '0',
                'campaign_id' => $this->getContractorCampaignId($this->campaign),
                'activity_id' => $this->getContractorActivityId($this->activity),
                'comment' => (string) $this->comment,
                'physical_address' => (string) $this->physical_address,
                'physical_passport_series' => (string) $this->physical_passport_series,
                'physical_passport_number' => (string) $this->physical_passport_number,
                'physical_passport_date_output' => ($d = date_create($this->physical_passport_date_output)) ? $d->format('Y-m-d') : null,
            ],
            'ContactForm' => [
                'primary_phone_number' => (string) $this->contact_phone,
                'email_address' => (string) $this->contact_email,
                'skype_account' => (string) $this->contact_skype,
            ],
            'ClientForm' => [
                'client_status_id' => ClientStatus::STATUS_ACTIVE,
                'client_check_id' => $this->getClientCheckId($this->client_check),
                'client_reason_id' => $this->getClientReasonId($this->client_reason),
                'client_type_id' => $this->getClientTypeId($this->client_type, ClientType::TYPE_CLIENT),
                'deal_type_id' => $this->getDealTypeId($this->deal_type),
            ],
        ];
    }
}
