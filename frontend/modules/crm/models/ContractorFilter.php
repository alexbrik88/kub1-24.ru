<?php

namespace frontend\modules\crm\models;

use common\db\SortBuilder;
use common\models\Contractor;
use common\models\Company;
use common\models\employee\Employee;
use common\models\FilterInterface;
use DateTimeInterface;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\data\Sort;
use yii\db\ActiveQuery;

final class ContractorFilter extends Model implements FilterInterface
{
    /**
     * @var string|null
     */
    public $client_position_id;

    /**
     * @var string|null
     */
    public $event_type;

    /**
     * @var string|null
     */
    public $client_status_id;

    /**
     * @var string|null
     */
    public $client_type_id;

    /**
     * @var string|null
     */
    public $campaign_id;

    /**
     * @var string|null
     */
    public $activity_id;

    /**
     * @var string|null
     */
    public $client_check_id;

    /**
     * @var string|null
     */
    public $client_reason_id;

    /**
     * @var string|null
     */
    public $responsible_employee_id;

    /**
     * @var string|null
     */
    public $search;

    /**
     * @var string|null
     */
    public $contact_name;

    /**
     * @var string|null
     */
    public $contact_phone;

    /**
     * @var string|null
     */
    public $contact_email;

    /**
     * @var DateTimeInterface|null
     */
    public ?DateTimeInterface $dateTo;

    /**
     * @var DateTimeInterface|null
     */
    public ?DateTimeInterface $dateFrom;

    /**
     * @var int|null
     */
    public $owner_id;

    /**
     * @var int[]|null
     */
    public $activity_ids;

    /**
     * @var int[]|null
     */
    public $campaign_ids;

    /**
     * @var int[]|null
     */
    public $client_check_ids;

    /**
     * @var Company
     */
    private Company $company;

    /**
     * @var Config
     */
    private Config $config;

    /**
     * @var Employee|null
     */
    private ?Employee $owner;

    /**
     * @var ContractorListFactory
     */
    private ContractorListFactory $listFactory;

    /**
     * @var int
     */
    private int $pageSize;

    /**
     * @param Company $company
     * @param Employee|null $owner
     * @param DateTimeInterface|null $dateFrom
     * @param DateTimeInterface|null $dateTo
     * @param int|null $pageSize
     */
    public function __construct(
        Company $company,
        ?Employee $owner,
        ?DateTimeInterface $dateFrom = null,
        ?DateTimeInterface $dateTo = null,
        ?int $pageSize = null
    ) {
        $this->company = $company;
        $this->owner = $owner;
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
        $this->listFactory = new ContractorListFactory($this);
        $this->pageSize = $pageSize ?: self::DEFAULT_PAGE_SIZE;
        $this->config = Config::getInstance($company) ?: Config::createInstance($company);

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels(): array
    {
        return [
            'campaign_id' => 'Источник',
            'activity_id' => 'Вид деятельности',
            'client_check_id' => 'Чек',
            'client_status_id' => 'Статус',
            'client_type_id' => 'Тип',
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['client_position_id'], 'integer'],
            [['event_type'], 'integer'],
            [['client_status_id'], 'integer'],
            [['client_type_id'], 'integer'],
            [['campaign_id'], 'integer'],
            [['activity_id'], 'integer'],
            [['client_check_id'], 'integer'],
            [['client_reason_id'], 'integer'],
            [['activity_ids'], 'each', 'rule' => ['integer']],
            [['activity_ids'], 'filter', 'filter' => [$this, 'normalizeIds']],

            [['campaign_ids'], 'each', 'rule' => ['integer']],
            [['campaign_ids'], 'filter', 'filter' => [$this, 'normalizeIds']],

            [['client_check_ids'], 'each', 'rule' => ['integer']],
            [['client_check_ids'], 'filter', 'filter' => [$this, 'normalizeIds']],

            [['responsible_employee_id'], 'integer'],
            [['search'], 'string', 'max' => 45],
            [['contact_name'], 'string', 'max' => 255],
            [['contact_phone'], 'string', 'max' => 45],
            [['contact_email'], 'string', 'max' => 45],
        ];
    }

    /**
     * @param mixed $value
     * @return array
     */
    public function normalizeIds($value): array
    {
        if (is_array($value)) {
            return array_filter($value);
        }

        return [];
    }

    /**
     * @return ActiveQuery
     */
    public function createQuery(): ActiveQuery
    {
        $query = Contractor::find()->alias('contractor')
            ->joinWith(['client crm_client', 'defaultContact crm_contact'])
            ->andWhere([
                'contractor.company_id' => $this->company->id,
                'contractor.is_deleted' => false,
            ])
            ->andWhere([
                'or',
                ['contractor.is_customer' => true],
                ['not', ['crm_client.contractor_id' => null]],
            ])
            ->andFilterWhere([
                'crm_client.event_type' => $this->event_type,
                'crm_client.client_status_id' => $this->client_status_id,
                'crm_client.client_type_id' => $this->client_type_id,
                'crm_client.client_check_id' => $this->client_check_id,
                'crm_client.client_reason_id' => $this->client_reason_id,
                'contractor.campaign_id' => $this->campaign_id,
                'contractor.activity_id' => $this->activity_id,
                'contractor.responsible_employee_id' => $this->responsible_employee_id,
                'crm_contact.contact_name' => $this->contact_name,
                'crm_contact.client_position_id' => $this->client_position_id,
                'crm_contact.primary_phone_number' => $this->contact_phone,
                'crm_contact.email_address' => $this->contact_email,
            ])->groupBy('contractor.id');

        if ($this->activity_ids) {
            $query->andWhere(['in', Contractor::tableName() . '.activity_id', $this->activity_ids]);
        }

        if ($this->campaign_ids) {
            $query->andWhere(['in', Contractor::tableName() . '.campaign_id', $this->campaign_ids]);
        }

        if ($this->client_check_ids) {
            $query->andWhere(['in', Client::tableName() . '.client_check_id', $this->client_check_ids]);
        }

        // TODO: фильтрация по таблице контактов
        if ($this->dateFrom && $this->dateTo) {
            $query->andWhere([
                'between',
                Contractor::tableName() . '.created_at',
                $this->dateFrom->getTimestamp(),
                $this->dateTo->getTimestamp(),
            ]);
        }

        if (!$this->config->show_customers) {
            $query->andWhere(['is not', Client::tableName() . '.contractor_id', null]);
        }

        if ($this->owner) {
            $on = sprintf('%s.id = %s.contractor_id', Contractor::tableName(), Task::tableName());
            $query->leftJoin(Task::tableName(), $on)->andWhere(['or',
                [Contractor::tableName() . '.responsible_employee_id' => $this->owner->id],
                [Task::tableName() . '.employee_id' => $this->owner->id],
            ]);
        }

        if (($search = trim($this->search)) && strlen($search)) {
            $query->andWhere([
                'or',
                ['like', 'contractor.name', $search],
                ['like', 'crm_contact.contact_name', $search],
                ['like', 'crm_contact.primary_phone_number', $search],
                ['like', 'crm_contact.secondary_phone_number', $search],
                ['like', 'crm_contact.email_address', $search],
            ]);
        }

        return $query;
    }

    /**
     * @inheritDoc
     */
    public function getDataProvider(): DataProviderInterface
    {
        return new ActiveDataProvider([
            'query' => $this->createQuery(),
            'sort' => $this->getSort(),
            'pagination' => [
                'pageSize' => $this->pageSize,
            ],
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        $builder = new SortBuilder();
        $builder->addOrder('created_at', Contractor::tableName());
        $builder->addOrder('name', Contractor::tableName(), 'name');
        $builder->addDefaultOrder('created_at', SORT_DESC);

        return $builder->buildSort();
    }

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
        $filter = new self($this->company, $this->owner);

        return $filter->createQuery()->count();
    }

    /**
     * @return ContractorListFactory
     */
    public function getListFactory(): ContractorListFactory
    {
        return $this->listFactory;
    }
}
