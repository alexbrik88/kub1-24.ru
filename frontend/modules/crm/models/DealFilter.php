<?php

namespace frontend\modules\crm\models;

use common\db\SortBuilder;
use common\models\Company;
use common\models\Contractor;
use common\models\FilterInterface;
use common\models\employee\Employee;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\data\Sort;
use yii\db\ActiveQuery;
use yii\db\Expression;

final class DealFilter extends Model implements FilterInterface
{
    /**
     * @var int
     */
    public $contractor_id;

    /**
     * @var int
     */
    public $employee_id;

    /**
     * @var int
     */
    public $deal_stage_id;

    /**
     * @var string
     */
    public $contact_name;

    /**
     * @var string
     */
    public $search;

    /**
     * @var Company
     */
    private Company $company;

    /**
     * @var Employee|null
     */
    private ?Employee $owner;

    /**
     * @var DealType|null
     */
    private ?DealType $dealType;

    /**
     * @var DealListFactory
     */
    private DealListFactory $listFactory;

    /**
     * @var int
     */
    private int $pageSize;

    /**
     * @param Company $company
     * @param DealType|null $dealType
     * @param int|null $pageSize
     */
    public function __construct(Company $company, ?DealType $dealType = null, ?int $pageSize = null, ?Employee $owner = null)
    {
        $this->company = $company;
        $this->owner = $owner;
        $this->dealType = $dealType;
        $this->listFactory = new DealListFactory($this);
        $this->pageSize = $pageSize ?: self::DEFAULT_PAGE_SIZE;

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['contractor_id'], 'integer'],
            [['deal_stage_id'], 'integer'],
            [['employee_id'], 'integer'],
            [['contact_name'], 'string'],
            [['search'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getDataProvider(): DataProviderInterface
    {
        return new ActiveDataProvider([
            'query' => $this->createQuery()->with([
                'contractor',
                'contact',
                'currency',
                'stage',
                'type',
                'employeeCompany',
            ]),
            'sort' => $this->getSort(),
            'pagination' => [
                'pageSize' => $this->pageSize,
            ],
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        $builder = new SortBuilder();

        $builder->addOrder('name', Deal::tableName());
        $builder->addOrder('amount', Deal::tableName());
        $builder->addOrder('created_at', Deal::tableName());
        $builder->addOrder('staged_at', Deal::tableName(), 'staged_at', SORT_DESC);
        $builder->addOrders(['name', 'amount', 'created_at', 'staged_at'], Deal::tableName(), 'deal_id', SORT_DESC);
        $builder->addDefaultOrder('created_at', SORT_DESC);

        return $builder->buildSort();
    }

    /**
     * @return ActiveQuery
     */
    public function createQuery(): ActiveQuery
    {
        $query = Deal::find()
            ->joinWith(['contractor', 'contact'], false)
            ->andWhere([Deal::tableName() . '.company_id' => $this->company->id])
            ->andFilterWhere([
                Deal::tableName() . '.deal_type_id' => $this->dealType->deal_type_id ?? null,
                Deal::tableName() . '.contractor_id' => $this->contractor_id,
                Deal::tableName() . '.deal_stage_id' => $this->deal_stage_id,
                Deal::tableName() . '.employee_id' => $this->employee_id,
            ]);

        if ($this->owner) {
            $query->andWhere([
                Deal::tableName() . '.employee_id' => $this->owner->id,
            ]);
        }

        if (strlen($this->search) > 0) {
            $query->andWhere(['or',
                ['like', Contractor::tableName() . '.name', $this->search],
                ['like', Deal::tableName() . '.name', $this->search],
            ]);
        }

        if (strlen($this->contact_name) > 0) {
            $query->andWhere(['or',
                [
                    Contact::tableName() . '.contact_name' => $this->contact_name
                ],
                [
                    Deal::tableName() . '.contact_id' => null,
                    Contractor::tableName() . '.contact_is_director' => false,
                    Contractor::tableName() . '.contact_name' => $this->contact_name,
                ],
                [
                    Deal::tableName() . '.contact_id' => null,
                    Contractor::tableName() . '.contact_is_director' => true,
                    Contractor::tableName() . '.director_name' => $this->contact_name,
                ],
            ]);
        }

        return $query;
    }

    /**
     * @return DealListFactory
     */
    public function getListFactory(): DealListFactory
    {
        return $this->listFactory;
    }

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
        $filter = new self($this->company, $this->dealType, $this->pageSize);

        return $filter->createQuery()->count();
    }

    /**
     * @return DealTypeSummary
     * @throws
     */
    public function getSummary(): DealTypeSummary
    {
        $rows = $this
            ->createQuery()
            ->select([
                new Expression(sprintf('%s as deal_stage_id', Deal::tableName() . '.deal_stage_id')),
                new Expression(sprintf('SUM(%s) as amount', Deal::tableName() . '.amount')),
                new Expression(sprintf('COUNT(%s) as count', Deal::tableName() . '.deal_id')),
            ])
            ->groupBy([
                Deal::tableName() . '.deal_stage_id',
            ])
            ->createCommand()
            ->queryAll();

        return new DealTypeSummary($rows);
    }
}
