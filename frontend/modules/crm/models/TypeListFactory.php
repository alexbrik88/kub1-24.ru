<?php

namespace frontend\modules\crm\models;

use common\models\ListInterface;

final class TypeListFactory
{
    /**
     * @var TypeFilter
     */
    private TypeFilter $filter;

    /**
     * @param TypeFilter $filter
     */
    public function __construct(TypeFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return ListInterface
     */
    public function createEmployeeList(): ListInterface
    {
        $id = $this->filter
            ->createQuery()
            ->select('employee_id')
            ->groupBy('employee_id')
            ->andWhere(['not', ['employee_id' => null]])
            ->column();

        return (new ListFactory)->createEmployeeList($id);
    }

    /**
     * @return ListInterface
     */
    public function createStatusList(): ListInterface
    {
        $statuses = $this->filter
            ->createQuery()
            ->select('status')
            ->groupBy('status')
            ->column();

        $callback = function (int $status) use ($statuses) {
            return in_array($status, $statuses);
        };

        return new CommonList(array_filter(AbstractType::TYPE_STATUS_LIST, $callback, ARRAY_FILTER_USE_KEY));
    }
}
