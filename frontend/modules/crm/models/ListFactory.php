<?php

namespace frontend\modules\crm\models;

use common\models\Agreement;
use common\models\AgreementTemplate;
use common\models\AgreementType;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use common\components\helpers\ArrayHelper;
use common\models\company\CompanyType;
use common\models\currency\Currency;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use common\models\ListInterface;
use DateInterval;
use DatePeriod;
use DateTimeImmutable;
use frontend\components\WebUser;
use frontend\rbac\permissions\crm\ClientAccess;
use yii\db\Expression;
use yii\web\User;

/**
 * Фабрика полных списков типов
 */
final class ListFactory
{
    /**
     * @var User|WebUser
     */
    private User $user;

    /**
     * @var AbstractType[]
     */
    private static $cache = [];

    /**
     * @throws
     */
    public function __construct()
    {
        $this->user = (new WebUserFactory)->createUser();
    }

    /**
     * @param string|AbstractType $modelClass
     * @param int|null $status
     * @return ListInterface
     */
    public function createTypeList(string $modelClass, ?int $status = null, bool $resetCache = false): ListInterface
    {
        if ($resetCache || !isset(self::$cache[$modelClass])) {
            self::$cache[$modelClass] = $modelClass::createListQuery($this->user->identity->company)->all();
        }

        $records = ArrayHelper::index(self::$cache[$modelClass], $modelClass::getIdKey());

        if ($status) {
            $records = array_filter($records, function (AbstractType $record) use ($status) {
                return ($record->getStatus() == $status);
            });
        }

        return new CommonList(ArrayHelper::getColumn($records, 'name'));
    }

    /**
     * @return ListInterface
     */
    public function createReasonList(): ListInterface
    {
        return $this->createTypeList(ClientReason::class, AbstractType::TYPE_STATUS_ACTIVE);
    }

    /**
     * @return ListInterface
     */
    public function createCheckList(): ListInterface
    {
        return $this->createTypeList(ClientCheck::class, AbstractType::TYPE_STATUS_ACTIVE);
    }

    /**
     * @return ListInterface
     */
    public function createPositionList(): ListInterface
    {
        return $this->createTypeList(ClientPosition::class, AbstractType::TYPE_STATUS_ACTIVE);
    }

    /**
     * @return ListInterface
     */
    public function createActivityList(): ListInterface
    {
        return $this->createTypeList(ContractorActivity::class, AbstractType::TYPE_STATUS_ACTIVE);
    }

    /**
     * @return ListInterface
     */
    public function createCampaignList(): ListInterface
    {
        return $this->createTypeList(ContractorCampaign::class, AbstractType::TYPE_STATUS_ACTIVE);
    }

    /**
     * @return ListInterface
     */
    public function createStatusList(): ListInterface
    {
        return $this->createTypeList(ClientStatus::class, AbstractType::TYPE_STATUS_ACTIVE);
    }

    /**
     * @return ListInterface
     */
    public function createClientTypeList(): ListInterface
    {
        return $this->createTypeList(ClientType::class, AbstractType::TYPE_STATUS_ACTIVE);
    }

    /**
     * @return ListInterface
     */
    public function createDealList(): ListInterface
    {
        return $this->createTypeList(DealType::class, AbstractType::TYPE_STATUS_ACTIVE);
    }

    /**
     * @param array|null $dealStageId
     * @param DealType|null $dealType
     * @return ListInterface
     */
    public function createDealStageList(?DealType $dealType = null, array $dealStageId = null): ListInterface
    {
        $items = DealStage::find()
            ->andFilterWhere(['in', 'deal_stage_id', $dealStageId])
            ->andFilterWhere(['deal_type_id' => $dealType ? $dealType->deal_type_id : null])
            ->orderBy(['order' => SORT_ASC])
            ->indexBy(DealStage::getIdKey())
            ->all();

        return new CommonList(array_map(fn (DealStage $dealStage): string => $dealStage->name, $items));
    }

    /**
     * @param array|null $employeeId
     * @return ListInterface
     */
    public function createEmployeeList(?array $employeeId = null): ListInterface
    {
        $query = EmployeeCompany::find()
            ->indexBy('employee_id')
            ->andWhere([EmployeeCompany::tableName() . '.company_id' => $this->user->identity->company->id])
            ->orderBy([
                EmployeeCompany::tableName() . '.lastname' => SORT_ASC,
                EmployeeCompany::tableName() . '.firstname' => SORT_ASC,
                EmployeeCompany::tableName() . '.patronymic' => SORT_ASC,
            ]);

        if ($employeeId !== null) {
            $query->andWhere(['employee_id' => $employeeId]);
        } else { // Если список идентификаторов сотрудников не передан, тогда выбираем только актуальных
            $query->joinWith('employee')->andWhere([
                EmployeeCompany::tableName() . '.is_working' => true,
                Employee::tableName() . '.is_deleted' => false,
                Employee::tableName() . '.is_active' => true,
            ]);
        }

        $employees = $query->all();
        $callback = function (EmployeeCompany $employeeCompany) {
            return $employeeCompany->getShortFio();
        };

        return new CommonList(array_map($callback, $employees));
    }

    /**
     * @return ListInterface
     * @deprecated
     */
    public function createClientList(): ListInterface
    {
        $owner_id = $this->user->can(ClientAccess::VIEW_ALL) ? null : $this->user->identity->id;
        $clients = Contractor::find()
            ->joinWith(['client'])
            ->andWhere([
                Client::tableName() . '.company_id' => $this->user->identity->company->id,
                Contractor::tableName() . '.is_deleted' => false,
            ])
            ->andFilterWhere([Contractor::tableName() . '.responsible_employee_id' => $owner_id])
            ->indexBy('id')
            ->orderBy(Contractor::tableName() . '.name')
            ->all();

        $callback = function (Contractor $contractor): string {
            return $contractor->getNameWithType();
        };

        return new CommonList(array_map($callback, $clients));
    }

    /**
     * @return ListInterface
     * @deprecated
     */
    public function createContactListById($ids = null): ListInterface
    {
        $owner_id = $this->user->can(ClientAccess::VIEW_ALL) ? null : $this->user->identity->id;
        $items = Contact::find()->alias('contact')->joinWith('contractor contractor')
            ->andWhere([
                'contractor.company_id' => $this->user->identity->company->id,
            ])
            ->andFilterWhere(['contact.contact_id' => $ids])
            ->select('contact.contact_name')
            ->indexBy('contact.contact_id')
            ->orderBy('contact.contact_name')
            ->column();

        return new CommonList($items);
    }

    /**
     * @param array|null $employeeId
     * @return ListInterface
     */
    public function createContractorList(?array $contractorId = null): ListInterface
    {
        $owner = $this->user->can(ClientAccess::VIEW_ALL) ? null : $this->user->identity;
        $query = Contractor::find()
            ->joinWith(['client'])
            ->andWhere([
                Contractor::tableName() . '.company_id' => $this->user->identity->company->id,
                Contractor::tableName() . '.is_deleted' => false,
            ])
            ->andWhere(['or',
                ['=', Contractor::tableName() . '.is_customer', true],
                ['is not', Client::tableName() . '.contractor_id', null],
            ])
            ->andFilterWhere(['in', Contractor::tableName() . '.id', $contractorId])
            ->indexBy('id')
            ->orderBy(Contractor::tableName() . '.name');

        if ($owner) {
            $on = sprintf('%s.id = %s.contractor_id', Contractor::tableName(), Task::tableName());
            $query->leftJoin(Task::tableName(), $on)->andWhere(['or',
                [Contractor::tableName() . '.responsible_employee_id' => $owner->id],
                [Task::tableName() . '.employee_id' => $owner->id],
            ]);
        }

        $callback = function (Contractor $contractor): string {
            return $contractor->getNameWithType();
        };

        return new CommonList(array_map($callback, $query->all()));
    }

    /**
     * @return ListInterface
     */
    public function createCompanyTypeList(): ListInterface
    {
        $orderBy = sprintf('id = %s DESC, id = %s DESC, name_short ASC', CompanyType::TYPE_OOO, CompanyType::TYPE_IP);
        $items = CompanyType::find()
            ->select('name_short')
            ->indexBy('id')
            ->andWhere(['!=', 'id', CompanyType::TYPE_EMPTY])
            ->orderBy(new Expression($orderBy))
            ->column();

        return new CommonList($items);
    }

    /**
     * @return ListInterface
     */
    public function createCheckingAccountantList(): ListInterface
    {
        $items = CheckingAccountant::find()
            ->select('bank_name')
            ->andWhere(['company_id' => $this->user->identity->company->id])
            ->andWhere(['not', ['type' => CheckingAccountant::TYPE_CLOSED]])
            ->indexBy('id')
            ->column();

        return new CommonList($items);
    }

    /**
     * @return ListInterface
     */
    public function createAgreementTemplateList(): ListInterface
    {
        $models = AgreementTemplate::find()
            ->andWhere([
                'company_id' => $this->user->identity->company->id,
                'status' => AgreementTemplate::STATUS_ACTIVE,
            ])
            ->indexBy('id')
            ->all();

        $items = array_map(fn(AgreementTemplate $template) => $template->getFullName(), $models);

        return new CommonList($items);
    }

    /**
     * @return ListInterface
     */
    public function createAgreementTypeList(): ListInterface
    {
        $items = AgreementType::find()
            ->select('name')
            ->indexBy('id')
            ->orderBy('sort')
            ->column();

        return new CommonList($items);
    }

    /**
     * @param Contractor $contractor
     * @return ListInterface
     * @deprecated
     */
    public function createAgreementTypeList2(Contractor $contractor): ListInterface // TODO: remove
    {
        $items = Agreement::find()
            ->joinWith('agreementType')
            ->select(AgreementType::tableName() . '.name')
            ->andWhere([
                Agreement::tableName() . '.company_id' => $contractor->company->id,
                Agreement::tableName() . '.contractor_id' => $contractor->id,
            ])
            ->andWhere(['not', ['document_type_id' => null]])
            ->indexBy('document_type_id')
            ->groupBy('document_type_id')
            ->column();

        return new CommonList($items);
    }

    /**
     * @return ListInterface
     * @throws
     */
    public function createTimeList(): ListInterface
    {
        $items = [];
        $dateBegin = (new DateTimeImmutable)->setTime(10, 0);
        $period = new DatePeriod(
            $dateBegin,
            new DateInterval('PT15M'),
            $dateBegin->add(new DateInterval('P1D'))
        );

        foreach ($period as $dateTime) {
            $time = $dateTime->format('H:i');
            $items[$time] = $time;
        }

        return new CommonList($items);
    }

    /**
     * @return ListInterface
     */
    public function createCurrencyList(): ListInterface
    {
        $items = Currency::find()
            ->select('name')
            ->indexBy('id')
            ->orderBy(['sort' => SORT_ASC])
            ->column();

        return new CommonList($items);
    }

    /**
     * @param array $contractorId
     * @return ListInterface
     */
    public function createContactList(array $contractorId): ListInterface
    {
        $items = Contact::find()
            ->select('contact_name')
            ->andWhere(['contractor_id' => $contractorId])
            ->orderBy(['contact_default' => SORT_DESC, 'contact_name' => SORT_ASC])
            ->indexBy('contact_id')
            ->column();

        return new CommonList($items);
    }
}
