<?php

namespace frontend\modules\crm\models;

use common\models\Company;
use common\models\EmployeeCompany;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * @property int $company_id
 * @property string $name
 * @property string $status
 * @property-read Company $company
 * @property-read EmployeeCompany|null $employeeCompany
 * @property-read string $created_at
 * @property-read string $updated_at
 */
abstract class AbstractType extends ActiveRecord
{
    /**
     * @var bool
     */
    public bool $canDelete = true;

    /** @var int */
    public const TYPE_STATUS_ACTIVE = 1;

    /** @var int */
    public const TYPE_STATUS_INACTIVE = 0;

    /** @var string[] */
    public const TYPE_STATUS_LIST = [
        self::TYPE_STATUS_ACTIVE => 'Используется',
        self::TYPE_STATUS_INACTIVE => 'Архив',
    ];

    /**
     * @return string
     * @deprecated
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'status' => 'Статус',
        ];
    }

    /**
     * @param Company $company
     * @return ActiveQueryInterface
     */
    public static function createListQuery(Company $company): ActiveQueryInterface
    {
        return static::find()->andWhere(['company_id' => $company->id])->orderBy('name');
    }

    /**
     * @return ActiveQuery
     */
    public function getCompany(): ActiveQuery
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @param Company $company
     * @param mixed $id
     * @return static|null
     */
    public static function findById(Company $company, $id): ?self
    {
        if (!is_scalar($id)) {
            return null;
        }

        return static::findOne(['company_id' => $company->id, static::getIdKey() => $id]);
    }

    /**
     * @param Company $company
     * @return ActiveRecord|static|null
     */
    public static function findFirst(Company $company): ?self
    {
        return static::find()
            ->andWhere(['company_id' => $company->id, 'status' => self::TYPE_STATUS_ACTIVE])
            ->orderBy('name')
            ->one();
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return (int) $this->status;
    }

    /**
     * @return string
     */
    public function getStatusLabel(): string
    {
        return static::TYPE_STATUS_LIST[$this->getStatus()];
    }

    /**
     * @return ActiveQuery
     */
    public function getEmployeeCompany(): ActiveQuery
    {
        return $this->hasOne(EmployeeCompany::class, ['employee_id' => 'employee_id', 'company_id' => 'company_id']);
    }

    /**
     * @return string
     */
    public static function getIdKey(): string
    {
        return static::primaryKey()[0];
    }

    /**
     * @return bool
     */
    public static function isCreatable(): bool
    {
        return true;
    }

    /**
     * @param int $company_id
     * @return array
     */
    abstract public static function getDefaultRows(int $company_id): array;

    /**
     * @return string
     */
    abstract public function getTypeName(): string;
}
