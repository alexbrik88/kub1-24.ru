<?php

namespace frontend\modules\crm\models;

use frontend\components\WebUser;
use Yii;
use yii\web\User;

final class WebUserFactory
{
    /**
     * @return User|WebUser
     * @throws
     */
    public function createUser(): object
    {
        return Yii::createObject(User::class);
    }
}
