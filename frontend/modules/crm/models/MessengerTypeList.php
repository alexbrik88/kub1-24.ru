<?php

namespace frontend\modules\crm\models;

use common\models\ListInterface;

final class MessengerTypeList implements ListInterface
{
    /** @var int */
    public const MESSENGER_TYPE_TELEGRAM = 1;

    /** @var int */
    public const MESSENGER_TYPE_VIBER = 2;

    /** @var int */
    public const MESSENGER_TYPE_WHATSAPP = 3;

    /**
     * @@inheritDoc
     */
    public function getItems(): array
    {
        return [
            self::MESSENGER_TYPE_TELEGRAM => 'Telegram',
            self::MESSENGER_TYPE_VIBER => 'Viber',
            self::MESSENGER_TYPE_WHATSAPP => 'WhatsApp',
        ];
    }
}
