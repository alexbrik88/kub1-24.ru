<?php

namespace frontend\modules\crm\models;

use common\models\Company;
use common\models\EmployeeCompany;
use yii\base\Model;

final class TaskSelectForm extends Model
{
    /**
     * @var mixed
     */
    public $task_id;

    /**
     * @var mixed
     */
    public $employee_id;

    /**
     * @var mixed
     */
    public $status;

    /**
     * @var bool
     */
    public bool $canChangeEmployee;

    /**
     * @var Company
     */
    private Company $company;

    /**
     * @var TaskFilter
     */
    private TaskFilter $filter;

    /**
     * @param Company $company
     * @param TaskFilter $filter
     * @param bool $canChangeEmployee
     */
    public function __construct(Company $company, TaskFilter $filter, bool $canChangeEmployee)
    {
        $this->company = $company;
        $this->filter = $filter;
        $this->filter->status = null;
        $this->canChangeEmployee = $canChangeEmployee;

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels(): array
    {
        return [
            'employee_id' => 'Ответственный',
            'status' => 'Статус',
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['task_id'], 'required'],
            [['task_id'], 'each', 'rule' => ['integer']],
            [['employee_id'], 'integer'],
            [
                ['employee_id'],
                'exist',
                'targetClass' => EmployeeCompany::class,
                'targetAttribute' => 'employee_id',
                'filter' => [
                    'company_id' => $this->company->id,
                    'is_working' => true,
                ],
            ],
            [['status'], 'integer'],
            [['status'], 'in', 'range' => array_keys(Task::STATUS_LIST)],
        ];
    }

    /**
     * @return bool
     */
    public function updateModels(): bool
    {
        /** @var Task[] $tasks */
        $tasks = $this->filter->createQuery()->andWhere(['task_id' => $this->task_id])->all();

        if (empty($tasks)) {
            return false;
        }

        foreach ($tasks as $task) {
            $task->status = $this->status ?: $task->status;
            $task->employee_id = ($this->employee_id && $this->canChangeEmployee)
                ? $this->employee_id
                : $task->employee_id;
            if ($task->status == Task::STATUS_COMPLETED && empty($task->result)) {
                $task->result = 'Нет данных';
            }

            $task->save(false);
        }

        Task::updateStatuses();

        return true;
    }
}
