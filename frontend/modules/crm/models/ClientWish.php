<?php

namespace frontend\modules\crm\models;

use Yii;
use common\models\EmployeeCompany;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "crm_client_wish".
 *
 * @property int $id
 * @property int $company_id
 * @property int $contractor_id
 * @property int $number
 * @property int $responsible_employee_id
 * @property int $status_id
 * @property string $date
 * @property string $text
 */
class ClientWish extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_DONE = 2;
    const STATUS_REJECTED = 3;

    public static $statuses = [
        self::STATUS_ACTIVE => 'В работе',
        self::STATUS_DONE => 'Сделано',
        self::STATUS_REJECTED => 'Не сделаем',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_client_wish';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contractor_id' => 'Клиент',
            'number' => '№№',
            'responsible_employee_id' => 'Ответственный',
            'status_id' => 'Статус',
            'date' => 'Дата добавления',
            'text' => 'Описание',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getEmployeeCompany(): ActiveQuery
    {
        return $this->hasOne(EmployeeCompany::class, [
            'employee_id' => 'responsible_employee_id',
            'company_id' => 'company_id',
        ]);
    }

    public function getStatusName() : string
    {
        return self::$statuses[$this->status_id] ?? '';
    }
}
