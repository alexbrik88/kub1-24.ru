<?php

namespace frontend\modules\crm\models;

use common\models\ListInterface;

final class CommonList implements ListInterface
{
    /**
     * @var string[]
     */
    private array $items;

    /**
     * @param array $items
     */
    public function __construct(array $items)
    {
        $this->items = $items;
    }

    /**
     * @inheritDoc
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
