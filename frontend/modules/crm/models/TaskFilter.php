<?php

namespace frontend\modules\crm\models;

use common\db\SortBuilder;
use common\models\Contractor;
use common\models\Company;
use common\models\employee\Employee;
use common\models\FilterInterface;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\ActiveQuery;

final class TaskFilter extends Model implements FilterInterface
{
    /** @var int В работе + Просрочена */
    public const STATUS_ACTIVE = -1;

    /** @var string[] */
    public const STATUS_LIST = [
        self::STATUS_ACTIVE => 'В работе + Просрочена',
        Task::STATUS_IN_PROGRESS => 'В работе',
        Task::STATUS_EXPIRED => 'Просрочена',
        Task::STATUS_COMPLETED => 'Завершена',
    ];

    /**
     * @var mixed
     */
    public $contractor_id;

    /**
     * @var int
     */
    public $event_type;

    /**
     * @var int
     */
    public $employee_id;

    /**
     * @var int
     */
    public $contact_id;

    /**
     * @var int
     */
    public $status;

    /**
     * @var string
     */
    public $search;

    /**
     * @var Company
     */
    private Company $company;

    /**
     * @var Employee|null
     */
    private ?Employee $owner;

    /**
     * @var Contractor|null
     */
    private ?Contractor $contractor;

    /**
     * @var TaskListFactory
     */
    private TaskListFactory $listFactory;

    /**
     * @var int
     */
    private int $pageSize;

    /**
     * @param Company $company
     * @param Employee|null $owner
     * @param Contractor|null $contractor
     * @param int|null $pageSize
     */
    public function __construct(
        Company $company,
        ?Employee $owner,
        ?Contractor $contractor = null,
        ?int $pageSize = null
    ) {
        $this->company = $company;
        $this->owner = $owner;
        $this->contractor = $contractor;
        $this->listFactory = new TaskListFactory($this);
        $this->pageSize = $pageSize ?: self::DEFAULT_PAGE_SIZE;

        parent::__construct();
    }

    /**
     * @return ActiveQuery
     */
    public function createQuery(): ActiveQuery
    {
        $status = ($this->status == self::STATUS_ACTIVE)
            ? [Task::STATUS_IN_PROGRESS, Task::STATUS_EXPIRED] // В работе + Просрочена
            : $this->status;

        $query = Task::find()->andWhere([
            Task::tableName() . '.company_id' => $this->company->id,
        ])->andWhere([
            'not', [Task::tableName() . '.status' => Task::STATUS_EMPTY],
        ])->andFilterWhere([
            Task::tableName() . '.employee_id' => $this->owner ? $this->owner->id : $this->employee_id,
            Task::tableName() . '.contractor_id' => $this->contractor ? $this->contractor->id : $this->contractor_id,
            Task::tableName() . '.contact_id' => $this->contact_id,
            Task::tableName() . '.event_type' => $this->event_type,
            Task::tableName() . '.status' => $status,
        ]);

        if (strlen($this->search)) {
            $query->andWhere(['like', Task::tableName() . '.description', $this->search]);
        }

        return $query;
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels(): array
    {
        return [
            'date' => 'Дата',
            'event_type' => 'Событие',
            'employee_id' => 'Ответственный',
            'description' => 'Краткое описание',
            'time' => 'Время',
            'status' => 'Статус',
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['event_type'], 'integer'],
            [['employee_id'], 'integer'],
            [['contractor_id'], 'integer'],
            [['contact_id'], 'integer'],
            [['status'], 'integer'],
            [['search'], 'string', 'max' => 64],
        ];
    }

    /**
     * @return TaskListFactory
     */
    public function getListFactory(): TaskListFactory
    {
        return $this->listFactory;
    }

    /**
     * @inheritDoc
     */
    public function getDataProvider(): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'sort' => $this->getSort(),
            'query' => $this->createQuery()->with([
                'contractor',
                'employeeCompany',
            ]),
            'pagination' => [
                'pageSize' => $this->pageSize,
            ],
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        $builder = new SortBuilder();
        $builder->addOrder('task_number', Task::tableName());
        $builder->addOrder('date', Task::tableName(), 'datetime');
        $builder->addOrders(['task_number', 'datetime'], Task::tableName(), 'task_id');
        $builder->addDefaultOrder('date', SORT_DESC);

        return $builder->buildSort();
    }

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
        $filter = new self($this->company, $this->owner, $this->contractor);

        return $filter->createQuery()->count();
    }
}
