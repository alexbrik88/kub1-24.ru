<?php

namespace frontend\modules\crm\models;

use common\models\Contractor;
use common\db\ExpressionFactory;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property-read int $contact_id
 * @property int|null $contractor_id
 * @property int|null $client_position_id
 * @property boolean $contact_default
 * @property string $contact_name
 * @property string $email_address
 * @property string $skype_account
 * @property string $social_account
 * @property string $primary_phone_number
 * @property string $secondary_phone_number
 * @property int $primary_messenger_type
 * @property int $secondary_messenger_type
 * @property-read string $created_at
 * @property-read string $updated_at
 * @property-read ClientPosition|null $position
 * @property-read string $positionName
 */
final class Contact extends ActiveRecord
{
    /** @var int */
    public const DEFAULT_CONTACT_ID = -1;

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => (new ExpressionFactory)->createNowExpression(),
            ],
        ];
    }

    /**
     * @param Contractor $contractor
     * @return static[]
     */
    public static function findByContractor(Contractor $contractor): array
    {
        return self::find()->andWhere(['contractor_id' => $contractor->id])->all();
    }

    /**
     * @param Contractor $contractor
     * @param mixed $contact_id
     * @return static|null
     */
    public static function findById(Contractor $contractor, $contact_id): ?self
    {
        if (!is_scalar($contact_id)) {
            return null;
        }

        return Contact::findOne(['contractor_id' => $contractor->id, 'contact_id' => $contact_id]);
    }

    /**
     * @return ActiveQuery
     */
    public function getContractor(): ActiveQuery
    {
        return $this->hasOne(Contractor::class, ['id' => 'contractor_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPosition(): ActiveQuery
    {
        return $this->hasOne(ClientPosition::class, ['client_position_id' => 'client_position_id']);
    }

    /**
     * @return string
     */
    public function getPositionName(): string
    {
        return $this->position->name ?? '';
    }

    /**
     * @param ClientPosition|null $clientPosition
     * @return void
     */
    public function setPosition(?ClientPosition $clientPosition): void
    {
        $this->setAttribute('client_position_id', $clientPosition ? $clientPosition->client_position_id : null);
        $this->populateRelation('position', $clientPosition);
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        if ($this->client_position_id ||
            $this->contact_name ||
            $this->contact_default ||
            $this->email_address ||
            $this->skype_account ||
            $this->social_account ||
            $this->primary_phone_number ||
            $this->primary_messenger_type ||
            $this->secondary_phone_number ||
            $this->secondary_messenger_type) {
            return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%crm_contact}}';
    }

    /**
     * @param Contractor $contractor
     * @return static
     */
    public static function createDefaultContact(Contractor $contractor): self
    {
        return new Contact([
            'contact_name' => $contractor->getRealContactName(),
            'email_address' => $contractor->getRealContactEmail(),
            'primary_phone_number' => $contractor->getRealContactPhone(),
        ]);
    }
}
