<?php

namespace frontend\modules\crm\models;

use common\models\Agreement;
use common\models\AgreementTemplate;
use common\models\AgreementType;
use common\models\ListInterface;

final class AgreementListFactory
{
    /**
     * @var AgreementFilter
     */
    private AgreementFilter $filter;

    /**
     * @param AgreementFilter $filter
     */
    public function __construct(AgreementFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return ListInterface
     */
    public function createAgreementTypeList(): ListInterface
    {
        $items = $this
            ->filter
            ->createQuery()
            ->select('name')
            ->joinWith('agreementType')
            ->indexBy('document_type_id')
            ->orderBy(AgreementType::tableName() . '.sort')
            ->column();

        return new CommonList($items);
    }

    /**
     * @return ListInterface
     */
    public function createAgreementTemplateList(): ListInterface
    {
        $models = $this
            ->filter
            ->createQuery()
            ->joinWith('template')
            ->andWhere([AgreementTemplate::tableName() . '.status' => AgreementTemplate::STATUS_ACTIVE])
            ->indexBy('agreement_template_id')
            ->all();

        $items = array_map(fn(Agreement $agreement) => $agreement->template->getFullName(), $models);

        return new CommonList($items);
    }

    /**
     * @return ListInterface
     */
    public function createEmployeeList(): ListInterface
    {
        $id = $this->filter
            ->createQuery()
            ->select('created_by')
            ->groupBy('created_by')
            ->andWhere(['not', ['created_by' => null]])
            ->column();

        return (new ListFactory)->createEmployeeList($id);
    }
}
