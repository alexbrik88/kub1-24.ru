<?php

namespace frontend\modules\crm\models;

use common\models\Contractor;
use common\components\date\DateHelper;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use DateTime;
use DateInterval;
use yii\base\Model;

final class TaskForm extends Model
{
    /** @var string */
    public const SCENARIO_CREATE = 'create';

    /** @var string */
    public const SCENARIO_UPDATE = 'update';

    /** @var int */
    public const COMPLETED_RESET = -1;

    /** @var int */
    public const COMPLETED_NO_CHANGE = 0; // Сохранить

    /** @var int */
    public const COMPLETED_CHANGE = 1; // Завершить

    /** @var int */
    public const COMPLETED_CHANGE_ADDITIONAL = 2; // Завершить и добавить новую

    /** @var int[] */
    private const COMPLETED_LIST = [
        self::COMPLETED_RESET,
        self::COMPLETED_NO_CHANGE,
        self::COMPLETED_CHANGE,
        self::COMPLETED_CHANGE_ADDITIONAL,
    ];

    /** @var string */
    private const TIME_FORMAT = 'H:i';

    /**
     * @var int
     */
    public $task_number;

    /**
     * @var int
     */
    public $contractor_id;

    /**
     * @var int
     */
    public $event_type;

    /**
     * @var int
     */
    public $employee_id;

    /**
     * @var int
     */
    public $status = Task::STATUS_IN_PROGRESS;

    /**
     * @var string
     */
    public $description;

    /**
     * @var string
     */
    public $date;

    /**
     * @var string
     */
    public $time;

    /**
     * @var string
     */
    public $result;

    /**
     * @var string
     */
    public $task_result_id;

    /**
     * @var array
     */
    public $checklist = [];

    /**
     * @var bool Значение кнопки завершить
     */
    public $completed = self::COMPLETED_NO_CHANGE;

    /**
     * @var int
     */
    public $last_log_id;

    /**
     * @var int
     */
    public $contact_id;

    /**
     * @var Contractor|null
     */
    public ?Contractor $contractor;

    /**
     * @var bool
     */
    public bool $canChangeEmployee;

    /**
     * @var Task
     */
    private Task $task;

    /**
     * @var Employee
     */
    private Employee $employee;

    /**
     * @param Employee $employee
     * @param Task $task
     * @param Contractor|null $contractor
     * @param bool $canChangeEmployee
     * @param string $scenario
     */
    public function __construct(Employee $employee, Task $task, ?Contractor $contractor, bool $canChangeEmployee, string $scenario)
    {
        $this->employee = $employee;
        $this->contractor = $contractor;
        $this->task = $task;
        $this->canChangeEmployee = $canChangeEmployee;
        $this->scenario = $scenario;
        $this->employee_id = $employee->id;
        $this->contractor_id = $contractor ? $contractor->id : null;

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();

        $this->task_number = Task::getNextNumber($this->employee->company);
        $dateTime = $this->newDateTime();
        $this->date = $dateTime->format(DateHelper::FORMAT_USER_DATE);
        $this->time = $dateTime->format(self::TIME_FORMAT);

        if ($this->scenario == self::SCENARIO_UPDATE) {
            $attributes = $this->task->getAttributes();
            $attributes['date'] = DateHelper::format($attributes['datetime'], DateHelper::FORMAT_USER_DATE);
            $attributes['time'] = DateHelper::format($attributes['datetime'], self::TIME_FORMAT);
            $attributes['status'] = $this->task->status;
            $attributes['checklist'] = $this->task->checklist;
            $attributes['task_result_id'] = $this->task->task_result_id;
            $this->setAttributes($attributes, false);
        }
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'task_number' => 'Номер',
            'contractor_id' => 'Клиент',
            'event_type' => 'Событие',
            'employee_id' => 'Ответственный',
            'status' => 'Статус задачи',
            'description' => 'Описание задачи',
            'date' => 'Дата',
            'time' => 'Время',
            'result' => 'Результат по задаче',
            'last_log_id' => 'История действий',
            'contact_id' => 'Контакт',
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['task_number'], 'required'],
            [['task_number'], 'integer', 'min' => 1],

            [['contractor_id'], 'required'],
            [['contractor_id'], 'integer'],
            [
                ['contractor_id'],
                'exist',
                'targetClass' => Contractor::class,
                'targetAttribute' => 'id',
                'filter' => ['company_id' => $this->employee->company->id],
            ],
            [
                ['task_result_id'],
                'exist',
                'targetClass' => TaskResult::class,
                'targetAttribute' => 'task_result_id',
                'filter' => ['company_id' => $this->employee->company->id],
            ],

            [['employee_id'], 'integer'],
            [
                ['employee_id'],
                'exist',
                'targetClass' => EmployeeCompany::class,
                'targetAttribute' => 'employee_id',
                'filter' => [
                    'company_id' => $this->employee->company->id,
                    'is_working' => true,
                ],
            ],

            [['event_type'], 'required'],
            [['event_type'], 'integer'],
            [['event_type'], 'in', 'range' => array_keys((new TaskEventList)->getItems())],

            [['date'], 'required'],
            [['date'], 'date', 'format' => 'php:' . DateHelper::FORMAT_USER_DATE],

            [['time'], 'required'],
            [['time'], 'date', 'format' => 'php:H:i'],

            [['description'], 'required'],
            [['description'], 'string'],

            [['completed'], 'required'],
            [['completed'], 'integer'],
            [['completed'], 'in', 'range' => self::COMPLETED_LIST],

            [['result'], 'trim'],
            [['result'], 'string'],
            [
                ['result'], 'required',
                'when' => function($model) {
                    return in_array($model->completed, [
                        self::COMPLETED_CHANGE,
                        self::COMPLETED_CHANGE_ADDITIONAL,
                    ]);
                },
                'whenClient' => vsprintf('function (attr, val) {return val == "%u" || val == "%u";}', [
                    self::COMPLETED_CHANGE,
                    self::COMPLETED_CHANGE_ADDITIONAL,
                ]),
            ],

            [['checklist'], 'filter', 'filter' => function ($value) {
                $newValue = [];
                foreach ((array) $value as $item) {
                    if (isset($item['checked'], $item['text'])) {
                        $text = trim($item['text']);
                        if ($text) {
                            $newValue[] = [
                                'checked' => intval((bool) $item['checked']),
                                'text' => (string) $text,
                            ];
                        }
                    }
                }

                return $newValue;
            }],

            [['contact_id'], 'integer'],
            ['contact_id', 'in', 'range' => function($model, $attribute) {
                $range = [];
                if (!$model->hasErrors('contractor_id') && ($contractor = Contractor::findOne($model->contractor_id)) !== null) {
                    $range = array_keys($contractor->getAllContactsData());
                }

                return $range;
            }],
        ];
    }

    /**
     * @return bool
     */
    public function updateTask(): bool
    {
        $attributes = $this->getAttributes($this->activeAttributes());
        $attributes['company_id'] = $this->employee->company->id;
        $attributes['employee_id'] = $this->canChangeEmployee ? $attributes['employee_id'] : $this->employee->id;
        $attributes['datetime'] = DateHelper::format(
            "{$this->date} {$this->time}:00",
            DateHelper::FORMAT_DATETIME,
            DateHelper::FORMAT_USER_DATETIME
        );
        $attributes['status'] = $this->getStatus();
        $this->task->setAttributes($attributes, false);
        $this->task->checklist = $attributes['checklist'];
        $this->task->task_result_id = $attributes['task_result_id'] ?? null;
        if ($this->scenario == self::SCENARIO_CREATE) {
            $this->task->created_at = new \yii\db\Expression('NOW()');
            $this->task->created_at = new \yii\db\Expression('NOW()');
        }

        if ($this->task->save(false)) {
            Task::updateStatuses();
            if ($contractor = $this->task->contractor) {
                TaskComment::updateAll([
                    'contractor_id' => $contractor->id,
                ], [
                    'and',
                    ['crm_task_id' => $this->task->task_id],
                    [
                        'or',
                        ['contractor_id' => null],
                        ['not', ['contractor_id' => $contractor->id]],
                    ],
                ]);
            }

            return true;
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function scenarios(): array
    {
        return [
            self::SCENARIO_CREATE => [
                'task_number',
                'contractor_id',
                'employee_id',
                'event_type',
                'description',
                'date',
                'time',
                'checklist',
                'contact_id',
                'completed',
                'result',
                'task_result_id',
            ],
            self::SCENARIO_UPDATE => [
                'task_number',
                'contractor_id',
                'employee_id',
                'event_type',
                'description',
                'date',
                'time',
                'checklist',
                'contact_id',
                'completed',
                'result',
                'task_result_id',
            ],
        ];
    }

    /**
     * @return int
     */
    private function getStatus(): int
    {
        $pair = [
            self::COMPLETED_RESET => Task::STATUS_IN_PROGRESS,
            self::COMPLETED_NO_CHANGE => $this->status,
            self::COMPLETED_CHANGE => Task::STATUS_COMPLETED,
            self::COMPLETED_CHANGE_ADDITIONAL => Task::STATUS_COMPLETED,
        ];

        return $pair[$this->completed];
    }

    /**
     * @return DateTime
     */
    private function createDateTime(): DateTime
    {
        $dateTime = new DateTime;

        foreach ([45, 30, 15, 0] as $minutes) {
            if ((int) $dateTime->format('i') >= $minutes) {
                $dateTime->setTime($dateTime->format('G'), $minutes, 0, 0);
                $dateTime->add(new DateInterval('PT15M'));

                break;
            }
        }

        return $dateTime;
    }

    /**
     * @return DateTime
     */
    private function newDateTime(): DateTime
    {
        $dateTime = date_create('noon');
        $now = date_create();
        if ($now > $dateTime) {
            $dateTime->modify('+1 days');
        }

        while (
            Task::find()->andWhere([
                'employee_id' => $this->employee->id,
                'company_id' => $this->employee->company->id,
                'datetime' => $dateTime->format('Y-m-d H:i:s')
            ])->exists()
        ) {
            $dateTime->modify('+1 hour');
        }

        return $dateTime;
    }

    /**
     * @return array
     */
    public function contactDropdownData(): array
    {
        $data = [];
        $options = [];
        if ($this->contractor_id) {
            $contractor = $this->employee->company->getContractors()->andWhere([
                'id' => $this->contractor_id,
            ])->one();
            if ($contractor !== null) {
                $contactsData = $contractor->getAllContactsData();
                foreach ($contactsData as $id => $contact) {
                    $data[$contact['id']] = $contact['name'];
                    $options[$contact['id']] = [
                        'data-phone' => $contact['phone'] ?: '---',
                        'data-email' => $contact['email'] ?: '---',
                    ];
                }
            }
        }

        return [
            'data' => $data,
            'options' => $options,
        ];
    }

    /**
     * @return array
     */
    public function getResultItems(): array
    {
        return TaskResult::find()->andWhere([
            'company_id' => $this->employee->company->id,
        ])->andFilterWhere([
            'or',
            ['status' => AbstractType::TYPE_STATUS_ACTIVE],
            ['status' => $this->task_result_id],
        ])->orderBy([
            'name' => SORT_ASC,
        ])->all();
    }

    /**
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }
}
