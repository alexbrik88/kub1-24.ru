<?php

namespace frontend\modules\crm\models;

/**
 * @property bool $checked
 * @property string $text
 */
final class TaskChecklistItem extends \yii\base\BaseObject
{
    private bool $_checked;
    private string $_text;

    public function setChecked($value) : void
    {
        $this->_checked = (bool) $value;
    }

    public function getChecked() : bool
    {
        return $this->_checked;
    }

    public function setText($value) : void
    {
        $this->_checked = (string) $value;
    }

    public function getText() : string
    {
        return $this->_checked;
    }
}
