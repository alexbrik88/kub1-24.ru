<?php

namespace frontend\modules\crm\models;

use common\components\date\DateHelper;
use common\models\Company;
use common\models\EmployeeCompany;
use common\models\Contractor;
use frontend\modules\crm\behaviors\ActualTaskBehavior;
use frontend\modules\crm\behaviors\AttributeLogBehavior;
use frontend\modules\crm\behaviors\TaskCompletedBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\Json;

/**
 * @property-read int $task_id Идентификатор задачи
 * @property int $task_number Номер задачи
 * @property int $contractor_id Идентификатор клиента
 * @property int $company_id Идентификатор компании
 * @property int $employee_id Идентификатор ответственного сотрудника
 * @property int $event_type
 * @property int $status Статус задачи
 * @property string $description Описание задачи
 * @property string $datetime Дата
 * @property string $completed_at Дата завершения
 * @property string $result Результат
 * @property array $checklist Чек-лист
 * @property array $contact_id Идентификатор контакта
 * @property-read string $created_at
 * @property-read string $updated_at
 * @property-read string $statusName Имя название статуса
 * @property-read string $eventName Событие
 * @property-read EmployeeCompany $employeeCompany
 * @property-read Contractor $contractor
 * @property-read Contact[] $contacts
 * @property-read Contact[] $defaultContacts
 */
final class Task extends ActiveRecord
{
    /** @var int */
    public const STATUS_EMPTY = 0;

    /** @var int */
    public const STATUS_IN_PROGRESS = 1;

    /** @var int */
    public const STATUS_EXPIRED = 2;

    /** @var int */
    public const STATUS_COMPLETED = 3;

    /** @var string[] */
    public const STATUS_LIST = [
        self::STATUS_IN_PROGRESS => 'В работе',
        self::STATUS_EXPIRED => 'Просрочена',
        self::STATUS_COMPLETED => 'Завершена',
    ];

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
            'log' => [
                'class' => AttributeLogBehavior::class,
            ],
            'actualTask' => [
                'class' => ActualTaskBehavior::class,
            ],
            'taskCompleted' => [
                'class' => TaskCompletedBehavior::class,
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        TaskComment::deleteAll(['crm_task_id' => $this->task_id]);

        return true;
    }

    /**
     * @param Company $company
     * @param int|mixed $taskId
     * @return static|null
     */
    public static function createNewEmpty(Company $company, ?Contractor $contractor = null): self
    {
        $model = new self;
        $model->status = self::STATUS_EMPTY;
        $model->company_id = $company->id;
        $model->contractor_id = $contractor ? $contractor->id : null;
        $model->save(false);

        return $model;
    }

    /**
     * @param Company $company
     * @param int|mixed $taskId
     * @return static|null
     */
    public static function findById(Company $company, $taskId): ?self
    {
        if (!is_scalar($taskId)) {
            return null;
        }

        return self::findOne(['company_id' => $company->id, 'task_id' => $taskId]);
    }

    /**
     * @param Contractor $contractor
     * @param int|null $status
     * @return static[]
     */
    public static function findByContractor(Contractor $contractor, ?int $status = null): array
    {
        return self::find()
            ->andWhere(['contractor_id' => $contractor->id])
            ->andFilterWhere(['status' => $status])
            ->all();
    }

    /**
     * @param Contractor $contractor
     * @return ActiveRecord|static
     */
    public static function findActual(Contractor $contractor): ?self
    {
        return self::find()
            ->andWhere(['contractor_id' => $contractor->id])
            ->andFilterWhere(['!=', 'status', self::STATUS_COMPLETED])
            ->orderBy('datetime')
            ->one();
    }

    /**
     * @param Company $company
     * @return int
     */
    public static function getNextNumber(Company $company): int
    {
        $number = self::find()->andWhere(['company_id' => $company->id])->max('task_number') ?: 0;
        $number++;

        return $number;
    }

    /**
     * @return string
     */
    public function getEventName(): string
    {
        return (new TaskEventList)->getItems()[$this->event_type] ?? '';
    }

    /**
     * @return ActiveQuery
     */
    public function getComments(): ActiveQuery
    {
        return $this->hasMany(TaskComment::class, ['crm_task_id' => 'task_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getContractor(): ActiveQuery
    {
        return $this->hasOne(Contractor::class, ['id' => 'contractor_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getContacts(): ActiveQuery
    {
        return $this->hasMany(Contact::class, ['contractor_id' => 'contractor_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDefaultContacts(): ActiveQuery
    {
        return $this->getContacts()->andOnCondition(['contact_default' => 1]);
    }

    /**
     * @return ActiveQuery
     */
    public function getEmployeeCompany(): ActiveQuery
    {
        return $this->hasOne(EmployeeCompany::class, ['company_id' => 'company_id', 'employee_id' => 'employee_id']);
    }

    /**
     * Получить название статуса
     *
     * @return string
     */
    public function getStatusName(): ?string
    {
        return self::STATUS_LIST[$this->status] ?? null;
    }

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%crm_task}}';
    }

    /**
     * @return void
     */
    public static function updateStatuses(): void
    {
        self::updateAll(
            [
                'status' => self::STATUS_EXPIRED,
            ],
            [
                'and',
                ['=', 'status', self::STATUS_IN_PROGRESS],
                ['<', 'datetime', date(DateHelper::FORMAT_DATETIME)],
            ],
        );

        self::updateAll(
            [
                'status' => self::STATUS_IN_PROGRESS,
            ],
            [
                'and',
                ['=', 'status', self::STATUS_EXPIRED],
                ['>', 'datetime', date(DateHelper::FORMAT_DATETIME)],
            ],
        );
    }

    private ?array $_checklist = null;

    /**
     * @return array
     */
    public function getChecklist(): array
    {
        if ($this->_checklist === null) {
            $this->_checklist = [];
            $checklist = (array) Json::decode($this->__checklist);
            foreach ($checklist as $item) {
                if (isset($item['checked'], $item['text'])) {
                    $this->_checklist[] = [
                        'checked' => intval((bool) $item['checked']),
                        'text' => (string) $item['text'],
                    ];
                }
            }
        }

        return $this->_checklist;
    }

    public function setChecklist(?array $value = null): void
    {
        $this->_checklist = [];
        $this->__checklist = null;

        if (!empty($value)) {
            foreach ($value as $item) {
                if (isset($item['checked'], $item['text'])) {
                    $this->_checklist[] = [
                        'checked' => intval((bool) $item['checked']),
                        'text' => (string) $item['text'],
                    ];
                }
            }
            if (!empty($this->_checklist)) {
                $this->__checklist = Json::encode($this->_checklist);
            }
        }
    }

    public function getAllContactsData(): array
    {
        if ($contractor = $this->contractor) {
            return $contractor->getAllContactsData();
        }

        return [];
    }

    public function getContactData(): array
    {
        return $this->getAllContactsData()[$this->contact_id] ?? [];
    }
}
