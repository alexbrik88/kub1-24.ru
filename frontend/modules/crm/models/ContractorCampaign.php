<?php

namespace frontend\modules\crm\models;

use common\models\Contractor;
use frontend\modules\crm\behaviors\CanDeleteBehavior;

/**
 * Тип источника контрагента
 *
 * @property-read int $id
 */
final class ContractorCampaign extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['canDelete'] = [
            'class' => CanDeleteBehavior::class,
            'modelClass' => Contractor::class,
            'modelAttribute' => 'campaign_id',
        ];

        return $behaviors;
    }

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%contractor_campaign}}';
    }

    /**
     * @inheritDoc
     */
    public static function getDefaultRows(int $company_id): array
    {
        return [
            ['company_id' => $company_id, 'name' => 'Яндекс.Директ'],
            ['company_id' => $company_id, 'name' => 'Google.AdWords'],
            ['company_id' => $company_id, 'name' => 'Facebook'],
            ['company_id' => $company_id, 'name' => 'Instagram'],
            ['company_id' => $company_id, 'name' => 'ВКонтакте'],
            ['company_id' => $company_id, 'name' => 'Рекомендация партнера'],
            ['company_id' => $company_id, 'name' => 'Рекомендация клиента'],
            ['company_id' => $company_id, 'name' => 'Реклама'],
            ['company_id' => $company_id, 'name' => 'Холодный звонок'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'Источник';
    }
}
