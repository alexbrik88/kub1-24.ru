<?php

namespace frontend\modules\crm\models;

use common\db\SortBuilder;
use common\models\Company;
use common\models\FilterInterface;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\data\Sort;
use yii\db\ActiveQuery;

final class TypeFilter extends Model implements FilterInterface
{
    /**
     * @var mixed
     */
    public $employee_id;

    /**
     * @var mixed
     */
    public $status;

    /**
     * @var Company
     */
    private Company $company;

    /**
     * @var string|AbstractType
     */
    private string $modelClass;

    /**
     * @var TypeListFactory
     */
    private TypeListFactory $listFactory;

    /**
     * @param Company $company
     * @param string $modelClass
     */
    public function __construct(Company $company, string $modelClass)
    {
        $this->company = $company;
        $this->modelClass = $modelClass;
        $this->listFactory = new TypeListFactory($this);

        parent::__construct();
    }

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!is_a($this->modelClass, AbstractType::class, true)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['employee_id'], 'integer'],
            [['status'], 'boolean'],
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function createQuery(): ActiveQuery
    {
        return $this
            ->modelClass::find()
            ->andFilterWhere([
                'company_id' => $this->company->id,
                'employee_id' => $this->employee_id,
                'status' => $this->status,
            ]);
    }

    /**
     * @inheritDoc
     */
    public function getDataProvider(): DataProviderInterface
    {
        return new ActiveDataProvider([
            'query' => $this->createQuery(),
            'sort' => $this->getSort(),
            'pagination' => false,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        $builder = new SortBuilder();
        $builder->addOrder('id', $this->modelClass::tableName(), $this->modelClass::getIdKey());
        $builder->addOrder('name', $this->modelClass::tableName());
        $builder->addOrder('created_at', $this->modelClass::tableName());
        $builder->addDefaultOrder('name');

        return $builder->buildSort();
    }

    /**
     * @return TypeListFactory
     */
    public function getListFactory(): TypeListFactory
    {
        return $this->listFactory;
    }

    /**
     * @return string
     */
    public function getModelClass(): string
    {
        return $this->modelClass;
    }
}
