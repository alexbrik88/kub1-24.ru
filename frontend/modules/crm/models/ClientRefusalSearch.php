<?php

namespace frontend\modules\crm\models;

use common\models\Contractor;
use common\models\EmployeeCompany;
use frontend\modules\crm\models\ClientRefusal;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * ClientRefusalSearch represents the model behind the search form of `frontend\modules\crm\models\ClientRefusal`.
 */
class ClientRefusalSearch extends ClientRefusal
{
    const SCENARIO_CLIENT = 'client';

    public $period;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'refusal_id', 'employee_id', 'contractor_id'], 'integer'],
            [['created_at', 'details'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => [
                'id',
                'refusal_id',
                'employee_id',
                'contractor_id',
                'created_at',
                'details',
            ],
            self::SCENARIO_CLIENT => [
                'id',
                'refusal_id',
                'employee_id',
                'created_at',
                'details',
            ],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientRefusal::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');

            return $dataProvider;
        }

        $this->querySetCondition($query);

        return $dataProvider;
    }

    /**
     * @param $type
     * @return array
     */
    public function querySetCondition($query)
    {
        $query->andWhere([
            'crm_client_refusal.company_id' => $this->company_id,
        ]);
        if ($this->period) {
            $query->andWhere([
                'between',
                'crm_client_refusal.created_at',
                $this->period['from'].' 00:00:00',
                $this->period['to'].' 23:59:59',
            ]);
        }

        $query->andFilterWhere([
            'crm_client_refusal.id' => $this->id,
            'crm_client_refusal.contractor_id' => $this->contractor_id,
            'crm_client_refusal.refusal_id' => $this->refusal_id,
            'crm_client_refusal.employee_id' => $this->employee_id,
        ]);

        $query->andFilterWhere(['like', 'details', $this->details]);
    }

    /**
     * @param $type
     * @return array
     */
    public function getRefusalItems()
    {
        $query = Refusal::find()->alias('crm_refusal')
            ->innerJoin([
                'crm_client_refusal' => ClientRefusal::tableName(),
            ], '{{crm_client_refusal}}.[[refusal_id]] = {{crm_refusal}}.[[refusal_id]]')
            ->select(['crm_refusal.name'])
            ->andWhere([
                'crm_refusal.company_id' => $this->company_id,
            ]);
        $this->querySetCondition($query);
        $query->groupBy('crm_refusal.refusal_id')->indexBy('refusal_id');

        return (['' => 'Все'] + $query->column());
    }

    /**
     * @param $type
     * @return array
     */
    public function getEmployeeItems()
    {
        $query = EmployeeCompany::find()->alias('employee_company')
            ->innerJoin([
                'crm_client_refusal' => ClientRefusal::tableName(),
            ], '{{crm_client_refusal}}.[[employee_id]] = {{employee_company}}.[[employee_id]]')
            ->andWhere([
                'employee_company.company_id' => $this->company_id,
            ]);
        $this->querySetCondition($query);
        $query->groupBy('employee_company.employee_id');

        return (['' => 'Все'] + ArrayHelper::map($query->all(), 'employee_id', 'shortFio'));
    }

    /**
     * @param $type
     * @return array
     */
    public function getContractorItems()
    {
        $query = Contractor::getSorted()
            ->innerJoin([
                'crm_client_refusal' => ClientRefusal::tableName(),
            ], '{{crm_client_refusal}}.[[contractor_id]] = {{contractor}}.[[id]]')
            ->select([
                'contractor_name' => 'IF(
                    {{company_type}}.[[id]] IS NULl,
                    {{contractor}}.[[name]],
                    CONCAT({{company_type}}.[[name_short]], " ", {{contractor}}.[[name]])
                )',
                'contractor.id',
            ]);
        $this->querySetCondition($query);
        $query->groupBy('contractor.id')->indexBy('id');

        return (['' => 'Все'] + $query->column());
    }
}
