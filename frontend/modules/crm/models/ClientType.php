<?php

namespace frontend\modules\crm\models;

use common\models\Company;
use frontend\modules\crm\behaviors\CanDeleteBehavior;
use yii\behaviors\AttributeBehavior;

/**
 * Тип клиента
 *
 * @property-read int $client_type_id
 * @property string $value
 */
final class ClientType extends AbstractType
{
    /** @var int Клиент */
    public const TYPE_CLIENT = 1;

    /** @var int Партнер */
    public const TYPE_PARTNER = 2;

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['canDelete'] = [
            'class' => CanDeleteBehavior::class,
            'modelClass' => Client::class,
            'modelAttribute' => 'client_type_id',
        ];
        $behaviors['attribute'] = [
            'class' => AttributeBehavior::class,
            'attributes' => [self::EVENT_AFTER_FIND => ['canDelete']],
            'value' => fn() => $this->canDelete && $this->value === null,
        ];

        return $behaviors;
    }

    /**
     * @param Company $company
     * @param int $value
     * @return static
     */
    public static function getByTypeValue(Company $company, int $value): self
    {
        return self::findOne(['company_id' => $company->id, 'value' => $value])
            ?: new self(['client_type_id' => self::TYPE_CLIENT]);
    }

    /**
     * @return bool
     */
    public static function isCreatable(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%crm_client_type}}';
    }

    /**
     * @inheritDoc
     */
    public static function getDefaultRows(int $company_id): array
    {
        return [
            ['company_id' => $company_id, 'value' => self::TYPE_CLIENT, 'name' => 'Клиент'],
            ['company_id' => $company_id, 'value' => self::TYPE_PARTNER, 'name' => 'Партнер'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'Тип отношений';
    }
}
