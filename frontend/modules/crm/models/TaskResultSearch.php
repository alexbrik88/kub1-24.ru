<?php

namespace frontend\modules\crm\models;

use common\models\Company;
use frontend\modules\crm\models\TaskResult;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TaskResultSearch represents the model behind the search form of `frontend\modules\crm\models\TaskResult`.
 */
class TaskResultSearch extends TaskResult
{
    private Company $_company;

    public function __construct(Company $company, $params = [])
    {
        $this->_company = $company;

        parent::__construct($params);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['task_result_id', 'company_id', 'employee_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaskResult::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'task_result_id' => $this->task_result_id,
            'company_id' => $this->company_id,
            'employee_id' => $this->employee_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
