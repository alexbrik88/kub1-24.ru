<?php

namespace frontend\modules\crm\models;

use common\models\Contractor;
use common\models\ListInterface;

/**
 * Фабрика сокращенных списков для фильтров на странице списка клиентов
 */
final class ContractorListFactory
{
    /**
     * @var ContractorFilter
     */
    private $filter;

    /**
     * @param ContractorFilter $clientFilter
     */
    public function __construct(ContractorFilter $clientFilter)
    {
        $this->filter = $clientFilter;
    }

    /**
     * @return ListInterface
     */
    public function createEmployeeList(): ListInterface
    {
        $column = Contractor::tableName() . '.responsible_employee_id';
        $id = $this->filter
            ->createQuery()
            ->select($column)
            ->groupBy($column)
            ->andWhere(['not', [$column => null]])
            ->column();

        return (new ListFactory)->createEmployeeList($id);
    }

    /**
     * @return ListInterface
     */
    public function createContactPhoneList(): ListInterface
    {
        return $this->createContactList('primary_phone_number');
    }

    /**
     * @return ListInterface
     */
    public function createContactEmailList(): ListInterface
    {
        return $this->createContactList('email_address');
    }

    /**
     * @return ListInterface
     */
    public function createContactNameList(): ListInterface
    {
        return $this->createContactList('contact_name');
    }

    /**
     * @return ListInterface
     */
    public function createReasonList(): ListInterface
    {
        return $this->createTypeList(ClientReason::class, Client::tableName(), 'client_reason_id');
    }

    /**
     * @return ListInterface
     */
    public function createCheckList(): ListInterface
    {
        return $this->createTypeList(ClientCheck::class, Client::tableName(), 'client_check_id');
    }

    /**
     * @return ListInterface
     */
    public function createPositionList(): ListInterface
    {
        $items = ClientPosition::find()->where([
            'client_position_id' => $this->createContactList('client_position_id')->getItems(),
        ])->select('name')->indexBy('client_position_id')->column();

        asort($items);

        return new CommonList($items);
    }

    /**
     * @return ListInterface
     */
    public function createActivityList(): ListInterface
    {
        return $this->createTypeList(ContractorActivity::class, Contractor::tableName(), 'activity_id');
    }

    /**
     * @return ListInterface
     */
    public function createCampaignList(): ListInterface
    {
        return $this->createTypeList(ContractorCampaign::class, Contractor::tableName(), 'campaign_id');
    }

    /**
     * @return ListInterface
     */
    public function createStatusList(): ListInterface
    {
        return $this->createTypeList(ClientStatus::class, Client::tableName(), 'client_status_id');
    }

    /**
     * @return ListInterface
     */
    public function createClientTypeList(): ListInterface
    {
        return $this->createTypeList(ClientType::class, Client::tableName(), 'client_type_id');
    }

    /**
     * @return ListInterface
     */
    public function createTaskEventList(): ListInterface
    {
        $eventTypes = $this->filter
            ->createQuery()
            ->select(Client::tableName() . '.event_type')
            ->groupBy(Client::tableName() . '.event_type')
            ->column();

        $items = (new TaskEventList)->getItems();
        $filter = function(int $eventType) use ($eventTypes): bool {
            return in_array($eventType, $eventTypes);
        };

        return new CommonList(array_filter($items, $filter, ARRAY_FILTER_USE_KEY));
    }

    /**
     * @return ListInterface
     */
    public function createDealList(): ListInterface
    {
        return $this->createTypeList(DealType::class, Client::tableName(), 'deal_type_id');
    }

    /**
     * @param string $column
     * @return ListInterface
     */
    private function createContactList(string $column): ListInterface
    {
        $items = [];
        $values = $this->filter->createQuery()->select('crm_contact.'.$column)->distinct()->column();
        foreach ($values as $value) {
            if (strlen($value) > 0) {
                $items[$value] = $value;
            }
        }

        asort($items);

        return new CommonList($items);




        /** @var Contractor[] $contractors */
        $contractors = $this
            ->filter
            ->createQuery()
            // TODO: ->andWhere()
            ->all();

        $items = [];

        foreach ($contractors as $contractor) {
            $value = $contractor->getActualContact()->getAttribute($column);

            if (strlen($value) > 0) {
                $items[$value] = $value;
            }
        }

        asort($items);

        return new CommonList($items);
    }

    /**
     * @param string|AbstractType $modelClass
     * @param string $joinTable
     * @param string $joinColumn
     * @return ListInterface
     */
    private function createTypeList(string $modelClass, string $joinTable, string $joinColumn): ListInterface
    {
        $column = $modelClass::tableName() . '.name';
        $on = sprintf('%s.%s = %s.%s', $joinTable, $joinColumn, $modelClass::tableName(), $modelClass::getIdKey());

        $items = $this->filter
            ->createQuery()
            ->leftJoin($modelClass::tableName(), $on)
            ->select($column)
            ->groupBy($column)
            ->orderBy($column)
            ->indexBy($modelClass::tableName() . '.' . $modelClass::getIdKey())
            ->andWhere(['not', [$column => null]])
            ->column();

        return new CommonList($items);
    }
}
