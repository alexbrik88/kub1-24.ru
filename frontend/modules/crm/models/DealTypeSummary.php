<?php

namespace frontend\modules\crm\models;

final class DealTypeSummary
{
    /**
     * @var array
     */
    public array $amount = [];

    /**
     * @var array
     */
    public array $count = [];

    /**
     * @param array $rows
     */
    public function __construct(array $rows)
    {
        foreach ($rows as $row) {
            $this->amount[$row['deal_stage_id']] = (int) $row['amount'] ?: 0;
            $this->count[$row['deal_stage_id']] = (int) $row['count'] ?: 0;
        }
    }

    /**
     * @param int $dealTypeId
     * @return int
     */
    public function getAmount(int $dealTypeId): int
    {
        return $this->amount[$dealTypeId] ?? 0;
    }

    /**
     * @param int $dealTypeId
     * @return int
     */
    public function getCount(int $dealTypeId): int
    {
        return $this->count[$dealTypeId] ?? 0;
    }
}
