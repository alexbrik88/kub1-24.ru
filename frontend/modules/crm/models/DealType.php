<?php

namespace frontend\modules\crm\models;

use frontend\modules\crm\behaviors\CanDeleteBehavior;
use yii\db\ActiveQuery;

/**
 * @property-read int $deal_type_id
 * @property-read DealStage[] $stages
 */
final class DealType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['canDelete'] = [
            'class' => CanDeleteBehavior::class,
            'modelClass' => Client::class,
            'modelAttribute' => 'deal_type_id',
        ];

        return $behaviors;
    }

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%crm_deal_type}}';
    }

    /**
     * @return DealStage
     */
    public function getFirstStage(): ?DealStage
    {
        $stages = $this->stages; // Да-да

        return reset($stages) ?: null;
    }

    /**
     * @return ActiveQuery
     */
    public function getStages(): ActiveQuery
    {
        return $this
            ->hasMany(DealStage::class, ['deal_type_id' => 'deal_type_id'])
            ->orderBy(['order' => SORT_ASC])
            ->indexBy(DealStage::getIdKey()); // TODO: кажется, что это лажа
    }

    /**
     * @return int
     */
    public function getStagesCount(): int
    {
        return count($this->stages);
    }

    /**
     * @inheritDoc
     */
    public static function getDefaultRows(int $company_id): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'Тип сделки';
    }
}
