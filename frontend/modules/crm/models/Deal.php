<?php

namespace frontend\modules\crm\models;

use common\components\date\DateHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\EmployeeCompany;
use DateTimeImmutable;
use frontend\modules\crm\behaviors\DealStagedAtBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * @property-read int $deal_id
 * @property int $company_id
 * @property int $contractor_id
 * @property int $contact_id
 * @property int $employee_id
 * @property int $deal_type_id
 * @property int $deal_stage_id
 * @property int $currency_id
 * @property int $amount
 * @property string $name
 * @property string $comment
 * @property string $staged_at
 * @property-read string $created_at
 * @property-read string $updated_at
 * @property-read string $stageDayCount
 * @property-read DealType $type
 * @property-read DealStage $stage
 * @property-read Contractor $contractor
 * @property-read Contact $contact
 * @property-read Currency $currency
 * @property-read EmployeeCompany $employeeCompany
 */
final class Deal extends ActiveRecord
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
            'dealStagedAt' => [
                'class' => DealStagedAtBehavior::class,
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public static function tableName(): string
    {
        return '{{%crm_deal}}';
    }

    /**
     * @param Company $company
     * @param mixed $dealId
     * @return Deal|null
     */
    public static function findById(Company $company, $dealId): ?self
    {
        if (!is_scalar($dealId)) {
            return null;
        }

        return self::findOne(['company_id' => $company->id, 'deal_id' => $dealId]);
    }

    /**
     * @return ActiveQuery
     */
    public function getContractor(): ActiveQuery
    {
        return $this->hasOne(Contractor::class, ['id' => 'contractor_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getContact(): ActiveQuery
    {
        return $this->hasOne(Contact::class, ['contact_id' => 'contact_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEmployeeCompany(): ActiveQuery
    {
        return $this->hasOne(EmployeeCompany::class, ['employee_id' => 'employee_id', 'company_id' => 'company_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCurrency(): ActiveQuery
    {
        return $this->hasOne(Currency::class, ['id' => 'currency_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getType(): ActiveQuery
    {
        return $this->hasOne(DealType::class, ['deal_type_id' => 'deal_type_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getStage(): ActiveQuery
    {
        return $this->hasOne(DealStage::class, ['deal_stage_id' => 'deal_stage_id']);
    }

    /**
     * @return int
     */
    public function getStageDayCount(): int
    {
        $now = (new DateTimeImmutable)->setTime(0, 0, 0, 0);
        $staged = DateTimeImmutable::createFromFormat(DateHelper::FORMAT_DATETIME, $this->staged_at)
            ->setTime(0, 0, 0, 0);

        return $now->diff($staged)->days;
    }

    /**
     * @return string
     */
    public function getAmountWithCurrency(): string
    {
        return sprintf(
            '%s %s',
            Yii::$app->formatter->format($this->amount / 100, ['decimal', 2]),
            $this->currency->name
        );
    }

    /**
     * @param Company $company
     * @return int
     */
    public static function getNextNumber(Company $company): int
    {
        $number = self::find()->andWhere(['company_id' => $company->id])->count();
        $number++;

        return $number;
    }
}
