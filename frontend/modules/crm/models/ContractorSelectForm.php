<?php

namespace frontend\modules\crm\models;

use common\models\Company;
use common\models\Contractor;
use common\models\EmployeeCompany;
use yii\base\Model;

final class ContractorSelectForm extends Model
{
    /**
     * @var int[]
     */
    public $contractor_id;

    /**
     * @var int
     */
    public $responsible_employee_id;

    /**
     * @var int
     */
    public $responsible_employee_task = 1;

    /**
     * @var int
     */
    public $client_status_id;

    /**
     * @var int
     */
    public $client_type_id;

    /**
     * @var int
     */
    public $activity_id;

    /**
     * @var int
     */
    public $campaign_id;

    /**
     * @var bool
     */
    public bool $canChangeEmployee;

    /**
     * @var Company
     */
    private Company $company;

    /**
     * @var ContractorFilter
     */
    private ContractorFilter $filter;

    /**
     * @param Company $company
     * @param ContractorFilter $filter
     * @param bool $canChangeEmployee
     */
    public function __construct(Company $company, ContractorFilter $filter, bool $canChangeEmployee)
    {
        $this->company = $company;
        $this->filter = $filter;
        $this->canChangeEmployee = $canChangeEmployee;

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'activity_id' => 'Вид деятельности',
            'campaign_id' => 'Источник',
            'client_status_id' => 'Статус',
            'client_type_id' => 'Тип',
            'responsible_employee_id' => 'Ответственный',
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['contractor_id'], 'required'],
            [['contractor_id'], 'each', 'rule' => ['integer']],

            [['activity_id'], 'integer'],
            [
                ['activity_id'],
                'exist',
                'targetClass' => ContractorActivity::class,
                'targetAttribute' => 'id',
                'filter' => ['company_id' => $this->company->id],
            ],

            [['campaign_id'], 'integer'],
            [
                ['campaign_id'],
                'exist',
                'targetClass' => ContractorCampaign::class,
                'targetAttribute' => 'id',
                'filter' => ['company_id' => $this->company->id],
            ],

            [['client_status_id'], 'integer'],
            [
                ['client_status_id'],
                'exist',
                'targetClass' => ClientStatus::class,
                'targetAttribute' => 'client_status_id',
                'filter' => ['company_id' => $this->company->id],
            ],

            [['client_type_id'], 'integer'],
            [
                ['client_type_id'],
                'exist',
                'targetClass' => ClientType::class,
                'targetAttribute' => 'client_type_id',
                'filter' => ['company_id' => $this->company->id],
            ],

            [['responsible_employee_task'], 'boolean'],
            [['responsible_employee_id'], 'integer'],
            [
                ['responsible_employee_id'],
                'exist',
                'targetClass' => EmployeeCompany::class,
                'targetAttribute' => 'employee_id',
                'filter' => [
                    'company_id' => $this->company->id,
                    'is_working' => true,
                ],
            ],
        ];
    }

    /**
     * @return bool
     */
    public function updateModels(): bool
    {
        /** @var Contractor[] $contractors */
        $contractors = $this->filter
            ->createQuery()
            ->andWhere([Contractor::tableName() . '.id' => $this->contractor_id])
            ->all();

        if (empty($contractors)) {
            return false;
        }

        foreach ($contractors as $contractor) {
            $client = $contractor->client ?: Client::createInstance($contractor);
            $client->client_status_id = $this->client_status_id ?: $client->client_status_id;
            $client->client_type_id = $this->client_type_id ?: $client->client_type_id;
            $contractor->activity_id = $this->activity_id ?: $contractor->activity_id;
            $contractor->campaign_id = $this->campaign_id ?: $contractor->campaign_id;

            $updateTasks = false;
            if ($this->responsible_employee_id && $this->canChangeEmployee) {
                $contractor->responsible_employee_id = $this->responsible_employee_id;
                if ($this->responsible_employee_task) {
                    $updateTasks = true;
                }
            }

            $contractor->save(false, ['activity_id', 'campaign_id', 'responsible_employee_id']);
            $client->save(false);

            if ($updateTasks) {
                \Yii::$app->db->createCommand()->update(Task::tableName(), [
                    'employee_id' => $contractor->responsible_employee_id,
                ], [
                    'company_id' => $contractor->company_id,
                    'contractor_id' => $contractor->id,
                ])->execute();
            }
        }

        return true;
    }
}
