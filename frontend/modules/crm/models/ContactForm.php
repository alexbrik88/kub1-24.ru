<?php

namespace frontend\modules\crm\models;

use common\models\Contractor;
use yii\base\Model;

final class ContactForm extends Model
{
    use ValidateTypeTrait;

    /** @var int Количество создаваеммых форм */
    public const CONTACT_COUNT = 3;

    /**
     * @var int
     */
    public $client_position_id;

    /**
     * @var bool
     */
    public $contact_default;

    /**
     * @var string
     */
    public $contact_name;

    /**
     * @var string
     */
    public $email_address;

    /**
     * @var string
     */
    public $skype_account;

    /**
     * @var string
     */
    public $social_account;

    /**
     * @var string
     */
    public $primary_phone_number;

    /**
     * @var string
     */
    public $secondary_phone_number;

    /**
     * @var string
     */
    public $primary_messenger_type;

    /**
     * @var string
     */
    public $secondary_messenger_type;

    /**
     * @var Contact
     */
    public Contact $contact;

    /**
     * @var string[]
     */
    private array $requiredAttributes = [];

    /**
     * @var Contractor
     */
    private Contractor $contractor;

    /**
     * @param Contractor $contractor
     * @param Contact $contact
     * @param array $requiredAttributes
     */
    public function __construct(Contractor $contractor, Contact $contact, array $requiredAttributes = [])
    {
        $this->contractor = $contractor;
        $this->contact = $contact;
        $this->requiredAttributes = $requiredAttributes;
        $attributes = $contact->getAttributes($this->activeAttributes());
        $this->setAttributes($attributes, false);

        parent::__construct();
    }

    /**
     * @param Contractor $contractor
     * @return static[]
     */
    public static function createMultiple(Contractor $contractor): array
    {
        $forms = [];
        $contacts = Contact::findByContractor($contractor);

        for ($index = 0; $index < self::CONTACT_COUNT; $index++) {
            $contact = $contacts[$index] ?? new Contact();
            $forms[$index] = new self($contractor, $contact);
        }

        return $forms;
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels(): array
    {
        return [
            'client_position_id' => 'Должность',
            'contact_default' => 'Выводить в карточке',
            'contact_name' => 'ФИО',
            'email_address' => 'E-mail',
            'skype_account' => 'Skype',
            'social_account' => 'Ссылка на соц. сеть',
            'primary_phone_number' => 'Телефон',
            'secondary_phone_number' => 'Телефон 2',
            'primary_messenger_type' => 'Мессенджер',
            'secondary_messenger_type' => 'Мессенджер',
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [$this->requiredAttributes, 'required'],

            [['client_position_id'], 'integer'],
            [
                ['client_position_id'], 'exist',
                'targetClass' => ClientPosition::class,
                'targetAttribute' => 'client_position_id',
                'filter' => [
                    'company_id' => $this->contractor->company_id,
                ],
            ],

            [['contact_default'], 'default', 'value' => false],
            [['contact_default'], 'boolean'],

            [['contact_name'], 'default', 'value' => ''],
            [['contact_name'], 'trim'],
            [['contact_name'], 'string', 'max' => 255],

            [['email_address'], 'default', 'value' => ''],
            [['email_address'], 'trim'],
            [['email_address'], 'string', 'max' => 255],
            [['email_address'], 'email'],

            [['skype_account'], 'default', 'value' => ''],
            [['skype_account'], 'trim'],
            [['skype_account'], 'string', 'max' => 255],

            [['social_account'], 'default', 'value' => ''],
            [['social_account'], 'trim'],
            [['social_account'], 'string', 'max' => 255],
            [['social_account'], 'url'],

            [['primary_phone_number', 'secondary_phone_number'], 'default', 'value' => ''],
            [['primary_phone_number', 'secondary_phone_number'], 'trim'],
            [['primary_phone_number', 'secondary_phone_number'], 'string', 'max' => 45],

            [['primary_messenger_type', 'secondary_messenger_type'], 'default', 'value' => null],
            [['primary_messenger_type', 'secondary_messenger_type'], 'integer'],
            [
                ['primary_messenger_type', 'secondary_messenger_type'],
                'in',
                'range' => array_keys((new MessengerTypeList)->getItems()),
            ],
        ];
    }

    /**
     * @return bool
     * @throws
     */
    public function updateContact(): bool
    {
        $attributes = $this->getAttributes($this->activeAttributes());
        $attributes['contractor_id'] = $this->contractor->id;

        $this->contact->setAttributes($attributes, false);

        if ($this->contact->isEmpty()) {
            return $this->contact->isNewRecord ? $this->contact->delete() : true;
        }

        if ($this->contact->contact_name == '' && $this->contractor->face_type == Contractor::TYPE_PHYSICAL_PERSON) {
            $this->contact->contact_name = $this->contractor->getNameWithType();
        }

        return $this->contact->save(false);
    }
}
