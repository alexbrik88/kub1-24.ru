<?php

namespace frontend\modules\crm\models;

use Yii;
use common\models\EmployeeCompany;

/**
 * This is the model class for table "crm_task_comment".
 *
 * @property int $id
 * @property int $crm_task_id
 * @property int $employee_id
 * @property int $company_id
 * @property string $created_at
 * @property string $text
 *
 * @property CrmTask $crmTask
 * @property EmployeeCompany $employee
 */
class TaskComment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_task_comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'crm_task_id' => 'Crm Task ID',
            'employee_id' => 'Employee ID',
            'company_id' => 'Company ID',
            'created_at' => 'Created At',
            'text' => 'Text',
        ];
    }

    /**
     * Gets query for [[CrmTask]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(CrmTask::className(), ['task_id' => 'crm_task_id']);
    }

    /**
     * Gets query for [[Employee]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeCompany()
    {
        return $this->hasOne(EmployeeCompany::className(), ['employee_id' => 'employee_id', 'company_id' => 'company_id']);
    }
}
