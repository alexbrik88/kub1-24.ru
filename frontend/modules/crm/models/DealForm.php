<?php

namespace frontend\modules\crm\models;

use common\models\Contractor;
use common\models\currency\Currency;
use common\models\EmployeeCompany;
use frontend\components\WebUser;
use frontend\rbac\permissions\crm\DealAccess;
use Yii;
use yii\base\Model;
use yii\web\User;

final class DealForm extends Model
{
    /**
     * @var bool
     */
    public bool $canChangeEmployee;

    /**
     * @var int
     */
    public $contractor_id;

    /**
     * @var int
     */
    public $contact_id;

    /**
     * @var int
     */
    public $employee_id;

    /**
     * @var int
     */
    public $currency_id = Currency::DEFAULT_ID;

    /**
     * @var int
     */
    public $deal_stage_id;

    /**
     * @var string
     */
    public $amount;

    /**
     * @var string
     */
    public $name;

    /**
     * @var $string
     */
    public $comment;

    /**
     * @var int
     */
    public int $nextNumber = 1;

    /**
     * @var Deal
     */
    private Deal $deal;

    /**
     * @var DealType
     */
    private DealType $dealType;

    /**
     * @var User|WebUser
     */
    private User $user;

    /**
     * @param Deal $deal
     * @param DealType $dealType
     * @param User $user
     */
    public function __construct(Deal $deal, DealType $dealType, User $user)
    {
        $this->deal = $deal;
        $this->dealType = $dealType;
        $this->user = $user;
        $this->canChangeEmployee = $this->user->can(DealAccess::EDIT_ALL);
        $this->employee_id = $this->user->identity->id;
        $this->nextNumber = Deal::getNextNumber($this->user->identity->company);
        $this->name = $this->nextNumber;
        $this->deal_stage_id = ($ds = $this->dealType->getFirstStage()) ? $ds->deal_stage_id : null;

        if (!$deal->isNewRecord) {
            $this->setAttributes($this->deal->getAttributes(), false);
            $this->amount = $this->deal->amount / 100;
        }

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels(): array
    {
        return [
            'name' => 'Название',
            'comment' => 'Комментарий',
            'deal_stage_id' => "Этап воронки «{$this->dealType->name}»",
            'contractor_id' => 'Клиент',
            'contact_id' => 'Контакт',
            'employee_id' => 'Ответсветнный',
            'currency_id' => 'Валюта',
            'amount' => 'Сумма',
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['name'], 'trim'],
            [['name'], 'string', 'min' => 1, 'max' => 85],

            [['comment'], 'string', 'max' => 65535],
            [['comment'], 'trim'],
            [['comment'], 'default', 'value' => ''],

            [['contractor_id'], 'required'],
            [['contractor_id'], 'integer'],
            [
                ['contractor_id'],
                'exist',
                'targetClass' => Contractor::class,
                'targetAttribute' => 'id',
                'filter' => ['company_id' => $this->user->identity->company->id],
            ],

            [['employee_id'], 'required'],
            [['employee_id'], 'integer'],
            [
                ['employee_id'],
                'exist',
                'targetClass' => EmployeeCompany::class,
                'targetAttribute' => 'employee_id',
                'filter' => [
                    'company_id' => $this->user->identity->company->id,
                    'is_working' => true,
                ],
            ],

            [['comment'], 'string', 'max' => 65535],
            [['comment'], 'trim'],
            [['comment'], 'default', 'value' => ''],

            [['deal_stage_id'], 'required'],
            [['deal_stage_id'], 'integer'],
            [
                ['deal_stage_id'],
                'exist',
                'targetClass' => DealStage::class,
                'targetAttribute' => 'deal_stage_id',
                'filter' => ['deal_type_id' => $this->dealType->deal_type_id],
            ],

            [['amount'], 'default', 'value' => 0.0],
            [
                ['amount'],
                'number',
                'numberPattern' => Yii::$app->params['numberPattern'],
                'min' => 0.0,
                'max' => Yii::$app->params['maxCashSum'] * 100,
            ],

            [['currency_id'], 'required'],
            [['currency_id'], 'string'],
            [['currency_id'], 'exist', 'targetClass' => Currency::class, 'targetAttribute' => 'id'],

            [['contact_id'], 'required'],
            [['contact_id'], 'integer'],
            [
                ['contact_id'],
                'exist',
                'targetClass' => Contact::class,
                'targetAttribute' => ['contact_id', 'contractor_id'],
                'when' => fn (self $dealForm) => ($dealForm->contact_id != Contact::DEFAULT_CONTACT_ID),
            ],
        ];
    }

    /**
     * @return bool
     */
    public function updateDeal(): bool
    {
        $attributes = $this->getAttributes($this->activeAttributes());
        $attributes['company_id'] = $this->user->identity->company->id;
        $attributes['employee_id'] = $this->canChangeEmployee ? $this->employee_id : $this->user->identity->id;
        $attributes['deal_type_id'] = $this->dealType->deal_type_id;
        $attributes['contact_id'] = ($this->contact_id != Contact::DEFAULT_CONTACT_ID) ? $this->contact_id : null;

        $this->deal->setAttributes($attributes, false);
        $this->deal->amount = str_replace(',', '.', $this->amount * 100);

        return $this->deal->save(false);
    }

    /**
     * @return Deal
     */
    public function getDeal(): Deal
    {
        return $this->deal;
    }

    /**
     * @return DealType
     */
    public function getDealType(): DealType
    {
        return $this->dealType;
    }

    /**
     * @return User|WebUser
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
