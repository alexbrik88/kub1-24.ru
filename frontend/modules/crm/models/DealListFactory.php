<?php

namespace frontend\modules\crm\models;

use common\models\Contractor;
use common\models\ListInterface;

final class DealListFactory
{
    /**
     * @var DealFilter
     */
    private DealFilter $filter;

    /**
     * @var ListFactory
     */
    private ListFactory $listFactory;

    /**
     * @var array|null
     */
    private ?array $_filterData = null;

    /**
     * @param DealFilter $filter
     */
    public function __construct(DealFilter $filter)
    {
        $this->filter = $filter;
        $this->listFactory = new ListFactory();
    }

    /**
     * @return ListInterface
     */
    public function createContractorList(): ListInterface
    {
        return $this->listFactory->createContractorList($this->getColumnData('contractor_id'));
    }

    /**
     * @return ListInterface
     */
    public function createEmployeeList(): ListInterface
    {
        return $this->listFactory->createEmployeeList($this->getColumnData('employee_id'));
    }

    /**
     * @return ListInterface
     */
    public function createContactList(): ListInterface
    {
        $items = [];
        $rows = $this->getFilterData();

        foreach ($rows as $row) {
            if ($row['contact_id']) {
                $items[$row['contact_name']] = $row['contact_name'];
            } elseif ($row['contact_is_director']) {
                $items[$row['director_name']] = $row['director_name'];
            } else {
                $items[$row['contractor_contact_name']] = $row['contractor_contact_name'];
            }
        }

        asort($items, SORT_STRING);

        return new CommonList($items);
    }

    /**
     * @return ListInterface
     */
    public function createDealStageList(): ListInterface
    {
        return $this->listFactory->createDealStageList(null, $this->getColumnData('deal_stage_id'));
    }

    /**
     * @param string|null $column
     * @return array
     */
    private function getColumnData(?string $column = null): array
    {
        $data = [];

        foreach ($this->getFilterData() as $row) {
            if ($row[$column] !== null) {
                $data[$column] = $row[$column];
            }
        }

        return array_unique($data);
    }

    /**
     * @return array
     * @throws
     */
    private function getFilterData(): array
    {
        if ($this->_filterData === null) {
            $this->_filterData = $this->filter
                ->createQuery()
                ->select([
                    Deal::tableName() . '.contractor_id',
                    Deal::tableName() . '.contact_id',
                    Deal::tableName() . '.deal_stage_id',
                    Deal::tableName() . '.employee_id',
                    // Контакты
                    Contact::tableName() . '.contact_name',
                    Contractor::tableName() . '.contact_is_director',
                    Contractor::tableName() . '.director_name',
                    Contractor::tableName() . '.contact_name as contractor_contact_name',
                ])
                ->groupBy([
                    Deal::tableName() . '.contractor_id',
                    Deal::tableName() . '.contact_id',
                    Deal::tableName() . '.deal_stage_id',
                    Deal::tableName() . '.employee_id',
                    // Контакты
                    Contact::tableName() . '.contact_name',
                    Contractor::tableName() . '.contact_is_director',
                    Contractor::tableName() . '.director_name',
                    Contractor::tableName() . '.contact_name',
                ])
                ->createCommand()
                ->queryAll();
        }

        return $this->_filterData;
    }
}
