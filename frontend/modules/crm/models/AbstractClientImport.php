<?php

namespace frontend\modules\crm\models;

use common\models\Contractor;
use common\models\employee\Employee;
use common\services\TransactionManager;
use yii\web\UnprocessableEntityHttpException;
use yii\base\Model;

abstract class AbstractClientImport extends Model
{
    protected Employee $employee;
    protected TransactionManager $transaction;
    protected int $company_id;
    protected int $employee_id;
    protected ?int $defaultClientType = null;
    protected array $importErrors = [];
    protected static array $data = [];
    protected static array $data2 = [];
    protected $cash = [];

    public function __construct(Employee $employee, TransactionManager $transaction, ?int $defaultClientType = null)
    {
        $this->employee = $employee;
        $this->transaction = $transaction;
        $this->company_id = $employee->company->id;
        $this->employee_id = $employee->id;
        $this->defaultClientType = $defaultClientType;
    }

    public function import($runValidation = true): bool
    {
        if ($runValidation && !$this->validate()) {
            return false;
        }

        $model = new CreateContractorForm($this->employee, static::FACE_TYPE);
        if ($this->date && ($d = date_create($this->date))) {
            $model->contractorForm->created_at = $d->getTimestamp();
        }
        $formData = $this->createContractorFormData();

        if ($model->load($formData)) {
            if ($model->validate()) {
                try {
                    $this->transaction->wrap(function () use ($model) {
                        if (!$model->createPerson()) {
                            throw new UnprocessableEntityHttpException('Data Validation Failed.');
                        }
                    });

                    return true;
                } catch (UnprocessableEntityHttpException $e) {
                    // do nothing
                }
            }
            if ($model->contractorForm->hasErrors()) {
                \common\components\helpers\ModelHelper::logErrors($model->contractorForm, __METHOD__);
            }
            if ($model->clientForm->hasErrors()) {
                \common\components\helpers\ModelHelper::logErrors($model->clientForm, __METHOD__);
            }
            if ($model->contactForm->hasErrors()) {
                \common\components\helpers\ModelHelper::logErrors($model->contactForm, __METHOD__);
            }
        }

        return false;
    }

    protected function getContractorActivityId(string $name, ?int $default = null): ?int
    {
        return $this->getIdByName(ContractorActivity::class, 'id', $name, $default);
    }

    protected function getContractorCampaignId(string $name, ?int $default = null): ?int
    {
        return $this->getIdByName(ContractorCampaign::class, 'id', $name, $default);
    }

    protected function getClientPositionId(string $name, ?int $default = null): ?int
    {
        return $this->getIdByName(ClientPosition::class, 'client_position_id', $name, $default);
    }

    protected function getClientCheckId(string $name, ?int $default = null): ?int
    {
        return $this->getIdByName(ClientCheck::class, 'client_check_id', $name, $default);
    }

    protected function getClientReasonId(string $name, ?int $default = null): ?int
    {
        return $this->getIdByName(ClientReason::class, 'client_reason_id', $name, $default);
    }

    protected function getClientStatusId($value): ?int
    {
        return $this->getIdByValue(ClientStatus::class, 'client_status_id', $value);
    }

    protected function getClientTypeId(string $name, ?int $default = null): ?int
    {
        return $this->getIdByName(ClientType::class, 'client_type_id', $name, $default);
    }

    protected function getDealTypeId(string $name, ?int $default = null): ?int
    {
        return $this->getIdByName(DealType::class, 'deal_type_id', $name, $default);
    }

    protected function getIdByName(string $class, string $attribute, string $name, ?int $default = null): ?int
    {
        if (empty($name)) {
            return $default;
        }

        $lowerName = mb_strtolower($name);

        if (!isset(self::$data[$class])) {
            self::$data[$class] = $class::find()->select([
                $attribute,
                'LOWER([[name]]) AS [[lower_name]]',
            ])->where([
                'company_id' => $this->company_id,
            ])->indexBy('lower_name')->column();
        }

        if (!array_key_exists($lowerName, self::$data[$class])) {
            $model = new $class([
                'company_id' => $this->company_id,
                'employee_id' => $this->employee_id,
                'name' => $name,
                'status' => AbstractType::TYPE_STATUS_ACTIVE,
            ]);
            $model->save();
            self::$data[$class][$lowerName] = $model->$attribute;
        }

        return self::$data[$class][$lowerName];
    }

    protected function getIdByValue(string $class, string $attribute, int $value, ?int $default = null): ?int
    {
        if (!isset(self::$data2[$class])) {
            self::$data2[$class] = $class::find()->select([
                $attribute,
                'value',
            ])->where([
                'company_id' => $this->company_id,
            ])->andWhere([
                'not',
                ['value' => null],
            ])->indexBy('value')->column();
        }

        return self::$data2[$class][$value] ?? $default;
    }

    public function importErrors() : array
    {
        return $this->importErrors;
    }

    abstract public function createContractorFormData() : array;
}
