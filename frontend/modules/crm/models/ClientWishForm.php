<?php

namespace frontend\modules\crm\models;

use common\models\Contractor;
use common\models\EmployeeCompany;
use yii\helpers\ArrayHelper;

class ClientWishForm extends \yii\base\Model
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_STATUS = 'status';


    public $responsible_employee_id;
    public $status_id;
    public $text;

    private Contractor $contractor;
    private ClientWish $wish;
    private $data = [];

    public function __construct(Contractor $contractor, ClientWish $wish, $params = [])
    {
        $this->contractor = $contractor;
        $this->wish = $wish;

        parent::__construct($params);
    }

    public function init()
    {
        parent::init();

        $this->load($this->wish->getAttributes(), '');
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => [
                'text',
                'responsible_employee_id',
            ],
            self::SCENARIO_UPDATE => [
                'text',
                'status_id',
                'responsible_employee_id',
            ],
            self::SCENARIO_STATUS => [
                'status_id'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'text',
                ],
                'trim',
            ],
            [
                [
                    'text',
                    'responsible_employee_id',
                    'status_id',
                ],
                'required',
            ],
            [
                [
                    'text',
                ],
                'string',
                'max' => 500,
            ],
            [
                [
                    'responsible_employee_id',
                ],
                'in',
                'range' => array_keys($this->responsibleData()),
            ],
            [
                [
                    'status_id',
                ],
                'in',
                'range' => [
                    ClientWish::STATUS_ACTIVE,
                    ClientWish::STATUS_DONE,
                    ClientWish::STATUS_REJECTED,
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'responsible_employee_id' => 'Ответственный',
            'status_id' => 'Статус',
            'text' => 'Описание',
        ];
    }

    /**
     * responsible employee data
     */
    public function responsibleData()
    {
        if (!isset($this->data['responsibleData'])) {
            $this->data['responsibleData'] = EmployeeCompany::find()->where([
                'company_id' => $this->contractor->company_id,
            ])->andFilterWhere([
                'or',
                ['is_working' => true],
                ['employee_id' => $this->wish->responsible_employee_id],
            ])->orderBy([
                'lastname' => SORT_ASC,
                'firstname' => SORT_ASC,
                'patronymic' => SORT_ASC,
            ])->indexBy('employee_id')->all();
        }

        return $this->data['responsibleData'];
    }

    public function responsibleDropdownData() : array
    {
        return ArrayHelper::map($this->responsibleData(), 'employee_id', 'shortFio');
    }

    public function statusDropdownData() : array
    {
        return ClientWish::$statuses;
    }

    public function save(bool $validatre = true) : bool
    {
        if (!$this->validate()) {
            return false;
        }

        $method = sprintf("set%sAttributes", ucfirst($this->scenario));

        if (method_exists($this, $method)) {
            $this->$method();
        } else {
            return false;
        }

        return $this->wish->save(false);
    }

    private function setCreateAttributes()
    {
        $this->setSafeAttributes();

        $this->wish->setIsNewRecord(true);
        unset($this->wish->id);

        $this->wish->company_id = $this->contractor->company_id;
        $this->wish->contractor_id = $this->contractor->id;
        $this->wish->status_id = ClientWish::STATUS_ACTIVE;
        $this->wish->date = date('Y-m-d');
        $this->wish->number = (ClientWish::find()->where([
            'contractor_id' => $this->contractor->id,
        ])->max('number') ?: 0) + 1;
    }

    private function setUpdateAttributes()
    {
        $this->setSafeAttributes();
    }

    private function setStatusAttributes()
    {
        $this->setSafeAttributes();
    }

    private function setSafeAttributes()
    {
        foreach ($this->safeAttributes() as $key => $attribute) {
            $this->wish->$attribute = $this->$attribute;
        }
    }
}
