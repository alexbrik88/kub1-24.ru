<?php

namespace frontend\modules\crm\models;

use common\models\EmployeeCompany;
use frontend\rbac\permissions\crm\DealAccess;
use yii\base\Model;
use yii\web\User;

final class DealSelectForm extends Model
{
    /**
     * @var int[]
     */
    public $deal_id;

    /**
     * @var int
     */
    public $deal_stage_id;

    /**
     * @var int
     */
    public $employee_id;

    /**
     * @var string
     */
    public $delete;

    /**
     * @var DealType
     */
    private DealType $dealType;

    /**
     * @var DealFilter
     */
    private DealFilter $dealFilter;

    /**
     * @var User
     */
    private User $user;

    /**
     * @param DealType $dealType
     * @param DealFilter $dealFilter
     * @param User $user
     */
    public function __construct(DealType $dealType, DealFilter $dealFilter, User $user)
    {
        $this->dealType = $dealType;
        $this->dealFilter = $dealFilter;
        $this->user = $user;

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels(): array
    {
        return [
            'deal_stage_id' => 'Этап',
            'employee_id' => 'Ответственный',
        ];
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['deal_id'], 'each', 'rule' => ['integer']],

            [['deal_stage_id'], 'integer'],
            [
                ['deal_stage_id'],
                'exist',
                'targetClass' => DealStage::class,
                'targetAttribute' => 'deal_stage_id',
                'filter' => ['deal_type_id' => $this->dealType->deal_type_id],
            ],

            [['employee_id'], 'integer'],
            [
                ['employee_id'],
                'exist',
                'targetClass' => EmployeeCompany::class,
                'targetAttribute' => 'employee_id',
                'filter' => ['company_id' => $this->dealType->company_id],
            ],

            [['delete'], 'string'],
        ];
    }

    /**
     * @return bool
     * @throws
     */
    public function updateDeals(): bool
    {
        /** @var Deal[] $deals */
        $deals = $this->dealFilter
            ->createQuery()
            ->andWhere([Deal::tableName() . '.deal_id' => $this->deal_id])
            ->all();

        if (empty($deals)) {
            return false;
        }

        foreach ($deals as $deal) {
            if (strlen($this->delete)) {
                $deal->delete();
                continue;
            }

            if ($this->user->can(DealAccess::EDIT_ALL)) {
                $deal->employee_id = $this->employee_id ?: $deal->employee_id;
            }

            $deal->deal_stage_id = $this->deal_stage_id ?: $deal->deal_stage_id;
            $deal->save(false);
        }

        return true;
    }
}
