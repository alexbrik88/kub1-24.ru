<?php

namespace frontend\modules\crm\models;

use common\models\EmployeeCompany;
use common\models\employee\Employee;
use yii\base\Model;
use yii\data\ActiveDataProvider;

final class TaskCommentForm extends Model
{
    const PAGE_SIZE = 30;
    const FIRST_PAGE_SIZE = 5;

    /**
     * @var string
     */
    public $text;

    private Task $task;
    private EmployeeCompany $employeeCompany;
    private TaskComment $taskComment;

    /**
     * @param EmployeeCompany $employeeCompany
     * @param Task $task
     */
    public function __construct(EmployeeCompany $employeeCompany, Task $task, TaskComment $taskComment)
    {
        $this->employeeCompany = $employeeCompany;
        $this->task = $task;
        $this->taskComment = $taskComment;

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();

        $this->text = $this->taskComment->text;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['text'], 'trim'],
            [['text'], 'required'],
            [['text'], 'string', 'max' => 510],
        ];
    }

    /**
     * @return Task
     */
    public function getTask() : Task
    {
        return $this->task;
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        if (!$this->validate()) {
            return false;
        }

        $model = $this->taskComment;
        $model->text = $this->text;
        if ($model->isNewRecord) {
            $model->crm_task_id = $this->task->task_id;
            $model->contractor_id = $this->task->contractor_id;
            $model->employee_id = $this->employeeCompany->employee_id;
            $model->company_id = $this->employeeCompany->company_id;
        }

        if ($model->save(false)) {
            return true;
        }

        return false;
    }

    /**
     * @return ActiveDataProvider
     */
    public function search($lastId = null): ActiveDataProvider
    {
        $query = TaskComment::find()->where([
            'or',
            ['contractor_id' => $this->task->contractor_id ?: -1],
            ['crm_task_id' => $this->task->task_id],
        ])->andWHere([
            'company_id' => $this->task->company_id,
        ])->with('employeeCompany');

        $query->andFilterWhere(['<', 'id', $lastId]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $lastId ? self::PAGE_SIZE : self::FIRST_PAGE_SIZE,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        return $dataProvider;
    }
}
