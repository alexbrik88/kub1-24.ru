<?php

namespace frontend\modules\crm\models;

use common\models\Company;
use common\models\Contractor;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Дополнительные данные о контрагенте (покупателе)
 * @see Contractor::getClient()
 *
 * @property-read int $contractor_id
 * @property int $company_id
 * @property int|null $client_check_id
 * @property int|null $client_reason_id
 * @property int|null $client_status_id
 * @property int|null $client_type_id
 * @property int|null $deal_type_id
 * @property int|null $deal_stage_id
 * @property int|null $task_id
 * @property int|null $client_refusal_id
 * @property int|null $event_type
 * @property string|null $site_url
 * @property bool $is_crm_created
 * @property string|null $client_refusal_details
 * @property-read string $created_at
 * @property-read string $updated_at
 * @property-read ClientCheck|null $check
 * @property-read DealType|null $deal
 * @property-read DealStage|null $dealStage
 * @property-read ClientReason|null $reason
 * @property-read ClientStatus|null $status
 * @property-read ClientType|null $type
 * @property-read Task|null $task
 * @property-read Company $company
 */
final class Client extends ActiveRecord
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @param Contractor $contractor
     * @return static
     */
    public static function createInstance(Contractor $contractor): self
    {
        $status = ClientStatus::getByStatusValue($contractor->company, ClientStatus::STATUS_ACTIVE);
        $type = ClientType::getByTypeValue($contractor->company, ClientType::TYPE_CLIENT);
        $client = new self([
            'contractor_id' => $contractor->id,
            'company_id' => $contractor->company_id,
            'client_status_id' => $status->client_status_id,
            'client_type_id' => $type->client_type_id,
            'is_crm_created' => $contractor->isNewRecord,
        ]);

        $client->populateRelation('company', $contractor->company);
        $client->populateRelation('status', $status);
        $client->populateRelation('type', $type);

        return $client;
    }

    /**
     * @return ActiveQuery
     */
    public static function find(): ActiveQuery
    {
        return parent::find()->with([
            'check',
            'deal',
            'dealStage',
            'reason',
            'status',
            'type',
            'task',
        ]);
    }

    /**
     * @return ActiveQuery
     */
    public function getCheck(): ActiveQuery
    {
        return $this->hasOne(ClientCheck::class, ['client_check_id' => 'client_check_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDeal(): ActiveQuery
    {
        return $this->hasOne(DealType::class, ['deal_type_id' => 'deal_type_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDealStage(): ActiveQuery
    {
        return $this->hasOne(DealStage::class, ['deal_stage_id' => 'deal_stage_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getReason(): ActiveQuery
    {
        return $this->hasOne(ClientReason::class, ['client_reason_id' => 'client_reason_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getStatus(): ActiveQuery
    {
        return $this->hasOne(ClientStatus::class, ['client_status_id' => 'client_status_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getType(): ActiveQuery
    {
        return $this->hasOne(ClientType::class, ['client_type_id' => 'client_type_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTask(): ActiveQuery
    {
        return $this->hasOne(Task::class, ['task_id' => 'task_id']);
    }

    /**
     * @return ActiveQuery
     * @deprecated
     */
    public function getCompany(): ActiveQuery
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return ActiveQuery
     * @deprecated
     */
    public function getContractor(): ActiveQuery
    {
        return $this->hasOne(Contractor::class, ['id' => 'contractor_id']);
    }

    /**
     * Используется для отправки почты
     *
     * @return string
     */
    public function getEmailSubject(): string
    {
        return '';
    }

    /**
     * @return int
     */
    public function getId(): int // TODO: используется при отправке почты?
    {
        return $this->contractor_id;
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%crm_client}}';
    }
}
