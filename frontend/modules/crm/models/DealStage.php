<?php

namespace frontend\modules\crm\models;

use frontend\modules\crm\behaviors\CanDeleteBehavior;

/**
 * @property-read int $deal_stage_id
 * @property-read int $order
 */
final class DealStage extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['canDelete'] = [
            'class' => CanDeleteBehavior::class,
            'modelClass' => Client::class,
            'modelAttribute' => 'deal_stage_id',
        ];

        return $behaviors;
    }

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%crm_deal_stage}}';
    }

    /**
     * @inheritDoc
     */
    public static function getDefaultRows(int $company_id): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'Этап сделки';
    }
}
