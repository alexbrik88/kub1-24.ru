<?php

namespace frontend\modules\crm\models;

use common\models\Company;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * @property int $log_id
 * @property int $log_type
 * @property int $company_id
 * @property int $employee_id
 * @property int $model_id
 * @property string| $model_class
 * @property string|null $attribute_name
 * @property string|null $new_attribute_value
 * @property string|null $old_attribute_value
 * @property-read string $created_at
 * @property-read Company $company
 * @property-read Employee $employee
 * @property-read EmployeeCompany $employeeCompany
 */
final class Log extends ActiveRecord
{
    /** @var int */
    public const LOG_TYPE_MODEL_CREATE = 1;

    /** @var int */
    public const LOG_TYPE_MODEL_UPDATE = 2;

    /** @var int */
    public const LOG_TYPE_MODEL_DELETE = 3;

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getEmployee(): ActiveQuery
    {
        return $this->hasOne(Employee::class, ['id' => 'employee_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEmployeeCompany(): ActiveQuery
    {
        return $this->hasOne(EmployeeCompany::class, ['employee_id' => 'employee_id', 'company_id' => 'company_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCompany(): ActiveQuery
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @inheritDoc
     */
    public static function tableName(): string
    {
        return '{{%crm_log}}';
    }
}
