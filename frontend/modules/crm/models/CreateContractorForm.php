<?php

namespace frontend\modules\crm\models;

use common\models\Contractor;
use common\models\company\CompanyType;
use common\models\employee\Employee;
use yii\base\Model;

final class CreateContractorForm extends Model
{
    /**
     * @var Contractor
     */
    public Contractor $contractorForm;

    /**
     * @var ClientForm
     */
    public ClientForm $clientForm;

    /**
     * @var ContactForm
     */
    public ContactForm $contactForm;

    /**
     * @var Employee
     */
    private Employee $employee;

    /**
     * @var int
     */
    private int $faceType;

    /**
     * @param Employee $employee
     * @param int $faceType
     * @param DealType|null $dealType
     */
    public function __construct(Employee $employee, int $faceType, ?DealType $dealType = null)
    {
        $this->faceType = $faceType;
        $this->contractorForm = new Contractor([
            'face_type' => Contractor::TYPE_LEGAL_PERSON,
            'type' => Contractor::TYPE_POTENTIAL_CLIENT,
            'status' => Contractor::ACTIVE,
            'company_id' => $employee->company->id,
            'employee_id' => $employee->id,
            'responsible_employee_id' => $employee->id,
            'scenario' => 'insert',
            'chief_accountant_is_director' => true,
            'contact_is_director' => true,
            'company_type_id' => $this->faceType == Contractor::TYPE_LEGAL_PERSON ? CompanyType::TYPE_OOO : null,
        ]);

        $this->clientForm = new ClientForm($this->contractorForm, $dealType);
        $this->contactForm = new ContactForm(
            $this->contractorForm,
            new Contact(['contact_default' => true]),
            $this->faceType == Contractor::TYPE_LEGAL_PERSON ? ['contact_name'] : [],
        );

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function init(): void
    {
        $mutator = new ContractorFormMutator($this->contractorForm);
        $mutator->mutateRequiredValidator($this->faceType);
        $mutator->mutateWhenClient();
    }

    /**
     * @inheritDoc
     */
    public function load($data, $formName = null): bool
    {
        $isLoad = [
            $this->contractorForm->load($data),
            $this->clientForm->load($data),
            $this->contactForm->load($data),
        ];

        return (array_sum($isLoad) > 0);
    }

    /**
     * @inheritDoc
     */
    public function validate($attributeNames = null, $clearErrors = true): bool
    {
        $isValid = [
            $this->isActive(),
            $this->contractorForm->validate($attributeNames, $clearErrors),
            $this->clientForm->validate($attributeNames, $clearErrors),
            $this->contactForm->validate($attributeNames, $clearErrors),
        ];

        return (array_sum($isValid) === count($isValid));
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return ($this->contractorForm->face_type == $this->faceType);
    }

    /**
     * @return bool
     */
    public function createPerson(): bool
    {
        return ($this->contractorForm->save() && $this->clientForm->saveModel() && $this->contactForm->updateContact());
    }
}
