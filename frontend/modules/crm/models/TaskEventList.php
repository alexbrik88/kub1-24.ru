<?php

namespace frontend\modules\crm\models;

use common\models\ListInterface;

final class TaskEventList implements ListInterface
{
    /** @var int */
    public const EVENT_TYPE_CALL = 1;

    /** @var int */
    public const EVENT_TYPE_MEETING = 2;

    /** @var int */
    public const EVENT_TYPE_ONLINE_MEETING = 3;

    /** @var int */
    public const EVENT_TYPE_EMAIL = 4;

    /** @var int */
    public const EVENT_TYPE_MESSAGE = 5;

    /**
     * @@inheritDoc
     */
    public function getItems(): array
    {
        return [
            self::EVENT_TYPE_CALL => 'Звонок',
            self::EVENT_TYPE_MEETING => 'Встреча',
            self::EVENT_TYPE_ONLINE_MEETING => 'Встреча онлайн',
            self::EVENT_TYPE_EMAIL => 'E-mail',
            self::EVENT_TYPE_MESSAGE => 'Сообщение',
        ];
    }

    /**
     * @@inheritDoc
     */
    public static function getIcons(): array
    {
        return [
            self::EVENT_TYPE_CALL => 'phone-call',
            self::EVENT_TYPE_MEETING => 'briefcase2',
            self::EVENT_TYPE_ONLINE_MEETING => 'conference',
            self::EVENT_TYPE_EMAIL => 'email2',
            self::EVENT_TYPE_MESSAGE => 'message',
        ];
    }
}
