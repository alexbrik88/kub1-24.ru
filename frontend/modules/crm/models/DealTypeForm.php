<?php

namespace frontend\modules\crm\models;

use common\components\helpers\ArrayHelper;

/**
 * @property-read DealType $type
 */
final class DealTypeForm extends AbstractTypeForm
{
    /** @var array */
    public const DEFAULT_STAGES = [
        ['order' => 1, 'name' => 'Первичный контакт'],
        ['order' => 2, 'name' => 'Переговоры с ЛПР'],
        ['order' => 3, 'name' => 'Коммерческое предложение'],
        ['order' => 4, 'name' => 'Договор'],
        ['order' => 5, 'name' => 'Выставить счет'],
        ['order' => 6, 'name' => 'Оплата'],
    ];

    /**
     * @var DealStageForm[]
     */
    public $stageForms = [];

    /**
     * @inheritDoc
     */
    public function init(): void
    {
        parent::init();

        $data = self::DEFAULT_STAGES;

        if ($this->scenario != self::SCENARIO_CREATE) {
            $data = array_map(function (DealStage $stage) {
                $attributes = $stage->getAttributes();
                $attributes['id'] = $stage->deal_stage_id;

                return $attributes;
            }, $this->type->stages);
        }

        $this->loadStages($data);
    }

    /**
     * @param array $data
     * @return DealStageForm
     */
    public function createStageForm(array $data = []): DealStageForm
    {
        $stage = new DealStage();
        $scenario = DealStageForm::SCENARIO_CREATE;

        if (isset($data['id']) && is_scalar($data['id']) && isset($this->type->stages[$data['id']])) {
            $stage = $this->type->stages[$data['id']];
            $scenario = DealStageForm::SCENARIO_UPDATE;
        }

        $form = new DealStageForm($stage, $this->type, $this->employee, $scenario);
        $form->load($data, '');

        return $form;
    }

    /**
     * @param array $data
     * @param string|null $formName
     * @return bool
     * @throws
     */
    public function load($data, $formName = null): bool
    {
        if (parent::load($data, $formName)) {
            $this->stageForms = [];
            $formName = $this->createStageForm()->formName();

            if (isset($data[$formName]) && is_array($data[$formName])) {
                $this->loadStages($data[$formName]);
            }

            return true;
        }

        return false;
    }

    /**
     * @param null $attributeNames
     * @param bool $clearErrors
     * @return bool
     */
    public function validate($attributeNames = null, $clearErrors = true): bool
    {
        $isValid = parent::validate($attributeNames, $clearErrors);

        if ($this->stageForms) {
            $isValid = (DealStageForm::validateMultiple($this->stageForms) && $isValid);
        }

        return $isValid;
    }

    /**
     * @return bool
     */
    public function saveModel(): bool
    {
        $attributes = $this->getAttributes($this->activeAttributes());
        $attributes['company_id'] = $this->employee->company->id;
        $attributes['employee_id'] = $this->employee->id;
        $this->type->setAttributes($attributes, false);

        if ($this->type->save() && $this->saveStages()) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     * @throws
     */
    private function saveStages(): bool
    {
        $activeId = ArrayHelper::getColumn($this->stageForms, 'id');

        foreach ($this->type->stages as $stage) {
            if (!in_array($stage->deal_stage_id, $activeId) && $stage->canDelete) {
                $stage->delete();
            }
        }

        array_walk($this->stageForms, function (DealStageForm $form) {
            $form->saveModel();
        });

        return true;
    }

    /**
     * @param array $data
     * @return void
     */
    private function loadStages(array $data)
    {
        $order = 0;
        $this->stageForms = [];

        foreach ($data as $values) {
            $values['order'] = ++$order;
            $this->stageForms[] = $this->createStageForm($values);
        }
    }
}
