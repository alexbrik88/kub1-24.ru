<?php

namespace frontend\modules\crm\models;

use yii\base\Model;

final class ConfigForm extends Model
{
    /**
     * @var Config
     */
    private Config $config;

    /**
     * @var bool
     */
    public $show_customers;

    /**
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
        $attributes = $config->getAttributes($this->activeAttributes());
        $this->setAttributes($attributes, false);

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels(): array
    {
        return [
            'show_customers' => 'Выводить покупателей в CRM',
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            ['show_customers', 'required'],
            ['show_customers', 'boolean'],
        ];
    }

    /**
     * @return bool
     */
    public function updateConfig(): bool
    {
        $attributes = $this->getAttributes($this->activeAttributes());
        $this->config->setAttributes($attributes, false);

        return $this->config->save(false);
    }
}
