<?php

namespace frontend\modules\crm\models;

use common\models\ListInterface;

final class TaskListFactory
{
    /**
     * @var TaskFilter
     */
    private TaskFilter $filter;

    /**
     * @param TaskFilter $filter
     */
    public function __construct(TaskFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return ListInterface
     */
    public function createEmployeeList(): ListInterface
    {
        $id = $this->filter
            ->createQuery()
            ->select('employee_id')
            ->distinct()
            ->andWhere(['not', ['employee_id' => null]])
            ->column();

        return (new ListFactory)->createEmployeeList($id);
    }

    /**
     * @return ListInterface
     */
    public function createTaskEventList(): ListInterface
    {
        $eventTypes = $this->filter
            ->createQuery()
            ->select('event_type')
            ->groupBy('event_type')
            ->column();

        $items = (new TaskEventList)->getItems();
        $filter = function(int $eventType) use ($eventTypes): bool {
            return in_array($eventType, $eventTypes);
        };

        return new CommonList(array_filter($items, $filter, ARRAY_FILTER_USE_KEY));
    }

    /**
     * @return ListInterface
     */
    public function createStatusList(): ListInterface
    {
        $statuses = $this->filter
            ->createQuery()
            ->select('status')
            ->groupBy('status')
            ->column();

        $callback = function (int $status) use ($statuses) {
            if ($status == TaskFilter::STATUS_ACTIVE) {
                return (in_array(Task::STATUS_IN_PROGRESS, $statuses) && in_array(Task::STATUS_EXPIRED, $statuses));
            }

            return in_array($status, $statuses);
        };

        return new CommonList(array_filter(TaskFilter::STATUS_LIST, $callback, ARRAY_FILTER_USE_KEY));
    }

    /**
     * @return ListInterface
     */
    public function createClientList(): ListInterface
    {
        $id = $this->filter
            ->createQuery()
            ->select('contractor_id')
            ->groupBy('contractor_id')
            ->column();
        $items = (new ListFactory)->createClientList()->getItems();
        $callback = function (int $clientId) use ($id) {
            return in_array($clientId, $id);
        };

        return new CommonList(array_filter($items, $callback, ARRAY_FILTER_USE_KEY));
    }

    /**
     * @return ListInterface
     */
    public function createContactList(\yii\db\ActiveQuery $query): ListInterface
    {
        $id = $query
            ->select([Task::tableName() . '.contact_id'])
            ->distinct()
            ->andWhere(['>', 'contact_id', 0])
            ->column();
        $items = (new ListFactory)->createContactListById($id)->getItems();

        return new CommonList($items);
    }

    /**
     * @param string|AbstractType $modelClass
     * @return ListInterface
     */
    private function createTypeList(string $modelClass): ListInterface
    {
        $column = $modelClass::tableName() . '.name';
        $on = sprintf(
            '%s.%s = %s.%s',
            Task::tableName(),
            $modelClass::getIdKey(),
            $modelClass::tableName(),
            $modelClass::getIdKey()
        );

        $items = $this->filter
            ->createQuery()
            ->leftJoin($modelClass::tableName(), $on)
            ->select($column)
            ->groupBy($column)
            ->orderBy($column)
            ->indexBy($modelClass::getIdKey())
            ->andWhere(['not', [$column => null]])
            ->column();

        return new CommonList($items);
    }
}
