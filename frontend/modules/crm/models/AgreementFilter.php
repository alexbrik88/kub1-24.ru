<?php

namespace frontend\modules\crm\models;

use common\models\Agreement;
use common\models\Contractor;
use common\models\FilterInterface;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\data\Sort;
use yii\db\ActiveQuery;

final class AgreementFilter extends Model implements FilterInterface
{
    /**
     * @var int|null
     */
    public $document_type_id;

    /**
     * @var int|null
     */
    public $agreement_template_id;

    /**
     * @var int|null
     */
    public $created_by;

    /**
     * @var string|null
     */
    public $search;

    /**
     * @var Contractor
     */
    private Contractor $contractor;

    /**
     * @var AgreementListFactory
     */
    private AgreementListFactory $listFactory;

    /**
     * @var int
     */
    private int $pageSize;

    /**
     * @param Contractor $contractor
     * @param int|null $pageSize
     */
    public function __construct(Contractor $contractor, ?int $pageSize = null)
    {
        $this->contractor = $contractor;
        $this->listFactory = new AgreementListFactory($this);
        $this->pageSize = $pageSize ?: self::DEFAULT_PAGE_SIZE;

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['document_type_id'], 'integer'],
            [['agreement_template_id'], 'integer'],
            [['created_by'], 'integer'],
            [['search'], 'string'],
        ];
    }

    /**
     * @return DataProviderInterface
     */
    public function getDataProvider(): DataProviderInterface
    {
        return new ActiveDataProvider([
            'query' => $this->createQuery()->with([
                'agreementType',
                'template',
                'files',
            ]),
            'sort' => $this->getSort(),
            'pagination' => [
                'pageSize' => $this->pageSize,
            ],
        ]);
    }

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
        $filter = new self($this->contractor);

        return $filter->createQuery()->count();
    }

    /**
     * @return ActiveQuery
     */
    public function createQuery(): ActiveQuery
    {
        $query = Agreement::find()
            ->andWhere([
                Agreement::tableName() . '.company_id' => $this->contractor->company->id,
                Agreement::tableName() . '.contractor_id' => $this->contractor->id,
            ])
            ->andFilterWhere([
                Agreement::tableName() . '.agreement_template_id' => $this->agreement_template_id,
                Agreement::tableName() . '.created_by' => $this->created_by,
                Agreement::tableName() . '.document_type_id' => $this->document_type_id,
            ]);

        if (strlen($this->search) > 0) {
            $query->andWhere(['like', Agreement::tableName() . '.document_name', $this->search]);
        }

        return $query;
    }

    /**
     * @return Sort
     */
    public function getSort(): Sort
    {
        return new Sort(['defaultOrder' => ['document_date' => SORT_DESC]]);
    }

    /**
     * @return AgreementListFactory
     */
    public function getListFactory(): AgreementListFactory
    {
        return $this->listFactory;
    }
}
