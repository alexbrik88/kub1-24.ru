<?php

namespace frontend\modules\crm\models;

use frontend\modules\crm\behaviors\CanDeleteBehavior;

/**
 * Причина обращения клиента
 *
 * @property-read int $client_reason_id
 */
final class ClientReason extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['canDelete'] = [
            'class' => CanDeleteBehavior::class,
            'modelClass' => Client::class,
            'modelAttribute' => 'client_reason_id',
        ];

        return $behaviors;
    }

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%crm_client_reason}}';
    }

    /**
     * @inheritDoc
     */
    public static function getDefaultRows(int $company_id): array
    {
        return [
            ['company_id' => $company_id, 'name' => 'Консультация'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'Тип обращения';
    }
}
