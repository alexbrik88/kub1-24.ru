<?php

namespace frontend\modules\crm\models;

use common\components\date\DateHelper;
use common\models\Agreement;
use common\models\AgreementTemplate;
use common\models\AgreementType;
use common\models\Contractor;
use common\models\company\CheckingAccountant;
use yii\base\Model;
use yii\web\JsExpression;

final class AgreementForm extends Model
{
    /**
     * @var string
     */
    public $checking_accountant_id;

    /**
     * @var int
     */
    public $document_type_id;

    /**
     * @var bool
     */
    public $create_agreement_from;

    /**
     * @var string
     */
    public $document_name;

    /**
     * @var int
     */
    public $document_number;

    /**
     * @var int|null
     */
    public $agreement_template_id;

    /**
     * @var string
     */
    public $document_date;

    /**
     * @var string
     */
    public $payment_limit_date;

    /**
     * @var Agreement
     */
    public Agreement $agreement;

    /**
     * @var Contractor
     */
    public Contractor $contractor;

    /**
     * @param Agreement $agreement
     * @param Contractor $contractor
     */
    public function __construct(Agreement $agreement, Contractor $contractor)
    {
        $this->agreement = $agreement;
        $this->contractor = $contractor;

        $this->setAttributes($agreement->getAttributes(), false);
        $this->document_date = DateHelper::format($agreement->document_date,
            DateHelper::FORMAT_USER_DATE,
            DateHelper::FORMAT_DATE
        );

        if ($agreement->payment_limit_date) {
            $this->payment_limit_date = DateHelper::format(
                $agreement->payment_limit_date,
                DateHelper::FORMAT_USER_DATE,
                DateHelper::FORMAT_DATE
            );
        }

        if ($agreement->agreement_template_id) {
            $this->create_agreement_from = true;
        }

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'create_agreement_from' => 'Создать по шаблону',
            'document_type_id' => 'Тип документа',
            'document_name' => 'Наименование документа',
            'document_number' => 'Номер документа',
            'document_date' => 'Дата документа',
            'checking_accountant_id' => 'Мой расчетный счет',
            'agreement_template_id' => 'Шаблон',
            'payment_limit_date' => 'Дата окончания документа',
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['create_agreement_from'], 'required'],
            [['create_agreement_from'], 'boolean'],

            [['document_date'], 'required'],
            [['document_date'], 'date', 'format' => 'php:' . DateHelper::FORMAT_USER_DATE],

            [['document_name'], 'required'],
            [['document_name'], 'string'],

            [['document_number'], 'required'],
            [['document_number'], 'integer'],

            [['payment_limit_date'], 'date', 'format' => 'php:' . DateHelper::FORMAT_USER_DATE],

            [
                ['agreement_template_id', 'checking_accountant_id', 'payment_limit_date'],
                'required',
                'when' => function (): bool {
                    return (bool) $this->create_agreement_from;
                },
                'whenClient' => new JsExpression('function() { return false; }'),
            ],

            [['agreement_template_id'], 'integer'],
            [
                ['agreement_template_id'],
                'exist',
                'targetClass' => AgreementTemplate::class,
                'targetAttribute' => 'id',
                'filter' => [
                    'company_id' => $this->contractor->company->id,
                ],
            ],

            [['checking_accountant_id'], 'integer'],
            [
                ['checking_accountant_id'],
                'exist',
                'targetClass' => CheckingAccountant::class,
                'targetAttribute' => 'id',
                'filter' => [
                    'company_id' => $this->contractor->company->id,
                ],
            ],

            [['document_type_id'], 'required'],
            [['document_type_id'], 'integer'],
            [
                ['document_type_id'],
                'exist',
                'targetClass' => AgreementType::class,
                'targetAttribute' => 'id',
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => [
                'agreement_template_id',
                'checking_accountant_id',
                'create_agreement_from',
                'document_date',
                'document_name',
                'document_number',
                'document_type_id',
                'payment_limit_date',
            ],
        ];
    }

    /**
     * @return bool
     */
    public function saveModel(): bool
    {
        $attributes = $this->getAttributes($this->activeAttributes());
        $attributes['is_created'] = true;

        if (!$this->create_agreement_from) {
            $attributes['agreement_template_id'] = null;
            $attributes['checking_accountant_id'] = null;
            $attributes['payment_limit_date'] = null;
        } else {
            $attributes['payment_limit_date'] = DateHelper::format(
                $this->payment_limit_date,
                DateHelper::FORMAT_DATE,
                DateHelper::FORMAT_USER_DATE
            );

            $attributes['company_rs'] = CheckingAccountant::findOne($this->checking_accountant_id)->rs;
        }

        $attributes['document_date'] = DateHelper::format(
            $this->document_date,
            DateHelper::FORMAT_DATE,
            DateHelper::FORMAT_USER_DATE
        );

        $this->agreement->setAttributes($attributes, false);

        if ($this->agreement->save(false)) {
            $this->agreement->generateEssence();

            return true;
        }

        return false;
    }

    /**
     * @return bool
     * @throws
     */
    public function removeModel(): bool
    {
        return $this->agreement->delete();
    }
}
