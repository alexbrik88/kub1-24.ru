<?php

namespace frontend\modules\crm\models;

use common\models\Company;
use yii\db\ActiveRecord;

/**
 * @property-read int $company_id
 * @property bool $show_customers
 */
final class Config extends ActiveRecord
{
    /**
     * @param Company $company
     * @return static
     */
    public static function createInstance(Company $company): self
    {
        return new self([
            'company_id' => $company->id,
            'show_customers' => false,
        ]);
    }

    /**
     * @param Company $company
     * @return static|null
     */
    public static function getInstance(Company $company): ?self
    {
        return self::findOne(['company_id' => $company->id]);
    }

    /**
     * @inheritDoc
     */
    public static function tableName(): string
    {
        return '{{%crm_config}}';
    }
}
