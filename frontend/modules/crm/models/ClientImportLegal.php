<?php

namespace frontend\modules\crm\models;

use common\models\Contractor;
use common\models\company\CompanyType;
use frontend\modules\cash\modules\banking\components\vidimus\ContractorHelper;

final class ClientImportLegal extends AbstractClientImport
{
    const FACE_TYPE = Contractor::TYPE_LEGAL_PERSON;

    public $date;
    public $name;
    public $ITN;
    public $contact_name;
    public $contact_position;
    public $contact_phone;
    public $contact_email;
    public $contact_skype;
    public $deal_type;
    public $campaign;
    public $activity;
    public $client_type;
    public $client_check;
    public $client_reason;
    public $comment;
    public $legal_address;
    public $director_post_name;
    public $director_name;
    public $PPC;
    public $current_account;
    public $BIC;
    public $site_url;

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [
                [
                    'date',
                    'name',
                    'ITN',
                    'contact_name',
                    'contact_position',
                    'contact_phone',
                    'contact_email',
                    'contact_skype',
                    'deal_type',
                    'campaign',
                    'activity',
                    'client_type',
                    'client_check',
                    'client_reason',
                    'comment',
                    'legal_address',
                    'director_post_name',
                    'director_name',
                    'PPC',
                    'current_account',
                    'BIC',
                    'site_url',
                ],
                'trim',
            ],
            [
                [
                    'name',
                    'ITN',
                    'contact_name',
                ],
                'required',
            ],
            [
                ['ITN'],
                'integer',
                'min' => 0,
            ],
            [
                ['ITN'],
                'unique',
                'targetClass' => Contractor::class,
                'filter' => function ($query) {
                    $query->andWhere([
                        'company_id' => $this->company_id,
                        'is_deleted' => false,
                    ]);
                    if (empty($this->PPC)) {
                        $query->andWhere([
                            'PPC' => $this->PPC,
                        ]);
                    } else {
                        $query->andWhere([
                            'or',
                            'PPC' => null,
                            'PPC' => '',
                        ]);
                    }
                },
                'message' => 'Такой клиент уже есть.',
            ],
            [['contact_email'], 'email'],
            [
                ['BIC'], 'string',
                'length' => 9,
            ],
            [
                ['BIC'], 'exist',
                'targetClass' => \common\models\dictionary\bik\BikDictionary::class,
                'targetAttribute' => 'bik',
                'message' => 'Значение "БИК" неверно. Возможно ваш банк изменил БИК. Проверьте на сайте банка.'
            ],
            [
                'BIC', \common\components\validators\BikValidator::className(),
                'related' => [],
            ],
            [
                ['current_account'], 'string',
                'length' => 20,
            ],
        ];
    }

    public function attributeLabels() : array
    {
        return [
            'date' => 'Дата добавления',
            'name' => 'Название',
            'ITN' => 'ИНН',
            'contact_name' => 'ФИО контакта',
            'contact_position' => 'Должность контакта',
            'contact_phone' => 'Телефон контакта',
            'contact_email' => 'E-mail контакта',
            'contact_skype' => 'Skype',
            'deal_type' => 'Тип сделки',
            'campaign' => 'Источник',
            'activity' => 'Вид деятельности',
            'client_type' => 'Тип отношений',
            'client_check' => 'Чек',
            'client_reason' => 'Тип обращения',
            'comment' => 'Комментарий',
            'legal_address' => 'Юридический адрес',
            'director_post_name' => 'Должность руководителя',
            'director_name' => 'ФИО руководителя',
            'PPC' => 'КПП',
            'current_account' => 'Расчетный счет',
            'BIC' => 'БИК',
            'site_url' => 'Сайт',
        ];
    }

    public function createContractorFormData() : array
    {
        list($opf, $name) = ContractorHelper::getOpfAndName(strval($this->name));

        if ($opf === null) {
            $opf = strlen($this->ITN) === 12 ? CompanyType::TYPE_IP : CompanyType::TYPE_OOO;
        }

        return [
            'Contractor' => [
                'face_type' => static::FACE_TYPE,
                'ITN' => (string) $this->ITN,
                'name' => (string) $name,
                'company_type_id' => $opf,
                'PPC' => (string) $this->PPC,
                'legal_address' => (string) $this->legal_address,
                'director_post_name' => (string) $this->director_post_name,
                'director_name' => (string) $this->director_name,
                'current_account' => (string) $this->current_account,
                'BIC' => (string) $this->BIC,
                'campaign_id' => $this->getContractorCampaignId($this->campaign),
                'activity_id' => $this->getContractorActivityId($this->activity),
                'comment' => (string) $this->comment,
            ],
            'ContactForm' => [
                'contact_name' => (string) $this->contact_name,
                'client_position_id' => $this->getClientPositionId($this->contact_position),
                'primary_phone_number' => (string) $this->contact_phone,
                'email_address' => (string) $this->contact_email,
                'skype_account' => (string) $this->contact_skype,
            ],
            'ClientForm' => [
                'client_status_id' => $this->getClientStatusId(ClientStatus::STATUS_ACTIVE),
                'client_check_id' => $this->getClientCheckId($this->client_check),
                'client_reason_id' => $this->getClientReasonId($this->client_reason),
                'client_type_id' => $this->getClientTypeId($this->client_type, $this->defaultClientType),
                'deal_type_id' => $this->getDealTypeId($this->deal_type),
                'site_url' => (string) $this->site_url,
            ],
        ];
    }
}
