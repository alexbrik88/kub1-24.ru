<?php

namespace frontend\modules\crm\models;

use common\models\Contractor;
use yii\validators\RequiredValidator;
use yii\validators\StringValidator;
use yii\validators\Validator;

final class ContractorFormMutator
{
    /** @var Contractor */
    private Contractor $contractor;

    /**
     * @param Contractor $contractor
     */
    public function __construct(Contractor $contractor)
    {
        $this->contractor = $contractor;
    }

    /**
     * @param int $faceType
     * @return void
     */
    public function mutateRequiredValidator(int $faceType): void
    {
        $validators = $this->contractor->getActiveValidators();

        array_walk($validators, function (Validator $validator) use ($faceType) {
            if ($validator instanceof RequiredValidator) {
                $validator->attributes = ($faceType == Contractor::TYPE_PHYSICAL_PERSON)
                    ? ['face_type', 'physical_lastname']
                    : ['face_type', 'company_type_id', 'name'];
            }

            if ($validator instanceof StringValidator && in_array('ITN', $validator->attributes)) {
                $validator->length = [10, 12];
                $validator->init();
            }
        });
    }

    /**
     * @return void
     */
    public function mutateWhenClient(): void
    {
        $validators = $this->contractor->getActiveValidators();

        array_walk($validators, function (Validator $validator) {
            $validator->whenClient = null;
        });
    }
}
