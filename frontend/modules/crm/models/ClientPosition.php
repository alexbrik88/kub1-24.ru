<?php

namespace frontend\modules\crm\models;

use frontend\modules\crm\behaviors\CanDeleteBehavior;

/**
 * Должность клиента
 *
 * @property-read int $client_position_id
 */
final class ClientPosition extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['canDelete'] = [
            'class' => CanDeleteBehavior::class,
            'modelClass' => Contact::class,
            'modelAttribute' => 'client_position_id',
            'hasCompany' => false,
        ];

        return $behaviors;
    }

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%crm_client_position}}';
    }

    /**
     * @inheritDoc
     */
    public static function getDefaultRows(int $company_id): array
    {
        return [
            ['company_id' => $company_id, 'name' => 'Бухгалтер'],
            ['company_id' => $company_id, 'name' => 'Генеральный директор'],
            ['company_id' => $company_id, 'name' => 'Главный бухгалтер'],
            ['company_id' => $company_id, 'name' => 'Директор'],
            ['company_id' => $company_id, 'name' => 'Директор по развитию'],
            ['company_id' => $company_id, 'name' => 'Заместитель генерального директора'],
            ['company_id' => $company_id, 'name' => 'Исполнительный директор'],
            ['company_id' => $company_id, 'name' => 'Коммерческий директор'],
            ['company_id' => $company_id, 'name' => 'Маркетолог'],
            ['company_id' => $company_id, 'name' => 'Менеджер'],
            ['company_id' => $company_id, 'name' => 'Менеджер по персоналу'],
            ['company_id' => $company_id, 'name' => 'Начальник отдела по работе с корпоративными клиентами'],
            ['company_id' => $company_id, 'name' => 'Помощник'],
            ['company_id' => $company_id, 'name' => 'Помощник руководителя'],
            ['company_id' => $company_id, 'name' => 'Руководитель'],
            ['company_id' => $company_id, 'name' => 'Секретарь'],
            ['company_id' => $company_id, 'name' => 'Соучредитель'],
            ['company_id' => $company_id, 'name' => 'Финансовый директор'],
            ['company_id' => $company_id, 'name' => 'Юрист'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'Должность';
    }
}
