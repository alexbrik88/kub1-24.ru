<?php

namespace frontend\modules\crm\models;

use common\models\employee\Employee;

final class DealStageForm extends AbstractTypeForm
{
    /**
     * @var mixed
     */
    public $order;

    /**
     * @var DealType
     */
    private DealType $deal;

    /**
     * @param DealStage $type
     * @param DealType $deal
     * @param Employee $employee
     * @param string $scenario
     */
    public function __construct(DealStage $type, DealType $deal, Employee $employee, string $scenario)
    {
        $this->deal = $deal;

        parent::__construct($type, $employee, $scenario);
    }

    /**
     * @inheritDoc
     */
    public function scenarios(): array
    {
        return [
            self::SCENARIO_CREATE => ['name', 'status', 'order'],
            self::SCENARIO_UPDATE => ['id', 'name', 'status', 'order'],
            self::SCENARIO_DELETE => ['id'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function saveModel(): bool
    {
        if ($this->deal->deal_type_id) {
            $attributes = $this->getAttributes($this->activeAttributes());
            $attributes['company_id'] = $this->employee->company->id;
            $attributes['employee_id'] = $this->employee->id;
            $attributes['deal_type_id'] = $this->deal->deal_type_id;
            $this->type->setAttributes($attributes, false);

            if ($this->type->save()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['name'], 'required'],
            [['name'], 'trim'],
            [['name'], 'string', 'min' => 1, 'max' => 64],
            [['status'], 'required'],
            [['status'], 'boolean'],
            [['order'], 'required'],
            [['order'], 'integer'],
        ];
    }
}
