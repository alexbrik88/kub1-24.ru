<?php

namespace frontend\modules\crm\models;

use common\models\Company;
use frontend\modules\crm\behaviors\CanDeleteBehavior;
use yii\behaviors\AttributeBehavior;

/**
 * Статус клиента
 *
 * @property-read int $client_status_id
 * @property string $value
 */
final class ClientStatus extends AbstractType
{
    /** @var int В работе */
    public const STATUS_IN_WORK = 1;

    /** @var int Отказ */
    public const STATUS_REFUSED = 2;

    /** @var int Активный */
    public const STATUS_ACTIVE = 3;

    /** @var int Неактивный */
    public const STATUS_INACTIVE = 4;

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['canDelete'] = [
            'class' => CanDeleteBehavior::class,
            'modelClass' => Client::class,
            'modelAttribute' => 'client_status_id',
        ];
        $behaviors['attribute'] = [
            'class' => AttributeBehavior::class,
            'attributes' => [self::EVENT_AFTER_FIND => ['canDelete']],
            'value' => fn() => $this->canDelete && $this->value === null,
        ];

        return $behaviors;
    }

    /**
     * @param Company $company
     * @param int $value
     * @return static
     */
    public static function getByStatusValue(Company $company, int $value): self
    {
        return self::findOne(['company_id' => $company->id, 'value' => $value]) ?: new self;
    }

    /**
     * @return bool
     */
    public static function isCreatable(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%crm_client_status}}';
    }

    /**
     * @inheritDoc
     */
    public static function getDefaultRows(int $company_id): array
    {
        return [
            ['company_id' => $company_id, 'value' => self::STATUS_IN_WORK, 'name' => 'В работе'],
            ['company_id' => $company_id, 'value' => self::STATUS_REFUSED, 'name' => 'Отказ'],
            ['company_id' => $company_id, 'value' => self::STATUS_ACTIVE, 'name' => 'Активен'],
            ['company_id' => $company_id, 'value' => self::STATUS_INACTIVE, 'name' => 'Не активен'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'Статус клиента';
    }
}
