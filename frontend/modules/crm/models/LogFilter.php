<?php

namespace frontend\modules\crm\models;

use common\db\SortBuilder;
use common\models\Company;
use common\models\FilterInterface;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\data\Sort;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

final class LogFilter implements FilterInterface
{
    /**
     * @var Company
     */
    private Company $company;

    /**
     * @var string|ActiveRecord
     */
    private string $modelClass;

    /**
     * @var int
     */
    private int $modelId;

    /**
     * @param Company $company
     * @param string $modelClass
     * @param int $modelId
     */

    /**
     * @var LogListFactory
     */
    private $listFactory;

    /**
     * @param Company $company
     * @param ActiveRecord $model
     */
    public function __construct(Company $company, ActiveRecord $model)
    {
        $this->company = $company;
        $this->modelClass = get_class($model);
        $this->modelId = (int) $model->getAttribute($model::primaryKey()[0]);
        $this->listFactory = new LogListFactory($this);
    }

    /**
     * @inheritDoc
     */
    public function getDataProvider(): DataProviderInterface
    {
        return new ActiveDataProvider([
            'query' => $this->createQuery()->with(['company', 'employee']),
            'sort' => $this->getSort(),
            'pagination' => false,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        $builder = new SortBuilder();
        $builder->addOrder('created_at', Log::tableName(), 'created_at');
        $builder->addOrder('created_at', Log::tableName(), 'log_id');
        $builder->addDefaultOrder('created_at', SORT_DESC);

        return $builder->buildSort();
    }

    /**
     * @return LogListFactory
     */
    public function getListFactory(): LogListFactory
    {
        return $this->listFactory;
    }

    /**
     * @return ActiveQuery
     */
    private function createQuery(): ActiveQuery
    {
        return Log::find()->andWhere([
            'company_id' => $this->company->id,
            'model_class' => $this->modelClass,
            'model_id' => $this->modelId,
        ]);
    }
}
