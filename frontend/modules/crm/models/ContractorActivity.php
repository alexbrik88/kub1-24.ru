<?php

namespace frontend\modules\crm\models;

use common\models\Contractor;
use frontend\modules\crm\behaviors\CanDeleteBehavior;

/**
 * Вид деятельности контрагента
 *
 * @property-read int $id
 */
final class ContractorActivity extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['canDelete'] = [
            'class' => CanDeleteBehavior::class,
            'modelClass' => Contractor::class,
            'modelAttribute' => 'activity_id',
        ];

        return $behaviors;
    }

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%contractor_activity}}';
    }

    /**
     * @inheritDoc
     */
    public static function getDefaultRows(int $company_id): array
    {
        return [
            ['company_id' => $company_id, 'name' => 'Оптовая торговля'],
            ['company_id' => $company_id, 'name' => 'Информация и связь'],
            ['company_id' => $company_id, 'name' => 'Недвижимое имущество'],
            ['company_id' => $company_id, 'name' => 'Транспортировка'],
            ['company_id' => $company_id, 'name' => 'Хранение'],
            ['company_id' => $company_id, 'name' => 'Деятельность в области спорта, отдыха и развлечений'],
            ['company_id' => $company_id, 'name' => 'Реклама'],
            ['company_id' => $company_id, 'name' => 'Творческая деятельность'],
            ['company_id' => $company_id, 'name' => 'Маркетинг'],
            ['company_id' => $company_id, 'name' => 'Медицина и здравоохранение'],
            ['company_id' => $company_id, 'name' => 'Общепит'],
            ['company_id' => $company_id, 'name' => 'Гостиничный бизнес'],
            ['company_id' => $company_id, 'name' => 'Издательство'],
            ['company_id' => $company_id, 'name' => 'Радиовещание и телевидение'],
            ['company_id' => $company_id, 'name' => 'Производство'],
            ['company_id' => $company_id, 'name' => 'Строительство'],
            ['company_id' => $company_id, 'name' => 'Другое'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'Вид деятельности';
    }
}
