<?php

namespace frontend\modules\crm\models;

use common\models\employee\Employee;
use yii\base\Model;

abstract class AbstractTypeForm extends Model
{
    /** @var string */
    public const SCENARIO_CREATE = 'create';

    /** @var string */
    public const SCENARIO_UPDATE = 'update';

    /** @var string */
    public const SCENARIO_DELETE = 'delete';

    /**
     * @var mixed
     */
    public $id;

    /**
     * @var mixed
     */
    public $name;

    /**
     * @var mixed
     */
    public $status;

    /**
     * @var AbstractType
     */
    public AbstractType $type;

    /**
     * @var Employee
     */
    protected Employee $employee;

    /**
     * @param AbstractType $type
     * @param Employee $employee
     * @param string $scenario
     */
    public function __construct(AbstractType $type, Employee $employee, string $scenario)
    {
        $this->employee = $employee;
        $this->type = $type;
        $this->scenario = $scenario;

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function init(): void
    {
        $this->id = $this->type->getAttribute($this->type::getIdKey());
        $this->name = $this->type->name;
        $this->status = ($this->type->status !== null) ? $this->type->getStatus(): AbstractType::TYPE_STATUS_ACTIVE;
    }

    /**
     * @param AbstractType $type
     * @param Employee $employee
     * @param string $scenario
     * @return static
     */
    public static function createTypeForm(AbstractType $type, Employee $employee, string $scenario): self
    {
        return new static($type, $employee, $scenario);
    }

    /**
     * @return string[]
     */
    public function attributeLabels(): array
    {
        return [
            'name' => 'Название',
            'status' => 'Статус',
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['name'], 'required'],
            [['name'], 'trim'],
            [['name'], 'string', 'min' => 1, 'max' => 64],
            [
                ['name'],
                'unique',
                'targetClass' => get_class($this->type),
                'targetAttribute' => 'name',
                'filter' => [
                    'and',
                    ['=', 'company_id', $this->employee->company->id],
                    ['not in', $this->type::getIdKey(), [$this->type->getAttribute($this->type::getIdKey())]]
                ],
            ],
            [['status'], 'default', 'value' => AbstractType::TYPE_STATUS_ACTIVE],
            [['status'], 'required'],
            [['status'], 'boolean'],
        ];
    }

    /**
     * @return bool
     * @throws
     */
    public function deleteModel(): bool
    {
        if ($this->type->canDelete) {
            return $this->type->delete();
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function scenarios(): array
    {
        return [
            self::SCENARIO_CREATE => ['name', 'status'],
            self::SCENARIO_UPDATE => ['id', 'name', 'status'],
            self::SCENARIO_DELETE => ['id'],
        ];
    }

    /**
     * @return bool
     */
    abstract public function saveModel(): bool;
}
