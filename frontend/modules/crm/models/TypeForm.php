<?php

namespace frontend\modules\crm\models;

final class TypeForm extends AbstractTypeForm
{
    /**
     * @return bool
     */
    public function saveModel(): bool
    {
        $attributes = $this->getAttributes($this->activeAttributes());
        $attributes['company_id'] = $this->employee->company->id;
        $attributes['employee_id'] = $this->employee->id;
        $this->type->setAttributes($attributes, false);

        return $this->type->save(false);
    }
}
