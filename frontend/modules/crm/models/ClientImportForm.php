<?php

namespace frontend\modules\crm\models;

use common\models\Contractor;
use frontend\modules\crm\services\Client\ImportFromExcelService;
use yii\base\Model;
use yii\web\UploadedFile;

final class ClientImportForm extends Model
{
    public ?UploadedFile $file = null;

    private $result;

    /**
     * @var ImportFromExcelService
     */
    private ImportFromExcelService $importService;

    /**
     * @param ImportFromExcelService $importService
     */
    public function __construct(ImportFromExcelService $importService)
    {
        $this->importService = $importService;

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels(): array
    {
        return [
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['file'], 'required'],
            [
                ['file'],
                'file',
                'mimeTypes' => ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
            ],
        ];
    }

    /**
     * @return bool
     */
    public function import(): bool
    {
        $this->result = null;

        if (!$this->validate()) {
            $this->result = $this->getFirstError('file');

            return false;
        }

        $importStatus = $this->importService->import($this->file->tempName);
        $this->result = $this->importService->importReport();

        return $importStatus;
    }

    /**
     * @return bool
     */
    public function getImportReport(): array
    {
        return (array) $this->result;
    }
}
