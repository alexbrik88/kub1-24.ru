<?php

namespace frontend\modules\crm\models;

use Yii;
use frontend\modules\crm\behaviors\CanDeleteBehavior;

/**
 * This is the model class for table "crm_task_result".
 *
 * @property int $task_result_id
 * @property int $company_id
 * @property int|null $employee_id
 * @property int $status_id
 * @property string $name
 * @property int $created_at
 * @property int $updated_at
 */
class TaskResult extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['canDelete'] = [
            'class' => CanDeleteBehavior::class,
            'modelClass' => Task::class,
            'modelAttribute' => 'task_result_id',
        ];

        return $behaviors;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_task_result';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Текст варианта ответа',
            'status' => 'Статус',
        ];
    }

    /**
     * @inheritDoc
     */
    public static function getDefaultRows(int $company_id): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'Вариант ответа в "Результат по задаче"';
    }
}
