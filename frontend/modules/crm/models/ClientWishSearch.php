<?php

namespace frontend\modules\crm\models;

use common\models\Contractor;
use common\models\EmployeeCompany;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * ClientWishSearch represents the model behind the search form of `frontend\modules\crm\models\ClientWish`.
 */
class ClientWishSearch extends ClientWish
{
    private Contractor $_contractor;
    private $_data = [];
    private $query;

    /**
     * Constructor
     */
    public function __construct(Contractor $contractor, $params = [])
    {
        $this->_contractor = $contractor;

        parent::__construct($params);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number', 'responsible_employee_id', 'status_id'], 'integer'],
            [['date', 'text'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientWish::find();

        $query->andWhere(['contractor_id' => $this->_contractor->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'number' => $this->number,
            'responsible_employee_id' => $this->responsible_employee_id,
            'status_id' => $this->status_id,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text]);

        $this->query = clone $query;

        return $dataProvider;
    }

    public function statusFilterItems()
    {
        if (empty($this->query)) return [];

        if (!isset($this->_data['statusFilterItems'])) {
            $this->_data['statusFilterItems'] = ['' => 'Все'];
            $itemIdArray = (clone $this->query)->select('status_id')->distinct()->column();

            foreach (ClientWish::$statuses as $key => $value) {
                if (in_array($key, $itemIdArray)) {
                    $this->_data['statusFilterItems'][$key] = $value;
                }
            }
        }

        return $this->_data['statusFilterItems'];
    }

    public function responsibleFilterItems()
    {
        if (empty($this->query)) return [];

        if (!isset($this->_data['responsibleFilterItems'])) {
            $itemIdArray = (clone $this->query)->select('responsible_employee_id')->distinct()->column();
            $items = EmployeeCompany::find()->where([
                'company_id' => $this->_contractor->company_id,
                'employee_id' => $itemIdArray,
            ])->orderBy([
                'lastname' => SORT_ASC,
                'firstname' => SORT_ASC,
                'patronymic' => SORT_ASC,
            ])->all();
            $this->_data['responsibleFilterItems'] = ['' => 'Все'] + ArrayHelper::map($items, 'employee_id', 'shortFio');
        }

        return $this->_data['responsibleFilterItems'];
    }
}
