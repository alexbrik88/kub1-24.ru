<?php

namespace frontend\modules\crm\models;

use yii\validators\InlineValidator;
use Yii;

trait ValidateTypeTrait
{
    /**
     * @param string $attribute
     * @param array|null $params
     * @param InlineValidator $validator
     * @return void
     */
    public function validateType(string $attribute, ?array $params, InlineValidator $validator): void
    {
        if (!$this->hasErrors($attribute)) {
            $typeList = (new ListFactory)->createTypeList($params['modelClass'], null, true);

            if (!isset($typeList->getItems()[$this->$attribute])) {
                $validator->addError($this, $attribute, Yii::t('yii', '{attribute} is invalid.'));
            }
        }
    }
}
