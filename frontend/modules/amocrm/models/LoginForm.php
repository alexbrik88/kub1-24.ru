<?php

namespace frontend\modules\amocrm\models;

use Yii;
use common\models\Company;
use common\models\EmployeeCompany;
use common\models\amocrm\AmocrmWidget;
use common\models\amocrm\AmocrmWidgetUser;
use common\models\employee\Employee;
use Lcobucci\JWT\Token;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\di\Instance;
use yii\web\UnauthorizedHttpException;

class LoginForm extends Model
{
    const SCENARIO_LOGIN = 'login';
    const SCENARIO_COMPANY = 'company';

    public $username;
    public $password;
    public $company_id;

    private $token;
    private $widget;
    private $_user = false;

    public function __construct(Token $token, ?array $config = [])
    {
        $this->token = $token;

        parent::__construct($config);
    }

    public function init()
    {
        parent::init();

        if ($accountId = $this->token->getClaim('account_id')) {
            $this->widget = AmocrmWidget::findOne($accountId);
        }
    }

    public function scenarios() : array
    {
        return [
            self::SCENARIO_LOGIN => [
                'username',
                'password',
            ],
            self::SCENARIO_COMPANY => [
                'username',
                'password',
                'company_id',
            ],
        ];
    }

    public function rules() : array
    {
        return [
            [['username'], 'trim'],
            [
                [
                    'username',
                    'password',
                    'company_id',
                ],
                'required',
            ],
            ['password', 'validatePassword'],
            ['company_id', 'validateCompany'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неверный пользователь или пароль.');
            }
        }
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateCompany($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $items = $this->getDropdownItems();
            if (!$items || !in_array($this->company_id, array_keys($items))) {
                $this->addError($attribute, 'Необходимо выбрать компанию, для которой подключается интеграция.');
            }
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return Employee|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Employee::findIdentityByLogin($this->username);
        }

        return $this->_user;
    }

    public function attributeLabels() : array
    {
        return [
            'username' => 'Логин',
            'password' => 'Пароль',
            'company_id' => 'Компания',
        ];
    }

    public function getDropdownItems() : array
    {
        if ($user = $this->getUser()) {
            if ($this->widget) {
                $companyArray = $user->getCompanies()->andWhere([
                    'id' => $this->widget->company_id,
                ])->all();
            } else {
                $companyArray = $user->companiesByChief;
            }

            return ArrayHelper::map($companyArray, 'id', 'shortTitle');
        }

        return [];
    }

    public function save() : bool
    {
        if (!$this->validate() || $this->scenario != self::SCENARIO_COMPANY) {
            return false;
        }

        $data = $this->token->getClaims();

        $widget = $this->widget ?: new AmocrmWidget([
            'account_id' => (string) $data['account_id'],
            'employee_id' => $this->getUser()->id,
            'company_id' => $this->company_id,
            'user_id' => (string) $data['user_id'],
            'client_uuid' => (string) $data['client_uuid'],
            'iss' => (string) $data['iss'],
            'aud' => (string) $data['aud'],
        ]);
        $user = AmocrmWidgetUser::findOne([
            'account_id' => (string) $data['account_id'],
            'user_id' => (string) $data['user_id'],
        ]) ?: new AmocrmWidgetUser([
            'account_id' => (string) $data['account_id'],
            'user_id' => (string) $data['user_id'],
            'employee_id' => $this->getUser()->id,
            'company_id' => $this->company_id,
            'client_uuid' => (string) $data['client_uuid'],
            'jti' => (string) $data['jti'],
            'iss' => (string) $data['iss'],
            'aud' => (string) $data['aud'],
        ]);

        return Yii::$app->db->transaction(function ($db) use ($widget, $user) {
            if ((!$widget->getIsNewRecord() || $widget->save()) && (!$user->getIsNewRecord() || $user->save(false))) {
                return true;
            }

            $db->transaction->rollBack();

            return false;
        });
    }
}
