<?php

namespace frontend\modules\amocrm\modules\iframe\helpers;

class Url extends \yii\helpers\BaseUrl
{
    protected static $token = false;

    public static function token() : ?string
    {
        if (self::$token === false) {
            self::$token = $_GET['access-token'] ?? null;
        }

        return self::$token;
    }

    public static function toRoute($route, $scheme = false)
    {
        $route = (array) $route;

        if ($scheme === false) {
            $route['access-token'] = self::token();
        }

        return parent::toRoute($route, $scheme);
    }
}
