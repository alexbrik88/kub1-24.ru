<?php

namespace frontend\modules\amocrm\modules\iframe;

use Yii;
use frontend\modules\amocrm\modules\iframe\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Request;
use yii\web\Response;

/**
 * iframe module definition class
 */
class Module extends \yii\base\Module
{
    public static $noAuthActions = [
        'amocrm/iframe/default/error',
        'amocrm/iframe/default/page-load',
    ];

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'frontend\modules\amocrm\modules\iframe\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        Yii::$app->errorHandler->errorAction = '/amocrm/iframe/default/error';

        Yii::$app->params['bsVersion'] = '4';

        Yii::$app->set('user', [
            'class' => 'frontend\components\WebUser',
            'identityClass' => 'common\models\employee\Employee',
            'enableSession' => false,
            'loginUrl' => null,
            'on afterLogin' => function ($event) {
                if ($event->identity->is_active == false) {
                    throw new ForbiddenHttpException("Ваш аккаунт заблокирован. Обратитесь в службу технической поддержки.");
                }
            },
        ]);
    }

    /**
     * This method is invoked right before an action within this module is executed.
     *
     * @param yii\base\Action $action the action to be executed.
     * @return bool whether the action should continue to be executed.
     */
    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        $lead_id = Yii::$app->request->get('lead_id');
        if ($lead_id !== null && strval(intval($lead_id)) !== $lead_id) {
            throw new BadRequestHttpException(Yii::t('yii', 'Invalid data received for parameter "{param}".', [
                'param' => 'lead_id',
            ]));
        }

        Yii::$app->controller->enableCsrfValidation = false;

        return true;
    }
}
