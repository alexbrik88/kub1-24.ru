<?php

use kartik\select2\Select2;

/* @var $model common\models\product\Product */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $this yii\web\View */

?>

<?= $form->field($model, 'login')->hiddenInput()->label(false) ?>

<?= $form->field($model, 'password')->hiddenInput()->label(false) ?>

<?= $form->field($model, 'company_id')->widget(Select2::class, [
    'size' => Select2::SMALL,
    'data' => ['' => '']+$model->getDropdownItems(),
    'hideSearch' => true,
    'pluginOptions' => [
        'width' => '100%'
    ],
    'options' => [
        'class' => 'form-control form-control-sm',
    ]
]) ?>
