<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\modules\amocrm\models\InstallForm */

$this->registerJs(<<<JS
    $(document).on("submit", "#install-form", function (e) {
        e.preventDefault();
        let data = $(this).serialize();
        $.pjax({
            url: this.actions,
            data: data,
            container: "#content-container",
        });
    });
JS
);
?>

<?php $form = ActiveForm::begin([
    'id' => 'install-form',
    'fieldConfig' => Yii::$app->params['amocrm']['fieldConfig'],
    'enableClientValidation' => false,
    'enableAjaxValidation' => false,
]); ?>

    <div class="form-body">
        <?= $this->render('_install_'.$model->scenario, [
            'model' => $model,
            'form' => $form,
        ]); ?>

        <div class="mt-3 d-flex justify-content-between">
            <?= Html::submitButton('Войти', [
                'id' => 'product-form-submit',
                'class' => 'btn btn-sm btn-kub-primary ladda-button',
            ]) ?>
        </div>
    </div>

<?php ActiveForm::end() ?>
