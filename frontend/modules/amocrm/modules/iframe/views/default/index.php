<?php

/* @var $this yii\web\View */
/* @var $url string */

if (isset($url)) {
    $this->registerJs(<<<JS
AMO.loadPage("{$url}");
JS
    );
}
