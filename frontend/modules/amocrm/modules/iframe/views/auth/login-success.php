<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\modules\amocrm\models\LoginForm */

$this->registerJs(<<<JS
    AMO.loginSuccess();
JS
);
?>
