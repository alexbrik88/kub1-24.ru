<?php

use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model frontend\modules\amocrm\models\LoginForm */

$data = ['' => '']+$model->getDropdownItems();
?>

<?= $form->field($model, 'username')->hiddenInput()->label(false) ?>

<?= $form->field($model, 'password')->hiddenInput()->label(false) ?>

<?= $form->field($model, 'company_id')->widget(Select2::class, [
    'size' => Select2::SMALL,
    'data' => $data,
    'hideSearch' => true,
    'pluginOptions' => [
        'width' => '100%'
    ],
    'options' => [
        'class' => 'form-control form-control-sm',
    ]
]) ?>