<?php

use common\models\document\status\InvoiceStatus;
use frontend\components\Icon;
use frontend\models\Documents;
use frontend\modules\amocrm\modules\iframe\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */

$relatedId = 'related_documents_'.$model->id;
$token = Yii::$app->request->get('access-token');

$relatedButtons = [];
if (!$model->has_upd) {
    if ($model->has_services) {
        if ($model->can_add_act) {
            $relatedButtons[] = Html::button(Icon::get('add-icon').' Акт', [
                'title' => 'Создать Акт',
                'class' => 'create_related_doc btn btn-sm btn-kub-secondary mr-2',
                'data' => [
                    'url' => Url::to(['act/create', 'invoiceId' => $model->id, 'type' => $model->type, 'access-token' => $token]),
                ],
            ]);
        } elseif ($doc = $model->acts[0] ?? null) {
            if ($doc->uid == null) {
                $doc->updateAttributes([
                    'uid' => $doc::generateUid(),
                ]);
            }
            $relatedButtons[] = Html::button('Акт № '.$doc->getFullNumber(), [
                'class' => 'amocrm_action btn btn-sm btn-kub-secondary mr-2',
                'data' => [
                    'action-data' => [
                        'action' => 'getBrTab',
                        'data' => [
                            'url' => Url::to([
                                "/documents/act/out-view",
                                'uid' => $doc->uid,
                                'view' => 'pdf',
                            ], true)
                        ],
                    ],
                ],
            ]);
        }
    }
    if ($model->has_goods) {
        if ($model->can_add_packing_list) {
            $relatedButtons[] = Html::button(Icon::get('add-icon').' ТН', [
                'title' => 'Создать Товарную накладную',
                'class' => 'create_related_doc btn btn-sm btn-kub-secondary mr-2',
                'data' => [
                    'url' => Url::to(['packing-list/create', 'invoiceId' => $model->id, 'type' => $model->type, 'access-token' => $token]),
                ],
            ]);
        } elseif ($doc = $model->packingLists[0] ?? null) {
            if ($doc->uid == null) {
                $doc->updateAttributes([
                    'uid' => $doc::generateUid(),
                ]);
            }
            $relatedButtons[] = Html::button('ТН № '.$doc->getFullNumber(), [
                'class' => 'amocrm_action btn btn-sm btn-kub-secondary mr-2',
                'data' => [
                    'action-data' => [
                        'action' => 'getBrTab',
                        'data' => [
                            'url' => Url::to([
                                "/documents/packing-list/out-view",
                                'uid' => $doc->uid,
                                'view' => 'pdf',
                            ], true)
                        ],
                    ],
                ],
            ]);
        }
    }
    if ($model->getHasNds()) {
        if ($model->can_add_invoice_facture) {
            $relatedButtons[] = Html::button(Icon::get('add-icon').' СФ', [
                'title' => 'Создать Счет-фактуру',
                'class' => 'create_related_doc btn btn-sm btn-kub-secondary mr-2',
                'data' => [
                    'url' => Url::to(['invoice-facture/create', 'invoiceId' => $model->id, 'type' => $model->type, 'access-token' => $token]),
                ],
            ]);
        } elseif ($doc = $model->invoiceFactures[0] ?? null) {
            if ($doc->uid == null) {
                $doc->updateAttributes([
                    'uid' => $doc::generateUid(),
                ]);
            }
            $relatedButtons[] = Html::button('СФ № '.$doc->getFullNumber(), [
                'class' => 'amocrm_action btn btn-sm btn-kub-secondary mr-2',
                'data' => [
                    'action-data' => [
                        'action' => 'getBrTab',
                        'data' => [
                            'url' => Url::to([
                                "/documents/invoice-facture/out-view",
                                'uid' => $doc->uid,
                                'view' => 'pdf',
                            ], true)
                        ],
                    ],
                ],
            ]);
        }
    }
}
if (!($model->has_act || $model->has_packing_list || $model->has_invoice_facture)) {
    if ($model->can_add_upd) {
        $relatedButtons[] = Html::button(Icon::get('add-icon').' УПД', [
            'title' => 'Создать УПД',
            'class' => 'create_related_doc btn btn-sm btn-kub-secondary mr-2',
            'data' => [
                'url' => Url::to(['upd/create', 'invoiceId' => $model->id, 'type' => $model->type, 'access-token' => $token]),
            ],
        ]);
    } elseif ($doc = $model->upds[0] ?? null) {
        if ($doc->uid == null) {
            $doc->updateAttributes([
                'uid' => $doc::generateUid(),
            ]);
        }
        $relatedButtons[] = Html::button('УПД № '.$doc->getFullNumber(), [
            'class' => 'amocrm_action btn btn-sm btn-kub-secondary mr-2',
            'data' => [
                'action-data' => [
                    'action' => 'getBrTab',
                    'data' => [
                        'url' => Url::to([
                            "/documents/upd/out-view",
                            'uid' => $doc->uid,
                            'view' => 'pdf',
                        ], true)
                    ],
                ],
            ],
        ]);
    }
}
?>

<div class="mt-2 related_documents_wrap">
    <?= implode("\n", $relatedButtons) ?>

    <?= Icon::get('question', [
        'class' => 'tooltip-question-icon js-tooltip',
        'title' => implode('<br>', [
            'Акт - можно создать когда в счете есть услуги.',
            'Товарная накладная (ТН) - можно создать когда в счете есть товары.',
            'Счет-фактура (СФ) - можно создать когда счет с НДС.',
            'УПД - можно создать когда по счету не созданы другие документы.',
        ]),
        'data' => [
            'trigger' => 'hover',
        ],
    ]) ?>
</div>