<?php
use common\components\TextHelper;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\NdsOsno;
use common\models\document\Invoice;
use common\models\project\ProjectEstimate;
use frontend\models\Documents;
use yii\bootstrap4\Html;
use frontend\modules\documents\components\InvoiceHelper;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $projectEstimateId integer */

$currCode = $model->currency_name == Currency::DEFAULT_NAME ? '' : " ({$model->currency_name})";
$projectEstimate = ProjectEstimate::findOne(['id' => $projectEstimateId]);

$this->registerCss('
    #select2-projectestimate-nds_out-container {
        color: #0097fd;
    }
');

?>

<table id="invoice-sum" class="total-txt text-right table-resume">
    <tr class="mb-1 discount_column<?= $model->has_discount ? '': ' hidden'; ?>">
        <td class="label">
            Сумма скидки<?php if ($currCode):?> <span class="total_currency_label"><?= $currCode ?></span><?php endif; ?>:
        </td>
        <td>
            <strong class="discount_sum"><?= TextHelper::moneyFormatFromIntToFloat($model->view_total_discount); ?></strong>
        </td>
    </tr>
    <tr class="">
        <td class="label">
            Итого<?php if ($currCode):?> <span class="total_currency_label"><?= $currCode ?></span><?php endif; ?>:
        </td>
        <td>
            <strong class="total_price"><?= TextHelper::moneyFormatFromIntToFloat($model->view_total_amount); ?></strong>
        </td>
    </tr>
    <?php if ($projectEstimate && !is_null($projectEstimate->nds_out)): ?>
        <tr class="">
            <td class="label" style="padding-top: 3px">
                <?php \yii\widgets\Pjax::begin([
                    'id' => 'nds_view_type_id-pjax-container',
                    'enablePushState' => false,
                    'linkSelector' => false,
                    'options' => ['style' => 'display: inline-block;']
                    ]); ?>
                            <?= \kartik\select2\Select2::widget([
                    'hideSearch' => true,
                    'model' => $model,
                    'attribute' => 'nds_view_type_id',
                    'data' => Invoice::$ndsViewList,
                    'options' => [
                    'class' => 'form-control nds-select',
                    'data' => ['id' => $projectEstimate->nds_out],
                    ],
                    'pluginOptions' => [
                    'width' => '100%',
                    ]
                    ]) ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </td>
            <td>
                <strong class="including_nds"><?= TextHelper::moneyFormatFromIntToFloat($model->view_total_nds); ?></strong>
            </td>
        </tr>
    <?php elseif ($model->type == Documents::IO_TYPE_IN): ?>
        <tr class="">
            <td class="label" style="padding-top: 3px">
                <?php \yii\widgets\Pjax::begin([
                    'id' => 'nds_view_type_id-pjax-container',
                    'enablePushState' => false,
                    'linkSelector' => false,
                    'options' => ['style' => 'display: inline-block;']
                ]); ?>
                    <?= \kartik\select2\Select2::widget([
                        'hideSearch' => true,
                        'model' => $model,
                        'attribute' => 'nds_view_type_id',
                        'data' => Invoice::$ndsViewList,
                        'options' => [
                            'class' => 'form-control nds-select',
                            'data' => ['id' => $model->nds_view_type_id],
                        ],
                        'pluginOptions' => [
                            'width' => '100%',
                        ]
                    ]) ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </td>
            <td>
                <strong class="including_nds"><?= TextHelper::moneyFormatFromIntToFloat($model->view_total_nds); ?></strong>
            </td>
        </tr>
    <?php else: ?>
        <tr class="nds-view-item type-0 <?= $model->nds_view_type_id == 0 ? '' : 'hidden'; ?>">
            <td class="label">
                <span>В том числе НДС:</span>
            </td>
            <td>
                <strong class="including_nds"><?= TextHelper::moneyFormatFromIntToFloat($model->view_total_nds); ?></strong>
            </td>
        </tr>
        <tr class="nds-view-item type-1 <?= $model->nds_view_type_id == 1 ? '' : 'hidden'; ?>">
            <td class="label">
                НДС сверху:
            </td>
            <td>
                <strong class="including_nds"><?= TextHelper::moneyFormatFromIntToFloat($model->view_total_nds); ?></strong>
            </td>
        </tr>
        <tr class="nds-view-item type-2 <?= $model->nds_view_type_id == 2 ? '' : 'hidden'; ?>">
            <td class="label">
                Без налога (НДС)
            </td>
            <td>
                <?= Html::activeHiddenInput($model, 'nds_view_type_id', [
                    'data' => ['id' => $model->nds_view_type_id],
                ]) ?>
                <strong>-</strong>
            </td>
        </tr>
    <?php endif; ?>
    <tr class="">
        <td class="label">
            Всего к оплате<span class="total_currency_label"><?= $currCode ?></span>:
        </td>
        <td>
            <strong class="amount"><?= TextHelper::moneyFormatFromIntToFloat($model->view_total_with_nds); ?></strong>
        </td>
    </tr>
</table>


<?php
if ($model->type == Documents::IO_TYPE_IN) {
$this->registerJs('
    var checkInInvoiceNds = function() {
        if ($("#invoice-nds_view_type_id").val() == "2") {
            $(".with-nds-item").addClass("hidden");
            $(".without-nds-item").removeClass("hidden");
        } else {
            $(".with-nds-item").removeClass("hidden");
            $(".without-nds-item").addClass("hidden");
        }
    }
    $(document).on("change", "#invoice-nds_view_type_id", function() {
        checkInInvoiceNds();
    });
    $(document).on("change", "#invoice-contractor_id", function() {
        $.pjax.reload("#nds_view_type_id-pjax-container", {"type": "post", "data": $(this).closest("form").serialize()});
        $(document).on("pjax:complete", "#nds_view_type_id-pjax-container", function(){
            INVOICE.recalculateInvoiceTable();
            checkInInvoiceNds();

            $("select.nds-select").select2({
                theme: "krajee-bs4",
                width: "100%",
                minimumResultsForSearch: -1,
                escapeMarkup: function(markup) {
                    return markup;
                }
            });
        });
    });
');
}
?>
