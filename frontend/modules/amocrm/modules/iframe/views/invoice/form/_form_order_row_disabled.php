<?php

use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\product\Product;
use common\models\product\ProductUnit;
use common\models\TaxRate;
use frontend\models\Documents;
use kartik\select2\Select2;
use yii\helpers\Html;
use common\components\ImageHelper;

/* @var \common\models\document\Order $order */
/* @var integer $ioType */
/* @var integer $number */
/* @var integer $precision */
/* @var string $ndsCellClass */

$baseName = 'orderArray[' . $number . ']';
$isExist = Order::find()->where(['id' => $order->id])->exists();
$prodType = $order->productionType;
if ($order->quantity != intval($order->quantity)) {
    $order->quantity = rtrim(number_format($order->quantity, 10, '.', ''), 0);
}
$taxRate = ($model->type == Documents::IO_TYPE_IN) ? $order->purchaseTaxRate : $order->saleTaxRate;
$taxRateId = $taxRate->id;
$taxRateName = $taxRate->name;
$unitItems = ArrayHelper::map(ProductUnit::findSorted()->all(), 'id', 'name');
$taxRates = TaxRate::sortedArray();
$taxItems = ArrayHelper::map($taxRates, 'id', 'name');
$taxOptions = [];
foreach ($taxRates as $rate) {
    $taxOptions[$rate->id] = ['data-rate' => $rate->rate];
}
?>

<tr id="model_<?= $order->product_id; ?>" class="product-row" role="row">
    <td class="product-delete delete-column-left" style="white-space: nowrap;">
        <div style="max-width: 45px">
        </div>
    </td>
    <td class="col_invoice_form_article<?= $userConfig->invoice_form_article ? '' : ' hidden'; ?>">
        <div style="max-width: 100px" class="order-param-value">
            <?= $order->product->article ?>
        </div>
    </td>
    <td style="position: relative;">
        <!-- FLOAT WIDTH INPUT -->
        <div style="max-width: 300px">
        <input readonly type="text" class="product-title form-control form-control-sm tooltip-product"
               style="padding-right: 25px; width: 100%;"
                name="<?= $baseName; ?>[title]"
                data-value = "<?= $order->productTitle; ?>"
                value="<?= $order->productTitle; ?>">
        <?php if ($order->isDeleteAllowed) : ?>
            <span class="product-title-clear">×</span>
        <?php endif ?>
        </div>
    </td>
    <td>
        <input readonly type="hidden" class="tax-rate" disabled="disabled"
               value="<?= $ioType == Documents::IO_TYPE_OUT ? $order->saleTaxRate->rate : $order->purchaseTaxRate->rate; ?>"/>

        <input readonly type="hidden" class="order-id"
               name="<?= $baseName; ?>[id]" value="<?= $order->id; ?>"/>
        <input readonly type="hidden" class="product-id"
               name="<?= $baseName; ?>[product_id]" value="<?= $order->product_id; ?>"/>
        <input readonly type="hidden" class="max-product-count"
               name="<?= $baseName; ?>[max_product_count]" value="<?= $order->max_product_count; ?>" />


        <!-- FLOAT WIDTH input -->
        <div style="width: 100px">
        <?php if (!$order->unit || $order->unit->name == Product::DEFAULT_VALUE) : ?>
            <?= Html::hiddenInput($baseName . '[count]', 1, [
                'class' => 'product-count',
                'data-value' => 1,
                'readonly' => true
            ]) ?>
            <span><?= Product::DEFAULT_VALUE ?></span>
        <?php else : ?>
            <?= Html::input($order->isHour ? 'text' : 'number', $baseName . '[count]', $order->quantity, [
                'class' => 'product-count form-control-number form-control form-control-sm' . ($order->dependentNumber > 1 ? ' tooltip2' : ''),
                'data-value' => $order->quantity,
                'data-hour' => $order->isHour ? 'true' : 'false',
                'min' => 0,
                'max' => Order::MAX_QUANTITY,
                'step' => 'any',
                'lang' => 'en',
                'readonly' => true,
                'data-tooltip-content' => '#tooltip_' . ($prodType ? 'goods' : 'service') . '_no_edit',
            ]); ?>
        <?php endif ?>
        </div>
    </td>
    <td class="">
        <div style="width: 90px" class="order-param-value product-unit-name">
            <?= Select2::widget([
                'readonly' => true,
                'name' => $baseName . '[unit_id]',
                'data' => ['' => Product::DEFAULT_VALUE] + $unitItems,
                'options' => [],
                'value' => $order->unit_id,
                'pluginOptions' => [
                    'minimumResultsForSearch' => -1,
                    'width' => '100%'
                ],
            ]); ?>
            <?php // echo $order->unit ? $order->unit->name : Product::DEFAULT_VALUE; ?>
        </div>
    </td>
    <td class="<?= $ndsCellClass ?>">
        <div style="width: 90px" class="order-param-value price-for-sell-nds-name" data-id="<?= $taxRateId ?>" data-name="<?= $taxRateName ?>">
            <?= Select2::widget([
                'readonly' => true,
                'name' => $baseName . ($ioType == Documents::IO_TYPE_OUT ? '[sale_tax_rate_id]' : '[purchase_tax_rate_id]'),
                'data' => $taxItems,
                'options' => [
                    'class' => 'order_product_tax_rate',
                    'options' => $taxOptions,
                ],
                'value' => $ioType == Documents::IO_TYPE_OUT ? $order->sale_tax_rate_id : $order->purchase_tax_rate_id,
                'pluginOptions' => [
                    'minimumResultsForSearch' => -1,
                    'width' => '100%'
                ],
            ]); ?>
            <?php // echo $taxRateName ?>
        </div>
    </td>
    <td class="price-one">
        <!-- FLOAT WIDTH INPUT -->
        <div style="width: 120px">
        <?= Html::input('number', $baseName . '[price]', TextHelper::moneyFormatFromIntToFloat($order->view_price_base, $precision), [
            'class' => 'form-control-number form-control form-control-sm price-input',
            'data-value' => TextHelper::moneyFormatFromIntToFloat($order->view_price_base, $precision),
            'min' => 0,
            'max' => Order::MAX_PRICE,
            'step' => 'any',
            'readonly' => true
        ]); ?>
        </div>
    </td>
    <td class="discount discount_column<?= $model->has_discount ? '': ' hidden'; ?>">
        <!-- FLOAT WIDTH INPUT -->
        <div style="width: 120px">
        <?= Html::input('number', $baseName . '[discount]', $order->discount, [
            'class' => 'form-control form-control-sm discount-input',
            'data-value' => $order->discount,
            'min' => 0,
            'max' => Order::MAX_DISCOUNT,
            'step' => 'any',
            'readonly' => true
        ]); ?>
        </div>
    </td>
    <td class="discount_column<?= $model->has_discount ? '': ' hidden'; ?>">
        <div style="width:120px" class="order-param-value">
            <span class="price-one-with-nds">
                <?= TextHelper::moneyFormatFromIntToFloat($order->view_price_one, $precision) ?>
            </span>
        </div>
    </td>
    <td class="markup markup_column<?= $model->has_markup ? '': ' hidden'; ?>">
        <!-- FLOAT WIDTH INPUT -->
        <div style="width: 120px">
        <?= Html::input('number', $baseName . '[markup]', $order->markup, [
            'class' => 'form-control-number form-control form-control-sm markup-input',
            'data-value' => $order->markup,
            'min' => 0,
            'max' => Order::MAX_MARKUP,
            'step' => 'any',
            'readonly' => true
        ]); ?>
        </div>
    </td>
    <td class="markup_column<?= $model->has_markup ? '': ' hidden'; ?>">
        <div style="width:100px" class="order-param-value">
            <span class="price-one-with-nds">
                <?= TextHelper::moneyFormatFromIntToFloat($order->view_price_one, $precision) ?>
            </span>
        </div>
    </td>
    <td class="weight weight_column<?= $model->has_weight ? '' : ' hidden'; ?>">
        <!-- FLOAT WIDTH INPUT -->
        <div style="width: 120px">
        <?= Html::input('number', $baseName . '[weight]', $order->weight, [
            'class' => 'form-control-number form-control form-control-sm weight-input',
            'data-value' => $order->weight,
            'min' => 0,
            'step' => 'any',
            'readonly' => true
        ]); ?>
        </div>
    </td>
    <td class="volume volume_column<?= $model->has_volume ? '' : ' hidden'; ?>">
        <!-- FLOAT WIDTH INPUT -->
        <div style="width: 120px">
        <?= Html::input('number', $baseName . '[volume]', $order->volume, [
            'class' => 'form-control-number form-control form-control-sm volume-input',
            'data-value' => $order->volume,
            'min' => 0,
            'step' => 'any',
            'readonly' => true
        ]); ?>
        </div>
    </td>
    <td class="" style="text-align: right;">
        <div class="order-param-value price-with-nds">
            <?= TextHelper::moneyFormatFromIntToFloat($order->view_total_amount, $precision); ?>
        </div>
    </td>
</tr>

<style type="text/css">
    select[readonly].select2-hidden-accessible + .select2-container {
        pointer-events: none;
        touch-action: none;
    }
    #table-product-list-body .select2-selection {
        background-color: #e9ecef!important;
        box-shadow: none;
    }
</style>