<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */

$this->title = 'Выставить счет';
?>
<div class="invoice-create">

    <?= $this->render('form/_form', [
        'model' => $model,
        'company' => $company,
        'ioType' => $ioType,
        'invoiceContractEssence' => $invoiceContractEssence,
        'invoiceEssence' => $invoiceEssence,
    ]) ?>

</div>
