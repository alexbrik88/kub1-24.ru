<?php

use common\components\TextHelper;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductUnit;
use common\models\product\Store;
use common\models\TaxRate;
use frontend\models\Documents;
use frontend\modules\amocrm\modules\iframe\helpers\Url;
use frontend\widgets\ProductGroupDropdownWidget;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use frontend\rbac\permissions;
use frontend\components\XlsHelper;

/* @var $model common\models\product\Product */
/* @var $isService bool */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $this yii\web\View */
/* @var $document string */
/* @var $documentType int */

$taxItems = ArrayHelper::map(TaxRate::sortedArray(), 'id', 'name');

$disabled = isset($model->production_type) && in_array($model->production_type, [
    Product::PRODUCTION_TYPE_SERVICE,
    Product::PRODUCTION_TYPE_GOODS,
]) ? null : true;

$productGroupData = [];
$productUnitsData = [];
$productUnitsOptions = [];
if (isset($model->production_type)) {
    $productGroupArray = ProductGroup::getGroups(null, $model->production_type);
    $productGroupData = ArrayHelper::map($productGroupArray, 'id', 'title');

    $units = ProductUnit::findSorted()
        ->andWhere(($model->production_type == Product::PRODUCTION_TYPE_SERVICE) ? ['services' => 1] : ['goods' => 1])
        ->all();
    $productUnitsData = ArrayHelper::map($units, 'id', 'name');
    foreach ($units as $unit) {
        $productUnitsOptions[$unit->id] = ['title' => $unit->title];
    }
} else {
    $productGroupData = ['' => ''];
    $productUnitsData = ['' => ''];
}

$isService = ((string)$model->production_type == (string)Product::PRODUCTION_TYPE_SERVICE);

echo Html::activeHiddenInput($model, 'production_type', [
    'id' => 'production_type_input',
]);

$types = Product::$productionTypesOne;
unset($types[2]);
$types = array_reverse($types, true);


$isFirstCreate = strpos(Yii::$app->request->referrer, 'first-create');
$isCreateFor2Type = strpos(Yii::$app->request->referrer, 'create?type=2');

$this->registerJs('
$(document).on("change", "#product-not_for_sale", function() {
    $("input.for_sale_input").prop("disabled", this.checked);
    $("input.for_sale_input:radio:not(.md-radiobtn)");
    if (this.checked) {
        $("#product-price_for_sell_with_nds").val("");
        $(".field-product-price_for_sell_with_nds").removeClass("has-error");
        $(".field-product-price_for_sell_with_nds .help-block-error").html("");
    }
});
');

?>

<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'production_type')->label('Тип')->radioList($types, [
            'class' => 'radio-list',
            'item' => function ($index, $label, $name, $checked, $value) {
                $label = Html::label(Html::radio($name, $checked, ['value' => $value]) . ' ' . $label);

                return Html::a($label, Url::current(['type' => $value]), [
                    'class' => 'mr-4 product-form-pjax-link',
                    'style' => 'color: #001424',
                ]);
            }
        ])->inline() ?>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'title')->textInput([
            'maxlength' => true,
            'disabled' => $disabled,
        ]) ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'group_id')->widget(Select2::class, [
            'size' => 'sm',
            'data' => $productGroupData,
            'options' => [
                'disabled' => $disabled,
            ],
            'pluginOptions' => [
                'width' => '100%'
            ],
        ]); ?>
    </div>

    <?php if ($documentType == Documents::IO_TYPE_IN) : ?>
        <div class="col-6">
            <?= $form->field($model, 'price_for_buy_with_nds')->textInput([
                'disabled' => $disabled,
                'value' => $model->price_for_buy_with_nds == null ? '' : TextHelper::moneyFormatFromIntToFloat($model->price_for_buy_with_nds),
            ]) ?>
        </div>
    <?php endif ?>
    <?php if ($documentType == Documents::IO_TYPE_OUT) : ?>
        <div class="col-6">
            <?= $form->field($model, 'price_for_sell_with_nds')->textInput([
                'disabled' => $disabled,
                'value' => $model->price_for_sell_with_nds == null ? '' : TextHelper::moneyFormatFromIntToFloat($model->price_for_sell_with_nds),
            ]) ?>
        </div>
    <?php endif ?>

    <?php if ($company->hasNds()) : ?>
        <?php if ($documentType == Documents::IO_TYPE_IN) : ?>
            <div class="col-6">
                <?= $form->field($model, 'price_for_buy_nds_id')->label()->radioList($taxItems, [
                    'item' => function ($index, $label, $name, $checked, $value) use ($disabled) {
                        $input = Html::radio($name, $checked, [
                            'value' => $value,
                            'disabled' => $disabled,
                        ]);

                        return Html::tag('label', $input . $label, [
                            'class' => 'mr-2',
                        ]);
                    },
                ]) ?>
            </div>
        <?php endif ?>
        <?php if ($documentType == Documents::IO_TYPE_OUT) : ?>
            <div class="col-6">
                <?= $form->field($model, 'price_for_sell_nds_id')->label()->radioList($taxItems, [
                    'item' => function ($index, $label, $name, $checked, $value) use ($disabled) {
                        $input = Html::radio($name, $checked, [
                            'value' => $value,
                            'disabled' => $disabled,
                        ]);

                        return Html::tag('label', $input . $label, [
                            'class' => 'mr-2',
                        ]);
                    },
                ]) ?>
            </div>
        <?php endif ?>
    <?php endif ?>

    <div class="col-6">
        <?= $form->field($model, 'product_unit_id')->widget(Select2::class, [
            'size' => 'sm',
            'data' => $productUnitsData,
            'options' => [
                'options' => $productUnitsOptions,
                'disabled' => $disabled,
            ],
            'pluginOptions' => [
                'width' => '100%'
            ]
        ]) ?>
    </div>
</div>

<?php if ($model->production_type == Product::PRODUCTION_TYPE_GOODS) : ?>
<div>
    <span class="forProduct link link_collapse link_bold mb-4 collapsed" type="button" data-toggle="collapse" data-target="#forProduct2" aria-expanded="true" aria-controls="forProduct2">
        <span class="link-txt">Дополнительные поля</span>
        <svg class="link-shevron svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
        </svg>
    </span>
</div>

<div id="forProduct2" class="collapse forProduct">
    <div class="dopColumns">
        <div class="row">
            <?php if ($documentType == Documents::IO_TYPE_OUT) : ?>
                <div class="col-6">
                    <?= $form->field($model, 'price_for_buy_with_nds')->textInput([
                        'disabled' => $disabled,
                        'value' => $model->price_for_buy_with_nds == null ? '' : TextHelper::moneyFormatFromIntToFloat($model->price_for_buy_with_nds),
                    ]) ?>
                </div>
            <?php endif ?>
            <?php if ($documentType == Documents::IO_TYPE_IN) : ?>
                <div class="col-6">
                    <?= $form->field($model, 'price_for_sell_with_nds')->textInput([
                        'disabled' => $disabled,
                        'value' => $model->price_for_sell_with_nds == null ? '' : TextHelper::moneyFormatFromIntToFloat($model->price_for_sell_with_nds),
                    ]) ?>
                </div>
            <?php endif ?>

            <?php if ($company->hasNds()) : ?>
                <?php if ($documentType == Documents::IO_TYPE_OUT) : ?>
                    <div class="col-6">
                        <?= $form->field($model, 'price_for_buy_nds_id')->label()->radioList($taxItems, [
                            'item' => function ($index, $label, $name, $checked, $value) use ($disabled) {
                                $input = Html::radio($name, $checked, [
                                    'value' => $value,
                                    'disabled' => $disabled,
                                ]);

                                return Html::tag('label', $input . $label, [
                                    'class' => 'mr-2',
                                ]);
                            },
                        ]) ?>
                    </div>
                <?php endif ?>
                <?php if ($documentType == Documents::IO_TYPE_IN) : ?>
                    <div class="col-6">
                        <?= $form->field($model, 'price_for_sell_nds_id')->label()->radioList($taxItems, [
                            'item' => function ($index, $label, $name, $checked, $value) use ($disabled) {
                                $input = Html::radio($name, $checked, [
                                    'value' => $value,
                                    'disabled' => $disabled,
                                ]);

                                return Html::tag('label', $input . $label, [
                                    'class' => 'mr-2',
                                ]);
                            },
                        ]) ?>
                    </div>
                <?php endif ?>
            <?php endif ?>
        </div>
        <?= $this->render('_product_params', [
            'model' => $model,
            'form' => $form,
        ]) ?>
    </div>
</div>
<?php endif ?>
