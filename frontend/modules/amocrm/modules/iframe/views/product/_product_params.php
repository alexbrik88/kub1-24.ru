<?php
use common\models\address\Country;
use common\models\product\Product;
use common\models\product\ProductUnit;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $model common\models\product\Product */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $this yii\web\View */

$storeId = $model->productStoreByStore ? $model->productStoreByStore->store_id : null;
?>

<div class="row">
    <div class="col-6">
        <?= $form->field($model, "article")->textInput([
            'placeholder' => Product::DEFAULT_VALUE,
        ]); ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, "initQuantity[$storeId]")->textInput([
            'id' => 'product-initQuantity',
            'placeholder' => Product::DEFAULT_VALUE,
        ]); ?>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'code')->textInput([
            'placeholder' => Product::DEFAULT_VALUE,
        ]); ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'box_type')->textInput([
            'placeholder' => Product::DEFAULT_VALUE,
        ]); ?>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'count_in_package')->textInput([
            'placeholder' => Product::DEFAULT_VALUE,
        ]); ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'mass_net')->textInput([
            'placeholder' => Product::DEFAULT_VALUE,
            'disabled' => isset(ProductUnit::$countableUnits[$model->mass_gross]),
        ]); ?>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'mass_gross')->textInput([
            'placeholder' => Product::DEFAULT_VALUE,
            'disabled' => isset(ProductUnit::$countableUnits[$model->mass_gross]),
        ]); ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'country_origin_id')->widget(Select2::class, [
            'size' => 'sm',
            'data' => ArrayHelper::map(Country::getCountries(), 'id', 'name_short'),
            'options' => [
                'placeholder' => '',
            ],
            'pluginOptions' => [
                'width' => '100%',
                'minimumResultsForSearch' => -1
            ]
        ]); ?>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'customs_declaration_number')->textInput([
            'placeholder' => Product::DEFAULT_VALUE,
        ]); ?>
    </div>
</div>

<?php
$massGrossId = \yii\helpers\Html::getInputId($model, 'mass_gross');
$massNetId = \yii\helpers\Html::getInputId($model, 'mass_net');
$productUnitId = \yii\helpers\Html::getInputId($model, 'product_unit_id');
$countableUnits = json_encode(array_keys(ProductUnit::$countableUnits));
$js = <<<JS
    var productMassGross = $('#$massGrossId');
    var productMassNet = $('#$massNetId');
    var countableUnits = $countableUnits;
    $('#$productUnitId').on('change init', '', function (e) {
        productMassGross.attr('disabled', countableUnits.indexOf(parseInt(this.value)) === -1);
        productMassNet.attr('disabled', countableUnits.indexOf(parseInt(this.value)) === -1);
    }).trigger('init');

JS;
$this->registerJs($js);
?>
