<?php
use common\models\product\Product;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
/* @var $form yii\widgets\ActiveForm */
/* @var $documentType int */
/* @var $document string */

?>

<?php Pjax::begin([
    'id' => 'product-form-pjax',
    'linkSelector' => '.product-form-pjax-link',
    'enablePushState' => false,
]) ?>

<?php $form = ActiveForm::begin([
    'id' => 'new-product-invoice-form',
    'fieldConfig' => Yii::$app->params['amocrm']['fieldConfig'],
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
]); ?>

    <div class="form-body">
        <?= Html::hiddenInput('documentType', $model->production_type); ?>

        <?= $this->render('_main_form', [
            'company' => $company,
            'model' => $model,
            'form' => $form,
            'documentType' => $documentType,
            'document' => null,
        ]); ?>

        <div class="mt-3 d-flex justify-content-between">
            <?= Html::submitButton('Сохранить', [
                'id' => 'product-form-submit',
                'class' => 'btn btn-sm btn-kub-primary ladda-button',
            ]) ?>
            <?= Html::submitButton('Отменить', [
                'class' => 'btn btn-sm btn-kub-secondary',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
    </div>

<?php ActiveForm::end() ?>

<?php Pjax::end() ?>
