<?php

use common\components\date\DateHelper;
use common\models\Contractor;
use common\models\company\CompanyType;
use frontend\models\Documents;
use frontend\modules\amocrm\modules\iframe\helpers\Url;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use kartik\date\DatePicker;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $cancelUrl string */

$types = [
    Contractor::TYPE_LEGAL_PERSON => 'Юридическое лицо',
    Contractor::TYPE_PHYSICAL_PERSON => 'Физическое лицо',
];

$datepickerConfig = Yii::$app->params['amocrm']['datepickerConfig'];
if (isset($model->physical_passport_date_output)) {
    $model->physical_passport_date_output = date_create($model->physical_passport_date_output)->format(DateHelper::FORMAT_USER_DATE);
}

if ($model->isNewRecord) {
    $contractorTypeList = [
        Contractor::TYPE_LEGAL_PERSON => 'Юридическое лицо',
        Contractor::TYPE_PHYSICAL_PERSON => 'Физическое лицо'];
} else {
    if ($model->face_type == Contractor::TYPE_LEGAL_PERSON)
        $contractorTypeList = [Contractor::TYPE_LEGAL_PERSON => 'Юридическое лицо'];
    else
        $contractorTypeList = [Contractor::TYPE_PHYSICAL_PERSON => 'Физическое лицо'];
}

$typeLabel = $model->type == Contractor::TYPE_SELLER ? 'Тип поставщика ' : 'Тип покупателя ';
$showContractorType = Yii::$app->request->post('show_contractor_type');
$innPlaceholder = 'Автозаполнение по ИНН' . ($showContractorType ? ($documentType == Documents::IO_TYPE_OUT ? ' покупателя' : ' продавца') : '');
$innLabel = 'ИНН' . ($showContractorType ? ($documentType == Documents::IO_TYPE_OUT ?  ' покупателя' : ' продавца') : '');
$companyTypes = $model->getTypeArray();
$companyTypeJs = json_encode(array_flip($companyTypes));
?>

<?php Pjax::begin([
    'id' => 'contractor-form-pjax',
    'linkSelector' => '.contractor-form-pjax-link',
    'formSelector' => false,
    'enablePushState' => false,
]) ?>

<?php $form = ActiveForm::begin([
    'id' => 'new-contractor-invoice-form',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldConfig' => Yii::$app->params['amocrm']['fieldConfig'],
]); ?>

<?= Html::hiddenInput('documentType', $model->type); ?>

<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'face_type')->label($typeLabel)->radioList($types, [
            'class' => 'radio-list',
            'item' => function ($index, $label, $name, $checked, $value) {
                $label = Html::label(Html::radio($name, $checked, ['value' => $value]) . ' ' . $label);

                return Html::a($label, Url::current(['face_type' => $value]), [
                    'class' => 'mr-4 contractor-form-pjax-link',
                    'style' => 'color: #001424',
                ]);
            }
        ])->inline() ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'ITN')->label($innLabel)->textInput([
            'placeholder' => $model->face_type == Contractor::TYPE_PHYSICAL_PERSON ? null : $innPlaceholder,
            'maxlength' => true,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
                'prod' => YII_ENV_PROD ? 1 : 0,
            ],
            'disabled' => $model->isNewRecord ? null : true,
        ]); ?>
    </div>
</div>

<?php if ($model->face_type == Contractor::TYPE_LEGAL_PERSON) : ?>
    <?= $this->render('_partial_main_info', [
        'model' => $model,
        'form' => $form,
        'documentType' => $model->type,
        'companyTypes' => $companyTypes,
    ]) ?>
    <?= $this->render('_partial_details', [
        'form' => $form,
        'model' => $model,
    ]) ?>
<?php endif ?>
<?php if ($model->face_type == Contractor::TYPE_PHYSICAL_PERSON) : ?>
    <?= $this->render('_partial_fiz_face', [
        'form' => $form,
        'model' => $model,
    ]) ?>
<?php endif ?>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('Сохранить', [
        'class' => 'btn btn-sm btn-kub-primary',
        'data-style' => 'expand-right',
    ]) ?>
    <?= Html::button('Отменить', [
        'class' => 'btn btn-sm btn-kub-secondary',
        'data-dismiss' => 'modal'
    ]); ?>
</div>

<?php ActiveForm::end(); ?>

<?php $this->registerJs(<<<JS
    var contractorIsNewRecord = $("#contractor-is_new_record").val();
    var companyType = $companyTypeJs;
    $('[id="contractor-itn"]').suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: '78497656dfc90c2b00308d616feb9df60c503f51',
        type: 'PARTY',
        count: 10,

        onSelect: function(suggestion) {
            var companyTypeId = '-1';
            if (!empty(suggestion.data.opf) && !empty(companyType[suggestion.data.opf.short])) {
                companyTypeId = companyType[suggestion.data.opf.short];
            }
            $('#contractor-name').val(suggestion.data.name.full);
            $('#contractor-itn').val(suggestion.data.inn);
            $('#contractor-ppc').val(suggestion.data.kpp);
            $('#contractor-bin').val(suggestion.data.ogrn);
            if (!empty(suggestion.data.management)) {
                $('#contractor-director_name').val(suggestion.data.management.name);
                $('#contractor-director_post_name').val(suggestion.data.management.post);
            }
            $('#contractor-companytypeid').val(companyTypeId).trigger('change');
            if ($('#contractor-companytypeid').val() == companyType['ИП']) {
                $('#contractor-director_name').val(suggestion.data.name.full);
                $('.field-contractor-ppc').hide();
            } else {
                $('.field-contractor-ppc').show();
            }
            var address = '';
            if (suggestion.data.address.data && suggestion.data.address.data.postal_code) {
                if (suggestion.data.address.value.indexOf(suggestion.data.address.data.postal_code) == -1) {
                    address += suggestion.data.address.data.postal_code + ', ';
                }
            }
            address += suggestion.data.address.value;
            $('#contractor-legal_address').val(address);
            $('#contractor-actual_address').val(address);
        }
    });
JS
) ?>

<?php Pjax::end() ?>
