<?php

use frontend\components\Icon;
use frontend\modules\amocrm\modules\iframe\helpers\Url;
use kartik\date\DatePicker;
use yii\helpers\Html;

/* @var $form yii\widgets\ActiveForm */
/* @var $model common\models\Contractor */

$datepickerConfig = Yii::$app->params['amocrm']['datepickerConfig'];
$toggleText = 'Указать дополнительные данные необходимые для акта, товарной накладной, счет-фактуры';
$toggleLabel = Html::tag('span', $toggleText, ['class' => 'link-txt']);
$toggleIcon = Icon::get('shevron', ['class' => 'link-shevron']);
?>

<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'physical_lastname')->label('Фамилия')->textInput([
            'maxlength' => true,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
            ],
        ]); ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'physical_firstname')->label('Имя')->textInput([
            'maxlength' => true,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
            ],
        ]); ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'physical_patronymic')->label('Отчество')->textInput([
            'maxlength' => true,
            'readonly' => (boolean)$model->physical_no_patronymic,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
            ],
        ]); ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'physical_no_patronymic', [
            'checkTemplate' => "{label}\n<div class=\"form-control-block-sm px-0\">{input}</div>\n{hint}\n{error}",
        ])->label('Нет отчества')->checkbox([], false); ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'physical_address')->textInput([
            'maxlength' => true,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
            ],
        ])->label('Адрес регистрации') ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'contact_email')->textInput([
            'maxlength' => true,
        ])->label('Email покупателя (для отправки счетов)</span>') ?>
    </div>
</div>

<div class="mb-3">
    <?= Html::tag('span', $toggleLabel." ".$toggleIcon, [
        'class' => 'link link_collapse link_bold cursor-pointer collapsed',
        'data-toggle' => 'collapse',
        'data-target' => '#dopColumns3',
        'aria-expanded' => 'false',
        'aria-controls' => 'dopColumns3',
    ]) ?>
</div>

<div id="dopColumns3" class="collapse dopColumns">
    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'physical_passport_series')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9{2} 9{2}',
                'options' => [
                    //'class' => 'form-control',
                    'placeholder' => 'XX XX',
                ],
            ]); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'physical_passport_number')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9{6}',
                'options' => [
                    //'class' => 'form-control',
                    'placeholder' => 'XXXXXX',
                ],
            ]); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'physical_passport_date_output')
                ->label('Дата выдачи')
                ->widget(DatePicker::class, $datepickerConfig) ?>
        </div>
    </div>
</div>
