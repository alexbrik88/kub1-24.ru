<?php

namespace frontend\modules\amocrm\modules\iframe\controllers;

use Yii;
use frontend\modules\amocrm\components\CheckAccess;
use frontend\modules\amocrm\models\LoginForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;

/**
 * Auth controller for the `iframe` module
 */
class AuthController extends Controller
{
    public $layout = 'main';

    /**
     * @var Lcobucci\JWT\Token
     */
    private $token;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'actions' => [
                    'check-access',
                ],
            ],
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'actions' => [
                            'check-access',
                        ],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            if ($token = Yii::$app->request->getHeaders()->get('X-Auth-Token')) {
                                $this->token = Yii::$app->jwt->loadToken($token);
                            }

                            if (empty($this->token)) {
                                throw new UnauthorizedHttpException('Your request was made with invalid credentials.');
                            }

                            return true;
                        },
                    ],
                    [
                        'actions' => [
                            'login',
                        ],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            $this->token = Yii::$app->jwt->loadToken(Yii::$app->request->get('access-token'));
                            if (empty($this->token)) {
                                throw new UnauthorizedHttpException('Your request was made with invalid credentials.');
                            }

                            return true;
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionCheckAccess()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $checkAccess = new CheckAccess($this->token);

        return [
            'jwt' => strval($this->token),
            'isWidgetOk' => $checkAccess->isWidgetOk,
            'isUserOk' => $checkAccess->isUserOk,
            'token' => $checkAccess->token,
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'main';

        $model = new LoginForm($this->token, [
            'scenario' => LoginForm::SCENARIO_LOGIN,
        ]);

        $postData = Yii::$app->request->post();
        if (isset($postData[$model->formName()]['company_id'])) {
            $model->scenario = LoginForm::SCENARIO_COMPANY;
        }

        if ($model->load($postData) && $model->validate()) {
            if ($model->scenario == LoginForm::SCENARIO_LOGIN) {
                $model->scenario = LoginForm::SCENARIO_COMPANY;
                $companyList = $model->getDropdownItems();
                if (count($companyList) === 1) {
                    $model->company_id = array_key_first($companyList);
                }
            } elseif ($model->save()) {
                return $this->render('login-success', [
                    'model' => $model,
                ]);
            }
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }
}
