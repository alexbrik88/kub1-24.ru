<?php

namespace frontend\modules\amocrm\modules\iframe\controllers;

use Yii;
use yii\web\Controller;

/**
 * Default controller for the `amocrm` module
 */
class DefaultController extends Controller
{
    public $layout = 'main';

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => \yii\web\ErrorAction::className(),
            ],
        ];
    }
}
