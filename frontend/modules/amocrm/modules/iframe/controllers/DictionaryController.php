<?php

namespace frontend\modules\amocrm\modules\iframe\controllers;

use common\components\AddressDictionaryHelper;
use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\Ifns;
use common\models\address\AddressDictionary as Fias;
use common\models\dictionary\address\AddressDictionary;
use common\models\dictionary\bik\BikDictionary;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\product\Product;
use frontend\rbac\permissions;
use Yii;
use yii\web\Controller;
use yii\helpers\Html;

/**
 * Dictionary controller
 */
class DictionaryController extends Controller
{
    /**
     * @var string
     */
    public $layout = '//view/layouts/clear';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'authenticator' => [
                'class' => 'frontend\modules\amocrm\components\QueryParamAuth',
            ],
            'ajax' => [
                'class' => 'yii\filters\AjaxFilter',
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionBik($q = '')
    {
        $bikArray = [];
        $q = trim($q);
        if ($q !== '') {
            $bikArray = BikDictionary::find()
                ->byActive()
                ->byBikFilter($q)
                ->limit(20)
                ->asArray()
                ->all();
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $bikArray;
    }

    /**
     * @param $type
     * @param $q
     * @return array
     */
    public function actionAddress($type, $q)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $searcher = new AddressDictionary($type, $q, Yii::$app->request->get());

        return (array) $searcher->search();
    }

    /**
     * @param null $companyId
     * @param $q
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionEmployee($companyId = null, $q)
    {
        $companyId = $companyId ? $companyId : Yii::$app->user->identity->company->id;
        $companyIdArray = Yii::$app->user->identity->getCompanies()->andWhere([
            '!=', 'id', $companyId
        ])->select('id')->column();
        $employeeArray = [];
        $q = trim($q);
        if ($q !== '') {
            $employeeArray = Employee::find()->alias('employee')
                ->distinct()
                ->joinWith('employeeCompany employee_company', false)
                ->leftJoin([
                    'employee_company_exists' => EmployeeCompany::tableName(),
                ], '{{employee_company_exists}}.[[employee_id]] = {{employee}}.[[id]] AND {{employee_company_exists}}.[[company_id]] = :company', [
                    ':company' => $companyId,
                ])
                ->select(['employee.email', 'employee_company.*', 'DATE_FORMAT({{employee_company}}.[[date_hiring]], "%d.%m.%Y") as date_hiring_format',
                    'DATE_FORMAT({{employee_company}}.[[birthday]], "%d.%m.%Y") as birthday_format',
                    'DATE_FORMAT({{employee_company}}.[[date_dismissal]], "%d.%m.%Y") as date_dismissal_format',])
                ->andWhere(['employee_company_exists.company_id' => null])
                ->andWhere([
                    'or',
                    ['employee_company.company_id' => $companyIdArray],
                    ['employee.id' => Employee::SUPPORT_KUB_EMPLOYEE_ID],
                ])
                ->andWhere(['employee.email' => $q])
                ->byIsDeleted(Employee::NOT_DELETED)
                ->limit(1)
                ->asArray()
                ->all();
        }
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $employeeArray;
    }

    /**
     * @param $q
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionIfns()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $q = ltrim(Yii::$app->request->get('q'), '0');
        if ($q !== null) {
            $query = Ifns::find()
                ->select('ga, gb, g1, g2, g4, g6, g7, g8, g9, g11')
                ->where(['like', 'ga', $q.'%', false])
                ->limit(10);

            $result = $query->all();

            array_walk($result, function (&$data) {
                $data->g1 = implode(', ', array_filter(explode(',', $data->g1)));
            });

            return $result;
        }

        return [];
    }

    /**
     * @return array
     */
    public function actionFias($q = null, $level, $guid = null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (/*empty($q) || */ empty($level) || (empty($guid) && $level != 1)) {
            return [];
        }

        $query = Fias::find()
            ->select(['AOGUID', 'FULLNAME'])
            ->where(['AOLEVEL' => $level, 'ACTSTATUS' => 1])
            ->andWhere(['like', 'FULLNAME', $q . '%', false])
            ->orderBy(['FORMALNAME' => SORT_ASC])
            ->limit(100);

        if ($guid && $level != 1) {
            $query->andWhere(['PARENTGUID' => $guid]);
        }

        return $query->all();
    }

    /**
     * @return array
     */
    public function actionFiasItem($guid = null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return ['result' => $guid ? Fias::findOne(['AOGUID' => $guid, 'ACTSTATUS' => 1]) : null];
    }
}
