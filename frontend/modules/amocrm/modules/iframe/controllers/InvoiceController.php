<?php

namespace frontend\modules\amocrm\modules\iframe\controllers;

use Yii;
use common\components\date\DateHelper;
use common\models\document\Invoice;
use common\models\document\InvoiceContractEssence;
use common\models\document\InvoiceEssence;
use frontend\models\Documents;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\modules\amocrm\modules\iframe\models\InvoiceSearch;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * InvoiceController implements the CRUD actions for Invoice model.
 */
class InvoiceController extends Controller
{
    public $layout = 'main';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'authenticator' => [
                'class' => 'frontend\modules\amocrm\components\QueryParamAuth',
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'actions' => [
                            'index',
                        ],
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\document\Document::INDEX,
                        ],
                        'roleParams' => function () {
                            return [
                                'ioType' => Documents::IO_TYPE_OUT,
                            ];
                        },
                    ],
                    [
                        'actions' => [
                            'create',
                        ],
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\document\Document::CREATE,
                        ],
                        'roleParams' => function () {
                            return [
                                'ioType' => Documents::IO_TYPE_OUT,
                            ];
                        },
                    ],
                    [
                        'actions' => [
                            'view',
                            'item-view',
                        ],
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\document\Document::VIEW,
                        ],
                        'roleParams' => function () {
                            return [
                                'model' => $this->findModel(Yii::$app->request->get('id')),
                            ];
                        },
                    ],
                    [
                        'actions' => [
                            'update',
                        ],
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\document\Document::UPDATE,
                        ],
                        'roleParams' => function () {
                            return [
                                'model' => $this->findModel(Yii::$app->request->get('id')),
                            ];
                        },
                    ],
                    [
                        'actions' => [
                            'delete',
                        ],
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\document\Document::DELETE,
                        ],
                        'roleParams' => function () {
                            return [
                                'model' => $this->findModel(Yii::$app->request->get('id')),
                            ];
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Displays a single Invoice model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionIndex($lead_id)
    {
        $searchModel = new InvoiceSearch([
            'company_id' => Yii::$app->user->identity->company->id,
            'account_id' => Yii::$app->amocrmWidget->account_id,
            'lead_id' => $lead_id,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Invoice model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionItemView($id)
    {
        return $this->renderPartial('_item_view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Invoice model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($lead_id, $type = Documents::IO_TYPE_OUT)
    {
        $this->checkType($type);

        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        if (!$company->createInvoiceAllowed($type)) {
            throw new ForbiddenHttpException('Для текущего тарифа исчерпан лимит счетов.');
        }

        $invoiceContractEssence = $company->invoiceContractEssence ?: new InvoiceContractEssence([
            'company_id' => $company->id,
        ]);
        $invoiceEssence = $company->invoiceEssence ?: new InvoiceEssence([
            'company_id' => $company->id,
        ]);

        $model = new Invoice();
        $model->scenario = 'insert';
        $model->company_id = $company->id;
        $model->type = $type;
        $model->price_precision = 2;
        $model->document_date = date(DateHelper::FORMAT_DATE);
        $model->is_invoice_contract = false;
        $model->contract_essence_template = Invoice::CONTRACT_ESSENCE_TEMPLATE_EMPTY;
        $model->nds_view_type_id = $company->nds_view_type_id;
        $model->isAutoinvoice = 0;
        $model->document_number = (string) Invoice::getNextDocumentNumber($company->id, $type, null, $model->document_date);
        $model->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime("+10 day"));
        $model->show_article = $employee->config->invoice_form_article;
        if (ArrayHelper::getValue($company, ['invoiceEssence', 'is_checked'])) {
            $model->comment = $company->invoiceEssence->text;
        }

        $lastInvoice = $company->getInvoices()->andWhere([
            'type' => $type,
            'is_deleted' => false,
        ])->orderBy([
            'id' => SORT_DESC,
        ])->one();

        if ($lastInvoice !== null) {
            $model->is_invoice_contract = $lastInvoice->is_invoice_contract;
        }

        $model->populateRelation('company', $company);

        if (Yii::$app->request->isPjax) {
            $model->load(Yii::$app->request->post());
            if (($contractor = $model->contractor) !== null && $contractor->nds_view_type_id !== null) {
                $model->nds_view_type_id = $contractor->nds_view_type_id;
            }

            return $this->renderAjax('create', [
                'model' => $model,
                'company' => $company,
                'ioType' => $type,
                'invoiceContractEssence' => $invoiceContractEssence,
                'invoiceEssence' => $invoiceEssence,
            ]);
        }

        if (InvoiceHelper::load($model, Yii::$app->request->post()) && InvoiceHelper::save($model)) {
            Yii::$app->amocrmWidget->link('invoices', $model, ['lead_id' => $lead_id]);
            if ($invoiceContractEssence->load(Yii::$app->request->post())) {
                $invoiceContractEssence->text = $model->contract_essence;
                if (($invoiceContractEssence->is_checked && $invoiceContractEssence->text) ||
                    !$invoiceContractEssence->is_checked) {
                    $invoiceContractEssence->save();
                }
            }
            if ($invoiceEssence->load(Yii::$app->request->post())) {
                $invoiceEssence->text = $model->comment;
                if (($invoiceEssence->is_checked && $invoiceEssence->text) ||
                    !$invoiceEssence->is_checked) {
                    $invoiceEssence->save();
                }
            }

            return $this->redirect([
                'view',
                'id' => $model->id,
                'access-token' => Yii::$app->request->get('access-token'),
            ]);
        }

        return $this->render('create', [
            'model' => $model,
            'company' => $company,
            'ioType' => $type,
            'invoiceContractEssence' => $invoiceContractEssence,
            'invoiceEssence' => $invoiceEssence,
        ]);
    }

    /**
     * Updates an existing Invoice model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $type)
    {
        $model = $this->findModel($id, $type);

        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        $model->populateRelation('company', $company);

        $invoiceContractEssence = $company->invoiceContractEssence ?: new InvoiceContractEssence([
            'company_id' => $company->id,
        ]);
        $invoiceEssence = $company->invoiceEssence ?: new InvoiceEssence([
            'company_id' => $company->id,
        ]);

        if (Yii::$app->request->isPjax) {
            $model->load(Yii::$app->request->post());
            if (($contractor = $model->contractor) !== null && $contractor->nds_view_type_id !== null) {
                $model->nds_view_type_id = $contractor->nds_view_type_id;
            }

            return $this->renderAjax('create', [
                'model' => $model,
                'company' => $company,
                'ioType' => $type,
                'invoiceContractEssence' => $invoiceContractEssence,
                'invoiceEssence' => $invoiceEssence,
            ]);
        }

        if (InvoiceHelper::load($model, Yii::$app->request->post()) && InvoiceHelper::save($model)) {
            if ($invoiceContractEssence->load(Yii::$app->request->post())) {
                $invoiceContractEssence->text = $model->contract_essence;
                if (($invoiceContractEssence->is_checked && $invoiceContractEssence->text) ||
                    !$invoiceContractEssence->is_checked) {
                    $invoiceContractEssence->save();
                }
            }
            if ($invoiceEssence->load(Yii::$app->request->post())) {
                $invoiceEssence->text = $model->comment;
                if (($invoiceEssence->is_checked && $invoiceEssence->text) ||
                    !$invoiceEssence->is_checked) {
                    $invoiceEssence->save();
                }
            }

            return $this->redirect([
                'view',
                'id' => $model->id,
                'access-token' => Yii::$app->request->get('access-token'),
            ]);
        }

        return $this->render('update', [
            'model' => $model,
            'company' => $company,
            'ioType' => $model->type,
            'invoiceContractEssence' => $invoiceContractEssence,
            'invoiceEssence' => $invoiceEssence,
        ]);
    }

    /**
     * Deletes an existing Invoice model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $type)
    {
        $model = $this->findModel($id, $type);

        Yii::$app->db->transaction(function (Connection $db) use ($model) {

            if ($model->deletingAll()) {
                Yii::$app->session->setFlash('success', 'Счет удален');

                return true;
            }
            \Yii::$app->session->setFlash('error', $model->getFirstError('id') ?: "Ошибка при удалении счета.");
            $db->transaction->rollBack();

            return false;
        });

        return $this->redirect(['default/blank']);
    }

    /**
     * @param integer $type
     * @throws NotFoundHttpException if the type is not valid
     */
    protected function checkType($type)
    {
        $typeArray = [Documents::IO_TYPE_IN, Documents::IO_TYPE_OUT];

        if (!in_array($type, $typeArray)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Invoice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Invoice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $type = null, $throw = true)
    {
        $companyId = ArrayHelper::getValue(Yii::$app->user, ['identity', 'company', 'id']);

        $model = Invoice::find()->andWhere([
            'id' => $id,
            'company_id' => $companyId,
            'is_deleted' => false,
        ])->andFilterWhere([
            'type' => $type,
        ])->one();

        if ($throw && $model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $model;
    }
}
