<?php

namespace frontend\modules\amocrm\modules\iframe\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\amocrm\AmocrmWidgetInvoice;
use common\models\document\Invoice;

/**
 * InvoiceSearch represents the model behind the search form of `common\models\document\Invoice`.
 */
class InvoiceSearch extends Invoice
{
    public $account_id;
    public $lead_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'type',
                    'invoice_status_id',
                ],
                'safe',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Invoice::find()->joinWith('amocrmWidgetInvoices link', false);

        $query->andWhere([
            'invoice.company_id' => $this->company_id,
            'invoice.is_deleted' => false,
            'link.account_id' => $this->account_id,
            'link.lead_id' => $this->lead_id,
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'document_date' => SORT_DESC,
                    'document_number' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'type' => $this->type,
            'invoice_status_id' => $this->invoice_status_id,
        ]);

        $query->andFilterWhere(['like', 'uid', $this->uid])
            ->andFilterWhere(['like', 'document_number', $this->document_number]);

        return $dataProvider;
    }
}
