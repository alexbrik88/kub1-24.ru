<?php

namespace frontend\modules\amocrm\modules\iframe\assets;

use Yii;
use yii\web\AssetBundle;
use yii\web\View;

/**
 * CommonAsset
 */
class CommonAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/modules/amocrm/modules/iframe/assets/web';

    /**
     * @var array
     */
    public $css = [
    ];

    /**
     * @var array
     */
    public $js = [
        'js/common.js',
    ];

    /**
     * @var array
     */
    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\widgets\PjaxAsset',
    ];
}
