<?php

namespace frontend\modules\amocrm\modules\iframe\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * CustomAsset
 */
class CustomAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/modules/amocrm/modules/iframe/assets/web';

    /**
     * @var array
     */
    public $css = [
        'css/tooltipster-kub.css',
        'css/style.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'js/script.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'frontend\modules\amocrm\modules\iframe\assets\AmocrmIframeAsset',
    ];
}
