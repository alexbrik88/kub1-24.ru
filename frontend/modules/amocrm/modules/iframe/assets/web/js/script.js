var arrow = '<svg class="svg-icon" viewBox="0 0 12 6"><path d="M.427 2.008A1.125 1.125 0 0 1 1.823.242l4.18 3.305L10.178.252a1.125 1.125 0 1 1 1.394 1.766C9.147 3.96 7.77 5.045 7.444 5.276c-.49.346-.867.726-1.437.724-.577.002-.894-.286-1.444-.724-.367-.293-1.746-1.382-4.136-3.268z"></path></svg>';

$.fn.kvDatepicker.defaults.templates.leftArrow = arrow;
$.fn.kvDatepicker.defaults.templates.rightArrow = arrow;

$(document).ready(function() {
    $('.js-tooltip').tooltipster({
        theme: 'tooltipster-kub',
        contentAsHTML: true,
    });
});

function createSimpleSelect2(id)
{
    if (!$("#" + id).length)
        return false;

    var select2_modal = {
        "allowClear":false,
        "placeholder":"",
        "width":"100%",
        "minimumResultsForSearch":Infinity,
        "theme":"krajee-bs4",
        "language":"ru-RU"
    };

    var s2options_modal = {
        "themeCss":".select2-container--krajee-bs4",
        "sizeCss":" input-sm",
        "doReset":true,
        "doToggle":false,
        "doOrder":false
    };

    if (jQuery("#" + id).data("select2")) { jQuery("#" + id).select2("destroy"); }
    jQuery.when(jQuery("#" + id).select2(select2_modal)).done(initS2Loading(id,"s2options_modal"));
}
