window.copyToClipboard = function (str) {
    let el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
};
window.empty = function (mixedVar) {
    let undef;
    let key;
    let i;
    let len;
    const emptyValues = [undef, null, false, 0, '', '0'];
    for (i = 0, len = emptyValues.length; i < len; i++) {
        if (mixedVar === emptyValues[i]) {
            return true;
        }
    }
    if (typeof mixedVar === 'object') {
        for (key in mixedVar) {
            if (mixedVar.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
    }
    return false;
};
if (typeof window.AMO === "undefined") {
    window.amoAccessToken = null;
    window.AMO = {
        token: function() {
            return window.amoAccessToken;
        },
        setToken: function(token) {
            window.amoAccessToken = token;
        },
        copyToClipboard: function (el) {
            console.log(el);
            let $el = $(el);
            let str = $el.data('text') || $el.text();
            copyToClipboard(str);
            $el.tooltipster({
                content:"Скопировано в буфер обмена",
                theme: "tooltipster-kub",
                trigger: "custom",
                timer: 500
            });
            $el.tooltipster("show");
        },
        addTokenParam: function(uri, token) {
            console.log({'addTokenParam': {'url': uri, 'token': token}});
            if (uri && token) {
                let a = document.createElement('a');
                a.href = uri;
                console.log({'hostname': {'url': a.hostname, 'location': window.location.hostname}});
                if (a.hostname == window.location.hostname) {
                    let uriParams = new URLSearchParams(a.search);
                    uriParams.set('access-token', token);
                    a.search = uriParams.toString();
                    console.log('newUrl: '+a.href);
                    return a.href;
                }
            }
            return uri;
        },
        getToken: function() {
            window.parent.postMessage(JSON.stringify({'action':'getToken','data':{}}), '*');
        },
        loginSuccess: function() {
            window.parent.postMessage(JSON.stringify({'action':'loginSuccess','data':{}}), '*');
        },
        doAction: function(action, event, data) {
            if (typeof AMO.actions[action] === 'function') {
                AMO.actions[action](event, data);
            }
        },
        actions: {
            setToken: function (d) {
                if (d.token && typeof d.token === 'string' && d.token !== '') {
                    AMO.setToken(d.token);
                    AMO.initEvents();
                } else {
                    console.log('setToken: invalid variable type');
                }
            },
        },
        messageListener: function(event) {
            let data = JSON.parse(event.data);
            if (data.action) {
                AMO.doAction(data.action, data.data, event);
            }
        },
        init: function () {
            if (window.addEventListener) {
                window.addEventListener("message", this.messageListener, false);
            } else if (window.attachEvent) {
                window.attachEvent('onmessage', this.messageListener);
            } else {
                window['onmessage'] = this.messageListener;
            }
            AMO.getToken();
        },
        initEvents: function () {
            $( document ).ajaxSend(function( event, jqxhr, options ) {
                let token = AMO.token();
                if (token) {
                    options.url = AMO.addTokenParam(options.url, token);
                }
                console.log('ajaxSend: '+token);
            });
            $(document).on('pjax:beforeSend', function(xhr, options) {
                let token = AMO.token();
                if (token) {
                    options.url = AMO.addTokenParam(options.url, token);
                }
                console.log('pjaxSend: '+token);
            });
            $(document).on('click', '*.amocrm_action[data-action-data]', function(event) {
                event.preventDefault();
                let actionData = $(this).data('action-data');
                if (actionData.action) {
                    window.parent.postMessage(JSON.stringify(actionData), '*');
                }
                console.log(actionData);
            });
            $(document).on('click', '.create_related_doc', function(event) {
                event.preventDefault();
                let $el = $(this);
                let url = $el.data('url');
                if (url) {
                    $.post(url, function (data) {
                        if (data.status == 'success') {
                            let $item = $el.closest('.list_item_view');
                            let itemUrl = $item.data('item-url');
                            if (itemUrl) {
                                $.get(itemUrl, function (data) {
                                    $('.related_documents_wrap', $item).html($('.related_documents_wrap', $(data)).html());
                                });
                            }
                        } else {
                            console.log(data);
                        }
                    });
                }
            });
        },
    };
    $(document).ready(function () {
        console.log('kub.ready');
        AMO.init();
    });
    $(document).on('pjax:error', function(event, xhr, textStatus, errorThrown, options) {
        event.preventDefault();
        console.log('pjax error:', event, xhr, textStatus, errorThrown, options);
    });
    $(document).on('click', '.copy_to_clipboard', function(event) {
        event.preventDefault();
        AMO.copyToClipboard(this);
    });
}
