if (window.addEventListener) {
    window.addEventListener("message", handlerName, false);
} else if (window.attachEvent) {
    window.attachEvent('onmessage', handlerName);
} else {
    window['onmessage'] = handlerName;
}
