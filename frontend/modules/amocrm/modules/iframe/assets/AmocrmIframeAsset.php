<?php

namespace frontend\modules\amocrm\modules\iframe\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * AmocrmIframeAsset
 */
class AmocrmIframeAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/modules/amocrm/modules/iframe/assets/web';

    /**
     * @var array
     */
    public $css = [
    ];

    /**
     * @var array
     */
    public $js = [
    ];

    /**
     * @var array
     */
    public $depends = [
        'frontend\modules\amocrm\modules\iframe\assets\CommonAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
        'yii\widgets\PjaxAsset',
        'kartik\date\DatePickerAsset',
        'common\assets\TooltipsterAsset',
        'common\assets\SuggestionsCssAsset',
        'common\assets\SuggestionsJqueryAsset',
        'common\assets\MomentAsset',
    ];

    public function init()
    {
        parent::init();

        // prevent load Bootstrap3 files
        Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapThemeAsset'] = ['css' => [],'js' => []];
        Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapPluginAsset'] = ['css' => [],'js' => []];
        Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = ['css' => [],'js' => []];
    }

}
