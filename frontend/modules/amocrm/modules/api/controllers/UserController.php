<?php

namespace frontend\modules\amocrm\modules\api\controllers;

use Yii;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * User controller for the `widget` module
 */
class UserController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'frontend\modules\amocrm\components\QueryParamAuth',
            ],
            'corsFilter'  => [
                'class' => 'yii\filters\Cors',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return array_merge(parent::verbs(), [
            'index' => ['GET'],
            'delete' => ['POST'],
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $result = [];
        $userArray = Yii::$app->amocrmWidget->amocrmWidgetUsers;
        foreach ($userArray as $user) {
            $result[] = [
                'user_id' => $user->user_id,
                'fio' => $user->employeeCompany->fio,
                'short_fio' => $user->employeeCompany->shortFio,
            ];
        }

        return $result;
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionDelete()
    {
        $userId = Yii::$app->request->get('user_id');
        $amocrmUser = $userId ? Yii::$app->amocrmWidget->getWidgetUserByUserId($userId) : Yii::$app->amocrmUser;

        if ($amocrmUser === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if ($amocrmUser->delete() === false) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        Yii::$app->getResponse()->setStatusCode(204);
    }
}
