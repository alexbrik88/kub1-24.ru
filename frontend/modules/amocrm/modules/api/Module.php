<?php

namespace frontend\modules\amocrm\modules\api;

use Yii;
use yii\web\ForbiddenHttpException;

/**
 * api module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'frontend\modules\amocrm\modules\api\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        Yii::$app->set('user', [
            'class' => 'frontend\components\WebUser',
            'identityClass' => 'common\models\employee\Employee',
            'enableSession' => false,
            'loginUrl' => null,
            'on afterLogin' => function ($event) {
                if ($event->identity->is_active == false) {
                    throw new ForbiddenHttpException("Ваш аккаунт заблокирован. Обратитесь в службу технической поддержки.");
                }
            },
        ]);
    }
}
