<?php

namespace frontend\modules\amocrm\components;

use Yii;
use common\models\amocrm\AmocrmWidgetUser;

/**
 * QueryParamAuth is an action filter that supports the authentication based on the access token passed through a query parameter.
 */
class QueryParamAuth extends \yii\filters\auth\QueryParamAuth
{
    /**
     * @var string the parameter name for passing the access token
     */
    public $tokenParam = 'access-token';


    /**
     * {@inheritdoc}
     */
    public function authenticate($user, $request, $response)
    {
        $accessToken = $request->get($this->tokenParam);
        if (is_string($accessToken) && $this->isAccessTokenValid($accessToken)) {
            $widgetUser = AmocrmWidgetUser::findOne(['token' => $accessToken]);
            if ($widgetUser !== null) {
                $employeeCompany = $widgetUser->employeeCompany;
                if ($employeeCompany && $employeeCompany->is_working) {
                    $employee = $employeeCompany->employee;
                    $company = $employeeCompany->company;
                    if ($employee->is_deleted || $company->blocked) {
                        return null;
                    }
                    if ($user->login($employee)) {
                        $employee->setCompany($company->id);
                        Yii::$app->set('amocrmUser', $widgetUser);
                        Yii::$app->set('amocrmWidget', $widgetUser->amocrmWidget);

                        return $employee;
                    }
                }
            }
        }
        if ($accessToken !== null) {
            $this->handleFailure($response);
        }

        return null;
    }

    private function isAccessTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['amocrm']['tokenExpire'] ?? 3600;
        $parts = explode('_', $token);
        $timestamp = (int)end($parts);

        return $timestamp + $expire >= time();
    }
}
