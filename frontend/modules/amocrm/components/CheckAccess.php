<?php

namespace frontend\modules\amocrm\components;

use Yii;
use common\models\Company;
use common\models\amocrm\AmocrmWidget;
use common\models\amocrm\AmocrmWidgetUser;
use common\models\employee\Employee;
use Lcobucci\JWT\Token;
use yii\base\Component;
use yii\di\Instance;
use yii\web\UnauthorizedHttpException;

class CheckAccess extends Component
{
    private $_jwt;
    private $_token;
    private $_isWidgetOk = false;
    private $_isUserOk = false;

    public function __construct(Token $token, ?array $config = [])
    {
        $this->_jwt = $token;

        parent::__construct($config);
    }

    public function init() : void
    {
        parent::init();

        $data = $this->_jwt->getClaims();
        if ($widget = AmocrmWidget::findOne(['account_id' => $data['account_id']])) {
            $this->_isWidgetOk = true;
            if ($user = AmocrmWidgetUser::findOne(['account_id' => $data['account_id'], 'user_id' => $data['user_id']])) {
                $this->_isUserOk = true;
                $this->_token = Yii::$app->security->generateRandomString() . '_' . time();
                $user->updateAttributes(['token' => $this->_token]);
            }
        }
    }

    public function getIsWidgetOk() : bool
    {
        return $this->_isWidgetOk;
    }

    public function getIsUserOk() : bool
    {
        return $this->_isUserOk;
    }

    public function getToken() : ?string
    {
        return $this->_token;
    }
}
