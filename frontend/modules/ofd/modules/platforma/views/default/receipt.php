<?php

use frontend\modules\ofd\modules\platforma\components\Helper;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * @var $this yii\web\View
 * @var $model frontend\modules\ofd\modules\platforma\models\ReceiptForm
 */

$ofdName = "\"{$model->helper->ofd->name}\"";
$this->title = 'Загрузить чеки';

?>

<div class="platforma-default-receipt">
    <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
        'id' => 'receipt_form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
        'options' => [
            'data' => [
                'pjax' => true,
            ],
        ],
    ])); ?>

        <?= $form->field($model, 'store_id')->dropDownList($model->storeDropdownList()); ?>

        <?php Pjax::begin([
            'id' => 'kkt_id_pjax',
            'linkSelector' => false,
            'enablePushState' => false,
            'enableReplaceState' => false,
            'timeout' => 10000,
        ]); ?>

        <?= $form->field($model, 'kkt_id')->dropDownList($model->kktDropdownList()); ?>

        <?php Pjax::end(); ?>

        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'from')->textInput([
                    'class' => 'form-control date-picker ico',
                ]); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'till')->textInput([
                    'class' => 'form-control date-picker ico',
                ]); ?>
            </div>
        </div>

        <div class="d-flex justify-content-between">
            <?= Html::submitButton('Загрузить', [
                'class' => 'button-regular button-regular_red ladda-button min-w-130',
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'button-regular button-regular_red min-w-130',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
    <?php $form->end(); ?>
</div>
