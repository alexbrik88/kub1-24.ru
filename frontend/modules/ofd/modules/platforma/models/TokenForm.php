<?php

namespace frontend\modules\ofd\modules\platforma\models;

use common\models\Company;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use common\models\ofd\OfdUser;
use common\models\ofd\Ofd;
use frontend\modules\ofd\modules\platforma\components\Helper;
use frontend\models\RegistrationForm as RegForm;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class TokenForm
 */
class TokenForm extends \yii\base\Model
{
    public $userId;
    public $token;

    protected $_ofd;

    /**
     * Constructor.
     * @param Ofd $ofd
     * @param array $config name-value pairs that will be used to initialize the object properties
     */
    public function __construct(Ofd $ofd, $config = [])
    {
        $this->_ofd = $ofd;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'token'], 'trim'],
            [['userId', 'token'], 'required'],
            [['userId', 'token'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userId' => 'userId',
            'token' => 'token',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getOfd() : Ofd
    {
        return $this->_ofd;
    }

    /**
     * @inheritdoc
     */
    public function save()
    {
        if ($this->validate()) {
            $ofdUser = OfdUser::findOne([
                'user_uid' => $this->userId,
                'ofd_id' => $this->ofd->id,
            ]);

            if ($ofdUser === null) {
                return [
                    'statusCode' => 404,
                    'data' => null,
                ];
            }

            $ofdUser->setDataParam('access_token', $this->token);

            if ($ofdUser->save()) {
                $this->importStoreAndKkt($ofdUser);

                return [
                    'statusCode' => 200,
                    'data' => null,
                ];
            } else {
                \common\components\helpers\ModelHelper::logErrors($ofdUser, __METHOD__);
            }
        }

        $errors = [];
        foreach ($this->errors as $key => $value) {
            $errors[] = [
                'code' => 2001,
                'subject' => $key,
                'reason' => reset($value),
            ];
        }

        return [
            'statusCode' => 400,
            'data' => [
                'errors' => $errors,
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function importStoreAndKkt($ofdUser)
    {
        $helper = new Helper($this->ofd, $ofdUser->employee,  $ofdUser->company, [
            'ofdUser' => $ofdUser,
        ]);
        $helper->storeRequest();
        $helper->kktRequest();
    }
}
