<?php

namespace frontend\modules\ofd\modules\platforma\models;

use common\models\ofd\OfdKkt;
use common\models\ofd\OfdStatementUpload;
use common\models\ofd\OfdStore;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\ofd\modules\platforma\components\Helper;
use yii\helpers\ArrayHelper;

/**
 * Class ReceiptForm
 */
class ReceiptForm extends \yii\base\Model
{
    public $store_id;
    public $kkt_id;
    public $from;
    public $till;

    protected $ofdHelper;
    protected $_storeArray;
    protected $_kktArray;
    protected $_result;

    /**
     * Constructor.
     * @param Company $company
     * @param array $config name-value pairs that will be used to initialize the object properties
     */
    public function __construct(Helper $helper, $config = [])
    {
        $this->ofdHelper = $helper;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->from = $this->till = date('d.m.Y');
        $this->store_id = array_key_first($this->storeDropdownList());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_id', 'kkt_id', 'from', 'till'], 'required'],
            [
                'store_id', 'exist',
                'targetClass' => OfdStore::class,
                'targetAttribute' => ['store_id' => 'id'],
                'filter' => [
                    'company_id' => $this->ofdHelper->company->id,
                    'ofd_id' => $this->ofdHelper->ofd->id,
                ],
            ],
            [
                'kkt_id', 'exist',
                'targetClass' => OfdKkt::class,
                'targetAttribute' => ['kkt_id' => 'id'],
                'filter' => function ($query) {
                    $query->andWhere([
                        'store_id' => $this->store_id,
                        'company_id' => $this->ofdHelper->company->id,
                        'ofd_id' => $this->ofdHelper->ofd->id,
                    ]);
                },
            ],
            [['from', 'till'], 'date'],
            ['till', function ($attribute, $params) {
                if ($this->hasErrors('from') || $this->hasErrors('till')) {
                    return;
                }
                $from = date_create($this->from);
                $till = date_create($this->till);
                if ($till < $from) {
                    $this->addError($attribute, 'Дата конца периода не может быть меньше даты начала периода.');
                    return;
                }
                $from->modify('+1 month');
                if ($from < $till) {
                    $this->addError($attribute, 'Период не может быть более одного месяца.');
                    return;
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'store_id' => 'Магазин',
            'kkt_id' => 'Терминал',
            'from' => 'Начало периода',
            'till' => 'Конец периода',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getHelper()
    {
        return $this->ofdHelper;
    }

    /**
     * @inheritdoc
     */
    public function storeArray() : array
    {
        if ($this->_storeArray === null) {
            $this->_storeArray = OfdStore::find()->andWhere([
                'company_id' => $this->ofdHelper->company->id,
                'ofd_id' => $this->ofdHelper->ofd->id,
            ])->orderBy([
                'name' => SORT_ASC,
            ])->indexBy('id')->all();
        }

        return $this->_storeArray;
    }

    /**
     * @inheritdoc
     */
    public function kktArray() : array
    {
        if ($this->_kktArray === null) {
            $this->_kktArray = OfdKkt::find()->andWhere([
                'store_id' => $this->store_id,
                'company_id' => $this->ofdHelper->company->id,
                'ofd_id' => $this->ofdHelper->ofd->id,
            ])->orderBy([
                'name' => SORT_ASC,
            ])->indexBy('id')->all();
        }

        return $this->_kktArray;
    }

    /**
     * @inheritdoc
     */
    public function getStore()
    {
        return ArrayHelper::getValue($this->storeArray(), $this->store_id);
    }

    /**
     * @inheritdoc
     */
    public function storeDropdownList() : array
    {
        return ArrayHelper::map($this->storeArray(), 'id', 'local_name');
    }

    /**
     * @inheritdoc
     */
    public function kktDropdownList() : array
    {
        return ArrayHelper::map($this->kktArray(), 'id', 'name');
    }

    /**
     * @inheritdoc
     */
    public function save()
    {
        $this->_result = null;

        if (!$this->validate()) {
            return false;
        }

        $store = OfdStore::findOne([
            'id' => $this->store_id,
            'company_id' => $this->ofdHelper->company->id,
            'ofd_id' => $this->ofdHelper->ofd->id,
        ]);
        $kkt = OfdKkt::findOne([
            'id' => $this->kkt_id,
            'company_id' => $this->ofdHelper->company->id,
            'ofd_id' => $this->ofdHelper->ofd->id,
        ]);
        $from = date_create($this->from)->setTime(0, 0, 0);
        $till = date_create($this->till)->setTime(23, 59, 59);

        $statementUpload = new OfdStatementUpload;
        $statementUpload->company_id = $this->ofdHelper->company->id;
        $statementUpload->ofd_id = $this->ofdHelper->ofd->id;
        $statementUpload->store_id = $store->id;
        $statementUpload->kkt_id = $kkt->id;
        $statementUpload->source = OfdStatementUpload::SOURCE_OFD;
        $statementUpload->period_from = $from->format('Y-m-d');
        $statementUpload->period_till = $till->format('Y-m-d');
        $statementUpload->saved_count = 0;

        if ($statementUpload->save()) {
            LogHelper::log($statementUpload, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE);
            $items = $this->ofdHelper->client->getReceipts($store, $kkt, $from, $till);
            $this->_result = $this->ofdHelper->saveReceipts($store, $kkt, $items, $statementUpload);
        } else {
            $this->_result = [
                'sum' => 0,
                'count' => 0,
                'insert' => 0,
            ];
        }
        $this->_result['store'] = $store;
        $this->_result['kkt'] = $kkt;
        $this->_result['period'] = [
            'from' => $from,
            'till' => $till,
        ];

        return true;
    }

    /**
     * @inheritdoc
     */
    public function getResult()
    {
        return $this->_result;
    }
}
