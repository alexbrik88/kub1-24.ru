<?php

namespace frontend\modules\ofd\modules\platforma\models;

use common\models\Company;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use common\models\ofd\OfdUser;
use common\models\ofd\Ofd;
use frontend\models\RegistrationForm as RegForm;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class AuthForm
 */
class AuthForm extends \yii\base\Model
{
    public $userId;
    public $username;
    public $password;
    public $companyInn;

    protected $_ofd;

    private $_user = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $attemptsParams = ArrayHelper::getValue(Yii::$app->params, 'login_attempt');

        $behaviors[] = [
            'class' => 'common\components\LoginAttemptBehavior',
            'useKey' => false,
            'attempts' => ArrayHelper::getValue($attemptsParams, 'attempts', 5),
            'duration' => ArrayHelper::getValue($attemptsParams, 'duration', 300),
            'disableDuration' => ArrayHelper::getValue($attemptsParams, 'disableDuration', 600),
            'message' => 'Вы превысили допустимое количество попыток входа. Через {disableDuration} вы можете попробовать войти в аккаунт еще раз.',
        ];

        return $behaviors;
    }

    /**
     * Constructor.
     * @param Ofd $ofd
     * @param array $config name-value pairs that will be used to initialize the object properties
     */
    public function __construct(Ofd $ofd, $config = [])
    {
        $this->_ofd = $ofd;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'username',
                    'companyInn',
                ], 'trim',
            ],
            [
                [
                    'userId',
                    'username',
                    'password',
                ], 'required',
            ],
            [
                [
                    'userId',
                    'username',
                    'password',
                    'companyInn',
                ], 'string',
            ],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неверный пользователь или пароль.');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userId' => 'userId',
            'username' => 'username',
            'password' => 'password',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getOfd() : Ofd
    {
        return $this->_ofd;
    }

    /**
     * Finds user by [[username]]
     *
     * @return Employee|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Employee::findIdentityByLogin($this->username);
        }

        return $this->_user;
    }

    /**
     * @inheritdoc
     */
    public function save()
    {
        if ($this->validate()) {
            $employee = $this->getUser();

            $ofdUser = OfdUser::findOne([
                'user_uid' => $this->userId,
                'ofd_id' => $this->ofd->id,
            ]);

            if ($ofdUser !== null) {
                if ($ofdUser->employee_id == $employee->id) {
                    return [
                        'statusCode' => 200,
                        'data' => [
                            'userId' => $this->userId,
                            'token' => $ofdUser->auth_key,
                        ]
                    ];
                } else {
                    return [
                        'statusCode' => 409,
                        'data' => [
                            'errors' => [
                                'code' => 2004,
                            ],
                        ]
                    ];
                }
            } else {
                $query = $employee->getCompanies();
                if ($inn = preg_replace('/\D/', '', $this->companyInn)) {
                    $query->orderBy([
                        "IF([[inn]]=\"{$inn}\",0,1)" => SORT_ASC
                    ]);
                }
                $query->addOrderBy([
                    "IF([[id]]=\"{$employee->company_id}\",0,1)" => SORT_ASC
                ]);
                $company = $query->one();
                $ofdUser = new OfdUser([
                    'employee_id' => $employee->id,
                    'company_id' => $company->id,
                    'ofd_id' => $this->ofd->id,
                    'user_uid' => $this->userId,
                    'data' => [
                        'userId' => $this->userId,
                    ],
                ]);

                if ($ofdUser->save()) {
                    return [
                        'statusCode' => 200,
                        'data' => [
                            'userId' => $this->userId,
                            'token' => $ofdUser->auth_key,
                        ]
                    ];
                } else {
                    \common\components\helpers\ModelHelper::logErrors($ofdUser, __METHOD__);

                    return [
                        'statusCode' => 400,
                        'data' => [
                            'errors' => [
                                'code' => 2001,
                            ],
                        ]
                    ];
                }
            }
        }

        $errors = [];
        foreach ($this->errors as $key => $value) {
            $errors[] = [
                'code' => 2001,
                'subject' => $key,
                'reason' => reset($value),
            ];
        }

        return [
            'statusCode' => 400,
            'data' => [
                'errors' => $errors,
            ]
        ];
    }
}
