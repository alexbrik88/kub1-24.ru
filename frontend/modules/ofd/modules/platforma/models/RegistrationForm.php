<?php

namespace frontend\modules\ofd\modules\platforma\models;

use common\components\DadataClient;
use common\models\company\CompanyType;
use common\models\company\RegistrationPageType;
use common\models\employee\Employee;
use common\models\ofd\OfdUser;
use common\models\ofd\Ofd;
use frontend\models\RegistrationForm as RegForm;
use yii\helpers\ArrayHelper;

/**
 * Class RegistrationForm
 */
class RegistrationForm extends \yii\base\Model
{
    public $userId;
    public $email;
    public $companyInn;
    public $companyKpp;
    public $companyType;
    public $taxSystem;

    protected $_ofd;

    /**
     * Constructor.
     * @param Ofd $ofd
     * @param array $config name-value pairs that will be used to initialize the object properties
     */
    public function __construct(Ofd $ofd, $config = [])
    {
        $this->_ofd = $ofd;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'email',
                    'companyInn',
                    'companyKpp',
                    'companyType',
                    'taxSystem',
                ], 'trim',
            ],
            [
                [
                    'userId',
                    'email',
                ], 'required',
            ],
            [
                [
                    'userId',
                    'companyInn',
                    'companyKpp',
                    'companyType',
                    'taxSystem',
                ], 'string',
            ],
            [['email'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userId' => 'userId',
            'email' => 'email',
            'companyType' => 'companyType',
            'taxSystem' => 'taxSystem',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getOfd() : Ofd
    {
        return $this->_ofd;
    }

    /**
     * @inheritdoc
     */
    public function save()
    {
        if ($this->validate()) {
            /**
             * Если пользователь существует
             */
            if (Employee::findIdentityByLogin($this->email) !== null) {
                return [
                    'statusCode' => 409,
                    'data' => [
                        'errors' => [
                            'code' => 2005,
                        ],
                    ]
                ];
            }
            /**
             * Если интеграция существует
             */
            if (OfdUser::findOne([
                'user_uid' => $this->userId,
                'ofd_id' => $this->ofd->id,
            ]) !== null) {
                return [
                    'statusCode' => 409,
                    'data' => [
                        'errors' => [
                            'code' => 2004,
                        ],
                    ]
                ];
            }

            $companyType = CompanyType::find()->select('id')->andWhere([
                'name_short' => mb_strtoupper($this->companyType ?? ''),
            ])->scalar() ?? CompanyType::TYPE_OOO;

            $taxSystem = mb_strtoupper($this->taxSystem ?? '');

            //Регистрация в КУБ
            $model = new RegForm();
            $model->email = $this->email;
            $model->companyType = $companyType;
            $model->checkrules = 1;
            $model->registrationPageTypeId = RegistrationPageType::PAGE_TYPE_EVOTOR;

            switch ($taxSystem) {
                case 'ОСНО':
                    $model->taxationTypeOsno = 1;
                    break;

                case 'ЕНВД':
                    $model->taxationTypeEnvd = 1;
                    break;

                default:
                    $model->taxationTypeUsn = 1;
                    break;
            }

            if ($model->save()) {
                $employee = $model->getUser();
                $company = $model->getCompany();

                $ofdUser = new OfdUser([
                    'employee_id' => $employee->id,
                    'company_id' => $employee->company_id,
                    'ofd_id' => $this->ofd->id,
                    'user_uid' => $this->userId,
                    'data' => [
                        'userId' => $this->userId,
                    ],
                ]);

                // Пробуем получить реквизиты компании
                if ($company && $this->companyInn) {
                    $companyAttributes = DadataClient::getCompanyAttributes($this->companyInn, $this->companyKpp);
                    if (!empty($companyAttributes)) {
                        $company->updateAttributes($companyAttributes);
                    }
                }

                if ($ofdUser->save()) {
                    return [
                        'statusCode' => 200,
                        'data' => [
                            'userId' => $this->userId,
                            'token' => $ofdUser->auth_key,
                        ]
                    ];
                } else {
                    \common\components\helpers\ModelHelper::logErrors($ofdUser, __METHOD__);
                }
            } else {
                \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);
            }
        }

        $errors = [];
        foreach ($this->errors as $key => $value) {
            $errors[] = [
                'code' => 2001,
                'subject' => $key,
                'reason' => reset($value),
            ];
        }

        return [
            'statusCode' => 400,
            'data' => [
                'errors' => $errors,
            ]
        ];
    }
}
