<?php

namespace frontend\modules\ofd\modules\platforma\components;

use Yii;
use common\models\ofd\Ofd;
use common\models\ofd\OfdKkt;
use common\models\ofd\OfdOperationType;
use common\models\ofd\OfdReceiptItem;
use common\models\ofd\OfdStore;
use frontend\modules\ofd\components\AbstractClient;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client as HttpClient;
use yii\httpclient\Exception;
use yii\httpclient\Request;
use yii\httpclient\Response;

/**
 * Класс для работы с API Эвотор
 *
 * @see https://developer.evotor.ru/docs/rest_overview.html
 */
class Client extends AbstractClient
{

    const APPLICATION_NAME = 'КУБ-24';
    const APPLICATION_TOKEN = 'wpeo98uj23048h3208h';
    const BASE_URI = 'https://api.evotor.ru/';

    public static $storesPath = '/api/v1/ofd/stores';
    public static $devicesPath = '/api/v1/ofd/devices';
    public static $documentsPath = '/api/v1/ofd/documents/{store-id}';

    protected $base_url;
    protected $app_token;
    protected $access_token;

    public function init()
    {
        parent::init();

        $this->base_url = ArrayHelper::getValue($this->_appData, 'base_url');
        $this->app_token = ArrayHelper::getValue($this->_appData, 'app_token');
        $this->access_token = ArrayHelper::getValue($this->_userData, 'access_token');

        if (!$this->access_token) {
            throw new InvalidConfigException('The "access_token" property mast by set.');
        }
        if (!$this->base_url) {
            throw new InvalidConfigException('The "base_url" property mast by set.');
        }
    }

    public function getRequest() : Request
    {
        $client =  (new HttpClient([
            'baseUrl' => $this->base_url,
        ]));

        if (ArrayHelper::getValue(Yii::$app->params, ['ofd', 'platforma', 'debugMode'])) {
            $client->attachBehavior('logger', [
                'class' => 'common\components\HttpClientLoggerBehavior',
                'logName' => 'ofd_platforma_client',
            ]);
        }

        return $client->createRequest()->setHeaders([
            'Accept' => 'application/vnd.evotor.v2+json',
            'Content-Type' => 'application/vnd.evotor.v2+json',
            'Authorization' => "Bearer {$this->access_token}",
        ]);
    }

    public function getStores() : array
    {
        $result = [];
        $this->response = $this->getRequest()
            ->setUrl(self::$storesPath)
            ->send();

        $this->log($this->response, __METHOD__);

        if ($this->response->isOk) {
            $data = $this->response->data['data'] ?? [];
            if (is_array($data) && count($data) > 0) {
                foreach ($data as $item) {
                    $result[] = [
                        'uid' => $item['id'],
                        'parent_uid' => $item['parent_id'] ?? null,
                        'store_type_id' => OfdStore::TYPE_STORE,
                        'name' => $item['name'],
                        'address' => $item['address'] ?? null,
                    ];
                }
            }
        }

        return $result;
    }

    public function getKkts(OfdStore $store = null) : array
    {
        $result = [];
        $this->response = $this->getRequest()
            ->setUrl(self::$devicesPath)
            ->send();

        $this->log($this->response, __METHOD__);

        if ($this->response->isOk) {
            $data = $this->response->data['data'] ?? [];
            if (is_array($data) && count($data) > 0) {
                foreach ($data as $item) {
                    $result[] = [
                        'uid' => $item['id'],
                        'store_uid' => $item['store_id'],
                        'name' => $item['name'],
                        'address' => $item['address'],
                    ];
                }
            }
        }

        return $result;
    }

    public function getReceipts($store = null, $kkt = null, \DateTime $periodFrom, \DateTime $periodTill) : array
    {
        $kktUid = $kkt->uid;
        $result = [];

        $from = (clone $periodFrom)->setTime(0, 0, 0);
        $maxTill = clone $periodTill;
        do {
            $till = min((clone $from)->modify('+6 days')->setTime(23, 59, 59), $maxTill);

            $this->response = $this->getRequest()->setUrl([
                strtr(self::$documentsPath, ['{store-id}' => $store->uid]),
                'deviceId' => $kktUid,
                'from' => $from->format('Y-m-d\TH:i:s\.Z\Z'),
                'to' => $till->format('Y-m-d\TH:i:s\.Z\Z'),
                'type' => 'SELL',
            ])->send();

            if ($this->response->isOk) {
                $data = $this->response->data['data'] ?? [];

                if (is_array($data) && count($data) > 0) {
                    foreach ($data as $item) {
                        if (isset($item['operationType']) && in_array($item['operationType'], OfdOperationType::$typeList)) {
                            $time = date_create_from_format('Y.m.d H:i:s', substr($item['receiptDate'], 0, 19));
                            $receiptItems = [];
                            foreach ($item['items'] as $product) {
                                $receiptItems[] = [
                                    'sum' => $product['sum'],
                                    'price' => $product['price'],
                                    'quantity' => $product['quantity'],
                                    'nds_no' => $product['ndsNo'] ?? 0,
                                    'nds_0' => $product['nds0'] ?? 0,
                                    'nds_10' => $product['nds10'] ?? 0,
                                    'nds_20' => max($product['nds20'] ?? 0, $product['nds18'] ?? 0),
                                    'nds_calculated_20' => max($product['ndsCalculated18'] ?? 0, $product['ndsCalculated20'] ?? 0),
                                    'nds_calculated_10' => $product['ndsCalculated18'] ?? 0,
                                    'barcode' => $product['barcode'] ?? null,
                                    'name' => $product['name'],
                                    'product_type' => $product['productType'] ?? OfdReceiptItem::TYPE_PRODUCT,
                                    'product_code' => $product['productCode'] ?? null,
                                    'modifiers' => $product['modifiers'] ?? null,
                                ];
                            }

                            $result[] = [
                                'date_time' => $time->format('Y-m-d H:i:s'),
                                'document_number' => $item['fiscalDocumentNumber'],
                                'kkt_factory_number' => $item['fiscalDriveNumber'],
                                'kkt_registration_number' => $item['kktRegId'],
                                'kkt_uid' => $kktUid,
                                'shift_number' => $item['shiftNumber'],
                                'number' => $item['requestNumber'],
                                'operation_type_id' => $item['operationType'],
                                'address' => $item['retailPlaceAddress'] ?? '',
                                'place' => $item['retailPlace'] ?? '',
                                'total_sum' => $item['totalSum'],
                                'cash_sum' => $item['cashTotalSum'] ?? 0,
                                'cashless_sum' => $item['ecashTotalSum'] ?? 0,
                                'nds_20' => max($item['nds20'] ?? 0, $item['nds18'] ?? 0),
                                'nds_10' => $item['nds10'] ?? 0,
                                'nds_0' => $item['nds0'] ?? 0,
                                'nds_no' => $item['ndsNo'] ?? 0,
                                'nds_calculated_20' => max($item['ndsCalculated18'] ?? 0, $item['ndsCalculated20'] ?? 0),
                                'nds_calculated_10' => $item['ndsCalculated18'] ?? 0,
                                'operator_name' => $item['operator'] ?? '',
                                'operator_inn' => $item['operatorInn'] ?? '',
                                'fiscal_tag' => $item['fiscalSign'] ?? '',
                                'contractor_name' => $item['contractor_name'] ?? '',
                                'contractor_inn' => $item['contractor_inn'] ?? '',
                                'items' => $receiptItems,
                                'modifiers' => $item['modifiers'] ?? null,
                            ];
                        }
                    }
                }
            }

            $from->modify('+7 days');
        } while ($till < $periodTill);

        return $result;
    }
}
