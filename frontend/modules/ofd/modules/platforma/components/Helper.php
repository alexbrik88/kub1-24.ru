<?php

namespace frontend\modules\ofd\modules\platforma\components;

use common\models\ofd\OfdKkt;
use common\models\ofd\OfdStatementUpload;
use common\models\ofd\OfdStore;
use frontend\modules\ofd\components\AbstractHelper;

/**
 * Class Helper
 *
 * @see https://developer.evotor.ru/docs/rest_overview.html
 */
class Helper extends AbstractHelper
{
    const APPLICATION_NAME = 'КУБ.ФинДиректор';

    public function storeRequest()
    {
        $client = $this->getClient();
        $items = $client->getStores();
        $result = $this->saveStores($items);

        return $result;
    }

    public function kktRequest(OfdStore $store = null)
    {
        $client = $this->getClient();
        $items = $client->getKkts($store);
        $result = $this->saveKkts($items);

        return $result;
    }

    public function receiptRequest(OfdStore $store = null, OfdKkt $kkt = null, \DateTime $periodFrom, \DateTime $periodTill, int $source) : ?array
    {
        if (isset($kkt)) {
            $statementUpload = new OfdStatementUpload;
            $statementUpload->company_id = $this->_company->id;
            $statementUpload->ofd_id = $this->_ofd->id;
            $statementUpload->store_id = $store ? $store->id : null;
            $statementUpload->kkt_id = $kkt->id;
            $statementUpload->source = $source;
            $statementUpload->period_from = $from->format('Y-m-d');
            $statementUpload->period_till = $till->format('Y-m-d');
            $statementUpload->saved_count = 0;

            if ($statementUpload->save()) {
                LogHelper::log($statementUpload, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE);
                $client = $this->getClient();
                $items = $client->getReceipts($store, $kkt, $periodFrom, $periodTill);
                $result = $this->saveReceipts($store, $kkt, $items, $statementUpload);

                return $result;
            }
        }

        return null;

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatementUploads()
    {
        return OfdStatementUpload::find()->andWhere([
            'ofd_id' => $this->ofd->id,
            'company_id' => $this->company->id,
        ]);
    }
}
