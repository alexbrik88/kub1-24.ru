<?php

namespace frontend\modules\ofd\modules\platforma\controllers;

use common\models\ofd\Ofd;
use common\models\ofd\OfdUser;
use frontend\modules\ofd\modules\platforma\models\AuthForm;
use frontend\modules\ofd\modules\platforma\models\RegistrationForm;
use frontend\modules\ofd\modules\platforma\models\TokenForm;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\UnauthorizedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Api controller for the `platforma` module
 */
class ApiController extends \yii\rest\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['authenticator']);

        $behaviors['access'] = [
            'class' => 'yii\filters\AccessControl',
            'denyCallback' => function ($rule, $action) {
                Yii::$app->response->on('beforeSend', function ($event) {
                    $response = $event->sender;
                    $response->data = [
                        'errors' => [
                            'code' => 1001,
                        ],
                    ];
                });

                $this->log("Invalid Token");

                throw new \yii\web\HttpException(401, 'Invalid Token.');
            },
            'rules' => [
                [
                    'allow' => true,
                    'matchCallback' => function ($rule, $action) {
                        $appToken = ArrayHelper::getValue(Yii::$app->params, [
                            'ofd',
                            'platforma',
                            'app_token',
                        ]);
                        if (!empty($appToken)) {
                            if ($authHeader = Yii::$app->request->headers->get('Authorization')) {
                                if (preg_match('/^Bearer\s+(.*?)$/', $authHeader, $matches)) {
                                    $token = $matches[1];
                                } else {
                                    $token = null;
                                }

                                return $appToken === $token;
                            }
                        }

                        return false;
                    }
                ],
            ],
        ];

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return array_merge(parent::verbs(), [
            'auth' => ['POST'],
            'registration' => ['POST'],
            'token' => ['POST'],
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionAuth()
    {
        $model = new AuthForm($this->getOfd());
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');

        $result = $model->save();
        $this->log(VarDumper::dumpAsString($result));

        $response = Yii::$app->getResponse();
        $response->setStatusCode($result['statusCode']);

        return $result['data'];
    }

    /**
     * @return string
     */
    public function actionRegistration()
    {
        $model = new RegistrationForm($this->getOfd());
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');

        $result = $model->save();
        $this->log(VarDumper::dumpAsString($result));

        $response = Yii::$app->getResponse();
        $response->setStatusCode($result['statusCode']);

        return $result['data'];
    }

    /**
     * @return string
     */
    public function actionToken()
    {
        $model = new TokenForm($this->getOfd());
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');

        $result = $model->save();
        $this->log(VarDumper::dumpAsString($result));

        $response = Yii::$app->getResponse();
        $response->setStatusCode($result['statusCode']);

        return $result['data'];
    }

    /**
     * @return string
     */
    public function actionAppEvent()
    {
        $data = Json::decode(Yii::$app->getRequest()->getRawBody());
        $this->log(VarDumper::dumpAsString($data));

        $type = $data['type'] ?? null;

        switch ($type) {
            case 'ApplicationUninstalled':
                $userUid = $data['data']['userId'] ?? null;
                if ($userUid) {
                    $ofd = $this->getOfd();
                    $ofdUser = OfdUser::findOne([
                        'user_uid' => $userUid,
                        'ofd_id' => $ofd->id,
                    ]);
                    if ($ofdUser !== null) {
                        $ofdUser->delete();
                    }
                }
                break;
        }

        return;
    }

    /**
     * @return Ofd
     * @throws NotFoundHttpException
     */
    protected function getOfd()
    {
        $model = Ofd::findOne([
            'alias' =>$this->module->id,
            'is_active' => true,
        ]);

        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $model;
    }

    /**
     * @param \yii\base\Action $action
     * @return bool|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->log();

        if (parent::beforeAction($action)) {
            return true;
        }

        return false;
    }

    /**
     * @param string $message
     */
    public function log(string $message = null)
    {
        if ($message === null) {
            $r = Yii::$app->request;
            $message = sprintf("\n\n%s [%s] %s\n\n%s\n", date('Y-m-d H:i:s'), $r->method, $this->action->id ?? '', VarDumper::dumpAsString([
                'headers' => getallheaders(),
                '$_GET' => $_GET,
                '$_POST' => $_POST,
                '$_SERVER' => $_SERVER,
                'rawBody' => $r->rawBody,
            ]));
        }
        file_put_contents(
            Yii::getAlias('@runtime/logs/ofd.platforma.api.log'),
            $message,
            FILE_APPEND
        );
    }
}
