<?php

namespace frontend\modules\ofd\modules\platforma\controllers;

use frontend\modules\ofd\components\BaseOfdController;
use frontend\modules\ofd\modules\platforma\components\Helper;
use frontend\modules\ofd\modules\platforma\models\ReceiptForm;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;
use yii\widgets\ActiveForm;

/**
 * Default controller for the `platforma` module
 */
class DefaultController extends BaseOfdController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\UserRole::ROLE_CHIEF,
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'ajax' => [
                'class' => 'yii\filters\AjaxFilter',
                'only' => [
                    'import',
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIntegration()
    {
        $employee = Yii::$app->user->identity;
        $ofdUser = $this->getOfdUser();

        if ($ofdUser && Yii::$app->request->post('delete') && $ofdUser->delete()) {
            $ofdUser = null;
        }

        return $this->render('integration', [
            'employee' => $employee,
            'companyCount' => $employee->getCompanies()->count(),
            'ofdUser' => $ofdUser,
            'ofd' => $this->ofd,
        ]);
    }

    /**
     * @return string
     */
    public function actionDelete()
    {
        $employee = Yii::$app->user->identity;

        if ($ofdUser = $this->getOfdUser()) {
            $ofdUser->delete();
        }

        return $this->redirect(Yii::$app->request->referrer ?: [
            'integration',
            'p' => Yii::$app->getRequest()->get('p'),
        ]);
    }

    /**
     * @return string
     */
    public function actionStore()
    {
        return $this->render('store');
    }

    /**
     * @return string
     */
    public function actionDevice()
    {
        return $this->render('device');
    }

    /**
     * @return string
     */
    public function actionImport()
    {
        $employee = Yii::$app->user->identity;
        $helper = new Helper($this->ofd, $employee,  $employee->company, [
            'ofdUser' => $this->getOfdUser(),
        ]);
        $helper->storeRequest();
        $helper->kktRequest();
    }

    /**
     * @return string
     */
    public function actionReceipt()
    {
        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $ofdUser = $this->getOfdUser();
        if ($ofdUser === null) {
            return $this->redirect([
                'integration',
                'p' => Yii::$app->getRequest()->get('p'),
            ]);
        }
        $ofdStoreArray = $company->getOfdStores()->andWhere([
            'ofd_id' => $this->ofd->id,
        ])->orderBy([
            'name' => SORT_ASC,
        ])->indexBy('id')->all();
        $model = new ReceiptForm($this->getHelper());
        $model->store_id = Yii::$app->request->get('store_id') ?: (reset($ofdStoreArray)->id ?? null);
        $model->kkt_id = Yii::$app->request->get('kkt_id');

        if ($store_id = Yii::$app->request->post('store_id')) {
            $model->store_id = $store_id;

            return $this->render('receipt', [
                'model' => $model,
            ]);
        }

        if (Yii::$app->request->post('ajax')) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->load(Yii::$app->request->post());

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->render('receipt-result', [
                'result' => $model->getResult(),
                'store_id' => $model->store_id,
                'kkt_id' => $model->kkt_id,
            ]);
        }

        return $this->render('receipt', [
            'model' => $model,
            'ofdStoreArray' => $ofdStoreArray,
        ]);
    }

    /**
     * @return OfdUser
     */
    protected function getOfdUser()
    {
        return Yii::$app->user->identity->company->getOfdUsersByOfd($this->ofd)->one();
    }
}
