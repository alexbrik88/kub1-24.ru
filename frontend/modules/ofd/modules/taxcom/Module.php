<?php

namespace frontend\modules\ofd\modules\taxcom;

/**
 * Taxcom module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'frontend\modules\ofd\modules\taxcom\controllers';

    /**
     * {@inheritdoc}
     */
    public $layout = 'ofd';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
        $this->setLayoutPath($this->module->getLayoutPath());
    }
}
