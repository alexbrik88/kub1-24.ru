<?php

use frontend\modules\cash\modules\ofd\components\Ofd;
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var \common\models\ofd\Ofd $ofd
 * @var \common\models\ofd\OfdUser $ofdUser
 * @var $this yii\web\View
 * @var $employee common\models\employee\Employee
 */

$ofdName = "\"{$ofd->name}\"";
$this->title = 'Интеграция c ' . $ofdName;
//$p = Yii::$app->request->get('p');
?>

<div class="taxcom-default-integration">
    <?php if ($ofdUser === null) : ?>

        <?php $form = ActiveForm::begin([
            'id' => 'statement-request-form',
            'action' => ['integration'],
            'options' => [
                'data' => [
                    'pjax' => true,
                ]
            ]
        ]); ?>

        <?= Html::hiddenInput('auth', 1) ?>

        <div class="row">
            <div class="col-12" style="font-size: 14px;">
                Для автоматической загрузки выписки нужно ПОДТВЕРДИТЬ ИНТЕГРАЦИЮ в ОФД <b style="display: contents"><?= $ofd->name ?></b>. <br>
                После этого вы будете перенаправлены обратно в сервис для загрузки операций.
            </div>
        </div>

        <div class="row pt-3">
            <div class="col-4">
                <?= Html::submitButton('Подтвердить интеграцию', [
                    'class' => 'button-clr button-regular button-regular_red w-100'
                ]) ?>
            </div>
            <div class="column ml-auto">
                <?php if (Ofd::isCashOperationsPage()): ?>

                    <?= Html::a('Отменить', 'javascript:void(0)', [
                        'class' => 'button-regular button-hover-transparent button-clr button-width ofd-module-close-link',
                    ]) ?>

                <?php else: ?>

                    <?= Html::a('Отменить', 'javascript:void(0)', [
                        'class' => 'button-regular button-hover-transparent button-clr button-width',
                        'data-dismiss' => 'modal'
                    ]) ?>

                <?php endif; ?>
            </div>
        </div>

        <?php $form->end() ?>

        <?php if ($companyCount > 1) { ?>
            <hr/>
            <p>
                <b>Внимание!</b>
                <br>
                Аккаунт <?= $ofdName ?> будет подключён к компании
                &laquo;<b><?= $employee->company->getShortTitle() ?></b>&raquo;.
                <br>
                Выберите другую компанию, если это требуется.
            </p>
        <?php } ?>
    <?php else : ?>
        <div>
            <strong>Дата подключения:</strong>
            <?= date('d.m.Y', $ofdUser->created_at) ?>
        </div>
        <div class="mt-5 d-flex justify-content-between">
            <div class="">
                <?= \frontend\widgets\ConfirmModalWidget::widget([
                    'options' => [
                        'id' => 'delete-confirm',
                    ],
                    'toggleButton' => [
                        'tag' => 'a',
                        'label' => 'Удалить интеграцию с ' . $ofdName,
                        'class' => '',
                    ],
                    'confirmUrl' => Url::toRoute([
                        'delete',
                        'p' => Yii::$app->request->get('p'),
                    ]),
                    'confirmParams' => [],
                    'message' => "Вы уверены, что хотите удалить интеграцию с {$ofdName}?",
                ]) ?>
            </div>
            <div class="ml-auto">
                <?= Html::button('Отмена', [
                    'class' => 'button-regular button-hover-transparent',
                    'data-dismiss' => 'modal',
                ]) ?>
            </div>
        </div>
    <?php endif ?>
</div>
