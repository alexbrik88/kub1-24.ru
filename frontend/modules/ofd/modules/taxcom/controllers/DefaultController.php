<?php

namespace frontend\modules\ofd\modules\taxcom\controllers;

use common\models\ofd\OfdUser;
use frontend\modules\ofd\components\BaseOfdController;
use frontend\modules\ofd\modules\taxcom\components\Helper;
use frontend\modules\ofd\modules\taxcom\models\ReceiptForm;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;
use yii\widgets\ActiveForm;

/**
 * Default controller for the `taxcom` module
 */
class DefaultController extends BaseOfdController
{
    const MESSAGE_AUTH_ERROR = 'Во время авторизации произошла ошибка';
    const MESSAGE_AUTH_SUCCESS = 'Вы успешно авторизованы';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\UserRole::ROLE_CHIEF,
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string|Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionIntegration()
    {
        $employee = Yii::$app->user->identity;
        $ofdUser = $this->getOfdUser();

        if ($ofdUser && Yii::$app->request->post('delete') && $ofdUser->delete()) {
            $ofdUser = null;
        }

        $helper = new Helper($this->ofd, $employee,  $employee->company, ['ofdUser' => $ofdUser]);

        if (Yii::$app->request->post('auth')) {
            Url::remember(Url::current(), $this->ofd->alias . 'ReturnRedirect');
            return $this->redirect($helper->getAuthUrl());
        }

        return $this->render('integration', [
            'employee' => $employee,
            'companyCount' => $employee->getCompanies()->count(),
            'ofdUser' => $ofdUser,
            'ofd' => $this->ofd,
        ]);
    }

    public function actionReturn()
    {
        $redirectUrl = Url::previous($this->ofd->alias . 'ReturnRedirect');
        $code = Yii::$app->request->get('code');

        if ($code) {

            $employee = Yii::$app->user->identity;
            $company = $employee->company;
            $ofdUser = $this->getOfdUser();
            $helper = new Helper($this->ofd, $employee,  $employee->company, ['ofdUser' => $ofdUser]);

            if ($tokenData = $helper->getAuthToken($code)) {

                $ofdUserCondition = [
                    'employee_id' => $employee->id,
                    'company_id' => $company->id,
                    'ofd_id' => $this->ofd->id
                ];

                $ofdUser = OfdUser::findOne($ofdUserCondition) ?: new OfdUser($ofdUserCondition);
                $ofdUser->setData($tokenData);

                if ($ofdUser->save()) {

                    $helper->client->setUserData($tokenData);

                    $stores = $helper->storeRequest();
                    foreach ($stores as $store) {
                        $kkts = $helper->kktRequest($store);
                        foreach ($kkts as $kkt) {
                            $helper->fnRequest($kkt);
                        }
                    }

                    Yii::$app->session->setFlash('success', self::MESSAGE_AUTH_SUCCESS);

                } else {
                    Yii::$app->session->setFlash('error', self::MESSAGE_AUTH_ERROR);
                    Yii::warning(static::class . "->actionReturn() " . var_export($ofdUser->errors, true), 'ofd');
                }
            }

        } else {
            Yii::$app->session->setFlash('error', self::MESSAGE_AUTH_ERROR);
            Yii::warning(static::class . "->actionReturn() " . var_export('code not found', true), 'ofd');
        }

        return $this->redirect($redirectUrl ? : ['index']);
    }

    /**
     * @return Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete()
    {
        $employee = Yii::$app->user->identity;

        if ($ofdUser = $this->getOfdUser()) {
            $ofdUser->delete();
        }

        return $this->redirect(Yii::$app->request->referrer ?: [
            'integration',
            'p' => Yii::$app->getRequest()->get('p'),
        ]);
    }

    /**
     * @return string
     */
    public function actionStore()
    {
        return $this->render('store');
    }

    /**
     * @return string
     */
    public function actionDevice()
    {
        return $this->render('device');
    }

    /**
     *
     */
    public function actionImport()
    {
        $employee = Yii::$app->user->identity;
        $helper = new Helper($this->ofd, $employee,  $employee->company, [
            'ofdUser' => $this->getOfdUser(),
        ]);

        $stores = $helper->storeRequest();
        foreach ($stores as $store) {
            $kkts = $helper->kktRequest($store);
            foreach ($kkts as $kkt) {
                $helper->fnRequest($kkt);
            }
        }
    }

    /**
     * @return array|string|Response
     * @throws NotFoundHttpException
     */
    public function actionReceipt()
    {
        set_time_limit(1800);

        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $ofdUser = $this->getOfdUser();
        if ($ofdUser === null) {
            return $this->redirect([
                'integration',
                'p' => Yii::$app->getRequest()->get('p'),
            ]);
        }
        $ofdStoreArray = $company->getOfdStores()->andWhere([
            'ofd_id' => $this->ofd->id,
        ])->orderBy([
            'name' => SORT_ASC,
        ])->indexBy('id')->all();
        $model = new ReceiptForm($this->getHelper());
        $model->store_id = reset($ofdStoreArray)->id ?? null;

        if ($store_id = Yii::$app->request->post('store_id')) {
            $model->store_id = $store_id;

            return $this->render('receipt', [
                'model' => $model,
            ]);
        }

        if (Yii::$app->request->post('ajax')) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->load(Yii::$app->request->post());

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->render('receipt-result', [
                'result' => $model->getResult(),
            ]);
        }

        return $this->render('receipt', [
            'model' => $model,
            'ofdStoreArray' => $ofdStoreArray,
        ]);
    }

    /**
     * @return OfdUser
     */
    protected function getOfdUser()
    {
        return Yii::$app->user->identity->company->getOfdUsersByOfd($this->ofd)->one();
    }
}
