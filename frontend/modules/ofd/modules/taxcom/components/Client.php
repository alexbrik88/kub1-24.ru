<?php

namespace frontend\modules\ofd\modules\taxcom\components;

use common\components\curl\Curl;
use common\models\ofd\OfdOperationType;
use Yii;
use common\models\ofd\Ofd;
use common\models\ofd\OfdKkt;
use common\models\ofd\OfdReceiptItem;
use common\models\ofd\OfdStore;
use frontend\modules\ofd\components\AbstractClient;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

class Client extends AbstractClient {

    const OUTLET_LIST = 'OutletList';
    const KKT_LIST = 'KKTList';
    const FN_LIST = 'FnHistory';
    const SHIFT_LIST = 'ShiftList';
    const DOCUMENT_LIST = 'DocumentList';
    const DOCUMENT_INFO = 'DocumentInfo';

    // Версия документа ФФД 1.1
    const FD_ITEMS_LIST = '1059'; // предмет расчета
    const FD_ITEM_SUM = '1043'; // стоимость предмета расчета с учетом скидок и наценок
    const FD_ITEM_PRICE = '1079'; // цена за единицу предмета расчета с учетом скидок и наценок
    const FD_ITEM_QUANTITY = '1023'; // количество предмета расчета
    const FD_ITEM_NDS_KEY = '1199'; // ставка НДС: 18% - 1, 10% - 2, 18/118 - 3, 10/110 - 4, 0% - 5, безНДС - 6
    const FD_ITEM_NAME = '1030'; // наименование предмета расчета
    const FD_ITEM_SUBJECT = '1212'; // признак предмета расчета

    const FD_NDS_NO = 6;
    const FD_NDS_0 = 5;
    const FD_NDS_10_110 = 4;
    const FD_NDS_20_120 = 3;
    const FD_NDS_10 = 2;
    const FD_NDS_20 = 1;

    const FD_SUBJECT_PRODUCT = [1,2];
    const FD_SUBJECT_SERVICE = [3,4];
    
    public $_debugMode = true;

    // PROD
    protected static $authHost = 'https://auth-lk-ofd.taxcom.ru';
    protected static $apiHost = 'https://api-lk-ofd.taxcom.ru';

    // TEST
    // protected static $authHost = 'http://taxcom';
    // protected static $apiHost = 'http://taxcom';

    protected static $returnUri = '/ofd/taxcom/default/return';
    protected static $authUri = '/core/connect/authorize';
    protected static $authTokenUri = '/core/connect/token';

    public $rawData = [
        self::OUTLET_LIST => [],
        self::KKT_LIST => [],
        self::FN_LIST => [],
        self::SHIFT_LIST => [],
        self::DOCUMENT_LIST => []
    ];

    public function getStores(): array
    {
        $this->_getOfdData(self::OUTLET_LIST);

        $result = [];
        foreach ($this->rawData[self::OUTLET_LIST] as $item) {
            $result[] = [
                'uid' => $item['id'],
                'parent_uid' => null,
                'name' => $item['name'],
                'address' => $item['address'],
                'store_type_id' => OfdStore::TYPE_STORE,
            ];
        }

        return $result;
    }

    public function getKkts(OfdStore $store = null): array
    {
        $this->_getOfdData(self::KKT_LIST, ['id' => $store->uid]);

        $result = [];
        foreach ($this->rawData[self::KKT_LIST] as $item) {
            $result[] = [
                'uid' => $item['kktRegNumber'],
                'registration_number' => $item['kktRegNumber'],
                'store_uid' => $item['outlet_id'],
                'name' => $item['name'],
                'address' => $store->address,
            ];
        }

        return $result;
    }

    public function getFns(OfdKkt $kkt = null): array
    {
        $this->_getOfdData(self::FN_LIST, ['kktRegNumber' => $kkt->registration_number]);

        $result = [];

        foreach ($this->rawData[self::FN_LIST] as $item) {
            $result[] = [
                'fn' => $item['fn'],
                'isActive' => ($item['statusFn'] == 'Active')
            ];
        }

        return $result;
    }

    public function getReceipts(OfdStore $store = null, OfdKkt $kkt = null, \DateTime $periodFrom, \DateTime $periodTill): array
    {
        $begin = $periodFrom->format('Y-m-d\TH:i:s');
        $end   = $periodTill->format('Y-m-d\TH:i:s');

        $this->_getOfdData(self::SHIFT_LIST, ['fn' => $kkt->fiscal_number, 'begin' => $begin, 'end' => $end]);

        if (empty($this->rawData[self::SHIFT_LIST]) && strlen($kkt->other_fiscal_numbers)) {
            $otherFiscalNumbers = json_decode($kkt->other_fiscal_numbers);
            foreach ($otherFiscalNumbers as $otherFn) {
                $this->_getOfdData(self::SHIFT_LIST, ['fn' => $otherFn, 'begin' => $begin, 'end' => $end]);
            }
        }

        foreach ($this->rawData[self::SHIFT_LIST] as $shift) {
            $this->_getOfdData(self::DOCUMENT_LIST, [
                'fn' => $shift['fn'],
                'shift' => $shift['shiftNumber'],
                'kktRegNumber' => $kkt->registration_number
            ]);

            foreach ($this->rawData[self::DOCUMENT_LIST] as &$doc) {
                $doc['items'] = $this->_getOfdReceipt(self::DOCUMENT_INFO, ['fn' => $doc['fn'], 'fd' => $doc['document_number']]);
            }
        }

        return $this->rawData[self::DOCUMENT_LIST];
    }

    private function _getOfdReceipt($apiMethod, $params = []) {

        $requestHeader = [
            'content-Type: application/json; charset=utf-8',
            'Authorization: Bearer ' . $this->_userData['sessionToken'],
        ];


        $apiPath = '/API/v2/' . $apiMethod . '/?' . http_build_query($params);

        $curl = new \common\components\curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
        ])->get(self::$apiHost . $apiPath);

        $this->debugLog($curl, __METHOD__);

        $requestResult = json_decode($response, true);

        if (!$curl->errorCode) {

            if ($requestResult && isset($requestResult['document']) && isset($requestResult['document'][self::FD_ITEMS_LIST])) {

                $receiptItems = [];
                foreach ($requestResult['document'][self::FD_ITEMS_LIST] as $p) {
                    $receiptItems[] = [
                        'sum' => $p[self::FD_ITEM_SUM],
                        'price' => $p[self::FD_ITEM_PRICE],
                        'quantity' => $p[self::FD_ITEM_QUANTITY],
                        'nds_no' => (self::FD_NDS_NO == $p[self::FD_ITEM_NDS_KEY]) ? $p[self::FD_ITEM_SUM] : 0,
                        'nds_0' => (self::FD_NDS_0 == $p[self::FD_ITEM_NDS_KEY]) ? 0 : 0,
                        'nds_10' => (self::FD_NDS_10 == $p[self::FD_ITEM_NDS_KEY]) ? 10/100 * $p[self::FD_ITEM_SUM] : 0,
                        'nds_20' => (self::FD_NDS_20 == $p[self::FD_ITEM_NDS_KEY]) ? 20/100 * $p[self::FD_ITEM_SUM] : 0,
                        'nds_calculated_20' => (self::FD_NDS_20_120 == $p[self::FD_ITEM_NDS_KEY]) ? 20/120 * $p[self::FD_ITEM_SUM] : 0,
                        'nds_calculated_10' => (self::FD_NDS_10_110 == $p[self::FD_ITEM_NDS_KEY]) ? 10/110 * $p[self::FD_ITEM_SUM] : 0,
                        'barcode' => null,
                        'name' => $p[self::FD_ITEM_NAME],
                        'product_type' => in_array($p[self::FD_ITEM_SUBJECT], self::FD_SUBJECT_PRODUCT) ? 1 : 0,
                        'product_code' => null,
                        'modifiers' => null,
                    ];
                }

            } else {
                $this->errorLog($curl, __METHOD__);

                return [];
            }

            return $receiptItems;
        }

        $this->errorLog($curl, __METHOD__);

        return false;

    }

    private function _getOfdData($apiMethod, $params = []) {

        $params['pn'] = 1;   // page number
        $params['ps'] = 1000; // records per page
        $requestsLimit = 10; // max 10,000 records per one request // todo: check this limit!

        $requestHeader = [
            'content-Type: application/json; charset=utf-8',
            'Authorization: Bearer ' . $this->_userData['sessionToken'],
        ];

        while ($requestsLimit > 0) {

            $apiPath = '/API/v2/' . $apiMethod . '/?' . http_build_query($params);

            $curl = new \common\components\curl\Curl();
            $response = $curl->setOptions([
                CURLOPT_HTTPHEADER => $requestHeader,
            ])->get(self::$apiHost . $apiPath);

            $this->debugLog($curl, __METHOD__);

            $requestResult = json_decode($response, true);

            // Success exit (no more records)
            if (isset($requestResult['counts']) && $requestResult['counts']['recordInResponceCount'] == 0)
                return true;

            if ($requestsLimit == 0)
                return false;

            $params['pn']++;
            $requestsLimit--;

            if (!$curl->errorCode) {

                if ($requestResult && isset($requestResult['records'])) {

                    // Список магазинов
                    if (self::OUTLET_LIST == $apiMethod) {
                        foreach ($requestResult['records'] as $r) {
                            $this->rawData[self::OUTLET_LIST][] = [
                                'id' => $r['id'],
                                'name' => $r['name'],
                                'address' => $r['address'],
                                'problem' => $r['problemIndicator']
                            ];
                        }
                    // Список ККТ
                    } elseif (self::KKT_LIST == $apiMethod) {
                        foreach ($requestResult['records'] as $r) {
                            $this->rawData[self::KKT_LIST][] = [
                                'outlet_id' => $params['id'],
                                'name' => $r['name'],
                                'kktRegNumber' => $r['kktRegNumber'],
                                'problem' => $r['problemIndicator']
                            ];
                        }
                    // Список ФН
                    } elseif (self::FN_LIST == $apiMethod) {
                        foreach ($requestResult['records'] as $r) {
                            $this->rawData[self::FN_LIST][] = [
                                'fn' => $r['fn'],
                                'kktRegNumber' => $params['kktRegNumber'],
                                'statusFn' => $r['statusFn']
                            ];
                        }
                    // Список смен
                    } elseif (self::SHIFT_LIST == $apiMethod) {
                        foreach ($requestResult['records'] as $r) {
                            $this->rawData[self::SHIFT_LIST][] = [
                                'fn' => $params['fn'],
                                'shiftNumber' => $r['shiftNumber']
                            ];
                        }
                    // Список чеков
                    } elseif (self::DOCUMENT_LIST == $apiMethod) {
                        foreach ($requestResult['records'] as $r) {
                            $this->rawData[self::DOCUMENT_LIST][] = [
                                'fn' => $params['fn'],
                                'date_time' => $r['dateTime'],
                                'document_number' => $r['fdNumber'],
                                'kkt_factory_number' => null, // wtf
                                'kkt_registration_number' => $params['kktRegNumber'],
                                'kkt_uid' => $params['kktRegNumber'],
                                'shift_number' => $r['shiftNumber'],
                                'number' => $r['numberInShift'],
                                'operation_type_id' => self::_getOperationTypeId($r['accountingType']),
                                'address' => null, // wtf
                                'place' => null, // wtf
                                'total_sum' => $r['sum'],
                                'cash_sum' => $r['cash'],
                                'cashless_sum' => $r['electronic'],
                                'nds_20' => max($r['nds20'] ?? 0, $r['nds18'] ?? 0),
                                'nds_10' => $r['nds10'],
                                'nds_0' => $r['nds0Sum'],
                                'nds_no' => $r['nondsSum'],
                                'nds_calculated_20' => max($r['ndsC20'] ?? 0, $r['ndsC18'] ?? 0),
                                'nds_calculated_10' => $r['ndsC10'],
                                'operator_name' => $r['cashier'],
                                'operator_inn' => '',
                                'fiscal_tag' => $r['fpd'],
                                'contractor_name' => '',
                                'contractor_inn' => '',
                                'items' => [], // to fill later
                                'modifiers' => null,
                            ];
                        }
                    } else {
                        return false;
                    }

                    // Success exit (one page total)
                    if ($requestResult['counts']['recordCount'] == $requestResult['counts']['recordInResponceCount'])
                        return true;

                    continue;

                } elseif (!empty($requestResult['commonDescription'])) {
                    if (Yii::$app->id == 'app-frontend') {
                        Yii::$app->session->setFlash('error', $requestResult['commonDescription']);
                    }
                    $this->errorLog($curl, __METHOD__);

                    return false;
                }
            }

            $this->errorLog($curl, __METHOD__);

            return false;

        };

        return false;
    }

    /**
     * @param $accountingType
     * @return int|null
     */
    private static function _getOperationTypeId($accountingType)
    {
        switch ($accountingType) {
            case 'Income':
                return OfdOperationType::INCOME;
            case 'IncomeReturn':
                return OfdOperationType::INCOME_RETUSN;
            case 'Expenditure':
                return OfdOperationType::EXPENSE;
            case 'ExpendituReReturn':
                return OfdOperationType::EXPENSE_RETUSN;
        }

        return null;
    }

    public function getAuthUrl()
    {
        $clientId = Yii::$app->params['ofd']['taxcom']['client_id'] ?? null;
        $redirectUri = Yii::$app->request->hostInfo . Url::to(self::$returnUri);
        $responseType = 'code';
        $scope = 'api';

        return self::$authHost . self::$authUri . '?' . http_build_query([
            'scope' => $scope,
            'client_id' => $clientId,
            'redirect_uri' => $redirectUri,
            'response_type' => $responseType,
        ]);
    }

    /**
     * @param $code
     * @return array
     */
    public function getAuthToken($code)
    {
        $clientId = Yii::$app->params['ofd']['taxcom']['client_id'] ?? null;
        $clientSecret = Yii::$app->params['ofd']['taxcom']['client_secret'] ?? null;
        $redirectUri = Yii::$app->request->hostInfo . Url::to(self::$returnUri);
        $grandType = 'authorization_code';
        
        $requestHeader = [
            'Authorization: Basic ' . base64_encode(urlencode($clientId) .':'. urlencode($clientSecret)),
        ];

        $requestData = [
            'code' => $code,            
            'grant_type' => $grandType,
            'redirect_uri' => $redirectUri,
        ];

        $time = time();
        
        $curl = new \common\components\curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => http_build_query($requestData),
        ])->post(self::$authHost . self::$authTokenUri);

        $this->debugLog($curl, __METHOD__);

        $result = json_decode($response, true);

        if (!$curl->errorCode) {

            if (isset($result['access_token'], $result['expires_in'])) {

                return [
                    'sessionToken' => $result['access_token'],
                    'sessionTokenExpires' => $time + $result['expires_in']
                ];

            } elseif (!empty($result['commonDescription'])) {
                if (Yii::$app->id == 'app-frontend') {
                    Yii::$app->session->setFlash('error', $result['commonDescription']);
                }
                $this->errorLog($curl, __METHOD__);

                return [];
            }
        }

        $this->errorLog($curl, __METHOD__);

        return [];
    }
 
    // LOG ////////////////////////

    /**
     * @param Curl $curl
     * @return string
     */
    public static function curlLogMsg(Curl $curl)
    {
        if (method_exists($curl, 'getDump')) {
            return $curl->getDump();
        } else {
            $data = [
                'curl_info' => curl_getinfo($curl->curl),
                'curl_postfields' => ArrayHelper::getValue($curl->getOptions(), CURLOPT_POSTFIELDS),
                'response_headers' => $curl->responseHeaders,
                'response' => $curl->response,
            ];
            if ($curl->errorCode || $curl->errorText) {
                $data['error_code'] = $curl->errorCode;
                $data['error_text'] = $curl->errorText;
            }

            return VarDumper::dumpAsString($data);
        }
    }

    /**
     * @inheritdoc
     */
    public function debugLog(Curl $curl, $method)
    {
        if ($this->_debugMode) {
            $logFile = Yii::getAlias('@runtime/logs/debug_taxcom_2' . '.log');
            file_put_contents($logFile, $method . "\n" . self::curlLogMsg($curl) . "\n\n", FILE_APPEND);
        }
    }

    /**
     * @inheritdoc
     */
    public function errorLog(Curl $curl, $method)
    {
        // Yii::warning($method . "\n" . self::curlLogMsg($curl) . "\n", 'ofd');
    }    
}