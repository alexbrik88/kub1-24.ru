<?php

namespace frontend\modules\ofd\modules\taxcom\components;

use common\models\ofd\OfdKkt;
use common\models\ofd\OfdStatementUpload;
use common\models\ofd\OfdStore;
use frontend\modules\ofd\components\AbstractHelper;
use frontend\models\log\LogHelper;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;

/**
 * Class Helper
 */
class Helper extends AbstractHelper {

    public function storeRequest()
    {
        /** @var Client $client */
        $client = $this->getClient();
        $items = $client->getStores();
        $result = $this->saveStores($items);

        return $result;
    }

    public function kktRequest(OfdStore $store = null)
    {
        /** @var Client $client */
        $client = $this->getClient();
        $items = $client->getKkts($store);
        $result = $this->saveKkts($items);

        return $result;
    }

    public function fnRequest(OfdKkt $kkt = null)
    {
        /** @var Client $client */
        $client = $this->getClient();
        $items = $client->getFns($kkt);
        $result = $this->saveFns($kkt, $items);

        return $result;
    }

    public function receiptRequest(OfdStore $store = null, OfdKkt $kkt = null, \DateTime $periodFrom, \DateTime $periodTill, int $source) : ?array
    {
        // todo
    }

    /**
     * @return string
     */
    public function getAuthUrl()
    {
        /** @var Client $client */
        $client = $this->getClient();
        return $client->getAuthUrl();
    }

    /**
     * @param $code
     * @return array
     */
    public function getAuthToken($code)
    {
        /** @var Client $client */
        $client = $this->getClient();
        return $client->getAuthToken($code);
    }
}