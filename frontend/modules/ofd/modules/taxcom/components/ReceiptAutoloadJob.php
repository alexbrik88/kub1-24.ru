<?php

namespace frontend\modules\ofd\modules\taxcom\components;

use common\models\ofd\Ofd;
use common\models\ofd\OfdKkt;
use common\models\ofd\OfdStatementUpload;
use common\models\ofd\OfdStore;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;

/**
 * Автоматическая загрузка чеков для терминалов магазина
 */
class ReceiptAutoloadJob  extends \yii\base\BaseObject implements \yii\queue\JobInterface
{
    public $storeId;

    /**
     * @param Queue $queue which pushed and is handling the job
     * @return void|mixed result of the job execution
     */
    public function execute($queue)
    {
        if ($store = OfdStore::findOne($this->storeId)) {
            $ofd = Ofd::findOne([
                'id' => Ofd::TAXCOM,
                'is_active' => 1,
            ]);
            if ($ofd && $store->company && $store->kkts) {
                $helper = new Helper($ofd, null, $store->company);
                $from = date_create('-1 day')->setTime(0, 0, 0);
                $till = date_create('-1 day')->setTime(23, 59, 59);

                foreach ($store->kkts as $kkt) {
                    $statementUpload = new OfdStatementUpload;
                    $statementUpload->company_id = $helper->company->id;
                    $statementUpload->ofd_id = $helper->ofd->id;
                    $statementUpload->store_id = $store->id;
                    $statementUpload->kkt_id = $kkt->id;
                    $statementUpload->source = OfdStatementUpload::SOURCE_OFD_AUTO;
                    $statementUpload->period_from = $from->format('Y-m-d');
                    $statementUpload->period_till = $till->format('Y-m-d');
                    $statementUpload->saved_count = 0;

                    if ($statementUpload->save()) {
                        LogHelper::log($statementUpload, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE);
                        $items = $helper->client->getReceipts($store, $kkt, $from, $till);
                        $helper->saveReceipts($store, $kkt, $items, $statementUpload);
                    }
                }
            } else {
            }
        }
    }
}
