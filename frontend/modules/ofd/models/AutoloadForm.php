<?php

namespace frontend\modules\ofd\models;

use Yii;
use common\models\ofd\Ofd;
use common\models\ofd\OfdStore;

/**
 * ofd module definition class
 */
class AutoloadForm extends \yii\base\Model
{
    public static $modeList = [
        Ofd::AUTOLOAD_MODE_NO => 'Нет',
        Ofd::AUTOLOAD_MODE_DAILY => 'Каждый день',
    ];

    /**
     * form attributes
     */
    public $mode;

    /**
     * @var OfdStore
     */
    private $_store;

    /**
     * {@inheritdoc}
     */
    public function __construct(OfdStore $store, $params = [])
    {
        $this->_store = $store;
        $this->mode = $store->autoload_mode;

        parent::__construct($params);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mode'], 'in', 'range' => array_keys(self::$modeList)],
        ];
    }

    /**
     * @inheritdoc
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        return $this->_store->updateAttributes(['autoload_mode' => $this->mode]) === 1;
    }
}
