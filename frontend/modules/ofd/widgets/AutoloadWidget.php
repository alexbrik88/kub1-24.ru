<?php

namespace frontend\modules\ofd\widgets;

use common\models\ofd\OfdStatementUpload;
use common\models\ofd\OfdStore;
use frontend\modules\ofd\models\AutoloadForm;
use yii\base\Widget;

class AutoloadWidget extends Widget
{
    private $_store;

    public function setStore($store)
    {
        if ($store instanceof OfdStore)
            $this->_store = $store;
    }

    public function getStore()
    {
        return $this->_store;
    }

    public function run()
    {
        if ($store = $this->getStore()) {
            $model = new AutoloadForm($store);
            $uploadedArray = OfdStatementUpload::find()->andWhere([
                'store_id' => $store->id,
                'source' => OfdStatementUpload::SOURCE_OFD_AUTO,
            ])->orderBy([
                'created_at' => SORT_DESC,
            ])->limit(5)->all();
        } else {
            $model = null;
            $uploadedArray = [];
        }

        return $this->render('_autoload', [
            'model' => $model,
            'store' => $store,
            'uploadedArray' => $uploadedArray,
        ]);
    }
}
