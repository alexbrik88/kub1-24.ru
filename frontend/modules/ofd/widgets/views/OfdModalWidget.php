<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $modalOptions array */
/* @var $pjaxOptions array */
/* @var $content mixed */

$js = <<<JS
var vidimusSubmitClicked = false;

$(document).on("click", ".ofd_module_open_link", function(e) {
    e.preventDefault();
    $.pjax({url: this.href, container: "#ofd_module_pjax", push: $("#ofd_module_modal").data("push-state")});
    $("#ofd_module_modal").modal("show");
});

$(document).on("hide.bs.modal", "#ofd_module_modal", function (e) {
    var modal = $(this);
    if (modal.data("push-state")) {
        history.pushState({}, modal.data("page-title"), modal.data("page-url"));
    }
});

$(document).on("hidden.bs.modal", "#ofd_module_modal", function (e) {
    $("#ofd_module_pjax", this).html("");
});
JS;

$this->registerJs($js);
?>

<?= Html::beginTag('div', $modalOptions); ?>
    <div class="modal-dialog">
        <div class="modal-content">
            <?php Pjax::begin($pjaxOptions); ?>

            <?= $content ?>

            <?php Pjax::end(); ?>
        </div>
    </div>
<?= Html::endTag('div') ?>