<?php

namespace frontend\modules\ofd\widgets;

use frontend\modules\cash\modules\banking\components\Banking;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class OfdModalWidget extends Widget
{
    public static $rendered = false;

    /**
     * Show the modal on page load
     * @var boolean
     */

    public $showModal = false;
    /**
     * whether to enable push state
     * @var boolean
     */
    public $enablePushState = true;

    public function run()
    {
        return OfdModalWidget::$rendered ? null : $this->runWidget();
    }

    public function runWidget()
    {
        OfdModalWidget::$rendered = true;

        $enablePushState = $this->enablePushState ? 'true' : 'false';
        $view = $this->view;

        $modalOptions = [
            'id' => 'ofd_module_modal',
            'class' => 'modal fade',
            'role' => 'modal',
            'data-push-state' => $enablePushState,
            'data-page-title' => $view->title,
            'data-page-url' => Url::to(Banking::currentRoute()),
        ];

        $pjaxOptions = [
            'id' => 'ofd_module_pjax',
            'enablePushState' => $this->enablePushState,
            'enableReplaceState' => false,
            'timeout' => 10000,
            'linkSelector' => '.ofd_module_link',
            'options' => [
                'class' => 'modal-content',
                'data-push-state' => $enablePushState,
            ]
        ];

        $content = ArrayHelper::getValue($view->params, 'ofdModuleContent', '');

        return $this->render('OfdModalWidget', [
            'modalOptions' => $modalOptions,
            'pjaxOptions' => $pjaxOptions,
            'content' => $content,
            'show' => !empty($content) || $this->showModal
        ]);
    }
}
