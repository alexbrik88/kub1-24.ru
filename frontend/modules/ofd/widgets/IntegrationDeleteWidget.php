<?php

namespace frontend\modules\ofd\widgets;

use yii\base\Widget;

class IntegrationDeleteWidget extends Widget
{
    /**
     * @var string
     */
    public $ofdName = false;

    public function run()
    {
        return $this->render('integrationDeleteWidget', [
            'ofdName' => $this->ofdName,
        ]);
    }
}
