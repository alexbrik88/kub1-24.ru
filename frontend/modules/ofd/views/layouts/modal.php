<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this \yii\web\View */
/* @var $content string */

$this->beginPage();
$this->head();
$this->beginBody();
Pjax::begin([
    'id' => 'ofd_module_pjax',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'timeout' => 10000,
]);
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<div class="modal-body">
    <?= $content ?>
</div>

<?php
Pjax::end();
$this->endBody();
$this->endPage(true);