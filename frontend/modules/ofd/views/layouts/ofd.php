<?php
/**
 * @var $this  yii\web\View
 */

use backend\models\Bank;
use common\components\ImageHelper;
use common\components\banking\AbstractService;
use common\models\dictionary\bik\BikDictionary;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\Module;
use frontend\widgets\Alert;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$ofd = $this->context->ofd;

$this->beginContent('@frontend/modules/ofd/views/layouts/main.php');
?>

<div class="ofd-content" style="position: relative; min-height: 110px;">
    <div class="d-flex">
        <div class="mr-3">
            <?= Html::img('/img/ofd/' . $ofd->logo, [
                'style' => 'width: 150px;',
            ]); ?>
        </div>
        <div style="padding: 6px 10px; font-size: 10px; line-height: 18px; background-color: #eee;">
            Для обеспечения безопасности данных используется протокол зашифрованного соединения SSL
            - надежный протокол для передачи конфиденциальной информации
            и соблюдаются требования международного стандарта PCI DSS
            по хранению и передаче конфиденциальной информации.
        </div>
    </div>
    <hr/>

    <?= Alert::widget(); ?>

    <?php echo $content ?>

    <div class="statement-loader"></div>
</div>

<?php $this->endContent(); ?>
