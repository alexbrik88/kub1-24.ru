<?php

use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Запрос чеков из ОФД';


/** @var $employee  */
/** @var $company  */
/** @var $ofdUserArray array */
?>

<?php if ($ofdUserArray) : ?>
    <div class="row">
        <?php foreach ($ofdUserArray as $ofdUser) : ?>
            <?php $ofd = $ofdUser->ofd ?>
            <div class="col-xs-6">
                <?= Html::a($ofd->name, [
                    "/ofd/{$ofd->alias}/default/integration",
                ], [
                    'class' => 'button-regular button-hover-transparent ofd_module_open_link',
                ]) ?>
            </div>
        <?php endforeach ?>
    </div>
<?php else : ?>
    Для настройки интеграции с вашим ОФД, перейдите на вкладку
    <?= Html::a('ОФД', ['/ofd/default/index']) ?>
<?php endif ?>
