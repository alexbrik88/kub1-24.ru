<?php

use frontend\modules\ofd\components\OfdHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $employee common\models\employee\Employee */
/* @var $searchModel frontend\modules\ofd\models\OfdSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Подключение ОФД для автоматической загрузки чеков';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ofd-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= common\components\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        //'emptyText' => $emptyMessage,
        'tableOptions' => [
            'class' => 'table table-striped table-bordered table-hover dataTable customers_table',
        ],
        'headerRowOptions' => [
            'class' => 'heading',
        ],
        'layout' => '{items}',
        'columns' => [
            [
                'attribute' => 'logo',
                'label' => false,
                'enableSorting' => false,
                'format' => 'html',
                'contentOptions' => [
                    'class' => 'py-0 px-1',
                ],
                'value' => function ($model) {
                    return Html::img('/img/ofd/' . $model->logo, [
                        'style' => 'height: 100px;',
                    ]);
                }
            ],
            [
                'attribute' => 'name',
                'enableSorting' => false,
                'label' => 'ОФД',
            ],
            [
                'attribute' => 'is_active',
                'enableSorting' => false,
                'label' => false,
                'format' => 'raw',
                'value' => function ($model) use ($employee) {
                    if ($model->is_active) {
                        $label = $employee->getOfdUser($model)->exists() ? 'Отключить' : 'Подключить';
                        return Html::a($label, [
                            "/ofd/{$model->alias}/default/integration",
                        ], [
                            'class' => 'button-regular button-hover-transparent ofd_module_open_link',
                            'style' => 'width: 130px;',
                        ]);
                    } else {
                        return Html::button('Скоро', [
                            'class' => 'button-regular button-hover-transparent disabled',
                            'style' => 'width: 130px; background-color: #eee; color: #999;',
                            'disabled' => true,
                        ]);
                    }
                }
            ],
            [
                'label' => 'Дата подключения',
                'enableSorting' => false,
                'value' => function ($model) use ($employee) {
                    if ($model->is_active) {
                        if ($integration = (new OfdHelper($employee, $model))->getOfdUser()) {
                            return date('d.m.Y', $integration->created_at);
                        }
                    }

                    return '--';
                }
            ],
        ],
    ]); ?>


</div>
