<?php

namespace frontend\modules\ofd\controllers;

use common\models\ofd\Ofd;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\ofd\models\OfdSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `ofd` module
 */
class DefaultController extends Controller
{
    /**
     * @var array
     */
    public $notAjaxAction = [
        'index',
        'view',
    ];

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\UserRole::ROLE_CHIEF,
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $employee = Yii::$app->user->identity;
        $searchModel = new OfdSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'employee' => $employee,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ofd model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionSelect()
    {
        $this->layout = 'main';

        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        $ofdUserArray = $employee->getOfdUsers()->andWhere([
            'company_id' => $company->id,
        ])->all();

        return $this->render('select', [
            'employee' => $employee,
            'company' => $company,
            'ofdUserArray' => $ofdUserArray,
        ]);
    }

    /**
     * Finds the Ofd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ofd the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ofd::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * This method is invoked right after an action is executed.
     * @param  yii\base\Action $action
     * @param  mixed $result
     * @return mixed
     */
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        /**
         * If the request is not Ajax rendering parent page
         */
        if (!Yii::$app->request->isAjax && !in_array($action->id, $this->notAjaxAction)) {
            $domDocument = new \DOMDocument();
            @$domDocument->loadHTML($result);

            if ($ofdModulePjax = $domDocument->getElementById('ofd_module_pjax')) {
                $ofdContent = "";
                $innerNodes = $ofdModulePjax->childNodes;
                for ($i = 0; $i < $innerNodes->length; $i++) {
                    $ofdContent .= $domDocument->saveHTML($innerNodes->item($i));
                }
                $this->view->params['ofdModuleContent'] = $ofdContent;
            }

            try {
                $params = Banking::routeDecode(Yii::$app->request->get('p'));
                $route = ArrayHelper::remove($params, 0);
                Yii::$app->request->setQueryParams(array_merge(Yii::$app->request->getQueryParams(), $params));
                $result = Yii::$app->runAction($route, $params);
            } catch (\Exception $e) {
                $result = Yii::$app->runAction($this->pageRoute, $this->pageRouteParams);
            }
        }

        return $result;
    }
}
