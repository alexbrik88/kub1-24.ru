<?php

namespace frontend\modules\ofd\components;

use common\models\ofd\Ofd;
use common\models\ofd\OfdKkt;
use common\models\ofd\OfdStore;
use frontend\modules\ofd\components\OfdHelper;
use Yii;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class AbstractClient
 */
abstract class AbstractClient extends \yii\base\BaseObject
{
    protected $_appData;
    protected $_userData;
    protected $_response;

    /**
     * {@inheritdoc}
     */
    public function __construct(array $appData, array $userData, $config = [])
    {
        $this->_appData = $appData;
        $this->_userData = $userData;

        parent::__construct($config);
    }

    abstract public function getStores() : array;

    abstract public function getKkts(OfdStore $store = null) : array;

    abstract public function getReceipts(OfdStore $store = null, OfdKkt $kkt = null, \DateTime $periodFrom, \DateTime $periodTill) : array;

    /**
     * {@inheritdoc}
     */
    protected function setResponse(\yii\httpclient\Response $response = null)
    {
        $this->_response = $response;
    }

    /**
     * {@inheritdoc}
     */
    protected function getResponse()
    {
        return $this->_response;
    }

    /**
     * {@inheritdoc}
     */
    public function appData($param = null, $default = null)
    {
        if ($param === null) {
            return $this->_appData;
        }

        return ArrayHelper::getValue($this->_appData, $param, $default);
    }

    /**
     * {@inheritdoc}
     */
    public function setUserData(array $data)
    {
        $this->_userData = $data;
    }

    /**
     * {@inheritdoc}
     */
    public function userData($param = null, $default = null)
    {
        if ($param === null) {
            return $this->_userData;
        }

        return ArrayHelper::getValue($this->_userData, $param, $default);
    }

    /**
     * {@inheritdoc}
     */
    public function log($response, $method = '')
    {
        $message = sprintf("\n\n%s %s\n\n%s\n", date('Y-m-d H:i:s'), $method, VarDumper::dumpAsString($response));

        file_put_contents(
            Yii::getAlias('@runtime/logs/ofd.platforma.client.log'),
            $message,
            FILE_APPEND
        );
    }
}
