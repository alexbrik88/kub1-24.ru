<?php

namespace frontend\modules\ofd\components;

/**
 * OfdException represents an exception caused by incorrect Ofd API request.
 */
class OfdException extends \yii\base\Exception
{
    /**
     * @return string the user-friendly name of this exception
     */
    public function getName()
    {
        return 'Ofd Error';
    }
}
