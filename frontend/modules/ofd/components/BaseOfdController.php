<?php

namespace frontend\modules\ofd\components;

use common\models\ofd\Ofd;
use common\models\ofd\OfdStore;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\ofd\models\AutoloadForm;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * BaseOfd controller for the `ofd` module
 * @property Ofd $ofd
 */
class BaseOfdController extends Controller
{
    /**
     * Run this route if the request is not Ajax
     * @var array|string
     */
    public $pageRoute = 'retail/ofd/index';
    public $pageRouteParams = [];

    /**
     * @var array
     */
    public $notAjaxAction = [
        'return',
    ];

    protected $_ofd;
    protected $_helper;

    /**
     * @return Ofd
     * @throws NotFoundHttpException
     */
    public function getOfd()
    {
        if ($this->_ofd === null) {
            $this->_ofd = Ofd::findOne([
                'alias' =>$this->module->id,
                'is_active' => true,
            ]);

            if ($this->_ofd === null) {
                throw new NotFoundHttpException('The requested ofd not exist.');
            }
        }

        return $this->_ofd;
    }

    /**
     * @return AbstractHelper
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function getHelper()
    {
        if ($this->_helper === null) {
            $ofd = $this->getOfd();
            $class = "frontend\\modules\\ofd\\modules\\{$ofd->alias}\\components\\Helper";
            $employee = Yii::$app->user->identity;
            $this->_helper = new $class($ofd, $employee, $employee->company);
        }

        return $this->_helper;
    }

    /**
     * set statement autoload mode
     */
    public function actionSetAutoload($storeId)
    {
        $company = Yii::$app->user->identity->company;
        $ofd = $this->getOfd();
        $store = OfdStore::findOne([
            'id' => $storeId,
            'company_id' => $company->id,
            'ofd_id' => $ofd->id,
        ]);

        if ($store === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $model = new AutoloadForm($store);

        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('ajax')) {
            $model->load(Yii::$app->request->post());

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return 1;
        }

        return 0;
    }

    /**
     * This method is invoked right after an action is executed.
     * @param  yii\base\Action $action
     * @param  mixed $result
     * @return mixed
     */
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        /**
         * If the request is not Ajax rendering parent page
         */
        if (!Yii::$app->request->isAjax && !in_array($action->id, $this->notAjaxAction)) {
            $domDocument = new \DOMDocument();
            @$domDocument->loadHTML($result);

            if ($ofdModulePjax = $domDocument->getElementById('ofd_module_pjax')) {
                $ofdContent = "";
                $innerNodes = $ofdModulePjax->childNodes;
                for ($i = 0; $i < $innerNodes->length; $i++) {
                    $ofdContent .= $domDocument->saveHTML($innerNodes->item($i));
                }
                $this->view->params['ofdModuleContent'] = $ofdContent;
            }

            try {
                $params = Banking::routeDecode(Yii::$app->request->get('p'));
                $route = ArrayHelper::remove($params, 0);
                Yii::$app->request->setQueryParams(array_merge(Yii::$app->request->getQueryParams(), $params));
                $result = Yii::$app->runAction($route, $params);
            } catch (\Exception $e) {
                $result = Yii::$app->runAction($this->pageRoute, $this->pageRouteParams);
            }
        }

        return $result;
    }
}
