<?php

namespace frontend\modules\ofd\components;

use Yii;
use common\models\Company;
use common\models\TaxRate;
use common\models\address\Country;
use common\models\employee\Employee;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\companyStructure\SalePoint;
use common\models\document\InvoiceIncomeItem;
use common\models\ofd\Ofd;
use common\models\ofd\OfdImportConfig;
use common\models\ofd\OfdReceipt;
use common\models\ofd\OfdReceiptItem;
use common\models\ofd\OfdKkt;
use common\models\ofd\OfdOperationType;
use common\models\ofd\OfdOperator;
use common\models\ofd\OfdStatementUpload;
use common\models\ofd\OfdStore;
use common\models\ofd\OfdUser;
use common\models\product\Product;
use common\models\product\ProductUnit;
use frontend\modules\cash\models\CashContractorType;
use frontend\modules\retail\models\ImportConfigForm;
use yii\helpers\ArrayHelper;

/**
 * Class AbstractHelper
 *
 * @property Company $company
 * @property Ofd $ofd
 * @property AbstractClient $client
 */
abstract class AbstractHelper extends \yii\base\BaseObject
{
    protected $_employee;
    protected $_company;
    protected $_company_id;
    protected $_ofd;
    protected $_client;
    protected $_lastError;
    protected $_ofdUser = false;
    protected $_operators;

    /**
     * Constructor.
     * @param Company $company
     * @param array $config name-value pairs that will be used to initialize the object properties
     */
    public function __construct(Ofd $ofd, Employee $employee = null, Company $company, $config = [])
    {
        $this->_company = $company;
        $this->_employee = $employee ?: ($company->employeeChief ?: $company->ownerEmployee);
        $this->_company_id = $company->id;
        $this->_ofd = $ofd;

        parent::__construct($config);
    }

    abstract public function storeRequest();

    abstract public function kktRequest(OfdStore $store = null);

    abstract public function receiptRequest(OfdStore $store = null, OfdKkt $kkt = null, \DateTime $periodFrom, \DateTime $periodTill, int $source);

    public function getEmployee()
    {
        return $this->_employee;
    }

    public function getCompany()
    {
        return $this->_company;
    }

    public function getOfd()
    {
        return $this->_ofd;
    }

    /**
     * API Client for current Ofd
     *
     * @return AbstractClient
     */
    public function getClient() : AbstractClient
    {
        if ($this->_client === null) {
            $class = "frontend\\modules\\ofd\\modules\\{$this->ofd->alias}\\components\\Client";
            $this->_client = new $class(
                ArrayHelper::getValue(Yii::$app->params, ['ofd', $this->ofd->alias]),
                ArrayHelper::getValue($this->getOfdUser(), 'data', [])
            );
        }

        return $this->_client;
    }

    /**
     * @return OfdUser|null
     */
    public function setOfdUser(OfdUser $ofdUser = null)
    {
        $this->_ofdUser = $ofdUser;
    }

    /**
     * @return OfdUser|null
     */
    public function getOfdUser()
    {
        if ($this->_ofdUser === false) {
            if (($model = $this->_company->getOfdUsersByOfd($this->_ofd)->one()) !== null) {
                $model->populateRelation('company', $this->getCompany());
                $model->populateRelation('ofd', $this->getOfd());
            }

            $this->_ofdUser = $model;
        }

        return $this->_ofdUser;
    }

    /**
     * @return OfdUser
     */
    public function getNewOfdUser()
    {
        return new OfdUser([
            'employee_id' => $this->_employee->id,
            'company_id' => $this->_company->id,
            'ofd_id' => $this->_ofd->id,
        ]);
    }

    /**
     * @return boolean
     */
    public function getHasOfdUser()
    {
        return $this->getOfdUser() !== null;
    }

    public function saveStores(array $items)
    {
        $companyId = $this->_company->id;
        $ofdId = $this->_ofd->id;
        $employeeId = $this->_employee ? $this->_employee->id : null;
        $rows = [];
        $salePointRows = [];
        $salePointQuery = (new \yii\db\Query)->from(SalePoint::tableName())->andWhere([
            'company_id' => $companyId,
        ]);

        foreach ($items as $item) {
            $rows[] = [
                $companyId,
                $ofdId,
                $item['uid'],
                $item['parent_uid'],
                $item['store_type_id'],
                $item['name'],
                $item['name'],
                $item['address'],
            ];

            if (!empty($item['uid'])) {
                $existQuery = (clone $salePointQuery)->andWhere([
                    'name' => $item['name'],
                ]);
                if (!$existQuery->exists()) {
                    $salePointRows[] = [
                        $companyId,
                        $employeeId,
                        SalePoint::TYPE_SHOP,
                        SalePoint::STATUS_ACTIVE,
                        $item['name'],
                        '',
                        strval($item['address']),
                        $item['uid'],
                    ];
                }
            }
        }

        if (!empty($rows)) {
            $db = Yii::$app->db;
            $sql = $db->queryBuilder->batchInsert(OfdStore::tableName(), [
                'company_id',
                'ofd_id',
                'uid',
                'parent_uid',
                'store_type_id',
                'name',
                'local_name',
                'address',
            ], $rows);

            $execute = $db->createCommand($sql . '
                ON DUPLICATE KEY UPDATE
                    [[parent_uid]] = VALUES([[parent_uid]]),
                    [[name]] = VALUES([[name]]),
                    [[address]] = VALUES([[address]])
            ')->execute();

            if (!empty($salePointRows)) {
                $sql = $db->createCommand()->batchInsert(SalePoint::tableName(), [
                    'company_id',
                    'responsible_employee_id',
                    'type_id',
                    'status_id',
                    'name',
                    'city',
                    'address',
                    'ofd_store_uid',
                ], $salePointRows)->getRawSql();
                $sql = 'INSERT IGNORE'.substr($sql, 6);
                $db->createCommand($sql)->execute();
            }

            if ($this->_company->ofdImportConfig && $this->_company->ofdImportConfig->import_config_id == OfdImportConfig::STORE_CASHBOX) {
                $model = new ImportConfigForm($this->_company);
                $model->save();
            }

            return OfdStore::findAll([
                'company_id' => $companyId,
                'ofd_id' => $ofdId
            ]);
        }


        return [];


    }

    public function saveKkts(array $items)
    {
        $companyId = $this->_company->id;
        $ofdId = $this->_ofd->id;
        $storeArray = OfdStore::find()->select('id')->where([
            'company_id' => $companyId,
            'ofd_id' => $ofdId,
        ])->indexBy('uid')->column();
        $rows = [];
        foreach ($items as $item) {
            $rows[] = [
                $storeArray[$item['store_uid']] ?? null,
                $item['uid'],
                $item['store_uid'],
                $companyId,
                $ofdId,
                $item['factory_number'] ?? null,
                $item['registration_number'] ?? null,
                $item['registration_status'] ?? null,
                $item['model'] ?? null,
                $item['format_fd'] ?? null,
                $item['name'],
                $item['address'],
                $item['place'] ?? $item['address'],
            ];
        }

        if (!empty($rows)) {
            $db = Yii::$app->db;
            $sql = $db->queryBuilder->batchInsert(OfdKkt::tableName(), [
                'store_id',
                'uid',
                'store_uid',
                'company_id',
                'ofd_id',
                'factory_number',
                'registration_number',
                'registration_status',
                'model',
                'format_fd',
                'name',
                'address',
                'place',
            ], $rows);

            $execute = $db->createCommand($sql . '
                ON DUPLICATE KEY UPDATE
                    [[store_id]] = VALUES([[store_id]]),
                    [[store_uid]] = VALUES([[store_uid]]),
                    [[name]] = VALUES([[name]]),
                    [[address]] = VALUES([[address]])
            ')->execute();

            if ($this->_company->ofdImportConfig && $this->_company->ofdImportConfig->import_config_id == OfdImportConfig::KKT_CASHBOX) {
                $model = new ImportConfigForm($this->_company);
                $model->save();
            }

            return OfdKkt::findAll([
                'company_id' => $companyId,
                'store_id' => $storeArray
            ]);
        }

        return [];
    }

    public function saveFns(OfdKkt $kkt, array $items)
    {
        $activeFN = null;
        $otherFNs = [];
        foreach ($items as $key => $item) {
            if ($item['isActive']) {
                $activeFN = $item['fn'];
            } else {
                $otherFNs[] = $item['fn'];
            }
        }

        $kkt->updateAttributes([
            'fiscal_number' => $activeFN,
            'other_fiscal_numbers' => json_encode($otherFNs)
        ]);

        return $kkt;
    }

    public function saveReceipts(OfdStore $store, OfdKkt $kkt, array $receiptArray, OfdStatementUpload $statementUpload)
    {
        if (empty($receiptArray)) {
            return [
                'sum' => 0,
                'count' => 0,
                'insert' => 0,
            ];
        }

        $sum = 0;
        $insert = 0;
        $count = count($receiptArray);
        $companyId = $this->_company->id;
        $ofdImportConfig = $this->_company->ofdImportConfig;
        $casbox = $ofdImportConfig ? $ofdImportConfig->cashbox($store, $kkt) : null;
        $isAccounting = $casbox ? $casbox->is_accounting : true;
        $casboxId = $casbox ? $casbox->id : null;
        $employeeId = $this->_employee ? $this->_employee->id : null;
        $uploadId = $statementUpload->id;
        $uploadId = $statementUpload->id;
        $ofdId = $this->_ofd->id;
        $storeId = $store->id;
        $salePointId = $store->salePoint ? $store->salePoint->id : null;
        $kktId = $kkt->id;
        $kktUid = $kkt->uid;
        $created_at = time();
        $receiptRows = [];
        $receiptItemsRows = [];
        $cashOrderFlowsRows = [];
        $operators = [];
        $db = Yii::$app->db;

        list($productMap, $serviceMap) = $this->productData($receiptArray);

        foreach ($receiptArray as $receipt) {
            $sum += $receipt['total_sum'];
            $items = $receipt['items'];
            $receiptUid = sprintf('%010d', $companyId)."_".
                date_create($receipt['date_time'])->format('YmdHis')."_".
                $receipt['kkt_registration_number']."_".
                $receipt['document_number'];

            $receiptRows[] = [
                $companyId,
                $receipt['date_time'],
                $receipt['kkt_registration_number'],
                $receipt['document_number'],
                $uploadId,
                $receiptUid,
                $ofdId,
                $storeId,
                $kktId,
                $kktUid,
                $created_at,
                $receipt['kkt_factory_number'],
                $receipt['shift_number'],
                $receipt['number'],
                $receipt['operation_type_id'],
                $receipt['address'],
                $receipt['place'],
                $receipt['total_sum'],
                $receipt['cash_sum'],
                $receipt['cashless_sum'],
                $this->paymentTypeId($receipt),
                $receipt['nds_20'],
                $receipt['nds_10'],
                $receipt['nds_0'],
                $receipt['nds_no'],
                $receipt['nds_calculated_20'],
                $receipt['nds_calculated_10'],
                $receipt['operator_name'],
                $receipt['operator_inn'],
                $this->operatorId($receipt),
                $receipt['fiscal_tag'],
                $receipt['contractor_name'],
                $receipt['contractor_inn'],
                json_encode($receipt['modifiers']),
            ];

            $created_at = time();
            if ($receipt['cash_sum'] > 0 && $casboxId !== null) {
                $flowType = OfdOperationType::flowType($receipt['operation_type_id']);
                if ($flowType !== null) {
                    $date = explode(' ', $receipt['date_time'])[0];
                    $cashOrderFlowsRows[] = [
                        // 'date',
                        $date,
                        // 'recognition_date',
                        $date,
                        // 'author_id',
                        $employeeId,
                        // 'company_id',
                        $companyId,
                        // 'is_internal_transfer',
                        0,
                        // 'cashbox_id',
                        $casboxId,
                        // 'number',
                        $receipt['document_number'],
                        // 'flow_type',
                        $flowType,
                        // 'is_accounting',
                        $isAccounting,
                        // 'has_invoice',
                        0,
                        // 'contractor_id',
                        CashContractorType::CUSTOMERS_TEXT,
                        // 'amount',
                        $receipt['cash_sum'],
                        // 'created_at',
                        $created_at,
                        // 'income_item_id',
                        InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER,
                        // 'description',
                        'Загружено из ОФД',
                        // 'sale_point_id',
                        $salePointId,
                        // 'ofd_receipt_uid',
                        $receiptUid,
                        // 'application',
                        '',
                        // 'other',
                        '',
                        // 'is_taxable',
                        1,
                        // 'is_prepaid_expense',
                        0,
                    ];
                }
            }

            $operators[$receipt['operator_inn']] = $receipt['operator_name'];
            $i = 0;
            foreach ($items as $key => $item) {
                $i++;
                if (isset($item['product_type'])) {
                    if ($item['product_type'] == OfdReceiptItem::TYPE_PRODUCT) {
                        $productId = $productMap[mb_strtolower($item['name'])] ?? null;
                    } else {
                        $productId = $serviceMap[mb_strtolower($item['name'])] ?? null;
                    }
                } else {
                    $productId = null;
                }
                $receiptItemsRows[] = [
                    null,
                    $companyId,
                    $receipt['date_time'],
                    $receiptUid."_".sprintf('%03d', $i),
                    $receiptUid,
                    $productId,
                    $item['name'],
                    $item['barcode'],
                    $item['price'],
                    $item['quantity'],
                    $item['nds_20'],
                    $item['nds_10'],
                    $item['nds_0'],
                    $item['nds_no'],
                    $item['nds_calculated_20'],
                    $item['nds_calculated_10'],
                    $item['sum'],
                    $item['product_type'],
                    $item['product_code'],
                    json_encode($item['modifiers']),
                ];
            }
        }

        if (!empty($receiptRows)) {
            $sql = $db->createCommand()->batchInsert(OfdReceipt::tableName(), [
                'company_id',
                'date_time',
                'kkt_registration_number',
                'document_number',
                'ofd_statement_upload_id',
                'uid',
                'ofd_id',
                'store_id',
                'kkt_id',
                'kkt_uid',
                'created_at',
                'kkt_factory_number',
                'shift_number',
                'number',
                'operation_type_id',
                'address',
                'place',
                'total_sum',
                'cash_sum',
                'cashless_sum',
                'payment_type_id',
                'nds_20',
                'nds_10',
                'nds_0',
                'nds_no',
                'nds_calculated_20',
                'nds_calculated_10',
                'operator_name',
                'operator_inn',
                'operator_id',
                'fiscal_tag',
                'contractor_name',
                'contractor_inn',
                '_modifiers',
            ], $receiptRows)->getRawSql();
            $sql = 'INSERT IGNORE'.substr($sql, 6);
            $insert = $db->createCommand($sql)->execute();
        }

        if ($insert > 0 && !empty($receiptItemsRows)) {
            $receiptUidIdMap = (new \yii\db\Query)->select('id')->from(OfdReceipt::tableName())->where([
                'ofd_statement_upload_id' => $uploadId,
            ])->indexBy('uid')->column();
            foreach ($receiptItemsRows as $key => $value) {
                $receiptId = $receiptUidIdMap[$value[4]] ?? null;
                if ($receiptId !== null) {
                    $receiptItemsRows[$key][0] = $receiptId;
                } else {
                    unset($receiptItemsRows[$key]);
                }
            }
            if (!empty($receiptItemsRows)) {
                $sql = $db->createCommand()->batchInsert(OfdReceiptItem::tableName(), [
                    'receipt_id',
                    'company_id',
                    'date_time',
                    'uid',
                    'receipt_uid',
                    'product_id',
                    'name',
                    'barcode',
                    'price',
                    'quantity',
                    'nds_20',
                    'nds_10',
                    'nds_0',
                    'nds_no',
                    'nds_calculated_20',
                    'nds_calculated_10',
                    'sum',
                    'product_type',
                    'product_code',
                    '_modifiers',
                ], $receiptItemsRows)->getRawSql();
                $sql = 'INSERT IGNORE'.substr($sql, 6);
                $db->createCommand($sql)->execute();
            }
        }

        if (!empty($cashOrderFlowsRows)) {
            $sql = $db->createCommand()->batchInsert(CashOrderFlows::tableName(), [
                'date',
                'recognition_date',
                'author_id',
                'company_id',
                'is_internal_transfer',
                'cashbox_id',
                'number',
                'flow_type',
                'is_accounting',
                'has_invoice',
                'contractor_id',
                'amount',
                'created_at',
                'income_item_id',
                'description',
                'sale_point_id',
                'ofd_receipt_uid',
                'application',
                'other',
                'is_taxable',
                'is_prepaid_expense',
            ], $cashOrderFlowsRows)->getRawSql();
            $sql = 'INSERT IGNORE'.substr($sql, 6);
            $db->createCommand($sql)->execute();
        }

        if ($insert > 0) {
            $statementUpload->updateAttributes([
                'saved_count' => $insert,
            ]);
        }

        return [
            'sum' => $sum,
            'count' => $count,
            'insert' => $insert,
        ];
    }

    /**
     * @return array
     */
    private function productData(&$receiptArray)
    {
        $productList = [];
        $serviceList = [];
        $productMap = [];
        $serviceMap = [];
        foreach ($receiptArray as $k1 => &$receipt) {
            foreach ($receipt['items'] as $k2 => $item) {
                $name = mb_strtolower($item['name']);
                switch ($item['product_type']) {
                    case 1:
                    case 2:
                        $productList[$name] = [
                            'name' => $item['name'],
                            'price' => $item['price'],
                            'barcode' => $item['barcode'],
                        ];
                        break;
                    case 3:
                    case 4:
                        $serviceList[$name] = [
                            'name' => $item['name'],
                            'price' => $item['price'],
                            'barcode' => $item['barcode'],
                        ];
                        break;
                }
            }
        }

        $productArray = empty($productList) ? [] : $this->_company->getProducts()->andWhere([
            'production_type' => Product::PRODUCTION_TYPE_GOODS,
            'is_deleted' => false,
        ])->select([
            'id',
            'title',
        ])->asArray()->all();

        $serviceArray = empty($serviceList) ? [] : $this->_company->getProducts()->andWhere([
            'production_type' => Product::PRODUCTION_TYPE_SERVICE,
            'is_deleted' => false,
        ])->select([
            'id',
            'title',
        ])->asArray()->all();

        foreach ($productArray as $product) {
            $productMap[mb_strtolower($product['title'])] = $product['id'];
        }

        foreach ($serviceArray as $product) {
            $serviceMap[mb_strtolower($product['title'])] = $product['id'];
        }

        foreach ($productMap as $title => $id) {
            if (isset($productList[$title])) {
                unset($productList[$title]);
            }
        }

        foreach ($serviceMap as $title => $id) {
            if (isset($serviceList[$title])) {
                unset($serviceList[$title]);
            }
        }

        if (!empty($productList)) {
            $productMap = $productMap + $this->addProduct($productList, Product::PRODUCTION_TYPE_GOODS);
        }

        if (!empty($serviceList)) {
            $serviceMap = $serviceMap + $this->addProduct($serviceList, Product::PRODUCTION_TYPE_SERVICE);
        }

        return [
            $productMap,
            $serviceMap,
        ];
    }

    /**
     * @return array
     */
    private function addProduct($items, $type)
    {
        if (empty($items)) {
            return [];
        }

        $rows = [];
        $creatorId = $this->_employee ? $this->_employee->id : null;
        $companyId = $this->_company->id;
        if ($this->_company->companyTaxationType->osno) {
            $ndsId = TaxRate::RATE_20;
        } else {
            $ndsId = TaxRate::RATE_WITHOUT;
        }
        $createdAt = time();
        foreach ($items as $i) {
            $rows[] = [
                $creatorId,
                $companyId,
                $type,
                $createdAt,
                $i['name'],
                $i['barcode'],
                ProductUnit::UNIT_COUNT,
                $ndsId,
                $i['price'],
                Country::COUNTRY_WITHOUT,
            ];
        }

        Yii::$app->db->createCommand()->batchInsert(Product::tableName(), [
            'creator_id',
            'company_id',
            'production_type',
            'created_at',
            'title',
            'barcode',
            'product_unit_id',
            'price_for_sell_nds_id',
            'price_for_sell_with_nds',
            'country_origin_id',
        ], $rows)->execute();

        $productMap = [];
        $productArray = $this->_company->getProducts()->andWhere([
            'production_type' => $type,
            'is_deleted' => false,
            'created_at' => $createdAt,
        ])->select([
            'id',
            'title',
        ])->asArray()->all();

        foreach ($productArray as $product) {
            $productMap[mb_strtolower($product['title'])] = $product['id'];
        }

        return $productMap;
    }

    /**
     * @return integer
     */
    public function operatorId($receipt)
    {
        if ($this->_operators === null) {
            $this->_operators = OfdOperator::find()->select([
                'id',
                'name',
            ])->andWhere([
                'company_id' => $this->_company_id,
            ])->indexBy('name')->column();
        }

        if (isset($this->_operators[$receipt['operator_name']]) || array_key_exists($receipt['operator_name'], $this->_operators)) {
            return $this->_operators[$receipt['operator_name']];
        }

        try {
            Yii::$app->db->createCommand()->insert(OfdOperator::tableName(), [
                'company_id' => $this->_company_id,
                'name' => $receipt['operator_name'],
            ])->execute();
        } catch (\yii\db\IntegrityException $e) {
           // do nothing if exist
        }

        $operator = (new \yii\db\Query)->select('*')->from(OfdOperator::tableName())->where([
            'company_id' => $this->_company_id,
            'name' => $receipt['operator_name'],
        ])->one();

        $this->_operators[$receipt['operator_name']] = $operator ? $operator['id'] : null;

        return $this->_operators[$receipt['operator_name']];
    }

    /**
     * @return integer
     */
    public function paymentTypeId($receipt)
    {
        if ($receipt['cashless_sum'] > 0) {
            return $receipt['cash_sum'] > 0 ? OfdReceipt::PAY_BOTH : OfdReceipt::PAY_CASHLESS;
        }

        return OfdReceipt::PAY_CASH;
    }
}
