<?php

namespace frontend\modules\system\controllers;

use common\models\Company;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use frontend\models\Documents;
use frontend\modules\subscribe\forms\PaymentForm;
use frontend\rbac\UserRole;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\web\NotFoundHttpException;

/**
 * Class SellingController
 * @package frontend\modules\system\controllers
 */
class SellingController extends Controller
{
    public $layoutWrapperCssClass;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserRole::ROLE_SYSADMIN],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionSelect()
    {
        $searchModel = new \frontend\modules\system\models\CompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->renderAjax('select', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionCreate($cid, $eid = null)
    {
        $company = $this->findCompany($cid);
        $employee = $eid ? $this->findEmployee($eid) : $company->getEmployeeChief();

        $companyArray = $employee ? $employee->getCompanies()
            ->alias('company')
            ->joinWith(['companyType companyType'])
            ->isBlocked(false)
            ->orderBy([
                new \yii\db\Expression('IF({{company}}.[[id]] = :id, 0, 1) ASC'),
                new \yii\db\Expression("ISNULL({{companyType}}.[[name_short]])"),
                "companyType.name_short" => SORT_ASC,
                "company.name_short" => SORT_ASC,
            ])
            ->params([':id' => $company->id])
            ->all() : [];

        if (empty($companyArray)) {
            throw new NotFoundHttpException();
        }

        $model = new PaymentForm($company, [
            'employee' => $employee,
        ]);


        if (Yii::$app->request->post('ajax') && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(\Yii::$app->request->post()) && $model->makePayment(true, false)) {
            return $this->redirect([
                '/documents/invoice/view',
                'type' => Documents::IO_TYPE_OUT,
                'id' => $model->invoice->id,
            ]);
        }

        return $this->render('create', [
            'model' => $model,
            'company' => $company,
            'companyArray' => $companyArray,
        ]);
    }

    /**
     * @return string
     */
    public function actionSummary($id)
    {
        $model = new PaymentForm($this->findCompany($id));
        $model->load(Yii::$app->request->post());

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $this->renderPartial('@frontend/modules/subscribe/views/default/summary', ['model' => $model]);
    }

    /**
     * @param $id
     * @return Employee
     * @throws NotFoundHttpException
     */
    protected function findEmployee($id)
    {
        if (($model = Employee::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @param $id
     * @return Company
     * @throws NotFoundHttpException
     */
    protected function findCompany($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException();
        }
    }
}
