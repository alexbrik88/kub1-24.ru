<?php

use common\components\grid\GridView;
use frontent\components\Icon;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\system\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Выберите компанию, на которую выставить счет';

$employee = $searchModel->employee;
?>

<style type="text/css">
    #system_selling_select_grid tr.cursor-pointer:hover td {
        color: #000;
        background-color: #f9f9f9;
    }
</style>

<?php Pjax::begin([
    'id'=>'system_selling_select_pjax',
    'enablePushState' => false,
    'enableReplaceState' => false,
]); ?>

<div class="form-group">
    <span class="label cursor-pointer" data-toggle="collapse" data-target="#collapseSearchFilter" aria-expanded="false" aria-controls="collapseSearchFilter">
        Фильтр
        <span class="caret"></span>
    </span>
    <div class="collapse" id="collapseSearchFilter">
        <?php $form = ActiveForm::begin([
            'action' => Url::current([
                'id' => null,
                'inn' => null,
                'name_short' => null,
                'hide_blocked' => null,
                'hide_test' => null,
                'company_type_id' => null,
            ]),
            'method' => 'get',
            'options' => [
                'data-pjax' => 1,
                'class' => 'pt-3',
            ],
            'fieldConfig' => Yii::$app->params['kubFieldConfig'],
            'enableClientValidation' => false,
        ]); ?>

            <div class="d-flex" style="margin: 0 -4px;">
                <div style="width: 30%; padding: 0 4px;"><?= $form->field($searchModel, 'user_email') ?></div>
                <div style="width: 15%; padding: 0 4px;"><?= $form->field($searchModel, 'id') ?></div>
                <div style="width: 20%; padding: 0 4px;"><?= $form->field($searchModel, 'inn') ?></div>
                <div style="width: 35%; padding: 0 4px;"><?= $form->field($searchModel, 'name_short') ?></div>
            </div>

            <div class="d-flex">
                <?= $form->field($searchModel, 'hide_test', ['options' => ['class' => '']])->checkbox(['class' => '']) ?>
                <?= Html::resetButton('Очистить', ['class' => 'button-regular button-hover-content-red button-width ml-auto']) ?>
                <?= Html::submitButton('Применить', ['class' => 'button-regular button-regular_red button-width ml-2']) ?>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

<?= GridView::widget([
    'id' => 'system_selling_select_grid',
    'dataProvider' => $dataProvider,
    'tableOptions' => [
        'class' => 'table table-bordered',
        'style' => 'table-layout: fixed;'
    ],
    'rowOptions' => function ($model, $key, $index, $grid) use ($employee) {
        return [
            'class' => 'cursor-pointer',
            'data-url' => Url::to([
                '/system/selling/create',
                'cid' => $model->id,
                'eid' => $employee ? $employee->id : null
            ]),
            'onclick' => "window.location.href = $(this).data('url');"
        ];
    },
    'layout' => $this->render('//layouts/grid/layout_no_perpage'),
    'columns' => [
        [
            'attribute' => 'id',
            'headerOptions' => [
                'width' => '10%',
            ],
        ],
        [
            'attribute' => 'inn',
        ],
        [
            'attribute' => 'name_short',
            'value' => 'shortTitle',
            'headerOptions' => [
                'width' => '50%',
            ],
            'contentOptions' => function ($model, $key, $index, $column) {
                return [
                    'class' => 'text-truncate',
                    //'title' => $model->getShortTitle(),
                ];
            },
        ],
        [
            'label' => 'Создана',
            'attribute' => 'created_at',
            'format' => ['date', 'php:d.m.Y'],
        ],
    ],
]); ?>

<?php Pjax::end(); ?>

<div class="d-flex">
    <button type="button" class="button-width button-regular button-hover-transparent ml-auto" data-dismiss="modal">Отменить</button>
</div>
