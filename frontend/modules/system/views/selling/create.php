<?php

use common\models\service\PaymentType;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\modules\subscribe\forms\PaymentForm;
use frontend\components\Icon;
use frontend\themes\kub\modules\subscribe\assets\SubscribeAsset;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use yii\bootstrap4\Modal;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/* @var $this \yii\web\View */
/* @var $company common\models\Company */
/* @var $companyArray array */
/* @var $model frontend\modules\subscribe\forms\PaymentForm */

$this->title = 'Счет на оплату сервиса для '.$company->getShortTitle();

SubscribeAsset::register($this);

$hasIp = false;
foreach ($companyArray as $c) {
    if ($c->getIsLikeIP()) {
        $hasIp = true;
        break;
    }
}

$formData = $model->getFormData($hasIp);

$tariffArray = ArrayHelper::getValue($formData, 'tariffArray', []);
$tariffPrices = ArrayHelper::getValue($formData, 'tariffPrices', []);
$discountByCount = ArrayHelper::getValue($formData, 'discountByCount', []);
$groupArray = ArrayHelper::getValue($formData, 'groupArray', []);
$tariffDurations = ArrayHelper::getValue($formData, 'tariffDurations', []);
$tariffPerQuantity = ArrayHelper::getValue($formData, 'tariffPerQuantity', []);
$limit = SubscribeTariff::find()->select('tariff_limit')->actual()->andWhere([
    'tariff_group_id' => SubscribeTariffGroup::STANDART,
])->orderBy(['price' => SORT_ASC])->scalar();

$expiresArray = [];
foreach ($groupArray as $group) {
    $subscribes = SubscribeHelper::getPayedSubscriptions($company->id, $group->id);
    if ($subscribes) {
        $expiresArray[$group->id] = [
            'name' => $group->name,
            'date' => $date = SubscribeHelper::getExpireDate($subscribes),
            'days' => Yii::t('yii', '{n, plural, =0{# дня} 1{# день} one{# день} few{# дня} other{# дней}}', [
                'n' => SubscribeHelper::getExpireLeftDays($date),
            ]),
            'is_trial' => $group->id == SubscribeTariffGroup::STANDART && end($subscribes)->tariff_id == SubscribeTariff::TARIFF_TRIAL,
        ];
    }
}
$groupsSelected = [SubscribeTariffGroup::STANDART];
$durationList = (array) array_unique(ArrayHelper::getColumn($tariffArray, 'duration_month'));
rsort($durationList);
$defaultDuration = max($durationList);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-pay',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'contentCloning' => true,
        'interactive' => true,
        'functionBefore' => new JsExpression('function(instance, helper) {
            var content = $($(helper.origin).data("tooltip-content"));
            instance.content(content);
        }'),
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-click',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'contentCloning' => false,
        'interactive' => true,
    ],
]);
?>

<?= $this->render('@frontend/modules/subscribe/views/default/_style') ?>

<div class="wrap">
    <h4><?= Html::encode($this->title) ?></h4>
</div>

<?php $form = ActiveForm::begin([
    'id' => 'subscribe-tariff-form',
    'enableAjaxValidation' => true,
    'options' => [
        'data' => [
            'discounts' => $discountByCount,
            'prices' => $tariffPrices,
            'durations' => $tariffDurations,
            'durationlist' => $durationList,
            'quantity' => $tariffPerQuantity,
            'mutually-exclusive' => PaymentForm::$mutuallyExclusive,
            'allow-submit' => 1,
        ],
    ],
]); ?>

    <div class="wrap">
        <?= $this->render('@frontend/modules/subscribe/views/default/_select_company', [
            'model' => $model,
            'form' => $form,
            'company' => $company,
            'companyArray' => $companyArray,
            'tariffArray' => $tariffArray,
        ]); ?>
    </div>

    <div class="row">
        <div class="col-9">
            <div id="subscribe-tariff-items" class="row">
                <?php foreach ($groupArray as $group) : ?>
                    <?= $this->render('@frontend/modules/subscribe/views/default/_select_tariff', [
                        'model' => $model,
                        'company' => $company,
                        'isOneCompany' => count($companyArray) == 1,
                        'group' => $group,
                        'discountByCount' => $discountByCount,
                        'expiresArray' => $expiresArray,
                        'limit' => $limit,
                        'groupsSelected' => $groupsSelected,
                        'durationList' => $durationList,
                        'defaultDuration' => $defaultDuration,
                    ]) ?>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-3">
            <div class="wrap p-3">
                <div class="mb-3" style="font-size: 18px; font-weight: bold;">
                    Оплатить за
                </div>
                <div class="">
                    <?= $form->field($model, 'durationMonth', [
                        'labelOptions' => [
                            'class' => 'label',
                        ],
                    ])->radioList(array_combine($durationList, $durationList), [
                        'class' => 'd-flex',
                        'item' => function ($i, $label, $name, $checked, $value) use ($defaultDuration) {
                            $options = [
                                'label' => $label,
                                'value' => $value,
                                'class' => 'durationmonth_radio',
                            ];
                            $wrapperOptions = [
                                'class' => 'durationmonth_item durationmonth_'.$value,
                                'data-value' => $value,
                            ];

                            $html = Html::beginTag('div', $wrapperOptions) . "\n";
                            $html .= Html::radio($name, $value == $defaultDuration, $options) . "\n";
                            $html .= Html::endTag('div') . "\n";

                            return $html;
                        },
                    ])->label('Срок (месяцев)'); ?>
                </div>

                <?= Html::tag('div', $this->render('@frontend/modules/subscribe/views/default/summary', ['model' => $model]), [
                    'id' => 'subscribe-tariff-form-summary',
                    'class' => 'mb-3',
                    'data-url' => Url::to(['summary', 'id' => $company->id]),
                ]) ?>

                <div class="bordered">
                    <div class="text-center tariff_pay_btn">
                        <?= Html::activeHiddenInput($model, 'paymentTypeId', ['value' => PaymentType::TYPE_INVOICE]) ?>
                        <?= Html::submitButton('Выставить счет', [
                            'class' => 'button-regular button-hover-transparent w-100',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $form->end(); ?>

<div class="tooltip-template hidden">
    <div id="pay-extra-tooltip">
        Можно оплатить больше компаний чем есть в аккаунте.
        <br>
        После оплаты пользователь сможет добавить новую компанию и ей сразу включится оплаченный тариф
        <br>
        <strong>Важно:</strong> в аккаунт можно добавить новую компанию, только если все имеющиеся компании уже оплачены.
    </div>
</div>
