<?php

namespace frontend\modules\system\models;

use common\models\Company;
use common\models\employee\Employee;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * class CompanySearch 
 */
class CompanySearch extends Company
{
    public $hide_blocked = 1;
    public $hide_test = 1;
    public $user_email;

    private $_employee = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'inn',
                    'name_short',
                    'user_email',
                ], 'trim',
            ],
            [
                [
                    'id',
                    'inn',
                    'name_short',
                    'user_email',
                    'hide_blocked',
                    'hide_test',
                    'company_type_id',
                ], 'safe',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getEmployee()
    {
        if (empty($this->user_email)) return null;

        if ($this->_employee === false) {
            $this->_employee = Employee::findIdentityByLogin($this->user_email);
        }

        return $this->_employee;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return \yii\base\Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'hide_test' => 'Скрыть тестовые',
            'user_email' => 'Email пользователя',
        ]);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = Company::find();

        if ($this->user_email) {
            if ($employee = $this->getEmployee()) {
                $query = $employee->getCompanies();
            } else {
                $query->where('0=1');
            }
        }

        if ($this->hide_blocked) {
            $query->andWhere([
                'blocked' => false,
            ]);
        }

        if ($this->hide_test) {
            $query->andWhere([
                'test' => false,
            ]);
        }

        $query
            ->andFilterWhere(['id' => $this->id])
            ->andFilterWhere(['company_type_id' => $this->company_type_id])
            ->andFilterWhere(['like', 'inn', $this->inn])
            ->andFilterWhere(['like', 'name_short', $this->name_short])
        ;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
                'attributes' => [
                    'id',
                    'inn',
                    'name_short',
                    'created_at',
                ],
            ],
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

        return $dataProvider;
    }
}
