<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 17.01.2017
 * Time: 8:38
 */

namespace frontend\modules\reports\components;

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankFlowToInvoice;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\User;
use frontend\models\Documents;
use frontend\modules\documents\models\InvoiceSearch;
use frontend\modules\reports\models\DebtReportSearch;
use frontend\rbac\permissions;
use Yii;
use yii\base\Exception;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class DebtsHelper
 * @package common\components\debts
 */
class DebtsHelper
{
    /**
     *
     */
    const PERIOD_0_10 = 1;
    /**
     *
     */
    const PERIOD_11_30 = 2;
    /**
     *
     */
    const PERIOD_31_60 = 3;
    /**
     *
     */
    const PERIOD_61_90 = 4;
    /**
     *
     */
    const PERIOD_MORE_90 = 5;
    /**
     *
     */
    const PERIOD_0_MORE_90 = 6;
    /**
     *
     */
    const PERIOD_ACTUAL = 7;

    /**
     *
     */
    const PERIOD_OVERDUE = 8;

    /**
     *
     */
    const DEBT_JANUARY = '01';

    /**
     *
     */
    const DEBT_FEBRUARY = '02';

    /**
     *
     */
    const DEBT_MARCH = '03';

    /**
     *
     */
    const DEBT_APRIL = '04';

    /**
     *
     */
    const DEBT_MAY = '05';

    /**
     *
     */
    const DEBT_JUNE = '06';

    /**
     *
     */
    const DEBT_JULY = '07';

    /**
     *
     */
    const DEBT_AUGUST = '08';

    /**
     *
     */
    const DEBT_SEPTEMBER = '09';

    /**
     *
     */
    const DEBT_OCTOBER = '10';

    /**
     *
     */
    const DEBT_NOVEMBER = '11';

    /**
     *
     */
    const DEBT_DECEMBER = '12';

    /**
     * @var array
     */
    public static $month = [
        self::DEBT_JANUARY => 'debt_january',
        self::DEBT_FEBRUARY => 'debt_february',
        self::DEBT_MARCH => 'debt_march',
        self::DEBT_APRIL => 'debt_april',
        self::DEBT_MAY => 'debt_may',
        self::DEBT_JUNE => 'debt_june',
        self::DEBT_JULY => 'debt_jule',
        self::DEBT_AUGUST => 'debt_august',
        self::DEBT_SEPTEMBER => 'debt_september',
        self::DEBT_OCTOBER => 'debt_october',
        self::DEBT_NOVEMBER => 'debt_november',
        self::DEBT_DECEMBER => 'debt_december',
    ];

    /**
     * @var array
     */
    public static $debtPeriods = [
        self::PERIOD_0_10 => 'debt_0_10',
        self::PERIOD_11_30 => 'debt_11_30',
        self::PERIOD_31_60 => 'debt_31_60',
        self::PERIOD_61_90 => 'debt_61_90',
        self::PERIOD_MORE_90 => 'debt_more_90',
    ];

    /**
     * @var array
     */
    private static $_data = [];

    /**
     * @param null $type
     * @param bool $allSum
     * @param bool $format
     * @param null $contractorID
     * @param int $documentType
     * @return int|string
     * @throws Exception
     */
    public static function getDebtsSum($type = null, $allSum = false, $format = true, $contractorID = null, $documentType = Documents::IO_TYPE_OUT, $employeeID = null, $expenditureItemID = null, $projectId = null)
    {
        $key = json_encode([$type, $allSum, $contractorID, $documentType, $employeeID, $expenditureItemID]);

        if (!isset(self::$_data['getDebtsSum'][$key])) {
            $query = self::getDebts($type, $allSum, false, $contractorID, $documentType, $employeeID, $expenditureItemID, $projectId);
            self::$_data['getDebtsSum'][$key] = (int) (new Query)->from(['t' => $query])->sum('sum', \Yii::$app->db2);
        }

        return $format ? TextHelper::invoiceMoneyFormat(self::$_data['getDebtsSum'][$key], 2) : self::$_data['getDebtsSum'][$key];
    }

    /**
     * @param Company $company
     * @param string $currencyName
     * @param int $periodId
     * @param int $ioType
     * @param bool $allSum
     * @param bool $payed
     * @param int $contractorID
     * @param int $documentType
     * @param int $employeeID
     * @return int|string
     * @throws Exception
     */
    public static function getForeignCurrencyDebtsSum(
        Company $company,
        $currencyName,
        $periodId,
        $ioType = Documents::IO_TYPE_OUT,
        $allSum = false,
        $payed = false,
        $contractorID = null,
        $employeeID = null,
        $expenditureItemID = null
    ) {
        $key = json_encode([
            $company->id,
            $currencyName,
            $periodId,
            $ioType,
            $allSum,
            $payed,
            $contractorID,
            $employeeID,
            $expenditureItemID,
        ]);

        if (!isset(self::$_data['getDebtsSum'][$key])) {
            $query = self::getForeignCurrencyDebts(
                $company,
                $currencyName,
                $periodId,
                $ioType,
                $allSum,
                $payed,
                $contractorID,
                $employeeID,
                $expenditureItemID
            );
            self::$_data['getDebtsSum'][$key] = (int) (new Query)->from(['t' => $query])->sum('remaining_amount', \Yii::$app->db2);
        }

        return self::$_data['getDebtsSum'][$key];
    }

    /**
     * @param null $type
     * @param bool $allSum
     * @param null $contractorID
     * @param int $documentType
     * @return int|string
     * @throws Exception
     */
    public static function getDebtsSumCount($type = null, $allSum = false, $contractorID = null, $documentType = Documents::IO_TYPE_OUT)
    {
        $query = self::getDebts($type, $allSum, false, $contractorID, $documentType);

        return $query->andHaving(['>', 'sum', 0])->groupBy('contractor_id')->count('*', \Yii::$app->db2);
    }

    /**
     * @param null $type
     * @param bool $allSum
     * @param null $contractorID
     * @param int $documentType
     * @return int|string
     * @throws Exception
     */
    public static function getDebtsSumInvoiceCount($type = null, $allSum = false, $contractorID = null, $documentType = Documents::IO_TYPE_OUT)
    {
        $query = self::getDebts($type, $allSum, false, $contractorID, $documentType);

        return $query->andHaving(['>', 'sum', 0])->count('*', \Yii::$app->db2);
    }

    /**
     * @param int $documentType
     * @return mixed
     * @throws Exception
     */
    public static function getPayedSum($documentType = Documents::IO_TYPE_OUT)
    {
        return self::getDebts(self::PERIOD_MORE_90, true, true, null, $documentType)->sum('total_amount_with_nds', \Yii::$app->db2);
    }

    /**
     * @param $type
     * @param null $contractorID
     * @param bool $withCurrentDebt
     * @param int $documentType
     * @return float
     * @throws Exception
     */
    public static function getDebtsPercentage($type, $contractorID = null, $withCurrentDebt = false, $documentType = Documents::IO_TYPE_OUT)
    {
        $sum = self::getDebtsSum($type, false, false, $contractorID, $documentType);
        if ($withCurrentDebt) {
            $allSum = self::getDebtsSum($type, true, false, $contractorID, $documentType) + self::getCurrentDebt($contractorID, $documentType);
        } else {
            $allSum = self::getDebtsSum($type, true, false, $contractorID, $documentType);
        }
        $allSum = $allSum == 0 ? 1 : $allSum;

        return round(($sum / $allSum) * 100, 2);
    }

    /**
     * @param $count
     * @param int $documentType
     * @return array
     */
    public static function getTop($count, $documentType = Documents::IO_TYPE_OUT)
    {
        $query = \Yii::$app->user->identity->company->getInvoices()->select([
            'sum' => 'SUM({{invoice}}.[[remaining_amount]])',
            'invoice.contractor_id',
        ])->joinWith('contractor', false)->andWhere([
            'contractor.is_deleted' => false,
            'invoice.is_deleted' => false,
            'invoice.type' => $documentType,
            'invoice.invoice_status_id' => InvoiceStatus::$payAllowed,
        ])->andWhere([
            '<', 'payment_limit_date', date(DateHelper::FORMAT_DATE),
        ])->groupBy('contractor_id')->orderBy([
            'sum' => SORT_DESC,
        ])->limit($count)->asArray()->indexBy('contractor_id');

        \frontend\modules\documents\models\InvoiceSearch::searchByRoleFilter($query);

        return $query->column();
    }

    /**
     * @param $count
     * @param int $documentType
     * @return array
     */
    public static function getTopByEmployee($count, $documentType = Documents::IO_TYPE_OUT)
    {
        $query = \Yii::$app->user->identity->company->getInvoices()->select([
            'sum' => 'SUM({{invoice}}.[[remaining_amount]])',
            'invoice.document_author_id',
        ])->joinWith('contractor', false)->andWhere([
            'contractor.is_deleted' => false,
            'invoice.is_deleted' => false,
            'invoice.type' => $documentType,
            'invoice.invoice_status_id' => InvoiceStatus::$payAllowed,
        ])->andWhere([
            '<', 'payment_limit_date', date(DateHelper::FORMAT_DATE),
        ])->groupBy('document_author_id')->orderBy([
            'sum' => SORT_DESC,
        ])->limit($count)->asArray()->indexBy('document_author_id');

        \frontend\modules\documents\models\InvoiceSearch::searchByRoleFilter($query);

        return $query->column();
    }

    /**
     * @param $count
     * @param int $documentType
     * @return array
     */
    public static function getTopByExpenditureItem($count)
    {
        $query = \Yii::$app->user->identity->company->getInvoices()->select([
            'sum' => 'SUM({{invoice}}.[[remaining_amount]])',
            'invoice.invoice_expenditure_item_id',
        ])->joinWith('contractor', false)->andWhere([
            'contractor.is_deleted' => false,
            'invoice.is_deleted' => false,
            'invoice.type' => Documents::IO_TYPE_IN,
            'invoice.invoice_status_id' => InvoiceStatus::$payAllowed,
        ])->andWhere([
            '<', 'payment_limit_date', date(DateHelper::FORMAT_DATE),
        ])->groupBy('invoice_expenditure_item_id')->orderBy([
            'sum' => SORT_DESC,
        ])->limit($count)->asArray()->indexBy('invoice_expenditure_item_id');

        \frontend\modules\documents\models\InvoiceSearch::searchByRoleFilter($query);

        return $query->column();
    }

    /**
     * @param Company $company
     * @param string $currencyName
     * @param int $periodId
     * @param int $ioType
     * @param bool $allSum
     * @param bool $payed
     * @param int $contractorID
     * @param int $documentType
     * @param int $employeeID
     * @return \common\models\document\query\InvoiceQuery
     * @throws Exception
     */
    public static function getForeignCurrencyDebts(
        Company $company,
        $currencyName,
        $periodId,
        $ioType = Documents::IO_TYPE_OUT,
        $allSum = false,
        $payed = false,
        $contractorID = null,
        $employeeID = null,
        $expenditureItemID = null
    ) {
        $period = self::getPeriodByDebtType($periodId);
        $query = \common\models\document\ForeignCurrencyInvoice::find()->alias('invoice')
            ->andWhere([
                'invoice.company_id' => $company->id,
                'invoice.type' => $ioType,
                'invoice.is_deleted' => false,
                'invoice.currency_name' => $currencyName,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
            ->andFilterWhere([
                'invoice.contractor_id' => $contractorID,
                'invoice.document_author_id' => $employeeID,
                'invoice.invoice_expenditure_item_id' => $expenditureItemID,
            ]);
        if (!$payed) {
            $query->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_PAYED]]);
            if ($period['actual']) {
                $query->andWhere(['>=', 'invoice.payment_limit_date', date('Y-m-d')]);
            } elseif ($period['overdue']) {
                $query->andWhere(['<', 'invoice.payment_limit_date', date('Y-m-d')]);
            } else {
                $query->andWhere(['<', 'invoice.payment_limit_date', date('Y-m-d')]);
            }
        } else {
            $query->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_OVERDUE]]);
        }
        if (!$allSum) {
            if ($period['min']) {
                $query->andWhere(['<=', 'invoice.payment_limit_date', (new \DateTime("- {$period['min']} days"))->format('Y-m-d')]);
            }
            if ($period['max']) {
                $query->andWhere(['>=', 'invoice.payment_limit_date', (new \DateTime("- {$period['max']} days"))->format('Y-m-d')]);
            }
        }

        \frontend\modules\documents\models\InvoiceSearch::searchByRoleFilter($query);

        return $query;
    }

    /**
     * @param null $type
     * @param bool $allSum
     * @param bool $payed
     * @param null $contractorID
     * @param int $documentType
     * @param int $employeeID
     * @return \common\models\document\query\InvoiceQuery
     * @throws Exception
     */
    public static function getDebts($type = null, $allSum = false, $payed = false, $contractorID = null, $documentType = Documents::IO_TYPE_OUT, $employeeID = null, $expenditureItemID = null, $projectId = null)
    {
        $contractor = $contractorID ? Contractor::findOne($contractorID) : null;
        if ($type) {
            $period = self::getPeriodByDebtType($type);
        }
        $query = Invoice::find()
            ->select([
                'invoice.*',
                'sum' => '(IFNULL([[total_amount_with_nds]], 0) - IFNULL([[payment_partial_amount]], 0))'
            ])
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => \Yii::$app->user->identity->company->id,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_REJECTED]]);
        if ($contractor) {
            $query->andWhere(['invoice.type' => $contractor->type == Contractor::TYPE_CUSTOMER ? Documents::IO_TYPE_OUT : Documents::IO_TYPE_IN]);
        } else {
            $query->andWhere(['invoice.type' => $documentType]);
        }
        if (!$payed) {
            $query->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_PAYED]]);
            if ($period['actual']) {
                $query->andWhere(['>=', 'invoice.payment_limit_date', date('Y-m-d')]);
            }
            elseif ($period['overdue']) {
                $query->andWhere(['<', 'invoice.payment_limit_date', date('Y-m-d')]);
            } else {
                $query->andWhere(['<', 'invoice.payment_limit_date', date('Y-m-d')]);
            }
        } else {
            $query->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_OVERDUE]]);
        }
        if ($contractorID) {
            $query->andWhere(['invoice.contractor_id' => $contractorID]);
        }
        if ($employeeID) {
            $query->andWhere(['invoice.document_author_id' => $employeeID]);
        }
        if ($expenditureItemID) {
            $query->andWhere(['invoice.invoice_expenditure_item_id' => $expenditureItemID]);
        }
        if (!$allSum) {
            if ($period['min']) {
                    $query->andWhere(['<=', 'invoice.payment_limit_date', (new \DateTime("- {$period['min']} days"))->format('Y-m-d')]);
            }
            if ($period['max']) {
                $query->andWhere(['>=', 'invoice.payment_limit_date', (new \DateTime("- {$period['max']} days"))->format('Y-m-d')]);
            }
        }

        $query->leftJoin('contractor', '{{invoice}}.[[contractor_id]] = {{contractor}}.[[id]]');

        InvoiceSearch::searchByRoleFilter($query);

        return $query;
    }

    /**
     * @param int $type
     * @param bool|true $format
     * @param null $contractorID
     * @param null $month
     * @param $year
     * @return mixed|string
     */
    public static function getDebtMonth($type = DebtReportSearch::BILLED_INVOICES, $format = true, $contractorID = null, $month = null, $year)
    {
        $query = Invoice::find()
            ->byCompany(\Yii::$app->user->identity->company->id)
            ->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
            ->byDeleted();
        if ($contractorID) {
            $query->byContractorId($contractorID);
        } else {
            $query->byIOType(Documents::IO_TYPE_OUT);
        }
        if ($type == DebtReportSearch::PAYED_INVOICES) {
            if ($month && $year) {
                $query->andWhere(['between', 'date(from_unixtime(`' . Invoice::tableName() . '`.`invoice_status_updated_at`))', $year . '-' . $month . '-01', $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year)]);
            } elseif ($year) {
                $query->andWhere(['between', 'date(from_unixtime(`' . Invoice::tableName() . '`.`invoice_status_updated_at`))', $year . '-01-01', $year . '-12-31']);
            }
        } else {
            if ($month && $year) {
                $query->andWhere(['between', Invoice::tableName() . '.document_date', $year . '-' . $month . '-01', $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year)]);
            } elseif ($year) {
                $query->andWhere(['between', Invoice::tableName() . '.document_date', $year . '-01-01', $year . '-12-31']);
            }
        }
        if ($type == DebtReportSearch::PAYED_INVOICES) {
            $query
                ->select(['IFNULL(SUM(`payment_partial_amount`), 0) as sum'])
                ->andWhere(['or',
                    ['in', Invoice::tableName() . '.invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]],
                    ['and',
                        [Invoice::tableName() . '.invoice_status_id' => InvoiceStatus::STATUS_OVERDUE],
                        ['not', ['remaining_amount' => null]],
                    ],
                ]);
        } elseif ($type == DebtReportSearch::DEBT_INVOICES) {
            $query
                ->select(['IFNULL(SUM(`remaining_amount`), 0) as sum'])
                ->byStatus(InvoiceStatus::$payAllowed);
        } else {
            $query->select(['IFNULL(SUM(`total_amount_with_nds`), 0) as sum']);
        }

        $query->leftJoin('contractor', 'invoice.contractor_id = contractor.id');
        \frontend\modules\documents\models\InvoiceSearch::searchByRoleFilter($query);

        return $format ? TextHelper::invoiceMoneyFormat(current($query->column(\Yii::$app->db2)), 2) : current($query->column(\Yii::$app->db2));
    }

    /**
     * @param null $contractorID
     * @param int $documentType
     * @return mixed
     */
    public static function getCurrentDebt($contractorID = null, $documentType = Documents::IO_TYPE_OUT)
    {
        $sum = self::getBaseQueryCurrentDebt($contractorID, $documentType);

        return current($sum->column(\Yii::$app->db2));
    }

    /**
     * @param null $contractorID
     * @param int $documentType
     * @return int|string
     */
    public static function getCurrentDebtCount($contractorID = null, $documentType = Documents::IO_TYPE_OUT)
    {
        $query = self::getBaseQueryCurrentDebt($contractorID, $documentType);

        return $query->andHaving(['>', 'sum', 0])->groupBy('contractor_id')->count('*', \Yii::$app->db2);
    }

    /**
     * @param null $contractorID
     * @param int $documentType
     * @return int|string
     */
    public static function getCurrentDebtInvoiceCount($contractorID = null, $documentType = Documents::IO_TYPE_OUT)
    {
        $query = self::getBaseQueryCurrentDebt($contractorID, $documentType);

        return $query->andHaving(['>', 'sum', 0])->count('*', \Yii::$app->db2);
    }

    /**
     * @param null $contractorID
     * @param int $documentType
     * @param int $employeeID
     * @return \common\models\document\query\InvoiceQuery
     */
    public static function getBaseQueryCurrentDebt($contractorID = null, $documentType = Documents::IO_TYPE_OUT)
    {
        $query = Invoice::find()
            ->select([
                'sum' => 'IFNULL(SUM({{invoice}}.[[total_amount_with_nds]]), 0) - IFNULL(SUM({{invoice}}.[[payment_partial_amount]]), 0)',
            ])
            ->byCompany(Yii::$app->user->identity->company->id)
            ->byIOType($documentType)
            ->byDeleted()
            ->andWhere(['not', ['invoice.invoice_status_id' => [
                InvoiceStatus::STATUS_PAYED,
                InvoiceStatus::STATUS_REJECTED,
            ]]])
            ->andWhere(['>=', 'invoice.payment_limit_date', date('Y-m-d')]);
        if ($contractorID) {
            $query->byContractorId($contractorID);
        }

        $query->leftJoin('contractor', '{{invoice}}.[[contractor_id]] = {{contractor}}.[[id]]');

        InvoiceSearch::searchByRoleFilter($query);

        return $query;
    }

    /**
     * @param $type
     * @return array
     * @throws Exception
     */
    public static function getPeriodByDebtType($type)
    {
        switch ($type) {
            case self::PERIOD_0_10:
                $min = 1;
                $max = 10;
                break;
            case self::PERIOD_11_30:
                $min = 11;
                $max = 30;
                break;
            case self::PERIOD_31_60:
                $min = 31;
                $max = 60;
                break;
            case self::PERIOD_61_90:
                $min = 61;
                $max = 90;
                break;
            case self::PERIOD_MORE_90:
                $min = 91;
                break;
            case self::PERIOD_0_MORE_90:
                $min = 1;
                break;
            case self::PERIOD_ACTUAL:
                $actual = true;
                break;
            case self::PERIOD_OVERDUE:
                $overdue = true;
                break;
            default:
                throw new Exception('Type: ' . $type . ' not found.');
        }

        return [
            'min' => isset($min) ? $min : null,
            'max' => isset($max) ? $max : null,
            'actual' => isset($actual) ? $actual : null,
            'overdue' => isset($overdue) ? $overdue : null
        ];
    }

    /**
     * @param $month
     * @return array
     */
    public static function getNewClientsMonthInvoiceSum($month)
    {
        $year = isset(\Yii::$app->request->get('DebtReportSearch')['year']) ? \Yii::$app->request->get('DebtReportSearch')['year'] : date('Y');
        $newQuery = Invoice::find()
            ->select(['IFNULL(SUM(total_amount_with_nds), 0) - IFNULL(SUM(remaining_amount), 0) as sum'])
            ->byIOType(Documents::IO_TYPE_OUT)
            ->andWhere(['in', 'contractor_id', Invoice::find()
                ->select(['contractor_id'])
                ->where(['in', 'invoice.id', Invoice::find()
                    ->byIOType(Documents::IO_TYPE_OUT)
                    ->andWhere(['or',
                        ['in', 'invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]],
                        ['and',
                            ['invoice_status_id' => InvoiceStatus::STATUS_OVERDUE],
                            ['not', ['remaining_amount' => null]],
                        ],
                    ])
                    ->byDeleted()
                    ->andWhere(['company_id' => \Yii::$app->user->identity->company->id])
                    ->groupBy('contractor_id')
                    ->orderBy(['invoice_status_updated_at' => SORT_DESC])
                    ->column(\Yii::$app->db2)])
                ->andWhere(['between', 'date(from_unixtime(`' . Invoice::tableName() . '`.`invoice_status_updated_at`))', $year . '-' . $month . '-01', $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year)])
                ->column(\Yii::$app->db2)])
            ->andWhere(['or',
                ['in', 'invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]],
                ['and',
                    ['invoice_status_id' => InvoiceStatus::STATUS_OVERDUE],
                    ['not', ['remaining_amount' => null]],
                ],
            ])
            ->byDeleted()
            ->andWhere(['between', 'date(from_unixtime(`' . Invoice::tableName() . '`.`invoice_status_updated_at`))', $year . '-' . $month . '-01', $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year)]);

        $newQuery->leftJoin('contractor', 'invoice.contractor_id = contractor.id');
        InvoiceSearch::searchByRoleFilter($newQuery);

        $payedByNewClients = current($newQuery->column(\Yii::$app->db2));


        $oldQuery = Invoice::find()
                ->select(['IFNULL(SUM(total_amount_with_nds), 0) - IFNULL(SUM(remaining_amount), 0) as sum'])
                ->byIOType(Documents::IO_TYPE_OUT)
                ->andWhere(['or',
                    ['in', 'invoice.invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]],
                    ['and',
                        ['invoice.invoice_status_id' => InvoiceStatus::STATUS_OVERDUE],
                        ['not', ['invoice.remaining_amount' => null]],
                    ],
                ])
                ->andWhere(['invoice.company_id' => \Yii::$app->user->identity->company->id])
                ->byDeleted()
                ->andWhere([
                    'between',
                    'date(from_unixtime({{invoice}}.[[invoice_status_updated_at]]))',
                    "{$year}-{$month}-01",
                    "{$year}-{$month}-" . cal_days_in_month(CAL_GREGORIAN, $month, $year),
                ]);

        $oldQuery->leftJoin('contractor', '{{invoice}}.[[contractor_id]] = {{contractor}}.[[id]]');
        \frontend\modules\documents\models\InvoiceSearch::searchByRoleFilter($oldQuery);

        $payedByOldClients = current($oldQuery->column(\Yii::$app->db2)) - $payedByNewClients;

        return [
            'payedByNewClients' => $payedByNewClients,
            'payedByOldClients' => $payedByOldClients,
        ];
    }

    /**
     * @param $month
     * @return array
     */
    public static function getNewClientsMonth($month)
    {
        $year = isset(\Yii::$app->request->get('DebtReportSearch')['year']) ? \Yii::$app->request->get('DebtReportSearch')['year'] : date('Y');
        $newQuery = Invoice::find()
            ->where(['in', 'invoice.id', Invoice::find()
                ->byIOType(Documents::IO_TYPE_OUT)
                ->andWhere(['or',
                    ['in', 'invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]],
                    ['and',
                        ['invoice_status_id' => InvoiceStatus::STATUS_OVERDUE],
                        ['not', ['remaining_amount' => null]],
                    ],
                ])
                ->andWhere(['invoice.company_id' => \Yii::$app->user->identity->company->id])
                ->byDeleted()
                ->groupBy('invoice.contractor_id')
                ->orderBy(['invoice.invoice_status_updated_at' => SORT_DESC])
                ->column(\Yii::$app->db2)])
            ->andWhere([
                'between',
                'date(from_unixtime({{invoice}}.[[invoice_status_updated_at]]))',
                "{$year}-{$month}-01",
                "{$year}-{$month}-" . cal_days_in_month(CAL_GREGORIAN, $month, $year),
            ]);

        $newQuery->leftJoin('contractor', 'invoice.contractor_id = contractor.id');
        InvoiceSearch::searchByRoleFilter($newQuery);

        $newClients = (int) $newQuery->count('*', \Yii::$app->db2);

        $oldQuery = Invoice::find()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->andWhere(['or',
                ['in', 'invoice.invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]],
                ['and',
                    ['invoice.invoice_status_id' => InvoiceStatus::STATUS_OVERDUE],
                    ['not', ['invoice.remaining_amount' => null]],
                ],
            ])
            ->byDeleted()
            ->andWhere(['invoice.company_id' => \Yii::$app->user->identity->company->id])
            ->andWhere([
                'between',
                'date(from_unixtime({{invoice}}.[[invoice_status_updated_at]]))',
                "{$year}-{$month}-01",
                "{$year}-{$month}-" . cal_days_in_month(CAL_GREGORIAN, $month, $year),
            ])
            ->groupBy('invoice.contractor_id');

        $oldQuery->leftJoin('contractor', 'invoice.contractor_id = contractor.id');
        InvoiceSearch::searchByRoleFilter($oldQuery);

        $oldClients = $oldQuery->count('*', \Yii::$app->db2) - $newClients;

        return [
            'newClients' => $newClients,
            'oldClients' => $oldClients,
        ];
    }

    /**
     * @param $dateRange
     * @return array
     */
    public static function getNewClients($dateRange)
    {
        $result = [];
        $allDebtOnStartPeriod = 0;
        $allNewInvoicesSum = 0;
        $allDebtOnEndPeriod = 0;

        $firstPayedInvoices = Invoice::find()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->andWhere(['company_id' => \Yii::$app->user->identity->company->id])
            ->byStatus([InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL])
            ->byDeleted()
            ->groupBy('contractor_id')
            ->orderBy(['invoice_status_updated_at' => SORT_DESC])
            ->column(\Yii::$app->db2);

        $query = Invoice::find()->joinWith('contractor', false)
            ->select(['invoice.contractor_id', 'invoice.document_author_id'])
            ->where(['in', 'invoice.id', $firstPayedInvoices])
            ->andWhere(['between', 'date(from_unixtime({{invoice}}.[[invoice_status_updated_at]]))', $dateRange['from'], $dateRange['to']]);
        // STRICT BY USER
        \frontend\modules\documents\models\InvoiceSearch::searchByRoleFilter($query);
        $newClients = $query->asArray()->all(\Yii::$app->db2);
        foreach ($newClients as $newClient) {
            $debtOnStartPeriodQuery = Invoice::find()->joinWith('contractor', false)
                ->select(['IFNULL(SUM(total_amount_with_nds), 0) - IFNULL(SUM(payment_partial_amount), 0) as sum'])
                ->byIOType(Documents::IO_TYPE_OUT)
                ->andWhere(['invoice.company_id' => \Yii::$app->user->identity->company->id])
                ->byContractorId($newClient['contractor_id'])
                ->andWhere(['invoice.document_author_id' => $newClient['document_author_id']])
                ->byStatus(InvoiceStatus::STATUS_OVERDUE)
                ->andWhere(['<', 'date(from_unixtime(`' . Invoice::tableName() . '`.`invoice_status_updated_at`))', $dateRange['from']])
                ->byDeleted();
            \frontend\modules\documents\models\InvoiceSearch::searchByRoleFilter($debtOnStartPeriodQuery);
            $debtOnStartPeriod = current($debtOnStartPeriodQuery->column(\Yii::$app->db2));

            $newInvoicesSumQuery = Invoice::find()->joinWith('contractor', false)
                ->byIOType(Documents::IO_TYPE_OUT)
                ->andWhere(['invoice.company_id' => \Yii::$app->user->identity->company->id])
                ->byContractorId($newClient['contractor_id'])
                ->andWhere(['document_author_id' => $newClient['document_author_id']])
                ->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
                ->andWhere(['between', '`' . Invoice::tableName() . '`.`document_date`', $dateRange['from'], $dateRange['to']])
                ->byDeleted();
            \frontend\modules\documents\models\InvoiceSearch::searchByRoleFilter($newInvoicesSumQuery);
            $newInvoicesSum = $newInvoicesSumQuery->sum('total_amount_with_nds', \Yii::$app->db2);

            $debtOnEndPeriodQuery = Invoice::find()->joinWith('contractor', false)
                ->select(['IFNULL(SUM(total_amount_with_nds), 0) - IFNULL(SUM(remaining_amount), 0) as sum'])
                ->byIOType(Documents::IO_TYPE_OUT)
                ->andWhere(['invoice.company_id' => \Yii::$app->user->identity->company->id])
                ->byContractorId($newClient['contractor_id'])
                ->andWhere(['document_author_id' => $newClient['document_author_id']])
                ->andWhere(['or',
                    ['in', Invoice::tableName() . '.invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]],
                    ['and',
                        [Invoice::tableName() . '.invoice_status_id' => InvoiceStatus::STATUS_OVERDUE],
                        ['not', [Invoice::tableName() . '.remaining_amount' => null]],
                    ],
                ])
                ->andWhere(['between', 'date(from_unixtime(`' . Invoice::tableName() . '`.`invoice_status_updated_at`))', $dateRange['from'], $dateRange['to']])
                ->byDeleted();
            \frontend\modules\documents\models\InvoiceSearch::searchByRoleFilter($debtOnEndPeriodQuery);
            $debtOnEndPeriod = current($debtOnEndPeriodQuery->column(\Yii::$app->db2));

            $allDebtOnStartPeriod += $debtOnStartPeriod;
            $allNewInvoicesSum += $newInvoicesSum;
            $allDebtOnEndPeriod += $debtOnEndPeriod;

            if (isset($result[$newClient['document_author_id']])) {
                $result[$newClient['document_author_id']]['debtOnStartPeriod'] += $debtOnStartPeriod;
                $result[$newClient['document_author_id']]['newInvoicesSum'] += $newInvoicesSum;
                $result[$newClient['document_author_id']]['debtOnEndPeriod'] += $debtOnEndPeriod;
            } else {
                $result[$newClient['document_author_id']] = [
                    'author' => Employee::findOne($newClient['document_author_id'])->getFio(true),
                    'debtOnStartPeriod' => $debtOnStartPeriod,
                    'newInvoicesSum' => $newInvoicesSum,
                    'debtOnEndPeriod' => $debtOnEndPeriod,
                ];
            }
        }

        $result['all'] = [
            'allDebtOnStartPeriod' => $allDebtOnStartPeriod,
            'allNewInvoicesSum' => $allNewInvoicesSum,
            'allDebtOnEndPeriod' => $allDebtOnEndPeriod,
        ];

        return $result;
    }

    /**
     * @param $debtArray
     * @param $attributeName
     * @param $allAttributeName
     * @param $result
     * @return mixed
     */
    public static function setNewClientsDebt($debtArray, $attributeName, $allAttributeName, $result)
    {
        $allDebt = 0;
        foreach ($debtArray as $oneDebtArray) {
            $allDebt += $oneDebtArray['total_amount_with_nds'];
            if (isset($result[$oneDebtArray['document_author_id']])) {
                if (isset($result[$oneDebtArray['document_author_id']][$attributeName])) {
                    $result[$oneDebtArray['document_author_id']][$attributeName] += $oneDebtArray['total_amount_with_nds'];
                } else {
                    $result[$oneDebtArray['document_author_id']] = [
                        $attributeName => $oneDebtArray['total_amount_with_nds'],
                    ];
                }
            } else {
                $result[$oneDebtArray['document_author_id']] = [
                    'author' => Employee::findOne($oneDebtArray['document_author_id'])->getFio(true),
                    $attributeName => $oneDebtArray['total_amount_with_nds'],
                ];
            }
        }
        $result['all'] = [
            $allAttributeName => $allDebt,
        ];

        return $result;
    }

    public static function getOverdueDebtsByPeriod($period = null, $documentType = Documents::IO_TYPE_OUT)
    {
        $period = [
            'min' => ArrayHelper::getValue($period, 'min', null),
            'max' => ArrayHelper::getValue($period, 'max', null)
        ];

        $minDate = (new \DateTime("- {$period['min']} days"))->format('Y-m-d');
        //$maxDate = new \DateTime("- {$period['max']} days");

        $sumWithoutPayments = "(IFNULL(invoice.total_amount_with_nds, 0) - IFNULL(invoice.payment_partial_amount, 0))";
        $sumWithPayments = "(IFNULL(invoice.total_amount_with_nds, 0))";

        $query = Invoice::find()
            ->joinWith('contractor', false)
            ->joinWith('cashBankFlows', false)
            ->joinWith('cashOrderFlows', false)
            ->joinWith('cashEmoneyFlows', false)
            ->select([
                'invoice.*',
                'sum' => "
                  (CASE WHEN cash_bank_flows.date <= '{$minDate}' THEN {$sumWithoutPayments} ELSE
                    (CASE WHEN cash_order_flows.date <= '{$minDate}' THEN {$sumWithoutPayments} ELSE
                     (CASE WHEN cash_emoney_flows.date <= '{$minDate}' THEN {$sumWithoutPayments} ELSE {$sumWithPayments} END) END) END)"
            ])
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => \Yii::$app->user->identity->company->id,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
            ->andWhere(['invoice.type' => $documentType]);

        if (null !== $period['min']) {
            $query->andWhere(['<', 'invoice.payment_limit_date', $minDate]);
        }
        //if (null !== $period['max']) {
        //    $query->andWhere(['>', 'invoice.payment_limit_date', $maxDate->format('Y-m-d')]);
        //}

        // STRICT BY USER
        \frontend\modules\documents\models\InvoiceSearch::searchByRoleFilter($query);

        return $query;
    }
}
