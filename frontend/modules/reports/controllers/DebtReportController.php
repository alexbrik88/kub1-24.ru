<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.01.2017
 * Time: 4:12
 */

namespace frontend\modules\reports\controllers;


use common\components\filters\AccessControl;
use common\models\employee\EmployeeRole;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\components\PageSize;
use frontend\models\Documents;
use frontend\modules\reports\components\DebtsHelper;
use frontend\modules\reports\models\DebtReportSearch;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class DebtReportController
 * @package frontend\modules\reports\controllers
 */
class DebtReportController extends FrontendController
{
    /**
     * @var string
     */
    public $debtLayout = 'debt-report';

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            permissions\Reports::REPORTS_CLIENTS,
                        ],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     */
    public function actionDebtor()
    {
        // STRICT BY USER
        $employeeID = (Yii::$app->user->identity->currentRoleId != EmployeeRole::ROLE_CHIEF) ? Yii::$app->user->id : null;

        $this->layout = $this->debtLayout;

        $searchModel = new DebtReportSearch();
        $dataProvider = $searchModel->findDebtor(Yii::$app->request->get());
        $dataProvider->pagination->pageSize = PageSize::get();

        $currentDebt = DebtsHelper::getCurrentDebt(null, 2, $employeeID);
        $debtSum10 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_0_10, false, false, null, 2, $employeeID);
        $debtSum30 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_11_30, false, false, null, 2, $employeeID);
        $debtSum60 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_31_60, false, false, null, 2, $employeeID);
        $debtSum90 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_61_90, false, false, null, 2, $employeeID);
        $debtSumMore90 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, false, false, null, 2, $employeeID);
        $allDebtSum = $currentDebt + $debtSum10 + $debtSum30 + $debtSum60 + $debtSum90 + $debtSumMore90;
        $formatSum = $allDebtSum ? $allDebtSum : 1;

        \common\models\company\CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 55);

        return $this->render('debtor', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'currentDebt' => $currentDebt,
            'debtSum10' => $debtSum10,
            'debtSum30' => $debtSum30,
            'debtSum60' => $debtSum60,
            'debtSum90' => $debtSum90,
            'debtSumMore90' => $debtSumMore90,
            'allDebtSum' => $allDebtSum,
            'formatSum' => $formatSum,
        ]);
    }

    /**
     * @return string
     */
    public function actionDebtorMonth()
    {
        $this->layout = $this->debtLayout;

        $searchModel = new DebtReportSearch();
        $dataProvider = $searchModel->findDebtorMonth(Yii::$app->request->get());

        $dataProvider->pagination->pageSize = PageSize::get();

        return $this->render('debtor-month', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionNewClients()
    {
        $this->layout = $this->debtLayout;

        $monthChart = [
            '01' => DebtsHelper::getNewClientsMonthInvoiceSum('01'),
            '02' => DebtsHelper::getNewClientsMonthInvoiceSum('02'),
            '03' => DebtsHelper::getNewClientsMonthInvoiceSum('03'),
            '04' => DebtsHelper::getNewClientsMonthInvoiceSum('04'),
            '05' => DebtsHelper::getNewClientsMonthInvoiceSum('05'),
            '06' => DebtsHelper::getNewClientsMonthInvoiceSum('06'),
            '07' => DebtsHelper::getNewClientsMonthInvoiceSum('07'),
            '08' => DebtsHelper::getNewClientsMonthInvoiceSum('08'),
            '09' => DebtsHelper::getNewClientsMonthInvoiceSum('09'),
            '10' => DebtsHelper::getNewClientsMonthInvoiceSum('10'),
            '11' => DebtsHelper::getNewClientsMonthInvoiceSum('11'),
            '12' => DebtsHelper::getNewClientsMonthInvoiceSum('12'),
        ];

        $clientsChart = [
            '01' => DebtsHelper::getNewClientsMonth('01'),
            '02' => DebtsHelper::getNewClientsMonth('02'),
            '03' => DebtsHelper::getNewClientsMonth('03'),
            '04' => DebtsHelper::getNewClientsMonth('04'),
            '05' => DebtsHelper::getNewClientsMonth('05'),
            '06' => DebtsHelper::getNewClientsMonth('06'),
            '07' => DebtsHelper::getNewClientsMonth('07'),
            '08' => DebtsHelper::getNewClientsMonth('08'),
            '09' => DebtsHelper::getNewClientsMonth('09'),
            '10' => DebtsHelper::getNewClientsMonth('10'),
            '11' => DebtsHelper::getNewClientsMonth('11'),
            '12' => DebtsHelper::getNewClientsMonth('12'),
        ];

        $searchModel = new DebtReportSearch();
        $dataProvider = $searchModel->findNewClients(Yii::$app->request->get());

        return $this->render('new-clients', [
            'monthChart' => $monthChart,
            'clientsChart' => $clientsChart,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}