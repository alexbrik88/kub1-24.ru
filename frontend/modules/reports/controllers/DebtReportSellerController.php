<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 05.07.2018
 * Time: 11:19
 */

namespace frontend\modules\reports\controllers;


use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\models\company\CompanyFirstEvent;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\components\PageSize;
use frontend\models\Documents;
use frontend\modules\reports\components\DebtsHelper;
use frontend\modules\reports\models\DebtReportSearch;
use frontend\rbac\UserRole;
use Yii;

class DebtReportSellerController extends FrontendController
{
    /**
     * @var string
     */
    public $debtLayout = 'debt-report-seller';

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\Reports::REPORTS_SUPPLIERS,
                        ],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    public function actionDebtor()
    {
        $this->layout = $this->debtLayout;

        $searchModel = new DebtReportSearch();
        $dataProvider = $searchModel->findDebtor(Yii::$app->request->get(), Documents::IO_TYPE_IN);
        $dataProvider->pagination->pageSize = PageSize::get();

        $currentDebt = DebtsHelper::getCurrentDebt(null, Documents::IO_TYPE_IN);
        $debtSum10 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_0_10, false, false, null, Documents::IO_TYPE_IN);
        $debtSum30 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_11_30, false, false, null, Documents::IO_TYPE_IN);
        $debtSum60 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_31_60, false, false, null, Documents::IO_TYPE_IN);
        $debtSum90 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_61_90, false, false, null, Documents::IO_TYPE_IN);
        $debtSumMore90 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, false, false, null, Documents::IO_TYPE_IN);
        $allDebtSum = $currentDebt + $debtSum10 + $debtSum30 + $debtSum60 + $debtSum90 + $debtSumMore90;
        $formatSum = $allDebtSum ? $allDebtSum : 1;

        CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 55);

        return $this->render('debtor', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'currentDebt' => $currentDebt,
            'debtSum10' => $debtSum10,
            'debtSum30' => $debtSum30,
            'debtSum60' => $debtSum60,
            'debtSum90' => $debtSum90,
            'debtSumMore90' => $debtSumMore90,
            'allDebtSum' => $allDebtSum,
            'formatSum' => $formatSum,
        ]);
    }
}