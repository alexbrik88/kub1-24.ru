<?php

namespace frontend\modules\reports\controllers;

use common\components\filters\AccessControl;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use common\models\company\CompanyFirstEvent;
use common\models\document\InvoiceExpenditureItem;
use common\models\employee\Employee;
use common\models\ExpenseItemFlowOfFunds;
use common\models\IncomeItemFlowOfFunds;
use common\models\service\SubscribeTariffGroup;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\components\PageSize;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\cash\models\CashContractorType;
use frontend\modules\reports\components\DebtsHelper;
use frontend\modules\reports\models\AnalysisSearch;
use frontend\modules\reports\models\BalanceCompanyItem;
use frontend\modules\reports\models\BalanceItemAmount;
use frontend\modules\reports\models\BalanceSearch;
use frontend\modules\reports\models\CashFlowsAutoPlan;
use frontend\modules\reports\models\debtor\DebtorHelper;
use frontend\modules\reports\models\DebtReportSearch;
use frontend\modules\reports\models\ExpensesSearch;
use frontend\modules\reports\models\IncomeSearch;
use frontend\modules\reports\models\PaymentCalendarSearch;
use frontend\modules\reports\models\PlanCashFlows;
use frontend\modules\reports\models\PlanFactSearch;
use frontend\modules\reports\models\ProfitAndLossCompanyItem;
use frontend\modules\reports\models\ProfitAndLossSearchModel;
use frontend\rbac\UserRole;
use Yii;
use yii\base\InvalidParamException;
use yii\db\ActiveRecord;
use yii\db\Connection;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

use frontend\modules\analytics\models\FlowOfFundsReportSearch;
use frontend\modules\analytics\models\OddsSearch;

/**
 * Class FinanceController
 * @package frontend\modules\reports\controllers
 */
class FinanceController extends FrontendController
{
    /**
     * @var
     */
    public $year;

    /**
     * @var string
     */
    public $layout = 'finance';

    /**
     * @var array
     */
    static private $analyticsClasses = [
        'FlowOfFundsReportSearch',
        'ProfitAndLossSearchModel',
        'PlanFactSearch',
        'BalanceSearch',
        'ExpensesSearch',
        'IncomeSearch',
        'PaymentCalendarSearch',
        'OddsSearch'
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\Reports::FINANCE,
                        ]
                    ],
                ],
            ],
        ]);
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (!Yii::$app->request->isAjax) {
                foreach (self::$analyticsClasses as $class) {
                    if (\Yii::$app->request->get($class)) {
                        $year = \common\components\helpers\ArrayHelper::getValue(\Yii::$app->request->get($class), 'year');
                        if ($year > 2010 && $year < 2100) {
                            \Yii::$app->session->set('modules.reports.finance.year', $year);
                            $getParams = \Yii::$app->request->queryParams;
                            unset($getParams[$class]);
                            return $this->redirect(Url::toRoute([Url::base()] + $getParams))->send();
                        }
                    }
                }
            }

            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function actionXyz()
    {
        $searchModel = new AnalysisSearch([
            'company' => Yii::$app->user->identity->company,
            'dateRange' => StatisticPeriod::getSessionPeriod(),
        ]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        $dataProvider->pagination->pageSize = PageSize::get();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param int $activeTab
     * @return string
     * @throws \yii\base\Exception|\Exception
     */
    public function actionOdds($activeTab = OddsSearch::TAB_ODDS, $periodSize = 'months')
    {
        $searchModel = new OddsSearch();

        $dataFull = ($periodSize == 'days') ?
            $searchModel->searchByDays($activeTab, Yii::$app->request->get()) :
            $searchModel->search($activeTab, Yii::$app->request->get());
        $data = &$dataFull['data'];
        $growingData = &$dataFull['growingData'];
        $totalData = &$dataFull['totalData'];

        $warnings = $searchModel->generateWarnings(ArrayHelper::getValue($totalData, 'growing.data'), $activeTab == OddsSearch::TAB_ODDS ? $data : []);
        CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 60);

        return $this->render('odds2', [
            'searchModel' => $searchModel,
            'data' => $data,
            'growingData' => $growingData,
            'totalData' => $totalData,
            'activeTab' => $activeTab,
            'periodSize' => $periodSize,
            'warnings' => $warnings,
        ]);
    }

    /**
     * @return bool
     */
    public function actionChangeBalanceBlock()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $items = Yii::$app->request->post('items', []);
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        foreach ($items as $itemID => $sort) {
            /* @var $balanceCompanyItem BalanceCompanyItem */
            $balanceCompanyItem = BalanceCompanyItem::find()
                ->andWhere(['and',
                    ['company_id' => $company->id],
                    ['item_id' => $itemID],
                ])->one();
            if ($balanceCompanyItem) {
                $balanceCompanyItem->sort = (int)$sort;
                $balanceCompanyItem->save(true, ['sort']);
            }
        }
        return true;
    }

    /**
     * @return array
     */
    public function actionChangeProfitAndLossBlock()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $itemID = Yii::$app->request->post('item_id');
        $expenseType = Yii::$app->request->post('expense_type');
        $year = Yii::$app->request->post('year');
        if ($itemID && in_array($expenseType, array_keys(ProfitAndLossCompanyItem::$expenseTypes)) && $year) {
            /* @var $model ProfitAndLossCompanyItem */
            $model = ProfitAndLossCompanyItem::find()
                ->andWhere(['and',
                    ['company_id' => $company->id],
                    ['item_id' => $itemID],
                    ['can_update' => true],
                ])->one();
            if ($model) {
                $model->expense_type = $expenseType;
                if ($model->save()) {
                    $searchModel = new ProfitAndLossSearchModel();
                    $searchModel->year = $year;
                    $currentMonthNumber = date('n');
                    $currentQuarter = (int)ceil(date('m') / 3);
                    $data = $searchModel->handleItems();

                    return [
                        'result' => true,
                        'html' => $this->renderPartial('_partial/profit_and_loss_table', [
                            'searchModel' => $searchModel,
                            'currentMonthNumber' => $currentMonthNumber,
                            'currentQuarter' => $currentQuarter,
                            'data' => $data,
                        ]),
                    ];
                }
            }
        }

        return [
            'result' => false,
        ];
    }

    /**
     * @return array
     * @throws \yii\base\Exception
     */
    public function actionChangeBlock()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        /* @var $company Company */
        $company = $user->company;
        $itemID = Yii::$app->request->post('item_id');
        $blockTypeID = Yii::$app->request->post('block_type_id');
        $type = Yii::$app->request->post('type');
        $year = Yii::$app->request->post('year');
        $reportType = Yii::$app->request->post('reportType');
        $periodSize = Yii::$app->request->post('periodSize', 'months');
        if ($itemID !== null && $blockTypeID !== null && $type !== null && $year !== null && $reportType) {
            /* @var $className IncomeItemFlowOfFunds|ExpenseItemFlowOfFunds */
            $className = null;
            $block = FlowOfFundsReportSearch::$blockByType[$blockTypeID];
            if ($type == 'income') {
                $className = IncomeItemFlowOfFunds::className();
            } elseif ($type == 'expense') {
                $className = ExpenseItemFlowOfFunds::className();
            } else {
                return ['result' => false];
            }
            /* @var $model IncomeItemFlowOfFunds|ExpenseItemFlowOfFunds */
            $model = $className::find()->andWhere(['and',
                ['company_id' => $user->company->id],
                [$type . '_item_id' => $itemID],
            ])->one();
            if ($model) {
                $model->flow_of_funds_block = $block;
                if ($model->save()) {
                    if ($reportType == 'odds') {
                        $searchModel = new FlowOfFundsReportSearch();
                        $searchModel->year = $year;

                        if ($periodSize == 'days') {
                            $dataFull = $searchModel->searchByActivityDays(Yii::$app->request->get());
                            $data = &$dataFull['result'];
                            $dataDAY = &$dataFull['resultDAY'];
                        } else {
                            $data = $searchModel->searchByActivity(Yii::$app->request->get());
                            $dataDAY = null;
                        }

                        $expenditureItems = isset($data['itemName']) ? $data['itemName'] : [];
                        asort($expenditureItems);
                        $types = isset($data['types']) ? $data['types'] : [];
                        $balance = isset($data['balance']) ? $data['balance'] : 0;
                        $growingBalance = isset($data['growingBalance']) ? $data['growingBalance'] : 0;
                        $blocks = isset($data['blocks']) ? $data['blocks'] : [];
                        $checkMonth = $company->isFreeTariff && $searchModel->year == date('Y');

                        //unset($data['itemName']);
                        //unset($data['types']);
                        //unset($data['balance']);
                        //unset($data['growingBalance']);
                        //unset($data['blocks']);

                        $warnings = []; //$searchModel->generateWarnings($company, $growingBalance, $types);

                        $activeTab = FlowOfFundsReportSearch::TAB_ODDS;

                        return [
                            'result' => true,
                            'html' => $this->renderPartial('odds', [
                                'searchModel' => $searchModel,
                                'data' => $data,
                                'dataDAY' => $dataDAY,
                                'expenditureItems' => $expenditureItems,
                                'types' => $types,
                                'balance' => $balance,
                                'growingBalance' => $growingBalance,
                                'blocks' => $blocks,
                                'warnings' => $warnings,
                                'checkMonth' => $checkMonth,
                                'activeTab' => $activeTab,
                                'periodSize' => $periodSize,
                            ]),
                        ];
                    } else {
                        $searchModel = new PaymentCalendarSearch();
                        $searchModel->year = $year;
                        $data = $searchModel->searchByActivity(Yii::$app->request->get());
                        $currentMonthNumber = date('n');
                        $currentQuarter = (int)ceil(date('m') / 3);
                        $isCurrentYear = date('Y') == $searchModel->year;
                        $hasFlows = $searchModel->company->getCashBankFlows()->exists() ||
                            $searchModel->company->getCashOrderFlows()->exists() ||
                            $searchModel->company->getCashEmoneyFlows()->exists();

                        return [
                            'result' => true,
                            'html' => $this->renderPartial('_partial/payment_calendar_by_activity', [
                                'searchModel' => $searchModel,
                                'activeTab' => PaymentCalendarSearch::TAB_BY_ACTIVITY,
                                'data' => $data,
                                'currentMonthNumber' => $currentMonthNumber,
                                'currentQuarter' => $currentQuarter,
                                'isCurrentYear' => $isCurrentYear,
                                'hasFlows' => $hasFlows
                            ]),
                        ];
                    }
                }
            }
        }

        return ['result' => false];
    }

    /**
     * @return bool|string
     * @throws \Exception
     */
    public function actionItemList()
    {
        $items = Yii::$app->request->post('items');
        $items = json_decode($items, true);
        if ($items) {
            $reportType = $items['reportType'] ?? 'pk';
            $activeTab = $items['activeTab'] ?? 4;
            if ($reportType == 'odds' || $reportType == 'expenses' || $reportType == 'income') {
                $searchModel = new FlowOfFundsReportSearch();
                $view = '_partial/item_table';
            } else {
                $searchModel = new PaymentCalendarSearch();
                $searchModel->payment_type = isset($items['payment_type']) ? $items['payment_type'] : null;
                $view = '_partial/plan_item_table';
            }

            $searchModel->income_item_id = isset($items['income']) ? $items['income'] : null;
            $searchModel->expenditure_item_id = isset($items['expense']) ? $items['expense'] : null;
            $searchModel->period = isset($items['period']) ? $items['period'] : null;
            $searchModel->group = isset($items['group']) ? $items['group'] : null;
            $searchModel->flow_type = isset($items['flow_type']) ? $items['flow_type'] : null;
            $searchModel->payment_type = isset($items['payment_type']) ? $items['payment_type'] : null;
            $itemsDataProvider = $searchModel->searchItems(Yii::$app->request->get());
            $itemsDataProvider->pagination->pageSize = \frontend\components\PageSize::get('per-page', 50);

            return $this->render($view, [
                'searchModel' => $searchModel,
                'itemsDataProvider' => $itemsDataProvider,
                'activeTab' => $activeTab,
                'subTab' => null
            ]);
        }

        return false;
    }

    /**
     * @return string
     */
    public function actionBalance()
    {
        $model = new BalanceSearch(['company' => Yii::$app->user->identity->company]);
        if (Yii::$app->request->get('BalanceSearch')) {
            $bSearch = Yii::$app->request->get('BalanceSearch');
            $model->setYear(ArrayHelper::getValue($bSearch, 'year'));
        }
        $balance = $model->search(Yii::$app->request->queryParams);
        $this->year = $model->year;

        BalanceCompanyItem::loadCompanyItems();

        return $this->render('balance', [
            'model' => $model,
            'balance' => $balance,
        ]);
    }

    /**
     * @return Response
     * @throws \Throwable
     */
    public function actionUpdateBalanceItem()
    {
        $year = Yii::$app->request->post('year');
        $itemID = Yii::$app->request->post('itemID');
        $itemAmount = Yii::$app->request->post('BalanceItemAmount');
        Yii::$app->session->set('activeCheckbox', Yii::$app->request->post('activeCheckbox'));
        if ($itemID && $year && $itemAmount) {
            if (BalanceItemAmount::loadItems($itemAmount, $itemID, $year)) {
                Yii::$app->session->setFlash('success', 'Данные обновлены.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при обновлении данных.');
            }
        }

        return $this->redirect(['balance', 'year' => $year]);
    }

    /**
     * @param int $activeTab
     * @return string
     * @throws \Exception
     */
    public function actionPaymentCalendar($activeTab = PaymentCalendarSearch::TAB_BY_PURSE)
    {
        $searchModel = new PaymentCalendarSearch();
        $data = []; // $searchModel->search($activeTab, Yii::$app->request->get());
        $subTab = Yii::$app->request->get('subTab');
        $searchModel->period = ['start' => '01', 'end' => '12'];
        $itemsDataProvider = ($subTab == 'register') ? $searchModel->searchItems(Yii::$app->request->get()) : null;
        $this->year = $searchModel->year;
        $currentMonthNumber = date('n');
        $currentQuarter = (int)ceil(date('m') / 3);

        return $this->render('payment-calendar', [
            'activeTab' => $activeTab,
            'searchModel' => $searchModel,
            'data' => $data,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'itemsDataProvider' => $itemsDataProvider,
            'hasFlows' => true,
        ]);
    }

    /**
     * @param int $activeTab
     * @return string
     * @throws \Exception
     */
    public function actionBreakevenPoint()
    {
        return $this->render('breakeven-point', []);
    }

    /**
     * @param int $activeTab
     * @return string
     * @throws \Exception
     */
    public function actionIncome($activeTab = IncomeSearch::TAB_BY_ACTIVITY)
    {
        $searchModel = new IncomeSearch();
        $data = []; //$searchModel->search($activeTab, Yii::$app->request->get());
        $this->year = $searchModel->year;
        $currentMonthNumber = date('n');
        $currentQuarter = (int)ceil(date('m') / 3);

        return $this->render('income', [
            'activeTab' => $activeTab,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'searchModel' => $searchModel,
            'data' => $data,
        ]);
    }

    /**
     * @param int $activeTab
     * @return string
     * @throws \Exception
     */
    public function actionExpenses($activeTab = ExpensesSearch::TAB_BY_ACTIVITY)
    {
        $searchModel = new ExpensesSearch();
        $data = []; //$searchModel->search($activeTab, Yii::$app->request->get());
        $this->year = $searchModel->year;
        $currentMonthNumber = date('n');
        $currentQuarter = (int)ceil(date('m') / 3);

        return $this->render('expenses', [
            'activeTab' => $activeTab,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'searchModel' => $searchModel,
            'data' => $data,
        ]);
    }

    /**
     * @param int $activeTab
     * @return string
     */
    public function actionProfitAndLoss($activeTab = ProfitAndLossSearchModel::TAB_ALL_OPERATIONS)
    {
        $searchModel = new ProfitAndLossSearchModel();
        $currentMonthNumber = date('n');
        $currentQuarter = (int)ceil(date('m') / 3);
        $searchModel->load(Yii::$app->request->get());
        $this->year = $searchModel->year;
        $data = $searchModel->handleItems();
        ProfitAndLossCompanyItem::loadCompanyItems();

        return $this->render('profit-and-loss', [
            'searchModel' => $searchModel,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'data' => $data,
            'activeTab' => $activeTab
        ]);
    }

    /**
     * @return string
     */
    public function actionBreakEven()
    {
        return $this->render('break-even', [
        ]);
    }

    /**
     * @param $type
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionDebtor($type)
    {
        if (!in_array($type, [Documents::IO_TYPE_IN, Documents::IO_TYPE_OUT]))
            throw new NotFoundHttpException('The requested page does not exist.');

        $searchModel = new DebtReportSearch();
        $dataProvider = $searchModel->findDebtor(Yii::$app->request->get(), $type);
        $dataProvider->pagination->pageSize = PageSize::get();

        $currentDebt = DebtsHelper::getCurrentDebt(null, $type);
        $debtSum10 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_0_10, false, false, null, $type);
        $debtSum30 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_11_30, false, false, null, $type);
        $debtSum60 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_31_60, false, false, null, $type);
        $debtSum90 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_61_90, false, false, null, $type);
        $debtSumMore90 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, false, false, null, $type);
        $allDebtSum = $currentDebt + $debtSum10 + $debtSum30 + $debtSum60 + $debtSum90 + $debtSumMore90;
        $formatSum = $allDebtSum ? $allDebtSum : 1;

        return $this->render('debtor', [
            'type' => $type,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'currentDebt' => $currentDebt,
            'debtSum10' => $debtSum10,
            'debtSum30' => $debtSum30,
            'debtSum60' => $debtSum60,
            'debtSum90' => $debtSum90,
            'debtSumMore90' => $debtSumMore90,
            'allDebtSum' => $allDebtSum,
            'formatSum' => $formatSum,
        ]);
    }

    /**
     * @param int $activeTab
     * @return mixed|string|Response
     * @throws \Throwable
     */
    public function actionCreatePlanItem($activeTab = PaymentCalendarSearch::TAB_BY_PURSE)
    {
        /** @var Company $company */
        $company = Yii::$app->user->identity->company;
        $checkingAccountant = $company->mainCheckingAccountant;
        $cashbox = $company->getCashboxes()->andWhere(['is_main' => true])->one();
        $emoney = $company->getEmoneys()->andWhere(['is_main' => true])->one();

        $model = new PlanCashFlows();
        $model->company_id = $company->id;
        $model->flow_type = CashFlowsBase::FLOW_TYPE_INCOME;
        $model->checking_accountant_id = ($checkingAccountant) ? $checkingAccountant->id : null;
        $model->cashbox_id = ($cashbox) ? $cashbox->id : null;
        $model->emoney_id = ($emoney) ? $emoney->id : null;

        $subTab = Yii::$app->request->get('subTab');
        $year = Yii::$app->request->get('year', date('Y'));

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->_save()) {
            Yii::$app->session->setFlash('success', 'Плановая операция добавлена.');

            return $this->redirect(['payment-calendar', 'activeTab' => $activeTab, 'subTab' => $subTab, 'PaymentCalendarSearch' => [
                'year' => $year,
            ]]);
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_partial/plan_item_form', [
                'model' => $model,
                'company' => $company,
                'activeTab' => $activeTab,
                'subTab' => Yii::$app->request->get('subTab'),
                'year' => $year,
            ]);
        }

        return $this->render('_partial/plan_item_form', [
            'model' => $model,
            'company' => $company,
            'activeTab' => $activeTab,
            'subTab' => Yii::$app->request->get('subTab'),
            'year' => $year,
        ]);
    }

    /**
     * @param $id
     * @param int $activeTab
     * @return mixed|string|Response
     * @throws \Throwable
     */
    public function actionUpdatePlanItem($id, $activeTab = PaymentCalendarSearch::TAB_BY_PURSE)
    {
        $company = Yii::$app->user->identity->company;
        $model = PlanCashFlows::findOne($id);
        if ($model == null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $year = Yii::$app->request->get('year');
        $year = $year ? $year : date('Y');
        $subTab = Yii::$app->request->get('subTab');

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->_update()) {
            Yii::$app->session->setFlash('success', 'Плановая операция обновлена.');

            return $this->redirect(['payment-calendar',
                'activeTab' => $activeTab,
                'PaymentCalendarSearch' => [
                    'year' => $year,
                ],
                'subTab' => $subTab
            ]);
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_partial/plan_item_form', [
                'model' => $model,
                'company' => $company,
                'activeTab' => $activeTab,
                'year' => $year,
                'subTab' => $subTab
            ]);
        }

        return $this->render('_partial/plan_item_form', [
            'model' => $model,
            'company' => $company,
            'activeTab' => $activeTab,
            'year' => $year,
            'subTab' => $subTab
        ]);
    }

    /**
     * @param $id
     * @param int $activeTab
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeletePlanItem($id, $activeTab = PaymentCalendarSearch::TAB_BY_PURSE)
    {
        $model = PlanCashFlows::findOne($id);
        $year = Yii::$app->request->get('year');
        $year = $year ? $year : date('Y');
        $subTab = Yii::$app->request->get('subTab');

        if ($model == null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        if ($model->delete()) {
            Yii::$app->session->setFlash('success', 'Плановая операция удалена.');
        } else {
            Yii::$app->session->setFlash('error', 'Произошла ошибка при удалении плановой операции.');
        }

        return $this->redirect(['payment-calendar',
            'activeTab' => $activeTab,
            'PaymentCalendarSearch' => [
                'year' => $year,
            ],
            'subTab' => $subTab
        ]);
    }

    /**
     * @param $id
     * @param $tb
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteFlowItem($id, $tb)
    {
        $model = $this->findFlowItem($tb, $id);
        // prevent pjax call
        if (!Yii::$app->request->isPjax) {
            if ($model->delete()) {
                Yii::$app->session->setFlash('success', 'Операция удалена.');
            } else {
                Yii::$app->session->setFlash('error', 'Произошла ошибка при удалении операции.');
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     */
    public function actionManyDelete()
    {
        $idArray = Yii::$app->request->post('flowId', []);
        foreach ($idArray as $id) {
            $model = PlanCashFlows::findOne(['id' => $id, 'company_id' => Yii::$app->user->identity->company_id]);
            if ($model == null) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
            $model->delete();
        }
        Yii::$app->session->setFlash('success', 'Плановые операции удалены.');

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     */
    public function actionManyDeleteFlowItem()
    {
        $data = Yii::$app->request->post('flowId', []);
        foreach ($data as $tb => $idArray) {
            foreach ($idArray as $id) {
                $model = $this->findFlowItem($tb, $id);
                $model->delete();
            }
        }
        Yii::$app->session->setFlash('success', 'Операции удалены.');

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionManyFlowItem()
    {
        $data = Yii::$app->request->post('flowId', []);
        $incomeItemID = Yii::$app->request->post('incomeItemIdManyItem');
        $expenseItemID = Yii::$app->request->post('expenditureItemIdManyItem');
        foreach ($data as $tb => $idArray) {
            foreach ($idArray as $id) {
                $model = $this->findFlowItem($tb, $id);
                $attribute = $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? 'income_item_id' : 'expenditure_item_id';
                $value = $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? $incomeItemID : $expenseItemID;
                if ($model instanceof CashBankFlows) {
                    $model->setScenario('update');
                }
                $model->$attribute = $value;
                $model->save(true, [$attribute]);
            }
        }
        Yii::$app->session->setFlash('success', 'Статьи по операциям изменены.');

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param int $activeTab
     * @return string
     * @throws \Exception
     */
    public function actionPlanFact($activeTab = PlanFactSearch::TAB_BY_ACTIVITY)
    {
        $searchModel = new PlanFactSearch();
        $data = []; //$searchModel->search($activeTab, Yii::$app->request->get());
        $this->year = $searchModel->year;
        $currentMonthNumber = date('n');
        $currentQuarter = (int)ceil(date('m') / 3);
        $checkMonth = $searchModel->company->isFreeTariff && $searchModel->isCurrentYear;

        return $this->render('plan-fact', [
            'activeTab' => $activeTab,
            'searchModel' => $searchModel,
            'data' => $data,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'checkMonth' => $checkMonth,
        ]);
    }

    /**
     * @param $activeTab
     * @param $year
     * @return Response
     * @throws \Throwable
     */
    public function actionUpdateAutoPlan($activeTab, $year)
    {
        $data = Yii::$app->request->post('CashFlowsAutoPlan', []);
        $result = Yii::$app->db->transaction(function (Connection $db) use ($data) {
            foreach ($data as $item) {
                $isDeleted = (int)$item['is_deleted'];
                if ($isDeleted) {
                    continue;
                }
                $model = new CashFlowsAutoPlan();
                $model->company_id = Yii::$app->user->identity->company_id;
                $model->flow_type = $item['flow_type'];
                $model->payment_type = $item['payment_type'];
                $model->item = $item['item'];
                $model->contractor_id = $item['contractor_id'];
                $model->amount = $item['amount'];
                $model->plan_date = $item['plan_date'];
                $model->end_plan_date = $item['end_plan_date'];
                $model->is_deleted = $isDeleted;
                if (in_array($model->contractor_id, [
                    CashContractorType::BANK_TEXT,
                    CashContractorType::ORDER_TEXT,
                    CashContractorType::EMONEY_TEXT
                ])) {
                    $model->item = InvoiceExpenditureItem::ITEM_OTHER;
                }
                if (!$model->save()) {
                    $db->transaction->rollBack();
                    return false;
                }
                if (!$model->is_deleted) {
                    $planCashFlows = new PlanCashFlows();
                    $planCashFlows->company_id = Yii::$app->user->identity->company_id;
                    $planCashFlows->contractor_id = $model->contractor_id;
                    $planCashFlows->flow_type = $model->flow_type;
                    $planCashFlows->payment_type = $model->payment_type;
                    $planCashFlows->amount = $item['amount'];
                    $planCashFlows->expenditure_item_id = $model->item;
                    $planCashFlows->is_repeated = true;
                    $planCashFlows->date = $model->plan_date;
                    $planCashFlows->planEndDate = $model->end_plan_date;
                    $planCashFlows->period = PlanCashFlows::PERIOD_MONTH;
                    $planCashFlows->description = 'Создано АвтоПланированием';
                    if (!$planCashFlows->_save()) {
                        $db->transaction->rollBack();
                        return false;
                    }
                }
            }

            return true;
        });
        if ($result) {
            Yii::$app->session->setFlash('success', 'Изменения по автопланированию сохранены.');
        } else {
            Yii::$app->session->setFlash('error', 'Произошла ошибка при изменении автопланирования.');
        }

        return $this->redirect(['payment-calendar', 'activeTab' => $activeTab, ['PaymentCalendarSearch' => ['year' => $year]]]);
    }

    /**
     * @param $type
     * @param $year
     * @return string
     * @throws \Exception
     */
    public function actionGetXls($type, $year)
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        Yii::$app->response->format = Response::FORMAT_RAW;

        if (in_array($type, [FlowOfFundsReportSearch::TAB_ODDS, FlowOfFundsReportSearch::TAB_ODDS_BY_PURSE])) {
            $searchModel = new FlowOfFundsReportSearch();
        } elseif (in_array($type, [PaymentCalendarSearch::TAB_BY_PURSE, PaymentCalendarSearch::TAB_BY_ACTIVITY])) {
            $searchModel = new PaymentCalendarSearch();
        } elseif (in_array($type, [PlanFactSearch::TAB_BY_PURSE, PlanFactSearch::TAB_BY_ACTIVITY])) {
            $searchModel = new PlanFactSearch();
        } elseif (in_array($type, [ExpensesSearch::TAB_BY_PURSE, ExpensesSearch::TAB_BY_ACTIVITY])) {
            $searchModel = new ExpensesSearch();
        } elseif (in_array($type, [IncomeSearch::TAB_BY_PURSE, IncomeSearch::TAB_BY_ACTIVITY])) {
            $searchModel = new IncomeSearch();
        } elseif (in_array($type, [BalanceSearch::TAB_BALANCE])) {
            $searchModel = new BalanceSearch(['company' => Yii::$app->user->identity->company]);
        } elseif (in_array($type, [ProfitAndLossSearchModel::TAB_ALL_OPERATIONS, ProfitAndLossSearchModel::TAB_ACCOUNTING_OPERATIONS_ONLY])) {
            $searchModel = new ProfitAndLossSearchModel();
        } else {
            throw new InvalidParamException('Invalid type param.');
        }
        $searchModel->year = $year;
        $searchModel->generateXls($type);

        return;
    }

    /**
     * @param $flowType
     * @param $paymentType
     * @param $item
     * @param $contractorID
     * @return CashFlowsAutoPlan|ActiveRecord|null
     */
    private function findCashFlowsAutoPlan($flowType, $paymentType, $item, $contractorID)
    {
        return CashFlowsAutoPlan::find()
            ->andWhere(['and',
                ['company_id' => Yii::$app->user->identity->company_id],
                ['flow_type' => $flowType],
                ['payment_type' => $paymentType],
                ['item' => $item],
                ['contractor_id' => $contractorID],
            ])->one();
    }

    /**
     * @param $tableName
     * @param $id
     * @return CashBankFlows|CashOrderFlows|CashEmoneyFlows
     * @throws NotFoundHttpException
     */
    private function findFlowItem($tableName, $id)
    {
        switch ($tableName) {
            case CashBankFlows::tableName():
                $className = CashBankFlows::className();
                break;
            case CashOrderFlows::tableName():
                $className = CashOrderFlows::className();
                break;
            case CashEmoneyFlows::tableName():
                $className = CashEmoneyFlows::className();
                break;
            case PlanCashFlows::tableName():
                $className = PlanCashFlows::className();
                break;
            default:
                throw new NotFoundHttpException('The requested flow does not exist.');
        }
        $model = $className::find()
            ->andWhere(['id' => $id])
            ->andWhere(['company_id' => Yii::$app->user->identity->company_id])
            ->one();
        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $model;
    }
}