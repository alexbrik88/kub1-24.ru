<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.11.2017
 * Time: 9:23
 */

namespace frontend\modules\reports\controllers;

use common\components\filters\AccessControl;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\modules\reports\models\SellingReportSearch;
use frontend\rbac\UserRole;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class SellingReportController
 * @package frontend\modules\reports\controllers
 */
class SellingReportController extends FrontendController
{
    /**
     * @var string
     */
    public $layout = 'selling-report';

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\Reports::REPORTS_SALES,
                        ],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('_to_analytics');

        //$searchModel = new SellingReportSearch([
        //    'companyId' => Yii::$app->user->identity->company->id,
        //]);
        //
        //$searchModel->buildDataArray(Yii::$app->request->get());
        //$chartsOptions = $searchModel->getHighchartsOptions();
        //$tableData = $searchModel->getTableData();
        //$totalDateData = [];
        //if (isset($tableData['totalDateData'])) {
        //    $totalDateData = $tableData['totalDateData'];
        //    unset($tableData['totalDateData']);
        //}
        //
        //\common\models\company\CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 56);
        //
        //return $this->render('index', [
        //    'searchModel' => $searchModel,
        //    'chartsOptions' => $chartsOptions,
        //    'tableData' => $tableData,
        //    'totalDateData' => $totalDateData,
        //]);
    }
}