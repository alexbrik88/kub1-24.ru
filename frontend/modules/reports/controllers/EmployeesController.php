<?php

namespace frontend\modules\reports\controllers;

use common\components\filters\AccessControl;
use common\models\employee\Employee;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\modules\reports\models\EmployeesReportSearch;
use frontend\rbac\UserRole;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Response;

/**
 * Class EmployeesController
 * @package frontend\modules\reports\controllers
 */
class EmployeesController extends FrontendController
{
    /**
     * @var string
     */
    public $layout = 'employees';

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\Reports::REPORTS_EMPLOYEES,
                        ],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new EmployeesReportSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
