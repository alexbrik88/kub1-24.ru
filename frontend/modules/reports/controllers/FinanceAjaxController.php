<?php

namespace frontend\modules\reports\controllers;

use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use common\models\company\CompanyFirstEvent;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\OrderAct;
use common\models\document\PackingList;
use common\models\document\query\InvoiceQuery;
use common\models\document\Upd;
use common\models\employee\Config;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\analytics\models\OddsSearch;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\modules\reports\models\BalanceSearch;
use frontend\modules\reports\models\DebtReportSearch;
use frontend\modules\reports\models\ExpensesSearch;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use frontend\modules\reports\models\IncomeSearch;
use frontend\modules\reports\models\PaymentCalendarSearch;
use frontend\modules\reports\models\PlanCashContractor;
use frontend\modules\reports\models\PlanCashFlows;
use frontend\modules\reports\models\PlanCashRule;
use frontend\modules\reports\models\ProfitAndLossCompanyItem;
use frontend\modules\reports\models\ProfitAndLossSearchModel;
use Yii;
use frontend\components\FrontendController;
use yii\base\Exception;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use frontend\components\PageSize;

/**
 * Class FinanceAjaxController
 */
class FinanceAjaxController extends FrontendController
{
    const VIEW = '@frontend/modules/reports/views/finance/';
    static $monthsByQuarter = [1 => [1,3], 2 => [4,6], 3 => [7,9], 4 => [10,12]];

    // Items
    private $itemsSearchModel;
    private $itemsDataProvider;
    private $itemsSearchBy;
    private $isItemsSearchByChanged;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\Reports::VIEW,
                        ],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return bool|string
     * @throws \Exception
     */
    public function actionGetProfitAndLossItems()
    {
        // raw data
        $searchBy = Yii::$app->request->post('searchBy');
        $enableToggle = Yii::$app->request->post('enableToggle', false);
        $year = Yii::$app->request->post('year', date('Y'));
        $quarter = Yii::$app->request->post('quarter', null);
        $month =  Yii::$app->request->post('month', null);
        $article = Yii::$app->request->post('article', '-');

        if (!in_array($searchBy, ['docs', 'flows']))
            throw new \Exception('Page not found.');

        // calc data
        if ($quarter >= 1 && $quarter <= 4) {
            $monthStart = self::$monthsByQuarter[$quarter][0];
            $monthEnd   = self::$monthsByQuarter[$quarter][1];
        } elseif ($month >= 1 && $month <= 12) {
            $monthStart = $monthEnd = $month;
        } else {
            $monthStart = 1;
            $monthEnd = 12;
        }

        // needed data
        list($flowType, $itemId) = explode('-', $article);
        $periodStart = $year . '-' . str_pad($monthStart, 2, '0', STR_PAD_LEFT) . '-' . '01';
        $periodEnd = $year . '-' . str_pad($monthEnd, 2, '0', STR_PAD_LEFT) . '-' . cal_days_in_month(CAL_GREGORIAN, $monthEnd, $year);

        $this->itemsSearchModel = new ProfitAndLossSearchModel();
        $this->itemsSearchBy = $searchBy;

        if ($searchBy == 'flows') {
            $this->itemsDataProvider = $this->_getProfitAndLossItemsByFlow($flowType, $itemId, $periodStart, $periodEnd);
            if ($this->itemsDataProvider->totalCount == 0 && $enableToggle) {
                $this->itemsSearchBy = 'docs';
                $this->isItemsSearchByChanged = true;
                $this->itemsSearchModel = new ProfitAndLossSearchModel();
                $this->itemsDataProvider = $this->_getProfitAndLossItemsByDoc($flowType, $itemId, $periodStart, $periodEnd);
            }
        } elseif ($searchBy == 'docs') {
            $this->itemsDataProvider = $this->_getProfitAndLossItemsByDoc($flowType, $itemId, $periodStart, $periodEnd);
            if ($this->itemsDataProvider->totalCount == 0 && $enableToggle) {
                $this->itemsSearchBy = 'flows';
                $this->isItemsSearchByChanged = true;
                $this->itemsSearchModel = new ProfitAndLossSearchModel();
                $this->itemsDataProvider = $this->_getProfitAndLossItemsByFlow($flowType, $itemId, $periodStart, $periodEnd);
            }
        }

        $this->itemsDataProvider->pagination->pageSize = PageSize::get('per-page', 50);

        return $this->renderAjax(self::VIEW . '_partial/profit_and_loss_items_' . $this->itemsSearchBy, [
            'searchModel' => $this->itemsSearchModel,
            'itemsDataProvider' => $this->itemsDataProvider,
            'searchBy' => $this->itemsSearchBy,
            'isSearchByChanged' => $this->isItemsSearchByChanged ? $this->itemsSearchBy : false
        ]);
    }

    private function _getProfitAndLossItemsByDoc($flowType, $itemId, $periodStart, $periodEnd)
    {
        if ($flowType && $itemId) {

            $searchModel = &$this->itemsSearchModel;
            $searchModel->ioType = ($flowType == 'income') ? Documents::IO_TYPE_OUT : Documents::IO_TYPE_IN;
            $searchModel->accountingOperationsOnly = Yii::$app->request->post('accountingOperationsOnly');

            if ($flowType == 'income') {
                if ($itemId == 'revenue_doc') {
                    $searchModel->isRevenue = true;
                } else {
                    // no another income articles
                }
            } else {
                $searchModel->expenditure_item_id = $itemId;
            }

            return $searchModel->searchItemsByDocs($periodStart, $periodEnd);
        }

        throw new \yii\db\Exception('Page not found!');
    }

    private function _getProfitAndLossItemsByFlow($flowType, $itemId, $periodStart, $periodEnd)
    {
        if ($flowType && $itemId) {

            $searchModel = &$this->itemsSearchModel;
            $searchModel->accountingOperationsOnly = Yii::$app->request->post('accountingOperationsOnly');

            if ($flowType == 'income') {
                if ($itemId == 'revenue_flow') {
                    $searchModel->isRevenue = true;
                } else {
                    $searchModel->income_item_id = $itemId;
                }
            } else {
                $searchModel->expenditure_item_id = $itemId;

            }

            return $searchModel->searchItemsByFlows($periodStart, $periodEnd);
        }

        throw new \yii\db\Exception('Page not found!');
    }

    /**
     * @param int $activeTab
     * @return mixed|string|Response
     * @throws \Throwable
     */
    public function actionCreatePlanItem($activeTab = PaymentCalendarSearch::TAB_BY_PURSE)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var Company $company */
        $company = Yii::$app->user->identity->company;
        $checkingAccountant = $company->mainCheckingAccountant;
        $cashbox = $company->getCashboxes()->andWhere(['is_main' => true])->one();
        $emoney = $company->getEmoneys()->andWhere(['is_main' => true])->one();

        $model = new PlanCashFlows();
        $model->company_id = $company->id;
        $model->flow_type = CashFlowsBase::FLOW_TYPE_INCOME;
        $model->checking_accountant_id = ($checkingAccountant) ? $checkingAccountant->id : null;
        $model->cashbox_id = ($cashbox) ? $cashbox->id : null;
        $model->emoney_id = ($emoney) ? $emoney->id : null;

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->_save())
                return [
                    'success' => 'true',
                    'msg' => ($model->is_repeated ? 'Плановые операции добавлены' : 'Плановая операциия добавлена')];
            else
                return [
                    'success' => false,
                    'msg' => 'Ошибка при создании '.($model->is_repeated ? 'плановых операций' : 'плановой операции')];
        }

        // todo: not needed
        $subTab = Yii::$app->request->get('subTab');
        $year = Yii::$app->request->get('year', date('Y'));

        return $this->renderAjax('_partial/plan_item_form', [
            'model' => $model,
            'company' => $company,
            'activeTab' => $activeTab,
            'subTab' => $subTab,
            'year' => $year,
        ]);
    }

    /**
     * @param $id
     * @param int $activeTab
     * @return array|mixed|string
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionUpdatePlanItem($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $company = Yii::$app->user->identity->company;
        $model = PlanCashFlows::findOne($id);
        if ($model == null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->_update())
                return ['success' => true, 'msg' => 'Плановая операция обновлена.'];
            else
                return ['success' => false, 'msg' => 'Ошибка при обновлении плановой операции.'];
        }

        return $this->renderAjax(self::VIEW . '_partial/plan_item_form', [
            'model' => $model,
            'company' => $company,
        ]);
    }

    /**
     * @param $id
     * @return array
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteFlowItem($id, $tb)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findFlowItem($tb, $id);

        if ($model == null) {
            return ['success' => 'false', 'msg' => 'The requested page does not exist.'];
        }

        if ($model->delete()) {
            return ['success' => 'true', 'msg' => ($tb == 'plan_cash_flow' ? 'Плановая операция' : 'Операция') . ' удалена.'];
        }

        return ['success' => 'false', 'msg' => 'Произошла ошибка при удалении ' . ($tb == 'plan_cash_flow' ? 'плановой' : '') . ' операции.'];
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionManyDeleteFlowItem()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $data = Yii::$app->request->post('flowId', []);

        foreach ($data as $tb => $idArray) {
            foreach ($idArray as $id) {
                $model = $this->findFlowItem($tb, $id);
                $model->delete();
            }
        }

        return ['success' => 'true', 'msg' => 'Операции удалены.'];
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionManyDeleteDocItem()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $data = Yii::$app->request->post('docId', []);

        foreach ($data as $tb => $idArray) {
            foreach ($idArray as $id) {
                $model = $this->findDocItem($tb, $id);
                switch($tb) {
                    case 'act':
                        $invoices = $model->invoices;
                        OrderAct::deleteByAct($model->id);
                        LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT);
                        foreach ($invoices as $invoice) InvoiceHelper::checkForAct($invoice->id, $invoices);
                        break;
                    case 'packing_list':
                        $invoice = $model->invoice;
                        LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT);
                        InvoiceHelper::checkForPackingList($invoice->id);
                        break;
                    case 'upd':
                        $invoices = $model->invoices;
                        LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT);
                        foreach ($invoices as $invoice) InvoiceHelper::checkForUpd($invoice->id, $invoices);
                        break;
                }
            }
        }

        return ['success' => 'true', 'msg' => 'Операции удалены.'];
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionManyFlowItem()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $data = Yii::$app->request->post('flowId', []);
        $incomeItemID = Yii::$app->request->post('incomeItemIdManyItem');
        $expenseItemID = Yii::$app->request->post('expenditureItemIdManyItem');
        foreach ($data as $tb => $idArray) {
            foreach ($idArray as $id) {
                $model = $this->findFlowItem($tb, $id);
                $attribute = $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? 'income_item_id' : 'expenditure_item_id';
                $value = $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? $incomeItemID : $expenseItemID;
                if ($model instanceof CashBankFlows) {
                    $model->setScenario('update');
                }
                $model->$attribute = $value;
                if (!$model->save(true, [$attribute])) {
                    return ['success' => false, 'msg' => 'Ошибка при обновлении операций'];
                }
            }
        }

        return ['success' => true, 'msg' => 'Статьи по операциям изменены'];
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionManyDocItem()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $data = Yii::$app->request->post('docId', []);
        $incomeItemID = Yii::$app->request->post('incomeItemIdManyItem');
        $expenseItemID = Yii::$app->request->post('expenditureItemIdManyItem');
        foreach ($data as $tb => $idArray) {
            foreach ($idArray as $id) {
                /** @var Act|PackingList|Upd $model */
                $model = $this->findDocItem($tb, $id);

                // can't change article
                if ($model->type == Documents::IO_TYPE_OUT || !$expenseItemID)
                    continue;

                $isSaved = null;

                switch($tb) {
                    case 'act':
                    case 'upd':
                        foreach ($model->invoices as $invoice) {
                            $invoice->invoice_expenditure_item_id = (int)$expenseItemID;
                            $isSaved = LogHelper::save($invoice, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE, function (Invoice $invoice) {
                                return $invoice->save(true, ['invoice_expenditure_item_id']);
                            });
                        }
                        break;
                    case 'packing_list':
                        $invoice = $model->invoice;
                        $invoice->invoice_expenditure_item_id = (int)$expenseItemID;
                        $isSaved = LogHelper::save($invoice, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE, function (Invoice $invoice) {
                            return $invoice->save(true, ['invoice_expenditure_item_id']);
                        });
                        break;
                }

                if ($isSaved !== null && !$isSaved) {
                    return ['success' => false, 'msg' => 'Ошибка при обновлении документов'];
                }
            }
        }

        return ['success' => true, 'msg' => 'Статьи по документам изменены'];
    }

    /**
     * @param $activeTab
     * @return string
     */
    public function actionPaymentCalendarPart($activeTab)
    {
        $searchModel = new PaymentCalendarSearch();
        $data = $searchModel->search($activeTab, Yii::$app->request->post());
        $searchModel->period = ['start' => '01', 'end' => '12'];
        $currentMonthNumber = date('n');
        $currentQuarter = (int)ceil(date('m') / 3);

        $tableShowBy = ($activeTab == PaymentCalendarSearch::TAB_BY_ACTIVITY ?
            'payment_calendar_by_activity' :
            'payment_calendar_by_purse');

        return $this->renderPartial(self::VIEW . '_partial/' . $tableShowBy, [
            'searchModel' => $searchModel,
            'activeTab' => $activeTab,
            'data' => $data,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'hasFlows' => false,
            'floorMap' => Yii::$app->request->post('floorMap', [])
        ]);
    }

    /**
     * @param int $activeTab
     * @return string
     * @throws \yii\base\Exception|\Exception
     */
    public function actionOddsPart($activeTab = OddsSearch::TAB_ODDS, $periodSize = 'months')
    {
        $searchModel = new OddsSearch();

        $dataFull = ($periodSize == 'days') ?
            $searchModel->searchByDays($activeTab, Yii::$app->request->get()) :
            $searchModel->search($activeTab, Yii::$app->request->get());
        $data = &$dataFull['data'];
        $growingData = &$dataFull['growingData'];
        $totalData = &$dataFull['totalData'];

        return $this->render(self::VIEW . '_partial/odds2_table' . ($periodSize == 'days' ? '_days' : ''), [
            'searchModel' => $searchModel,
            'data' => $data,
            'growingData' => $growingData,
            'totalData' => $totalData,
            'activeTab' => $activeTab,
            'periodSize' => $periodSize,
            'warnings' => [],
            'floorMap' => Yii::$app->request->post('floorMap', [])
        ]);
    }

    /**
     * @param int $activeTab
     * @return string
     * @throws \Exception
     */
    public function actionExpensesPart($activeTab = ExpensesSearch::TAB_BY_ACTIVITY)
    {
        $searchModel = new ExpensesSearch();
        $data = $searchModel->search($activeTab, Yii::$app->request->get());
        $currentMonthNumber = date('n');
        $currentQuarter = (int)ceil(date('m') / 3);
        $tableShowBy = ($activeTab == ExpensesSearch::TAB_BY_ACTIVITY ? 'expenses_by_activity' : 'expenses_by_purse');

        return $this->renderPartial(self::VIEW . '_partial/' . $tableShowBy, [
            'activeTab' => $activeTab,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'searchModel' => $searchModel,
            'data' => $data,
            'floorMap' => Yii::$app->request->post('floorMap', [])
        ]);
    }

    /**
     * @param int $activeTab
     * @return string
     * @throws \Exception
     */
    public function actionIncomePart($activeTab = IncomeSearch::TAB_BY_ACTIVITY)
    {
        $searchModel = new IncomeSearch();
        $data = $searchModel->search($activeTab, Yii::$app->request->get());
        $currentMonthNumber = date('n');
        $currentQuarter = (int)ceil(date('m') / 3);
        $tableShowBy = ($activeTab == IncomeSearch::TAB_BY_ACTIVITY ? 'income_by_activity' : 'income_by_purse');

        return $this->renderPartial(self::VIEW . '_partial/' . $tableShowBy, [
            'activeTab' => $activeTab,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'searchModel' => $searchModel,
            'data' => $data,
            'floorMap' => Yii::$app->request->post('floorMap', [])
        ]);
    }

    /**
     * @param int $activeTab
     * @return string
     */
    public function actionProfitAndLossPart($activeTab = ProfitAndLossSearchModel::TAB_ALL_OPERATIONS)
    {
        $searchModel = new ProfitAndLossSearchModel();
        $currentMonthNumber = date('n');
        $currentQuarter = (int)ceil(date('m') / 3);
        $searchModel->load(Yii::$app->request->get());
        $data = $searchModel->handleItems();
        ProfitAndLossCompanyItem::loadCompanyItems();

        return $this->renderPartial(self::VIEW . '_partial/profit_and_loss_table', [
            'activeTab' => $activeTab,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'searchModel' => $searchModel,
            'data' => $data,
            'floorMap' => Yii::$app->request->post('floorMap', [])
        ]);
    }

    /**
     * @return string|null
     */
    public function actionGetPlanFactData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('chart-plan-fact-ajax')) {

            $period = Yii::$app->request->post('period');
            $offset = Yii::$app->request->post('offset');
            $purse = Yii::$app->request->post('purse');
            $onPage = Yii::$app->request->post('onPage');

            $this->_savePeriodState($onPage, $period);

            return $this->renderPartial(self::VIEW . '_charts/_chart_plan_fact_days', [
                'customOffset' => $offset,
                'customPeriod' => $period,
                'customPurse' => $purse
            ]);
        }

        return null;
    }

    /**
     * @return string|null
     */
    public function actionGetBalanceData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('chart-balance-ajax')) {

            $year = Yii::$app->request->post('year');
            $offset = Yii::$app->request->post('offset');
            $article = Yii::$app->request->post('article');

            $model = new BalanceSearch(['company' => Yii::$app->user->identity->company]);
            $model->setYear($year ?? date('Y'));

            return $this->renderPartial(self::VIEW . '_charts/_chart_balance', [
                'customOffset' => $offset,
                'customArticle' => $article,
                'model' => $model
            ]);
        }

        return null;
    }

    /**
     * @return string|null
     */
    public function actionGetBalanceStructureData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('chart-balance-structure-ajax')) {

            $year = Yii::$app->request->post('year', date('Y'));
            $month = Yii::$app->request->post('month');

            $model = new BalanceSearch(['company' => Yii::$app->user->identity->company]);
            $model->year = $year;

            return $this->renderPartial(self::VIEW . '_charts/_chart_balance_structure', [
                'customMonth' => $month,
                'model' => $model
            ]);
        }

        return null;
    }

    /**
     * @return string|null
     * @throws Exception
     */
    public function actionGetExpenseChartsData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('chart-expense-ajax')) {

            $chart = Yii::$app->request->post('chart');
            $year = Yii::$app->request->post('year', date('Y'));
            $month = Yii::$app->request->post('month', date('m'));
            $period = Yii::$app->request->post('period');
            $offset = Yii::$app->request->post('offset');
            $purse = Yii::$app->request->post('purse');
            $article = Yii::$app->request->post('article');
            $client = Yii::$app->request->post('client');

            $model = new ExpensesSearch();
            $model->year = $year;

            switch ($chart) {
                case 'main':
                    $chartView = '_chart_expense_main';
                    break;
                case 'expense-income':
                    $chartView = '_chart_expense_income';
                    break;
                case 'expense-structure-1':
                    $chartView = '_chart_expense_structure_1';
                    break;
                case 'expense-structure-2':
                    $chartView = '_chart_expense_structure_2';
                    break;
                default:
                    throw new Exception('chart type not found');
            }

            return $this->renderPartial(self::VIEW . '_charts/' . $chartView, [
                'model' => $model,
                'customOffset' => $offset,
                'customPeriod' => $period,
                'customPurse' => $purse,
                'customMonth' => $month,
                'customArticle' => $article,
                'customClient' => $client
            ]);
        }

        return null;
    }

    /**
     * @return string|null
     * @throws Exception
     */
    public function actionGetIncomeChartsData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('chart-income-ajax')) {

            $chart = Yii::$app->request->post('chart');
            $year = Yii::$app->request->post('year', date('Y'));
            $month = Yii::$app->request->post('month', date('m'));
            $period = Yii::$app->request->post('period');
            $offset = Yii::$app->request->post('offset');
            $purse = Yii::$app->request->post('purse');
            $article = Yii::$app->request->post('article');
            $client = Yii::$app->request->post('client');

            $model = new IncomeSearch();
            $model->year = $year;

            switch ($chart) {
                case 'main':
                    $chartView = '_chart_income_main';
                    break;
                case 'income-structure-1':
                    $chartView = '_chart_income_structure_1';
                    break;
                case 'cash':
                    $chartView = '_chart_income_cash';
                    break;
                case 'top':
                    $chartView = '_chart_income_top';
                    break;
                default:
                    throw new Exception('chart type not found');
            }

            return $this->renderPartial(self::VIEW . '_charts/' . $chartView, [
                'model' => $model,
                'customOffset' => $offset,
                'customPeriod' => $period,
                'customPurse' => $purse,
                'customMonth' => $month,
                'customArticle' => $article,
                'customClient' => $client
            ]);
        }

        return null;
    }

    /**
     * @return string|null
     */
    public function actionGetProfitLossChartsData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('chart-profit-loss-ajax')) {

            $chartsGroup = Yii::$app->request->post('chartsGroup');
            $year = Yii::$app->request->post('year', date('Y'));
            $month = Yii::$app->request->post('month', date('m'));
            $offset = Yii::$app->request->post('offset');
            $amountKey = Yii::$app->request->post('amountKey', 'amount');

            $model = new ProfitAndLossSearchModel();
            $model->year = $year;

            // todo: cache years data
            $model->handleItems();

            return $this->renderPartial(self::VIEW . '_charts/_chart_profit_loss', [
                'customChartsGroup' => $chartsGroup,
                'customMonth' => $month,
                'customOffset' => $offset,
                'customAmountKey' => $amountKey,
                'model' => $model
            ]);
        }

        return null;
    }

    /**
     * @return string|null
     * @throws Exception
     */
    public function actionGetDebtorChartsData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('chart-debtor-ajax')) {

            $type = Yii::$app->request->post('type');

            if (!in_array($type, [Documents::IO_TYPE_IN, Documents::IO_TYPE_OUT]))
                throw new NotFoundHttpException('The requested page does not exist.');

            $chart = Yii::$app->request->post('chart');
            $year = Yii::$app->request->post('year', date('Y'));
            $period = Yii::$app->request->post('period');
            $offset = Yii::$app->request->post('offset');
            $debtorPeriod = Yii::$app->request->post('debtor-period');

            $model = new DebtReportSearch();
            $model->year = $year;

            switch ($chart) {
                case 'main':
                    $chartView = '_chart_debtor_main';
                    break;
                default:
                    throw new Exception('chart type not found');
            }

            return $this->renderPartial(self::VIEW . '_charts/' . $chartView, [
                'type' => $type,
                'model' => $model,
                'customOffset' => $offset,
                'customPeriod' => $period,
                'customDebtPeriod' => $debtorPeriod,
            ]);
        }

        return null;
    }

    /**
     * @param $onPage
     * @param $period
     */
    protected function _savePeriodState($onPage, $period)
    {
        if ($onPage == 'PK')
            $attr = 'report_pc_chart_period';
        elseif ($onPage == 'ODDS')
            $attr = 'report_odds_chart_period';
        else
            $attr = null;

        if ($attr) {
            /** @var Config $config */
            $config = Yii::$app->user->identity->config;

            if ($period == "days" && $config->$attr != 0)
                $config->updateAttributes([$attr => 0]);
            elseif ($period == "months" && $config->$attr != 1)
                $config->updateAttributes([$attr => 1]);
        }
    }

    /**
     * @param $tableName
     * @param $id
     * @return CashBankFlows|CashOrderFlows|CashEmoneyFlows
     * @throws NotFoundHttpException
     */
    private function findFlowItem($tableName, $id)
    {
        switch ($tableName) {
            case CashBankFlows::tableName():
                $className = CashBankFlows::class;
                break;
            case CashOrderFlows::tableName():
                $className = CashOrderFlows::class;
                break;
            case CashEmoneyFlows::tableName():
                $className = CashEmoneyFlows::class;
                break;
            case PlanCashFlows::tableName():
                $className = PlanCashFlows::class;
                break;
            default:
                throw new NotFoundHttpException('The requested page does not exist.');
        }
        $model = $className::find()
            ->andWhere(['id' => $id])
            ->andWhere(['company_id' => Yii::$app->user->identity->company_id])
            ->one();
        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $model;
    }

    /**
     * @param $tableName
     * @param $id
     * @return CashBankFlows|CashOrderFlows|CashEmoneyFlows
     * @throws NotFoundHttpException
     */
    private function findDocItem($tableName, $id)
    {
        switch ($tableName) {
            case Act::tableName():
                $className = Act::class;
                break;
            case PackingList::tableName():
                $className = PackingList::class;
                break;
            case Upd::tableName():
                $className = Upd::class;
                break;
            default:
                throw new NotFoundHttpException('The requested page does not exist.');
        }
        $model = $className::find()
            ->joinWith('invoice')
            ->andWhere(['invoice.company_id' => Yii::$app->user->identity->company_id])
            ->andWhere(["{$tableName}.id" => $id])
            ->one();
        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $model;
    }
}