<?php

namespace frontend\modules\reports\controllers;

use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\cash\query\CashFlowBase;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\document\PackingList;
use frontend\components\StatisticPeriod;
use frontend\models\ContractorSearch;
use frontend\modules\documents\models\InvoiceSearch;
use frontend\modules\reports\models\ContractorSimpleSearch;
use frontend\modules\reports\models\PaymentCalendarSearch;
use frontend\modules\reports\models\PlanCashContractor;
use frontend\modules\reports\models\PlanCashRule;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\db\Expression;
use yii\db\Query;
use frontend\components\FrontendController;
use common\models\Contractor;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class FinanceAjaxController
 */
class AutoPlanAjaxController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => 'common\components\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\Reports::VIEW,
                        ],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return array
     */
    public function actionAddContractor()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $planType = Yii::$app->request->get('planType', null);
        $flowType = Yii::$app->request->get('flowType', null);

        if ($planType === null || $flowType === null) {
            return ['success' => false, 'message' => 'not enought params'];
        }

        $contractorsArr = Yii::$app->request->post('Contractor');
        $amountByContractor = Yii::$app->request->post('amountByContractor', []);

        foreach ((array)$contractorsArr as $contractorId => $value) {

            /** @var Contractor $contractor */
            $contractor = Contractor::find()->where([
                'id' => $contractorId
            ])->andWhere(['OR',
                ['=',  'company_id', Yii::$app->user->identity->company_id],
                ['is', 'company_id', null], // global contractors (e.g. "Куб")
            ])->one();

            if ($contractor && $contractor->type == Contractor::TYPE_CUSTOMER) {
                if (null === $contractor->invoice_income_item_id) {
                    $contractor->invoice_income_item_id = InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER;
                    // $contractor->updateAttributes(['invoice_income_item_id']);
                }
                $itemId = $contractor->invoice_income_item_id;
                $paymentType = $this->getContractorPaymentType($contractor);
            } elseif ($contractor && $contractor->type == Contractor::TYPE_SELLER) {
                if (null === $contractor->invoice_expenditure_item_id) {
                    $contractor->invoice_expenditure_item_id = InvoiceExpenditureItem::ITEM_PRODUCT;
                    // $contractor->updateAttributes(['invoice_income_item_id']);
                }
                $itemId = $contractor->invoice_expenditure_item_id;
                $paymentType = PlanCashContractor::getPaymentType($contractor);
            } else {
                continue;
            }

            if (!$this->findModel($planType, $contractorId, $flowType, $paymentType, $itemId)) {
                $model = new PlanCashContractor([
                    'company_id' => Yii::$app->user->identity->company->id,
                    'contractor_id' => $contractor->id,
                    'payment_delay' => $contractor->type == Contractor::TYPE_SELLER ? $contractor->seller_payment_delay : $contractor->customer_payment_delay,
                    'plan_type' => $planType,
                    'flow_type' => $flowType,
                    'payment_type' => $paymentType,
                    'item_id' => $itemId,
                    'created_at' => time(),
                    'updated_at' => time(),
                ]);

                if ($planType == PlanCashContractor::PLAN_BY_PREVIOUS_FLOWS) {
                    if (isset($amountByContractor[$contractorId])) {
                        $model->amount = $amountByContractor[$contractorId];
                    }
                    $model->plan_date = (new PaymentCalendarSearch())->calculatePlanMonthDate($flowType, $paymentType = null, $itemID = null, $contractorID = null) ?: date('d.m.Y');
                    $model->end_plan_date = date('31.12.Y');
                }

                if (!$model->save()) {

                    $errors = [];
                    foreach ($model->getFirstErrors() as $e) {
                        $errors[] = $e;
                    }

                    return ['success' => false, 'message' => implode("<br/>", $errors), 'errors' => $model->getErrors()];
                }

                //////////////////////////
                /// CREATE AUTOPLAN FLOWS
                if ($model->plan_type == PlanCashContractor::PLAN_BY_PREVIOUS_FLOWS) {
                    $model->createRepeatedFlows();
                }
                if ($model->plan_type == PlanCashContractor::PLAN_BY_FUTURE_INVOICES) {
                    $model->createFlowsByActualInvoices();
                }
                /////////////////////////
            }
        }

        return ['success' => true];
    }

    public function actionUpdateRules()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        foreach ([CashFlowsBase::FLOW_TYPE_EXPENSE, CashFlowsBase::FLOW_TYPE_INCOME] as $flowType) {

            if ($flowType == CashFlowsBase::FLOW_TYPE_EXPENSE) {
                $ruleItemsIds = Yii::$app->request->post('expenditure_items_ids', []);
            } else {
                $ruleItemsIds = Yii::$app->request->post('income_items_ids', []);
            }

            // remove old
            $oldRuleItems = PlanCashRule::find()->where([
                'company_id' => Yii::$app->user->identity->company->id,
                'flow_type' => $flowType
            ])->all();
            foreach ($oldRuleItems as $oldRuleItem) {
                if (($flowType == CashFlowsBase::FLOW_TYPE_EXPENSE && !in_array($oldRuleItem->expenditure_item_id, $ruleItemsIds))
                ||  ($flowType == CashFlowsBase::FLOW_TYPE_INCOME && !in_array($oldRuleItem->income_item_id, $ruleItemsIds))) {
                    $oldRuleItem->deletePlanContractors();
                    $oldRuleItem->delete();
                }
            }

            // add new
            foreach ($ruleItemsIds as $itemId) {

                if (!($model = $this->findRuleModel($flowType, $itemId))) {
                    $model = new PlanCashRule([
                        'company_id' => Yii::$app->user->identity->company->id,
                        'flow_type' => $flowType,
                        'created_at' => time(),
                        'updated_at' => time()
                    ]);

                    if ($flowType == CashFlowsBase::FLOW_TYPE_EXPENSE)
                        $model->expenditure_item_id = $itemId;
                    else
                        $model->income_item_id = $itemId;

                    $errors = [];
                    foreach ($model->getFirstErrors() as $e) {
                        $errors[] = $e;
                    }

                    if (!$model->save()) {
                        return ['success' => false, 'message' => implode("<br/>", $errors), 'errors' => $model->getErrors()];
                    }
                }

                $model->addPlanContractors();
            }

        }

        return ['success' => true];
    }

    public function actionUpdateContractor($planType, $contractorId, $flowType, $paymentType, $itemId = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $paymentType = PlanCashContractor::normalizePaymentType($paymentType);

        /** @var $model PlanCashContractor */
        if (!$model = $this->findModel($planType, $contractorId, $flowType, $paymentType, $itemId)) {
            return ['success' => false, 'message' => 'Контрагент не найден'];
        }

        $model->load(Yii::$app->request->post());

        if ($model->save()) {

            //////////////////////////
            /// UPDATE AUTOPLAN FLOWS
            $model->updateRepeatedFlows();
            /////////////////////////

            return ['success' => true];
        } else {

            $errors = [];
            foreach ($model->getFirstErrors() as $e) {
                $errors[] = $e;
            }

            return ['success' => false, 'message' => implode("<br/>", $errors), 'errors' => $model->getErrors()];
        }
    }

    public function actionDeleteContractor($planType, $contractorId, $flowType, $paymentType, $itemId = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $paymentType = PlanCashContractor::normalizePaymentType($paymentType);

        /** @var $model PlanCashContractor */
        if (!$model = $this->findModel($planType, $contractorId, $flowType, $paymentType, $itemId)) {
            return ['success' => false, 'message' => 'Контрагент не найден'];
        }

        //////////////////////////
        /// DELETE AUTOPLAN FLOWS
        $model->deleteFlows();
        /////////////////////////

        if ($model->delete()) {
            return ['success' => true];
        } else {

            return ['success' => false, 'message' => 'Ошибка удаления', 'errors' => $model->getErrors()];
        }
    }

    protected function findModel($planType, $contractorId, $flowType, $paymentType, $itemId)
    {
        /* @var PackingList $model */
        $query = PlanCashContractor::find()
            ->where(['company_id' => Yii::$app->user->identity->company->id])
            ->andWhere(['contractor_id' => $contractorId])
            ->andWhere(['plan_type' => $planType])
            ->andWhere(['flow_type' => $flowType])
            ->andWhere(['payment_type' => $paymentType])
            ->andWhere(['item_id' => $itemId]);

        $models = $query->all();

        if (!$models || count($models) > 1)
            return false;

        return $models[0];
    }

    protected function findRuleModel($flowType, $itemId)
    {
        /* @var PackingList $model */
        $query = PlanCashRule::find()
            ->where(['company_id' => Yii::$app->user->identity->company->id])
            ->andWhere(['flow_type' => $flowType]);

        if ($flowType == CashFlowsBase::FLOW_TYPE_EXPENSE) {
            $query->andWhere(['expenditure_item_id' => $itemId]);
        }
        elseif ($flowType == CashFlowsBase::FLOW_TYPE_INCOME) {
            $query->andWhere(['income_item_id' => $itemId]);
        }

        $models = $query->all();

        if (!$models || count($models) > 1)
            return false;

        return $models[0];
    }

    protected function getContractorPaymentType(Contractor $model)
    {
        $bankDate = (int)CashBankFlows::find()->where(['contractor_id' => $model->id])->orderBy(['date' => SORT_DESC])->select('UNIX_TIMESTAMP(date)')->scalar();
        $orderDate = (int)CashOrderFlows::find()->where(['contractor_id' => $model->id])->orderBy(['date' => SORT_DESC])->select('UNIX_TIMESTAMP(date)')->scalar();
        $emoneyDate = (int)CashEmoneyFlows::find()->where(['contractor_id' => $model->id])->orderBy(['date' => SORT_DESC])->select('UNIX_TIMESTAMP(date)')->scalar();

        if ($bankDate >= $orderDate && $bankDate >= $emoneyDate)
            return PlanCashContractor::PAYMENT_TYPE_BANK;
        if ($orderDate >= $bankDate && $orderDate >= $emoneyDate)
            return PlanCashContractor::PAYMENT_TYPE_ORDER;
        if ($emoneyDate >= $bankDate && $emoneyDate >= $orderDate)
            return PlanCashContractor::PAYMENT_TYPE_EMONEY;

        return PlanCashContractor::PAYMENT_TYPE_BANK;
    }
}