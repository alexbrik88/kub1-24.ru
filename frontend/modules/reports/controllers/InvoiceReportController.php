<?php

namespace frontend\modules\reports\controllers;

use common\components\filters\AccessControl;
use common\models\document\Invoice;
use common\models\document\InvoiceInToInvoiceOut;
use common\models\document\status\InvoiceStatus;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\models\InvoiceSearch;
use frontend\modules\reports\components\DebtsHelper;
use frontend\modules\reports\models\InvoiceReportSearch;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Connection;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use common\models\Company;
use yii\web\Response;

/**
 * TemplateController implements the CRUD actions for Template model.
 */
class InvoiceReportController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\Reports::REPORTS_INVOICES,
                        ],
                    ],
                ],
            ],
        ]);
    }

    public function actionCreated()
    {
        $searchModel = new InvoiceReportSearch([
            'company_id' => Yii::$app->user->identity->company->id,
            'dateRange' => StatisticPeriod::getSessionPeriod(),
            'type' => Documents::IO_TYPE_OUT,
        ]);
        $dataProvider = $searchModel->created(\Yii::$app->request->get());

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        \common\models\company\CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 54);

        return $this->render('created', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPayed()
    {
        $searchModel = new InvoiceReportSearch([
            'company_id' => Yii::$app->user->identity->company->id,
            'dateRange' => StatisticPeriod::getSessionPeriod(),
            'type' => Documents::IO_TYPE_OUT,
        ]);
        $dataProvider = $searchModel->payed(\Yii::$app->request->get());

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('payed', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionNotPayed()
    {
        $searchModel = new InvoiceReportSearch([
            'company_id' => Yii::$app->user->identity->company->id,
            'dateRange' => StatisticPeriod::getSessionPeriod(),
            'type' => Documents::IO_TYPE_OUT,
        ]);
        $dataProvider = $searchModel->notPayed(\Yii::$app->request->get());

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('not-payed', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionNewClients()
    {
        $searchModel = new InvoiceReportSearch();
        $dataProvider = $searchModel->newClients(StatisticPeriod::getSessionPeriod(), Yii::$app->request->get());
        $newClients = DebtsHelper::getNewClients(StatisticPeriod::getSessionPeriod());
        $allSum = $newClients['all'];
        unset($newClients['all']);

        return $this->render('new-clients', [
            'newClients' => $newClients,
            'allSum' => $allSum,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLinked()
    {
        $searchModel = new InvoiceReportSearch([
            'company_id' => Yii::$app->user->identity->company->id,
            'dateRange' => StatisticPeriod::getSessionPeriod(),
            'type' => Documents::IO_TYPE_OUT,
        ]);
        $dataProvider = $searchModel->linked(\Yii::$app->request->get());
        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('linked', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLinkedInvoicesList()
    {
        $this->layout = '@frontend/views/layouts/empty';
        $company = Yii::$app->user->identity->company;
        // table filters
        $contractorId = Yii::$app->request->post('contractor_id', null);
        $documentNumber = Yii::$app->request->post('documentNumber', null);
        // link data
        $step = Yii::$app->request->get('step', Yii::$app->request->post('step'));
        $linkNumber = Yii::$app->request->post('linkNumber', null);
        $inInvoices = Yii::$app->request->post('inInvoices', []);
        $outInvoices = Yii::$app->request->post('outInvoices', []);
        $dateRange = StatisticPeriod::getSessionPeriod();

        $excludeIds = InvoiceInToInvoiceOut::find()
            ->alias('i2o')
            ->select('i2o.invoice_id')
            ->leftJoin(['i' => 'invoice'], 'i2o.invoice_id = i.id AND i2o.company_id = ' . $company->id)
            ->where(['i2o.company_id' => $company->id])
            ->andWhere([
                'i.type' => ($step > 1) ? Documents::IO_TYPE_IN : Documents::IO_TYPE_OUT,
                'i.is_deleted' => 0
            ])
            ->andFilterWhere(['not', ['i2o.number' => $linkNumber]])
            ->asArray()
            ->column();

        $searchModel = new InvoiceSearch([
            'company_id' => $company->id,
            'type' => ($step > 1) ? Documents::IO_TYPE_IN : Documents::IO_TYPE_OUT,
            'excludeIds' => $excludeIds
        ]);

        $searchModel->contractor_id = $contractorId;
        $searchModel->document_number = $documentNumber;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $dateRange);

        return $this->renderAjax('linked/_modal_invoices', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'documentNumber' => $documentNumber,
            'step' => $step,
            'linkNumber' => $linkNumber,
            'inInvoices' => $inInvoices,
            'outInvoices' => $outInvoices,
        ]);
    }

    public function actionCreateLinkInOutInvoices()
    {
        $company = Yii::$app->user->identity->company;
        $linkNumber = Yii::$app->request->post('linkNumber', null);
        $inInvoices = Yii::$app->request->post('inInvoices', []);
        $outInvoices = Yii::$app->request->post('outInvoices', []);

        if (empty($inInvoices) || empty($outInvoices)) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'success' => 0,
                'message' => 'Счета не найдены'
            ];
        }

        $isLinked = Yii::$app->db->transaction(function (Connection $db) use ($company, $linkNumber, $outInvoices, $inInvoices) {
            if ($linkNumber) {
                InvoiceInToInvoiceOut::deleteAll([
                    'company_id' => $company->id,
                    'number' => $linkNumber
                ]);
            }

            return InvoiceInToInvoiceOut::linkInvoices($company->id, $outInvoices, $inInvoices);
        });

        if ($isLinked) {
            Yii::$app->session->setFlash('success', 'Сохранено');
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось сохранить');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionDeleteLinkInOutInvoices($linkNumber)
    {
        $company = Yii::$app->user->identity->company;

        $models = InvoiceInToInvoiceOut::findAll([
            'company_id' => $company->id,
            'number' => (int)$linkNumber
        ]);

        $isDeleted = !count($models);

        foreach ($models as $model)
            $isDeleted = $model->delete();

        if ($isDeleted) {
            Yii::$app->session->setFlash('success', 'Удалено');
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось удалить');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionSaveLinkedTableState()
    {
        $ret = 0;
        $target = Yii::$app->request->post('target');
        $active = Yii::$app->request->post('active');

        if ($target === 'in' && $active !== null) {
            Yii::$app->session->set('report.invoice.linked.button.in', (int)$active);
            $ret = 1;
        }
        if ($target === 'out' && $active !== null) {
            Yii::$app->session->set('report.invoice.linked.button.out', (int)$active);
            $ret = 1;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $ret;
    }
}
