<?php

namespace frontend\modules\reports\controllers;

use common\components\filters\AccessControl;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\components\PageSize;
use frontend\components\StatisticPeriod;
use frontend\modules\reports\models\DisciplineSearch;
use frontend\rbac\UserRole;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * DisciplineisController
 */
class DisciplineController extends \frontend\components\FrontendController
{
    public $layout = 'debt-report';

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\Reports::REPORTS_CLIENTS,
                        ],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return mixed
     */
    public function actionIndex($period = DisciplineSearch::DEFAULT_PERIOD)
    {
        $searchModel = new DisciplineSearch([
            'company' => Yii::$app->user->identity->company,
            'period' => $period,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        $dataProvider->pagination->pageSize = PageSize::get();

        \common\models\company\CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 59);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
