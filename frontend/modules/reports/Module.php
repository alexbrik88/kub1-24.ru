<?php

namespace frontend\modules\reports;

use Yii;

/**
 * reports module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\reports\controllers';
    /**
     * @inheritdoc
     */
    public $layout = 'invoice-report';

    /**
     * {@inheritdoc}
     */
    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        if (Yii::$app->view->theme === null) {
            Yii::$app->params['bsVersion'] = '4';
            $bandle = Yii::$app->assetManager->getBundle('frontend\themes\kub\assets\KubAsset');
            Yii::$app->params['kubAssetBaseUrl'] = $bandle->baseUrl;
            Yii::$app->params['kubAssetBasePath'] = $bandle->basePath;
            Yii::$app->view->theme = Yii::createObject([
                'class' => 'yii\base\Theme',
                'basePath' => '@frontend/themes/kub',
                'baseUrl' => '@web/themes/kub',
                'pathMap' => [
                    '@common/models/file/widgets/views' => '@frontend/themes/kub/file',
                    '@frontend/views' => '@frontend/themes/kub/views',
                    '@frontend/modules' => '@frontend/themes/kub/modules',
                    '@frontend/widgets' => '@frontend/themes/kub/widgets',
                ],
            ]);

            $user = Yii::$app->user->identity;
            if ($user->is_old_kub_theme) {
                $user->updateAttributes(['is_old_kub_theme' => 0]);
            }
        }

        return true;
    }
}
