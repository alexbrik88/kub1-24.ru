<?php

use common\components\date\DateHelper;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\document\Invoice;
use frontend\modules\documents\components\FilterHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\document\status\InvoiceStatus;
use frontend\widgets\RangeButtonWidget;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\reports\models\InvoiceReportSearch */

$this->title = 'Отчет по долгам';

$statisticItemsArray = $searchModel->notPayedStatistic();
$employeeFilterItems = ArrayHelper::map($statisticItemsArray, 'id', function ($row) {
    return $row['lastname'] . ' ' . mb_substr($row['firstname'], 0, 1) . '.' . mb_substr($row['patronymic'], 0, 1) . '.';
});

$sumDebtStart = 0;
$sumCreated = 0;
$sumPayed = 0;
$sumDebtEnd = 0;
?>

<div class="row">
    <div class="col-md-3 col-md-push-9">
        <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
    </div>
    <div class="col-md-9 col-md-pull-3 created-scroll">
        <table class="table table-striped table-bordered table-hover">
            <tr class="heading">
                <th style="width: 20%;">Сотрудник</th>
                <th style="width: 18%;">Долг на начало периода</th>
                <th style="width: 18%;">Долги, оплаченные за период</th>
                <th style="width: 18%;">Счета, выставленные за период, но не оплаченные в нем</th>
                <th style="width: 18%;">Долг на конец периода</th>
                <th style="width: 8%;">%%</th>
            </tr>
            <?php if (!empty($searchModel->getEmployees())) : ?>
                <?php foreach ($searchModel->getEmployees() as $employee): ?>
                    <tr>
                        <td><?= $employee['lastname'] ?> <?= mb_substr($employee['firstname'], 0, 1) ?>
                            .<?= mb_substr($employee['patronymic'], 0, 1) ?>.
                        </td>
                        <td><?= TextHelper::invoiceMoneyFormat($searchModel->getDebtStartAmount($employee['document_author_id']), 2) ?></td>
                        <td><?= TextHelper::invoiceMoneyFormat($searchModel->getPayedAmount($employee['document_author_id']), 2) ?></td>
                        <td><?= TextHelper::invoiceMoneyFormat($searchModel->getCreatedAmount($employee['document_author_id']), 2) ?></td>
                        <td><?= TextHelper::invoiceMoneyFormat($searchModel->getDebtEndAmount($employee['document_author_id']), 2) ?></td>
                        <td><?= $searchModel->getPercentDebtEndAmount($employee['document_author_id']) ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td><b>Итого</b></td>
                    <td><b><?= TextHelper::invoiceMoneyFormat($searchModel->getTotalDebtStartAmount(), 2) ?></b></td>
                    <td><b><?= TextHelper::invoiceMoneyFormat($searchModel->getTotalPayedAmount(), 2) ?></b></td>
                    <td><b><?= TextHelper::invoiceMoneyFormat($searchModel->getTotalCreatedAmount(), 2) ?></b></td>
                    <td><b><?= TextHelper::invoiceMoneyFormat($searchModel->getTotalDebtEndAmount(), 2) ?></b></td>
                    <td><b>100</b></td>
                </tr>
            <?php else : ?>
                <tr>
                    <td>--/--</td>
                    <td>0,00</td>
                    <td>0,00</td>
                    <td>0,00</td>
                    <td>0,00</td>
                    <td>0,00</td>
                </tr>
            <?php endif; ?>
        </table>
    </div>
</div>

<div class="portlet box darkblue blk_wth_srch">
    <div class="portlet-title row-fluid">
        <div class="caption list_recip col-md-3 col-sm-3">
            Список счетов
        </div>
    </div>

    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable status_nowrap overfl_text_hid invoice-table',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => (Yii::$app->controller->id === 'default') ? "{items}\n{pager}" : $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'columns' => [
                    [
                        'attribute' => 'contractor_id',
                        'label' => 'Контрагент',
                        'class' => DropDownSearchDataColumn::className(),
                        'enableSorting' => false,
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '20%',
                        ],
                        'filter' => FilterHelper::getContractorList($searchModel->type, Invoice::tableName(), true, false, false),
                        'format' => 'raw',
                        'value' => 'contractor_name_short',
                    ],

                    [
                        'attribute' => 'document_date',
                        'label' => 'Дата счёта',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '12%',
                        ],
                        'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
                    ],
                    [
                        'attribute' => 'document_number',
                        'label' => '№ счёта',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '12%',
                        ],
                        'format' => 'raw',
                        'value' => function (Invoice $data) {
                            return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                                'model' => $data,
                            ])
                                ? Html::a($data->fullNumber, ['/documents/invoice/view',
                                    'type' => $data->type,
                                    'id' => $data->id,
                                    'contractorId' => $data->contractor_id,
                                ])
                                : $data->fullNumber;
                        },
                    ],
                    [
                        'label' => 'Сумма',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '12%',
                        ],
                        'attribute' => 'total_amount_with_nds',
                        'format' => 'raw',
                        'value' => function (Invoice $model) {
                            $amount = '<span>' . TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2) . '</span>';
                            if ($model->invoice_status_id == InvoiceStatus::STATUS_PAYED_PARTIAL) {
                                $amount .= ' / <span style="color: #45b6af;" title="Оплаченная сумма">' . TextHelper::invoiceMoneyFormat($model->payment_partial_amount, 2) . '</span>';
                            } elseif ($model->invoice_status_id == InvoiceStatus::STATUS_OVERDUE && $model->remaining_amount !== null) {
                                $amount .= ' / <span style="color: #f3565d;" title="Неоплаченная сумма">' . TextHelper::invoiceMoneyFormat($model->remaining_amount, 2) . '</span>';
                            }

                            return $amount;
                        },
                    ],
                    [
                        'label' => 'Статус',
                        'class' => DropDownDataColumn::className(),
                        'attribute' => 'invoice_status_id',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '14%',
                        ],
                        'filter' => $searchModel->getStatusItemsByQuery($dataProvider->query),
                        'format' => 'text',
                        'value' => function (Invoice $model) {
                            return $model->invoiceStatus->name;
                        },
                    ],
                    [
                        'label' => 'Выставил счет',
                        'class' => DropDownSearchDataColumn::className(),
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '20%',
                        ],
                        'attribute' => 'document_author_id',
                        'filter' => ['' => 'Все'] + $employeeFilterItems,
                        'value' => function (Invoice $model) {
                            return $model->documentAuthor->getFio(true);
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
