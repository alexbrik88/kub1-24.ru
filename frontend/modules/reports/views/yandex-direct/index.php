<?php
/**
 * @var $this yii\web\View
 * @var $user \common\models\employee\Employee
 * @var $searchModel \frontend\models\yandexDirect\YandexDirectCampaignReportSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $averageCost double
 * @var $totalImpressionsCount int
 * @var $totalClicksCount int
 * @var $averageCtr double
 * @var $totalCost double
 * @var $averageClickCost double
 * @var $averageAvgPageViewsCount double
 * @var $averageConversionRate double
 * @var $averageCostPerConversion double
 * @var $totalConversionsCount int|null
 * @var $averageGoalsRoi double|null
 * @var $totalRevenue double
 */

use common\components\grid\DropDownSearchDataColumn;
use common\widgets\Modal;
use yii\bootstrap\Html;
use common\components\date\DateHelper;
use frontend\widgets\RangeButtonWidget;
use common\components\grid\GridView;
use common\models\yandex\YandexDirectCampaignReport;
use frontend\components\StatisticPeriod;
use yii\helpers\Url;
use yii\widgets\Pjax;
use Carbon\Carbon;

/**
 * @var $lastOperation \common\models\integration\LastOperationInIntegration|null
 * @var $accessToken string|null
 * @var $balanceAmount int
 */

$this->title = 'Yandex Direct';

$userConfig = $user->config;
$tabConfigClass = [
    'status' => 'col_yandex_direct_status' . ($userConfig->yandex_direct_status ? '' : ' hidden'),
    'strategy' => 'col_yandex_direct_strategy' . ($userConfig->yandex_direct_strategy ? '' : ' hidden'),
    'placement' => 'col_yandex_direct_placement' . ($userConfig->yandex_direct_placement ? '' : ' hidden'),
    'budget' => 'col_yandex_direct_budget' . ($userConfig->yandex_direct_budget ? '' : ' hidden'),
];
$lastOperationDate = null;
if ($lastOperation !== null) {
    $lastOperationDate = Carbon::createFromTimestamp($lastOperation->date)->format('Данные загружены d.m.Y в H:i');
}
?>
<p><?= Html::a('Назад к списку', Url::to(['/integration'])); ?></p>
<div class="portlet box">
    <div class="pull-right">
        <h3 class="page-title">Баланс (без НДС): <span><?= TextHelper::numberFormat($balanceAmount) ?> ₽</span></h3>
    </div>
    <h3 class="page-title yd-title"><img src="/images/yandex_direct_logo.png" width="143"/></h3 class="page-title yd-title">

    <?php if ($lastOperationDate !== null): ?>
        <div class="yd-statement-refresh" style="display:inline-block;padding-top:10px;">
            <?= Html::tag('span', $lastOperationDate, [
                'style' => 'display:inline-block; margin-left: 10px; font-size: 13px;font-weight: 400;letter-spacing: normal; cursor: pointer;',
            ]); ?>
            <?= Html::a('<i class="icon-refresh" style="font-size:20px;"></i>', null, [
                'class' => 'upload-yandex-direct',
                'style' => 'padding: 10px; display: block; float: right; margin-top: -12px;',
                'data' => [
                    'url' => Url::to(['/integration/yandex-direct/upload']),
                ],
            ]) ?>
        </div>
    <?php endif; ?>

    <div class="row" style="margin-top: 20px;margin-bottom: 25px;">
        <div class="col-md-9 col-sm-9"></div>
        <div class="col-md-3 col-sm-3">
            <?php if ($accessToken === null): ?>
                <?= Html::a('<i class="fa"></i> Настроить интеграцию', Yii::$app->yandexDirect->getAuthorizationUrl(), [
                    'class' => 'btn green-haze w100proc no-padding',
                ]) ?>
            <?php else: ?>
                <?= Html::a('<i class="fa fa-download"></i> Загрузить выписку', null, [
                    'class' => 'btn yellow w100proc no-padding upload-yandex-direct',
                    'data' => [
                        'url' => Url::to(['/integration/yandex-direct/upload']),
                    ],
                ]) ?>
            <?php endif; ?>
        </div>
        <div class="col-md-3 col-sm-3"></div>
        <div class="col-md-3 col-sm-3"></div>
        <div class="col-md-3 col-sm-3">
            <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-bordered table-hover" style="width: auto;">
                <tbody>
                <tr role="row">
                    <td class="text-left" style="font-weight: 600;">Всего</td>
                    <td class="text-left" style="font-weight: 600;">Ср. расход за день (руб.)</td>
                    <td class="text-left" style="font-weight: 600;">Показы</td>
                    <td class="text-left" style="font-weight: 600;">Клики</td>
                    <td class="text-left" style="font-weight: 600;">CTR (%)</td>
                    <td class="text-left" style="font-weight: 600;">Расход (руб.)</td>
                    <td class="text-left" style="font-weight: 600;">Ср. цена клика (руб.)</td>
                    <td class="text-left" style="font-weight: 600;">Глубина (стр.)</td>
                    <td class="text-left" style="font-weight: 600;">Конверсия (%)</td>
                    <td class="text-left" style="font-weight: 600;">Цена цели (руб.)</td>
                    <td class="text-left" style="font-weight: 600;">Конверсии</td>
                    <td class="text-left" style="font-weight: 600;">Рентабельность</td>
                    <td class="text-left" style="font-weight: 600;">Доход</td>
                </tr>
                <tr>
                    <td><?= StatisticPeriod::getSessionName() ?></td>
                    <td><?= $averageCost ?></td>
                    <td><?= $totalImpressionsCount ?></td>
                    <td><?= $totalClicksCount ?></td>
                    <td><?= $averageCtr ?></td>
                    <td><?= $totalCost ?></td>
                    <td><?= $averageClickCost ?></td>
                    <td><?= $averageAvgPageViewsCount ?></td>
                    <td><?= $averageConversionRate ?></td>
                    <td><?= $averageCostPerConversion ?></td>
                    <td><?= $totalConversionsCount ?: '--'; ?></td>
                    <td><?= $averageGoalsRoi ?></td>
                    <td><?= $totalRevenue ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="portlet box darkblue">
        <div class="portlet-title"></div>
        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable customers_table fix-thead',
                        'id' => 'datatable_ajax',
                        'aria-describedby' => 'datatable_ajax_info',
                        'role' => 'grid',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    'options' => [
                        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                    'columns' => [
                        [
                            'attribute' => 'date',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (YandexDirectCampaignReport $report) {
                                return Html::a($report->campaign->name, Url::to(['view-campaign', 'id' => $report->campaign_id]));
                            },
                            'class' => DropDownSearchDataColumn::class,
                            'filter' => $searchModel->getCampaignFilterList(),
                        ],
                        [
                            'attribute' => 'status',
                            'label' => 'Статус',
                            'format' => 'raw',
                            'value' => function (YandexDirectCampaignReport $report) {
                                return $report->campaign->status ?: '--';
                            },
                            'headerOptions' => [
                                'class' => $tabConfigClass['status'],
                            ],
                            'contentOptions' => [
                                'class' => $tabConfigClass['status'],
                            ],
                            'class' => DropDownSearchDataColumn::class,
                            'filter' => $searchModel->getStatusFilterList(),
                        ],
                        [
                            'attribute' => 'strategy',
                            'label' => 'Стратегия',
                            'format' => 'raw',
                            'value' => function (YandexDirectCampaignReport $report) {
                                return $report->campaign->strategy ?: '--';
                            },
                            'headerOptions' => [
                                'class' => $tabConfigClass['strategy'],
                            ],
                            'contentOptions' => [
                                'class' => $tabConfigClass['strategy'],
                            ],
                            'class' => DropDownSearchDataColumn::class,
                            'filter' => $searchModel->getStrategyFilterList(),
                        ],
                        [
                            'attribute' => 'showingPlace',
                            'label' => 'Место показов',
                            'format' => 'raw',
                            'value' => function (YandexDirectCampaignReport $report) {
                                return $report->campaign->showing_place ?: '--';
                            },
                            'headerOptions' => [
                                'class' => $tabConfigClass['placement'],
                            ],
                            'contentOptions' => [
                                'class' => $tabConfigClass['placement'],
                            ],
                            'class' => DropDownSearchDataColumn::class,
                            'filter' => $searchModel->getPlacementFilterList(),
                        ],
                        [
                            'attribute' => 'daily_budget_amount',
                            'label' => 'Бюджет, ₽',
                            'headerOptions' => [
                                'class' => 'sorting ' . $tabConfigClass['budget'],
                            ],
                            'contentOptions' => [
                                'class' => $tabConfigClass['budget'],
                            ],
                            'format' => 'raw',
                            'value' => function (YandexDirectCampaignReport $report) {
                                return TextHelper::numberFormat($report->campaign->daily_budget_amount ?: 0);
                            },
                        ],
                        [
                            'attribute' => 'cost',
                            'label' => 'Расход, ₽',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (YandexDirectCampaignReport $report) {
                                return $report->campaign_name;
                            },
                        ],
                        [
                            'attribute' => 'impressions_count',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (YandexDirectCampaignReport $report) {
                                return $report->impressions_count;
                            },
                        ],
                        [
                            'attribute' => 'clicks_count',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (YandexDirectCampaignReport $report) {
                                return $report->clicks_count;
                            },
                        ],
                        [
                            'attribute' => 'ctr',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (YandexDirectCampaignReport $report) {
                                return $report->ctr;
                            },
                        ],
                        [
                            'attribute' => 'cost',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (YandexDirectCampaignReport $report) {
                                return $report->cost;
                            },
                        ],
                        [
                            'attribute' => 'avg_cpc',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (YandexDirectCampaignReport $report) {
                                return $report->avg_cpc ?: '--';
                            },
                        ],
                        [
                            'attribute' => 'avg_page_views_count',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (YandexDirectCampaignReport $report) {
                                return $report->avg_page_views_count ?: '--';
                            },
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
<?php Modal::begin([
    'id' => 'upload-yandex-direct-modal',
    'header' => '<h1>Запрос выписки из Yandex Direct</h1>',
]);
Pjax::begin([
    'id' => 'upload-yandex-direct-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]);
Pjax::end();
Modal::end();