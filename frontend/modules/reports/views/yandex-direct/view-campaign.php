<?php
/**
 * @var $this yii\web\View
 * @var $user \common\models\employee\Employee
 * @var $searchModel \frontend\models\yandexDirect\YandexDirectAdGroupReportSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $campaign \common\models\yandex\YandexDirectCampaignReport;
 * @var $ad_group \common\models\yandex\YandexDirectAdGroupReport;
 */

use Carbon\Carbon;
use common\components\grid\DropDownSearchDataColumn;
use common\models\employee\Employee;
use common\widgets\Modal;
use yii\bootstrap\Html;
use frontend\widgets\RangeButtonWidget;
use common\components\grid\GridView;
use common\models\yandex\YandexDirectAdGroupReport;
use common\components\TextHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->title = 'Yandex Direct';

$lastOperationDate = $lastOperation
    ? Carbon::createFromTimestamp($lastOperation->date)->format('Данные загружены d.m.Y в H:i')
    : null;
?>
<p><?= Html::a('Назад к списку', Url::to(['/integration'])); ?></p>
<div class="portlet box">
    <div class="pull-right">
        <h3 class="page-title">Баланс (без НДС): <span>0 ₽</span></h3>
    </div>
    <h3 class="page-title yd-title"><img src="/images/yandex_direct_logo.png" width="143"/></h3 class="page-title yd-title">

    <?php if ($lastOperationDate !== null): ?>
        <div class="yd-statement-refresh" style="display:inline-block;padding-top:10px;">
            <?= Html::tag('span', $lastOperationDate, [
                'style' => 'display:inline-block; margin-left: 10px; font-size: 13px;font-weight: 400;letter-spacing: normal; cursor: pointer;',
            ]); ?>
            <?= Html::a('<i class="icon-refresh" style="font-size:20px;"></i>', 'javascript:void(0)', [
                'onclick' => new \yii\web\JsExpression('$("#refresh_data_form").submit()'),
                'style' => 'padding: 10px; display: block; float: right; margin-top: -12px;',
            ]) ?>
        </div>

        <?php
        $form = ActiveForm::begin([
            'id' => 'refresh_data_form',
            'method' => 'POST',
            'action' => '/integration/yandex-direct/upload',
            'fieldConfig' => ['options' => ['class' => '']],
        ]);
        echo Html::hiddenInput('GetReportForm[dateFrom]', Carbon::createFromFormat('Y-m-d', $searchModel->maxDate)->format('d.m.Y'));
        echo Html::hiddenInput('GetReportForm[dateTo]', date('d.m.Y'));
        $form->end();
        ?>

    <?php endif; ?>

    <div class="row" style="margin-top: 20px;margin-bottom: 25px;">
        <div class="col-md-9 col-sm-9">
            <a href="index">Все кампании</a> >
            <span><?= $campaign->campaign->name ?></span>
        </div>
        <div class="col-md-3 col-sm-3">
            <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-bordered table-hover" style="width: auto;">
                <tbody>
                <tr role="row">
                    <td class="text-left" style="font-weight: 600;">Стратегия</td>
                    <td class="text-left" style="font-weight: 600;">Место показов</td>
                    <td class="text-left" style="font-weight: 600;">Бюджет</td>
                </tr>
                <tr>
                    <td><?= $campaign->campaign->strategy ?: '--' ?></td>
                    <td><?= $campaign->campaign->showing_place ?: '--' ?></td>
                    <td><?= TextHelper::numberFormat($campaign->campaign->daily_budget_amount ?: 0) ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="portlet box darkblue">
        <div class="portlet-title">
            <div class="caption">
                Группы объявлений: <?= $dataProvider->totalCount ?>
            </div>
        </div>
        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable customers_table fix-thead',
                        'id' => 'datatable_ajax',
                        'aria-describedby' => 'datatable_ajax_info',
                        'role' => 'grid',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    'options' => [
                        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                    'columns' => [
                        [
                            'attribute' => 'ad_group_id',
                            'label' => 'Название',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'contentOptions' => [
                                'class' => 'text-left text-ellipsis',
                            ],
                            'format' => 'raw',
                            'value' => function (YandexDirectAdGroupReport $report) {
                                return Html::a($report->adGroup->name, Url::to(['view-group', 'id' => $report->ad_group_id]));
                            },
                            'class' => DropDownSearchDataColumn::class,
                            'filter' => $searchModel->getAdGroupFilterList(),
                        ],
                        [
                            'attribute' => 'status',
                            'label' => 'Статус',
                            'format' => 'raw',
                            'value' => function (YandexDirectAdGroupReport $report) {
                                return $report->adGroup->status ?: '--';
                            },
                            'class' => DropDownSearchDataColumn::class,
                            'filter' => $searchModel->getStatusFilterList(),
                        ],
                        [
                            'attribute' => 'cost',
                            'label' => 'Расход, ₽',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'contentOptions' => [
                                'class' => 'text-right',
                            ],
                            'format' => 'raw',
                            'value' => function (YandexDirectAdGroupReport $report) {
                                return TextHelper::numberFormat($report->cost);
                            },
                        ],
                        [
                            'attribute' => 'impressions_count',
                            'label' => 'Показы',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'contentOptions' => [
                                'class' => 'text-right',
                            ],
                            'format' => 'raw',
                            'value' => function (YandexDirectAdGroupReport $report) {
                                return TextHelper::numberFormat($report->impressions_count);
                            },
                        ],
                        [
                            'attribute' => 'clicks_count',
                            'label' => 'Клики',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'contentOptions' => [
                                'class' => 'text-right',
                            ],
                            'format' => 'raw',
                            'value' => function (YandexDirectAdGroupReport $report) {
                                return TextHelper::numberFormat($report->clicks_count);
                            },
                        ],
                        [
                            'attribute' => 'ctr',
                            'label' => 'CTR (%)',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'contentOptions' => [
                                'class' => 'text-right',
                            ],
                            'format' => 'raw',
                            'value' => function (YandexDirectAdGroupReport $report) {
                                return TextHelper::numberFormat($report->ctr);
                            },
                        ],
                        [
                            'attribute' => 'avg_cpc',
                            'label' => 'Ср. цена клика, ₽',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'contentOptions' => [
                                'class' => 'text-right',
                            ],
                            'format' => 'raw',
                            'value' => function (YandexDirectAdGroupReport $report) {
                                return TextHelper::numberFormat($report->avg_cpc ?: 0);
                            },
                        ],
                        [
                            'attribute' => 'conversions_count',
                            'label' => 'Конверсии',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'contentOptions' => [
                                'class' => 'text-right',
                            ],
                            'format' => 'raw',
                            'value' => function (YandexDirectAdGroupReport $report) {
                                return TextHelper::numberFormat($report->conversions_count ?: 0);
                            },
                        ],
                        [
                            'attribute' => 'conversion_rate',
                            'label' => 'Конверсия (%)',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'contentOptions' => [
                                'class' => 'text-right',
                            ],
                            'format' => 'raw',
                            'value' => function (YandexDirectAdGroupReport $report) {
                                return TextHelper::numberFormat($report->conversion_rate ?: 0);
                            },
                        ],
                        [
                            'attribute' => 'goals_roi',
                            'label' => 'Цена цели, ₽',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'contentOptions' => [
                                'class' => 'text-right',
                            ],
                            'format' => 'raw',
                            'value' => function (YandexDirectAdGroupReport $report) {
                                return TextHelper::numberFormat($report->goals_roi ?: 0);
                            },
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
<?php Modal::begin([
    'id' => 'upload-yandex-direct-modal',
    'header' => '<h1>Запрос выписки из Yandex Direct</h1>',
]);
Pjax::begin([
    'id' => 'upload-yandex-direct-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]);
Pjax::end();
Modal::end();