<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 05.07.2018
 * Time: 12:08
 */

use common\widgets\Modal;
use common\models\Company;
use common\components\ImageHelper;
use frontend\rbac\UserRole;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\helpers\Url;

$this->beginContent('@frontend/views/layouts/main.php');
$company = Yii::$app->user->identity->company;
?>
    <div class="debt-report-content container-fluid" style="padding: 0; margin-top: -10px;">
        <?php NavBar::begin([
            'options' => [
                'class' => 'navbar-report navbar-default',
            ],
            'brandOptions' => [
                'style' => 'margin-left: 0;'
            ],
            'containerOptions' => [
                'style' => 'padding: 0;'
            ],
            'innerContainerOptions' => [
                'class' => 'container-fluid',
                'style' => 'padding: 0;'
            ],
        ]);
        echo Nav::widget([
            'id' => 'debt-report-menu',
            'items' => [
                ['label' => 'Мы должны', 'url' => ['/reports/debt-report-seller/debtor']],
            ],
            'options' => ['class' => 'navbar-nav'],
        ]);
        NavBar::end(); ?>
        <?= $content; ?>
    </div>
<?php $this->endContent(); ?>