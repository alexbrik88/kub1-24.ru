<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.01.2017
 * Time: 4:17
 */

use frontend\rbac\UserRole;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;

$this->beginContent('@frontend/views/layouts/main.php');
?>
<div class="debt-report-content container-fluid" style="padding: 0; margin-top: -10px;">
    <?php NavBar::begin([
        //'brandLabel' => 'Отчет',
        //'brandUrl' => null,
        'options' => [
            'class' => 'navbar-report navbar-default',
        ],
        'brandOptions' => [
            'style' => 'margin-left: 0;'
        ],
        'containerOptions' => [
            'style' => 'padding: 0;'
        ],
        'innerContainerOptions' => [
            'class' => 'container-fluid',
            'style' => 'padding: 0;'
        ],
    ]);
    echo Nav::widget([
        'id' => 'debt-report-menu',
        'items' => [
            [
                'label' => 'АВС анализ',
                'url' => ['/reports/analysis/index',],
                'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF),
            ],
            ['label' => 'Должники', 'url' => ['/reports/debt-report/debtor']],
            [
                'label' => 'Платежная дисциплина',
                'url' => ['/reports/discipline/index',],
                'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                           Yii::$app->user->can(UserRole::ROLE_SUPERVISOR) ||
                           Yii::$app->user->can(UserRole::ROLE_MANAGER),
            ],
            ['label' => 'По месяцам', 'url' => ['/reports/debt-report/debtor-month']],
            ['label' => 'Новые клиенты', 'url' => ['/reports/debt-report/new-clients']],
        ],
        'options' => ['class' => 'navbar-nav'],
    ]);
    NavBar::end();
    ?>
    <?= $content; ?>
</div>
<?php $this->endContent(); ?>