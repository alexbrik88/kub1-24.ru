<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.04.2019
 * Time: 12:29
 */

use common\components\helpers\Html;
use yii\helpers\Url;
use frontend\modules\reports\models\ExpensesSearch;

/* @var $this yii\web\View
 * @var $activeTab integer
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $searchModel ExpensesSearch
 * @var $data array
 */

$this->title = 'Расходы';
?>
<div class="portlet box odds-report-header">
    <div class="btn-group pull-right title-buttons">
        <div class="portlet-body employee-wrapper">
            <div class="tabbable tabbable-tabdrop">
                <ul class="nav nav-tabs li-border float-r segmentation-tabs">
                    <li class="<?= $activeTab == ExpensesSearch::TAB_BY_ACTIVITY ? 'active' : null; ?>">
                        <?= Html::a('Расходы списком',
                            Url::to(['expenses', 'activeTab' => ExpensesSearch::TAB_BY_ACTIVITY, 'ExpensesSearch' => [
                                'year' => $searchModel->year,
                            ]]), [
                                'aria-expanded' => 'false',
                                'style' => 'margin-right: 0;',
                            ]); ?>
                    </li>
                    <li class="<?= $activeTab == ExpensesSearch::TAB_BY_PURSE ? 'active' : null; ?>"
                        style="margin-right: 0;float: right;">
                        <?= Html::a('Расходы по кошелькам',
                            Url::to(['expenses', 'activeTab' => ExpensesSearch::TAB_BY_PURSE, 'ExpensesSearch' => [
                                'year' => $searchModel->year,
                            ]]), [
                                'aria-expanded' => 'false',
                                'style' => 'margin-right: 0;',
                            ]); ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <h3 class="page-title">
        <?= $this->title; ?>
        <img src="/img/open-book.svg" class="expenses-panel-trigger">
    </h3>
</div>
<?= $this->render('_partial/expenses_charts', [
    'searchModel' => $searchModel,
    'activeTab' => $activeTab,
]); ?>
<div class="tab-content">
    <div class="tab-pane fade in active">
        <?php if ($activeTab == ExpensesSearch::TAB_BY_ACTIVITY): ?>
            <?= $this->render('_partial/expenses_by_activity', [
                'activeTab' => $activeTab,
                'currentMonthNumber' => $currentMonthNumber,
                'currentQuarter' => $currentQuarter,
                'searchModel' => $searchModel,
                'data' => $data,
            ]); ?>
        <?php elseif ($activeTab == ExpensesSearch::TAB_BY_PURSE): ?>
            <?= $this->render('_partial/expenses_by_purse', [
                'activeTab' => $activeTab,
                'currentMonthNumber' => $currentMonthNumber,
                'currentQuarter' => $currentQuarter,
                'searchModel' => $searchModel,
                'data' => $data,
            ]); ?>
        <?php endif; ?>
        <?= Html::hiddenInput('activeTab', $activeTab, [
            'id' => 'active-tab_report',
        ]); ?>
    </div>
</div>
<?= $this->render('_partial/expenses_panel'); ?>
<?= $this->render('_partial/item_table', [
    'searchModel' => $searchModel,
]); ?>
<div id="visible-right-menu" style="display: none;">
    <div id="visible-right-menu-wrapper" style="z-index: 10050!important;"></div>
</div>
<div id="hellopreloader" style="display: none;">
    <div id="hellopreloader_preload"></div>
</div>
<?php $this->registerJs('
     $(".expenses-panel-trigger").click(function (e) {
        $(".expenses-panel").toggle("fast");
        $("#visible-right-menu").show();
        $("html").attr("style", "overflow: hidden;");
        return false;
    });
    
    $(document).on("click", ".panel-block .side-panel-close, .panel-block .side-panel-close-button", function (e) {
        var $panel = $(this).closest(".panel-block");
        
        $panel.toggle("fast");
        $("#visible-right-menu").hide();
        $("html").removeAttr("style");
    });

    $("#visible-right-menu").click(function (e) {
        var $panel = $(".panel-block:visible");
        
        $panel.toggle("fast");
        $(this).hide();
        $("html").removeAttr("style");
    });
    
    $(document).on("submit", "#js-cash_flow_update_form, #cash-order-form, #cash-emoney-form", function (e) {
        $(this).prepend(\'<input type="hidden" name="fromOdds" value="1">\');
        $.ajax({
            "type": "post",
            "url": $(this).attr("action"),
            "data": $(this).serialize(),
            "success": function(data) {
                $(".odds-model-movement, #ajax-modal-box").modal("hide");
                location.href = location.href;
            }
        });
    
        return false;
    });
    $(document).on("shown.bs.modal", "#many-item", function () {
        var $includeExpenditureItem = $(".joint-operation-checkbox.expense-item:checked").length > 0;
        var $includeIncomeItem = $(".joint-operation-checkbox.income-item:checked").length > 0;
        var $modal = $(this);
        var $header = $modal.find(".modal-header h1");
        var $additionalHeaderText = null;
    
        if ($includeExpenditureItem) {
            $(".expenditure-item-block").removeClass("hidden");
        }
        if ($includeIncomeItem) {
            $(".income-item-block").removeClass("hidden");
        }
        if ($includeExpenditureItem && $includeIncomeItem) {
            $additionalHeaderText = " прихода / расхода";
        } else if ($includeExpenditureItem) {
            $additionalHeaderText = " расхода";
        } else if ($includeIncomeItem) {
            $additionalHeaderText = " прихода";
        }
        $header.append("<span class=additional-header-text>" + $additionalHeaderText + "</span>")
        $(".joint-operation-checkbox:checked").each(function() {
            $modal.find("form#js-cash_flow_update_item_form").prepend($(this).clone().hide());
        });
    });
    $(document).on("hidden.bs.modal", "#many-item", function () {
        $(".expenditure-item-block").addClass("hidden");
        $(".income-item-block").addClass("hidden");
        $(".additional-header-text").remove();
        $(".modal#many-item form#js-cash_flow_update_item_form .joint-operation-checkbox").remove();
    });
    $(document).on("submit", "form#js-cash_flow_update_item_form", function (e) {
        var l = Ladda.create($(this).find(".btn-save")[0]);
        var $hasError = false;
    
        l.start();
        $(".field-expensessearch-expenditureitemidmanyitem:visible").each(function () {
            $(this).removeClass("has-error");
            $(this).find(".help-block").text("");
            if ($(this).find("select").val() == "") {
                $hasError = true;
                $(this).addClass("has-error");
                $(this).find(".help-block").text("Необходимо заполнить.");
            }
        });
        if ($hasError) {
            return false;
        }
    });
'); ?>
