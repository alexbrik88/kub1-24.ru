<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 22.02.2019
 * Time: 19:23
 */

use frontend\modules\reports\models\PlanCashFlows;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use common\components\helpers\Html;
use common\components\helpers\ArrayHelper;
use frontend\modules\cash\models\CashContractorType;
use common\models\Contractor;
use common\models\Company;
use common\models\cash\CashFlowsBase;
use kartik\select2\Select2;
use frontend\widgets\ContractorDropdown;
use frontend\widgets\ExpenditureDropdownWidget;
use yii\helpers\Url;
use common\components\date\DateHelper;

/* @var $this yii\web\View
 * @var $model PlanCashFlows
 * @var $company Company
 * @var $activeTab integer
 * @var $year integer
 */

$flowTypes = PlanCashFlows::$flowTypes;
if (!$model->isNewRecord) {
    $flowTypes = [$model->flow_type => PlanCashFlows::$flowTypes[$model->flow_type]];
}

$sellerArray = ArrayHelper::map(CashContractorType::find()->andWhere([
    '!=', 'name', CashContractorType::BALANCE_TEXT,
])->all(), 'name', 'text');
$customerArray = ArrayHelper::map(CashContractorType::find()->all(), 'name', 'text');

$cashContractorArray = ArrayHelper::map(CashContractorType::find()->all(), 'name', 'text');

$income = 'income' . ($model->flow_type == CashFlowsBase::FLOW_TYPE_INCOME ? '' : ' hidden');
$expense = 'expense' . ($model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE ? '' : ' hidden');

if ($model->first_flow_id) {
    $model->is_repeated = true;
}
?>
<?php Pjax::begin([
    'id' => 'plan_item-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]); ?>
<?php $form = ActiveForm::begin([
    'action' => $model->isNewRecord ?
        Url::to(['create-plan-item', 'activeTab' => $activeTab, 'year' => $year]) :
        Url::to(['update-plan-item', 'id' => $model->id, 'activeTab' => $activeTab, 'year' => $year]),
    'id' => 'js-plan_cash_flow_form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldClass' => 'common\components\floatLabelField\FloatLabelActiveField',
    'fieldConfig' => [
        'template' => "{input}\n{label}\n{error}\n",
    ],
    'options' => [
        'class' => 'form-md-underline',
        'data' => [
            'type-income' => CashFlowsBase::FLOW_TYPE_INCOME,
            'type-expense' => CashFlowsBase::FLOW_TYPE_EXPENSE,
        ],
    ],
]); ?>
<div class="col-md-12 p-l-0 p-r-0 form-group field-plancashflows-flow_type" style="margin-bottom: 10px;">
    <label class="col-md-4 control-label width-label" style="font-weight: bold!important;">Тип</label>
    <div class="col-md-8 p-l-0 p-r-0">
        <?= Html::activeRadioList($model, 'flow_type', $flowTypes, [
            'uncheck' => null,
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::radio($name, $checked, [
                    'class' => 'flow-type-toggle-input',
                    'value' => $value,
                    'label' => $label,
                ]);
            },
        ]); ?>
        <p class="help-block help-block-error "></p>
    </div>
</div>
<div class="col-md-4"></div>
<?= $form->field($model, 'is_repeated', [
    'template' => "{label}\n{input}",
    'options' => [
        'class' => 'col-md-8 p-l-0 p-r-0 form-group form-md-line-input form-md-floating-label',
        'style' => 'display: inline-block;padding-top: 0;margin-bottom: 20px;',
    ],
    'labelOptions' => [
        'class' => 'control-label',
        'style' => 'padding: 6px 0; margin-right: 9px;',
    ],
])->checkbox([
    'disabled' => !$model->isNewRecord && ($model->is_repeated || $model->first_flow_id),
], true) ?>
<?= $form->field($model, 'payment_type')->hiddenInput()->label(false)->error(false); ?>
<div class="col-md-12 p-l-0 p-r-0 form-group field-plancashflows-payment_type required">
    <label class="col-md-4 control-label width-label" style="font-weight: bold!important;">Тип оплаты</label>
    <div class="col-md-8 p-l-0 p-r-0">
        <?= Html::activeRadioList($model, 'payment_type', PlanCashFlows::$paymentTypes); ?>
    </div>
</div>
<div class="col-md-12" style="margin-bottom: 10px;">
    <?= $form->field($model, 'amount')->textInput([
        'value' => !empty($model->amount) ? str_replace('.', ',', $model->amount / 100) : null,
        'class' => 'form-control input-sm js_input_to_money_format' . ($model->amount ? ' edited' : ''),
    ]); ?>
</div>
<div class="col-md-12" style="margin-bottom: 10px;">
    <?= $form->field($model, 'date')->textInput([
        'maxlength' => true,
        'class' => 'form-control input-sm date-picker' . ($model->date ? ' edited' : ''),
        'autocomplete' => 'off',
        'value' => DateHelper::format($model->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
    ]); ?>
    <?= Html::tag('i', '', [
        'id' => 'show-plancashflowsdate',
        'class' => 'fa fa-calendar',
        'style' => 'position: absolute; top: 30px; right: 15px; color: #cecece; cursor: pointer;',
    ]); ?>
</div>
<div class="repeated-fields hidden">
    <div class="col-md-12" style="margin-bottom: 10px;">
        <?= $form->field($model, 'planEndDate')->textInput([
            'maxlength' => true,
            'class' => 'form-control input-sm date-picker' . ($model->planEndDate ? ' edited' : ''),
            'autocomplete' => 'off',
            'value' => $model->planEndDate,
        ]); ?>
        <?= Html::tag('i', '', [
            'id' => 'show-plancashflowsdate',
            'class' => 'fa fa-calendar',
            'style' => 'position: absolute; top: 30px; right: 15px; color: #cecece; cursor: pointer;',
        ]); ?>
    </div>
    <div class="col-md-12" style="margin-bottom: 10px;">

        <?= $form->field($model, 'period', [
            'template' => "{label}\n{input}\n{error}",
            'options' => [
                'class' => '',
            ],
            'labelOptions' => [
                'style' => 'position: relative;transition: 0.2s ease all;color: #999;',
            ],
        ])->widget(Select2::classname(), [
            'data' => PlanCashFlows::$periods,
            'options' => [
                'id' => 'period_id',
                'class' => 'period-items-depend',
            ],
            'pluginOptions' => [],
        ])->label('Периодичность'); ?>
    </div>
</div>
<div class="flow-type-toggle <?= $expense; ?>">
    <div class="col-md-12" style="margin-bottom: 10px;">
        <?= $form->field($model, 'contractor_id', [
            'template' => "{label}\n{input}\n{error}",
            'options' => [
                'class' => '',
            ],
            'labelOptions' => [
                'style' => 'position: relative;transition: 0.2s ease all;color: #999;',
            ],
        ])->widget(ContractorDropdown::class, [
            'company' => $company,
            'contractorType' => Contractor::TYPE_SELLER,
            'staticData' => $sellerArray,
            'options' => [
                'id' => 'seller_contractor_id',
                'class' => 'contractor-items-depend',
                'placeholder' => '',
                'disabled' => $model->flow_type == CashFlowsBase::FLOW_TYPE_INCOME,
                'data' => [
                    'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                ],
            ],
        ])->label('Поставщик'); ?>
    </div>
    <div class="col-md-12" style="margin-bottom: 10px;">
        <?= $form->field($model, 'expenditure_item_id', [
            'template' => "{label}\n{input}\n{error}",
            'options' => [
                'class' => '',
            ],
            'labelOptions' => [
                'style' => 'position: relative;transition: 0.2s ease all;color: #999;',
            ],
        ])->widget(ExpenditureDropdownWidget::classname(), [
            'options' => [
                'class' => 'flow-expense-items',
                'prompt' => '',
                'disabled' => $model->flow_type == CashFlowsBase::FLOW_TYPE_INCOME,
            ],
            'pluginOptions' => [],
        ]); ?>
        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
            'inputId' => 'cashbankflowsform-expenditure_item_id',
        ]) ?>
    </div>
</div>
<div class="flow-type-toggle <?= $income; ?>">
    <div class="col-md-12" style="margin-bottom: 10px;">
        <?= $form->field($model, 'contractor_id', [
            'template' => "{label}\n{input}\n{error}",
            'options' => [
                'class' => '',
            ],
            'labelOptions' => [
                'style' => 'position: relative;transition: 0.2s ease all;color: #999;',
            ],
        ])->widget(ContractorDropdown::class, [
            'company' => $company,
            'contractorType' => Contractor::TYPE_CUSTOMER,
            'staticData' => $customerArray,
            'options' => [
                'id' => 'customer_contractor_id',
                'class' => 'contractor-items-depend cash_contractor_id_select',
                'placeholder' => '',
                'disabled' => $model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE,
                'data' => [
                    'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                ],
            ],
        ])->label('Покупатель'); ?>
    </div>
    <div class="col-md-12" style="margin-bottom: 10px;">
        <?= $form->field($model, 'income_item_id', [
            'template' => "{label}\n{input}\n{error}",
            'options' => [
                'class' => '',
            ],
            'labelOptions' => [
                'style' => 'position: relative;transition: 0.2s ease all;color: #999;',
            ],
        ])->widget(ExpenditureDropdownWidget::classname(), [
            'income' => true,
            'options' => [
                'class' => 'flow-income-items cash_income_item_id_select',
                'prompt' => '',
                'disabled' => $model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE,
            ],
            'pluginOptions' => [],
        ]); ?>
        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
            'inputId' => 'cashbankflowsform-income_item_id',
            'type' => 'income',
        ]); ?>
    </div>
</div>
<div class="col-md-12" style="margin-bottom: 70px;">
    <?= $form->field($model, 'description')->textarea(['rows' => 1]); ?>
</div>
<?= $form->field($model, 'updateRepeatedType')->hiddenInput()->label(false); ?>
<div class="form-actions">
    <div class="row action-buttons">
        <div class="col-sm-5 col-xs-5" style="width: 11%;">
            <?php if (!$model->isNewRecord && ($model->is_repeated || $model->first_flow_id)): ?>
                <?= Html::button('Сохранить', [
                    'class' => 'btn darkblue text-white',
                    'data-toggle' => 'modal',
                    'data-target' => '#save-repeated-flows',
                    'style' => 'width: 100%;',
                ]); ?>
            <?php endif; ?>
            <div class="<?= !$model->isNewRecord && ($model->is_repeated || $model->first_flow_id) ? 'hidden' : null; ?>">
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                    'class' => 'btn darkblue text-white mt-ladda-btn ladda-button widthe-100',
                    'data-style' => 'expand-right',
                ]); ?>
            </div>
        </div>
        <div class="col-sm-2 col-xs-2" style="width: 8.5%;"></div>
        <div class="col-sm-5 col-xs-5" style="width: 9.9%;">
            <?= Html::button('Отменить', [
                'class' => 'btn darkblue text-white pull-right widthe-100 side-panel-close-button',
            ]); ?>
        </div>
    </div>
</div>
<?php $form->end(); ?>
<?php Pjax::end(); ?>
<?php $this->registerJs('
    var $cashContractorArray = ' . json_encode($cashContractorArray) . ';

    $(document).on("click", "#show-plancashflowsdate", function(e) {
        $("#plancashflows-date").datepicker("show");
    });
    $(document).on("change", "form#js-plan_cash_flow_form input.flow-type-toggle-input[type=radio]", function(e) {
        var form = this.form;
        var value = $("input.flow-type-toggle-input:checked", form).val();
        var isRepeated = $("#plancashflows-is_repeated", form).is(":checked");
        var $dateLabel = $(".field-plancashflows-date label", form);
        var $dateEndLabel = $(".field-plancashflows-planenddate label", form);

        if ($(".flow-type-toggle", form).length > 0) {
            $(".has-error", form).removeClass("has-error");
            $(".help-block.help-block-error", form).html("");
            $(".flow-type-toggle", form).addClass("hidden");
            $(".flow-type-toggle input, .flow-type-toggle select", form).prop("disabled", true);
            if (value == $(form).data("type-expense")) {
                $(".flow-type-toggle.expense", form).removeClass("hidden");
                $(".flow-type-toggle.expense input, .flow-type-toggle.expense select", form).prop("disabled", false);
                $(".field-plancashflows-amount label", form).text("Сумма планового расхода");
                $dateEndLabel.text("Плановая дата последнего расхода");
                if (isRepeated) {
                    $dateLabel.text("Плановая дата первого расхода");
                } else {
                    $dateLabel.text("Дата планового расхода");
                }
            } else if (value == $(form).data("type-income")) {
                $(".flow-type-toggle.income", form).removeClass("hidden");
                $(".flow-type-toggle.income input, .flow-type-toggle.income select", form).prop("disabled", false);
                $(".field-plancashflows-amount label", form).text("Сумма планового прихода");
                $dateEndLabel.text("Плановая дата последнего прихода");
                if (isRepeated) {
                    $dateLabel.text("Плановая дата первого прихода");
                } else {
                    $dateLabel.text("Дата планового прихода");
                }
            }
        }
    });
    $(document).on("change", "#plancashflows-is_repeated", function(e) {
        var form = this.form;
        var $flowType = $("input.flow-type-toggle-input[type=radio]:checked", form).val();
        var $flowTypeText = "";
        var $repeatedFields = $(".repeated-fields", form);
        var $dateStartLabel = $(".field-plancashflows-date label", form);

        if ($flowType == $(form).data("type-expense")) {
            $flowTypeText = "расхода";
        } else if ($flowType == $(form).data("type-income")) {
            $flowTypeText = "прихода";
        }
        if ($(this).is(":checked")) {
            $dateStartLabel.text("Плановая дата первого " + $flowTypeText);
            $repeatedFields.removeClass("hidden");
        } else {
            $dateStartLabel.text("Дата планового " + $flowTypeText);
            $repeatedFields.addClass("hidden");
        }
    });

    $("#plancashflows-date, #plancashflows-planenddate").change(function (e) {
        if ($(this).val() !== "" && !$(this).hasClass("edited")) {
            $(this).addClass("edited")
        }
    });

    $("form#js-plan_cash_flow_form .date-picker").datepicker({
        keyboardNavigation: false,
        forceParse: false,
        language: "ru",
        format: "dd.mm.yyyy",
        autoclose: true
    });

    $("form#js-plan_cash_flow_form input[type=radio]").uniform();

    $("#plancashflows-payment_type, #plancashflows-flow_type").change(function (e) {
        checkCashContractor();
    });

    function checkCashContractor() {
        var $paymentType = $("#plancashflows-payment_type input:checked").val();
        var $flowType = $("#plancashflows-flow_type input:checked").val();
        var $contractorInput = $("#customer_contractor_id");
        var $cashContractorIndividual = $.extend([], $cashContractorArray);

        if ($flowType == 0) {
            $contractorInput = $("#seller_contractor_id");
            delete $cashContractorIndividual.balance;
        }
        for ($item in $cashContractorArray) {
            $contractorInput.find("option[value=" + $item + "]").remove();
        }
        if ($paymentType == 1) {
            delete $cashContractorIndividual.bank;
        } else if ($paymentType == 2) {
            delete $cashContractorIndividual.order;
        } else if ($paymentType == 3) {
            delete $cashContractorIndividual.emoney;
        }
        var $newOptions = "";
        for ($item in $cashContractorIndividual) {
            $newOptions += "<option value=" + $item + ">" + $cashContractorIndividual[$item] + "</option>";
        }
        if ($newOptions !== "") {
            $contractorInput.prepend($newOptions);
        }
    };
'); ?>
