<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.06.2018
 * Time: 18:14
 */

use frontend\modules\reports\models\FlowOfFundsReportSearch;
use common\components\TextHelper;

/* @var $this yii\web\View
 * @var $warnings []
 */
?>
<div class="portlet box odds-warning">
    <h3 class="page-title">
        ПРЕДУПРЕЖДЕНИЯ
    </h3>

    <div class="pagination">
        1-<?= count($warnings); ?>
    </div>
</div>
<div class="col-md-12 odds-warning-blocks">
    <?php $number = 1; ?>
    <?php foreach ($warnings as $key => $data): ?>
        <div class="col-md-3 one-warning-block">
            <?php if ($key == 1): ?>
                <span class="number"><?= $number; ?></span>
                Остаток по движению денег не может быть отрицательным!<br>
                У вас остаток на конец <?= $warnings[$key]['month'] ?>
                <span style="color: red;">
                        -<?= TextHelper::invoiceMoneyFormat($warnings[$key]['amount'], 2); ?> <i class="fa fa-rub"></i>
                    </span>
                <?= $warnings[$key]['moreThenOne'] ? 'и есть еще месяца с отрицательным остатком.' : '.'; ?><br>
                Возможно у вас:
                <ul style="padding-left: 17px;">
                    <li>
                        в кассе или в банке не внесены Начальные остатки;
                    </li>
                    <li>
                        не отмечены все приходы.
                    </li>
                </ul>
            <?php elseif ($key == 2): ?>
                <span class="number"><?= $number; ?></span>
                Просроченная задолженность покупателей составляет
                <?= TextHelper::invoiceMoneyFormat($warnings[$key]['totalDebtSum'], 2); ?> <i class="fa fa-rub"></i><br>
                Если вы соберете хотя бы 50% долгов уже в
                <?= FlowOfFundsReportSearch::$monthIn[date('m')]; ?>, то приход по операционной
                деятельности увеличится на <?= $warnings[$key]['incomePercent']; ?>%
            <?php elseif ($key == 3): ?>
                <span class="number"><?= $number; ?></span>
                У вас есть неоплаченные счета от поставщиков на сумму
                <?= TextHelper::invoiceMoneyFormat($warnings[$key]['inInvoiceDebtSum'], 2); ?> <i class="fa fa-rub"></i>
                <br>
                <?= $warnings[$key]['message']; ?>
            <?php elseif ($key == 4): ?>
                <span class="number"><?= $number; ?></span>
                <?= $warnings[$key]['message']; ?>
            <?php endif; ?>
        </div>
        <?php $number++; ?>
    <?php endforeach; ?>
</div>
