<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 22.02.2019
 * Time: 0:42
 */

use common\components\helpers\Html;

/* @var $this yii\web\View */
?>
<div class="plan_item-panel panel-block">
    <div class="main-block" style="height: 100%;">
        <div class="header">
            <?= Html::button('×&times;', [
                'class' => 'close side-panel-close',
            ]); ?>
            <table style="width: 100%; height: ">
                <tr>
                    <td style="width: 10%; vertical-align: middle; text-align: center;">
                        <div class="header-img">
                            <img src="/img/icons/clockwise-rotation.png">
                        </div>
                    </td>
                    <td class="plan_item-header" style="padding-left: 10px; font-size: 18px;">

                    </td>
                </tr>
            </table>
        </div>
        <div class="plan_item-form"></div>
    </div>
</div>