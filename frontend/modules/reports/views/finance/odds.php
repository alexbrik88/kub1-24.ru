<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 26.10.2017
 * Time: 7:57
 */

use yii\bootstrap\Html;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $searchModel FlowOfFundsReportSearch
 * @var $data []
 * @var $expenditureItems []
 * @var $types []
 * @var $balance []
 * @var $growingBalance []
 * @var $blocks []
 * @var $warnings []
 * @var $checkMonth boolean
 * @var $activeTab integer
 */

$this->title = 'Отчет о Движении Денежных Средств';
$currentMonthNumber = date('n');
$currentQuarter = (int)ceil(date('m') / 3);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized', 'tooltip_pay-odds'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>
<?= $this->render('_partial/odds-panel'); ?>
    <div class="portlet box odds-report-header" style="<?= $searchModel->company->isFreeTariff ? 'margin-bottom: 15px;' : null; ?>">
        <div class="btn-group pull-right title-buttons">
            <div class="portlet-body employee-wrapper">
                <div class="tabbable tabbable-tabdrop">
                    <ul class="nav nav-tabs li-border float-r segmentation-tabs">
                        <li class="<?= $activeTab == FlowOfFundsReportSearch::TAB_ODDS ? 'active' : null; ?>">
                            <?= Html::a('ОДДС по видам деятельности',
                                Url::to(['odds', 'activeTab' => FlowOfFundsReportSearch::TAB_ODDS, 'FlowOfFundsReportSearch' => ['year' => $searchModel->year]]), [
                                    'aria-expanded' => 'false',
                                    'style' => 'margin-right: 0;',
                                ]); ?>
                        </li>
                        <li class="<?= $activeTab == FlowOfFundsReportSearch::TAB_ODDS_BY_PURSE ? 'active' : null; ?>"
                            style="margin-right: 0;float: right;">
                            <?= Html::a('ОДДС по кошелькам',
                                Url::to(['odds', 'activeTab' => FlowOfFundsReportSearch::TAB_ODDS_BY_PURSE, 'FlowOfFundsReportSearch' => ['year' => $searchModel->year]]), [
                                    'aria-expanded' => 'false',
                                    'style' => 'margin-right: 0;',
                                ]); ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <h3 class="page-title" style="<?= $searchModel->company->isFreeTariff ? 'margin-bottom: 5px;' : null; ?>">
            <?= $this->title; ?>
            <img src="/img/open-book.svg" class="odds-panel-trigger">
        </h3>
        <?php if ($searchModel->company->isFreeTariff): ?>
            <span class="free-tariff-warning">Отчет доступен полностью только на платном тарифе</span>
        <?php endif; ?>
    </div>
    <div class="tab-content">
        <div class="tab-pane fade in active">
            <?php if ($activeTab == FlowOfFundsReportSearch::TAB_ODDS): ?>
                <?= $this->render('_partial/odds_by_activity', [
                    'searchModel' => $searchModel,
                    'data' => $data,
                    'expenditureItems' => $expenditureItems,
                    'types' => $types,
                    'balance' => $balance,
                    'growingBalance' => $growingBalance,
                    'blocks' => $blocks,
                    'checkMonth' => $checkMonth,
                    'activeTab' => $activeTab,
                    'currentMonthNumber' => $currentMonthNumber,
                    'currentQuarter' => $currentQuarter,
                ]); ?>
            <?php elseif ($activeTab == FlowOfFundsReportSearch::TAB_ODDS_BY_PURSE): ?>
                <?= $this->render('_partial/odds_by_purse', [
                    'searchModel' => $searchModel,
                    'data' => $data,
                    'expenditureItems' => $expenditureItems,
                    'types' => $types,
                    'balance' => $balance,
                    'growingBalance' => $growingBalance,
                    'blocks' => $blocks,
                    'checkMonth' => $checkMonth,
                    'activeTab' => $activeTab,
                    'currentMonthNumber' => $currentMonthNumber,
                    'currentQuarter' => $currentQuarter,
                ]); ?>
            <?php endif; ?>
            <?= Html::hiddenInput('activeTab', $activeTab, [
                'id' => 'active-tab_report',
            ]); ?>
        </div>
    </div>
<?= $this->render('_partial/item_table', [
    'searchModel' => $searchModel,
]); ?>
<?= $this->render('_partial/odds-warnings', [
    'warnings' => $warnings,
]); ?>
    <div class="tooltip-template" style="display: none;">
        <span id="tooltip_financial-operations-block" style="display: inline-block; text-align: center;">
            Привлечение денег (кредиты, займы)<br>
            в компанию и их возврат. <br>
            Выплата дивидендов.<br>
            Предоставление займов и депозитов.
        </span>
        <span id="tooltip_operation-activities-block" style="display: inline-block; text-align: center;">
                Движение денег, связанное с основной деятельностью компании <br>
                (оплата от покупателей, зарплата, аренда, покупка товаров и т.д.)
        </span>
        <span id="tooltip_investment-activities-block" style="display: inline-block; text-align: center;">
                Покупка и продажа оборудования и других основных средств. <br>
                Затраты на новые проекты и поступление выручки от них. <br>
        </span>
        <span id="tooltip_pay" style="display: inline-block; text-align: center;">
            <span style="display: block;font-size: 16px;font-weight: bold;margin-bottom: 10px;">Способ оплаты</span>
            <?= Html::a('Картой', null, [
                'class' => 'btn darkblue',
                'style' => 'background: #45b6af;color: white;width: 135px;',
                'data-href' => Url::to(['/odds-payment/online-payment']),
            ]); ?>
            <?= Html::a('Выставить счет', null, [
                'class' => 'btn darkblue',
                'style' => 'background: #ffaa24;color: #ffffff;width: 135px;',
                'data-href' => Url::to(['/odds-payment/expose-invoice']),
            ]); ?>
            <span style="display: block;font-size: 15px;margin-top: 10px;">
                После оплаты наш специалист<br> свяжется с вами
            </span>
        </span>
    </div>
    <div id="hellopreloader" style="display: none;">
        <div id="hellopreloader_preload"></div>
    </div>
    <div id="visible-right-menu" style="display: none;">
        <div id="visible-right-menu-wrapper"></div>
    </div>
<?php $this->registerJs('
$(document).ready(function(){
    $(".odds-panel-trigger").click(function(){
        $(".odds-panel").toggle("fast");
        $(this).toggleClass("active");
        $("#visible-right-menu").show();
        $("html").attr("style", "overflow: hidden;");
        return false;
    });
    $("#visible-right-menu").click(function () {
        $(".odds-panel").toggle("fast");
        $(".odds-panel-trigger").toggleClass("active");
        $(this).hide();
        $("html").removeAttr("style");
    });
    $(".odds-panel .side-panel-close").click(function () {
        $(".odds-panel").toggle("fast");
        $(".odds-panel-trigger").toggleClass("active");
        $("#visible-right-menu").hide();
        $("html").removeAttr("style");
    });
    $(".odds-panel .read-more").click(function () {
        $(this).hide();
        $(".odds-panel .read-more-text").slideDown();
        $(".odds-panel .hide-more-text").show();
    });
    $(".odds-panel .hide-more-text").click(function () {
        $(this).hide();
        $(".odds-panel .read-more-text").slideUp();
        $(".odds-panel .read-more").show();
    });
    $(document).on("click", ".tooltip_pay-odds a", function () {
        $.post($(this).data("href"), null, function($data) {
            $("button.pay").append($data);
        });
    });
    $(document).on("submit", "#js-cash_flow_update_form, #cash-order-form, #cash-emoney-form", function (e) {
        $(this).prepend(\'<input type="hidden" name="fromOdds" value="1">\');
        $.ajax({
            "type": "post",
            "url": $(this).attr("action"),
            "data": $(this).serialize(),
            "success": function(data) {
                $(".odds-model-movement, #ajax-modal-box").modal("hide");
                location.href = location.href;
            }
        });
    
        return false;
    });
    $(document).on("shown.bs.modal", "#many-item", function () {
        var $includeExpenditureItem = $(".joint-operation-checkbox.expense-item:checked").length > 0;
        var $includeIncomeItem = $(".joint-operation-checkbox.income-item:checked").length > 0;
        var $modal = $(this);
        var $header = $modal.find(".modal-header h1");
        var $additionalHeaderText = null;
    
        if ($includeExpenditureItem) {
            $(".expenditure-item-block").removeClass("hidden");
        }
        if ($includeIncomeItem) {
            $(".income-item-block").removeClass("hidden");
        }
        if ($includeExpenditureItem && $includeIncomeItem) {
            $additionalHeaderText = " прихода / расхода";
        } else if ($includeExpenditureItem) {
            $additionalHeaderText = " расхода";
        } else if ($includeIncomeItem) {
            $additionalHeaderText = " прихода";
        }
        $header.append("<span class=additional-header-text>" + $additionalHeaderText + "</span>")
        $(".joint-operation-checkbox:checked").each(function() {
            $modal.find("form#js-cash_flow_update_item_form").prepend($(this).clone().hide());
        });
    });
    $(document).on("hidden.bs.modal", "#many-item", function () {
        $(".expenditure-item-block").addClass("hidden");
        $(".income-item-block").addClass("hidden");
        $(".additional-header-text").remove();
        $(".modal#many-item form#js-cash_flow_update_item_form .joint-operation-checkbox").remove();
    });
    $(document).on("submit", "form#js-cash_flow_update_item_form", function (e) {
        var l = Ladda.create($(this).find(".btn-save")[0]);
        var $hasError = false;
    
        l.start();
        $(".field-flowoffundsreportsearch-incomeitemidmanyitem:visible, .field-flowoffundsreportsearch-expenditureitemidmanyitem:visible").each(function () {
            $(this).removeClass("has-error");
            $(this).find(".help-block").text("");
            if ($(this).find("select").val() == "") {
                $hasError = true;
                $(this).addClass("has-error");
                $(this).find(".help-block").text("Необходимо заполнить.");
            }
        });
        if ($hasError) {
            return false;
        }
    });
});
'); ?>