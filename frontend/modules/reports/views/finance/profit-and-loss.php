<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 15.04.2019
 * Time: 15:25
 */

use frontend\modules\reports\models\ProfitAndLossSearchModel;

/* @var $this yii\web\View
 * @var $searchModel ProfitAndLossSearchModel
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $data array
 */

$this->title = "Отчет о Прибылях и Убытках для {$searchModel->company->companyTaxationType->getName()}";
?>
<div class="portlet box odds-report-header">
    <h3 class="page-title">
        <?= $this->title; ?>
        <img src="/img/open-book.svg" class="profit-and-loss-panel-trigger">
    </h3>
</div>
<div class="tab-content">
    <div class="tab-pane fade in active">
        <?= $this->render('_partial/profit_and_loss_table', [
            'searchModel' => $searchModel,
            'currentMonthNumber' => $currentMonthNumber,
            'currentQuarter' => $currentQuarter,
            'data' => $data,
        ]); ?>
    </div>
</div>
<div id="visible-right-menu" style="display: none;">
    <div id="visible-right-menu-wrapper" style="z-index: 10050!important;"></div>
</div>
<div id="hellopreloader" style="display: none;">
    <div id="hellopreloader_preload"></div>
</div>
<?= $this->render('_partial/profit_and_loss_panel'); ?>
<?php $this->registerJs('
     $(".profit-and-loss-panel-trigger").click(function (e) {
        $(".profit-and-loss-panel").toggle("fast");
        $("#visible-right-menu").show();
        $("html").attr("style", "overflow: hidden;");
        return false;
    });
    
    $(document).on("click", ".panel-block .side-panel-close, .panel-block .side-panel-close-button", function (e) {
        var $panel = $(this).closest(".panel-block");
        
        $panel.toggle("fast");
        $("#visible-right-menu").hide();
        $("html").removeAttr("style");
    });

    $("#visible-right-menu").click(function (e) {
        var $panel = $(".panel-block:visible");
        
        $panel.toggle("fast");
        $(this).hide();
        $("html").removeAttr("style");
    });
'); ?>