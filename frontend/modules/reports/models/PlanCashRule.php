<?php

namespace frontend\modules\reports\models;

use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use frontend\models\Documents;
use frontend\modules\integration\models\bitrix24\Invoice;
use common\models\Contractor;
use Yii;
use common\models\Company;

/**
 * This is the model class for table "plan_cash_rule".
 *
 * @property int $id
 * @property int $company_id
 * @property int $flow_type
 * @property int|null $income_item_id
 * @property int|null $expenditure_item_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Company $company
 */
class PlanCashRule extends \yii\db\ActiveRecord
{
    const INCOME_PAYMENT_FROM_BUYER = InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER;

    const INCOME_ITEMS_IDS = [
        InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER => 'Оплата от покупателя',
        //InvoiceIncomeItem::ITEM_AGENT_FEE => 'Агентское вознаграждение',
        InvoiceIncomeItem::ITEM_COMMISSION => 'Комиссия'
    ];

    const EXPENDITURE_ITEMS_IDS = [
        InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT => 'Оплата поставщику',
        InvoiceExpenditureItem::ITEM_PRODUCT => 'Товар',
        InvoiceExpenditureItem::ITEM_DELIVERY => 'Доставка',
        //InvoiceExpenditureItem::ITEM_STATIONERY => 'Канцелярские товары',
        InvoiceExpenditureItem::ITEM_FURNITURE => 'Мебель',
        InvoiceExpenditureItem::ITEM_OFFICE_EQUIPMENT => 'Оргтехника',
        InvoiceExpenditureItem::ITEM_PROGRAMS => 'Программное обеспечение',
        InvoiceExpenditureItem::ITEM_SERVICE => 'Услуги',
        InvoiceExpenditureItem::ITEM_HOUSEHOLD_EXPENSES => 'Хоз. расходы',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plan_cash_rule';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'flow_type', 'created_at', 'updated_at'], 'required'],
            [['company_id', 'flow_type', 'income_item_id', 'expenditure_item_id', 'created_at', 'updated_at'], 'integer'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['expenditure_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceExpenditureItem::className(), 'targetAttribute' => ['expenditure_item_id' => 'id']],
            [['income_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceIncomeItem::className(), 'targetAttribute' => ['income_item_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'flow_type' => 'Flow Type',
            'income_item_id' => 'Income Item ID',
            'expenditure_item_id' => 'Expenditure Item ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    public function deletePlanContractors() {

        $contractors = PlanCashContractor::find()->where(['rule_id' => $this->id])->all();
        foreach ($contractors as $c) {
            $c->deleteFlows();
            $c->delete();
        }

        return true;
    }

    public function addPlanContractors()
    {
        $ioType = $this->flow_type + 1;

        $contractorsIds = Contractor::find()
            ->where(['company_id' => $this->company_id])
            ->andWhere(['type' => $ioType])
            ->byIsDeleted(false)
            ->byStatus(Contractor::ACTIVE)
            ->andWhere(($ioType == Contractor::TYPE_SELLER) ?
                ['invoice_expenditure_item_id' => $this->expenditure_item_id] :
                ['invoice_income_item_id' => $this->income_item_id])
            ->select('id')
            ->distinct()
            ->column();

        $query2 = PlanCashContractor::find()
            ->where(['company_id' => $this->company_id])
            ->andWhere(['flow_type' => $this->flow_type])
            ->andWhere(['contractor_id' => $contractorsIds])
            ->select('contractor_id')
            ->distinct();

        $planContractorIds = $query2->column();

        foreach ($contractorsIds as $id) {

            if (in_array($id, $planContractorIds)) {
                // contractor present in plan
                continue;
            }

            if ($contractor = Contractor::findOne($id)) {

                $model = new PlanCashContractor([
                    'company_id' => $this->company_id,
                    'contractor_id' => $contractor->id,
                    'plan_type' => PlanCashContractor::PLAN_BY_FUTURE_INVOICES,
                    'flow_type' => $this->flow_type,
                    'payment_type' => PlanCashContractor::getPaymentType($contractor),
                    'item_id' => ($ioType == Contractor::TYPE_SELLER) ? $this->expenditure_item_id : $this->income_item_id,
                    'payment_delay' => $contractor->payment_delay,
                    'created_at' => time(),
                    'updated_at' => time(),
                    'rule_id' => $this->id
                ]);

                if (!$model->save()) {

                    // var_dump('addPlanContractors: ', $model->getErrors()); exit;

                    return false;
                }

                //////////////////////////
                /// CREATE AUTOPLAN FLOWS
                $model->createFlowsByActualInvoices();
                /////////////////////////

            }
        }

        return true;
    }

    /**
     * Get rules articles by flows
     * @param $company_id
     * @return array
     */
    public static function getArticles($company_id)
    {
        /** @var PlanCashRule[] $articles */
        $articles = self::find()
            ->where(['company_id' => $company_id])
            ->all();

        $ret = [0 => [], 1 => []];
        foreach ($articles as $v) {

            if (!isset($ret[$v->flow_type]))
                $ret[$v->flow_type] = [];

            $itemId = ($v->flow_type == 0) ? $v->expenditure_item_id : $v->income_item_id;
            $ret[$v->flow_type][$itemId] = $itemId;
        }

        return $ret;
    }
}
