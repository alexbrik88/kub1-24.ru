<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 15.04.2018
 * Time: 5:55
 */

namespace frontend\modules\reports\models;


use common\models\company\CompanyTaxationType;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\Order;
use common\models\document\OrderAct;
use common\models\document\OrderPackingList;
use common\models\document\PackingList;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use common\models\product\Product;
use frontend\models\Documents;
use yii\base\Model;
use Yii;

/**
 * Class ProfitSearch
 * @package frontend\modules\reports\models
 *
 * @property Employee $user
 */
class ProfitSearch extends Model
{
    /**
     *
     */
    const JANUARY = '01';

    /**
     *
     */
    const FEBRUARY = '02';

    /**
     *
     */
    const MARCH = '03';

    /**
     *
     */
    const APRIL = '04';

    /**
     *
     */
    const MAY = '05';

    /**
     *
     */
    const JUNE = '06';

    /**
     *
     */
    const JULY = '07';

    /**
     *
     */
    const AUGUST = '08';

    /**
     *
     */
    const SEPTEMBER = '09';

    /**
     *
     */
    const OCTOBER = '10';

    /**
     *
     */
    const NOVEMBER = '11';

    /**
     *
     */
    const DECEMBER = '12';

    /**
     *
     */
    const TYPE_INVOICE = 1;

    /**
     *
     */
    const TYPE_ACT_AND_PACKING_LIST = 2;

    /**
     * @var int
     */
    public $year = null;

    /**
     * @var int
     */
    public $type = self::TYPE_ACT_AND_PACKING_LIST;

    /**
     * @var array
     */
    public static $month = [
        self::JANUARY => 'Январь',
        self::FEBRUARY => 'Февраль',
        self::MARCH => 'Март',
        self::APRIL => 'Апрель',
        self::MAY => 'Май',
        self::JUNE => 'Июнь',
        self::JULY => 'Июль',
        self::AUGUST => 'Август',
        self::SEPTEMBER => 'Сентябрь',
        self::OCTOBER => 'Октябрь',
        self::NOVEMBER => 'Ноябрь',
        self::DECEMBER => 'Декабрь',
    ];

    /**
     * @var Employee
     */
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'type'], 'integer'],
        ];
    }

    /**
     * @param array $params
     * @param array $config
     */
    public function __construct($params = [], $config = [])
    {
        $this->load($params);
        $this->year = $this->year ? $this->year : date('Y');
        if (!Yii::$app->user->isGuest) {
            $this->_user = Yii::$app->user->identity;
        }

        parent::__construct($config);
    }

    /**
     * @return Employee|null
     */
    public function getUser()
    {
        return $this->_user;
    }

    /**
     * @return array
     */
    public function calculateReceiptData()
    {
        $result = [
            'totalAmount' => 0,
        ];
        foreach (self::$month as $monthNumber => $monthName) {
            $dateFrom = $this->year . '-' . $monthNumber . '-01';
            $dateTo = $this->year . '-' . $monthNumber . '-' . cal_days_in_month(CAL_GREGORIAN, $monthNumber, $this->year);
            if ($this->type == self::TYPE_INVOICE) {
                $amount = Invoice::find()
                    ->byCompany($this->user->company_id)
                    ->byIOType(Documents::IO_TYPE_OUT)
                    ->byDeleted(Invoice::NOT_IS_DELETED)
                    ->byDocumentDateRange($dateFrom, $dateTo)
                    ->sum('total_amount_with_nds');
            } else {
                $amount = Act::find()
                        ->byCompany($this->user->company_id)
                        ->byIOType(Documents::IO_TYPE_OUT)
                        ->byDocumentDateRange($dateFrom, $dateTo)
                        ->sum('order_sum') +
                    PackingList::find()
                        ->byCompany($this->user->company_id)
                        ->byIOType(Documents::IO_TYPE_OUT)
                        ->byDocumentDateRange($dateFrom, $dateTo)
                        ->sum('orders_sum');
            }
            if ($this->user->company->companyTaxationType->osno) {
                $amount = $amount / 118 * 100;
            }
            $result[$monthNumber] = $amount;
            $result['totalAmount'] += $amount;
        }

        return $result;
    }

    /**
     * @return array
     */
    public function calculatePriceData()
    {
        $result = [
            'totalAmount' => 0,
        ];
        foreach (self::$month as $monthNumber => $monthName) {
            $dateFrom = $this->year . '-' . $monthNumber . '-01';
            $dateTo = $this->year . '-' . $monthNumber . '-' . cal_days_in_month(CAL_GREGORIAN, $monthNumber, $this->year);
            if ($this->type == self::TYPE_INVOICE) {
                $amount = Invoice::find()
                    ->leftJoin(Order::tableName(), '{{invoice}}.[[id]] = {{order}}.[[invoice_id]]')
                    ->leftJoin(Product::tableName(), '{{order}}.[[product_id]] = {{product}}.[[id]]')
                    ->byCompany($this->user->company_id)
                    ->byIOType(Documents::IO_TYPE_OUT)
                    ->byDeleted(Invoice::NOT_IS_DELETED)
                    ->byDocumentDateRange($dateFrom, $dateTo)
                    ->sum(Product::tableName() . '.price_for_buy_with_nds * ' . Order::tableName() . '.quantity');
            } else {
                $amount = Act::find()
                        ->leftJoin(OrderAct::tableName(), '{{act}}.[[id]] = {{order_act}}.[[act_id]]')
                        ->leftJoin(Product::tableName(), '{{order_act}}.[[product_id]] = {{product}}.[[id]]')
                        ->byCompany($this->user->company_id)
                        ->byIOType(Documents::IO_TYPE_OUT)
                        ->byDocumentDateRange($dateFrom, $dateTo)
                        ->sum(Product::tableName() . '.price_for_buy_with_nds * ' . OrderAct::tableName() . '.quantity')
                    +
                    PackingList::find()
                        ->leftJoin(OrderPackingList::tableName(), '{{packing_list}}.[[id]] = {{order_packing_list}}.[[packing_list_id]]')
                        ->leftJoin(Product::tableName(), '{{order_packing_list}}.[[product_id]] = {{product}}.[[id]]')
                        ->byCompany($this->user->company_id)
                        ->byIOType(Documents::IO_TYPE_OUT)
                        ->byDocumentDateRange($dateFrom, $dateTo)
                        ->sum(Product::tableName() . '.price_for_buy_with_nds * ' . OrderPackingList::tableName() . '.quantity');
            }
            if ($this->user->company->companyTaxationType->osno) {
                $amount = $amount / 118 * 100;
            }
            $result[$monthNumber] = $amount;
            $result['totalAmount'] += $amount;
        }

        return $result;
    }

    /**
     * @param $receiptData
     * @param $priceData
     * @return array
     */
    public function calculateGrossProfit($receiptData, $priceData)
    {
        $result = [];
        foreach (self::$month as $monthNumber => $monthName) {
            $result[$monthNumber] = $receiptData[$monthNumber] - $priceData[$monthNumber];
        }
        $result['totalAmount'] = $receiptData['totalAmount'] - $priceData['totalAmount'];

        return $result;
    }

    /**
     * @return mixed
     */
    public function calculateConsumptionData()
    {
        $result['total']['totalAmount'] = 0;
        /* @var $expenditureItems InvoiceExpenditureItem[] */
        $expenditureItems = InvoiceExpenditureItem::find()
            ->andWhere(['not', ['in', 'id', [InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT,
                InvoiceExpenditureItem::ITEM_RETURN_FOUNDER, InvoiceExpenditureItem::ITEM_RETURN,
                InvoiceExpenditureItem::ITEM_DIVIDEND_PAYMENT, InvoiceExpenditureItem::ITEM_BORROWING_EXTRADITION,
                InvoiceExpenditureItem::ITEM_NDS, InvoiceExpenditureItem::ITEM_NDFL_DIVIDENDS,
                InvoiceExpenditureItem::ITEM_TAX, InvoiceExpenditureItem::ITEM_OWN_FOUNDS,
                InvoiceExpenditureItem::ITEM_LOAN_REPAYMENT, InvoiceExpenditureItem::ITEM_REPAYMENT_CREDIT]]])
            ->andWhere(['or',
                ['is', 'company_id', null],
                ['company_id' => $this->user->company_id],
            ])
            ->all();
        foreach ($expenditureItems as $expenditureItem) {
            $result['items'][$expenditureItem->id] = [
                'id' => $expenditureItem->id,
                'name' => $expenditureItem->name,
                'totalAmount' => 0,
            ];
            foreach (self::$month as $monthNumber => $monthName) {
                if (!isset($result['total'][$monthNumber])) {
                    $result['total'][$monthNumber] = 0;
                }
                $dateFrom = $this->year . '-' . $monthNumber . '-01';
                $dateTo = $this->year . '-' . $monthNumber . '-' . cal_days_in_month(CAL_GREGORIAN, $monthNumber, $this->year);
                if ($this->type == self::TYPE_INVOICE) {
                    $amount = Invoice::find()
                        ->byCompany($this->user->company_id)
                        ->byIOType(Documents::IO_TYPE_IN)
                        ->byDeleted(Invoice::NOT_IS_DELETED)
                        ->byDocumentDateRange($dateFrom, $dateTo)
                        ->andWhere([Invoice::tableName() . '.invoice_expenditure_item_id' => $expenditureItem->id])
                        ->andWhere([Invoice::tableName() . '.invoice_status_id' => InvoiceStatus::STATUS_PAYED])
                        ->sum('total_amount_with_nds');
                } else {
                    $amount = Act::find()
                            //->leftJoin(Invoice::tableName(), '{{act}}.[[invoice_id]] = {{invoice}}.[[id]]')
                            ->byCompany($this->user->company_id)
                            ->byIOType(Documents::IO_TYPE_IN)
                            ->byDocumentDateRange($dateFrom, $dateTo)
                            ->andWhere([Invoice::tableName() . '.invoice_expenditure_item_id' => $expenditureItem->id])
                            ->sum(Act::tableName() . '.order_sum') +
                        PackingList::find()
                            //->leftJoin(Invoice::tableName(), '{{packing_list}}.[[invoice_id]] = {{invoice}}.[[id]]')
                            ->byCompany($this->user->company_id)
                            ->byIOType(Documents::IO_TYPE_IN)
                            ->byDocumentDateRange($dateFrom, $dateTo)
                            ->andWhere([Invoice::tableName() . '.invoice_expenditure_item_id' => $expenditureItem->id])
                            ->sum(PackingList::tableName() . '.orders_sum');
                }
                if ($this->user->company->companyTaxationType->osno) {
                    $amount = $amount / 118 * 100;
                }
                $result['items'][$expenditureItem->id][$monthNumber] = $amount;
                $result['items'][$expenditureItem->id]['totalAmount'] += $amount;
                $result['total'][$monthNumber] += $amount;
                $result['total']['totalAmount'] += $amount;
            }
            $deleteItem = true;
            foreach ($result['items'][$expenditureItem->id] as $key => $oneData) {
                if (in_array($key, ['id', 'name', 'totalAmount'])) {
                    continue;
                }
                if ($oneData !== 0) {
                    $deleteItem = false;
                    break;
                }
            }
            if ($deleteItem) {
                unset($result['items'][$expenditureItem->id]);
            }
        }

        return $result;
    }

    /**
     * @param $grossProfit
     * @param $consumptionData
     * @return array
     */
    public function calculateProfitOrLesion($grossProfit, $consumptionData)
    {
        $result = [];
        foreach (self::$month as $monthNumber => $monthName) {
            $result[$monthNumber] = $grossProfit[$monthNumber] - $consumptionData[$monthNumber];
        }
        $result['totalAmount'] = $grossProfit['totalAmount'] - $consumptionData['totalAmount'];

        return $result;
    }

    /**
     * @param $profitOrLesion
     * @return array
     */
    public function calculateTax($profitOrLesion)
    {
        $result = [];
        foreach (self::$month as $monthNumber => $monthName) {
            if ($profitOrLesion[$monthNumber] <= 0) {
                $result[$monthNumber] = 0;
            } else {
                if ($this->user->company->companyTaxationType->osno) {
                    $result[$monthNumber] = $profitOrLesion[$monthNumber] / 100 * 20;
                } elseif ($this->user->company->companyTaxationType->usn &&
                    $this->user->company->companyTaxationType->usn_type == CompanyTaxationType::INCOME_EXPENSES &&
                    $this->user->company->companyTaxationType->usn_percent > 0
                ) {
                    $result[$monthNumber] = $profitOrLesion[$monthNumber] / 100 * intval($this->user->company->companyTaxationType->usn_percent);
                } else {
                    $result[$monthNumber] = 0;
                }
            }
        }
        if ($profitOrLesion['totalAmount'] <= 0) {
            $result['totalAmount'] = 0;
        } else {
            if ($this->user->company->companyTaxationType->osno) {
                $result['totalAmount'] = $profitOrLesion['totalAmount'] / 100 * 20;
            } elseif ($this->user->company->companyTaxationType->usn &&
                $this->user->company->companyTaxationType->usn_type == CompanyTaxationType::INCOME_EXPENSES &&
                $this->user->company->companyTaxationType->usn_percent > 0
            ) {
                $result['totalAmount'] = $profitOrLesion['totalAmount'] / 100 * intval($this->user->company->companyTaxationType->usn_percent);
            } else {
                $result['totalAmount'] = 0;
            }
        }

        return $result;
    }

    /**
     * @param $profitOrLesion
     * @param $tax
     * @return array
     */
    public function calculateNetProfit($profitOrLesion, $tax)
    {
        $result = [];
        foreach (self::$month as $monthNumber => $monthName) {
            $result[$monthNumber] = $profitOrLesion[$monthNumber] - $tax[$monthNumber];
        }
        $result['totalAmount'] = $profitOrLesion['totalAmount'] - $tax['totalAmount'];

        return $result;
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        $range = [];
        $registrationYear = date('Y', $this->user->company->created_at);
        $currentYear = date('Y');
        foreach (range($registrationYear, $currentYear) as $value) {
            $range[$value] = $value;
        }
        arsort($range);

        return $range;
    }
}