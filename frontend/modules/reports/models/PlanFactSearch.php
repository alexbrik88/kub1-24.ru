<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 28.02.2019
 * Time: 0:39
 */

namespace frontend\modules\reports\models;


use common\components\TextHelper;
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use Yii;
use yii\base\InvalidParamException;
use yii\db\Query;

/**
 * Class PlanFactSearch
 * @package frontend\modules\reports\models
 */
class PlanFactSearch extends FlowOfFundsReportSearch
{
    /**
     *
     */
    const TAB_BY_ACTIVITY = 5;
    /**
     *
     */
    const TAB_BY_PURSE = 6;

    /**
     * @var array
     */
    public static $flowOfFundsTypeByPlanFactTab = [
        self::TAB_BY_ACTIVITY => FlowOfFundsReportSearch::TAB_ODDS,
        self::TAB_BY_PURSE => FlowOfFundsReportSearch::TAB_ODDS_BY_PURSE,
    ];

    /**
     * @param $type
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function search($type, $params = [])
    {
        if ($type == self::TAB_BY_ACTIVITY) {
            return $this->searchByActivity($params);
        } elseif ($type == self::TAB_BY_PURSE) {
            return $this->searchByPurse($params);
        }
        throw new InvalidParamException('Invalid type param.');
    }

    /**
     * @param $flowType
     * @param $items
     * @param $monthNumberFrom
     * @param $monthNumberTo
     * @param $factAmount
     * @param null $paymentType
     * @return array
     */
    public function getTooltipData($flowType, $items, $monthNumberFrom, $monthNumberTo, $factAmount, $paymentType = null)
    {
        $data = [];
        $dateFrom = $this->year . '-' . $monthNumberFrom . '-01';
        $dateTo = $this->year . '-' . $monthNumberTo . '-' . cal_days_in_month(CAL_GREGORIAN, $monthNumberTo, $this->year);
        $query = PlanCashFlows::find()
            ->andWhere(['company_id' => $this->company->id])
            ->andWhere(['flow_type' => $flowType])
            ->andWhere(['between', 'date', $dateFrom, $dateTo]);
        if ($flowType == CashFlowsBase::FLOW_TYPE_INCOME) {
            $query->andWhere(['in', 'income_item_id', $items]);
        } elseif ($flowType == CashFlowsBase::FLOW_TYPE_EXPENSE) {
            $query->andWhere(['in', 'expenditure_item_id', $items]);
        } else {
            throw new InvalidParamException('Invalid flow type param.');
        }
        if ($paymentType) {
            $query->andWhere(['payment_type' => $paymentType]);
        }

        $amount = $query->sum('amount');
        $data['factAmount'] = TextHelper::invoiceMoneyFormat($factAmount, 2);
        if ($amount) {
            $data['planAmount'] = TextHelper::invoiceMoneyFormat($amount, 2);
            if ($flowType == CashFlowsBase::FLOW_TYPE_INCOME) {
                $diffAmount = $factAmount - $amount;
            } else {
                $diffAmount = $amount - $factAmount;
            }
            $data['diff'] = TextHelper::invoiceMoneyFormat($diffAmount, 2);
            $data['deviation'] = TextHelper::invoiceMoneyFormat(round($diffAmount / $amount * 100, 2) * 100, 2);
        } else {
            $data['planAmount'] = '-';
            $data['diff'] = '-';
            $data['deviation'] = '-';
        }

        return $data;
    }

    /**
     * @param $deviation
     * @return string
     */
    public function getItemColor($deviation, $isNewTheme = false)
    {
        $deviation = floatval(str_replace([' ', ','], ['', '.'],$deviation));

        $colors = [
            [
                'from' => 1,
                'to' => 10,
                'colorMinus' => ($isNewTheme) ? 'rgba(227, 6, 17, 0.05)' : '#f3565d29',
                'colorPlus' => ($isNewTheme) ? 'rgba(38, 205, 88, 0.05)' : '#45b6af1c',
            ],
            [
                'from' => 11,
                'to' => 25,
                'colorMinus' => ($isNewTheme) ? 'rgba(227, 6, 17, 0.1)' : '#f3565d4d',
                'colorPlus' => ($isNewTheme) ? 'rgba(38, 205, 88, 0.1)' : '#45b6af54',
            ],
            [
                'from' => 26,
                'to' => 50,
                'colorMinus' => ($isNewTheme) ? 'rgba(227, 6, 17, 0.15)' : '#f3565d85',
                'colorPlus' => ($isNewTheme) ? 'rgba(38, 205, 88, 0.15)' : '#45b6af8c',
            ],
            [
                'from' => 51,
                'to' => 75,
                'colorMinus' => ($isNewTheme) ? 'rgba(227, 6, 17, 0.2)' : '#f3565db8',
                'colorPlus' => ($isNewTheme) ? 'rgba(38, 205, 88, 0.2)' : '#45b6afbd',
            ],
            [
                'from' => 76,
                'colorMinus' => ($isNewTheme) ? 'rgba(227, 6, 17, 0.25)' : '#f3565d',
                'colorPlus' => ($isNewTheme) ? 'rgba(38, 205, 88, 0.25)' : '#45b6af',
            ],
        ];
        foreach ($colors as $color) {
            if (isset($color['to'])) {
                if ($deviation > 0) {
                    if ($deviation >= $color['from'] && $deviation <= $color['to']) {
                        return $color['colorPlus'];
                    }
                } else {
                    if ($deviation <= -$color['from'] && $deviation >= -$color['to']) {
                        return $color['colorMinus'];
                    }
                }
            } else {
                if ($deviation > 0 && $deviation > $color['from']) {
                    return $color['colorPlus'];
                } elseif ($deviation < 0 && $deviation < -$color['from']) {
                    return $color['colorMinus'];
                }
            }
        }

        return '#ffffff';
    }

    /**
     * @param $typeID
     * @return array
     */
    public function getItemsByActivityTypeID($typeID)
    {
        $flowType = self::$flowTypeByActivityType[$typeID];
        $block = self::$blockByType[$typeID];
        $itemIDs = self::$typeItem[$typeID];
        $params = $this->getParamsByFlowType($flowType);
        if (in_array($typeID, [self::INCOME_OPERATING_ACTIVITIES, self::WITHOUT_TYPE])) {
            if ($flowType == CashFlowsBase::FLOW_TYPE_INCOME) {
                $itemIDs = InvoiceIncomeItem::find()
                    ->andWhere(['company_id' => null])
                    ->andWhere(['not', ['in', 'id', self::$typeItem[self::RECEIPT_FINANCING_TYPE_FIRST]]])
                    ->andWhere(['not', ['id' => $params['withoutItem']]])
                    ->column();
            } else {
                $itemIDs = InvoiceExpenditureItem::find()
                    ->andWhere(['company_id' => null])
                    ->andWhere(['not', ['in', 'id', self::$typeItem[self::RECEIPT_FINANCING_TYPE_SECOND]]])
                    ->andWhere(['not', ['id' => $params['withoutItem']]])
                    ->column();
            }
        }
        $items = (new Query)
            ->select($params['flowOfFundItemName'])
            ->from($params['itemFlowOfFundsTableName'])
            ->leftJoin(['invoiceItem' => $params['invoiceItemTableName']],
                "invoiceItem.id = " . $params['itemFlowOfFundsTableName'] . ".{$params['flowOfFundItemName']}")
            ->andWhere([$params['itemFlowOfFundsTableName'] . '.company_id' => $this->company->id])
            ->andWhere(['or',
                ['flow_of_funds_block' => $block],
                ['and',
                    ['flow_of_funds_block' => null],
                    ['in', $params['itemFlowOfFundsTableName'] . '.' . $params['flowOfFundItemName'], $itemIDs],
                ],
                $typeID == self::INCOME_OPERATING_ACTIVITIES || $typeID == self::WITHOUT_TYPE ?
                    ['and',
                        ['flow_of_funds_block' => null],
                        ['not', ['invoiceItem.company_id' => null]],
                    ] : [],
            ])->column();


        return $items;
    }
}