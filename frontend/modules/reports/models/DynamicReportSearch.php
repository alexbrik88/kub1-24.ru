<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 17.11.2017
 * Time: 8:29
 */

namespace frontend\modules\reports\models;


use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use frontend\models\Documents;
use yii\data\ArrayDataProvider;
use yii\web\JsExpression;

/**
 * Class DynamicReportSearch
 * @package frontend\modules\reports\models
 */
class DynamicReportSearch extends Contractor
{
    /**
     * period ID constants
     */
    const PERIOD_DAY = 1;
    /**
     *
     */
    const PERIOD_WEEK = 2;
    /**
     *
     */
    const PERIOD_MONTH = 3;
    /**
     *
     */
    const PERIOD_QUARTER = 4;

    /**
     * @var
     */
    protected $_periodParams;
    /**
     * @var
     */
    protected $_data;

    /**
     * @var int
     */
    public $period = self::PERIOD_DAY;

    /**
     * @var array
     */
    public static $periodArray = [
        self::PERIOD_DAY => 'День',
        self::PERIOD_WEEK => 'Неделя',
        self::PERIOD_MONTH => 'Месяц',
        self::PERIOD_QUARTER => 'Квартал',
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['period'], 'filter', 'filter' => function ($value) {
                return (int)$value;
            }],
            [
                ['period'], 'in', 'range' => array_keys(self::$periodArray),
                'message' => 'Не верно выбран период.'
            ],
        ];
    }

    /**
     * @param $params
     */
    public function buildData($params)
    {
        $this->load($params, '');
        $periodParams = $this->getPeriodDates();
        foreach ($periodParams as $periodParam) {
            $query = Invoice::find()
                ->joinWith('contractor', false)
                ->select([
                    "total_amount_with_nds",
                    "CAST((UNIX_TIMESTAMP('$periodParam') - UNIX_TIMESTAMP(payment_limit_date)) / 86400 as SIGNED) as dateDiff"
                ])
                ->byCompany(\Yii::$app->user->identity->company->id)
                ->byDeleted()
                ->byIOType(Documents::IO_TYPE_OUT)
                ->andWhere(['not', [Invoice::tableName() . '.invoice_status_id' => [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_REJECTED]]])
                ->andWhere(['<=', Invoice::tableName() . '.payment_limit_date', $periodParam])
                ->asArray();
            \frontend\modules\documents\models\InvoiceSearch::searchByRoleFilter($query);
            $this->_data[$periodParam]['overdue'] = $query->all(\Yii::$app->db2);

            $query = Invoice::find()
                ->joinWith('contractor', false)
                ->byCompany(\Yii::$app->user->identity->company->id)
                ->byDeleted()
                ->byIOType(Documents::IO_TYPE_OUT)
                ->andWhere(['not', [Invoice::tableName() . '.invoice_status_id' => [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_REJECTED, InvoiceStatus::STATUS_OVERDUE]]])
                ->andWhere(['>', Invoice::tableName() . '.payment_limit_date', $periodParam])
                ->andWhere(['<=', Invoice::tableName() . '.document_date', $periodParam]);
            \frontend\modules\documents\models\InvoiceSearch::searchByRoleFilter($query);
            $this->_data[$periodParam]['current'] = $query->sum('total_amount_with_nds', \Yii::$app->db2);
        }
    }

    /**
     * @return array
     */
    public function getHighchartsOptions()
    {
        $categories = [];
        foreach ($this->_periodParams as $date) {
            $categories[] = DateHelper::format($date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        }

        $series = [
            'all' => [
                'type' => 'column',
                'name' => 'Вся задолженность',
                'pointWidth' => 17,
                'data' => [],
            ],
            'current' => [
                'type' => 'column',
                'name' => 'Текущие',
                'color' => '#45b6af',
                'pointWidth' => 17,
                'data' => [],
            ],
            10 => [
                'type' => 'column',
                'name' => '1-10 дней',
                'pointWidth' => 17,
                'data' => [],
            ],
            30 => [
                'type' => 'column',
                'name' => '11-30 дней',
                'pointWidth' => 17,
                'data' => [],
            ],
            60 => [
                'type' => 'column',
                'name' => '31-60 дней',
                'pointWidth' => 17,
                'data' => [],
            ],
            90 => [
                'type' => 'column',
                'name' => '61-90 дней',
                'pointWidth' => 17,
                'data' => [],
            ],
            'more90' => [
                'type' => 'column',
                'name' => 'Больше 90 дней',
                'pointWidth' => 17,
                'data' => [],
            ],
        ];

        foreach ($this->_data as $period => $data) {
            if (isset($data['current']) && !empty($data['current'])) {
                $series['current']['data'][$period] = (int)$data['current'];
            } else {
                $series['current']['data'][$period] = 0;
            }
            $series[10]['data'][$period] = 0;
            $series[30]['data'][$period] = 0;
            $series[60]['data'][$period] = 0;
            $series[90]['data'][$period] = 0;
            $series['all']['data'][$period] = 0;
            $series['more90']['data'][$period] = 0;
            if (isset($data['overdue']) && !empty($data['overdue'])) {
                foreach ($data['overdue'] as $oneInvoiceData) {
                    if ($oneInvoiceData['dateDiff'] > 90) {
                        $series['more90']['data'][$period] += (int)$oneInvoiceData['total_amount_with_nds'];
                    } elseif ($oneInvoiceData['dateDiff'] >= 61 && $oneInvoiceData['dateDiff'] <= 90) {
                        $series[90]['data'][$period] += (int)$oneInvoiceData['total_amount_with_nds'];
                    } elseif ($oneInvoiceData['dateDiff'] >= 31 && $oneInvoiceData['dateDiff'] <= 60) {
                        $series[60]['data'][$period] += (int)$oneInvoiceData['total_amount_with_nds'];
                    } elseif ($oneInvoiceData['dateDiff'] >= 11 && $oneInvoiceData['dateDiff'] <= 30) {
                        $series[30]['data'][$period] += (int)$oneInvoiceData['total_amount_with_nds'];
                    } elseif ($oneInvoiceData['dateDiff'] >= 1 && $oneInvoiceData['dateDiff'] <= 10) {
                        $series[10]['data'][$period] += (int)$oneInvoiceData['total_amount_with_nds'];
                    }
                    $series['all']['data'][$period] += (int)$oneInvoiceData['total_amount_with_nds'];
                }
            }
        }
        foreach ($series as $key => $oneSeries) {
            foreach ($oneSeries['data'] as $periodName => $value) {
                $oneSeries['data'][$periodName] = (float)TextHelper::moneyFormatFromIntToFloat($value);
            }
            $series[$key]['data'] = array_values($oneSeries['data']);
        }
        $series = array_values($series);

        return [
            'title' => [
                'text' => '',
            ],
            'lang' => [
                'printChart' => 'На печать',
                'downloadPNG' => 'Скачать PNG',
                'downloadJPEG' => 'Скачать JPEG',
                'downloadPDF' => 'Скачать PDF',
                'downloadSVG' => 'Скачать SVG',
                'contextButtonTitle' => 'Меню',
            ],
            'xAxis' => [
                'categories' => $categories,
            ],
            'yAxis' => [
                'min' => 0,
                'title' => ['text' => 'Сумма'],
            ],
            'series' => $series,
        ];
    }

    /**
     * @return ArrayDataProvider
     */
    public function getArrayDataTable()
    {
        $models = [];
        $key = 0;
        foreach ($this->_data as $periodDate => $data) {
            $models[$key]['period_date'] = $periodDate;
            if (isset($data['current']) && $data['current'] !== null) {
                $models[$key]['current_invoice_sum'] = $data['current'];
            } else {
                $models[$key]['current_invoice_sum'] = 0;
            }
            $models[$key]['debt_1_10'] = 0;
            $models[$key]['debt_11_30'] = 0;
            $models[$key]['debt_31_60'] = 0;
            $models[$key]['debt_61_90'] = 0;
            $models[$key]['debt_more_90'] = 0;
            $models[$key]['debt_all'] = 0;
            if (isset($data['overdue']) && !empty($data['overdue'])) {
                foreach ($data['overdue'] as $oneInvoiceData) {
                    if ($oneInvoiceData['dateDiff'] > 90) {
                        $models[$key]['debt_more_90'] += (int)$oneInvoiceData['total_amount_with_nds'];
                    } elseif ($oneInvoiceData['dateDiff'] >= 61 && $oneInvoiceData['dateDiff'] <= 90) {
                        $models[$key]['debt_61_90'] += (int)$oneInvoiceData['total_amount_with_nds'];
                    } elseif ($oneInvoiceData['dateDiff'] >= 31 && $oneInvoiceData['dateDiff'] <= 60) {
                        $models[$key]['debt_31_60'] += (int)$oneInvoiceData['total_amount_with_nds'];
                    } elseif ($oneInvoiceData['dateDiff'] >= 11 && $oneInvoiceData['dateDiff'] <= 30) {
                        $models[$key]['debt_11_30'] += (int)$oneInvoiceData['total_amount_with_nds'];
                    } elseif ($oneInvoiceData['dateDiff'] >= 1 && $oneInvoiceData['dateDiff'] <= 10) {
                        $models[$key]['debt_1_10'] += (int)$oneInvoiceData['total_amount_with_nds'];
                    }
                    $models[$key]['debt_all'] += (int)$oneInvoiceData['total_amount_with_nds'];
                }
            }
            $key++;
        }

        return new ArrayDataProvider([
            'allModels' => $models,
            'sort' => [
                'attributes' => [
                    'period_date',
                    'current_invoice_sum',
                    'debt_1_10',
                    'debt_11_30',
                    'debt_31_60',
                    'debt_61_90',
                    'debt_more_90',
                    'debt_all',
                ],
                'defaultOrder' => [
                    'period_date' => SORT_DESC,
                ],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    }

    /**
     * @return array
     */
    public function getPeriodItems()
    {
        $periodItems = [];
        foreach (self::$periodArray as $id => $name) {
            $periodItems[] = [
                'label' => $name,
                'url' => ['/reports/debt-report/dynamic', 'period' => $id],
                'options' => ($this->period == $id) ? ['class' => 'active'] : [],
                'linkOptions' => ['class' => 'dynamic-pjax-link']
            ];
        }

        return $periodItems;
    }

    /**
     * @return mixed
     */
    public function getPeriodName()
    {
        return self::$periodArray[$this->period];
    }

    /**
     * @return array
     */
    public function getPeriodDates()
    {
        switch ($this->period) {
            case self::PERIOD_DAY:
                for ($i = 13; $i >= 0; $i--) {
                    $this->_periodParams[] = date(DateHelper::FORMAT_DATE, strtotime('-' . $i . ' days'));
                }
                break;
            case self::PERIOD_WEEK:
                $lastDate = date(DateHelper::FORMAT_DATE);
                for ($i = 11; $i >= 0; $i--) {
                    $lastDate = date(DateHelper::FORMAT_DATE, strtotime('last Monday ' . $lastDate));
                    $this->_periodParams[] = $lastDate;
                }
                krsort($this->_periodParams);
                $this->_periodParams = array_values($this->_periodParams);
                break;
            case self::PERIOD_MONTH:
                $lastDate = date('Y-m-01');
                $this->_periodParams[] = date('Y-m-t');
                for ($i = 10; $i >= 0; $i--) {
                    $timestamp = strtotime('last month ' . $lastDate);
                    $lastDate = date('Y-m-01', $timestamp);
                    $this->_periodParams[] = date('Y-m-t', $timestamp);
                }
                krsort($this->_periodParams);
                $this->_periodParams = array_values($this->_periodParams);
                break;
            case self::PERIOD_QUARTER:
                $quarterNumber = (int)ceil(date('m') / 3);
                $currentQuarterEndMonth = $quarterNumber * 3;
                $currentEndQuarterTimestamp = strtotime('01.' . $currentQuarterEndMonth . '.' . date('Y'));
                $currentEndQuarterDate = date('Y-m-t', $currentEndQuarterTimestamp);
                $this->_periodParams[] = $currentEndQuarterDate;
                for ($i = 10; $i >= 0; $i--) {
                    $currentEndQuarterTimestamp = strtotime('-3 month ' . date(DateHelper::FORMAT_DATE, $currentEndQuarterTimestamp));
                    $currentEndQuarterDate = date('Y-m-t', $currentEndQuarterTimestamp);
                    $this->_periodParams[] = $currentEndQuarterDate;
                }
                krsort($this->_periodParams);
                $this->_periodParams = array_values($this->_periodParams);
        }

        return $this->_periodParams;
    }
}