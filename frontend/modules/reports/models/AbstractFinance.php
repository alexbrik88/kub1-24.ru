<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 28.02.2019
 * Time: 0:48
 */

namespace frontend\modules\reports\models;


use common\components\excel\Excel;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\document\OrderAct;
use common\models\document\OrderPackingList;
use common\models\document\OrderUpd;
use common\models\document\PackingList;
use common\models\document\Upd;
use common\models\ExpenseItemFlowOfFunds;
use common\models\IncomeItemFlowOfFunds;
use frontend\modules\cash\models\CashContractorType;
use yii\base\InvalidParamException;
use yii\base\Model;
use common\models\Company;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class AbstractFinance
 * @package frontend\modules\reports\models
 *
 * @property Company $company
 * @property bool $isCurrentYear
 */
abstract class AbstractFinance extends Model
{
    const JANUARY = '01';
    const FEBRUARY = '02';
    const MARCH = '03';
    const APRIL = '04';
    const MAY = '05';
    const JUNE = '06';
    const JULY = '07';
    const AUGUST = '08';
    const SEPTEMBER = '09';
    const OCTOBER = '10';
    const NOVEMBER = '11';
    const DECEMBER = '12';

    const RECEIPT_FINANCING_TYPE_FIRST = 1;
    const RECEIPT_FINANCING_TYPE_SECOND = 2;
    const INCOME_OPERATING_ACTIVITIES = 3;
    const WITHOUT_TYPE = 0;
    const INCOME_INVESTMENT_ACTIVITIES = 4;
    const EXPENSE_INVESTMENT_ACTIVITIES = 5;

    const INCOME_CASH_BANK = 1;
    const EXPENSE_CASH_BANK = 2;
    const INCOME_CASH_ORDER = 3;
    const EXPENSE_CASH_ORDER = 4;
    const INCOME_CASH_EMONEY = 5;
    const EXPENSE_CASH_EMONEY = 6;
    const INCOME_ACQUIRING = 7;
    const EXPENSE_ACQUIRING = 8;

    const FINANCIAL_OPERATIONS_BLOCK = 1;
    const OPERATING_ACTIVITIES_BLOCK = 2;
    const INVESTMENT_ACTIVITIES_BLOCK = 3;

    const CASH_BANK_BLOCK = 1;
    const CASH_ORDER_BLOCK = 2;
    const CASH_EMONEY_BLOCK = 3;
    const CASH_ACQUIRING_BLOCK = 4;

    const GROUP_DATE = 1;
    const GROUP_CONTRACTOR = 2;
    const GROUP_PAYMENT_TYPE = 3;

    const TYPE_BY_ACTIVITY = 1;
    const TYPE_BY_PURSE = 2;

    const ACT_BLOCK = 1;
    const PACKING_LIST_BLOCK = 2;
    const UPD_BLOCK = 3;

    const MOVEMENT_TYPE_FLOWS = 'flows';
    const MOVEMENT_TYPE_DOCS = 'docs';
    const MOVEMENT_TYPE_DOCS_AND_FLOWS = 'docs_flows';

    /**
     * @var array
     */
    public static $month = [
        self::JANUARY => 'Январь',
        self::FEBRUARY => 'Февраль',
        self::MARCH => 'Март',
        self::APRIL => 'Апрель',
        self::MAY => 'Май',
        self::JUNE => 'Июнь',
        self::JULY => 'Июль',
        self::AUGUST => 'Август',
        self::SEPTEMBER => 'Сентябрь',
        self::OCTOBER => 'Октябрь',
        self::NOVEMBER => 'Ноябрь',
        self::DECEMBER => 'Декабрь',
    ];

    /**
     * @var array
     */
    public static $monthIn = [
        self::JANUARY => 'январе',
        self::FEBRUARY => 'феврале',
        self::MARCH => 'марте',
        self::APRIL => 'апреле',
        self::MAY => 'мае',
        self::JUNE => 'июне',
        self::JULY => 'июле',
        self::AUGUST => 'августе',
        self::SEPTEMBER => 'сентябре',
        self::OCTOBER => 'октябре',
        self::NOVEMBER => 'ноябре',
        self::DECEMBER => 'декабре',
    ];

    /**
     * @var array
     */
    public static $monthEnd = [
        self::JANUARY => 'января',
        self::FEBRUARY => 'февраля',
        self::MARCH => 'марта',
        self::APRIL => 'апреля',
        self::MAY => 'мая',
        self::JUNE => 'июня',
        self::JULY => 'июля',
        self::AUGUST => 'августа',
        self::SEPTEMBER => 'сентября',
        self::OCTOBER => 'октября',
        self::NOVEMBER => 'ноября',
        self::DECEMBER => 'декабря',
    ];

    /**
     * @var array
     */
    public static $blocks = [
        self::FINANCIAL_OPERATIONS_BLOCK => 'Финансовая деятельность',
        self::OPERATING_ACTIVITIES_BLOCK => 'Операционная деятельность',
        self::INVESTMENT_ACTIVITIES_BLOCK => 'Инвестиционная деятельность',
    ];

    /**
     * @var array
     */
    public static $purseBlocks = [
        self::CASH_BANK_BLOCK => 'Банк',
        self::CASH_ORDER_BLOCK => 'Касса',
        self::CASH_EMONEY_BLOCK => 'E-money',
    ];

    /**
     * @var array
     */
    public static $purseBlocksClass = [
        self::CASH_BANK_BLOCK => CashBankFlows::class,
        self::CASH_ORDER_BLOCK => CashOrderFlows::class,
        self::CASH_EMONEY_BLOCK => CashEmoneyFlows::class
    ];

    public static $purseTypesClass = [
        self::INCOME_CASH_BANK => CashBankFlows::class,
        self::EXPENSE_CASH_BANK => CashBankFlows::class,
        self::INCOME_CASH_ORDER => CashOrderFlows::class,
        self::EXPENSE_CASH_ORDER => CashOrderFlows::class,
        self::INCOME_CASH_EMONEY => CashEmoneyFlows::class,
        self::EXPENSE_CASH_EMONEY => CashEmoneyFlows::class
    ];

    /**
     * @var array
     */
    public static $types = [
        self::INCOME_OPERATING_ACTIVITIES => 'Приход',
        self::WITHOUT_TYPE => 'Расход',
        self::RECEIPT_FINANCING_TYPE_FIRST => 'Приход',
        self::RECEIPT_FINANCING_TYPE_SECOND => 'Расход',
        self::INCOME_INVESTMENT_ACTIVITIES => 'Приход',
        self::EXPENSE_INVESTMENT_ACTIVITIES => 'Расход',
    ];

    /**
     * @var array
     */
    public static $purseTypes = [
        self::INCOME_CASH_BANK => 'Приход',
        self::EXPENSE_CASH_BANK => 'Расход',
        self::INCOME_CASH_ORDER => 'Приход',
        self::EXPENSE_CASH_ORDER => 'Расход',
        self::INCOME_CASH_EMONEY => 'Приход',
        self::EXPENSE_CASH_EMONEY => 'Расход',
        self::INCOME_ACQUIRING => 'Приход',
        self::EXPENSE_ACQUIRING => 'Расход',
    ];

    /**
     * @var array
     */
    public static $purseGrowingBalanceLabelByType = [
        self::EXPENSE_CASH_BANK => 'Остаток по банку',
        self::EXPENSE_CASH_ORDER => 'Остаток по кассе',
        self::EXPENSE_CASH_EMONEY => 'Остаток по E-money',
    ];

    /**
     * @var array
     */
    public static $growingBalanceLabelByType = [
        self::RECEIPT_FINANCING_TYPE_SECOND => 'Остаток по фин. деятельности',
        self::WITHOUT_TYPE => 'Остаток по оп. деятельности',
        self::EXPENSE_INVESTMENT_ACTIVITIES => 'Остаток по инвест. деятельности',
    ];

    /**
     * @var array
     */
    public static $blockByType = [
        self::RECEIPT_FINANCING_TYPE_FIRST => self::FINANCIAL_OPERATIONS_BLOCK,
        self::RECEIPT_FINANCING_TYPE_SECOND => self::FINANCIAL_OPERATIONS_BLOCK,
        self::INCOME_OPERATING_ACTIVITIES => self::OPERATING_ACTIVITIES_BLOCK,
        self::WITHOUT_TYPE => self::OPERATING_ACTIVITIES_BLOCK,
        self::INCOME_INVESTMENT_ACTIVITIES => self::INVESTMENT_ACTIVITIES_BLOCK,
        self::EXPENSE_INVESTMENT_ACTIVITIES => self::INVESTMENT_ACTIVITIES_BLOCK,
    ];

    /**
     * @var array
     */
    public static $purseBlockByType = [
        self::INCOME_CASH_BANK => self::CASH_BANK_BLOCK,
        self::EXPENSE_CASH_BANK => self::CASH_BANK_BLOCK,
        self::INCOME_CASH_ORDER => self::CASH_ORDER_BLOCK,
        self::EXPENSE_CASH_ORDER => self::CASH_ORDER_BLOCK,
        self::INCOME_CASH_EMONEY => self::CASH_EMONEY_BLOCK,
        self::EXPENSE_CASH_EMONEY => self::CASH_EMONEY_BLOCK,
        self::INCOME_ACQUIRING => self::CASH_ACQUIRING_BLOCK,
        self::EXPENSE_ACQUIRING => self::CASH_ACQUIRING_BLOCK,
    ];

    /**
     * @var array
     */
    public static $flowTypeByPurseType = [
        self::INCOME_CASH_BANK => CashFlowsBase::FLOW_TYPE_INCOME,
        self::INCOME_CASH_ORDER => CashFlowsBase::FLOW_TYPE_INCOME,
        self::INCOME_CASH_EMONEY => CashFlowsBase::FLOW_TYPE_INCOME,
        self::INCOME_ACQUIRING => CashFlowsBase::FLOW_TYPE_INCOME,
        self::EXPENSE_CASH_BANK => CashFlowsBase::FLOW_TYPE_EXPENSE,
        self::EXPENSE_CASH_ORDER => CashFlowsBase::FLOW_TYPE_EXPENSE,
        self::EXPENSE_CASH_EMONEY => CashFlowsBase::FLOW_TYPE_EXPENSE,
        self::EXPENSE_ACQUIRING => CashFlowsBase::FLOW_TYPE_EXPENSE,
    ];

    /**
     * @var array
     */
    public static $flowTypeByActivityType = [
        self::RECEIPT_FINANCING_TYPE_FIRST => CashFlowsBase::FLOW_TYPE_INCOME,
        self::INCOME_OPERATING_ACTIVITIES => CashFlowsBase::FLOW_TYPE_INCOME,
        self::INCOME_INVESTMENT_ACTIVITIES => CashFlowsBase::FLOW_TYPE_INCOME,
        self::RECEIPT_FINANCING_TYPE_SECOND => CashFlowsBase::FLOW_TYPE_EXPENSE,
        self::WITHOUT_TYPE => CashFlowsBase::FLOW_TYPE_EXPENSE,
        self::EXPENSE_INVESTMENT_ACTIVITIES => CashFlowsBase::FLOW_TYPE_EXPENSE,
    ];

    /**
     * @var array
     */
    public static $cashContractorTypeByTableName = [
        CashBankFlows::class => [
            CashContractorType::ORDER_TEXT,
            CashContractorType::EMONEY_TEXT,
        ],
        CashOrderFlows::class => [
            CashContractorType::BANK_TEXT,
            CashContractorType::EMONEY_TEXT,
        ],
        CashEmoneyFlows::class => [
            CashContractorType::ORDER_TEXT,
            CashContractorType::BANK_TEXT,
        ],
    ];

    /**
     * @var array
     */
    public static $cashContractorTextByType = [
        CashFlowsBase::FLOW_TYPE_INCOME => [
            CashContractorType::BANK_TEXT => 'Банка',
            CashContractorType::ORDER_TEXT => 'Кассы',
            CashContractorType::EMONEY_TEXT => 'E-money',
        ],
        CashFlowsBase::FLOW_TYPE_EXPENSE => [
            CashContractorType::BANK_TEXT => 'Банк',
            CashContractorType::ORDER_TEXT => 'Кассу',
            CashContractorType::EMONEY_TEXT => 'E-money',
        ],
    ];

    /**
     * @var array
     */
    public static $typeItem = [
        AbstractFinance::INCOME_OPERATING_ACTIVITIES => [],
        AbstractFinance::WITHOUT_TYPE => [],
        AbstractFinance::RECEIPT_FINANCING_TYPE_FIRST => [
            InvoiceIncomeItem::ITEM_LOAN,
            InvoiceIncomeItem::ITEM_CREDIT,
            InvoiceIncomeItem::ITEM_FROM_FOUNDER,
            InvoiceIncomeItem::ITEM_BORROWING_REDEMPTION,
            InvoiceIncomeItem::ITEM_RECEIVED_PERCENTS,
            InvoiceIncomeItem::ITEM_RECEIVED_PERCENTS_BY_DEPOSITS,
            InvoiceIncomeItem::ITEM_INPUT_MONEY,
            InvoiceIncomeItem::ITEM_BORROWING_DEPOSIT,
        ],
        AbstractFinance::RECEIPT_FINANCING_TYPE_SECOND => [
            InvoiceExpenditureItem::ITEM_LOAN_REPAYMENT,
            InvoiceExpenditureItem::ITEM_REPAYMENT_CREDIT,
            InvoiceExpenditureItem::ITEM_PAYMENT_PERCENT,
            InvoiceExpenditureItem::ITEM_RETURN_FOUNDER,
            InvoiceExpenditureItem::ITEM_BORROWING_EXTRADITION,
            InvoiceExpenditureItem::ITEM_DIVIDEND_PAYMENT,
            InvoiceExpenditureItem::ITEM_OUTPUT_MONEY,
            InvoiceExpenditureItem::ITEM_OUTPUT_PROFIT,
            InvoiceExpenditureItem::ITEM_DEPOSIT,
        ],
        AbstractFinance::INCOME_INVESTMENT_ACTIVITIES => [],
        AbstractFinance::EXPENSE_INVESTMENT_ACTIVITIES => [
            InvoiceExpenditureItem::ITEM_BALANCE_ARTICLE_FIXED,
            InvoiceExpenditureItem::ITEM_BALANCE_ARTICLE_INTANGIBLE,
        ],
    ];

    public static $typesByBlock = [
        self::FINANCIAL_OPERATIONS_BLOCK => [
            self::RECEIPT_FINANCING_TYPE_FIRST,
            self::RECEIPT_FINANCING_TYPE_SECOND
        ],
        self::OPERATING_ACTIVITIES_BLOCK => [
            self::INCOME_OPERATING_ACTIVITIES,
            self::WITHOUT_TYPE
        ],
        self::INVESTMENT_ACTIVITIES_BLOCK => [
            self::INCOME_INVESTMENT_ACTIVITIES,
            self::EXPENSE_INVESTMENT_ACTIVITIES
        ]
    ];

    /**
     * @var int
     */
    public $year = null;

    /**
     * @var
     */
    public $expenditure_item_id;

    /**
     * @var
     */
    public $income_item_id;

    /**
     * @var
     */
    public $cash_contractor;

    /**
     * @var Company
     */
    protected $_company = null;

    /**
     * @var ActiveQuery
     */
    protected $_filterQuery;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['year',], 'integer'],
        ];
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->year = \Yii::$app->session->get('modules.reports.finance.year', date('Y'));
        $this->_company = Yii::$app->user->identity->company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @return bool
     */
    public function getIsCurrentYear()
    {
        return date('Y') == $this->year;
    }

    /**
     * @param $flowType
     * @return mixed
     */
    public function getParamsByFlowType($flowType)
    {
        $result['flowType'] = $flowType;
        if ($flowType == CashFlowsBase::FLOW_TYPE_INCOME) {
            $result['itemFlowOfFundsTableName'] = IncomeItemFlowOfFunds::tableName();
            $result['invoiceItemTableName'] = InvoiceIncomeItem::tableName();
            $result['itemAttrName'] = $result['flowOfFundItemName'] = 'income_item_id';
            $result['withoutItem'] = InvoiceIncomeItem::ITEM_OWN_FOUNDS;
            $result['flowTypeText'] = 'ПРИХОДЫ';
            $result['prevSumAttrName'] = 'incomePrevSum';
        } elseif ($flowType == CashFlowsBase::FLOW_TYPE_EXPENSE) {
            $result['itemFlowOfFundsTableName'] = ExpenseItemFlowOfFunds::tableName();
            $result['invoiceItemTableName'] = InvoiceExpenditureItem::tableName();
            $result['itemAttrName'] = 'expenditure_item_id';
            $result['flowOfFundItemName'] = 'expense_item_id';
            $result['withoutItem'] = InvoiceExpenditureItem::ITEM_OWN_FOUNDS;
            $result['flowTypeText'] = 'РАСХОДЫ';
            $result['prevSumAttrName'] = 'expenditurePrevSum';
        } else {
            throw new InvalidParamException('Invalid flow type param.');
        }

        return $result;
    }

    /**
     * @param $flowType
     * @return array
     */
    public function getItemsByFlowType($flowType)
    {
        if ($flowType == CashFlowsBase::FLOW_TYPE_INCOME) {
            $items = InvoiceIncomeItem::find()
                ->andWhere(['or',
                    ['company_id' => null],
                    ['company_id' => $this->company->id],
                ])
                ->andWhere(['not', ['id' => InvoiceIncomeItem::ITEM_OWN_FOUNDS]])
                ->column();
        } elseif ($flowType == CashFlowsBase::FLOW_TYPE_EXPENSE) {
            $items = InvoiceExpenditureItem::find()
                ->andWhere(['or',
                    ['company_id' => null],
                    ['company_id' => $this->company->id],
                ])
                ->andWhere(['not', ['id' => InvoiceExpenditureItem::ITEM_OWN_FOUNDS]])
                ->column();
        } else {
            throw new InvalidParamException('Invalid flow type param.');
        }

        return $items;
    }

    /**
     * @param $className CashBankFlows|CashOrderFlows|CashEmoneyFlows|PlanCashFlows
     * @param $flowTypeParams
     * @param $dateFrom
     * @param $dateTo
     * @return ActiveQuery
     */
    public function getDefaultCashFlowsQuery($className, $flowTypeParams, $dateFrom, $dateTo)
    {
        return $className::find()
            ->leftJoin(['itemFlowOfFunds' => $flowTypeParams['itemFlowOfFundsTableName']],
                "itemFlowOfFunds.{$flowTypeParams['flowOfFundItemName']} = " . $className::tableName() . ".{$flowTypeParams['itemAttrName']} AND
                itemFlowOfFunds.company_id = " . $this->company->id)
            ->leftJoin(['invoiceItem' => $flowTypeParams['invoiceItemTableName']],
                "invoiceItem.id = " . $className::tableName() . ".{$flowTypeParams['itemAttrName']}")
            ->andWhere(['not', [$className::tableName() . ".{$flowTypeParams['itemAttrName']}" => $flowTypeParams['withoutItem']]])
            ->andWhere(['between', $className::tableName() . '.date', $dateFrom, $dateTo])
            ->andWhere([$className::tableName() . '.company_id' => $this->company->id]);
    }

    /**
     * @param ActiveQuery $query
     * @param $flowTypeParams
     * @param $typeID
     * @return ActiveQuery
     */
    public function filterByActivityQuery(ActiveQuery $query, $flowTypeParams, $typeID)
    {
        $block = self::$blockByType[$typeID];
        $itemIDs = self::$typeItem[$typeID];
        if (in_array($typeID, [self::INCOME_OPERATING_ACTIVITIES, self::WITHOUT_TYPE])) {
            if ($flowTypeParams['flowType'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                $itemIDs = InvoiceIncomeItem::find()
                    ->andWhere(['company_id' => null])
                    ->andWhere(['not', ['in', 'id', self::$typeItem[self::RECEIPT_FINANCING_TYPE_FIRST]]])
                    ->andWhere(['not', ['id' => $flowTypeParams['withoutItem']]])
                    ->column();
            } else {
                $itemIDs = InvoiceExpenditureItem::find()
                    ->andWhere(['company_id' => null])
                    ->andWhere(['not', ['in', 'id', self::$typeItem[self::RECEIPT_FINANCING_TYPE_SECOND]]])
                    ->andWhere(['not', ['id' => $flowTypeParams['withoutItem']]])
                    ->column();
            }
        }

        return $query->andWhere(['and',
            ['not', ['contractor_id' => [
                CashContractorType::BANK_TEXT,
                CashContractorType::ORDER_TEXT,
                CashContractorType::EMONEY_TEXT,
                //CashContractorType::BALANCE_TEXT,
            ]]],
            ['or',
                ['itemFlowOfFunds.flow_of_funds_block' => $block],
                ['and',
                    ['itemFlowOfFunds.flow_of_funds_block' => null],
                    ['in', "itemFlowOfFunds.{$flowTypeParams['flowOfFundItemName']}", $itemIDs],
                ],
                $typeID == self::INCOME_OPERATING_ACTIVITIES || $typeID == self::WITHOUT_TYPE ?
                    ['and',
                        ['itemFlowOfFunds.flow_of_funds_block' => null],
                        ['not', ['invoiceItem.company_id' => null]],
                    ] : [],
            ],
        ]);
    }

    /**
     * @param ActiveQuery $query
     * @param $flowTypeParams
     * @param $type
     * @return ActiveQuery
     */
    public function filterByPurseQuery(ActiveQuery $query)
    {
        return $query->andWhere(['not',
                ['contractor_id' =>
                    [
                        CashContractorType::BANK_TEXT,
                        CashContractorType::ORDER_TEXT,
                        CashContractorType::EMONEY_TEXT,
                    ],
                ],
            ]
        )->andWhere(['or',
            ['invoiceItem.company_id' => null],
            ['invoiceItem.company_id' => $this->company->id],
        ]);
    }

    /**
     * @param $className CashBankFlows|CashOrderFlows|CashEmoneyFlows|PlanCashFlows
     * @param $flowType
     * @param $contractors
     * @param $dateFrom
     * @param $dateTo
     * @return ActiveQuery
     */
    public function getDefaultItemsByCashFlowContractorQuery($className, $flowType, $contractors, $dateFrom, $dateTo)
    {
        return $className::find()
            ->select([
                $className::tableName() . '.contractor_id as contractorType',
                'SUM(' . $className::tableName() . '.amount) as flowSum',
                'date',
            ])->andWhere(['and',
                ['flow_type' => $flowType],
                ['in', 'contractor_id', $contractors,],
                [$className::tableName() . '.company_id' => $this->company->id],
                ['between', $className::tableName() . '.date', $dateFrom, $dateTo],
            ]);
    }

    /**
     * @param $className CashBankFlows|CashOrderFlows|CashEmoneyFlows|PlanCashFlows
     * @param $from
     * @param $to
     * @return mixed
     */
    public function getCashFlowsContractorBalanceAmount($className, $from, $to)
    {
        return $className::find()
            ->andWhere(['company_id' => $this->company->id])
            ->andWhere(['contractor_id' => CashContractorType::BALANCE_TEXT])
            ->andWhere(['between', 'date', $from, $to])
            ->sum('amount');
    }

    /**
     * @return array
     */
    public function getContractorFilterItems()
    {
        $query = clone $this->_filterQuery;

        $tContractor = Contractor::tableName();
        $tCashContractor = CashContractorType::tableName();
        $contractorIdArray = $query
            ->distinct()
            ->select("t.contractor_id")
            ->column();
        $cashContractorArray = ArrayHelper::map(
            CashContractorType::find()
                ->andWhere(["$tCashContractor.name" => $contractorIdArray])
                ->all(),
            'name',
            'text'
        );
        $contractorArray = ArrayHelper::map(
            Contractor::getSorted()
                ->andWhere(["$tContractor.id" => $contractorIdArray])
                ->all(),
            'id',
            'shortName'
        );

        return (['' => 'Все контрагенты'] + $cashContractorArray + $contractorArray);
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function getReasonFilterItems()
    {
        $query = clone $this->_filterQuery;
        $e_reasonArray = $query
            ->distinct()
            ->select(new Expression('
                CONCAT("e_", `expenditure`.`id`) `reason_id`, 
                IF (LENGTH(`expenditureParent`.`name`) > 0, CONCAT(`expenditureParent`.`name`, " - ", `expenditure`.`name`), `expenditure`.`name`) `reason_name`
            '))
            ->leftJoin(InvoiceExpenditureItem::tableName() . ' `expenditure`', '`expenditure`.`id` = `expenditure_item_id`')
            ->leftJoin(InvoiceExpenditureItem::tableName() . ' `expenditureParent`', '`expenditureParent`.`id` = `expenditure`.`parent_id`')
            ->andWhere(['not', ['expenditure.id' => null]])
            ->createCommand()
            ->queryAll();

        $query = clone $this->_filterQuery;
        $i_reasonArray = $query
            ->distinct()
            ->select(new Expression('
                CONCAT("i_", `income`.`id`) `reason_id`, 
                IF (LENGTH(`incomeParent`.`name`) > 0, CONCAT(`incomeParent`.`name`, " - ", `income`.`name`), `income`.`name`) `reason_name`
            '))
            ->leftJoin(InvoiceIncomeItem::tableName() . ' `income`', '`income`.`id` = `income_item_id`')
            ->leftJoin(InvoiceIncomeItem::tableName() . ' `incomeParent`', '`incomeParent`.`id` = `income`.`parent_id`')
            ->andWhere(['not', ['income.id' => null]])
            ->createCommand()
            ->queryAll();

        $reasonArray = ArrayHelper::map(array_merge($e_reasonArray, $i_reasonArray), 'reason_id', 'reason_name');
        natsort($reasonArray);

        return $reasonArray;
    }

    /**
     *
     */
    public function checkCashContractor()
    {
        foreach ([CashContractorType::BANK_TEXT, CashContractorType::ORDER_TEXT, CashContractorType::EMONEY_TEXT] as $cashContractor) {
            if (isset($this->income_item_id[$cashContractor])) {
                $this->cash_contractor[$cashContractor] = $cashContractor;
            }
            if (isset($this->expenditure_item_id[$cashContractor])) {
                $this->cash_contractor[$cashContractor] = $cashContractor;
            }
        }
    }

    /**
     * @return array
     */
    public static function flowClassArray()
    {
        return [
            CashBankFlows::className(),
            CashOrderFlows::className(),
            CashEmoneyFlows::className(),
        ];
    }

    /**
     * @return array
     */
    public static function docClassArray()
    {
        return [
            'acts' => Act::className(),
            'packingLists' => PackingList::className(),
            'upds' => Upd::className(),
        ];
    }

    public static $docOrdersTables = [
        'acts' => [
            'class' => OrderAct::class,
            'name' => 'order_act',
            'key' => 'act_id'
        ],
        'packingLists' => [
            'class' => OrderPackingList::class,
            'name' => 'order_packing_list',
            'key' => 'packing_list_id'
        ],
        'upds' => [
            'class' => OrderUpd::class,
            'name' => 'order_upd',
            'key' => 'upd_id'
        ]
    ];

    /**
     * @param $itemName
     * @param $data
     * @return mixed
     */
    public function buildItem($itemName, $data)
    {
        $itemData['itemName'] = $itemName;
        foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText) {
            if (isset($data[$monthNumber])) {
                if (is_array($data[$monthNumber])) {
                    $itemData["item{$monthNumber}"] = $data[$monthNumber]['flowSum'] / 100;
                } else {
                    $itemData["item{$monthNumber}"] = $data[$monthNumber] / 100;
                }
            } else {
                $itemData["item{$monthNumber}"] = 0;
            }
        }
        if (isset($data['totalFlowSum'])) {
            if (is_array($data['totalFlowSum'])) {
                $itemData['dataYear'] = isset($data['totalFlowSum']['flowSum']) ? $data['totalFlowSum']['flowSum'] / 100 : 0;
            } else {
                $itemData['dataYear'] = $data['totalFlowSum'] / 100;
            }
        } else {
            $itemData['dataYear'] = 0;
        }

        return $itemData;
    }

    /**
     * @param $blockByType
     * @param $incomeBlocks
     * @param $result
     * @return mixed
     */
    public function calculateBlocksSum($blockByType, $incomeBlocks, $result)
    {
        $result['blocks'] = [];
        if (isset($result['types'])) {
            foreach ($result['types'] as $key => $data) {
                foreach ($data as $month => $value) {
                    $formattedValue = $month == 'totalFlowSum' ? $value : $value['flowSum'];
                    if (!isset($result['blocks'][$blockByType[$key]][$month]['flowSum'])) {
                        $result['blocks'][$blockByType[$key]][$month]['flowSum'] = 0;
                    }
                    if (in_array($key, $incomeBlocks)) {
                        $result['blocks'][$blockByType[$key]][$month]['flowSum'] += $formattedValue;
                    } else {
                        $result['blocks'][$blockByType[$key]][$month]['flowSum'] -= $formattedValue;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param $blocks
     * @param $result
     * @param $prevSum
     * @return mixed
     */
    public function calculateGrowingBalanceByBlock($blocks, $result, $prevSum)
    {
        foreach ($blocks as $blockType => $blockName) {
            foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText) {
                $formattedValue = isset($result['blocks'][$blockType][$monthNumber]['flowSum']) ?
                    $result['blocks'][$blockType][$monthNumber]['flowSum'] : 0;
                $prevFlowSum = 0;
                if (!isset($growingBalanceByBlock[$blockType][$monthNumber]['flowSum'])) {
                    $result['growingBalanceByBlock'][$blockType][$monthNumber]['flowSum'] = $formattedValue;
                }
                $prevMonth = $monthNumber - 1;
                if ($prevMonth === 0) {
                    if (isset($prevSum[$blockType]['incomePrevSum'])) {
                        $prevFlowSum += $prevSum[$blockType]['incomePrevSum'];
                    }
                    if (isset($prevSum[$blockType]['expenditurePrevSum'])) {
                        $prevFlowSum -= $prevSum[$blockType]['expenditurePrevSum'];
                    }
                } else {
                    $prevMonth = $prevMonth < 10 ? (0 . $prevMonth) : $prevMonth;
                    $prevFlowSum = $result['growingBalanceByBlock'][$blockType][$prevMonth]['flowSum'];
                }
                $result['growingBalanceByBlock'][$blockType][$monthNumber]['flowSum'] += $prevFlowSum;
            }
        }

        return $result;
    }

    /**
     * @param $blocks
     * @param $result
     * @param $prevSum
     * @return mixed
     */
    public function calculateGrowingBalanceByBlockByActivity($blocks, $result, $prevSum, $factBalanceContractor = 0)
    {
        foreach ($blocks as $blockType => $blockName) {
            foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText) {

                $formattedValue = isset($result['blocks'][$blockType][$monthNumber]['flowSum']) ? $result['blocks'][$blockType][$monthNumber]['flowSum'] : 0;

                if ($isFactMonth = (int)$monthNumber <= date('m') && $this->year == date('Y'))
                    $formattedValue = 0;

                $prevFlowSum = 0;
                if (!isset($growingBalanceByBlock[$blockType][$monthNumber]['flowSum'])) {
                    $result['growingBalanceByBlock'][$blockType][$monthNumber]['flowSum'] = $formattedValue;
                }

                $prevMonth = $monthNumber - 1;
                if ($prevMonth === 0) {
                    if (isset($prevSum[$blockType]['incomePrevSum'])) {
                        $prevFlowSum += $prevSum[$blockType]['incomePrevSum'];
                    }
                    if (isset($prevSum[$blockType]['expenditurePrevSum'])) {
                        $prevFlowSum -= $prevSum[$blockType]['expenditurePrevSum'];
                    }
                } else {
                    $prevMonth = $prevMonth < 10 ? (0 . $prevMonth) : $prevMonth;
                    $prevFlowSum = $result['growingBalanceByBlock'][$blockType][$prevMonth]['flowSum'];
                }

                $result['growingBalanceByBlock'][$blockType][$monthNumber]['flowSum'] += $prevFlowSum;
            }
        }

        // start fact balance
        foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText) {
            $isCurrMonth = (int)$monthNumber == date('m') && $this->year == date('Y');
            $isPlanMonth = (int)$monthNumber > date('m') && $this->year == date('Y');
            if ($isCurrMonth || $isPlanMonth) {
                $result['growingBalanceByBlock'][self::OPERATING_ACTIVITIES_BLOCK][$monthNumber]['flowSum'] += $factBalanceContractor;
            }
        }

        return $result;
    }

    /**
     * @param $blocks
     * @param $result
     * @param $prevSum
     * @return mixed
     */
    public function calculateGrowingBalanceByBlockByPurse($blocks, $result, $prevSum, $year)
    {
        foreach ($blocks as $blockType => $blockName) {

            $purseBlocksClass = new self::$purseBlocksClass[$blockType];
            $todayFactGrowingBalance = $purseBlocksClass->getBalanceAtEnd(new \DateTime(), $this->company);
            $currMonthEndPlanBalance = 0;

            foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText) {

                $isPlanMonth = (int)$monthNumber > date('m');
                $isCurrMonth = (int)$monthNumber == date('m') && $year >= date('Y');

                if ($isCurrMonth)
                    $formattedValue = $todayFactGrowingBalance + $currMonthEndPlanBalance;
                elseif ($isPlanMonth)
                    $formattedValue = (isset($result['blocks'][$blockType][$monthNumber]['flowSum'])) ?
                        $result['blocks'][$blockType][$monthNumber]['flowSum'] : 0;
                else
                    $formattedValue = 0;

                $prevFlowSum = 0;
                if (!isset($growingBalanceByBlock[$blockType][$monthNumber]['flowSum'])) {
                    $result['growingBalanceByBlock'][$blockType][$monthNumber]['flowSum'] = $formattedValue;
                }

                $prevMonth = $monthNumber - 1;
                if ($prevMonth === 0) {
                    if (isset($prevSum[$blockType]['incomePrevSum'])) {
                        $prevFlowSum += $prevSum[$blockType]['incomePrevSum'];
                    }
                    if (isset($prevSum[$blockType]['expenditurePrevSum'])) {
                        $prevFlowSum -= $prevSum[$blockType]['expenditurePrevSum'];
                    }
                } else {
                    $prevMonth = $prevMonth < 10 ? (0 . $prevMonth) : $prevMonth;
                    $prevFlowSum = $result['growingBalanceByBlock'][$blockType][$prevMonth]['flowSum'];
                }
                $result['growingBalanceByBlock'][$blockType][$monthNumber]['flowSum'] += $prevFlowSum;
            }
        }

        return $result;
    }

    /**
     * @param $type
     * @param $data
     * @param $title
     */
    public function buildXls($type, $data, $title)
    {
        $formattedData = [];
        if (in_array($type, [
            PaymentCalendarSearch::TAB_BY_ACTIVITY,
            FlowOfFundsReportSearch::TAB_ODDS,
            PlanFactSearch::TAB_BY_ACTIVITY
        ])) {
            $oddsTypes = self::$types;
            $incomeTypes = [
                self::RECEIPT_FINANCING_TYPE_FIRST,
                self::INCOME_OPERATING_ACTIVITIES,
                self::INCOME_INVESTMENT_ACTIVITIES
            ];
            $oddsBlocks = self::$blocks;
            $blockByType = self::$blockByType;
            $growingBalanceLabel = self::$growingBalanceLabelByType;
        } elseif (in_array($type, [
            PaymentCalendarSearch::TAB_BY_PURSE,
            FlowOfFundsReportSearch::TAB_ODDS_BY_PURSE,
            PlanFactSearch::TAB_BY_PURSE
        ])) {
            $oddsTypes = self::$purseTypes;
            $incomeTypes = [
                self::INCOME_CASH_BANK,
                self::INCOME_CASH_ORDER,
                self::INCOME_CASH_EMONEY
            ];
            $oddsBlocks = self::$purseBlocks;
            $blockByType = self::$purseBlockByType;
            $growingBalanceLabel = self::$purseGrowingBalanceLabelByType;
        } else {
            throw new InvalidParamException('Invalid type param.');
        }
        $blocks = isset($data['blocks']) ? $data['blocks'] : [];
        $types = isset($data['types']) ? $data['types'] : [];
        $expenditureItems = isset($data['itemName']) ? $data['itemName'] : [];
        $balance = isset($data['balance']) ? $data['balance'] : 0;
        $growingBalance = isset($data['growingBalance']) ? $data['growingBalance'] : 0;
        asort($expenditureItems);

        foreach ($oddsTypes as $typeID => $typeName) {
            $class = in_array($typeID, $incomeTypes) ? 'income' : 'expense';
            if ($class == 'income') {
                $itemData = isset($blocks[$blockByType[$typeID]]) ? $blocks[$blockByType[$typeID]] : [];
                $formattedData[] = $this->buildItem($oddsBlocks[$blockByType[$typeID]], $itemData);
            }
            $itemData = isset($types[$typeID]) ? $types[$typeID] : [];
            $formattedData[] = $this->buildItem('    ' . $typeName, $itemData);
            if (isset($data[$typeID])) {
                foreach ($expenditureItems[$typeID] as $expenditureItemID => $expenditureItemName) {
                    if (isset($data[$typeID][$expenditureItemID])) {
                        $formattedData[] = $this->buildItem('        ' . $expenditureItemName, $data[$typeID][$expenditureItemID]);
                    }
                }
            }
            if ($class == 'expense') {
                $formattedData[] = $this->buildItem($growingBalanceLabel[$typeID], $data['growingBalanceByBlock'][$blockByType[$typeID]]);
            }
        }
        $formattedData[] = $this->buildItem('Результат по месяцу', $balance);
        $formattedData[] = $this->buildItem('Остаток на конец месяца', $growingBalance);
        $columns[] = [
            'attribute' => 'itemName',
            'header' => 'Статьи',
        ];
        foreach (self::$month as $monthNumber => $monthText) {
            $columns[] = [
                'attribute' => "item{$monthNumber}",
                'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                'header' => $monthText,
            ];
        }
        $columns[] = [
            'attribute' => 'dataYear',
            'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
            'header' => $this->year,
        ];

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $formattedData,
            'title' => $title,
            'rangeHeader' => range('A', 'N'),
            'columns' => $columns,
            'format' => 'Xlsx',
            'fileName' => $title,
        ]);
    }
}