<?php
/**
 * Created by PhpStorm.
 * User: �����
 * Date: 26.10.2017
 * Time: 8:29
 */

namespace frontend\modules\reports\models;


use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\modules\acquiring\models\AcquiringOperation;
use frontend\modules\cash\models\CashContractorType;
use frontend\modules\documents\components\InvoiceStatistic;
use frontend\modules\reports\components\DebtsHelper;
use yii\base\InvalidParamException;
use Yii;
use common\components\TextHelper;
use common\models\document\Invoice;
use frontend\models\Documents;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class FlowOfFundsReportSearch
 * @package frontend\modules\reports\models
 */
class FlowOfFundsReportSearch extends AbstractFinance implements FinanceReport
{
    const TAB_ODDS = 1;
    const TAB_ODDS_BY_PURSE = 2;

    public $contractor_id;
    public $reason_id;
    public $period;
    public $group;
    public $flow_type;
    public $payment_type;

    public $incomeItemIdManyItem;
    public $expenditureItemIdManyItem;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['contractor_id', 'reason_id', 'payment_type'], 'integer'],
        ]);
    }

    public function getDaysInMonth($month) {
        if (strlen($month) == 1) { $month = '0'.$month; }
        $start = (new \DateTime())->createFromFormat('Y-m-d', $this->year.'-'.$month.'-'.'01');
        $curr = $start;
        $endDate = $start->format('Y-m-t');
        $ret = [];
        while(true) {
            $date = $curr->format('Y-m-d');
            $ret[$date] = $curr->format('d');
            $curr = $curr->modify("+1 day");

            if ($date == $endDate)
                break;
        }

        return $ret;
    }



    /**
     * @param $type
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function search($type, $params = [])
    {
        if ($type == self::TAB_ODDS_BY_PURSE) {
            return $this->searchByPurse($params);
        } elseif ($type == self::TAB_ODDS) {
            return $this->searchByActivity($params);
        }
        throw new InvalidParamException('Invalid type param.');
    }

    /**
     * @param $type
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function searchByDays($type, $params = [])
    {
        if ($type == self::TAB_ODDS_BY_PURSE) {
            $params['searchInMonth'] = true;
            return $this->searchByPurseDays($params);
        } elseif ($type == self::TAB_ODDS) {
            $params['searchInMonth'] = true;
            return $this->searchByActivityDays($params);
        }
        throw new InvalidParamException('Invalid type param.');
    }

    /**
     * @param $type
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function searchInMonth($type, $params = [])
    {
        if ($type == self::TAB_ODDS_BY_PURSE) {
            $params['searchInMonth'] = true;
            return $this->searchByPurse($params);
        } elseif ($type == self::TAB_ODDS) {
            $params['searchInMonth'] = true;
            return $this->searchByActivity($params);
        }
        throw new InvalidParamException('Invalid type param.');
    }

    /**
     * @param array $params
     * @return array
     */
    public function searchByActivity($params = [])
    {
        $this->_load($params);
        $result = [];
        $prevSum = [
            'incomePrevSum' => 0,
            'expenditurePrevSum' => 0,
        ];
        $result['balance']['totalFlowSum'] = 0;
        $statisticsPeriod = self::$month;

        foreach ($statisticsPeriod as $monthNumber => $monthText) {

            $dateFrom = $this->year . '-' . $monthNumber . '-01';
            $dateTo = $this->year . '-' . $monthNumber . '-' . cal_days_in_month(CAL_GREGORIAN, $monthNumber, $this->year);

            if (!isset($result['growingBalance'][$monthNumber])) {
                $result['growingBalance'][$monthNumber] = 0;
            }
            if (!isset($result['growingBalanceContractorBalance'][$monthNumber])) {
                $result['growingBalanceContractorBalance'][$monthNumber] = 0;
            }
            /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
            foreach (self::flowClassArray() as $className) {
                $result['growingBalanceContractorBalance'][$monthNumber] += $this->getCashFlowsContractorBalanceAmount($className, $dateFrom, $dateTo);
            }
        }
        foreach (self::$typeItem as $typeID => $itemIDs) {
            $type = self::$flowTypeByActivityType[$typeID];
            $params = $this->getParamsByFlowType($type);
            foreach ($statisticsPeriod as $monthNumber => $monthText) {
                $dateFrom = $this->year . '-' . $monthNumber . '-01';
                $dateTo = $this->year . '-' . $monthNumber . '-' . cal_days_in_month(CAL_GREGORIAN, $monthNumber, $this->year);
                /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
                foreach (self::flowClassArray() as $className) {
                    if (!isset($result['balance'][$monthNumber])) {
                        $result['balance'][$monthNumber] = 0;
                    }
                    $query = $this->getDefaultCashFlowsQuery($className, $params, $dateFrom, $dateTo);
                    $items = $this->filterByActivityQuery($query, $params, $typeID)
                        ->select([
                            $className::tableName() . ".{$params['itemAttrName']}",
                            'invoiceItem.name as itemName',
                            'itemFlowOfFunds.flow_of_funds_block as flowOfFundsBlock',
                            'SUM(' . $className::tableName() . '.amount) as flowSum'
                        ])
                        ->groupBy([$className::tableName() . ".{$params['itemAttrName']}"])
                        ->asArray()
                        ->all();
                    foreach ($items as $item) {
                        $itemID = $item[$params['itemAttrName']];
                        $flowSum = (int)$item['flowSum'];
                        $itemName = $item['itemName'];
                        // Сумма по статьям помесячно
                        if (isset($result[$typeID][$itemID][$monthNumber])) {
                            $result[$typeID][$itemID][$monthNumber]['flowSum'] += $flowSum;
                        } else {
                            $result[$typeID][$itemID][$monthNumber] = [
                                'flowSum' => $flowSum,
                            ];
                        }
                        // Итого по статьям
                        if (isset($result[$typeID][$itemID]['totalFlowSum'])) {
                            $result[$typeID][$itemID]['totalFlowSum'] += $flowSum;
                        } else {
                            $result[$typeID][$itemID]['totalFlowSum'] = $flowSum;
                        }
                        // Существующие статьи
                        $result['itemName'][$typeID][$itemID] = $itemName;
                        // Сумма по типам помесячно
                        if (isset($result['types'][$typeID][$monthNumber]['flowSum'])) {
                            $result['types'][$typeID][$monthNumber]['flowSum'] += $flowSum;
                        } else {
                            $result['types'][$typeID][$monthNumber]['flowSum'] = $flowSum;
                        }
                        // Итого по типам
                        if (isset($result['types'][$typeID]['totalFlowSum'])) {
                            $result['types'][$typeID]['totalFlowSum'] += $flowSum;
                        } else {
                            $result['types'][$typeID]['totalFlowSum'] = $flowSum;
                        }
                    }
                }
                // Сальдо
                $sum = isset($result['types'][$typeID][$monthNumber]['flowSum']) ?
                    (int)$result['types'][$typeID][$monthNumber]['flowSum'] : 0;
                if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                    $result['balance'][$monthNumber] += $sum;
                } else {
                    $result['balance'][$monthNumber] -= $sum;
                }
                // Сальдо нарастающим
                $previousMonth = intval($monthNumber) - 1;
                $previousMonth = $previousMonth > 9 ? $previousMonth : ('0' . $previousMonth);

                $result['growingBalance'][$monthNumber] = $result['balance'][$monthNumber] +
                    $result['growingBalanceContractorBalance'][$monthNumber] +
                    (isset($result['growingBalance'][$previousMonth]) ? $result['growingBalance'][$previousMonth] : 0);
            }
            $sum = isset($result['types'][$typeID]['totalFlowSum']) ?
                $result['types'][$typeID]['totalFlowSum'] : 0;
            if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                $result['balance']['totalFlowSum'] += $sum;
            } else {
                $result['balance']['totalFlowSum'] -= $sum;
            }
            if (isset($result['itemName'][$typeID])) {
                asort($result['itemName'][$typeID]);
            }
        }
        if ($this->year !== min($this->getYearFilter())) {
            $dateFrom = min($this->getYearFilter()) . '-01-01';
            $dateTo = ($this->year - 1) . '-12-31';

            foreach (self::$typeItem as $typeID => $itemIDs) {
                $type = self::$flowTypeByActivityType[$typeID];
                $params = $this->getParamsByFlowType($type);
                foreach (self::flowClassArray() as $className) {
                    $query = $this->getDefaultCashFlowsQuery($className, $params, $dateFrom, $dateTo);
                    $items = $this->filterByActivityQuery($query, $params, $typeID)
                        ->select([
                            $className::tableName() . ".{$params['itemAttrName']}",
                            'invoiceItem.name as itemName',
                            'itemFlowOfFunds.flow_of_funds_block as flowOfFundsBlock',
                            'SUM(' . $className::tableName() . '.amount) as flowSum'
                        ])
                        ->groupBy([$className::tableName() . ".{$params['itemAttrName']}"])
                        ->asArray()
                        ->all();
                    foreach ($items as $item) {
                        $prevSum[$params['prevSumAttrName']] += (int)$item['flowSum'];
                        if (!isset($prevSum[self::$blockByType[$typeID]][$params['prevSumAttrName']])) {
                            $prevSum[self::$blockByType[$typeID]][$params['prevSumAttrName']] = $item['flowSum'];
                        } else {
                            $prevSum[self::$blockByType[$typeID]][$params['prevSumAttrName']] += $item['flowSum'];
                        }
                    }
                }
            }
            $balanceSum = 0;
            /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
            foreach (self::flowClassArray() as $className) {
                $balanceSum += $this->getCashFlowsContractorBalanceAmount($className, $dateFrom, $dateTo);
            }
            $previousBalance = $prevSum['incomePrevSum'] - $prevSum['expenditurePrevSum'] + $balanceSum;
            foreach ($result['growingBalance'] as $key => $value) {
                $result['growingBalance'][$key] += $previousBalance;
            }
        }

        // Calculate Block Sum
        $incomeBlocks = [self::RECEIPT_FINANCING_TYPE_FIRST, self::INCOME_OPERATING_ACTIVITIES, self::INCOME_INVESTMENT_ACTIVITIES];
        $result['blocks'] = [];
        if (isset($result['types'])) {
            foreach ($result['types'] as $key => $data) {
                foreach ($data as $month => $value) {
                    $formattedValue = $month == 'totalFlowSum' ? $value : $value['flowSum'];
                    if (!isset($result['blocks'][self::$blockByType[$key]][$month]['flowSum'])) {
                        $result['blocks'][self::$blockByType[$key]][$month]['flowSum'] = 0;
                    }
                    if (in_array($key, $incomeBlocks)) {
                        $result['blocks'][self::$blockByType[$key]][$month]['flowSum'] += $formattedValue;

                        // add balance
                        //if ($key == self::INCOME_OPERATING_ACTIVITIES && $month != 'totalFlowSum') {
                        //    $result['blocks'][self::$blockByType[$key]][$month]['flowSum'] += $result['growingBalanceContractorBalance'][$month];
                        //}

                    } else {
                        $result['blocks'][self::$blockByType[$key]][$month]['flowSum'] -= $formattedValue;
                    }
                }
            }
        }

        // Calculate Growing Balance By Block
        foreach (self::$blocks as $blockType => $blockName) {
            $key = 0;
            foreach ($statisticsPeriod as $monthNumber => $monthText) {
                $formattedValue = isset($result['blocks'][$blockType][$monthNumber]['flowSum']) ?
                    $result['blocks'][$blockType][$monthNumber]['flowSum'] : 0;
                $prevFlowSum = 0;
                if (!isset($growingBalanceByBlock[$blockType][$monthNumber]['flowSum'])) {
                    $result['growingBalanceByBlock'][$blockType][$monthNumber]['flowSum'] = $formattedValue;
                }

                if ($key === 0) {
                    if (isset($prevSum[$blockType]['incomePrevSum'])) {
                        $prevFlowSum += $prevSum[$blockType]['incomePrevSum'];
                    }
                    if (isset($prevSum[$blockType]['expenditurePrevSum'])) {
                        $prevFlowSum -= $prevSum[$blockType]['expenditurePrevSum'];
                    }
                } else {
                    $prevMonth = $monthNumber - 1;
                    $prevMonth = $prevMonth < 10 ? (0 . $prevMonth) : $prevMonth;
                    $prevFlowSum = $result['growingBalanceByBlock'][$blockType][$prevMonth]['flowSum'];
                }
                $result['growingBalanceByBlock'][$blockType][$monthNumber]['flowSum'] += $prevFlowSum;
                $key++;
            }
        }

        //$result = $this->calculateBlocksSum(self::$blockByType, [
        //    self::RECEIPT_FINANCING_TYPE_FIRST,
        //    self::INCOME_OPERATING_ACTIVITIES,
        //    self::INCOME_INVESTMENT_ACTIVITIES,
        //], $result);
        //$result = $this->calculateGrowingBalanceByBlock(self::$blocks, $result, $prevSum);

        return $result;
    }

    /**
     * @param array $params
     * @return array
     */
    public function searchByActivityDays($params = [])
    {
        $this->_load($params);
        $result = [];
        $resultDAY = [];

        $prevSum = [
            'incomePrevSum' => 0,
            'expenditurePrevSum' => 0,
        ];

        $result['balance'] = ["01" => 0, "02" => 0, "03" => 0, "04" => 0, "05" => 0, "06" => 0, "07" => 0, "08" => 0, "09" => 0, "10" => 0, "11" => 0, "12" => 0];
        $result['growingBalanceContractorBalance'] = ["01" => 0, "02" => 0, "03" => 0, "04" => 0, "05" => 0, "06" => 0, "07" => 0, "08" => 0, "09" => 0, "10" => 0, "11" => 0, "12" => 0];
        $result['balance']['totalFlowSum'] = 0;
        for ($m=1; $m <= 12; $m++) {
            foreach ($this->getDaysInMonth($m) as $k=>$v) {
                $resultDAY['balance'][$k] = 0;
                $resultDAY['growingBalanceContractorBalance'][$k] = 0;
            }
        }
        $resultDAY['balance']['totalFlowSum'] = 0;

        $statisticsPeriod = self::$month;

        ///foreach ($statisticsPeriod as $monthNumber => $monthText) {

            $dateFrom = $this->year . '-01-01';
            $dateTo = $this->year . '-12-31';

            /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
            foreach (self::flowClassArray() as $className) {

                //$result['growingBalanceContractorBalance'][$monthNumber] += $this->getCashFlowsContractorBalanceAmount($className, $dateFrom, $dateTo);

                $growingBalanceContractorArr = $className::find()
                    ->andWhere(['company_id' => $this->company->id])
                    ->andWhere(['contractor_id' => CashContractorType::BALANCE_TEXT])
                    ->andWhere(['between', 'date', $dateFrom, $dateTo])
                    ->select(['amount', 'date'])->asArray()->all();
                foreach ($growingBalanceContractorArr as $item) {

                    $monthNumber = substr($item['date'], 5, 2);
                    $dayNumber = $item['date'];

                    $result['growingBalanceContractorBalance'][$monthNumber] += $item['amount'];
                    $resultDAY['growingBalanceContractorBalance'][$dayNumber] += $item['amount'];
                }
            }

        ///}

        foreach (self::$typeItem as $typeID => $itemIDs) {
            $type = self::$flowTypeByActivityType[$typeID];
            $params = $this->getParamsByFlowType($type);

            ///foreach ($statisticsPeriod as $monthNumber => $monthText) {

                $dateFrom = $this->year . '-01-01';
                $dateTo = $this->year . '-12-31';

                /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
                foreach (self::flowClassArray() as $className) {
                    $query = $this->getDefaultCashFlowsQuery($className, $params, $dateFrom, $dateTo);
                    $items = $this->filterByActivityQuery($query, $params, $typeID)
                        ->select([
                            $className::tableName() . ".{$params['itemAttrName']}",
                            'invoiceItem.name as itemName',
                            'itemFlowOfFunds.flow_of_funds_block as flowOfFundsBlock',
                            $className::tableName() . '.amount as flowSum',
                            'date'
                        ])
                        //->groupBy([$className::tableName() . ".{$params['itemAttrName']}"])
                        ->asArray()
                        ->all();

                    // prepare
                    $fs0 = ['flowSum' => 0];
                    foreach ($items as $item) {
                        $itemID = $item[$params['itemAttrName']];
                        if (!isset($result[$typeID][$itemID])) $result[$typeID][$itemID] = ["01" => $fs0, "02" => $fs0, "03" => $fs0, "04" => $fs0, "05" => $fs0, "06" => $fs0, "07" => $fs0, "08" => $fs0, "09" => $fs0, "10" => $fs0, "11" => $fs0, "12" => $fs0];
                        if (!isset($result['types'][$typeID])) $result['types'][$typeID] = ["01" => $fs0, "02" => $fs0, "03" => $fs0, "04" => $fs0, "05" => $fs0, "06" => $fs0, "07" => $fs0, "08" => $fs0, "09" => $fs0, "10" => $fs0, "11" => $fs0, "12" => $fs0];
                        for ($m=1; $m <= 12; $m++) {
                            foreach ($this->getDaysInMonth($m) as $k=>$v) {
                                if (!isset($resultDAY[$typeID][$itemID][$k])) $resultDAY[$typeID][$itemID][$k] = $fs0;
                                if (!isset($resultDAY['types'][$typeID][$k])) $resultDAY['types'][$typeID][$k] = $fs0;
                            }
                        }
                        if (!isset($result[$typeID][$itemID]['totalFlowSum'])) $result[$typeID][$itemID]['totalFlowSum'] = 0;
                        if (!isset($result['types'][$typeID]['totalFlowSum'])) $result['types'][$typeID]['totalFlowSum'] = 0;
                    }

                    foreach ($items as $item) {
                        $itemID = $item[$params['itemAttrName']];
                        $flowSum = (int)$item['flowSum'];
                        $itemName = $item['itemName'];

                        $monthNumber = substr($item['date'], 5, 2);
                        $dayNumber = $item['date'];

                        // Сумма по статьям помесячно
                        $result[$typeID][$itemID][$monthNumber]['flowSum'] += $flowSum;
                        $resultDAY[$typeID][$itemID][$dayNumber]['flowSum'] += $flowSum;

                        // Итого по статьям
                        $result[$typeID][$itemID]['totalFlowSum'] += $flowSum;

                        // Существующие статьи
                        $result['itemName'][$typeID][$itemID] = $itemName;

                        // Сумма по типам помесячно
                        $result['types'][$typeID][$monthNumber]['flowSum'] += $flowSum;
                        $resultDAY['types'][$typeID][$dayNumber]['flowSum'] += $flowSum;

                        // Итого по типам
                        $result['types'][$typeID]['totalFlowSum'] += $flowSum;
                    }
                }

                // Сальдо
                foreach (self::$month as $monthNumber => $monthName) {
                    $sum = isset($result['types'][$typeID][$monthNumber]['flowSum']) ? (int)$result['types'][$typeID][$monthNumber]['flowSum'] : 0;
                    if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $result['balance'][$monthNumber] += $sum;
                    } else {
                        $result['balance'][$monthNumber] -= $sum;
                    }
                    // Сальдо нарастающим
                    $previousMonth = intval($monthNumber) - 1;
                    $previousMonth = $previousMonth > 9 ? $previousMonth : ('0' . $previousMonth);

                    $result['growingBalance'][$monthNumber] = $result['balance'][$monthNumber] +
                        // $result['growingBalanceContractorBalance'][$monthNumber] +
                        (isset($result['growingBalance'][$previousMonth]) ? $result['growingBalance'][$previousMonth] : 0);
                }
                // Сальдо по дням
                for ($m=1; $m<=12; $m++) foreach ($this->getDaysInMonth($m) as $date => $dayName) {
                    $sum = isset($resultDAY['types'][$typeID][$date]['flowSum']) ? (int)$resultDAY['types'][$typeID][$date]['flowSum'] : 0;
                    if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $resultDAY['balance'][$date] += $sum;
                    } else {
                        $resultDAY['balance'][$date] -= $sum;
                    }
                    // Сальдо нарастающим
                    $prevDate = date('Y-m-d', strtotime($date) - 24 * 3600);

                    $resultDAY['growingBalance'][$date] = $resultDAY['balance'][$date] +
                        // $resultDAY['growingBalanceContractorBalance'][$date] +
                        (isset($resultDAY['growingBalance'][$prevDate]) ? $resultDAY['growingBalance'][$prevDate] : 0);
                }

            ////}

            $sum = isset($result['types'][$typeID]['totalFlowSum']) ? $result['types'][$typeID]['totalFlowSum'] : 0;
            if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                $result['balance']['totalFlowSum'] += $sum;
                $resultDAY['balance']['totalFlowSum'] += $sum;
            } else {
                $result['balance']['totalFlowSum'] -= $sum;
                $resultDAY['balance']['totalFlowSum'] -= $sum;
            }
            if (isset($result['itemName'][$typeID])) {
                asort($result['itemName'][$typeID]);
            }
        }

        ////// PREV YEARS
        if ($this->year !== min($this->getYearFilter())) {

            $dateFrom = min($this->getYearFilter()) . '-01-01';
            $dateTo = ($this->year - 1) . '-12-31';

            foreach (self::$typeItem as $typeID => $itemIDs) {
                $type = self::$flowTypeByActivityType[$typeID];
                $params = $this->getParamsByFlowType($type);
                foreach (self::flowClassArray() as $className) {
                    $query = $this->getDefaultCashFlowsQuery($className, $params, $dateFrom, $dateTo);
                    $items = $this->filterByActivityQuery($query, $params, $typeID)
                        ->select([
                            $className::tableName() . ".{$params['itemAttrName']}",
                            'invoiceItem.name as itemName',
                            'itemFlowOfFunds.flow_of_funds_block as flowOfFundsBlock',
                            'SUM(' . $className::tableName() . '.amount) as flowSum'
                        ])
                        ->groupBy([$className::tableName() . ".{$params['itemAttrName']}"])
                        ->asArray()
                        ->all();
                    foreach ($items as $item) {
                        $prevSum[$params['prevSumAttrName']] += (int)$item['flowSum'];
                        if (!isset($prevSum[self::$blockByType[$typeID]][$params['prevSumAttrName']])) {
                            $prevSum[self::$blockByType[$typeID]][$params['prevSumAttrName']] = $item['flowSum'];
                        } else {
                            $prevSum[self::$blockByType[$typeID]][$params['prevSumAttrName']] += $item['flowSum'];
                        }
                    }
                }
            }

            $balanceSum = 0;
            // /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
            // foreach (self::flowClassArray() as $className) {
            //     $balanceSum += $this->getCashFlowsContractorBalanceAmount($className, $dateFrom, $dateTo);
            // }
            $previousBalance = $prevSum['incomePrevSum'] - $prevSum['expenditurePrevSum'] + $balanceSum;

            // Sum with prev years
            foreach ($result['growingBalance'] as $key => $value) {
                $result['growingBalance'][$key] += $previousBalance;
            }
            // Sum with prev years by days
            foreach ($resultDAY['growingBalance'] as $key => $value) {
                $resultDAY['growingBalance'][$key] += $previousBalance;
            }
        }

        // Calculate Block Sum
        $incomeBlocks = [self::RECEIPT_FINANCING_TYPE_FIRST, self::INCOME_OPERATING_ACTIVITIES, self::INCOME_INVESTMENT_ACTIVITIES];
        $result['blocks'] = [];
        if (isset($result['types'])) {
            foreach ($result['types'] as $key => $data) {
                foreach ($data as $month => $value) {
                    $formattedValue = $month == 'totalFlowSum' ? $value : $value['flowSum'];
                    if (!isset($result['blocks'][self::$blockByType[$key]][$month]['flowSum'])) {
                        $result['blocks'][self::$blockByType[$key]][$month]['flowSum'] = 0;
                    }
                    if (in_array($key, $incomeBlocks)) {
                        $result['blocks'][self::$blockByType[$key]][$month]['flowSum'] += $formattedValue;

                        // add balance
                        //if ($key == self::INCOME_OPERATING_ACTIVITIES && $month != 'totalFlowSum') {
                        //    $result['blocks'][self::$blockByType[$key]][$month]['flowSum'] += $result['growingBalanceContractorBalance'][$month];
                        //}

                    } else {
                        $result['blocks'][self::$blockByType[$key]][$month]['flowSum'] -= $formattedValue;
                    }
                }
            }
        }
        // Calculate Block Sum by day
        $resultDAY['blocks'] = [];
        if (isset($resultDAY['types'])) {
            foreach ($resultDAY['types'] as $key => $data) {
                foreach ($data as $date => $value) {
                    $formattedValue = $date == 'totalFlowSum' ? $value : $value['flowSum'];
                    if (!isset($resultDAY['blocks'][self::$blockByType[$key]][$date]['flowSum'])) {
                        $resultDAY['blocks'][self::$blockByType[$key]][$date]['flowSum'] = 0;
                    }
                    if (in_array($key, $incomeBlocks)) {
                        $resultDAY['blocks'][self::$blockByType[$key]][$date]['flowSum'] += $formattedValue;

                        // add balance
                        //if ($key == self::INCOME_OPERATING_ACTIVITIES && $date != 'totalFlowSum') {
                        //    $resultDAY['blocks'][self::$blockByType[$key]][$date]['flowSum'] += $resultDAY['growingBalanceContractorBalance'][$date];
                        //}

                    } else {
                        $resultDAY['blocks'][self::$blockByType[$key]][$date]['flowSum'] -= $formattedValue;
                    }
                }
            }
        }

        // Calculate Growing Balance By Block
        foreach (self::$blocks as $blockType => $blockName) {
            $key = 0;
            foreach ($statisticsPeriod as $monthNumber => $monthText) {
                $formattedValue = isset($result['blocks'][$blockType][$monthNumber]['flowSum']) ? $result['blocks'][$blockType][$monthNumber]['flowSum'] : 0;
                $prevFlowSum = 0;
                if (!isset($growingBalanceByBlock[$blockType][$monthNumber]['flowSum'])) {
                    $result['growingBalanceByBlock'][$blockType][$monthNumber]['flowSum'] = $formattedValue;
                }

                if ($key === 0) {
                    if (isset($prevSum[$blockType]['incomePrevSum'])) {
                        $prevFlowSum += $prevSum[$blockType]['incomePrevSum'];
                    }
                    if (isset($prevSum[$blockType]['expenditurePrevSum'])) {
                        $prevFlowSum -= $prevSum[$blockType]['expenditurePrevSum'];
                    }
                } else {
                    $prevMonth = $monthNumber - 1;
                    $prevMonth = $prevMonth < 10 ? (0 . $prevMonth) : $prevMonth;
                    $prevFlowSum = $result['growingBalanceByBlock'][$blockType][$prevMonth]['flowSum'];
                }
                $result['growingBalanceByBlock'][$blockType][$monthNumber]['flowSum'] += $prevFlowSum;
                $key++;
            }
        }
        // Calculate Growing Balance By Block by day
        foreach (self::$blocks as $blockType => $blockName) {
            $key = 0;
            for($m=1; $m<=12; $m++) foreach ($this->getDaysInMonth($m) as $date => $dayText) {
                $formattedValue = isset($resultDAY['blocks'][$blockType][$date]['flowSum']) ? $resultDAY['blocks'][$blockType][$date]['flowSum'] : 0;
                $prevFlowSum = 0;
                if (!isset($growingBalanceByBlock[$blockType][$date]['flowSum'])) {
                    $resultDAY['growingBalanceByBlock'][$blockType][$date]['flowSum'] = $formattedValue;
                }

                if ($key === 0) {
                    if (isset($prevSum[$blockType]['incomePrevSum'])) {
                        $prevFlowSum += $prevSum[$blockType]['incomePrevSum'];
                    }
                    if (isset($prevSum[$blockType]['expenditurePrevSum'])) {
                        $prevFlowSum -= $prevSum[$blockType]['expenditurePrevSum'];
                    }
                } else {
                    $prevDate = date('Y-m-d', strtotime($date) - 24 * 3600);
                    $prevFlowSum = $resultDAY['growingBalanceByBlock'][$blockType][$prevDate]['flowSum'];
                }
                $resultDAY['growingBalanceByBlock'][$blockType][$date]['flowSum'] += $prevFlowSum;
                $key++;
            }
        }

        //$result = $this->calculateBlocksSum(self::$blockByType, [
        //    self::RECEIPT_FINANCING_TYPE_FIRST,
        //    self::INCOME_OPERATING_ACTIVITIES,
        //    self::INCOME_INVESTMENT_ACTIVITIES,
        //], $result);
        //$result = $this->calculateGrowingBalanceByBlock(self::$blocks, $result, $prevSum);

        return ['result' => $result, 'resultDAY' => $resultDAY];
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function searchByPurse($params = [])
    {
        $this->_load($params);
        $result = [];
        $prevSum = [
            'incomePrevSum' => 0,
            'expenditurePrevSum' => 0,
        ];
        $result['balance']['totalFlowSum'] = 0;

        $statisticsPeriod = self::$month;

        foreach ($statisticsPeriod as $monthNumber => $monthText) {
            if (!isset($result['growingBalance'][$monthNumber])) {
                $result['growingBalance'][$monthNumber] = 0;
            }
        }
        foreach (self::$purseTypes as $typeID => $typeName) {
            /* @var $flowClassName CashBankFlows|CashOrderFlows|CashEmoneyFlows */
            $flowClassName = $this->getFlowClassNameByType(self::$purseBlockByType[$typeID]);
            $type = self::$flowTypeByPurseType[$typeID];
            $params = $this->getParamsByFlowType($type);
            foreach ($statisticsPeriod as $monthNumber => $monthText) {
                $dateFrom = $this->year . '-' . $monthNumber . '-01';
                $dateTo = $this->year . '-' . $monthNumber . '-' . cal_days_in_month(CAL_GREGORIAN, $monthNumber, $this->year);
                if (!isset($result['balance'][$monthNumber])) {
                    $result['balance'][$monthNumber] = 0;
                }
                $query = $this->getDefaultCashFlowsQuery($flowClassName, $params, $dateFrom, $dateTo);
                $items = $this->filterByPurseQuery($query)
                    ->select([
                        $flowClassName::tableName() . ".{$params['itemAttrName']}",
                        'invoiceItem.name as itemName',
                        'SUM(' . $flowClassName::tableName() . '.amount) as flowSum'
                    ])
                    ->groupBy([$flowClassName::tableName() . ".{$params['itemAttrName']}"])
                    ->asArray()
                    ->all();
                $itemsByCashContractor = $this->getDefaultItemsByCashFlowContractorQuery(
                    $flowClassName, $type, self::$cashContractorTypeByTableName[$flowClassName], $dateFrom, $dateTo
                )
                    ->groupBy([$flowClassName::tableName() . '.contractor_id'])
                    ->asArray()
                    ->all();
                foreach ($itemsByCashContractor as $itemByCashContractor) {
                    $items[] = $itemByCashContractor;
                }
                foreach ($items as $item) {
                    if (isset($item['contractorType'])) {
                        $itemID = $item['contractorType'];
                        $flowSum = (int)$item['flowSum'];
                        if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                            $itemName = 'Приход из ' . self::$cashContractorTextByType[$type][$itemID];
                        } else {
                            $itemName = 'Перевод в ' . self::$cashContractorTextByType[$type][$itemID];
                        }
                    } else {
                        $itemID = $item[$params['itemAttrName']];
                        $flowSum = (int)$item['flowSum'];
                        $itemName = $item['itemName'];
                    }
                    // Сумма по статьям помесячно
                    if (isset($result[$typeID][$itemID][$monthNumber])) {
                        $result[$typeID][$itemID][$monthNumber]['flowSum'] += $flowSum;
                    } else {
                        $result[$typeID][$itemID][$monthNumber] = ['flowSum' => $flowSum,];
                    }
                    // Итого по статьям
                    if (isset($result[$typeID][$itemID]['totalFlowSum'])) {
                        $result[$typeID][$itemID]['totalFlowSum'] += $flowSum;
                    } else {
                        $result[$typeID][$itemID]['totalFlowSum'] = $flowSum;
                    }
                    // Существующие статьи
                    $result['itemName'][$typeID][$itemID] = $itemName;
                    // Сумма по типам помесячно
                    if (isset($result['types'][$typeID][$monthNumber]['flowSum'])) {
                        $result['types'][$typeID][$monthNumber]['flowSum'] += $flowSum;
                    } else {
                        $result['types'][$typeID][$monthNumber]['flowSum'] = $flowSum;
                    }
                    // Итого по типам
                    if (isset($result['types'][$typeID]['totalFlowSum'])) {
                        $result['types'][$typeID]['totalFlowSum'] += $flowSum;
                    } else {
                        $result['types'][$typeID]['totalFlowSum'] = $flowSum;
                    }
                }
                // Сальдо
                $sum = isset($result['types'][$typeID][$monthNumber]['flowSum']) ?
                    (int)$result['types'][$typeID][$monthNumber]['flowSum'] : 0;
                if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                    $result['balance'][$monthNumber] += $sum;
                } else {
                    $result['balance'][$monthNumber] -= $sum;
                }
                // Сальдо нарастающим
                $previousMonth = intval($monthNumber) - 1;
                $previousMonth = $previousMonth > 9 ? $previousMonth : ('0' . $previousMonth);

                $result['growingBalance'][$monthNumber] = $result['balance'][$monthNumber] +
                    (isset($result['growingBalance'][$previousMonth]) ? $result['growingBalance'][$previousMonth] : 0);
            }
            $sum = isset($result['types'][$typeID]['totalFlowSum']) ?
                $result['types'][$typeID]['totalFlowSum'] : 0;
            if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                $result['balance']['totalFlowSum'] += $sum;
            } else {
                $result['balance']['totalFlowSum'] -= $sum;
            }
            if (isset($result['itemName'][$typeID])) {
                asort($result['itemName'][$typeID]);
            }
        }
        if ($this->year !== min($this->getYearFilter()) || $inMonth) {
            $dateFrom = min($this->getYearFilter()) . '-01-01';
            $dateTo = ($this->year - 1) . '-12-31';

            foreach (self::$purseTypes as $typeID => $typeName) {
                /* @var $flowClassName CashBankFlows|CashOrderFlows|CashEmoneyFlows */
                $flowClassName = $this->getFlowClassNameByType(self::$purseBlockByType[$typeID]);
                $type = self::$flowTypeByPurseType[$typeID];
                $params = $this->getParamsByFlowType($type);
                $query = $this->getDefaultCashFlowsQuery($flowClassName, $params, $dateFrom, $dateTo);
                $items = $this->filterByPurseQuery($query)
                    ->select([
                        $flowClassName::tableName() . ".{$params['itemAttrName']}",
                        'invoiceItem.name as itemName',
                        'SUM(' . $flowClassName::tableName() . '.amount) as flowSum'
                    ])
                    ->groupBy([$flowClassName::tableName() . ".{$params['itemAttrName']}"])
                    ->asArray()
                    ->all();
                $itemsByCashContractor = $this->getDefaultItemsByCashFlowContractorQuery(
                    $flowClassName, $type, self::$cashContractorTypeByTableName[$flowClassName], $dateFrom, $dateTo
                )
                    ->groupBy([$flowClassName::tableName() . '.contractor_id'])
                    ->asArray()
                    ->all();
                foreach ($itemsByCashContractor as $itemByCashContractor) {
                    $items[] = $itemByCashContractor;
                }
                foreach ($items as $item) {
                    $prevSum[$params['prevSumAttrName']] += (int)$item['flowSum'];
                    if (!isset($prevSum[self::$purseBlockByType[$typeID]][$params['prevSumAttrName']])) {
                        $prevSum[self::$purseBlockByType[$typeID]][$params['prevSumAttrName']] = $item['flowSum'];
                    } else {
                        $prevSum[self::$purseBlockByType[$typeID]][$params['prevSumAttrName']] += $item['flowSum'];
                    }
                }
            }
            $previousBalance = $prevSum['incomePrevSum'] - $prevSum['expenditurePrevSum'];
            foreach ($result['growingBalance'] as $key => $value) {
                $result['growingBalance'][$key] += $previousBalance;
            }
        }

        //$result = $this->calculateBlocksSum(self::$purseBlockByType, [
        //    self::INCOME_CASH_BANK,
        //    self::INCOME_CASH_ORDER,
        //    self::INCOME_CASH_EMONEY
        //], $result);
        //$result = $this->calculateGrowingBalanceByBlock(self::$purseBlocks, $result, $prevSum);

        // Calculate Block Sum
        $incomeBlocks = [self::INCOME_CASH_BANK, self::INCOME_CASH_ORDER, self::INCOME_CASH_EMONEY];
        $result['blocks'] = [];
        if (isset($result['types'])) {
            foreach ($result['types'] as $key => $data) {
                foreach ($data as $month => $value) {
                    $formattedValue = $month == 'totalFlowSum' ? $value : $value['flowSum'];
                    if (!isset($result['blocks'][self::$purseBlockByType[$key]][$month]['flowSum'])) {
                        $result['blocks'][self::$purseBlockByType[$key]][$month]['flowSum'] = 0;
                    }
                    if (in_array($key, $incomeBlocks)) {
                        $result['blocks'][self::$purseBlockByType[$key]][$month]['flowSum'] += $formattedValue;
                    } else {
                        $result['blocks'][self::$purseBlockByType[$key]][$month]['flowSum'] -= $formattedValue;
                    }
                }
            }
        }

        // Calculate Growing Balance By Block
        foreach (self::$purseBlocks as $blockType => $blockName) {
            $key = 0;
            foreach ($statisticsPeriod as $monthNumber => $monthText) {
                $formattedValue = isset($result['blocks'][$blockType][$monthNumber]['flowSum']) ?
                    $result['blocks'][$blockType][$monthNumber]['flowSum'] : 0;
                $prevFlowSum = 0;
                if (!isset($growingBalanceByBlock[$blockType][$monthNumber]['flowSum'])) {
                    $result['growingBalanceByBlock'][$blockType][$monthNumber]['flowSum'] = $formattedValue;
                }

                if ($key === 0) {
                    if (isset($prevSum[$blockType]['incomePrevSum'])) {
                        $prevFlowSum += $prevSum[$blockType]['incomePrevSum'];
                    }
                    if (isset($prevSum[$blockType]['expenditurePrevSum'])) {
                        $prevFlowSum -= $prevSum[$blockType]['expenditurePrevSum'];
                    }
                } else {
                    $prevMonth = $monthNumber - 1;
                    $prevMonth = $prevMonth < 10 ? (0 . $prevMonth) : $prevMonth;
                    $prevFlowSum = $result['growingBalanceByBlock'][$blockType][$prevMonth]['flowSum'];
                }
                $result['growingBalanceByBlock'][$blockType][$monthNumber]['flowSum'] += $prevFlowSum;
                $key++;
            }
        }


        return $result;
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function searchByPurseDays($params = [])
    {
        $this->_load($params);
        $result = [];
        $resultDAY = [];

        $prevSum = [
            'incomePrevSum' => 0,
            'expenditurePrevSum' => 0,
        ];

        $result['balance'] = ["01" => 0, "02" => 0, "03" => 0, "04" => 0, "05" => 0, "06" => 0, "07" => 0, "08" => 0, "09" => 0, "10" => 0, "11" => 0, "12" => 0];
        $result['growingBalanceContractorBalance'] = ["01" => 0, "02" => 0, "03" => 0, "04" => 0, "05" => 0, "06" => 0, "07" => 0, "08" => 0, "09" => 0, "10" => 0, "11" => 0, "12" => 0];
        $result['balance']['totalFlowSum'] = 0;
        for ($m=1; $m <= 12; $m++) {
            foreach ($this->getDaysInMonth($m) as $k=>$v) {
                $resultDAY['balance'][$k] = 0;
                $resultDAY['growingBalanceContractorBalance'][$k] = 0;
            }
        }
        $resultDAY['balance']['totalFlowSum'] = 0;

        $statisticsPeriod = self::$month;

        foreach (self::$purseTypes as $typeID => $typeName) {
            /* @var $flowClassName CashBankFlows|CashOrderFlows|CashEmoneyFlows */
            $flowClassName = $this->getFlowClassNameByType(self::$purseBlockByType[$typeID]);
            $type = self::$flowTypeByPurseType[$typeID];
            $params = $this->getParamsByFlowType($type);

            ////foreach ($statisticsPeriod as $monthNumber => $monthText) {

                $dateFrom = $this->year . '-01-01';
                $dateTo = $this->year . '-12-31';

                //$query = $this->getDefaultCashFlowsQuery($flowClassName, $params, $dateFrom, $dateTo);

                $query = $flowClassName::find()
                    ->leftJoin(['itemFlowOfFunds' => $params['itemFlowOfFundsTableName']],
                        "itemFlowOfFunds.{$params['flowOfFundItemName']} = " . $flowClassName::tableName() . ".{$params['itemAttrName']} AND
                    itemFlowOfFunds.company_id = " . $this->company->id)
                    ->leftJoin(['invoiceItem' => $params['invoiceItemTableName']],
                        "invoiceItem.id = " . $flowClassName::tableName() . ".{$params['itemAttrName']}")
                    ->andWhere(['not', [$flowClassName::tableName() . ".{$params['itemAttrName']}" => null]])
                    //->andWhere(['not', ['contractor_id' => 'company']])
                    ->andWhere(['between', $flowClassName::tableName() . '.date', $dateFrom, $dateTo])
                    ->andWhere([$flowClassName::tableName() . '.company_id' => $this->company->id]);

                $items = $this->filterByPurseQuery($query)
                    ->select([
                        $flowClassName::tableName() . ".{$params['itemAttrName']}",
                        'invoiceItem.name as itemName',
                        $flowClassName::tableName() . '.amount as flowSum',
                        'date'
                    ])
                    //->groupBy([$flowClassName::tableName() . ".{$params['itemAttrName']}"])
                    ->asArray()
                    ->all();

                // prepare
                $fs0 = ['flowSum' => 0];
                foreach ($items as $item) {
                    $itemID = $item[$params['itemAttrName']];
                    if (!isset($result[$typeID][$itemID])) $result[$typeID][$itemID] = ["01" => $fs0, "02" => $fs0, "03" => $fs0, "04" => $fs0, "05" => $fs0, "06" => $fs0, "07" => $fs0, "08" => $fs0, "09" => $fs0, "10" => $fs0, "11" => $fs0, "12" => $fs0];
                    if (!isset($result['types'][$typeID])) $result['types'][$typeID] = ["01" => $fs0, "02" => $fs0, "03" => $fs0, "04" => $fs0, "05" => $fs0, "06" => $fs0, "07" => $fs0, "08" => $fs0, "09" => $fs0, "10" => $fs0, "11" => $fs0, "12" => $fs0];
                    for ($m=1; $m <= 12; $m++) {
                        foreach ($this->getDaysInMonth($m) as $k=>$v) {
                            if (!isset($resultDAY[$typeID][$itemID][$k])) $resultDAY[$typeID][$itemID][$k] = $fs0;
                            if (!isset($resultDAY['types'][$typeID][$k])) $resultDAY['types'][$typeID][$k] = $fs0;
                        }
                    }
                    if (!isset($result[$typeID][$itemID]['totalFlowSum'])) $result[$typeID][$itemID]['totalFlowSum'] = 0;
                    if (!isset($result['types'][$typeID]['totalFlowSum'])) $result['types'][$typeID]['totalFlowSum'] = 0;
                }

                $itemsByCashContractor =
                    $this->getDefaultItemsByCashFlowContractorQuery($flowClassName, $type, self::$cashContractorTypeByTableName[$flowClassName], $dateFrom, $dateTo)
                        ->groupBy([$flowClassName::tableName() . '.contractor_id', 'date'])
                        ->asArray()
                        ->all();

                // prepare 2
                foreach ($itemsByCashContractor as $itemByCashContractor) {
                    $itemID = $itemByCashContractor['contractorType'];
                    $items[] = $itemByCashContractor;
                    if (!isset($result[$typeID][$itemID])) $result[$typeID][$itemID] = ["01" => $fs0, "02" => $fs0, "03" => $fs0, "04" => $fs0, "05" => $fs0, "06" => $fs0, "07" => $fs0, "08" => $fs0, "09" => $fs0, "10" => $fs0, "11" => $fs0, "12" => $fs0];
                    if (!isset($result['types'][$typeID])) $result['types'][$typeID] = ["01" => $fs0, "02" => $fs0, "03" => $fs0, "04" => $fs0, "05" => $fs0, "06" => $fs0, "07" => $fs0, "08" => $fs0, "09" => $fs0, "10" => $fs0, "11" => $fs0, "12" => $fs0];
                    for ($m=1; $m <= 12; $m++) {
                        foreach ($this->getDaysInMonth($m) as $k=>$v) {
                            if (!isset($resultDAY[$typeID][$itemID][$k])) $resultDAY[$typeID][$itemID][$k] = $fs0;
                            if (!isset($resultDAY['types'][$typeID][$k])) $resultDAY['types'][$typeID][$k] = $fs0;
                        }
                    }
                    if (!isset($result[$typeID][$itemID]['totalFlowSum'])) $result[$typeID][$itemID]['totalFlowSum'] = 0;
                    if (!isset($result['types'][$typeID]['totalFlowSum'])) $result['types'][$typeID]['totalFlowSum'] = 0;
                }

                foreach ($items as $item) {
                    if (isset($item['contractorType'])) {
                        $itemID = $item['contractorType'];
                        $flowSum = (int)$item['flowSum'];
                        if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                            $itemName = 'Приход из ' . self::$cashContractorTextByType[$type][$itemID];
                        } else {
                            $itemName = 'Перевод в ' . self::$cashContractorTextByType[$type][$itemID];
                        }
                    } else {
                        $itemID = $item[$params['itemAttrName']];
                        $flowSum = (int)$item['flowSum'];
                        $itemName = $item['itemName'];
                    }

                    $monthNumber = substr($item['date'], 5, 2);
                    $dayNumber = $item['date'];

                    // Сумма по статьям помесячно
                    $result[$typeID][$itemID][$monthNumber]['flowSum'] += $flowSum;
                    $resultDAY[$typeID][$itemID][$dayNumber]['flowSum'] += $flowSum;

                    // Итого по статьям
                    $result[$typeID][$itemID]['totalFlowSum'] += $flowSum;

                    // Существующие статьи
                    $result['itemName'][$typeID][$itemID] = $itemName;

                    // Сумма по типам помесячно
                    $result['types'][$typeID][$monthNumber]['flowSum'] += $flowSum;
                    $resultDAY['types'][$typeID][$dayNumber]['flowSum'] += $flowSum;

                    // Итого по типам
                    $result['types'][$typeID]['totalFlowSum'] += $flowSum;
                }

                // Сальдо
                foreach (self::$month as $monthNumber => $monthName) {
                    $sum = isset($result['types'][$typeID][$monthNumber]['flowSum']) ?
                        (int)$result['types'][$typeID][$monthNumber]['flowSum'] : 0;
                    if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $result['balance'][$monthNumber] += $sum;
                    } else {
                        $result['balance'][$monthNumber] -= $sum;
                    }
                    // Сальдо нарастающим
                    $previousMonth = intval($monthNumber) - 1;
                    $previousMonth = $previousMonth > 9 ? $previousMonth : ('0' . $previousMonth);

                    $result['growingBalance'][$monthNumber] = $result['balance'][$monthNumber] +
                        (isset($result['growingBalance'][$previousMonth]) ? $result['growingBalance'][$previousMonth] : 0);
                }
                for ($m=1; $m<=12; $m++) foreach ($this->getDaysInMonth($m) as $date => $dayName) {
                    $sum = isset($resultDAY['types'][$typeID][$date]['flowSum']) ?
                        (int)$resultDAY['types'][$typeID][$date]['flowSum'] : 0;
                    if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $resultDAY['balance'][$date] += $sum;
                    } else {
                        $resultDAY['balance'][$date] -= $sum;
                    }
                    // Сальдо нарастающим
                    $prevDate = date('Y-m-d', strtotime($date) - 24 * 3600);

                    $resultDAY['growingBalance'][$date] = $resultDAY['balance'][$date] +
                        (isset($resultDAY['growingBalance'][$prevDate]) ? $resultDAY['growingBalance'][$prevDate] : 0);
                }

            /////}

            $sum = isset($result['types'][$typeID]['totalFlowSum']) ? $result['types'][$typeID]['totalFlowSum'] : 0;
            if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                $result['balance']['totalFlowSum'] += $sum;
                $resultDAY['balance']['totalFlowSum'] += $sum;
            } else {
                $result['balance']['totalFlowSum'] -= $sum;
                $resultDAY['balance']['totalFlowSum'] -= $sum;
            }
            if (isset($result['itemName'][$typeID])) {
                asort($result['itemName'][$typeID]);
            }
        }

        ////// PREV YEARS
        if ($this->year !== min($this->getYearFilter())) {

            $dateFrom = min($this->getYearFilter()) . '-01-01';
            $dateTo = ($this->year - 1) . '-12-31';

            foreach (self::$purseTypes as $typeID => $typeName) {
                /* @var $flowClassName CashBankFlows|CashOrderFlows|CashEmoneyFlows */
                $flowClassName = $this->getFlowClassNameByType(self::$purseBlockByType[$typeID]);
                $type = self::$flowTypeByPurseType[$typeID];
                $params = $this->getParamsByFlowType($type);
                $query = $this->getDefaultCashFlowsQuery($flowClassName, $params, $dateFrom, $dateTo);
                $items = $this->filterByPurseQuery($query)
                    ->select([
                        $flowClassName::tableName() . ".{$params['itemAttrName']}",
                        'invoiceItem.name as itemName',
                        'SUM(' . $flowClassName::tableName() . '.amount) as flowSum'
                    ])
                    ->groupBy([$flowClassName::tableName() . ".{$params['itemAttrName']}"])
                    ->asArray()
                    ->all();
                $itemsByCashContractor = $this->getDefaultItemsByCashFlowContractorQuery(
                    $flowClassName, $type, self::$cashContractorTypeByTableName[$flowClassName], $dateFrom, $dateTo
                )
                    ->groupBy([$flowClassName::tableName() . '.contractor_id'])
                    ->asArray()
                    ->all();
                foreach ($itemsByCashContractor as $itemByCashContractor) {
                    $items[] = $itemByCashContractor;
                }
                foreach ($items as $item) {
                    $prevSum[$params['prevSumAttrName']] += (int)$item['flowSum'];
                    if (!isset($prevSum[self::$purseBlockByType[$typeID]][$params['prevSumAttrName']])) {
                        $prevSum[self::$purseBlockByType[$typeID]][$params['prevSumAttrName']] = $item['flowSum'];
                    } else {
                        $prevSum[self::$purseBlockByType[$typeID]][$params['prevSumAttrName']] += $item['flowSum'];
                    }
                }
            }
            $previousBalance = $prevSum['incomePrevSum'] - $prevSum['expenditurePrevSum'];

            // Sum with prev years
            foreach ($result['growingBalance'] as $key => $value) {
                $result['growingBalance'][$key] += $previousBalance;
            }
            // Sum with prev years by days
            foreach ($resultDAY['growingBalance'] as $key => $value) {
                $resultDAY['growingBalance'][$key] += $previousBalance;
            }
        }

        // Calculate Block Sum
        $incomeBlocks = [self::INCOME_CASH_BANK, self::INCOME_CASH_ORDER, self::INCOME_CASH_EMONEY];
        $result['blocks'] = [];
        if (isset($result['types'])) {
            foreach ($result['types'] as $key => $data) {
                foreach ($data as $month => $value) {
                    $formattedValue = $month == 'totalFlowSum' ? $value : $value['flowSum'];
                    if (!isset($result['blocks'][self::$purseBlockByType[$key]][$month]['flowSum'])) {
                        $result['blocks'][self::$purseBlockByType[$key]][$month]['flowSum'] = 0;
                    }
                    if (in_array($key, $incomeBlocks)) {
                        $result['blocks'][self::$purseBlockByType[$key]][$month]['flowSum'] += $formattedValue;
                    } else {
                        $result['blocks'][self::$purseBlockByType[$key]][$month]['flowSum'] -= $formattedValue;
                    }
                }
            }
        }
        // Calculate Block Sum by days
        $resultDAY['blocks'] = [];
        if (isset($resultDAY['types'])) {
            foreach ($resultDAY['types'] as $key => $data) {
                foreach ($data as $date => $value) {
                    $formattedValue = $date == 'totalFlowSum' ? $value : $value['flowSum'];
                    if (!isset($resultDAY['blocks'][self::$purseBlockByType[$key]][$date]['flowSum'])) {
                        $resultDAY['blocks'][self::$purseBlockByType[$key]][$date]['flowSum'] = 0;
                    }
                    if (in_array($key, $incomeBlocks)) {
                        $resultDAY['blocks'][self::$purseBlockByType[$key]][$date]['flowSum'] += $formattedValue;
                    } else {
                        $resultDAY['blocks'][self::$purseBlockByType[$key]][$date]['flowSum'] -= $formattedValue;
                    }
                }
            }
        }

        // Calculate Growing Balance By Block
        foreach (self::$purseBlocks as $blockType => $blockName) {
            $key = 0;
            foreach ($statisticsPeriod as $monthNumber => $monthText) {
                $formattedValue = isset($result['blocks'][$blockType][$monthNumber]['flowSum']) ?
                    $result['blocks'][$blockType][$monthNumber]['flowSum'] : 0;
                $prevFlowSum = 0;
                if (!isset($growingBalanceByBlock[$blockType][$monthNumber]['flowSum'])) {
                    $result['growingBalanceByBlock'][$blockType][$monthNumber]['flowSum'] = $formattedValue;
                }

                if ($key === 0) {
                    if (isset($prevSum[$blockType]['incomePrevSum'])) {
                        $prevFlowSum += $prevSum[$blockType]['incomePrevSum'];
                    }
                    if (isset($prevSum[$blockType]['expenditurePrevSum'])) {
                        $prevFlowSum -= $prevSum[$blockType]['expenditurePrevSum'];
                    }
                } else {
                    $prevMonth = $monthNumber - 1;
                    $prevMonth = $prevMonth < 10 ? (0 . $prevMonth) : $prevMonth;
                    $prevFlowSum = $result['growingBalanceByBlock'][$blockType][$prevMonth]['flowSum'];
                }
                $result['growingBalanceByBlock'][$blockType][$monthNumber]['flowSum'] += $prevFlowSum;
                $key++;
            }
        }
        // Calculate Growing Balance By Block by day
        foreach (self::$purseBlocks as $blockType => $blockName) {
            $key = 0;
            for($m=1; $m<=12; $m++) foreach ($this->getDaysInMonth($m) as $date => $dayText) {
                $formattedValue = isset($resultDAY['blocks'][$blockType][$date]['flowSum']) ?
                    $resultDAY['blocks'][$blockType][$date]['flowSum'] : 0;
                $prevFlowSum = 0;
                if (!isset($growingBalanceByBlock[$blockType][$date]['flowSum'])) {
                    $resultDAY['growingBalanceByBlock'][$blockType][$date]['flowSum'] = $formattedValue;
                }

                if ($key === 0) {
                    if (isset($prevSum[$blockType]['incomePrevSum'])) {
                        $prevFlowSum += $prevSum[$blockType]['incomePrevSum'];
                    }
                    if (isset($prevSum[$blockType]['expenditurePrevSum'])) {
                        $prevFlowSum -= $prevSum[$blockType]['expenditurePrevSum'];
                    }
                } else {
                    $prevDate = date('Y-m-d', strtotime($date) - 24 * 3600);
                    $prevFlowSum = $resultDAY['growingBalanceByBlock'][$blockType][$prevDate]['flowSum'];
                }
                $resultDAY['growingBalanceByBlock'][$blockType][$date]['flowSum'] += $prevFlowSum;
                $key++;
            }
        }

        return ['result' => $result, 'resultDAY' => $resultDAY];
    }

    /**
     * @param Company $company
     * @param $growingBalance
     * @param $types
     * @return array
     * @throws \yii\base\Exception
     */
    public function generateWarnings(Company $company, $growingBalance, $types)
    {
        $warnings = [];
        if ($growingBalance) {
            foreach ($growingBalance as $monthNumber => $oneAmount) {
                if ($oneAmount < 0) {
                    if (isset($warnings[1])) {
                        $warnings[1]['moreThenOne'] = true;
                    } else {
                        $warnings[1] = [
                            'amount' => abs($oneAmount),
                            'month' => self::$monthEnd[$monthNumber],
                            'moreThenOne' => false,
                        ];
                    }
                }
            }
        }

        $totalDebtSum = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_0_MORE_90, true, false);
        if ($totalDebtSum) {
            $incomeOperatingActivitiesAmount = 1;
            if (isset($types[self::INCOME_OPERATING_ACTIVITIES][date('m')]['flowSum'])
            && $types[self::INCOME_OPERATING_ACTIVITIES][date('m')]['flowSum'] > 0) {
                $incomeOperatingActivitiesAmount = $types[self::INCOME_OPERATING_ACTIVITIES][date('m')]['flowSum'];
            }
            $incomePercent = round(($totalDebtSum / 2 / $incomeOperatingActivitiesAmount) * 100, 2);
            $warnings[2] = [
                'totalDebtSum' => $totalDebtSum,
                'incomePercent' => $incomePercent,
            ];
        }
        $inInvoiceDebtSum = Invoice::find()
            ->select([
                'SUM(' . Invoice::tableName() . '.total_amount_with_nds) - IFNULL(SUM(' . Invoice::tableName() . '.payment_partial_amount), 0) AS sum',
            ])
            ->byIOType(Documents::IO_TYPE_IN)
            ->byDeleted()
            ->byStatus(InvoiceStatistic::$invoiceStatuses[InvoiceStatistic::NOT_PAID][Documents::IO_TYPE_IN])
            ->byCompany($company->id)
            ->asArray()
            ->one();
        $inInvoiceDebtSum = $inInvoiceDebtSum ? $inInvoiceDebtSum['sum'] : 0;
        if ($inInvoiceDebtSum) {
            $warnings[3] = [
                'inInvoiceDebtSum' => $inInvoiceDebtSum,
            ];
            $growingBalanceAmount = 0;
            if (isset($growingBalance[date('m')])) {
                $growingBalanceAmount = $growingBalance[date('m')];
            }
            if ($growingBalanceAmount >= $inInvoiceDebtSum) {
                $warnings[3]['message'] = 'Текущего остатка денежных средств хватит полностью оплатить все счета поставщикам.';
            } else {
                $canPayedPercent = round(($growingBalanceAmount / $inInvoiceDebtSum) * 100, 2);
                $warnings[3]['message'] = 'Текущего остатка денежных средств НЕ хватит полностью оплатить все счета
                 поставщикам. Можно оплатить только ' . $canPayedPercent . '% задолженности перед поставщиками.';
                if ($totalDebtSum) {
                    $zRaznitsa = $inInvoiceDebtSum - $growingBalanceAmount;
                    if ($zRaznitsa <= $totalDebtSum) {
                        $warnings[4] = [
                            'message' => 'Собрав ' . round(($zRaznitsa / $totalDebtSum) * 100, 2) . '% долгов ваших клиентов, вы сможете полностью оплатить долги перед поставщиками.',
                        ];
                    } else {
                        $warnings[4] = [
                            'message' => '<span style="color: red;">Плохие новости!</span><br>
                            Даже собрав 100% задолженности покупателей, вы не сможете рассчитаться с поставщиками.<br>
                            Не хватает ' . TextHelper::invoiceMoneyFormat($zRaznitsa - $totalDebtSum, 2) . ' <i class="fa fa-rub"></i>.
                            Выход:
                            <ul style="padding-left: 17px;">
                                <li>
                                    Увеличить продажи;
                                </li>
                                <li>
                                    Внести собственные деньги в компанию (дать займ);
                                </li>
                                <li>
                                    Взять кредит.
                                </li>
                            </ul>',
                            'debtPercent' => round(($zRaznitsa / $totalDebtSum) * 100, 2),
                        ];
                    }
                }
            }
        }

        return $warnings;
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     * @throws \Exception
     */
    public function searchItems($params = [])
    {
        $this->load($params);
        $this->checkCashContractor();
        $periodStart = $periodEnd = null;
        if ($this->period) {
            if (isset($this->period['day'])) {
                $day = ($this->period['day'] > 9) ? $this->period['day'] : ('0' . $this->period['day']);
                $periodStart = $this->year . '-' . $this->period['start'] . '-' . $day;
                $periodEnd = $this->year . '-' . $this->period['end'] . '-' . $day;
            } else {
                $periodStart = $this->year . '-' . $this->period['start'] . '-01';
                $periodEnd = $this->year . '-' . $this->period['end'] . '-' . cal_days_in_month(CAL_GREGORIAN, $this->period['end'], $this->year);
            }
        }
        // payment_type: 1-bank, 2-order, 3-emoney, 4-acquiring
        if ($this->payment_type == self::CASH_ACQUIRING_BLOCK) {
            $query = (new Query)->from(['t' => $this->getAcquiringQuery($this->company->id, $periodStart, $periodEnd)->groupBy('t.id')]);
        } elseif ($this->payment_type) {
            $query = (new Query)
                ->from(['t' => $this->getFlowQuery($this->payment_type, $this->company->id, $periodStart, $periodEnd)->groupBy('t.id')]);
        } else {
            $cbf = $this->getFlowQuery(self::CASH_BANK_BLOCK, $this->company->id, $periodStart, $periodEnd)->groupBy('t.id');
            $cef = $this->getFlowQuery(self::CASH_EMONEY_BLOCK, $this->company->id, $periodStart, $periodEnd)->groupBy('t.id');
            $cof = $this->getFlowQuery(self::CASH_ORDER_BLOCK, $this->company->id, $periodStart, $periodEnd)->groupBy('t.id');
            $aof = $this->getAcquiringQuery($this->company->id, $periodStart, $periodEnd)->groupBy('t.id');
            $query = (new Query)
                ->from(['t' => $cbf->union($cef, true)->union($cof, true)->union($aof, true)]);
        }

        $query->addSelect([
            't.id',
            't.tb',
            'SUM(amountExpense) as amountExpense',
            'SUM(amountIncome) as amountIncome',
            't.rs',
            't.created_at',
            't.date',
            't.flow_type',
            't.amount',
            't.contractor_id',
            't.description',
            't.expenditure_item_id',
            't.income_item_id',
            't.document_number',
            't.document_additional_number',
            't.incomeItemName',
            't.expenseItemName',
        ]);
        switch ($this->group) {
            case self::GROUP_DATE:
                $query->groupBy('t.date');
                break;
            case self::GROUP_CONTRACTOR:
                $query->groupBy('t.contractor_id');
                break;
            case self::GROUP_PAYMENT_TYPE:
                $query->groupBy(['t.tb', 't.rs']);
                break;
            default:
                $query->groupBy('t.id');
                break;
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'date' => [
                        'asc' => [
                            'date' => SORT_ASC,
                            'created_at' => SORT_ASC,
                        ],
                        'desc' => [
                            'date' => SORT_DESC,
                            'created_at' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'amountIncome' => [
                        'asc' => [
                            'flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'amountExpense' => [
                        'asc' => [
                            'flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'contractor_id',
                    'description',
                    'created_at',
                    'billPaying' => [
                        'asc' => ['document_number' => SORT_ASC, 'document_additional_number' => SORT_ASC],
                        'desc' => ['document_number' => SORT_DESC, 'document_additional_number' => SORT_ASC],
                        'default' => SORT_ASC
                    ],
                ],
                'defaultOrder' => [
                    'date' => SORT_DESC,
                ],
            ],
        ]);

        $query->andFilterWhere(['t.contractor_id' => $this->contractor_id]);

        if (!empty($this->income_item_id) && !empty($this->expenditure_item_id)) {
            $query->andWhere(['or',
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_EXPENSE],
                    ['in', 'expenditure_item_id', $this->expenditure_item_id],
                ],
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_INCOME],
                    ['in', 'income_item_id', $this->income_item_id],
                ],
                $this->cash_contractor ? ['in', 'contractor_id', $this->cash_contractor] : [],
            ]);
        } elseif (!empty($this->income_item_id)) {
            $query->andWhere(['or',
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_INCOME],
                    ['in', 'income_item_id', $this->income_item_id],
                ],
                $this->cash_contractor ? ['in', 'contractor_id', $this->cash_contractor] : [],
            ]);
        } elseif (!empty($this->expenditure_item_id)) {
            $query->andWhere(['or',
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_EXPENSE],
                    ['in', 'expenditure_item_id', $this->expenditure_item_id],
                ],
                $this->cash_contractor ? ['in', 'contractor_id', $this->cash_contractor] : [],
            ]);
        } else {
            if ($this->cash_contractor) {
                $query->andWhere(['in', 'contractor_id', $this->cash_contractor]);
            } else {
                $query->andWhere(['flow_type' => null]);
            }
        }

        if ($this->reason_id == 'empty') {
            $query->andWhere(['or',
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_EXPENSE],
                    ['expenditure_item_id' => null],
                ],
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_INCOME],
                    ['income_item_id' => null],
                ],
            ]);
        } elseif (!empty($this->reason_id)) {
            $this->expenditure_item_id = $this->income_item_id = null;
            if (substr($this->reason_id, 0, 2) === 'e_') {
                $this->expenditure_item_id = substr($this->reason_id, 2);
            } elseif (substr($this->reason_id, 0, 2) === 'i_') {
                $this->income_item_id = substr($this->reason_id, 2);
            }
            $query->andFilterWhere([
                'expenditure_item_id' => $this->expenditure_item_id,
                'income_item_id' => $this->income_item_id,
            ]);
        }
        $this->_filterQuery = clone $query;

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $range = [];
        $cashOrderMinDate = CashOrderFlows::find()
            ->byCompany($company->id)
            ->min('date');
        $cashBankMinDate = CashBankFlows::find()
            ->byCompany($company->id)
            ->min('date');
        $cashEmoneyMinDate = CashEmoneyFlows::find()
            ->byCompany($company->id)
            ->min('date');
        $minDates = [];

        if ($cashOrderMinDate) $minDates[] = $cashOrderMinDate;
        if ($cashBankMinDate) $minDates[] = $cashBankMinDate;
        if ($cashEmoneyMinDate) $minDates[] = $cashEmoneyMinDate;

        $minCashDate = !empty($minDates) ? min($minDates) : date(DateHelper::FORMAT_DATE);
        $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
        $currentYear = date('Y');
        foreach (range($registrationYear, $currentYear) as $value) {
            $range[$value] = $value;
        }
        arsort($range);

        return $range;
    }

    /**
     * @param $type
     * @throws \Exception
     */
    public function generateXls($type)
    {
        $data = $this->search($type, Yii::$app->request->get());
        $this->buildXls($type, $data, "ОДДС за {$this->year} год");
    }

    /**
     * @param $data
     * @param null $formName
     */
    public function _load($data, $formName = null)
    {
        if ($this->load($data, $formName)) {
            if (!in_array($this->year, $this->getYearFilter())) {
                $this->year = date('Y');
            }
        }
    }

    /**
     * @param $type
     * @return string
     * @throws \Exception
     */
    private function getFlowClassNameByType($type)
    {
        switch ($type) {
            case self::CASH_BANK_BLOCK:
                $className = CashBankFlows::class;
                break;
            case self::CASH_ORDER_BLOCK:
                $className = CashOrderFlows::class;
                break;
            case self::CASH_EMONEY_BLOCK:
                $className = CashEmoneyFlows::class;
                break;
            case self::CASH_ACQUIRING_BLOCK:
                $className = AcquiringOperation::class;
                break;
            default:
                throw new \Exception('Invalid type.');
                break;
        }

        return $className;
    }

    /**
     * @param $cashBlock
     * @param $companyID
     * @param $periodStart
     * @param $periodEnd
     * @return \common\models\cash\query\CashFlowBase|\common\models\cash\query\CashOrderFlow
     * @throws \Exception
     */
    private function getFlowQuery($cashBlock, $companyID, $periodStart, $periodEnd)
    {
        /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
        $className = $this->getFlowClassNameByType($cashBlock);
        $tableName = $className::tableName();

        return $className::find()
            ->select([
                't.id',
                new Expression('"' . $tableName . '" as tb'),
                "IF({{t}}.[[flow_type]] = 0, SUM({{t}}.[[amount]]), 0) as amountExpense",
                "IF({{t}}.[[flow_type]] = 1, SUM({{t}}.[[amount]]), 0) as amountIncome",
                $cashBlock == self::CASH_BANK_BLOCK ? 't.rs as rs' : new Expression('null as rs'),
                't.created_at',
                't.date',
                't.flow_type',
                't.amount',
                't.contractor_id',
                't.description',
                't.expenditure_item_id',
                't.income_item_id',
                Invoice::tableName() . '.document_number',
                Invoice::tableName() . '.document_additional_number',
                InvoiceIncomeItem::tableName() . '.name incomeItemName',
                InvoiceExpenditureItem::tableName() . '.name expenseItemName',
            ])
            ->from(['t' => $tableName])
            ->joinWith('invoices')
            ->joinWith('incomeReason')
            ->joinWith('expenditureReason')
            ->andWhere(['t.company_id' => $companyID])
            ->andWhere(['between', 't.date', $periodStart, $periodEnd]);
    }

    /**
     * @param $companyID
     * @param $periodStart
     * @param $periodEnd
     * @return \common\modules\acquiring\query\AcquiringQuery
     */
    private function getAcquiringQuery($companyID,  $periodStart, $periodEnd) {
        $tableName = AcquiringOperation::tableName();

        return AcquiringOperation::find()
            ->select([
                't.id',
                new Expression('"' . $tableName . '" as tb'),
                "IF({{t}}.[[flow_type]] = 0, SUM({{t}}.[[amount]]), 0) as amountExpense",
                "IF({{t}}.[[flow_type]] = 1, SUM({{t}}.[[amount]]), 0) as amountIncome",
                new Expression('null as rs'),
                new Expression('null as created_at'),
                't.date',
                't.flow_type',
                't.amount',
                't.contractor_id',
                't.description',
                't.expenditure_item_id',
                't.income_item_id',
                new Expression('null as document_number'),
                new Expression('null as document_additional_number'),
                InvoiceIncomeItem::tableName() . '.name incomeItemName',
                InvoiceExpenditureItem::tableName() . '.name expenseItemName',
            ])
            ->from(['t' => $tableName])
            ->joinWith('incomeItem')
            ->joinWith('expenditureItem')
            ->andWhere(['t.company_id' => $companyID])
            ->andWhere(['between', 't.date', $periodStart, $periodEnd]);
    }

    /**
     * @param $dates
     * @param bool $plan
     * @return array
     */
    public function searchByActivityByDate($dates, $plan = false)
    {
        $result = [];

        $flowClassList = (!$plan) ?
            self::flowClassArray() :
            [PlanCashFlows::class];

        foreach ($dates as $date) {
            $dateFrom = $date;
            $dateTo = $date;
            if (!isset($result['growingBalance'][$date])) {
                $result['growingBalance'][$date] = 0;
            }
            if (!isset($result['growingBalanceContractorBalance'][$date])) {
                $result['growingBalanceContractorBalance'][$date] = 0;
            }
            /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
            foreach ($flowClassList as $className) {
                $result['growingBalanceContractorBalance'][$date] += $this->getCashFlowsContractorBalanceAmount($className, $dateFrom, $dateTo);
            }

            /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
            foreach ($flowClassList as $className) {
                if (!isset($result['balance'][$date])) {
                    $result['balance'][$date] = 0;
                }
                $query = $className::find()
                    ->andWhere(['between', $className::tableName() . '.date', $dateFrom, $dateTo])
                    ->andWhere([$className::tableName() . '.company_id' => $this->company->id])
                    ->andWhere(['not', ['contractor_id' =>
                        [
                            CashContractorType::BANK_TEXT,
                            CashContractorType::ORDER_TEXT,
                            CashContractorType::EMONEY_TEXT,
                        ]
                    ]]);
                $items = $query
                    ->select([
                        $className::tableName() . ".flow_type",
                        'SUM(' . $className::tableName() . '.amount) as flowSum'
                    ])
                    ->groupBy([$className::tableName() . ".flow_type"])
                    ->asArray()
                    ->all();

                foreach ($items as $item) {
                    $flowSum = (int)$item['flowSum'];

                    if ($item['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $result['balance'][$date] += $flowSum;
                    } else {
                        $result['balance'][$date] -= $flowSum;
                    }
                }
            }
        }

        $result['prevBalance'] = 0;
        $result['prevBalanceContractorBalance'] = 0;
        if (str_replace('-', '', $this->getDateFirstFlowFilter()) < str_replace('-', '', reset($dates))) {
            $dateFrom = $this->getDateFirstFlowFilter();
            $currDate = \DateTime::createFromFormat('Y-m-d', reset($dates));
            $currDate->modify("-1 day");
            $dateTo = $currDate->format('Y-m-d');

            /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
            foreach ($flowClassList as $className) {
                $result['prevBalanceContractorBalance'] += $this->getCashFlowsContractorBalanceAmount($className, $dateFrom, $dateTo);
            }

            foreach ($flowClassList as $className) {
                $query = $className::find()
                    ->andWhere(['between', $className::tableName() . '.date', $dateFrom, $dateTo])
                    ->andWhere([$className::tableName() . '.company_id' => $this->company->id])
                    ->andWhere(['not', ['contractor_id' =>
                        [
                            CashContractorType::BANK_TEXT,
                            CashContractorType::ORDER_TEXT,
                            CashContractorType::EMONEY_TEXT,
                        ]
                    ]]);
                $items = $query
                    ->select([
                        $className::tableName() . ".flow_type",
                        'SUM(' . $className::tableName() . '.amount) as flowSum'
                    ])
                    ->groupBy([$className::tableName() . ".flow_type"])
                    ->asArray()
                    ->all();

                foreach ($items as $item) {
                    $flowSum = (int)$item['flowSum'];

                    if ($item['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $result['prevBalance'] += $flowSum;
                    } else {
                        $result['prevBalance'] -= $flowSum;
                    }
                }
            }
        }

        // add current periods
        foreach ($dates as $date) {
            $currDate = \DateTime::createFromFormat('Y-m-d', $date);
            $currDate->modify("-1 day");
            $previousDate = $currDate->format('Y-m-d');

            $result['growingBalance'][$date] = $result['balance'][$date] +
                $result['growingBalanceContractorBalance'][$date] +
                (isset($result['growingBalance'][$previousDate]) ? $result['growingBalance'][$previousDate] : 0);
        }

        // add all prev years
        foreach ($dates as $date) {
            $result['growingBalance'][$date] += $result['prevBalance'];
            $result['growingBalance'][$date] += $result['prevBalanceContractorBalance'];
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getDateFirstFlowFilter()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $range = [];
        $cashOrderMinDate = CashOrderFlows::find()
            ->byCompany($company->id)
            ->min('date');
        $cashBankMinDate = CashBankFlows::find()
            ->byCompany($company->id)
            ->min('date');
        $cashEmoneyMinDate = CashEmoneyFlows::find()
            ->byCompany($company->id)
            ->min('date');
        $minDates = [];

        if ($cashOrderMinDate) $minDates[] = $cashOrderMinDate;
        if ($cashBankMinDate) $minDates[] = $cashBankMinDate;
        if ($cashEmoneyMinDate) $minDates[] = $cashEmoneyMinDate;

        $minCashDate = !empty($minDates) ? min($minDates) : date(DateHelper::FORMAT_DATE);

        return $minCashDate;
    }
}