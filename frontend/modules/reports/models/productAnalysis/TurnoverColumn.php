<?php

namespace frontend\modules\reports\models\productAnalysis;

use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\OrderAct;
use common\models\document\OrderPackingList;
use common\models\document\OrderUpd;
use common\models\document\PackingList;
use common\models\document\status\ActStatus;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\PackingListStatus;
use common\models\document\status\UpdStatus;
use common\models\document\Upd;
use common\models\product\Product;
use yii\db\Expression;
use yii\db\Query;
use common\models\document\Order;

class TurnoverColumn {

    /**
     * @return yii\db\Query
     */
    public static function query($select, $where, $searchByProductionType = Product::PRODUCTION_TYPE_GOODS)
    {
        if ($searchByProductionType == Product::PRODUCTION_TYPE_GOODS) {
            // оборот по товарным накладным
            $query1 = OrderPackingList::find()->alias('orderDoc')->select($select)
                ->addSelect(['by_document' => new Expression('"packing-list"')])
                ->leftJoin(['document' => PackingList::tableName()], '{{document}}.[[id]] = {{orderDoc}}.[[packing_list_id]]')
                ->leftJoin(['invoice' => Invoice::tableName()], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
                ->leftJoin(['product' => Product::tableName()], '{{product}}.[[id]] = {{orderDoc}}.[[product_id]]')
                ->leftJoin(['order' => Order::tableName()], '{{order}}.[[id]] = {{orderDoc}}.[[order_id]]')
                ->andWhere([
                    'product.production_type' => $searchByProductionType,
                    'invoice.is_deleted' => false,
                    'invoice.invoice_status_id' => InvoiceStatus::$validInvoices,
                ])
                ->andWhere([
                    'or',
                    ['document.status_out_id' => PackingListStatus::$turmoverStatus],
                    ['document.status_out_id' => null],
                ]);
        } else {
            // оборот актам
            $query1 = OrderAct::find()->alias('orderDoc')->select($select)
                ->addSelect(['by_document' => new Expression('"act"')])
                ->leftJoin(['document' => Act::tableName()], '{{document}}.[[id]] = {{orderDoc}}.[[act_id]]')
                ->leftJoin(['invoice' => Invoice::tableName()], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
                ->leftJoin(['product' => Product::tableName()], '{{product}}.[[id]] = {{orderDoc}}.[[product_id]]')
                ->leftJoin(['order' => Order::tableName()], '{{order}}.[[id]] = {{orderDoc}}.[[order_id]]')
                ->andWhere([
                    'product.production_type' => $searchByProductionType,
                    'invoice.is_deleted' => false,
                    'invoice.invoice_status_id' => InvoiceStatus::$validInvoices,
                ])
                ->andWhere([
                    'or',
                    ['document.status_out_id' => ActStatus::$turmoverStatus],
                    ['document.status_out_id' => null],
                ]);
        }
        // оборот по УПД
        $query2 = OrderUpd::find()->alias('orderDoc')->select($select)
            ->addSelect(['by_document' => new Expression('"upd"')])
            ->leftJoin(['document' => Upd::tableName()], '{{document}}.[[id]] = {{orderDoc}}.[[upd_id]]')
            ->leftJoin(['invoice' => Invoice::tableName()], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
            ->leftJoin(['product' => Product::tableName()], '{{product}}.[[id]] = {{orderDoc}}.[[product_id]]')
            ->leftJoin(['order' => Order::tableName()], '{{order}}.[[id]] = {{orderDoc}}.[[order_id]]')
            ->andWhere([
                'product.production_type' => $searchByProductionType,
                'invoice.is_deleted' => false,
                'invoice.invoice_status_id' => InvoiceStatus::$validInvoices,
            ])
            ->andWhere([
                'or',
                ['document.status_out_id' => UpdStatus::$turmoverStatus],
                ['document.status_out_id' => null],
            ]);

        if ($where) {
            $query1->andWhere($where);
            $query2->andWhere($where);
        }

        return $query1->union($query2, true);
    }

}