<?php

namespace frontend\modules\reports\models\productAnalysis;

use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\document\Invoice;
use common\models\employee\Employee;
use common\models\product\Product;
use common\models\product\ProductInitialBalance;
use common\models\product\ProductSearch;
use common\models\product\ProductTurnoverSearch;
use DateTime;
use frontend\models\Documents;
use Yii;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use frontend\modules\cash\models\CashContractorType;
use frontend\modules\reports\models\AbstractFinance;
use frontend\modules\reports\models\PlanCashFlows;
use yii\db\Expression;
use yii\db\Query;

class ProductAnalysisCharts extends AbstractFinance {

    public $yearMonth;

    /**
     * @param $left
     * @param $right
     * @param int $offsetYear
     * @param string $offsetMonth
     * @return array
     * @throws \Exception
     */
    public function getFromCurrentMonthsPeriods($left, $right, $offsetYear = 0, $offsetMonth = "0") {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month")->modify("{$offsetMonth} month");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr->format('Y-m-01'),
                'to' => $curr->format('Y-m-t'),
            ];
            $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }

    /**
     * @param array $dates
     * @param int $groupId
     * @param string $period
     * @return array
     * @throws \yii\base\ErrorException
     */
    public function getTurnoverSeriesData($dates, $groupId = null, $period = "months")
    {
        $resultFact = [];
        $resultBalance = [];
        $resultMargin = [];

        $INCOME = Documents::IO_TYPE_IN;
        $OUTCOME = Documents::IO_TYPE_OUT;

        $dateStart = $dates[0]['from'];
        $dateEnd = $dates[count($dates)-1]['to'];
        $dateKeyLength = ($period == "months") ? 7 : 10;

        // 0. Zeroes
        foreach ($dates as $date) {

            $pos = substr($date['from'], 0, $dateKeyLength);

            $resultFact[$INCOME][$pos] = 0;
            $resultFact[$OUTCOME][$pos] = 0;
            $resultBalance[$pos] = 0;
            $resultMargin[$pos] = 0;
        }
        // 1. Sells/Buys
        $select = new Expression('
            `document`.`type` AS type, 
            `document`.`document_date` AS date, 
            `orderDoc`.`quantity` AS quantity, 
            `order`.`purchase_price_with_vat` AS buy_price, 
            `order`.`selling_price_with_vat` AS sell_price,
            `product`.`price_for_buy_with_nds` AS product_buy_price
        ');
        $where = [
            'and',
            ['invoice.company_id' => $this->company->id],
            ['between', 'document.document_date', $dateStart, $dateEnd],
        ];

        if ($groupId) {
            $where[] = ['product.group_id' => $groupId];
        }

        $query = new Query;
        $query->from(['t' => TurnoverColumn::query($select, $where)]);
        $turnovers = $query->orderBy('date')->all(Yii::$app->db2);

        foreach ($turnovers as $f) {
            $pos = substr($f['date'], 0, $dateKeyLength);
            if ($f['type'] == Documents::IO_TYPE_IN) {
                $resultFact[$f['type']][$pos] += $f['buy_price'] * $f['quantity'] / 100;
            }
            if ($f['type'] == Documents::IO_TYPE_OUT) {
                $resultFact[$f['type']][$pos] += $f['sell_price'] * $f['quantity'] / 100;
            }
        }

        // 2. Balance Start Point
        $startPoint = $this->getBalanceChartStartPoint($dateStart, $dateEnd, $groupId);
        foreach ($resultBalance as $pos => $r) {
            $resultBalance[$pos] += $startPoint;
        }

        // 3. Initial Balance
        $initialBalanceByMonth = $this->getInitialBalanceArray($dateStart);
        foreach ($resultBalance as $pos => $r) {
            foreach ($initialBalanceByMonth as $i) {
                $iPos = substr($i['date'], 0, $dateKeyLength);
                if ($iPos <= $pos || strlen($iPos) === 0)
                    $resultBalance[$pos] += $i['sum'] / 100;
            }
        }

        // 4. Balance
        foreach ($resultBalance as $pos => $r) {
            foreach ($turnovers as $f) {
                $fPos = substr($f['date'], 0, $dateKeyLength);
                if ($fPos <= $pos) {
                    if ($f['type'] == Documents::IO_TYPE_IN) {
                        $resultBalance[$pos] += $f['product_buy_price'] * $f['quantity'] / 100;
                    }
                    if ($f['type'] == Documents::IO_TYPE_OUT) {
                        $resultBalance[$pos] -= $f['product_buy_price'] * $f['quantity'] / 100;
                    }
                }
            }
        }

        // 5. Margin
        foreach ($turnovers as $f) {
            $pos = substr($f['date'], 0, $dateKeyLength);
            if ($f['type'] == Documents::IO_TYPE_OUT) {
                $resultMargin[$pos] += $f['quantity'] * ($f['sell_price'] - $f['product_buy_price']) / 100;
            }
        }

        return [
            'incomeFact' => array_values($resultFact[$INCOME]),
            'outcomeFact' => array_values($resultFact[$OUTCOME]),
            'balanceFact' => array_values($resultBalance),
            'marginFact' => array_values($resultMargin),
        ];
    }

    public function getTopProductSeriesData($yearMonth, $groupId = null, $period = "months")
    {
        $result = [];

        if (strlen($yearMonth) === 6) {
            $dateStart = date_create_from_format('Ymd', $yearMonth . '01');
            $dateEnd = (clone $dateStart)->modify('last day of');
        } elseif (strlen($yearMonth) === 4) {
            $dateStart =  date_create_from_format('Ymd', $yearMonth . '0101');
            $dateEnd   =  date_create_from_format('Ymd', $yearMonth . '1231');
        } else {
            $dateStart = $dateEnd = new DateTime;
        }

        $select = new Expression('
            `document`.`type` AS type, 
            `document`.`document_date` AS date, 
            `orderDoc`.`quantity` AS quantity, 
            `order`.`purchase_price_with_vat` AS buy_price, 
            `order`.`selling_price_with_vat` AS sell_price,
            `product`.`price_for_buy_with_nds` AS product_buy_price,
            `product`.`title` AS title,
            `product`.`id` AS id
        ');
        $where = [
            'and',
            ['invoice.company_id' => $this->company->id],
            ['between', 'document.document_date', $dateStart->format('Y-m-d'), $dateEnd->format('Y-m-d')],
            ['document.type' => Documents::IO_TYPE_OUT]
        ];

        if ($groupId) {
            $where[] = ['product.group_id' => $groupId];
        }

        $query = new Query;
        $query->from(['t' => TurnoverColumn::query($select, $where)]);
        $turnovers = $query->all(Yii::$app->db2);

        // 0. Zeroes
        foreach ($turnovers as $p) {
            $pos = $p['id'];
            $result[$pos] = [
                'name' => $p['title'],
                'revenue' => 0,
                'margin' => 0,
                'quantity' => 0,
                'marginPercent' => 0,
                '_totalSellAmount' => 0,
                '_totalBuyAmount' => 0,
            ];
        }

        // 1. Amounts
        foreach ($turnovers as $p) {
            $pos = $p['id'];
            $result[$pos]['revenue'] += $p['sell_price'] * $p['quantity'] / 100;
            $result[$pos]['margin'] += $p['quantity'] * ($p['sell_price'] - $p['product_buy_price']) / 100;
            $result[$pos]['quantity'] += $p['quantity'];
            $result[$pos]['_totalSellAmount'] += $p['sell_price'] * $p['quantity'] / 100;
            $result[$pos]['_totalBuyAmount'] += $p['product_buy_price'] * $p['quantity'] / 100;
        }

        // 2. Margin percent
        foreach ($result as $k=>$r) {
            $result[$k]['marginPercent'] = ($r['_totalSellAmount']) > 0 ?
                round(100 * ((float)$r['_totalSellAmount'] - (float)$r['_totalBuyAmount']) / (float)$r['_totalSellAmount'], 2) : "-";
        }

        // 3. Sort
        $return = [
            'topRevenue' => [],
            'topMargin' => []
        ];

        uasort($result, function ($a, $b) {
            if ($a['margin'] == $b['margin']) return 0;
            return ($a['margin'] > $b['margin']) ? -1 : 1;
        });

        $return['topMargin'] = $result;

        uasort($result, function ($a, $b) {
            if ($a['revenue'] == $b['revenue']) return 0;
            return ($a['revenue'] > $b['revenue']) ? -1 : 1;
        });

        $return['topRevenue'] = $result;

        return $return;
    }

    /**
     * @param null $groupId
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getInitialBalanceArray($dateStart, $groupId = null)
    {
        if ($groupId) {
            $idArray = Product::find()->where([
                'company_id' => $this->company->id,
                'group_id' => $groupId
            ])->select('id')->column();
        } else {
            $idArray = null;
        }

        $query = Product::find()
            ->joinWith('productStores store', false)
            ->leftJoin(['pib' => ProductInitialBalance::tableName()], 'product.id = pib.product_id')
            ->byUser()
            ->where([
                'product.company_id' => $this->company->id,
                'product.production_type' => 1,
            ])
            ->andWhere(['>', 'pib.date', $dateStart]);
        if ($idArray) {
            $query->andWhere(['product.id' => $idArray]);
        }

        return $query
            ->select(new Expression('GROUP_CONCAT(`product`.`id`), `pib`.`date`, SUM(`store`.`initial_quantity` * `product`.`price_for_buy_with_nds`) AS sum'))
            ->groupBy(['DATE_FORMAT(`date`, "%Y%m")'])
            ->asArray()
            ->all();
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $range = [];
        $invoiceMinDate = Invoice::find()
            ->byCompany($company->id)
            ->min('document_date');

        $minDates = [];

        if ($invoiceMinDate) $minDates[] = $invoiceMinDate;

        $minCashDate = !empty($minDates) ? min($minDates) : date('Y-m-d');
        $registrationDate = date_create_from_format('Y-m-d', $minCashDate);
        $currentDate = (clone $registrationDate)->modify('first day of this month');

        while ($currentDate->format('Ym') <= date('Ym')) {
            $range[$currentDate->format('Ym')] = \yii\helpers\ArrayHelper::getValue(AbstractFinance::$month, substr($currentDate->format('Ym'), -2, 2)) . ' ' . substr($currentDate->format('Ym'), 0, 4);

            if ($currentDate->format('m') == 12) {
                $range[$currentDate->format('Y')] = $currentDate->format('Y');
            }

            $currentDate = $currentDate->modify('last day of this month')->setTime(23, 59, 59);
            $currentDate = $currentDate->modify("+1 second");
        }
        if ($currentDate->format('m') < 12)
            $range[$currentDate->format('Y')] = $currentDate->format('Y');

        $range = array_reverse($range, true);

        return $range;
    }

    protected function getBalanceChartStartPoint($dateStart, $dateEnd, $groupId)
    {
        if ($groupId) {
            $idArray = Product::find()->where([
                'company_id' => $this->company->id,
                'group_id' => $groupId
            ])->select('id')->column();
        } else {
            $idArray = null;
        }

        $model = new ProductSearch([
            'company_id' => $this->company->id,
            'production_type' => Product::PRODUCTION_TYPE_GOODS,
            'turnoverType' => ProductSearch::TURNOVER_BY_AMOUNT,
        ]);
        $model->setDateStart((date_create_from_format('Y-m-d', $dateStart))->setTime(0,0,0));
        $model->setDateEnd((date_create_from_format('Y-m-d', $dateEnd))->setTime(0,0,0));
        return $model->balanceAtDate(true, $idArray, false) / 100;
    }
}