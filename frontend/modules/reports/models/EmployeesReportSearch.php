<?php

namespace frontend\modules\reports\models;

use common\models\Company;
use common\models\cash\CashFlowsBase;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use DateTime;
use frontend\models\Documents;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class EmployeesReportSearch
 * @package frontend\modules\reports\models
 *
 */
class EmployeesReportSearch extends EmployeeCompany
{
    public $curr_sum;
    public $curr_count;
    public $curr_paid;
    public $curr_debt;

    protected $_company;
    protected $_date;
    protected $_dateItems;
    protected $_datePeriod;
    protected $_report_data;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params, '');

        $dateFrom = $this->getDateFrom();
        $dateTill = $this->getDateTill();

        $allModels = $this->getEmployeeCompanies()->all();
        $dataFound = [
            'sum' => $this->getSumQuery($dateFrom, $dateTill)->all(),
            'count' => $this->getCountQuery($dateFrom, $dateTill)->all(),
            'paid' => $this->getPaidQuery($dateFrom, $dateTill)->all(),
            'debt' => $this->getDebtQuery($dateFrom, $dateTill)->column(),
        ];

        $dataProvider = new ArrayDataProvider([
            'allModels' => $this->dataProcessing($allModels, $dataFound),
            'pagination' => false,
            'sort' => [
                'attributes' => [
                    'fio',
                    'curr_sum',
                    'curr_count',
                    'curr_paid',
                    'curr_debt',
                ],
                'defaultOrder' => [
                    'fio' => SORT_ASC,
                ],
            ]
        ]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function dataProcessing($allModels, $dataFound)
    {
        $data = [];

        foreach ($allModels as $model) {
            foreach (array_keys($dataFound) as $param) {
                foreach ($this->getDateKeys() as $date) {
                    $data[$model->employee_id][$param][$date] = 0;
                }
            }
        }

        $debt = ArrayHelper::remove($dataFound, 'debt', 0);
        foreach ($dataFound as $param => $paramData) {
            foreach ($paramData as $row) {
                if (array_key_exists($row['user'], $data)) {
                    $data[$row['user']][$param][$row['date']] = $row['value'] * 1;
                }
            }
        }

        foreach ($data as $id => $userData) {
            $userDebt = ArrayHelper::remove($debtData, $id, 0);
            foreach ($this->getDateKeys() as $date) {
                $userDebt = $userDebt + $data[$id]['sum'][$date] - $data[$id]['paid'][$date];
                $data[$id]['debt'][$date] = $userDebt;
            }
        }

        $currKey = $this->getDate()->format('Ym');
        array_walk($allModels, function (&$model) use ($data, $currKey) {
            $model->setReportData($data[$model->employee_id], $currKey);
        });

        return $allModels;
    }

    /**
     * @return array
     */
    public function getEmployeeCompanies()
    {
        $query = static::find()->alias('user')
            ->leftJoin(['employee' => Employee::tableName()], '{{user}}.[[employee_id]] = {{employee}}.[[id]]')
            ->where([
                'employee.is_deleted' => false,
                'user.company_id' => $this->company->id,
                'user.is_working' => true,
            ]);

        if (!Yii::$app->user->can(\frontend\rbac\permissions\Contractor::INDEX_STRANGERS)) {
            $query->andWhere([
                "user.employee_id" => Yii::$app->user->id,
            ]);
        }

        return $query;
    }

    /**
     * @return array
     */
    public function getStartDate()
    {
        $query = $this->company->getInvoices()
            ->select('document_date')
            ->andWhere([
                'is_deleted' => false,
                'type' => Documents::IO_TYPE_OUT,
            ])
            ->orderBy(['document_date' => SORT_ASC]);

        $firstDate = $query->scalar();
        $date = $firstDate ? date_create($firstDate) : date_create();

        return $date->modify('first day of this month');
    }

    /**
     * @return array
     */
    public function getDateItems()
    {
        if ($this->_dateItems === null) {
            $this->_dateItems = [];
            $date = $this->getStartDate();
            $currentDate = date_create();

            do {
                $this->_dateItems[$date->format('Y-m')] = Yii::$app->formatter->asDate($date, 'LLLL yyyy');
                $date->modify('+1 month');
            } while ($date <= $currentDate);
        }

        return $this->_dateItems;
    }

    /**
     * @inheritdoc
     */
    public function setCompany(Company $company)
    {
        $this->_company = $company;
    }

    /**
     * @return array
     */
    public function getCompany()
    {
        if ($this->_company === null) {
            $this->_company = Yii::$app->user->identity->company;
        }

        return $this->_company;
    }

    /**
     * @inheritdoc
     */
    public function setDate($value)
    {
        $date = date_create($value . '-01') ? : date_create();
        $this->_date = $date->modify('first day of this month');
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        if ($this->_date === null) {
            $this->_date = date_create()->modify('first day of this month');
        }

        return clone $this->_date;
    }

    /**
     * @return \DateTime
     */
    public function getPrevDate()
    {
        $date = clone $this->getDate();

        return $date->modify('first day of -1 month');
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
        $date = clone $this->getDate();

        return $date->modify('first day of -11 month');
    }

    /**
     * @return \DateTime
     */
    public function getDateTill()
    {
        $date = $this->getDate();

        return $date->modify('last day of this month');
    }

    /**
     * @return array
     */
    public function getDatePeriod()
    {
        if ($this->_datePeriod === null) {
            $this->_datePeriod = [];
            $date = $this->getDateFrom();
            $m = $this->getDateTill()->format('Ym') * 1;
            do {
                $k = $date->format('Ym') * 1;
                $this->_datePeriod[$k] = clone $date;
                $date->modify('first day of +1 month');
            } while ($k < $m);
        }

        $a = [];
        foreach ($this->_datePeriod as $k => $v) {
            $a[$k] = clone $v;
        }

        return $a;
    }

    /**
     * @return array
     */
    public function getDateKeys()
    {
        return array_keys($this->getDatePeriod());
    }

    /**
     * @inheritdoc
     */
    public function setReportData($value, $key)
    {
        $this->_report_data = $value;
        $this->curr_sum = $value['sum'][$key];
        $this->curr_count = $value['count'][$key];
        $this->curr_paid = $value['paid'][$key];
        $this->curr_debt = $value['debt'][$key];
    }

    /**
     * @return array|integer
     */
    public function getReportData($param = null, $date = null)
    {
        if ($param === null) {
            return $this->_report_data;
        }
        if ($date === null) {
            return isset($this->_report_data[$param]) ? $this->_report_data[$param] : [];
        }

        return isset($this->_report_data[$param][$date]) ? $this->_report_data[$param][$date] : 0;
    }

    /**
     * @return \DateTime
     */
    public function getSumQuery(DateTime $dateFrom, DateTime $dateTill)
    {
        $query = new Query;
        $query
            ->select([
                'user' => 'invoice.document_author_id',
                'date' => 'DATE_FORMAT({{invoice}}.[[document_date]], "%Y%m")',
                'value' => 'SUM({{invoice}}.[[total_amount_with_nds]])',
            ])
            ->from(Invoice::tableName())
            ->andWhere(['invoice.company_id' => $this->company->id])
            ->andWhere(['invoice.is_deleted' => false])
            ->andWhere(['invoice.type' => Documents::IO_TYPE_OUT])
            ->andWhere(['invoice.invoice_status_id' => InvoiceStatus::$validInvoices])
            ->andWhere(['between', 'invoice.document_date', $dateFrom->format('Y-m-d'), $dateTill->format('Y-m-d')])
            ->groupBy(['user', 'date']);

        return $query;
    }

    /**
     * @return \DateTime
     */
    public function getCountQuery(DateTime $dateFrom, DateTime $dateTill)
    {
        $query = new Query;
        $query
            ->select([
                'user' => 'invoice.document_author_id',
                'date' => 'DATE_FORMAT({{invoice}}.[[document_date]], "%Y%m")',
                'value' => 'COUNT({{invoice}}.[[id]])',
            ])
            ->from(['invoice' => Invoice::tableName()])
            ->andWhere(['invoice.company_id' => $this->company->id])
            ->andWhere(['invoice.is_deleted' => false])
            ->andWhere(['invoice.type' => Documents::IO_TYPE_OUT])
            ->andWhere(['invoice.invoice_status_id' => InvoiceStatus::$validInvoices])
            ->andWhere(['between', 'invoice.document_date', $dateFrom->format('Y-m-d'), $dateTill->format('Y-m-d')])
            ->groupBy(['user', 'date']);

        return $query;
    }

    /**
     * @return \DateTime
     */
    public function getPaidQuery(DateTime $dateFrom, DateTime $dateTill)
    {
        $paySelect = ['flow.date', 'link.amount', 'link.invoice_id'];
        $payWhere = [
            'and',
            ['flow.company_id' => $this->company->id],
            ['flow.flow_type' => CashFlowsBase::FLOW_TYPE_INCOME],
            ['between', 'flow.date', $dateFrom->format('Y-m-d'), $dateTill->format('Y-m-d')],
        ];
        $payQuery = new Query;
        $payQuery->select([
            'invoice_id',
            'flow_date' => 'date',
            'flow_sum' => 'SUM([[amount]])',
        ])->from(['t' => CashFlowsBase::getAllFlows($paySelect, $payWhere, true, 'innerJoin')])->groupBy('t.invoice_id');

        $query = new Query;
        $query
            ->select([
                'user' => 'invoice.document_author_id',
                'date' => 'DATE_FORMAT({{pay}}.[[flow_date]], "%Y%m")',
                'value' => 'SUM({{pay}}.[[flow_sum]])',
            ])
            ->from(['pay' => $payQuery])
            ->innerJoin(['invoice' => Invoice::tableName()], '{{invoice}}.[[id]] = {{pay}}.[[invoice_id]]')
            ->andWhere(['invoice.company_id' => $this->company->id])
            ->andWhere(['invoice.is_deleted' => false])
            ->andWhere(['invoice.type' => Documents::IO_TYPE_OUT])
            ->andWhere(['invoice.invoice_status_id' => InvoiceStatus::$validInvoices])
            ->groupBy(['user', 'date']);

        return $query;
    }

    /**
     * @return \DateTime
     */
    public function getDebtQuery(DateTime $dateTill)
    {
        $paySelect = ['link.amount', 'link.invoice_id'];
        $payWhere = [
            'and',
            ['flow.company_id' => $this->company->id],
            ['flow.flow_type' => CashFlowsBase::FLOW_TYPE_INCOME],
            ['<', 'flow.date', $dateTill->format('Y-m-d')],
        ];
        $payQuery = new Query;
        $payQuery->select([
            'invoice_id',
            'flow_sum' => 'SUM([[amount]])',
        ])->from(['t' => CashFlowsBase::getAllFlows($paySelect, $payWhere, true, 'innerJoin')])->groupBy('t.invoice_id');

        $query = new Query;
        $query
            ->select([
                'value' => '(SUM({{invoice}}.[[total_amount_with_nds]]) - IFNULL(SUM({{pay}}.[[flow_sum]]), 0))',
                'user' => 'invoice.document_author_id',
            ])
            ->from(['invoice' => Invoice::tableName()])
            ->leftJoin(['pay' => $payQuery], '{{invoice}}.[[id]] = {{pay}}.[[invoice_id]]')
            ->andWhere(['invoice.company_id' => $this->company->id])
            ->andWhere(['invoice.is_deleted' => false])
            ->andWhere(['invoice.type' => Documents::IO_TYPE_OUT])
            ->andWhere(['invoice.invoice_status_id' => InvoiceStatus::$validInvoices])
            ->andWhere(['<', 'document_date', $dateTill->format('Y-m-d')])
            ->groupBy('user')
            ->indexBy('user');

        return $query;
    }
}
