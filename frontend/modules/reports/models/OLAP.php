<?php

namespace frontend\modules\reports\models;

use yii\base\Model;

class OLAP extends Model {

    const TABLE_OLAP_DOCS = 'olap_documents';
    const TABLE_OLAP_FLOWS = 'olap_flows';
    const TABLE_OLAP_INVOICES = 'olap_invoices';
    const TABLE_PRODUCT_TURNOVER = 'product_turnover';

    const EMPTY_YEAR = ['01' => 0, '02' => 0, '03' => 0, '04' => 0, '05' => 0, '06' => 0, '07' => 0, '08' => 0, '09' => 0, '10' => 0, '11' => 0, '12' => 0];
    private static $_EMPTY_YEAR_IN_DAYS = [];
    
    /**
     * @param $year
     * @return string
     */
    public static function getSeq($year)
    {
        return 'seq_0_to_' . ($year == date('Y') ? (date('n') - 1) : 11);
    }

    /**
     * @param $year
     * @return string
     */
    public static function getInterval($year)
    {
        $startDate = $year . '-01-31';

        if ($year == date('Y')) {
            $lastDate = date('Y-m-d');
            $replacedDate = date('Y-m-t');

            return "IF (('{$startDate}' + INTERVAL (seq) MONTH) = '{$replacedDate}', '{$lastDate}', ('{$startDate}' + INTERVAL (seq) MONTH))";
        }

        return "('{$startDate}' + INTERVAL (seq) MONTH)";
    }

    /**
     * @param $year
     * @return string
     */
    public static function getIntervalLastInFuture($year)
    {
        $startDate = $year . '-01-31';

        if ($year == date('Y')) {
            $lastDate = date('2999-m-01');
            $replacedDate = date('Y-m-t');

            return "IF (('{$startDate}' + INTERVAL (seq) MONTH) = '{$replacedDate}', '{$lastDate}', ('{$startDate}' + INTERVAL (seq) MONTH))";
        }

        return "('{$startDate}' + INTERVAL (seq) MONTH)";
    }

    public static function getEmptyYearInDays($year)
    {
        if (empty(self::$_EMPTY_YEAR_IN_DAYS)) {
            for ($m=1; $m<=12; $m++) {
                $month = str_pad($m, 2, "0", STR_PAD_LEFT);
                $daysCount = (new \DateTime("{$year}{$month}01"))->format('t');
                for ($d=1; $d<=$daysCount; $d++) {
                    $day = str_pad($d, 2, "0", STR_PAD_LEFT);
                    self::$_EMPTY_YEAR_IN_DAYS[$month.$day] = 0;
                }
            }
        }

        return self::$_EMPTY_YEAR_IN_DAYS;
    }
}