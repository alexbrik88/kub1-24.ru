<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.11.2017
 * Time: 9:43
 */

namespace frontend\modules\reports\models;

use common\components\date\DateHelper;
use common\components\helpers\Month;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\status\InvoiceStatus;
use common\models\product\Product;
use frontend\models\Documents;
use Yii;
use yii\base\Model;
use yii\web\JsExpression;

/**
 * Class SellingReportSearch
 * @package frontend\modules\reports\models
 *
 * @property array $periodParams
 * @property string $periodName
 */
class SellingReportSearch extends Model
{
    /**
     * period ID constants
     */
    const PERIOD_LAST_QUARTER = 1;
    /**
     *
     */
    const PERIOD_PREV_QUARTER = 2;
    /**
     *
     */
    const PERIOD_LAST_YEAR_M = 3;
    /**
     *
     */
    const PERIOD_LAST_YEAR_Q = 4;
    /**
     *
     */
    const PERIOD_LAST_4_QUARTERS = 5;
    /**
     *
     */
    const PERIOD_PREV_YEAR_M = 6;
    /**
     *
     */
    const PERIOD_PREV_YEAR_Q = 7;
    /**
     *
     */
    const PERIOD_LAST_4_MONTH = 8;

    /**
     * period step constants
     */
    const PERIOD_STEP_MONTH = 'MONTH';
    /**
     *
     */
    const PERIOD_STEP_QUARTER = 'QUARTER';
    /**
     *
     */
    const TYPE_PAID = 1;
    /**
     *
     */
    const TYPE_NOT_PAID = 2;

    /**
     * @var
     */
    protected $_periodParams;
    /**
     * @var
     */
    protected $_dataArray;


    /**
     * @var
     */
    public $companyId;
    /**
     * @var
     */
    public $period;
    /**
     * @var int
     */
    public $type = self::TYPE_PAID;

    /**
     * @var array
     */
    public static $periodArray = [
        self::PERIOD_LAST_4_MONTH => 'Последние 4 месяца',
        self::PERIOD_LAST_QUARTER => 'Этот квартал',
        self::PERIOD_PREV_QUARTER => 'Предыдущий квартал',
        // self::PERIOD_LAST_YEAR_M => 'Месяца текущего года',
        self::PERIOD_LAST_YEAR_Q => 'Кварталы текущего года',
        self::PERIOD_LAST_4_QUARTERS => 'Последние 4 квартала',
        // self::PERIOD_PREV_YEAR_M => 'Месяца прошлого года',
        self::PERIOD_PREV_YEAR_Q => 'Кварталы прошлого года',
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['period'], 'filter', 'filter' => function ($value) {
                return (int)$value;
            }],
            [
                ['period'], 'in', 'range' => array_keys(self::$periodArray),
                'message' => 'Не верно выбран период.'
            ],
            [['type'], 'safe'],
        ];
    }

    /**
     * @param $params
     */
    public function buildDataArray($params)
    {
        $orderTable = Order::tableName();
        $invoiceTable = Invoice::tableName();
        $productTable = Product::tableName();
        $contractorTable = Contractor::tableName();

        $this->load($params, '');
        if (!$this->validate()) {
            $this->period = self::PERIOD_LAST_4_MONTH;
        }

        $this->setPeriodParams($this->period);
        $periodFrom = $this->periodParams['range']['from'];
        $periodTill = $this->periodParams['range']['till'];
        $step = $this->periodParams['step'];

        $query = Order::find()
            ->leftJoin($invoiceTable, "{{{$invoiceTable}}}.[[id]] = {{{$orderTable}}}.[[invoice_id]]")
            ->leftJoin($productTable, "{{{$productTable}}}.[[id]] = {{{$orderTable}}}.[[product_id]]")
            ->leftJoin($contractorTable, "{{{$contractorTable}}}.[[id]] = {{{$invoiceTable}}}.[[contractor_id]]")
            ->select([
                "$productTable.id as product_id",
                "$productTable.title as product_title",
                "$orderTable.invoice_id",
                "$orderTable.amount_sales_with_vat as amount",
                "$orderTable.quantity as count",
                "CONCAT(YEAR({{{$invoiceTable}}}.[[document_date]]), {$step}({{{$invoiceTable}}}.[[document_date]])) [[period_step]]"
            ])
            ->andWhere([Invoice::tableName() . '.company_id' => $this->companyId])
            ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
            ->andWhere([Invoice::tableName() . '.is_deleted' => 0])
            ->andWhere(['between', Invoice::tableName() . '.document_date', $periodFrom, $periodTill]);

        \frontend\modules\documents\models\InvoiceSearch::searchByRoleFilter($query);

        if ($this->type) {
            if ($this->type == self::TYPE_PAID) {
                $query->andWhere([Invoice::tableName() . '.invoice_status_id' => InvoiceStatus::STATUS_PAYED]);
            } else {
                $query->andWhere(['not', [Invoice::tableName() . '.invoice_status_id' => [
                    InvoiceStatus::STATUS_PAYED,
                    InvoiceStatus::STATUS_REJECTED,
                    InvoiceStatus::STATUS_AUTOINVOICE
                ]]]);
            }
        } else {
            $query->andWhere(['not', [Invoice::tableName() . '.invoice_status_id' => [
                InvoiceStatus::STATUS_REJECTED,
                InvoiceStatus::STATUS_AUTOINVOICE
            ]]]);
        }
        $orderData = $query
            ->orderBy([$productTable . '.title' => SORT_ASC])
            ->groupBy(Order::tableName() . '.id')
            ->asArray()
            ->all();

        foreach ($orderData as $oneOrderData) {
            if (!isset($this->_dataArray[$oneOrderData['period_step']]['totalAmount'])) {
                $this->_dataArray[$oneOrderData['period_step']]['totalAmount'] = 0;
                $this->_dataArray[$oneOrderData['period_step']]['totalCount'] = 0;
            }
            if (isset($this->_dataArray[$oneOrderData['period_step']][$oneOrderData['product_id']])) {
                $this->_dataArray[$oneOrderData['period_step']][$oneOrderData['product_id']]['amount'] += $oneOrderData['amount'];
                $this->_dataArray[$oneOrderData['period_step']][$oneOrderData['product_id']]['count'] += (int)$oneOrderData['count'];
                $this->_dataArray[$oneOrderData['period_step']]['totalAmount'] += $oneOrderData['amount'];
                $this->_dataArray[$oneOrderData['period_step']]['totalCount'] += (int)$oneOrderData['count'];
            } else {
                $this->_dataArray[$oneOrderData['period_step']]['totalAmount'] += $oneOrderData['amount'];
                $this->_dataArray[$oneOrderData['period_step']]['totalCount'] += (int)$oneOrderData['count'];
                $this->_dataArray[$oneOrderData['period_step']][$oneOrderData['product_id']] = [
                    'count' => (int)$oneOrderData['count'],
                    'product_title' => $oneOrderData['product_title'],
                    'amount' => $oneOrderData['amount'],
                ];
            }
        }
    }

    /**
     * @return array
     */
    public function getHighchartsOptions()
    {
        $categories = [];
        $otherData = [];
        $totalSum = [];
        $series = [
            'Прочее' => [
                'name' => 'Прочее',
                'data' => [
                    0, 0, 0, 0,
                ],
            ]
        ];
        $subPeriodColumns = $this->getSubPeriodColumns();
        foreach ($subPeriodColumns as $key => $column) {
            $otherData[$column['attribute']] = 0;
            $categories[] = [$column['label']];
        }

        if ($this->_dataArray) {
            foreach ($this->_dataArray as $key => $data) {
                foreach ($data as $productID => $attributes) {
                    if ($productID == 'totalAmount' || $productID == 'totalCount') {
                        continue;
                    }
                    $series[$attributes['product_title']]['name'] = $attributes['product_title'];
                    foreach ($subPeriodColumns as $periodKey => $column) {
                        if (!isset($totalSum[$periodKey]) && $column['attribute'] == $key) {
                            $totalSum[$periodKey] = $this->_dataArray[$key]['totalAmount'];
                        }
                        if ($column['attribute'] == $key) {
                            if (!isset($series[$attributes['product_title']]['data'][$periodKey])) {
                                $series[$attributes['product_title']]['data'][$periodKey] = (int)($attributes['amount'] / 100);
                            } else {
                                $series[$attributes['product_title']]['data'][$periodKey] += (int)($attributes['amount'] / 100);
                            }
                        } else {
                            if (!isset($series[$attributes['product_title']]['data'][$periodKey])) {
                                $series[$attributes['product_title']]['data'][$periodKey] = null;
                            }
                        }
                    }
                }
            }
        }
        foreach ($series as $productTitle => $oneData) {
            if ($productTitle == 'Прочее') {
                continue;
            }
            foreach ($oneData['data'] as $key => $periodAmount) {
                if (!empty($periodAmount)) {
                    if (isset($totalSum[$key])) {
                        if (round($periodAmount / ($totalSum[$key] / 100), 4) * 100 < 5) {
                            $series['Прочее']['data'][$key] += $series[$productTitle]['data'][$key];
                            $series[$productTitle]['data'][$key] = null;
                        }
                    }
                }
                if (empty($series[$productTitle]['data'][0]) && empty($series[$productTitle]['data'][1]) &&
                    empty($series[$productTitle]['data'][2]) && empty($series[$productTitle]['data'][3])
                ) {
                    unset($series[$productTitle]);
                }
            }
        }

        return [
            'chart' => [
                'type' => 'column',
            ],
            'title' => [
                'text' => '',
            ],
            'lang' => [
                'printChart' => 'На печать',
                'downloadPNG' => 'Скачать PNG',
                'downloadJPEG' => 'Скачать JPEG',
                'downloadPDF' => 'Скачать PDF',
                'downloadSVG' => 'Скачать SVG',
                'contextButtonTitle' => 'Меню',
            ],
            'xAxis' => [
                'categories' => $categories,
            ],
            'yAxis' => [
                'min' => 0,
                'title' => ['text' => 'Сумма'],
                'stackLabels' => [
                    'allowOverlap' => true,
                    'enabled' => true,
                    'style' => [
                        'fontWeight' => '500',
                        'color' => '#333333',
                        'fontSize' => '20px;'
                    ],
                    'formatter' => new JsExpression('function() {
                        return Highcharts.numberFormat(this.total, 2, ",", " ");
                    }'),
                ],
            ],
            'series' => array_values($series),
            'plotOptions' => [
                'column' => [
                    'stacking' => 'normal',
                    'borderWidth' => 0,
                    'dataLabels' => [
                        'enabled' => true,
                        'formatter' => new JsExpression('function() {
                            return this.percentage > 0 ? Highcharts.numberFormat(this.percentage, 2, ",", " ") + " %" : false;
                        }'),
                        'style' => [
                            'color' => '#333333',
                            'textOutline' => null
                        ],
                    ],
                    'tooltip' => [
                        'valueDecimals' => 2,
                    ],
                ],
            ],
            'legend' => [
                'labelFormatter' => new JsExpression("function() {
                    var name = this.name;
                    var truncateName = name;
                    if (name.length > 30) {
                        truncateName = name.substr(0, 30) + '...';
                    }
                    return '<span title=\"' + name + '\">' + truncateName + '</span>';
                }"),
                'useHTML' => true,
            ],
            'tooltip' => [
                'formatter' => new JsExpression('
                    function() {
                        if (this.y > 0) {
                            var content = "";
                            content += "<span style=\"font-size: 10px\">" + this.key + "</span><br/>";
                            content += "<span style=\"color:" + this.point.color + "\">●</span> ";
                            content += this.series.name + ": <b>" + this.y + "</b><br/>";
                            return content;
                        }
                        return null;
                    }
                '),
            ]
        ];
    }

    /**
     * @return array
     */
    public function getTableData()
    {
        $result = [];
        if ($this->_dataArray) {
            foreach ($this->_dataArray as $date => $data) {
                $totalAmount = $data['totalAmount'];
                $totalCount = $data['totalCount'];
                $result['totalDateData'][$date] = [
                    'totalAmount' => $totalAmount,
                    'totalCount' => $totalCount,
                ];
                if (isset($result['totalDateData']['totalAmount'])) {
                    $result['totalDateData']['totalAmount'] += $totalAmount;
                    $result['totalDateData']['totalCount'] += $totalCount;
                } else {
                    $result['totalDateData']['totalAmount'] = $totalAmount;
                    $result['totalDateData']['totalCount'] = $totalCount;
                }

                unset($data['totalAmount']);
                unset($data['totalCount']);

                foreach ($data as $productID => $oneData) {
                    if (isset($result[$oneData['product_title']])) {
                        $result[$oneData['product_title']]['totalCount'] += $oneData['count'];
                        $result[$oneData['product_title']]['totalAmount'] += $oneData['amount'];
                    } else {
                        $result[$oneData['product_title']]['totalAmount'] = $oneData['amount'];
                        $result[$oneData['product_title']]['totalCount'] = $oneData['count'];
                    }
                    $result[$oneData['product_title']]['product_id'] = $productID;
                    $result[$oneData['product_title']]['product_title'] = $oneData['product_title'];
                    if (isset($result[$oneData['product_title']]['data'][$date])) {
                        $result[$oneData['product_title']]['data'][$date]['count'] += $oneData['count'];
                        $result[$oneData['product_title']]['data'][$date]['amount'] += $oneData['amount'];
                    } else {
                        $result[$oneData['product_title']]['data'][$date]['count'] = $oneData['count'];
                        $result[$oneData['product_title']]['data'][$date]['amount'] = $oneData['amount'];
                    }
                    $result[$oneData['product_title']]['data'][$date]['percent'] = round($result[$oneData['product_title']]['data'][$date]['amount'] / ($totalAmount ? $totalAmount : 1) * 100, 2);
                }
            }
            ksort($result);
        }
        $result = $this->compareTableData($result);

        return $result;
    }

    public function compareTableData($data)
    {
        $invoiceTable = Invoice::tableName();
        $contractorTable = Contractor::tableName();
        $orderTable = Order::tableName();
        $periodFrom = $this->periodParams['range']['from'];
        $periodTill = $this->periodParams['range']['till'];
        $previousPeriodFrom = $periodFrom;
        $previousPeriodTo = $periodTill;

        $step = $this->periodParams['step'];
        if ($step == self::PERIOD_STEP_MONTH) {
            $newPeriodFrom = date(DateHelper::FORMAT_DATE, strtotime('-1 month ' . $periodFrom));
        } else {
            $newPeriodFrom = date(DateHelper::FORMAT_DATE, strtotime('-3 month ' . $periodFrom));
        }
        switch ($this->period) {
            case self::PERIOD_LAST_4_MONTH:
                $previousPeriodFrom = date(DateHelper::FORMAT_DATE, strtotime('-8 month ' . $periodFrom));
                $previousPeriodTo = date(DateHelper::FORMAT_DATE, strtotime('-4 month ' . $periodFrom));
                break;
            case self::PERIOD_LAST_QUARTER:
                $previousPeriodFrom = date(DateHelper::FORMAT_DATE, strtotime('-6 month ' . $periodFrom));
                $previousPeriodTo = date(DateHelper::FORMAT_DATE, strtotime('-3 month ' . $periodFrom));
                break;
            case self::PERIOD_PREV_QUARTER:
                $previousPeriodFrom = date(DateHelper::FORMAT_DATE, strtotime('-6 month ' . $periodFrom));
                $previousPeriodTo = date(DateHelper::FORMAT_DATE, strtotime('-3 month ' . $periodFrom));
                break;
            case self::PERIOD_LAST_YEAR_Q:
                $previousPeriodFrom = date(DateHelper::FORMAT_DATE, strtotime('-24 month ' . $periodFrom));
                $previousPeriodTo = date(DateHelper::FORMAT_DATE, strtotime('-12 month ' . $periodFrom));
                break;
            case self::PERIOD_LAST_4_QUARTERS:
                $previousPeriodFrom = date(DateHelper::FORMAT_DATE, strtotime('-24 month ' . $periodFrom));
                $previousPeriodTo = date(DateHelper::FORMAT_DATE, strtotime('-12 month ' . $periodFrom));
                break;
            case self::PERIOD_PREV_YEAR_Q:
                $previousPeriodFrom = date(DateHelper::FORMAT_DATE, strtotime('-24 month ' . $periodFrom));
                $previousPeriodTo = date(DateHelper::FORMAT_DATE, strtotime('-12 month ' . $periodFrom));
                break;
        }

        foreach ($data as $productID => $productData) {
            if ($productID == 'totalDateData') {
                continue;
            }
            if (isset($productData['data'])) {
                foreach ($this->getSubPeriodColumns() as $subPeriodColumn) {
                    if (!isset($productData['data'][$subPeriodColumn['attribute']])) {
                        $data[$productID]['data'][$subPeriodColumn['attribute']] = [
                            'count' => 0,
                            'amount' => 0,
                            'percent' => '0,00',
                        ];
                    }
                }
                $productData = $data[$productID]['data'];
                ksort($productData);
                $productData = array_values($productData);
                krsort($productData);
                foreach ($productData as $key => $productOnePeriodData) {
                    if (isset($productData[$key - 1])) {
                        $a = $productOnePeriodData['amount'] - $productData[$key - 1]['amount'];
                        if ($a > 0) {
                            $productData[$key]['arrow_status'] = true;
                        } elseif ($a < 0) {
                            $productData[$key]['arrow_status'] = false;
                        } else {
                            $productData[$key]['arrow_status'] = null;
                        }
                    }
                }
                $oldKeys = array_keys($data[$productID]['data']);
                asort($oldKeys);
                $oldKeys = array_values($oldKeys);
                foreach ($oldKeys as $key => $oldKey) {
                    $productData[$oldKey] = $productData[$key];
                    unset($productData[$key]);
                }

                //var_dump($oldKeys, $productData, $data); exit;

                $previousAmountPeriod = Order::find()
                    ->leftJoin($invoiceTable, "{{{$invoiceTable}}}.[[id]] = {{{$orderTable}}}.[[invoice_id]]")
                    ->leftJoin($contractorTable, "{{{$contractorTable}}}.[[id]] = {{{$invoiceTable}}}.[[contractor_id]]")
                    ->andWhere([Invoice::tableName() . '.company_id' => $this->companyId])
                    ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
                    ->andWhere([Invoice::tableName() . '.is_deleted' => 0])
                    ->andWhere([Order::tableName() . '.product_id' => $productID])
                    ->andWhere(['between', Invoice::tableName() . '.document_date', $newPeriodFrom, $periodFrom]);

                \frontend\modules\documents\models\InvoiceSearch::searchByRoleFilter($previousAmountPeriod);

                if ($this->type) {
                    if ($this->type == self::TYPE_PAID) {
                        $previousAmountPeriod->andWhere([Invoice::tableName() . '.invoice_status_id' => InvoiceStatus::STATUS_PAYED]);
                    } else {
                        $previousAmountPeriod->andWhere(['not', [Invoice::tableName() . '.invoice_status_id' => [
                            InvoiceStatus::STATUS_PAYED,
                            InvoiceStatus::STATUS_REJECTED,
                            InvoiceStatus::STATUS_AUTOINVOICE
                        ]]]);
                    }
                } else {
                    $previousAmountPeriod->andWhere(['not', [Invoice::tableName() . '.invoice_status_id' => [
                        InvoiceStatus::STATUS_REJECTED,
                        InvoiceStatus::STATUS_AUTOINVOICE
                    ]]]);
                }
                $firstKey = current(array_keys($productData));
                $a = $productData[$firstKey]['amount'] - $previousAmountPeriod->sum('amount_sales_with_vat');
                if ($a > 0) {
                    $productData[$firstKey]['arrow_status'] = true;
                } elseif ($a < 0) {
                    $productData[$firstKey]['arrow_status'] = false;
                } else {
                    $productData[$firstKey]['arrow_status'] = null;
                }

                $previousPeriodTotalAmount = Order::find()
                    ->leftJoin($invoiceTable, "{{{$invoiceTable}}}.[[id]] = {{{$orderTable}}}.[[invoice_id]]")
                    ->leftJoin($contractorTable, "{{{$contractorTable}}}.[[id]] = {{{$invoiceTable}}}.[[contractor_id]]")
                    ->andWhere([Invoice::tableName() . '.company_id' => $this->companyId])
                    ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
                    ->andWhere([Invoice::tableName() . '.is_deleted' => 0])
                    ->andWhere([Order::tableName() . '.product_id' => $productID])
                    ->andWhere(['between', Invoice::tableName() . '.document_date', $previousPeriodFrom, $previousPeriodTo]);

                \frontend\modules\documents\models\InvoiceSearch::searchByRoleFilter($previousPeriodTotalAmount);

                if ($this->type) {
                    if ($this->type == self::TYPE_PAID) {
                        $previousPeriodTotalAmount->andWhere([Invoice::tableName() . '.invoice_status_id' => InvoiceStatus::STATUS_PAYED]);
                    } else {
                        $previousPeriodTotalAmount->andWhere(['not', [Invoice::tableName() . '.invoice_status_id' => [
                            InvoiceStatus::STATUS_PAYED,
                            InvoiceStatus::STATUS_REJECTED,
                            InvoiceStatus::STATUS_AUTOINVOICE
                        ]]]);
                    }
                } else {
                    $previousPeriodTotalAmount->andWhere(['not', [Invoice::tableName() . '.invoice_status_id' => [
                        InvoiceStatus::STATUS_REJECTED,
                        InvoiceStatus::STATUS_AUTOINVOICE
                    ]]]);
                }
                $a = $data[$productID]['totalAmount'] - $previousPeriodTotalAmount->sum('amount_sales_with_vat');
                if ($a > 0) {
                    $data[$productID]['arrow_status'] = true;
                } elseif ($a < 0) {
                    $data[$productID]['arrow_status'] = false;
                } else {
                    $data[$productID]['arrow_status'] = null;
                }
                $data[$productID]['data'] = $productData;
            }
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getTableColumns()
    {
        $expensesColumn = [
            [
                'label' => 'Товары&nbsp;/&nbsp;Услуги',
                'attribute' => 'expenses',
                'type' => 'name',
            ],
        ];
        $summaryColumn = [
            [
                'label' => $this->periodParams['title'],
                'attribute' => 'summary',
                'type' => 'amount',
            ],
        ];
        $columnArray = array_merge($expensesColumn, $this->subPeriodColumns, $summaryColumn);
        $compareArrtibute = 'summaryPriorStep';
        foreach ($columnArray as $key => $column) {
            if ($column['type'] == 'amount') {
                if ($column['attribute'] == 'summary') {
                    $columnArray[$key]['compareArrtibute'] = 'summaryPrior';
                } else {
                    $columnArray[$key]['compareArrtibute'] = $compareArrtibute;
                    $compareArrtibute = $column['attribute'];
                }
            }
        }

        return $columnArray;
    }

    /**
     * @return mixed
     */
    public function getPeriodName()
    {
        return self::$periodArray[$this->period];
    }

    /**
     * @param $periodId
     */
    public function setPeriodParams($periodId)
    {
        switch ($periodId) {
            case self::PERIOD_PREV_QUARTER:
                $date = new \DateTime('first day of -3 month');
                $month = $date->format('n');
                $querter = ceil($month / 3);
                $year = $date->format('Y');
                $title = $querter . '-й квартал, ' . $year . ' г.';

                $monthNumber = $month - ($querter - 1) * 3;
                $offset = 1 - $monthNumber;
                $date->modify("$offset month");
                $datePrior = clone $date;
                $range = [
                    'from' => $date->modify('first day of this month')->format('Y-m-d'),
                    'till' => $date->modify('last day of + 2 month')->format('Y-m-d'),
                ];
                $rangePrior = [
                    'from' => $datePrior->modify('first day of - 3 month')->format('Y-m-d'),
                    'till' => $datePrior->modify('last day of + 2 month')->format('Y-m-d'),
                ];
                $step = self::PERIOD_STEP_MONTH;
                break;

            case self::PERIOD_LAST_YEAR_M:
                $date = new \DateTime();
                $year = $date->format('Y');
                $title = $year . ' г.';
                $yearPrior = $year - 1;
                $range = [
                    'from' => $date->modify("first day of Jan $year")->format('Y-m-d'),
                    'till' => $date->modify("last day of Dec $year")->format('Y-m-d'),
                ];
                $rangePrior = [
                    'from' => $date->modify("first day of Jan $yearPrior")->format('Y-m-d'),
                    'till' => $date->modify("last day of Dec $yearPrior")->format('Y-m-d'),
                ];
                $step = self::PERIOD_STEP_MONTH;
                break;

            case self::PERIOD_LAST_YEAR_Q:
                $date = new \DateTime();
                $year = $date->format('Y');
                $title = $year . ' г.';
                $yearPrior = $year - 1;
                $range = [
                    'from' => $date->modify("first day of Jan $year")->format('Y-m-d'),
                    'till' => $date->modify("last day of Dec $year")->format('Y-m-d'),
                ];
                $rangePrior = [
                    'from' => $date->modify("first day of Jan $yearPrior")->format('Y-m-d'),
                    'till' => $date->modify("last day of Dec $yearPrior")->format('Y-m-d'),
                ];
                $step = self::PERIOD_STEP_QUARTER;
                break;

            case self::PERIOD_LAST_4_QUARTERS:
                $offset = (date('n') % 3) + 8;
                $date = new \DateTime("- $offset month");
                $title = 'Последние 4 квартала';
                $datePrior = clone $date;
                $range = [
                    'from' => $date->modify("first day of this month")->format('Y-m-d'),
                    'till' => $date->modify("last day of +11 month")->format('Y-m-d'),
                ];
                $rangePrior = [
                    'from' => $datePrior->modify("first day of -12 month")->format('Y-m-d'),
                    'till' => $datePrior->modify("last day of +11 month")->format('Y-m-d'),
                ];
                $step = self::PERIOD_STEP_QUARTER;
                break;

            case self::PERIOD_PREV_YEAR_M:
                $date = new \DateTime('-1 year');
                $year = $date->format('Y');
                $title = $year . ' г.';
                $yearPrior = $year - 1;
                $range = [
                    'from' => $date->modify("first day of Jan $year")->format('Y-m-d'),
                    'till' => $date->modify("last day of Dec $year")->format('Y-m-d'),
                ];
                $rangePrior = [
                    'from' => $date->modify("first day of Jan $yearPrior")->format('Y-m-d'),
                    'till' => $date->modify("last day of Dec $yearPrior")->format('Y-m-d'),
                ];
                $step = self::PERIOD_STEP_MONTH;
                break;

            case self::PERIOD_PREV_YEAR_Q:
                $date = new \DateTime('-1 year');
                $year = $date->format('Y');
                $title = $year . ' г.';
                $yearPrior = $year - 1;
                $range = [
                    'from' => $date->modify("first day of Jan $year")->format('Y-m-d'),
                    'till' => $date->modify("last day of Dec $year")->format('Y-m-d'),
                ];
                $rangePrior = [
                    'from' => $date->modify("first day of Jan $yearPrior")->format('Y-m-d'),
                    'till' => $date->modify("last day of Dec $yearPrior")->format('Y-m-d'),
                ];
                $step = self::PERIOD_STEP_QUARTER;
                break;

            case self::PERIOD_LAST_4_MONTH:
                $date = new \DateTime('-3 month');
                $title = 'Последние 4 месяца';
                $datePrior = clone $date;
                $range = [
                    'from' => $date->modify("first day of this month")->format('Y-m-d'),
                    'till' => $date->modify("last day of + 3 month")->format('Y-m-d'),
                ];
                $rangePrior = [
                    'from' => $datePrior->modify("first day of - 4 month")->format('Y-m-d'),
                    'till' => $datePrior->modify("last day of + 3 month")->format('Y-m-d'),
                ];
                $step = self::PERIOD_STEP_MONTH;
                break;

            case self::PERIOD_LAST_QUARTER:
            default:
                $date = new \DateTime();
                $month = $date->format('n');
                $querter = ceil($month / 3);
                $year = $date->format('Y');
                $title = $querter . '-й квартал, ' . $year . ' г.';

                $monthNumber = $month - ($querter - 1) * 3;
                $offset = 1 - $monthNumber;
                $date->modify("$offset month");
                $datePrior = clone $date;
                $range = [
                    'from' => $date->modify('first day of this month')->format('Y-m-d'),
                    'till' => $date->modify('last day of + 2 month')->format('Y-m-d'),
                ];
                $rangePrior = [
                    'from' => $datePrior->modify('first day of - 3 month')->format('Y-m-d'),
                    'till' => $datePrior->modify('last day of + 2 month')->format('Y-m-d'),
                ];
                $step = self::PERIOD_STEP_MONTH;
                break;
        }

        $this->_periodParams = [
            'id' => $periodId,
            'title' => $title,
            'range' => $range,
            'rangePrior' => $rangePrior,
            'step' => $step,
        ];
    }

    /**
     * @return mixed
     */
    public function getPeriodParams()
    {
        return $this->_periodParams;
    }

    /**
     * @return array
     */
    public function getSubPeriodColumns()
    {
        $from = $this->periodParams['range']['from'];
        $till = $this->periodParams['range']['till'];
        $step = $this->periodParams['step'];

        return $this->getPeriodColumnsByStep($from, $till, $step);
    }

    /**
     * @return mixed
     */
    public function getPeriodPriorLastColumn()
    {
        $from = $this->periodParams['rangePrior']['from'];
        $till = $this->periodParams['rangePrior']['till'];
        $step = $this->periodParams['step'];

        $columns = $this->getPeriodColumnsByStep($from, $till, $step);

        return end($columns);
    }

    /**
     * @param $from
     * @param $till
     * @param $step
     * @return array
     */
    public function getPeriodColumnsByStep($from, $till, $step)
    {
        $columns = [];
        $date = new \DateTime($from);
        $dateTill = new \DateTime($till);
        $modify = $step == self::PERIOD_STEP_MONTH ? '+1 month' : '+3 month';

        for ($date; $date < $dateTill; $date->modify($modify)) {
            $monthNumber = $date->format('n');
            $columns[] = [
                'label' => (
                    $step == self::PERIOD_STEP_MONTH ?
                        Month::$monthFullRU[$date->format('n')] :
                        ceil($monthNumber / 3) . '-й квартал'
                    ) . ', ' . $date->format('Y') . ' г.',

                'attribute' => $date->format('Y') . (
                    $step == self::PERIOD_STEP_MONTH ? $monthNumber : ceil($monthNumber / 3)
                    ),
                'type' => 'amount',
            ];
        }

        return $columns;
    }

    /**
     * @return array
     */
    public function getPeriodItems()
    {
        $periodItems = [];
        foreach (self::$periodArray as $id => $name) {
            $periodItems[] = [
                'label' => $name,
                'url' => ['/reports/selling-report/index', 'period' => $id],
                'options' => ($this->period == $id) ? ['class' => 'active'] : [],
                'linkOptions' => ['class' => 'expenses-pjax-link']
            ];
        }

        return $periodItems;
    }
}