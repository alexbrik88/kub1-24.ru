<?php

namespace frontend\modules\reports\models;

use common\components\date\DatePickerFormatBehavior;
use common\components\TextHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\document\Invoice;
use common\models\document\InvoiceIncomeItem;
use common\models\document\status\InvoiceStatus;
use frontend\models\Documents;
use Yii;
use common\models\Company;
use common\models\Contractor;

/**
 * This is the model class for table "plan_cash_contractor".
 *
 * @property int $id
 * @property int $company_id
 * @property int $contractor_id
 * @property int $rule_id
 * @property int $plan_type 0 - по счетам, 1 - по предыдущим периодам
 * @property int $flow_type 0 - Расход, 1 - Приход
 * @property int $payment_type 1 - Банк, 2 - Касса, 3 - E-money
 * @property int $item_id Статья
 * @property int $payment_delay
 * @property string $amount
 * @property string $plan_date
 * @property string $end_plan_date
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Company $company
 * @property Contractor $contractor
 */
class PlanCashContractor extends \yii\db\ActiveRecord
{
    const PLAN_BY_FUTURE_INVOICES = 0;
    const PLAN_BY_PREVIOUS_FLOWS = 1;

    const PAYMENT_TYPE_BANK = 1;
    const PAYMENT_TYPE_ORDER = 2;
    const PAYMENT_TYPE_EMONEY = 3;

    public static $PAYMENT_TO_NAME = [
        self::PAYMENT_TYPE_BANK => 'Банк',
        self::PAYMENT_TYPE_ORDER => 'Касса',
        self::PAYMENT_TYPE_EMONEY => 'E-money',
    ];

    public static $PAYMENT_TO_CLASS = [
        self::PAYMENT_TYPE_BANK => CashBankFlows::class,
        self::PAYMENT_TYPE_ORDER => CashOrderFlows::class,
        self::PAYMENT_TYPE_EMONEY => CashEmoneyFlows::class,
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plan_cash_contractor';
    }

    public static function normalizePaymentType($paymentType)
    {
        if ($paymentType == 'cash_bank_flows') return self::PAYMENT_TYPE_BANK;
        elseif ($paymentType == 'cash_order_flows') return self::PAYMENT_TYPE_ORDER;
        elseif ($paymentType == 'cash_emoney_flows') return self::PAYMENT_TYPE_EMONEY;

        return (int)$paymentType;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => DatePickerFormatBehavior::className(),
                'attributes' => [
                    'plan_date' => [
                        'message' => 'Дата расхода указана неверно.',
                    ],
                    'end_plan_date' => [
                        'message' => 'Дата окончания планирования указана неверно.',
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'contractor_id', 'plan_type', 'flow_type', 'payment_type', 'item_id', 'created_at', 'updated_at'], 'required'],
            [['company_id', 'contractor_id', 'plan_type', 'flow_type', 'payment_type', 'payment_delay', 'created_at', 'updated_at'], 'integer'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['contractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::className(), 'targetAttribute' => ['contractor_id' => 'id']],
            [['rule_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => PlanCashRule::className(), 'targetAttribute' => ['rule_id' => 'id']],
            [['end_plan_date'], 'safe'],
            ['plan_date','validateDates'],
            [['amount'],
                'number',
                'numberPattern' => Yii::$app->params['numberPattern'],
                'max' => Yii::$app->params['maxCashSum'] * 100, // with kopeck
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
            [['payment_delay'], 'required', 'when' => function ($model) {
                return ($model->plan_type == self::PLAN_BY_FUTURE_INVOICES);
            },],
            [['item_id'], 'safe'],

        ];
    }

    public function validateDates(){
        if(strtotime($this->plan_date) > strtotime($this->end_plan_date)){
            $this->addError('end_date','Дата окончания планирования должна быть больше ' . date('d.m.Y', strtotime($this->plan_date)));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'contractor_id' => 'Contractor ID',
            'plan_type' => 'Тип плана',
            'flow_type' => 'Тип операции',
            'payment_type' => 'Тип платежа',
            'item_id' => 'Статья',
            'payment_delay' => 'Дней отсрочки',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'rule_id' => 'Rule Id'
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->amount = TextHelper::parseMoneyInput($this->amount);

        if ($this->isAttributeChanged('amount', false)) {
            $this->amount = bcmul($this->amount, 100, 0);
        }

        return parent::beforeValidate();
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * Gets query for [[Contractor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    public function createFlowsByActualInvoices() 
    {
        $actualInvoices = Invoice::find()
            ->where(['company_id' => $this->company_id])
            ->andWhere(['contractor_id' => $this->contractor_id])
            ->andWhere(['>=', 'payment_limit_date', date('Y-m-d')])
            ->andWhere(['not', ['invoice_status_id' => [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]]])
            ->all();

        foreach ($actualInvoices as $invoice) {

            if (PlanCashFlows::find()->where(['company_id' => $this->company_id, 'invoice_id' => $invoice->id])->exists()) {
                continue;
            }

            $planCashFlows = new PlanCashFlows();
            $planCashFlows->is_repeated = FALSE;
            $planCashFlows->company_id = $this->company_id;
            $planCashFlows->contractor_id = $this->contractor_id;
            $planCashFlows->flow_type = $this->flow_type;
            $planCashFlows->payment_type = $this->payment_type;
            $planCashFlows->amount = bcdiv($invoice->total_amount_with_nds, 100);
            if ($this->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE) {
                $planCashFlows->expenditure_item_id = $this->item_id;
            } else {
                $planCashFlows->income_item_id = $this->item_id;
            }
            $planCashFlows->description = 'Создано АвтоПланированием по Счету №' . $invoice->getFullNumber();
            $planCashFlows->plan_cash_contractor_id = $this->id;
            $planCashFlows->invoice_id = $invoice->id;
            $planCashFlows->date = $invoice->payment_limit_date;

            if (!$planCashFlows->save()) {

                //var_dump($planCashFlows->getErrors()); exit;
                return false;
            }
        }

        return true;
    }
    
    public function createRepeatedFlows()
    {
        if (empty($this->plan_date) || empty($this->end_plan_date))
            return false;
        
        $planCashFlows = new PlanCashFlows();
        $planCashFlows->is_repeated = TRUE;
        $planCashFlows->company_id = $this->company_id;
        $planCashFlows->contractor_id = $this->contractor_id;
        $planCashFlows->flow_type = $this->flow_type;
        $planCashFlows->payment_type = $this->payment_type;
        $planCashFlows->amount = bcdiv($this->amount, 100);
        if ($this->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE) {
            $planCashFlows->expenditure_item_id = $this->item_id;
        } else {
            $planCashFlows->income_item_id = InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER;
        }
        $planCashFlows->date = $this->plan_date;
        $planCashFlows->planEndDate = $this->end_plan_date;
        $planCashFlows->period = PlanCashFlows::PERIOD_MONTH;
        $planCashFlows->description = 'Создано АвтоПланированием';
        $planCashFlows->plan_cash_contractor_id = $this->id;
        if (!$planCashFlows->_save()) {
            return false;
        }

        return true;
    }

    public function updateRepeatedFlows()
    {
        if (!$this->isNewRecord)
            $this->deleteRepeatedFlows();

        $this->createRepeatedFlows();
    }

    public function deleteRepeatedFlows()
    {
        $flows = PlanCashFlows::find()->where(['plan_cash_contractor_id' => $this->id, 'is_repeated' => 1])->all();
        foreach ($flows as $flow) {
            $flow->delete();
        }

        return true;
    }

    public function deleteFlows()
    {
        $flows = PlanCashFlows::find()->where(['plan_cash_contractor_id' => $this->id])->all();
        foreach ($flows as $flow) {
            $flow->delete();
        }

        return true;
    }

    public static function deleteFlowByInvoice(Invoice $invoice)
    {
        /** @var PlanCashFlows $flow */
        $flow = PlanCashFlows::find()
            ->where(['company_id' => $invoice->company_id])
            ->andWhere(['invoice_id' => $invoice->id])
            ->one();

        if ($flow) {
            $flow->delete();
        }

        return true;
    }

    public static function updateFlowByInvoice(Invoice $invoice)
    {
        /** @var PlanCashFlows $flow */
        $flow = PlanCashFlows::find()
            ->where(['company_id' => $invoice->company_id])
            ->andWhere(['invoice_id' => $invoice->id])
            ->one();

        $hasNewContractorInPlan = PlanCashContractor::find()
            ->where(['company_id' => $invoice->company_id])
            ->andWhere(['contractor_id' => $invoice->contractor_id])
            ->andWhere(['plan_type' => PlanCashContractor::PLAN_BY_FUTURE_INVOICES])
            ->exists();

        if ($hasNewContractorInPlan) {

            if ($flow) {

                $flow->updateAttributes([
                    'date' => $invoice->payment_limit_date,
                    'contractor_id' => $invoice->contractor_id
                ]);

            } else {

                return self::createFlowByInvoice($invoice);

            }

        } else {

            if ($flow) {
                $flow->delete();
            }
        }

        return true;
    }

    public static function createFlowByInvoice(Invoice $invoice) 
    {
        $flowType = $invoice->type == Documents::IO_TYPE_IN ?
            CashFlowsBase::FLOW_TYPE_EXPENSE : CashFlowsBase::FLOW_TYPE_INCOME;
        $itemId = ($flowType == CashFlowsBase::FLOW_TYPE_EXPENSE) ?
            $invoice->contractor->invoice_expenditure_item_id : $invoice->contractor->invoice_income_item_id;

        // prev periods
        if (PlanCashContractor::find()
            ->where(['company_id' => $invoice->company_id])
            ->andWhere(['contractor_id' => $invoice->contractor_id])
            ->andWhere(['plan_type' => PlanCashContractor::PLAN_BY_PREVIOUS_FLOWS])
            ->andWhere(['flow_type' => $flowType])
            ->exists()) {

            return true;
        }


        /** @var PlanCashContractor $planContractor */
        $planContractor = PlanCashContractor::find()
            ->where(['company_id' => $invoice->company_id])
            ->andWhere(['contractor_id' => $invoice->contractor_id])
            ->andWhere(['plan_type' => PlanCashContractor::PLAN_BY_FUTURE_INVOICES])
            ->andWhere(['flow_type' => $flowType])
            ->andWhere(['item_id' => $itemId])
            ->one();

        if (!$planContractor) {

            /** @var PlanCashRule $planRule */
            $planRule = PlanCashRule::find()
                ->where(['company_id' => $invoice->company_id])
                ->andWhere(['flow_type' => $flowType])
                ->andWhere(($flowType == CashFlowsBase::FLOW_TYPE_EXPENSE) ?
                    ['or', ['expenditure_item_id' => null], ['expenditure_item_id' => $itemId]] :
                    ['or', ['income_item_id' => null], ['income_item_id' => $itemId]]
                )->one();

            if ($planRule) {

                $newPlanContractor = new PlanCashContractor([
                    'company_id' => $invoice->company_id,
                    'contractor_id' => $invoice->contractor_id,
                    'plan_type' => PlanCashContractor::PLAN_BY_FUTURE_INVOICES,
                    'flow_type' => $flowType,
                    'payment_type' => self::getPaymentType($invoice->contractor),
                    'item_id' => $itemId,
                    'payment_delay' => $invoice->contractor->payment_delay,
                    'created_at' => time(),
                    'updated_at' => time(),
                    'rule_id' => $planRule->id
                ]);

                if ($newPlanContractor->save()) {
                    $planContractor = $newPlanContractor;
                } else {
                    // var_dump($newPlanContractor->getErrors()); exit;
                    Yii::error('PlanCashContractor::createFlowByInvoice (398):' .
                        serialize($newPlanContractor->getErrors()));

                    return false;
                }
            }
        }

        /** @var \frontend\modules\reports\models\PlanCashContractor $planContractor */
        if ($planContractor && !PlanCashFlows::find()->where(['invoice_id' => $invoice->id])->exists()) {

            $planCashFlows = new PlanCashFlows();
            $planCashFlows->is_repeated = FALSE;

            $planCashFlows->company_id = $planContractor->company_id;
            $planCashFlows->contractor_id = $planContractor->contractor_id;
            $planCashFlows->flow_type = $planContractor->flow_type;
            $planCashFlows->payment_type = $planContractor->payment_type;
            $planCashFlows->amount = round($invoice->total_amount_with_nds / 100, 2);
            if ($planContractor->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE) {
                $planCashFlows->expenditure_item_id = $planContractor->item_id;
            } else {
                $planCashFlows->income_item_id = $planContractor->item_id;
            }
            $planCashFlows->description = 'Создано АвтоПланированием по Счету №' . $invoice->getFullNumber();
            $planCashFlows->plan_cash_contractor_id = $planContractor->id;
            $planCashFlows->invoice_id = $invoice->id;
            $planCashFlows->date = $invoice->payment_limit_date;

            if (!$planCashFlows->save()) {
                //var_dump($planCashFlows->getErrors()); exit;
                Yii::error('PlanCashContractor::createFlowByInvoice (430):' .
                    serialize($planCashFlows->getErrors()));

                return false;
            }
        }

        return true;
    }

    /**
     * @param $model Contractor
     * @return int
     */
    public static function getPaymentType($model)
    {
        if ($model instanceof Contractor)
        {
            $bankDate = (int)CashBankFlows::find()->where(['contractor_id' => $model->id])->orderBy(['date' => SORT_DESC])->select('UNIX_TIMESTAMP(date)')->scalar();
            $orderDate = (int)CashOrderFlows::find()->where(['contractor_id' => $model->id])->orderBy(['date' => SORT_DESC])->select('UNIX_TIMESTAMP(date)')->scalar();
            $emoneyDate = (int)CashEmoneyFlows::find()->where(['contractor_id' => $model->id])->orderBy(['date' => SORT_DESC])->select('UNIX_TIMESTAMP(date)')->scalar();

            if ($bankDate >= $orderDate && $bankDate >= $emoneyDate)
                return PlanCashContractor::PAYMENT_TYPE_BANK;
            if ($orderDate >= $bankDate && $orderDate >= $emoneyDate)
                return PlanCashContractor::PAYMENT_TYPE_ORDER;
            if ($emoneyDate >= $bankDate && $emoneyDate >= $orderDate)
                return PlanCashContractor::PAYMENT_TYPE_EMONEY;
        }

        return PlanCashContractor::PAYMENT_TYPE_BANK;
    }

    /**
     * @param $company_id
     * @return array
     */
    public static function getPrevPeriodsContractors($company_id)
    {
        return PlanCashContractor::find()
            ->where([
                'company_id' => $company_id,
                'plan_type' => self::PLAN_BY_PREVIOUS_FLOWS
            ])->indexBy('contractor_id')->select('contractor_id')
            ->column();
    }
}
