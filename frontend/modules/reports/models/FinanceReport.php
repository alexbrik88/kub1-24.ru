<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 28.02.2019
 * Time: 14:03
 */

namespace frontend\modules\reports\models;


/**
 * Interface FinanceReport
 * @package frontend\modules\reports\models
 */
interface FinanceReport
{
    /**
     * Build data for report by type param (activity or purse).
     * @param $type integer
     * @param array $params
     * @return array
     */
    public function search($type, $params = []);

    /**
     * Build data for report by activity.
     * @param array $params
     * @return array
     */
    public function searchByActivity($params = []);

    /**
     * Build data for report by purse.
     * @param array $params
     * @return array
     */
    public function searchByPurse($params = []);

    /**
     * Returns array for year filter.
     * @return array
     */
    public function getYearFilter();

    /**
     * Generate xls report by type param (activity or purse).
     * @param $type integer
     * @return mixed
     */
    public function generateXls($type);
}