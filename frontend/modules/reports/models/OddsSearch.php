<?php

namespace frontend\modules\reports\models;

use common\modules\acquiring\models\AcquiringOperation;
use Yii;
use yii\base\Exception;
use yii\web\NotFoundHttpException;
use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\ExpenseItemFlowOfFunds;
use common\models\IncomeItemFlowOfFunds;
use frontend\modules\documents\components\InvoiceStatistic;
use frontend\modules\reports\components\DebtsHelper;
use common\components\TextHelper;
use common\models\document\Invoice;
use frontend\models\Documents;

/**
 * Class FlowOfFundsReportSearch
 * @package frontend\modules\reports\models
 */
class OddsSearch extends OLAP
{
    const TAB_ODDS = 1;
    const TAB_ODDS_BY_PURSE = 2;
    const MIN_REPORT_DATE = '2010';

    public $year;
    public $contractor_id;
    public $reason_id;
    public $period;
    public $group;
    public $flow_type;
    public $payment_type;

    private $_company;

    public $incomeItemIdManyItem;
    public $expenditureItemIdManyItem;

    public $incomeItemName = [];
    public $expenditureItemName = [];

    public $incomeItemSort = [];
    public $expenditureItemSort = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['contractor_id', 'reason_id', 'payment_type'], 'integer'],
        ]);
    }

    public function init()
    {
        parent::init();

        //$this->year = \Yii::$app->session->get('modules.reports.finance.year', date('Y'));
        $this->year = date('Y');
        $this->_company = Yii::$app->user->identity->company;
        $this->incomeItemName = InvoiceIncomeItem::find()
            ->select('name')
            ->where(['or', ['company_id' => null], ['company_id' => $this->_company->id]])
            ->indexBy('id')
            ->column(Yii::$app->db2);
        $this->expenditureItemName = InvoiceExpenditureItem::find()
            ->select('name')
            ->where(['or', ['company_id' => null], ['company_id' => $this->_company->id]])
            ->indexBy('id')
            ->column(Yii::$app->db2);
        $this->incomeItemSort = IncomeItemFlowOfFunds::find()
            ->select('flow_of_funds_block')
            ->where(['company_id' => $this->_company->id])
            ->andWhere(['not', ['flow_of_funds_block' => null]])
            ->indexBy('income_item_id')
            ->column(Yii::$app->db2);
        $this->expenditureItemSort = ExpenseItemFlowOfFunds::find()
            ->select('flow_of_funds_block')
            ->where(['company_id' => $this->_company->id])
            ->andWhere(['not', ['flow_of_funds_block' => null]])
            ->indexBy('expense_item_id')
            ->column(Yii::$app->db2);
    }

    /**
     * @param $type
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function search($type, $params = [])
    {
        if ($type == self::TAB_ODDS_BY_PURSE) {
            return $this->searchByPurse($params);
        } elseif ($type == self::TAB_ODDS) {
            return $this->searchByActivity($params);
        }
        throw new NotFoundHttpException('Invalid type param.');
    }

    /**
     * @param $type
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function searchByDays($type, $params = [])
    {
        if ($type == self::TAB_ODDS_BY_PURSE) {
            return $this->searchByPurseDays($params);
        } elseif ($type == self::TAB_ODDS) {
            return $this->searchByActivityDays($params);
        }
        throw new NotFoundHttpException('Invalid type param.');
    }

    private function __getActivityBlockType($type, $itemId)
    {
        if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
            if (isset($this->incomeItemSort[$itemId])) {
                return AbstractFinance::$typesByBlock[$this->incomeItemSort[$itemId]][0];
            } elseif (in_array($itemId, AbstractFinance::$typeItem[AbstractFinance::RECEIPT_FINANCING_TYPE_FIRST])) {
                return AbstractFinance::RECEIPT_FINANCING_TYPE_FIRST;
            } else {
                return AbstractFinance::INCOME_OPERATING_ACTIVITIES;
            }
        }
        if ($type == CashFlowsBase::FLOW_TYPE_EXPENSE) {
            if (isset($this->expenditureItemSort[$itemId])) {
                return AbstractFinance::$typesByBlock[$this->expenditureItemSort[$itemId]][1];
            } elseif (in_array($itemId, AbstractFinance::$typeItem[AbstractFinance::RECEIPT_FINANCING_TYPE_SECOND])) {
                return AbstractFinance::RECEIPT_FINANCING_TYPE_SECOND;
            } else {
                return AbstractFinance::WITHOUT_TYPE;
            }
        }

        throw new Exception('getActivityWallet: wallet not found');
    }

    private function __getCashContractorName($type, $itemId, $contractorId = null)
    {
        if (in_array($contractorId, ['bank', 'order', 'emoney'])) {
            return ($type == 0 ? 'Перевод в ' : 'Приход из ') .
                ArrayHelper::getValue(AbstractFinance::$cashContractorTextByType, "{$type}.{$contractorId}");
        }

        return ArrayHelper::getValue(($type == 0) ? $this->expenditureItemName : $this->incomeItemName, $itemId);
    }

    public function searchByActivity($params = [])
    {
        $rawData = $this->getByActivityRawData();
        $data = self::getByActivityEmptyData();
        $growingData = self::getByActivityEmptyGrowingData();
        $totalData = self::getEmptyTotalData();

        foreach ($rawData as $d) {

            $type = $this->__getActivityBlockType($d['type'], $d['item_id']);
            $wallet = AbstractFinance::$blockByType[$type];
            $item = $d['item_id'];
            $m = $d['m'];
            $flowKoef = ($d['type'] == 0 ? -1 : 1);
            $amount = $d['amount'];
            $growingAmount = $d['growing_amount'];

            if (!isset($data[$wallet]['levels'][$type]['levels'][$item])) {
                $data[$wallet]['levels'][$type]['levels'][$item] = [
                    'title' => $this->__getCashContractorName($d['type'], $d['item_id']),
                    'data' => self::EMPTY_YEAR
                ];
            }

            $data[$wallet]['levels'][$type]['levels'][$item]['data'][$m] += $amount;
            $data[$wallet]['levels'][$type]['data'][$m] += $amount;
            $data[$wallet]['data'][$m] += $flowKoef * $amount;
            $growingData[$wallet]['data'][$m] += $flowKoef * $growingAmount;
            $totalData['month']['data'][$m] += $flowKoef * $amount;
            $totalData['growing']['data'][$m] += $flowKoef * $growingAmount;
        }

        // sort
        foreach ($data as &$d1)
            foreach ($d1['levels'] as &$d2)
                uasort($d2['levels'], function ($a, $b) {
                    if ($a['title'] == $b['title']) return 0;
                    return ($a['title'] < $b['title']) ? -1 : 1;
                });

        return [
            'data' => $data,
            'growingData' => $growingData,
            'totalData' => $totalData
        ];
    }

    public function searchByActivityDays($params = [])
    {
        $rawDataArr = $this->getByActivityRawDataDay();
        $data = self::getByActivityEmptyData(true);
        $growingData = self::getByActivityEmptyGrowingData(true);
        $totalData = self::getEmptyTotalData(true);

        // start year balance
        foreach ($rawDataArr['growingStart'] as $d) {

            $type = $this->__getActivityBlockType($d['type'], $d['item_id']);
            $wallet = AbstractFinance::$blockByType[$type];
            $flowKoef = ($d['type'] == 0 ? -1 : 1);

            foreach ($growingData[$wallet]['data'] as $k => &$g) {
                $g += $flowKoef * $d['amount'];
            }
            foreach ($totalData['growing']['data'] as &$t) {
                $t += $flowKoef * $d['amount'];
            }
        }

        foreach ($rawDataArr['flat'] as $d) {

            $type = $this->__getActivityBlockType($d['type'], $d['item_id']);
            $wallet = AbstractFinance::$blockByType[$type];
            $item = $d['item_id'];
            $key = $d['m'].$d['d'];
            $flowKoef = ($d['type'] == 0 ? -1 : 1);
            $amount = $d['amount'];

            if (!isset($data[$wallet]['levels'][$type]['levels'][$item])) {
                $data[$wallet]['levels'][$type]['levels'][$item] = [
                    'title' => $this->__getCashContractorName($d['type'], $d['item_id']),
                    'data' => self::getEmptyYearInDays($this->year)
                ];
            }

            $data[$wallet]['levels'][$type]['levels'][$item]['data'][$key] += $amount;
            $data[$wallet]['levels'][$type]['data'][$key] += $amount;
            $data[$wallet]['data'][$key] += $flowKoef * $amount;
            $totalData['month']['data'][$key] += $flowKoef * $amount;

            foreach ($growingData[$wallet]['data'] as $k => &$g) {
                if ($k >= $key)
                    $g += $flowKoef * $amount;
            }
            foreach ($totalData['growing']['data'] as $k => &$t) {
                if ($k >= $key)
                    $t += $flowKoef * $amount;
            }
        }

        // sort
        foreach ($data as &$d1)
            foreach ($d1['levels'] as &$d2)
                uasort($d2['levels'], function ($a, $b) {
                    if ($a['title'] == $b['title']) return 0;
                    return ($a['title'] < $b['title']) ? -1 : 1;
                });

        return [
            'data' => $data,
            'growingData' => $growingData,
            'totalData' => $totalData
        ];
    }

    public function searchByPurse($params = [])
    {
        $rawData = $this->getByPurseRawData();
        $data = self::getByPurseEmptyData();
        $growingData = self::getByPurseEmptyGrowingData();
        $totalData = self::getEmptyTotalData();
        foreach ($rawData as $d) {

            $wallet = $d['wallet'];
            $type = ($d['type'] == 0 ? 1 : 0) + (($d['wallet'] - 1) * 2) + 1;
            $item = $d['item_id'];
            $m = $d['m'];
            $flowKoef = ($d['type'] == 0 ? -1 : 1);
            $amount = $d['amount'];
            $growingAmount = $d['growing_amount'];

            if (!isset($data[$wallet]['levels'][$type]['levels'][$item])) {
                $data[$wallet]['levels'][$type]['levels'][$item] = [
                    'title' => $this->__getCashContractorName($d['type'], $d['item_id'], $d['contractor_id']),
                    'data' => self::EMPTY_YEAR
                ];
            }

            $data[$wallet]['levels'][$type]['levels'][$item]['data'][$m] += $amount;
            $data[$wallet]['levels'][$type]['data'][$m] += $amount;
            $data[$wallet]['data'][$m] += $flowKoef * $amount;
            $growingData[$wallet]['data'][$m] += $flowKoef * $growingAmount;
            $totalData['month']['data'][$m] += $flowKoef * $amount;
            $totalData['growing']['data'][$m] += $flowKoef * $growingAmount;
        }

        // sort
        foreach ($data as &$d1)
            foreach ($d1['levels'] as &$d2)
                uasort($d2['levels'], function ($a, $b) {
                    if ($a['title'] == $b['title']) return 0;
                    return ($a['title'] < $b['title']) ? -1 : 1;
                });

        return [
            'data' => $data,
            'growingData' => $growingData,
            'totalData' => $totalData
        ];
    }

    public function searchByPurseDays($params = [])
    {
        $rawDataArr = $this->getByPurseRawDataDay();
        $data = self::getByPurseEmptyData(true);
        $growingData = self::getByPurseEmptyGrowingData(true);
        $totalData = self::getEmptyTotalData(true);

        // start year balance
        foreach ($rawDataArr['growingStart'] as $d) {
            foreach ($growingData[$d['wallet']]['data'] as $k => &$g) {
                    $g += $d['amount'];
            }
            foreach ($totalData['growing']['data'] as &$t) {
                $t += $d['amount'];
            }
        }

        foreach ($rawDataArr['flat'] as $d) {

            $wallet = $d['wallet'];
            $type = ($d['type'] == 0 ? 1 : 0) + (($d['wallet'] - 1) * 2) + 1;
            $item = $d['item_id'];
            $key = $d['m'].$d['d'];
            $flowKoef = ($d['type'] == 0 ? -1 : 1);
            $amount = $d['amount'];

            if (!isset($data[$wallet]['levels'][$type]['levels'][$item])) {
                $data[$wallet]['levels'][$type]['levels'][$item] = [
                    'title' => $this->__getCashContractorName($d['type'], $d['item_id'], $d['contractor_id']),
                    'data' => self::getEmptyYearInDays($this->year)
                ];
            }

            $data[$wallet]['levels'][$type]['levels'][$item]['data'][$key] += $amount;
            $data[$wallet]['levels'][$type]['data'][$key] += $amount;
            $data[$wallet]['data'][$key] += $flowKoef * $amount;
            $totalData['month']['data'][$key] += $flowKoef * $amount;

            foreach ($growingData[$wallet]['data'] as $k => &$g) {
                if ($k >= $key)
                    $g += $flowKoef * $amount;
            }
            foreach ($totalData['growing']['data'] as $k => &$t) {
                if ($k >= $key)
                    $t += $flowKoef * $amount;
            }
        }

        // sort
        foreach ($data as &$d1)
            foreach ($d1['levels'] as &$d2)
                uasort($d2['levels'], function ($a, $b) {
                    if ($a['title'] == $b['title']) return 0;
                    return ($a['title'] < $b['title']) ? -1 : 1;
                });

        return [
            'data' => $data,
            'growingData' => $growingData,
            'totalData' => $totalData
        ];
    }

    public function getByActivityRawData()
    {
        $table = self::TABLE_OLAP_FLOWS;
        $companyId = $this->_company->id;
        $year = $this->year;

        $seq = 'seq_0_to_11';
        $interval = "('{$year}-01-31' + INTERVAL (seq) MONTH)";

        $query = "
            SELECT
              t.item_id,
              t.type,
              DATE_FORMAT({$interval}, '%m') m,
              SUM(IF(DATE_FORMAT(t.date, '%Y%m') = DATE_FORMAT({$interval}, '%Y%m'), t.amount, 0)) amount,
              SUM(t.amount) growing_amount
            FROM {$seq}
            JOIN {$table} t ON
              t.company_id = {$companyId}
              AND t.date <= {$interval}
              AND t.contractor_id NOT IN ('bank', 'order', 'emoney')
            GROUP BY t.type, t.item_id, m
          ";

        return Yii::$app->db2->createCommand($query)->queryAll();
    }

    public function getByActivityRawDataDay()
    {
        $table = self::TABLE_OLAP_FLOWS;
        $companyId = $this->_company->id;
        $year = $this->year;

        $query = "
            SELECT
              t.item_id,
              t.type,
              DATE_FORMAT(t.date, '%m') m,
              DATE_FORMAT(t.date, '%d') d,
              SUM(t.amount) amount
            FROM {$table} t
            WHERE
              t.company_id = {$companyId}
              AND t.date BETWEEN '{$year}-01-01' AND '{$year}-12-31'
              AND t.contractor_id NOT IN ('bank', 'order', 'emoney')
            GROUP BY t.type, t.item_id, m, d
          ";

        $queryGrowingStart = "
            SELECT
              t.type,
              t.item_id,
              SUM(t.amount) amount
            FROM {$table} t
            WHERE
              t.company_id = {$companyId}
              AND t.date < '{$year}-01-01'
              AND t.contractor_id NOT IN ('bank', 'order', 'emoney')
            GROUP BY t.type, t.item_id
        ";

        return [
            'flat' => Yii::$app->db2->createCommand($query)->queryAll(),
            'growingStart' => Yii::$app->db2->createCommand($queryGrowingStart)->queryAll(),
        ];
    }

    public function getByPurseRawData()
    {
        $table = self::TABLE_OLAP_FLOWS;
        $companyId = $this->_company->id;
        $year = $this->year;
        $seq = 'seq_0_to_11';
        $interval = "('{$year}-01-31' + INTERVAL (seq) MONTH)";
        $query = "
            SELECT
              t.wallet,
              t.type,
              t.item_id,
              t.contractor_id,
              DATE_FORMAT({$interval}, '%m') m,
              DATE_FORMAT({$interval}, '%d') d,
              SUM(IF(DATE_FORMAT(t.date, '%Y%m') = DATE_FORMAT({$interval}, '%Y%m'), t.amount, 0)) amount,
              SUM(t.amount) growing_amount
            FROM {$seq}
            JOIN {$table} t ON
              t.company_id = {$companyId}
              AND t.date <= {$interval}
              /* AND t.contractor_id NOT IN ('bank', 'order', 'emoney') */
            GROUP BY t.wallet, t.type, t.item_id, m
          ";

        $tableAcquiring = AcquiringOperation::tableName();
        $queryAcquiring = "
            SELECT
              4 wallet,
              t.flow_type type,
              IF(t.flow_type = 0, t.expenditure_item_id, t.income_item_id) item_id,
              t.contractor_id,
              DATE_FORMAT({$interval}, '%m') m,
              DATE_FORMAT({$interval}, '%d') d,
              SUM(IF(DATE_FORMAT(t.date, '%Y%m') = DATE_FORMAT({$interval}, '%Y%m'), t.amount, 0)) amount,
              SUM(t.amount) growing_amount
            FROM {$seq}
            JOIN {$tableAcquiring} t ON
              t.company_id = {$companyId}
              AND t.date <= {$interval}
            GROUP BY t.flow_type, IF(t.flow_type = 0, t.expenditure_item_id, t.income_item_id), m
          ";

        return array_merge(
            Yii::$app->db2->createCommand($query)->queryAll(),
            Yii::$app->db->createCommand($queryAcquiring)->queryAll()
        );
    }

    public function getByPurseRawDataDay()
    {
        $table = self::TABLE_OLAP_FLOWS;
        $companyId = $this->_company->id;
        $year = $this->year;

        $query = "
            SELECT
              t.wallet,
              t.item_id,
              t.type,
              t.contractor_id,
              DATE_FORMAT(t.date, '%m') m,
              DATE_FORMAT(t.date, '%d') d,
              SUM(t.amount) amount
            FROM {$table} t
            WHERE
              t.company_id = {$companyId}
              AND t.date BETWEEN '{$year}-01-01' AND '{$year}-12-31'
              /* AND t.contractor_id NOT IN ('bank', 'order', 'emoney') */
            GROUP BY t.wallet, t.type, t.item_id, m, d
          ";

        $queryGrowingStart = "
            SELECT
              t.wallet,
              SUM(IF(t.type = 0, -1, 1) * t.amount) amount
            FROM {$table} t
            WHERE
              t.company_id = {$companyId}
              AND t.date < '{$year}-01-01'
              /* AND t.contractor_id NOT IN ('bank', 'order', 'emoney') */
            GROUP BY t.wallet
        ";

        return [
            'flat' => Yii::$app->db2->createCommand($query)->queryAll(),
            'growingStart' => Yii::$app->db2->createCommand($queryGrowingStart)->queryAll(),
        ];
    }

    public function getByActivityEmptyData($byDays = false)
    {
        return [
            AbstractFinance::OPERATING_ACTIVITIES_BLOCK => [
                'title' => 'Операционная деятельность',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'levels' => [
                    AbstractFinance::INCOME_OPERATING_ACTIVITIES => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::WITHOUT_TYPE => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ],
                'question' => '#tooltip_operation-activities-block',
            ],
            AbstractFinance::FINANCIAL_OPERATIONS_BLOCK => [
                'title' => 'Финансовая деятельность',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'levels' => [
                    AbstractFinance::RECEIPT_FINANCING_TYPE_FIRST => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::RECEIPT_FINANCING_TYPE_SECOND => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ],
                'question' => '#tooltip_financial-operations-block',
            ],
            AbstractFinance::INVESTMENT_ACTIVITIES_BLOCK => [
                'title' => 'Инвестиционная деятельность',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'levels' => [
                    AbstractFinance::INCOME_INVESTMENT_ACTIVITIES => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::EXPENSE_INVESTMENT_ACTIVITIES => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ],
                'question' => '#tooltip_investment-activities-block',
            ],
        ];
    }

    public function getByActivityEmptyGrowingData($byDays = false)
    {
        return [
            AbstractFinance::OPERATING_ACTIVITIES_BLOCK => [
                'title' => 'Остаток по оп. деятельности',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
            ],
            AbstractFinance::FINANCIAL_OPERATIONS_BLOCK => [
                'title' => 'Остаток по фин. деятельности',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
            ],
            AbstractFinance::INVESTMENT_ACTIVITIES_BLOCK => [
                'title' => 'Остаток по инвест. деятельности',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
            ]
        ];
    }

    public function getByPurseEmptyData($byDays = false)
    {
        return [
            AbstractFinance::CASH_BANK_BLOCK => [
                'title' => 'Банк',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'levels' => [
                    AbstractFinance::INCOME_CASH_BANK => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::EXPENSE_CASH_BANK => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ]
            ],
            AbstractFinance::CASH_ORDER_BLOCK => [
                'title' => 'Касса',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'levels' => [
                    AbstractFinance::INCOME_CASH_ORDER => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::EXPENSE_CASH_ORDER => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                ]
            ],
            AbstractFinance::CASH_ACQUIRING_BLOCK => [
                'title' => 'Интернет-эквайринг',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'levels' => [
                    AbstractFinance::INCOME_ACQUIRING => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::EXPENSE_ACQUIRING => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ]
                ],
            ],
            AbstractFinance::CASH_EMONEY_BLOCK => [
                'title' => 'E-money',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                'levels' => [
                    AbstractFinance::INCOME_CASH_EMONEY => [
                        'title' => 'Приход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ],
                    AbstractFinance::EXPENSE_CASH_EMONEY => [
                        'title' => 'Расход',
                        'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
                        'levels' => []
                    ]
                ],
            ],
        ];
    }

    public function getByPurseEmptyGrowingData($byDays = false)
    {
        return [
            AbstractFinance::CASH_BANK_BLOCK => [
                'title' => 'Остаток по банку',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
            ],
            AbstractFinance::CASH_ORDER_BLOCK => [
                'title' => 'Остаток по кассе',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
            ],
            AbstractFinance::CASH_ACQUIRING_BLOCK => [
                'title' => 'Остаток по интернет-эквайрингу',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
            ],
            AbstractFinance::CASH_EMONEY_BLOCK => [
                'title' => 'Остаток по E-money',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
            ],
        ];
    }

    public function getEmptyTotalData($byDays = false)
    {
        return [
            'month' => [
                'title' => 'Результат по месяцу',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
            ],
            'growing' => [
                'title' => 'Остаток на конец месяца',
                'data' => ($byDays) ? self::getEmptyYearInDays($this->year) : self::EMPTY_YEAR,
            ]
        ];
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        return [date('Y') => date('Y')];
        $tableFlows = self::TABLE_OLAP_FLOWS;
        $minDate = Yii::$app->db2->createCommand("SELECT MIN(`date`) FROM {$tableFlows} WHERE company_id = " . $this->_company->id)->queryScalar();
        $minCashDate = !empty($minDate) ? max(self::MIN_REPORT_DATE, $minDate) : date(DateHelper::FORMAT_DATE);
        $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
        $currentYear = date('Y');
        foreach (range($registrationYear, $currentYear) as $value) {
            $range[$value] = $value;
        }
        arsort($range);

        return $range;
    }

    /**
     * @return bool
     */
    public function getIsCurrentYear()
    {
        return date('Y') == $this->year;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->_company;
    }


    /** ADDITIONAL METHODS */

    public function generateWarnings($growingBalance, $types)
    {
        $warnings = [];
        if ($growingBalance) {
            foreach ($growingBalance as $monthNumber => $oneAmount) {
                if ($oneAmount < 0) {
                    if (isset($warnings[1])) {
                        $warnings[1]['moreThenOne'] = true;
                    } else {
                        $warnings[1] = [
                            'amount' => abs($oneAmount),
                            'month' => AbstractFinance::$monthEnd[substr($monthNumber, 0, 2)],
                            'moreThenOne' => false,
                        ];
                    }
                }
            }
        }

        $totalDebtSum = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_0_MORE_90, true, false);
        if ($totalDebtSum) {
            $incomeOperatingActivitiesAmount = 1;
            $types = (isset($types[AbstractFinance::OPERATING_ACTIVITIES_BLOCK]['levels'][AbstractFinance::INCOME_OPERATING_ACTIVITIES])) ?
                $types[AbstractFinance::OPERATING_ACTIVITIES_BLOCK]['levels'][AbstractFinance::INCOME_OPERATING_ACTIVITIES] : null;
            if (isset($types['data'][date('m')]) && $types['data'][date('m')] > 0) {
                $incomeOperatingActivitiesAmount = $types['data'][date('m')];
            }
            $incomePercent = round(($totalDebtSum / 2 / $incomeOperatingActivitiesAmount) * 100, 2);
            $warnings[2] = [
                'totalDebtSum' => $totalDebtSum,
                'incomePercent' => $incomePercent,
            ];
        }
        $inInvoiceDebtSum = Invoice::find()
            ->select([
                'SUM(' . Invoice::tableName() . '.total_amount_with_nds) - IFNULL(SUM(' . Invoice::tableName() . '.payment_partial_amount), 0) AS sum',
            ])
            ->byIOType(Documents::IO_TYPE_IN)
            ->byDeleted()
            ->byStatus(InvoiceStatistic::$invoiceStatuses[InvoiceStatistic::NOT_PAID][Documents::IO_TYPE_IN])
            ->byCompany($this->_company->id)
            ->asArray()
            ->one();
        $inInvoiceDebtSum = $inInvoiceDebtSum ? $inInvoiceDebtSum['sum'] : 0;
        if ($inInvoiceDebtSum) {
            $warnings[3] = [
                'inInvoiceDebtSum' => $inInvoiceDebtSum,
            ];
            $growingBalanceAmount = 0;
            if (isset($growingBalance[date('m')])) {
                $growingBalanceAmount = $growingBalance[date('m')];
            }
            if ($growingBalanceAmount >= $inInvoiceDebtSum) {
                $warnings[3]['message'] = 'Текущего остатка денежных средств хватит полностью оплатить все счета поставщикам.';
            } else {
                $canPayedPercent = round(($growingBalanceAmount / $inInvoiceDebtSum) * 100, 2);
                $warnings[3]['message'] = 'Текущего остатка денежных средств НЕ хватит полностью оплатить все счета
                 поставщикам. Можно оплатить только ' . $canPayedPercent . '% задолженности перед поставщиками.';
                if ($totalDebtSum) {
                    $zRaznitsa = $inInvoiceDebtSum - $growingBalanceAmount;
                    if ($zRaznitsa <= $totalDebtSum) {
                        $warnings[4] = [
                            'message' => 'Собрав ' . round(($zRaznitsa / $totalDebtSum) * 100, 2) . '% долгов ваших клиентов, вы сможете полностью оплатить долги перед поставщиками.',
                        ];
                    } else {
                        $warnings[4] = [
                            'message' => '<span style="color: red;">Плохие новости!</span><br>
                            Даже собрав 100% задолженности покупателей, вы не сможете рассчитаться с поставщиками.<br>
                            Не хватает ' . TextHelper::invoiceMoneyFormat($zRaznitsa - $totalDebtSum, 2) . ' <i class="fa fa-rub"></i>.
                            Выход:
                            <ul style="padding-left: 17px;">
                                <li>
                                    Увеличить продажи;
                                </li>
                                <li>
                                    Внести собственные деньги в компанию (дать займ);
                                </li>
                                <li>
                                    Взять кредит.
                                </li>
                            </ul>',
                            'debtPercent' => round(($zRaznitsa / $totalDebtSum) * 100, 2),
                        ];
                    }
                }
            }
        }

        return $warnings;
    }

    public function getDaysInMonth($month)
    {
        if (strlen($month) == 1) { $month = '0'.$month; }
        $start = (new \DateTime())->createFromFormat('Y-m-d', $this->year.'-'.$month.'-'.'01');
        $curr = $start;
        $endDate = $start->format('mt');
        $ret = [];
        $err = 1;
        while(true) {
            $date = $curr->format('md');
            $ret[$date] = $curr->format('d');
            $curr = $curr->modify("+1 day");

            if ($date >= $endDate)
                break;

            if (++$err > 31) {
                echo 'getDaysInMonth: error';
                exit;
            }
        }

        return $ret;
    }
}