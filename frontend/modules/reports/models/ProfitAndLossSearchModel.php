<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 15.04.2019
 * Time: 15:34
 */

namespace frontend\modules\reports\models;

use common\components\date\DateHelper;
use common\components\excel\Excel;
use common\components\helpers\ArrayHelper;
use common\components\helpers\Html;
use common\components\ImageHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\company\CompanyTaxationType;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceAct;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\document\InvoiceUpd;
use common\models\document\OrderAct;
use common\models\document\OrderPackingList;
use common\models\document\OrderUpd;
use common\models\document\PackingList;
use common\models\document\status\InvoiceStatus;
use common\models\document\Upd;
use common\models\product\Product;
use common\models\TaxRate;
use frontend\models\Documents;
use frontend\modules\cash\models\CashContractorType;
use common\models\Contractor;
use kartik\checkbox\CheckboxX;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class ProfitAndLossSearchModel
 * @package frontend\modules\reports\models
 */
class ProfitAndLossSearchModel extends AbstractFinance
{
    use profitAndLoss\RevenueTrait;
    use profitAndLoss\FixedCostsTrait;
    use profitAndLoss\OperatingCostsTrait;
    use profitAndLoss\AnotherIncomeTrait;
    use profitAndLoss\AnotherCostsTrait;
    use profitAndLoss\VariableCostsTrait;
    use profitAndLoss\ReceivedPercentTrait;
    use profitAndLoss\PaymentPercentTrait;
    use profitAndLoss\PaymentDividendTrait;
    use profitAndLoss\ChartsTrait;

    const TAB_ALL_OPERATIONS = 10;
    const TAB_ACCOUNTING_OPERATIONS_ONLY = 11;

    // custom filters (items table)
    public $contractor_id;
    public $reason_id;
    public $period;
    public $group;
    public $flow_type;
    public $payment_type;
    public $doc_type;

    // structure filters (items table)
    public $isRevenue;
    public $ioType;
    public $accountingOperationsOnly;

    // modals params
    public $incomeItemIdManyItem;
    public $expenditureItemIdManyItem;

    public static $items = [
        [
            'label' => 'Выручка',
            'addCheckboxX' => true,
            'getter' => 'getRevenue',
            'options' => [
                'class' => 'text-bold expenditure_type',
                'data-children' => 'revenue',
            ],
        ],
        [
            'label' => 'Переменные расходы',
            'addCheckboxX' => true,
            'getter' => 'getVariableCosts',
            'options' => [
                'class' => 'text-bold expenditure_type expenses-block',
                'data-block' => ProfitAndLossCompanyItem::VARIABLE_EXPENSE_TYPE,
                'data-children' => 'variable-costs',
            ]
        ],
        [
            'label' => 'Маржинальный доход',
            'getter' => 'getMarginalIncome',
            'options' => [
                'class' => 'text-bold expenditure_type',
            ],
        ],
        [
            'label' => 'Маржинальность',
            'getter' => 'getMarginality',
            'suffix' => '%',
            'options' => [
                'class' => 'expenditure_type',
            ],
        ],
        [
            'label' => 'Постоянные расходы',
            'addCheckboxX' => true,
            'getter' => 'getFixedCosts',
            'options' => [
                'class' => 'text-bold expenditure_type expenses-block',
                'data-block' => ProfitAndLossCompanyItem::CONSTANT_EXPENSE_TYPE,
                'data-children' => 'fixed-costs',
            ],
        ],
        [
            'label' => 'Валовая прибыль',
            'getter' => 'getGrossProfit',
            'options' => [
                'class' => 'text-bold expenditure_type',
            ],
        ],
        //[
        //    'label' => 'Рентабельность по валовой прибыли',
        //    'getter' => 'getProfitabilityByGrossProfit',
        //    'suffix' => '%',
        //    'options' => [
        //        'class' => 'expenditure_type',
        //    ],
        //],
        [
            'label' => 'Операционные расходы',
            'addCheckboxX' => true,
            'getter' => 'getOperatingCosts',
            'options' => [
                'class' => 'text-bold expenditure_type expenses-block',
                'data-block' => ProfitAndLossCompanyItem::OPERATING_EXPENSE_TYPE,
                'data-children' => 'operating-costs',
            ],
        ],
        [
            'label' => 'Операционная прибыль',
            'getter' => 'getOperatingProfit',
            'options' => [
                'class' => 'text-bold expenditure_type',
            ],
        ],
        //[
        //    'label' => 'Рентабельность по операционной прибыли',
        //    'getter' => 'getProfitabilityByOperatingProfit',
        //    'suffix' => '%',
        //    'options' => [
        //        'class' => 'expenditure_type',
        //    ],
        //],
        [
            'label' => 'Другие доходы',
            'addCheckboxX' => true,
            'getter' => 'getAnotherIncome',
            'options' => [
                'class' => 'text-bold expenditure_type',
                'data-children' => 'another-income',
            ],
        ],
        [
            'label' => 'Другие расходы',
            'addCheckboxX' => true,
            'getter' => 'getAnotherCosts',
            'options' => [
                'class' => 'text-bold expenditure_type expenses-block',
                'data-block' => ProfitAndLossCompanyItem::OTHER_EXPENSE_TYPE,
                'data-children' => 'another-costs',
            ],
        ],
        [
            'label' => 'EBITDA',
            'getter' => 'getEBITDA',
            'options' => [
                'class' => 'text-bold expenditure_type',
            ],
        ],
        [
            'label' => 'Проценты полученные',
            'getter' => 'getReceivedPercent',
            'options' => [
                'class' => 'expenditure_type',
            ],
        ],
        [
            'label' => 'Проценты уплаченные',
            'getter' => 'getPaymentPercent',
            'options' => [
                'class' => 'expenditure_type',
            ],
        ],
        [
            'label' => 'Прибыль до налогообложения',
            'getter' => 'getProfitBeforeTax',
            'options' => [
                'class' => 'expenditure_type',
            ],
        ],
        [
            'label' => 'Налоги',
            'getter' => 'getTax',
            'options' => [
                'class' => 'expenditure_type',
            ],
        ],
        [
            'label' => 'Выплата дивидендов',
            'getter' => 'getPaymentDividend',
            'options' => [
                'class' => 'expenditure_type',
            ],
            'isGetterOnly' => true,
        ],
        [
            'label' => 'Чистая прибыль/убыток',
            'getter' => 'getNetIncomeLoss',
            'options' => [
                'class' => 'text-bold expenditure_type',
            ],
        ],
        [
            'label' => 'Рентабельность по чистой прибыли',
            'getter' => 'getProfitabilityByNetIncomeLoss',
            'suffix' => '%',
            'options' => [
                'class' => 'expenditure_type',
            ],
        ],
        [
            'getter' => 'getPaymentDividend',
        ],
        [
            'label' => 'Нераспределенная прибыль',
            'getter' => 'getUndestributedProfits',
            'options' => [
                'class' => 'expenditure_type',
            ],
        ],        
    ];

    /**
     * @var
     */
    protected $_yearFilter;
    /**
     * @var
     */
    protected $_month;
    /**
     * @var array
     */
    protected $data = [];
    /**
     * @var
     */
    protected $_minCashYearMonth;

    /**
     * @var array
     */
    protected static $notAccountingContractors = [];

    protected $result = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['contractor_id', 'reason_id', 'payment_type', 'doc_type'], 'integer'],
        ]);
    }

    /**
     *
     */
    public function init() {

        $this->_minCashYearMonth = date('Ym');

        return parent::init();
    }

    /**
     * @return array
     */
    public function handleItems()
    {
        self::$notAccountingContractors = $this->_getNotAccountingContractors();

        $years = array_reverse($this->getYearFilter());

        foreach ($years as $year) {

            // needed for charts 2019-2020
            //if ($year > $this->year) {
            //    break;
            //}

            $this->result[$year] = [];
            foreach (static::$items as $item) {
                $getter = ArrayHelper::remove($item, 'getter');
                $item = self::_getBlankParent($item);

                if (isset($item['isGetterOnly'])) {
                    $this->$getter($item, $year);
                    continue;
                }

                $this->result[$year] = array_merge($this->result[$year], $this->$getter($item, $year));
            }

        }

        return $this->result[$this->year];
    }

    public function getRow($year, $name, $amountKey = 'amount')
    {
        if (isset($this->result[$year]) && isset($this->result[$year][$name]) && isset($this->result[$year][$name][$amountKey]))
            return $this->result[$year][$name][$amountKey];

        return [];
    }

    public function getValue($name, $year, $month, $amountKey = 'amount')
    {
        $month = str_pad($month, 2, "0", STR_PAD_LEFT);

        if (isset($this->result[$year]) &&
            isset($this->result[$year][$name]) &&
            isset($this->result[$year][$name][$amountKey]) &&
            isset($this->result[$year][$name][$amountKey][$month]))

                return $this->result[$year][$name][$amountKey][$month];

        return null;
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// Getters working with DB in Traits
    ////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param $item
     * @return mixed
     */
    public function getUndestributedProfits($item, $year)
    {
        if (!array_key_exists('undestributedProfits' . $year, $this->data)) {
            if (is_array($this->data['netIncomeLoss' . $year]) && is_array($this->data['paymentDividend' . $year])) {
                $netIncomeLoss = current($this->data['netIncomeLoss' . $year]);
                $paymentDividend = current($this->data['paymentDividend' . $year]);

                // clear before calc
                foreach ([&$netIncomeLoss, &$paymentDividend] as $ki => &$data) {
                    foreach (['quarter-1', 'quarter-2', 'quarter-3', 'quarter-4', 'total'] as $key) {

                        if (isset($data['amount'][$key])) unset($data['amount'][$key]);
                        if (isset($data['amountTax'][$key])) unset($data['amountTax'][$key]);
                    }
                }

                foreach ($netIncomeLoss['amount'] as $key => $amount) {
                    $item['amount'][$key] = $netIncomeLoss['amount'][$key] - $paymentDividend['amount'][$key];
                    if ($key - 1 > 0) {
                        $item['amount'][$key] += $item['amount'][str_pad($key - 1, 2, "0", STR_PAD_LEFT)];
                    } else {
                        // january current + december prev
                        if (isset($this->data['undestributedProfits' . ($year - 1)])) {
                            $prevUndestributedProfit = current($this->data['undestributedProfits' . ($year - 1)]);
                            $item['amount'][$key] += $prevUndestributedProfit['amount']["total"];
                        }
                    }
                }

                foreach ($netIncomeLoss['amountTax'] as $key => $amount) {
                    $item['amountTax'][$key] = $netIncomeLoss['amountTax'][$key] - $paymentDividend['amountTax'][$key];
                    if ($key - 1 > 0) {
                        $item['amountTax'][$key] += $item['amountTax'][str_pad($key - 1, 2, "0", STR_PAD_LEFT)];
                    } else {
                        // january current + december prev
                        if (isset($this->data['undestributedProfits' . ($year - 1)])) {
                            $prevUndestributedProfit = current($this->data['undestributedProfits' . ($year - 1)]);
                            $item['amountTax'][$key] += $prevUndestributedProfit['amountTax']["total"];
                        }

                    }
                }

                // floor quarters/total
                foreach ([1 => "03", 2 => "06", 3 => "09", 4 => "12"] as $q=>$m) {
                    $item['amount']['quarter-'.$q] = $item['amount'][$m];
                    $item['amountTax']['quarter-'.$q] = $item['amountTax'][$m];
                }
                $item['amount']['total'] = $item['amount']["12"];
                $item['amountTax']['total'] = $item['amountTax']["12"];
                
                $this->data['undestributedProfits' . $year]['undestributedProfits'] = $item;
            }
        }

        return $this->data['undestributedProfits' . $year];
    }

    /**
     * @param $item
     * @return mixed
     */
    public function getOperatingProfit($item, $year)
    {
        if (!array_key_exists('operatingProfit' . $year, $this->data)) {
            if (is_array($this->data['grossProfit' . $year]) && is_array($this->data['operatingCosts' . $year])) {
                $grossProfitData = current($this->data['grossProfit' . $year]);
                $operatingCostsData = current($this->data['operatingCosts' . $year]);
                foreach (self::$month as $monthNumber => $monthText) {
                    $item['amount'][$monthNumber] = $grossProfitData['amount'][$monthNumber] - $operatingCostsData['amount'][$monthNumber];
                    $item['amountTax'][$monthNumber] = $grossProfitData['amountTax'][$monthNumber] - $operatingCostsData['amountTax'][$monthNumber];
                }
                $this->data['operatingProfit' . $year] = $this->calculateQuarterAndTotalAmount(['operatingProfit' => $item]);
            }
        }

        return $this->data['operatingProfit' . $year];
    }

    /**
     * @param $item
     * @return mixed
     */
    public function getProfitabilityByOperatingProfit($item, $year)
    {
        if (!array_key_exists('profitabilityByOperatingProfit' . $year, $this->data)) {
            if (is_array($this->data['operatingProfit' . $year]) && is_array($this->data['revenue' . $year])) {
                $revenueData = current($this->data['revenue' . $year]);
                $operatingProfitData = current($this->data['operatingProfit' . $year]);
                foreach ($revenueData['amount'] as $key => $amount) {
                    $revenueAmount = $amount > 0 ? $amount : 1;
                    $item['amount'][$key] = round($operatingProfitData['amount'][$key] / $revenueAmount * 10000, 2);
                }
                foreach ($revenueData['amountTax'] as $key => $amount) {
                    $revenueAmount = $amount > 0 ? $amount : 1;
                    $item['amountTax'][$key] = round($operatingProfitData['amountTax'][$key] / $revenueAmount * 10000, 2);
                }                
                $this->data['profitabilityByOperatingProfit' . $year]['profitabilityByOperatingProfit'] = $item;
            }
        }

        return $this->data['profitabilityByOperatingProfit' . $year];
    }

    /**
     * @param $item
     * @return mixed
     */
    public function getEBITDA($item, $year)
    {
        if (!array_key_exists('ebitda' . $year, $this->data)) {
            if (is_array($this->data['operatingProfit' . $year]) && is_array($this->data['anotherIncome' . $year]) && is_array($this->data['anotherCosts' . $year])) {
                $operatingProfitData = current($this->data['operatingProfit' . $year]);
                $anotherIncomeData = current($this->data['anotherIncome' . $year]);
                $anotherCostsData = current($this->data['anotherCosts' . $year]);
                foreach (self::$month as $monthNumber => $monthText) {
                    $item['amount'][$monthNumber] = $operatingProfitData['amount'][$monthNumber] + $anotherIncomeData['amount'][$monthNumber] - $anotherCostsData['amount'][$monthNumber];
                    $item['amountTax'][$monthNumber] = $operatingProfitData['amountTax'][$monthNumber] + $anotherIncomeData['amountTax'][$monthNumber] - $anotherCostsData['amountTax'][$monthNumber];
                }
                $this->data['ebitda' . $year] = $this->calculateQuarterAndTotalAmount(['ebitda' => $item]);
            }
        }

        return $this->data['ebitda' . $year];
    }

    /**
     * @param $item
     * @return mixed
     */
    public function getNetIncomeLoss($item, $year)
    {
        if (!array_key_exists('netIncomeLoss' . $year, $this->data)) {
            if (
                is_array($this->data['profitBeforeTax' . $year]) &&
                is_array($this->data['tax' . $year])
            ) {
                $profitBeforeTax = current($this->data['profitBeforeTax' . $year]);
                $taxData = current($this->data['tax' . $year]);
                foreach ($profitBeforeTax['amount'] as $key => $amount) {
                    $item['amount'][$key] = $profitBeforeTax['amount'][$key] - $taxData['amount'][$key];
                }
                foreach ($profitBeforeTax['amount'] as $key => $amount) {
                    $item['amountTax'][$key] = $profitBeforeTax['amountTax'][$key] - $taxData['amountTax'][$key];
                }
                $this->data['netIncomeLoss' . $year]['netIncomeLoss'] = $item;
            }
        }

        return $this->data['netIncomeLoss' . $year];
    }

    /**
     * @param $item
     * @return mixed
     */
    public function getProfitabilityByNetIncomeLoss($item, $year)
    {
        if (!array_key_exists('profitabilityByNetIncomeLoss' . $year, $this->data)) {
            if (is_array($this->data['netIncomeLoss' . $year]) && is_array($this->data['revenue' . $year])) {
                $revenueData = current($this->data['revenue' . $year]);
                $netIncomeLossData = current($this->data['netIncomeLoss' . $year]);
                foreach ($revenueData['amount'] as $key => $amount) {
                    $revenueAmount = $amount > 0 ? $amount : 9E9;
                    $item['amount'][$key] = round($netIncomeLossData['amount'][$key] / $revenueAmount * 100 * 100, 2);
                }
                foreach ($revenueData['amountTax'] as $key => $amount) {
                    $revenueAmount = $amount > 0 ? $amount : 9E9;
                    $item['amountTax'][$key] = round($netIncomeLossData['amountTax'][$key] / $revenueAmount * 100 * 100, 2);
                }                
                $this->data['profitabilityByNetIncomeLoss' . $year]['profitabilityByNetIncomeLoss'] = $item;
            }
        }

        return $this->data['profitabilityByNetIncomeLoss' . $year];
    }

    /**
     * @param $item
     * @return mixed
     */
    public function getGrossProfit($item, $year)
    {
        if (!array_key_exists('grossProfit' . $year, $this->data)) {
            if (is_array($this->data['marginalIncome' . $year]) && is_array($this->data['fixedCosts' . $year])) {
                $marginalData = current($this->data['marginalIncome' . $year]);
                $fixedCostsData = current($this->data['fixedCosts' . $year]);
                foreach (self::$month as $monthNumber => $monthText) {
                    $item['amount'][$monthNumber] = $marginalData['amount'][$monthNumber] - $fixedCostsData['amount'][$monthNumber];
                    $item['amountTax'][$monthNumber] = $marginalData['amountTax'][$monthNumber] - $fixedCostsData['amountTax'][$monthNumber];
                }
                $this->data['grossProfit' . $year] = $this->calculateQuarterAndTotalAmount(['grossProfit' => $item]);
            }
        }

        return $this->data['grossProfit' . $year];
    }

    /**
     * @param $item
     * @return mixed
     */
    public function getProfitabilityByGrossProfit($item, $year)
    {
        if (!array_key_exists('profitabilityByGrossProfit' . $year, $this->data)) {
            if (is_array($this->data['grossProfit' . $year]) && is_array($this->data['revenue' . $year])) {
                $revenueData = current($this->data['revenue' . $year]);
                $grossProfitData = current($this->data['grossProfit' . $year]);
                foreach ($revenueData['amount'] as $key => $amount) {
                    $revenueAmount = $amount > 0 ? $amount : 1;
                    $item['amount'][$key] = round($grossProfitData['amount'][$key] / $revenueAmount * 10000, 2);
                }
                foreach ($revenueData['amountTax'] as $key => $amount) {
                    $revenueAmount = $amount > 0 ? $amount : 1;
                    $item['amountTax'][$key] = round($grossProfitData['amountTax'][$key] / $revenueAmount * 10000, 2);
                }                
                $this->data['profitabilityByGrossProfit' . $year]['profitabilityByGrossProfit'] = $item;
            }
        }

        return $this->data['profitabilityByGrossProfit' . $year];
    }

    /**
     * @param $item
     * @return mixed
     */
    public function getMarginalIncome($item, $year)
    {
        if (!array_key_exists('marginalIncome' . $year, $this->data)) {
            if (is_array($this->data['variableCosts' . $year]) && is_array($this->data['revenue' . $year])) {
                $revenueData = current($this->data['revenue' . $year]);
                $variableCostsData = current($this->data['variableCosts' . $year]);
                foreach (self::$month as $monthNumber => $monthText) {
                    $item['amount'][$monthNumber] = $revenueData['amount'][$monthNumber] - $variableCostsData['amount'][$monthNumber];
                    $item['amountTax'][$monthNumber] = $revenueData['amountTax'][$monthNumber] - $variableCostsData['amountTax'][$monthNumber];
                }
                $this->data['marginalIncome' . $year] = $this->calculateQuarterAndTotalAmount(['marginalIncome' => $item]);
            }
        }

        return $this->data['marginalIncome' . $year];
    }

    /**
     * @param $item
     * @return mixed
     */
    public function getMarginality($item, $year)
    {
        if (!array_key_exists('marginality' . $year, $this->data)) {
            if (is_array($this->data['marginalIncome' . $year]) && is_array($this->data['revenue' . $year])) {
                $revenueData = current($this->data['revenue' . $year]);
                $marginalIncomeData = current($this->data['marginalIncome' . $year]);
                foreach ($revenueData['amount'] as $key => $amount) {
                    $revenueAmount = $amount > 0 ? $amount : 9E9;
                    $item['amount'][$key] = round($marginalIncomeData['amount'][$key] / $revenueAmount * 10000, 2);
                }
                foreach ($revenueData['amountTax'] as $key => $amount) {
                    $revenueAmount = $amount > 0 ? $amount : 9E9;
                    $item['amountTax'][$key] = round($marginalIncomeData['amountTax'][$key] / $revenueAmount * 10000, 2);
                }
                $this->data['marginality' . $year]['marginality'] = $item;
            }
        }

        return $this->data['marginality' . $year];
    }

    /**
     * @param $item
     * @return mixed
     */
    public function getProfitBeforeTax($item, $year)
    {
        //  "Прибыль до налогообложения" = EBIDTA + "Проценты полученные" - "Проценты уплаченные""
        if (!array_key_exists('profitBeforeTax' . $year, $this->data)) {

            if (is_array($this->data['ebitda' . $year]) && is_array($this->data['paymentPercent' . $year]) && is_array($this->data['receivedPercent' . $year])) {
                $ebitda = current($this->data['ebitda' . $year]);
                $paymentPercent = current($this->data['paymentPercent' . $year]);
                $receivedPercent = current($this->data['receivedPercent' . $year]);

                foreach (self::$month as $monthNumber => $monthText) {
                    $item['amount'][$monthNumber] = $ebitda['amount'][$monthNumber] + ($receivedPercent['amount'][$monthNumber] - $paymentPercent['amount'][$monthNumber]);
                    $item['amountTax'][$monthNumber] = $ebitda['amountTax'][$monthNumber] + ($receivedPercent['amountTax'][$monthNumber] - $paymentPercent['amountTax'][$monthNumber]);
                }
                $this->data['profitBeforeTax' . $year] = $this->calculateQuarterAndTotalAmount(['profitBeforeTax' => $item]);
            }
        }

        return $this->data['profitBeforeTax' . $year];
    }

    /**
     * @param $item
     * @return mixed
     */
    public function getTax($item, $year)
    {
        if (!array_key_exists('tax' . $year, $this->data)) {
            if ($this->company->companyTaxationType->usn) {
                if ($this->company->companyTaxationType->usn_type == CompanyTaxationType::INCOME) {
                    $revenueData = current($this->data['revenue' . $year]);
                    foreach ($revenueData['amountTax'] as $key => $amount) {
                        $item['amount'][$key] = max(0, round($revenueData['amountTax'][$key] * $this->company->companyTaxationType->usn_percent / 100, 2));
                        $item['amountTax'][$key] = max(0, round($revenueData['amountTax'][$key] * $this->company->companyTaxationType->usn_percent / 100, 2));
                    }
                } else {
                    $profitBeforeTax = current($this->data['profitBeforeTax' . $year]);
                    foreach ($profitBeforeTax['amountTax'] as $key => $amount) {
                        $item['amount'][$key] = max(0, round($profitBeforeTax['amountTax'][$key] * $this->company->companyTaxationType->usn_percent / 100, 2));
                        $item['amountTax'][$key] = max(0, round($profitBeforeTax['amountTax'][$key] * $this->company->companyTaxationType->usn_percent / 100, 2));
                    }
                }
            } elseif ($this->company->companyTaxationType->osno) {
                $ebidta = current($this->data['ebitda' . $year]);
                foreach ($ebidta['amountTax'] as $key => $amount) {
                    $item['amount'][$key] = max(0, round($ebidta['amountTax'][$key] * 20 / 100, 2));
                    $item['amountTax'][$key] = max(0, round($ebidta['amountTax'][$key] * 20 / 100, 2));
                }
            }

            $this->data['tax' . $year] = $this->calculateQuarterAndTotalAmount(['tax' => $item]);
        }

        return $this->data['tax' . $year];
    }

    /**
     * @param $row
     * @param $key
     * @return string
     * @throws \Exception
     */
    public function renderRow($row, $key)
    {
        $currentQuarter = (int)ceil(date('m') / 3);
        $isCurrentYear = date('Y') == $this->year;
        $options = ArrayHelper::remove($row, 'options', []);
        $suffix = ArrayHelper::remove($row, 'suffix');
        $content = Html::beginTag('tr', $options);
        if (isset($row['addCheckboxX']) && $row['addCheckboxX']) {
            $label = CheckboxX::widget([
                    'name' => 'profit_and_loss-type',
                    'value' => true,
                    'options' => [
                        'class' => 'profit_and_loss-type',
                    ],
                    'pluginOptions' => [
                        'size' => 'xs',
                        'threeState' => false,
                        'inline' => false,
                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                    ],
                ]) . " {$row['label']}";
        } elseif (isset($row['canDrag']) && $row['canDrag']) {
            $label = ImageHelper::getThumb('img/menu-humburger.png', [15, 10], [
                    'class' => 'sortable-row-icon',
                    'style' => 'margin-right: 15px;',
                ]) . $row['label'];
        } else {
            $label = $row['label'];
        }
        $style = isset($options['style']) ? $options['style'] : null;
        $content .= Html::tag('td', $label, [
            'style' => 'width: 270px; ' . $style,
        ]);
        foreach (self::$month as $monthNumber => $monthName) {
            $quarter = ceil($monthNumber / 3);
            $val = $row['amount'][$monthNumber];
            $content .= Html::tag('td', number_format($val / 100, 2, ',', ' ') . $suffix, [
                'class' => "value-cell quarter-month-{$quarter} month-{$monthNumber} item-{$key}",
                'style' => 'width: 117px; display: ' . ($currentQuarter == $quarter && $isCurrentYear ? null : 'none'),
            ]);
            $quarter = (int)$monthNumber / 3;
            if (is_int($quarter)) {
                $val = $row['amount']['quarter-' . $quarter];
                $content .= Html::tag('td', number_format($val / 100, 2, ',', ' ') . $suffix, [
                    'class' => "value-cell quarter-{$quarter} item-{$key}",
                    'style' => 'width: 172px; display: ' . ($currentQuarter == $quarter && $isCurrentYear ? 'none' : null),
                ]);
            }
        }
        $val = $row['amount']['total'];
        $content .= Html::tag('td', number_format($val / 100, 2, ',', ' ') . $suffix, [
            'class' => "value-cell total-value item-{$key}",
        ]);
        $content .= Html::endTag('tr') . "\n";

        return $content;
    }

    public function getFromCurrentMonthsPeriods($left, $right, $offsetYear = 0, $offsetMonth = "0") {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month")->modify("{$offsetMonth} month");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr->format('Y-m-01'),
                'to' => $curr->format('Y-m-t'),
            ];
            $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        if (empty($this->_yearFilter)) {
            $range = [];
            $cashOrderMinDate = CashOrderFlows::find()
                ->byCompany($this->company->id)
                ->min('date');
            $cashBankMinDate = CashBankFlows::find()
                ->byCompany($this->company->id)
                ->min('date');
            $cashEmoneyMinDate = CashEmoneyFlows::find()
                ->byCompany($this->company->id)
                ->min('date');
            $minDates = [];

            if ($cashOrderMinDate) $minDates[] = $cashOrderMinDate;
            if ($cashBankMinDate) $minDates[] = $cashBankMinDate;
            if ($cashEmoneyMinDate) $minDates[] = $cashEmoneyMinDate;

            $minCashDate = !empty($minDates) ? min($minDates) : date(DateHelper::FORMAT_DATE);
            $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
            $currentYear = date('Y');
            foreach (range($registrationYear, $currentYear) as $value) {
                $range[$value] = $value;
            }
            arsort($range);
            $this->_yearFilter = $range;
            $this->_minCashYearMonth = DateHelper::format($minCashDate, 'Ym', DateHelper::FORMAT_DATE);
        }

        return $this->_yearFilter;
    }

    /**
     * @param $items
     * @return mixed
     */
    private function calculateQuarterAndTotalAmount($items)
    {
        // clear before calc
        foreach ($items as $ki => $data) {
            foreach (['amount', 'amountTax'] as $ka) {

                if (!isset($data[$ka])) {
                    continue;
                }

                foreach ($data[$ka] as $month => $oneAmount) {
                    if (in_array($month, ['quarter-1', 'quarter-2', 'quarter-3', 'quarter-4', 'total']))
                        unset($items[$ki][$ka][$month]);
                }
            }
        }

        foreach ($items as $ki => $data) {
            foreach (['amount', 'amountTax'] as $ka) {

                if (!isset($data[$ka])) {
                    continue;
                }

                foreach ($data[$ka] as $month => $oneAmount) {

                    if ((int)$month < 1 || (int)$month > 12)
                        continue;

                    if (!isset($items[$ki][$ka]['quarter-' . ceil($month / 3)])) {
                        $items[$ki][$ka]['quarter-' . ceil($month / 3)] = $oneAmount;
                    } else {
                        $items[$ki][$ka]['quarter-' . ceil($month / 3)] += $oneAmount;
                    }
                    if (!isset($items[$ki][$ka]['total'])) {
                        $items[$ki][$ka]['total'] = $oneAmount;
                    } else {
                        $items[$ki][$ka]['total'] += $oneAmount;
                    }
                }
            }
        }

        return $items;
    }

    private function _getNotAccountingContractors()
    {
        return Contractor::find()
            ->where([
                'company_id' => $this->company->id,
                'not_accounting' => true
            ])
            ->select('id')
            ->column();
    }

    private static function _getBlankParent($item)
    {
        foreach (self::$month as $monthNumber => $monthText) {
            $item['amount'][$monthNumber] = null;
            $item['amountTax'][$monthNumber] = null;
        }

        return $item;
    }

    private static function _getBlankItem($class, $name, $dataBlock = null, $dataArticle = null, $movementType = null)
    {
        return [
            'label' => htmlspecialchars($name),
            'options' => [
                'class' => 'hidden ' . $class,
                'style' => 'padding-left: 25px;'
            ],
            'data-block' => $dataBlock,
            'data-article' => $dataArticle,
            'data-movement-type' => $movementType,
            'amount' =>
                ['01' => 0, '02' => 0, '03' => 0, '04' => 0, '05' => 0, '06' => 0, '07' => 0, '08' => 0, '09' => 0, '10' => 0, '11' => 0, '12' => 0] +
                ['quarter-1' => 0, 'quarter-2' => 0, 'quarter-3' => 0, 'quarter-4' => 0] +
                ['total' => 0],
            'amountTax' =>
                ['01' => 0, '02' => 0, '03' => 0, '04' => 0, '05' => 0, '06' => 0, '07' => 0, '08' => 0, '09' => 0, '10' => 0, '11' => 0, '12' => 0] +
                ['quarter-1' => 0, 'quarter-2' => 0, 'quarter-3' => 0, 'quarter-4' => 0] +
                ['total' => 0]
        ];
    }

    public function getMinCashYearMonth()
    {
        if (empty($this->_yearFilter))
            $this->getYearFilter();

        return $this->_minCashYearMonth;
    }

    ////////////////////
    /// SEARCH ITEMS ///
    /// ////////////////

    private function getFlowClassNameByType($type)
    {
        switch ($type) {
            case self::CASH_BANK_BLOCK:
                $className = CashBankFlows::class;
                break;
            case self::CASH_ORDER_BLOCK:
                $className = CashOrderFlows::class;
                break;
            case self::CASH_EMONEY_BLOCK:
                $className = CashEmoneyFlows::class;
                break;
            default:
                throw new \Exception('Invalid type.');
                break;
        }

        return $className;
    }

    private function getDocClassNameByType($type)
    {
        switch ($type) {
            case self::ACT_BLOCK:
                $className = Act::class;
                break;
            case self::PACKING_LIST_BLOCK:
                $className = PackingList::class;
                break;
            case self::UPD_BLOCK:
                $className = Upd::class;
                break;
            default:
                throw new \Exception('Invalid type.');
                break;
        }

        return $className;
    }

    private function getFlowQuery($cashBlock, $companyID, $periodStart, $periodEnd)
    {
        /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
        $className = $this->getFlowClassNameByType($cashBlock);
        $tableName = $className::tableName();
        //$revenueIds = ($this->isRevenue) ? $this->_getRevenueFlowsIds($companyID, $className, $tableName, $periodStart, $periodEnd) : [];

        return $className::find()
            ->select([
                't.id',
                new Expression('"' . $tableName . '" as tb'),
                "IF({{t}}.[[flow_type]] = 0, SUM({{t}}.[[amount]]), 0) as amountExpense",
                "IF({{t}}.[[flow_type]] = 1, SUM({{t}}.[[amount]]), 0) as amountIncome",
                $cashBlock == self::CASH_BANK_BLOCK ? 't.rs as rs' : new Expression('null as rs'),
                $cashBlock == self::CASH_BANK_BLOCK ? new Expression("1 AS is_accounting") : "t.is_accounting",
                't.created_at',
                't.date',
                't.recognition_date',
                't.flow_type',
                't.amount',
                't.contractor_id',
                't.description',
                't.expenditure_item_id',
                't.income_item_id',
                Invoice::tableName() . '.document_number',
                Invoice::tableName() . '.document_additional_number',
                InvoiceIncomeItem::tableName() . '.name incomeItemName',
                InvoiceExpenditureItem::tableName() . '.name expenseItemName',
            ])
            ->from(['t' => $tableName])
            ->joinWith('invoices')
            ->joinWith('incomeReason')
            ->joinWith('expenditureReason')
            ->andWhere(['t.company_id' => $companyID])
            ->andWhere(['between', 't.recognition_date', $periodStart, $periodEnd])
            ->andWhere(['or',
                ['invoice.id' => null],
                ['or',
                    ['and',
                        ['not', ['invoice.id' => null]],
                        ['invoice.has_act' => 0],
                        ['invoice.has_packing_list' => 0],
                        ['invoice.has_upd' => 0],
                    ],
                    ['and',
                        ['not', ['invoice.id' => null]],
                        ['or',
                            ['invoice.need_act' => 0],
                            ['invoice.need_packing_list' => 0],
                            ['invoice.need_upd' => 0],
                        ]
                    ],
                ]
            ]);
    }

    private function getDocQuery($docBlock, $companyID, $periodStart, $periodEnd)
    {
        /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
        $className = $this->getDocClassNameByType($docBlock);
        $tableName = $className::tableName();

        $attrPrice = ($this->ioType == 2) ? 'selling_price_with_vat' : 'purchase_price_with_vat';

        return $className::find()
            ->select([
                't.id',
                new Expression('"' . $tableName . '" as tb'),
                new Expression("SUM(order.{$attrPrice} * order.quantity) amount"),

                't.created_at',
                't.document_date',
                't.document_number',
                't.document_additional_number',
                't.type',
                'invoice.id AS invoice_document_number',
                'invoice.contractor_id',
                'invoice.invoice_expenditure_item_id',
            ])
            ->from(['t' => $tableName])
            ->joinWith('invoice')
            ->joinWith('orders')
            ->andWhere(['invoice.company_id' => $companyID])
            ->andWhere(['between', 't.document_date', $periodStart, $periodEnd])
            ->groupBy('t.id');
    }

    public function searchItemsByDocs($periodStart, $periodEnd)
    {
        $this->load(\Yii::$app->request->get());

        if ($this->doc_type) {
            $query = (new Query)
                ->from(['t' => $this->getDocQuery($this->doc_type, $this->company->id, $periodStart, $periodEnd)]);
        } else {
            $ta = $this->getDocQuery(self::ACT_BLOCK, $this->company->id, $periodStart, $periodEnd);
            $tp = $this->getDocQuery(self::PACKING_LIST_BLOCK, $this->company->id, $periodStart, $periodEnd);
            $tu = $this->getDocQuery(self::UPD_BLOCK, $this->company->id, $periodStart, $periodEnd);
            $query = (new Query)
                ->from(['t' => $ta->union($tp, true)->union($tu, true)]);
        }

        $query->addSelect([
            't.id',
            't.tb',
            't.document_date',
            't.document_number',
            't.document_additional_number',
            't.type',
            't.amount',
            't.invoice_document_number',            
            't.contractor_id',
            't.invoice_expenditure_item_id'
        ]);
        $query->andWhere(['type' => $this->ioType]);
        $query->andFilterWhere(['t.contractor_id' => $this->contractor_id]);
        $query->groupBy('t.id');

        if (!empty($this->expenditure_item_id)) {
            $query->andWhere(['in', 'invoice_expenditure_item_id', $this->expenditure_item_id]);
        }

        if ($this->accountingOperationsOnly) {
            $notAccountingContractors = $this->_getNotAccountingContractors();
            $query->andFilterWhere(['not', ['t.contractor_id' => $notAccountingContractors]]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'amount',
                    'document_number' => [
                        'asc' => [
                            '`document_number` * 1' => SORT_ASC,
                            '`document_additional_number` * 1' => SORT_ASC,
                        ],
                        'desc' => [
                            '`document_number` * 1' => SORT_DESC,
                            '`document_additional_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'invoice_document_number' => [
                        'asc' => [
                            '`invoice_document_number` * 1' => SORT_ASC,
                            '`document_additional_number` * 1' => SORT_ASC,
                        ],
                        'desc' => [
                            '`invoice_document_number` * 1' => SORT_DESC,
                            '`document_additional_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'document_date' => [
                        'asc' => [
                            'document_date' => SORT_ASC,
                            'created_at' => SORT_ASC,
                        ],
                        'desc' => [
                            'document_date' => SORT_DESC,
                            'created_at' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                ],
                'defaultOrder' => [
                    'document_date' => SORT_DESC,
                ],
            ],
        ]);

        $this->_filterQuery = clone $query;

        return $dataProvider;
    }

    public function searchItemsByFlows($periodStart, $periodEnd)
    {
        $this->load(\Yii::$app->request->get());
        $this->checkCashContractor();

        // payment_type: 1-bank, 2-order, 3-emoney
        if ($this->payment_type) {
            $query = (new Query)
                ->from(['t' => $this->getFlowQuery($this->payment_type, $this->company->id, $periodStart, $periodEnd)->groupBy('t.id')]);
        } else {
            $cbf = $this->getFlowQuery(self::CASH_BANK_BLOCK, $this->company->id, $periodStart, $periodEnd)->groupBy('t.id');
            $cef = $this->getFlowQuery(self::CASH_EMONEY_BLOCK, $this->company->id, $periodStart, $periodEnd)->groupBy('t.id');
            $cof = $this->getFlowQuery(self::CASH_ORDER_BLOCK, $this->company->id, $periodStart, $periodEnd)->groupBy('t.id');
            $query = (new Query)
                ->from(['t' => $cbf->union($cef, true)->union($cof, true)]);
        }

        $query->addSelect([
            't.id',
            't.tb',
            'SUM(amountExpense) as amountExpense',
            'SUM(amountIncome) as amountIncome',
            't.rs',
            't.created_at',
            't.date',
            't.flow_type',
            't.amount',
            't.contractor_id',
            't.description',
            't.expenditure_item_id',
            't.income_item_id',
            't.document_number',
            't.document_additional_number',
            't.incomeItemName',
            't.expenseItemName',
            't.is_accounting'
        ]);
        $query->groupBy('t.id');

        $query->andFilterWhere(['t.contractor_id' => $this->contractor_id]);

        if ($this->accountingOperationsOnly) {
            $notAccountingContractors = $this->_getNotAccountingContractors();
            $query->andWhere(['or',
                ['t.tb' => ['cash_bank_flows']],
                ['and',
                    ['t.tb' => ['cash_emoney_flows', 'cash_order_flows']],
                    ['t.is_accounting' => 1],
                    ['not', ['t.contractor_id' => $notAccountingContractors]]
                ],
            ]);
        }

        if ($this->isRevenue) {
            $query->andWhere(['flow_type' => CashBankFlows::FLOW_TYPE_INCOME])
                ->andWhere(['not', ['in', 'income_item_id', [
                    InvoiceIncomeItem::ITEM_STARTING_BALANCE,
                    InvoiceIncomeItem::ITEM_RETURN,
                    InvoiceIncomeItem::ITEM_BORROWING_REDEMPTION,
                    InvoiceIncomeItem::ITEM_BUDGET_RETURN,
                    InvoiceIncomeItem::ITEM_LOAN,
                    InvoiceIncomeItem::ITEM_CREDIT,
                    InvoiceIncomeItem::ITEM_ENSURE_PAYMENT,
                    InvoiceIncomeItem::ITEM_FROM_FOUNDER,
                    InvoiceIncomeItem::ITEM_OWN_FOUNDS,
                    InvoiceIncomeItem::ITEM_OTHER,
                    InvoiceIncomeItem::ITEM_RECEIVED_PERCENTS,
                    InvoiceIncomeItem::ITEM_RECEIVED_PERCENTS_BY_DEPOSITS
            ]]]);
        } else if (!empty($this->income_item_id) && !empty($this->expenditure_item_id)) {
            $query->andWhere(['or',
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_EXPENSE],
                    ['in', 'expenditure_item_id', $this->expenditure_item_id],
                ],
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_INCOME],
                    ['in', 'income_item_id', $this->income_item_id],
                ],
                $this->cash_contractor ? ['in', 'contractor_id', $this->cash_contractor] : [],
            ]);
        } elseif (!empty($this->income_item_id)) {
            $query->andWhere(['or',
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_INCOME],
                    ['in', 'income_item_id', $this->income_item_id],
                ],
                $this->cash_contractor ? ['in', 'contractor_id', $this->cash_contractor] : [],
            ]);
        } elseif (!empty($this->expenditure_item_id)) {
            $query->andWhere(['or',
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_EXPENSE],
                    ['in', 'expenditure_item_id', $this->expenditure_item_id],
                ],
                $this->cash_contractor ? ['in', 'contractor_id', $this->cash_contractor] : [],
            ]);
        } else {
            if ($this->cash_contractor) {
                $query->andWhere(['in', 'contractor_id', $this->cash_contractor]);
            } else {
                // $query->andWhere(['flow_type' => null]);
            }
        }

        if ($this->reason_id == 'empty') {
            $query->andWhere(['or',
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_EXPENSE],
                    ['expenditure_item_id' => null],
                ],
                ['and',
                    ['flow_type' => CashBankFlows::FLOW_TYPE_INCOME],
                    ['income_item_id' => null],
                ],
            ]);
        } elseif (!empty($this->reason_id)) {
            $this->expenditure_item_id = $this->income_item_id = null;
            if (substr($this->reason_id, 0, 2) === 'e_') {
                $this->expenditure_item_id = substr($this->reason_id, 2);
            } elseif (substr($this->reason_id, 0, 2) === 'i_') {
                $this->income_item_id = substr($this->reason_id, 2);
            }
            $query->andFilterWhere([
                'expenditure_item_id' => $this->expenditure_item_id,
                'income_item_id' => $this->income_item_id,
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'date' => [
                        'asc' => [
                            'date' => SORT_ASC,
                            'created_at' => SORT_ASC,
                        ],
                        'desc' => [
                            'date' => SORT_DESC,
                            'created_at' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'recognition_date' => [
                        'asc' => [
                            'recognition_date' => SORT_ASC,
                            'created_at' => SORT_ASC,
                        ],
                        'desc' => [
                            'recognition_date' => SORT_DESC,
                            'created_at' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'amountIncome' => [
                        'asc' => [
                            'flow_type' => SORT_DESC,
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_DESC,
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'amountExpense' => [
                        'asc' => [
                            'flow_type' => SORT_ASC,
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_ASC,
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'contractor_id',
                    'description',
                    'created_at',
                    'billPaying' => [
                        'asc' => ['document_number' => SORT_ASC, 'document_additional_number' => SORT_ASC],
                        'desc' => ['document_number' => SORT_DESC, 'document_additional_number' => SORT_ASC],
                        'default' => SORT_ASC
                    ],
                ],
                'defaultOrder' => [
                    'recognition_date' => SORT_DESC,
                ],
            ],
        ]);

        $this->_filterQuery = clone $query;

        return $dataProvider;
    }

    public function generateXls($type)
    {
        $data = $this->handleItems();
        ProfitAndLossCompanyItem::loadCompanyItems();

        // header
        $columns = [];
        $columns[] = [
            'attribute' => 'itemName',
            'header' => 'Статьи',
        ];
        foreach (AbstractFinance::$month as $monthNumber => $monthText) {
            $columns[] = [
                'attribute' => "item{$monthNumber}",
                'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                'header' => $monthText,
            ];
        }
        $columns[] = [
            'attribute' => 'dataYear',
            'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
            'header' => $this->year,
        ];

        // data
        $formattedData = [];
        foreach ($data as $row) {
            $formattedData[] = $this->_buildXlsRow($row, $type);
        }

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $formattedData,
            'title' => "Баланс за {$this->year} год",
            'rangeHeader' => range('A', 'N'),
            'columns' => $columns,
            'format' => 'Xlsx',
            'fileName' => "Баланс за {$this->year} год",
        ]);
    }

    protected function _buildXlsRow($data, $type)
    {
        $amountKey = ($type == self::TAB_ACCOUNTING_OPERATIONS_ONLY) ? 'amountTax' : 'amount';
        $row = [
            'itemName' => trim(strip_tags($data['label'] ?? '')),
            'dataYear' => ($data[$amountKey]['total'] ?? '0.00') / 100
        ];
        foreach (AbstractFinance::$month as $monthNumber => $monthText) {
            $row['item' . $monthNumber] = ($data[$amountKey][$monthNumber] ?? '0.00') / 100;
        }

        return $row;
    }
}