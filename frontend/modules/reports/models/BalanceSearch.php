<?php

namespace frontend\modules\reports\models;

use common\components\date\DateHelper;
use common\components\excel\Excel;
use common\components\helpers\Month;
use common\components\ImageHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\document\status\InvoiceStatus;
use common\models\product\Product;
use common\models\product\ProductInitialBalance;
use common\models\product\ProductSearch;
use common\models\product\ProductStore;
use frontend\models\Documents;
use kartik\checkbox\CheckboxX;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class BalanceSearch
 * @package frontend\modules\reports\models
 *
 */
class BalanceSearch extends Model
{
    const TAB_BALANCE = 9;

    const TYPE_ASSETS = 1;
    const TYPE_PASSIVE = 2;

    protected $data = [];
    protected $_company;
    protected $_year;
    protected $_months;

    /**
     * @var array
     */
    protected $_profitAndLossModel;
    protected $_tempUndestributedProfit = [];

    public function init()
    {
        // need for calc "Undestributed profit" rows
        $this->_loadProfitAndLossData();
        $this->_year = \Yii::$app->session->get('modules.reports.finance.year', date('Y'));

        return parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '' => '',
        ];
    }

    /**
     * @inheritdoc
     */
    public function setCompany(Company $company)
    {
        $this->_company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        if ($this->_company === null) {
            throw new InvalidConfigException('The "company" property is not set.');
        }

        return $this->_company;
    }

    /**
     * @inheritdoc
     */
    public function setYear($value)
    {
        $years = $this->getYearFilter();
        if (!in_array($value, $years)) {
            $value = date('Y');
        }

        $this->_year = $value;
    }

    /**
     * @inheritdoc
     */
    public function getYear()
    {
        return $this->_year ?: date('Y');
    }

    /**
     * @inheritdoc
     */
    public function handleItems($itemsArray)
    {
        $result = [];
        foreach ($itemsArray as $item) {
            $getter = ArrayHelper::remove($item, 'getter');
            $items = ArrayHelper::remove($item, 'items');
            foreach ($this->months as $date) {
                $item['data'][$date->format('n')] = $this->$getter($date);
            }
            $existsMonthCount = count($item['data']);
            if ($existsMonthCount < 12) {
                for ($i = 1; $i <= 12 - $existsMonthCount; $i++) {
                    $item['data'][$existsMonthCount + $i] = null;
                }
            }
            $quarterAmount = null;
            $totalAmount = null;
            $newItemData = [];
            foreach ($item['data'] as $key => $oneAmount) {
                $newItemData[$key] = $oneAmount;
                if ($key > date('n') && $this->year == date('Y')) {
                    $totalAmount = $item['data'][date('n')];
                } else {
                    $totalAmount = $oneAmount;
                }
                if ($key > date('n') && ceil($key / 3) == ceil(date('n') / 3) && $this->year == date('Y')) {
                    $quarterAmount = $item['data'][date('n')];
                } else {
                    $quarterAmount = $oneAmount;
                }
                if (is_int($key / 3)) {
                    $newItemData['quarter-' . ($key / 3)] = $quarterAmount;
                    $quarterAmount = null;
                }
            }
            $newItemData['total'] = $totalAmount;
            $item['data'] = $newItemData;
            $item['items'] = $items ? $this->handleItems($items) : null;
            $result[] = $item;
        }

        return $result;
    }

    /**
     * @return Company
     */
    public function getMonths()
    {
        if ($this->_months === null) {
            $this->_months = [];
            $currentYear = date('Y');
            $toMonth = $this->year == $currentYear ? date('n') : 12;
            $date = date_create("{$this->year}-01-01");
            $date->modify('last day of this month')->setTime(23, 59, 59);

            $this->_months[] = clone $date;
            while ($date->format('n') != $toMonth) {
                $date->modify('last day of +1 month');
                $this->_months[] = $this->year == $currentYear && $date->format('n') == date('n') ? date_create() : clone $date;
            }
        }

        $result = [];
        foreach ($this->_months as $m) {
            $result[] = clone $m;
        }

        return $result;
    }

    /**
     * @param $params
     * @return array
     */
    public function search($params)
    {
        $this->_load($params, '');

        $result = $this->handleItems($this->buildItems());

        return $result;
    }

    protected function _loadProfitAndLossData()
    {
        $this->_profitAndLossModel = new ProfitAndLossSearchModel();
        $this->_profitAndLossModel->year = $this->year;
        $this->_profitAndLossModel->handleItems();
    }

    /**
     * @return integer|null
     */
    public function getAssets($date)
    {
        $key = 'getAssets' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getFixedAssets(clone $date) +
                $this->getCurrentAssets(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getFixedAssets($date)
    {
        $key = 'getFixedAssets' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getRealEstateObjects(clone $date) +
                $this->getEquipmentAssets(clone $date) +
                $this->getTransportAssets(clone $date) +
                $this->getIntangibleAssets(clone $date) +
                $this->getOtherAssets(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getRealEstateObjects($date)
    {
        return $this->getBalanceItemAmount(BalanceItem::ITEM_REAL_ESTATE_OBJECT, $date);
    }

    /**
     * @return integer|null
     */
    public function getEquipmentAssets($date)
    {
        return $this->getBalanceItemAmount(BalanceItem::ITEM_EQUIPMENT_ASSETS, $date);
    }

    /**
     * @return integer|null
     */
    public function getTransportAssets($date)
    {
        return $this->getBalanceItemAmount(BalanceItem::ITEM_TRANSPORT_ASSETS, $date);
    }

    /**
     * @param $date
     * @return null|string
     */
    public function getIntangibleAssets($date)
    {
        return $this->getBalanceItemAmount(BalanceItem::ITEM_INTANGIBLE_ASSETS, $date);
    }

    /**
     * @return integer|null
     */
    public function getOtherAssets($date)
    {
        return $this->getBalanceItemAmount(BalanceItem::ITEM_OTHER_ASSETS, $date);
    }

    /**
     * @return integer|null
     */
    public function getCurrentAssets($date)
    {
        $key = 'getCurrentAssets' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getStocks(clone $date) +
                $this->getReceivables(clone $date) +
                $this->getCash(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getCapitalUndestributedProfits($date)
    {
        $key = 'getCapitalUndestributedProfits' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {

            if ($this->_profitAndLossModel instanceof ProfitAndLossSearchModel) {
                $year = $date->format('Y');
                $month = $date->format('m');

                if ($row = $this->_profitAndLossModel->getRow($year, 'undestributedProfits'))
                    return $row[$month] ?? null;
            }

            return null;

            //$this->data[$key] = $this->getUndestributedProfits(clone $date) +
            //    $this->getUndestributedProfitsPrev(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getStocks($date)
    {
        $key = 'getStocks' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {

            //$this->data[$key] = $this->company->getProducts()->alias('product')
            //    ->byUser()
            //    ->andWhere(['product.not_for_sale' => false])
            //    ->andWhere(['product.production_type' => Product::PRODUCTION_TYPE_GOODS])
            //    ->andWhere(['product.is_deleted' => false])
            //    ->andWhere(['and',
            //        ['not', ['product.status' => Product::DELETED]],
            //        ['not', ['product.status' => Product::ARCHIVE]],
            //    ])
            //    ->andWhere(['<=', 'pib.date', $date->format('Y-m-d')])
            //    ->leftJoin(['pib' => ProductInitialBalance::tableName()], "{{product}}.[[id]] = {{pib}}.[[product_id]]")
            //    ->leftJoin(['r' => Product::reserveQuery($this->company->id)], '{{product}}.[[id]] = {{r}}.[[product_id]]')
            //    ->leftJoin(['s' => ProductStore::find()
            //        ->select(['product_id', 'total' => 'SUM([[quantity]])'])
            //        ->andWhere(['store_id' => $this->company->getStores()->select('id')->column(\Yii::$app->db2)])
            //        ->groupBy('product_id')], '{{product}}.[[id]] = {{s}}.[[product_id]]')
            //    ->sum('IFNULL({{product}}.[[price_for_buy_with_nds]], 0) * IFNULL({{s}}.[[total]], 0)', Yii::$app->db2);

            $searchModel = new ProductSearch([
                'company_id' => $this->company->id,
                'production_type' => Product::PRODUCTION_TYPE_GOODS,
                'status' => Product::ACTIVE,
                'turnoverType' => ProductSearch::TURNOVER_BY_AMOUNT,
            ]);
            $searchModel->dateStart = $searchModel->dateEnd = $date;
            $this->data[$key] = $searchModel->balanceAtDate(false, $searchModel->baseQuery()->select(['product.id'])->column(), false);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getReceivables($date)
    {
        $key = 'getReceivables' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getTradeReceivables(clone $date) +
                $this->getPrepaymentsToSuppliers(clone $date) +
                $this->getBudgetOverpayment(clone $date) +
                $this->getLoansIssued(clone $date) +
                $this->getOtherInvestments(clone $date) +
                $this->getRentalDeposit(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getTradeReceivables($date)
    {
        $key = 'getTradeReceivables' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {
            $query = $this->company->getInvoices()->alias('invoice')
                ->andWhere(['invoice.is_deleted' => false])
                ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
                ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_PAYED]])
                ->andWhere(['invoice.type' => Documents::IO_TYPE_OUT])
                ->andWhere(['or',
                    ['or',
                        ['invoice.has_act' => true],
                        ['invoice.has_packing_list' => true],
                        ['invoice.has_upd' => true]
                    ],
                    ['or',
                        ['invoice.need_act' => false],
                        ['invoice.need_packing_list' => false],
                        ['invoice.need_upd' => false]
                    ],
                ]);

            if ($date->format('Ym') != date('Ym'))
                $query->andWhere(['<=', 'invoice.payment_limit_date', $date->format('Y-m-d')]);

            $this->data[$key] = $query->sum('{{invoice}}.[[total_amount_with_nds]] - IFNULL({{invoice}}.[[payment_partial_amount]], 0)');
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getPrepaymentsToSuppliers($date)
    {
        $key = 'getPrepaymentsToSuppliers' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getPrepaidAmount($date, Documents::IO_TYPE_IN, CashFlowsBase::FLOW_TYPE_EXPENSE);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getBudgetOverpayment($date)
    {
        return $this->getBalanceItemAmount(BalanceItem::ITEM_BUDGET_OVERPAYMENT, $date);
    }

    /**
     * @return integer|null
     */
    public function getLoansIssued($date)
    {
        $key = 'getLoansIssued' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {
            $select = ['amount'];
            $where1 = [
                'and',
                ['flow.company_id' => $this->company->id],
                ['flow.flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE],
                ['flow.expenditure_item_id' => InvoiceExpenditureItem::ITEM_BORROWING_EXTRADITION],
                ['<=', 'flow.date', $date->format('Y-m-d')],
            ];
            $where2 = [
                'and',
                ['flow.company_id' => $this->company->id],
                ['flow.flow_type' => CashFlowsBase::FLOW_TYPE_INCOME],
                ['flow.income_item_id' => InvoiceIncomeItem::ITEM_BORROWING_REDEMPTION],
                ['<=', 'flow.date', $date->format('Y-m-d')],
            ];
            $sum1 = CashFlowsBase::getAllFlows($select, $where1)->sum('amount');
            $sum2 = CashFlowsBase::getAllFlows($select, $where2)->sum('amount');

            $this->data[$key] = max(0, $sum1 - $sum2);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getOtherInvestments($date)
    {
        return $this->getBalanceItemAmount(BalanceItem::ITEM_OTHER_INVESTMENTS, $date);
    }

    /**
     * @return integer|null
     */
    public function getCash($date)
    {
        $key = 'getCash' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getBankAccounts(clone $date) +
                $this->getCashbox(clone $date) +
                $this->getEmoney(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getBankAccounts($date)
    {
        $key = 'getBankAccounts' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {
            $d = $date->format('Y-m-d');
            $in = $this->company->getCashBankFlows()->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
                ->andWhere(['<=', 'date', $d])->sum('amount');
            $out = $this->company->getCashBankFlows()->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
                ->andWhere(['<=', 'date', $d])->sum('amount');
            $this->data[$key] = $in - $out;
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getCashbox($date)
    {
        $key = 'getCashbox' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {
            $d = $date->format('Y-m-d');
            $in = $this->company->getCashOrderFlows()->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
                ->andWhere(['<=', 'date', $d])->sum('amount');
            $out = $this->company->getCashOrderFlows()->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
                ->andWhere(['<=', 'date', $d])->sum('amount');
            $this->data[$key] = $in - $out;
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getEmoney($date)
    {
        $key = 'getEmoney' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {
            $d = $date->format('Y-m-d');
            $in = $this->company->getCashEmoneyFlows()->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
                ->andWhere(['<=', 'date', $d])->sum('amount');
            $out = $this->company->getCashEmoneyFlows()->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
                ->andWhere(['<=', 'date', $d])->sum('amount');
            $this->data[$key] = $in - $out;
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getLiabilities($date)
    {
        $key = 'getLiabilities' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getCapital(clone $date) +
                $this->getLongtermDuties(clone $date) +
                $this->getShorttermLiabilities(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getCapital($date)
    {
        $key = 'getCapital' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getAuthorizedCapital(clone $date) +
                $this->getCapitalUndestributedProfits(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getAuthorizedCapital($date)
    {
        return $this->getBalanceItemAmount(BalanceItem::ITEM_AUTHORIZED_CAPITAL, $date) > 0 ?
            $this->getBalanceItemAmount(BalanceItem::ITEM_AUTHORIZED_CAPITAL, $date) :
            $this->company->capital;
    }

    /**
     * @return integer|null
     */
    public function getUndestributedProfits($date)
    {
        if ($this->_profitAndLossModel instanceof ProfitAndLossSearchModel) {

            $year = $date->format('Y');
            $month = $date->format('m');

            $netIncomeLoss = $this->_profitAndLossModel->getValue('netIncomeLoss', $year, $month);
            $paymentDividend = $this->_profitAndLossModel->getValue('paymentDividend', $year, $month);

            if ($month > 1) {
                $prevMonth = str_pad($month - 1, 2, "0", STR_PAD_LEFT);
                $prevUndestributedProfit = $this->_tempUndestributedProfit[$prevMonth];
            } else {
                $prevUndestributedProfit = 0;
            }

            $undestributedProfit = ($netIncomeLoss - $paymentDividend) + $prevUndestributedProfit;

            $this->_tempUndestributedProfit[$month] = $undestributedProfit;

            return $undestributedProfit;
        }

        return null;
    }

    /**
     * @return integer|null
     */
    public function getUndestributedProfitsPrev($date)
    {
        return $this->getCapitalUndestributedProfits($date) - $this->getUndestributedProfits($date);
    }

    /**
     * @return integer|null
     */
    public function getLongtermDuties($date)
    {
        $key = 'getLongtermDuties' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getLongtermLoans(clone $date) +
                $this->getAccountsPayable(clone $date) +
                $this->getTargetedFinancing(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getLongtermLoans($date)
    {
        return null;
    }

    /**
     * @return integer|null
     */
    public function getAccountsPayable($date)
    {
        return null;
    }

    /**
     * @return integer|null
     */
    public function getTargetedFinancing($date)
    {
        return null;
    }

    /**
     * @return integer|null
     */
    public function getShorttermLiabilities($date)
    {
        $key = 'getShorttermLiabilities' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getShorttermBorrowings(clone $date) +
                $this->getShorttermAccountsPayable(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @param $date
     * @return mixed
     */
    public function getBalanceCheck($date)
    {
        $key = 'getBalanceCheck' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getAssets(clone $date) - $this->getLiabilities(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getShorttermBorrowings($date)
    {
        $key = 'getShorttermBorrowings' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {
            $select = ['amount'];
            $where1 = [
                'and',
                ['flow.company_id' => $this->company->id],
                ['flow.flow_type' => CashFlowsBase::FLOW_TYPE_INCOME],
                ['in', 'flow.income_item_id', [InvoiceIncomeItem::ITEM_LOAN, InvoiceIncomeItem::ITEM_CREDIT]],
                ['<=', 'flow.date', $date->format('Y-m-d')],
            ];
            $where2 = [
                'and',
                ['flow.company_id' => $this->company->id],
                ['flow.flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE],
                ['in', 'flow.expenditure_item_id', [InvoiceExpenditureItem::ITEM_REPAYMENT_CREDIT, InvoiceExpenditureItem::ITEM_LOAN_REPAYMENT]],
                ['<=', 'flow.date', $date->format('Y-m-d')],
            ];
            $sum1 = CashFlowsBase::getAllFlows($select, $where1)->sum('amount');
            $sum2 = CashFlowsBase::getAllFlows($select, $where2)->sum('amount');

            $this->data[$key] = max(0, $sum1 - $sum2);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getShorttermAccountsPayable($date)
    {
        $key = 'getShorttermAccountsPayable' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getVendorInvoicesPayable(clone $date) +
                $this->getPrepaymentOfCustomers(clone $date) +
                $this->getCommodityLoans(clone $date) +
                $this->getRentalArrears(clone $date) +
                $this->getSalaryArrears(clone $date) +
                $this->getBudgetArrears(clone $date);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getVendorInvoicesPayable($date)
    {
        $key = 'getVendorInvoicesPayable' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {

            //$this->data[$key] = $this->company->getInvoices()->alias('invoice')
            //    ->andWhere(['invoice.is_deleted' => false])
            //    ->andWhere(['invoice.type' => Documents::IO_TYPE_IN])
            //    ->andWhere(['in', 'invoice.invoice_status_id', [
            //        InvoiceStatus::STATUS_CREATED,
            //        InvoiceStatus::STATUS_OVERDUE,
            //        InvoiceStatus::STATUS_PAYED_PARTIAL,
            //    ],])
            //    ->andWhere(['<=', 'invoice.document_date', $date->format('Y-m-d')])
            //    ->sum('{{invoice}}.[[total_amount_with_nds]] - IFNULL({{invoice}}.[[payment_partial_amount]], 0)');

            $query = $this->company->getInvoices()->alias('invoice')
                ->andWhere(['invoice.is_deleted' => false])
                ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
                ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_PAYED]])
                ->andWhere(['invoice.type' => Documents::IO_TYPE_IN])
                ->andWhere(['or',
                    ['or',
                        ['invoice.has_act' => true],
                        ['invoice.has_packing_list' => true],
                        ['invoice.has_upd' => true]
                    ],
                    ['or',
                        ['invoice.need_act' => false],
                        ['invoice.need_packing_list' => false],
                        ['invoice.need_upd' => false]
                    ],
                ]);

            if ($date->format('Ym') != date('Ym'))
                $query->andWhere(['<=', 'invoice.payment_limit_date', $date->format('Y-m-d')]);

            $this->data[$key] = $query->sum('{{invoice}}.[[total_amount_with_nds]] - IFNULL({{invoice}}.[[payment_partial_amount]], 0)');
        }

        return $this->data[$key];
    }


    /**
     * @param $date
     * @return mixed
     */
    public function getRentalDeposit($date)
    {
        $key = 'getRentalDeposit' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {
            $select = ['amount'];
            $where1 = [
                'and',
                ['flow.company_id' => $this->company->id],
                ['flow.flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE],
                ['flow.expenditure_item_id' => InvoiceExpenditureItem::ITEM_ENSURE_PAYMENT],
                ['<=', 'flow.date', $date->format('Y-m-d')],
            ];
            $where2 = [
                'and',
                ['flow.company_id' => $this->company->id],
                ['flow.flow_type' => CashFlowsBase::FLOW_TYPE_INCOME],
                ['flow.income_item_id' => InvoiceIncomeItem::ITEM_ENSURE_PAYMENT],
                ['<=', 'flow.date', $date->format('Y-m-d')],
            ];
            $sum1 = CashFlowsBase::getAllFlows($select, $where1)->sum('amount');
            $sum2 = CashFlowsBase::getAllFlows($select, $where2)->sum('amount');

            $this->data[$key] = max(0, $sum1 - $sum2);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getPrepaymentOfCustomers($date)
    {
        $key = 'getPrepaymentOfCustomers' . $date->format('Ym');
        if (!array_key_exists($key, $this->data)) {
            $this->data[$key] = $this->getPrepaidAmount($date, Documents::IO_TYPE_OUT, CashFlowsBase::FLOW_TYPE_INCOME);
        }

        return $this->data[$key];
    }

    /**
     * @return integer|null
     */
    public function getCommodityLoans($date)
    {
        return null;
    }

    /**
     * @return integer|null
     */
    public function getRentalArrears($date)
    {
        return null;
    }

    /**
     * @return integer|null
     */
    public function getSalaryArrears($date)
    {
        return null;
    }

    /**
     * @return integer|null
     */
    public function getBudgetArrears($date)
    {
        return $this->getBalanceItemAmount(BalanceItem::ITEM_BUDGET_ARREARS, $date);
    }

    public function getIncompleteProjects($date) {
        return 0;
    }

    /**
     * @param $row
     * @return string
     * @throws \Exception
     */
    public function renderRow($row)
    {
        $currentQuarter = (int)ceil(date('m') / 3);
        $isCurrentYear = date('Y') == $this->year;

        $options = ArrayHelper::remove($row, 'options', []);
        $skipEmpty = ArrayHelper::remove($row, 'skipEmpty', false);
        $canUpdate = ArrayHelper::remove($row, 'canUpdate', false);
        if ($skipEmpty && array_sum($row['data']) === 0) {
            Html::addCssClass($options, 'hidden');
        }
        $content = Html::beginTag('tr', $options);
        $label = null;
        if (isset($row['addCheckboxX']) && $row['addCheckboxX']) {
            $label = CheckboxX::widget([
                    'name' => 'balance-type',
                    'value' => true,
                    'options' => [
                        'class' => 'balance-type',
                    ],
                    'pluginOptions' => [
                        'size' => 'xs',
                        'threeState' => false,
                        'inline' => false,
                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                    ],
                ]) . $row['label'];
        } else {
            if (stristr($row['label'], 'Активы') === false && stristr($row['label'], 'Пассивы') === false &&
                stristr($row['label'], 'АКТИВ - ПАССИВ') === false) {
                $label = ImageHelper::getThumb('img/menu-humburger.png', [15, 10], [
                    'class' => 'sortable-row-icon',
                    'style' => 'margin-right: 15px;',
                ]);
            }
            $label .= $row['label'];
            if ($canUpdate) {
                $label .= '<span class="glyphicon glyphicon-pencil pull-right update-balance-item" style="cursor: pointer; display:inline-block; color:#5b9bd1"></span>';
            }
        }
        $style = isset($options['style']) ? $options['style'] : null;
        $content .= Html::tag('td', $label, [
            'style' => 'width: 270px; ' . $style,
        ]);
        foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthName) {
            $quarter = ceil($monthNumber / 3);
            $val = $row['data'][(int)$monthNumber];
            $content .= Html::tag('td', number_format($val / 100, 2, ',', ' '), [
                'class' => 'value-cell quarter-month-' . $quarter,
                'style' => 'width: 117px; display: ' . ($currentQuarter == $quarter && $isCurrentYear ? null : 'none'),
            ]);
            $quarter = (int)$monthNumber / 3;
            if (is_int($quarter)) {
                $val = $row['data']['quarter-' . $quarter];
                $content .= Html::tag('td', number_format($val / 100, 2, ',', ' '), [
                    'class' => 'value-cell quarter-' . $quarter,
                    'style' => 'width: 172px; display: ' . ($currentQuarter == $quarter && $isCurrentYear ? 'none' : null),
                ]);
            }
        }
        $val = $row['data']['total'];
        $content .= Html::tag('td', number_format($val / 100, 2, ',', ' '), [
            'class' => 'value-cell total-value',
        ]);
        $content .= Html::endTag('tr') . "\n";
        if (isset($row['items'])) {
            foreach ($row['items'] as $item) {
                $content .= $this->renderRow($item);
            }
        }

        return $content;
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $range = [];
        $cashOrderMinDate = CashOrderFlows::find()
            ->byCompany($company->id)
            ->min('date');
        $cashBankMinDate = CashBankFlows::find()
            ->byCompany($company->id)
            ->min('date');
        $cashEmoneyMinDate = CashEmoneyFlows::find()
            ->byCompany($company->id)
            ->min('date');
        $minDates = [];

        if ($cashOrderMinDate) $minDates[] = $cashOrderMinDate;
        if ($cashBankMinDate) $minDates[] = $cashBankMinDate;
        if ($cashEmoneyMinDate) $minDates[] = $cashEmoneyMinDate;

        $minCashDate = !empty($minDates) ? min($minDates) : date(DateHelper::FORMAT_DATE);
        $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
        $currentYear = date('Y');
        foreach (range($registrationYear, $currentYear) as $value) {
            $range[$value] = $value;
        }
        arsort($range);

        return $range;
    }

    //public function getYearFilter()
    //{
    //    $result = [];
    //    foreach (array_reverse(range(BalanceSearch::$minYear, date('Y'))) as $year) {
    //        $result[$year] = $year;
    //    }
//
    //    return $result;
    //}

    /**
     * @return array
     */
    public function buildItems()
    {
        $fixedAssetItems = $this->getFormattedItems([
            BalanceItem::ITEM_REAL_ESTATE_OBJECT,
            BalanceItem::ITEM_EQUIPMENT_ASSETS,
            BalanceItem::ITEM_TRANSPORT_ASSETS,
            BalanceItem::ITEM_INTANGIBLE_ASSETS,
            BalanceItem::ITEM_OTHER_ASSETS,
        ], [
            'class' => 'hidden fixed-assets',
        ]);
        $receivablesItems = $this->getFormattedItems([
            BalanceItem::ITEM_TRADE_RECEIVABLES,
            BalanceItem::ITEM_PREPAYMENTS_TO_SUPPLIERS,
            BalanceItem::ITEM_BUDGET_OVERPAYMENT,
            BalanceItem::ITEM_LOANS_ISSUED,
            BalanceItem::ITEM_OTHER_INVESTMENTS,
            BalanceItem::ITEM_RENTAL_DEPOSIT,
        ], [
            'class' => 'hidden receivable',
            'style' => 'padding-left: 25px;',
        ]);
        $cashItems = $this->getFormattedItems([
            BalanceItem::ITEM_BANK_ACCOUNTS,
            BalanceItem::ITEM_CASHBOX,
            BalanceItem::ITEM_EMONEY,
        ], [
            'class' => 'hidden cash',
            'style' => 'padding-left: 25px;',
        ]);
        $capitalUndestributedProfitsItems = $this->getFormattedItems([
            BalanceItem::ITEM_UNDESTRIBUTED_PROFITS,
            BalanceItem::ITEM_UNDESTRIBUTED_PROFITS_PREV,
        ], [
            'class' => 'hidden capital-undestributed-profits',
            'style' => 'padding-left: 25px;',
        ]);
        $shortTermAccountsPayableItems = $this->getFormattedItems([
            BalanceItem::ITEM_VENDOR_INVOICES_PAYABLE,
            BalanceItem::ITEM_PREPAYMENT_OF_CUSTOMERS,
            BalanceItem::ITEM_SALARY_ARREARS,
            BalanceItem::ITEM_BUDGET_ARREARS,
        ], [
            'class' => 'hidden shorttermAccountsPayable',
            'style' => 'padding-left: 25px;',
        ]);

        return [
            [
                'label' => 'АКТИВЫ ' .
                    '<span class="tooltip-balance ico-question valign-middle" data-tooltip-content="#tooltip-balance-assets"></span>',
                'getter' => 'getAssets',
                'options' => [
                    'class' => 'assets-row text-bold',
                ],
                'items' => [
                    [
                        'label' => 'Основные средства',
                        'addCheckboxX' => true,
                        'getter' => 'getFixedAssets',
                        'options' => [
                            'class' => 'text-bold',
                            'data-children' => 'fixed-assets',
                        ],
                        'items' => $fixedAssetItems,
                    ],
                    [
                        'label' => 'Оборотные активы',
                        'addCheckboxX' => true,
                        'getter' => 'getCurrentAssets',
                        'options' => [
                            'class' => 'text-bold',
                            'data-children' => 'current-assets',
                        ],
                        'items' => [
                            [
                                'label' => BalanceItem::findOne(BalanceItem::ITEM_STOCKS)->name,
                                'options' => [
                                    'class' => 'hidden current-assets',
                                ],
                                'getter' => BalanceItem::$itemGetter[BalanceItem::ITEM_STOCKS],
                            ],
                            [
                                'label' => BalanceItem::findOne(BalanceItem::ITEM_INCOMPLETE_PROJECTS)->name,
                                'options' => [
                                    'class' => 'hidden current-assets',
                                ],
                                'getter' => BalanceItem::$itemGetter[BalanceItem::ITEM_INCOMPLETE_PROJECTS],
                            ],
                            [
                                'label' => 'Дебиторская задолженность',
                                'addCheckboxX' => true,
                                'getter' => 'getReceivables',
                                'options' => [
                                    'class' => 'hidden text-bold',
                                    'data-children' => 'receivable',
                                    'style' => 'padding-left: 25px;',
                                ],
                                'items' => $receivablesItems,
                            ],
                            [
                                'label' => 'Денежные средства',
                                'addCheckboxX' => true,
                                'getter' => 'getCash',
                                'options' => [
                                    'class' => 'hidden text-bold',
                                    'data-children' => 'cash',
                                    'style' => 'padding-left: 25px;',
                                ],
                                'items' => $cashItems,
                            ],
                        ],
                    ],
                ],
            ],
            [
                'label' => 'ПАССИВЫ ' .
                    '<span class="tooltip-balance ico-question valign-middle" data-tooltip-content="#tooltip-balance-liabilities"></span>',
                'getter' => 'getLiabilities',
                'options' => [
                    'class' => 'liabilities-row text-bold',
                ],
                'items' => [
                    [
                        'label' => 'Собственный капитал',
                        'addCheckboxX' => true,
                        'getter' => 'getCapital',
                        'options' => [
                            'class' => 'text-bold',
                            'data-children' => 'capital',
                        ],
                        'items' => [
                            [
                                'label' => BalanceItem::findOne(BalanceItem::ITEM_AUTHORIZED_CAPITAL)->name,
                                'options' => [
                                    'class' => 'hidden capital',
                                ],
                                'getter' => BalanceItem::$itemGetter[BalanceItem::ITEM_AUTHORIZED_CAPITAL],
                            ],
                            [
                                'label' => 'Нераспределенная прибыль',
                                'addCheckboxX' => true,
                                'getter' => 'getCapitalUndestributedProfits',
                                'options' => [
                                    'class' => 'hidden capital text-bold',
                                    'data-children' => 'capital-undestributed-profits',
                                    'style' => 'padding-left: 25px;',
                                ],
                                'items' => $capitalUndestributedProfitsItems,
                            ],
                        ],
                    ],
                    [
                        'label' => 'Долгосрочные обязательства',
                        'addCheckboxX' => true,
                        'getter' => 'getLongtermDuties',
                        'options' => [
                            'class' => 'text-bold',
                            'data-children' => 'longtermDuties',
                        ],
                        'items' => [
                            [
                                'label' => BalanceItem::findOne(BalanceItem::ITEM_LONG_TERM_LOANS)->name,
                                'options' => [
                                    'class' => 'hidden longtermDuties',
                                ],
                                'getter' => BalanceItem::$itemGetter[BalanceItem::ITEM_LONG_TERM_LOANS],
                            ],
                        ],
                    ],
                    [
                        'label' => 'Краткосрочные обязательства',
                        'addCheckboxX' => true,
                        'getter' => 'getShorttermLiabilities',
                        'options' => [
                            'class' => 'text-bold',
                            'data-children' => 'shorttermLiabilities',
                        ],
                        'items' => [
                            [
                                'label' => BalanceItem::findOne(BalanceItem::ITEM_SHORT_TERM_BORROWINGS)->name,
                                'options' => [
                                    'class' => 'hidden shorttermLiabilities',
                                ],
                                'getter' => BalanceItem::$itemGetter[BalanceItem::ITEM_SHORT_TERM_BORROWINGS],
                            ],
                            [
                                'label' => 'Кредиторская задолженность',
                                'addCheckboxX' => true,
                                'getter' => 'getShorttermAccountsPayable',
                                'options' => [
                                    'class' => 'hidden text-bold',
                                    'data-children' => 'shorttermAccountsPayable',
                                    'style' => 'padding-left: 25px;',
                                ],
                                'items' => $shortTermAccountsPayableItems,
                            ],
                        ],
                    ],
                ],
            ],
            [
                'label' => 'АКТИВ - ПАССИВ',
                'getter' => 'getBalanceCheck',
                'options' => [
                    'class' => 'text-bold',
                ],
            ],
        ];
    }

    /**
     * @param $items
     * @param array $options
     * @return array
     */
    public function getFormattedItems($items, $options = [])
    {
        $formattedItems = [];
        $company = Yii::$app->user->identity->company;
        /* @var $items \frontend\modules\reports\models\BalanceItem[] */
        $items = BalanceItem::find()
            ->leftJoin(['companyItem' => BalanceCompanyItem::tableName()], "
                companyItem.item_id = balance_item.id 
                AND companyItem.company_id = {$company->id}
            ")
            ->andWhere(['in', 'id', $items])
            ->orderBy('companyItem.sort')
            ->all();
        foreach ($items as $item) {
            $formattedItems[] = [
                'label' => $item->subname ?: $item->name,
                'canUpdate' => in_array($item->id, [
                    BalanceItem::ITEM_REAL_ESTATE_OBJECT,
                    BalanceItem::ITEM_EQUIPMENT_ASSETS,
                    BalanceItem::ITEM_TRANSPORT_ASSETS,
                    BalanceItem::ITEM_INTANGIBLE_ASSETS,
                    BalanceItem::ITEM_OTHER_ASSETS,
                    BalanceItem::ITEM_BUDGET_OVERPAYMENT,
                    BalanceItem::ITEM_OTHER_INVESTMENTS,
                    BalanceItem::ITEM_AUTHORIZED_CAPITAL,
                    BalanceItem::ITEM_BUDGET_ARREARS,
                ]),
                'options' => array_merge($options, [
                    'data-itemid' => $item->id,
                ]),
                'getter' => BalanceItem::$itemGetter[$item->id],
            ];
        }

        return $formattedItems;
    }

    /**
     * @param $data
     * @param null $formName
     */
    public function _load($data, $formName = null)
    {
        if ($this->load($data, $formName)) {
            if (!in_array($this->year, $this->getYearFilter())) {
                $this->_year = date('Y');
            }
        }
    }

    /**
     * @param bool $text
     * @return array
     */
    public function getChartPeriod($text = true)
    {
        $month = [];
        $currMonth = $this->year == date('Y') ? date('m') : 12;
        $currYear = $this->year;
        for ($i = $currMonth + 1; $i <= 12; $i++) {
            $monthNumber = $i > 9 ? $i : "0{$i}";
            $month[] = $text ?
                Month::$monthShort[$i] . '.' . ($currYear - 1) :
                date_create($currYear - 1 . '-' . $monthNumber . '-01');
        }
        for ($i = 1; $i <= $currMonth; $i++) {
            $monthNumber = $i > 9 ? $i : "0{$i}";
            $month[] = $text ?
                Month::$monthShort[$i] . '.' . ($currYear) :
                date_create($currYear . '-' . $monthNumber . '-01');
        }

        return $month;
    }

    public function getChartAmount($type = self::TYPE_ASSETS)
    {
        $result = [];
        $items = $type == self::TYPE_ASSETS ? [
            [
                'name' => 'Запасы',
                'items' => [
                    BalanceItem::ITEM_STOCKS,
                ],
                'color' => 'rgba(183,225,115,1)',
            ],
            [
                'name' => 'Дебиторы',
                'items' => [
                    BalanceItem::ITEM_TRADE_RECEIVABLES,
                    BalanceItem::ITEM_PREPAYMENTS_TO_SUPPLIERS,
                    BalanceItem::ITEM_BUDGET_OVERPAYMENT,
                    BalanceItem::ITEM_LOANS_ISSUED,
                    BalanceItem::ITEM_OTHER_INVESTMENTS,
                    BalanceItem::ITEM_RENTAL_DEPOSIT,
                ],
                'color' => 'rgba(160,64,0,1)',
            ],
            [
                'name' => 'Деньги',
                'items' => [
                    BalanceItem::ITEM_BANK_ACCOUNTS,
                    BalanceItem::ITEM_CASHBOX,
                    BalanceItem::ITEM_EMONEY,
                ],
                'color' => 'rgba(17,122,101,1)',
            ],
            [
                'name' => 'Внеоборотные активы',
                'items' => [
                    BalanceItem::ITEM_REAL_ESTATE_OBJECT,
                    BalanceItem::ITEM_EQUIPMENT_ASSETS,
                    BalanceItem::ITEM_TRANSPORT_ASSETS,
                    BalanceItem::ITEM_INTANGIBLE_ASSETS,
                    BalanceItem::ITEM_OTHER_ASSETS,
                ],
                'color' => 'rgba(41,138,188,1)',
            ],
        ] : [
            [
                'name' => 'Капитал',
                'items' => [
                    BalanceItem::ITEM_AUTHORIZED_CAPITAL,
                    BalanceItem::ITEM_UNDESTRIBUTED_PROFITS,
                ],
                'color' => 'rgba(183,225,115,1)',
            ],
            [
                'name' => 'Кредиторы',
                'items' => [
                    BalanceItem::ITEM_VENDOR_INVOICES_PAYABLE,
                    BalanceItem::ITEM_PREPAYMENT_OF_CUSTOMERS,
                    BalanceItem::ITEM_SALARY_ARREARS,
                    BalanceItem::ITEM_BUDGET_ARREARS,
                ],
                'color' => 'rgba(160,64,0,1)',
            ],
            [
                'name' => 'Кредиты / Займы',
                'items' => [
                    BalanceItem::ITEM_LONG_TERM_LOANS,
                    BalanceItem::ITEM_SHORT_TERM_BORROWINGS,
                ],
                'color' => 'rgba(41,138,188,1)',
            ],
        ];

        $i = 0;
        foreach ($items as $item) {
            $formattedItems = ArrayHelper::map($this->getFormattedItems($item['items']), 'label', 'getter');
            foreach ($formattedItems as $label => $getter) {
                if (!isset($result[$i])) {
                    $result[$i] = [
                        'name' => $item['name'],
                        'data' => [],
                    ];
                }
                foreach ($this->getChartPeriod(false) as $key => $date) {
                    $amount = $this->$getter($date) / 100;
                    if (!isset($result[$i]['data'][$key])) {
                        $result[$i]['data'][$key] = $amount;
                    } else {
                        $result[$i]['data'][$key] += $amount;
                    }
                }
            }
            $i++;
        }

        return $result;
    }

    /**
     * @param $itemID
     * @param $date
     * @return null|string
     */
    private function getBalanceItemAmount($itemID, $date)
    {
        /* @var $balanceItemAmount BalanceItemAmount */
        $balanceItemAmount = BalanceItemAmount::find()
            ->andWhere(['and',
                ['company_id' => $this->company->id],
                ['item_id' => $itemID],
                ['month' => $date->format('m')],
                ['year' => $date->format('Y')],
            ])
            ->one();

        return $balanceItemAmount ? $balanceItemAmount->amount : null;
    }

    /**
     * @param $date
     * @param $documentType
     * @param $flowType
     * @return mixed
     */
    private function getPrepaidAmount($date, $documentType, $flowType)
    {
        return $this->company->getInvoices()->alias('invoice')
                ->joinWith('acts')
                ->joinWith('packingLists')
                ->joinWith('upds')
                ->andWhere(['invoice.is_deleted' => false])
                ->andWhere(['invoice.type' => $documentType])
                ->andWhere(['invoice.invoice_status_id' => InvoiceStatus::STATUS_PAYED])
                ->andWhere(['<=', 'invoice.invoice_status_updated_at', $date->getTimestamp()])
                ->andWhere(['or',
                    ['and',
                        ['act.id' => null],
                        ['packing_list.id' => null],
                        ['upd.id' => null],
                    ],
                    ['or',
                        ['and',
                            ['not', ['act.id' => null]],
                            ['>', 'act.document_date', $date->format('Y-m-d')],
                        ],
                        ['and',
                            ['not', ['packing_list.id' => null]],
                            ['>', 'packing_list.document_date', $date->format('Y-m-d')],
                        ],
                        ['and',
                            ['not', ['upd.id' => null]],
                            ['>', 'upd.document_date', $date->format('Y-m-d')],
                        ],
                    ],
                ])
                ->andWhere(['and',
                    ['invoice.need_act' => true],
                    ['invoice.need_packing_list' => true],
                    ['invoice.need_upd' => true],
                ])
                ->sum('invoice.total_amount_with_nds') +
            $this->company->getCashBankFlows()->alias('cbf')
                ->select([
                    'cbf.amount',
                    'SUM(invoice.total_amount_with_nds) as invoiceAmount',
                ])
                ->joinWith('invoices')
                ->andWhere(['cbf.flow_type' => $flowType])
                ->andWhere(['<=', 'date', $date->format('Y-m-d')])
                ->andHaving(['invoiceAmount' => null])
                ->andWhere(['or',
                    ['cbf.is_prepaid_expense' => true],
                    ['>', 'cbf.recognition_date', $date->format('Y-m-d')]
                ])
                ->groupBy('cbf.id')
                ->sum('amount') +
            $this->company->getCashBankFlows()->alias('cbf')
                ->select([
                    'cbf.amount',
                    'SUM(invoice.total_amount_with_nds) as invoiceAmount',
                ])
                ->joinWith('invoices')
                ->andWhere(['cbf.flow_type' => $flowType])
                ->andWhere(['<=', 'date', $date->format('Y-m-d')])
                ->andHaving(['and',
                    ['not', ['invoiceAmount' => null]],
                    'invoiceAmount < cbf.amount'
                ])
                ->andWhere(['or',
                    ['cbf.is_prepaid_expense' => true],
                    ['>', 'cbf.recognition_date', $date->format('Y-m-d')]
                ])
                ->groupBy('cbf.id')
                ->sum('amount') +
            $this->company->getCashOrderFlows()->alias('cof')
                ->select([
                    'cof.amount',
                    'SUM(invoice.total_amount_with_nds) as invoiceAmount',
                ])
                ->joinWith('invoices')
                ->andWhere(['cof.flow_type' => $flowType])
                ->andWhere(['<=', 'date', $date->format('Y-m-d')])
                ->andHaving(['invoiceAmount' => null])
                ->andWhere(['or',
                    ['cof.is_prepaid_expense' => true],
                    ['>', 'cof.recognition_date', $date->format('Y-m-d')]
                ])
                ->groupBy('cof.id')
                ->sum('amount') +
            $this->company->getCashOrderFlows()->alias('cof')
                ->select([
                    'cof.amount',
                    'SUM(invoice.total_amount_with_nds) as invoiceAmount',
                ])
                ->joinWith('invoices')
                ->andWhere(['cof.flow_type' => $flowType])
                ->andWhere(['<=', 'date', $date->format('Y-m-d')])
                ->andHaving(['and',
                    ['not', ['invoiceAmount' => null]],
                    'invoiceAmount < cof.amount'
                ])
                ->andWhere(['or',
                    ['cof.is_prepaid_expense' => true],
                    ['>', 'cof.recognition_date', $date->format('Y-m-d')]
                ])
                ->groupBy('cof.id')
                ->sum('amount');
    }

    ////////////////////////////////
    ///////////// CHART ////////////
    ////////////////////////////////

    public function getFromCurrentMonthsPeriods($left, $right, $offsetYear = 0, $offsetMonth = "0") {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month")->modify("{$offsetMonth} month");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = clone ($curr->modify('last day of this month')->setTime(23, 59, 59));
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }

    /**
     * @param $type
     * @throws \Exception
     */
    public function generateXls($type)
    {
        $balance = $this->search($type, Yii::$app->request->get());

        // header
        $columns = [];
        $columns[] = [
            'attribute' => 'itemName',
            'header' => 'Статьи',
        ];
        foreach (AbstractFinance::$month as $monthNumber => $monthText) {
            $monthNumber = (int)$monthNumber;
            $columns[] = [
                'attribute' => "item{$monthNumber}",
                'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                'header' => $monthText,
            ];
        }
        $columns[] = [
            'attribute' => 'dataYear',
            'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
            'header' => $this->year,
        ];

        // data
        $formattedData = [];
        foreach ($balance as $level1) {
            $formattedData[] = $this->_buildXlsRow($level1, 1);
            if ($level1['items']) foreach ($level1['items'] as $level2) {
                $formattedData[] = $this->_buildXlsRow($level2, 2);
                if ($level2['items']) foreach ($level2['items'] as $level3) {
                    $formattedData[] = $this->_buildXlsRow($level3, 3);
                    if ($level3['items']) foreach ($level3['items'] as $level4) {
                        $formattedData[] = $this->_buildXlsRow($level4, 4);
                    }
                }
            }
        }

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $formattedData,
            'title' => "Баланс за {$this->year} год",
            'rangeHeader' => range('A', 'N'),
            'columns' => $columns,
            'format' => 'Xlsx',
            'fileName' => "Баланс за {$this->year} год",
        ]);
    }
    
    protected function _buildXlsRow($data, $level = 1)
    {
        $row = [
            'itemName' => str_repeat(' ', ($level-1) * 5) . trim(strip_tags($data['label'] ?? '')),
            'dataYear' => ($data['data']['total'] ?? '0.00') / 100
        ];
        for ($i=1;$i<=12;$i++)
            $row['item'.$i] = ($data['data'][$i] ?? '0.00') / 100;

        return $row;
    }
}
