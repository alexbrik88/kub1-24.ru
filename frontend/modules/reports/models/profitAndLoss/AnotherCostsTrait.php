<?php

namespace frontend\modules\reports\models\profitAndLoss;

use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceExpenditureItem;
use frontend\modules\reports\models\ProfitAndLossCompanyItem;
use yii\db\Expression;

trait AnotherCostsTrait {

    /**
     * @param $parentItem
     * @param $year
     * @return mixed
     */
    public function getAnotherCosts($parentItem, $year)
    {
        $result = [];

        if (!array_key_exists('anotherCosts' . $year, $this->data)) {

            $dateFrom = "{$year}-01-01";
            $dateTo = "{$year}-12-31";

            $profitAndLossCompanyItems = ProfitAndLossCompanyItem::find()
                ->joinWith('item')
                ->andWhere(['profit_and_loss_company_item.expense_type' => ProfitAndLossCompanyItem::OTHER_EXPENSE_TYPE])
                ->andWhere(['profit_and_loss_company_item.company_id' => $this->company->id])
                ->indexBy(['item_id'])
                ->orderBy(InvoiceExpenditureItem::tableName() . '.name')
                ->asArray()
                ->all();

            $ONLY_EXPENDITURE_ITEMS = array_keys($profitAndLossCompanyItems);

            foreach (self::flowClassArray() as $flowClassName) {
                $flowTableName = $flowClassName::tableName();

                $flows = $flowClassName::find()
                    ->andWhere(["{$flowClassName::tableName()}.company_id" => $this->company->id])
                    ->byFlowType(CashFlowsBase::FLOW_TYPE_EXPENSE)
                    ->andWhere(['expenditure_item_id' => $ONLY_EXPENDITURE_ITEMS])
                    ->andWhere(['between', 'recognition_date', $dateFrom, $dateTo])
                    ->select([
                        new Expression("DISTINCT({$flowTableName}.id)"),
                        "{$flowTableName}.expenditure_item_id as item_id",
                        "{$flowTableName}.amount as sum",
                        "{$flowTableName}.recognition_date",
                        "{$flowTableName}.contractor_id",
                        $flowClassName == CashBankFlows::class ?
                            new Expression("1 AS is_accounting") : "{$flowTableName}.is_accounting"
                    ])
                    ->asArray()
                    ->all();

                foreach ($flows as $flow) {
                    $monthNumber = substr($flow['recognition_date'], 5, 2);
                    $key = "expense-{$flow['item_id']}";

                    if (!isset($result[$key])) {
                        $companyItem = $profitAndLossCompanyItems[$flow['item_id']]['item'];
                        $itemName = $companyItem['name'] ?? "item_id={$flow['item_id']}";

                        $result[$key] = self::_getBlankItem('another-costs expenses-block', $itemName, null, $key, self::MOVEMENT_TYPE_FLOWS);
                        $result[$key]['canDrag'] = $companyItem['can_update'] ?? false;
                        $result[$key]['data-block'] = ProfitAndLossCompanyItem::OTHER_EXPENSE_TYPE;
                        $result[$key]['data-id'] = $flow['item_id'];
                    }

                    $result[$key]['amount'][$monthNumber] += $flow['sum'];
                    $parentItem['amount'][$monthNumber] += $flow['sum'];

                    // amount taxable
                    if ($flow['is_accounting']) {
                        $isContractorAccounting = !in_array($flow['contractor_id'], self::$notAccountingContractors);
                        if ($isContractorAccounting || $flowClassName == CashBankFlows::class) {
                            $result[$key]['amountTax'][$monthNumber] += $flow['sum'];
                            $parentItem['amountTax'][$monthNumber] += $flow['sum'];
                        }
                    }
                }
            }

            $result = ['totalAnotherCosts' => $parentItem] + $result;
            $this->data['anotherCosts' . $year] = $this->calculateQuarterAndTotalAmount($result);
        }

        return $this->data['anotherCosts' . $year];

    }
}