<?php

namespace frontend\modules\reports\models\profitAndLoss;

use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use frontend\models\Documents;
use frontend\modules\reports\models\AbstractFinance;
use frontend\modules\reports\models\ProfitAndLossCompanyItem;
use yii\db\Expression;

trait OperatingCostsTrait {

    /**
     * @param $parentItem
     * @param $year
     * @return mixed
     */
    public function getOperatingCosts($parentItem, $year)
    {
        $result = [];

        if (!array_key_exists('operatingCosts' . $year, $this->data)) {

            $dateFrom = "{$year}-01-01";
            $dateTo = "{$year}-12-31";

            $profitAndLossCompanyItems = ProfitAndLossCompanyItem::find()
                ->joinWith('item')
                ->andWhere(['profit_and_loss_company_item.expense_type' => ProfitAndLossCompanyItem::OPERATING_EXPENSE_TYPE])
                ->andWhere(['profit_and_loss_company_item.company_id' => $this->company->id])
                ->indexBy(['item_id'])
                ->orderBy(InvoiceExpenditureItem::tableName() . '.name')
                ->asArray()
                ->all();

            $ONLY_EXPENDITURE_ITEMS = array_keys($profitAndLossCompanyItems);

            //////////// BY DOC ////////////
            /// сумма входящих Актов, ТН и УПД, у которых указанные статьи и дата которых попадает в нужный месяц

            foreach (self::docClassArray() as $docsAlias => $docClassName) {
                $t = $docClassName::tableName();

                /** @var  $docClassName Act */
                $docData = Invoice::find()
                    ->joinWith($docsAlias, false)
                    ->select(["{$t}.id", "{$t}.document_date", "invoice.invoice_expenditure_item_id as item_id", 'invoice.contractor_id'])
                    ->andWhere(['invoice.company_id' => $this->company->id])
                    ->andWhere(['invoice.type' => Documents::IO_TYPE_IN])
                    ->andWhere(['invoice.invoice_expenditure_item_id' => $ONLY_EXPENDITURE_ITEMS])
                    ->andWhere(['between', "{$t}.document_date", $dateFrom, $dateTo])
                    ->indexBy('id')
                    ->asArray()
                    ->all();

                $orderTable = AbstractFinance::$docOrdersTables[$docsAlias];

                $amount = $orderTable['class']::find()
                    ->leftJoin('order', "{$orderTable['name']}.order_id = `order`.id")
                    ->where([$orderTable['key'] => array_column($docData, 'id')])
                    ->select(new Expression("{$orderTable['key']}, SUM(`order`.purchase_price_with_vat * {$orderTable['name']}.quantity) sum"))
                    ->groupBy($orderTable['key'])
                    ->indexBy($orderTable['key'])
                    ->asArray()
                    ->all();

                array_walk($docData, function(&$v, $k) use ($amount) { $v['sum'] = $amount[$k]['sum'] ?? 0; });

                foreach ($docData as $doc) {
                    $monthNumber = substr($doc['document_date'], 5, 2);
                    $key = "expense-{$doc['item_id']}";

                    if (!isset($result[$key])) {
                        $companyItem = $profitAndLossCompanyItems[$doc['item_id']]['item'];
                        $itemName = $companyItem['name'] ?? "item_id={$doc['item_id']}";

                        $result[$key] = self::_getBlankItem('operating-costs expenses-block', $itemName, null, $key, self::MOVEMENT_TYPE_DOCS_AND_FLOWS);
                        $result[$key]['canDrag'] = $companyItem['can_update'] ?? false;
                        $result[$key]['data-block'] = ProfitAndLossCompanyItem::OPERATING_EXPENSE_TYPE;
                        $result[$key]['data-id'] = $doc['item_id'];
                    }

                    $result[$key]['amount'][$monthNumber] += $doc['sum'];
                    $parentItem['amount'][$monthNumber] += $doc['sum'];

                    // amount taxable
                    $isAccounting = !in_array($doc['contractor_id'], self::$notAccountingContractors);
                    if ($isAccounting) {
                        $result[$key]['amountTax'][$monthNumber] += $doc['sum'];
                        $parentItem['amountTax'][$monthNumber] += $doc['sum'];
                    }
                }
            }

            //////////// BY NO DOC ////////////
            ///  1) есть привязка к счетам, у которых НЕТ Актов/ТН/УПД или у них стоит "Без Акта/ТН/УПД" и "Дата признания дохода" в нужном нам месяце +
            ///  2) нет привязки к счетам и "Дата признания дохода" нужном месяце

            foreach (self::flowClassArray() as $flowClassName) {
                $flowTableName = $flowClassName::tableName();

                $flows = $flowClassName::find()
                    ->andWhere(["{$flowClassName::tableName()}.company_id" => $this->company->id])
                    ->byFlowType(CashFlowsBase::FLOW_TYPE_EXPENSE)
                    ->andWhere(['expenditure_item_id' => $ONLY_EXPENDITURE_ITEMS])
                    ->select([
                        new Expression("DISTINCT({$flowTableName}.id)"),
                        "{$flowTableName}.expenditure_item_id as item_id",
                        "{$flowTableName}.amount as sum",
                        "{$flowTableName}.recognition_date",
                        "{$flowTableName}.contractor_id",
                        $flowClassName == CashBankFlows::class ?
                            new Expression("1 AS is_accounting") : "{$flowTableName}.is_accounting"
                    ])
                    ->joinWith([
                        'invoices',
                    ], false)
                    ->andWhere(['between', 'recognition_date', $dateFrom, $dateTo])
                    ->andWhere(['or',
                        ['invoice.id' => null],
                        ['or',
                            ['and',
                                ['not', ['invoice.id' => null]],
                                ['invoice.has_act' => 0],
                                ['invoice.has_packing_list' => 0],
                                ['invoice.has_upd' => 0],
                            ],
                            ['and',
                                ['not', ['invoice.id' => null]],
                                ['or',
                                    ['invoice.need_act' => 0],
                                    ['invoice.need_packing_list' => 0],
                                    ['invoice.need_upd' => 0],
                                ]
                            ],
                        ]
                    ])
                    ->asArray()
                    ->all();

                foreach ($flows as $flow) {
                    $monthNumber = substr($flow['recognition_date'], 5, 2);
                    $key = "expense-{$flow['item_id']}";

                    if (!isset($result[$key])) {
                        $companyItem = $profitAndLossCompanyItems[$flow['item_id']]['item'];
                        $itemName = $companyItem['name'] ?? "item_id={$flow['item_id']}";

                        $result[$key] = self::_getBlankItem('operating-costs expenses-block', $itemName, null, $key, self::MOVEMENT_TYPE_DOCS_AND_FLOWS);
                        $result[$key]['canDrag'] = $companyItem['can_update'] ?? false;
                        $result[$key]['data-block'] = ProfitAndLossCompanyItem::CONSTANT_EXPENSE_TYPE;
                        $result[$key]['data-id'] = $flow['item_id'];
                    }

                    $result[$key]['amount'][$monthNumber] += $flow['sum'];
                    $parentItem['amount'][$monthNumber] += $flow['sum'];

                    // amount taxable
                    if ($flow['is_accounting']) {
                        $isContractorAccounting = !in_array($flow['contractor_id'], self::$notAccountingContractors);
                        if ($isContractorAccounting || $flowClassName == CashBankFlows::class) {
                            $result[$key]['amountTax'][$monthNumber] += $flow['sum'];
                            $parentItem['amountTax'][$monthNumber] += $flow['sum'];
                        }
                    }
                }
            }

            uasort($result, function ($a, $b) {
                return $a['label'] <=> $b['label'];
            });

            $result = ['totalOperatingCosts' => $parentItem] + $result;
            $this->data['operatingCosts' . $year] = $this->calculateQuarterAndTotalAmount($result);
        }

        return $this->data['operatingCosts' . $year];
    }

}