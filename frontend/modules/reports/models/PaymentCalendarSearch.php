<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 21.02.2019
 * Time: 23:31
 */

namespace frontend\modules\reports\models;


use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\components\helpers\Month;
use common\components\TextHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\Employee;
use frontend\modules\cash\models\CashContractorType;
use Google\AdsApi\AdManager\v201805\Date;
use yii\base\InvalidParamException;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class PaymentCalendarSearch
 * @package frontend\modules\reports\models
 *
 * @property Company $company
 */
class PaymentCalendarSearch extends AbstractFinance implements FinanceReport
{
    /**
     *
     */
    const TAB_BY_ACTIVITY = 3;
    /**
     *
     */
    const TAB_BY_PURSE = 4;

    /**
     * Min flows count to add contractors flows to auto-plan list
     */
    const AUTO_PLAN_CONTRACTOR_FLOWS_COUNT = 3;

    /**
     * @var
     */
    public $period;

    /**
     * @var
     */
    public $group;

    /**
     * @var
     */
    public $payment_type;

    /**
     * @var
     */
    public $flow_type;

    /**
     * @var
     */
    public $contractor_id;

    /**
     * @var
     */
    public $contractor_name;

    /**
     * @var
     */
    public $reason_id;

    /**
     * @vars
     */
    protected $_bankArray;
    protected $_cashboxArray;
    protected $_emoneyArray;

    /**
     * @var array
     */
    public static $cashContractorTypeByTypes = [
        self::INCOME_CASH_BANK => [
            CashContractorType::ORDER_TEXT,
            CashContractorType::EMONEY_TEXT,
        ],
        self::EXPENSE_CASH_BANK => [
            CashContractorType::ORDER_TEXT,
            CashContractorType::EMONEY_TEXT,
        ],
        self::INCOME_CASH_ORDER => [
            CashContractorType::BANK_TEXT,
            CashContractorType::EMONEY_TEXT,
        ],
        self::EXPENSE_CASH_ORDER => [
            CashContractorType::BANK_TEXT,
            CashContractorType::EMONEY_TEXT,
        ],
        self::INCOME_CASH_EMONEY => [
            CashContractorType::ORDER_TEXT,
            CashContractorType::BANK_TEXT,
        ],
        self::EXPENSE_CASH_EMONEY => [
            CashContractorType::ORDER_TEXT,
            CashContractorType::BANK_TEXT,
        ],
    ];

    /**
     * @var array
     */
    public static $paymentTypeByType = [
        self::INCOME_CASH_BANK => PlanCashFlows::PAYMENT_TYPE_BANK,
        self::EXPENSE_CASH_BANK => PlanCashFlows::PAYMENT_TYPE_BANK,
        self::INCOME_CASH_ORDER => PlanCashFlows::PAYMENT_TYPE_ORDER,
        self::EXPENSE_CASH_ORDER => PlanCashFlows::PAYMENT_TYPE_ORDER,
        self::INCOME_CASH_EMONEY => PlanCashFlows::PAYMENT_TYPE_EMONEY,
        self::EXPENSE_CASH_EMONEY => PlanCashFlows::PAYMENT_TYPE_EMONEY,
    ];

    public $incomeItemIdManyItem;
    public $expenditureItemIdManyItem;

    /**
     * @var array
     */
    public static $paymentTypeTextByClassName = [
        CashBankFlows::class => 'Банк',
        CashOrderFlows::class => 'Касса',
        CashEmoneyFlows::class => 'E-money',
    ];

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['contractor_id', 'reason_id', 'payment_type'], 'integer'],
            [['contractor_name'], 'safe']
        ]);
    }

    /**
     * @param $type
     * @param array $params
     * @return array
     */
    public function search($type, $params = [])
    {
        if ($type == self::TAB_BY_PURSE) {
            return $this->searchByPurse($params);
        } elseif ($type == self::TAB_BY_ACTIVITY) {
            return $this->searchByActivity($params);
        }
        throw new InvalidParamException('Invalid type param.');
    }

    /**
     * @param array $params
     * @return array
     */
    public function searchByPurse($params = [])
    {
        $this->_load($params);
        $isCurrYear = $this->year == date('Y');
        $currMonth = date('m');
        $result = [];

        $result['balance']['totalFlowSum'] = 0;
        $prevSum = ['incomePrevSum' => 0, 'expenditurePrevSum' => 0];
        foreach (self::$month as $monthNumber => $monthText) {
            $result['balance'][$monthNumber] = 0;
            $result['growingBalance'][$monthNumber] = 0;
            $result['growingBalanceContractorBalance'][$monthNumber] = 0;
        }

        $dateFrom = "{$this->year}-01-01";
        $dateTo = "{$this->year}-12-31";
        $factDateFrom = min($this->getRealFlowsYearFilter()) . '-01-01';
        $factDateTo = date('Y-m-d');

        // 1. Start balance plan
        $b = PlanCashFlows::find()
            ->andWhere(['company_id' => $this->company->id])
            ->andWhere(['contractor_id' => CashContractorType::BALANCE_TEXT])
            ->andWhere(['between', 'date', $dateFrom, $dateTo])
            ->select('SUM(amount) amount, date')
            ->groupBy('date')
            ->asArray()
            ->all();

        foreach ($b as $bb) {
            $monthNumber = substr($bb['date'], 5, 2);
            $result['growingBalanceContractorBalance'][$monthNumber] += $bb['amount'];
        }

        // 2. Start balance fact
        //if ($isCurrYear) {
        //    foreach (self::$purseBlocksClass as $purseClassName) {
        //        $result['growingBalance'][$currMonth] += $this->getCashFlowsContractorBalanceAmount($purseClassName, $factDateFrom, $factDateTo);
        //    }
        //}

        foreach (self::$purseTypes as $typeID => $typeName) {

            $type = self::$flowTypeByPurseType[$typeID];
            $params = $this->getParamsByFlowType($type);
            $paymentType = self::$paymentTypeByType[$typeID];

            // Общий остаток на конец месяца
            $todayFactGrowingBalance = 0;
            $currEndMonthPlanBalance = 0;
            if (in_array($typeID, [self::INCOME_CASH_BANK, self::INCOME_CASH_ORDER, self::INCOME_CASH_EMONEY])) { // calc 1 time
                $purseBlocksClass = new self::$purseTypesClass[$typeID];
                $todayFactGrowingBalance = $purseBlocksClass->getBalanceAtEnd(new \DateTime(), $this->company);
                $result['growingBalance'][$currMonth] += $todayFactGrowingBalance + $currEndMonthPlanBalance;
            }

            $query = $this->getDefaultCashFlowsQuery(PlanCashFlows::class, $params, $dateFrom, $dateTo);
            $d = $this->filterByPurseQuery($query)
                ->select([
                    PlanCashFlows::tableName() . ".{$params['itemAttrName']}",
                    'invoiceItem.name as itemName',
                    'SUM(' . PlanCashFlows::tableName() . '.amount) as flowSum',
                    'date'
                ])
                ->andWhere([PlanCashFlows::tableName() . '.payment_type' => $paymentType])
                ->groupBy([PlanCashFlows::tableName() . ".{$params['itemAttrName']}", "date"])
                ->asArray()
                ->all();

            $itemsByCashContractor = $this->getDefaultItemsByCashFlowContractorQuery(PlanCashFlows::class, $type, self::$cashContractorTypeByTypes[$typeID], $dateFrom, $dateTo)
                ->andWhere(['payment_type' => $paymentType,])
                ->groupBy([PlanCashFlows::tableName() . '.contractor_id', 'date'])
                ->asArray()
                ->all();

            $d = array_merge($d, $itemsByCashContractor);

            foreach ($d as $item) {

                $monthNumber = substr($item['date'], 5, 2);

                // NEW CALC
                $isCurrYear = $this->year == date('Y');
                //$isCurrMonth = (int)$monthNumber == date('m') && $isCurrYear;
                //$isPlanMonth = (int)$monthNumber > date('m') && $isCurrYear;

                if (isset($item['contractorType'])) {
                    $itemID = $item['contractorType'];
                    $flowSum = (int)$item['flowSum'];
                    if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $itemName = 'Приход из ' . self::$cashContractorTextByType[$type][$item['contractorType']];
                    } else {
                        $itemName = 'Перевод в ' . self::$cashContractorTextByType[$type][$item['contractorType']];
                    }
                } else {
                    $itemID = $item[$params['itemAttrName']];
                    $flowSum = (int)$item['flowSum'];
                    $itemName = $item['itemName'];
                }
                // Сумма по статьям помесячно
                if (isset($result[$typeID][$itemID][$monthNumber])) {
                    $result[$typeID][$itemID][$monthNumber]['flowSum'] += $flowSum;
                } else {
                    $result[$typeID][$itemID][$monthNumber] = ['flowSum' => $flowSum];
                }
                // Итого по статьям
                if (isset($result[$typeID][$itemID]['totalFlowSum'])) {
                    $result[$typeID][$itemID]['totalFlowSum'] += $flowSum;
                } else {
                    $result[$typeID][$itemID]['totalFlowSum'] = $flowSum;
                }
                // Существующие статьи
                $result['itemName'][$typeID][$itemID] = $itemName;
                // Сумма по типам помесячно
                if (isset($result['types'][$typeID][$monthNumber]['flowSum'])) {
                    $result['types'][$typeID][$monthNumber]['flowSum'] += $flowSum;
                } else {
                    $result['types'][$typeID][$monthNumber]['flowSum'] = $flowSum;
                }
                // Итого по типам
                if (isset($result['types'][$typeID]['totalFlowSum'])) {
                    $result['types'][$typeID]['totalFlowSum'] += $flowSum;
                } else {
                    $result['types'][$typeID]['totalFlowSum'] = $flowSum;
                }

                // Сальдо
                //$sum = isset($result['types'][$typeID][$monthNumber]['flowSum']) ? (int)$result['types'][$typeID][$monthNumber]['flowSum'] : 0;
                if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                    $result['balance'][$monthNumber] += $flowSum;
                } else {
                    $result['balance'][$monthNumber] -= $flowSum;
                }
            }

            $sum = isset($result['types'][$typeID]['totalFlowSum']) ? $result['types'][$typeID]['totalFlowSum'] : 0;
            if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                $result['balance']['totalFlowSum'] += $sum;
            } else {
                $result['balance']['totalFlowSum'] -= $sum;
            }
            if (isset($result['itemName'][$typeID])) {
                asort($result['itemName'][$typeID]);
            }
        }

        // 5. growing balance (Сальдо нарастающим)
        foreach (self::$month as $monthNumber => $monthName) {
            // NEW CALC
            $isPlanMonth = (int)$monthNumber > date('m') && $isCurrYear;
            if ($isPlanMonth) {
                $previousMonth = intval($monthNumber) - 1;
                $previousMonth = $previousMonth > 9 ? $previousMonth : ('0' . $previousMonth);
                $result['growingBalance'][$monthNumber] += $result['growingBalanceContractorBalance'][$monthNumber];
                $result['growingBalance'][$monthNumber] += $result['balance'][$monthNumber];
                $result['growingBalance'][$monthNumber] += $result['growingBalance'][$previousMonth] ?? 0;
            }
        }

        //if ($this->year !== min($this->getYearFilter())) {
        //    $dateFrom = min($this->getYearFilter()) . '-01-01';
        //    $dateTo = ($this->year - 1) . '-12-' . cal_days_in_month(CAL_GREGORIAN, 12, $this->year - 1);
        //    foreach (self::$purseTypes as $typeID => $typeName) {
        //        $type = self::$flowTypeByPurseType[$typeID];
        //        $paymentType = self::$purseBlockByType[$typeID];
        //        $params = $this->getParamsByFlowType($type);
        //        $query = $this->getDefaultCashFlowsQuery(PlanCashFlows::className(), $params, $dateFrom, $dateTo);
        //        $items = $this->filterByPurseQuery($query)
        //            ->select([
        //                PlanCashFlows::tableName() . ".{$params['itemAttrName']}",
        //                'invoiceItem.name as itemName',
        //                'SUM(' . PlanCashFlows::tableName() . '.amount) as flowSum'
        //            ])
        //            ->andWhere([PlanCashFlows::tableName() . '.payment_type' => $paymentType])
        //            ->groupBy([PlanCashFlows::tableName() . ".{$params['itemAttrName']}"])
        //            ->asArray()
        //            ->all();
        //        $itemsByCashContractor = $this->getDefaultItemsByCashFlowContractorQuery(
        //            PlanCashFlows::className(), $type, self::$cashContractorTypeByTypes[$typeID], $dateFrom, $dateTo
        //        )
        //            ->andWhere(['payment_type' => $paymentType,])
        //            ->groupBy([PlanCashFlows::tableName() . '.contractor_id'])
        //            ->asArray()
        //            ->all();
        //        foreach ($itemsByCashContractor as $itemByCashContractor) {
        //            $items[] = $itemByCashContractor;
        //        }
        //        foreach ($items as $item) {
        //            $prevSum[$params['prevSumAttrName']] += (int)$item['flowSum'];
        //            if (!isset($prevSum[self::$purseBlockByType[$typeID]][$params['prevSumAttrName']])) {
        //                $prevSum[self::$purseBlockByType[$typeID]][$params['prevSumAttrName']] = $item['flowSum'];
        //            } else {
        //                $prevSum[self::$purseBlockByType[$typeID]][$params['prevSumAttrName']] += $item['flowSum'];
        //            }
        //        }
        //    }
        //    $previousBalance = $prevSum['incomePrevSum'] - $prevSum['expenditurePrevSum'];
        //    foreach ($result['growingBalance'] as $key => $value) {
        //        $result['growingBalance'][$key] += $previousBalance;
        //    }
        //}
        $result = $this->calculateBlocksSum(self::$purseBlockByType, [
            self::INCOME_CASH_BANK,
            self::INCOME_CASH_ORDER,
            self::INCOME_CASH_EMONEY
        ], $result);
        $result = $this->calculateGrowingBalanceByBlockByPurse(self::$purseBlocks, $result, $prevSum, $this->year);

        return $result;
    }

    /**
     * @param array $params
     * @return array
     */
    public function searchByActivity($params = [])
    {
        $this->_load($params);
        $isCurrYear = $this->year == date('Y');
        $currMonth = date('m');
        $result = [];

        // 0. zeroes
        $result['balance']['totalFlowSum'] = 0;
        $prevSum = ['incomePrevSum' => 0, 'expenditurePrevSum' => 0];
        foreach (self::$month as $monthNumber => $monthText) {
            $result['balance'][$monthNumber] = 0;
            $result['growingBalance'][$monthNumber] = 0;
            $result['balanceContractorPlan'][$monthNumber] = 0;
        }

        $dateFrom = "{$this->year}-01-01";
        $dateTo = "{$this->year}-12-31";
        $factDateFrom = min($this->getRealFlowsYearFilter()) . '-01-01';
        $factDateTo = date('Y-m-d');

        // 1. Start balance plan
        $b = PlanCashFlows::find()
            ->andWhere(['company_id' => $this->company->id])
            ->andWhere(['contractor_id' => CashContractorType::BALANCE_TEXT])
            ->andWhere(['between', 'date', $dateFrom, $dateTo])
            ->select('SUM(amount) amount, date')
            ->groupBy('date')
            ->asArray()
            ->all();

        foreach ($b as $bb) {
            $monthNumber = substr($bb['date'], 5, 2);
            $result['balanceContractorPlan'][$monthNumber] += $bb['amount'];
            // $result['balance'][$monthNumber] += $bb['amount']; // add balance to "result by month"
        }

        // 2. Start balance fact
        if ($isCurrYear) {
            foreach (self::$purseBlocksClass as $purseClassName) {
                $result['growingBalance'][$currMonth] += $this->getCashFlowsContractorBalanceAmount($purseClassName, $factDateFrom, $factDateTo);
            }
        }
        $factBalanceContractor = array_sum($result['growingBalance']);

        // MAIN FOREACH
        foreach (self::$typeItem as $typeID => $itemIDs) {

            $type = self::$flowTypeByActivityType[$typeID];
            $params = $this->getParamsByFlowType($type);

            // 3. current month balance
            if ($isCurrYear) {
                $prevSum[self::$blockByType[$typeID]]['incomePrevSum'] = 0;
                $prevSum[self::$blockByType[$typeID]]['expenditurePrevSum'] = 0;
                foreach (self::$purseBlocksClass as $purseClassName) {
                    $purseClass = new $purseClassName;
                    $query = $this->getDefaultCashFlowsQuery($purseClass::className(), $params, $factDateFrom, $factDateTo);
                    $items = $this->filterByActivityQuery($query, $params, $typeID)
                        ->select([
                            $purseClass::tableName() . ".{$params['itemAttrName']}",
                            'invoiceItem.name as itemName',
                            'itemFlowOfFunds.flow_of_funds_block as flowOfFundsBlock',
                            'SUM(' . $purseClass::tableName() . '.amount) as flowSum'
                        ])
                        ->groupBy([$purseClass::tableName() . ".{$params['itemAttrName']}"])
                        ->asArray()
                        ->all();
                    foreach ($items as $item) {
                        $prevSum[$params['prevSumAttrName']] += (int)$item['flowSum'];
                        if (!isset($prevSum[self::$blockByType[$typeID]][$params['prevSumAttrName']])) {
                            $prevSum[self::$blockByType[$typeID]][$params['prevSumAttrName']] = $item['flowSum'];
                        } else {
                            $prevSum[self::$blockByType[$typeID]][$params['prevSumAttrName']] += $item['flowSum'];
                        }

                    }
                }
                $result['growingBalance'][$currMonth] += $prevSum[self::$blockByType[$typeID]]['incomePrevSum'];
                $result['growingBalance'][$currMonth] -= $prevSum[self::$blockByType[$typeID]]['expenditurePrevSum'];
            }

            // 4. plan flows by items
            $query = $this->getDefaultCashFlowsQuery(PlanCashFlows::class, $params, $dateFrom, $dateTo);
            $d = $this->filterByActivityQuery($query, $params, $typeID)
                ->select([
                    PlanCashFlows::tableName() . ".{$params['itemAttrName']}",
                    'invoiceItem.name as itemName',
                    'itemFlowOfFunds.flow_of_funds_block as flowOfFundsBlock',
                    'SUM(' . PlanCashFlows::tableName() . '.amount) as flowSum',
                    'date'
                ])
                ->groupBy([PlanCashFlows::tableName() . ".{$params['itemAttrName']}", 'date'])
                ->asArray()
                ->all();


            foreach ($d as $item) {

                $monthNumber = substr($item['date'], 5, 2);
                $isCurrMonth = (int)$monthNumber == date('m') && $isCurrYear;
                $isPlanMonth = (int)$monthNumber > date('m') && $isCurrYear;

                $itemID = $item[$params['itemAttrName']];
                $flowSum = $item['flowSum'];
                $itemName = $item['itemName'];
                // Сумма по статьям помесячно
                if (isset($result[$typeID][$itemID][$monthNumber])) {
                    $result[$typeID][$itemID][$monthNumber]['flowSum'] += $flowSum;
                } else {
                    $result[$typeID][$itemID][$monthNumber] = ['flowSum' => $flowSum];
                }
                // Итого по статьям
                if (isset($result[$typeID][$itemID]['totalFlowSum'])) {
                    $result[$typeID][$itemID]['totalFlowSum'] += $flowSum;
                } else {
                    $result[$typeID][$itemID]['totalFlowSum'] = $flowSum;
                }
                // Существующие статьи
                $result['itemName'][$typeID][$itemID] = $itemName;
                // Сумма по типам помесячно
                if (isset($result['types'][$typeID][$monthNumber]['flowSum'])) {
                    $result['types'][$typeID][$monthNumber]['flowSum'] += $flowSum;
                } else {
                    $result['types'][$typeID][$monthNumber]['flowSum'] = $flowSum;
                }

                // Итого по типам
                if ($isPlanMonth) {
                    if (isset($result['types'][$typeID]['totalFlowSum'])) {
                        $result['types'][$typeID]['totalFlowSum'] += $flowSum;
                    } else {
                        $result['types'][$typeID]['totalFlowSum'] = $flowSum;
                    }
                } else if ($isCurrMonth) {

                }

                // Сальдо
                //$sum = isset($result['types'][$typeID][$monthNumber]['flowSum']) ? (int)$result['types'][$typeID][$monthNumber]['flowSum'] : 0;
                if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                    $result['balance'][$monthNumber] += $flowSum;
                } else {
                    $result['balance'][$monthNumber] -= $flowSum;
                }
            }
            //}

            $sum = isset($result['types'][$typeID]['totalFlowSum']) ?
                $result['types'][$typeID]['totalFlowSum'] : 0;
            if ($type == CashFlowsBase::FLOW_TYPE_INCOME) {
                $result['balance']['totalFlowSum'] += $sum;
            } else {
                $result['balance']['totalFlowSum'] -= $sum;
            }
            if (isset($result['itemName'][$typeID])) {
                asort($result['itemName'][$typeID]);
            }
        }

        // 5. growing balance (Сальдо нарастающим)
        foreach (self::$month as $monthNumber => $monthName) {
            // NEW CALC
            $isPlanMonth = (int)$monthNumber > date('m') && $isCurrYear;
            if ($isPlanMonth) {
                $previousMonth = intval($monthNumber) - 1;
                $previousMonth = $previousMonth > 9 ? $previousMonth : ('0' . $previousMonth);
                $result['growingBalance'][$monthNumber] += $result['balanceContractorPlan'][$monthNumber];
                $result['growingBalance'][$monthNumber] += $result['balance'][$monthNumber];
                $result['growingBalance'][$monthNumber] += $result['growingBalance'][$previousMonth] ?? 0;
            }
        }

        //$result = $this->calculateBlocksSum(self::$blockByType, [
        //    self::RECEIPT_FINANCING_TYPE_FIRST,
        //    self::INCOME_OPERATING_ACTIVITIES,
        //    self::INCOME_INVESTMENT_ACTIVITIES,
        //], $result);

        $result['blocks'] = [];
        if (isset($result['types'])) {
            foreach ($result['types'] as $key => $data) {
                foreach ($data as $month => $value) {
                    $formattedValue = $month == 'totalFlowSum' ? $value : $value['flowSum'];
                    if (!isset($result['blocks'][self::$blockByType[$key]][$month]['flowSum'])) {
                        $result['blocks'][self::$blockByType[$key]][$month]['flowSum'] = 0;
                    }
                    if (in_array($key, [self::RECEIPT_FINANCING_TYPE_FIRST, self::INCOME_OPERATING_ACTIVITIES, self::INCOME_INVESTMENT_ACTIVITIES])) {
                        $result['blocks'][self::$blockByType[$key]][$month]['flowSum'] += $formattedValue;
                    } else {
                        $result['blocks'][self::$blockByType[$key]][$month]['flowSum'] -= $formattedValue;
                    }
                }
            }
        }

        // add balance
        foreach (self::$month as $month => $monthName) {
            if (!isset($result['blocks'][AbstractFinance::OPERATING_ACTIVITIES_BLOCK][$month]['flowSum']))
                $result['blocks'][AbstractFinance::OPERATING_ACTIVITIES_BLOCK][$month]['flowSum'] = 0;

            $result['blocks'][AbstractFinance::OPERATING_ACTIVITIES_BLOCK][$month]['flowSum'] += $result['balanceContractorPlan'][$month];
        }

        $result = $this->calculateGrowingBalanceByBlockByActivity(self::$blocks, $result, $prevSum, $factBalanceContractor, $result['balanceContractorPlan']);

        return $result;
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     * @throws \Exception
     */
    public function searchItems($params = [])
    {
        $this->_load($params);
        $this->checkCashContractor();
        if (!$this->period) {
            $this->period['start'] = '01';
            $this->period['end'] = '12';
        }
        $periodStart = $this->year . '-' . $this->period['start'] . '-01';
        $periodEnd = $this->year . '-' . $this->period['end'] . '-' . cal_days_in_month(CAL_GREGORIAN, $this->period['end'], $this->year);
        $query = PlanCashFlows::find()
            ->select([
                't.id',
                't.payment_type',
                "IF({{t}}.[[flow_type]] = 0, SUM({{t}}.[[amount]]), 0) as amountExpense",
                "IF({{t}}.[[flow_type]] = 1, SUM({{t}}.[[amount]]), 0) as amountIncome",
                't.created_at',
                't.date',
                't.flow_type',
                't.amount',
                't.contractor_id',
                't.description',
                't.expenditure_item_id',
                't.income_item_id',
                't.checking_accountant_id',
                't.cashbox_id',
                't.emoney_id',
                InvoiceIncomeItem::tableName() . '.name incomeItemName',
                InvoiceExpenditureItem::tableName() . '.name expenseItemName',
            ])
            ->from(['t' => PlanCashFlows::tableName()])
            ->joinWith('expenditureItem')
            ->joinWith('incomeItem')
            ->joinWith('contractor', false)
            ->andWhere(['t.company_id' => $this->company->id])
            ->andWhere(['between', 't.date', $periodStart, $periodEnd]);
        switch ($this->group) {
            case self::GROUP_DATE:
                $query->groupBy('t.date');
                break;
            case self::GROUP_CONTRACTOR:
                $query->groupBy('t.contractor_id');
                break;
            case self::GROUP_PAYMENT_TYPE:
                $query = PlanCashFlows::find()
                    ->select([
                        't.id',
                        't.payment_type',
                        "SUM({{planCash}}.[[amountExpensePartial]]) as amountExpense",
                        "SUM({{planCash}}.[[amountIncomePartial]]) as amountIncome",
                        't.created_at',
                        't.date',
                        't.flow_type',
                        't.amount',
                        't.contractor_id',
                        't.description',
                        't.expenditure_item_id',
                        't.income_item_id',
                        InvoiceIncomeItem::tableName() . '.name incomeItemName',
                        InvoiceExpenditureItem::tableName() . '.name expenseItemName',
                    ])
                    ->from(['t' => PlanCashFlows::tableName()])
                    ->joinWith('expenditureItem')
                    ->joinWith('incomeItem')
                    ->leftJoin(['planCash' => PlanCashFlows::find()
                        ->select([
                            PlanCashFlows::tableName() . '.id',
                            "IF(flow_type = 0, amount, 0) as amountExpensePartial",
                            "IF(flow_type = 1, amount, 0) as amountIncomePartial",
                        ])
                        ->joinWith('expenditureItem')
                        ->joinWith('incomeItem')
                        ->andWhere([PlanCashFlows::tableName() . '.company_id' => $this->company->id])
                        ->andWhere(['between', 'date', $periodStart, $periodEnd])
                    ], 'planCash.id = t.id')
                    ->andWhere(['t.company_id' => $this->company->id])
                    ->andWhere(['between', 't.date', $periodStart, $periodEnd])
                    ->groupBy(['t.payment_type']);
                break;
            default:
                $query->groupBy('t.id');
                break;
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query->asArray(),
            'sort' => [
                'attributes' => [
                    'date' => [
                        'asc' => [
                            'date' => SORT_ASC,
                            'created_at' => SORT_ASC,
                        ],
                        'desc' => [
                            'date' => SORT_DESC,
                            'created_at' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'amountIncome' => [
                        'asc' => [
                            'flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'amountExpense' => [
                        'asc' => [
                            'flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'contractor_id',
                    'description',
                    'created_at',
                ],
                'defaultOrder' => [
                    'date' => SORT_DESC,
                ],
            ],
        ]);
        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        $query->andFilterWhere(['t.contractor_id' => $this->contractor_id]);

        if (is_array($this->income_item_id) || is_array($this->expenditure_item_id)) {
            if (!empty($this->income_item_id) && !empty($this->expenditure_item_id)) {
                $query->andWhere(['or',
                    ['and',
                        ['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE],
                        ['in', 'expenditure_item_id', $this->expenditure_item_id],
                    ],
                    ['and',
                        ['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME],
                        ['in', 'income_item_id', $this->income_item_id],
                    ],
                    $this->cash_contractor ? ['in', 'contractor_id', $this->cash_contractor] : [],
                ]);
            } elseif (!empty($this->income_item_id)) {
                $query->andWhere(['or',
                    ['and',
                        ['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME],
                        ['in', 'income_item_id', $this->income_item_id],
                    ],
                    $this->cash_contractor ? ['in', 'contractor_id', $this->cash_contractor] : [],
                ]);
            } elseif (!empty($this->expenditure_item_id)) {
                $query->andWhere(['or',
                    ['and',
                        ['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE],
                        ['in', 'expenditure_item_id', $this->expenditure_item_id],
                    ],
                    $this->cash_contractor ? ['in', 'contractor_id', $this->cash_contractor] : [],
                ]);
            } else {
                if ($this->cash_contractor) {
                    $query->andWhere(['in', 'contractor_id', $this->cash_contractor]);
                } else {
                    $query->andWhere(['flow_type' => null]);
                }
            }
        }

        $query->andFilterWhere(['payment_type' => $this->payment_type]);

        if ($this->contractor_name) {
            $query->andFilterWhere(['or',
                ['like', 'contractor.name', $this->contractor_name],
                ['like', 'contractor.ITN', $this->contractor_name],
            ]);
        }

        if ($this->reason_id == 'empty') {
            $query->andWhere(['or',
                ['and',
                    ['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE],
                    ['expenditure_item_id' => null],
                ],
                ['and',
                    ['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME],
                    ['income_item_id' => null],
                ],
            ]);
        } elseif (!empty($this->reason_id)) {
            $this->expenditure_item_id = $this->income_item_id = null;
            if (substr($this->reason_id, 0, 2) === 'e_') {
                $this->expenditure_item_id = substr($this->reason_id, 2);
            } elseif (substr($this->reason_id, 0, 2) === 'i_') {
                $this->income_item_id = substr($this->reason_id, 2);
            }
            $query->andFilterWhere([
                'expenditure_item_id' => $this->expenditure_item_id,
                'income_item_id' => $this->income_item_id,
            ]);
        }
        $this->_filterQuery = clone $query;

        return $dataProvider;
    }

    /**
     * @param array $selectedContractorsFlows
     * @return mixed
     */
    public function searchAutoPlanItems($selectedContractorsFlows = null, $skipByCashFlowAutoPlan = true)
    {
        $result['years'] = [];
        $contractorItems = [];
        foreach ([CashFlowsBase::FLOW_TYPE_EXPENSE, CashFlowsBase::FLOW_TYPE_INCOME] as $flowType) {
            $params = $this->getParamsByFlowType($flowType);
            $contractors = ($selectedContractorsFlows !== null) ?
                $selectedContractorsFlows : $this->getAutoPlanContractors($flowType);

            foreach ($this->getAutoPlanYears() as $year) {
                $dateFrom = $year . '-01-01';
                $dateTo = $year . '-12-31';
                /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
                foreach (self::flowClassArray() as $className) {
                    foreach ($contractors[$className] as $itemID => $contractorIDs) {
                        $items = $this->getDefaultCashFlowsQuery($className, $params, $dateFrom, $dateTo)
                            ->select([
                                $className::tableName() . ".{$params['itemAttrName']} as itemID",
                                'invoiceItem.name as itemName',
                                $className::tableName() . '.amount as flowSum',
                                $className::tableName() . '.contractor_id as contractorID',
                                new Expression('"' . $className::tableName() . '" as paymentType'),
                                'MONTH(' . $className::tableName() . '.date) as monthNumber',
                            ])
                            ->andWhere(['flow_type' => $flowType])
                            ->andWhere(['in', $className::tableName() . '.contractor_id', $contractorIDs])
                            ->andWhere(['invoiceItem.id' => $itemID])
                            ->orderBy(['invoiceItem.name' => SORT_ASC])
                            ->asArray()
                            ->all();
                        foreach ($items as $item) {
                            $cashFlowsAutoPlanItem = $this->findCashFlowAutoPlan(
                                $flowType,
                                $this->getPaymentTypeByTableName($item['paymentType']),
                                $item['itemID'],
                                $item['contractorID']
                            );
                            if ($cashFlowsAutoPlanItem && ($cashFlowsAutoPlanItem->is_deleted || (!$cashFlowsAutoPlanItem->is_deleted && $cashFlowsAutoPlanItem->end_plan_date > date(DateHelper::FORMAT_DATE)))) {
                                if ($skipByCashFlowAutoPlan)
                                    continue;
                            }
                            $flowSum = $item['flowSum'];
                            $contractorID = $item['contractorID'];
                            $contractor = null;
                            if (in_array($contractorID, [
                                CashContractorType::BANK_TEXT,
                                CashContractorType::ORDER_TEXT,
                                CashContractorType::EMONEY_TEXT
                            ])) {

                                /* @var $cashFlowsAutoPlanItem CashFlowsAutoPlan */
                                $cashFlowsAutoPlanItem = CashFlowsAutoPlan::find()
                                    ->andWhere(['and',
                                        ['company_id' => $this->company->id],
                                        ['flow_type' => $flowType],
                                        ['payment_type' => $this->getPaymentTypeByTableName($item['paymentType'])],
                                        ['contractor_id' => $item['contractorID']],
                                    ])->one();
                                if (
                                    ($className == CashBankFlows::className() && $contractorID == CashContractorType::BANK_TEXT) ||
                                    ($className == CashOrderFlows::className() && $contractorID == CashContractorType::ORDER_TEXT) ||
                                    ($className == CashEmoneyFlows::className() && $contractorID == CashContractorType::EMONEY_TEXT) ||
                                    ($cashFlowsAutoPlanItem && ($cashFlowsAutoPlanItem->is_deleted || (!$cashFlowsAutoPlanItem->is_deleted && $cashFlowsAutoPlanItem->end_plan_date > date(DateHelper::FORMAT_DATE))))
                                ) {
                                    continue;
                                }

                                $item['itemID'] = $contractorID;
                                $itemName = self::$cashContractorTextByType[$flowType][$contractorID];
                                $contractor = CashContractorType::findOne(['name' => $contractorID])->text;
                                if ($flowType == CashFlowsBase::FLOW_TYPE_INCOME) {
                                    $item['itemName'] = "Приход из {$itemName}";
                                } else {
                                    $item['itemName'] = "Перевод в {$itemName}";
                                }
                            }

                            $result['years'][$year] = $year;
                            if ($contractor === null) {
                                $contractor = Contractor::findOne($item['contractorID']);
                                $contractor = $contractor ? $contractor->getNameWithType() : null;
                            }

                            if (!(isset($contractorItems["{$flowType}-{$item['paymentType']}-{$item['itemID']}-{$item['contractorID']}-{$year}"]))) {
                                $contractorItems["{$flowType}-{$item['paymentType']}-{$item['itemID']}-{$item['contractorID']}-{$year}"] = [
                                    'flowType' => $flowType,
                                    'flowTypeName' => "Плановые {$params['flowTypeText']}",
                                    'paymentType' => $item['paymentType'],
                                    'paymentTypeName' => self::$paymentTypeTextByClassName[$className],
                                    'itemID' => $item['itemID'],
                                    'itemName' => $item['itemName'],
                                    'contractorID' => $item['contractorID'],
                                    'contractorName' => $contractor,
                                    'flowSum' => $flowSum,
                                    'month' => [
                                        $item['monthNumber'] => $item['monthNumber'],
                                    ],
                                    'year' => $year,
                                ];
                            } else {
                                $contractorItems["{$flowType}-{$item['paymentType']}-{$item['itemID']}-{$item['contractorID']}-{$year}"]['flowSum'] += $flowSum;
                                $contractorItems["{$flowType}-{$item['paymentType']}-{$item['itemID']}-{$item['contractorID']}-{$year}"]['month'][$item['monthNumber']] = $item['monthNumber'];
                            }
                        }
                    }
                }
            }
        }
        array_multisort(
            array_column($contractorItems, 'itemName'),
            SORT_ASC,
            array_column($contractorItems, 'contractorName'),
            SORT_ASC,
            $contractorItems
        );
        if (!isset($result['years'][date('Y')])) {
            $contractorItems = $result['years'] = [];
        }
        foreach ($contractorItems as $contractorItemData) {
            $monthCount = count($contractorItemData['month']);
            $flowSum = round($contractorItemData['flowSum'] / $monthCount, 2);
            // Сумма по типу прихода/расхода
            if (!isset($result[$contractorItemData['flowType']]['flowSum'][$contractorItemData['year']])) {
                $result[$contractorItemData['flowType']]['name'] = $contractorItemData['flowTypeName'];
                $result[$contractorItemData['flowType']]['flowSum'][$contractorItemData['year']] = $flowSum;
            } else {
                $result[$contractorItemData['flowType']]['flowSum'][$contractorItemData['year']] += $flowSum;
            }
            // Сумма по типу оплат
            if (!isset($result[$contractorItemData['flowType']][$contractorItemData['paymentType']]['flowSum'][$contractorItemData['year']])) {
                $result[$contractorItemData['flowType']][$contractorItemData['paymentType']]['name'] = $contractorItemData['paymentTypeName'];
                $result[$contractorItemData['flowType']][$contractorItemData['paymentType']]['flowSum'][$contractorItemData['year']] = $flowSum;
            } else {
                $result[$contractorItemData['flowType']][$contractorItemData['paymentType']]['flowSum'][$contractorItemData['year']] += $flowSum;
            }
            // Сумма по статьям
            if (!isset($result[$contractorItemData['flowType']][$contractorItemData['paymentType']][$contractorItemData['itemID']]['flowSum'][$contractorItemData['year']])) {
                $result[$contractorItemData['flowType']][$contractorItemData['paymentType']][$contractorItemData['itemID']]['name'] = $contractorItemData['itemName'];
                $result[$contractorItemData['flowType']][$contractorItemData['paymentType']][$contractorItemData['itemID']]['flowSum'][$contractorItemData['year']] = $flowSum;
            } else {
                $result[$contractorItemData['flowType']][$contractorItemData['paymentType']][$contractorItemData['itemID']]['flowSum'][$contractorItemData['year']] += $flowSum;
            }
            // Сумма по контрагентам
            if (!isset($result[$contractorItemData['flowType']][$contractorItemData['paymentType']][$contractorItemData['itemID']][$contractorItemData['contractorID']]['flowSum'][$contractorItemData['year']])) {
                $result[$contractorItemData['flowType']][$contractorItemData['paymentType']][$contractorItemData['itemID']][$contractorItemData['contractorID']]['name'] = $contractorItemData['contractorName'];
                $result[$contractorItemData['flowType']][$contractorItemData['paymentType']][$contractorItemData['itemID']][$contractorItemData['contractorID']]['flowSum'][$contractorItemData['year']] = $flowSum;
            } else {
                $result[$contractorItemData['flowType']][$contractorItemData['paymentType']][$contractorItemData['itemID']][$contractorItemData['contractorID']]['flowSum'][$contractorItemData['year']] += $flowSum;
            }
        }

        return $result;
    }

    /**
     * @param $flowType
     * @return array
     */
    public function getAutoPlanContractors($flowType, $minFlowsCount = null)
    {
        $minusMonthArray = range(0, 3);
        $result = [];
        $params = $this->getParamsByFlowType($flowType);
        /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
        foreach (self::flowClassArray() as $className) {
            $result[$className] = [];
            $existsContractors = [];
            foreach ($minusMonthArray as $monthCount) {
                $prevDate = strtotime("-{$monthCount} month");
                $dateFrom = date('Y-m-01', $prevDate);
                $dateTo = date('Y-m-', $prevDate) . cal_days_in_month(CAL_GREGORIAN, date('m', $prevDate), date('Y', $prevDate));
                $contractors = $this->getDefaultCashFlowsQuery($className, $params, $dateFrom, $dateTo)
                    ->select([
                        $className::tableName() . '.contractor_id as contractorID',
                        "invoiceItem.id as itemID",
                    ])
                    ->andWhere(['flow_type' => $flowType])
                    ->andWhere(['or',
                        ['invoiceItem.company_id' => null],
                        ['invoiceItem.company_id' => $this->company->id],
                    ])
                    ->groupBy($className::tableName() . '.contractor_id')
                    ->asArray()
                    ->all();

                //var_dump($contractors);

                foreach ($contractors as $contractor) {
                    if (!isset($existsContractors[$contractor['itemID']][$contractor['contractorID']])) {
                        $existsContractors[$contractor['itemID']][$contractor['contractorID']] = 1;
                    } else {
                        $existsContractors[$contractor['itemID']][$contractor['contractorID']] += 1;
                    }
                }
            }

            //var_dump($existsContractors);die;
            if ($minFlowsCount === null)
                $minFlowsCount = self::AUTO_PLAN_CONTRACTOR_FLOWS_COUNT;

            foreach ($existsContractors as $itemID => $contractors) {
                foreach ($contractors as $contractorID => $cashFlowsCount) {
                    if ($cashFlowsCount >= $minFlowsCount) {
                        $result[$className][$itemID][] = $contractorID;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param $flowType
     * @param null $paymentType
     * @param null $itemID
     * @param null $contractorID
     * @return float
     */
    public function calculatePlanMonthAmount($flowType, $paymentType = null, $itemID = null, $contractorID = null)
    {
        $flowArray = $this->getFlowArrayByPaymentType($paymentType);
        $params = $this->getParamsByFlowType($flowType);
        $contractors = $this->getAutoPlanContractors($flowType);
        $prevDate = strtotime("-2 month");
        $dateFrom = date('Y-m-01', $prevDate);
        $dateTo = date('Y-m-') . cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
        $amount = null;
        $cashFlowsAutoPlanItemAmount = null;
        /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
        foreach ($flowArray as $className) {
            foreach ($contractors[$className] as $itemID => $contractorIDs) {
                $query = $this->getDefaultCashFlowsQuery($className, $params, $dateFrom, $dateTo)
                    ->andWhere(['flow_type' => $flowType])
                    ->andWhere(['in', $className::tableName() . '.contractor_id', $contractorIDs])
                    ->andWhere(['invoiceItem.id' => $itemID]);
                if ($contractorID) {
                    $query->andWhere([$className::tableName() . '.contractor_id' => $contractorID]);
                }
                if ($itemID) {
                    $cashContractors = array_intersect($contractorIDs, [CashContractorType::BANK_TEXT, CashContractorType::ORDER_TEXT, CashContractorType::EMONEY_TEXT]);
                    $query->andWhere(['or',
                        ["itemFlowOfFunds.{$params['flowOfFundItemName']}" => $itemID],
                        $cashContractors ?
                            ['in', $className::tableName() . '.contractor_id', $cashContractors] :
                            [],
                    ]);
                } else {
                    $query->andWhere(['or',
                        ['invoiceItem.company_id' => null],
                        ['invoiceItem.company_id' => $this->company->id],
                    ]);
                }
                $amount += $query->sum('amount');
            }
        }

        return round($amount / 3, 2) + $cashFlowsAutoPlanItemAmount;
    }

    /**
     * @param $flowType
     * @param null $paymentType
     * @param null $itemID
     * @param null $contractorID
     * @return false|string
     */
    public function calculatePlanMonthDate($flowType, $paymentType = null, $itemID = null, $contractorID = null)
    {
        $flowArray = $this->getFlowArrayByPaymentType($paymentType);
        $params = $this->getParamsByFlowType($flowType);
        $contractors = $this->getAutoPlanContractors($flowType);
        $prevDate = strtotime("-2 month");
        $dateFrom = date('Y-m-01', $prevDate);
        $dateTo = date('Y-m-') . cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
        $dayCount = $daySum = $planDate = null;
        /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
        foreach ($flowArray as $className) {
            foreach ($contractors[$className] as $itemID => $contractorIDs) {
                $query = $this->getDefaultCashFlowsQuery($className, $params, $dateFrom, $dateTo)
                    ->select([
                        'DAY(' . $className::tableName() . '.date) as day',
                    ])
                    ->andWhere(['flow_type' => $flowType])
                    ->andWhere(['in', $className::tableName() . '.contractor_id', $contractorIDs])
                    ->andWhere(['invoiceItem.id' => $itemID]);
                if ($contractorID) {
                    $query->andWhere([$className::tableName() . '.contractor_id' => $contractorID]);
                }
                if ($itemID) {
                    $query->andWhere(['or',
                        ["itemFlowOfFunds.{$params['flowOfFundItemName']}" => $itemID],
                        ['in', $className::tableName() . '.contractor_id', [CashContractorType::BANK_TEXT, CashContractorType::ORDER_TEXT, CashContractorType::EMONEY_TEXT]]
                    ]);
                } else {
                    $query->andWhere(['or',
                        ['invoiceItem.company_id' => null],
                        ['invoiceItem.company_id' => $this->company->id],
                    ]);
                }
                foreach ($query->column() as $day) {
                    $dayCount++;
                    $daySum += $day;
                }
            }
        }
        if ($dayCount) {
            $planDay = floor($daySum / $dayCount);
            $planDay = $planDay < 10 ? "0{$planDay}" : $planDay;
            $planDate = date("{$planDay}.m.Y");
            if ($planDate <= date(DateHelper::FORMAT_USER_DATE)) {
                $planDate = date(DateHelper::FORMAT_USER_DATE, strtotime('+1 month', strtotime($planDate)));
            }
        }

        return $planDate;
    }

    /**
     * @param null $paymentType
     * @return array
     */
    public function getFlowArrayByPaymentType($paymentType = null)
    {
        if ($paymentType === null) {
            $flowArray = self::flowClassArray();
        } else {
            switch ($paymentType) {
                case CashBankFlows::tableName():
                    $flowArray[] = CashBankFlows::className();
                    break;
                case CashOrderFlows::tableName():
                    $flowArray[] = CashOrderFlows::className();
                    break;
                case CashEmoneyFlows::tableName():
                    $flowArray[] = CashEmoneyFlows::className();
                    break;
                default:
                    throw new InvalidParamException('Invalid payment type param.');
            }
        }

        return $flowArray;
    }

    /**
     * @param $tableName
     * @return array|int
     */
    public function getPaymentTypeByTableName($tableName)
    {
        if ($tableName === null) {
            return [
                PlanCashFlows::PAYMENT_TYPE_BANK,
                PlanCashFlows::PAYMENT_TYPE_ORDER,
                PlanCashFlows::PAYMENT_TYPE_EMONEY,
            ];
        } else {
            switch ($tableName) {
                case CashBankFlows::tableName():
                    return PlanCashFlows::PAYMENT_TYPE_BANK;
                case CashOrderFlows::tableName():
                    return PlanCashFlows::PAYMENT_TYPE_ORDER;
                case CashEmoneyFlows::tableName():
                    return PlanCashFlows::PAYMENT_TYPE_EMONEY;
                default:
                    throw new InvalidParamException('Invalid table name param.');
            }
        }
    }

    /**
     * @param $type
     * @throws \Exception
     */
    public function generateXls($type)
    {
        $data = $this->search($type, Yii::$app->request->get());
        $this->buildXls($type, $data, "ПК за {$this->year} год");
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        $minDate = PlanCashFlows::find()
            ->andWhere(['company_id' => $this->company->id])
            ->min('date');
        $maxDate = PlanCashFlows::find()
            ->andWhere(['company_id' => $this->company->id])
            ->max('date');
        if (!$minDate) {
            return [
                date('Y') => date('Y'),
            ];
        }
        $minYear = DateHelper::format($minDate, 'Y', DateHelper::FORMAT_DATE);
        $maxYear = DateHelper::format($maxDate, 'Y', DateHelper::FORMAT_DATE);
        foreach (range($minYear, $maxYear) as $value) {
            $range[$value] = $value;
        }
        arsort($range);

        return $range;
    }

    /**
     * @return array
     */
    public function getRealFlowsYearFilter()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $range = [];
        $cashOrderMinDate = CashOrderFlows::find()
            ->byCompany($company->id)
            ->min('date');
        $cashBankMinDate = CashBankFlows::find()
            ->byCompany($company->id)
            ->min('date');
        $cashEmoneyMinDate = CashEmoneyFlows::find()
            ->byCompany($company->id)
            ->min('date');
        $minDates = [];

        if ($cashOrderMinDate) $minDates[] = $cashOrderMinDate;
        if ($cashBankMinDate) $minDates[] = $cashBankMinDate;
        if ($cashEmoneyMinDate) $minDates[] = $cashEmoneyMinDate;

        $minCashDate = !empty($minDates) ? min($minDates) : date(DateHelper::FORMAT_DATE);
        $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
        $currentYear = date('Y');
        foreach (range($registrationYear, $currentYear) as $value) {
            $range[$value] = $value;
        }
        arsort($range);

        return $range;
    }

    /**
     * @return mixed
     */
    public function getAutoPlanYears()
    {
        $range = [];
        foreach (range(date('Y') - 2, date('Y')) as $value) {
            $range[$value] = $value;
        }

        return $range;
    }

    /**
     * @param $flowType
     * @param $paymentType
     * @param $item
     * @param $contractorID
     * @return CashFlowsAutoPlan|null|\yii\db\ActiveRecord
     */
    public function findCashFlowAutoPlan($flowType, $paymentType, $item, $contractorID)
    {
        return CashFlowsAutoPlan::find()
            ->andWhere(['and',
                ['company_id' => $this->company->id],
                ['flow_type' => $flowType],
                ['payment_type' => $paymentType],
                ['item' => $item],
                ['contractor_id' => $contractorID],
            ])->one();
    }

    /**
     * @param $data
     * @param null $formName
     */
    public function _load($data, $formName = null)
    {
        if ($this->load($data, $formName)) {
            if (!in_array($this->year, $this->getRealFlowsYearFilter())) {
                $this->year = date('Y');
            }
        }
    }

    /**
     * @param $flowType
     * @param bool $plan
     * @param null $year
     * @return array
     */
    public function getChartAmount($flowType, $plan = false, $year = null)
    {
        $result = [];
        $flowArray = [];
        $year = $year ?: $this->year;
        if ($plan) {
            $flowArray[] = PlanCashFlows::className();
        } else {
            $flowArray = self::flowClassArray();
        }
        foreach ($flowArray as $flowClassName) {
            foreach (Month::$monthShort as $monthNumber => $monthName) {
                $formattedMonthNumber = $monthNumber > 9 ? $monthNumber : "0{$monthNumber}";
                if (!$plan && $this->year == $year && $monthNumber > date('m')) {
                    $amount = $flowClassName == CashEmoneyFlows::className() ? (float)PlanCashFlows::find()
                            ->andWhere(['company_id' => $this->company->id])
                            ->andWhere(['not', ['contractor_id' => [
                                CashContractorType::BANK_TEXT,
                                CashContractorType::ORDER_TEXT,
                                CashContractorType::EMONEY_TEXT,
                                CashContractorType::BALANCE_TEXT,
                            ]]])
                            ->andWhere(['flow_type' => $flowType])
                            ->andWhere(['between', 'date', "{$year}-$formattedMonthNumber-01", "{$year}-$formattedMonthNumber-" . cal_days_in_month(CAL_GREGORIAN, $formattedMonthNumber, $year)])
                            ->sum('amount') / 100 : 0;
                } else {
                    $amount = (float)$flowClassName::find()
                            ->andWhere(['company_id' => $this->company->id])
                            ->andWhere(['not', ['contractor_id' => [
                                CashContractorType::BANK_TEXT,
                                CashContractorType::ORDER_TEXT,
                                CashContractorType::EMONEY_TEXT,
                                CashContractorType::BALANCE_TEXT,
                            ]]])
                            ->andWhere(['flow_type' => $flowType])
                            ->andWhere(['between', 'date', "{$year}-$formattedMonthNumber-01", "{$year}-$formattedMonthNumber-" . cal_days_in_month(CAL_GREGORIAN, $formattedMonthNumber, $year)])
                            ->sum('amount') / 100;
                }
                if (isset($result[$monthNumber])) {
                    $result[$monthNumber] += $amount;
                } else {
                    $result[$monthNumber] = $amount;
                }
            }
        }
        $result = array_values($result);
        return $result;
    }

    /**
     * @param null $year
     * @return array
     */
    public function getChartReminderAmount($year = null)
    {
        $year = $year ?: $this->year;
        $oddsSearch = new FlowOfFundsReportSearch();
        $data = $oddsSearch->searchByActivity(['FlowOfFundsReportSearch' => ['year' => $year]]);

        $ret = array_map(function ($amount, $monthNumber) use ($year) {
            if ($year == date('Y') && $monthNumber > date('m')) {
                return '';
            }

            return $amount / 100;
        }, array_values($data['growingBalance']), array_keys($data['growingBalance']));

        return $ret;
    }

    public function getFromCurrentDaysPeriods($left, $right, $offsetYear = 0, $offsetDay = "0") {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} days")->modify("{$offsetDay} days");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr->format('Y-m-d'),
                'to' => $curr->format('Y-m-d'),
            ];
            $curr = $curr->modify("+1 days");
        }

        return $ret;
    }

    public function getFromCurrentMonthsPeriods($left, $right, $offsetYear = 0, $offsetMonth = "0") {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month")->modify("{$offsetMonth} month");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr->format('Y-m-01'),
                'to' => $curr->format('Y-m-t'),
            ];
            $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }

    function getChartYearPeriods($year = null) {
        $left = 1;
        $right = 12;
        $start = (new \DateTime())->modify("first day of January " . $year ?: date('Y'));
        $curr = $start->modify("-{$left} months");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr->format('Y-m-d'),
                'to' => $curr->format('Y-m-t')
            ];

            $curr = $curr->modify("+1 month");
        }

        return $ret;
    }

    function getChartPlanBalanceFuturePoint($toDate, $purse = null)
    {
        $user = Yii::$app->user->identity;
        $company = $user->company;

        $factAmount = (int)array_sum(
            array_column($this->getPeriodChartInfo([
                'from' => new \DateTime(),
                'to' => new \DateTime(),
            ], $purse), 'endBalance'));

        /** @var $planCashFlow $planCashFlow */
        $t = PlanCashFlows::tableName();
        $i = CashFlowsBase::FLOW_TYPE_INCOME;
        $select = "IF({{{$t}}}.[[flow_type]] = {$i}, {{{$t}}}.[[amount]], - {{{$t}}}.[[amount]])";
        $query = PlanCashFlows::find()
            ->select(['flow_sum' => $select])
            ->where(['company_id' => $company->id])
            ->andFilterWhere(['payment_type' => $purse]) // !
            ->andWhere(['>', $t.'.date', date('Y-m-d')])
            ->andWhere(['<=', $t.'.date', $toDate]);

        $planAmount = (new Query)->from(['flow' => $query])->sum('flow_sum');

        return (float)bcdiv(bcadd($factAmount, $planAmount), 100, 2);
    }

    /**
     * @param array $dates
     * @param int $purse
     * @param string $period
     * @return array
     * @throws \yii\base\ErrorException
     */
    public function getPlanFactSeriesData($dates, $purse = null, $period = "days")
    {
        $resultFact = [];
        $resultPlan = [];
        $resultBalance = [];
        $resultCurrMonthPlanBalance = 0;

        $INCOME = CashFlowsBase::FLOW_TYPE_INCOME;
        $OUTCOME = CashFlowsBase::FLOW_TYPE_EXPENSE;
        $EXCLUDE_CONTRACTORS = [
            CashContractorType::BANK_TEXT,
            CashContractorType::ORDER_TEXT,
            CashContractorType::EMONEY_TEXT,
            CashContractorType::BALANCE_TEXT
        ];

        switch ($purse) {
            case 1:
                $flowArray = [CashBankFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case 2:
                $flowArray = [CashOrderFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case 3:
                $flowArray = [CashEmoneyFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            default:
                $flowArray = self::flowClassArray();
                $planFlowArray = [PlanCashFlows::class];
                break;
        }

        $dateStart = $dates[0]['from'];
        $dateEnd = $dates[count($dates)-1]['to'];

        // 0. Zeroes
        $dateKeyLength = ($period == "months") ? 7 : 10;
        foreach ($dates as $date) {
            
            $pos = substr($date['from'], 0, $dateKeyLength);

            $resultFact[$INCOME][$pos] = 0;
            $resultFact[$OUTCOME][$pos] = 0;
            $resultPlan[$INCOME][$pos] = 0;
            $resultPlan[$OUTCOME][$pos] = 0;
            $resultBalance[$pos] = 0;
        }

        // 1. Fact
        foreach ($flowArray as $flowClassName) {
            /** @var CashBankFlows $flowClassName */
            $flows = $flowClassName::find()
                ->andWhere(['company_id' => $this->company->id])
                ->andWhere(['between', 'date', $dateStart, $dateEnd])
                ->select(['amount', 'flow_type', 'date', 'contractor_id'])
                ->asArray()
                ->all();

            foreach ($flows as $f) {
                $pos = substr($f['date'], 0, $dateKeyLength);
                if (in_array($f['contractor_id'], $EXCLUDE_CONTRACTORS)) {
                    $resultBalance[$pos] += (float)bcdiv(($f['flow_type'] == $INCOME ? '' : '-') . $f['amount'], 100, 2);
                    continue;
                }

                $resultFact[$f['flow_type']][$pos] += (float)bcdiv($f['amount'], 100, 2);
            }
        }

        // 2. Plan
        foreach ($planFlowArray as $flowClassName) {
            /** @var PlanCashFlows $flowClassName */
            $flows = $flowClassName::find()
                ->andWhere(['company_id' => $this->company->id])
                ->andWhere(['not', ['contractor_id' => $EXCLUDE_CONTRACTORS]])
                ->andWhere(['between', 'date', $dateStart, $dateEnd])
                ->andFilterWhere(['payment_type' => $purse]) // !
                ->select(['amount', 'flow_type', 'date'])
                ->asArray()
                ->all();

            foreach ($flows as $f) {
                $pos = substr($f['date'], 0, $dateKeyLength);
                $resultPlan[$f['flow_type']][$pos] += (float)bcdiv($f['amount'], 100, 2);

                // curr month (future days) plan sum
                if ("months" == $period
                && date('Ym', strtotime($f['date'])) == date('Ym')
                && date('d', strtotime($f['date'])) > date('d')
                ) {
                    $resultCurrMonthPlanBalance += (float)bcdiv(($f['flow_type'] == $INCOME ? '' : '-') . $f['amount'], 100, 2);
                }
            }
        }

        // 3. Balance
        $factStartAmount = (float)bcdiv(array_sum(
            array_column($this->getPeriodChartInfo([
                'from' => date_create_from_format('Y-m-d', $dateStart),
                'to' => date_create_from_format('Y-m-d', $dateStart),
            ], $purse), 'beginBalance')), 100, 2);

        $keys = array_keys($resultBalance);
        foreach ($dates as $date) {
            $pos = substr($date['from'], 0, $dateKeyLength);
            $resultBalance[$pos] += ($resultFact[$INCOME][$pos] - $resultFact[$OUTCOME][$pos]);
            if (array_search($pos, $keys) == 0)
                $resultBalance[$pos] += $factStartAmount;
            else
                $resultBalance[$pos] += $resultBalance[$keys[array_search($pos, $keys)-1]];
        }

        return [
            'incomeFlowsFact' => array_values($resultFact[$INCOME]),
            'outcomeFlowsFact' => array_values($resultFact[$OUTCOME]),
            'incomeFlowsPlan' => array_values($resultPlan[$INCOME]),
            'outcomeFlowsPlan' => array_values($resultPlan[$OUTCOME]),
            'balanceFact' => array_values($resultBalance),
            'balancePlanCurrMonth' => $resultCurrMonthPlanBalance
        ];
    }

    /**
     * @param $flowType
     * @param bool $plan
     * @param null $year
     * @return array
     */
    public function getChartAmountByDate($flowType, $dates, $plan = false, $purse = null)
    {
        $result = [];
        $flowArray = [];
        if ($plan) {
            $flowArray[] = PlanCashFlows::className();
            $purseClassName = CashEmoneyFlows::className(); // any
        } else {
            switch ($purse) {
                case 1:
                    $purseClassName = CashBankFlows::className();
                    $flowArray = [$purseClassName];
                    break;
                case 2:
                    $purseClassName = CashOrderFlows::className();
                    $flowArray = [$purseClassName];
                    break;
                case 3:
                    $purseClassName = CashEmoneyFlows::className();
                    $flowArray = [$purseClassName];
                    break;
                default:
                    $purseClassName = CashEmoneyFlows::className(); // any
                    $flowArray = self::flowClassArray();
                    break;
            }
        }



        foreach ($flowArray as $flowClassName) {
            foreach ($dates as $date) {

                $dateStart = $date['from'];
                $dateEnd = $date['to'];
                $isFuturePeriod = str_replace('-', '', $dateStart) > str_replace('-', '', date('Y-m-d'));

                if (!$plan && $isFuturePeriod) {
                    $amount = $flowClassName == $purseClassName ? (float)PlanCashFlows::find()
                            ->andWhere(['company_id' => $this->company->id])
                            ->andWhere(['not', ['contractor_id' => [
                                CashContractorType::BANK_TEXT,
                                CashContractorType::ORDER_TEXT,
                                CashContractorType::EMONEY_TEXT,
                                CashContractorType::BALANCE_TEXT,
                            ]]])
                            ->andWhere(['flow_type' => $flowType])
                            ->andWhere(['between', 'date', $dateStart, $dateEnd])
                            ->andFilterWhere(['payment_type' => $purse])
                            ->sum('amount') / 100 : 0;
                } elseif ($plan && $isFuturePeriod) {
                    $result[$dateStart] = "";
                    continue;
                } elseif ($plan) {

                    $amount = (float)$flowClassName::find()
                            ->andWhere(['company_id' => $this->company->id])
                            ->andWhere(['not', ['contractor_id' => [
                                CashContractorType::BANK_TEXT,
                                CashContractorType::ORDER_TEXT,
                                CashContractorType::EMONEY_TEXT,
                                CashContractorType::BALANCE_TEXT,
                            ]]])
                            ->andWhere(['flow_type' => $flowType])
                            ->andWhere(['between', 'date', $dateStart, $dateEnd])
                            ->andFilterWhere(['payment_type' => $purse])
                            ->sum('amount') / 100;

                } else {
                    $amount = (float)$flowClassName::find()
                            ->andWhere(['company_id' => $this->company->id])
                            ->andWhere(['not', ['contractor_id' => [
                                CashContractorType::BANK_TEXT,
                                CashContractorType::ORDER_TEXT,
                                CashContractorType::EMONEY_TEXT,
                                CashContractorType::BALANCE_TEXT,
                            ]]])
                            ->andWhere(['flow_type' => $flowType])
                            ->andWhere(['between', 'date', $dateStart, $dateEnd])
                            ->sum('amount') / 100;
                }
                if (isset($result[$dateStart])) {
                    $result[$dateStart] += $amount;
                } else {
                    $result[$dateStart] = $amount;
                }
            }
        }
        $result = array_values($result);

        return $result;
    }

    /**
     * С учетом области видимости кассы: руководитель/бухгалтер/...
     * @param $dates
     * @return array
     * @throws \yii\base\ErrorException
     */
    public function getChartBalanceByDateFact($dates, $prevPeriods = false, $purse = null)
    {
        $result = [];

        if ($prevPeriods) {
            $factAmount = (float)bcdiv(array_sum(
                array_column($this->getPeriodChartInfo([
                    'from' => new \DateTime(),
                    'to' => new \DateTime(),
                ], $purse), 'endBalance')), 100, 2);
        }

        foreach ($dates as $date) {

            $dateStart = $date['from'];
            $dateEnd = $date['to'];
            $isPlanDate = str_replace('-', '', $dateStart) > str_replace('-', '', date('Y-m-d'));

            if ($isPlanDate) {
                $result[] = ($prevPeriods) ? $factAmount : 0;
                continue;
            } else {
                $amount = 0;
                $stat = $this->getPeriodChartInfo([
                    'from' => date_create_from_format('Y-m-d', $dateStart),
                    'to' => date_create_from_format('Y-m-d', $dateEnd),
                ], $purse);
                foreach ($stat as $v) {
                    $amount += $v['endBalance'];
                }
                $result[] = $amount / 100;
            }
        }

        return $result;
    }

    /**
     * Плановые операции
     * @param $dates
     * @param bool $plan
     * @param bool $hideFuturePeriods
     * @return array
     */
    public function getChartBalanceByDatePlan($dates, $plan = false, $hideFuturePeriods = false)
    {
        $oddsSearch = new FlowOfFundsReportSearch();
        $data = $oddsSearch->searchByActivityByDate($dates, $plan);

        $ret = [];
        foreach ($data['growingBalance'] as $date => $amount) {
            $isMoreThenCurrent = str_replace('-', '', $date) > str_replace('-', '', date('Y-m-d'));
            if (!$plan && $isMoreThenCurrent && $hideFuturePeriods) {
                $ret[] = '';
            } else {
                $ret[] = round($amount / 100);
            }
        }

        return $ret;
    }

    /**
     * @param null $company
     * @param bool|false $isMobile
     * @param array $period
     * @param Employee|null $user
     * @return array
     * @throws \yii\base\ErrorException
     */
    public function getPeriodChartInfo($period = [], $purse = null)
    {
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $periodFrom = \yii\helpers\ArrayHelper::getValue($period, 'from');
        $periodTo = ArrayHelper::getValue($period, 'to');

        if (!$purse || $purse == 1)
            $data[] = [
                'beginBalance' => (new CashBankFlows())->getBalanceAtBegin($periodFrom, $company),
                'endBalance' => (new CashBankFlows())->getBalanceAtEnd($periodTo, $company)
            ];
        if (!$purse || $purse == 2)
            $data[] = [
                'beginBalance' => (new CashOrderFlows())->getBalanceAtBegin($periodFrom, $company),
                'endBalance' => (new CashOrderFlows())->getBalanceAtEnd($periodTo, $company)
            ];
        if (!$purse || $purse == 3)
            $data[] = [
                'beginBalance' => (new CashEmoneyFlows())->getBalanceAtBegin($periodFrom, $company),
                'endBalance' => (new CashEmoneyFlows())->getBalanceAtEnd($periodTo, $company)
            ];

        return $data;

    }

    /**
     * @param bool $name
     * @return array
     */
    public function getTooltipStructureByContractors($dateFrom, $dateTo, $purse = null, $flowType = 0)
    {
        $amountsFact = [];
        $amountsPlan = [];
        $countsFact = [];
        $countsPlan = [];

        $exceptContractors = [
            CashContractorType::BANK_TEXT,
            CashContractorType::ORDER_TEXT,
            CashContractorType::EMONEY_TEXT,
            CashContractorType::BALANCE_TEXT,
            CashContractorType::COMPANY_TEXT,
        ];

        $articleAttr = ($flowType == 0) ? 'expenditure_item_id' : 'income_item_id';

        switch ($purse) {
            case 1:
                $flowArray = [CashBankFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case 2:
                $flowArray = [CashOrderFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case 3:
                $flowArray = [CashEmoneyFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            default:
                $flowArray = self::flowClassArray();
                $planFlowArray = [PlanCashFlows::class];
                break;
        }

        // 1. Fact
        foreach ($flowArray as $flowClassName) {
            $flows = $flowClassName::find()
                ->select(['contractor_id', 'amount', 'date'])
                ->andWhere(['company_id' => $this->company->id])
                ->andWhere(['between', 'date', $dateFrom, $dateTo])
                ->andWhere(['not', ['contractor_id' => $exceptContractors]])
                ->andWhere(['flow_type' => $flowType])
                ->andWhere(['not', [$articleAttr => ($flowType == 0) ? InvoiceExpenditureItem::ITEM_OWN_FOUNDS : InvoiceIncomeItem::ITEM_OWN_FOUNDS]])
                ->asArray()
                ->all();

            foreach ($flows as $f) {
                $DATE = $f['date'];
                if (!isset($amountsFact[$DATE])) {
                    $amountsFact[$DATE] = [];
                    $countsFact[$DATE] = [];
                }
                if (!isset($amountsFact[$DATE][$f['contractor_id']])) {
                    $amountsFact[$DATE][$f['contractor_id']] = 0;
                    $countsFact[$DATE][$f['contractor_id']] = 0;
                }
                $amountsFact[$DATE][$f['contractor_id']] += $f['amount'];
                $countsFact[$DATE][$f['contractor_id']] += 1;
            }
        }

        // 2. Plan
        foreach ($planFlowArray as $flowClassName) {
            $flows = $flowClassName::find()
                ->select(['contractor_id', 'amount', 'date'])
                ->andWhere(['company_id' => $this->company->id])
                ->andWhere(['flow_type' => $flowType])
                ->andWhere(['between', 'date', $dateFrom, $dateTo])
                ->andWhere(['not', ['contractor_id' => $exceptContractors]])
                ->andWhere(['not', [$articleAttr => ($flowType == 0) ? InvoiceExpenditureItem::ITEM_OWN_FOUNDS : InvoiceIncomeItem::ITEM_OWN_FOUNDS]])
                ->andFilterWhere(['payment_type' => $purse]) // !
                ->asArray()
                ->all();

            foreach ($flows as $f) {
                $DATE = $f['date'];
                if (!isset($amountsPlan[$DATE])) {
                    $amountsPlan[$DATE] = [];
                    $countsPlan[$DATE] = [];
                }
                if (!isset($amountsPlan[$DATE][$f['contractor_id']])) {
                    $amountsPlan[$DATE][$f['contractor_id']] = 0;
                    $countsPlan[$DATE][$f['contractor_id']] = 0;
                }
                $amountsPlan[$DATE][$f['contractor_id']] += $f['amount'];
                $countsPlan[$DATE][$f['contractor_id']] += 1;
            }
        }

        // 3. Names + merge amounts
        $items = [];
        foreach ($amountsFact as $date => $itemsAmounts) {
            $items[$date] = [];
            foreach ($itemsAmounts as $itemId => $amount) {
                $items[$date][$itemId] = [
                    'date' => $date,
                    'name' => ($itemModel = Contractor::findOne($itemId)) ? $itemModel->getTitle(true) : '---',
                    'amountFact' => round($amount / 100, 2),
                    'amountPlan' => 0,
                    'countFact' => $countsFact[$date][$itemId],
                    'countPlan' => 0
                ];
            }
        }
        foreach ($amountsPlan as $date => $itemsAmounts) {
            if (!isset($items[$date]))
                $items[$date] = [];
            foreach ($itemsAmounts as $itemId => $amount) {
                if (!isset($items[$date][$itemId])) {
                    $items[$date][$itemId] = [
                        'date' => $date,
                        'name' => ($itemModel = Contractor::findOne($itemId)) ? $itemModel->getTitle(true) : '---',
                        'amountFact' => 0,
                        'amountPlan' => 0,
                        'countFact' => 0,
                        'countPlan' => 0
                    ];
                }
                $items[$date][$itemId]['amountPlan'] = round($amount / 100, 2);
                $items[$date][$itemId]['countPlan'] = $countsPlan[$date][$itemId];
            }
        }

        foreach ($items as $date => &$itemsByDay) {
            uasort($itemsByDay, function ($a, $b) {
                if ($a['amountFact'] == $b['amountFact'])
                    return $a['amountPlan'] > $b['amountPlan'] ? -1 : 1;

                return ($a['amountFact'] > $b['amountFact']) ? -1 : 1;
            });
        }

        return $items;
    }
}