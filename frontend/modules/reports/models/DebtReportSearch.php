<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.01.2017
 * Time: 4:29
 */

namespace frontend\modules\reports\models;


use common\components\date\DateHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankFlowToInvoice;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyFlowToInvoice;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderFlowToInvoice;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\models\InvoiceSearch;
use frontend\modules\reports\components\DebtsHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class DebtReportSearch
 * @package frontend\modules\reports\models
 */
class DebtReportSearch extends Contractor
{
    /**
     * @var
     */
    public $title;

    /**
     * @var
     */
    public $year = null;

    /**
     * @var
     */
    public $debtType = self::BILLED_INVOICES;

    /**
     * @var
     */
    private $_query;

    /**
     *
     */
    const BILLED_INVOICES = 1;
    /**
     *
     */
    const PAYED_INVOICES = 2;
    /**
     *
     */
    const DEBT_INVOICES = 3;

    /**
     * @var array
     */
    public $debtItems = [
        self::BILLED_INVOICES => 'Выставлено счетов',
        self::PAYED_INVOICES => 'Оплачено счетов',
        self::DEBT_INVOICES => 'Не оплачено',
    ];

    public $debt_january_sum;
    public $debt_february_sum;
    public $debt_march_sum;
    public $debt_may_sum;
    public $debt_april_sum;
    public $debt_june_sum;
    public $debt_jule_sum;
    public $debt_august_sum;
    public $debt_september_sum;
    public $debt_october_sum;
    public $debt_november_sum;
    public $debt_december_sum;

    public $debt_0_10_sum;
    public $debt_11_30_sum;
    public $debt_31_60_sum;
    public $debt_61_90_sum;
    public $debt_more_90_sum;
    public $current_debt_sum;
    public $debt_all_sum;

    protected $company;
    protected $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'name', 'debtType', 'year', 'responsible_employee_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param int $documentType
     * @return Query
     */
    public function getDebtQuery($documentType = Documents::IO_TYPE_OUT)
    {
        $query = (new Query)
            ->select([
                'invoice.contractor_id',
                'debt' => 'SUM({{invoice}}.[[remaining_amount]])',
            ])
            ->from('invoice')
            ->where([
                'invoice.company_id' => Yii::$app->user->identity->company->id,
                'invoice.type' => $documentType,
                'invoice.invoice_status_id' => InvoiceStatus::$payAllowed,
                'invoice.is_deleted' => false,
            ])
            ->groupBy('contractor_id');

        $query->leftJoin('contractor', '{{invoice}}.[[contractor_id]] = {{contractor}}.[[id]]');

        InvoiceSearch::searchByRoleFilter($query);

        return $query;
    }

    /**
     * @param $params
     * @param int $documentType
     * @return ActiveDataProvider
     * @throws \yii\base\Exception
     */
    public function findDebtor($params, $documentType = Documents::IO_TYPE_OUT)
    {
        $this->load($params);

        $query = self::find()
            ->addSelect([
                'contractor.*',
                'current_debt_sum' => 'IFNULL(`current_debt`.`debt`, 0)',
                'debt_0_10_sum' => 'IFNULL(`debt_0_10`.`debt`, 0)',
                'debt_11_30_sum' => 'IFNULL(`debt_11_30`.`debt`, 0)',
                'debt_31_60_sum' => 'IFNULL(`debt_31_60`.`debt`, 0)',
                'debt_61_90_sum' => 'IFNULL(`debt_61_90`.`debt`, 0)',
                'debt_more_90_sum' => 'IFNULL(`debt_more_90`.`debt`, 0)',
                'debt_all_sum' => 'IFNULL(`debt_0_10`.`debt`, 0) +
                                   IFNULL(`debt_11_30`.`debt`, 0) +
                                   IFNULL(`debt_31_60`.`debt`, 0) +
                                   IFNULL(`debt_61_90`.`debt`, 0) +
                                   IFNULL(`debt_more_90`.`debt`, 0) +
                                   IFNULL(`current_debt`.`debt`, 0)',
            ])
            ->leftJoin([
                'current_debt' => $this->getDebtQuery($documentType)
                    ->andWhere(['>=', 'payment_limit_date', date('Y-m-d')]),
            ], '{{contractor}}.[[id]] = {{current_debt}}.[[contractor_id]]');
        foreach (DebtsHelper::$debtPeriods as $pId => $pName) {
            $period = DebtsHelper::getPeriodByDebtType($pId);
            $subquery = $this->getDebtQuery($documentType)
                ->andWhere(['<=', 'payment_limit_date', (new \DateTime("- {$period['min']} days"))->format('Y-m-d')]);
            if ($period['max']) {
                $subquery->andWhere(['>=', 'payment_limit_date', (new \DateTime("- {$period['max']} days"))->format('Y-m-d')]);
            }

            $query->leftJoin([
                $pName => $subquery,
            ], "{{contractor}}.[[id]] = {{{$pName}}}.[[contractor_id]]");
        }


        $query->andFilterWhere(['contractor.id' => $this->name]);

        $query->andFilterWhere(['like', 'contractor.name', $this->title]);

        \frontend\models\ContractorSearch::searchByRoleFilter($query);

        $this->_query = clone $query;

        $query->having(['>', 'debt_all_sum', 0]);

        return new ActiveDataProvider([
            'query' => $query,
            'db' => Yii::$app->db2,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'current_debt_sum',
                    'debt_0_10_sum',
                    'debt_11_30_sum',
                    'debt_31_60_sum',
                    'debt_61_90_sum',
                    'debt_more_90_sum',
                    'debt_all_sum',
                ],
                'defaultOrder' => [
                    'debt_all_sum' => SORT_DESC,
                ],
            ],
        ]);
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function findDebtorMonth($params)
    {
        $contractors = $this->getContractorMonthQuery()->groupBy(Contractor::tableName() . '.id');

        $this->load($params);

        $this->year = $this->year ? $this->year : date('Y');

        foreach (DebtsHelper::$month as $number => $debtName) {
            $monthDays = cal_days_in_month(CAL_GREGORIAN, $number, $this->year);
            $from = \DateTime::createFromFormat('Y-m-d', "{$this->year}-{$number}-01")->setTime(0, 0, 0);
            $till = \DateTime::createFromFormat('Y-m-d', "{$this->year}-{$number}-{$monthDays}")->setTime(23, 59, 59);
            $subQuery = Invoice::find()
                ->select([
                    Invoice::tableName() . '.contractor_id',
                ])
                ->andWhere(['and',
                    [Invoice::tableName() . '.is_deleted' => 0],
                    [Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT],
                    [Invoice::tableName() . '.company_id' => \Yii::$app->user->identity->company->id],
                    ['not', [Invoice::tableName() . '.invoice_status_id' => InvoiceStatus::STATUS_REJECTED]],
                ]);

            $subQuery->leftJoin('contractor', '{{invoice}}.[[contractor_id]] = {{contractor}}.[[id]]');

            InvoiceSearch::searchByRoleFilter($subQuery);

            if ($this->debtType == DebtReportSearch::PAYED_INVOICES) {
                $tCashBankFlowToInvoice = CashBankFlowToInvoice::tableName();
                $tCashBankFlows = CashBankFlows::tableName();
                $tCashOrderFlowToInvoice = CashOrderFlowToInvoice::tableName();
                $tCashOrderFlows = CashOrderFlows::tableName();
                $tCashEmoneyFlowToInvoice = CashEmoneyFlowToInvoice::tableName();
                $tCashEmoneyFlows = CashEmoneyFlows::tableName();

                $subQuery->addSelect([
                    'SUM(IFNULL(`cashBankToInvoiceUpdate`.`amount`, 0)) + SUM(IFNULL(`cashOrderToInvoiceUpdate`.`amount`, 0)) + SUM(IFNULL(`cashEmoneyToInvoiceUpdate`.`amount`, 0)) as invoice_sum'
                ])
                    ->andWhere(['in', 'invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]])
                    ->leftJoin("$tCashBankFlowToInvoice as cashBankToInvoice", "invoice.id = cashBankToInvoice.invoice_id")
                    ->leftJoin("$tCashBankFlows as cashBankFlow", [
                        'and',
                        "cashBankToInvoice.flow_id = cashBankFlow.id",
                        ['between', 'cashBankFlow.date', $from->format(DateHelper::FORMAT_DATE), $till->format(DateHelper::FORMAT_DATE)],
                    ])
                    ->leftJoin("$tCashBankFlowToInvoice as cashBankToInvoiceUpdate", [
                        'and',
                        "invoice.id = cashBankToInvoiceUpdate.invoice_id",
                        "cashBankFlow.id = cashBankToInvoiceUpdate.flow_id",
                    ])

                    ->leftJoin("$tCashOrderFlowToInvoice as cashOrderToInvoice", "invoice.id = cashOrderToInvoice.invoice_id")
                    ->leftJoin("$tCashOrderFlows as cashOrderFlow", [
                        'and',
                        "cashOrderToInvoice.flow_id = cashOrderFlow.id",
                        ['between', 'cashOrderFlow.date', $from->format(DateHelper::FORMAT_DATE), $till->format(DateHelper::FORMAT_DATE)],
                    ])
                    ->leftJoin("$tCashOrderFlowToInvoice as cashOrderToInvoiceUpdate", [
                        'and',
                        "invoice.id = cashOrderToInvoiceUpdate.invoice_id",
                        "cashOrderFlow.id = cashOrderToInvoiceUpdate.flow_id",
                    ])

                    ->leftJoin("$tCashEmoneyFlowToInvoice as cashEmoneyToInvoice", "invoice.id = cashEmoneyToInvoice.invoice_id")
                    ->leftJoin("$tCashEmoneyFlows as cashEmoneyFlow", [
                        'and',
                        "cashEmoneyToInvoice.flow_id = cashEmoneyFlow.id",
                        ['between', 'cashEmoneyFlow.date', $from->format(DateHelper::FORMAT_DATE), $till->format(DateHelper::FORMAT_DATE)],
                    ])
                    ->leftJoin("$tCashEmoneyFlowToInvoice as cashEmoneyToInvoiceUpdate", [
                        'and',
                        "invoice.id = cashEmoneyToInvoiceUpdate.invoice_id",
                        "cashEmoneyFlow.id = cashEmoneyToInvoiceUpdate.flow_id",
                    ]);
            } elseif ($this->debtType == DebtReportSearch::DEBT_INVOICES) {
                $subQuery
                    ->addSelect(['SUM(IFNULL(`total_amount_with_nds`, 0)) - SUM(IFNULL(`payment_partial_amount`, 0)) as invoice_sum'])
                    ->andWhere(['between', 'document_date', $from->format('Y-m-d'), $till->format('Y-m-d')])
                    ->andWhere(['invoice_status_id' => InvoiceStatus::$payAllowed]);
            } else {
                $subQuery
                    ->andWhere(['between', 'document_date', $from->format('Y-m-d'), $till->format('Y-m-d')])
                    ->addSelect(['SUM(IFNULL(`total_amount_with_nds`, 0)) as invoice_sum']);
            }

            $contractors->leftJoin([
                $debtName => $subQuery->groupBy('contractor_id')
            ], $debtName . '.contractor_id = contractor.id');
        }

        $contractors->andFilterWhere([Contractor::tableName() . '.id' => $this->name]);

        if (!isset($params['sort'])) {
            $contractors->orderBy('face_type, company_type.name_short, name');
        }

        return new ActiveDataProvider([
            'query' => $contractors,
            'db' => Yii::$app->db2,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'debt_january_sum',
                    'debt_february_sum',
                    'debt_march_sum',
                    'debt_may_sum',
                    'debt_april_sum',
                    'debt_june_sum',
                    'debt_jule_sum',
                    'debt_august_sum',
                    'debt_september_sum',
                    'debt_october_sum',
                    'debt_november_sum',
                    'debt_december_sum',
                    'debt_all_sum',
                ],
            ],
        ]);
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function findNewClients($params)
    {
        $this->load($params);

        $this->year = $this->year ? $this->year : date('Y');

        $newClients = $this->getNewContractorsQuery();

        foreach (DebtsHelper::$month as $number => $debtName) {
            $subQuery = Invoice::find()
                ->select([
                    'invoice_sum' => 'SUM(IFNULL(`total_amount_with_nds`, 0)) - SUM(IFNULL(`remaining_amount`, 0))',
                    'invoice.contractor_id',
                ])
                ->andWhere(['and',
                    ['invoice.is_deleted' => false],
                    ['invoice.type' => Documents::IO_TYPE_OUT],
                    ['invoice.company_id' => \Yii::$app->user->identity->company->id],
                    ['or',
                        ['in', 'invoice.invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]],
                        ['and',
                            ['invoice.invoice_status_id' => InvoiceStatus::STATUS_OVERDUE],
                            ['not', ['invoice.remaining_amount' => null]],
                        ],
                    ],
                    ['between', 'date(from_unixtime(`invoice_status_updated_at`))', $this->year . '-' . $number . '-01', $this->year . '-' . $number . '-' . cal_days_in_month(CAL_GREGORIAN, $number, $this->year)]
                ]);

            $subQuery->leftJoin('contractor', 'invoice.contractor_id = contractor.id');
            \frontend\modules\documents\models\InvoiceSearch::searchByRoleFilter($subQuery);

            $newClients->leftJoin([
                $debtName => $subQuery->groupBy('contractor_id'),
            ], $debtName . '.contractor_id = contractor.id');
        }
        if (!isset($params['sort'])) {
            $newClients->orderBy(['face_type' => SORT_ASC, CompanyType::tableName() . '.name_short' => SORT_ASC, Contractor::tableName() . '.name' => SORT_ASC]);
        }

        $newClients->andFilterWhere([Contractor::tableName() . '.id' => $this->name]);

        return new ActiveDataProvider([
            'query' => $newClients,
            'db' => Yii::$app->db2,
            'pagination' => [
                'pageSize' => \Yii::$app->request->get('per-page'),
            ],
            'sort' => [
                'attributes' => [
                    'debt_january_sum',
                    'debt_february_sum',
                    'debt_march_sum',
                    'debt_may_sum',
                    'debt_april_sum',
                    'debt_june_sum',
                    'debt_jule_sum',
                    'debt_august_sum',
                    'debt_september_sum',
                    'debt_october_sum',
                    'debt_november_sum',
                    'debt_december_sum',
                    'defaultOrder' => [
                        'name' => [
                            'asc' => [
                                'ISNULL(`' . CompanyType::tableName() . '`.`name_short`)' => SORT_ASC,
                                CompanyType::tableName() . '.name_short' => SORT_ASC,
                                Contractor::tableName() . '.name' => SORT_ASC,
                            ],
                            'desc' => [
                                'ISNULL(`' . CompanyType::tableName() . '`.`name_short`)' => SORT_DESC,
                                CompanyType::tableName() . '.name_short' => SORT_DESC,
                                Contractor::tableName() . '.name' => SORT_DESC,
                            ],
                            'default' => SORT_ASC,
                        ],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @param int $documentType
     * @return array
     */
    public function getContractorFilter($documentType = Documents::IO_TYPE_OUT)
    {
        $query = Contractor::getSorted()
            ->select([
                'name' => "IF(
                    {{contractor}}.[[company_type_id]] = :typeIP,
                    CONCAT({{company_type}}.[[name_short]], ' ', {{contractor}}.[[name]]),
                    CONCAT({{company_type}}.[[name_short]], {{contractor}}.[[name]])
                )",
                'contractor.id',
            ])
            ->distinct()
            ->innerJoin('invoice', '{{contractor}}.[[id]] = {{invoice}}.[[contractor_id]]')
            ->andWhere([
                'invoice.company_id' => Yii::$app->user->identity->company->id,
                'invoice.type' => $documentType,
                'invoice.invoice_status_id' => InvoiceStatus::$payAllowed,
                'invoice.is_deleted' => false,
            ])
            ->andFilterWhere(['like', 'contractor.name', $this->title])
            ->params([':typeIP' => CompanyType::TYPE_IP])
            ->asArray()
            ->indexBy('id');

        // STRICT BY USER
        \frontend\models\ContractorSearch::searchByRoleFilter($query);

        return ArrayHelper::merge([null => 'Все'], $query->column(\Yii::$app->db2));
    }

    /**
     *
     */
    public function getNewContractorsFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map($this->getNewContractorsQuery(false)
            ->orderBy('face_type, company_type.name_short, name')
            ->all(Yii::$app->db2), 'id', 'nameWithType'));
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        $range = [];
        $registrationYear = date('Y', \Yii::$app->user->identity->company->created_at);
        $currentYear = date('Y');
        foreach (range($registrationYear, $currentYear) as $value) {
            $range[$value] = $value;
        }
        arsort($range);

        return $range;
    }

    /**
     * @param bool|true $addSelect
     * @return \common\models\ContractorQuery
     */
    public function getNewContractorsQuery($addSelect = true)
    {
        $firstPayedInvoices = Invoice::find()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->andWhere(['or',
                ['in', 'invoice.invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]],
                ['and',
                    ['invoice.invoice_status_id' => InvoiceStatus::STATUS_OVERDUE],
                    ['not', ['invoice.remaining_amount' => null]],
                ],
            ])
            ->andWhere(['invoice.company_id' => \Yii::$app->user->identity->company->id])
            ->byDeleted();

        $firstPayedInvoices->leftJoin('contractor', 'invoice.contractor_id = contractor.id');
        InvoiceSearch::searchByRoleFilter($firstPayedInvoices);

        $firstPayedInvoices = $firstPayedInvoices->groupBy('invoice.contractor_id')
            ->orderBy(['invoice_status_updated_at' => SORT_DESC])
            ->column(\Yii::$app->db2);

        $query = self::find();
        if ($addSelect) {
            $query->addSelect([
                'contractor.*',
                'debt_january_sum' => 'IF(from_unixtime(MIN(`invoice`.`invoice_status_updated_at`), "%m") = "01", IFNULL(`debt_january`.`invoice_sum`, 0), 0)',
                'debt_february_sum' => 'IF(from_unixtime(MIN(`invoice`.`invoice_status_updated_at`), "%m") = "02", IFNULL(`debt_february`.`invoice_sum`, 0), 0)',
                'debt_march_sum' => 'IF(from_unixtime(MIN(`invoice`.`invoice_status_updated_at`), "%m") = "03", IFNULL(`debt_march`.`invoice_sum`, 0), 0)',
                'debt_may_sum' => 'IF(from_unixtime(MIN(`invoice`.`invoice_status_updated_at`), "%m") = "04", IFNULL(`debt_may`.`invoice_sum`, 0), 0)',
                'debt_april_sum' => 'IF(from_unixtime(MIN(`invoice`.`invoice_status_updated_at`), "%m") = "05", IFNULL(`debt_april`.`invoice_sum`, 0), 0)',
                'debt_june_sum' => 'IF(from_unixtime(MIN(`invoice`.`invoice_status_updated_at`), "%m") = "06", IFNULL(`debt_june`.`invoice_sum`, 0), 0)',
                'debt_jule_sum' => 'IF(from_unixtime(MIN(`invoice`.`invoice_status_updated_at`), "%m") = "07", IFNULL(`debt_jule`.`invoice_sum`, 0), 0)',
                'debt_august_sum' => 'IF(from_unixtime(MIN(`invoice`.`invoice_status_updated_at`), "%m") = "08", IFNULL(`debt_august`.`invoice_sum`, 0), 0)',
                'debt_september_sum' => 'IF(from_unixtime(MIN(`invoice`.`invoice_status_updated_at`), "%m") = "09", IFNULL(`debt_september`.`invoice_sum`, 0), 0)',
                'debt_october_sum' => 'IF(from_unixtime(MIN(`invoice`.`invoice_status_updated_at`), "%m") = "10", IFNULL(`debt_october`.`invoice_sum`, 0), 0)',
                'debt_november_sum' => 'IF(from_unixtime(MIN(`invoice`.`invoice_status_updated_at`), "%m") = "11", IFNULL(`debt_november`.`invoice_sum`, 0), 0)',
                'debt_december_sum' => 'IF(from_unixtime(MIN(`invoice`.`invoice_status_updated_at`), "%m") = "12", IFNULL(`debt_december`.`invoice_sum`, 0), 0)',
            ]);
        }

        \frontend\models\ContractorSearch::searchByRoleFilter($query);

        return $query->joinWith('companyType')
            ->joinWith('invoices')
            ->andWhere(['or',
                ['in', 'invoice.invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]],
                ['and',
                    ['invoice.invoice_status_id' => InvoiceStatus::STATUS_OVERDUE],
                    ['not', ['invoice.remaining_amount' => null]],
                ]
            ])
            ->andWhere(['in', Contractor::tableName() . '.id', Invoice::find()
                ->select(['contractor_id'])
                ->andWhere(['in', 'id', $firstPayedInvoices])
                ->andWhere(['between', 'date(from_unixtime(`' . Invoice::tableName() . '`.`invoice_status_updated_at`))', $this->year . '-01-01', $this->year . '-12-31'])
                ->column(\Yii::$app->db2)])
            ->groupBy('invoice.contractor_id')
            ->byDeleted();
    }

    /**
     * @param bool|false $filter
     * @return ActiveQuery
     */
    public function getContractorMonthQuery($filter = false)
    {
        $query = self::find();
        if (!$filter) {
            $query->addSelect([
                'contractor.*',
                'debt_january_sum' => 'IFNULL(`debt_january`.`invoice_sum`, 0)',
                'debt_february_sum' => 'IFNULL(`debt_february`.`invoice_sum`, 0)',
                'debt_march_sum' => 'IFNULL(`debt_march`.`invoice_sum`, 0)',
                'debt_may_sum' => 'IFNULL(`debt_may`.`invoice_sum`, 0)',
                'debt_april_sum' => 'IFNULL(`debt_april`.`invoice_sum`, 0)',
                'debt_june_sum' => 'IFNULL(`debt_june`.`invoice_sum`, 0)',
                'debt_jule_sum' => 'IFNULL(`debt_jule`.`invoice_sum`, 0)',
                'debt_august_sum' => 'IFNULL(`debt_august`.`invoice_sum`, 0)',
                'debt_september_sum' => 'IFNULL(`debt_september`.`invoice_sum`, 0)',
                'debt_october_sum' => 'IFNULL(`debt_october`.`invoice_sum`, 0)',
                'debt_november_sum' => 'IFNULL(`debt_november`.`invoice_sum`, 0)',
                'debt_december_sum' => 'IFNULL(`debt_december`.`invoice_sum`, 0)',
                'debt_all_sum' => 'IFNULL(`debt_january`.`invoice_sum`, 0) +
                                    IFNULL(`debt_february`.`invoice_sum`, 0) +
                                    IFNULL(`debt_march`.`invoice_sum`, 0) +
                                    IFNULL(`debt_april`.`invoice_sum`, 0) +
                                    IFNULL(`debt_may`.`invoice_sum`, 0) +
                                    IFNULL(`debt_june`.`invoice_sum`, 0) +
                                    IFNULL(`debt_jule`.`invoice_sum`, 0) +
                                    IFNULL(`debt_august`.`invoice_sum`, 0) +
                                    IFNULL(`debt_september`.`invoice_sum`, 0) +
                                    IFNULL(`debt_october`.`invoice_sum`, 0) +
                                    IFNULL(`debt_november`.`invoice_sum`, 0) +
                                    IFNULL(`debt_december`.`invoice_sum`, 0)'
            ]);
        }

        $query->byCompany(\Yii::$app->user->identity->company->id)
            ->byContractor(Contractor::TYPE_CUSTOMER)
            ->joinWith('companyType')
            ->having(['>', 'debt_all_sum', 0]);

        \frontend\models\ContractorSearch::searchByRoleFilter($query);

        return $query;
    }

    public function getResponsibleItemsByQuery(ActiveQuery $query)
    {
        $dataArray = Yii::$app->user->identity->company
            ->getEmployeeCompanies()
            ->andWhere([
                'employee_id' => $this->_query->select([
                    'contractor.responsible_employee_id',
                    'contact' => 'IF(contractor.contact_is_director = true, contractor.director_name, contractor.contact_name)',
                ])->distinct()->column(\Yii::$app->db2),
            ])
            ->orderBy(['lastname' => SORT_ASC, 'firstname_initial' => SORT_ASC, 'patronymic_initial' => SORT_ASC])
            ->all(Yii::$app->db2);

        return ['' => 'Все'] + ArrayHelper::map($dataArray, 'employee_id', function ($model) {
                return $model->getFio(true);
            });
    }

    public function getFromCurrentMonthsPeriods($left, $right, $offsetYear = 0, $offsetMonth = "0") {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month")->modify("{$offsetMonth} month");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr->format('Y-m-01'),
                'to' => $curr->format('Y-m-t'),
            ];
            $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }
}