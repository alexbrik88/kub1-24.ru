<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.05.2020
 * Time: 22:07
 */

namespace frontend\modules\reports\models\marketing;

use Carbon\Carbon;
use common\models\employee\Employee;
use common\models\googleAds\GoogleAdsKeywordReport;
use common\models\integration\LastOperationInIntegration;
use common\modules\marketing\models\channel\EventHelper;
use common\models\vkAds\VkAdsReport;
use common\models\yandex\YandexDirectAdReport;
use frontend\components\StatisticPeriod;
use frontend\modules\integration\models\IntegrationEvents;
use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\helpers\Url;

class IntegrationsStatisticSearch extends Model
{
    /** @var Employee */
    private $user;

    public function __construct(Employee $employee, $config = [])
    {
        parent::__construct($config);
        $this->user = $employee;
    }

    public function search()
    {
        $integrations = $this->user->company->integration;
        $period = StatisticPeriod::getSessionPeriod();

        $yandexDirectQuery = YandexDirectAdReport::find()
            ->select([
                'SUM(impressions_count) as impressions_count',
                'SUM(clicks_count) as clicks_count',
                'SUM(cost) as expense',
                '(SUM(ctr) / COUNT(*)) as ctr',
            ])
            ->andWhere(['between', 'date', $period['from'], $period['to']])
            ->andWhere(['company_id' => $this->user->company_id])
            ->groupBy('company_id')
            ->asArray()
            ->one();
        $googleAdsQuery = GoogleAdsKeywordReport::find()
            ->select([
                'SUM(impressions_count) as impressions_count',
                'SUM(clicks_count) as clicks_count',
                'SUM(cost) as expense',
                '(SUM(ctr) / COUNT(*)) as ctr',
            ])
            ->andWhere(['between', 'date', $period['from'], $period['to']])
            ->andWhere(['company_id' => $this->user->company_id])
            ->groupBy('company_id')
            ->asArray()
            ->one();
        /*
        $vkAdsQuery = VkAdsReport::find()
            ->select([
                'SUM(impressions_count) as impressions_count',
                'SUM(clicks_count) as clicks_count',
                'SUM(spent_amount) as expense',
                new Expression('\'-\' as ctr'),
            ])
            ->andWhere(['between', 'date', $period['from'], $period['to']])
            ->andWhere(['company_id' => $this->user->company_id])
            ->groupBy('company_id')
            ->asArray()
            ->one();
        */

        $lastYandexDirectOperationDate = $this->user->currentEmployeeCompany->company->getLastOperationsInIntegrations()
            ->select('date')
            ->andWhere(['type' => LastOperationInIntegration::YANDEX_DIRECT_TYPE])
            ->scalar();
        $lastGoogleAdsOperationDate = $this->user->currentEmployeeCompany->company->getLastOperationsInIntegrations()
            ->select('date')
            ->andWhere(['type' => LastOperationInIntegration::GOOGLE_ADS_TYPE])
            ->scalar();
        /*
        $lastVkAdsOperationDate = $this->user->currentEmployeeCompany->company->getLastOperationsInIntegrations()
            ->select('date')
            ->andWhere(['type' => LastOperationInIntegration::VK_ADS_TYPE])
            ->scalar();
        */

        return new ArrayDataProvider([
            'allModels' => [
                array_merge([
                    'id' => 'yandex_direct',
                    'channel' => 'Яндекс.Директ',
                    'url' => '/reports/yandex-direct/index',
                    'status' => is_array($integrations[Employee::INTEGRATION_YANDEX_DIRECT] ?? null),
                    'balance_amount' => '-',
                    'event' => EventHelper::getEvents($this->user->company_id, IntegrationEvents::INTEGRATION_TYPE_YANDEX_DIRECT),
                    'channel_data' => [
                        'connect_url' => Yii::$app->yandexDirect->getAuthorizationUrl(),
                        'date_last_load' => $lastYandexDirectOperationDate
                            ? Carbon::createFromTimestamp($lastYandexDirectOperationDate)->format('d.m.Y в H:i')
                            : null,
                    ],
                ], $yandexDirectQuery ?: []),
                array_merge([
                    'id' => 'google_adwords',
                    'channel' => 'Google.Adwords',
                    'url' => '/reports/google-ads/index',
                    'status' => isset($integrations[Employee::INTEGRATION_GOOGLE_ADS]['accessToken'])
                        && isset($integrations[Employee::INTEGRATION_GOOGLE_ADS]['customerId']),
                    'balance_amount' => $integrations[Employee::INTEGRATION_GOOGLE_ADS]['balance_amount'] ?? 0,
                    'event' => EventHelper::getEvents($this->user->company_id, IntegrationEvents::INTEGRATION_TYPE_GOOGLE_ADS),
                    'channel_data' => [
                        'connect_url' => null,
                        'connect_modal_id' => '#vk_connect_modal',
                        'date_last_load' => $lastGoogleAdsOperationDate
                            ? Carbon::createFromTimestamp($lastGoogleAdsOperationDate)->format('d.m.Y в H:i')
                            : null,
                    ],
                ], $googleAdsQuery ?: []),
                /*
                array_merge([
                    'id' => 'vk',
                    'channel' => 'Вконтакте',
                    'url' => '/integration/vk-ads/index',
                    'status' => is_array($integrations[Employee::INTEGRATION_VK] ?? null),
                    'balance_amount' => $integrations[Employee::INTEGRATION_VK]['budgetAmount'] ?? 0,
                    'event' => EventHelper::getEvents($this->user->company_id, IntegrationEvents::INTEGRATION_TYPE_VK_ADS),
                    'channel_data' => [
                        'connect_url' => Url::to('/integration/vk/setting'),
                        'date_last_load' =>  $lastVkAdsOperationDate
                            ? Carbon::createFromTimestamp($lastVkAdsOperationDate)->format('d.m.Y в H:i')
                            : null,
                    ],
                ], $vkAdsQuery ?: []),
                */
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => ['channel'],
                'defaultOrder' => [
                    'channel' => SORT_DESC,
                ],
            ],
        ]);
    }
}