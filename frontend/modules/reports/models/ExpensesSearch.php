<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.04.2019
 * Time: 12:35
 */

namespace frontend\modules\reports\models;


use common\components\date\DateHelper;
use common\components\excel\Excel;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\modules\acquiring\models\AcquiringOperation;
use frontend\modules\cash\models\CashContractorType;
use yii\base\InvalidParamException;
use Yii;
use yii\db\Expression;

/**
 * Class ExpensesSearch
 * @package frontend\modules\reports\models
 */
class ExpensesSearch extends AbstractFinance implements FinanceReport
{
    /**
     *
     */
    const BLOCK_EXPENSES = 'expenses';
    /**
     *
     */
    const TAB_BY_ACTIVITY = 7;
    /**
     *
     */
    const TAB_BY_PURSE = 8;

    public $incomeItemIdManyItem;
    public $expenditureItemIdManyItem;

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [

        ]);
    }

    /**
     * @param int $type
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function search($type, $params = [])
    {
        if ($type == self::TAB_BY_PURSE) {
            return $this->searchByPurse($params);
        } elseif ($type == self::TAB_BY_ACTIVITY) {
            return $this->searchByActivity($params);
        }
        throw new InvalidParamException('Invalid type param.');
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function searchByPurse($params = [])
    {
        $this->_load($params);
        $result = [];
        $result['growingBalance']['totalFlowSum'] = 0;

        $dateFrom = $this->year . '-01-01';
        $dateTo = $this->year . '-12-31';

        foreach (self::$month as $monthNumber => $monthText) {
            $result['growingBalance'][$monthNumber] = 0;
        }
        foreach (self::$purseTypes as $typeID => $typeName) {
            if (in_array($typeID, [
                self::INCOME_CASH_BANK,
                self::INCOME_CASH_ORDER,
                self::INCOME_CASH_EMONEY,
                self::INCOME_ACQUIRING,
            ])) {
                continue;
            }
            /* @var $flowClassName CashBankFlows|CashOrderFlows|CashEmoneyFlows */
            $flowClassName = $this->getFlowClassNameByType(self::$purseBlockByType[$typeID]);
            $params = $this->getParamsByFlowType(CashFlowsBase::FLOW_TYPE_EXPENSE);

            //foreach (self::$month as $monthNumber => $monthText) {

            $query = $this->getDefaultCashFlowsQuery($flowClassName, $params, $dateFrom, $dateTo);
            $items = $this->filterByPurseQuery($query)
                ->select([
                    $flowClassName::tableName() . ".{$params['itemAttrName']}",
                    'invoiceItem.name as itemName',
                    'SUM(' . $flowClassName::tableName() . '.amount) as flowSum',
                    'date'
                ])
                ->groupBy([$flowClassName::tableName() . ".{$params['itemAttrName']}", "date"])
                ->asArray()
                ->all();
            $itemsByCashContractor = $this->getDefaultItemsByCashFlowContractorQuery(
                $flowClassName,
                CashFlowsBase::FLOW_TYPE_EXPENSE,
                self::$cashContractorTypeByTableName[$flowClassName],
                $dateFrom,
                $dateTo
            )
                ->groupBy([$flowClassName::tableName() . '.contractor_id', 'date'])
                ->asArray()
                ->all();
            foreach ($itemsByCashContractor as $itemByCashContractor) {
                $items[] = $itemByCashContractor;
            }

            foreach ($items as $item) {

                $monthNumber = substr($item['date'], 5, 2);

                if (isset($item['contractorType'])) {
                    $itemID = $item['contractorType'];
                    $flowSum = (int)$item['flowSum'];
                    $itemName = 'Перевод в ' . self::$cashContractorTextByType[CashFlowsBase::FLOW_TYPE_EXPENSE][$itemID];
                } else {
                    $itemID = $item[$params['itemAttrName']];
                    $flowSum = (int)$item['flowSum'];
                    $itemName = $item['itemName'];
                }
                // Сумма по статьям помесячно
                if (isset($result[$typeID][$itemID][$monthNumber])) {
                    $result[$typeID][$itemID][$monthNumber]['flowSum'] += $flowSum;
                } else {
                    $result[$typeID][$itemID][$monthNumber] = ['flowSum' => $flowSum,];
                }
                // Итого по статьям
                if (isset($result[$typeID][$itemID]['totalFlowSum'])) {
                    $result[$typeID][$itemID]['totalFlowSum'] += $flowSum;
                } else {
                    $result[$typeID][$itemID]['totalFlowSum'] = $flowSum;
                }
                // Существующие статьи
                $result['itemName'][$typeID][$itemID] = $itemName;
                // Сумма по типам помесячно
                if (isset($result['types'][$typeID][$monthNumber]['flowSum'])) {
                    $result['types'][$typeID][$monthNumber]['flowSum'] += $flowSum;
                } else {
                    $result['types'][$typeID][$monthNumber]['flowSum'] = $flowSum;
                }
                // Итого по типам
                if (isset($result['types'][$typeID]['totalFlowSum'])) {
                    $result['types'][$typeID]['totalFlowSum'] += $flowSum;
                } else {
                    $result['types'][$typeID]['totalFlowSum'] = $flowSum;
                }

                $result['growingBalance'][$monthNumber] += $flowSum;
                $result['growingBalance']['totalFlowSum'] += $flowSum;
            }
            //}

            if (isset($result['itemName'][$typeID])) {
                asort($result['itemName'][$typeID]);
            }
        }

        return $result;
    }

    /**
     * @param array $params
     * @return array
     */
    public function searchByActivity($params = [])
    {
        $this->_load($params);
        $result = [];
        $result['growingBalance']['totalFlowSum'] = 0;

        $dateFrom = $this->year . '-01-01';
        $dateTo = $this->year . '-12-31';

        foreach (self::$month as $monthNumber => $monthText) {
            $result['growingBalance'][$monthNumber] = 0;
        }
        $params = $this->getParamsByFlowType(CashFlowsBase::FLOW_TYPE_EXPENSE);

        //foreach (self::$month as $monthNumber => $monthText) {

        /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
        foreach (self::flowClassArray() as $className) {
            $items = $this->getDefaultCashFlowsQuery($className, $params, $dateFrom, $dateTo)
                ->select([
                    $className::tableName() . ".{$params['itemAttrName']}",
                    'invoiceItem.name as itemName',
                    'itemFlowOfFunds.flow_of_funds_block as flowOfFundsBlock',
                    'SUM(' . $className::tableName() . '.amount) as flowSum',
                    'date'
                ])
                ->andWhere(['not', ['contractor_id' => [
                    CashContractorType::BANK_TEXT,
                    CashContractorType::ORDER_TEXT,
                    CashContractorType::EMONEY_TEXT,
                    CashContractorType::BALANCE_TEXT,
                ]]])
                ->groupBy([$className::tableName() . ".{$params['itemAttrName']}", 'date'])
                ->asArray()
                ->all();

            foreach ($items as $item) {

                $monthNumber = substr($item['date'], 5, 2);

                $itemID = $item[$params['itemAttrName']];
                $flowSum = (int)$item['flowSum'];
                $itemName = $item['itemName'];
                // Сумма по статьям помесячно
                if (isset($result[self::BLOCK_EXPENSES][$itemID][$monthNumber])) {
                    $result[self::BLOCK_EXPENSES][$itemID][$monthNumber]['flowSum'] += $flowSum;
                } else {
                    $result[self::BLOCK_EXPENSES][$itemID][$monthNumber] = [
                        'flowSum' => $flowSum,
                    ];
                }
                // Итого по статьям
                if (isset($result[self::BLOCK_EXPENSES][$itemID]['totalFlowSum'])) {
                    $result[self::BLOCK_EXPENSES][$itemID]['totalFlowSum'] += $flowSum;
                } else {
                    $result[self::BLOCK_EXPENSES][$itemID]['totalFlowSum'] = $flowSum;
                }
                // Существующие статьи
                $result['itemName'][self::BLOCK_EXPENSES][$itemID] = $itemName;
                // Сумма по типам помесячно
                if (isset($result['types'][self::BLOCK_EXPENSES][$monthNumber]['flowSum'])) {
                    $result['types'][self::BLOCK_EXPENSES][$monthNumber]['flowSum'] += $flowSum;
                } else {
                    $result['types'][self::BLOCK_EXPENSES][$monthNumber]['flowSum'] = $flowSum;
                }
                // Итого по типам
                if (isset($result['types'][self::BLOCK_EXPENSES]['totalFlowSum'])) {
                    $result['types'][self::BLOCK_EXPENSES]['totalFlowSum'] += $flowSum;
                } else {
                    $result['types'][self::BLOCK_EXPENSES]['totalFlowSum'] = $flowSum;
                }

                $result['growingBalance'][$monthNumber] += $flowSum;
                $result['growingBalance']['totalFlowSum'] += $flowSum;
            }
        }
        //}

        if (isset($result['itemName'][self::BLOCK_EXPENSES])) {
            asort($result['itemName'][self::BLOCK_EXPENSES]);
        }

        return $result;
    }

    /**
     * @param $type
     * @throws \Exception
     */
    public function generateXls($type)
    {
        $data = $this->search($type, Yii::$app->request->get());
        $this->buildXls($type, $data, "Расходы за {$this->year} год");
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        $range = [];
        $cashOrderMinDate = CashOrderFlows::find()
            ->byCompany($this->company->id)
            ->min('date');
        $cashBankMinDate = CashBankFlows::find()
            ->byCompany($this->company->id)
            ->min('date');
        $cashEmoneyMinDate = CashEmoneyFlows::find()
            ->byCompany($this->company->id)
            ->min('date');
        $minDates = [];

        if ($cashOrderMinDate) $minDates[] = $cashOrderMinDate;
        if ($cashBankMinDate) $minDates[] = $cashBankMinDate;
        if ($cashEmoneyMinDate) $minDates[] = $cashEmoneyMinDate;

        $minCashDate = !empty($minDates) ? min($minDates) : date(DateHelper::FORMAT_DATE);
        $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
        $currentYear = date('Y');
        foreach (range($registrationYear, $currentYear) as $value) {
            $range[$value] = $value;
        }
        arsort($range);

        return $range;
    }

    /**
     * @param $type
     * @param $data
     * @param $title
     */
    public function buildXls($type, $data, $title)
    {
        $blocks = isset($data['blocks']) ? $data['blocks'] : [];
        $types = isset($data['types']) ? $data['types'] : [];
        $expenditureItems = isset($data['itemName']) ? $data['itemName'] : [];
        $growingBalance = isset($data['growingBalance']) ? $data['growingBalance'] : 0;
        asort($expenditureItems);
        if ($type == self::TAB_BY_ACTIVITY) {
            $oddsTypes = [
                self::BLOCK_EXPENSES => 'Расходы',
            ];
            $oddsBlocks = $blockByType = [];
        } else {
            $oddsBlocks = self::$purseBlocks;
            $blockByType = self::$purseBlockByType;
            $oddsTypes = self::$purseTypes;
        }
        foreach ($oddsTypes as $typeID => $typeName) {
            $typeName = $type == self::TAB_BY_PURSE ? ('    ' . $typeName) : $typeName;
            if (in_array($typeID, [
                self::INCOME_CASH_BANK,
                self::INCOME_CASH_ORDER,
                self::INCOME_CASH_EMONEY,
            ])) {
                continue;
            }
            if ($type == self::TAB_BY_PURSE) {
                $itemData = isset($blocks[$blockByType[$typeID]]) ? $blocks[$blockByType[$typeID]] : [];
                $formattedData[] = $this->buildItem($oddsBlocks[$blockByType[$typeID]], $itemData);
            }
            $itemData = isset($types[$typeID]) ? $types[$typeID] : [];
            $formattedData[] = $this->buildItem($typeName, $itemData);
            if (isset($data[$typeID])) {
                foreach ($expenditureItems[$typeID] as $expenditureItemID => $expenditureItemName) {
                    if (isset($data[$typeID][$expenditureItemID])) {
                        $formattedData[] = $this->buildItem('        ' . $expenditureItemName, $data[$typeID][$expenditureItemID]);
                    }
                }
            }
        }
        $formattedData[] = $this->buildItem('Остаток на конец месяца', $growingBalance);

        $columns[] = [
            'attribute' => 'itemName',
            'header' => 'Статьи',
        ];
        foreach (self::$month as $monthNumber => $monthText) {
            $columns[] = [
                'attribute' => "item{$monthNumber}",
                'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
                'header' => $monthText,
            ];
        }
        $columns[] = [
            'attribute' => 'dataYear',
            'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00,
            'header' => $this->year,
        ];

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $formattedData,
            'title' => $title,
            'rangeHeader' => range('A', 'N'),
            'columns' => $columns,
            'format' => 'Xlsx',
            'fileName' => $title,
        ]);
    }

    /**
     * @param bool $plan
     * @return array
     */
    public function getFlowAmountByPeriod($plan = false)
    {
        $result = [];
        foreach (self::$month as $monthNumber => $monthName) {
            if ($this->year == date('Y') && $monthNumber > date('m') && !$plan) {
                $result[] = 0;
                continue;
            }
            $dateFrom = $this->year . '-' . $monthNumber . '-01';
            $dateTo = $this->year . '-' . $monthNumber . '-' . cal_days_in_month(CAL_GREGORIAN, $monthNumber, $this->year);
            if ($plan) {
                $result[] = PlanCashFlows::find()
                        ->andWhere(['company_id' => $this->company->id])
                        ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
                        ->andWhere(['between', 'date', $dateFrom, $dateTo])
                        ->andWhere(['not', ['contractor_id' => [
                            CashContractorType::BANK_TEXT,
                            CashContractorType::ORDER_TEXT,
                            CashContractorType::EMONEY_TEXT,
                            CashContractorType::BALANCE_TEXT,
                        ]]])
                        ->sum('amount') / 100;
            } else {
                $amount = 0;
                foreach (self::flowClassArray() as $flowClassName) {
                    $amount += $flowClassName::find()
                        ->andWhere(['company_id' => $this->company->id])
                        ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
                        ->andWhere(['between', 'date', $dateFrom, $dateTo])
                        ->andWhere(['not', ['contractor_id' => [
                            CashContractorType::BANK_TEXT,
                            CashContractorType::ORDER_TEXT,
                            CashContractorType::EMONEY_TEXT,
                            CashContractorType::BALANCE_TEXT,
                        ]]])
                        ->sum('amount');
                }
                $result[] = $amount / 100;
            }
        }

        return $result;
    }

    /**
     * @param bool $plan
     * @return array
     */
    public function getFlowAmountByItems($plan = false)
    {
        $dateFrom = $this->year . '-01-01';
        $dateTo = $this->year . '-12-31';
        if ($this->year == date('Y')) {
            $dateTo = $this->year . '-' . date('m') . '-' . cal_days_in_month(CAL_GREGORIAN, date('m'), $this->year);
        }
        $maxItems = $this->getMaxItemsFlow();
        $result = [];
        foreach ($maxItems as $maxItem) {
            if ($plan) {
                $query = PlanCashFlows::find()
                    ->andWhere(['company_id' => $this->company->id])
                    ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
                    ->andWhere(['expenditure_item_id' => $maxItem])
                    ->andWhere(['between', 'date', $dateFrom, $dateTo])
                    ->andWhere(['not', ['contractor_id' => [
                        CashContractorType::BANK_TEXT,
                        CashContractorType::ORDER_TEXT,
                        CashContractorType::EMONEY_TEXT,
                        CashContractorType::BALANCE_TEXT,
                    ]]]);
                $result[] = $query->sum('amount') / 100;
            } else {
                $amount = 0;
                foreach (self::flowClassArray() as $flowClassName) {
                    $query = $flowClassName::find()
                        ->andWhere(['company_id' => $this->company->id])
                        ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
                        ->andWhere(['expenditure_item_id' => $maxItem])
                        ->andWhere(['between', 'date', $dateFrom, $dateTo])
                        ->andWhere(['not', ['contractor_id' => [
                            CashContractorType::BANK_TEXT,
                            CashContractorType::ORDER_TEXT,
                            CashContractorType::EMONEY_TEXT,
                            CashContractorType::BALANCE_TEXT,
                        ]]]);
                    $amount += $query->sum('amount');
                }
                $result[] = $amount / 100;
            }
        }
        if ($plan) {
            $query = PlanCashFlows::find()
                ->andWhere(['company_id' => $this->company->id])
                ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
                ->andWhere(['not', ['in', 'expenditure_item_id', $maxItems]])
                ->andWhere(['between', 'date', $dateFrom, $dateTo])
                ->andWhere(['not', ['contractor_id' => [
                    CashContractorType::BANK_TEXT,
                    CashContractorType::ORDER_TEXT,
                    CashContractorType::EMONEY_TEXT,
                    CashContractorType::BALANCE_TEXT,
                ]]]);
            $result[] = $query->sum('amount') / 100;
        } else {
            $amount = 0;
            foreach (self::flowClassArray() as $flowClassName) {
                $query = $flowClassName::find()
                    ->andWhere(['company_id' => $this->company->id])
                    ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
                    ->andWhere(['not', ['in', 'expenditure_item_id', $maxItems]])
                    ->andWhere(['between', 'date', $dateFrom, $dateTo])
                    ->andWhere(['not', ['contractor_id' => [
                        CashContractorType::BANK_TEXT,
                        CashContractorType::ORDER_TEXT,
                        CashContractorType::EMONEY_TEXT,
                        CashContractorType::BALANCE_TEXT,
                    ]]]);
                $amount += $query->sum('amount');
            }
            $result[] = $amount / 100;
        }

        return $result;
    }

    /**
     * @param bool $name
     * @return array
     */
    public function getMaxItemsFlow($name = false)
    {
        $items = [];
        $cbf = CashBankFlows::find()
            ->select([
                'expenditure_item_id',
                'amount',
            ])
            ->andWhere(['company_id' => $this->company->id])
            ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
            ->andWhere(['between', 'date', $this->year . '-01-01', $this->year . '-12-31'])
            ->andWhere(['not', ['contractor_id' => [
                CashContractorType::BANK_TEXT,
                CashContractorType::ORDER_TEXT,
                CashContractorType::EMONEY_TEXT,
                CashContractorType::BALANCE_TEXT,
            ]]]);
        $cof = CashOrderFlows::find()
            ->select([
                'expenditure_item_id',
                'amount',
            ])
            ->andWhere(['company_id' => $this->company->id])
            ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
            ->andWhere(['between', 'date', $this->year . '-01-01', $this->year . '-12-31'])
            ->andWhere(['not', ['contractor_id' => [
                CashContractorType::BANK_TEXT,
                CashContractorType::ORDER_TEXT,
                CashContractorType::EMONEY_TEXT,
                CashContractorType::BALANCE_TEXT,
            ]]]);
        $query = CashEmoneyFlows::find()
            ->select([
                'expenditure_item_id',
                'amount',
            ])
            ->andWhere(['company_id' => $this->company->id])
            ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
            ->andWhere(['not', ['contractor_id' => [
                CashContractorType::BANK_TEXT,
                CashContractorType::ORDER_TEXT,
                CashContractorType::EMONEY_TEXT,
                CashContractorType::BALANCE_TEXT,
            ]]])
            ->union($cbf)
            ->union($cof)
            ->asArray()
            ->all();
        foreach ($query as $item) {
            if (!isset($items[$item['expenditure_item_id']])) {
                $items[$item['expenditure_item_id']] = 0;
            }
            $items[$item['expenditure_item_id']] += $item['amount'];
        }
        arsort($items);
        $keys = array_keys(array_slice($items, 0, 5, true));
        if ($name) {
            $items = [];
            foreach ($keys as $key) {
                $items[] = InvoiceExpenditureItem::findOne($key)->name;
            }
            $items[] = 'Остальное';

            return $items;
        }

        return $keys;
    }

    /**
     * @param $data
     * @param null $formName
     */
    public function _load($data, $formName = null)
    {
        if ($this->load($data, $formName)) {
            if (!in_array($this->year, $this->getYearFilter())) {
                $this->year = date('Y');
            }
        }
    }

    /**
     * @param $type
     * @return string
     * @throws \Exception
     */
    private function getFlowClassNameByType($type)
    {
        switch ($type) {
            case self::CASH_BANK_BLOCK:
                $className = CashBankFlows::class;
                break;
            case self::CASH_ORDER_BLOCK:
                $className = CashOrderFlows::class;
                break;
            case self::CASH_EMONEY_BLOCK:
                $className = CashEmoneyFlows::class;
                break;
            case self::CASH_ACQUIRING_BLOCK:
                $className = AcquiringOperation::class;
                break;
            default:
                throw new \Exception('Invalid type.');
                break;
        }

        return $className;
    }


    ///////////////
    /// Charts ////
    ///////////////

    /**
     * @param $dates
     * @param int $purse
     * @param string $period
     * @param int $expenditureItemId
     * @param int $supplierId
     * @return array
     */
    public function getPlanFactSeriesData($dates, $purse = null, $period = "months", $expenditureItemId = null, $supplierId = null)
    {
        $resultFact = [];
        $resultPlan = [];
        $resultBalance = [];
        $resultCurrMonthPlanBalance = 0;

        $INCOME = CashFlowsBase::FLOW_TYPE_INCOME;
        $OUTCOME = CashFlowsBase::FLOW_TYPE_EXPENSE;
        $EXCLUDE_CONTRACTORS = [
            CashContractorType::BANK_TEXT,
            CashContractorType::ORDER_TEXT,
            CashContractorType::EMONEY_TEXT,
            CashContractorType::BALANCE_TEXT
        ];

        switch ($purse) {
            case 1:
                $flowArray = [CashBankFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case 2:
                $flowArray = [CashOrderFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case 3:
                $flowArray = [CashEmoneyFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            default:
                $flowArray = self::flowClassArray();
                $planFlowArray = [PlanCashFlows::class];
                break;
        }

        $dateStart = $dates[0]['from'];
        $dateEnd = $dates[count($dates)-1]['to'];

        // 0. Zeroes
        $dateKeyLength = ($period == "months") ? 7 : 10;
        foreach ($dates as $date) {

            $pos = substr($date['from'], 0, $dateKeyLength);

            $resultFact[$INCOME][$pos] = 0;
            $resultFact[$OUTCOME][$pos] = 0;
            $resultPlan[$INCOME][$pos] = 0;
            $resultPlan[$OUTCOME][$pos] = 0;
            //$resultBalance[$pos] = 0;
        }

        // 1. Fact
        foreach ($flowArray as $flowClassName) {
            /** @var CashBankFlows $flowClassName */
            $flows = $flowClassName::find()
                ->andWhere(['company_id' => $this->company->id])
                ->andWhere(['between', 'date', $dateStart, $dateEnd])
                ->select(['amount', 'flow_type', 'date', 'contractor_id', 'expenditure_item_id'])
                ->asArray()
                ->all();

            foreach ($flows as $f) {
                $pos = substr($f['date'], 0, $dateKeyLength);
                if (in_array($f['contractor_id'], $EXCLUDE_CONTRACTORS)) {
                    //$resultBalance[$pos] += (float)bcdiv(($f['flow_type'] == $INCOME ? '' : '-') . $f['amount'], 100, 2);
                    continue;
                }

                if ($f['flow_type'] == $OUTCOME) {
                    if ($expenditureItemId && $f['expenditure_item_id'] != $expenditureItemId)
                        continue;
                    if ($supplierId && $f['contractor_id'] != $supplierId)
                        continue;
                }

                $resultFact[$f['flow_type']][$pos] += (float)bcdiv($f['amount'], 100, 2);
            }
        }

        // 2. Plan
        foreach ($planFlowArray as $flowClassName) {
            /** @var PlanCashFlows $flowClassName */
            $flows = $flowClassName::find()
                ->andWhere(['company_id' => $this->company->id])
                ->andWhere(['not', ['contractor_id' => $EXCLUDE_CONTRACTORS]])
                ->andWhere(['between', 'date', $dateStart, $dateEnd])
                ->andFilterWhere(['payment_type' => $purse]) // !
                ->select(['amount', 'flow_type', 'date', 'contractor_id', 'expenditure_item_id'])
                ->asArray()
                ->all();

            foreach ($flows as $f) {
                $pos = substr($f['date'], 0, $dateKeyLength);

                if ($f['flow_type'] == $OUTCOME) {
                    if ($expenditureItemId && $f['expenditure_item_id'] != $expenditureItemId)
                        continue;
                    if ($supplierId && $f['contractor_id'] != $supplierId)
                        continue;
                }

                $resultPlan[$f['flow_type']][$pos] += (float)bcdiv($f['amount'], 100, 2);

                // curr month (future days) plan sum
                if ("months" == $period
                    && date('Ym', strtotime($f['date'])) == date('Ym')
                    && date('d', strtotime($f['date'])) > date('d')
                ) {
                    $resultCurrMonthPlanBalance += (float)bcdiv(($f['flow_type'] == $INCOME ? '' : '-') . $f['amount'], 100, 2);
                }
            }
        }

        return [
            'incomeFlowsFact' => array_values($resultFact[$INCOME]),
            'outcomeFlowsFact' => array_values($resultFact[$OUTCOME]),
            'incomeFlowsPlan' => array_values($resultPlan[$INCOME]),
            'outcomeFlowsPlan' => array_values($resultPlan[$OUTCOME]),
        ];
    }

    /**
     * @param $dates
     * @param int $purse
     * @param string $period
     * @param int $expenditureItemId
     * @param int $supplierId
     * @return array
     */
    public function getFactSeriesData($dates, $purse = null, $period = "months", $expenditureItemId = null, $supplierId = null)
    {
        $resultFact = [];
        $resultPlan = [];
        $resultBalance = [];

        $INCOME = CashFlowsBase::FLOW_TYPE_INCOME;
        $OUTCOME = CashFlowsBase::FLOW_TYPE_EXPENSE;
        $EXCLUDE_CONTRACTORS = [
            CashContractorType::BANK_TEXT,
            CashContractorType::ORDER_TEXT,
            CashContractorType::EMONEY_TEXT,
            CashContractorType::BALANCE_TEXT
        ];

        switch ($purse) {
            case 1:
                $flowArray = [CashBankFlows::class];
                break;
            case 2:
                $flowArray = [CashOrderFlows::class];
                break;
            case 3:
                $flowArray = [CashEmoneyFlows::class];
                break;
            default:
                $flowArray = self::flowClassArray();
                break;
        }

        $dateStart = $dates[0]['from'];
        $dateEnd = $dates[count($dates)-1]['to'];

        // 0. Zeroes
        $dateKeyLength = ($period == "months") ? 7 : 10;
        foreach ($dates as $date) {

            $pos = substr($date['from'], 0, $dateKeyLength);

            $resultFact[$INCOME][$pos] = 0;
            $resultFact[$OUTCOME][$pos] = 0;
            $resultPlan[$INCOME][$pos] = 0;
            $resultPlan[$OUTCOME][$pos] = 0;
            //$resultBalance[$pos] = 0;
        }

        // 1. Fact
        foreach ($flowArray as $flowClassName) {
            /** @var CashBankFlows $flowClassName */
            $flows = $flowClassName::find()
                ->andWhere(['company_id' => $this->company->id])
                ->andWhere(['between', 'date', $dateStart, $dateEnd])
                ->select(['amount', 'flow_type', 'date', 'contractor_id', 'expenditure_item_id'])
                ->asArray()
                ->all();

            foreach ($flows as $f) {
                $pos = substr($f['date'], 0, $dateKeyLength);
                if (in_array($f['contractor_id'], $EXCLUDE_CONTRACTORS)) {
                    //$resultBalance[$pos] += (float)bcdiv(($f['flow_type'] == $INCOME ? '' : '-') . $f['amount'], 100, 2);
                    continue;
                }

                if ($f['flow_type'] == $OUTCOME) {
                    if ($expenditureItemId && $f['expenditure_item_id'] != $expenditureItemId)
                        continue;
                    if ($supplierId && $f['contractor_id'] != $supplierId)
                        continue;
                }

                $resultFact[$f['flow_type']][$pos] += (float)bcdiv($f['amount'], 100, 2);
            }
        }

        return [
            'incomeFlowsFact' => array_values($resultFact[$INCOME]),
            'outcomeFlowsFact' => array_values($resultFact[$OUTCOME]),
        ];
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @return array
     */
    public function getChartStructureByArticles($dateFrom, $dateTo, $purse = null)
    {
        $exceptContractors = [
            CashContractorType::BANK_TEXT,
            CashContractorType::ORDER_TEXT,
            CashContractorType::EMONEY_TEXT,
            CashContractorType::BALANCE_TEXT,
        ];

        switch ($purse) {
            case 1:
                $flowArray = [CashBankFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case 2:
                $flowArray = [CashOrderFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case 3:
                $flowArray = [CashEmoneyFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            default:
                $flowArray = self::flowClassArray();
                $planFlowArray = [PlanCashFlows::class];
                break;
        }

        // 1. Fact
        $amountsFact = [];
        foreach ($flowArray as $flowClassName) {
            $flows = $flowClassName::find()
                ->select(['expenditure_item_id', 'amount'])
                ->andWhere(['company_id' => $this->company->id])
                ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
                ->andWhere(['between', 'date', $dateFrom, $dateTo])
                ->andWhere(['not', ['contractor_id' => $exceptContractors]])
                ->andWhere(['not', ['expenditure_item_id' => InvoiceExpenditureItem::ITEM_OWN_FOUNDS]])
                ->asArray()
                ->all();

            foreach ($flows as $f) {
                if (!isset($amountsFact[$f['expenditure_item_id']])) {
                    $amountsFact[$f['expenditure_item_id']] = 0;
                }
                $amountsFact[$f['expenditure_item_id']] += $f['amount'];
            }
        }

        // 2. Plan
        $amountsPlan = [];
        foreach ($planFlowArray as $flowClassName) {
            $flows = $flowClassName::find()
                ->select(['expenditure_item_id', 'amount'])
                ->andWhere(['company_id' => $this->company->id])
                ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
                ->andWhere(['between', 'date', $dateFrom, $dateTo])
                ->andWhere(['not', ['contractor_id' => $exceptContractors]])
                ->andWhere(['not', ['expenditure_item_id' => InvoiceExpenditureItem::ITEM_OWN_FOUNDS]])
                ->andFilterWhere(['payment_type' => $purse]) // !
                ->asArray()
                ->all();

            foreach ($flows as $f) {
                if (!isset($amountsPlan[$f['expenditure_item_id']])) {
                    $amountsPlan[$f['expenditure_item_id']] = 0;
                }
                $amountsPlan[$f['expenditure_item_id']] += $f['amount'];
            }
        }

        // 3. Names + merge amounts
        $items = [];
        foreach ($amountsFact as $itemId => $amount) {
            $items[$itemId] = [
                'id' => $itemId,
                'name' => ($itemModel = InvoiceExpenditureItem::findOne($itemId)) ? $itemModel->name : '---',
                'amountFact' => round($amount / 100, 2),
                'amountPlan' => 0
            ];
        }
        foreach ($amountsPlan as $itemId => $amount) {
            if (!isset($items[$itemId])) {
                $items[$itemId] = [
                    'id' => $itemId,
                    'name' => ($itemModel = InvoiceExpenditureItem::findOne($itemId)) ? $itemModel->name : '---',
                    'amountFact' => 0,
                    'amountPlan' => 0
                ];
            }
            $items[$itemId]['amountPlan'] = round($amount / 100, 2);
        }

        uasort($items, function ($a, $b) {
            if ($a['amountFact'] == $b['amountFact']) return 0;
            return ($a['amountFact'] > $b['amountFact']) ? -1 : 1;
        });

        return $items ?: [['name' => 'Без расходов', 'amountFact' => 0, 'amountPlan' => 0]];
    }

    /**
     * @param bool $name
     * @return array
     */
    public function getChartStructureBySuppliers($dateFrom, $dateTo, $purse = null)
    {
        $exceptContractors = [
            CashContractorType::BANK_TEXT,
            CashContractorType::ORDER_TEXT,
            CashContractorType::EMONEY_TEXT,
            CashContractorType::BALANCE_TEXT,
            CashContractorType::COMPANY_TEXT,
        ];

        switch ($purse) {
            case 1:
                $flowArray = [CashBankFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case 2:
                $flowArray = [CashOrderFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            case 3:
                $flowArray = [CashEmoneyFlows::class];
                $planFlowArray = [PlanCashFlows::class];
                break;
            default:
                $flowArray = self::flowClassArray();
                $planFlowArray = [PlanCashFlows::class];
                break;
        }

        // 1. Fact
        $amountsFact = [];
        foreach ($flowArray as $flowClassName) {
            $flows = $flowClassName::find()
                ->select(['contractor_id', 'amount'])
                ->andWhere(['company_id' => $this->company->id])
                ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
                ->andWhere(['between', 'date', $dateFrom, $dateTo])
                ->andWhere(['not', ['contractor_id' => $exceptContractors]])
                ->andWhere(['not', ['contractor_id' => InvoiceExpenditureItem::ITEM_OWN_FOUNDS]])
                ->asArray()
                ->all();

            foreach ($flows as $f) {
                if (!isset($amountsFact[$f['contractor_id']])) {
                    $amountsFact[$f['contractor_id']] = 0;
                }
                $amountsFact[$f['contractor_id']] += $f['amount'];
            }
        }

        // 2. Plan
        $amountsPlan = [];
        foreach ($planFlowArray as $flowClassName) {
            $flows = $flowClassName::find()
                ->select(['contractor_id', 'amount'])
                ->andWhere(['company_id' => $this->company->id])
                ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE])
                ->andWhere(['between', 'date', $dateFrom, $dateTo])
                ->andWhere(['not', ['contractor_id' => $exceptContractors]])
                ->andWhere(['not', ['contractor_id' => InvoiceExpenditureItem::ITEM_OWN_FOUNDS]])
                ->andFilterWhere(['payment_type' => $purse]) // !
                ->asArray()
                ->all();

            foreach ($flows as $f) {
                if (!isset($amountsPlan[$f['contractor_id']])) {
                    $amountsPlan[$f['contractor_id']] = 0;
                }
                $amountsPlan[$f['contractor_id']] += $f['amount'];
            }
        }

        // 3. Names + merge amounts
        $items = [];
        foreach ($amountsFact as $itemId => $amount) {
            $items[$itemId] = [
                'id' => $itemId,
                'name' => ($itemModel = Contractor::findOne($itemId)) ? $itemModel->getTitle(true) : '---',
                'amountFact' => round($amount / 100, 2),
                'amountPlan' => 0
            ];
        }
        foreach ($amountsPlan as $itemId => $amount) {
            if (!isset($items[$itemId])) {
                $items[$itemId] = [
                    'id' => $itemId,
                    'name' => ($itemModel = Contractor::findOne($itemId)) ? $itemModel->getTitle(true) : '---',
                    'amountFact' => 0,
                    'amountPlan' => 0
                ];
            }
            $items[$itemId]['amountPlan'] = round($amount / 100, 2);
        }

        uasort($items, function ($a, $b) {
            if ($a['amountFact'] == $b['amountFact']) return 0;
            return ($a['amountFact'] > $b['amountFact']) ? -1 : 1;
        });

        return $items ?: [['name' => 'Без расходов', 'amountFact' => 0, 'amountPlan' => 0]];
    }


    public function getFromCurrentMonthsPeriods($left, $right, $offsetYear = 0, $offsetMonth = "0") {
        $start = new \DateTime();
        $curr = $start->modify("{$offsetYear} years")->modify("-{$left} month")->modify("{$offsetMonth} month");
        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {
            $ret[] = [
                'from' => $curr->format('Y-m-01'),
                'to' => $curr->format('Y-m-t'),
            ];
            $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }
}