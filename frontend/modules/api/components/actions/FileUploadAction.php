<?php

namespace frontend\modules\api\components\actions;

use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\document\Upd;
use common\models\employee\Employee;
use common\models\file;
use frontend\models\Documents;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\rest\Action;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Class UploadAction
 * @package frontend\modules\api\components\actions
 *
 * @example acton definition:
 * public function actions()
 * {
 *      return [
 *          'file-upload' => [
 *              'class' => \frontend\modules\api\components\actions\FileUploadAction::className(),
 *              'modelClass' => Model::className(),
 *          ],
 *      ];
 * }
 *
 * @example url path: /path/to/controller/get-file?id=1&filename=example.jpg
 *
 * @todo: create class/interface/behavior to detect `right` models.
 */
class FileUploadAction extends Action
{
    /**
     * PRIMARY_KEY field name
     * @var string
     */
    public $idField = 'id';

    /**
     * Whether to check company in getting model.
     * For example it is unnecessary for admin.
     * @var bool
     */
    public $strictByCompany = true;

    /**
     * @var int
     */
    public $maxFileCount = 3;

    /**
     * @var int
     */
    public $maxFileSize;

    /**
     * @var string
     */
    public $getFileAction = 'file-get';

    /**
     * @var string
     */
    public $uploadDirectory;

    /**
     * Runs the action.
     * This method displays the view requested by the user.
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function run($id)
    {
        $ownerModel = $this->findOwnerModel($id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $ownerModel);
        }

        $company = Yii::$app->user->identity->company;

        if (!$company->uploadAllowed()) {
            throw new ForbiddenHttpException("У вас закончилось место на диске для хранения сканов и документов. Использовано более 1 ГБ. Освободите место или перейдите на платный тариф.");
        }

        $fileCount = \common\models\file\File::find()->andWhere([
            'owner_model' => $ownerModel->className(),
            'owner_id' => $ownerModel->id,
        ])->count();
        $scanCount = \common\models\document\ScanDocument::find()->andWhere([
            'owner_model' => $ownerModel->className(),
            'owner_id' => $ownerModel->id,
        ])->count();

        if (($fileCount + $scanCount) >= $this->maxFileCount) {
            throw new ForbiddenHttpException('Максимальное количество файлов: ' . $this->maxFileCount . '.');
        }

        if ($this->maxFileSize !== null) {
            file\File::$maxFileSize = $this->maxFileSize;
        }
        $model = new file\File();
        $model->owner_model = $ownerModel->className();
        $model->owner_id = $ownerModel->id;
        $model->uploadDirectoryPath = $this->uploadDirectory ? : 'documents' . DIRECTORY_SEPARATOR . $ownerModel::$uploadDirectory;
        $model->file = UploadedFile::getInstanceByName('file');

        if ($model->save()) {
            if ($ownerModel->hasAttribute('has_file') && !$ownerModel->has_file) {
                $ownerModel->updateAttributes(['has_file' => true]);
            }
            $event = null;
            switch (true) {
                case $ownerModel instanceof Invoice:
                    $event = $ownerModel->type == Documents::IO_TYPE_IN ? 39 :
                            ($ownerModel->type == Documents::IO_TYPE_OUT ? 17 : null);
                    break;
                case $ownerModel instanceof Act:
                    $event = $ownerModel->type == Documents::IO_TYPE_IN ? 41 : null;
                    break;
                case $ownerModel instanceof PackingList:
                    $event = $ownerModel->type == Documents::IO_TYPE_IN ? 43 : null;
                    break;
                case $ownerModel instanceof InvoiceFacture:
                    $event = $ownerModel->type == Documents::IO_TYPE_IN ? 45 : null;
                    break;
            }
            if ($event) {
                \common\models\company\CompanyFirstEvent::checkEvent($company, $event);
            }

            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::toRoute([
                $this->getFileAction,
                'id' => $ownerModel->id,
                'fid' => $model->id,
            ], true));
        } else {
            $size = round(file\File::$maxFileSize / (1024*1024), 2);
            if (!$model->errors) {
                $model->addError('file', 'Ошибка при загрузке файла.' .
                    " Разрешена загрузка файлов размером до {$size}МБ и только со следующими расширениями: " .
                    $model->fileExtensions);
            }
        }

        return $model;
    }

    /**
     * @param $id
     * @return ActiveRecord
     * @throws NotFoundHttpException
     */
    public function findOwnerModel($id)
    {
        /* @var ActiveRecord $class */
        $class = $this->modelClass;
        $query = $class::find();

        if ($this->strictByCompany) {
            $query->byCompany(Yii::$app->user->identity->company->id);
        }

        $query->andWhere([
            $class::tableName() . '.' . $this->idField => $id,
        ]);

        $model = $query->one();

        if ($model === null) {
            throw new NotFoundHttpException('Document not found.');
        }

        return $model;
    }
}
