<?php

namespace frontend\modules\api\components\actions;

use common\models\file\FileHeaderHelper;
use common\models\ICompanyStrictQuery;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\rest\Action;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class FileGetAction
 * @package frontend\modules\api\components\actions
 *
 * @example acton definition:
 * public function actions()
 * {
 *      return [
 *          'get-file' => [
 *              'class' => \frontend\modules\api\components\actions\FileGetAction::className(),
 *              'modelClass' => Model::className(),
 *          ],
 *      ];
 * }
 *
 * @example url path: /path/to/controller/get-file?id=1&filename=example.jpg
 */
class FileGetAction extends Action
{
    /**
     * PRIMARY_KEY field name
     * @var string
     */
    public $idField = 'id';

    /**
     * Whether to check company in getting model.
     * For example it is unnecessary for admin.
     * @var bool
     */
    public $strictByCompany = true;

    /**
     * Runs the action.
     * This method displays the view requested by the user.
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function run($id, $fid)
    {
        $ownerModel = $this->findOwnerModel($id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $ownerModel);
        }

        $model = $this->findFileModel($fid, $ownerModel);
        $filePath = $model->getFilePath();

        if (!file_exists($filePath) || !is_readable($filePath)) {
            throw new NotFoundHttpException('File not found..');
        }

        return Yii::$app->response->sendFile($filePath, $model->filename_full, [
            'mimeType' => finfo_file(finfo_open(FILEINFO_MIME_TYPE), $filePath),
        ]);
    }

    /**
     * @param $id
     * @return ActiveRecord
     * @throws NotFoundHttpException
     */
    public function findOwnerModel($id)
    {
        /* @var ActiveRecord $class */
        $class = $this->modelClass;
        $query = $class::find();

        if ($this->strictByCompany) {
            $query->byCompany(Yii::$app->user->identity->company->id);
        }

        $query->andWhere([
            $class::tableName() . '.' . $this->idField => $id,
        ]);

        $model = $query->one();

        if ($model === null) {
            throw new NotFoundHttpException('Document not found.');
        }

        return $model;
    }

    /**
     * @param $fid
     * @param $ownerModel
     * @return File
     * @throws NotFoundHttpException
     */
    public function findFileModel($fid, $ownerModel)
    {
        $model = \common\models\file\File::findOne([
            'id' => $fid,
            'owner_id' => $ownerModel->id,
            'owner_model' => $ownerModel->className(),
        ]);

        if ($model === null) {
            throw new NotFoundHttpException('File not found.');
        }

        return $model;
    }
}
