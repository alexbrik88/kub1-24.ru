<?php

namespace frontend\modules\api\components\actions;

use common\models\document\ScanDocument;
use frontend\models\Documents;
use Yii;
use yii\db\ActiveRecord;
use yii\rest\Action;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class ScanBindAction
 *
 * @example acton definition:
 * public function actions()
 * {
 *      return [
 *          'scan-bind' => [
 *              'class' => \frontend\modules\api\components\actions\ScanBindAction::className(),
 *              'modelClass' => Model::className(),
 *          ],
 *      ];
 * }
 *
 */
class ScanBindAction extends Action
{
    /**
     * PRIMARY_KEY field name
     * @var string
     */
    public $idField = 'id';

    /**
     * Whether to check company in getting model.
     * For example it is unnecessary for admin.
     * @var bool
     */
    public $strictByCompany = true;

    /**
     * @var int
     */
    public $maxFileCount = 3;

    /**
     * Runs the action.
     * This method displays the view requested by the user.
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function run($id, $sid)
    {
        $ownerModel = $this->findOwnerModel($id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $ownerModel);
        }

        $model = $this->findScanModel($sid, $ownerModel);

        if ($model->getOwner() !== null) {
            throw new ForbiddenHttpException('Скан уже связан с документом.');
        }

        $fileCount = \common\models\file\File::find()->andWhere([
            'owner_model' => $ownerModel->className(),
            'owner_id' => $ownerModel->id,
        ])->count();
        $scanCount = \common\models\document\ScanDocument::find()->andWhere([
            'owner_model' => $ownerModel->className(),
            'owner_id' => $ownerModel->id,
        ])->count();

        if (($fileCount + $scanCount) >= $this->maxFileCount) {
            throw new ForbiddenHttpException('Максимальное количество файлов: ' . $this->maxFileCount . '.');
        }

        $model->updateAttributes([
            'owner_model' => $ownerModel->className(),
            'owner_table' => $ownerModel->tableName(),
            'owner_id' => $ownerModel->id,
            'contractor_id' => isset($ownerModel->contractor_id) ? $ownerModel->contractor_id : (
                isset($ownerModel->invoice) ? $ownerModel->invoice->contractor_id : null
            ),
            'document_type_id' => Documents::typeIdByClass($ownerModel->className()),
        ]);
        $ownerModel->updateHasFile();

        Yii::$app->getResponse()->setStatusCode(204);
    }

    /**
     * @param $id
     * @return ActiveRecord
     * @throws NotFoundHttpException
     */
    public function findOwnerModel($id)
    {
        /* @var ActiveRecord $class */
        $class = $this->modelClass;
        $query = $class::find();

        if ($this->strictByCompany) {
            $query->byCompany(Yii::$app->user->identity->company->id);
        }

        $query->andWhere([
            $class::tableName() . '.' . $this->idField => $id,
        ]);

        $model = $query->one();

        if ($model === null) {
            throw new NotFoundHttpException('Document not found.');
        }

        return $model;
    }

    /**
     * @param $fid
     * @param $ownerModel
     * @return File
     * @throws NotFoundHttpException
     */
    public function findScanModel($sid)
    {
        $model = \common\models\document\ScanDocument::findOne([
            'id' => $sid,
            'company_id' => Yii::$app->user->identity->company->id,
        ]);

        if ($model === null) {
            throw new NotFoundHttpException('Scan not found.');
        }

        return $model;
    }
}
