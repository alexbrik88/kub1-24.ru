<?php

namespace frontend\modules\api\components\actions;

use common\models\file\File;
use common\models\file\FileHeaderHelper;
use common\models\file\widgets\FileList;
use common\models\ICompanyStrictQuery;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\rest\Action;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class ListAction
 * @package frontend\modules\api\components\actions
 *
 * @example acton definition:
 * public function actions()
 * {
 *      return [
 *          'file-list' => [
 *              'class' => \frontend\modules\api\components\actions\FileListAction::className(),
 *              'modelClass' => Act::className(),
 *          ],
 *      ];
 * }
 *
 * @example url path: /path/to/controller/get-file?id=1&filename=example.jpg
 *
 * @todo: create class/interface/behavior to detect `right` models.
 */
class FileListAction extends Action
{
    /**
     * PRIMARY_KEY field name
     * @var string
     */
    public $idField = 'id';

    /**
     * Whether to check company in getting model.
     * For example it is unnecessary for admin.
     * @var bool
     */
    public $strictByCompany = true;

    /**
     * Runs the action.
     * This method displays the view requested by the user.
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function run($id)
    {
        $ownerModel = $this->findOwnerModel($id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $ownerModel);
        }

        return File::find()->andWhere([
            'owner_id' => $ownerModel->id,
            'owner_model' => $ownerModel->className(),
        ])->all();
    }

    /**
     * @param $id
     * @return ActiveRecord
     * @throws NotFoundHttpException
     */
    public function findOwnerModel($id)
    {
        /* @var ActiveRecord $class */
        $class = $this->modelClass;
        $query = $class::find();

        if ($this->strictByCompany) {
            $query->byCompany(Yii::$app->user->identity->company->id);
        }

        $query->andWhere([
            $class::tableName() . '.' . $this->idField => $id,
        ]);

        $model = $query->one();

        if ($model === null) {
            throw new NotFoundHttpException('Document not found.');
        }

        return $model;
    }
}
