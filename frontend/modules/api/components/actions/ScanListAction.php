<?php

namespace frontend\modules\api\components\actions;

use common\models\document\ScanDocument;
use Yii;
use yii\db\ActiveRecord;
use yii\rest\Action;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class ScanListAction
 *
 * @example acton definition:
 * public function actions()
 * {
 *      return [
 *          'scan-list' => [
 *              'class' => \frontend\modules\api\components\actions\ScanListAction::className(),
 *              'modelClass' => Model::className(),
 *          ],
 *      ];
 * }
 *
 */
class ScanListAction extends Action
{
    /**
     * PRIMARY_KEY field name
     * @var string
     */
    public $idField = 'id';

    /**
     * Whether to check company in getting model.
     * For example it is unnecessary for admin.
     * @var bool
     */
    public $strictByCompany = true;

    /**
     * Runs the action.
     * This method displays the view requested by the user.
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function run($id)
    {
        $ownerModel = $this->findOwnerModel($id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $ownerModel);
        }

        return ScanDocument::find()->andWhere([
            'owner_id' => $ownerModel->id,
            'owner_model' => $ownerModel->className(),
        ])->all();
    }

    /**
     * @param $id
     * @return ActiveRecord
     * @throws NotFoundHttpException
     */
    public function findOwnerModel($id)
    {
        /* @var ActiveRecord $class */
        $class = $this->modelClass;
        $query = $class::find();

        if ($this->strictByCompany) {
            $query->byCompany(Yii::$app->user->identity->company->id);
        }

        $query->andWhere([
            $class::tableName() . '.' . $this->idField => $id,
        ]);

        $model = $query->one();

        if ($model === null) {
            throw new NotFoundHttpException('Document not found.');
        }

        return $model;
    }
}
