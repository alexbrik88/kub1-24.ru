<?php

namespace frontend\modules\api\components\actions;

use common\models\document\ScanDocument;
use Yii;
use yii\db\ActiveRecord;
use yii\rest\Action;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class ScanUnbindAction
 *
 * @example acton definition:
 * public function actions()
 * {
 *      return [
 *          'scan-bind' => [
 *              'class' => \frontend\modules\api\components\actions\ScanUnbindAction::className(),
 *              'modelClass' => Model::className(),
 *          ],
 *      ];
 * }
 *
 */
class ScanUnbindAction extends Action
{
    /**
     * PRIMARY_KEY field name
     * @var string
     */
    public $idField = 'id';

    /**
     * Whether to check company in getting model.
     * For example it is unnecessary for admin.
     * @var bool
     */
    public $strictByCompany = true;

    /**
     * @var int
     */
    public $maxFileCount = 3;

    /**
     * Runs the action.
     * This method displays the view requested by the user.
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function run($id, $sid)
    {
        $ownerModel = $this->findOwnerModel($id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $ownerModel);
        }

        $model = $this->findScanModel($sid, $ownerModel);

        $model->updateAttributes([
            'owner_model' => null,
            'owner_table' => null,
            'owner_id' => null,
        ]);
        $ownerModel->updateHasFile();

        Yii::$app->getResponse()->setStatusCode(204);
    }

    /**
     * @param $id
     * @return ActiveRecord
     * @throws NotFoundHttpException
     */
    public function findOwnerModel($id)
    {
        /* @var ActiveRecord $class */
        $class = $this->modelClass;
        $query = $class::find();

        if ($this->strictByCompany) {
            $query->byCompany(Yii::$app->user->identity->company->id);
        }

        $query->andWhere([
            $class::tableName() . '.' . $this->idField => $id,
        ]);

        $model = $query->one();

        if ($model === null) {
            throw new NotFoundHttpException('Document not found.');
        }

        return $model;
    }

    /**
     * @param $fid
     * @param $ownerModel
     * @return File
     * @throws NotFoundHttpException
     */
    public function findScanModel($sid, $ownerModel)
    {
        $model = \common\models\document\ScanDocument::findOne([
            'id' => $sid,
            'company_id' => Yii::$app->user->identity->company->id,
        ]);

        if ($model === null) {
            throw new NotFoundHttpException('Scan not found.');
        }

        return $model;
    }
}
