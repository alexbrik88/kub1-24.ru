<?php

namespace frontend\modules\api\components;

use yii\web\AssetBundle;

/**
 * Class SwaggerUiAsset
 * @package frontend\modules\api\components
 */
class SwaggerUiAsset extends AssetBundle
{
    public $sourcePath = '@vendor/swagger-api/swagger-ui/dist';

    public $css = [
        'swagger-ui.css',
    ];
    public $js = [
        'swagger-ui-bundle.js',
        'swagger-ui-standalone-preset.js',
    ];
}
