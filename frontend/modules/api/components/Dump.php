<?php

namespace frontend\modules\api\components;

use Yii;
use yii\web\Request;
use yii\web\Response;

/**
 * Class SwaggerUiAsset
 * @package frontend\modules\api\components
 */
class Dump
{
    public static function request(Request $r)
    {
        $result = sprintf('%s %s %s', $r->method, $r->url, $_SERVER['SERVER_PROTOCOL'] ?? '')."\n";
        foreach ($r->getHeaders() as $name => $values) {
            $name = str_replace(' ', '-', ucwords(str_replace('-', ' ', $name)));
            foreach ($values as $value) {
                $result .= "$name: $value\n";
            }
        }
        if ($body = $r->rawBody) {
            $result .= "\n";
            $result .= $r->rawBody."\n";
        }

        return $result;
    }
    public static function response(Response $r, $withContent = true)
    {
        $result = sprintf('HTTP/%s %s %s', $r->version, $r->getStatusCode(), $r->statusText)."\n";
        foreach ($r->getHeaders() as $name => $values) {
            $name = str_replace(' ', '-', ucwords(str_replace('-', ' ', $name)));
            foreach ($values as $value) {
                $result .= "$name: $value\n";
            }
        }

        if ($r->content) {
            $result .= "\n";
            $result .= ($withContent ? $r->content : '[...]')."\n";
        }

        return $result;
    }
}
