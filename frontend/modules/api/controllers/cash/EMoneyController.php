<?php

namespace frontend\modules\api\controllers\cash;

use common\models\cash\CashFlowsBase;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\api\models\cash\CashEmoneySearch;
use frontend\rbac\permissions;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class EMoneyController
 * @package frontend\modules\api\controllers\cash
 */
class EMoneyController extends \yii\rest\ActiveController
{
    /**
     * @var string the model class name.
     */
    public $modelClass = 'common\models\cash\CashEmoneyFlows';
    /**
     * @var string the scenario used for creating a model.
     * @see \yii\base\Model::scenarios()
     */
    //public $createScenario = 'create';
    /**
     * @var string the scenario used for updating a model.
     * @see \yii\base\Model::scenarios()
     */
    //public $updateScenario = 'update';

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'prepareDataProvider' => [$this, 'prepareDataProvider'],
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view' => [
                'class' => 'yii\rest\ViewAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'create' => [
                'class' => 'common\components\rest\CreateAction',
                'modelClass' => $this->modelClass,
                'newModel' => [$this, 'newModel'],
                'scenario' => $this->createScenario,
                'checkAccess' => [$this, 'checkAccess'],
                'success' => function ($model) {
                    LogHelper::log($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE);
                },
            ],
            'update' => [
                'class' => 'common\components\rest\UpdateAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'scenario' => $this->updateScenario,
                'checkAccess' => [$this, 'checkAccess'],
                'success' => function ($model) {
                    LogHelper::log($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_UPDATE);
                },
            ],
            'delete' => [
                'class' => 'common\components\rest\DeleteAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'checkAccess' => [$this, 'checkAccess'],
                'success' => function ($model) {
                    LogHelper::log($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_DELETE);
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return array_merge(parent::verbs(), [
            'emoney' => ['GET'],
            'statistic' => ['GET'],
        ]);
    }

    /**
     * Checks the privilege of the current user.
     *
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string $action the ID of the action to be executed
     * @param object $model the model to be accessed. If null, it means no specific model is being accessed.
     * @param array $params additional parameters
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case 'index':
            case 'emoney':
            case 'statistic':
                $can = Yii::$app->getUser()->can(permissions\Cash::INDEX);
                break;
            case 'view':
                $can = Yii::$app->getUser()->can(permissions\Cash::VIEW);
                break;
            case 'create':
                $can = Yii::$app->getUser()->can(permissions\Cash::CREATE, ['model' => $model])
                    && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                break;
            case 'update':
            case 'status':
                $can = Yii::$app->getUser()->can(permissions\Cash::UPDATE, ['model' => $model])
                    && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                break;
            case 'delete':
                $can = Yii::$app->getUser()->can(permissions\Cash::DELETE, ['model' => $model]);
                break;
            default:
                $can = false;
                break;
        }

        if (!$can) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    /**
     * @return array
     */
    public function actionEmoney()
    {
        $this->checkAccess($this->action->id);

        return $this->module->getCompany()->getEmoneys()->all();
    }

    /**
     * @return array
     */
    public function actionStatistic()
    {
        $this->checkAccess($this->action->id);

        $period = $this->module->getPeriod('periodFrom', 'periodTo');

        $searchModel = $this->getSearchModel();
        $searchModel->beginAt = $period['from'];
        $searchModel->endAt = $period['to'];

        $dataProvider = $searchModel->search(['CashEmoneySearch' => Yii::$app->request->get()], [
            'from' => $period['from']->format('Y-m-d'),
            'to' => $period['to']->format('Y-m-d'),
        ]);

        return [
            'dateStart' => $searchModel->periodStartDate,
            'dateEnd' => $searchModel->periodEndDate,
            'balanceStart' => (int) $searchModel->getBalanceAtBegin($period['from']),
            'balanceEnd' => (int) $searchModel->getBalanceAtEnd($period['to']),
            'totalIncome' => (int) $searchModel->periodIncome,
            'totalExpense' => (int) $searchModel->periodExpense,
            'countIncome' => (int) $searchModel->countIncome,
            'countExpense' => (int) $searchModel->countExpense,
        ];
    }

    /**
     * @param yii\base\Action $action
     * @return ActiveDataProvider
     */
    public function prepareDataProvider($action)
    {
        $period = $this->module->getPeriod('periodFrom', 'periodTo');

        $searchModel = $this->getSearchModel();

        return $searchModel->search(['CashEmoneySearch' => Yii::$app->request->get()], [
            'from' => $period['from']->format('Y-m-d'),
            'to' => $period['to']->format('Y-m-d'),
        ]);
    }

    /**
     * @return CashEmoneySearch
     */
    public function getSearchModel()
    {
        return new CashEmoneySearch([
            'employeeCompany' => Yii::$app->user->identity->currentEmployeeCompany,
        ]);
    }

    /**
     * @param $id
     * @return Act
     * @throws NotFoundHttpException
     */
    public function findModel($id)
    {
        $model = $this->modelClass::find()->andWhere([
            'id' => $id,
            'company_id' => $this->module->getCompany()->id,
        ])->one();

        if ($model === null) {
            throw new NotFoundHttpException('Model loading failed. Model not found.');
        }

        return $model;
    }

    /**
     * @param string $scenario
     * @param yii\base\Action $action
     * @return yii\db\ActiveRecord
     */
    public function newModel($scenario, $action)
    {
        return new $this->modelClass([
            'company_id' => $this->module->getCompany()->id,
            'flow_type' => $this->module->getParameter('flow_type', [
                CashFlowsBase::FLOW_TYPE_INCOME,
                CashFlowsBase::FLOW_TYPE_EXPENSE,
            ]),
        ]);
    }
}
