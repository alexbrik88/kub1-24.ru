<?php

namespace frontend\modules\api\controllers\cash;

use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\employee\Employee;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\cash\models\CashContractorType;
use frontend\modules\api\models\cash\CashOrderSearch;
use frontend\rbac\permissions;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class OrderController
 * @package frontend\modules\api\controllers\cash
 */
class OrderController extends \yii\rest\ActiveController
{
    /**
     * @var string the model class name.
     */
    public $modelClass = 'common\models\cash\CashOrderFlows';
    /**
     * @var string the scenario used for creating a model.
     * @see \yii\base\Model::scenarios()
     */
    //public $createScenario = 'create';
    /**
     * @var string the scenario used for updating a model.
     * @see \yii\base\Model::scenarios()
     */
    //public $updateScenario = 'update';

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'prepareDataProvider' => [$this, 'prepareDataProvider'],
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view' => [
                'class' => 'yii\rest\ViewAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'create' => [
                'class' => 'common\components\rest\CreateAction',
                'modelClass' => $this->modelClass,
                'newModel' => [$this, 'newModel'],
                'scenario' => $this->createScenario,
                'checkAccess' => [$this, 'checkAccess'],
                'success' => function ($model) {
                    LogHelper::log($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE);
                },
            ],
            'update' => [
                'class' => 'common\components\rest\UpdateAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'scenario' => $this->updateScenario,
                'checkAccess' => [$this, 'checkAccess'],
                'success' => function ($model) {
                    LogHelper::log($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_UPDATE);
                },
            ],
            'delete' => [
                'class' => 'common\components\rest\DeleteAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'checkAccess' => [$this, 'checkAccess'],
                'success' => function ($model) {
                    LogHelper::log($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_DELETE);
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return array_merge(parent::verbs(), [
            'statistic' => ['GET'],
        ]);
    }

    /**
     * Checks the privilege of the current user.
     *
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string $action the ID of the action to be executed
     * @param object $model the model to be accessed. If null, it means no specific model is being accessed.
     * @param array $params additional parameters
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case 'index':
            case 'cashbox':
            case 'statistic':
            case 'self-contractors':
                $can = Yii::$app->getUser()->can(permissions\CashOrder::INDEX);
                break;
            case 'view':
                $can = Yii::$app->getUser()->can(permissions\CashOrder::VIEW);
                break;
            case 'create':
                $can = Yii::$app->getUser()->can(permissions\CashOrder::CREATE, ['model' => $model])
                    && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                break;
            case 'update':
            case 'status':
                $can = Yii::$app->getUser()->can(permissions\CashOrder::UPDATE, ['model' => $model])
                    && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                break;
            case 'delete':
                $can = Yii::$app->getUser()->can(permissions\CashOrder::DELETE, ['model' => $model]);
                break;
            default:
                $can = false;
                break;
        }

        if (!$can) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    /**
     * @return array
     */
    public function actionCashbox()
    {
        $this->checkAccess($this->action->id);

        return Yii::$app->user->identity->getCashboxes()->all();
    }

    /**
     * @return array
     */
    public function actionStatistic()
    {
        $this->checkAccess($this->action->id);

        $period = $this->module->getPeriod('periodFrom', 'periodTo');

        $searchModel = $this->getSearchModel();
        $searchModel->beginAt = $period['from'];
        $searchModel->endAt = $period['to'];

        $dataProvider = $searchModel->search(['CashOrderSearch' => Yii::$app->request->get()], [
            'from' => $period['from']->format('Y-m-d'),
            'to' => $period['to']->format('Y-m-d'),
        ]);

        return [
            'dateStart' => $searchModel->periodStartDate,
            'dateEnd' => $searchModel->periodEndDate,
            'balanceStart' => (int) $searchModel->getBalanceAtBegin($period['from']),
            'balanceEnd' => (int) $searchModel->getBalanceAtEnd($period['to']),
            'totalIncome' => (int) $searchModel->periodIncome,
            'totalExpense' => (int) $searchModel->periodExpense,
            'countIncome' => (int) $searchModel->countIncome,
            'countExpense' => (int) $searchModel->countExpense,
        ];
    }

    /**
     * @return array
     */
    public function actionSelfContractors()
    {
        $result = [];

        $flow_type = Yii::$app->request->get('flow_type');
        $exclude_cashbox = Yii::$app->request->get('exclude_cashbox');

        $cashboxList = $this->module->getCompany()->getCashboxes()
            ->select(['name'])
            ->andWhere(['is_closed' => false])
            ->andFilterWhere(['not', ['id' => $exclude_cashbox]])
            ->orderBy([
                'is_main' => SORT_DESC,
                'name' => SORT_ASC,
            ])
            ->indexBy('id')
            ->column();

        $selfFlowQuery = CashContractorType::find()
            ->andWhere(['not', ['name'=> CashContractorType::COMPANY_TEXT]])
            ->andWhere(['not', ['name'=> CashContractorType::ORDER_TEXT]]);
        if (isset($flow_type) && $flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE) {
            $selfFlowQuery->andWhere(['not', ['name'=> CashContractorType::BALANCE_TEXT]]);
        }
        $selfFlowResult = $selfFlowQuery->select(['text', 'name'])->indexBy('name')->column();

        if (count($cashboxList) > 1) {
            $result += array_slice($selfFlowResult, 0, 1, true);
            foreach ($cashboxList as $key => $value) {
                $optionKey = CashContractorType::ORDER_TEXT . '.' . $key;
                $result[$optionKey] = $value;
            }
            $result += array_slice($selfFlowResult, 1, null, true);
        }  else {
            $result += $selfFlowResult;
        }

        $data = [];
        foreach ($result as $key => $value) {
            $data[] = [
                'id' => $key,
                'name' => $value,
            ];
        }

        return $data;
    }

    /**
     * @param yii\base\Action $action
     * @return ActiveDataProvider
     */
    public function prepareDataProvider($action)
    {
        $period = $this->module->getPeriod('periodFrom', 'periodTo');

        return $this->getSearchModel()->search(['CashOrderSearch' => Yii::$app->request->get()], [
            'from' => $period['from']->format('Y-m-d'),
            'to' => $period['to']->format('Y-m-d'),
        ]);
    }

    /**
     * @return CashOrderSearch
     */
    public function getSearchModel()
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $cashboxIds = $user->getCashboxes()->select('id')->column();
        $cashbox = $this->module->getParameter('cashbox', $cashboxIds, $cashboxIds);

        return new CashOrderSearch([
            'employeeCompany' => $user->currentEmployeeCompany,
            'company_id' => $user->company->id,
            'cashbox_id' => $cashbox,
        ]);
    }

    /**
     * @param $id
     * @return Act
     * @throws NotFoundHttpException
     */
    public function findModel($id)
    {
        $model = $this->modelClass::find()->andWhere([
            'id' => $id,
            'company_id' => $this->module->getCompany()->id,
        ])->one();

        if ($model === null) {
            throw new NotFoundHttpException('Model loading failed. Model not found.');
        }

        return $model;
    }

    /**
     * @param string $scenario
     * @param yii\base\Action $action
     * @return yii\db\ActiveRecord
     */
    public function newModel($scenario, $action)
    {
        return new $this->modelClass([
            'author_id' => Yii::$app->user->identity->id,
            'company_id' => $this->module->getCompany()->id,
            'flow_type' => $this->module->getParameter('flow_type', [
                CashFlowsBase::FLOW_TYPE_INCOME,
                CashFlowsBase::FLOW_TYPE_EXPENSE,
            ]),
        ]);
    }
}
