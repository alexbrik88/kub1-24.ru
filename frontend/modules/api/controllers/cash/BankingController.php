<?php

namespace frontend\modules\api\controllers\cash;

use Yii;

/**
 * Class BankingController
 * @package frontend\modules\api\controllers\cash
 */
class BankingController extends \yii\web\Controller
{
    public $layout = 'banking';

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\QueryParamAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [\frontend\rbac\UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return array
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
