<?php

namespace frontend\modules\api\controllers;

use common\models\document\ScanDocument;
use frontend\models\Documents;
use frontend\modules\documents\models\ScanDocumentSearch;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * ScanDocumentController implements the CRUD actions for ScanDocument model.
 */
class ScanDocumentController extends \yii\rest\ActiveController
{
    /**
     * @var string the model class name.
     */
    public $modelClass = ScanDocument::class;
    /**
     * @var string the scenario used for creating a model.
     * @see \yii\base\Model::scenarios()
     */
    public $createScenario = ScanDocument::SCENARIO_CREATE;

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
        ]);
    }

    /**
     * Checks the privilege of the current user.
     *
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string $action the ID of the action to be executed
     * @param object $model the model to be accessed. If null, it means no specific model is being accessed.
     * @param array $params additional parameters
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case 'index':
                $can = Yii::$app->getUser()->can(permissions\document\Document::INDEX);
                break;
            case 'view':
                $can = Yii::$app->getUser()->can(permissions\document\Document::VIEW, [
                    'model' => $model,
                ]);
                break;
            case 'create':
                $can = Yii::$app->getUser()->can(permissions\document\Document::CREATE);
                break;
            case 'update':
                $can = Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                    'model' => $model,
                ]);
                break;
            case 'delete':
                $can = Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
                    'model' => $model,
                ]);
                break;
            case 'type-list':
                $can = true;
                break;
            default:
                $can = false;
                break;
        }

        if (!$can) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH'],
            'delete' => ['DELETE'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'prepareDataProvider' => [$this, 'prepareDataProvider'],
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view' => [
                'class' => 'yii\rest\ViewAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'create' => [
                'class' => 'common\components\rest\CreateAction',
                'modelClass' => $this->modelClass,
                'newModel' => [$this, 'newModel'],
                'scenario' => $this->createScenario,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'update' => [
                'class' => 'yii\rest\UpdateAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'scenario' => $this->updateScenario,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'delete' => [
                'class' => 'yii\rest\DeleteAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
        ];
    }

    /**
     * @param yii\base\Action $action
     * @return ActiveDataProvider
     */
    public function prepareDataProvider($action)
    {
        $searchModel = new ScanDocumentSearch([
            'company_id' => $this->module->getCompany()->id,
        ]);

        return $searchModel->search(['ScanDocumentSearch' => Yii::$app->request->get()]);
    }

    /**
     * @param string $scenario
     * @param yii\base\Action $action
     * @return yii\db\ActiveRecord
     */
    public function newModel($scenario, $action)
    {
        return new $this->modelClass([
            'scenario' => $scenario,
            'company_id' => $this->module->getCompany()->id,
            'employee_id' => Yii::$app->user->id,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionFile($id)
    {
        $model = $this->findModel($id);
        $file = $model->file ? $model->file->getPath() : null;

        if ($file && is_file($file)) {
            $options = ['inline' => false];
            if ($model->file->ext == 'pdf') {
                $options['mimeType'] = 'application/pdf';
            }

            return \Yii::$app->response->sendFile($file, $model->file->filename_full, $options);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionTypeList()
    {
        $items = [];
        foreach (Documents::mapIdName() as $key => $value) {
            $items[] = [
                'id' => $key,
                'name' => $value,
            ];
        }

        return $items;
    }

    /**
     * Finds the ScanDocument model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ScanDocument the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
        $model = ScanDocument::findOne([
            'id' => $id,
            'company_id' => $this->module->getCompany()->id,
        ]);

        if ($model !== null) {
            $model->file;

            return $model;
        } else {
            throw new NotFoundHttpException("Object not found: $id");
        }
    }
}
