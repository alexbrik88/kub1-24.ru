<?php

namespace frontend\modules\api\controllers;

use common\components\date\DateHelper;
use common\models\Chat;
use common\models\chat\CrmUser;
use common\models\ChatVolume;
use common\models\Company;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use php_rutils\RUtils;
use WAMP\WAMPClient;
use Yii;
use yii\base\Exception;
use yii\bootstrap\Html;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Class ChatController
 * @package frontend\modules\api\controllers
 */
class ChatController extends \yii\rest\Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
        ]);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionSubscribers()
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $subscribers = [];
        $newCount = $this->newCount($user);

        if (($crm = $this->getCrmAccountant($user)) !== null) {
            $id = 'crm-' . $crm['user_id'];
            $lastMessageDate = Chat::find()->andWhere(['or',
                ['and',
                    ['from_employee_id' => $user->id],
                    ['to_employee_id' => $id],
                ],
                ['and',
                    ['from_employee_id' => $id],
                    ['to_employee_id' => $user->id],
                ],
            ])->max('created_at');
            $lastMessageDate = $lastMessageDate ? date(DateHelper::FORMAT_USER_DATE, $lastMessageDate) : null;

            $subscribers[] = [
                'id' => $id,
                'type' => 'accountant',
                'name' => $crm['user_fio'],
                'phone' => '',
                'position' => 'Бухгалтер CRM',
                'imgSrc' => (YII_ENV_DEV ? "http" : "https") . "://{$crm['chat_photo']}",
                'newCount' => ArrayHelper::getValue($newCount, $id, 0),
                'unreadMessagesCount' => $user->getUnreadMessagesCount($id),
                'mainPosition' => 'Бухгалтер',
                'lastMessageDate' => $lastMessageDate,
            ];
        }

        $employeeCompanies = $this->employeeCompanyQuery($user)
            ->orderBy(['lastname' => SORT_ASC, 'firstname' => SORT_ASC, 'patronymic' => SORT_ASC])
            ->all();

        /* @var $value EmployeeCompany */
        foreach ($employeeCompanies as $value) {
            $lastMessageDate = Chat::find()->andWhere(['or',
                ['and',
                    ['from_employee_id' => $user->id],
                    ['to_employee_id' => $value->employee_id],
                ],
                ['and',
                    ['from_employee_id' => $value->employee_id],
                    ['to_employee_id' => $user->id],
                ],
            ])->max('created_at');
            $lastMessageDate = $lastMessageDate ? date(DateHelper::FORMAT_USER_DATE, $lastMessageDate) : null;

            $subscribers[] = [
                'id' => $value->employee_id,
                'type' => $value->employee_id == Employee::SUPPORT_KUB_EMPLOYEE_ID ? 'support' : 'employee',
                'name' => $value->getFio(),
                'phone' => $value->phone ?: '',
                'position' => $value->employeeRole->name,
                'imgSrc' => Yii::$app->urlManager->createAbsoluteUrl('/frontend/web/img/chat/' . $value->employee->chat_photo),
                'newCount' => ArrayHelper::getValue($newCount, $value->employee_id, 0),
                'unreadMessagesCount' => $user->getUnreadMessagesCount($value->employee_id),
                'mainPosition' => $value->employee_id == Employee::SUPPORT_KUB_EMPLOYEE_ID ? 'Менеджер технической поддержки' : 'Сотрудники',
                'lastMessageDate' => $lastMessageDate,
            ];
        }

        return [
            'result' => true,
            'subscribers' => $subscribers,
            'newCount' => $this->newCount($user),
        ];
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionNewCount()
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        return [
            'result' => true,
            'newCount' => $this->newCount($user),
        ];
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionMessages()
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $afterID = Yii::$app->request->post('afterID');
        $beforeID = Yii::$app->request->post('beforeID');

        if (($subscriberID = Yii::$app->request->post('subscriberID')) === null) {
            return [
                'result' => false,
                'errors' => ['The parameter "subscriberID" is required.']
            ];
        }

        return [
            'result' => true,
            'messages' => $this->getMessages($user->id, $subscriberID, $afterID, $beforeID),
        ];
    }

    /**
     *
     */
    public function actionViewMessage()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $messageID = Yii::$app->request->post('messageID');
        $chat = Chat::findOne($messageID);
        if ($chat) {
            $chat->is_new = 0;
            $chat->is_viewed = true;
            $chat->save(true, ['is_new', 'is_viewed']);
            $client = new WAMPClient('http://127.0.0.1:' . Yii::$app->params['chat']['port']);
            $client->connect();
            $client->call('send', [
                'chat' => $chat,

            ]);
            $client->disconnect();
        }
    }

    /**
     * @return array
     */
    public function actionSendMessage()
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        if (($subscriberID = Yii::$app->request->post('subscriberID')) === null) {
            return [
                'result' => false,
                'errors' => ['The parameter "subscriberID" is required.']
            ];
        }

        if (($message = Yii::$app->request->post('message')) === null) {
            return [
                'result' => false,
                'errors' => ['The parameter "message" is required.']
            ];
        }

        if ($this->isSubscriberExists($user, $subscriberID)) {
            $chat = new Chat();
            $chat->from_employee_id = $user->id;
            $chat->to_employee_id = $subscriberID;
            $chat->message = $message;
            $chat->is_new = true;
            $chat->is_viewed = false;

            if ($chat->save()) {
                $chatVolume = $user->currentEmployeeCompany->getChatVolume($subscriberID);
                $result = [
                    'result' => true,
                    'id' => $chat->id,
                    'action_type' => 'ajax',
                    'name' => $chat->fromEmployee->getFio(),
                    'from_user_id' => $user->id,
                    'to_user_id' => $subscriberID,
                    'createdAt' => date('H:i', $chat->created_at),
                    'message' => $chat->message,
                    'notificationTime' => RUtils::dt()->ruStrFTime([
                        'date' => $chat->created_at,
                        'format' => 'd F',
                        'monthInflected' => true,
                    ]),
                    'notificationMessage' => strip_tags($chat->message),
                    'messageID' => $chat->id,
                    'fromEmployeePhoto' => $user->getChatPhotoSrc(),
                    'fromEmployeeHasSong' => $chatVolume ? $chatVolume->has_volume : null,
                    'messageType' => Chat::TYPE_MESSAGE,
                ];

                $this->sendBySocket($result);

                if (stristr($subscriberID, 'crm') !== false) {
                    $employeeChief = $user->company->getEmployeeChief();
                    $domain = YII_ENV_DEV ? 'http://devbuh.kub-24.ru/' : 'https://buh.kub-24.ru/';
                    $curl = curl_init($domain . 'chat/send-from-kub');
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query([
                        'Message' => [
                            'from_employee_id' => $user->id,
                            'to_employee_id' => preg_replace("/[^0-9]/", '', $subscriberID),
                            'message' => $chat->message,
                        ],
                        'User' => [
                            'user_fio' => $user->company->getChiefFio(),
                            'user_id' => $employeeChief->id,
                            'company_name' => $user->company->getShortName(),
                            'chat_photo' => $employeeChief->chat_photo,
                            'is_online' => $employeeChief->isOnline(),
                        ],
                    ]));
                    curl_exec($curl);
                }

                return [
                    'result' => true,
                    'messageId' => $chat->id,
                ];
            }
        }

        return ['result' => false,];
    }

    /**
     * @param $toEmployeeID
     * @return array
     */
    public function actionSendFile()
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        if (($subscriberID = Yii::$app->request->post('subscriberID')) === null) {
            return [
                'result' => false,
                'errors' => ['The parameter "subscriberID" is required.']
            ];
        }

        if (($file = UploadedFile::getInstanceByName('file')) === null) {
            return [
                'result' => false,
                'errors' => ['The parameter "file" is required.']
            ];
        } elseif ($file->size > 1024 * 1024 * 5) {
            return [
                'result' => false,
                'errors' => ['The maximum file size is 5 MB.']
            ];
        }

        if ($this->isSubscriberExists($user, $subscriberID)) {
            $chat = new Chat();
            $chat->from_employee_id = $user->id;
            $chat->to_employee_id = $subscriberID;
            $chat->message = $file->name;
            $chat->is_file = true;
            $chat->is_new = true;
            $chat->is_viewed = false;

            if ($chat->save() && $file->saveAs($chat->getUploadPath() . DIRECTORY_SEPARATOR . $file->name)) {
                $chatVolume = $user->currentEmployeeCompany->getChatVolume($subscriberID);
                $result = [
                    'result' => true,
                    'sendToSocket' => false,
                    'id' => $chat->id,
                    'action_type' => 'ajax',
                    'name' => $chat->fromEmployee->getFio(),
                    'from_user_id' => $user->id,
                    'to_user_id' => $subscriberID,
                    'createdAt' => date('H:i', $chat->created_at),
                    'message' => $chat->message,
                    'notificationTime' => RUtils::dt()->ruStrFTime([
                        'date' => $chat->created_at,
                        'format' => 'd F',
                        'monthInflected' => true,
                    ]),
                    'notificationMessage' => strip_tags($chat->message),
                    'messageID' => $chat->id,
                    'fromEmployeePhoto' => $user->getChatPhotoSrc(),
                    'fromEmployeeHasSong' => $chatVolume ? $chatVolume->has_volume : null,
                    'messageType' => Chat::TYPE_FILE,
                ];

                $this->sendBySocket($result);

                if (stristr($subscriberID, 'crm') !== false) {
                    $curl = curl_init('http://devbuh.kub-24.ru/chat/send-from-kub');
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query([
                        'Message' => [
                            'from_employee_id' => $user->id,
                            'to_employee_id' => preg_replace("/[^0-9]/", '', $subscriberID),
                            'message' => Html::a($file->name, '//' . Yii::$app->request->serverName . Url::to(['/chat/download-file', 'id' => $chat->id, 'crmID' => $subscriberID])),
                            'id' => $chat->id,
                        ],
                        'User' => [
                            'user_fio' => $user->getFio(),
                            'user_id' => $user->id,
                            'company_name' => $user->company->getTitle(),
                            'chat_photo' => $user->chat_photo,
                            'is_online' => $user->isOnline(),
                        ],
                    ]));
                    curl_exec($curl);
                }

                return [
                    'result' => true,
                    'messageId' => $chat->id,
                ];
            }
        }

        return ['result' => false];
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionGetFile($id)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        $model = Chat::findOne([
            'and',
            ['id' => $id],
            [
                'or',
                ['from_employee_id' => $user->id],
                ['to_employee_id' => $user->id],
            ]
        ]);

        if ($model !== null) {
            $uploadPath = $model->getUploadPath();
            if ($handle = opendir($uploadPath)) {
                while (false !== ($file = readdir($handle))) {
                    if (!in_array($file, ['..', '.'])) {
                        $filePath = $uploadPath . DIRECTORY_SEPARATOR . $file;

                        if (is_file($filePath)) {
                            return \Yii::$app->response->sendFile($filePath, $file)->send();
                        }
                    }
                }
            }
        }

        throw new NotFoundHttpException('Файл не найден');
    }

    /**
     * @param $userID
     * @return Chat[]
     */
    protected function isSubscriberExists(Employee $user, $subscriberID)
    {
        if ($user->id != $subscriberID) {
            if ($this->employeeCompanyQuery($user)->andWhere(['employee_id' => $subscriberID])->exists()) {
                return true;
            }

            if (($crm = $this->getCrmAccountant($user)) !== null && $subscriberID == 'crm-' . $crm['user_id']) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $userID
     * @return Chat[]
     */
    protected function getMessages($userID, $subscriberID, $afterID = null, $beforeID = null)
    {
        $query = Chat::find()->andWhere(['or',
            ['and',
                ['from_employee_id' => $subscriberID],
                ['to_employee_id' => $userID],
            ],
            ['and',
                ['from_employee_id' => $userID],
                ['to_employee_id' => $subscriberID],
            ],
        ]);
        $query->orderBy(['id' => SORT_DESC]);

        if ($afterID) {
            $query
                ->andWhere(['>', 'id', $afterID])
                ->orderBy(['id' => SORT_ASC]);
        } elseif ($beforeID) {
            $query->andWhere(['<', 'id', $beforeID]);
        }

        $result = $query->limit(50)->all();

        if ($afterID === null) {
            $result = array_reverse($result);
        }

        return $result;
    }

    /**
     * @param $message array
     * @return Chat[]
     */
    protected function employeeCompanyQuery($user)
    {
        return EmployeeCompany::find()->joinWith(['employee'], false)
            ->andWhere([
                'or',
                ['employee_company.company_id' => $user->company->id],
                [
                    'employee_company.company_id' => Yii::$app->params['service']['company_id'],
                    'employee_company.employee_id' => Employee::SUPPORT_KUB_EMPLOYEE_ID,
                ],
            ])
            ->andWhere(['employee_company.is_working' => 1])
            ->andWhere(['employee.is_deleted' => 0])
            ->andWhere(['not', ['employee.id' => $user->id]]);
    }

    /**
     * @param $message array
     * @return Chat[]
     */
    protected function newCount($user)
    {
        return Chat::find()
            ->select(['count' => 'COUNT([[to_employee_id]])'])
            ->where([
                'to_employee_id' => $user->id,
                'is_viewed' => false,
            ])
            ->groupBy('from_employee_id')
            ->indexBy('from_employee_id')
            ->column();
    }

    /**
     * @param $message array
     * @return Chat[]
     */
    protected function sendBySocket($message)
    {
        try {
            $client = new WAMPClient('http://127.0.0.1:' . Yii::$app->params['chat']['port']);
            $client->connect();
            $client->call('send', $message);
            $client->disconnect();
        } catch (\Exception $e) {
        } catch (\Throwable $e) {
        }
    }

    /**
     * @param Employee $user
     * @return mixed
     */
    protected function getCrmAccountant(Employee $user)
    {
        $domain = YII_ENV_DEV ? 'http://devbuh.kub-24.ru/' : 'https://buh.kub-24.ru/';
        $curl = curl_init($domain . 'site/get-accountant-to-kub-client');
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query([
            'Company' => [
                'inn' => $user->company->inn,
                'kpp' => $user->company->kpp,
            ],
        ]));
        $out = curl_exec($curl);
        $accountant = @unserialize($out) ? : null;
        if ($accountant) {
            if (($existsCrmUser = CrmUser::find()->where(['user_id' => $accountant['user_id']])->one()) == null) {
                $existsCrmUser = new CrmUser();
            }
            foreach ($accountant as $key => $value) {
                $existsCrmUser->$key = $value;
            }
            $existsCrmUser->save();

            if (!ChatVolume::find()->andWhere(['and',
                ['from_employee_id' => $user->id],
                ['to_employee_id' => 'crm-' . $accountant['user_id']],
            ])->exists()
            ) {
                $chatVolume = new ChatVolume();
                $chatVolume->company_id = $user->company->id;
                $chatVolume->from_employee_id = $user->id;
                $chatVolume->to_employee_id = 'crm-' . $accountant['user_id'];
                $chatVolume->has_volume = true;
                $chatVolume->save();
            }
        }

        return $accountant;
    }
}
