<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 25.12.2017
 * Time: 11:00
 */

namespace frontend\modules\api\controllers;

use common\models\employee\Employee;
use common\models\employee\EmployeeDevice;
use frontend\models\ChangeDuplicateNotifyForm;
use frontend\models\ChangeEmailForm;
use frontend\models\ChangeNotifyForm;
use frontend\models\ChangePasswordForm;
use frontend\models\ChangePushNotifyForm;
use frontend\models\ChangePushNoveltyForm;
use frontend\models\ChangeTimeZoneForm;
use Yii;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

/**
 * Class ProfileController
 * @package frontend\modules\api\controllers
 */
class ProfileController extends \yii\rest\Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'view' => ['GET', 'HEAD'],
            'update' => ['PUT', 'PATCH'],
            'current-time-zone' => ['GET', 'HEAD'],
            'current-email' => ['GET', 'HEAD'],
            'change-time-zone' => ['PUT', 'PATCH'],
        ];
    }

    /**
     * @return Employee
     */
    public function actionView()
    {
        return  Yii::$app->user->identity;
    }

    /**
     * @return Employee
     */
    public function actionUpdate()
    {
        $model = Yii::$app->user->identity;
        $model->scenario = Employee::SCENARIO_UPDATE;
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->save() === false && !$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        }

        return $model;
    }

    /**
     * @return array
     */
    public function actionCurrentTimeZone()
    {
        return  Yii::$app->user->identity->getTimeZone()
                ->select(['id', 'out_time_zone', 'time_zone'])
                ->asArray()
                ->one();
    }

    /**
     * @return array
     */
    public function actionChangeTimeZone()
    {
        $model = new ChangeTimeZoneForm([], Yii::$app->user->identity);

        $model->load(Yii::$app->request->post());

        if ($model->validate() && $model->save()) {
            return [
                'result' => true,
            ];
        }

        return [
            'result' => false,
            'errors' => $model->getErrors(),
        ];
    }

    /**
     * @return array
     */
    public function actionCurrentEmail()
    {
        return [
            'email' => Yii::$app->user->identity->email,
        ];
    }

    /**
     * @return array
     */
    public function actionChangeEmail()
    {
        $model = new ChangeEmailForm(Yii::$app->user->identity);

        $model->load(Yii::$app->request->post());

        if ($model->validate() && $model->save()) {
            return [
                'result' => true,
            ];
        }

        return [
            'result' => false,
            'errors' => $model->getErrors(),
        ];
    }

    /**
     * @return array
     */
    public function actionCurrentNotify()
    {
        $user = Yii::$app->user->identity;

        if ($user) {
            return [
                'result' => true,
                'push_notification_new_message' => $user->push_notification_new_message,
                'push_notification_create_closest_document' => $user->push_notification_create_closest_document,
                'push_notification_overdue_invoice' => $user->push_notification_overdue_invoice,
                'duplicate_notification_to_sms' => $user->duplicate_notification_to_sms,
                'duplicate_notification_to_email' => $user->duplicate_notification_to_email,
            ];
        }

        return [
            'result' => false,
        ];
    }

    /**
     * @return array
     */
    public function actionChangeNotify()
    {
        $changePushNotifyForm = new ChangePushNotifyForm([], Yii::$app->user->identity);
        $changeDuplicateNotifyForm = new ChangeDuplicateNotifyForm([], Yii::$app->user->identity);

        $changePushNotifyForm->load(Yii::$app->request->post());
        $changeDuplicateNotifyForm->load(Yii::$app->request->post());

        if (
            $changePushNotifyForm->validate() &&
            $changeDuplicateNotifyForm->validate() &&
            $changePushNotifyForm->savePushNotify() &&
            $changeDuplicateNotifyForm->saveNotify()
        ) {
            return [
                'result' => true,
            ];
        }

        return [
            'result' => false,
            'errors' => array_merge($changePushNotifyForm->getErrors(), $changeDuplicateNotifyForm->getErrors()),
        ];
    }

    /**
     * @return array
     */
    public function actionChangeDuplicateNotify()
    {
        $model = new ChangeDuplicateNotifyForm([], Yii::$app->user->identity);

        $model->load(Yii::$app->request->post());

        if ($model->validate() && $model->saveNotify()) {
            return [
                'result' => true,
            ];
        }

        return [
            'result' => false,
            'errors' => $model->getErrors(),
        ];
    }

    /**
     * @return array
     */
    public function actionChangePassword()
    {
        $model = new ChangePasswordForm([], Yii::$app->user->identity);

        $model->load(Yii::$app->request->post());

        if ($model->save()) {
            return [
                'result' => true,
            ];
        }

        return [
            'result' => false,
            'errors' => $model->getErrors(),
        ];
    }

    /**
     * @return array
     */
    public function actionSetPushToken()
    {
        $pushToken = Yii::$app->request->post('push_token');
        $user = Yii::$app->user->identity;

        if ($pushToken) {
            $user->push_token = $pushToken;

            if ($user->save(true, ['push_token'])) {
                return [
                    'result' => true,
                ];
            }

            return [
                'result' => false,
                'errors' => $user->getErrors(),
            ];
        }

        return [
            'result' => false,
        ];
    }

    /**
     * @return array
     */
    public function actionAddDeviceId()
    {
        $user = Yii::$app->user->identity;
        $deviceID = Yii::$app->request->post('device_id');

        if ($user && $deviceID) {
            if (EmployeeDevice::find()->andWhere(['and',
                ['employee_id' => $user->id],
                ['device_id' => $deviceID],
            ])->exists()
            ) {
                return [
                    'result' => true,
                ];
            }

            $employeeDevice = new EmployeeDevice();
            $employeeDevice->employee_id = $user->id;
            $employeeDevice->device_id = $deviceID;
            if ($employeeDevice->save()) {
                return [
                    'result' => true,
                ];
            }

            return [
                'result' => false,
                'errors' => $employeeDevice->getErrors(),
            ];
        }

        return [
            'result' => false,
        ];
    }
}
