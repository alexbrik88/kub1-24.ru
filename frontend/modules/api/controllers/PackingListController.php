<?php

namespace frontend\modules\api\controllers;

use common\models\document\Invoice;
use common\models\document\PackingList;
use frontend\components\StatisticPeriod;
use frontend\modules\api\models\PngHelper;
use frontend\modules\documents\components\PackingListHelper;
use frontend\modules\documents\models\PackingListSearch;
use frontend\rbac\permissions;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class PackingListController
 * @package frontend\modules\api\controllers
 */
class PackingListController extends \yii\rest\Controller
{
    /**
     * @var string the model class name.
     */
    public $modelClass = 'common\models\document\PackingList';

    public $layoutWrapperCssClass = '';

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return array_merge(parent::actions(), [
            'file-get' => [
                'class' => \frontend\modules\api\components\actions\FileGetAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'file-list' => [
                'class' => \frontend\modules\api\components\actions\FileListAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'file-delete' => [
                'class' => \frontend\modules\api\components\actions\FileDeleteAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'file-upload' => [
                'class' => \frontend\modules\api\components\actions\FileUploadAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'scan-bind' => [
                'class' => \frontend\modules\api\components\actions\ScanBindAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'scan-unbind' => [
                'class' => \frontend\modules\api\components\actions\ScanUnbindAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'scan-list' => [
                'class' => \frontend\modules\api\components\actions\ScanListAction::className(),
                'modelClass' => $this->modelClass,
            ],
        ]);
    }

    /**
     * Checks the privilege of the current user.
     *
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string $action the ID of the action to be executed
     * @param object $model the model to be accessed. If null, it means no specific model is being accessed.
     * @param array $params additional parameters
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case 'index':
                $can = Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                    'ioType' => $this->module->getIoType(),
                ]);
                break;
            case 'view':
            case 'pdf':
            case 'png':
                $can = Yii::$app->getUser()->can(permissions\document\Document::VIEW, [
                    'model' => $model,
                ]);
                break;
            case 'create':
                $can = Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
                    'ioType' => $model->type,
                ]);
                break;
            case 'update':
            case 'send':
                $can = Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                    'model' => $model,
                ]);
                break;
            case 'delete':
                $can = Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
                    'model' => $model,
                ]);
                break;
            default:
                $can = false;
                break;
        }

        if (!$can) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }

        $this->modelClass::$additionalFields[] = 'totalAmountWithNds';
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH'],
            'delete' => ['DELETE'],
            'pdf' => ['GET'],
            'png' => ['GET'],
            'file-get' => ['GET'],
            'file-list' => ['GET'],
            'file-upload' => ['POST'],
            'file-delete' => ['DELETE'],
            'scan-list' => ['GET'],
            'scan-bind' => ['POST'],
            'scan-unbind' => ['POST'],
        ];
    }

    /**
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionIndex()
    {
        $this->checkAccess($this->action->id);

        $period = $this->module->getPeriod('periodFrom', 'periodTo');

        \common\models\document\AbstractDocument::$additionalFields = $this->module->getExpand();

        $searchModel = new PackingListSearch([
            'type' => $this->module->getIoType(),
        ]);
        $get = Yii::$app->request->get();
        $searchData = (array) ArrayHelper::remove($get, 'PackingListSearch', []);
        $searchData = array_merge($get, $searchData);

        return $searchModel->search(['PackingListSearch' => $searchData], [
            'from' => $period['from']->format('Y-m-d'),
            'to' => $period['to']->format('Y-m-d'),
        ]);
    }

    /**
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $this->checkAccess($this->action->id, $model);

        return $model;
    }

    /**
     * Creates a new PackingList model.
     * @param integer $invoice_id Invoice::id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionCreate($invoice_id)
    {
        /* @var Invoice $invoice */
        $invoice = $this->module->findInvoice($invoice_id);

        $this->checkAccess($this->action->id, $invoice);

        $this->checkAccess('view', $invoice);

        if ($invoice->createPackingList()) {
            $model = $invoice->packingList;
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $response->getHeaders()->set('Location', Url::toRoute(['view', 'id' => $model->id], true));

            return $model;
        }

        throw new ForbiddenHttpException('Нельзя создать ТН.');
    }

    /**
     * @param $type
     * @param $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $this->checkAccess($this->action->id, $model);

        PackingListHelper::load($model, Yii::$app->getRequest()->getBodyParams());
        PackingListHelper::save($model);

        return $model;
    }

    /**
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $this->checkAccess($this->action->id, $model);

        if ($model->delete() === false) {
            if ($model->hasErrors()) {
                return $model;
            }
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        Yii::$app->getResponse()->setStatusCode(204);
    }

    /**
     * @param integer $id
     */
    public function actionPdf($id)
    {
        $model = $this->findModel($id);

        $this->checkAccess($this->action->id, $model);

        return Yii::$app->response->sendContentAsFile(
            $this->modelClass::getRenderer(
                null,
                $model,
                \common\components\pdf\PdfRenderer::DESTINATION_STRING
            )->output(false),
            $model->getPdfFileName(),
            ['mimeType' => 'application/pdf']
        );
    }

    /**
     * @param integer $id
     * @param integer $page
     */
    public function actionPng($id, $page = 1)
    {
        $model = $this->findModel($id);

        $this->checkAccess($this->action->id, $model);

        $pngInfo = PngHelper::convertPdf2Png($this->modelClass::getRenderer(
            null,
            $model,
            \common\components\pdf\PdfRenderer::DESTINATION_STRING
        )->output(false), $page);

        if ($pngInfo) {
            header('X-Pagination-Page-Count: ' . $pngInfo['pagesCount']);
            return Yii::$app->response->sendContentAsFile(
                $pngInfo['content'],
                $model->getPngFileName($page, $pngInfo['pagesCount']),
                ['mimeType' => 'image/png']
            );
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param int $id
     * @return array
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionSend($id)
    {
        $user = Yii::$app->user->identity;
        $model = $this->findModel($id);

        $this->checkAccess($this->action->id, $model);

        if (!$model->uid) {
            $model->updateAttributes(['uid' =>  $this->modelClass::generateUid()]);
        }
        $form = new \frontend\modules\documents\forms\InvoiceSendForm($user->currentEmployeeCompany, [
            'model' => $model,
        ]);

        $form->load(Yii::$app->request->post(), '');

        if ($form->validate()) {
            if ($send = $form->send()) {
                return [
                    'sent' => $send[0],
                    'total' => $send[1],
                ];
            }
        }

        if ($form->hasErrors()) {
            return $form;
        }
        throw new \yii\web\ServerErrorHttpException('Failed to send document for unknown reason.');
    }

    /**
     * @param integer $id
     * @return PackingList
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = PackingList::find()->alias('packing_list')->joinWith('invoice invoice')->andWhere([
            'packing_list.id' => $id,
            'invoice.company_id' => $this->module->getCompany()->id,
            'invoice.is_deleted' => false,
        ])->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
