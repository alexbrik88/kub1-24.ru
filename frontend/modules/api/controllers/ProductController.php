<?php

namespace frontend\modules\api\controllers;

use common\models\address\Country;
use common\models\product\Product;
use common\models\product\ProductSearch;
use common\models\product\ProductUnit;
use common\models\TaxRate;
use frontend\rbac\permissions;
use Yii;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Class ProductController
 * @package frontend\modules\api\controllers
 */
class ProductController extends \yii\rest\ActiveController
{
    /**
     * @var string the model class name.
     */
    public $modelClass = 'common\models\product\Product';
    /**
     * @var string the scenario used for creating a model.
     * @see \yii\base\Model::scenarios()
     */
    public $createScenario = Product::SCENARIO_CREATE;
    /**
     * @var string the scenario used for updating a model.
     * @see \yii\base\Model::scenarios()
     */
    public $updateScenario = Product::SCENARIO_UPDATE;

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
        ]);
    }

    /**
     * Checks the privilege of the current user.
     *
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string $action the ID of the action to be executed
     * @param object $model the model to be accessed. If null, it means no specific model is being accessed.
     * @param array $params additional parameters
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case 'index':
                $can = Yii::$app->getUser()->can(permissions\Product::INDEX);
                break;
            case 'view':
                $can = Yii::$app->getUser()->can(permissions\Product::VIEW, [
                    'model' => $model,
                ]);
                break;
            case 'create':
                $can = Yii::$app->getUser()->can(permissions\Product::CREATE);
                break;
            case 'update':
                $can = Yii::$app->getUser()->can(permissions\Product::UPDATE, [
                    'model' => $model,
                ]);
                break;
            case 'delete':
                $can = Yii::$app->getUser()->can(permissions\Product::DELETE, [
                    'model' => $model,
                ]);
                break;
            case 'units':
                $can = true;
                break;
            default:
                $can = false;
                break;
        }

        if (!$can) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return array_merge(parent::verbs(), [
            'units' => ['GET', 'HEAD'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'prepareDataProvider'],
            ],
            'view' => [
                'class' => 'yii\rest\ViewAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'findModel' => [$this, 'findModel'],
            ],
            'create' => [
                'class' => 'common\components\rest\CreateAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'newModel' => [$this, 'newModel'],
                'scenario' => $this->createScenario,
            ],
            'update' => [
                'class' => 'yii\rest\UpdateAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'findModel' => [$this, 'findModel'],
                'scenario' => $this->updateScenario,
            ],
            'delete' => [
                'class' => 'common\components\rest\DeleteAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'findModel' => [$this, 'findModel'],
            ],
            'units' => [
                'class' => 'yii\rest\IndexAction',
                'checkAccess' => [$this, 'checkAccess'],
                'modelClass' => 'common\models\product\ProductUnit',
            ],
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
        ];
    }

    /**
     * @param yii\base\Action $action
     * @return ActiveDataProvider
     */
    public function prepareDataProvider($action)
    {
        $searchModel = new ProductSearch([
            'company_id' => $this->module->getCompany()->id,
            'production_type' => $this->module->getParameter('type', [
                Product::PRODUCTION_TYPE_SERVICE,
                Product::PRODUCTION_TYPE_GOODS,
            ]),
            'status' => Product::ACTIVE,
        ]);

        return $searchModel->search(Yii::$app->request->get(), '');
    }

    /**
     * @param string $scenario
     * @param yii\base\Action $action
     * @return yii\db\ActiveRecord
     */
    public function newModel($scenario, $action)
    {
        $isOsno = $this->module->getCompany()->companyTaxationType->osno;

        return new $this->modelClass([
            'scenario' => $scenario,
            'company_id' => $this->module->getCompany()->id,
            'country_origin_id' => \common\models\address\Country::COUNTRY_WITHOUT,
            'product_unit_id' => ProductUnit::UNIT_COUNT,
            'price_for_buy_nds_id' => $isOsno ? (date('Y') > 2018 ? TaxRate::RATE_20 : TaxRate::RATE_18) : TaxRate::RATE_WITHOUT,
            'price_for_sell_nds_id' => $isOsno ? (date('Y') > 2018 ? TaxRate::RATE_20 : TaxRate::RATE_18) : TaxRate::RATE_WITHOUT,
        ]);
    }

    /**
     * @param string $id
     * @param yii\base\Action $action
     * @return ActiveRecordInterface the model found
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id, $action = null)
    {
        /* @var $modelClass ActiveRecordInterface */
        $modelClass = $this->modelClass;
        $model = $modelClass::find()
                 ->byDeleted()
                 ->byUser()
                 ->byCompany($this->module->getCompany()->id)
                 ->andWhere([$modelClass::tableName().'.id' => $id])
                 ->one();

        if (isset($model)) {
            return $model;
        } else {
            throw new NotFoundHttpException("Object not found: $id");
        }
    }
}
