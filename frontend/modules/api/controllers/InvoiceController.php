<?php

namespace frontend\modules\api\controllers;

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\components\pdf\PdfRenderer;
use common\components\TextHelper;
use common\models\Company;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\OrderAct;
use common\models\document\OrderHelper;
use common\models\document\OrderPackingList;
use common\models\document\status\ActStatus;
use common\models\document\status\InvoiceFactureStatus;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\PackingListStatus;
use common\models\document\status\UpdStatus;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\api\models\PngHelper;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\modules\documents\components\InvoiceStatistic;
use frontend\modules\documents\models\InvoiceSearch;
use frontend\modules\documents\forms\InvoiceFlowForm;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\rbac\permissions;
use Yii;
use yii\db\Connection;
use yii\db\Transaction;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

/**
 * Class InvoiceController
 * @package frontend\modules\api\controllers
 */
class InvoiceController extends \yii\rest\Controller
{
    public $layoutWrapperCssClass;

    /**
     * @var string the model class name.
     */
    public $modelClass = 'common\models\document\Invoice';

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return array_merge(parent::actions(), [
            'file-get' => [
                'class' => \frontend\modules\api\components\actions\FileGetAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'file-list' => [
                'class' => \frontend\modules\api\components\actions\FileListAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'file-delete' => [
                'class' => \frontend\modules\api\components\actions\FileDeleteAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'file-upload' => [
                'class' => \frontend\modules\api\components\actions\FileUploadAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'scan-bind' => [
                'class' => \frontend\modules\api\components\actions\ScanBindAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'scan-unbind' => [
                'class' => \frontend\modules\api\components\actions\ScanUnbindAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'scan-list' => [
                'class' => \frontend\modules\api\components\actions\ScanListAction::className(),
                'modelClass' => $this->modelClass,
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'view' => ['GET', 'HEAD'],
            'pdf' => ['GET', 'HEAD'],
            'png' => ['GET'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH'],
            'delete' => ['DELETE'],
            'statistic' => ['GET'],
            'next-number' => ['GET'],
            'left-quantity' => ['GET'],
            'send' => ['POST'],
            'paid' => ['POST'],
            'unpaid' => ['DELETE'],
            'file-get' => ['GET'],
            'file-list' => ['GET'],
            'file-upload' => ['POST'],
            'file-delete' => ['DELETE'],
            'scan-list' => ['GET'],
            'scan-bind' => ['POST'],
            'scan-unbind' => ['POST'],
        ];
    }

    /**
     * Checks the privilege of the current user.
     *
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string $action the ID of the action to be executed
     * @param object $model the model to be accessed. If null, it means no specific model is being accessed.
     * @param array $params additional parameters
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case 'index':
            case 'statistic':
                $can = Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                    'ioType' => $this->module->getIoType(),
                ]);
                break;
            case 'view':
            case 'pdf':
            case 'png':
                $can = Yii::$app->getUser()->can(permissions\document\Document::VIEW, [
                    'model' => $model,
                ]);
                break;
            case 'create':
                $can = Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
                    'ioType' => $this->module->getIoType(),
                ]);
                break;
            case 'update':
            case 'status':
            case 'send':
                $can = Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                    'model' => $model,
                ]);
                break;
            case 'paid':
            case 'unpaid':
                $can = Yii::$app->getUser()->can(permissions\document\Invoice::ADD_CASH_FLOW, [
                    'model' => $model,
                ]);
                break;
            case 'delete':
                $can = Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
                    'model' => $model,
                ]);
                break;
            case 'next-number':
            case 'left-quantity':
                $can = true;
                break;
            default:
                $can = false;
                break;
        }

        if (!$can) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    /**
     * @return ActiveDataProvider
     */
    public function actionIndex()
    {
        $this->checkAccess($this->action->id);

        \common\models\document\AbstractDocument::$additionalFields = $this->module->getExpand();

        $period = $this->module->getPeriod('periodFrom', 'periodTo');

        $searchModel = new InvoiceSearch([
            'type' => $this->module->getIoType(),
            'company_id' => $this->module->getCompany()->id,
        ]);

        $dataProvider = $searchModel->search(Yii::$app->request->get(), [
            'from' => $period['from']->format('Y-m-d'),
            'to' => $period['to']->format('Y-m-d'),
        ]);

        return $dataProvider;
    }

    /**
     * @param integer $id
     * @return ActiveDataProvider
     */
    public function actionView($id)
    {
        /* @var Invoice $model */
        $model = $this->findInvoice($id, null, true);

        $this->checkAccess($this->action->id, $model);

        return $model;
    }

    /**
     * @param integer $id
     */
    public function actionPdf($id)
    {
        /* @var Invoice $model */
        $model = $this->findInvoice($id);

        $this->checkAccess($this->action->id, $model);

        return Yii::$app->response->sendContentAsFile(
            $this->modelClass::getRenderer(null, $model, PdfRenderer::DESTINATION_STRING)->output(false),
            $model->getPdfFileName(),
            ['mimeType' => 'application/pdf']
        );
    }

    /**
     * @param integer $id
     * @param integer $page
     */
    public function actionPng($id, $page = 1)
    {
        $model = $this->findInvoice($id);

        $this->checkAccess($this->action->id, $model);

        $pngInfo = PngHelper::convertPdf2Png($this->modelClass::getRenderer(
            null,
            $model,
            \common\components\pdf\PdfRenderer::DESTINATION_STRING
        )->output(false), $page);

        if ($pngInfo) {
            header('X-Pagination-Page-Count: ' . $pngInfo['pagesCount']);
            return Yii::$app->response->sendContentAsFile(
                $pngInfo['content'],
                $model->getPngFileName($page, $pngInfo['pagesCount']),
                ['mimeType' => 'image/png']
            );
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionCreate()
    {
        $type = $this->module->getIoType();

        $this->checkAccess($this->action->id);

        /* @var Company $company */
        $company = $this->module->getCompany();

        if ($company->strict_mode) {
            throw new ForbiddenHttpException('Необходимо заполнить реквизиты вашей компании');
        }

        if (!$company->createInvoiceAllowed($type)) {
            throw new ForbiddenHttpException('Для текущего тарифа исчерпан лимит счетов.');
        }

        $model = new Invoice([
            'scenario' => 'insert',
            'type' => $type,
            'company_id' => $this->module->getCompany()->id,
            'document_date' => date(DateHelper::FORMAT_DATE),
            'payment_limit_date' => date_create('+10 days')->format('Y-m-d'),
            'price_precision' => 2,
            'nds_view_type_id' => $type == Documents::IO_TYPE_IN ? Invoice::NDS_VIEW_IN : $company->nds_view_type_id,
            'isAutoinvoice' => ($type == Documents::IO_TYPE_OUT) ? (int) Yii::$app->request->get('auto') : 0,
        ]);

        $model->populateRelation('company', $company);

        if ($type == Documents::IO_TYPE_OUT) {
            $model->document_number = Invoice::getNextDocumentNumber($company->id, $type, null, $model->document_date);
        }

        InvoiceHelper::load($model, Yii::$app->getRequest()->getBodyParams());
        if (InvoiceHelper::save($model)) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $response->getHeaders()->set('Location', Url::toRoute(['view', 'id' => $model->id], true));

            return $this->findInvoice($model->id, null, true);
        }

        return $model;
    }

    /**
     * @param $type
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /* @var Invoice $model */
        $model = $this->findInvoice($id);

        $this->checkAccess($this->action->id, $model);

        if ($model->is_subscribe_invoice) {
            throw new ForbiddenHttpException('Вы не можете редактировать системный счет.');
        }

        InvoiceHelper::load($model, Yii::$app->getRequest()->getBodyParams());

        if (InvoiceHelper::save($model)) {
            return $this->findInvoice($model->id, null, true);
        }

        return $model;
    }

    /**
     * @param $id
     * @return Response
     * @throws \Throwable
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     */
    public function actionDelete($id)
    {
        /** @var Invoice $model */
        $model = $this->findInvoice($id);

        $this->checkAccess($this->action->id, $model);

        Yii::$app->db->transaction(function (Connection $db) use ($model) {
            if ($model->deletingAll()) {
                Yii::$app->getResponse()->setStatusCode(204);

                return;
            }
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        });
    }

    /**
     * @return array
     */
    public function actionStatistic()
    {
        $this->checkAccess($this->action->id);

        $type = $this->module->getIoType();

        $period = $this->module->getPeriod('periodFrom', 'periodTo');

        $dateRange = [
            'from' => $period['from']->format('Y-m-d'),
            'to' => $period['to']->format('Y-m-d'),
        ];

        $employee = Yii::$app->user->identity;
        $result = [];
        foreach ([InvoiceStatistic::NOT_PAID, InvoiceStatistic::NOT_PAID_IN_TIME, InvoiceStatistic::PAID] as $status) {
            $result[$status] = InvoiceStatistic::getStatisticInfo(
                $type,
                $employee->company->id,
                $status,
                $dateRange,
                null,
                null,
                null,
                null,
                $employee
            );
        }

        return [
            'periodFrom' => $period['from']->format('Y-m-d'),
            'periodTo' => $period['to']->format('Y-m-d'),
            'statistic' => $result,
        ];
    }

    /**
     * @param $id
     * @return array
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionSend($id)
    {
        /* @var Invoice $model */
        $model = $this->findInvoice($id, Documents::IO_TYPE_OUT);

        $this->checkAccess($this->action->id, $model);

        $sendForm = new InvoiceSendForm(Yii::$app->user->identity->currentEmployeeCompany, [
            'model' => $model,
        ]);

        $sendForm->load(Yii::$app->request->post(), '');

        if ($sendForm->validate()) {
            $saved = $sendForm->send($this->user);
            if (is_array($saved)) {
                $company = $model->company;
                if ($company->first_send_invoice_date == null) {
                    $model->updateAttributes([
                        'first_send_invoice_date' => time(),
                    ]);
                }
                $model->updateAttributes([
                    'email_messages' => $model->email_messages + $saved[0],
                ]);

                return [
                    'sent' => $saved[0],
                    'total' => $saved[1],
                ];
            }
        }

        if ($sendForm->hasErrors()) {
            return $sendForm;
        }
        throw new ServerErrorHttpException('Failed to send invoice for unknown reason.');
    }

    /**
     * @param $type
     * @param $id
     *
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function actionPaid($id)
    {
        /* @var Invoice $model */
        $model = $this->findInvoice($id);

        $this->checkAccess($this->action->id, $model);

        if ($model->invoice_status_id == InvoiceStatus::STATUS_PAYED) {
            throw new ForbiddenHttpException('Счет уже оплачен.');
        }
        if (!in_array($model->invoice_status_id, InvoiceStatus::$payAllowed)) {
            throw new ForbiddenHttpException('Нельзя добавить оплату по счету.');
        }

        $flowForm = new InvoiceFlowForm([
            'scenario' => InvoiceFlowForm::SCENARIO_API_PAID,
            'invoice' => $model,
            'employee' => Yii::$app->user->identity,
            'createNew' => '1',
        ]);

        $flowForm->load(Yii::$app->getRequest()->getBodyParams(), '');

        if ($flowForm->validate()) {
            if ($flowForm->save()) {
                return $this->findInvoice($id);
            } else {
                throw new ServerErrorHttpException('Failed to create the invoice payment for unknown reason.');
            }
        }

        return $flowForm;
    }

    /**
     * @param $type
     * @param $id
     *
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function actionUnpaid($id)
    {
        /* @var Invoice $model */
        $model = $this->findInvoice($id);

        $this->checkAccess($this->action->id, $model);

        if (!in_array($model->invoice_status_id, [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL])) {
            throw new ForbiddenHttpException('Счет не имеет оплаты.');
        }

        foreach ($model->cashBankFlows as $cashFlow) {
            $cashFlow->unlinkInvoice($model);
        }
        foreach ($model->cashEmoneyFlows as $cashFlow) {
            $cashFlow->unlinkInvoice($model);
            if (!$cashFlow->getInvoices()->exists()) {
                $cashFlow->delete();
            }
        }
        foreach ($model->cashOrderFlows as $cashFlow) {
            $cashFlow->unlinkInvoice($model);
            if (!$cashFlow->getInvoices()->exists()) {
                $cashFlow->delete();
            }
        }

        return $model;
    }

    /**
     * @return array|Response
     * @throws ForbiddenHttpException
     */
    public function actionNextNumber()
    {
        $this->checkAccess($this->action->id);

        return [
            'number' => Invoice::getNextDocumentNumber(
                $this->module->getCompany(),
                Documents::IO_TYPE_OUT,
                null,
                date(DateHelper::FORMAT_DATE)
            ),
        ];
    }

    /**
     * @return array|Response
     * @throws ForbiddenHttpException
     */
    public function actionLeftQuantity()
    {
        $this->checkAccess($this->action->id);

        $company = $this->module->getCompany();

        $left = $company->getInvoiceLeft();

        if ($left < 100) {
            return $left;
        }

        Yii::$app->getResponse()->setStatusCode(204);

        return null;
    }

    /**
     * @param integer $id
     * @param integer $type
     * @param boolean $withExpand
     * @return Invoice
     * @throws NotFoundHttpException
     */
    public function findInvoice($id, $type = null, $withExpand = false)
    {
        $company = $this->module->getCompany();
        $company = $this->module->getCompany();

        $class = $this->modelClass;
        $model = $class::find()->andWhere([
            'id' => $id,
            'company_id' => $company->id,
            'is_deleted' => false,
        ])->andFilterWhere([
            'type' => $type,
        ])->one();

        if ($model !== null) {
            if ($withExpand) {
                $expand = $this->module->getExpand();

                foreach ((array) $expand as $relation) {
                    if ($relation && is_string($relation)) {
                        empty($model->$relation);
                    }
                }
            }

            return $model;
        } else {
            throw new NotFoundHttpException("Invoice not found: $id");
        }
    }
}
