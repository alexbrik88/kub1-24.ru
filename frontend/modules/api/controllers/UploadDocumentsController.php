<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.07.2017
 * Time: 10:14
 */

namespace frontend\modules\api\controllers;

use common\models\document\UploadedDocuments;
use Yii;

/**
 * Class UploadDocumentsController
 * @package frontend\modules\api\controllers
 */
class UploadDocumentsController extends \yii\rest\Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionUpload($id = null)
    {
        if ($_FILES) {
            return [
                'result' => true,
                'files' => UploadedDocuments::uploadFiles(),
            ];
        }

        return [
            'result' => false,
        ];
    }
}
