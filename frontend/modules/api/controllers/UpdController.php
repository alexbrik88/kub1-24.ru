<?php

namespace frontend\modules\api\controllers;

use common\models\document\Invoice;
use common\models\document\Upd;
use common\models\document\UpdPaymentDocument;
use frontend\components\StatisticPeriod;
use frontend\modules\api\models\PngHelper;
use frontend\modules\documents\models\UpdSearch;
use frontend\rbac\permissions;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class UpdController
 * @package frontend\modules\api\controllers
 */
class UpdController extends \yii\rest\Controller
{
    /**
     * @var string the model class name.
     */
    public $modelClass = 'common\models\document\Upd';

    public $layoutWrapperCssClass = '';

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return array_merge(parent::actions(), [
            'file-get' => [
                'class' => \frontend\modules\api\components\actions\FileGetAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'file-list' => [
                'class' => \frontend\modules\api\components\actions\FileListAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'file-delete' => [
                'class' => \frontend\modules\api\components\actions\FileDeleteAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'file-upload' => [
                'class' => \frontend\modules\api\components\actions\FileUploadAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'scan-bind' => [
                'class' => \frontend\modules\api\components\actions\ScanBindAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'scan-unbind' => [
                'class' => \frontend\modules\api\components\actions\ScanUnbindAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'scan-list' => [
                'class' => \frontend\modules\api\components\actions\ScanListAction::className(),
                'modelClass' => $this->modelClass,
            ],
        ]);
    }

    /**
     * Checks the privilege of the current user.
     *
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string $action the ID of the action to be executed
     * @param object $model the model to be accessed. If null, it means no specific model is being accessed.
     * @param array $params additional parameters
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case 'index':
                $can = Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                    'ioType' => $this->module->getIoType(),
                ]);
                break;
            case 'view':
            case 'pdf':
            case 'png':
                $can = Yii::$app->getUser()->can(permissions\document\Document::VIEW, [
                    'model' => $model,
                ]);
                break;
            case 'create':
                $can = Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
                    'ioType' => $model->type,
                ]);
                break;
            case 'update':
            case 'send':
                $can = Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                    'model' => $model,
                ]);
                break;
            case 'delete':
                $can = Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
                    'model' => $model,
                ]);
                break;
            default:
                $can = false;
                break;
        }

        if (!$can) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }

        $this->modelClass::$additionalFields[] = 'totalAmountWithNds';
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH'],
            'delete' => ['DELETE'],
            'pdf' => ['GET'],
            'png' => ['GET'],
            'file-get' => ['GET'],
            'file-list' => ['GET'],
            'file-upload' => ['POST'],
            'file-delete' => ['DELETE'],
            'scan-list' => ['GET'],
            'scan-bind' => ['POST'],
            'scan-unbind' => ['POST'],
        ];
    }

    /**
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionIndex()
    {
        $this->checkAccess($this->action->id);

        $period = $this->module->getPeriod('periodFrom', 'periodTo');

        $this->modelClass::$additionalFields = array_merge([
            'orders_sum',
        ], $this->module->getExpand());

        $searchModel = new UpdSearch([
            'type' => $this->module->getIoType(),
        ]);
        $get = Yii::$app->request->get();
        $searchData = (array) ArrayHelper::remove($get, 'UpdSearch', []);
        $searchData = array_merge($get, $searchData);

        return $searchModel->search(['UpdSearch' => $searchData], [
            'from' => $period['from']->format('Y-m-d'),
            'to' => $period['to']->format('Y-m-d'),
        ]);
    }

    /**
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $this->checkAccess($this->action->id, $model);

        return $model;
    }

    /**
     * @param $type
     * @param $invoiceId
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionCreate($invoice_id)
    {
        /* @var Invoice $invoice */
        $invoice = $this->module->findInvoice($invoice_id);

        $this->checkAccess($this->action->id, $invoice);

        $this->checkAccess('view', $invoice);

        if ($invoice->createUpd()) {
            $model = $invoice->upd;
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $response->getHeaders()->set('Location', Url::toRoute(['view', 'id' => $model->id], true));

            return $model;
        }

        throw new ForbiddenHttpException('Нельзя создать УПД.');
    }

    /**
     * @param $type
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionUpdate($type, $id)
    {
        $model = $this->findModel($id);

        $this->checkAccess($this->action->id, $model);

        $model->load(Yii::$app->getRequest()->getBodyParams());

        return Yii::$app->db->transaction(function ($db) use ($model) {
            UpdPaymentDocument::deleteAll(['upd_id' => $model->id]);
            $paymentsData = Yii::$app->request->post('payment', []);
            if (isset($paymentsData['number']) && is_array($paymentsData['number'])) {
                foreach ($paymentsData['number'] as $key => $value) {
                    if (($value = trim($value)) && ($date = \DateTime::createFromFormat('d.m.Y', $paymentsData['date'][$key]))) {
                        $payment = new UpdPaymentDocument([
                            'upd_id' => $model->id,
                            'payment_document_number' => $value,
                            'payment_document_date' => $date->format('Y-m-d'),
                        ]);
                        $payment->save();
                    }
                }
            }

            $linkParams = [
                'empty_unit_code' => 0,
                'empty_unit_name' => 0,
                'empty_product_code' => 0,
                'empty_box_type' => 0,
            ];
            $orderParams = Yii::$app->request->post('orderParams');
            if ($orderParams) {
                foreach ($model->invoice->orders as $order) {
                    if (isset($orderParams[$order->id]) && !array_diff_key($linkParams, $orderParams[$order->id])) {
                        $model->unlink('orders', $order, true);
                        $model->link('orders', $order, $orderParams[$order->id]);
                    }
                }
            }

            $model->ordinal_document_number = $model->document_number;
            if (!$model->save()) {
                $db->transaction->rollBack();

                return [
                    'result' => false,
                    'errors' => $model->getErrors(),
                ];
            }

            return [
                'result' => true,
            ];
        });
    }

    /**
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $this->checkAccess($this->action->id, $model);

        if ($model->delete() === false) {
            if ($model->hasErrors()) {
                return $model;
            }
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        Yii::$app->getResponse()->setStatusCode(204);
    }

    /**
     * @param integer $id
     */
    public function actionPdf($id)
    {
        $model = $this->findModel($id);

        $this->checkAccess($this->action->id, $model);

        return Yii::$app->response->sendContentAsFile(
            $this->modelClass::getRenderer(
                null,
                $model,
                \common\components\pdf\PdfRenderer::DESTINATION_STRING
            )->output(false),
            $model->getPdfFileName(),
            ['mimeType' => 'application/pdf']
        );
    }

    /**
     * @param integer $id
     * @param integer $page
     */
    public function actionPng($id, $page = 1)
    {
        $model = $this->findModel($id);

        $this->checkAccess($this->action->id, $model);

        $pngInfo = PngHelper::convertPdf2Png($this->modelClass::getRenderer(
            null,
            $model,
            \common\components\pdf\PdfRenderer::DESTINATION_STRING
        )->output(false), $page);

        if ($pngInfo) {
            header('X-Pagination-Page-Count: ' . $pngInfo['pagesCount']);
            return Yii::$app->response->sendContentAsFile(
                $pngInfo['content'],
                $model->getPngFileName($page, $pngInfo['pagesCount']),
                ['mimeType' => 'image/png']
            );
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param int $id
     * @return array
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionSend($id)
    {
        $user = Yii::$app->user->identity;
        $model = $this->findModel($id);

        $this->checkAccess($this->action->id, $model);

        if (!$model->uid) {
            $model->updateAttributes(['uid' =>  $this->modelClass::generateUid()]);
        }
        $form = new \frontend\modules\documents\forms\InvoiceSendForm($user->currentEmployeeCompany, [
            'model' => $model,
        ]);

        $form->load(Yii::$app->request->post(), '');

        if ($form->validate()) {
            if ($send = $form->send()) {
                return [
                    'sent' => $send[0],
                    'total' => $send[1],
                ];
            }
        }

        if ($form->hasErrors()) {
            return $form;
        }
        throw new \yii\web\ServerErrorHttpException('Failed to send document for unknown reason.');
    }

    /**
     * Finds the Upd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Upd the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $this->modelClass::$additionalFields = [
            'orders_sum',
        ];

        $model = $this->modelClass::find()->alias('upd')->joinWith('invoice invoice')->andWhere([
            'upd.id' => $id,
            'invoice.company_id' => $this->module->getCompany()->id,
            'invoice.is_deleted' => false,
        ])->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
