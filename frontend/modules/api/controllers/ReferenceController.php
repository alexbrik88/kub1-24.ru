<?php

namespace frontend\modules\api\controllers;

use common\models\AgreementType;
use common\models\address\AddressDictionary as Fias;
use common\models\company\CompanyType;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\document\NdsViewType;
use common\models\document\status\ActStatus;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\InvoiceFactureStatus;
use common\models\document\status\OrderDocumentStatus;
use common\models\document\status\PackingListStatus;
use common\models\document\status\PaymentOrderStatus;
use common\models\document\status\UpdStatus;
use common\models\dictionary\address\AddressDictionary;
use common\models\dictionary\bik\BikDictionary;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\product\ProductUnit;
use common\models\Ifns;
use common\models\TaxRate;
use common\models\TimeZone;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * ReferenceController
 */
class ReferenceController extends \yii\rest\Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
                'only' => [
                    'employee',
                    'expenditure',
                ],
            ],
        ]);
    }

    /**
     * Declares the allowed HTTP verbs.
     * Please refer to [[VerbFilter::actions]] on how to declare the allowed verbs.
     * @return array the allowed HTTP verbs.
     */
    protected function verbs()
    {
        return [
            'company-type'  => ['get'],
            'units'  => ['get'],
            'agreement-type'  => ['get'],
            'nds-value'  => ['get'],
            'nds-type'  => ['get'],
            'bik'  => ['get'],
            'address'  => ['get'],
            'employee'  => ['get'],
            'ifns'  => ['get'],
            'fias'  => ['get'],
            'fias-item'  => ['get'],
            'time-zone'  => ['get'],
            'document-status'  => ['get'],
            'employee-role'  => ['get'],
            'expenditure'  => ['get'],
            'income'  => ['get'],
            'bank-logo'  => ['get'],
        ];
    }

    /**
     * @return string
     */
    public function actionCompanyType()
    {
        return CompanyType::find()->select([
            "id",
            "name_short",
            "name_full",
            "in_company",
            "in_contractor",
        ])->all();
    }

    /**
     * @return string
     */
    public function actionUnits()
    {
        return ProductUnit::findSorted()->andFilterWhere([
            'goods' => Yii::$app->getRequest()->get('goods'),
            'services' => Yii::$app->getRequest()->get('services'),
        ])->all();
    }

    /**
     * @return string
     */
    public function actionAgreementType()
    {
        return AgreementType::find()->select([
            'id',
            'name',
            'name_dative',
        ])->all();
    }

    /**
     * @return string
     */
    public function actionNdsValue()
    {
        return TaxRate::find()->all();
    }

    /**
     * @return string
     */
    public function actionNdsType()
    {
        return NdsViewType::find()->all();
    }

    /**
     * @return string
     */
    public function actionBik($q = '')
    {
        return $q ? BikDictionary::find()
            ->byActive()
            ->byBikFilter($q)
            ->limit(20)
            ->asArray()
            ->all() : [];
    }

    /**
     * @param $type
     * @param $q
     * @return array
     */
    public function actionAddress($type, $q)
    {
        if (!in_array($type, AddressDictionary::$typeArray)) {
            throw new BadRequestHttpException(Yii::t('yii', 'Invalid data received for parameter "{param}".', [
                'param' => 'type',
            ]));
        }

        $searcher = new AddressDictionary($type, $q, Yii::$app->request->get());

        return (array) $searcher->search();
    }

    /**
     * @param $q
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionEmployee($q)
    {
        $companyId = $this->module->getCompany()->id;
        $employeeArray = [];
        $q = trim($q);
        if ($q !== '') {
            $employeeArray = Employee::find()
                ->select(['*', 'DATE_FORMAT(date_hiring, "%d.%m.%Y") as date_hiring_format',
                    'DATE_FORMAT(birthday, "%d.%m.%Y") as birthday_format',
                    'DATE_FORMAT(date_dismissal, "%d.%m.%Y") as date_dismissal_format',])
                ->andWhere(['!=', 'company_id', $companyId])
                ->byEmailFilter($q)
                ->byIsDeleted(Employee::NOT_DELETED)
                ->limit(20)
                ->asArray()
                ->all();
            /* @var $employee Employee */
            foreach ($employeeArray as $key => $employee) {
                if (EmployeeCompany::find()->andWhere(['and',
                        ['employee_id' => $employee['id']],
                        ['company_id' => $companyId],
                    ])->one() !== null
                ) {
                    unset($employeeArray[$key]);
                }
            }
        }

        return $employeeArray;
    }

    /**
     * @param $q
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionIfns($q = null)
    {
        if ($q) {
            $query = Ifns::find()
                ->select('ga, gb, g1, g2, g4, g6, g7, g8, g9, g11')
                ->where(['like', 'ga', $q.'%', false])
                ->limit(10);

            $result = $query->all();

            array_walk($result, function (&$data) {
                $data->g1 = implode(', ', array_filter(explode(',', $data->g1)));
            });

            return $result;
        }

        return [];
    }

    /**
     * @return array
     */
    public function actionFias($q, $level, $guid = null)
    {
        if (empty($q) || empty($level) || (empty($guid) && $level != 1)) {
            return [];
        }

        $query = Fias::find()
            ->select(['AOGUID', 'FULLNAME'])
            ->where(['AOLEVEL' => $level, 'ACTSTATUS' => 1])
            ->andWhere(['like', 'FULLNAME', $q . '%', false])
            ->orderBy(['FORMALNAME' => SORT_ASC])
            ->limit(100);

        if ($guid && $level != 1) {
            $query->andWhere(['PARENTGUID' => $guid]);
        }

        return $query->all();
    }

    /**
     * @return array
     */
    public function actionFiasItem($guid)
    {
        if ($result = Fias::findOne(['AOGUID' => $guid, 'ACTSTATUS' => 1])) {
            return $result;
        } else {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }
    }

    /**
     * @return array
     */
    public function actionTimeZone()
    {
        $timeZones = TimeZone::find()->select([
            'out_time_zone',
        ])->orderBy(['priority' => SORT_ASC])->indexBy('id')->column();

        return [
            'timeZones' => $timeZones,
        ];
    }

    /**
     * @return array
     */
    public function actionDocumentStatus($type)
    {
        switch ($type) {
            case 'act':
                $class = ActStatus::class;
                break;
            case 'invoice':
                $class = InvoiceStatus::class;
                break;
            case 'invoice-facture':
                $class = InvoiceFactureStatus::class;
                break;
            case 'order-document':
                $class = OrderDocumentStatus::class;
                break;
            case 'packing-list':
                $class = PackingListStatus::class;
                break;
            case 'payment-order':
                $class = PaymentOrderStatus::class;
                break;
            case 'upd':
                $class = UpdStatus::class;
                break;

            default:
                throw new BadRequestHttpException(Yii::t('yii', 'Invalid data received for parameter "{param}".', [
                    'param' => 'type',
                ]));
                break;
        }

        return $class::find()->all();
    }

    /**
     * @return array
     */
    public function actionEmployeeRole()
    {
        return EmployeeRole::find()->actual()->orderBy(['sort' => SORT_ASC])->all();
    }

    /**
     * @return array
     */
    public function actionExpenditure()
    {
        return InvoiceExpenditureItem::getList($this->module->getCompany()->id);
    }

    /**
     * @return array
     */
    public function actionIncome()
    {
        return array_values(InvoiceIncomeItem::getList($this->module->getCompany()->id));
    }

    /**
     * @param string $bik
     */
    public function actionBankLogo($bik, $small = null)
    {
        $small = (boolean) $small;
        $key = $small ? 'bank.small_logo.' . $bik : 'bank.logo.' . $bik;
        $file = Yii::$app->cache->getOrSet($key, function () use ($bik, $small) {
            return \common\models\bank\Bank::getLogoPath($bik, $small);
        }, 3600);

        if ($file && is_file($file)) {
            return \Yii::$app->response->sendFile($file);
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
