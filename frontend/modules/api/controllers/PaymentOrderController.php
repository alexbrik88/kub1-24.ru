<?php

namespace frontend\modules\api\controllers;

use common\models\document\PaymentOrder;
use common\models\product\Product;
use common\models\product\ProductSearch;
use common\models\product\ProductUnit;
use common\models\TaxRate;
use frontend\modules\documents\models\PaymentOrderSearch;
use frontend\rbac\permissions;
use Yii;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Class PaymentOrderController
 * @package frontend\modules\api\controllers
 */
class PaymentOrderController extends \yii\rest\ActiveController
{
    /**
     * @var string the model class name.
     */
    public $modelClass = 'common\models\document\PaymentOrder';

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
        ]);
    }

    /**
     * Checks the privilege of the current user.
     *
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string $action the ID of the action to be executed
     * @param object $model the model to be accessed. If null, it means no specific model is being accessed.
     * @param array $params additional parameters
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case 'index':
                $can = Yii::$app->getUser()->can(permissions\document\PaymentOrder::INDEX);
                break;
            case 'view':
            case 'payment-status':
                $can = Yii::$app->getUser()->can(permissions\document\PaymentOrder::VIEW, [
                    'model' => $model,
                ]);
                break;
            /*case 'create':
                $can = Yii::$app->getUser()->can(permissions\document\PaymentOrder::CREATE);
                break;
            case 'update':
                $can = Yii::$app->getUser()->can(permissions\document\PaymentOrder::UPDATE, [
                    'model' => $model,
                ]);
                break;
            case 'delete':
                $can = Yii::$app->getUser()->can(permissions\document\PaymentOrder::DELETE, [
                    'model' => $model,
                ]);
                break;*/
            default:
                $can = false;
                break;
        }

        if (!$can) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return array_merge(parent::verbs(), [
            'payment-status' => ['GET'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => [$this, 'prepareDataProvider'],
            ],
            'view' => [
                'class' => 'yii\rest\ViewAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'findModel' => [$this, 'findModel'],
            ],
            'create' => [
                'class' => 'common\components\rest\CreateAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'newModel' => [$this, 'newModel'],
                'scenario' => $this->createScenario,
            ],
            'update' => [
                'class' => 'yii\rest\UpdateAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'findModel' => [$this, 'findModel'],
                'scenario' => $this->updateScenario,
            ],
            'delete' => [
                'class' => 'common\components\rest\DeleteAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'findModel' => [$this, 'findModel'],
            ],
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
        ];
    }

    /**
     * @return array
     */
    public function actionPaymentStatus($id)
    {
        $this->checkAccess($this->action->id);

        $model = $this->findModel($id, $this->action);

        $response = Yii::$app->getResponse();
        $response->setStatusCode(422);

        return [
            'status' => 'unknown',
        ];
    }

    /**
     * @param yii\base\Action $action
     * @return ActiveDataProvider
     */
    public function prepareDataProvider($action)
    {
        $period = $this->module->getPeriod('periodFrom', 'periodTo');

        $searchModel = new PaymentOrderSearch([
            'company_id' => $this->module->getCompany()->id,
            'periodFrom' => $period['from']->format('Y-m-d'),
            'periodTo' => $period['to']->format('Y-m-d'),
        ]);

        return $searchModel->search(Yii::$app->request->get());
    }

    /**
     * @param string $scenario
     * @param yii\base\Action $action
     * @return yii\db\ActiveRecord
     */
    public function newModel($scenario, $action)
    {
        $company = $this->module->getCompany();

        return new $this->modelClass([
            'scenario' => $scenario,
            'company_id' => $company->id,
            'company_name' => $company->getTitle(true),
            'company_inn' => $company->inn,
            'company_kpp' => $company->kpp,
        ]);
    }

    /**
     * @param string $id
     * @param yii\base\Action $action
     * @return ActiveRecordInterface the model found
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id, $action = null)
    {
        /* @var $modelClass ActiveRecordInterface */
        $modelClass = $this->modelClass;

        if (($model = $modelClass::findOne(['id' => $id, 'company_id' => $this->module->getCompany()->id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException("Object not found: $id");
        }
    }
}
