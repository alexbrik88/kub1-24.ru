<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 22.11.2017
 * Time: 11:24
 */

namespace frontend\modules\api\controllers;

use common\models\Company;
use common\models\notification\Notification;
use Yii;
use yii\web\Response;

/**
 * Class NotificationController
 * @package frontend\modules\api\controllers
 */
class NotificationController extends \yii\rest\Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
        ]);
    }

    /**
     * @return array
     */
    public function actionGetNotifications()
    {
        $postNotification = Yii::$app->request->post('Notification');
        $companyID = isset($postNotification['company_id']) ? $postNotification['company_id'] : null;

        /* @var $company Company */
        if ($companyID && ($company = Company::find()->andWhere(['id' => $companyID])->one()) !== null) {
            $nextDay = strtotime("-1 day");

            return [
                'result' => true,
                'notifications' => Notification::find()
                    ->select(['title', 'fine', 'event_date', 'text'])
                    ->byActivationDate()
                    ->byEventDate($nextDay)
                    ->byNotificationType(Notification::NOTIFICATION_TYPE_TAX)
                    ->byWhom($company)
                    ->orderBy('event_date ASC')
                    ->asArray()
                    ->all(),
            ];
        }

        return [
            'result' => false,
        ];
    }
}
