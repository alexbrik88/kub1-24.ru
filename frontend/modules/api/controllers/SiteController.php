<?php

namespace frontend\modules\api\controllers;

use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\RegistrationForm;
use frontend\models\ResetPasswordForm;
use frontend\modules\api\models\ChangePasswordForm;
use Yii;
use yii\base\InvalidParamException;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\ErrorAction;
use yii\web\HttpException;

/**
 * Class SiteController
 * @package frontend\modules\api\controllers
 */
class SiteController extends \yii\rest\Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
                'optional' => [
                    'error',
                    'registration',
                    'request-password-reset',
                    'reset-password',
                ],
            ],
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
                            'registration',
                            'request-password-reset',
                            'reset-password',
                        ],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * Declares the allowed HTTP verbs.
     * Please refer to [[VerbFilter::actions]] on how to declare the allowed verbs.
     * @return array the allowed HTTP verbs.
     */
    protected function verbs()
    {
        return [
            'registration'  => ['post'],
            'request-password-reset'  => ['put', 'patch'],
            'reset-password'  => ['put', 'patch'],
        ];
    }

    /**
     * @return mixed
     */
    public function actionRegistration()
    {
        /* @var $model RegistrationForm */
        $model = new RegistrationForm(['scenario' => RegistrationForm::SCENARIO_DEFAULT]);

        $model->load(Yii::$app->getRequest()->getBodyParams(), '');

        if ($model->validate() && $model->save()) {
            $user = $model->getUser();
            Yii::$app->getResponse()->getHeaders()->set('X-Company-ID', $user->company_id);

            $mailer = clone \Yii::$app->mailer;
            $mailer->htmlLayout = 'layouts/html2';
            $mailer->compose([
                'html' => 'system/new-registration/html',
                'text' => 'system/new-registration/text',
            ], [
                'login' => $user->email,
                'password' => $model->password,
                'subject' => 'Добро пожаловать в КУБ24',
            ])
                ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
                ->setTo($user->email)
                ->setSubject('Добро пожаловать в КУБ24')
                ->send();

            return [
                'access_token' => $user->getAccessToken(),
                'company_id' => $user->company_id,
            ];
        }

        return $model;
    }

    /**
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();

        $model->load(Yii::$app->getRequest()->getBodyParams(), '');

        if ($model->validate()) {
            if (!$model->sendMailMobileApplication()) {
                throw new HttpException(424, "Не удалось отправить письмо на адрес {$model->email}.");
            }
            Yii::$app->getResponse()->setStatusCode(204);

            return;
        }

        return $model;
    }

    /**
     * @return mixed
     */
    public function actionResetPassword()
    {
        try {
            $model = new ResetPasswordForm(ArrayHelper::getValue(Yii::$app->getRequest()->getBodyParams(), 'token'));
        } catch (InvalidParamException $e) {
            $model = new Model;
            $model->addError('token', $e->getMessage());

            return $model;
        }

        $model->load(Yii::$app->getRequest()->getBodyParams(), '');

        if ($model->validate()) {
            if (!$model->resetPassword()) {
                throw new HttpException(424, "Во время изменения пароля возникла непредвиденная ошибка.");
            }
            Yii::$app->getResponse()->setStatusCode(204);

            return;
        }

        return $model;
    }
}
