<?php

namespace frontend\modules\api\controllers;

use common\models\Company;
use common\models\company\CompanyType;
use common\models\document\EmailSignature;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use Yii;
use yii\web\ServerErrorHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class EmailController
 * @package frontend\controllers
 */
class EmailController extends \yii\rest\Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'signature' => ['GET', 'POST', 'PUT', 'PATCH'],
        ];
    }

    /**
     * @return array
     */
    public function actionSignature()
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        /* @var $company Company */
        $company = $user->company;
        /* @var $employeeCompany EmployeeCompany */
        $employeeCompany = $user->getCurrentEmployeeCompany();

        $model = $company->getEmailSignature($user->id) ? : new EmailSignature([
            'company_id' => $company->id,
            'employee_id' => $user->id,
            'text' => "С уважением,\r\n{$employeeCompany->getFio(true)}\r\n" . (
                $company->company_type_id != CompanyType::TYPE_IP ? $employeeCompany->position : null
            ),
        ]);

        if (Yii::$app->getRequest()->getIsPost()) {
            $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        }
        if ($model->save() === false && !$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        }

        return $model;
    }
}
