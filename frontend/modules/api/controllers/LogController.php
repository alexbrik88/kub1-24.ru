<?php

namespace frontend\modules\api\controllers;

use frontend\models\log\Log;
use frontend\models\log\LogSearch;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * LogController
 */
class LogController extends \yii\rest\Controller
{
    public static $entityArray = [
        'act' => 'common\models\document\Act',
        'invoice' => [
            'common\models\document\Invoice',
            'common\models\document\InvoiceAuto',
        ],
        'invoice-facture' => 'common\models\document\InvoiceFacture',
        'packing-list' => 'common\models\document\PackingList',
        'upd' => 'common\models\document\Upd',
        'payment-order' => 'common\models\document\PaymentOrder',
        'order-document' => 'common\models\document\OrderDocument',
        'invoice-auto' => 'common\models\document\InvoiceAuto',
        'cash-bank-flows' => [
            'common\models\cash\CashBankFlows',
            'common\models\cash\form\CashBankFlowsForm',
        ],
        'cash-emoney-flows' => 'common\models\cash\CashEmoneyFlows',
        'cash-order-flows' => 'common\models\cash\CashOrderFlows',
        'cash-bank-statement-upload' => 'common\models\cash\CashBankStatementUpload',
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
        ]);
    }

    /**
     * Declares the allowed HTTP verbs.
     * Please refer to [[VerbFilter::actions]] on how to declare the allowed verbs.
     * @return array the allowed HTTP verbs.
     */
    protected function verbs()
    {
        return [
            'index'  => ['get'],
        ];
    }

    /**
     * Lists all Employee models.
     * @return mixed
     */
    public function actionIndex()
    {
        $params = Yii::$app->request->get();

        $model_id = ArrayHelper::remove($params, 'id');
        $name = ArrayHelper::remove($params, 'name');

        if ($model_id !== null && $name === null) {
            throw new BadRequestHttpException(Yii::t('yii', 'Missing required parameters: {params}', [
                'params' => 'name',
            ]));
        }

        $model_name = ArrayHelper::getValue(self::$entityArray, $name);
        if ($name !== null && $model_name === null) {
            throw new BadRequestHttpException(Yii::t('yii', 'Invalid data received for parameter "{param}".', [
                'param' => 'name',
            ]));
        }

        $searchModel = new LogSearch([
            'company_id' => $this->module->getCompany()->id,
            'model_name' => $model_name,
            'model_id' => $model_id,
        ]);

        $dataProvider = $searchModel->search(['LogSearch' => $params]);

        return $dataProvider;
    }
}
