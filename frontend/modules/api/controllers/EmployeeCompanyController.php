<?php

namespace frontend\modules\api\controllers;

use common\models\Company;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\employee\EmployeeSearch;
use common\models\TimeZone;
use frontend\models\EmployeeCompanySearch;
use frontend\rbac\permissions;
use Yii;
use yii\web\Response;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Class EmployeeCompanyController
 * @package frontend\modules\api\controllers
 */
class EmployeeCompanyController extends \yii\rest\ActiveController
{
    /**
     * @var string the model class name.
     */
    public $modelClass = 'common\models\EmployeeCompany';
    /**
     * @var string the scenario used for creating a model.
     * @see \yii\base\Model::scenarios()
     */
    public $createScenario = 'create';
    /**
     * @var string the scenario used for updating a model.
     * @see \yii\base\Model::scenarios()
     */
    public $updateScenario = 'default';

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'prepareDataProvider' => [$this, 'prepareDataProvider'],
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view' => [
                'class' => 'yii\rest\ViewAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'create' => [
                'class' => 'common\components\rest\CreateAction',
                'modelClass' => $this->modelClass,
                'newModel' => [$this, 'newModel'],
                'scenario' => $this->createScenario,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'update' => [
                'class' => 'yii\rest\UpdateAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'scenario' => $this->updateScenario,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
        ];
    }

    /**
     * Checks the privilege of the current user.
     *
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string $action the ID of the action to be executed
     * @param object $model the model to be accessed. If null, it means no specific model is being accessed.
     * @param array $params additional parameters
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case 'index':
                $can = Yii::$app->getUser()->can(permissions\Employee::INDEX);
                break;
            case 'view':
                $can = Yii::$app->getUser()->can(permissions\Employee::VIEW);
                break;
            case 'create':
                $can = Yii::$app->getUser()->can(permissions\Employee::CREATE);
                break;
            case 'update':
                $can = Yii::$app->getUser()->can(permissions\Employee::UPDATE, [
                    'model' => $model,
                ]);
                break;
            default:
                $can = false;
                break;
        }

        if ($can) return;

        throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
    }

    /**
     * @param yii\base\Action $action
     * @return ActiveDataProvider
     */
    public function prepareDataProvider($action)
    {
        $searchModel = new EmployeeCompanySearch([
            'company_id' => $this->module->getCompany()->id,
            'is_working' => 1,
        ]);

        return $searchModel->search(Yii::$app->request->get());
    }

    /**
     * @param string $scenario
     * @param yii\base\Action $action
     * @return yii\db\ActiveRecord
     */
    public function newModel($scenario, $action)
    {
        return new $this->modelClass([
            'scenario' => 'create',
            'send_email' => true,
            'company_id' => $this->module->getCompany()->id,
            'time_zone_id' => TimeZone::DEFAULT_TIME_ZONE,
            'is_product_admin' => true,
        ]);
    }

    /**
     * @param string $id
     * @param yii\base\Action $action
     * @return ActiveRecordInterface the model found
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id, $action = null)
    {
        $model = $this->modelClass::find()->joinWith('employee')->andWhere([
            'employee_company.employee_id' => $id,
            'employee_company.company_id' => $this->module->getCompany()->id,
            'employee.is_deleted' => false
        ])->one();

        if (isset($model)) {
            return $model;
        } else {
            throw new NotFoundHttpException("Object not found: $id");
        }
    }
}
