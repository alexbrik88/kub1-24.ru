<?php

namespace frontend\modules\api\controllers;

use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductGroupSearch;
use frontend\rbac\permissions;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class ProductGroupController
 * @package frontend\modules\api\controllers
 */
class ProductGroupController extends \yii\rest\ActiveController
{
    /**
     * @var string the model class name.
     */
    public $modelClass = 'common\models\product\ProductGroup';

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'actions' => ['options'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => [permissions\Product::INDEX],
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => [permissions\Product::CREATE],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => [permissions\Product::VIEW],
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => [permissions\Product::UPDATE],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => [permissions\Product::DELETE],
                    ],
                ],
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'prepareDataProvider' => [$this, 'prepareDataProvider'],
            ],
            'view' => [
                'class' => 'yii\rest\ViewAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
            ],
            'create' => [
                'class' => 'common\components\rest\CreateAction',
                'modelClass' => $this->modelClass,
                'newModel' => [$this, 'newModel'],
                'scenario' => $this->createScenario,
            ],
            'update' => [
                'class' => 'yii\rest\UpdateAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'scenario' => $this->updateScenario,
            ],
            'delete' => [
                'class' => 'yii\rest\DeleteAction',
                'modelClass' => $this->modelClass,
            ],
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
        ];
    }

    /**
     * @param yii\base\Action $action
     * @return ActiveDataProvider
     */
    public function prepareDataProvider($action)
    {
        $searchModel = new ProductGroupSearch([
            'company_id' => $this->module->getCompany()->id,
            'production_type' => $this->module->getParameter('production_type', [
                Product::PRODUCTION_TYPE_SERVICE,
                Product::PRODUCTION_TYPE_GOODS,
            ])
        ]);

        return $searchModel->search(Yii::$app->request->get(), '');
    }

    /**
     * @param string $scenario
     * @param yii\base\Action $action
     * @return yii\db\ActiveRecord
     */
    public function newModel($scenario, $action)
    {
        return new $this->modelClass([
            'company_id' => $this->module->getCompany()->id,
        ]);
    }

    /**
     * @param string $id
     * @param yii\base\Action $action
     * @return ActiveRecordInterface the model found
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id, $action = null)
    {
        /* @var $modelClass ActiveRecordInterface */
        $modelClass = $this->modelClass;
        $model = $modelClass::find()->where(['id' => $id])->andWhere([
            'or',
            ['company_id' => null],
            ['company_id' => $this->module->getCompany()->id],
        ])->one();

        if (isset($model)) {
            return $model;
        } else {
            throw new NotFoundHttpException("Object not found: $id");
        }
    }
}
