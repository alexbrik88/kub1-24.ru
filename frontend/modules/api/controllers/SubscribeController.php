<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.01.2018
 * Time: 5:26
 */

namespace frontend\modules\api\controllers;

use common\models\employee\Employee;
use common\models\service\Payment;
use common\models\service\PaymentType;
use common\models\service\PromoCode;
use common\models\service\PromoCodeGroup;
use common\models\service\Subscribe;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\modules\subscribe\forms\PaymentForm;
use frontend\modules\subscribe\forms\PromoCodeForm;
use frontend\modules\subscribe\forms\OnlinePaymentForm;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Response;

/**
 * Class SubscribeController
 * @package frontend\modules\api\controllers
 */
class SubscribeController extends \yii\rest\Controller
{
    public $layoutWrapperCssClass;

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
        ]);
    }

    /**
     * Declares the allowed HTTP verbs.
     * Please refer to [[VerbFilter::actions]] on how to declare the allowed verbs.
     * @return array the allowed HTTP verbs.
     */
    protected function verbs()
    {
        return [
            'create-payment'  => ['post'],
            'activate-promocode'  => ['post'],
            'payment-params'  => ['get'],
        ];
    }

    /**
     * @return Response
     * @throws \Exception
     * @throws \yii\base\Exception
     */
    public function actionPaymentParams()
    {
        $user = Yii::$app->user->identity;
        $company = $this->module->getCompany();
        if ($company === null) {
            return [
                'result' => false,
                'errors' => ['Компания не найдена.']
            ];
        }
        if ($company->strict_mode) {
            return [
                'result' => false,
                'errors' => ['Режим ограниченной функциональности. Необходимо заполнить профиль компании.']
            ];
        }

        $companyList = [];
        $tariffList = [];
        $companyArray = $user->companies;

        foreach ($companyArray as $company) {
            $companyList[] = [
                'id' => $company->id,
                'name' => $company->getTitle(true),
            ];
        }

        foreach (SubscribeTariff::paidStandartQuery()->with('discounts')->all() as $tariff) {
            $discount = [];
            foreach ($companyArray as $company) {
                $discount[$company->id] = $company->getDiscount($tariff->id);
            }
            $tariff = $tariff->toArray([], ['discounts']);
            $tariff['discountByCompany'] = $discount;

            $tariffList[] = $tariff;
        }

        return [
            'companyList' => $companyList,
            'tariffList' => $tariffList,
            'paymentTypeList' => PaymentType::find()->where(['not', ['id' => PaymentType::TYPE_PROMO_CODE]])->all(),
        ];
    }

    /**
     * @return Response
     * @throws \Exception
     * @throws \yii\base\Exception
     */
    public function actionCreatePayment()
    {
        $user = Yii::$app->user->identity;
        $company = $this->module->getCompany();
        if ($company === null) {
            return [
                'result' => false,
                'errors' => ['Компания не найдена.']
            ];
        }
        if ($company->strict_mode) {
            return [
                'result' => false,
                'errors' => ['Режим ограниченной функциональности. Необходимо заполнить профиль компании.']
            ];
        }

        $model = new PaymentForm($company, [
            'scenario' => PaymentForm::SCENARIO_COMPANY,
            'employee' => $user,
        ]);

        if ($model->load(Yii::$app->request->post(), '')) {
            if ($model->validate() && $model->makePayment()) {
                if ($model->paymentTypeId == PaymentType::TYPE_ONLINE) {
                    if ($model->payment->is_confirmed) {
                        Yii::$app->session->setFlash('success', 'Подписка уже оплачена.');
                    } else {
                        $paymentForm = new OnlinePaymentForm([
                            'scenario' => OnlinePaymentForm::SCENARIO_SEND,
                            'company' => $model->company,
                            'payment' => $model->payment,
                            'user' => $user,
                        ]);

                        return [
                            'result' => true,
                            'paymentId' => $model->payment->id,
                            'messages' => Yii::$app->session->getAllFlashes(true),
                            'onlinePayment' => [
                                'action' => Yii::$app->params['robokassa']['merchantFormAction'],
                                'method' => 'post',
                                'data' => $paymentForm->getFormFields(),
                            ],
                        ];
                    }
                }
                $messages = Yii::$app->session->getAllFlashes(true);

                return [
                    'result' => true,
                    'paymentId' => $model->payment->id,
                    'messages' => $messages,
                ];
            } else {
                $messages = (array) Yii::$app->session->getAllFlashes(true);

                return [
                    'result' => false,
                    'errors' => array_merge($model->errors, $messages),
                ];
            }
        }

        return [
            'result' => false,
            'errors' => ['Нет данных платежа.'],
        ];
    }

    /**
     * @return string|Response
     * @throws \Exception
     */
    public function actionActivatePromocode()
    {
        $company = $this->module->getCompany();
        $model = new PromoCodeForm([
            'company' => $company,
        ]);

        $model->load(Yii::$app->request->post(), '');

        if ($model->save()) {
            return [
                'result' => true,
                'messages' => 'Промокод  успешно активирован.',
            ];
        }

        return $model;
    }

    /**
     * @return array|Response
     * @throws \Exception
     */
    public function actionSubscribesExpires()
    {
        $company = $this->module->getCompany();
        $model = new PaymentForm($company);
        $formData = $model->getFormData();

        $groupArray = ArrayHelper::getValue($formData, 'groupArray', []);
        $tariffDurations = ArrayHelper::getValue($formData, 'tariffDurations', []);

        $expiresArray = [];
        foreach ($groupArray as $group) {
            $subscribes = SubscribeHelper::getPayedSubscriptions($company->id, $group->id);
            if ($subscribes) {
                $expiresArray[] = [
                    'id' => $group->id,
                    'name' => $group->name,
                    'expires_at' => $date = SubscribeHelper::getExpireDate($subscribes),
                    'days_number' => $n = SubscribeHelper::getExpireLeftDays($date),
                    'days_text' => Yii::t('yii', '{n, plural, =0{# дня} 1{# день} one{# день} few{# дня} other{# дней}}', ['n' => $n]),
                    'is_trial' => $group->id == SubscribeTariffGroup::STANDART && end($subscribes)->tariff_id == SubscribeTariff::TARIFF_TRIAL,
                ];
            }
        }

        return [
            'subscribes' => array_values($groupArray),
            'expires' => $expiresArray,
        ];
    }
}
