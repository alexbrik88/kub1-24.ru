<?php

namespace frontend\modules\api\controllers;

use common\models\Agreement;
use common\models\AgreementTitleTemplate;
use frontend\models\AgreementSearch;
use frontend\rbac\permissions;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * AgreementController
 * @package frontend\modules\api\controllers
 */
class AgreementController extends \yii\rest\ActiveController
{
    /**
     * @var common\models\Agreement
     */
    protected $_model = false;
    /**
     * @var string the model class name.
     */
    public $modelClass = 'common\models\Agreement';
    /**
     * @var string the scenario used for creating a model.
     * @see \yii\base\Model::scenarios()
     */
    public $createScenario = Agreement::SCENARIO_DEFAULT;
    /**
     * @var string the scenario used for updating a model.
     * @see \yii\base\Model::scenarios()
     */
    public $updateScenario = Agreement::SCENARIO_DEFAULT;

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return array_merge(parent::verbs(), [
            'file-get' => ['GET'],
            'file-list' => ['GET'],
            'file-upload' => ['POST'],
            'file-delete' => ['DELETE'],
            'scan-list' => ['GET'],
            'scan-bind' => ['POST'],
            'scan-unbind' => ['POST'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'prepareDataProvider' => [$this, 'prepareDataProvider'],
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view' => [
                'class' => 'yii\rest\ViewAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'create' => [
                'class' => 'common\components\rest\CreateAction',
                'modelClass' => $this->modelClass,
                'newModel' => [$this, 'newModel'],
                'scenario' => $this->createScenario,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'update' => [
                'class' => 'yii\rest\UpdateAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'scenario' => $this->updateScenario,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'delete' => [
                'class' => 'yii\rest\DeleteAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
            'file-get' => [
                'class' => \frontend\modules\api\components\actions\FileGetAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'file-list' => [
                'class' => \frontend\modules\api\components\actions\FileListAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'file-delete' => [
                'class' => \frontend\modules\api\components\actions\FileDeleteAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'file-upload' => [
                'class' => \frontend\modules\api\components\actions\FileUploadAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'scan-bind' => [
                'class' => \frontend\modules\api\components\actions\ScanBindAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'scan-unbind' => [
                'class' => \frontend\modules\api\components\actions\ScanUnbindAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'scan-list' => [
                'class' => \frontend\modules\api\components\actions\ScanListAction::className(),
                'modelClass' => $this->modelClass,
            ],
        ];
    }

    /**
     * Checks the privilege of the current user.
     *
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string $action the ID of the action to be executed
     * @param object $model the model to be accessed. If null, it means no specific model is being accessed.
     * @param array $params additional parameters
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case 'index':
                $can = Yii::$app->getUser()->can(permissions\Contractor::INDEX);
                break;
            case 'view':
                $can = Yii::$app->getUser()->can(permissions\Contractor::VIEW);
                break;
            case 'create':
                $can = Yii::$app->getUser()->can(permissions\Contractor::CREATE);
                break;
            case 'update':
                $can = Yii::$app->getUser()->can(permissions\Contractor::UPDATE, [
                    'model' => $model,
                ]);
                break;
            case 'delete':
                $can = Yii::$app->getUser()->can(permissions\Contractor::DELETE);
                break;
            default:
                $can = false;
                break;
        }

        if (!$can) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    /**
     * @param yii\base\Action $action
     * @return ActiveDataProvider
     */
    public function prepareDataProvider($action)
    {
        $searchModel = new AgreementSearch([
            'company_id' => $this->module->getCompany()->id,
        ]);

        return $searchModel->search(Yii::$app->request->get(), '');
    }

    /**
     * @param string $scenario
     * @param yii\base\Action $action
     * @return yii\db\ActiveRecord
     */
    public function newModel($scenario, $action)
    {
        return new $this->modelClass([
            'scenario' => $scenario,
            'company_id' => $this->module->getCompany()->id,
            'is_created' => true,
            'title_template_id' => AgreementTitleTemplate::TITLE_BY_TYPE,
        ]);
    }

    /**
     * @param string $id
     * @param yii\base\Action $action
     * @return ActiveRecordInterface the model found
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id, $action = null)
    {
        if ($this->_model === false) {
            $this->_model = $this->modelClass::find()->andWhere([
                'id' => $id,
                'company_id' => $this->module->getCompany()->id,
            ])->one();
        }

        if ($this->_model) {
            return $this->_model;
        } else {
            throw new NotFoundHttpException("Object not found: $id");
        }
    }
}
