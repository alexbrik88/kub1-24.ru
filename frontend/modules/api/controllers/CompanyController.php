<?php

namespace frontend\modules\api\controllers;

use common\components\date\DateHelper;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyTaxationType;
use common\models\CompanyProductType;
use common\models\EmployeeCompany;
use common\models\employee\EmployeeRole;
use common\models\service\Subscribe;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use common\models\TimeZone;
use frontend\models\RegistrationForm;
use frontend\rbac\permissions;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * CompanyController
 * @package frontend\modules\api\controllers
 */
class CompanyController extends \yii\rest\Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
                'except' => [
                    'index',
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH'],
            'change-company' => ['PUT', 'PATCH'],
        ];
    }

    /**
     * Checks the privilege of the current user.
     *
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string $action the ID of the action to be executed
     * @param object $model the model to be accessed. If null, it means no specific model is being accessed.
     * @param array $params additional parameters
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case 'index':
            case 'view':
            case 'change-company':
                $can = true;
                break;
            case 'create':
                $can = Yii::$app->getUser()->can(permissions\Company::CREATE);
                break;
            case 'update':
                $can = Yii::$app->getUser()->can(permissions\Company::UPDATE);
                break;
            default:
                $can = false;
                break;
        }

        if (!$can) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'options' => [
                'class' => 'yii\rest\OptionsAction',
                'resourceOptions' => ['GET', 'PUT', 'PATCH', 'HEAD', 'OPTIONS'],
            ],
        ];
    }

    /**
     * @return ActiveDataProvider
     */
    public function actionIndex()
    {
        $this->checkAccess($this->action->id);

        return new ActiveDataProvider([
            'query' => Yii::$app->user->identity->getCompanies()->with([
                'mainAccountant',
                'companyTaxationType',
                'companyType',
                'timeZone',
            ])->andWhere([
                'blocked' => false,
            ]),
        ]);
    }

    /**
     * @return Company
     */
    public function actionView($id)
    {
        Yii::$app->user->identity->setCompany($id);
        $model = $this->module->getCompany();

        $this->checkAccess($this->action->id, $model);

        return $model;
    }

    /**
     * @return yii\db\ActiveRecord
     */
    public function actionCreate()
    {
        $this->checkAccess($this->action->id);

        $model = new Company([
            'scenario' => Company::SCENARIO_USER_UPDATE,
            'owner_employee_id' => Yii::$app->user->id,
            'created_by' => Yii::$app->user->id,
            'time_zone_id' => TimeZone::DEFAULT_TIME_ZONE,
            'email' => $this->module->getCompany()->email,
            'main_id' => $this->module->getCompany()->main_id,
            'address_legal_is_actual' => true,
            'chief_is_chief_accountant' => true,
            'hasAccountants' => true,
        ]);

        $mainAccountant = new CheckingAccountant(['type' => CheckingAccountant::TYPE_MAIN]);
        $companyTaxationType = new CompanyTaxationType();

        $model->populateRelation('mainAccountant', $mainAccountant);
        $model->populateRelation('companyTaxationType', $companyTaxationType);

        $params = Yii::$app->getRequest()->getBodyParams();

        $model->load($params);
        $mainAccountant->load($params, 'CheckingAccountant');
        $companyTaxationType->load($params);

        $model->validate();
        $mainAccountant->validate();
        $companyTaxationType->validate();

        $isSaved = false;

        if (!$model->hasErrors() && !$mainAccountant->hasErrors() && !$companyTaxationType->hasErrors()) {
            $isSaved = Yii::$app->db->transaction(function ($db) use ($model) {
                if ($model->save()) {
                    FileHelper::createDirectory($model->getUploadPath(true, true));
                    $model->companyTaxationType->company_id = $model->id;
                    $model->mainAccountant->type = CheckingAccountant::TYPE_MAIN;
                    $model->mainAccountant->company_id = $model->id;
                    if ($model->companyTaxationType->save() && $model->mainAccountant->save()) {
                        $user = Yii::$app->user->identity;

                        $tariff = SubscribeTariff::findOne(SubscribeTariff::TARIFF_TRIAL);

                        $subscribe = new Subscribe([
                            'company_id' => $model->id,
                            'tariff_group_id' => $tariff->tariff_group_id,
                            'tariff_limit' => $tariff->tariff_limit,
                            'tariff_id' => $tariff->id,
                            'duration_month' => $tariff->duration_month,
                            'duration_day' => $tariff->duration_day,
                            'status_id' => SubscribeStatus::STATUS_PAYED,
                        ]);
                        $employeeCompany = new EmployeeCompany([
                            'employee_id' => $user->id,
                            'company_id' => $model->id,
                            'employee_role_id' => $user->employee_role_id,
                        ]);
                        $employeeCompany->setEmployee($user);
                        /* @var $notification Notification */
                        if ($subscribe->save(false) && $employeeCompany->save(false)) {
                            $model->updateAttributes(['strict_mode' => !$model->isStrictMode()]);
                            $user->updateAttributes(['company_id' => $model->id]);

                            return true;
                        }
                    }
                }
                $db->transaction->rollBack();

                return false;
            });
        }

        if ($isSaved) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $response->getHeaders()->set('Location', Url::toRoute(['view', 'id' => $model->id], true));

            return [
                'Company' => $model,
                'CompanyTaxationType' => $model->companyTaxationType,
                'CheckingAccountant' => $model->mainAccountant,
            ];
        } elseif (!$model->hasErrors() && !$companyTaxationType->hasErrors() && !$mainAccountant->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return [
            'Company' => $this->module->serializeModelErrors($model),
            'CompanyTaxationType' => $this->module->serializeModelErrors($model->companyTaxationType),
            'CheckingAccountant' => $this->module->serializeModelErrors($model->mainAccountant),
        ];
    }

    /**
     * @return yii\db\ActiveRecord
     */
    public function actionUpdate($id)
    {
        Yii::$app->user->identity->setCompany($id);
        $model = $this->module->getCompany();

        $this->checkAccess($this->action->id, $model);

        if (!Yii::$app->getUser()->can(permissions\Company::UPDATE, ['model' => $model])) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }

        $model->scenario = Company::SCENARIO_USER_UPDATE;

        $mainAccountant = $model->mainAccountant ? : new CheckingAccountant([
            'company_id' => $model->id,
            'type' => CheckingAccountant::TYPE_MAIN,
        ]);
        $companyTaxationType = $model->companyTaxationType;
        $model->populateRelation('mainAccountant', $mainAccountant);
        $model->populateRelation('companyTaxationType', $companyTaxationType);

        $params = Yii::$app->getRequest()->getBodyParams();

        $model->load($params);
        $mainAccountant->load($params, 'CheckingAccountant');
        $companyTaxationType->load($params);

        $model->validate();
        $mainAccountant->validate();
        $companyTaxationType->validate();

        $isSaved = false;
        if (!$model->hasErrors() && !$mainAccountant->hasErrors() && !$companyTaxationType->hasErrors()) {
            $isSaved = Yii::$app->db->transaction(function ($db) use ($model) {
                if ($model->save() && $model->companyTaxationType->save() && $model->mainAccountant->save()) {
                    if (Yii::$app->user->identity->employee_role_id === EmployeeRole::ROLE_CHIEF &&
                        !Yii::$app->user->identity->validate([
                            'position',
                            'lastname',
                            'firstname',
                            'patronymic',
                            'firstname_initial',
                            'patronymic_initial'
                        ])
                    ) {
                        $model->setDirectorInitials();
                    }

                    return true;
                }
                $db->transaction->rollBack();

                return false;
            });
        }

        if ($isSaved) {
            $response = Yii::$app->getResponse();
            $response->getHeaders()->set('Location', Url::toRoute(['view', 'id' => $model->id], true));

            return [
                'Company' => $model,
                'CompanyTaxationType' => $companyTaxationType,
                'CheckingAccountant' => $mainAccountant,
            ];
        } elseif (!$model->hasErrors() && !$companyTaxationType->hasErrors() && !$mainAccountant->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return [
            'Company' => $this->module->serializeModelErrors($model),
            'CompanyTaxationType' => $this->module->serializeModelErrors($companyTaxationType),
            'CheckingAccountant' => $this->module->serializeModelErrors($mainAccountant),
        ];
    }

    /**
     * @return yii\db\ActiveRecord
     */
    public function actionChangeCompany($id)
    {
        Yii::$app->user->identity->setCompany($id);
        $model = $this->module->getCompany();

        $this->checkAccess($this->action->id, $model);

        Yii::$app->user->identity->updateAttributes(['company_id' => $model->id]);

        $result = $model->toArray();

        $actualSubscribes = SubscribeHelper::getPayedSubscriptions($model->id);
        $expireDate = SubscribeHelper::getExpireDate($actualSubscribes);
        $result['expireSubscribeDate'] = date(DateHelper::FORMAT_USER_DATE, $expireDate);

        return $result;
    }

    /**
     * @return yii\db\ActiveRecord
     */
    public function findModel($id)
    {
        $model = Yii::$app->user->identity->getCompanies()->andWhere([
            'id' => $id,
            'blocked' => false,
        ])->one();

        if ($model === null) {
            throw new NotFoundHttpException("Object not found: $id");
        }

        return $model;
    }
}
