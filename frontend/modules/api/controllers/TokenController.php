<?php

namespace frontend\modules\api\controllers;

use common\models\employee\Employee;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class UserController
 * @package frontend\modules\api\controllers
 */
class TokenController extends \yii\rest\Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBasicAuth',
                'auth' => function ($username, $password) {
                    if ($username && $password) {
                        $user = Employee::findIdentityByLogin($username);
                        if ($user !== null && $user->validatePassword($password)) {
                            return $user;
                        }
                    }

                    return null;
                },
            ],
            /*'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],*/
        ]);
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'index' => ['GET'],
        ];
    }

    /**
     * @return array
     */
    public function actionIndex()
    {
        return ['access_token' => Yii::$app->user->identity->getAccessToken()];
    }
}
