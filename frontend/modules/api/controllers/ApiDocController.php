<?php

namespace frontend\modules\api\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Class ApiDocController
 * @package frontend\modules\api\controllers
 */
class ApiDocController extends \yii\web\Controller
{
    public $layout = 'swagger';

    /**
     * @return mixed
     */
    public function actionIndex($file = null)
    {
        if ($file !== null) {
            $path = Yii::getAlias("@frontend/modules/api/views/api-doc/file/{$file}");
            if (is_file($path)) {
                return \Yii::$app->response->sendFile($path);
            } else {
                throw new NotFoundHttpException();
            }
        }

        return $this->render('index');
    }

    /**
     * @return mixed
     */
    public function actionSwagger()
    {
        return \Yii::$app->response->sendContentAsFile($this->renderPartial('swagger'), 'swagger.yaml', [
            'mimeType' => 'application.yaml',
        ]);
    }
}
