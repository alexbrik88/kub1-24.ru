<?php

namespace frontend\modules\api\controllers;

use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\OrderAct;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\api\models\PngHelper;
use frontend\modules\documents\models\ActSearch;
use frontend\modules\documents\components\ActHelper;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\rbac\permissions;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class ActController
 * @package frontend\modules\api\controllers
 */
class ActController extends \yii\rest\ActiveController
{
    /**
     * @var common\models\document\Act
     */
    protected $_model = false;
    /**
     * @var string the model class name.
     */
    public $modelClass = 'common\models\document\Act';
    /**
     * @var string the scenario used for creating a model.
     * @see \yii\base\Model::scenarios()
     */
    public $createScenario = Act::SCENARIO_DEFAULT;
    /**
     * @var string the scenario used for updating a model.
     * @see \yii\base\Model::scenarios()
     */
    public $updateScenario = Act::SCENARIO_DEFAULT;

    public $layoutWrapperCssClass = '';

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'prepareDataProvider' => [$this, 'prepareDataProvider'],
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view' => [
                'class' => 'yii\rest\ViewAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'delete' => [
                'class' => 'yii\rest\DeleteAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
            'file-get' => [
                'class' => \frontend\modules\api\components\actions\FileGetAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'file-list' => [
                'class' => \frontend\modules\api\components\actions\FileListAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'file-delete' => [
                'class' => \frontend\modules\api\components\actions\FileDeleteAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'file-upload' => [
                'class' => \frontend\modules\api\components\actions\FileUploadAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'scan-bind' => [
                'class' => \frontend\modules\api\components\actions\ScanBindAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'scan-unbind' => [
                'class' => \frontend\modules\api\components\actions\ScanUnbindAction::className(),
                'modelClass' => $this->modelClass,
            ],
            'scan-list' => [
                'class' => \frontend\modules\api\components\actions\ScanListAction::className(),
                'modelClass' => $this->modelClass,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return array_merge(parent::verbs(), [
            'pdf' => ['GET'],
            'png' => ['GET'],
            'file-get' => ['GET'],
            'file-list' => ['GET'],
            'file-upload' => ['POST'],
            'file-delete' => ['DELETE'],
            'scan-list' => ['GET'],
            'scan-bind' => ['POST'],
            'scan-unbind' => ['POST'],
            'send' => ['POST'],
        ]);
    }

    /**
     * Checks the privilege of the current user.
     *
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string $action the ID of the action to be executed
     * @param object $model the model to be accessed. If null, it means no specific model is being accessed.
     * @param array $params additional parameters
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case 'index':
                $can = Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                    'ioType' => $this->module->getIoType(),
                ]);
                break;
            case 'view':
            case 'pdf':
            case 'png':
                $can = Yii::$app->getUser()->can(permissions\document\Document::VIEW, [
                    'model' => $model,
                ]);
                break;
            case 'create':
                $can = Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
                    'ioType' => $model->type,
                ]);
                break;
            case 'update':
            case 'send':
            case 'status':
                $can = Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                    'model' => $model,
                ]);
                break;
            case 'delete':
                $can = Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
                    'model' => $model,
                ]);
                break;
            default:
                $can = false;
                break;
        }

        if (!$can) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }

        $this->modelClass::$additionalFields[] = 'totalAmountWithNds';
    }

    /**
     * Creates a new Act model.
     * @param integer $invoice_id Invoice::id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionCreate($invoice_id)
    {
        /* @var Invoice $invoice */
        $invoice = $this->module->findInvoice($invoice_id);

        $this->checkAccess($this->action->id, $invoice);

        $this->checkAccess('view', $invoice);

        if ($invoice->createAct()) {
            $model = $invoice->act;
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $response->getHeaders()->set('Location', Url::toRoute(['view', 'id' => $model->id], true));

            return $model;
        }

        throw new ForbiddenHttpException('Нельзя создать Акт.');
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $this->checkAccess($this->action->id, $model);

        ActHelper::load($model, Yii::$app->getRequest()->getBodyParams());
        ActHelper::save($model);

        return $model;
    }

    /**
     * @param integer $id
     */
    public function actionPdf($id)
    {
        $model = $this->findModel($id);

        $this->checkAccess($this->action->id, $model);

        return Yii::$app->response->sendContentAsFile(
            $this->modelClass::getRenderer(
                null,
                $model,
                \common\components\pdf\PdfRenderer::DESTINATION_STRING
            )->output(false),
            $model->getPdfFileName(),
            ['mimeType' => 'application/pdf']
        );
    }

    /**
     * @param integer $id
     * @param integer $page
     */
    public function actionPng($id, $page = 1)
    {
        $model = $this->findModel($id);

        $this->checkAccess($this->action->id, $model);

        $pngInfo = PngHelper::convertPdf2Png($this->modelClass::getRenderer(
            null,
            $model,
            \common\components\pdf\PdfRenderer::DESTINATION_STRING
        )->output(false), $page);

        if ($pngInfo) {
            header('X-Pagination-Page-Count: ' . $pngInfo['pagesCount']);
            return Yii::$app->response->sendContentAsFile(
                $pngInfo['content'],
                $model->getPngFileName($page, $pngInfo['pagesCount']),
                ['mimeType' => 'image/png']
            );
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param int $id
     * @return array
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionSend($id)
    {
        $user = Yii::$app->user->identity;
        $model = $this->findModel($id);

        $this->checkAccess($this->action->id, $model);

        if (!$model->uid) {
            $model->updateAttributes(['uid' =>  $this->modelClass::generateUid()]);
        }
        $form = new \frontend\modules\documents\forms\InvoiceSendForm($user->currentEmployeeCompany, [
            'model' => $model,
        ]);

        $form->load(Yii::$app->request->post(), '');

        if ($form->validate()) {
            if ($send = $form->send()) {
                return [
                    'sent' => $send[0],
                    'total' => $send[1],
                ];
            }
        }

        if ($form->hasErrors()) {
            return $form;
        }
        throw new \yii\web\ServerErrorHttpException('Failed to send document for unknown reason.');
    }

    /**
     * @param yii\base\Action $action
     * @return ActiveDataProvider
     */
    public function prepareDataProvider($action)
    {
        $period = $this->module->getPeriod('periodFrom', 'periodTo');

        \common\models\document\AbstractDocument::$additionalFields = $this->module->getExpand();

        $searchModel = new ActSearch([
            'type' => $this->module->getIoType(),
        ]);
        $get = Yii::$app->request->get();
        $searchData = (array) ArrayHelper::remove($get, 'ActSearch', []);
        $searchData = array_merge($get, $searchData);

        return $searchModel->search(['ActSearch' => $searchData], [
            'from' => $period['from']->format('Y-m-d'),
            'to' => $period['to']->format('Y-m-d'),
        ]);
    }

    /**
     * @param $id
     * @return Act
     * @throws NotFoundHttpException
     */
    public function findModel($id)
    {
        $model = $this->modelClass::find()->joinWith('invoice')->andWhere([
            'act.id' => $id,
            'invoice.company_id' => $this->module->getCompany()->id,
            'invoice.is_deleted' => false,
        ])->one();

        if ($model === null) {
            throw new NotFoundHttpException('Model loading failed. Model not found.');
        }

        return $model;
    }
}
