<?php

namespace frontend\modules\api\controllers;

use common\models\Contractor;
use common\models\ContractorAccount;
use frontend\rbac\permissions;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class ContractorAccountController
 * @package frontend\modules\api\controllers
 */
class ContractorAccountController extends \yii\rest\ActiveController
{
    /**
     * @var string the model class name.
     */
    public $modelClass = 'common\models\ContractorAccount';

    protected $_contractor = false;

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return array_merge(parent::verbs(), [
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'prepareDataProvider' => [$this, 'prepareDataProvider'],
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view' => [
                'class' => 'yii\rest\ViewAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'create' => [
                'class' => 'common\components\rest\CreateAction',
                'modelClass' => $this->modelClass,
                'newModel' => [$this, 'newModel'],
                'scenario' => $this->createScenario,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'update' => [
                'class' => 'yii\rest\UpdateAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'scenario' => $this->updateScenario,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'delete' => [
                'class' => 'common\components\rest\DeleteAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
        ];
    }

    /**
     * Checks the privilege of the current user.
     *
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string $action the ID of the action to be executed
     * @param object $model the model to be accessed. If null, it means no specific model is being accessed.
     * @param array $params additional parameters
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case 'index':
                $can = Yii::$app->getUser()->can(permissions\Contractor::INDEX);
                break;
            case 'view':
                $can = Yii::$app->getUser()->can(permissions\Contractor::VIEW);
                break;
            case 'create':
                $can = $this->checkCompany() && Yii::$app->getUser()->can(permissions\Contractor::CREATE);
                break;
            case 'update':
                $can = $this->checkCompany() && Yii::$app->getUser()->can(permissions\Contractor::UPDATE, [
                    'model' => $model->contractor,
                ]);
                break;
            case 'delete':
                $can = $this->checkCompany() && Yii::$app->getUser()->can(permissions\Contractor::DELETE);
                break;
            default:
                $can = false;
                break;
        }

        if ($can) return;

        throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
    }

    /**
     * @return boolean
     */
    public function checkCompany()
    {
        if ($this->getContractor()->company_id == $this->module->getCompany()->id) {
            return true;
        }

        return false;
    }

    /**
     * @param yii\base\Action $action
     * @return ActiveDataProvider
     */
    public function prepareDataProvider($action)
    {
        return Yii::createObject([
            'class' => ActiveDataProvider::className(),
            'query' => $this->getContractor()->getContractorAccounts()->orderBy([
                'is_main' => SORT_DESC,
            ])->andFilterWhere([
                'is_main' => Yii::$app->getRequest()->get('is_main'),
                'bik' => Yii::$app->getRequest()->get('bik'),
                'rs' => Yii::$app->getRequest()->get('rs'),
            ]),
        ]);
    }

    /**
     * @param string $scenario
     * @param yii\base\Action $action
     * @return yii\db\ActiveRecord
     */
    public function newModel($scenario, $action)
    {
        $contractor = $this->getContractor();
        $model = new $this->modelClass([
            'scenario' => $scenario,
            'contractor_id' => $contractor->id,
        ]);
        $model->populateRelation('contractor', $contractor);

        return $model;
    }

    /**
     * @param string $id
     * @param yii\base\Action $action
     * @return ActiveRecordInterface the model found
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id, $action = null)
    {
        $contractor = $this->getContractor();
        $model = $this->modelClass::find()->andWhere([
            'id' => $id,
            'contractor_id' => $contractor->id,
        ])->one();

        if (isset($model)) {
            $model->populateRelation('contractor', $contractor);

            return $model;
        } else {
            throw new NotFoundHttpException("Object not found: $id");
        }
    }

    /**
     * @return ActiveRecordInterface the model found
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function getContractor()
    {
        if ($this->_contractor === false) {
            $this->_contractor = Contractor::find()->byDeleted()->andWhere([
                'id' => Yii::$app->request->get('cid'),
            ])->byCompany($this->module->getCompany()->id)->one();
        }

        if ($this->_contractor) {
            return $this->_contractor;
        } else {
            throw new NotFoundHttpException("Contractor not found");
        }
    }
}
