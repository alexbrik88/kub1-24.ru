<?php

namespace frontend\modules\api\controllers;

use frontend\modules\cash\components\CashStatisticInfo;
use frontend\rbac\permissions;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class CashController
 * @package frontend\modules\api\controllers
 */
class CashController extends \yii\rest\Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'statistic' => ['GET'],
        ];
    }

    /**
     * Checks the privilege of the current user.
     *
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string $action the ID of the action to be executed
     * @param object $model the model to be accessed. If null, it means no specific model is being accessed.
     * @param array $params additional parameters
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case 'statistic':
                $can = Yii::$app->getUser()->can(permissions\Cash::INDEX);
                break;
            default:
                $can = false;
                break;
        }

        if (!$can) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    /**
     * @return array
     */
    public function actionStatistic()
    {
        $this->checkAccess($this->action->id);

        $period = $this->module->getPeriod('periodFrom', 'periodTo');

        return [
            'periodFrom' => $period['from'],
            'periodTo' => $period['to'],
            'statistic' => CashStatisticInfo::getPeriodStatisticInfo($period),
        ];
    }
}
