<?php

namespace frontend\modules\api\controllers;

use common\models\Contractor;
use frontend\components\StatisticPeriod;
use frontend\models\ContractorSearch;
use frontend\rbac\permissions;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

/**
 * Class ContractorController
 * @package frontend\modules\api\controllers
 */
class ContractorController extends \yii\rest\ActiveController
{
    /**
     * @var string the model class name.
     */
    public $modelClass = 'common\models\Contractor';
    /**
     * @var string the scenario used for creating a model.
     * @see \yii\base\Model::scenarios()
     */
    public $createScenario = 'insert';
    /**
     * @var string the scenario used for updating a model.
     * @see \yii\base\Model::scenarios()
     */
    public $updateScenario = 'insert';

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'authenticator' => [
                'class' => 'yii\filters\auth\HttpBearerAuth',
            ],
            'preprocess' => [
                'class' => 'common\components\rest\ApiPreprocess',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return array_merge(parent::verbs(), [
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'prepareDataProvider' => [$this, 'prepareDataProvider'],
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view' => [
                'class' => 'yii\rest\ViewAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'create' => [
                'class' => 'common\components\rest\CreateAction',
                'modelClass' => $this->modelClass,
                'newModel' => [$this, 'newModel'],
                'scenario' => $this->createScenario,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'update' => [
                'class' => 'yii\rest\UpdateAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'scenario' => $this->updateScenario,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'delete' => [
                'class' => 'common\components\rest\DeleteAction',
                'modelClass' => $this->modelClass,
                'findModel' => [$this, 'findModel'],
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
        ];
    }

    /**
     * Checks the privilege of the current user.
     *
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string $action the ID of the action to be executed
     * @param object $model the model to be accessed. If null, it means no specific model is being accessed.
     * @param array $params additional parameters
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case 'index':
                $can = Yii::$app->getUser()->can(permissions\Contractor::INDEX, [
                    'type' => Yii::$app->request->get('type'),
                ]);
                break;
            case 'view':
                $can = Yii::$app->getUser()->can(permissions\Contractor::VIEW, [
                    'model' => $model,
                ]);
                break;
            case 'create':
                $can = Yii::$app->getUser()->can(permissions\Contractor::CREATE, [
                    'type' => Yii::$app->request->get('type'),
                ]);
                break;
            case 'update':
                $can = Yii::$app->getUser()->can(permissions\Contractor::UPDATE, [
                    'model' => $model,
                ]);
                break;
            case 'delete':
                $can = Yii::$app->getUser()->can(permissions\Contractor::DELETE, [
                    'model' => $model,
                ]);
                break;
            default:
                $can = false;
                break;
        }

        if ($can) return;

        throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
    }

    /**
     * @param yii\base\Action $action
     * @return ActiveDataProvider
     */
    public function prepareDataProvider($action)
    {
        $searchModel = new ContractorSearch([
            'company_id' => $this->module->getCompany()->id,
            'status' => Contractor::ACTIVE,
            'type' => $this->module->getParameter('type', [
                Contractor::TYPE_SELLER,
                Contractor::TYPE_CUSTOMER,
            ]),
        ]);

        return $searchModel->search(Yii::$app->request->get(), StatisticPeriod::getSessionPeriod(), '');
    }

    /**
     * @param string $scenario
     * @param yii\base\Action $action
     * @return yii\db\ActiveRecord
     */
    public function newModel($scenario, $action)
    {
        return new $this->modelClass([
            'scenario' => $scenario,
            'company_id' => $this->module->getCompany()->id,
            'employee_id' => Yii::$app->user->identity->id,
        ]);
    }

    /**
     * @param string $id
     * @param yii\base\Action $action
     * @return ActiveRecordInterface the model found
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id, $action = null)
    {
        $model = $this->modelClass::find()->byDeleted()->andWhere([
            'id' => $id,
        ])->byCompany($this->module->getCompany()->id)->with([
            'companyType',
        ])->one();

        if (isset($model)) {
            return $model;
        } else {
            throw new NotFoundHttpException("Object not found: $id");
        }
    }
}
