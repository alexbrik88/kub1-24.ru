<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 08.11.2017
 * Time: 11:12
 */

namespace frontend\modules\api\controllers;

use common\models\reference\CodeOkvedSection;
use common\models\reference\OkvedCode;
use common\models\reference\OkvedCodeCategory;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class CodeOkvedController
 * @package frontend\modules\api\controllers
 */
class CodeOkvedController extends \yii\rest\Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['authenticator']);

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'get-codes' => ['POST'],
            'get-categories' => ['POST'],
            'get-sections' => ['POST'],
        ];
    }

    /**
     * @return array
     */
    public function actionGetCodes()
    {
        $data = Yii::$app->request->post('OkvedCode');
        $code = ArrayHelper::getValue($data, 'code');
        $name = ArrayHelper::getValue($data, 'name');
        $sectionID = ArrayHelper::getValue($data, 'sectionID');
        $categoryID = ArrayHelper::getValue($data, 'categoryID');
        $byClass = ArrayHelper::getValue($data, 'by_class');
        $bySection = ArrayHelper::getValue($data, 'by_section');
        $byCategory = ArrayHelper::getValue($data, 'by_category');
        $result = [];
        $codes = OkvedCode::find()
            ->joinWith('category')
            ->joinWith('section')
            ->andFilterWhere(['like', OkvedCode::tableName() . '.code', $code])
            ->andFilterWhere(['like', OkvedCode::tableName() . '.name', $name])
            ->andFilterWhere(['like', OkvedCode::tableName() . '.category_id', $categoryID])
            ->andFilterWhere(['like', OkvedCode::tableName() . '.section_id', $sectionID]);

        if ($byClass) {
            /* @var $okvedCode OkvedCode */
            foreach ($codes->all() as $okvedCode) {
                if ($okvedCode->class !== null) {
                    if (isset($result[$okvedCode->class])) {
                        $result[$okvedCode->class]['codes'][] = [
                            'id' => $okvedCode->id,
                            'code' => $okvedCode->code,
                            'name' => $okvedCode->name,
                        ];
                    } else {
                        $result[$okvedCode->class] = [
                            'name' => $okvedCode->class,
                            'description' => $okvedCode->class_name,
                            'codes' => [
                                [
                                    'id' => $okvedCode->id,
                                    'code' => $okvedCode->code,
                                    'name' => $okvedCode->name,
                                ],
                            ]
                        ];
                    }
                }
            }
        } elseif ($bySection) {
            /* @var $okvedCode OkvedCode */
            foreach ($codes->all() as $okvedCode) {
                if ($okvedCode->section) {
                    if (isset($result[$okvedCode->section_id])) {
                        $result[$okvedCode->section_id]['codes'][] = [
                            'id' => $okvedCode->id,
                            'code' => $okvedCode->code,
                            'name' => $okvedCode->name,
                        ];
                    } else {
                        $result[$okvedCode->section_id] = [
                            'name' => $okvedCode->section->name,
                            'description' => $okvedCode->section->description,
                            'codes' => [
                                [
                                    'id' => $okvedCode->id,
                                    'code' => $okvedCode->code,
                                    'name' => $okvedCode->name,
                                ],
                            ]
                        ];
                    }
                }
            }
        } elseif ($byCategory) {
            /* @var $okvedCode OkvedCode */
            foreach ($codes->all() as $okvedCode) {
                if ($okvedCode->category) {
                    if (isset($result[$okvedCode->category_id])) {
                        $result[$okvedCode->category_id]['codes'][] = [
                            'id' => $okvedCode->id,
                            'code' => $okvedCode->code,
                            'name' => $okvedCode->name,
                        ];
                    } else {
                        $result[$okvedCode->category_id] = [
                            'name' => $okvedCode->category->name,
                            'description' => '',
                            'codes' => [
                                [
                                    'id' => $okvedCode->id,
                                    'code' => $okvedCode->code,
                                    'name' => $okvedCode->name,
                                ],
                            ]
                        ];
                    }
                }
            }
        } else {
            $result = $codes->asArray()->all();
        }

        return [
            'result' => true,
            'codes' => $result,
        ];
    }

    public function actionGetCodesByClass()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* @var $okvedCodes OkvedCode[] */
        $okvedCodes = OkvedCode::find()->all();
        $result = [];
        foreach ($okvedCodes as $okvedCode) {
            if (isset($result[$okvedCode->class])) {
                $result[$okvedCode->class]['codes'][] = [
                    'id' => $okvedCode->id,
                    'code' => $okvedCode->code,
                    'name' => $okvedCode->name,
                ];
            } else {
                $result[$okvedCode->class] = [
                    'class' => $okvedCode->class,
                    'class_name' => $okvedCode->class_name,
                    'codes' => [
                        [
                            'id' => $okvedCode->id,
                            'code' => $okvedCode->code,
                            'name' => $okvedCode->name,
                        ],
                    ]
                ];
            }
        }

        return [
            'result' => false,
            'data' => $result,
        ];
    }

    /**
     * @return array
     */
    public function actionGetCategories()
    {
        return [
            'result' => true,
            'categories' => OkvedCodeCategory::find()->asArray()->all(),
        ];
    }

    /**
     * @return array
     */
    public function actionGetSections()
    {
        return [
            'result' => true,
            'categories' => CodeOkvedSection::find()->asArray()->all(),
        ];
    }
}
