<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */

$this->title = 'Банк';

?>

<div class="row">
    <iframe src="<?= Url::to(['/cash/bank/banking']) ?>" style="
        display: block;
        margin: 0;
        background: #ffffff;
        border: none;
        height: 100vh;
        width: 100%;
    "></iframe>
</div>