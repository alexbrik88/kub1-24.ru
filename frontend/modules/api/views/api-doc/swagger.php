swagger: "2.0"
info:
  version: 1
  title: KUB Api Documentation
host: lk.kub-24.ru
tags:
- name: agreement
  description: Договор с контрагентом
- name: cash
  description: Деньги
- name: site
  description: Регистрация и восстановление пароля
- name: token
  description: Уникальный токен доступа
- name: reference
  description: Списки, статичные данные
- name: profile
  description: Профиль пользователя
- name: company
  description: Компании
- name: contractor
  description: Контрагенты
- name: contractor-account
  description: Счета контрагента
- name: employee-company
  description: Сотрудники
- name: product
  description: Товары/услуги
- name: product-group
  description: Группы товаров/услуг
- name: invoice
  description: Счета
- name: act
  description: Акты
- name: packing-list
  description: Товарные накладные
- name: invoice-facture
  description: Счета-фактуры
- name: upd
  description: УПД
- name: payment-order
  description: Платежное поручение
- name: cash
  description:
- name: scan-document
  description: Сканы документов
- name: out-invoice-widget
  description: Виджет выставления счетов по внешней ссылке
- name: log
  description: Действия
- name: subscribe
  description: Оплата сервиса
- name: email
  description: Отправка писем
- name: file
  description: Файлы документов
schemes:
  - https

paths:

<?= $this->render('swagger/paths/site') ?>
<?= $this->render('swagger/paths/token') ?>
<?= $this->render('swagger/paths/reference') ?>
<?= $this->render('swagger/paths/cash/bank') ?>
<?= $this->render('swagger/paths/cash/e-money') ?>
<?= $this->render('swagger/paths/cash/order') ?>
<?= $this->render('swagger/paths/company') ?>
<?= $this->render('swagger/paths/contractor') ?>
<?= $this->render('swagger/paths/contractor-account') ?>
<?= $this->render('swagger/paths/agreement') ?>
<?= $this->render('swagger/paths/employee-company') ?>
<?= $this->render('swagger/paths/product') ?>
<?= $this->render('swagger/paths/product-group') ?>
<?= $this->render('swagger/paths/profile') ?>
<?= $this->render('swagger/paths/invoice') ?>
<?= $this->render('swagger/paths/act') ?>
<?= $this->render('swagger/paths/packing-list') ?>
<?= $this->render('swagger/paths/invoice-facture') ?>
<?= $this->render('swagger/paths/upd') ?>
<?= $this->render('swagger/paths/payment-order') ?>
<?= $this->render('swagger/paths/scan-document') ?>
<?= $this->render('swagger/paths/out-invoice-widget') ?>
<?= $this->render('swagger/paths/log') ?>
<?= $this->render('swagger/paths/subscribe') ?>
<?= $this->render('swagger/paths/email') ?>
<?= $this->render('swagger/paths/file') ?>

components:
  parameters:
    AuthorizationBasic:
      name: Authorization
      in: header
      type: string
      description: '"Basic {base64(login:password)}"'
      required: true
    AuthorizationBearer:
      name: Authorization
      in: header
      type: string
      scheme: bearer
      description: '"Bearer {access_token}"'
      required: true
    AuthorizationApiKey:
      name: X-ApiKey
      in: header
      type: string
      description: '"{api_key}"'
      required: true
    CurrentCompany:
      name: X-Company-Id
      in: header
      type: integer
      description: Идентификатор текущей компании пользователя
      required: true
    objectId:
      name: id
      in: path
      type: integer
      description: Идентификатор объекта
      required: true
    documentType:
      name: type
      in: query
      type: integer
      schema:
        type: integer
      description: |
        Тип документа
        "1" входящий
        "2" исходящий
      required: true
    sort:
      name: sort
      in: query
      type: string
      description: |
        Сортировка списка
        "attribute_name" sort asc
        "-attribute_name" sort desc
      required: false
  schemas:

<?= $this->render('swagger/schemas/Act') ?>
<?= $this->render('swagger/schemas/Agreement') ?>
<?= $this->render('swagger/schemas/CashBankFlows') ?>
<?= $this->render('swagger/schemas/CashEMoneyFlows') ?>
<?= $this->render('swagger/schemas/CashOrderFlows') ?>
<?= $this->render('swagger/schemas/CheckingAccountant') ?>
<?= $this->render('swagger/schemas/Company') ?>
<?= $this->render('swagger/schemas/CompanyTaxationType') ?>
<?= $this->render('swagger/schemas/Contractor') ?>
<?= $this->render('swagger/schemas/ContractorAccount') ?>
<?= $this->render('swagger/schemas/EmployeeCompany') ?>
<?= $this->render('swagger/schemas/Invoice') ?>
<?= $this->render('swagger/schemas/Order') ?>
<?= $this->render('swagger/schemas/Product') ?>
<?= $this->render('swagger/schemas/ProductGroup') ?>
<?= $this->render('swagger/schemas/PaymentOrder') ?>
<?= $this->render('swagger/schemas/ScanDocument') ?>

definitions:

<?= $this->render('swagger/definitions/CashBankFlows') ?>
<?= $this->render('swagger/definitions/CashEMoneyFlows') ?>
<?= $this->render('swagger/definitions/CashOrderFlows') ?>
<?= $this->render('swagger/definitions/CheckingAccountant') ?>
<?= $this->render('swagger/definitions/Company') ?>
<?= $this->render('swagger/definitions/CompanyTaxationType') ?>
<?= $this->render('swagger/definitions/Contractor') ?>
<?= $this->render('swagger/definitions/ContractorAccount') ?>
<?= $this->render('swagger/definitions/EmployeeCompany') ?>
<?= $this->render('swagger/definitions/Product') ?>
<?= $this->render('swagger/definitions/ProductGroup') ?>
<?= $this->render('swagger/definitions/Invoice') ?>
<?= $this->render('swagger/definitions/Act') ?>
<?= $this->render('swagger/definitions/Agreement') ?>
<?= $this->render('swagger/definitions/PackingList') ?>
<?= $this->render('swagger/definitions/PaymentOrder') ?>
<?= $this->render('swagger/definitions/InvoiceFacture') ?>
<?= $this->render('swagger/definitions/Upd') ?>
<?= $this->render('swagger/definitions/ScanDocument') ?>
<?= $this->render('swagger/definitions/Log') ?>
