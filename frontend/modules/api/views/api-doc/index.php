<?php

use yii\helpers\Url;

$this->title = 'KUB Api Documentation';

$url = Url::base(true) . '/api/api-doc/swagger.yaml';

$JS = <<<JS
$(document).ready(function () {
    // Build a system
    const ui = SwaggerUIBundle({
        url: "{$url}",
        dom_id: "#swagger-ui",
        deepLinking: true,
        docExpansion: 'none',
        presets: [
        SwaggerUIBundle.presets.apis,
            SwaggerUIStandalonePreset
        ],
        plugins: [
            SwaggerUIBundle.plugins.DownloadUrl
        ],
        layout: "StandaloneLayout"
    });

    window.ui = ui;
});
JS;
$this->registerJs($JS);
?>

<div id="swagger-ui"></div>
