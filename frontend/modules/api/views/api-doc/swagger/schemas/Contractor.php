
    Contractor:
      type: object
      properties:
        face_type:
            type: integer
            description: |
                "0" Юр.лицо
                "1" Физ.лицо
                "2" Иностранное юр.лицо
        status:
            type: integer
            description: |
                "1" Активный
                "0" Неактивный
        companyTypeId:
            type: integer
            description: Форма компании, если face_type="0"
        ITN:
            type: string
            description: ИНН
        name:
            type: string
            description: Название контрагента
        director_post_name:
            type: string
            description: Должность руководителя
        director_name:
            type: string
            description: ФИО руководителя
        director_email:
            type: string
            description: E-mail руководителя
        director_phone:
            type: string
            description: Телефон руководителя
        chief_accountant_is_director:
            type: boolean
            description: Главный бухгалтер совпадает с руководителем
        chief_accountant_name:
            type: string
            description: ФИО главного бухгалтера
        chief_accountant_email:
            type: string
            description: E-mail главного бухгалтера
        chief_accountant_phone:
            type: string
            description: Телефон главного бухгалтера
        contact_is_director:
            type: boolean
            description: Контакт совпадает с руководителем
        contact_name:
            type: string
            description: ФИО контакта
        contact_email:
            type: string
            description: E-mail контакта
        contact_phone:
            type: string
            description: Телефон контакта
        legal_address:
            type: string
            description: Юридический адрес
        actual_address:
            type: string
            description: Фактический адрес
        postal_address:
            type: string
            description: Адрес для почты
        PPC:
            type: string
            description: КПП
        okpo:
            type: string
            description: ОКПО
        BIN:
            type: string
            description: ОГРН
        current_account:
            type: string
            description: Р/с
        BIC:
            type: string
            description: БИК банка
        bank_name:
            type: string
            description: Наименование банка
        bank_city:
            type: string
            description: Город банка
        corresp_account:
            type: string
            description: К/с банка
        taxation_system:
            type: integer
            description: "Налогообложение: 1 - С НДС, 0 - Без НДС"
        physical_firstname:
            type: string
            description: Имя физ.лица
        physical_lastname:
            type: string
            description: Фамилия физ.лица
        physical_patronymic:
            type: string
            description: Отчество физ.лица
        physical_no_patronymic:
            type: boolean
            description: Нет отчества физ.лица
        physical_passport_isRf:
            type: string
            description: "Паспорт: 1 - РФ, 0 - не РФ"
        physical_passport_series:
            type: string
            description: Серия
        physical_passport_number:
            type: string
            description: Номер
        physical_passport_issued_by:
            type: string
            description: Кем выдан
        physical_passport_date_output:
            type: string
            description: Дата выдачи
        physical_passport_department:
            type: string
            description: Код подразделения
        physical_address:
            type: string
            description: Адрес регистрации физ.лица
        responsible_employee_id:
            type: integer
            description: Ответственный сотрудник
        source:
            type: string
            description: Источник
        comment:
            type: string
            description: Комментарии
        payment_delay:
            type: string
            description: Отсрочка оплаты в днях
        order_currency:
            type: string
            description: Выставлять счета в валюте, если face_type=2
        not_accounting:
            type: boolean
            description: Не для бухгалтерии
        discount:
            type: number
            description: Фиксированная скидка на всё
      required:
        - face_type
        - ITN
        - name
        - companyTypeId
        - physical_lastname
        - physical_firstname
        - physical_patronymic
        - taxation_system
        - status
        - legal_address
        - physical_address
        - responsible_employee_id
        - director_post_name
        - director_name
