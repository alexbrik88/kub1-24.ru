
    CheckingAccountant:
      type: object
      properties:
        bik:
          type: string
        rs:
          type: string
      required:
        - bik
        - rs
