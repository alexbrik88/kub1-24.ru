
    ContractorAccount:
      type: object
      properties:
        bik:
          type: string
          description: БИК банка
        rs:
          type: string
          description: Номер расчетного счета
        is_main:
          type: integer
          description: Показатель основного р/с
          enum: [0,1]
      required:
        - bik
        - rs
