
    CashEMoneyFlows:
      type: object
      properties:
        emoney_id:
          type: integer
        contractor_id:
          type: integer
        expenditure_item_id:
          type: integer
        income_item_id:
          type: integer
        amount:
          type: number
        date:
          type: string
          format: date
        description:
          type: string
        invoices_list:
          type: array
          description: Оплаченные счета
          items:
            type: integer
            description: ID счета
      required:
        - emoney_id
        - contractor_id
        - income_item_id
        - expenditure_item_id
        - amount
        - date
        - description
