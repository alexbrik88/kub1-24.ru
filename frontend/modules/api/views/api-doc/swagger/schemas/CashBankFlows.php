
    CashBankFlows:
      type: object
      properties:
        rs:
          type: string
          description: Номер р/с компании
        contractor_id:
          type: integer
          description: ID контрагента
        expenditure_item_id:
          type: integer
          description: ID статьи расхода
        income_item_id:
          type: integer
          description: ID статьи прихода
        amount:
          type: number
          description: сумма
        date:
          type: string
          format: date
          description: дата
        recognition_date:
          type: string
          format: date
          description: дата признания дохода/расхода
        payment_order_number:
          type: string
          description: Номер ПП
        description:
          type: string
          description: Назначение
        taxpayers_status_id:
          type: integer
          description: Статус плательщика
        kbk:
          type: string
          description: КБК
        oktmo_code:
          type: string
          description: Код ОКТМО
        payment_details_id:
          type: integer
          description: ID Основания платежа
        tax_period_code:
          type: string
          description: Код налогового периода
        document_number_budget_payment:
          type: string
          description: Номер бюджетного документа
        document_date_budget_payment:
          type: string
          format: date
          description: Дата бюджетного платежа
        payment_type_id:
          type: integer
          description: ID Типа платежа
        uin_code:
          type: string
          description: Код УИН
        is_prepaid_expense:
          type: integer
          enum: [0,1]
          description: Признак авансового платежа
        invoices_list:
          type: array
          description: Оплаченные счета
          items:
            type: integer
            description: ID счета
      required:
        - rs
        - contractor_id
        - income_item_id
        - expenditure_item_id
        - amount
        - date
        - description
