
    Product:
      type: object
      properties:
        production_type:
          type: integer
        title:
          type: string
        code:
          type: string
        article:
          type: string
        barcode:
          type: string
        item_type_code:
          type: string
        product_unit_id:
          type: integer
        box_type:
          type: string
        count_in_place:
          type: string
        place_count:
          type: string
        mass_gross:
          type: string
        has_excise:
          type: boolean
        excise:
          type: string
        price_for_buy_nds_id:
          type: string
        price_for_buy_with_nds:
          type: string
        price_for_sell_nds_id:
          type: string
        price_for_sell_with_nds:
          type: string
        country_origin_id:
          type: integer
        customs_declaration_number:
          type: string
        group_id:
          type: integer
        not_for_sale:
          type: boolean
        provider_id:
          type: integer
        comment:
          type: string
        weight:
          type: string
        volume:
          type: string
        image:
          type: file
        comment_photo:
          type: string
      required:
        - production_type
        - title
        - product_unit_id
        - price_for_buy_with_nds
        - price_for_sell_with_nds
