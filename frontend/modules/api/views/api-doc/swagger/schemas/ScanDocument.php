
    ScanDocument:
      type: object
      properties:
        name:
          type: string
          description: Название
        description:
          type: string
          description: Описание
        contractor_id:
          type: integer
          description: ID контрагента
        document_type_id:
          type: integer
          description: ID типа документа
        upload:
          type: file
          description: |
            Файл скана документа
            до 3МБ (jpg, jpeg, png, bmp, pdf)
      required:
        - upload
