
    Agreement:
      type: object
      properties:
        contractor_id:
          type: integer
        document_type_id:
          type: integer
        document_name:
          type: string
        document_number:
          type: string
        document_date:
          type: string
          format: date
        files_upload:
          description: Uploading file array
          schema:
            type: file
        files_delete:
          description: Agreement ID array
          schema:
            type: integer
      required:
        - contractor_id
        - document_type_id
        - document_name
        - document_number
        - document_date
