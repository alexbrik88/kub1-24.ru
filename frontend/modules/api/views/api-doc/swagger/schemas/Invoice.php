
    Invoice:
      type: object
      properties:
        document_number:
          type: string
          description: Номер счета
        document_additional_number:
          type: string
          description: дополнительный номер
        document_date:
          type: date
          description: Дата счета
        company_rs:
          type: string
          description: Р/с компании
        contractor_id:
          type: integer
          description: ID контрагента (GET /api/contractor)
        payment_limit_date:
          type: date
          description: Дата "Оплатить до"
        remind_contractor:
          type: boolean
          description: |
            Отправить напоминание покупателю (если счет исходящий)
            "1" - Да
            "0" - Нет
        agreement:
          type: string
          description: |
            Основание (GET /api/agreement)
            Строка из значений свойст объекта Agreement, соединенных символом "&"
            "document_name&document_number&document_date&document_type_id&id"
        invoice_expenditure_item_id:
          type: integer
          description: ID Статьи расходов входящего счета (GET /api/reference/expenditure)
        has_discount:
          type: boolean
          description: |
            Указать скидку
            "1" - Да
            "0" - Нет
        has_markup:
          type: boolean
          description: |
            Указать наценку
            "1" - Да
            "0" - Нет
        show_paylimit_info:
          type: boolean
          description: |
            Указать срок оплаты
            "1" - Да
            "0" - Нет
        comment:
          type: string
          description: Комментарий в счете для покупателя
        price_precision:
          type: integer
          description: Количество знаков после запятой
          enum: [2,4]
        nds_view_type_id:
          type: integer
          enum: [0,1,2]
          description: |
            ID Типа отображения НДС (GET /api/reference/nds-type)
            Для нового счета брать из "company.nds_view_type_id"
            Для входящего счета, после выбора контрагента, из "contractor.nds_view_type_id"

      required:
        - document_number
        - document_date
        - company_rs
        - contractor_id
        - payment_limit_date
        - nds_view_type_id
