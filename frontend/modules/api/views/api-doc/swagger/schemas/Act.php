
    Act:
      type: object
      properties:
        document_number:
          type: string
        document_additional_number:
          type: string
        document_date:
          type: string
          format: date
      required:
        - document_number
        - document_date
