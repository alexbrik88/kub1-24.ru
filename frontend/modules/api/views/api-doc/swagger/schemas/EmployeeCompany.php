
    EmployeeCompany:
      type: object
      properties:
        lastname:
          type: string
          description: Фамилия
        firstname:
          type: string
          description: Имя
        patronymic:
          type: string
          description: Отчество
        sex:
          type: integer
          description: Пол
        birthday:
          type: date
          description: Дата рождения
        date_hiring:
          type: date
          description: Дата приема на работу
        date_dismissal:
          type: date
          description: Дата увольнения
        employee_role_id:
          type: integer
          description: Роль
        is_product_admin:
          type: integer
          description: Может добавлять номенклатуру
        phone:
          type: string
          description: Телефон
        time_zone_id:
          type: integer
          description: ID таймзоны
        can_invoice_add_flow:
          type: integer
          description: Может добавлять оплату по счетам (для Менеджера или Руководителя отдела)
        can_sign:
          type: integer
          description: Право подписи на документах
        sign_document_type_id:
          type: integer
          description: ID типа документа, дающего право подписи
        sign_document_number:
          type: integer
          description: Номер документа, дающего право подписи
        sign_document_date:
          type: integer
          description: Дата документа, дающего право подписи
        can_view_price_for_buy:
          type: integer
          description: Видит закупочные цены
      required:
        - email
        - lastname
        - firstname
        - patronymic
        - position
        - employee_role_id
        - date_hiring
        - phone
        - sex
