
    Company:
      type: object
      properties:
        self_employed:
          type: boolean
          description: '"1" - Самозанятый'
        company_type_id:
          type: integer
          description: Тип компании
        inn:
          type: string
          description: ИНН
        name_short:
          type: string
          description: Название организации (если тип коипнии не "ИП")
        name_full:
          type: string
          description: Полное наименование организации (если тип коипнии не "ИП")
        time_zone_id:
          type: integer
          description: Часовой пояс, ID.
        pdf_signed:
          type: boolean
          description: '"1" - Формировать PDF счета с печатью и подписью'
        pdf_act_signed:
          type: boolean
          description: '"1" - Формировать PDF акта с печатью и подписью'
        pdf_send_signed:
          type: boolean
          description: '"1" - Отправлять счет с печатью и подписью'
        pdf_act_send_signed:
          type: boolean
          description: '"1" - Отправлять акт с печатью и подписью'
        is_additional_number_before:
          type: boolean
          description: '"1" - Доп. номер перед номером счета'
        phone:
          type: string
          description: Телефон
        email:
          type: string
          description: Электронная почта
        chief_post_name:
          type: string
          description: Должность руководителя
        chief_lastname:
          type: string
          description: Фамилия руководителя
        chief_firstname:
          type: string
          description: Имя руководителя
        chief_patronymic:
          type: string
          description: Отчество руководителя
        has_chief_patronymic:
          type: boolean
          description: '"1" - Нет отчества руководителя'
        chief_is_chief_accountant:
          type: boolean
          description: '"1" - Гл. бухгалтер совпадает с руководителем'
        chief_accountant_lastname:
          type: string
          description: Фамилия бухгалтера
        chief_accountant_firstname:
          type: string
          description: Имя бухгалтера
        chief_accountant_patronymic:
          type: string
          description: Отчество бухгалтера
        has_chief_accountant_patronymic:
          type: boolean
          description: '"1" - Нет отчества бухгалтера'
        ogrn:
          type: string
          description: ОГРН
        kpp:
          type: string
          description: КПП (если тип коипнии не "ИП")
        okpo:
          type: string
          description: ОКПО
        address_legal:
          type: string
          description: Юридический адрес
        address_actual:
          type: string
          description: Фактический адрес
        okved:
          type: string
          description: ОКВЭД
        okato:
          type: string
          description: ОКАТО
        oktmo:
          type: string
          description: ОКТМО
        okogu:
          type: string
          description: ОКОГУ
        okfs:
          type: string
          description: ОКФС
        okopf:
          type: string
          description: ОКОПФ
        pfr:
          type: string
          description: ПФР
        fss:
          type: string
          description: ФСС
        tax_authority_registration_date:
          type: string
          description: Дата постановки на учёт в налоговом органе (дд.мм.гггг)
        ifns_ga:
          type: string
          description: ИФНС
        deleteLogoImage:
          type: boolean
          description: '"1" - Удалить логотип'
        deletePrintImage:
          type: boolean
          description: '"1" - Удалить печать'
        deleteChiefSignatureImage:
          type: boolean
          description: '"1" - Удалить подпись руководителя'
        deleteChiefAccountantSignatureImage:
          type: string
          description: '"1" - Удалить подпись бухгалтера'
      required:
        - company_type_id
        - inn
        - name_short
        - name_full
        - time_zone_id
        - phone
        - email
        - chief_post_name
        - chief_lastname
        - chief_firstname
        - chief_patronymic
        - kpp
        - address_legal
