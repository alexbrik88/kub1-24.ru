
    Order:
      type: object
      properties:
        id:
          type: integer
          description: ID наименования, если редактирование
        product_id:
          type: integer
          description: ID товара/услуги (GET /api/product)
        title:
          type: string
          description: Наименование товара/услуги
        quantity:
          type: number
          description: Количество
        price:
          type: number
          description: Цена за единицу
        discount:
          type: number
          description: Скидка %
        markup:
          type: number
          description: Наценка %
      required:
        - product_id
        - title
        - quantity
        - price
