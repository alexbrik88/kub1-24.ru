
    ProductGroup:
      type: object
      properties:
        title:
          type: string
        production_type:
          type: integer
      required:
        - title
        - production_type
