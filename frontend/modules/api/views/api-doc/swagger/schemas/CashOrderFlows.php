
    CashOrderFlows:
      type: object
      properties:
        cashbox_id:
          type: integer
          description: Касса
        contractor_id:
          type: integer
          description: Поставщик/Покупатель
        other_rs_id:
          type: integer
          description: ID расчетного счета, если контрагент банк
        expenditure_item_id:
          type: integer
          description: Статья расхода
        income_item_id:
          type: integer
          description: Статья прихода
        amount:
          type: number
          description: Сумма
        date:
          type: string
          format: date
          description: Дата оплаты
        number:
          type: number
          description: Номер
        description:
          type: string
          description: Назначение
        reason_id:
          type: integer
          description: Основание
        application:
          type: string
          description: Приложение
        is_accounting:
          type: integer
          enum: [0, 1]
          description: Учитывать в бухгалтерии
        recognition_date:
          type: string
          format: date
          description: Дата признания дохода
        is_prepaid_expense:
          type: integer
          enum: [0, 1]
          description: Авансовый платеж
        invoices_list:
          type: array
          description: Оплаченные счета
          items:
            type: integer
            description: ID счета
      required:
        - cashbox_id
        - contractor_id
        - other_rs_id
        - income_item_id
        - expenditure_item_id
        - amount
        - date
        - number
        - description
