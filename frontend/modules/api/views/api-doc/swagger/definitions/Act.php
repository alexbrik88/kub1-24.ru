
  Act:
    type: object
    properties:
      id:
        type: integer
      type:
        type: integer
      invoice_id:
        type: integer
      document_author_id:
        type: integer
      status_out_id:
        type: integer
      status_out_author_id:
        type: integer
      status_out_updated_at:
        type: integer
      document_date:
        type: date
      document_number:
        type: string
      document_additional_number:
        type: string
      ordinal_document_number:
        type: string
      order_sum:
        type: integer
      order_sum_without_nds:
        type: integer
      order_nds:
        type: integer
      basis_document_name:
        type: string
      basis_document_number:
        type: string
      basis_document_date:
        type: date
      basis_document_type_id:
        type: integer
      signed_by_employee_id:
        type: integer
      signed_by_name:
        type: string
      sign_document_type_id:
        type: integer
      sign_document_number:
        type: string
      sign_document_date:
        type: date
      signature_id:
        type: integer
      uid:
        type: string
      object_guid:
        type: string
      created_at:
        type: integer
      is_original:
        type: boolean
      is_original_updated_at:
        type: integer
      has_file:
        type: boolean
      totalAmountWithNds:
        type: integer
