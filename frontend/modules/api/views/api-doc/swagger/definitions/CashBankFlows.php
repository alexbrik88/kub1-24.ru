
  CashBankFlows:
    type: object
    properties:
      id:
        type: integer
      company_id:
        type: integer
      date:
        type: string
      recognition_date:
        type: string
      flow_type:
        type: integer
      has_invoice:
        type: integer
      amount:
        type: integer
      description:
        type: string
      expenditure_item_id:
        type: integer
      contractor_id:
        type: integer
      created_at:
        type: integer
      is_funding_flow:
        type: integer
      cash_funding_type_id:
        type: integer
      cash_id:
        type: integer
      rs:
        type: string
      bank_name:
        type: string
      payment_order_number:
        type: string
      income_item_id:
        type: integer
      is_taxable:
        type: integer
      taxpayers_status_id:
        type: integer
      kbk:
        type: string
      oktmo_code:
        type: string
      payment_details_id:
        type: integer
      tax_period_code:
        type: string
      document_number_budget_payment:
        type: string
      document_date_budget_payment:
        type: string
      payment_type_id:
        type: integer
      uin_code:
        type: string
      is_prepaid_expense:
        type: string
