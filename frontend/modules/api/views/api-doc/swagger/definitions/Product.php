
  Product:
    type: object
    properties:
      id:
        type: integer
      creator_id:
        type: integer
      company_id:
        type: integer
      production_type:
        type: integer
      status:
        type: integer
      created_at:
        type: integer
      title:
        type: string
      code:
        type: string
      article:
        type: string
      barcode:
        type: string
      item_type_code:
        type: string
      product_unit_id:
        type: integer
      box_type:
        type: string
      count_in_place:
        type: string
      place_count:
        type: string
      mass_gross:
        type: string
      has_excise:
        type: boolean
      excise:
        type: string
      price_for_buy_nds_id:
        type: integer
      price_for_buy_with_nds:
        type: integer
      price_for_sell_nds_id:
        type: integer
      price_for_sell_with_nds:
        type: integer
      country_origin_id:
        type: integer
      customs_declaration_number:
        type: string
      is_deleted:
        type: boolean
      object_guid:
        type: string
      group_id:
        type: integer
      not_for_sale:
        type: boolean
      provider_id:
        type: integer
      comment:
        type: string
      weight:
        type: string
      volume:
        type: string
      comment_photo:
        type: string
      price_for_buy_no_nds:
        type: integer
      price_for_sell_no_nds:
        type: integer
