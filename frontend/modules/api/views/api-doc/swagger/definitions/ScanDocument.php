
  ScanDocument:
    type: object
    properties:
      id:
        type: integer
      company_id:
        type: integer
      employee_id:
        type: integer
      owner_model:
        type: string
      owner_id:
        type: integer
      contractor_id:
        type: integer
      document_type_id:
        type: integer
      created_at:
        type: integer
      name:
        type: string
      description:
        type: string
      file:
        type: object
        properties:
          filename:
            type: string
          ext:
            type: string
          filename_full:
            type: string
