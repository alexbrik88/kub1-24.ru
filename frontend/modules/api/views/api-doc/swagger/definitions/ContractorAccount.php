
  ContractorAccount:
    type: object
    properties:
      id:
        type: integer
        description: ID
      contractor_id:
        type: integer
        description: Contractor ID
      rs:
        type: string
        description: Номер расчетного счета
      bank_name:
        type: string
        description: Наименование банка
      bank_city:
        type: string
        description: Город банка
      bik:
        type: string
        description: БИК банка
      ks:
        type: string
        description: К/с банка
      is_main:
        type: integer
        description: Показатель основного р/с 1|0
      created_at:
        type: integer
        description: UNIX timestamp
