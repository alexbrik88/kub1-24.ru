
  CompanyTaxationType:
    type: object
    properties:
      id:
        type: integer
      company_id:
        type: integer
      osno:
        type: boolean
      usn:
        type: boolean
      envd:
        type: boolean
      psn:
        type: boolean
      usn_type:
        type: integer
      usn_percent:
        type: integer
