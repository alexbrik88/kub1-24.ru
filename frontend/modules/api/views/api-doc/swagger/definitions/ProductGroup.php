
  ProductGroup:
    type: object
    properties:
      id:
        type: integer
      title:
        type: string
      company_id:
        type: integer
      production_type:
        type: integer
