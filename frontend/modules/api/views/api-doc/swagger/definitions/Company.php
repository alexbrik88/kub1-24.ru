
  Company:
    type: object
    properties:
      id:
        type: integer
      self_employed:
        type: boolean
      strict_mode:
        type: boolean
      name_full:
        type: string
      name_short:
        type: string
      address_legal:
        type: string
      address_actual:
        type: string
      address_legal_is_actual:
        type: boolean
      phone:
        type: string
      email:
        type: string
      chief_post_name:
        type: string
      chief_lastname:
        type: string
      chief_firstname:
        type: string
      chief_firstname_initials:
        type: string
      chief_patronymic:
        type: string
      chief_patronymic_initials:
        type: string
      chief_is_chief_accountant:
        type: boolean
      chief_accountant_lastname:
        type: string
      chief_accountant_firstname:
        type: string
      chief_accountant_firstname_initials:
        type: string
      chief_accountant_patronymic:
        type: string
      chief_accountant_patronymic_initials:
        type: string
      ogrn:
        type: string
      egrip:
        type: string
      inn:
        type: string
      kpp:
        type: string
      ip_lastname:
        type: string
      ip_firstname:
        type: string
      ip_firstname_initials:
        type: string
      ip_patronymic:
        type: string
      ip_patronymic_initials:
        type: string
      ip_certificate_number:
        type: string
      ip_certificate_date:
        type: string
      okpo:
        type: string
      okogu:
        type: string
      okato:
        type: string
      okved:
        type: string
      okfs:
        type: string
      okopf:
        type: string
      oktmo:
        type: string
      ifns_ga:
        type: string
      tax_authority_registration_date:
        type: string
      logo_link:
        type: string
      print_link:
        type: string
      chief_signature_link:
        type: string
      chief_accountant_signature_link:
        type: string
      created_at:
        type: integer
      created_by:
        type: string
      updated_at:
        type: integer
      blocked:
        type: boolean
      company_type_id:
        type: integer
      last_visit_at:
        type: integer
      active_subscribe_id:
        type: integer
      experience_id:
        type: integer
      time_zone_id:
        type: integer
      nds:
        type: string
      nds_view_type_id:
        type: integer
      documents_type:
        type: string
      object_guid:
        type: string
      main_id:
        type: integer
      has_chief_patronymic:
        type: boolean
      has_chief_accountant_patronymic:
        type: boolean
      comment:
        type: string
      free_tariff_start_at:
        type: integer
      is_free_tariff_notified:
        type: boolean
      expireSubscribeDate:
        type: string
