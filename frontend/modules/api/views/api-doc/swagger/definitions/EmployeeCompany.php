
  EmployeeCompany:
    type: object
    properties:
      employee_id:
        type: integer
      company_id:
        type: integer
      lastname:
        type: string
      firstname:
        type: string
      patronymic:
        type: string
      firstname_initial:
        type: string
      patronymic_initial:
        type: string
      sex:
        type: integer
      birthday:
        type: date
      is_working:
        type: boolean
      date_hiring:
        type: date
      date_dismissal:
        type: date
      employee_role_id:
        type: integer
      is_product_admin:
        type: integer
      phone:
        type: string
      time_zone_id:
        type: integer
      can_sign:
        type: integer
      can_invoice_add_flow:
        type: integer
      sign_document_type_id:
        type: integer
      sign_document_number:
        type: integer
      sign_document_date:
        type: integer
      signature_id:
        type: integer
      created_at:
        type: integer
      updated_at:
        type: integer
      can_view_price_for_buy:
        type: integer
