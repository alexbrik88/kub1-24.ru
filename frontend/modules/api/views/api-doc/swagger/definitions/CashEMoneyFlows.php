
  CashEMoneyFlows:
    type: object
    properties:
      id:
        type: integer
      company_id:
        type: integer
      emoney_id:
        type: integer
      date:
        type: string
      flow_type:
        type: integer
      has_invoice:
        type: integer
      amount:
        type: integer
      description:
        type: string
      expenditure_item_id:
        type: integer
      contractor_id:
        type: integer
      created_at:
        type: integer
      is_funding_flow:
        type: integer
      cash_funding_type_id:
        type: integer
      cash_id:
        type: integer
      income_item_id:
        type: integer
