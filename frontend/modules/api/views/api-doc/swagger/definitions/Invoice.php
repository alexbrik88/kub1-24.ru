
  Invoice:
    type: object
    properties:
      id:
        type: integer
      uid:
        type: string
      type:
        type: integer
      invoice_status_id:
        type: integer
      created_at:
        type: integer
      invoice_status_updated_at:
        type: integer
      invoice_status_author_id:
        type: integer
      document_author_id:
        type: integer
      document_date:
        type: date
      document_number:
        type: string
      document_additional_number:
        type: string
      payment_limit_date:
        type: date
      production_type:
        type: string
      invoice_expenditure_item_id:
        type: integer
      company_id:
        type: integer
      company_name_full:
        type: string
      company_name_short:
        type: string
      company_bank_name:
        type: string
      company_bank_city:
        type: string
      company_inn:
        type: string
      company_kpp:
        type: string
      company_egrip:
        type: string
      company_okpo:
        type: string
      company_bik:
        type: string
      company_ks:
        type: string
      company_rs:
        type: string
      company_phone:
        type: string
      company_address_legal_full:
        type: string
      company_chief_post_name:
        type: string
      company_chief_lastname:
        type: string
      company_chief_firstname_initials:
        type: string
      company_chief_patronymic_initials:
        type: string
      company_print_filename:
        type: string
      company_chief_signature_filename:
        type: string
      company_chief_accountant_lastname:
        type: string
      company_chief_accountant_firstname_initials:
        type: string
      company_chief_accountant_patronymic_initials:
        type: string
      contractor_id:
        type: integer
      contractor_name_full:
        type: string
      contractor_name_short:
        type: string
      contractor_director_name:
        type: string
      contractor_director_post_name:
        type: string
      contractor_bank_name:
        type: string
      contractor_bank_city:
        type: string
      contractor_address_legal_full:
        type: string
      contractor_inn:
        type: string
      contractor_kpp:
        type: string
      contractor_bik:
        type: string
      contractor_ks:
        type: string
      contractor_rs:
        type: string
      remind_contractor:
        type: boolean
      total_place_count:
        type: string
      total_mass_gross:
        type: string
      total_amount_no_nds:
        type: string
      total_amount_with_nds:
        type: string
      total_amount_has_nds:
        type: boolean
      total_amount_nds:
        type: string
      total_order_count:
        type: string
      view_total_base:
        type: integer
      view_total_discount:
        type: integer
      view_total_amount:
        type: integer
      view_total_no_nds:
        type: integer
      view_total_nds:
        type: integer
      view_total_with_nds:
        type: integer
      has_discount:
        type: boolean
      has_markup:
        type: boolean
      payment_form_id:
        type: integer
      payment_partial_amount:
        type: integer
      is_deleted:
        type: boolean
      is_subscribe_invoice:
        type: boolean
      subscribe_id:
        type: integer
      service_payment_id:
        type: integer
      email_messages:
        type: string
      object_guid:
        type: string
      company_checking_accountant_id:
        type: integer
      nds_view_type_id:
        type: integer
      signed_by_employee_id:
        type: integer
      signed_by_name:
        type: string
      sign_document_type_id:
        type: integer
      sign_document_number:
        type: string
      sign_document_date:
        type: date
      signature_id:
        type: integer
      autoinvoice_id:
        type: integer
      show_popup:
        type: boolean
      basis_document_name:
        type: string
      basis_document_number:
        type: string
      basis_document_date:
        type: string
      basis_document_type_id:
        type: integer
      remaining_amount:
        type: string
      comment:
        type: string
      comment_internal:
        type: string
      is_by_out_link:
        type: boolean
      by_out_link_id:
        type: integer
      is_additional_number_before:
        type: boolean
      price_precision:
        type: string
      show_article:
        type: string
      from_store_cabinet:
        type: string
      from_out_invoice:
        type: string
      currency_name:
        type: string
      currency_amount:
        type: string
      currency_rate:
        type: string
      currency_rate_type:
        type: string
      currency_rate_date:
        type: string
      currency_rate_amount:
        type: string
      currency_rate_value:
        type: string
      show_paylimit_info:
        type: boolean
      out_invoice_id:
        type: integer
      is_invoice_contract:
        type: boolean
      contract_essence:
        type: string
      has_services:
        type: boolean
      has_goods:
        type: boolean
      has_act:
        type: boolean
      has_packing_list:
        type: boolean
      has_invoice_facture:
        type: boolean
      has_upd:
        type: boolean
      has_file:
        type: boolean
      can_add_act:
        type: boolean
      can_add_packing_list:
        type: boolean
      can_add_invoice_facture:
        type: boolean
      can_add_upd:
        type: boolean
      agreement:
        type: string
      basisDocString:
        type: string
      viewLink:
        type: string
