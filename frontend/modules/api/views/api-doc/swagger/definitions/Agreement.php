
  Agreement:
    type: object
    properties:
      id:
        type: integer
      uid:
        type: string
      type:
        type: integer
      company_id:
        type: integer
      contractor_id:
        type: integer
      status_id:
        type: integer
      status_author_id:
        type: integer
      status_updated_at:
        type: integer
      document_date:
        type: date
      document_number:
        type: string
      document_additional_number:
        type: string
      document_name:
        type: string
      document_type_id:
        type: integer
      comment_internal:
        type: string
      is_created:
        type: boolean
      is_completed:
        type: boolean
      created_at:
        type: integer
      created_by:
        type: integer
      document_author_id:
        type: integer
      agreement_template_id:
        type: integer
      payment_delay:
        type: integer
      payment_limit_date:
        type: date
      company_rs:
        type: string
      has_file:
        type: boolean
