
  Contractor:
    type: object
    properties:
      id:
        type: integer
      company_id:
        type: integer
      type:
        type: integer
      status:
        type: integer
      company_type_id:
        type: integer
      name:
        type: string
      director_name:
        type: string
      director_post_name:
        type: string
      director_email:
        type: string
      director_phone:
        type: string
      chief_accountant_is_director:
        type: string
      chief_accountant_email:
        type: string
      chief_accountant_phone:
        type: string
      legal_address:
        type: string
      actual_address:
        type: string
      postal_address:
        type: string
      BIN:
        type: string
      ITN:
        type: string
      PPC:
        type: string
      current_account:
        type: string
      bank_name:
        type: string
      bank_city:
        type: string
      corresp_account:
        type: string
      BIC:
        type: string
      contact_is_director:
        type: string
      contact_name:
        type: string
      contact_phone:
        type: string
      contact_email:
        type: string
      created_at:
        type: integer
      employee_id:
        type: integer
      taxation_system:
        type: string
      chief_accountant_name:
        type: string
      is_deleted:
        type: boolean
      object_guid:
        type: string
      current_account_guid:
        type: string
      face_type:
        type: integer
      physical_firstname:
        type: string
      physical_lastname:
        type: string
      physical_patronymic:
        type: string
      physical_no_patronymic:
        type: string
      physical_address:
        type: string
      physical_passport_number:
        type: string
      physical_passport_series:
        type: string
      physical_passport_date_output:
        type: string
      physical_passport_issued_by:
        type: string
      physical_passport_department:
        type: string
      invoice_expenditure_item_id:
        type: integer
      nds_view_type_id:
        type: integer
      created_from_company_id:
        type: integer
      responsible_employee_id:
        type: integer
      source:
        type: string
      comment:
        type: string
      payment_delay:
        type: string
      order_currency:
        type: string
      last_basis_document:
        type: string
      physical_passport_isRf:
        type: string
      physical_passport_country:
        type: string
      okpo:
        type: string
      not_accounting:
        type: string
      discount:
        type: number
