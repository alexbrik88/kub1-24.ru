
  Log:
    type: object
    properties:
      id:
        type: integer
      created_at:
        type: integer
      author_id:
        type: integer
      company_id:
        type: integer
      log_entity_type_id:
        type: integer
      log_event_id:
        type: integer
      model_name:
        type: string
      model_id:
        type: integer
      attributesNewArray:
        type: object
        properties:
          attribute_name:
            type: string
      attributesOldArray:
        type: object
        properties:
          attribute_name:
            type: string
