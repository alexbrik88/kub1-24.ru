
  CheckingAccountant:
    type: object
    properties:
      id:
        type: integer
      created_at:
        type: integer
      updated_at:
        type: integer
      company_id:
        type: integer
      bank_name:
        type: string
      bank_city:
        type: string
      bik:
        type: string
      ks:
        type: string
      rs:
        type: string
      type:
        type: integer
      autoload_mode_id:
        type: integer
