
  /api/agreement:
    get:
      tags:
        - agreement
      summary: Список
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/documentType'
        - $ref: '#/components/parameters/sort'
        - name: periodFrom
          in: query
          type: string
          format: date
          required: true
          description: Дата начала периода
        - name: periodTo
          in: query
          type: string
          format: date
          required: true
          description: Дата конца периода
      produces:
        - application/json
        - application/xml
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              $ref: '#/definitions/Agreement'
        401:
          description: Unauthorized
    post:
      tags:
        - agreement
      summary: Создать
      consumes:
        - application/x-www-form-urlencoded
        - multipart/form-data
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: Agreement
          in: body
          description: Данные
          required: true
          schema:
            $ref: '#/components/schemas/Agreement'
      produces:
        - application/json
        - application/xml
      responses:
        201:
          description: Ok
          schema:
            $ref: '#/definitions/Agreement'
        401:
          description: Unauthorized
  /api/agreement/{id}:
    get:
      tags:
        - agreement
      summary: Данные
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/Agreement'
        401:
          description: Unauthorized
    put:
      tags:
        - agreement
      summary: Изменить
      consumes:
        - application/x-www-form-urlencoded
        - multipart/form-data
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - name: Agreement
          in: body
          description: Данные
          required: true
          schema:
            $ref: '#/components/schemas/Agreement'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/Agreement'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    patch:
      tags:
        - agreement
      summary: Изменить
      consumes:
        - application/x-www-form-urlencoded
        - multipart/form-data
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - name: Agreement
          in: body
          description: Данные
          required: true
          schema:
            $ref: '#/components/schemas/Agreement'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/Agreement'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    delete:
      tags:
        - agreement
      summary: Удалить
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      responses:
        204:
          description: Ok
        401:
          description: Unauthorized
