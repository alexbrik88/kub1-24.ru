
  /api/contractor:
    get:
      tags:
        - contractor
      summary: Список контрагентов
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: type
          in: query
          type: integer
          description: |
            Тип контрагента
            "1" Поставщик
            "2" Покупатель
          required: true
        - name: name
          in: query
          type: string
          description: Фильтр по названию
          required: false
        - $ref: '#/components/parameters/sort'
        - name: expand
          in: query
          type: string
          description: Связанные данные
          enum: [companyType]
      produces:
        - application/json
        - application/xml
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              $ref: '#/definitions/Contractor'
        401:
          description: Unauthorized
    post:
      tags:
        - contractor
      summary: Добавить контрагента
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - in: body
          description: информация о контрагенте
          required: true
          schema:
            $ref: '#/components/schemas/Contractor'
      produces:
        - application/json
        - application/xml
      responses:
        201:
          description: Ok
          schema:
            $ref: '#/definitions/Contractor'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
  /api/contractor/{id}:
    get:
      tags:
        - contractor
      summary: информация о контрагенте
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - name: expand
          in: query
          type: string
          description: Связанные данные
          enum: [companyType]
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/Contractor'
        401:
          description: Unauthorized
    put:
      tags:
        - contractor
      summary: Изменить контрагента
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - in: body
          description: информация о контрагенте
          required: true
          schema:
            $ref: '#/components/schemas/Contractor'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/Contractor'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    patch:
      tags:
        - contractor
      summary: Изменить контрагента
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - in: body
          description: информация о контрагенте
          required: true
          schema:
            $ref: '#/components/schemas/Contractor'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/Contractor'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    delete:
      tags:
        - contractor
      summary: Удалить контрагента
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      responses:
        204:
          description: Ok
        401:
          description: Unauthorized
