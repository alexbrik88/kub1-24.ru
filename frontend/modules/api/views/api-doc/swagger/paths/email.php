
  /api/email/signature:
    get:
      tags:
        - email
      summary: Подпись пользователя в письме
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBasic'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: object
            properties:
              company_id:
                type: integer
              employee_id:
                type: integer
              text:
                type: string
              id:
                type: integer
        401:
          description: Unauthorized

    post:
      tags:
        - email
      summary: Изменить подпись пользователя в письме
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBasic'
        - name: text
          in: body
          description: Текст подписи
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: object
            properties:
              company_id:
                type: integer
              employee_id:
                type: integer
              text:
                type: string
              id:
                type: integer
        401:
          description: Unauthorized
