<?php

use frontend\models\log\Log;
use frontend\modules\api\controllers\LogController;

$log = new Log;
$fields = implode(',', array_values($log->fields()));
$expand = implode(',', array_values($log->extraFields()));
$names = implode(',', array_keys(LogController::$entityArray));
?>

  /api/log:
    get:
      tags:
        - log
      summary: Действия с документами и финансами
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBasic'
        - name: name
          in: query
          type: string
          description: Наименование объекта
          enum: [<?= $names ?>]
          required: false
        - name: id
          in: query
          type: integer
          description: ID объекта
          required: false
        - name: author_id
          in: query
          type: integer
          description: ID автора действия над объектом
          required: false
        - name: log_event_id
          in: query
          type: integer
          description: ID тип действия над объектом
          required: false
        - name: log_entity_type_id
          in: query
          type: integer
          description: ID тип объекта
          required: false
        - name: fields
          in: query
          type: string
          description: Атрибуты объекта, если не нужны все (значения через запятую или массив)
          enum: [<?= $fields ?>]
          required: false
        - name: expand
          in: query
          type: string
          description: Связанные объекты (значения через запятую или массив)
          enum: [<?= $expand ?>]
          required: false
        - $ref: '#/components/parameters/sort'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              $ref: '#/definitions/Log'
        401:
          description: Unauthorized
