
  /api/reference/agreement-type:
    get:
      tags:
        - reference
      summary: Типы договоров с контрагентом
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              type: object
              properties:
                id:
                  type: integer
                name:
                  type: string
                name_dative:
                  type: string
                  description: дательный падеж
        401:
          description: Unauthorized
  /api/reference/bik:
    get:
      tags:
        - reference
      summary: Поиск банка по БИК
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: q
          in: query
          type: string
          description: БИК банка. Мин. 1, макс 9 цифр
          required: true
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              type: object
              properties:
                bik:
                  type: string
                is_active:
                  type: boolean
                ks:
                  type: string
                name:
                  type: string
                namemini:
                  type: string
                index:
                  type: string
                city:
                  type: string
                address:
                  type: string
                phone:
                  type: string
                okato:
                  type: string
                okpo:
                  type: string
                regnum:
                  type: string
                srok:
                  type: string
                dateadd:
                  type: string
                datechange:
                  type: string
                object_guid:
                  type: string
        401:
          description: Unauthorized
  /api/reference/company-type:
    get:
      tags:
        - reference
      summary: Формы компаний
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              type: object
              properties:
                id:
                  type: integer
                name_short:
                  type: string
                name_full:
                  type: string
                in_company:
                  type: integer
                in_contractor:
                  type: integer
        401:
          description: Unauthorized
  /api/reference/document-status:
    get:
      tags:
        - reference
      summary: Статусы документа
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: type
          in: query
          type: string
          enum: [invoice, act, packing-list, invoice-facture, upd]
          description: |
            Тип документа:
             "invoice" - Счет
             "act" - Акт
             "packing-list" - ТН
             "invoice-facture" - СФ
             "upd" - УПД
          required: true
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              type: object
              properties:
                id:
                  type: integer
                name:
                  type: string
        401:
          description: Unauthorized
  /api/reference/employee-role:
    get:
      tags:
        - reference
      summary: Роли сотрудника к компании
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              type: object
              properties:
                id:
                  type: integer
                name:
                  type: string
        401:
          description: Unauthorized
  /api/reference/expenditure:
    get:
      tags:
        - reference
      summary: Статьи расходов
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              type: object
              properties:
                id:
                  type: integer
                name:
                  type: string
        401:
          description: Unauthorized
  /api/reference/income:
    get:
      tags:
        - reference
      summary: Статьи прихода
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              type: object
              properties:
                id:
                  type: integer
                name:
                  type: string
        401:
          description: Unauthorized
  /api/reference/nds-type:
    get:
      tags:
        - reference
      summary: Возможные отображения НДС
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              type: object
              properties:
                id:
                  type: integer
                name:
                  type: string
                title:
                  type: string
        401:
          description: Unauthorized
  /api/reference/nds-value:
    get:
      tags:
        - reference
      summary: Возможные значения НДС
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              type: object
              properties:
                id:
                  type: integer
                name:
                  type: string
                rate:
                  type: string
        401:
          description: Unauthorized
  /api/reference/time-zone:
    get:
      tags:
        - reference
      summary: Таймзоны
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              type: object
              properties:
                id:
                  type: integer
                out_time_zone:
                  type: string
                time_zone:
                  type: string
        401:
          description: Unauthorized
  /api/reference/units:
    get:
      tags:
        - reference
      summary: Единицы измерения
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: goods
          in: query
          type: integer
          example: 1
          enum: [1,0]
          description: Фильтр, относится или нет к товарам
        - name: services
          in: query
          type: integer
          example: 1
          enum: [1,0]
          description: Фильтр, относится или нет к услугам
          required: false
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              type: object
              properties:
                id:
                  type: integer
                name:
                  type: string
                title:
                  type: string
                code_okei:
                  type: integer
                object_guid:
                  type: integer
                goods:
                  type: boolean
                services:
                  type: boolean
        401:
          description: Unauthorized
  /api/reference/bank-logo:
    get:
      tags:
        - reference
      summary: Логотип банка
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: bik
          in: query
          type: string
          description: Бик банка
          required: true
        - name: small
          in: query
          type: integer
          example: 1
          enum: [1,0]
          description: Флаг мини-логотип
          required: false
      produces:
        - application/json
      responses:
        200:
          description: Logo image
          content:
            image/png:
              schema:
                type: string
                format: binary
        401:
          description: Unauthorized
        404:
          description: Unauthorized
