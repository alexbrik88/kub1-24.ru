
  /api/invoice:
    get:
      tags:
        - invoice
      summary: Список счетов
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/documentType'
        - name: periodFrom
          in: query
          type: string
          format: date
          required: true
          description: Дата начала периода
        - name: periodTo
          in: query
          type: string
          format: date
          required: true
          description: Дата конца периода
        - name: sort
          in: query
          type: string
          enum: [document_date,payDate,document_number,total_amount_with_nds]
          description: Сортировка результата
        - name: InvoiceSearch[contractor_id]
          in: query
          type: integer
          description: Фильтр по ID контрагента
        - name: InvoiceSearch[invoice_status_id]
          in: query
          type: integer
          description: Фильтр по ID статуса счета
        - name: InvoiceSearch[document_author_id]
          in: query
          type: integer
          description: Фильтр по ID автора документа
        - name: InvoiceSearch[has_act]
          in: query
          type: integer
          enum: [0,1]
          description: Фильтр по наличию акта
        - name: InvoiceSearch[has_packing_list]
          in: query
          type: integer
          enum: [0,1]
          description: Фильтр но наличию ТН
        - name: InvoiceSearch[has_invoice_facture]
          in: query
          type: integer
          enum: [0,1]
          description: Фильтр по наличию СФ
        - name: InvoiceSearch[has_upd]
          in: query
          type: integer
          enum: [0,1]
          description: Фильтр по наличию УПД
        - name: InvoiceSearch[has_file]
          in: query
          type: integer
          enum: [0,1]
          description: Фильтр по наличию файла
        - name: InvoiceSearch[fileCount]
          in: query
          type: integer
          enum: [0,1]
          description: Фильтр по количеству файлов
        - name: InvoiceSearch[search]
          in: query
          type: string
          description: Фильтр по номеру счета иди названию контрагента
      produces:
        - application/json
        - application/xml
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              $ref: '#/definitions/Invoice'
        401:
          description: Unauthorized
    post:
      tags:
        - invoice
      summary: Создать счет
      description: |

      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/documentType'
        - name: Invoice
          in: body
          description: информация о счете
          required: true
          schema:
            $ref: '#/components/schemas/Invoice'
        - name: orderArray
          in: body
          description: информация о товарах/услугах
          required: true
          schema:
            type: array
            items:
              $ref: '#/components/schemas/Order'
      produces:
        - application/json
        - application/xml
      responses:
        201:
          description: Ok
          schema:
            $ref: '#/definitions/Invoice'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
  /api/invoice/{id}:
    get:
      tags:
        - invoice
      summary: Информация о счете
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - name: expand
          in: query
          type: string
          description: Связанные данные
          enum: [orders,contractor,company,acts,packingLists,invoiceFactures,upds]
          required: false
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/Invoice'
        401:
          description: Unauthorized
    put:
      tags:
        - invoice
      summary: Изменить счет
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - name: Invoice
          in: body
          description: информация о счете
          required: true
          schema:
            $ref: '#/components/schemas/Invoice'
        - name: orderArray
          in: body
          description: информация о товарах/услугах
          required: true
          schema:
            type: array
            items:
              $ref: '#/components/schemas/Order'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/Invoice'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    patch:
      tags:
        - invoice
      summary: Изменить счет
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - name: Invoice
          in: body
          description: информация о счете
          required: true
          schema:
            $ref: '#/components/schemas/Invoice'
        - name: orderArray
          in: body
          description: информация о товарах/услугах
          required: true
          schema:
            type: array
            items:
              $ref: '#/components/schemas/Order'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/Invoice'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    delete:
      tags:
        - invoice
      summary: Удалить счет
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      responses:
        204:
          description: Ok
        401:
          description: Unauthorized
  /api/invoice/{id}/paid:
    post:
      tags:
        - invoice
      summary: Счет оплачен
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - name: amount
          in: body
          type: number
          description: Сумма оплаты
          required: true
          example: "1250.50"
        - name: date_pay
          in: body
          type: string
          format: date
          description: Дата оплаты
          required: true
          example: 12.09.2018
        - name: flowType
          in: body
          type: integer
          description: |
            Тип оплаты
            "1" банк
            "2" касса
            "3" e-money
          required: true
          example: 1
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/Invoice'
        401:
          description: Unauthorized
        403:
          description: Forbidden
        422:
          description: Data validation failed
    delete:
      tags:
        - invoice
      summary: Снять оплату счета
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/Invoice'
        401:
          description: Unauthorized
        403:
          description: Forbidden
        404:
          description: Not Found
  /api/invoice/{id}/send:
    post:
      tags:
        - invoice
      summary: Отправит счет
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - name: subject
          in: body
          type: string
          description: Тема письма
          required: true
        - name: sendTo
          in: body
          type: string
          format: email
          description: Адрес(а) получателя
          required: true
        - name: emailText
          in: body
          type: string
          description: Текст письма
          required: false
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: object
            properties:
              sent:
                type: integer
                description: Количество отправленных
              total:
                type: integer
                description: Всего адресов
        401:
          description: Unauthorized
        403:
          description: Forbidden
        404:
          description: Not Found
        422:
          description: Data validation failed
  /api/invoice/{id}/pdf:
    get:
      tags:
        - invoice
      summary: Документ в формате pdf
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      produces:
        - application/pdf
      responses:
        200:
          description: Ok
          schema:
            type: file
        401:
          description: Unauthorized
        403:
          description: Forbidden
        404:
          description: Not Found
  /api/invoice/{id}/png:
    get:
      tags:
        - invoice
      summary: Документ в формате png
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - name: page
          in: query
          type: integer
          description: Номер страницы документа
      produces:
        - image/png
      responses:
        200:
          description: Ok
          schema:
            type: file
        401:
          description: Unauthorized
        403:
          description: Forbidden
        404:
          description: Not Found
  /api/invoice/next-number:
    get:
      tags:
        - invoice
      summary: Номер для нового исходящего счета
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: object
            properties:
              number:
                type: integer
                description: Номер следующего исходящего счета
        401:
          description: Unauthorized
        403:
          description: Forbidden
  /api/invoice/left-quantity:
    get:
      tags:
        - invoice
      summary: Сколько счетов еще можно создать
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: integer
            example: 5
        204:
          description: No limit.
        401:
          description: Unauthorized
