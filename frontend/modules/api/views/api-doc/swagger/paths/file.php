
  /api/{doc_type}/file-get:
    get:
      tags:
        - file
      summary: Скачать файл
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: doc_type
          in: path
          type: string
          required: true
          description: Тип документа
          enum: [act, agreement, invoice, invoice_facture, packing_list, upd]
        - name: id
          in: query
          type: integer
          required: true
          description: Идентификатор документа
        - name: fid
          in: query
          type: integer
          required: true
          description: Идентификатор файла
      produces:
        - image/png
        - image/gif
        - image/jpeg
      responses:
        200:
          description: Ok
        401:
          description: Unauthorized
  /api/{doc_type}/file-list:
    get:
      tags:
        - file
      summary: Список файлов
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: doc_type
          in: path
          type: string
          required: true
          description: Тип документа
          enum: [act, agreement, invoice, invoice_facture, packing_list, upd]
        - name: id
          in: query
          type: integer
          required: true
          description: Идентификатор документа
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              type: object
              properties:
                id:
                  type: integer
                filename:
                  type: string
                ext:
                  type: string
                filename_full:
                  type: string
                hash:
                  type: string
                created_at:
                  type: integer
                created_at_author_id:
                  type: integer
                owner_id:
                  type: integer
        401:
          description: Unauthorized
  /api/{doc_type}/file-upload:
    post:
      tags:
        - file
      summary: Загрузить файл
      consumes:
        - multipart/form-data
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: doc_type
          in: path
          type: string
          required: true
          description: Тип документа
          enum: [act, agreement, invoice, invoice_facture, packing_list, upd]
        - name: id
          in: query
          type: integer
          required: true
          description: Идентификатор документа
        - name: file
          in: body
          type: string
          format: binary
          required: true
          description: Файл
      produces:
        - application/json
      responses:
        201:
          description: Ok
          schema:
            type: object
            properties:
              id:
                type: integer
              filename:
                type: string
              ext:
                type: string
              filename_full:
                type: string
              hash:
                type: string
              created_at:
                type: integer
              created_at_author_id:
                type: integer
              owner_id:
                type: integer
        401:
          description: Unauthorized
  /api/{doc_type}/file-delete:
    delete:
      tags:
        - file
      summary: Удалить файл
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: doc_type
          in: path
          type: string
          required: true
          description: Тип документа
          enum: [act, agreement, invoice, invoice_facture, packing_list, upd]
        - name: id
          in: query
          type: integer
          required: true
          description: Идентификатор документа
        - name: fid
          in: query
          type: integer
          required: true
          description: Идентификатор файла
      responses:
        204:
          description: Ok
        401:
          description: Unauthorized

  /api/{doc_type}/scan-bind:
    post:
      tags:
        - file
      summary: Присвоить скан документу
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: doc_type
          in: path
          type: string
          required: true
          description: Тип документа
          enum: [act, agreement, invoice, invoice_facture, packing_list, upd]
        - name: id
          in: query
          type: integer
          required: true
          description: Идентификатор документа
        - name: sid
          in: query
          type: integer
          required: true
          description: Идентификатор скана
      responses:
        204:
          description: Ok
        401:
          description: Unauthorized
  /api/{doc_type}/scan-unbind:
    post:
      tags:
        - file
      summary: Отменить присвоение скана документу
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: doc_type
          in: path
          type: string
          required: true
          description: Тип документа
          enum: [act, agreement, invoice, invoice_facture, packing_list, upd]
        - name: id
          in: query
          type: integer
          required: true
          description: Идентификатор документа
        - name: sid
          in: query
          type: integer
          required: true
          description: Идентификатор скана
      responses:
        204:
          description: Ok
        401:
          description: Unauthorized
  /api/{doc_type}/scan-list:
    get:
      tags:
        - file
      summary: Список сканов, присвоенных документу
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: doc_type
          in: path
          type: string
          required: true
          description: Тип документа
          enum: [act, agreement, invoice, invoice_facture, packing_list, upd]
        - name: id
          in: query
          type: integer
          required: true
          description: Идентификатор документа
        - name: sid
          in: query
          type: integer
          required: true
          description: Идентификатор скана
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              type: object
              properties:
                id:
                  type: integer
                company_id:
                  type: integer
                employee_id:
                  type: integer
                owner_id:
                  type: integer
                created_at:
                  type: integer
                name:
                  type: string
                description:
                  type: string
        401:
          description: Unauthorized
