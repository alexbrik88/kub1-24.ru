
  /api/scan-document:
    get:
      tags:
        - scan-document
      summary: Список сканов документов
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: free
          in: query
          type: boolean
          description: '"1" Выбрать не прикрепленные'
        - name: contractor_id
          in: query
          type: integer
          description: Фильтр по контрагенту
        - name: document_type_id
          in: query
          type: integer
          description: Фильтр по типу документа
        - name: expand
          in: query
          type: string
          description: '"file" информация о файле'
        - $ref: '#/components/parameters/sort'
      produces:
        - application/json
        - application/xml
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              $ref: '#/definitions/ScanDocument'
        401:
          description: Unauthorized
    post:
      tags:
        - scan-document
      summary: Добавить скан документа
      consumes:
        - multipart/form-data
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - in: body
          description: информация о файле
          required: true
          schema:
            $ref: '#/components/schemas/ScanDocument'
      produces:
        - application/json
        - application/xml
      responses:
        201:
          description: Ok
          schema:
            $ref: '#/definitions/ScanDocument'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
  /api/scan-document/{id}:
    get:
      tags:
        - scan-document
      summary: Информация о скане документа
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - name: expand
          in: query
          type: string
          description: '"file" информация о файле'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/ScanDocument'
        401:
          description: Unauthorized
    put:
      tags:
        - scan-document
      summary: Изменить скан документа
      consumes:
        - multipart/form-data
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - in: body
          description: информация о файле
          required: true
          schema:
            $ref: '#/components/schemas/ScanDocument'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/ScanDocument'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    patch:
      tags:
        - scan-document
      summary: Изменить скан документа
      consumes:
        - multipart/form-data
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - in: body
          description: информация о файле
          required: true
          schema:
            $ref: '#/components/schemas/ScanDocument'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/ScanDocument'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    delete:
      tags:
        - scan-document
      summary: Удалить скан документа
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      responses:
        204:
          description: Ok
        401:
          description: Unauthorized
  /api/scan-document/{id}/file:
    get:
      tags:
        - scan-document
      summary: Файл скана документа
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      responses:
        200:
          description: Ok
          content:
            application/octet-stream:
              schema:
                type: string
        401:
          description: Unauthorized
  /api/scan-document/type-list:
    get:
      tags:
        - scan-document
      summary: Список типов документов
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
      produces:
        - application/json
        - application/xml
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              type: object
              properties:
                id:
                  type: integer
                name:
                  type: string

        401:
          description: Unauthorized
