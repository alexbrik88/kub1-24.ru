
  /api/company:
    get:
      tags:
        - company
      summary: Список компаний
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/sort'
        - name: expand
          in: query
          type: string
          description: Связанные данные
          enum: [mainCheckingAccountant,companyTaxationType,companyType,timeZone]
          required: false
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              $ref: '#/definitions/Company'
        401:
          description: Unauthorized
    post:
      tags:
        - company
      summary: Добавить компанию
      description: |
        поля для ИП, которые отличны от полей для ООО:
        {
          ...
          // Вместо данных руководителя 'chief_*' и бухгалтера 'chief_accountant_*'
          // ФИО
          'ip_lastname': 'Иванов',
          'ip_firstname': 'Иван',
          'ip_patronymic': 'Иванович',
          ...
          // Вместо ОГРН ('ogrn')
          // ОГРНИП
          'egrip': '123456789012345',
          ...
          // Патент. Где выдан, дата выдачи и дата окончания действия
          'ip_patent_city': 'Город',
          'patentDate': 'дд.мм.гггг',
          'patentDateEnd': 'дд.мм.гггг',
          ...
          // Вместо ПФР ('pfr')
          // ПФР (как ИП)
          'pfr_ip': '12345678901234',
          // ПФР (как работодателя)
          'pfr_employer': '12345678901234',
          ...
          // Свидетельство. Номер, дата выдачи, кем выдано
          'ip_certificate_number': '...',
          'certificateDate': 'дд.мм.гггг',
          'ip_certificate_issued_by': '...',
          ...
        }
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - in: body
          name: Company
          required: true
          schema:
            $ref: '#/components/schemas/Company'
        - in: body
          name: CompanyTaxationType
          required: true
          schema:
            $ref: '#/components/schemas/CompanyTaxationType'
        - in: body
          name: CheckingAccountant
          required: true
          schema:
            $ref: '#/components/schemas/CheckingAccountant'
      produces:
        - application/json
      responses:
        201:
          description: Ok
          schema:
            type: object
            properties:
              Company:
                $ref: '#/definitions/Company'
              CompanyTaxationType:
                $ref: '#/definitions/CompanyTaxationType'
              CheckingAccountant:
                $ref: '#/definitions/CheckingAccountant'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
  /api/company/{id}:
    get:
      tags:
        - company
      summary: Компания пользователя
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - name: expand
          in: query
          type: string
          description: Связанные данные
          enum: [mainCheckingAccountant,checkingAccountants,companyTaxationType,companyType,timeZone]
          required: false
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/Company'
        401:
          description: Unauthorized
    put:
      tags:
        - company
      summary: Изменить компанию
      description: |
        поля для ИП, которые отличны от полей для ООО:
        {
          ...
          // Вместо данных руководителя 'chief_*' и бухгалтера 'chief_accountant_*'
          // ФИО
          'ip_lastname': 'Иванов',
          'ip_firstname': 'Иван',
          'ip_patronymic': 'Иванович',
          ...
          // Вместо ОГРН ('ogrn')
          // ОГРНИП
          'egrip': '123456789012345',
          ...
          // Патент. Где выдан, дата выдачи и дата окончания действия
          'ip_patent_city': 'Город',
          'patentDate': 'дд.мм.гггг',
          'patentDateEnd': 'дд.мм.гггг',
          ...
          // Вместо ПФР ('pfr')
          // ПФР (как ИП)
          'pfr_ip': '12345678901234',
          // ПФР (как работодателя)
          'pfr_employer': '12345678901234',
          ...
          // Свидетельство. Номер, дата выдачи, кем выдано
          'ip_certificate_number': '...',
          'certificateDate': 'дд.мм.гггг',
          'ip_certificate_issued_by': '...',
          ...
        }
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - in: body
          name: Company
          required: true
          schema:
            $ref: '#/components/schemas/Company'
        - in: body
          name: CompanyTaxationType
          required: true
          schema:
            $ref: '#/components/schemas/CompanyTaxationType'
        - in: body
          name: CheckingAccountant
          required: true
          schema:
            $ref: '#/components/schemas/CheckingAccountant'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: object
            properties:
              Company:
                $ref: '#/definitions/Company'
              CompanyTaxationType:
                $ref: '#/definitions/CompanyTaxationType'
              CheckingAccountant:
                $ref: '#/definitions/CheckingAccountant'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    patch:
      tags:
        - company
      summary: Изменить компанию
      description: |
        поля для ИП, которые отличны от полей для ООО:
        {
          ...
          // Вместо данных руководителя 'chief_*' и бухгалтера 'chief_accountant_*'
          // ФИО
          'ip_lastname': 'Иванов',
          'ip_firstname': 'Иван',
          'ip_patronymic': 'Иванович',
          ...
          // Вместо ОГРН ('ogrn')
          // ОГРНИП
          'egrip': '123456789012345',
          ...
          // Патент. Где выдан, дата выдачи и дата окончания действия
          'ip_patent_city': 'Город',
          'patentDate': 'дд.мм.гггг',
          'patentDateEnd': 'дд.мм.гггг',
          ...
          // Вместо ПФР ('pfr')
          // ПФР (как ИП)
          'pfr_ip': '12345678901234',
          // ПФР (как работодателя)
          'pfr_employer': '12345678901234',
          ...
          // Свидетельство. Номер, дата выдачи, кем выдано
          'ip_certificate_number': '...',
          'certificateDate': 'дд.мм.гггг',
          'ip_certificate_issued_by': '...',
          ...
        }
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - in: body
          name: Company
          required: true
          schema:
            $ref: '#/components/schemas/Company'
        - in: body
          name: CompanyTaxationType
          required: true
          schema:
            $ref: '#/components/schemas/CompanyTaxationType'
        - in: body
          name: CheckingAccountant
          required: true
          schema:
            $ref: '#/components/schemas/CheckingAccountant'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: object
            properties:
              Company:
                $ref: '#/definitions/Company'
              CompanyTaxationType:
                $ref: '#/definitions/CompanyTaxationType'
              CheckingAccountant:
                $ref: '#/definitions/CheckingAccountant'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
