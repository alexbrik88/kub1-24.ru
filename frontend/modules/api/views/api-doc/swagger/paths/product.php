
  /api/product:
    get:
      tags:
        - product
      summary: Список товаров/услуг
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: type
          in: query
          description: "Тип: 0 услуга, 1 товар"
          enum: [0,1]
          required: true
        - name: title
          in: query
          type: string
          description: Фильтр по названию
          required: false
        - $ref: '#/components/parameters/sort'
      produces:
        - application/json
        - application/xml
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              $ref: '#/definitions/Product'
        401:
          description: Unauthorized
    post:
      tags:
        - product
      summary: Добавить товар/услугу
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - in: body
          description: информация о тваре/услуге
          required: true
          schema:
            $ref: '#/components/schemas/Product'
      produces:
        - application/json
        - application/xml
      responses:
        201:
          description: Ok
          schema:
            $ref: '#/definitions/Product'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
  /api/product/{id}:
    get:
      tags:
        - product
      summary: информация о тваре/услуге
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/Product'
        401:
          description: Unauthorized
    put:
      tags:
        - product
      summary: Изменить товар/услугу
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - in: body
          description: информация о тваре/услуге
          required: true
          schema:
            $ref: '#/components/schemas/Product'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/Product'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    patch:
      tags:
        - product
      summary: Изменить товар/услугу
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - in: body
          description: информация о тваре/услуге
          required: true
          schema:
            $ref: '#/components/schemas/Product'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/Product'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    delete:
      tags:
        - product
      summary: Удалить товар/услугу
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      responses:
        204:
          description: Ok
        401:
          description: Unauthorized
