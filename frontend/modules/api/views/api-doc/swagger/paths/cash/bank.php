
  /api/cash/bank:
    get:
      tags:
        - cash
      summary: Список движений по банку
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/sort'
        - name: periodFrom
          in: query
          type: string
          format: date
          required: true
          description: Дата начала периода
        - name: periodTo
          in: query
          type: string
          format: date
          required: true
          description: Дата конца периода
        - name: bik
          in: query
          type: string
          required: false
          description: БИК банка
        - name: flow_type
          in: query
          type: integer
          required: false
          enum: [0,1]
          description: |
            Тип операции:
            0 - расход
            1 - приход
        - name: contractor_id
          in: query
          type: integer
          required: false
          description: Фильтр по ID контрагента
        - name: expand
          in: query
          type: string
          description: Связанные данные
          enum: [contractor,invoices]
      produces:
        - application/json
        - application/xml
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              $ref: '#/definitions/CashBankFlows'
        401:
          description: Unauthorized
    post:
      tags:
        - cash
      summary: Создать движение по банку
      consumes:
        - application/x-www-form-urlencoded
        - application/json
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: flow_type
          in: query
          description: |
            Тип:
            1 - приход
            0 - расход
          type: integer
          required: true
          enum: [0,1]
        - in: body
          name: CashBankFlows
          required: true
          description: The CashBankFlows to create.
          schema:
            $ref: '#/components/schemas/CashBankFlows'
      produces:
        - application/json
        - application/xml
      responses:
        201:
          description: Ok
          schema:
            $ref: '#/definitions/CashBankFlows'
        401:
          description: Unauthorized
  /api/cash/bank/statistic:
    get:
      tags:
        - cash
      summary: Суммирующая статистика движеий по банку
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: periodFrom
          in: query
          type: string
          format: date
          required: true
          description: Дата начала периода
        - name: periodTo
          in: query
          type: string
          format: date
          required: true
          description: Дата конца периода
        - name: bik
          in: query
          type: string
          description: Бик банка, если не указан, поиск по всем банкам
          required: false
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: object
            properties:
              dateStart:
                type: string
              dateEnd:
                type: string
              balanceStart:
                type: integer
              balanceEnd:
                type: integer
              totalIncome:
                type: integer
              totalExpense:
                type: integer
              countIncome:
                type: integer
              countExpense:
                type: integer
        401:
          description: Unauthorized
  /api/cash/bank/{id}:
    get:
      tags:
        - cash
      summary: Данные движеия по банку
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/CashBankFlows'
        401:
          description: Unauthorized
    put:
      tags:
        - cash
      summary: Изменить движеие по банку
      consumes:
        - application/x-www-form-urlencoded
        - application/json
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - in: body
          name: CashBankFlows
          required: true
          description: The CashBankFlows to create.
          schema:
            $ref: '#/components/schemas/CashBankFlows'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/CashBankFlows'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    patch:
      tags:
        - cash
      summary: Изменить движеие по банку
      consumes:
        - application/x-www-form-urlencoded
        - application/json
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - in: body
          name: CashBankFlows
          required: true
          description: The CashBankFlows to create.
          schema:
            $ref: '#/components/schemas/CashBankFlows'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/CashBankFlows'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    delete:
      tags:
        - cash
      summary: Удалить движеие по банку
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      responses:
        204:
          description: Ok
        401:
          description: Unauthorized
