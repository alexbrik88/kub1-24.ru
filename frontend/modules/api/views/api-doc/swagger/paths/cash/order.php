
  /api/cash/order:
    get:
      tags:
        - cash
      summary: Список движений по кассе
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/sort'
        - name: periodFrom
          in: query
          type: string
          format: date
          required: true
          description: Дата начала периода
        - name: periodTo
          in: query
          type: string
          format: date
          required: true
          description: Дата конца периода
        - name: contractor_id
          in: query
          type: integer
          required: false
          description: Фильтр по ID контрагента
      produces:
        - application/json
        - application/xml
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              $ref: '#/definitions/CashOrderFlows'
        401:
          description: Unauthorized
    post:
      tags:
        - cash
      summary: Создать движение по кассе
      consumes:
        - application/x-www-form-urlencoded
        - application/json
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: flow_type
          in: query
          description: |
            Тип:
            1 - приход
            0 - расход
          type: integer
          required: true
          enum: [0,1]
        - in: body
          name: CashOrderFlows
          required: true
          description: The CashOrderFlows to create.
          schema:
            $ref: '#/components/schemas/CashOrderFlows'
      produces:
        - application/json
        - application/xml
      responses:
        201:
          description: Ok
          schema:
            $ref: '#/definitions/CashOrderFlows'
        401:
          description: Unauthorized
  /api/cash/order/statistic:
    get:
      tags:
        - cash
      summary: Суммирующая статистика движеий по кассе
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: periodFrom
          in: query
          type: string
          format: date
          required: true
          description: Дата начала периода
        - name: periodTo
          in: query
          type: string
          format: date
          required: true
          description: Дата конца периода
        - name: cashbox
          in: query
          type: integer
          description: ID кассы, если не указан, поиск по всем доступным кассам
          required: false
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: object
            properties:
              dateStart:
                type: string
              dateEnd:
                type: string
              balanceStart:
                type: integer
              balanceEnd:
                type: integer
              totalIncome:
                type: integer
              totalExpense:
                type: integer
              countIncome:
                type: integer
              countExpense:
                type: integer
        401:
          description: Unauthorized
  /api/cash/order/{id}:
    get:
      tags:
        - cash
      summary: Данные движеия по кассе
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/CashOrderFlows'
        401:
          description: Unauthorized
    put:
      tags:
        - cash
      summary: Изменить движеие по кассе
      consumes:
        - application/x-www-form-urlencoded
        - application/json
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - in: body
          name: CashOrderFlows
          required: true
          description: The CashOrderFlows to create.
          schema:
            $ref: '#/components/schemas/CashOrderFlows'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/CashOrderFlows'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    patch:
      tags:
        - cash
      summary: Изменить движеие по кассе
      consumes:
        - application/x-www-form-urlencoded
        - application/json
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - in: body
          name: CashOrderFlows
          required: true
          description: The CashOrderFlows to create.
          schema:
            $ref: '#/components/schemas/CashOrderFlows'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/CashOrderFlows'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    delete:
      tags:
        - cash
      summary: Удалить движеие по кассе
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      responses:
        204:
          description: Ok
        401:
          description: Unauthorized
  /api/cash/order/self-contractors:
    get:
      tags:
        - cash
      summary: Список контрагентов для перевода собственных средств
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: flow_type
          in: query
          description: |
            Тип:
            1 - приход
            0 - расход
          type: integer
          required: false
          enum: [0,1]
        - name: exclude_cashbox
          in: query
          type: string
          format: date
          required: true
          description: ID кассы, которую надо исключить из списка
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              type: object
              properties:
                id:
                  type: string
                name:
                  type: string
        401:
          description: Unauthorized