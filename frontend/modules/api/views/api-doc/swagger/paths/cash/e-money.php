
  /api/cash/e-money:
    get:
      tags:
        - cash
      summary: Список движений по e-money
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/sort'
        - name: periodFrom
          in: query
          type: string
          format: date
          required: true
          description: Дата начала периода
        - name: periodTo
          in: query
          type: string
          format: date
          required: true
          description: Дата конца периода
        - name: contractor_id
          in: query
          type: integer
          required: false
          description: Фильтр по ID контрагента
      produces:
        - application/json
        - application/xml
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              $ref: '#/definitions/CashEMoneyFlows'
        401:
          description: Unauthorized
    post:
      tags:
        - cash
      summary: Создать движение по e-money
      consumes:
        - application/x-www-form-urlencoded
        - application/json
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: flow_type
          in: query
          description: |
            Тип:
            1 - приход
            0 - расход
          type: integer
          required: true
          enum: [0,1]
        - in: body
          name: CashEMoneyFlows
          required: true
          description: The CashEMoneyFlows to create.
          schema:
            $ref: '#/components/schemas/CashEMoneyFlows'
      produces:
        - application/json
        - application/xml
      responses:
        201:
          description: Ok
          schema:
            $ref: '#/definitions/CashEMoneyFlows'
        401:
          description: Unauthorized
  /api/cash/e-money/statistic:
    get:
      tags:
        - cash
      summary: Суммирующая статистика движеий по e-money
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: periodFrom
          in: query
          type: string
          format: date
          required: true
          description: Дата начала периода
        - name: periodTo
          in: query
          type: string
          format: date
          required: true
          description: Дата конца периода
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: object
            properties:
              dateStart:
                type: string
              dateEnd:
                type: string
              balanceStart:
                type: integer
              balanceEnd:
                type: integer
              totalIncome:
                type: integer
              totalExpense:
                type: integer
              countIncome:
                type: integer
              countExpense:
                type: integer
        401:
          description: Unauthorized
  /api/cash/e-money/{id}:
    get:
      tags:
        - cash
      summary: Данные движеия по e-money
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/CashEMoneyFlows'
        401:
          description: Unauthorized
    put:
      tags:
        - cash
      summary: Изменить движеие по e-money
      consumes:
        - application/x-www-form-urlencoded
        - application/json
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - in: body
          name: CashEMoneyFlows
          required: true
          description: The CashEMoneyFlows to create.
          schema:
            $ref: '#/components/schemas/CashEMoneyFlows'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/CashEMoneyFlows'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    patch:
      tags:
        - cash
      summary: Изменить движеие по e-money
      consumes:
        - application/x-www-form-urlencoded
        - application/json
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - in: body
          name: CashEMoneyFlows
          required: true
          description: The CashEMoneyFlows to create.
          schema:
            $ref: '#/components/schemas/CashEMoneyFlows'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/CashEMoneyFlows'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    delete:
      tags:
        - cash
      summary: Удалить движеие по e-money
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      responses:
        204:
          description: Ok
        401:
          description: Unauthorized
