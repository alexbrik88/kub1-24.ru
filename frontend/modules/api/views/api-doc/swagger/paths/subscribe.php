
  /api/subscribe/payment-params:
    get:
      tags:
        - subscribe
      summary: Данные параметров
      description: |
        Скидка указана в процентах.
        Скидка отнимается от стоимости тарифа и полученное значение округляется до целых рублей.
        При оплате нескольких компаний, оплата рассчитывается сначала для каждой компании, потом значения суммируются.
        Если есть скидка персональная и за несколько компаний, то для рассчета берется большее значение.

        - Сумма для компании:
        discount = max(discountByTariff, discountByCount)
        amount = round(tariffPrice - tariffPrice * discount / 100)
        - Общая сумма:
        totalAmount = amount1 + amount2 + ...
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
      responses:
        200:
          description: Ok
          schema:
            type: object
            properties:
              companyList:
                type: array
                description: Список компаний доступных для оплаты
                items:
                  type: object
                  properties:
                    id:
                      type: integer
                    name:
                      type: string
              tariffList:
                type: array
                description: Список доступных тарифов
                items:
                  type: object
                  properties:
                    id:
                      type: integer
                    duration_month:
                      type: integer
                    duration_day:
                      type: integer
                    price:
                      type: integer
                    discounts:
                      type: array
                      description: Скидка на данный тариф от количества компаний
                      items:
                        type: object
                        properties:
                          id:
                            type: integer
                          tariff_id:
                            type: integer
                          quantity:
                            type: integer
                          percent:
                            type: number
                    discountByCompany:
                      type: object
                      description: Скидка на данный тариф для каждой компании
                      properties:
                        '{company_id}':
                          type: number
              paymentTypeList:
                type: array
                description: Список доступных способов оплаты
                items:
                  type: object
                  properties:
                    id:
                      type: integer
                    name:
                      type: string
              discountByCount:
                type: object
                description: Скидка от количества выбранных компаний
                properties:
                  '{count}':
                    type: number
        401:
          description: Unauthorized
  /api/subscribe/create-payment:
    post:
      tags:
        - subscribe
      summary: Создать оплату сервиса
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: tariffId
          in: body
          description: Идентификатор тарифа
          type: integer
          required: true
        - name: paymentTypeId
          in: body
          description: Идентификатор способа оплаты
          type: integer
          required: true
        - name: createInvoice
          in: body
          description: |
            Входящий счет в сервисе
            1 - Создавать
            0 - Не создавать
          type: integer
          required: true
        - name: createInvoice
          in: body
          description: Идентификаторы оплачиваемых компаний
          schema:
            type: array
            items:
              type: integer
        - name: companyId
          in: body
          description: Идентификатор оплачивающей компании
          type: integer
          required: true
      produces:
        - application/json
        - application/xml
      responses:
        200:
          description: Ok
        401:
          description: Unauthorized
  /api/subscribe/activate-promocode:
    post:
      tags:
        - subscribe
      summary: Активировать промокод
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: code
          in: body
          description: Промокод
          type: string
          required: true
      produces:
        - application/json
        - application/xml
      responses:
        200:
          description: Ok
          schema:
            type: object
            properties:
              result:
                type: boolean
                example: true
              messages:
                type: string
                example: Промокод  успешно зарегистирован.
        401:
          description: Unauthorized
        422:
          description: Data validation failed
  /api/subscribe/subscribes-expires:
    get:
      tags:
        - subscribe
      summary: Подписки
      description: Список доступных подписок + cписок актуальных подписок.
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
      responses:
        200:
          description: Ok
          schema:
            type: object
            properties:
              subscribes:
                type: array
                description: Список доступных подписок
                items:
                  type: object
                  properties:
                    id:
                      type: integer
                    name:
                      type: string
                    is_base:
                      type: integer
                    has_base:
                      type: integer
                    is_active:
                      type: integer
                    description:
                      type: string
              expires:
                type: array
                description: Список актуальных подписок
                items:
                  type: object
                  properties:
                    id:
                      type: integer
                    name:
                      type: string
                    expires_at:
                      type: integer
                    days_number:
                      type: integer
                    days_text:
                      type: string
                    is_trial:
                      type: ищщдуфт
        401:
          description: Unauthorized