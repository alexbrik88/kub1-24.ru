<?php

use common\models\company\CompanyType;

$typeArray = CompanyType::find()->select('name_short')->where(['in_contractor' => 1])->indexBy('id')->column();
?>

  /out/invoice/{id}:
    get:
      tags:
        - out-invoice-widget
      summary: Выставление счета на оплату
      description: Редирект пользователя на виджет выставления счетов
      consumes:
        - application/x-www-form-urlencoded
      produces:
        - text/html
      parameters:
        - name: id
          in: path
          description: Идентификатор виджета
          required: true
          type: string
        - name: uid
          in: query
          description: Идентификатор заказа в системе партнера (необходим для получения данных)
          type: string
        - name: hash
          in: query
          description: Проверочный код заказа в системе партнера (необходим для получения данных)
          type: string
      responses:
        200:
          description: Ok
        404:
          description: Not found
  http://site.com/URL_данных:
    post:
      tags:
        - out-invoice-widget
      summary: Получение данных счета
      description: Запрос данных счета по адресу, указанному в "URL данных"
      consumes:
        - application/x-www-form-urlencoded
      produces:
        - application/json
      parameters:
        - name: uid
          in: formData
          description: Идентификатор заказа в системе партнера
          required: true
          type: string
        - name: hash
          in: formData
          description: Проверочный код заказа в системе партнера
          required: true
          type: string
      responses:
        200:
          description: Ok
          schema:
            type: object
            properties:
              product:
                type: object
                properties:
                  123:
                    type: number
                  124:
                    type: number
                    description: '"ID товара/услуги":"количество товара/услуги"'
              price:
                type: object
                properties:
                  123:
                    type: number
                  124:
                    type: number
                    description: '"ID товара/услуги":"цена товара/услуги"'
              valid_until:
                type: string
                format: date
                description: Счет действителен до указанной даты
              email_content:
                type: string
                description: Дополнительный текст в письме
              attach:
                type: array
                description: Прикрепить файл
                items:
                  type: object
                  properties:
                    file_name:
                      type: string
                      description: Имя прикрепляемого файла
                    file_url:
                      type: string
                      description: URL прикрепляемого файла
                  required:
                    - file_name
                    - file_url
              email:
                type: string
                format: email
                description: E-mail
              phone:
                type: string
                description: Телефон
              comment:
                type: string
                description: Комментарий
              legal_type:
                type: integer
                description: |
                  Форма собственности
<?php foreach ($typeArray as $key => $value) : ?>
                  <?= "\"{$key}\" {$value}\n" ?>
<?php endforeach; ?>
              legal_name:
                type: string
                description: Название
              legal_inn:
                type: string
                description: ИНН
              legal_address:
                type: string
                description: Юридический адрес
              legal_kpp:
                type: string
                description: КПП
              legal_rs:
                type: string
                description: Расчетный счет
              legal_bik:
                type: string
                description: БИК банка
              chief_position:
                type: string
                description: Должность руководителя
              chief_name:
                type: string
                description: ФИО руководителя
  http://site.com/URL_результата:
    post:
      tags:
        - out-invoice-widget
      summary: Передача результата
      description: Передача данных счета по адресу, указанному в "URL результата"
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - name: uid
          in: formData
          description: Идентификатор заказа в системе партнера
          required: true
          type: string
        - name: hash
          in: formData
          description: Проверочный код заказа в системе партнера
          required: true
          type: string
        - name: invoice
          in: formData
          description: Данные счета
          required: true
          schema:
            type: object
            properties:
              number:
                type: string
                description: Номер счета
              date:
                type: string
                description: Дата счета
              items_count:
                type: string
                description: Количество наименований товаров/услуг
              sum:
                type: string
                description: Сумма счета без НДС
              nds:
                type: string
                description: Сумма НДС
              sum_with_nds:
                type: string
                description: Сумма счета с НДС
              provider_name:
                type: string
                description: Наименование продавца
              provider_address:
                type: string
                description: Адрес продавца
              provider_inn:
                type: string
                description: ИНН продавца
              provider_kpp:
                type: string
                description: КПП продавца
              provider_bank_bik:
                type: string
                description: Бик банка продавца
              provider_bank_ks:
                type: string
                description: К/с банка продавца
              provider_bank_rs:
                type: string
                description: Р/с продавца
              customer_name:
                type: string
                description: Наименование покупателя
              customer_address:
                type: string
                description: Адрес  покупателя
              customer_inn:
                type: string
                description: ИНН покупателя
              customer_kpp:
                type: string
                description: КПП покупателя
              customer_bank_bik:
                type: string
                description: БИК банка покупателя
              customer_bank_ks:
                type: string
                description: К/с банка покупателя
              customer_bank_rs:
                type: string
                description: Р/с покупателя
              items:
                type: object
                description: Данные о товарах/услугах счета
                properties:
                  id:
                    type: integer
                    description: Идентификатор товара/услуги
                  name:
                    type: string
                    description: Наименование товара/услуги
                  count:
                    type: number
                    description: Количество
                  unit_name:
                    type: string
                    description: Единица измерения
                  price:
                    type: number
                    description: Цена за единицу
                  amount:
                    type: number
                    description: Сумма
      responses:
        200:
          description: Ok
