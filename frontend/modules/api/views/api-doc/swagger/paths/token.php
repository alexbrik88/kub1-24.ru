
  /api/token:
    get:
      tags:
        - token
      summary: Получить уникальный токен доступа
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBasic'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: object
            properties:
              access_token:
                type: string
                description: The user access_token.
        401:
          description: Unauthorized
