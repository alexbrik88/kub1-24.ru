
  /api/payment-order:
    get:
      tags:
        - payment-order
      summary: Список ПП
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/documentType'
        - $ref: '#/components/parameters/sort'
        - name: periodFrom
          in: query
          type: string
          format: date
          required: true
          description: Дата начала периода
        - name: periodTo
          in: query
          type: string
          format: date
          required: true
          description: Дата конца периода
      produces:
        - application/json
        - application/xml
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              $ref: '#/definitions/PaymentOrder'
        401:
          description: Unauthorized
    post:
      tags:
        - payment-order
      summary: Создать ПП
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: invoice_id
          in: query
          description: Идентификатор счета
          type: integer
          required: true
      produces:
        - application/json
        - application/xml
      responses:
        201:
          description: Ok
          schema:
            $ref: '#/definitions/PaymentOrder'
        401:
          description: Unauthorized
  /api/payment-order/{id}:
    get:
      tags:
        - payment-order
      summary: Информация о ПП
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/PaymentOrder'
        401:
          description: Unauthorized
    put:
      tags:
        - payment-order
      summary: Изменить ПП
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - name: PaymentOrder
          in: body
          description: информация о ПП
          required: true
          schema:
            $ref: '#/components/schemas/PaymentOrder'
        - name: orderArray
          in: body
          description: информация о товарах/услугах
          required: true
          schema:
            type: object
            properties:
              "{order_id}":
                type: number
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/PaymentOrder'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    patch:
      tags:
        - payment-order
      summary: Изменить ПП
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - name: PaymentOrder
          in: body
          description: информация о ПП
          required: true
          schema:
            $ref: '#/components/schemas/PaymentOrder'
        - name: orderArray
          in: body
          description: информация о товарах/услугах
          required: true
          schema:
            type: object
            properties:
              "{order_id}":
                type: number
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/PaymentOrder'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    delete:
      tags:
        - payment-order
      summary: Удалить ПП
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      responses:
        204:
          description: Ok
        401:
          description: Unauthorized
  /api/payment-order/{id}/payment-status:
    get:
      tags:
        - payment-order
      summary: Статус оплаты
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: object
            properties:
              status:
                type: string
                example: success
        422:
          description: Unknown
          schema:
            type: object
            properties:
              status:
                type: string
                example: unknown
        401:
          description: Unauthorized
