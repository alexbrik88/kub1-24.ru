
  /api/profile/view:
    get:
      tags:
        - profile
      summary: Профиль пользователя
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: object
            properties:
              id:
                type: integer
                description: ID
              email:
                type: string
                description: Email
              created_at:
                type: integer
                description: Время создания (Unix timestamp)
              lastname:
                type: string
                description: Фамилия
              firstname:
                type: string
                description: Имя
              firstname_initial:
                type: string
                description: Инициал имени
              patronymic:
                type: string
                description: Отчество
              patronymic_initial:
                type: string
                description: Инициал отчества
              sex:
                type: integer
                description: Пол
              birthday:
                type: date
                description: Дата рождения
              phone:
                type: string
                description: Телефон
              time_zone_id:
                type: integer
                description: ID временной зоны
              last_visit_at:
                type: integer
                description: Последнее посещение (Unix timestamp)
              push_notification_new_message:
                type: boolean
              push_notification_create_closest_document:
                type: boolean
              push_notification_overdue_invoice:
                type: boolean
              duplicate_notification_to_sms:
                type: boolean
              duplicate_notification_to_email:
                type: boolean
        401:
          description: Unauthorized

  /api/profile/update:
    put:
      tags:
        - profile
      summary: Редактирование профиля пользователя
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - in: body
          name: lastname
          type: string
          description: Фамилия
        - in: body
          name: firstname
          type: string
          description: Имя
        - in: body
          name: patronymic
          type: string
          description: Отчество
        - in: body
          name: sex
          type: integer
          description: Пол
        - in: body
          name: birthday
          type: date
          description: Дата рождения
        - in: body
          name: phone
          type: string
          description: Телефон
        - in: body
          name: time_zone_id
          type: integer
          description: ID временной зоны
        - in: body
          name: push_notification_new_message
          type: boolean
        - in: body
          name: push_notification_create_closest_document
          type: boolean
        - in: body
          name: push_notification_overdue_invoice
          type: boolean
        - in: body
          name: duplicate_notification_to_sms
          type: boolean
        - in: body
          name: duplicate_notification_to_email
          type: boolean
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: object
            properties:
              id:
                type: integer
                description: ID
              email:
                type: string
                description: Email
              created_at:
                type: integer
                description: Время создания (Unix timestamp)
              lastname:
                type: string
                description: Фамилия
              firstname:
                type: string
                description: Имя
              firstname_initial:
                type: string
                description: Инициал имени
              patronymic:
                type: string
                description: Отчество
              patronymic_initial:
                type: string
                description: Инициал отчества
              sex:
                type: integer
                description: Пол
              birthday:
                type: date
                description: Дата рождения
              phone:
                type: string
                description: Телефон
              time_zone_id:
                type: integer
                description: ID временной зоны
              last_visit_at:
                type: integer
                description: Последнее посещение (Unix timestamp)
              push_notification_new_message:
                type: boolean
              push_notification_create_closest_document:
                type: boolean
              push_notification_overdue_invoice:
                type: boolean
              duplicate_notification_to_sms:
                type: boolean
              duplicate_notification_to_email:
                type: boolean
        401:
          description: Unauthorized

    patch:
      tags:
        - profile
      summary: Редактирование профиля пользователя
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - in: body
          name: lastname
          type: string
          description: Фамилия
        - in: body
          name: firstname
          type: string
          description: Имя
        - in: body
          name: patronymic
          type: string
          description: Отчество
        - in: body
          name: sex
          type: integer
          description: Пол
        - in: body
          name: birthday
          type: date
          description: Дата рождения
        - in: body
          name: phone
          type: string
          description: Телефон
        - in: body
          name: time_zone_id
          type: integer
          description: ID временной зоны
        - in: body
          name: push_notification_new_message
          type: boolean
        - in: body
          name: push_notification_create_closest_document
          type: boolean
        - in: body
          name: push_notification_overdue_invoice
          type: boolean
        - in: body
          name: duplicate_notification_to_sms
          type: boolean
        - in: body
          name: duplicate_notification_to_email
          type: boolean
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: object
            properties:
              id:
                type: integer
                description: ID
              email:
                type: string
                description: Email
              created_at:
                type: integer
                description: Время создания (Unix timestamp)
              lastname:
                type: string
                description: Фамилия
              firstname:
                type: string
                description: Имя
              firstname_initial:
                type: string
                description: Инициал имени
              patronymic:
                type: string
                description: Отчество
              patronymic_initial:
                type: string
                description: Инициал отчества
              sex:
                type: integer
                description: Пол
              birthday:
                type: date
                description: Дата рождения
              phone:
                type: string
                description: Телефон
              time_zone_id:
                type: integer
                description: ID временной зоны
              last_visit_at:
                type: integer
                description: Последнее посещение (Unix timestamp)
              push_notification_new_message:
                type: boolean
              push_notification_create_closest_document:
                type: boolean
              push_notification_overdue_invoice:
                type: boolean
              duplicate_notification_to_sms:
                type: boolean
              duplicate_notification_to_email:
                type: boolean
        401:
          description: Unauthorized
