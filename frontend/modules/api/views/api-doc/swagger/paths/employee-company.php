
  /api/employee-company:
    get:
      tags:
        - employee-company
      summary: Список сотрудников
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/sort'
      produces:
        - application/json
        - application/xml
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              $ref: '#/definitions/EmployeeCompany'
        401:
          description: Unauthorized
    post:
      tags:
        - employee-company
      summary: Добавить сотрудника
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - in: body
          description: Данные сотрудника
          required: true
          schema:
            $ref: '#/components/schemas/EmployeeCompany'
      produces:
        - application/json
        - application/xml
      responses:
        201:
          description: Ok
          schema:
            $ref: '#/definitions/EmployeeCompany'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
  /api/employee-company/{id}:
    get:
      tags:
        - employee-company
      summary: Данные сотрудника
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/EmployeeCompany'
        401:
          description: Unauthorized
    put:
      tags:
        - employee-company
      summary: Редактировать сотрудника
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - in: body
          description: Данные сотрудника
          required: true
          schema:
            $ref: '#/components/schemas/EmployeeCompany'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/EmployeeCompany'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    patch:
      tags:
        - employee-company
      summary: Редактировать сотрудника
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - in: body
          description: Данные сотрудника
          required: true
          schema:
            $ref: '#/components/schemas/EmployeeCompany'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/EmployeeCompany'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
