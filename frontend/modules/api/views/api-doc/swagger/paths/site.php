
  /api/site/registration:
    post:
      tags:
        - site
      summary: регистрация
      consumes:
        - application/x-www-form-urlencoded
      produces:
        - application/json
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - name: email
          in: formData
          description: E-mail
          required: true
          type: string
        - name: password
          in: formData
          description: Пароль
          required: true
          type: password
        - name: phone
          in: formData
          description: Телефон в формате +7(ХХХ) ХХХ-ХХ-ХХ
          required: false
          type: string
        - name: companyType
          in: formData
          description: Форма бизнеса
          required: true
          type: integer
        - name: taxationTypeOsno
          in: formData
          description: Система налогобложения ОСНО (общая)
          type: boolean
        - name: taxationTypeUsn
          in: formData
          description: Система налогобложения УСН (упращенка)
          type: boolean
        - name: taxationTypeEnvd
          in: formData
          description: Система налогобложения ЕНВД (вмененка)
          type: boolean
        - name: taxationTypePsn
          in: formData
          description: Система налогобложения ПСН (патент)
          type: boolean
        - name: promoCode
          in: formData
          description: Промокод
          type: string
        - name: checkrules
          in: formData
          description: Принимаю условия соглашения
          required: true
          type: boolean
      responses:
        200:
          description: Ok
          schema:
            type: object
            properties:
              access_token:
                type: string
                description: The user access_token.
        422:
          description: Data validation failed
  /api/site/request-password-reset:
    put:
      tags:
        - site
      summary: запрос кода для восстановления пароля
      consumes:
        - application/json
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - in: formData
          name: email
          description: Email.
          required: true
          type: string
      responses:
        204:
          description: Ok
        422:
          description: Data validation failed
        424:
          description: Send Email failed
  /api/site/reset-password:
    put:
      tags:
        - site
      summary: Восстановление пароля
      consumes:
        - application/json
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - in: formData
          name: token
          description: Код восстановления пароля.
          required: true
          type: string
        - in: formData
          name: password
          description: Новый пароль.
          required: true
          type: string
      responses:
        204:
          description: Ok
        422:
          description: Data validation failed
