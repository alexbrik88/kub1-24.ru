
  /api/contractor-account/{contractor_id}:
    get:
      tags:
        - contractor-account
      summary: Список счетов контрагента
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: contractor_id
          in: path
          type: integer
          description: Идентификатор контрагента
          required: true
        - name: is_main
          in: query
          type: integer
          enum: [1,0]
          description: Фильтр по показателю основного р/с 1|0
          required: false
        - name: bik
          in: query
          type: string
          description: Фильтр по БИК банка
          required: false
        - name: rs
          in: query
          type: string
          description: Фильтр по номеру р/с
          required: false
      produces:
        - application/json
        - application/xml
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              $ref: '#/definitions/ContractorAccount'
        401:
          description: Unauthorized
    post:
      tags:
        - contractor-account
      summary: Добавить р/с
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: contractor_id
          in: path
          type: integer
          description: Идентификатор контрагента
          required: true
        - in: body
          description: информация о р/с
          required: true
          schema:
            $ref: '#/components/schemas/ContractorAccount'
      produces:
        - application/json
        - application/xml
      responses:
        201:
          description: Ok
          schema:
            $ref: '#/definitions/ContractorAccount'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
  /api/contractor-account/{contractor_id}/{id}:
    get:
      tags:
        - contractor-account
      summary: Информация о р/с
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: contractor_id
          in: path
          type: integer
          description: Идентификатор контрагента
          required: true
        - $ref: '#/components/parameters/objectId'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/ContractorAccount'
        401:
          description: Unauthorized
    put:
      tags:
        - contractor-account
      summary: Изменить р/с контрагента
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: contractor_id
          in: path
          type: integer
          description: Идентификатор контрагента
          required: true
        - $ref: '#/components/parameters/objectId'
        - in: body
          description: информация о р/с
          required: true
          schema:
            $ref: '#/components/schemas/ContractorAccount'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/ContractorAccount'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    patch:
      tags:
        - contractor-account
      summary: Изменить р/с контрагента
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: contractor_id
          in: path
          type: integer
          description: Идентификатор контрагента
          required: true
        - $ref: '#/components/parameters/objectId'
        - in: body
          description: информация о р/с
          required: true
          schema:
            $ref: '#/components/schemas/ContractorAccount'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/ContractorAccount'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    delete:
      tags:
        - contractor-account
      summary: Удалить р/с контрагента
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: contractor_id
          in: path
          type: integer
          description: Идентификатор контрагента
          required: true
        - $ref: '#/components/parameters/objectId'
      responses:
        204:
          description: Ok
        401:
          description: Unauthorized
