
  /api/invoice-facture:
    get:
      tags:
        - invoice-facture
      summary: Список СФ
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/documentType'
        - $ref: '#/components/parameters/sort'
        - name: periodFrom
          in: query
          type: string
          format: date
          required: true
          description: Дата начала периода
        - name: periodTo
          in: query
          type: string
          format: date
          required: true
          description: Дата конца периода
        - name: invoice_id
          in: query
          type: integer
          description: Фильтр по ID счета
      produces:
        - application/json
        - application/xml
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              $ref: '#/definitions/InvoiceFacture'
        401:
          description: Unauthorized
    post:
      tags:
        - invoice-facture
      summary: Создать СФ
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: invoice_id
          in: query
          description: Идентификатор счета
          type: integer
          required: true
      produces:
        - application/json
        - application/xml
      responses:
        201:
          description: Ok
          schema:
            $ref: '#/definitions/InvoiceFacture'
        401:
          description: Unauthorized
  /api/invoice-facture/{id}:
    get:
      tags:
        - invoice-facture
      summary: Информация о СФ
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/InvoiceFacture'
        401:
          description: Unauthorized
    put:
      tags:
        - invoice-facture
      summary: Изменить СФ
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - in: body
          description: информация о СФ
          required: true
          schema:
            $ref: '#/definitions/InvoiceFacture'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/InvoiceFacture'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    patch:
      tags:
        - invoice-facture
      summary: Изменить СФ
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - in: body
          description: информация о СФ
          required: true
          schema:
            $ref: '#/definitions/InvoiceFacture'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/InvoiceFacture'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    delete:
      tags:
        - invoice-facture
      summary: Удалить СФ
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      responses:
        204:
          description: Ok
        401:
          description: Unauthorized
  /api/invoice-facture/{id}/pdf:
    get:
      tags:
        - invoice-facture
      summary: Документ в формате pdf
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      produces:
        - application/pdf
      responses:
        200:
          description: Ok
          schema:
            type: file
        401:
          description: Unauthorized
        403:
          description: Forbidden
        404:
          description: Not Found
  /api/invoice-facture/{id}/png:
    get:
      tags:
        - invoice-facture
      summary: Документ в формате png
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - name: page
          in: query
          type: integer
          description: Номер страницы документа
      produces:
        - image/png
      responses:
        200:
          description: Ok
          schema:
            type: file
        401:
          description: Unauthorized
        403:
          description: Forbidden
        404:
          description: Not Found
  /api/invoice-facture/{id}/send:
    post:
      tags:
        - invoice-facture
      summary: Отправить документ электронной почтой
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - name: subject
          in: body
          type: string
          description: Тема письма
          required: true
        - name: sendTo
          in: body
          type: string
          format: email
          description: Адрес(а) получателя
          required: true
        - name: emailText
          in: body
          type: string
          description: Текст письма
          required: false
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: object
            properties:
              sent:
                type: integer
                description: Количество отправленных
              total:
                type: integer
                description: Всего адресов
        401:
          description: Unauthorized
        403:
          description: Forbidden
        404:
          description: Not Found
        422:
          description: Data validation failed
