
  /api/act:
    get:
      tags:
        - act
      summary: Список актов
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/documentType'
        - $ref: '#/components/parameters/sort'
        - name: periodFrom
          in: query
          type: string
          format: date
          required: true
          description: Дата начала периода
        - name: periodTo
          in: query
          type: string
          format: date
          required: true
          description: Дата конца периода
        - name: invoice_id
          in: query
          type: integer
          description: Фильтр по ID счета
      produces:
        - application/json
        - application/xml
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              $ref: '#/definitions/Act'
        401:
          description: Unauthorized
    post:
      tags:
        - act
      summary: Создать акт
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: invoice_id
          in: query
          description: Идентификатор счета
          type: integer
          required: true
      produces:
        - application/json
        - application/xml
      responses:
        201:
          description: Ok
          schema:
            $ref: '#/definitions/Act'
        401:
          description: Unauthorized
  /api/act/{id}:
    get:
      tags:
        - act
      summary: Информация об акте
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/Act'
        401:
          description: Unauthorized
    put:
      tags:
        - act
      summary: Изменить акт
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - name: Act
          in: body
          description: информация об акте
          required: true
          schema:
            $ref: '#/components/schemas/Act'
        - name: orderArray
          in: body
          description: информация о товарах/услугах
          required: true
          schema:
            type: object
            properties:
              "{order_id}":
                type: number
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/Act'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    patch:
      tags:
        - act
      summary: Изменить акт
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - name: Act
          in: body
          description: информация об акте
          required: true
          schema:
            $ref: '#/components/schemas/Act'
        - name: orderArray
          in: body
          description: информация о товарах/услугах
          required: true
          schema:
            type: object
            properties:
              "{order_id}":
                type: number
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/Act'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    delete:
      tags:
        - act
      summary: Удалить акт
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      responses:
        204:
          description: Ok
        401:
          description: Unauthorized
  /api/act/{id}/pdf:
    get:
      tags:
        - act
      summary: Документ в формате pdf
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      produces:
        - application/pdf
      responses:
        200:
          description: Ok
          schema:
            type: file
        401:
          description: Unauthorized
        403:
          description: Forbidden
        404:
          description: Not Found
  /api/act/{id}/png:
    get:
      tags:
        - act
      summary: Документ в формате png
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - name: page
          in: query
          type: integer
          description: Номер страницы документа
      produces:
        - image/png
      responses:
        200:
          description: Ok
          schema:
            type: file
        401:
          description: Unauthorized
        403:
          description: Forbidden
        404:
          description: Not Found
  /api/act/{id}/send:
    post:
      tags:
        - act
      summary: Отправить документ электронной почтой
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - name: subject
          in: body
          type: string
          description: Тема письма
          required: true
        - name: sendTo
          in: body
          type: string
          format: email
          description: Адрес(а) получателя
          required: true
        - name: emailText
          in: body
          type: string
          description: Текст письма
          required: false
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: object
            properties:
              sent:
                type: integer
                description: Количество отправленных
              total:
                type: integer
                description: Всего адресов
        401:
          description: Unauthorized
        403:
          description: Forbidden
        404:
          description: Not Found
        422:
          description: Data validation failed
