
  /api/product-group:
    get:
      tags:
        - product-group
      summary: Группы товаров/услуг
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: production_type
          in: query
          description: "Тип: 0 услуга, 1 товар"
          enum: [0,1]
          required: true
      produces:
        - application/json
        - application/xml
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              $ref: '#/definitions/ProductGroup'
        401:
          description: Unauthorized
    post:
      tags:
        - product-group
      summary: Добавить группу
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - in: body
          description: информация о группе
          required: true
          schema:
            $ref: '#/components/schemas/ProductGroup'
      produces:
        - application/json
        - application/xml
      responses:
        201:
          description: Ok
          schema:
            $ref: '#/definitions/ProductGroup'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
  /api/product-group/{id}:
    get:
      tags:
        - product-group
      summary: Информация о группе
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/ProductGroup'
        401:
          description: Unauthorized
    put:
      tags:
        - product-group
      summary: Изменить группу
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - in: body
          description: информация о тваре/услуге
          required: true
          schema:
            type: object
            properties:
              title:
                type: string
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/ProductGroup'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    patch:
      tags:
        - product-group
      summary: Изменить группу
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - in: body
          description: Информация о группе
          required: true
          schema:
            type: object
            properties:
              title:
                type: string
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/ProductGroup'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    delete:
      tags:
        - product-group
      summary: Удалить группу
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      responses:
        204:
          description: Ok
        401:
          description: Unauthorized
