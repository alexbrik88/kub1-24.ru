
  /api/upd:
    get:
      tags:
        - upd
      summary: Список УПДов
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/documentType'
        - $ref: '#/components/parameters/sort'
        - name: periodFrom
          in: query
          type: string
          format: date
          required: true
          description: Дата начала периода
        - name: periodTo
          in: query
          type: string
          format: date
          required: true
          description: Дата конца периода
        - name: invoice_id
          in: query
          type: integer
          description: Фильтр по ID счета
      produces:
        - application/json
        - application/xml
      responses:
        200:
          description: Ok
          schema:
            type: array
            items:
              $ref: '#/definitions/Upd'
        401:
          description: Unauthorized
    post:
      tags:
        - upd
      summary: Создать УПД
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - name: invoice_id
          in: query
          description: Идентификатор счета
          type: integer
          required: true
      produces:
        - application/json
        - application/xml
      responses:
        201:
          description: Ok
          schema:
            $ref: '#/definitions/Upd'
        401:
          description: Unauthorized
  /api/upd/{id}:
    get:
      tags:
        - upd
      summary: Информация об УПД
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/Upd'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    put:
      tags:
        - upd
      summary: Изменить УПД
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - in: body
          description: информация об УПД
          required: true
          schema:
            $ref: '#/definitions/Upd'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/Upd'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    patch:
      tags:
        - upd
      summary: Изменить УПД
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - in: body
          description: информация об УПД
          required: true
          schema:
            $ref: '#/definitions/Upd'
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            $ref: '#/definitions/Upd'
        401:
          description: Unauthorized
        422:
          description: Data validation failed
    delete:
      tags:
        - upd
      summary: Удалить УПД
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      responses:
        204:
          description: Ok
        401:
          description: Unauthorized
  /api/upd/{id}/pdf:
    get:
      tags:
        - upd
      summary: Документ в формате pdf
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
      produces:
        - application/pdf
      responses:
        200:
          description: Ok
          schema:
            type: file
        401:
          description: Unauthorized
        403:
          description: Forbidden
        404:
          description: Not Found
  /api/upd/{id}/png:
    get:
      tags:
        - upd
      summary: Документ в формате png
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - name: page
          in: query
          type: integer
          description: Номер страницы документа
      produces:
        - image/png
      responses:
        200:
          description: Ok
          schema:
            type: file
        401:
          description: Unauthorized
        403:
          description: Forbidden
        404:
          description: Not Found
  /api/upd/{id}/send:
    post:
      tags:
        - upd
      summary: Отправить документ электронной почтой
      consumes:
        - application/x-www-form-urlencoded
      parameters:
        - $ref: '#/components/parameters/AuthorizationApiKey'
        - $ref: '#/components/parameters/AuthorizationBearer'
        - $ref: '#/components/parameters/CurrentCompany'
        - $ref: '#/components/parameters/objectId'
        - name: subject
          in: body
          type: string
          description: Тема письма
          required: true
        - name: sendTo
          in: body
          type: string
          format: email
          description: Адрес(а) получателя
          required: true
        - name: emailText
          in: body
          type: string
          description: Текст письма
          required: false
      produces:
        - application/json
      responses:
        200:
          description: Ok
          schema:
            type: object
            properties:
              sent:
                type: integer
                description: Количество отправленных
              total:
                type: integer
                description: Всего адресов
        401:
          description: Unauthorized
        403:
          description: Forbidden
        404:
          description: Not Found
        422:
          description: Data validation failed
