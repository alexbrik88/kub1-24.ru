<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.07.2017
 * Time: 9:31
 */

namespace frontend\modules\api\models;


use common\components\validators\PasswordValidator;
use common\models\employee\Employee;
use yii\base\Model;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class ChangePasswordForm
 * @package frontend\modules\api\models
 */
class ChangePasswordForm extends Model
{
    /**
     * @var
     */
    public $oldPassword;

    /**
     * @var
     */
    public $newPassword;

    /**
     * @var
     */
    public $newPasswordRepeat;

    /**
     * @var Employee
     */
    private $_user;


    /**
     * @param Employee $user
     * @param array $config
     * @throws NotFoundHttpException
     */
    public function __construct(Employee $user, $config = [])
    {
        $this->_user = $user;
        if (!$this->_user) {
            throw new NotFoundHttpException;
        }

        parent::__construct($config = []);
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['oldPassword', 'newPassword', 'newPasswordRepeat'], 'required'],
            [['oldPassword'], function ($attribute) {
                if (!$this->_user->validatePassword($this->$attribute)) {
                    $this->addError($attribute, 'Неверный пароль');
                }
            }],
            [['newPassword'], PasswordValidator::className(),],
            [['newPasswordRepeat'], 'compare', 'compareAttribute' => 'newPassword'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'oldPassword' => 'Старый пароль',
            'newPassword' => 'Новый пароль',
            'newPasswordRepeat' => 'Повтор нового пароля',
        ];
    }

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function save()
    {
        $user = $this->_user;
        $user->setPassword($this->newPassword);

        return $user->save(true, [
            'password',
        ]);
    }
}