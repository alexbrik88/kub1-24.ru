<?php

namespace frontend\modules\api\models;

class PngHelper {

    public static function convertPdf2Png($pdf_in, $page = 1)
    {
        $tmpDir = sys_get_temp_dir();
        
        $img = new \imagick();
        $img->setRegistry('temporary-path', $tmpDir);
        $img->setResolution(250, 250);
        $img->readImageBlob($pdf_in);
        $totalPages = $img->getNumberImages();

        $i=0;
        foreach($img as $_img) {

            $i++;

            if ($i == $page) {
                $white = new \imagick();
                $white->setRegistry('temporary-path', $tmpDir);
                $white->newImage($_img->getImageWidth(), $_img->getImageHeight(), "white");
                $white->compositeimage($_img, \imagick::COMPOSITE_OVER, 0, 0);
                $white->setImageFormat('png');

                return [
                    'content' => $white,
                    'pagesCount' => $totalPages
                ];
            }
        }

        return null;
    }
}