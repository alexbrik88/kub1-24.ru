<?php

namespace frontend\modules\api;

use common\models\api\ApiPartners;
use common\models\CreateType;
use common\models\company\ApiFirstVisit;
use common\models\document\Invoice;
use common\models\employee\Employee;
use common\models\file\FileUploadType;
use frontend\models\Documents;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UnsupportedMediaTypeHttpException;
use yii\web\User;

/**
 * api module definition class
 */
class Module extends \yii\base\Module
{
    private $apiPartner;

    public static $formats = [
        'application/json' => Response::FORMAT_JSON,
        'application/xml' => Response::FORMAT_XML,
    ];

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\api\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $requestId = Yii::$app->security->generateRandomString() . '_' . time();

        $_SERVER['X-Request-Id'] = $requestId;
        Yii::$app->response->headers->set('X-Request-Id', $requestId);
        Yii::$app->response->format = Response::FORMAT_RAW;

        Yii::$app->response->on('afterSend', function($event) use ($requestId) {
            $message = "[{$requestId}]\n\n";
            $message .= \frontend\modules\api\components\Dump::request(Yii::$app->request)."\n";
            $message .= "================================\n";
            $message .= \frontend\modules\api\components\Dump::response($event->sender, !$event->sender->getIsSuccessful());
            Yii::info($message, 'api_debug');
        });

        Yii::$app->set('user', [
            'class' => 'frontend\components\WebUser',
            'identityClass' => 'common\models\employee\Employee',
            'enableSession' => $this->getIsBanking(),
            'loginUrl' => null,
        ]);

        Yii::$app->user->on('afterLogin', function ($event) {
            if ($event->identity->is_active == false) {
                throw new ForbiddenHttpException("Ваш аккаунт заблокирован. Обратитесь в службу технической поддержки.");
            }
            $companyId = $this->getIsBanking() ?
                Yii::$app->getRequest()->get('company-id') :
                Yii::$app->request->headers->get('X-Company-ID');
            $event->identity->setCompany($companyId);
            if ($event->identity->company && $this->apiPartner) {
                Yii::$app->db->createCommand()->upsert(ApiFirstVisit::tableName(), [
                    'company_id' => $event->identity->company->id,
                    'api_partners_id' => $this->apiPartner->id,
                ], false)->execute();
            }
        });
    }

    /**
     * This method is invoked right before an action within this module is executed.
     *
     * @param yii\base\Action $action the action to be executed.
     * @return bool whether the action should continue to be executed.
     */
    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        $request = Yii::$app->getRequest();
        $headers = $request->getHeaders();

        if ($action->controller->id != 'api-doc') {
            $apiKey = $this->getIsBanking() ?  $request->get('api-key') : $headers->get('X-ApiKey');
            $apiPartner = ApiPartners::findByApiKey($apiKey)->one();

            if (!$apiPartner) {
                if (!$this->getIsBanking()) {
                    $this->setResponseFormat();
                }

                throw new ForbiddenHttpException('Invalid API Key.');
            }

            $this->apiPartner = $apiPartner;
            Yii::$app->params['create_type_id'] = CreateType::TYPE_API;
            Yii::$app->params['create_api_id'] = $apiPartner->id;
            Yii::$app->params['upload_type_id'] = FileUploadType::TYPE_API;
        }

        if (ArrayHelper::getValue(Yii::$app->params, 'api-log')) {
            $method = $request->getMethod();
            $logText = date('c') . " {$method} {$request->getPathInfo()}\n";
            $logText .= VarDumper::dumpAsString([
                'headers' => $headers->toArray(),
                'queryParams' => $request->getQueryParams(),
                'bodyParams' => $request->getBodyParams(),
            ]) . "\n\n";

            file_put_contents(Yii::getAlias("@runtime/logs/API-{$method}.log"), $logText, FILE_APPEND);
        }

        return true;
    }

    /**
     *
     */
    public function setResponseFormat()
    {
        $response = Yii::$app->getResponse();
        $types = Yii::$app->getRequest()->getAcceptableContentTypes();
        if (empty($types)) {
            $types['*/*'] = [];
        }

        foreach ($types as $type => $params) {
            if (isset(self::$formats[$type])) {
                $response->format = self::$formats[$type];
                $response->acceptMimeType = $type;
                $response->acceptParams = $params;
                return;
            }
        }

        if (isset($types['*/*'])) {
            // return the first format
            foreach (self::$formats as $type => $format) {
                $response->format = self::$formats[$type];
                $response->acceptMimeType = $type;
                $response->acceptParams = [];
                return;
            }
        }

        throw new UnsupportedMediaTypeHttpException('None of your requested content types is supported.');
    }

    /**
     * @param Action $action the action just executed.
     * @param mixed $result the action return result.
     * @return mixed the processed action result.
     */
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);
        if (!Yii::$app->user->isGuest && Yii::$app->user->identity->company !== null) {
            Yii::$app->getResponse()->getHeaders()->set('X-Company-ID', Yii::$app->user->identity->company->id);

            Yii::$app->db->createCommand()->upsert(\common\models\company\CompanyLastVisit::tableName(), [
                'company_id' => Yii::$app->user->identity->company->id,
                'employee_id' => Yii::$app->user->identity->id,
                'ip' => (string) Yii::$app->getRequest()->getUserIP(),
                'time' => time(),
            ], [
                'time' => time(),
            ])->execute();
        }

        return $result;
    }

    /**
     * Document type
     * @return Company
     * @throws NotFoundHttpException
     */
    public function getCompany()
    {
        $company = ArrayHelper::getValue(Yii::$app->user, ['identity', 'company']);
        if ($company === null) {
            throw new NotFoundHttpException("User company not found");
        }

        return $company;
    }

    /**
     * Document type
     * @return integer
     * @throws BadRequestHttpException if parameter "type" is invalid
     */
    public function getIoType()
    {
        $type = Yii::$app->request->get('type');

        if ($type === null) {
            throw new BadRequestHttpException(Yii::t('yii', 'Missing required parameters: {params}', [
                'params' => 'type',
            ]));
        } elseif (!in_array($type, [Documents::IO_TYPE_IN, Documents::IO_TYPE_OUT])) {
            throw new BadRequestHttpException(Yii::t('yii', 'Invalid data received for parameter "{param}".', [
                'param' => 'type',
            ]));
        }

        return $type;
    }

    /**
     * @param string $fromParamName
     * @param string $toParamName
     * @return array
     * @throws BadRequestHttpException if parameter "type" is invalid
     */
    public function getPeriod($fromParamName, $toParamName)
    {
        $valueFrom = Yii::$app->request->get($fromParamName);
        $valueTo = Yii::$app->request->get($toParamName);
        if ($valueFrom === null) {
            throw new BadRequestHttpException(Yii::t('yii', 'Missing required parameters: {params}', [
                'params' => $fromParamName,
            ]));
        }
        if ($valueTo === null) {
            throw new BadRequestHttpException(Yii::t('yii', 'Missing required parameters: {params}', [
                'params' => $toParamName,
            ]));
        }
        $dateFrom = date_create_from_format('Y-m-d H:i:s', $valueFrom . ' 00:00:00');
        $dateTo = date_create_from_format('Y-m-d H:i:s', $valueTo . ' 23:59:59');
        if ($dateFrom === false) {
            throw new BadRequestHttpException(Yii::t('yii', 'Invalid data received for parameter "{param}".', [
                'param' => $fromParamName,
            ]));
        }
        if ($dateTo === false) {
            throw new BadRequestHttpException(Yii::t('yii', 'Invalid data received for parameter "{param}".', [
                'param' => $toParamName,
            ]));
        }
        if ($dateTo < $dateFrom) {
            throw new BadRequestHttpException(Yii::t('yii', '"{param1}" cannot be less than "{param2}".', [
                'param1' => 'periodTo',
                'param2' => $fromParamName,
            ]));
        }

        return [
            'from' => $dateFrom,
            'to' => $dateTo,
        ];
    }

    /**
     * Return required GET parameter value
     * @return mixed
     * @throws BadRequestHttpException if parameter "type" is invalid
     */
    public function getParameter($name, array $valueArray, $defaultValue = null)
    {
        $value = Yii::$app->request->get($name);

        if ($value === null) {
            if ($defaultValue === null) {
                throw new BadRequestHttpException(Yii::t('yii', 'Missing required parameters: {params}', [
                    'params' => $name,
                ]));
            } else {
                return $defaultValue;
            }
        }

        array_walk($valueArray, function (&$item) {
            $item = (string) $item;
        });

        if (!in_array($value, $valueArray, true)) {
            throw new BadRequestHttpException(Yii::t('yii', 'Invalid data received for parameter "{param}".', [
                'param' => $name,
            ]));
        }

        return $value;
    }

    /**
     * Return expand GET parameter items
     * @return array
     */
    public function getExpand()
    {
        $expand = Yii::$app->request->get('expand');

        return is_string($expand) ? preg_split('/\s*,\s*/', $expand, -1, PREG_SPLIT_NO_EMPTY) : [];
    }

    /**
     * @return boolean
     */
    public function getIsBanking()
    {
        return Yii::$app->requestedRoute == 'api/cash/banking/index';
    }

    /**
     * @param integer $id
     * @param integer $type
     * @param boolean $withExpand
     * @return Invoice
     * @throws NotFoundHttpException
     */
    public function findInvoice($id, $type = null, $withExpand = false)
    {
        $model = Invoice::find()->andWhere([
            'id' => $id,
            'company_id' => Yii::$app->user->identity->company->id,
            'is_deleted' => false,
        ])->andFilterWhere([
            'type' => $type,
        ])->one();

        if (isset($model)) {
            if ($withExpand) {
                $expand = $this->getExpand();

                foreach ((array) $expand as $relation) {
                    if ($relation && is_string($relation)) {
                        empty($model->$relation);
                    }
                }
            }

            return $model;
        } else {
            throw new NotFoundHttpException("Invoice not found: $id");
        }
    }

    /**
     * Serializes the validation errors in a model.
     * @param Model $model
     * @return array the array representation of the errors
     */
    public function serializeModelErrors($model)
    {
        Yii::$app->response->setStatusCode(422, 'Data Validation Failed.');
        $result = [];
        foreach ($model->getFirstErrors() as $name => $message) {
            $result[] = [
                'field' => $name,
                'message' => $message,
            ];
        }

        return $result;
    }
}
