<?php

namespace frontend\modules\bitrix24;

use Yii;

/**
 * bitrix24 module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'frontend\modules\bitrix24\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
        Yii::$app->set('bitrix24Helper', [
            'class' => 'frontend\modules\bitrix24\components\Bitrix24Helper',
        ]);
    }
}
