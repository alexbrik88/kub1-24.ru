<?php

namespace frontend\modules\bitrix24\modules\iframe\components\helpers;

use common\models\document\Invoice;
use common\models\document\status\ActStatus;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\InvoiceFactureStatus;
use common\models\document\status\PackingListStatus;
use common\models\document\status\UpdStatus;
use frontend\components\Icon;
use frontend\models\Documents;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class DocStatus
{
    public static $data = [
        Documents::SLUG_INVOICE => [
            'icon' => [
                InvoiceStatus::STATUS_CREATED => 'new-doc',
                InvoiceStatus::STATUS_SEND => 'envelope',
                InvoiceStatus::STATUS_PAYED => 'check-2',
                InvoiceStatus::STATUS_VIEWED => 'check-2',
                InvoiceStatus::STATUS_APPROVED => 'check-double',
                InvoiceStatus::STATUS_PAYED_PARTIAL => 'check-2',
                InvoiceStatus::STATUS_OVERDUE => 'calendar',
                InvoiceStatus::STATUS_REJECTED => 'stop',
            ],
            'back' => [
                InvoiceStatus::STATUS_CREATED => '#4679ae',
                InvoiceStatus::STATUS_SEND => '#fac031',
                InvoiceStatus::STATUS_PAYED => '#26cd58',
                InvoiceStatus::STATUS_VIEWED => '#fac031',
                InvoiceStatus::STATUS_APPROVED => '#fac031',
                InvoiceStatus::STATUS_PAYED_PARTIAL => '#26cd58',
                InvoiceStatus::STATUS_OVERDUE => '#e30611',
                InvoiceStatus::STATUS_REJECTED => '#f2f3f7',
            ],
            'border' => [
                InvoiceStatus::STATUS_CREATED => '#4679ae',
                InvoiceStatus::STATUS_SEND => '#fac031',
                InvoiceStatus::STATUS_PAYED => '#26cd58',
                InvoiceStatus::STATUS_VIEWED => '#fac031',
                InvoiceStatus::STATUS_APPROVED => '#fac031',
                InvoiceStatus::STATUS_PAYED_PARTIAL => '#26cd58',
                InvoiceStatus::STATUS_OVERDUE => '#e30611',
                InvoiceStatus::STATUS_REJECTED => '#e2e5eb',
            ],
            'color' => [
                InvoiceStatus::STATUS_CREATED => '#ffffff',
                InvoiceStatus::STATUS_SEND => '#ffffff',
                InvoiceStatus::STATUS_PAYED => '#ffffff',
                InvoiceStatus::STATUS_VIEWED => '#ffffff',
                InvoiceStatus::STATUS_APPROVED => '#ffffff',
                InvoiceStatus::STATUS_PAYED_PARTIAL => '#ffffff',
                InvoiceStatus::STATUS_OVERDUE => '#ffffff',
                InvoiceStatus::STATUS_REJECTED => '#001424',
            ],
        ],
        Documents::SLUG_ACT => [
            'icon' => [
                ActStatus::STATUS_CREATED => 'check-2',
                ActStatus::STATUS_PRINTED => 'check-2',
                ActStatus::STATUS_SEND => 'envelope',
                ActStatus::STATUS_RECEIVED => 'check-double',
                ActStatus::STATUS_REJECTED => 'check-2',
            ],
            'back' => [
                ActStatus::STATUS_CREATED => '#26cd58',
                ActStatus::STATUS_PRINTED => '#26cd58',
                ActStatus::STATUS_SEND => '#fac031',
                ActStatus::STATUS_RECEIVED => '#fac031',
                ActStatus::STATUS_REJECTED => '#4679ae',
            ],
            'border' => [
                ActStatus::STATUS_CREATED => '#26cd58',
                ActStatus::STATUS_PRINTED => '#26cd58',
                ActStatus::STATUS_SEND => '#fac031',
                ActStatus::STATUS_RECEIVED => '#fac031',
                ActStatus::STATUS_REJECTED => '#4679ae',
            ],
            'color' => [
                ActStatus::STATUS_CREATED => '#ffffff',
                ActStatus::STATUS_PRINTED => '#ffffff',
                ActStatus::STATUS_SEND => '#ffffff',
                ActStatus::STATUS_RECEIVED => '#ffffff',
                ActStatus::STATUS_REJECTED => '#ffffff',
            ],
        ],
        Documents::SLUG_PACKING_LIST => [
            'icon' => [
                PackingListStatus::STATUS_CREATED => 'check-2',
                PackingListStatus::STATUS_PRINTED => 'check-2',
                PackingListStatus::STATUS_SEND => 'envelope',
                PackingListStatus::STATUS_RECEIVED => 'check-double',
                PackingListStatus::STATUS_REJECTED => 'check-2',
            ],
            'back' => [
                PackingListStatus::STATUS_CREATED => '#26cd58',
                PackingListStatus::STATUS_PRINTED => '#fac031',
                PackingListStatus::STATUS_SEND => '#fac031',
                PackingListStatus::STATUS_RECEIVED => '#26cd58',
                PackingListStatus::STATUS_REJECTED => '#4679ae',
            ],
            'border' => [
                PackingListStatus::STATUS_CREATED => '#26cd58',
                PackingListStatus::STATUS_PRINTED => '#fac031',
                PackingListStatus::STATUS_SEND => '#fac031',
                PackingListStatus::STATUS_RECEIVED => '#26cd58',
                PackingListStatus::STATUS_REJECTED => '#4679ae',
            ],
            'color' => [
                PackingListStatus::STATUS_CREATED => '#ffffff',
                PackingListStatus::STATUS_PRINTED => '#ffffff',
                PackingListStatus::STATUS_SEND => '#ffffff',
                PackingListStatus::STATUS_RECEIVED => '#ffffff',
                PackingListStatus::STATUS_REJECTED => '#ffffff',
            ],
        ],
        Documents::SLUG_INVOICE_FACTURE => [
            'icon' => [
                InvoiceFactureStatus::STATUS_CREATED => 'check-2',
                InvoiceFactureStatus::STATUS_PRINTED => 'check-2',
                InvoiceFactureStatus::STATUS_DELIVERED => 'check-double',
                InvoiceFactureStatus::STATUS_REJECTED => 'check-2',
            ],
            'back' => [
                InvoiceFactureStatus::STATUS_CREATED => '#26cd58',
                InvoiceFactureStatus::STATUS_PRINTED => '#26cd58',
                InvoiceFactureStatus::STATUS_DELIVERED => '#fac031',
                InvoiceFactureStatus::STATUS_REJECTED => '#4679ae',
            ],
            'border' => [
                InvoiceFactureStatus::STATUS_CREATED => '#26cd58',
                InvoiceFactureStatus::STATUS_PRINTED => '#26cd58',
                InvoiceFactureStatus::STATUS_DELIVERED => '#fac031',
                InvoiceFactureStatus::STATUS_REJECTED => '#4679ae',
            ],
            'color' => [
                InvoiceFactureStatus::STATUS_CREATED => '#ffffff',
                InvoiceFactureStatus::STATUS_PRINTED => '#ffffff',
                InvoiceFactureStatus::STATUS_DELIVERED => '#ffffff',
                InvoiceFactureStatus::STATUS_REJECTED => '#ffffff',
            ],
        ],
        Documents::SLUG_UPD => [
            'icon' => [
                UpdStatus::STATUS_CREATED => 'check-2',
                UpdStatus::STATUS_PRINTED => 'print',
                UpdStatus::STATUS_SEND => 'check-2',
                UpdStatus::STATUS_RECEIVED => 'check-double',
                UpdStatus::STATUS_REJECTED => 'check-2',
            ],
            'back' => [
                UpdStatus::STATUS_CREATED => '#26cd58',
                UpdStatus::STATUS_PRINTED => '#fac031',
                UpdStatus::STATUS_SEND => '#fac031',
                UpdStatus::STATUS_RECEIVED => '#26cd58',
                UpdStatus::STATUS_REJECTED => '#4679ae',
            ],
            'border' => [
                UpdStatus::STATUS_CREATED => '#26cd58',
                UpdStatus::STATUS_PRINTED => '#fac031',
                UpdStatus::STATUS_SEND => '#fac031',
                UpdStatus::STATUS_RECEIVED => '#26cd58',
                UpdStatus::STATUS_REJECTED => '#4679ae',
            ],
            'color' => [
                UpdStatus::STATUS_CREATED => '#ffffff',
                UpdStatus::STATUS_PRINTED => '#ffffff',
                UpdStatus::STATUS_SEND => '#ffffff',
                UpdStatus::STATUS_RECEIVED => '#ffffff',
                UpdStatus::STATUS_REJECTED => '#ffffff',
            ],
        ],
    ];

    public static function btn ($slug, $status, $default = null)
    {
        return self::$data[$slug][$status] ?? $default;
    }

    public static function icon ($slug, $status, $default = null)
    {
        return self::$data[$slug]['icon'][$status] ?? $default;
    }

    public static function back ($slug, $status, $default = null)
    {
        return self::$data[$slug]['back'][$status] ?? $default;
    }

    public static function border ($slug, $status, $default = null)
    {
        return self::$data[$slug]['border'][$status] ?? $default;
    }

    public static function color ($slug, $status, $default = null)
    {
        return self::$data[$slug]['color'][$status] ?? $default;
    }

    public static function tag ($model, $options = [])
    {
        $tag = ArrayHelper::remove($options, 'tag', 'span');
        $doc = Documents::getDocumentTypeByInstance($model);
        $slug = Documents::$typeToSlug[$doc] ?? '';
        $status = ($model instanceof Invoice) ? $model->invoiceStatus : $model->statusOut;
        $statusId = $status ? $status->id : null;
        $statusIcon = Icon::get(self::icon($slug, $statusId, 'new-doc'));
        $statusName = $status ? $status->name : '---';
        $options['style'] = [
            'color' => self::color($slug, $statusId, 'black'),
            'background-color' => self::back($slug, $statusId, 'white'),
            'border-color' => self::border($slug, $statusId, 'gray'),
        ];

        return Html::tag($tag, $statusIcon.' '.$statusName, $options);
    }
}
