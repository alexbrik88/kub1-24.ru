<?php

namespace frontend\modules\bitrix24\modules\iframe\widgets;

use Yii;
use frontend\modules\bitrix24\models\WidgetUser;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class CompanySelectWidget
 */
class CompanySelectWidget extends \yii\base\Widget
{
    public WidgetUser $widgetUser;
    public bool $disabled = false;

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $token = $this->widgetUser->token;
        $companyArray = $this->widgetUser->companies;
        ArrayHelper::multisort($companyArray, ['shortTitle']);

        $data = [];
        $options = [];
        foreach ($companyArray as $company) {
            $data[$company->id] = $company->shortTitle;
            $options[$company->id] = [
                'data-url' => Url::current(['access-token' => $this->widgetUser->tokenByCompany($company->id)]),
            ];
        }

        $this->registerJs();

        return Select2::widget([
            'name' => 'CID',
            'data' => $data,
            'options' => [
                'class' => ['class' => 'company-select-dropdown'],
                'placeholder' => '',
                'options' => $options,
                'disabled' => $this->disabled,
            ],
            'value' => $this->widgetUser->employee->company->id,
            'pluginOptions' => [
                'minimumResultsForSearch' => -1,
                'width' => '100%'
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function registerJs()
    {
        $script = <<<JS
            $(document).on('change', 'select.company-select-dropdown', function () {
                var val = this.value;
                if (val) {
                    var url = $(this).find(':selected').data('url') || null;
                    if (url) {
                        $(this).val(null).trigger('change');
                        window.location = url;
                    }
                }
            });
        JS;

        $this->view->registerJs($script);
    }
}
