<?php

namespace frontend\modules\bitrix24\modules\iframe\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\bitrix24\models\WidgetUser;
use common\models\bitrix24\AmocrmWidgetInvoice;
use common\models\document\Invoice;

/**
 * InvoiceSearch represents the model behind the search form of `common\models\document\Invoice`.
 */
class InvoiceSearch extends Invoice
{
    private WidgetUser $widgetUser;
    public $placement = [];

    /**
     * {@inheritdoc}
     */
    public function __construct(WidgetUser $widgetUser, $config = [])
    {
        $this->widgetUser = $widgetUser;

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'type',
                    'invoice_status_id',
                ],
                'safe',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Invoice::find()->leftJoin([
            'link' => '{{%bitrix24_widget_invoice}}',
        ], '{{invoice}}.[[id]]={{link}}.[[invoice_id]]');

        $query->andWhere([
            'invoice.company_id' => $this->widgetUser->companyIds,
            'invoice.is_deleted' => false,
            'link.account_id' => $this->widgetUser->client_id,
        ]);

        foreach ($this->placement as $key => $value) {
            $query->andFilterWhere([
                'link.'.$key => $value,
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'document_date' => SORT_DESC,
                    'document_number' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'type' => $this->type,
            'invoice_status_id' => $this->invoice_status_id,
        ]);

        $query->andFilterWhere(['like', 'uid', $this->uid])
            ->andFilterWhere(['like', 'document_number', $this->document_number]);

        return $dataProvider;
    }
}
