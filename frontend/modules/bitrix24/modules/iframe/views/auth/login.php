<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\modules\bitrix24\models\LoginForm */

?>

<?php $form = ActiveForm::begin([
    'id' => 'install-form',
    'fieldConfig' => Yii::$app->params['bitrix24']['fieldConfig'],
    'enableClientValidation' => false,
    'enableAjaxValidation' => false,
]); ?>

    <div class="form-body">
        <?= $this->render('login/_'.$model->scenario, [
            'model' => $model,
            'form' => $form,
        ]); ?>

        <div class="mt-3 d-flex justify-content-between">
            <?= Html::submitButton('Войти', [
                'id' => 'product-form-submit',
                'class' => 'btn btn-sm btn-kub-primary ladda-button',
            ]) ?>
        </div>
    </div>

<?php ActiveForm::end() ?>
