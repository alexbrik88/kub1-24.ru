<?php

use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model frontend\modules\bitrix24\models\LoginForm */

$data = ['' => '']+$model->getDropdownItems();
?>

<?= $form->field($model, 'username')->hiddenInput()->label(false) ?>

<?= $form->field($model, 'password')->hiddenInput()->label(false) ?>

<?= $form->field($model, 'company_id')->checkboxList($model->getDropdownItems()) ?>