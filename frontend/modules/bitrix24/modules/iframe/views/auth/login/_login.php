<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model frontend\modules\bitrix24\models\LoginForm */

?>

<?= $form->field($model, 'username')->textInput() ?>

<?= $form->field($model, 'password')->passwordInput() ?>
