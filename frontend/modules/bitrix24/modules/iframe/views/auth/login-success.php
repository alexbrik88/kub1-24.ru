<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\modules\bitrix24\models\LoginForm */

$this->registerJs('AMO.loginSuccess();');
?>
