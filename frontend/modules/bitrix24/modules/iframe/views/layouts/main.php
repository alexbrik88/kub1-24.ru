<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\components\Alert;
use frontend\components\Icon;
use yii\helpers\Html;
use yii\widgets\Pjax;

frontend\modules\bitrix24\modules\iframe\assets\IframeAsset::register($this);
frontend\modules\bitrix24\modules\iframe\assets\CustomAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?= frontend\modules\bitrix24\modules\iframe\widgets\Alert::widget() ?>

<?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
