<?php

use common\components\TextHelper;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\Store;
use frontend\models\Documents;
use frontend\modules\bitrix24\modules\iframe\components\grid\GridView;
use frontend\modules\bitrix24\modules\iframe\helpers\Url;
use yii\bootstrap4\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

$productIdArray = Yii::$app->request->post('product', []);
?>

<?php Pjax::begin([
    'id' => 'pjax-product-grid',
    'linkSelector' => false,
    'formSelector' => '#products_in_order',
    'timeout' => 5000,
    'enablePushState' => false,
    'enableReplaceState' => false,
]); ?>

    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>

    <?php if ($productType == 1 ) : ?>
        <?php $emptyText = 'Все товары уже добавлены'; ?>
        <h4 class="modal-title">Выбрать товар из списка</h4>
    <?php else : ?>
        <?php $emptyText = 'Все услуги уже добавлены'; ?>
        <h4 class="modal-title">Выбрать услугу из списка</h4>
    <?php endif; ?>

    <?= Html::beginForm('', 'get', [
        'id' => 'products_in_order',
        'class' => 'add-to-invoice',
        'data' => [
            'pjax' => true,
        ]
    ]); ?>

    <?= Html::hiddenInput('documentType', $documentType, [
        'id' => 'documentTypeHidden',
    ]); ?>

    <?=  Html::hiddenInput('productType', $productType, [
        'id' => 'productTypeHidden',
    ]); ?>

    <?=  Html::hiddenInput('searchTitle', $searchModel->title, [
        'id' => 'searchTitleHidden',
    ]); ?>

    <?=  Html::hiddenInput('store_id', $storeId, [
        'id' => 'storeIdHidden',
    ]); ?>

    <?php foreach ((array) $searchModel->exclude as $value) {
        echo Html::hiddenInput('exists[]', $value);
    } ?>

    <?php if ($productType == 1) {
        /* @var $user \common\models\employee\Employee */
        $user = Yii::$app->user->identity;

        $storeSelectList = $user->getStores()
            ->orderBy(['is_main' => SORT_DESC])
            ->indexBy('id')
            ->all();

        $selectedStoreId = $storeId;

        if ($selectedStoreId && ($selectedStore = Store::findOne($selectedStoreId))) {
            $currentStoreId = $selectedStore->id;
            $currentStoreName = $selectedStore->name;
        } elseif ($storeSelectList) {
            $currentStore = reset($storeSelectList);
            $currentStoreId = $currentStore->id;
            $currentStoreName = $currentStore->name;
        } else {
            $currentStoreId = null;
            $currentStoreName = "НЕТ СКЛАДА";
        }
    } ?>

    <div class="d-flex flex-nowrap mt-3">
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::textInput('title', $searchModel->title, [
                'type' => 'search',
                'placeholder' => 'Поиск...',
                'class' => 'form-control form-control-sm',
                'id' => 'product-title-search',
                'autocomplete' => 'off'
            ]) ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'btn btn-sm btn-kub-primary',
            ]); ?>
        </div>
    </div>


    <div class="add-to-invoice-table" id="add-to-invoice-tbody">
        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <?= GridView::widget([
                    'id' => 'product_select_grid',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'options' => [
                        //'class' => 'table-responsive',
                    ],
                    'tableOptions' => [
                        'class' => 'table table-bordered',
                        'id' => 'product_select_table',
                        'role' => 'grid',
                    ],
                    'rowOptions' => [
                        'role' => 'row',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    'filterRowOptions' => [
                        'class' => 'hidden',
                    ],
                    'pager' => [
                        'class' => 'yii\bootstrap4\LinkPager',
                    ],
                    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                    'layout' => "{items}\n{pager}",
                    'emptyText' => $searchModel->exclude ? $emptyText : 'Ничего не найдено.',
                    'columns' => [
                        [
                            'format' => 'raw',
                            'value' => function (Product $model) use ($productIdArray) {
                                return Html::checkbox('in_order[]', in_array($model->id, $productIdArray), [
                                    'id' => 'product-' . $model->id,
                                    'value' => $model->id,
                                    'class' =>'product_selected',
                                ]);
                            },
                            'headerOptions' => [
                                'width' => '3%'
                            ],
                            'header' => Html::checkbox('in_order_all', false, [
                                'id' => 'product_selected-all',
                            ]),
                        ],
                        [
                            'attribute' => 'title',
                            'label' => 'Продукция',
                            'headerOptions' => [
                                'width' => '65%'
                            ],
                            'format' => 'text',
                            'value' => function (Product $model) {
                                return $model->title_chunked;
                            },
                        ],
                        [
                            'attribute' => 'article',
                            'headerOptions' => [
                                'width' => '20%'
                            ],
                            'format' => 'text',
                            'visible' => $productType == Product::PRODUCTION_TYPE_GOODS,
                        ],
                        //Группа
                        [
                            'attribute' => 'group_id',
                            'label' => 'Группа',
                            'headerOptions' => [
                                'width' => '20%'
                            ],
                            'filter' => [null => 'Все'] + ArrayHelper::map(ProductGroup::getGroups(), 'id', 'title'),
                            'format' => 'raw',
                            'value' => 'group.title',
                            'visible' => $productType == Product::PRODUCTION_TYPE_GOODS,
                            's2width' => '200px'
                        ],
                        [
                            'attribute' => 'price_for_sell_with_nds',
                            'label' => 'Цена продажи',
                            'format' => 'raw',
                            'headerOptions' => [
                                'width' => '15%'
                            ],
                            'value' => function (Product $model) {
                                return TextHelper::invoiceMoneyFormat($model->price_for_sell_with_nds);
                            },
                            'visible' => $documentType == Documents::IO_TYPE_OUT,
                        ],
                        [
                            'attribute' => 'price_for_buy_with_nds',
                            'label' => 'Цена покупки',
                            'format' => 'raw',
                            'headerOptions' => [
                                'width' => '15%'
                            ],
                            'value' => function (Product $model) {
                                return TextHelper::invoiceMoneyFormat($model->price_for_buy_with_nds);
                            },
                            'visible' => $documentType == Documents::IO_TYPE_IN,
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>

    <?= Html::endForm(); ?>

    <div class="d-flex justify-content-between mt-2">
        <?= Html::button('Добавить отмеченные', [
            'class' => 'btn btn-sm btn-kub-primary ladda-button',
            'id' => 'add-to-invoice-button',
            'data-type' => $documentType,
            'data-url' => Url::to(['get-products']),
        ]); ?>
        <?= Html::submitButton('Отменить', [
            'class' => 'btn btn-sm btn-kub-secondary',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>

<script type="text/javascript">
$(document).on("change", "#product_selected-all", function() {
    $("input.product_selected").prop("checked", $(this).prop("checked")).trigger("change").uniform();
});
$(document).ready(function () {
    // init dropdown
    $('#storeDropdown').click();

    $('#datatable_ajax tbody td:first-child, #datatable_ajax thead th:first-child').on('click', function(e) {

        if($(e.target).attr('type') === 'checkbox')
            return;

        let $checkbox = $(this).find('[type="checkbox"]');
        if ($checkbox && $checkbox.prop('checked')) {
            $checkbox.prop('checked', false).trigger('change').uniform('refresh');
        } else if ($checkbox && !$checkbox.prop('checked')) {
            $checkbox.prop('checked', true).trigger('change').uniform('refresh');
        }

    });

});
</script>

<?php Pjax::end(); ?>