<?php

use frontend\modules\bitrix24\modules\iframe\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */

$this->title = $model->id;

$uid = $model->uid;
$viewUrl = Url::to([
    '/documents/invoice/out-view',
    'uid' => $uid,
], true);
$pdfUrl = Url::to([
    '/documents/invoice/download',
    'type' => 'pdf',
    'uid' => $uid,
], true);
?>
<div class="invoice-view" style="width: 710px; margin: 0 0 auto; padding: 0 16px;">
    <?= $this->render('@frontend/modules/documents/views/invoice/template', [
        'model' => $model,
        'addStamp' => true,
        'showHeader' => true,
    ]); ?>
    <div class="mt-3 d-flex">
        <?= Html::button('PDF', [
            'title' => 'PDF',
            'class' => 'bitrix24_action btn btn-sm btn-kub-primary mr-2 w-120p',
            'data' => [
                'action-data' => [
                    'action' => 'getBrTab',
                    'data' => ['url' => $pdfUrl],
                ],
            ],
        ]) ?>
        <?= Html::button('Ссылка', [
            'id' => md5($viewUrl),
            'title' => 'Скопировать ссылку на счет в буфер обмена',
            'class' => 'btn btn-sm btn-kub-primary copy_to_clipboard w-120p',
            'data-text' => $viewUrl,
        ]) ?>
    </div>
</div>
