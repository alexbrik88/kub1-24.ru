<?php

use common\models\Contractor;
use common\models\cash\CashBankFlows;
use common\models\company\CheckingAccountant;
use frontend\components\Icon;
use frontend\models\Documents;
use frontend\modules\bitrix24\modules\iframe\helpers\Url;
use frontend\widgets\ContractorDropdown;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */

frontend\modules\bitrix24\modules\iframe\assets\InvoiceFormAsset::register($this);

$rsData = $company->getRubleAccounts()
    ->select(['name', 'rs'])
    ->andWhere(['not', ['type' => CheckingAccountant::TYPE_CLOSED]])
    ->orderBy(['type' => SORT_ASC])
    ->indexBy('rs')
    ->column();

$headerInputConfig = [
    'options' => [
        'class' => 'ml-2',
    ],
];
$datepickerConfig = Yii::$app->params['bitrix24']['datepickerConfig'];
if ($model->document_date) {
    $model->document_date = date_format(date_create($model->document_date), 'd.m.Y');
}
if ($model->payment_limit_date) {
    $model->payment_limit_date = date_format(date_create($model->payment_limit_date), 'd.m.Y');
}
?>

<div class="invoice-form">

    <?php $form = ActiveForm::begin([
        'id' => 'invoice-form',
        'enableClientValidation' => false,
        'options' => [
            'data' => [
                'iotype' => $ioType,
            ]
        ],
        'fieldConfig' => Yii::$app->params['bitrix24']['fieldConfig'],
    ]); ?>

    <div class="form-group">
        <div class="d-flex align-items-start">
            <div class="text-nowrap font-weight-bold py-2">
                Исходящий счет №
            </div>
            <?= $form->field($model, 'document_number', $headerInputConfig)
                ->label(false)
                ->hint(false)
                ->textInput(['maxlength' => true, 'size' => 10]) ?>

            <?= $form->field($model, 'document_additional_number', $headerInputConfig)
                ->label(false)
                ->hint(false)
                ->textInput([
                    'maxlength' => true,
                    'size' => 10,
                    'placeholder' => 'доп. номер',
                ]) ?>

            <div class="ml-2 py-2">
                от
            </div>
            <div>
                <?= $form->field($model, 'document_date', $headerInputConfig)
                    ->label(false)
                    ->hint(false)
                    ->error(false)
                    ->widget(DatePicker::class, $datepickerConfig) ?>
                <?= $form->field($model, 'document_date', ['template' => "{error}"]); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-4 col-lg-5 col-md-6">
            <?= $form->field($model, 'company_rs')->widget(Select2::class, [
                'size' => Select2::SMALL,
                'data' => $rsData,
                'hideSearch' => true,
                'pluginOptions' => [
                    'width' => '100%'
                ],
                'options' => [
                    'class' => 'form-control form-control-sm',
                ]
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-4 col-lg-5 col-md-6">
            <?= $form->field($model, 'contractor_id')->label('Покупатель')->widget(ContractorDropdown::class, [
                'size' => Select2::SMALL,
                'company' => $model->company,
                'contractorType' => Contractor::TYPE_CUSTOMER,
                'dataUrl' => ['contractor/dropdown'],
                'staticData' => [
                    "add-modal-contractor" => Icon::get('add-icon') . ' Добавить покупателя',
                ],
                'addRequestParams' => [
                    'access-token' => Yii::$app->request->get('access-token'),
                ],
                'options' => [
                    'placeholder' => '',
                    'data-url' => Url::to(['contractor/create-modal']),
                    'disabled' => !$model->getIsContractorEditable(),
                ],
            ]); ?>
        </div>
    </div>

    <div class="d-flex">
        <?= $form->field($model, 'payment_limit_date')->widget(DatePicker::class, array_merge_recursive($datepickerConfig, [
            'options' => [
                'data-delay' => $model->contractor ? (
                    $model->type == Documents::IO_TYPE_IN ?
                    $model->contractor->seller_payment_delay :
                    $model->contractor->customer_payment_delay
                ) : 10,
            ],
        ])) ?>

        <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
            <div>
                <label class="label">&nbsp;</label>
                <div class="d-flex align-items-center">
                    <div class="form-control form-control-sm" style="border-color: transparent; width: auto;">
                        <?= Html::activeCheckbox($model, 'remind_contractor') ?>
                    </div>
                    <div>
                        <?= Icon::get('question', [
                            'class' => 'tooltip-question-icon js-tooltip',
                            'title' => 'Если счет не будет оплачен, то<br> будет автоматически отправлено <br>письмо вашему покупателю с <br>напоминанием об оплате счета',
                            'data' => [
                                'trigger' => 'hover',
                                'position' => 'bottom',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>

    <?= $this->render('_product_table_invoice', [
        'model' => $model,
        'ioType' => $ioType,
        'company' => $model->company,
        'invoiceContractEssence' => $invoiceContractEssence,
        'document' => null,
        'projectEstimateId' => null,
    ]); ?>

    <?= $form->field($model, 'comment')->textArea()->label('Комментарий в счете для покупателя '.Icon::get('question', [
        'class' => 'tooltip-question-icon js-tooltip',
        'title' => 'Добавьте к счету комментарий для покупателя. <br>Покупатель получит дополнительную информацию <br>о деталях оплаты, условиях договора и тд.',
        'data' => [
            'trigger' => 'hover',
            'position' => 'top',
        ],
    ])) ?>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-sm btn-kub-primary']) ?>
        <?= Html::tag('span', 'Отменить', [
            'class' => 'btn btn-sm btn-kub-secondary bitrix24_action',
            'data' => [
                'action-data' => [
                    'action' => 'closePopup',
                    'data' => [],
                ],
            ],
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php yii\bootstrap4\Modal::begin([
    'id' => 'add-new',
    'title' => '',
    'size' => yii\bootstrap4\Modal::SIZE_EXTRA_LARGE,
]) ?>
<?php yii\bootstrap4\Modal::end() ?>

<?php yii\bootstrap4\Modal::begin([
    'id' => 'add-from-exists',
    'closeButton' => false,
    'size' => yii\bootstrap4\Modal::SIZE_EXTRA_LARGE,
]) ?>
    <?php Pjax::begin([
        'id' => 'pjax-product-grid',
        'linkSelector' => false,
        'formSelector' => '#products_in_order',
        'timeout' => 5000,
        'enablePushState' => false,
        'enableReplaceState' => false,
    ]); ?>

    <?php Pjax::end(); ?>
<?php yii\bootstrap4\Modal::end() ?>
