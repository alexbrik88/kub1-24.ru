<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\bitrix24\modules\widget\models\InvoiceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="invoice-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'uid') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'invoice_status_id') ?>

    <?= $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'invoice_status_updated_at') ?>

    <?php // echo $form->field($model, 'invoice_status_author_id') ?>

    <?php // echo $form->field($model, 'document_author_id') ?>

    <?php // echo $form->field($model, 'document_date') ?>

    <?php // echo $form->field($model, 'document_date_i') ?>

    <?php // echo $form->field($model, 'document_number') ?>

    <?php // echo $form->field($model, 'document_additional_number') ?>

    <?php // echo $form->field($model, 'payment_limit_date') ?>

    <?php // echo $form->field($model, 'production_type') ?>

    <?php // echo $form->field($model, 'invoice_expenditure_item_id') ?>

    <?php // echo $form->field($model, 'company_id') ?>

    <?php // echo $form->field($model, 'company_name_full') ?>

    <?php // echo $form->field($model, 'company_name_short') ?>

    <?php // echo $form->field($model, 'company_bank_name') ?>

    <?php // echo $form->field($model, 'company_bank_city') ?>

    <?php // echo $form->field($model, 'company_inn') ?>

    <?php // echo $form->field($model, 'company_kpp') ?>

    <?php // echo $form->field($model, 'company_egrip') ?>

    <?php // echo $form->field($model, 'company_okpo') ?>

    <?php // echo $form->field($model, 'company_bik') ?>

    <?php // echo $form->field($model, 'company_ks') ?>

    <?php // echo $form->field($model, 'company_rs') ?>

    <?php // echo $form->field($model, 'company_phone') ?>

    <?php // echo $form->field($model, 'company_address_legal_full') ?>

    <?php // echo $form->field($model, 'company_chief_post_name') ?>

    <?php // echo $form->field($model, 'company_chief_lastname') ?>

    <?php // echo $form->field($model, 'company_chief_firstname_initials') ?>

    <?php // echo $form->field($model, 'company_chief_patronymic_initials') ?>

    <?php // echo $form->field($model, 'company_print_filename') ?>

    <?php // echo $form->field($model, 'company_chief_signature_filename') ?>

    <?php // echo $form->field($model, 'company_chief_accountant_lastname') ?>

    <?php // echo $form->field($model, 'company_chief_accountant_firstname_initials') ?>

    <?php // echo $form->field($model, 'company_chief_accountant_patronymic_initials') ?>

    <?php // echo $form->field($model, 'contractor_id') ?>

    <?php // echo $form->field($model, 'contractor_name_full') ?>

    <?php // echo $form->field($model, 'contractor_name_short') ?>

    <?php // echo $form->field($model, 'contractor_director_name') ?>

    <?php // echo $form->field($model, 'contractor_director_post_name') ?>

    <?php // echo $form->field($model, 'contractor_bank_name') ?>

    <?php // echo $form->field($model, 'contractor_bank_city') ?>

    <?php // echo $form->field($model, 'contractor_address_legal_full') ?>

    <?php // echo $form->field($model, 'contractor_inn') ?>

    <?php // echo $form->field($model, 'contractor_kpp') ?>

    <?php // echo $form->field($model, 'contractor_bik') ?>

    <?php // echo $form->field($model, 'contractor_ks') ?>

    <?php // echo $form->field($model, 'contractor_rs') ?>

    <?php // echo $form->field($model, 'contractor_passport') ?>

    <?php // echo $form->field($model, 'remind_contractor') ?>

    <?php // echo $form->field($model, 'total_place_count') ?>

    <?php // echo $form->field($model, 'total_mass_gross') ?>

    <?php // echo $form->field($model, 'total_amount_no_nds') ?>

    <?php // echo $form->field($model, 'total_amount_with_nds') ?>

    <?php // echo $form->field($model, 'total_amount_has_nds') ?>

    <?php // echo $form->field($model, 'total_amount_nds') ?>

    <?php // echo $form->field($model, 'total_order_count') ?>

    <?php // echo $form->field($model, 'view_total_base') ?>

    <?php // echo $form->field($model, 'view_total_discount') ?>

    <?php // echo $form->field($model, 'view_total_amount') ?>

    <?php // echo $form->field($model, 'view_total_no_nds') ?>

    <?php // echo $form->field($model, 'view_total_nds') ?>

    <?php // echo $form->field($model, 'view_total_with_nds') ?>

    <?php // echo $form->field($model, 'has_discount') ?>

    <?php // echo $form->field($model, 'has_markup') ?>

    <?php // echo $form->field($model, 'has_weight') ?>

    <?php // echo $form->field($model, 'has_volume') ?>

    <?php // echo $form->field($model, 'payment_form_id') ?>

    <?php // echo $form->field($model, 'payment_partial_amount') ?>

    <?php // echo $form->field($model, 'is_deleted') ?>

    <?php // echo $form->field($model, 'is_subscribe_invoice') ?>

    <?php // echo $form->field($model, 'subscribe_id') ?>

    <?php // echo $form->field($model, 'service_payment_id') ?>

    <?php // echo $form->field($model, 'email_messages') ?>

    <?php // echo $form->field($model, 'object_guid') ?>

    <?php // echo $form->field($model, 'company_checking_accountant_id') ?>

    <?php // echo $form->field($model, 'nds_view_type_id') ?>

    <?php // echo $form->field($model, 'signed_by_employee_id') ?>

    <?php // echo $form->field($model, 'signed_by_name') ?>

    <?php // echo $form->field($model, 'sign_document_type_id') ?>

    <?php // echo $form->field($model, 'sign_document_number') ?>

    <?php // echo $form->field($model, 'sign_document_date') ?>

    <?php // echo $form->field($model, 'signature_id') ?>

    <?php // echo $form->field($model, 'print_id') ?>

    <?php // echo $form->field($model, 'chief_signature_id') ?>

    <?php // echo $form->field($model, 'chief_accountant_signature_id') ?>

    <?php // echo $form->field($model, 'autoinvoice_id') ?>

    <?php // echo $form->field($model, 'show_popup') ?>

    <?php // echo $form->field($model, 'basis_document_name') ?>

    <?php // echo $form->field($model, 'basis_document_number') ?>

    <?php // echo $form->field($model, 'basis_document_date') ?>

    <?php // echo $form->field($model, 'basis_document_type_id') ?>

    <?php // echo $form->field($model, 'remaining_amount') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <?php // echo $form->field($model, 'comment_internal') ?>

    <?php // echo $form->field($model, 'is_by_out_link') ?>

    <?php // echo $form->field($model, 'by_out_link_id') ?>

    <?php // echo $form->field($model, 'by_donate_link_id') ?>

    <?php // echo $form->field($model, 'is_additional_number_before') ?>

    <?php // echo $form->field($model, 'price_precision') ?>

    <?php // echo $form->field($model, 'show_article') ?>

    <?php // echo $form->field($model, 'from_store_cabinet') ?>

    <?php // echo $form->field($model, 'from_out_invoice') ?>

    <?php // echo $form->field($model, 'from_demo_out_invoice') ?>

    <?php // echo $form->field($model, 'currency_name') ?>

    <?php // echo $form->field($model, 'currency_amount') ?>

    <?php // echo $form->field($model, 'currency_rate') ?>

    <?php // echo $form->field($model, 'currency_rate_type') ?>

    <?php // echo $form->field($model, 'currency_rate_date') ?>

    <?php // echo $form->field($model, 'currency_rate_amount') ?>

    <?php // echo $form->field($model, 'currency_rate_value') ?>

    <?php // echo $form->field($model, 'show_paylimit_info') ?>

    <?php // echo $form->field($model, 'out_invoice_id') ?>

    <?php // echo $form->field($model, 'is_invoice_contract') ?>

    <?php // echo $form->field($model, 'contract_essence') ?>

    <?php // echo $form->field($model, 'contract_essence_template') ?>

    <?php // echo $form->field($model, 'has_services') ?>

    <?php // echo $form->field($model, 'has_goods') ?>

    <?php // echo $form->field($model, 'has_act') ?>

    <?php // echo $form->field($model, 'has_packing_list') ?>

    <?php // echo $form->field($model, 'has_sales_invoice') ?>

    <?php // echo $form->field($model, 'has_proxy') ?>

    <?php // echo $form->field($model, 'has_waybill') ?>

    <?php // echo $form->field($model, 'has_invoice_facture') ?>

    <?php // echo $form->field($model, 'has_upd') ?>

    <?php // echo $form->field($model, 'has_file') ?>

    <?php // echo $form->field($model, 'can_add_act') ?>

    <?php // echo $form->field($model, 'can_add_packing_list') ?>

    <?php // echo $form->field($model, 'can_add_proxy') ?>

    <?php // echo $form->field($model, 'can_add_waybill') ?>

    <?php // echo $form->field($model, 'can_add_invoice_facture') ?>

    <?php // echo $form->field($model, 'can_add_upd') ?>

    <?php // echo $form->field($model, 'can_add_sales_invoice') ?>

    <?php // echo $form->field($model, 'out_view_count') ?>

    <?php // echo $form->field($model, 'out_save_count') ?>

    <?php // echo $form->field($model, 'out_pay_count') ?>

    <?php // echo $form->field($model, 'agreement_new_id') ?>

    <?php // echo $form->field($model, 'out_download_count') ?>

    <?php // echo $form->field($model, 'discount_type') ?>

    <?php // echo $form->field($model, 'is_hidden_discount') ?>

    <?php // echo $form->field($model, 'need_act') ?>

    <?php // echo $form->field($model, 'need_packing_list') ?>

    <?php // echo $form->field($model, 'need_sales_invoice') ?>

    <?php // echo $form->field($model, 'need_upd') ?>

    <?php // echo $form->field($model, 'create_type_id') ?>

    <?php // echo $form->field($model, 'create_api_id') ?>

    <?php // echo $form->field($model, 'store_id') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'project_id') ?>

    <?php // echo $form->field($model, 'project_estimate_id') ?>

    <?php // echo $form->field($model, 'not_for_bookkeeping') ?>

    <?php // echo $form->field($model, 'is_surcharge') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-kub-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
