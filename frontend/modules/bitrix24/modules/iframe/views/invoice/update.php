<?php

use frontend\modules\bitrix24\modules\iframe\widgets\CompanySelectWidget;

/* @var $this yii\web\View */
/* @var $company common\models\Company */
/* @var $model common\models\document\Invoice */

$this->title = 'Редактировать счет';

?>
<div class="invoice-update">
    <div class="mb-5">
        <?= CompanySelectWidget::widget([
            'widgetUser' => Yii::$app->bitrix24User,
            'disabled' => true,
        ]) ?>
    </div>

    <?= $this->render('form/_form', [
        'model' => $model,
        'company' => $company,
        'ioType' => $ioType,
        'invoiceContractEssence' => $invoiceContractEssence,
        'invoiceEssence' => $invoiceEssence,
    ]) ?>

</div>
