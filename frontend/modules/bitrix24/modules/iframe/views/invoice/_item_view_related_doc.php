<?php

use common\models\document\status\InvoiceStatus;
use frontend\components\Icon;
use frontend\modules\bitrix24\modules\iframe\components\helpers\DocStatus;
use frontend\modules\bitrix24\modules\iframe\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\document\AbstractDocument */
/* @var $slug string */

if ($model->uid == null) {
    $model->updateAttributes([
        'uid' => $model::generateUid(),
    ]);
}
$uid = $model->uid;
$viewUrl = Url::to([
    "/documents/{$slug}/out-view",
    'uid' => $uid,
], true);
$updateUrl = Url::to([
    "{$slug}/update",
    'id' => $model->id,
], true);
$pdfUrl = Url::to([
    "/documents/{$slug}/download",
    'type' => 'pdf',
    'uid' => $uid,
], true);
?>

<div class="py-1 border-top">
    <div>
        <?= $model->title ?>
    </div>
    <div class="text-muted">
        <?= count($model->ownOrders) ?>
        items
    </div>
    <div class="d-flex justify-content-between mt-2">
        <div class="d-flex">
            <?php /*
            <?= Html::button(Icon::get('pencil'), [
                'title' => 'Редактировать',
                'class' => 'bitrix24_action btn btn-sm btn-kub-secondary mr-2',
                'data' => [
                    'action-data' => [
                        'action' => 'getPopup',
                        'data' => ['url' => $updateUrl],
                    ],
                ],
            ]) ?>
            */ ?>
            <?= Html::button('PDF', [
                'title' => 'Скачать PDF',
                'class' => 'bitrix24_action btn btn-sm btn-kub-secondary mr-2',
                'data' => [
                    'action-data' => [
                        'action' => 'getBrTab',
                        'data' => ['url' => $pdfUrl],
                    ],
                ],
            ]) ?>
            <?= Html::button('Ссылка', [
                'title' => 'Скопировать ссылку на документ в буфер обмена',
                'class' => 'btn btn-sm btn-kub-secondary',
                'onclick' => 'copyToClipboard($(this).data(\'text\'))',
                'data-text' => $viewUrl,
            ]) ?>
        </div>
        <div class="">
            <?= DocStatus::tag($model, ['class' => 'btn btn-sm']) ?>
        </div>
    </div>
</div>
