<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\bitrix24\modules\widget\models\InvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="invoice-index">

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_item_view',
        'layout' => "{items}\n{pager}",
        'emptyText' => 'По этой сделке пока нет счетов',
        'pager' => [
            'class' => 'yii\bootstrap4\LinkPager',
            'maxButtonCount' => 5,
        ],
    ]); ?>

</div>
