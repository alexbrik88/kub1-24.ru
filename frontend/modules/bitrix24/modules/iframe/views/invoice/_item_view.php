<?php

use common\models\document\status\InvoiceStatus;
use frontend\components\Icon;
use frontend\modules\bitrix24\modules\iframe\components\helpers\DocStatus;
use frontend\modules\bitrix24\modules\iframe\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */
/* @var $widget yii\widgets\ListView */
/* @var $key integer */
/* @var $index integer */

$itemId = 'list_item_view_'.$model->id;
$accessToken = Yii::$app->bitrix24User->tokenByCompany($model->company_id);

$uid = $model->uid;
$scheme = Yii::$app->request->isSecureConnection ? 'https' : 'http';
$viewUrl = Url::toRoute([
    'view',
    'type' => $model->type,
    'id' => $model->id,
    'access-token' => $accessToken,
], $scheme);
$outViewUrl = Url::toRoute([
    '/documents/invoice/out-view',
    'uid' => $uid,
], $scheme);
$updateUrl = $model->invoice_status_id == InvoiceStatus::STATUS_PAYED ? null : Url::toRoute([
    'update',
    'type' => $model->type,
    'id' => $model->id,
    'access-token' => $accessToken,
], $scheme);
$pdfUrl = Url::toRoute([
    '/documents/invoice/download',
    'type' => 'pdf',
    'uid' => $uid,
], $scheme);
$itemUrl = Url::toRoute([
    'item-view',
    'id' => $model->id,
    'type' => $model->type,
    'access-token' => $accessToken,
], $scheme);
$amount = Yii::$app->formatter->asMoney($model->total_amount_with_nds);
if ($model->invoice_status_id == InvoiceStatus::STATUS_PAYED_PARTIAL) {
    $amount .= '/'.Html::tag('span', Yii::$app->formatter->asMoney($model->payment_partial_amount), [
        'class' => 'js-tooltip',
        'title' => 'Оплаченная сумма',
        'style' => 'color: #26cd58;'
    ]);
}
?>

<div id="<?= $itemId ?>" class="list_item_view py-2 border-bottom" data-item-url="<?= $itemUrl ?>" style="border-color: #f4f4f4;">
    <div class="d-flex justify-content-between">
        <?= Html::tag('strong', Html::encode($model->getTitle()), [
            'class' => 'link cursor_pointer bitrix24_action',
            'data' => [
                'action-data' => [
                    'action' => 'getPopup',
                    'data' => ['url' => $viewUrl],
                ],
            ],
        ]) ?>
        <strong>
            <?= $amount ?> р.
        </strong>
    </div>
    <div class="d-flex justify-content-between">
        <div class="text-muted">
            <?= Yii::$app->i18n->format('{n, plural, one{# позиция} few{# позиции} many{# позиций} other{# позиций}} в счете', [
                'n' => count($model->orders),
            ], 'ru_RU') ?>
        </div>
        <div class="text-muted">
            <?php if ($model->invoice_status_id == InvoiceStatus::STATUS_PAYED) : ?>
                <?php $date = $model->allFlowsQuery(['date'])->max('date') ?>
                Дата оплаты: <?= ($d = date_create($date)) ? $d->format('d.m.Y') : '---' ?>
            <?php else : ?>
                Оплатить до: <?= date_create($model->payment_limit_date)->format('d.m.Y') ?>
            <?php endif ?>
        </div>
    </div>
    <div class="d-flex justify-content-between mt-2">
        <div class="d-flex">
            <?php if ($updateUrl === null) : ?>
                <?= Html::beginTag('span', [
                    'id' => uniqid(),
                    'class' => 'js-tooltip',
                    'title' => 'Нельзя редактировать оплаченный счет',
                    'data' => [
                        'trigger' => 'hover',
                    ],
                ]) ?>
            <?php endif ?>
            <?= Html::button(Icon::get('pencil'), [
                'title' => $updateUrl === null ? null : 'Редактировать',
                'class' => 'bitrix24_action btn btn-sm btn-kub-secondary mr-2'.($updateUrl === null ? ' disabled' : ''),
                'disabled' => $updateUrl === null,
                'data' => $updateUrl ? [
                    'action-data' => [
                        'action' => 'getPopup',
                        'data' => ['url' => $updateUrl],
                    ],
                ] : null,
            ]) ?>
            <?php if ($updateUrl === null) : ?>
                <?= Html::endTag('span') ?>
            <?php endif ?>
            <?= Html::button('PDF', [
                'title' => 'Скачать PDF',
                'class' => 'bitrix24_action btn btn-sm btn-kub-secondary mr-2',
                'data' => [
                    'action-data' => [
                        'action' => 'getBrTab',
                        'data' => ['url' => $pdfUrl],
                    ],
                ],
            ]) ?>
            <?= Html::button('Ссылка', [
                'id' => md5($outViewUrl),
                'title' => 'Скопировать ссылку на счет в буфер обмена',
                'class' => 'btn btn-sm btn-kub-secondary copy_to_clipboard',
                'data-text' => $outViewUrl,
            ]) ?>
        </div>
        <div class="">
            <?= DocStatus::tag($model, [
                'class' => 'btn btn-sm br-w-2 js-tooltip',
                'title' => 'Статус счета',
                'data' => [
                    'trigger' => 'hover',
                ],
            ]) ?>
        </div>
    </div>
    <?= $this->render('_item_view_related', [
        'model' => $model,
        'accessToken' => $accessToken,
        'scheme' => $scheme,
    ]) ?>
</div>
