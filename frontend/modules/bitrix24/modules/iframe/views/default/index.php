<?php

use frontend\components\Icon;
use yii\helpers\Json;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $jwtToken string */
/* @var $urls array */

?>

<style type="text/css">
iframe {
    display: block;
    border: none;
    flex: 1;
}
iframe {
    display: block;
    border: none;
    flex: 1;
}
</style>
<div class="d-flex flex-column p-4" style="height: 100%;">
    <div class="row" style="flex: 1; overflow: hidden;">
        <div class="col-3 d-flex flex-column border-right">
            <div id="widget-action-buttons" class="d-flex align-items-center mb-4">
                <?= Html::button('Выставить счет', [
                    'id' => 'create_invoice',
                    'class' => 'authorised-enable  disabled btn btn-sm btn-kub-secondary mr-2',
                    'disabled' => true,
                ]) ?>
                <?= Html::button(Icon::get('repeat'), [
                    'id' => 'refresh_list',
                    'class' => 'authorised-enable  disabled btn btn-sm btn-kub-secondary mr-2',
                    'title' => 'Обновить список счетов',
                    'disabled' => true,
                ]) ?>
                <?= Html::button(Icon::get('logout'), [
                    'id' => 'kub_logout',
                    'class' => 'authorised-enable authorised-visible disabled hidden btn btn-sm btn-kub-secondary ml-auto',
                    'title' => 'Выйти из КУБ',
                    'disabled' => true,
                ]) ?>
            </div>
            <iframe id="kub_iframe_sidebar" class="" src=""></iframe>
        </div>
        <div class="col-9 d-flex flex-column">
            <iframe id="kub_iframe_content" class="" src=""></iframe>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        window.KUB = {
            urls: <?= Json::encode($urls) ?>,
            jwt: "<?= $jwtToken ?>",
            token: null,
            userIndexPath: '/bitrix24/api/user/index',
            userDeletePath: '/bitrix24/api/user/delete',
            checkAccess: function () {
                $.ajax({
                    url: KUB.urls.checkUrl,
                    type: "GET",
                    success: function (data) {
                        if (data.token) {
                            KUB.token = data.token;
                        } else {
                            KUB.token = null;
                        }

                        if (data.isUserOk) {
                            KUB.getTab(KUB.listUrl());
                            $('.authorised-enable ').prop('disabled', false).toggleClass('disabled', false);
                            $('.authorised-visible ').toggleClass('hidden', false);
                        } else {
                            KUB.getTab('');
                            KUB.getPopup(KUB.urls.loginUrl);
                            $('.authorised-enable ').prop('disabled', true).toggleClass('disabled', true);
                            $('.authorised-visible ').toggleClass('hidden', true);
                        }
                    },
                });
            },
            kubLogout: function () {
                $.ajax({
                    url: KUB.urls.logoutUrl,
                    type: "POST",
                    success: function (data) {
                        KUB.checkAccess();
                    },
                    error: function (data) {
                        KUB.checkAccess();
                    },
                });
            },
            userIndex: function () {
                let url = new URL(KUB.userIndexPath, KUB.urls.baseUrl);
                $.ajax({
                    url: this.iframeSrc(url),
                    type: "GET",
                    success: function (data) {
                        console.log(data);
                    }
                });
            },
            userDelete: function () {
                $.ajax({
                    url: this.iframeSrc(this.userDeletePath),
                    type: "POST",
                    crossDomain: true,
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Headers": "origin, content-type, accept"
                    },
                    success: function (data) {
                        console.log(data);
                        alert('User deleted');
                    }
                });
            },
            init: function () {
                if (window.addEventListener) {
                    window.addEventListener("message", KUB.messageListener, false);
                } else if (window.attachEvent) {
                    window.attachEvent('onmessage', KUB.messageListener);
                } else {
                    window['onmessage'] = KUB.messageListener;
                }
                KUB.initEvents();
                KUB.checkAccess();
            },
            initEvents: function () {
                $(document).on('click', '#refresh_list', function () {
                    KUB.refreshList();
                });
                $(document).on('click', '#create_invoice', function () {
                    KUB.actions.getPopup({'url':KUB.createUrl()});
                });
                $(document).on('click', '#kub_logout', function () {
                    KUB.kubLogout();
                });
            },
            iframeSrc: function (url) {
                url = new URL(url, KUB.urls.baseUrl);
                if (!url.searchParams.has('access-token')) {
                    url.searchParams.set('access-token', KUB.token);
                }
                return url.toString();
            },
            listUrl: function () {
                return KUB.iframeSrc(KUB.urls.listUrl);
            },
            createUrl: function () {
                return KUB.iframeSrc(KUB.urls.createUrl);
            },
            setJWT: function (target) {
                if (KUB.jwt) {
                    let message = JSON.stringify({'action':'setJWT','data':{'jwt':KUB.jwt}});
                    target.postMessage(message, '*');
                }
            },
            messageListener: function(event) {
                let data = JSON.parse(event.data);
                if (data.action) {
                    KUB.doAction(data.action, data.data, event);
                }
            },
            doAction: function(action, event, data) {
                if (typeof this.actions[action] === 'function') {
                    this.actions[action](event, data);
                }
            },
            setToken: function (target) {
                if (KUB.token) {
                    let message = JSON.stringify({'action':'setToken','data':{'token':KUB.token}});
                    target.postMessage(message, '*');
                }
            },
            refreshList: function () {
                KUB.actions.getTab({'url':KUB.listUrl()});
            },
            getTab: function (src) {
                document.getElementById('kub_iframe_sidebar').src = src;
            },
            getPopup: function (src) {
                document.getElementById('kub_iframe_content').src = src;
            },
            actions: {
                checkAccess: function (d, e) {
                    KUB.checkAccess();
                },
                refreshList: function (d, e) {
                    KUB.refreshList();
                },
                closePopup: function (d, e) {
                    KUB.getPopup('');
                },
                getToken: function (d, e) {
                    KUB.setToken(e.source);
                },
                getTab: function (d, e) {
                    KUB.getTab(KUB.iframeSrc(d.url));
                },
                getPopup: function (d, e) {
                    KUB.getPopup(KUB.iframeSrc(d.url));
                },
                getBrTab: function (d, e) {
                    window.open(d.url, '_blank').focus();
                },
            },
        };
        KUB.init();
    });
</script>
