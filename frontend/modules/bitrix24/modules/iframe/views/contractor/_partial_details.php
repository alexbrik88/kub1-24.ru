<?php

use frontend\components\Icon;
use frontend\modules\bitrix24\modules\iframe\helpers\Url;
use yii\helpers\Html;

/* @var $form yii\widgets\ActiveForm */
/* @var $model common\models\Contractor */

$toggleText = 'Указать дополнительные данные необходимые для акта, товарной накладной, счет-фактуры';
$toggleLabel = Html::tag('span', $toggleText, ['class' => 'link-txt']);
$toggleIcon = Icon::get('shevron', ['class' => 'link-shevron']);
?>

<div class="mb-3">
    <?= Html::tag('span', $toggleLabel." ".$toggleIcon, [
        'class' => 'link link_collapse link_bold cursor-pointer collapsed',
        'data-toggle' => 'collapse',
        'data-target' => '#dopColumns2',
        'aria-expanded' => 'false',
        'aria-controls' => 'dopColumns2',
    ]) ?>
</div>

<div id="dopColumns2" class="collapse dopColumns">
    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'director_post_name')->label('Должность руководителя'); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'director_name')->label('ФИО руководителя'); ?>
        </div>
        <div class="col-12">
            <?= $form->field($model, 'legal_address')->textInput([
                'maxlength' => true,
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'current_account')->textInput([
                'maxlength' => true,
            ]); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'BIC')->widget(\common\components\widgets\BikTypeahead::classname(), [
                'remoteUrl' => Url::to(['/bitrix24/iframe/dictionary/bik']),
                'related' => [
                    '#' . Html::getInputId($model, 'bank_name') => 'name',
                    '#' . Html::getInputId($model, 'bank_city') => 'city',
                    '#' . Html::getInputId($model, 'corresp_account') => 'ks',
                ],
            ])->textInput(['placeHolder' => 'Нужен для Актов и Товарных накладных', 'autocomplete' => 'off']); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'bank_name')->textInput([
                'maxlength' => true,
                'disabled' => true,
            ]); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'bank_city')->textInput([
                'maxlength' => true,
                'disabled' => true,
            ]); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'corresp_account')->textInput([
                'maxlength' => true,
                'disabled' => true,
            ]); ?>
        </div>
    </div>
</div>