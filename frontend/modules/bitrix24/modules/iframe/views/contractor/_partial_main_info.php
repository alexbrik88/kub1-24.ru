<?php

use common\components\helpers\Html;
use \frontend\models\Documents;
use kartik\select2\Select2;

/* @var $model common\models\Contractor */
/* @var $form yii\widgets\ActiveForm */
/* @var $companyTypes array */


$inputListConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
    'inputOptions' => [
        'class' => 'form-control'
    ]
];

?>
<?= Html::activeHiddenInput($model, 'type'); ?>
<?= Html::hiddenInput(null, $model->isNewRecord, [
    'id' => 'contractor-is_new_record',
]); ?>

<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'name')->textInput([
            'placeholder' => 'Без ООО/ИП',
            'maxlength' => true,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
                'content' => 'Впишите название компании без организационно-правовой формы (ИП, ООО, ЗАО, ПАО).',
            ],
        ])->label('Название контрагента') ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'companyTypeId')->label('Форма')->widget(Select2::class, [
            'size' => 'sm',
            'data' => $companyTypes,
            'disabled' => !$model->isNewRecord,
            'pluginOptions' => [
                'width' => '100%'
            ]
        ]); ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'PPC')->textInput([
            'maxlength' => true,
        ])->label('КПП') ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'director_email')->textInput([
            'maxlength' => true,
        ])->label('Email покупателя (для отправки счетов)') ?>
    </div>
</div>
