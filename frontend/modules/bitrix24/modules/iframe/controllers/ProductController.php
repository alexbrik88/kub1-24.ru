<?php

namespace frontend\modules\bitrix24\modules\iframe\controllers;

use Yii;
use common\models\TaxRate;
use common\models\address\Country;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductUnit;
use frontend\models\Documents;
use frontend\modules\bitrix24\models\ProductSearch;
use frontend\modules\export\models\one_c\OneCExport;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Product controller for the `iframe` module
 */
class ProductController extends Controller
{
    public $layout = 'main';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'authenticator' => [
                'class' => 'frontend\modules\bitrix24\components\QueryParamAuth',
            ],
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'actions' => [
                            'create-modal',
                        ],
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\Product::CREATE,
                        ],
                    ],
                    [
                        'actions' => [
                            'select-modal',
                            'get-products',
                            'search',
                        ],
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\Product::INDEX,
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionCreateModal($type = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $documentType = Documents::IO_TYPE_OUT;
        $productionType = $type;

        $model = $this->createHelper($type);
        $scenarioProd = $documentType == 1 ? 'zakup' : 'prod';
        $scenarioUslug = $productionType == 1 ? 'tovar' : 'uslug';
        $scenario = $scenarioProd . $scenarioUslug;
        $model->setScenario($scenario);
        $model->product_unit_id = ProductUnit::UNIT_COUNT;

        if ($company->companyTaxationType->osno) {
            $model->price_for_sell_nds_id = TaxRate::RATE_20;
            $model->price_for_buy_nds_id = TaxRate::RATE_20;
        } else {
            $model->price_for_sell_nds_id = TaxRate::RATE_WITHOUT;
            $model->price_for_buy_nds_id = TaxRate::RATE_WITHOUT;
        }

        if (Yii::$app->request->post('ajax') !== null) {
            $model->load(Yii::$app->request->post());

            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->_prepareFromInvoice() && $model->save()) {
            return [
                'items' => $model->getProductsByIds([$model->id], $company->id),
                'services' => $company->getProducts()->byDeleted()->andWhere([
                    'production_type' => Product::PRODUCTION_TYPE_SERVICE,
                ])->count(),
                'goods' => $company->getProducts()->byDeleted()->andWhere([
                    'production_type' => Product::PRODUCTION_TYPE_GOODS,
                ])->count(),
            ];
        }

        $header = 'Добавить товар/услугу';
        $body = $this->renderAjax('create-modal', [
            'model' => $model,
            'company' => $company,
            'documentType' => $documentType,
        ]);

        return [
            'header' => $header,
            'body' => $body,
        ];
    }

    /**
     * @return string|NotFoundHttpException
     */
    public function actionSelectModal($type)
    {
        $documentType = Documents::IO_TYPE_OUT;
        $storeId = Yii::$app->request->get('store_id');

        $searchModel = new ProductSearch();
        $searchModel->filterStatus = ProductSearch::IN_WORK;
        $searchModel->documentType = $documentType;
        $searchModel->production_type = $type;
        $searchModel->company_id = Yii::$app->user->identity->company->id;
        $searchModel->title = Yii::$app->request->get('title', '');
        $searchModel->exclude = Yii::$app->request->get('exists', []);
        $searchModel->store_id = $storeId;

        $dataProvider = $searchModel->search(Yii::$app->request->get());
        //$dataProvider->pagination->pageSize = 10;

        return $this->renderAjax('select-modal', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'documentType' => $documentType,
            'productType' => $type,
            'storeId' => $storeId
        ]);
    }

    /**
     * @param bool|false $toOrderDocument
     * @param int $documentType
     * @return array|ActiveRecord[]
     */
    public function actionGetProducts()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $product = new Product();

        return $product->getProductsByIds(Yii::$app->request->post('in_order'), Yii::$app->user->identity->company->id);
    }

    /**
     * @param $productionType integer
     * @return string
     */
    public function actionSearch($q = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $company = Yii::$app->user->identity->company;
        $productArray = $q ? $company->getProducts()
            ->select(['id', 'title text'])
            ->andWhere([
                'is_deleted' => false,
                'status' => Product::ACTIVE,
            ])
            ->andWhere([
                'or',
                ['like', 'title', $q],
                ['like', 'article', $q . '%', false],
            ])
            ->limit(5)
            ->asArray()
            ->all() : [];

        return $productArray;
    }

    /**
     * @param $productionType
     * @return Product
     */
    protected function createHelper($productionType)
    {
        $model = new Product();
        $model->production_type = $productionType;
        $model->creator_id = Yii::$app->user->id;
        $model->company_id = Yii::$app->user->identity->company->id;
        $model->country_origin_id = Country::COUNTRY_WITHOUT;
        $model->group_id = ProductGroup::WITHOUT;

        return $model;
    }
}
