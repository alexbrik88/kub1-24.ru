<?php

namespace frontend\modules\bitrix24\modules\iframe\controllers;

use Yii;
use frontend\modules\bitrix24\components\CheckAccess;
use frontend\modules\bitrix24\models\LoginForm;
use frontend\modules\bitrix24\models\SoloClients;
use frontend\modules\bitrix24\models\WidgetUser;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;

/**
 * Auth controller for the `iframe` module
 */
class AuthController extends Controller
{
    public $layout = 'main';

    /**
     * @var Lcobucci\JWT\Token
     */
    private $token;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'actions' => [
                    'check-access',
                ],
            ],
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'actions' => [
                            'check-access',
                            'login',
                            'logout',
                        ],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            $token = Yii::$app->request->get('access-token');
                            $this->token = Yii::$app->jwt->loadToken($token, true, false);
                            if (empty($this->token)) {
                                Yii::$app->bitrix24Helper->debugLog(__METHOD__, [
                                    'token' => $token,
                                    'decoded' => base64_decode($token),
                                ]);
                                throw new UnauthorizedHttpException('Your request was made with invalid credentials.');
                            }

                            return true;
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'create' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionCheckAccess()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $checkAccess = new CheckAccess($this->token);

        return [
            'jwt' => '',
            'isWidgetOk' => true,
            'isUserOk' => $checkAccess->isUserOk,
            'token' => $checkAccess->token,
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'main';

        $model = new LoginForm($this->token, [
            'scenario' => LoginForm::SCENARIO_LOGIN,
        ]);

        $postData = Yii::$app->request->post();
        if (isset($postData[$model->formName()]['company_id'])) {
            $model->scenario = LoginForm::SCENARIO_COMPANY;
        }

        if ($model->load($postData) && $model->validate()) {
            if ($model->scenario == LoginForm::SCENARIO_LOGIN && $model->getUserIsAdmin()) {
                $model->scenario = LoginForm::SCENARIO_COMPANY;
                $companyList = $model->getDropdownItems();
                if (count($companyList) === 1) {
                    $model->company_id = array_key_first($companyList);
                }
            } elseif ($model->save()) {
                return $this->render('login-success', [
                    'model' => $model,
                ]);
            }
        }
        \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $client = SoloClients::findOne(['portal' => $this->token->getClaim('DOMAIN')]);
        $user_id = $this->token->getClaim('user_id');

        return WidgetUser::deleteAll([
            'client_id' => $client->id,
            'user_id' => $user_id,
        ]);
    }
}
