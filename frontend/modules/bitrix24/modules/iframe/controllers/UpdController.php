<?php

namespace frontend\modules\bitrix24\modules\iframe\controllers;

use Yii;
use common\models\document\Invoice;
use common\models\document\Upd;
use frontend\models\Documents;
use frontend\rbac\permissions;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Upd controller for the `iframe` module
 */
class UpdController extends Controller
{
    public $layout = 'main';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'authenticator' => [
                'class' => 'frontend\modules\bitrix24\components\QueryParamAuth',
            ],
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'actions' => [
                            'create',
                        ],
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\document\Document::CREATE,
                        ],
                        'roleParams' => function () {
                            return [
                                'ioType' => Documents::IO_TYPE_OUT,
                            ];
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'create' => ['POST'],
                ],
            ],
            'strict' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                ],
                'denyCallback' => function () {
                    throw new ForbiddenHttpException('Необходимо заполнить профиль компании.');
                }
            ],
        ];
    }

    /**
     * @param $type
     * @param $invoiceId
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \Exception
     */
    public function actionCreate($type, $invoiceId)
    {
        $this->checkType($type);

        /* @var Invoice $invoice */
        $invoice = $this->findInvoiceModel($invoiceId, $type);
        $documentDate = null;
        $documentNumber = null;
        if ($invoice->type == Documents::IO_TYPE_IN) {
            $documentDate = $invoice->document_date;
            $documentNumber = $invoice->document_number;
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if ($invoice->canAddUpd) {
            if (!$invoice->need_act) {
                return [
                    'status' => 'error',
                    'message' => 'УПД не нужен.',
                ];
            }
            if ($invoice->createUpd($documentDate, $documentNumber)) {
                return [
                    'status' => 'success',
                    'message' => 'УПД создан.',
                ];
            } else {
                return [
                    'status' => 'error',
                    'message' => 'При создании УПД возникли ошибки.',
                ];
            }
        } else {
            return [
                'status' => 'error',
                'message' => 'Нельзя создать УПД.',
            ];
        }
    }

    /**
     * @param integer $type
     * @throws NotFoundHttpException if the type is not valid
     */
    protected function checkType($type)
    {
        $typeArray = [Documents::IO_TYPE_IN, Documents::IO_TYPE_OUT];

        if (!in_array($type, $typeArray)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Invoice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Invoice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findInvoiceModel($id, $type = null, $throw = true)
    {
        $companyId = ArrayHelper::getValue(Yii::$app->user, ['identity', 'company', 'id']);

        $model = Invoice::find()->andWhere([
            'id' => $id,
            'company_id' => $companyId,
            'is_deleted' => false,
        ])->andFilterWhere([
            'type' => $type,
        ])->one();

        if ($throw && $model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $model;
    }

    /**
     * Finds the Upd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Invoice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $type = null, $throw = true)
    {
        $companyId = ArrayHelper::getValue(Yii::$app->user, ['identity', 'company', 'id']);

        $model = Upd::find()->joinWith('invoices')->andWhere([
            'act.id' => $id,
            'invoice.company_id' => $companyId,
            'invoice.is_deleted' => false,
        ])->andFilterWhere([
            'act.type' => $type,
        ])->one();

        if ($throw && $model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $model;
    }
}
