<?php

namespace frontend\modules\bitrix24\modules\iframe\controllers;

use Yii;
use common\models\Contractor;
use frontend\modules\export\models\one_c\OneCExport;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Contractor controller for the `iframe` module
 */
class ContractorController extends Controller
{
    public $layout = 'main';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'authenticator' => [
                'class' => 'frontend\modules\bitrix24\components\QueryParamAuth',
            ],
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'actions' => [
                            'dropdown',
                        ],
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\Contractor::INDEX,
                        ],
                        'roleParams' => function () {
                            return [
                                'type' => Contractor::TYPE_CUSTOMER,
                            ];
                        },
                    ],
                    [
                        'actions' => [
                            'create-modal',
                        ],
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\Contractor::CREATE,
                        ],
                        'roleParams' => function () {
                            return [
                                'type' => Contractor::TYPE_CUSTOMER,
                            ];
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'dropdown' => [
                'class' => 'frontend\components\ContractorDropdownAction',
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionCreateModal($type = null, $face_type = null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (!isset($type) || !in_array($type, [Contractor::TYPE_SELLER, Contractor::TYPE_CUSTOMER])) {
            $type = Contractor::TYPE_CUSTOMER;
        }

        if (!isset($face_type) || !in_array($face_type, [Contractor::TYPE_LEGAL_PERSON, Contractor::TYPE_PHYSICAL_PERSON])) {
            $face_type = Contractor::TYPE_LEGAL_PERSON;
        }

        if ($face_type == Contractor::TYPE_PHYSICAL_PERSON) {
            $scenario = Contractor::SCENARIO_FIZ_FACE;
        } else {
            $scenario = Contractor::SCENARIO_LEGAL_FACE;
        }

        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        $model = new Contractor([
            'scenario' => $scenario,
            'type' => $type,
            'face_type' => $face_type,
            'status' => Contractor::ACTIVE,
            'taxation_system' => Contractor::WITHOUT_NDS,
            'chief_accountant_is_director' => 1,
            'contact_is_director' => 1,
            'company_id' => Yii::$app->user->identity->company->id,
            'employee_id' => Yii::$app->user->id,
            'object_guid' => OneCExport::generateGUID(),
        ]);

        if (Yii::$app->request->post('ajax') !== null) {
            $model->load(Yii::$app->request->post());

            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {

            if ($model->face_type == Contractor::TYPE_PHYSICAL_PERSON) {
                if (!empty($model->contact_email)) {
                    $model->director_email = $model->contact_email;
                }
            }

            if (empty($model->director_name)) {
                $model->director_name = 'ФИО руководителя';
            }

            if ($model->save()) {
                $model->contractorAccount->save();

                return $data = [
                    'id' => $model->id,
                    'name' => $model->name,
                    'name-short' => $model->companyType ? $model->companyType->name_short : '',
                ];
            } else {
                \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);
            }
        }

        $header = 'Добавить ' . ($model->type == Contractor::TYPE_SELLER ? 'поставщика' : 'покупателя');
        $body = $this->renderAjax('create-modal', [
            'model' => $model,
        ]);

        return [
            'header' => $header,
            'body' => $body,
        ];
    }
}
