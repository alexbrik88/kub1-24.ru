<?php

namespace frontend\modules\bitrix24\modules\iframe\controllers;

use Yii;
use frontend\modules\bitrix24\models\RequestData;
use frontend\modules\bitrix24\models\SoloClients;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `bitrix24` module
 */
class DefaultController extends Controller
{
    public $layout = 'default';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'Bitrix24AccessFilter' => [
                'class' => 'frontend\modules\bitrix24\components\Bitrix24AccessFilter',
                'scenario' => RequestData::SCENARIO_IFRAME,
                'only' => [
                    'index',
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => \yii\web\ErrorAction::className(),
            ],
        ];
    }

    /**
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionIndex($DOMAIN = null)
    {
        if (empty($DOMAIN) || ($client = SoloClients::findOne(['portal' => $DOMAIN])) === null) {
            return $this->render('client_not_found');
        }
        $helper = Yii::$app->bitrix24Helper;
        $jwtToken = $helper->genJwtToken();
        $placement = $helper->getPlacement();
        $urls = [
            'baseUrl' => Yii::$app->request->hostInfo,
            'checkUrl' => Url::to(['auth/check-access', 'access-token' => $jwtToken]),
            'loginUrl' => Url::to(['auth/login', 'access-token' => $jwtToken]),
            'logoutUrl' => Url::to(['auth/logout', 'access-token' => $jwtToken]),
            'listUrl' => Url::to(['invoice/index', 'placement' => $placement]),
            'createUrl' => Url::to([
                'invoice/create',
                'inn' => $helper->getCompanyInn(),
                'company_id' => $helper->getCompanyId(),
                'contact_id' => $helper->getContactId(),
                'deal_id' => $helper->getDealId(),
                'lead_id' => $helper->getLeadId(),
            ]),
        ];

        return $this->render('index', [
            'jwtToken' => $jwtToken,
            'urls' => $urls,
        ]);
    }
}
