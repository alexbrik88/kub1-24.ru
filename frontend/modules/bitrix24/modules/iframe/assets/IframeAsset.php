<?php

namespace frontend\modules\bitrix24\modules\iframe\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * IframeAsset
 */
class IframeAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/modules/bitrix24/modules/iframe/assets/web';

    /**
     * @var array
     */
    public $css = [
    ];

    /**
     * @var array
     */
    public $js = [
    ];

    /**
     * @var array
     */
    public $depends = [
        'frontend\modules\bitrix24\modules\iframe\assets\CommonAsset',
        'yii\widgets\PjaxAsset',
        'common\assets\SuggestionsCssAsset',
        'common\assets\SuggestionsJqueryAsset',
    ];

}
