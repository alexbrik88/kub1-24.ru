$(function () {
    // create invoice
    window.INVOICE = {
        $addNewModal: $('#add-new'),
        productSeletedToInvoice: [],
        $existedProductForm: $('#products_in_order'),
        $productNewBlock: $('#add-new'),
        $productAddNewButton: $('#product-add-new'),
        $productTypeBlock: $('.invoice-production-type-block'),
        $invoiceTable: $('#product-table-invoice'),
        $invoiceSum: $('#invoice-sum'),
        $btnProductSearch: $('#btn-product-search'),
        documentIoType: $('#invoice-form').data('iotype') || $('#first-invoice-form').data('iotype'),
        InvoiceOut: 2,
        InvoiceIn: 1,
        CurrencyRateRequestData: [],
        InvoiceAmountRequestData: [],
        currentContractorSelect: null,
        getFloatPrice: function (price) {
            return (price / 100).toFixed(2);
        },
        addProductToTable: function (items) {
            if (items) {
                var $template = INVOICE.$invoiceTable.find('.template').clone();
                $template.removeClass('template').addClass('product-row');
                $template.find('input').attr('disabled', false);
                var $itemArray = [];
                var itemNumber = parseInt($('#table-product-list-body').data('number')) || 0;
                var documentIoType = $('#documentType').val();

                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    var unitName = item.productUnit ? item.productUnit.name : '---';
                    $item = $template.clone();
                    $item.attr('id', 'model_' + item.id);
                    $item.find('.product-article').html(item.article || '&nbsp;');
                    $item.find('.product-delete').find('.remove-product-from-invoice').data('id', item.id);
                    $item.find('.product-title')
                        .attr('name', 'orderArray[' + itemNumber + '][title]')
                        .val(item.title);

                    if (documentIoType == INVOICE.InvoiceOut) {
                        if (item.priceForSellNds) {
                            $item.find('.tax-rate').val(item.priceForSellNds.rate);
                        } else {
                            $item.find('.tax-rate').val('');
                        }
                    } else {
                        if (item.priceForBuyNds) {
                            $item.find('.tax-rate').val(item.priceForBuyNds.rate);
                        } else {
                            $item.find('.tax-rate').val('');
                        }
                    }

                    $item.find('.order-id')
                        .attr('name', 'orderArray[' + itemNumber + '][id]')
                        .val('');
                    $item.find('.product-id')
                        .attr('name', 'orderArray[' + itemNumber + '][product_id]')
                        .val(item.id);

                    if ($item.find('.product-unit-name').find('select').length) {
                        $item.find('.product-unit-name').find('select')
                            .attr('name', 'orderArray[' + itemNumber + '][unit_id]')
                            .attr('id', 'orderArray__unit_' + itemNumber)
                            .addClass('select2me')
                            .removeAttr('disabled')
                            .val(item.productUnit ? item.productUnit.id : null);
                    } else {
                        $item.find('.product-unit-name').text(unitName);
                    }

                    $item.find('.product-count')
                        .attr('id', 'item-count' + item.id)
                        .attr('name', 'orderArray[' + itemNumber + '][count]')
                        .val(1);
                    if (unitName == '---') {
                        $item.find('.product-count').attr('type', 'hidden');
                        $item.find('.product-no-count').removeClass('hidden');
                    } else if (unitName == 'ч') {
                        $item.find('.product-count').attr('type', 'text');
                        $item.find('.product-count').attr('data-hour', 'true');
                    }

                    $item.find('.price-input')
                        .attr('name', 'orderArray[' + itemNumber + '][price]');
                    $item.find('.discount-input')
                        .attr('name', 'orderArray[' + itemNumber + '][discount]');
                    $item.find('.markup-input')
                        .attr('name', 'orderArray[' + itemNumber + '][markup]');
                    $item.find('.weight-input')
                        .attr('name', 'orderArray[' + itemNumber + '][weight]').val(item.weight);
                    $item.find('.volume-input')
                        .attr('name', 'orderArray[' + itemNumber + '][volume]').val(item.volume);

                    var itemNds = (documentIoType == INVOICE.InvoiceOut ? item.priceForSellNds : item.priceForBuyNds);
                    //$item.find('.price-for-sell-nds-name').text(itemNds.name).attr('data-id', itemNds.id).attr('data-name', itemNds.name);
                    $item.find('.price-for-sell-nds-name').attr('data-id', itemNds.id).attr('data-name', itemNds.name)
                        .find('select')
                        .attr('name', 'orderArray[' + itemNumber + '][' + (documentIoType == INVOICE.InvoiceOut ? 'sale_tax_rate_id' : 'purchase_tax_rate_id') + ']')
                        .attr('id', 'orderArray__nds_' + itemNumber)
                        .addClass('select2me')
                        .removeAttr('disabled')
                        .val(itemNds.id);

                    var viewType = parseInt($('#invoice-nds_view_type_id').val());

                    if (item.price_for_sell_with_nds == null) {
                        item.price_for_sell_with_nds = 0;
                    }
                    if (item.price_for_buy_with_nds == null) {
                        item.price_for_buy_with_nds = 0;
                    }

                    var priceForSell = viewType == 1 ? Math.round(item.price_for_sell_with_nds / (Number(item.priceForSellNds.rate) + 1)) : item.price_for_sell_with_nds;
                    var priceForBuy = viewType == 1 ? Math.round(item.price_for_buy_with_nds / (Number(item.priceForBuyNds.rate) + 1)) : item.price_for_buy_with_nds;
                    var priceForWithNds = documentIoType == INVOICE.InvoiceOut ? priceForSell : priceForBuy;

                    $item.find('.price-input').val(INVOICE.getFloatPrice(parseInt(priceForWithNds)));
                    $item.find('.price-one-with-nds').text(INVOICE.getFloatPrice(parseInt(priceForWithNds)));
                    $item.find('.price-with-nds').text(INVOICE.getFloatPrice((priceForWithNds)));

                    // For PriceList
                    if ($('#price-list-form').length) {
                        $item.find('.product-group-name').text(item.group_name);
                        $item.find('.product-comment').text(item.comment_photo);
                        $item.find('.product-production-type').val(item.production_type);
                        if (item.productStores.length) {
                            var reserveCol = $item.find('.product-quantity');
                            var quantityAllStores = 0;
                            $.each(item.productStores, function(i,v) {
                                $(reserveCol).attr('data-store-' + v.store_id, parseFloat(v.quantity));
                                quantityAllStores += parseFloat(v.quantity);
                            });
                            $(reserveCol).attr('data-store-all', quantityAllStores);
                        }
                    }

                    // For ScanRecognize
                    if (item.quantity > 0) {
                        $item.find('.product-count').val(item.quantity);
                    }
                    if (item.unknown_product_has_similar) {
                        $item.find('.product-title').after(this.getRecognizeLink(item, 'Б) Выбрать из похожих', 'exists'));
                    }
                    if (item.unknown_product_has_similar && item.unknown_product) {
                        $item.find('.product-title').after('<br/>');
                    }
                    if (item.unknown_product) {
                        $item.find('.product-title').after(this.getRecognizeLink(item, 'А) Добавить как новый', 'new'));
                    }

                    $itemArray.push($item);
                    itemNumber++;
                }

                var sortAfterAdding = $('#table-for-invoice').hasClass('sort_after_adding');

                for (i in $itemArray) {
                    var $beforeElement = '#from-new-add-row';
                    if (sortAfterAdding) {
                        $beforeElement = INVOICE.getProductInsertBeforeElement($itemArray[i]);
                    }

                    $itemArray[i].show().insertBefore($beforeElement);
                }
                // create select2
                $('#table-product-list-body').find('.select2me').each(function() {
                    createSimpleSelect2($(this).attr('id'));
                    $(this).removeClass('select2me');
                });

                if (!$("th.discount_column").hasClass("hidden")) {
                    $("table .discount-input").each(function () {
                        INVOICE.recalculateItemPrice($(this));
                    });
                }
                // For PriceList
                if ($('#price-list-form').length) {
                    window.PriceListForm.refreshMarkupDiscount();
                }

                INVOICE.recalculateInvoiceTable();
                INVOICE.checkProductTypeEnabled();
                INVOICE.tultipProduct('.tooltip-product');
                $('#table-product-list-body').data('number', itemNumber);
                if ($('#invoice-is_invoice_contract').val() == 1) {
                    checkContractEssenceTemplate();
                }
                setTimeout( function() {
                    $(document).trigger('productRow_insert.userHelp');
                }, 200);

                return true;
            }

            return false;
        },
        replaceProductInTable: function (items, pos) {
            if (items) {
                var $template = INVOICE.$invoiceTable.find('.template').clone();
                $template.removeClass('template').addClass('product-row');
                $template.find('input').attr('disabled', false);
                var $itemArray = [];
                var $posElement = $('#table-product-list-body').find('tr').eq(pos);
                var itemNumber = parseInt($('#table-product-list-body').data('number')) || 0;
                var quantity = parseInt($posElement.find('.unknown_product').data('quantity')) || 0;
                var documentIoType = $('#documentType').val();

                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    var unitName = item.productUnit ? item.productUnit.name : '---';
                    $item = $template.clone();
                    $item.attr('id', 'model_' + item.id);
                    $item.find('.product-article').html(item.article || '&nbsp;');
                    $item.find('.product-delete').find('.remove-product-from-invoice').data('id', item.id);
                    $item.find('.product-title')
                        .attr('name', 'orderArray[' + itemNumber + '][title]')
                        .val(item.title);

                    if (documentIoType == INVOICE.InvoiceOut) {
                        if (item.priceForSellNds) {
                            $item.find('.tax-rate').val(item.priceForSellNds.rate);
                        } else {
                            $item.find('.tax-rate').val('');
                        }
                    } else {
                        if (item.priceForBuyNds) {
                            $item.find('.tax-rate').val(item.priceForBuyNds.rate);
                        } else {
                            $item.find('.tax-rate').val('');
                        }
                    }

                    $item.find('.order-id')
                        .attr('name', 'orderArray[' + itemNumber + '][id]')
                        .val('');
                    $item.find('.product-id')
                        .attr('name', 'orderArray[' + itemNumber + '][product_id]')
                        .val(item.id);

                    $item.find('.product-unit-name').text(unitName);
                    $item.find('.product-count')
                        .attr('id', 'item-count' + item.id)
                        .attr('name', 'orderArray[' + itemNumber + '][count]')
                        .val(1);
                    if (unitName == '---') {
                        $item.find('.product-count').attr('type', 'hidden');
                        $item.find('.product-no-count').removeClass('hidden');
                    } else if (unitName == 'ч') {
                        $item.find('.product-count').attr('type', 'text');
                        $item.find('.product-count').attr('data-hour', 'true');
                    }

                    $item.find('.price-input')
                        .attr('name', 'orderArray[' + itemNumber + '][price]');
                    $item.find('.discount-input')
                        .attr('name', 'orderArray[' + itemNumber + '][discount]');
                    $item.find('.markup-input')
                        .attr('name', 'orderArray[' + itemNumber + '][markup]');
                    $item.find('.weight-input')
                        .attr('name', 'orderArray[' + itemNumber + '][weight]').val(item.weight);
                    $item.find('.volume-input')
                        .attr('name', 'orderArray[' + itemNumber + '][volume]').val(item.volume);

                    var itemNds = (documentIoType == INVOICE.InvoiceOut ? item.priceForSellNds : item.priceForBuyNds);
                    $item.find('.price-for-sell-nds-name').text(itemNds.name).attr('data-id', itemNds.id).attr('data-name', itemNds.name);

                    var viewType = parseInt($('#invoice-nds_view_type_id').val());

                    if (item.price_for_sell_with_nds == null) {
                        item.price_for_sell_with_nds = 0;
                    }
                    if (item.price_for_buy_with_nds == null) {
                        item.price_for_buy_with_nds = 0;
                    }

                    var priceForSell = viewType == 1 ? Math.round(item.price_for_sell_with_nds / (Number(item.priceForSellNds.rate) + 1)) : item.price_for_sell_with_nds;
                    var priceForBuy = viewType == 1 ? Math.round(item.price_for_buy_with_nds / (Number(item.priceForBuyNds.rate) + 1)) : item.price_for_buy_with_nds;
                    var priceForWithNds = documentIoType == INVOICE.InvoiceOut ? priceForSell : priceForBuy;

                    $item.find('.price-input').val(INVOICE.getFloatPrice(parseInt(priceForWithNds)));
                    $item.find('.price-one-with-nds').text(INVOICE.getFloatPrice(parseInt(priceForWithNds)));
                    $item.find('.price-with-nds').text(INVOICE.getFloatPrice((priceForWithNds)));

                    // For ScanRecognize
                    $item.find('.product-count').val(quantity);

                    $itemArray.push($item);
                    itemNumber++;
                }

                for (i in $itemArray) {
                    $itemArray[i].show().insertAfter($posElement);
                }

                $posElement.remove();

                if (!$("th.discount_column").hasClass("hidden")) {
                    $("table .discount-input").each(function () {
                        INVOICE.recalculateItemPrice($(this));
                    });
                }
                INVOICE.recalculateInvoiceTable();
                INVOICE.checkProductTypeEnabled();
                INVOICE.tultipProduct('.tooltip-product');
                $('#table-product-list-body').data('number', itemNumber);
                if ($('#invoice-is_invoice_contract').val() == 1) {
                    checkContractEssenceTemplate();
                }

                return true;
            }

            return false;
        },
        recalculateInvoiceTable: function () {
            var discount = 0,
                total = 0,
                tax = 0,
                amount = 0,
                weight = 0,
                volume = 0,
                ndsViewId = parseInt($('#invoice-nds_view_type_id').val());
            items = this.getItems();

            var productWeightPrecision = 2;
            var productVolumePrecision = 2;
            items.each(function () {
                var $row = $(this);
                var productWeight = parseFloat($row.find('.weight-input').val() || 0);
                var productVolume = parseFloat($row.find('.volume-input').val() || 0);
                productWeightPrecision = Math.max((productWeight + '').indexOf('.') > 0 ? (productWeight + '').split('.')[1].length : 0, productWeightPrecision);
                productVolumePrecision = Math.max((productVolume + '').indexOf('.') > 0 ? (productVolume + '').split('.')[1].length : 0, productVolumePrecision);
            });
            productWeightPrecision = Math.min(4, productWeightPrecision);
            productVolumePrecision = Math.min(4, productVolumePrecision);

            items.each(function () {
                var $row = $(this);
                var taxRate = parseFloat($row.find('.tax-rate').val() || 0),
                    productBasePrice = parseFloat($row.find('.price-input').val()),
                    productPrice = parseFloat($row.find('.price-one-with-nds').eq(0).text()),
                    productCount = parseFloat($row.find('.product-count').val()),
                    productWeight = parseFloat($row.find('.weight-input').val() || 0),
                    productVolume = parseFloat($row.find('.volume-input').val() || 0),

                    productTax = 0,
                    productTotal = 0,
                    productDiscount = 0;

                productTotal = productPrice * productCount;

                if (ndsViewId === 2) {
                    productTax = 0;
                } else if (ndsViewId === 1) {
                    productTax = productTotal * taxRate;
                } else {
                    productTax = productTotal * taxRate / (1 + taxRate);
                }

                if (productBasePrice > productPrice) {
                    productDiscount = (productBasePrice - productPrice) * productCount;
                }

                discount += productDiscount;
                total += productTotal;
                tax += productTax;

                weight += productWeight * productCount;
                volume += productVolume * productCount;
            });

            amount = (ndsViewId === 1) ? total + tax : total;

            $('.table-resume .discount_sum').text(discount.toFixed(2));
            $('.table-resume .total_price').text(total.toFixed(2));
            $('.table-resume .including_nds').text(tax.toFixed(2));
            $('.table-resume .amount').text(amount.toFixed(2));
            $('.table-resume .total_weight').text(weight.toFixed(productWeightPrecision));
            $('.table-resume .total_volume').text(volume.toFixed(productVolumePrecision));
        },
        getItem: function (id) {
            return this.$invoiceTable.find('#model_' + id);
        },
        recalculateItemPrice: function ($input) {
            var priceOne;
            var precision = parseInt($("#invoice-price_precision").val() || 2);
            var ratio = Math.pow(10, precision);
            var $row = $input.closest('tr');
            var $priceInput = $row.find('.price-input');
            var $discountInput = $row.find('.discount-input');
            var $markupInput = $row.find('.markup-input');
            var price = Math.round($priceInput.val() * ratio) / ratio || 0;
            var discount = 0;
            var $countInput = $row.find('.product-count');
            var countValue = $countInput.val() || 0;

            var count;
            if ($countInput.attr('type') == 'number') {
                count = countValue || 0;
            } else {
                if ($countInput.data('hour') && countValue.includes(':')) {
                    var countItems = countValue.split(':', 3);
                    countItems.forEach(function(el, i) {
                        if (i == 0) {
                            count = el;
                        } else if (i == 1) {
                            count = (count * 1 + (el.substring(0, 2) * 1) / 60).toFixed(2);
                        } else if (i == 2) {
                            count = (count * 1 + (el.substring(0, 2) * 1) / 3600).toFixed(4);
                        }
                    });
                } else {
                    count = countValue.replace(',', '.').replace(/[^\d\.]/g, '').split('.', 2).join('.');
                }
                count = count || '0';
            }
            count = count.replace(/^\./,"0.").replace(/(\.0*)$/,"");
            if (count != countValue) {
                $countInput.val(count);
            }
            if ($('.discount-percent').is(':checked')) {
                discount = Math.min(99.9999, Math.round($discountInput.val() * 1000000) / 1000000 || 0);
            } else if ($('.discount-ruble').is(':checked')) {
                discount = Math.round($discountInput.val() * 10000) / 10000 || 0;
            }
            var markup = Math.min(9999.9999, Math.round($markupInput.val() * 10000) / 10000 || 0);
            if ($('#invoice-has_markup').prop('checked') || $('#pricelist-has_markup').prop('checked')) {
                discount = 0;
                priceOne = Math.round((price * (100 + markup) / 100) * ratio) / ratio;
            } else if ($('#invoice-has_discount').prop('checked') || $('#pricelist-has_discount').prop('checked')) {
                markup = 0;
                if ($('.discount-percent').is(':checked')) {
                    priceOne = Math.round((price * (100 - discount) / 100) * ratio) / ratio;
                } else if ($('.discount-ruble').is(':checked')) {
                    priceOne = Math.round((price - discount) * ratio) / ratio;
                }
            } else {
                priceOne = price;
            }
            if ($priceInput.val() != price) {
                $priceInput.val(price.toFixed(precision));
            }
            if ($markupInput.val() != markup) {
                $markupInput.val(markup);
            }
            $row.find('.price-one-with-nds').text(priceOne.toFixed(precision));
            $row.find('.price-with-nds').text((priceOne * count).toFixed(precision));
        },
        incrementProductCount: function ($item) {
            var $input = $item.find('.product-count');

            $input.val(parseInt($input.val()) + 1);

            this.recalculateItemPrice($input);
            this.recalculateInvoiceTable();
        },
        getItems: function () {
            return INVOICE.$invoiceTable.find('.product-row');
        },
        checkProductTypeEnabled: function () {
            var disabled = this.getItems().length != 0;
            var $radioArray = this.$productTypeBlock.find('input.invoice-production-type');

            this.$productTypeBlock.find('input.invoice-production-type-hidden')
                .val($radioArray.filter(':checked').val());
        },
        searchProduct: function (url) {
            var exists = $('input.selected-product-id').map(function (idx, elem) {
                return $(elem).val();
            }).get();
            var store_id = ($('input#invoice-store_id').length) ? $('input#invoice-store_id').val() : null;
            $.pjax({
                url: url,
                container: '#pjax-product-grid',
                data: {exists: exists, store_id: store_id},
                push: false,
                timeout: 10000,
                scrollTo: false,
            });
            $('#add-from-exists').modal('show');
        },
        saveNewProduct: function ($modal) {
            var form = $('#new-product-invoice-form').serialize();
            var productionType = 1;//INVOICE.getProductionType();

            $.post('/product/create-from-invoice?productionType=' + productionType, form, function (data) {

                INVOICE.addProductToTable(data);
            });
        },
        getProductionType: function () {
            return INVOICE.$productTypeBlock.find('input.invoice-production-type:checked').val();
        },
        addNewProduct: function (url, formData) {
            if (formData) {
                $.post(
                    url,
                    formData,
                    function (data) {
                        $('#add-new').modal();
                        if (data) {
                            if (data.header !== undefined) {
                                $('#add-new .modal-title').html(data.header);
                                $('#add-new .modal-body').html(data.body);
                                $('.col-sm-4.col-md-5.control-label.bold-text').next().addClass('inp_one_line-product');
                                var urlunits = $('#get-product-units-url').val();
                                $("#product-production_type").on("change", "input", function () {
                                    $('#new-product-invoice-form input, #new-product-invoice-form select').attr('disabled', false);
                                    $('#new-product-invoice-form input:radio:not(.md-radiobtn), #new-product-invoice-form input:checkbox:not(.md-check)').uniform("refresh");
                                    if (!$('#unknown_product_recognized_id').length)
                                        $('#new-product-invoice-form input[type="text"]').val('');
                                    $('.error-summary').remove();
                                    if ($(this).val() == '0') {
                                        $('.forProduct').addClass('selectedDopColumns');
                                        $('.forService').removeClass('selectedDopColumns');
                                        $('#product-goods-group_id').attr('disabled', true);
                                        $('#product-service-group_id').removeAttr('disabled');
                                        $('.field-product-product_unit_id').removeClass('required');
                                        $('.import-products').hide();
                                    } else {
                                        $('.forProduct').removeClass('selectedDopColumns');
                                        $('#product-goods-group_id').removeAttr('disabled');
                                        $('#product-service-group_id').attr('disabled', true);
                                        $('.forService').addClass('selectedDopColumns');
                                        $('.import-products').show();
                                        $('.field-product-product_unit_id').addClass('required');
                                    }
                                    if ($('#is_first_invoice').val() == '1') {
                                        if ($(this).val() == '0') {
                                            $('#product-title').attr('placeholder', 'Например: Доставка товара');
                                            $('#product-price_for_sell_with_nds').attr('placeholder', 'Например: 500 (указывать только цифры)');
                                        }
                                        if ($(this).val() == '1') {
                                            $('#product-title').attr('placeholder', 'Например: Доска обрезная');
                                            $('#product-price_for_sell_with_nds').attr('placeholder', 'Например: 1000 (указывать только цифры)');
                                        }
                                    }

                                    $.post(urlunits, {
                                        productType: $(this).val()
                                    }).done(function (data) {
                                        if (data.html) {
                                            $('#product-product_unit_id').html($(data.html).html());
                                        }
                                    });
                                });
                                if ($('#new-product-invoice-form #product-production_type input').length == 1) {
                                    $("#new-product-invoice-form #product-production_type input").click().prop("checked", true);
                                }
                                setTimeout( function() {
                                    $(document).trigger('createInvoice_productForm_load.userHelp');
                                }, 200);
                            } else if (data.items !== undefined) {
                                var prodCount = data.services + data.goods;

                                $('.order-add-static-items .service-count-value').text(data.services);
                                if (data.services > 0) {
                                    $('li.select2-results__option.add-modal-services').removeClass('hidden');
                                }
                                $('.order-add-static-items .product-count-value').text(data.goods);
                                if (data.goods > 0) {
                                    $('li.select2-results__option.add-modal-products').removeClass('hidden');
                                }
                                if ($('.add-exists-product.hidden').length && prodCount > 0) {
                                    $('.add-first-product-button').addClass('hidden');
                                    $('.add-exists-product.hidden').removeClass('hidden');
                                }

                                if ($('#unknown_product_recognized_id').length && $('#unknown_product_recognized_id').val()) {
                                    // add recognized product
                                    var current_recognized_id = $('#unknown_product_recognized_id').val();
                                    var pos = $('.unknown_product').filter('[data-recognized_id="' + current_recognized_id + '"]').first().parents('tr').index();
                                    INVOICE.replaceProductInTable(data.items, pos);
                                    $('#from-new-add-row').hide();
                                    $('#unknown_product_recognized_id').val('');
                                } else {
                                    INVOICE.addProductToTable(data.items);
                                    $('#from-new-add-row').hide();
                                }

                                $('#add-new').modal('hide');
                            } else if (data.id && $("#new_product_id").length) {
                                $('#add-new').modal('hide');
                                $("#new_product_id").val(data.id).trigger("change");
                            } else {
                                $('#add-new').modal('hide');
                            }
                        }
                    }
                );
            } else {
                $.get(
                    url,
                    function (data) {
                        if (data) {
                            if (data.header !== undefined) {
                                $('#add-new').modal('show');
                                $('#add-new .modal-title').html(data.header);
                                $('#add-new .modal-body').html(data.body);
                                return;
                            }
                        }
                        $('#add-new').modal('hide');
                    }
                );
            }
        },
        closeModalDetailsAddress: function (data) {
            $('span.username').text(data.type + ' "' + data.name + '"');
            $('#add-new').modal('hide');
            $('.your-company').hide();
            $('[name="Company[strict_mode]"]').remove();
            if ($('*').is('.company-strict-mode-error')) {
                $('.company-strict-mode-error').parent().remove();
            }
            $('.company-in-strict-mode').remove();
            if ($('.error-summary ul').children().length > 1) {
                $('li.strict-mode-error').remove();
            } else {
                $('.error-summary').hide();
            }
            $('.create-invoice').show();

            return true;
        },
        addNewContractor: function (url, postData) {
            $.ajax({
                url: url,
                type: postData ? 'post' : 'get',
                data: postData || {},
                success: function (data) {
                    if (data) {
                        $('#add-new').modal();

                        if (data.header !== undefined) {
                            $('#add-new .modal-title').html(data.header);
                            $('#add-new .modal-body').html(data.body);
                            $('.col-sm-4.col-md-5.control-label.bold-text').next().addClass('inp_one_line-product');
                            $("#contractor-face_type").on("change", "input", function () {
                                if (!$('.page-content.payment-order').length) {
                                    $('.error-summary').remove();
                                }
                                $('.forContractor').toggleClass('selectedDopColumns');
                                $('.date-picker').datepicker({
                                    keyboardNavigation: false,
                                    forceParse: false,
                                    language: 'ru',
                                    autoclose: true
                                });
                            });
                        } else {
                            $select = INVOICE.currentContractorSelect;
                            $('#add-new').modal('hide');

                            if (data['update-contractor']) {
                                $('#contractor_update_button').val(data['name-short'] + ' ' + data['name']);
                            } else if (data['name-short'].length > 0) {
                                console.log($select);
                                $select.append('<option data-new="1" value="' + data.id + '">' + data['name-short'] + ' ' + data.name + '</option>');
                            } else {
                                $select.append('<option data-new="1" value="' + data.id + '">' + data.name + '</option>');
                            }
                            $("#add-first-contractor").hide();
                            $("#select-existing-contractor").show();

                            let contractorVal = $select.val();
                            if (Array.isArray(contractorVal)) {
                                contractorVal.push(data.id);
                                $select.val(contractorVal).trigger("change");
                            } else {
                                $select.val(data.id).trigger("change");
                            }
                            INVOICE.currentContractorSelect = null;
                        }
                    }
                }
            });
        },
        tultipProduct: function (selector) {
            if ($.isFunction($().tooltipster)) {
                $(selector).tooltipster({
                    "multiple": true,
                    "theme": ['tooltipster-kub'],
                    "functionBefore": function (instance, helper) {
                        instance.content(helper.origin.value);
                    }
                });
            }
        },
        removeProductFromInvoiceId: {},
        removeProductFromInvoice: function () {
            var btn = INVOICE.removeProductFromInvoiceId;

            if ($(btn).hasClass('from-new')) {
                $('#from-new-add-row').hide();
            } else {
                $(btn).parents('tr:first').remove();
                INVOICE.recalculateInvoiceTable();
                INVOICE.checkProductTypeEnabled();
            }
            if ($('#invoice-is_invoice_contract').val() == 1) {
                checkContractEssenceTemplate();
            }
        },
        getCurrencyRate: function (currencyName, rateDate) {
            if (currencyName && currencyName != 'RUB' && rateDate) {
                if (INVOICE.CurrencyRateRequestData[currencyName] == undefined) {
                    INVOICE.CurrencyRateRequestData[currencyName] = {};
                }
                if (INVOICE.CurrencyRateRequestData[currencyName][rateDate] == undefined) {
                    INVOICE.CurrencyRateRequestData[currencyName][rateDate] = {};
                }
                if (!INVOICE.CurrencyRateRequestData[currencyName][rateDate].value) {
                    $.ajax({
                        type: 'post',
                        url: $('#invoice-currency_rate_type').data('geturl'),
                        data: {name: currencyName, date: rateDate},
                        async: false,
                        success: function (data) {
                            console.log(data);
                            if (data.name && data.name == currencyName && data.amount && data.value) {
                                INVOICE.CurrencyRateRequestData[currencyName][rateDate] = data;
                                $(".invoice-panel div.currency_rate_value").data("initial", data.value).text(data.value);
                            }
                        },
                        complete: function () {
                        },
                    });
                }
                return INVOICE.CurrencyRateRequestData[currencyName][rateDate];
            }
            return {amount: 1, value: 1};
        },
        changeInvoiceCurrency: function (input) {
            var $input = $(input);
            var defaultName = $input.data('default');
            var currencyName = $input.val();
            var rateDate = $('#invoice-currency_ratedate').val();
            var rate = INVOICE.getCurrencyRate(currencyName, rateDate);
            var comment = $('#invoice-comment').val();
            var defaultComment = $('#invoice-comment').data('currency');
            var newComment;
            $('#invoice-currency_rate_type input.default-type').prop('checked', true);
            $('#invoice-currency_rate_type input[type=radio]:not(.md-radiobtn)').uniform('refresh');
            if (rate && rate.amount && rate.value) {
                $('span.currency_rate_amount').html(rate.amount);
                $('input.currency_rate_amount').val(rate.amount);
                $('span.currency_rate_value').html(rate.value);
                $('input.currency_rate_value').val(rate.value);
                $('span.currency_name').html(currencyName);
            }
            if (currencyName == defaultName) {
                var documentDate = $('input.invoice_document_date').val();
                $('span.currency_rate_date').html(documentDate);
                $('input.currency_rate_date').val(documentDate);
                $('#currency-rate-box').collapse('hide');
                $('.currency-view-toggle').collapse('hide');
                $('#invoice-comment').val('');
                $('.total_currency_label').html('');
                if (comment != '') {
                    newComment = comment.replace(new RegExp(defaultComment + '[\s\n]*'), '').trim();
                    $('#invoice-comment').val(newComment);
                }
            } else {
                $('.total_currency_label').html(' (' + currencyName + ')');
                if (comment == '' || comment.search(new RegExp(defaultComment.substring(0, 50))) == -1) {
                    newComment = (comment != '' ? comment + "\n" : '') + defaultComment;
                    $('#invoice-comment').val(newComment);
                }
                $('.currency-view-toggle').collapse('show');
            }
        },
        changeDocumentDate: function (date) {
            var $nameInput = $('#invoice-currency_name');
            var currencyName = $nameInput.val();
            var defaultName = $nameInput.data('default');
            $('span.currency_rate_date').html(date);
            $('input.currency_rate_date').val(date);
            if (currencyName != defaultName) {
                INVOICE.changeCurrencyRateDate(currencyName, date);
            }
        },
        changeCurrencyRateDate: function (currencyName, rateDate) {
            $('span.currency_rate_date').html(rateDate);
            var rate = INVOICE.getCurrencyRate(currencyName, rateDate);
            if (rate && rate.amount && rate.value) {
                $('span.currency_rate_amount').html(rate.amount);
                $('input.currency_rate_amount').val(rate.amount);
                $('span.currency_rate_value').html(rate.value);
                $('input.currency_rate_value').val(rate.value);
            }
        },
        changeCurrencyAmount: function (input) {
            var $input = $(input);
            var inputValue = $input.val();
            var value = inputValue.match(/1[0]{0,9}/) || '1';
            if (inputValue !== value) {
                $input.val(value);
            }
            $('span.currency_amount').html(value);
            $('input.currency_amount').val(value);
        },
        changeCurrencyRate: function (input) {
            var $input = $(input);
            var str = $input.val();
            var value = str.replace(/,/, '.').match(/(0|[1-9][0-9]{0,9})(\.[0-9]{0,4})?/);
            value = value ? value[0] : '';
            if (str != value) {
                $input.val(value);
            }
            value = parseFloat(value).toFixed(4);
            $('span.currency_rate').html(value);
            $('input.currency_rate').val(value);
        },
        setInvoiceCurrencyRate: function ($context, data) {
            $('#currency-rate-box', $context).collapse('hide');
            $.ajax({
                type: 'post',
                url: $('#invoice-currency_rate_type').data('seturl'),
                data: data,
                async: false,
                success: function (data) {
                    if (data.content) {
                        $context.html(data.content);
                        $('input[type=radio]:not(.md-radiobtn)', $context).uniform();
                        $('.collapse', $context).collapse({toggle: false});
                        $('#invoice-currency_ratedate').datepicker({
                            format: 'dd.mm.yyyy',
                            language: 'ru',
                            autoclose: true,
                            endDate: new Date(),
                        });
                    }
                },
                complete: function () {
                },
            });
        },
        setPayLimitInfo: function (input) {
            if ($('#invoice-comment').length > 0) {
                var $input = $(input);
                var newComment;
                var comment = $('#invoice-comment').val();
                var defaultComment = $('#invoice-comment').data('paylimit')
                    .replace('{{date}}', $('#invoice-payment_limit_date').val());
                if ($input.is(':checked')) {
                    var search = $('#invoice-comment').data('paylimit')
                        .replace('{{date}}', '');
                    if (comment == '' || comment.search(new RegExp(search)) == -1) {
                        newComment = (comment != '' ? comment + "\n" : '') + defaultComment;
                        $('#invoice-comment').val(newComment);
                    }
                } else {
                    var pattern = $('#invoice-comment').data('paylimit')
                        .replace('{{date}}.', '\\d{2}\\.\\d{2}\\.\\d{4}\\.[\\s\\n]*');
                    if (comment != '') {
                        newComment = comment.replace(new RegExp(pattern), '').trim();
                        $('#invoice-comment').val(newComment);
                    }
                }
            }
        },
        setContractPayLimitInfo: function (input) {
            if ($('#invoice-contract_essence').length > 0) {
                var $input = $(input);
                var newComment;
                var comment = $('#invoice-contract_essence').val();
                var defaultComment = $('#invoice-contract_essence').data('paylimit')
                    .replace('{{date}}', $('#invoice-payment_limit_date').val());
                if ($input.is(':checked')) {
                    var search = $('#invoice-contract_essence').data('paylimit')
                        .replace('{{date}}', '');
                    if (comment == '' || comment.search(new RegExp(search)) == -1) {
                        newComment = (comment != '' ? comment + "\n" : '') + defaultComment;
                        $('#invoice-contract_essence').val(newComment);
                    }
                } else {
                    var pattern = $('#invoice-contract_essence').data('paylimit')
                        .replace('{{date}}.', '\\d{2}\\.\\d{2}\\.\\d{4}\\.[\\s\\n]*');
                    if (comment != '') {
                        newComment = comment.replace(new RegExp(pattern), '').trim();
                        $('#invoice-contract_essence').val(newComment);
                    }
                }
            }
        },
        getInvoicesWithAmountOnDate: function (url, id, date) {
            if (INVOICE.InvoiceAmountRequestData[id] == undefined) {
                INVOICE.InvoiceAmountRequestData[id] = {};
            }
            if (INVOICE.InvoiceAmountRequestData[id][date] == undefined) {
                INVOICE.InvoiceAmountRequestData[id][date] = {};
                $.ajax({
                    type: 'post',
                    url: url,
                    data: {'id': id, 'date': date},
                    async: false,
                    success: function (data) {
                        if (data.result) {
                            INVOICE.InvoiceAmountRequestData[id][date] = data;
                        }
                    },
                    complete: function () {
                    },
                });
            }
            return INVOICE.InvoiceAmountRequestData[id][date];
        },
        getRecognizeLink: function (item, txt, cls) {
            var rid = item.recognized_id;
            var quantity = item.quantity || 0;
            return '<a href="#" style="color:red" class="link unknown_product ' + cls + '" data-recognized_id="' + rid + '" data-quantity="' + quantity + '">' + txt + '</a>';
        },
        getProductInsertBeforeElement: function($insertedBlock) {

            var ret = '#from-new-add-row';
            var newTitle = $insertedBlock.find('.product-title').val();
            var newProductionType = $insertedBlock.find('.product-production-type').val();

            $('#table-for-invoice tbody').find('tr').each(function() {
                if ($(this).find('.product-title').length) {
                    var currTitle = $(this).find('.product-title').val();
                    var currProductionType = $(this).find('.product-production-type').val();

                    if (newProductionType < currProductionType)
                        return true;
                    if (newProductionType > currProductionType) {
                        ret = $(this);
                        return false;
                    }

                    if (newTitle.localeCompare(currTitle, 'ru') <= 0) {
                        ret = $(this);
                        return false;
                    }
                }
            });

            return ret;
        }
    };

    $(document).on("change", "input.invoice_document_date", function () {
        INVOICE.changeDocumentDate($(this).val());
    });

    $(document).on("change", "#invoice-currency_name", function () {
        INVOICE.changeInvoiceCurrency(this);
    });

    $(document).on("change", "input.currency-rate-type-radio", function () {
        if (!$('#radio_currency_rate_custom').is(':checked')) {
            INVOICE.changeCurrencyRateDate($('#invoice-currency_name').val(), $('#invoice-currency_ratedate').val());
        }
        $('input.currency-custom-input').prop('disabled', !$('#radio_currency_rate_custom').is(':checked'));
    });

    $(document).on("change", "#invoice-currency_ratedate", function () {
        INVOICE.changeCurrencyRateDate($('#invoice-currency_name').val(), $(this).val());
    });

    $(document).on("input", "#invoice-currencyamountcustom", function () {
        INVOICE.changeCurrencyAmount(this);
    });
    $(document).on("input", "#invoice-currencyratecustom", function () {
        INVOICE.changeCurrencyRate(this);
    });

    $(document).on("click", "#currency_rate_apply", function () {
        var $context = $('#invoice_currency_rate_box');
        var data = $('input', $context).serialize();
        INVOICE.setInvoiceCurrencyRate($context, data);
    });

    $(document).on("click", "#currency_rate_cancel", function () {
        var $context = $('#invoice_currency_rate_box');
        INVOICE.setInvoiceCurrencyRate($context, {});
    });

    $(document).on("change", "#invoice-show_paylimit_info", function () {
        INVOICE.setPayLimitInfo(this);
        INVOICE.setContractPayLimitInfo(this);
    });

    $(document).on("change", "#invoice-hasnds", function () {
        var hasNds = $(this).is(":checked");
        var ndsViewId = hasNds ? 0 : 2;
        $(".with-nds-item").toggleClass("hidden", !hasNds);
        $(".without-nds-item").toggleClass("hidden", hasNds);
        $("#invoice-nds_view_type_id").val(ndsViewId);
        $("#invoice-nds_view_type_id").data('id', ndsViewId);
        $(".nds-view-item").addClass("hidden");
        $(".nds-view-item.type-" + ndsViewId).removeClass("hidden");
        INVOICE.recalculateInvoiceTable();
    });

    $(document).on('change', 'select#invoice-contractor_id', (function () {
        if ($(this).val() == 'add-modal-contractor') {
            let url = $(this).data('url');
            //url = URI.addTokenParam(url);
            $(this).val(null).trigger("change");
            INVOICE.currentContractorSelect = $(this);
            INVOICE.addNewContractor(url);
        }
    }));
    $(document).on('change', 'select#order-add-select', (function () {
        let value = this.value;
        if (parseInt(value)) {
            $(this).val(null).trigger("change");
            let url = $(this).data('get-url');
            //url = URI.addTokenParam(url);
            $.post(url, {in_order: [value]}, function (data) {
                INVOICE.addProductToTable(data);
                $('#from-new-add-row').hide();
            });
        }
    }));
    $(document).on('click', '.add-modal-product-new', function () {
        let url = $(this).data('url');
        //url = URI.addTokenParam(url);
        $('#order-add-select').select2('close');
        INVOICE.addNewProduct(url);
    });
    $(document).on('click', '.add-modal-products', function () {
        let url = $(this).data('url');
        //url = URI.addTokenParam(url);
        $('#order-add-select').select2('close');
        INVOICE.searchProduct(url);
    });
    $(document).on('click', '.add-modal-services', function () {
        let url = $(this).data('url');
        //url = URI.addTokenParam(url);
        $('#order-add-select').select2('close');
        INVOICE.searchProduct(url);
    });

    $(document).on('submit', '#new-product-invoice-form', function (event) {
        event.preventDefault();
        INVOICE.addNewProduct(this.action, $(this).serialize());
    });

    $(document).on('submit', '#new-contractor-invoice-form', function (event) {
        event.preventDefault();
        var url = this.action;
        var data = $(this).serialize();
        INVOICE.addNewContractor(url, data);
    });

    // select product from list and add to invoice
    $('#add-from-exists').on('shown.bs.modal', function () {
        INVOICE.productSeletedToInvoice = [];
    });
    $(document).on('click', '#pjax-product-grid a:not(.store-opt)', function (e) {
        e.preventDefault();
        var $product = INVOICE.productSeletedToInvoice;
        $.pjax.reload('#pjax-product-grid', {
            url: $(this).attr('href'),
            type: 'post',
            data: {product: $product},
            push: false,
            replace: false,
            timeout: 5000
        });
    });
    $(document).on('change', '#products_in_order .product_selected', function () {
        var prodId = $(this).val();
        if (this.checked) {
            INVOICE.productSeletedToInvoice.push(prodId);
        } else {
            INVOICE.productSeletedToInvoice = jQuery.grep(INVOICE.productSeletedToInvoice, function (value) {
                return value != prodId;
            });
        }
    });
    $(document).on('change', '#products_in_order .product_selected_one', function () {
        var prodId = $(this).val();
        if (this.checked) {
            INVOICE.productSeletedToInvoice = [prodId];
        } else {
            if (INVOICE.productSeletedToInvoice[0] == prodId) {
                INVOICE.productSeletedToInvoice = [];
            }
        }
    });
    $(document).on('click', '#add-to-invoice-button', function (e) {
        e.preventDefault();
        let url = $(this).data('url');
        //url = URI.addTokenParam(url);
        $.post(url, {in_order: INVOICE.productSeletedToInvoice}, function (data) {
            INVOICE.addProductToTable(data);
            INVOICE.productSeletedToInvoice = [];
            $('#from-new-add-row').hide();
            $('#add-from-exists').modal('hide');
        });
    });
    $(document).on('hidden.bs.modal', '#add-from-exists', function () {
        INVOICE.productSeletedToInvoice = [];
    });
    // end select product ============================================

    $(document).on("click", "#discount_submit", function (e) {
        var $input = $("#invoice_all_discount input#all-discount");
        var val = Math.max(Math.min(parseFloat($input.val()), 99.9999), 0);
        if (val != $input.val()) {
            $input.val(val);
        }
        $("#invoice_all_discount").addClass("hidden");
        $("table .markup-input").val(0);
        $("table .discount-input").val(val).each(function () {
            INVOICE.recalculateItemPrice($(this));
        });
        INVOICE.recalculateInvoiceTable();
    });

    $(document).on("click", "#markup_submit", function (e) {
        var $input = $("#invoice_all_markup input");
        var val = Math.max(Math.min(parseFloat($input.val()), 9999.9999), 0);
        if (val != $input.val()) {
            $input.val(val);
        }
        $("#invoice_all_markup").addClass("hidden");
        $("table .discount-input").val(0);
        $("table .markup-input").val(val).each(function () {
            INVOICE.recalculateItemPrice($(this));
        });
        INVOICE.recalculateInvoiceTable();
    });

    $(document).on('change', '#invoice-has_discount', function () {
        if ($(this).prop('checked')) {
            $('#invoice-has_markup').prop('checked', false);
            $('#invoice-has_markup:not(.md-check)').uniform('refresh');
            $('.markup_column').addClass('hidden');
            $("#invoice_all_markup input").val(0);
            $('.markup-input').val(0);
            $('.discount_column').removeClass('hidden');
        } else {
            $('.discount_column').addClass('hidden');
            $("#invoice_all_discount input").val(0);
            $('.discount-input').val(0);
            $('#discount_sum').text('0.00');
        }
        $('tr input.price-input:enabled').each(function () {
            INVOICE.recalculateItemPrice($(this));
        });
        INVOICE.recalculateInvoiceTable();
    });

    $(document).on('change', '#invoice-has_markup', function () {
        if ($(this).prop('checked')) {
            $('#invoice-has_discount').prop('checked', false);
            $('#invoice-has_discount:not(.md-check)').uniform('refresh');
            $('.discount_column').addClass('hidden');
            $("#invoice_all_discount input").val(0);
            $('.discount-input').val(0);
            $('#discount_sum').text('0.00');
            $('.markup_column').removeClass('hidden');
        } else {
            $('.markup_column').addClass('hidden');
            $("#invoice_all_markup input").val(0);
            $('.markup-input').val(0);
        }
        $('tr input.price-input:enabled').each(function () {
            INVOICE.recalculateItemPrice($(this));
        });
        INVOICE.recalculateInvoiceTable();
    });

    $(document).on('change', '#invoice-has_weight', function () {
        if ($(this).prop('checked')) {
            $('.weight_column').removeClass('hidden');
        } else {
            $('.weight_column').addClass('hidden');
        }
        $('tr input.price-input:enabled').each(function () {
            INVOICE.recalculateItemPrice($(this));
        });
        INVOICE.recalculateInvoiceTable();
    });

    $(document).on('change', '#invoice-has_volume', function () {
        if ($(this).prop('checked')) {
            $('.volume_column').removeClass('hidden');
        } else {
            $('.volume_column').addClass('hidden');
        }
        $('tr input.price-input:enabled').each(function () {
            INVOICE.recalculateItemPrice($(this));
        });
        INVOICE.recalculateInvoiceTable();
    });

    INVOICE.$invoiceTable.on('click', '.remove-product-from-invoice', function () {
        $('#modal-remove-one-product').modal("show");
        INVOICE.removeProductFromInvoiceId = this;
    });

    $(document).on('click', '#modal-remove-one-product .yes', function (e) {
        INVOICE.removeProductFromInvoice();
    });

    $(document).on("click", "#invoice_price_conf button", function () {
        var $btn = $(this);
        var $table = $btn.closest("table");
        var precision = parseInt($btn.val());
        $("#invoice-price_precision").val(precision);
        $("#table-product-list-body .product-row input.price-input").each(function () {
            $(this).val(parseFloat($(this).val()).toFixed(precision));
            INVOICE.recalculateItemPrice($(this));
        });
        INVOICE.recalculateInvoiceTable();
        $("#invoice_price_conf").addClass("hidden");
        $("#invoice_price_conf button").addClass("yellow").removeClass("yellow-text");
        $btn.removeClass("yellow").addClass("yellow-text");
    });

    INVOICE.$invoiceTable.on('keyup change', '.price-input, .discount-input, .markup-input, .weight-input, .volume-input', function (e) {
        INVOICE.recalculateItemPrice($(this));
        INVOICE.recalculateInvoiceTable();
    });

    INVOICE.$invoiceTable.on('blur', '.product-count', function () {
        INVOICE.recalculateItemPrice($(this));
        INVOICE.recalculateInvoiceTable();
    });

    INVOICE.checkProductTypeEnabled();

    $(document).on('change', '#invoice-nds_view_type_id', function () {
        $(this).data('id', this.value);
        INVOICE.recalculateInvoiceTable();
    });

    $(document).on('click', 'span.btn-add-line-table', function () {
        $('#from-new-add-row').show();
    });

    /* end invoice event */

    $(document).on('click', '.addColumns', function () {
        $(this).closest('form').find('.dopColumns').toggleClass('selectedDopColumns');
    });

    INVOICE.tultipProduct('.tooltip-product');

    $(document).on('change', '#invoice-document_date', function () {
        let $payInput = $('#invoice-payment_limit_date', this.form);
        if ($payInput.length > 0) {
            var date = moment($(this).val(), "DD.MM.YYYY");
            if (date.isValid()) {
                date.add(parseInt($payInput.data('delay')), 'days');
                $payInput.kvDatepicker("update", date.format('DD.MM.YYYY'));
            }
        }
    });
});