<?php

namespace frontend\modules\bitrix24\modules\iframe\assets;

use yii\web\AssetBundle;

/**
 * InvoiceFormAsset
 */
class InvoiceFormAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/modules/bitrix24/modules/iframe/assets/web';

    /**
     * @var array
     */
    public $css = [
        'css/invoice-form.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'js/invoice-form.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'frontend\modules\bitrix24\modules\iframe\assets\IframeAsset',
    ];
}
