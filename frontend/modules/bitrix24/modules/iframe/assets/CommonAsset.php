<?php

namespace frontend\modules\bitrix24\modules\iframe\assets;

use Yii;
use yii\web\AssetBundle;
use yii\web\View;

/**
 * CommonAsset
 */
class CommonAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/modules/bitrix24/modules/iframe/assets/web';

    /**
     * @var array
     */
    public $css = [
        'css/tooltipster-kub.css',
        'css/style.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'js/common.js',
    ];

    /**
     * @var array
     */
    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\widgets\PjaxAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
        'common\assets\TooltipsterAsset',
        'kartik\date\DatePickerAsset',
        'common\assets\MomentAsset',
    ];

    public function init()
    {
        parent::init();

        // prevent load Bootstrap3 files
        Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapThemeAsset'] = ['css' => [],'js' => []];
        Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapPluginAsset'] = ['css' => [],'js' => []];
        Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = ['css' => [],'js' => []];
    }
}
