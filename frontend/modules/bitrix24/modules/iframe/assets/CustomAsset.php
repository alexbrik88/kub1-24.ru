<?php

namespace frontend\modules\bitrix24\modules\iframe\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * CustomAsset
 */
class CustomAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/modules/bitrix24/modules/iframe/assets/web';

    /**
     * @var array
     */
    public $css = [
    ];

    /**
     * @var array
     */
    public $js = [
        'js/script.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'frontend\modules\bitrix24\modules\iframe\assets\IframeAsset',
    ];
}
