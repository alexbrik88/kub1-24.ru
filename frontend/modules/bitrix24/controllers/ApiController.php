<?php

namespace frontend\modules\bitrix24\controllers;

use frontend\modules\bitrix24\models\SoloAppHostApps;
use frontend\modules\bitrix24\models\SoloApplications;
use frontend\modules\bitrix24\models\SoloClients;
use frontend\modules\bitrix24\models\SoloPurchasedApplications;
use Yii;
use yii\web\Controller;
use yii\web\Response;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\HttpClient\HttpClient;

class ApiController extends Controller
{

    //TODO: переделать - что в данном модуле часть функций надо вынести в отдельный класс
    // public $serverUrl = 'https://kub.solo-it.ru';//Вывели данные о текущем URL
    public $settings = [];//Настройки текущего открытого приложения
    public $app;//Строчка из таблицы Application
    public $appInfobitrix24; // Информация возвращаемая самим Битриксом
    private \Bitrix24\SDK\Core\ApiClient $bitrix24App;

    public $layout = 'install';
    public $enableCsrfValidation = false;//Выключаем проверку cors

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'Bitrix24AccessFilter' => [
                'class' => 'frontend\modules\bitrix24\components\Bitrix24AccessFilter',
                'only' => [
                    'install',
                ],
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'install' => ['POST'],
                    'install-step' => ['POST'],
                ],
            ],
        ];
    }

    public function getServerUrl()
    {
        return Yii::$app->request->getHostInfo();
    }

    //Функция возвращает JSON ответ - если что-то пошло не так
    private function lost($error = 1){
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'error' => $error,
            'message' => 'Ошибка. Что-то пошло не так'
        ];
    }

    //Функция возвращает демо данные
    public function getAppSettingsDemo(){
        //TODO: Сделать так, чтобы мы могли тестировать на клиентских токенах, чтобы отлавливать ошибку
        $arr = [
            "app_server" => "kub.solo-it.ru",
            "domain" => "money-it.bitrix24.ru",
            "protocol" => 1,
            "lang" => "ru",
            "app_sid" => "ec933d681cfe1fde4b2dbf2172545ef2",
            "auth_id" => "c6610047fda20057c85000000001100e037da9fdff5dfaf6e1cc208af20486ddd4",
            "auth_expires" => 3600,
            "refresh_id" => "d0ed610047fda20057c85000000001100e03e746f48320b5de1af722c786c56350de",
            "member_id" => "a671913658e945a3c6dce712b0",
            "status" => "F",
            "placement" => "DEFAULT",
            "placement_options" => "{\"any\":\"7\\/\"}",
            "app_code" => "app1",
        ];
        return $arr;
    }
    //Функция из get и post запроса вытаскивает всю информацию о приложении
    public function getAppSettings($isTest = false)
    {
        $data_get = Yii::$app->request->get();
        $data_post = Yii::$app->request->post();
        $arr = [
                //Get запросы
            'domain' => $data_get['DOMAIN'] ?? null,
            'protocol' => $data_get['PROTOCOL'] ?? null,
            'lang' => $data_get['LANG'] ?? null,
            'app_sid' => $data_get['APP_SID'] ?? null,
                //Post запросы
            'auth_id' => $data_post['AUTH_ID'] ?? null,
            'auth_expires' => $data_post['AUTH_EXPIRES'] ?? null,
            'refresh_id' => $data_post['REFRESH_ID'] ?? null,
            'member_id' => $data_post['member_id'] ?? null,
            'status' => $data_post['status'] ?? null,
            'placement' => $data_post['PLACEMENT'] ?? null,
            'placement_options' => $data_post['PLACEMENT_OPTIONS'] ?? null,
        ];

        if($isTest) { // только если это тестовые данные
            $arr = $this->getAppSettingsDemo();//Загружаем данные демо
        }
        foreach ($arr as $value) {
            if (!$value) return false;//т.е. что-то не передано из ключей
        }
        $this->settings = $arr;//Сохраняем настройки
        return true;//И возвращаем что у нас все Окей!

    }



    public function actionInstall()
    {
        // Аналог install_solo_app_interface2.php
        $appCode = Yii::$app->request->get('app');//В старой версии было "code"
        if(!$appCode) return $this->lost(2235);//Не указано установочное приложение
        if(!$this->getAppSettings(false)) return $this->lost(8238);//Значит нам не передали все параментры приложения
        $this->settings['app_code'] = $appCode;//TODO: Проверить - а в таблице приложений у нас есть ли такое приложение с данным кодом?
        if(!$app = $this->getCurrentApp($appCode)) return $this->lost(7649);//Мы не нашли такого приложения в таблице

        $setting = new \stdClass();//Подготовка настроек для Angular приложения
        $setting->app_name = $app['name'];
        $setting->app_sid = $this->settings['app_sid'];
        $setting->protocol = $this->settings['protocol'];
        $setting->domain = $this->settings['domain'];
        $setting->LICENCE_LINK = $this->serverUrl . "/policy/privacy.php";
        $setting->BACKEND_URL = $this->serverUrl . "/bitrix24/api";
        $setting->token = $this->genJwtToken();

        return $this->render('install',['setting' => $setting]);
    }

    //Генерирует Jwt токен из settings для Angular приложения
    public function genJwtToken()
    {
        $jwt = Yii::$app->b24jwt;//Используем класс b24jwt а не jwt - так как на этом проекте уже есть jwt - и тут надо
        // нам отдельный токен для приложений Б24
        $time = time();
        $token = $jwt->getBuilder()
            ->issuedBy($this->serverUrl) // Configures the issuer (iss claim)
            ->permittedFor($this->serverUrl) // Configures the audience (aud claim)
            ->identifiedBy('4f1g23a12aa', true) // Configures the id (jti claim), replicating as a header item
            ->issuedAt($time) // Configures the time that the token was issue (iat claim)
            ->canOnlyBeUsedAfter($time + 60) // Configures the time that the token can be used (nbf claim)
            ->expiresAt($time + 3600) // Configures the expiration time of the token (exp claim)
            ->withClaim('app_server', $_SERVER['SERVER_NAME']); // Configures a new claim, APP_SERVER
        foreach ($this->settings as $key => $value){
            $token->withClaim($key, $value);//Все настройки сохраняем
        }
        $signer = $jwt->getSigner('HS256');
        $key = $jwt->getKey();
        return $token->getToken($signer, $key); // Retrieves the generated token;
    }

    public function getDataFromJwtToken(){
        if(!$this->settings->token) return false;
        $token = Yii::$app->b24jwt->getParser()->parse((string) $this->settings->token); // Parses from a string
        $arr = []; // Retrieves the token claims
        foreach ($token->getClaims() as $key => $obj){
            $arr[$key] = $obj->getValue();
        }
        $this->settings->claims = $arr;
        return $arr;
    }

    public function isValidateJwt(){
        //TODO: Надо сделать проверку или вынести на уровень Yii
        return true;
    }

    //Функция забирает из php://input данные с Ангулара
    public function getData(){
        $data = file_get_contents('php://input');
        $data = json_decode($data);
        $this->settings = $data;
        return $data;
    }

    public function actionInstallStep()
    {

        //Аналог main/app.install2
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = $this->getData();//Получаем данные от Angular приложения
        if(!$this->isValidateJwt()) return $this->lost(8354);//Проверяем Валидность Jwt
        if(!$tokenData = $this->getDataFromJwtToken()) return $this->lost(9394);//парсим JWT чтобы получить данные из токена

        $arResult = [];

        //Загружаем данные текущего приложения из БД по коду из токена
        if(!$app = $this->getCurrentApp($tokenData['app_code'])) return $this->lost(8374);//Мы не нашли такого приложения в таблице

        $step = $data->step;//получаем текущий шаг, который нам прислал angular
        $portal = $tokenData['domain'];//Вытаскиваем портал, на котором стоит приложение





        switch ($step) {
            case 0:// Поиск клиента portal
                $client = $this->getClientByPortal();
                if($client){
                    $arResult['message'] = "Найдена компания '{$client['title']}'";
                    $arResult['companyId'] = $client['id'];
                    $arResult['next'] = 2;
                }else{
                    $arResult['message'] = "Клиент не найден";
                    $arResult['next'] = 1;
                }
                break;
            case 1://создание клиента
                $apiClient = $this->makebitrix24App();
                $result = $apiClient->getResponse('user.current');
                $user_id = json_decode($result->getContent(), true)['result']['ID'] ?? null;
                $add = new SoloClients();
                $add->title = $portal;
                $add->portal = $portal;
                $add->data = '';
                $add->user_id = $user_id;
                $isAdd = $add->save();
                if($isAdd){
                    $arResult['message'] = "Клиент записан";
                    $arResult['companyId'] = $add->id;
                }else {
                    $arResult['error'] = "Ошибка при записи клиента.";
                }
                $arResult['next'] = ++$step;
                break;
            case 2://поиск пользователя - Админа портала пол id=1
                $arResult['next'] = 3;
                $apiClient = $this->makebitrix24App();
                $result = $apiClient->getResponse('app.info');
                $arResult['bitrix24AppInfo'] = json_decode($result->getContent(), true);//Получаем данные приложения
                $result = $apiClient->getResponse('scope');
                $arResult['scope'] = json_decode($result->getContent(), true);//Получаем данные о правах
                $result = $apiClient->getResponse('user.get',['FILTER'=>['ID'=>1]]);
                $arResult['user1'] = json_decode($result->getContent(), true);//Получаем данные о правах
                $uc = $arResult['user1']['result'][0];//Получили 1 пользователя портала





                $by = [];
                $order = [];

                $login = "admin_" . preg_replace("/[^a-zA-z\d]/", "_", $tokenData['domain']);
                $arResult['login'] = $login;
                //Далее можно искать в таблице пользователей
//                $user = Clients::find()
//                    ->where([
//                        "=login" => $login,
//                        'company_id' => $params['companyId']
//                    ])
//                    ->all();
//                $id = $user["id"];
//                if ($id) {
//                    $user = Clients::findOne($params['companyId']);
//                    $user->user_id = $id;
//                    $user->save();
//
//                    $arResult['result'] = [
//                        'message' => "Пользователь найден '" . implode(' ', [$user['name'], $user['last_name']]) . " <{$user['email']}>'",
//                        'data' => [
//                            'userId' => $id
//                        ]
//                    ];
//                    $arResult['next'] = 4;
//                } else {
//                    $arResult['result'] = [
//                        'message' => "Пользователь не найден",
//                    ];
//                    $arResult['next'] = 3;
//                }
                break;
            case 3://создание главного пользователя
                $arResult['next'] = 4;
//                $result = Clients::find()
//                    ->where([
//                        'id' => '1'
//                    ])
//                    ->one();
//                $uc = $result['result'][0];
//                $err5 = $result['result'];
//                $Cuser = new Clients();
//                $login = "admin_" . preg_replace("/[^a-zA-z\d]/", "_", $data['claims']['DOMAIN']);
//                $Cuser->name = $uc['NAME'];
//                $Cuser->last_name = $uc['LAST_NAME'];
//                $Cuser->login = $login;
//                $Cuser->email = $uc['EMAIL'];
//                $Cuser->lid = SITE_ID;
//                $Cuser->active = "Y";
//                $Cuser->password = $uc['EMAIL'] . "!asdqfqfe";
//                $Cuser->confirm_password = $uc['EMAIL'] . "!asdqfqfe";
//                $Cuser->personal_phone = $uc['PERSONAL_PHONE'];
//                $Cuser->work_phone = $uc['WORK_PHONE'];
//                $Cuser->work_position = $uc['WORK_POSITION'];
//                $Cuser->personal_city = $uc['PERSONAL_CITY'];
//                $Cuser->bx24_id = $uc['ID'];
//                $Cuser->company_id = $params['companyId'];
//                $Cuser->save();
//
//                $id = $Cuser->id;
//                if ($id) {
//                    $result = SoloClients::findOne($params['companyId']);
//                    $result->user_id = $id;
//                    $result->save();
//
//                    $arResult['result'] = [
//                        'message' => "Пользователь добавлен '" . implode(' ', [$uc['NAME'], $uc['LAST_NAME']]) . " <{$uc['EMAIL']}>'",
//                        'data' => [
//                            'userId' => $id
//                        ]
//                    ];
//                    $arResult['next'] = ++$step;
//                } else {
//                    $arResult['error'] = "Ошибка при добавлении пользователя";
//                    $arResult['error2'] = $Cuser->LAST_ERROR;
//                    $arResult['error3'] = $login = "admin_" . preg_replace("/[^a-zA-z\d]/", "_", $data['claims']['DOMAIN']);
//                    $arResult['error4'] = $result['result'];
//                    $arResult['error5'] = $err5;
//                }
                break;
            case 4://check
                $arResult['next'] = 5;
                $soloApp = SoloApplications::find()
                    ->where(['code' => $tokenData['app_code']])
                    ->one();
                $purchased = SoloPurchasedApplications::find()
                    ->where(['app_id' => $soloApp["id"]])
                    ->one();
                $arResult['app'] = [
                    $soloApp,
                    $purchased
                ];
//                if (!$purchased) {
//                    $arResult['result'] = [
//                        'message' => "Ok. Запуск установки разрешен.",
//                    ];
//                    $arResult['next'] = ++$step;
//                } else {
//                    $arResult['result'] = [
//                        'message' => "Приложение было ранее установлено. Переустановка",
//                        'data' => [
//                            'purchased' => $purchased['ID']
//                        ]
//                    ];
//                    $arResult['next'] = 6;
//                }
                break;
            case 5://install - Первичная установка
                $arResult['next'] = 6;


                $apiClient = $this->makebitrix24App();

//                $url = 'https://dev.kub-24.ru/bitrix24/iframe/default/index';
                $url = $this->getServerUrl() . '/bitrix24/iframe/default/index';
                $result = $apiClient->getResponse('placement.bind',['PLACEMENT'=>'CRM_LEAD_LIST_MENU', 'HANDLER' => $url]);
                $arResult['crm_placement_lead_menu'] = json_decode($result->getContent(), true);
                $result = $apiClient->getResponse('placement.bind',['PLACEMENT'=>'CRM_DEAL_LIST_MENU', 'HANDLER' => $url]);
                $arResult['crm_placement_deal_menu'] = json_decode($result->getContent(), true);
                $result = $apiClient->getResponse('placement.bind',['PLACEMENT'=>'CRM_LEAD_DETAIL_TAB', 'HANDLER' => $url]);
                $arResult['crm_placement_lead_tab'] = json_decode($result->getContent(), true);
                $result = $apiClient->getResponse('placement.bind',['PLACEMENT'=>'CRM_DEAL_DETAIL_TAB', 'HANDLER' => $url]);
                $arResult['crm_placement_deal_tab'] = json_decode($result->getContent(), true);
                $arResult['server_url'] = $this->getServerUrl();



//                $soloApp = SoloAppHostApps::find()
//                    ->where(['prefix' => $data['claims']['code']])
//                    ->one();
//                $month = new DateTime();
//                $demo = intval($soloApp['UF_DEMO_PERIOD']);
//                if (!$demo) {
//                    $demo = 14;
//                }
//                $month->add("P{$demo}D");
//
//                $now = new DateTime();
//
//                $addPurchased = new SoloPurchasedApplications();
//                $addPurchased->installed = "1";
//                $addPurchased->app = $soloApp["ID"];
//                $addPurchased->date_limit = $month;
//                $addPurchased->date = $now;
//                $addPurchased->settings = json_encode([]);
//                $addPurchased->client = $params['companyId'];
//
//                $addPurchased->save();
//
//                $id = $addPurchased->id;
//                if ($addPurchased) {
//                    $arResult['result'] = [
//                        'message' => "Приложение установлено",
//                        'data' => [
//                            'purchased' => $id
//                        ]
//                    ];
//                } else {
//                    $arResult['error'] = "Ошибка добавления приложения... ";
//                }
//                $arResult['next'] = 7;
                break;
            case 6:// переустановка
                $arResult['next'] = 7;
//                $soloApp = SoloAppHostApps::find()
//                    ->where(['code' => $data['claims']['code']])
//                    ->one();
//                $purchased = SoloPurchasedApplications::find()
//                    ->where(['app' => $soloApp["ID"]])
//                    ->one();
//                if ($purchased['date']) {
//                    $month = $purchased['date'];
//                } else {
//                    $month = new DateTime();
//                }
//                $addPurchased = SoloPurchasedApplications::findOne($params["purchased"]);
//                $addPurchased->installed = "1";
//                $addPurchased->date_limit = $month;
//                $addPurchased->save();
//
//                $id = $addPurchased->id;
//
//                if ($addPurchased) {
//                    $arResult['result'] = [
//                        'message' => "Приложение установлено.",
//                        'data' => [
//                            'purchased' => $id
//                        ]
//                    ];
//                } else {
//                    $arResult['error'] = "Ошибка добавления приложения...";
//                }
//                $arResult['next'] = ++$step;
                break;
            case 7://finish
//                $soloApp = SoloAppHostApps::find()
//                    ->where(['prefix' => $data['claims']['code']])
//                    ->one();
//                $bx24AppHostHelper = SoloAppHostApps::find()
//                    ->where(['companyId' => $params['companyId']])
//                    ->one();
//                $old_access_db = SoloAppHostApps::find()
//                    ->where([
//                        'purch_app' => $params['purchased'],
//                        'company' => $params['companyId'],
//                    ])
//                    ->all();
//                while ($old_access = $old_access_db) {
//                    $bx24AppHostHelper = SoloAppHostApps::findOne($old_access['id']);
//                    $bx24AppHostHelper->delete();
//                }
//
//                $add = new SoloAppHostApps();
//                $add->purch_app = $params['purchased'];
//                $add->active = "1";
//                $add->member_id = $data['claims']['member_id'];
//                $add->application_id = $soloApp['code'];
//                $add->company = $params['companyId'];
//                $add->app_secret = $soloApp['app_secret'];
//                $add->app_id = $soloApp['app_id'];
//                $add->domain = $data['claims']['DOMAIN'];
//                $add->access_token = $data['claims']['AUTH_ID'];
//                $add->expires = $data['claims']['AUTH_EXPIRES'];
//                $add->refresh_token = $data['claims']['REFRESH_ID'];
//                $add->save();
//
//                if ($add) {
//                    $purchased = SoloAppHostApps::findOne($params['purchased']);
//                    $id = $purchased->id;
//                    $arResult['result'] = [
//                        'message' => "Установка завершена. Запускаем приложение...",
//                        'data' => [
//                            'credentials' => $id
//                        ]
//                    ];
//                    $settings = json_decode($purchased["settings"], true);
//                    $arResult['purchased'] = $purchased;
//                    $arResult['$settings'] = $settings;
//                    $events = $app->event->get();
//                    $unbind = [];
//                    $arResult['events'] = $events['result'];
//
//                    $arResult['unbind'] = $unbind;
//                    $arResult['result_events'] = $app->event->get();
//                } else {
//                    $arResult['error'] = "Ошибка при сохранении данных авторизации приложения";
//                }
                break;
            default:
                $arResult['error'] = 'Unknown params';
                break;
        }



        sleep(0.5);

        $arResult['code'] = 0;
        $arResult['data']['next'] = $arResult['next'] ?? null;
        return $arResult;

//                'csrf_name' => Yii::$app->request->csrfParam,
//                'csrf_token' => Yii::$app->request->csrfToken

    }

    public function getCurrentApp($appCode)
    {
        $app = SoloApplications::find()
            ->where(['code' => $appCode])
            ->one();
        $this->app = $app;
        return $app;
    }

    //Возможность указать конкретный домен, либо берется из токена
    private function getClientByPortal($domain = '')
    {
        if(!$domain) $domain = $this->settings->claims['domain'];
        $client = SoloClients::find()
            ->where(['portal' => $domain])
            ->one();
//        $clients = SoloClients::find()->all();
//        echo $delete = SoloClients::findOne(1)->delete();
//        echo $delete = SoloClients::findOne(2)->delete();

//        var_dump($this->settings->claims['domain']->getValue());
//        var_dump($users);
//        if($users = Users::model()->findAll()) {
//
//        }
//        var_dump($client);
//        var_dump($clients);

//        die(111);
        return $client;
    }


    /**
     * Функция подготавливает из полученных данных Объект типа
     * @return \Bitrix24\SDK\Core\ApiClient|void
     * @throws \Bitrix24\SDK\Core\Exceptions\UnknownScopeCodeException
     */
    function makebitrix24App(){
        //было
        //https://matrix2020-56893.solo-it.ru/backend/install/install_solo_app_interface2.php?app=app1
        //Стало
        //https://kub.solo-it.ru/bitrix24/api/install?app=app1
        $claims = $this->settings->claims;

        if(!$claims || !$this->app) return false;
        $log = new Logger('name');
        $log->pushHandler(new StreamHandler('bitrix24-api-client-debug.log', Logger::DEBUG));

        $client = HttpClient::create(['http_version' => '2.0']);
        $traceableClient = new \Symfony\Component\HttpClient\TraceableHttpClient($client);
        $traceableClient->setLogger($log);

        $appProfile = new \Bitrix24\SDK\Core\Credentials\ApplicationProfile(
            $this->app->app_id,//'local.5f9d4c50b2bf08.70341243',
            $this->app->app_secret,//'YE56q7neK8SJgP8xqDBlTP2oPYSUf7HUySkob0w9wOWFr1XZCv',
            new \Bitrix24\SDK\Core\Credentials\Scope(
                [
                    'crm',//TODO: Вынести Scope
                ]
            )
        );
        $token = new \Bitrix24\SDK\Core\Credentials\AccessToken(
            $claims['auth_id'],//'50cc… access token',
            $claims['refresh_id'],//'404b… refresh token',
            3600
        );
        $domain = 'https://'.$claims['domain'];
        $credentials = \Bitrix24\SDK\Core\Credentials\Credentials::createForOAuth($token, $appProfile, $domain);



        try {
            $apiClient = new \Bitrix24\SDK\Core\ApiClient($credentials, $traceableClient, $log);


            $ed = new \Symfony\Component\EventDispatcher\EventDispatcher();
            $ed->addListener(
                \Bitrix24\SDK\Events\AuthTokenRenewedEvent::class,
                static function (\Bitrix24\SDK\Events\AuthTokenRenewedEvent $event) {
                    var_dump('AuthTokenRenewed!');
                    print($event->getRenewedToken()->getAccessToken()->getAccessToken() . PHP_EOL);
                }
            );

            //Пример как делать запросы
//            $result = $apiClient->getResponse('app.info');
//            $result = json_decode($result->getContent(), true);

            //Ниже пример того, что мы получим в резудбтате
//            Array
//            (
//                [result] => Array
//                (
//                    [ID] => 7
//            [CODE] => KUbitrix24.app1
//            [VERSION] => 1
//            [STATUS] => F
//            [INSTALLED] =>
//            [PAYMENT_EXPIRED] => N
//            [DAYS] =>
//            [LANGUAGE_ID] => ru
//            [LICENSE] => ru_nfr
//            [LICENSE_TYPE] => nfr
//            [LICENSE_FAMILY] => nfr
//        )
//
//    [time] => Array
//            (
//                [start] => 1640815438.2537
//            [finish] => 1640815438.2938
//            [duration] => 0.040112972259521
//            [processing] => 0.00074601173400879
//            [date_start] => 2021-12-30T01:03:58+03:00
//            [date_finish] => 2021-12-30T01:03:58+03:00
//        )
//
//)
//            $this->appInfobitrix24 = $result['result'];
            $this->bitrix24App = $apiClient;//

            return $apiClient;



//            $app = new \Bitrix24\SDK\Core\Core($apiClient, $ed, $log);
//
//            $log->debug('================================');
//
//            // api call with expired access token
//            $res = $app->call('app.info');
//            print('result:' . PHP_EOL);
//
//            var_dump($res->getResponseData()->getResult()->getResultData());
//            var_dump($res->getResponseData()->getTime()->getDuration());


//
//            $app = new \Bitrix24\SDK\Services\Main($apiClient, $ed, $log);
//            $app = new ёBitrix24\SDK\Core($apiClient, $ed, $log);


//            $coreBuilder = new \Bitrix24\SDK\Core\CoreBuilder();
//            $authCore = $coreBuilder->withWebhookUrl($domain . '/rest/1/qpkvi88xglywert4/');
//            $coreInterface = $coreBuilder->build($authCore);
//            $batch = new \Bitrix24\SDK\Core\Batch($coreInterface, $log);
//            $batchService = new \Bitrix24\SDK\Services\CRM\Deal\Service\Batch($batch, $log);
////
//            $list = $batchService->list(["DATE_CREATE" => "ASC"], ["UF_CRM_1613354197" => 1, '!UF_CRM_1613354833' => 1], ['ID', 'TITLE', 'UF_CRM_1595902763', 'UF_CRM_1601007247862', 'UF_CRM_1583308488', 'UF_CRM_1605511641'], 0);

//            print_r($batch);
//            ///////////////////////////////////////////////////////////////
//
//
//            $log->debug('================================');
//
//            // api call with expired access token
//            $res = $app->call('app.info');
//            print('result:' . PHP_EOL);
//
//            var_dump($res->getResponseData()->getResult()->getResultData());
//            var_dump($res->getResponseData()->getTime()->getDuration());
        } catch (\Throwable $exception) {
            print(sprintf('error: %s', $exception->getMessage()) . PHP_EOL);
            print(sprintf('class: %s', get_class($exception)) . PHP_EOL);
            print(sprintf('trace: %s', $exception->getTraceAsString()) . PHP_EOL);
        }
    }

    function makebitrix24AppFromToken(){
        $data = $this->settings->claims;

        $AUTH_ID = htmlspecialchars($data['claims']['AUTH_ID']);
        $REFRESH_ID = htmlspecialchars($data['claims']['REFRESH_ID']);
        $member_id = htmlspecialchars($data['claims']['member_id']);
        $DOMAIN = htmlspecialchars($data['claims']['DOMAIN']);
//    $config = self::getbitrix24ConnectionData($appCode, htmlspecialchars($data['claims']['member_id']));
        $client = new Bitrix24();
        $client->setAccessToken($AUTH_ID);
        $client->setRefreshToken($REFRESH_ID);
        $client->setMemberId($member_id);
        $client->setDomain($DOMAIN);

        $info = $client->call('app.info');
        $scope = $client->call('scope');
        Loader::includeModule('bitrix24.bitrix24apphost');
        $id = COption::GetOptionInt(bitrix24_bitrix24apphost::MODULE_ID, bitrix24_bitrix24apphost::HLB_APPLICATIONS_NAME);
        $configHelper = Application::getHLBHelper($id);

        $config = $configHelper->getList([
            'filter' => [
                'UF_CODE' => $info['result']['CODE']
            ]
        ])->fetch();

        $client->setApplicationId($config["UF_APP_ID"]);
        $client->setApplicationSecret($config["UF_APP_SECRET"]);
        $client->setApplicationScope($scope['result']);

        $client->setOnExpiredToken(function () use ($AUTH_ID, $client) {
            $start = $AUTH_ID;
            if (!$client->getApplicationId() || !$client->getApplicationSecret() || !$client->getRefreshToken() || !$client->getDomain()) {
                return false;
            }
            $queryUrl = 'https://' . $client->getDomain() . '/oauth/token/';
            $queryData = http_build_query($queryParams = array(
                'grant_type'    => 'refresh_token',
                'client_id'     => $client->getApplicationId(),
                'client_secret' => $client->getApplicationSecret(),
                'refresh_token' => $client->getRefreshToken(),
                'scope'         => implode(',', $client->getScope()),
            ));

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_HEADER         => 0,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL            => $queryUrl . '?' . $queryData,
            ));
            $result = curl_exec($curl);
            //		var_dump($result);
            curl_close($curl);
            $result = json_decode($result, 1);

            $client->setAccessToken($result['access_token']);
            $client->setRefreshToken($result['refresh_token']);
            return $start !== $result['access_token'];
        });
        return bitrix24App::createFromClient($client);
    }
}
