<?php

namespace frontend\modules\bitrix24\components;

use Yii;
use Lcobucci\JWT\Token;
use common\models\Company;
use common\models\employee\Employee;
use frontend\modules\bitrix24\models\SoloClients;
use frontend\modules\bitrix24\models\WidgetUser;
use yii\base\Component;
use yii\di\Instance;
use yii\web\UnauthorizedHttpException;

class CheckAccess extends Component
{
    private $_jwt;
    private $_token;
    private $_isWidgetOk = false;
    private $_isUserOk = false;

    public function __construct(Token $token, ?array $config = [])
    {
        $this->_jwt = $token;

        parent::__construct($config);
    }

    public function init() : void
    {
        parent::init();

        $data = $this->_jwt->getClaims();

        if ($client = SoloClients::findOne(['portal' => $data['DOMAIN']])) {
            $this->_isWidgetOk = true;
            if ($user = WidgetUser::findOne(['client_id' => $client->id, 'user_id' => $data['user_id']])) {
                $this->_isUserOk = true;
                $this->_token = Yii::$app->security->generateRandomString() . '_' . time();
                $user->updateAttributes(['token' => $this->_token]);
            }
        }

    }

    public function getIsWidgetOk() : bool
    {
        return $this->_isWidgetOk;
    }

    public function getIsUserOk() : bool
    {
        return $this->_isUserOk;
    }

    public function getToken() : ?string
    {
        return $this->_token;
    }
}
