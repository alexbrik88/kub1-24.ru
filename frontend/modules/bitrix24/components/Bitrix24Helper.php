<?php

namespace frontend\modules\bitrix24\components;

use Yii;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\HttpClient\HttpClient;
use frontend\modules\bitrix24\models\RequestData;
use frontend\modules\bitrix24\models\SoloApplications;
use yii\base\Component;
use yii\helpers\Json;
use yii\web\Request;

class Bitrix24Helper extends Component
{
    public static $placementMap = [
        'CRM_DEAL_DETAIL_TAB' => 'deal_id',
        'CRM_LEAD_DETAIL_TAB' => 'lead_id',
        'CRM_COMPANY_DETAIL_TAB' => 'company_id',
        'CRM_CONTACT_DETAIL_TAB' => 'contact_id',
    ];

    private Request $request;
    private RequestData $requestData;
    private SoloApplications $app;

    private $data = [];

    public function __construct()
    {
        $this->request = Yii::$app->request;
        $this->app = SoloApplications::findOne(1);

        parent::__construct();
    }

    public function getServerUrl()
    {
        return $this->request->getHostInfo();
    }

    public function getApp()
    {
        return $this->app;
    }

    public function setRequestData(RequestData $requestData) : void
    {
        $this->requestData = $requestData;
    }

    public function getRequestData() : RequestData
    {
        return $this->requestData;
    }

    /**
     * ID пользователя Bitrix24
     * @return int|null
     */
    public function getUserId()
    {
        return $this->getUserData()['ID'] ?? (Yii::$app->params['bitrix24']['user_id'] ?? null);
    }

    /**
     * ID компании Bitrix24
     * @return int|null
     */
    public function getCompanyId()
    {
        if (!array_key_exists('getCompanyId', $this->data)) {
            $id = $this->getPlacement()['company_id'] ?? null;
            if (!$id && ($data = $this->getDealData())) {
                $id = $data['COMPANY_ID'] ?? null;
            }
            $this->data['getCompanyId'] = $id;
        }

        return $this->data['getCompanyId'];
    }

    /**
     * ID контакта Bitrix24
     * @return int|null
     */
    public function getContactId()
    {
        if (!array_key_exists('getContactId', $this->data)) {
            $id = $this->getPlacement()['contact_id'] ?? null;
            if (!$id && ($data = $this->getDealData())) {
                $id = $data['CONTACT_ID'] ?? null;
            }
            $this->data['getContactId'] = $id;
        }

        return $this->data['getContactId'];
    }

    /**
     * ID сделки Bitrix24
     * @return int|null
     */
    public function getDealId()
    {
        return $this->getPlacement()['deal_id'] ?? null;
    }

    /**
     * ID лида Bitrix24
     * @return int|null
     */
    public function getLeadId()
    {
        return $this->getPlacement()['lead_id'] ?? null;
    }

    /**
     * ИНН компании Bitrix24
     * @return int|null
     */
    public function getCompanyInn()
    {
        return $this->getCompanyRequisite()['RQ_INN'] ?? null;
    }

    /**
     * Функция подготавливает из полученных данных Объект типа
     * @return \Bitrix24\SDK\Core\ApiClient|void
     * @throws \Bitrix24\SDK\Core\Exceptions\UnknownScopeCodeException
     */
    function getBitrix24ApiClien()
    {
        $claims = $this->getRequestData()->attributes ?? null;

        $app = $this->getApp();

        if(!$claims || !$app) return;

        $log = new Logger('name');
        $log->pushHandler(new StreamHandler('bitrix24-api-client-debug.log', Logger::DEBUG));

        $client = HttpClient::create(['http_version' => '2.0']);
        $traceableClient = new \Symfony\Component\HttpClient\TraceableHttpClient($client);
        $traceableClient->setLogger($log);

        $token = new \Bitrix24\SDK\Core\Credentials\AccessToken(
            $claims['AUTH_ID'],
            $claims['REFRESH_ID'],
            3600
        );

        $appProfile = new \Bitrix24\SDK\Core\Credentials\ApplicationProfile(
            $app->app_id,
            $app->app_secret,
            new \Bitrix24\SDK\Core\Credentials\Scope(['crm'])
        );

        $domain = 'https://'.$claims['DOMAIN'];

        $credentials = \Bitrix24\SDK\Core\Credentials\Credentials::createForOAuth($token, $appProfile, $domain);

        $apiClient = new \Bitrix24\SDK\Core\ApiClient($credentials, $traceableClient, $log);

        return $apiClient;
    }

    public function getDealData()
    {
        if (!array_key_exists('getDealData', $this->data)) {
            $data = [];
            if ($ID = $this->getDealId()) {
                if ($apiClient = $this->getBitrix24ApiClien()) {
                    $result = $apiClient->getResponse('crm.deal.get', ['ID' => "$ID"]);
                    try {
                        $data = json_decode($result->getContent(false), true);
                        $this->debugLog(__METHOD__, [$ID,$data]);
                    } catch (\Throwable $e) {
                        $this->debugLog(__METHOD__, [$ID,$e]);
                    }
                }
            }
            $this->data['getDealData'] = $data['result'] ?? [];
        }

        return $this->data['getDealData'];
    }

    public function getCompanyRequisite()
    {
        if (!array_key_exists('getCompanyRequisite', $this->data)) {
            $data = [];
            if ($ID = $this->getCompanyId()) {
                if ($apiClient = $this->getBitrix24ApiClien()) {
                    $result = $apiClient->getResponse('crm.requisite.list', [
                        'FILTER' => [
                            'ENTITY_TYPE_ID' => '4',
                            'ENTITY_ID' => "$ID",
                        ],
                        'SELECT' => ["ID", "NAME","RQ_INN"],
                    ]);
                    try {
                        $data = json_decode($result->getContent(false), true);
                        $this->debugLog(__METHOD__, [$ID, $data]);
                    } catch (\Throwable $e) {
                        $this->debugLog(__METHOD__, [$ID, $e]);
                    }
                }
            }
            $this->data['getCompanyRequisite'] = (isset($data['result']) && is_array($data['result'])) ? reset($data['result']) : [];
        }

        return $this->data['getCompanyRequisite'];
    }

    public function getUserData()
    {
        if (!array_key_exists('getUserData', $this->data)) {
            $data = [];
            if ($apiClient = $this->getBitrix24ApiClien()) {
                $result = $apiClient->getResponse('user.current');
                try {
                    $data = json_decode($result->getContent(false), true);
                    $this->debugLog(__METHOD__, $data);
                } catch (\Throwable $e) {
                    $this->debugLog(__METHOD__, $e);
                }
            }
            $this->data['getUserData'] = $data['result'] ?? [];
        }

        return $this->data['getUserData'];
    }

    public function genJwtToken()
    {
        $serverUrl = Yii::$app->request->getHostInfo();
        $jwt = Yii::$app->b24jwt;
        $time = time();
        $builder = $jwt->getBuilder()
            ->issuedBy($serverUrl)
            ->permittedFor($serverUrl)
            ->identifiedBy('d628f123-5123-473e-a123-ed123ef31f8f')
            ->issuedAt($time)
            ->canOnlyBeUsedAfter($time)
            ->expiresAt($time + 3600)
            ->withClaim('user_id', $this->getUserId());

        foreach ($this->getRequestData()->attributes as $key => $value){
            $builder->withClaim($key, $value);
        }

        $token = $builder->getToken($jwt->getSigner('HS256'), $jwt->getKey());

        return strval($token);
    }

    public function getPlacement()
    {
        if (!isset($this->data['getPlacement'])) {
            $this->data['getPlacement'] = [];
            $data = $this->getRequestData();
            if ($data) {
                $placement = self::$placementMap[$data->PLACEMENT] ?? null;
                if ($placement) {
                    $placementOptions = Json::decode($data->PLACEMENT_OPTIONS?? null);
                    $placementId = $placementOptions['ID'] ?? null;
                    if ($placementId) {
                        $this->data['getPlacement'] = [$placement => $placementId];
                    }
                }
            }
        }

        return $this->data['getPlacement'];
    }

    public function debugLog($method, $data)
    {
        $message = ($data instanceof \Throwable) ? $data->getTraceAsString() : \yii\helpers\VarDumper::dumpAsString($data);
        $message = date('c').' '.$method.PHP_EOL.$message.PHP_EOL;
        file_put_contents(Yii::getAlias('@runtime/logs/bitrix24debug.log'), $message, FILE_APPEND);
    }
}
