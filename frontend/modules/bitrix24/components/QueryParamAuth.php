<?php

namespace frontend\modules\bitrix24\components;

use Yii;
use common\models\Company;
use frontend\modules\bitrix24\models\SoloClients;
use frontend\modules\bitrix24\models\WidgetUser;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

/**
 * QueryParamAuth is an action filter that supports the authentication based on the access token passed through a query parameter.
 */
class QueryParamAuth extends \yii\filters\auth\QueryParamAuth
{
    /**
     * @var string the parameter name for passing the access token
     */
    public $tokenParam = 'access-token';



    /**
     * {@inheritdoc}
     */
    public function authenticate($user, $request, $response)
    {
        $accessToken = $request->get($this->tokenParam);
        if (is_string($accessToken) && $this->isAccessTokenValid($accessToken)) {
            $widgetUser = WidgetUser::findOne(['token' => substr($accessToken, -43)]);
            if ($widgetUser !== null && $widgetUser->employee !== null) {
                $employee = $widgetUser->employee;
                $allowedCompanyArray = ArrayHelper::index($widgetUser->companies, 'id');
                if (empty($allowedCompanyArray)) {
                    throw new ForbiddenHttpException('У вас нет доступа к активным компаниям. Обратитесь к администратору.');
                }
                $companyId = $this->companyId($accessToken);
                $company = $allowedCompanyArray[$companyId] ?? reset($allowedCompanyArray);
                if ($company) {
                    if ($user->login($employee)) {
                        $employee->setCompany($company);
                        Yii::$app->set('bitrix24User', $widgetUser);
                        Yii::$app->set('bitrix24Client', $widgetUser->client);

                        return $employee;
                    }
                }
            }
        }
        if ($accessToken !== null) {
            $this->handleFailure($response);
        }

        return null;
    }

    private function isAccessTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['bitrix24']['tokenExpire'] ?? 3600;
        $parts = explode('_', $token);
        $timestamp = (int)end($parts);

        return $timestamp + $expire >= time();
    }

    private function companyId($token)
    {
        if (strlen($token) > 43) {
            $parts = explode('_', $token);
            return reset($parts);
        } else {
            return null;
        }
    }
}
