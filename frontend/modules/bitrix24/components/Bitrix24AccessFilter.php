<?php

namespace frontend\modules\bitrix24\components;

use Yii;
use frontend\modules\bitrix24\models\RequestData;
use yii\base\ActionFilter;
use yii\web\ForbiddenHttpException;

class Bitrix24AccessFilter extends ActionFilter
{
    /**
     * @var array
     *
     * An array key is an action ID
     * An array value is a scenario name
     */
    public string $scenario = RequestData::SCENARIO_DEFAULT;

    /**
     * @var callable
     */
    public $denyCallback;

    public function init() : void
    {
        parent::init();
    }

    /**
     * @param Action $action the action to be executed.
     * @return bool whether the action should continue to be executed.
     */
    public function beforeAction($action)
    {
        $request = Yii::$app->getRequest();
        $getAttributes = array_combine(RequestData::$getAttributes, RequestData::$getAttributes);
        $postAttributes = array_combine(RequestData::$postAttributes, RequestData::$postAttributes);
        $getData = array_intersect_key($request->get(), $getAttributes);
        $postData = array_intersect_key($request->post(), $postAttributes);

        $model = new RequestData(['scenario' => $this->scenario]);

        $model->load($getData, '');
        $model->load($postData, '');

        if ($model->validate()) {
            Yii::$app->bitrix24Helper->setRequestData($model);

            return true;
        }

        if ($this->denyCallback !== null) {
            call_user_func($this->denyCallback, null, $action, $model);
        } else {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            Yii::$app->response->data = [
                'error' => 1,
                'message' => implode(' ', $model->getFirstErrors()),
            ];
            Yii::$app->response->send();
            die();
        }

        return false;
    }
}
