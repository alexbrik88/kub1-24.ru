<?php
/* @var $this yii\web\View */
?>

<script>
    window.soloapp = {
        APP_NAME: '<?=$setting->app_name?>',
        APP_SID: '<?=$setting->app_sid?>',
        PROTOCOL: <?=$setting->protocol?>,
        DOMAIN: '<?=$setting->domain?>',
        LICENCE_LINK: "<?=$setting->LICENCE_LINK?>",
        BACKEND_URL: "<?=$setting->BACKEND_URL?>",
        token: '<?=$setting->token?>'
    };
</script>
