<?php

namespace frontend\modules\bitrix24\models;

use Yii;
use common\models\document\Invoice;

/**
 * This is the model class for table "bitrix24_widget_invoice".
 *
 * @property int $invoice_id
 * @property int $account_id
 * @property int $lead_id
 * @property int $deal_id
 * @property int $contact_id
 * @property int $company_id
 *
 * @property SoloClients $account
 * @property Invoice $invoice
 */
class Bitrix24WidgetInvoice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bitrix24_widget_invoice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['invoice_id', 'account_id'], 'required'],
            [['invoice_id', 'account_id', 'lead_id', 'deal_id', 'contact_id', 'company_id'], 'integer'],
            [['invoice_id', 'account_id'], 'unique', 'targetAttribute' => ['invoice_id', 'account_id']],
            [['account_id'], 'exist', 'skipOnError' => true, 'targetClass' => SoloClients::className(), 'targetAttribute' => ['account_id' => 'account_id']],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => Invoice::className(), 'targetAttribute' => ['invoice_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'invoice_id' => 'Invoice ID',
            'account_id' => 'Account ID',
            'lead_id' => 'Lead ID',
            'deal_id' => 'Deal ID',
            'contact_id' => 'Contact ID',
            'company_id' => 'Company ID',
        ];
    }

    /**
     * Gets query for [[SoloClients]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(SoloClients::className(), ['id' => 'account_id']);
    }

    /**
     * Gets query for [[Invoice]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }
}
