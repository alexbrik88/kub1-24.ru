<?php

namespace frontend\modules\bitrix24\models;

use Yii;

/**
 * This is the model class for table "bitrix24_application".
 *
 * @property int $id
 * @property string|null $code
 * @property string|null $name
 * @property string|null $short_descr
 * @property int|null $demo_period
 * @property string|null $prefix
 * @property string|null $app_id
 * @property int|null $active
 * @property string|null $app_secret
 *
 * @property SoloAppHostApps[] $soloAppHostApps
 * @property SoloPurchasedApplications[] $soloPurchasedApplications
 */
class SoloApplications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bitrix24_application';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['demo_period', 'active'], 'integer'],
            [['code'], 'string', 'max' => 25],
            [['name'], 'string', 'max' => 75],
            [['short_descr'], 'string', 'max' => 200],
            [['prefix'], 'string', 'max' => 30],
            [['app_id', 'app_secret'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
            'short_descr' => 'Short Descr',
            'demo_period' => 'Demo Period',
            'prefix' => 'Prefix',
            'app_id' => 'App ID',
            'active' => 'Active',
            'app_secret' => 'App Secret',
        ];
    }

    /**
     * Gets query for [[SoloAppHostApps]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSoloAppHostApps()
    {
        return $this->hasMany(SoloAppHostApps::className(), ['application_id' => 'id']);
    }

    /**
     * Gets query for [[SoloPurchasedApplications]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSoloPurchasedApplications()
    {
        return $this->hasMany(SoloPurchasedApplications::className(), ['app_id' => 'id']);
    }
}
