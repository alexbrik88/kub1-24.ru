<?php

namespace frontend\modules\bitrix24\models;

use Yii;

/**
 * This is the model class for table "bitrix24_app_host_app".
 *
 * @property int $id
 * @property string|null $server_domain
 * @property string|null $scope
 * @property int|null $active
 * @property string|null $app_token
 * @property int $company_id
 * @property int $purch_app_id
 * @property string|null $refresh_token
 * @property int|null $user_id
 * @property string|null $client_endpoint
 * @property int|null $expires
 * @property string|null $status
 * @property string|null $server_endpoint
 * @property int|null $expires_in
 * @property string|null $access_token
 * @property string|null $domain
 * @property string|null $app_id
 * @property string|null $app_secret
 * @property int $application_id
 * @property string|null $member_id
 *
 * @property SoloApplications $application
 * @property SoloClients $company
 * @property SoloPurchasedApplications $purchApp
 */
class SoloAppHostApps extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bitrix24_app_host_app';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['active', 'company_id', 'purch_app_id', 'user_id', 'expires', 'expires_in', 'application_id'], 'integer'],
            [['company_id', 'purch_app_id', 'application_id'], 'required'],
            [['server_domain', 'scope', 'app_token', 'refresh_token', 'access_token', 'app_id', 'app_secret'], 'string', 'max' => 100],
            [['client_endpoint', 'server_endpoint', 'domain'], 'string', 'max' => 200],
            [['status'], 'string', 'max' => 10],
            [['member_id'], 'string', 'max' => 50],
            [['application_id'], 'exist', 'skipOnError' => true, 'targetClass' => SoloApplications::className(), 'targetAttribute' => ['application_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => SoloClients::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['purch_app_id'], 'exist', 'skipOnError' => true, 'targetClass' => SoloPurchasedApplications::className(), 'targetAttribute' => ['purch_app_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'server_domain' => 'Server Domain',
            'scope' => 'Scope',
            'active' => 'Active',
            'app_token' => 'App Token',
            'company_id' => 'Company ID',
            'purch_app_id' => 'Purch App ID',
            'refresh_token' => 'Refresh Token',
            'user_id' => 'User ID',
            'client_endpoint' => 'Client Endpoint',
            'expires' => 'Expires',
            'status' => 'Status',
            'server_endpoint' => 'Server Endpoint',
            'expires_in' => 'Expires In',
            'access_token' => 'Access Token',
            'domain' => 'Domain',
            'app_id' => 'App ID',
            'app_secret' => 'App Secret',
            'application_id' => 'Application ID',
            'member_id' => 'Member ID',
        ];
    }

    /**
     * Gets query for [[Application]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApplication()
    {
        return $this->hasOne(SoloApplications::className(), ['id' => 'application_id']);
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(SoloClients::className(), ['id' => 'company_id']);
    }

    /**
     * Gets query for [[PurchApp]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPurchApp()
    {
        return $this->hasOne(SoloPurchasedApplications::className(), ['id' => 'purch_app_id']);
    }
}
