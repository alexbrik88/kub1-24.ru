<?php

namespace frontend\modules\bitrix24\models;

use Yii;
use Lcobucci\JWT\Token;
use common\models\Company;
use common\models\employee\Employee;
use frontend\modules\bitrix24\models\SoloClients;
use frontend\modules\bitrix24\models\WidgetUser;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\di\Instance;
use yii\web\UnauthorizedHttpException;

class LoginForm extends Model
{
    const SCENARIO_LOGIN = 'login';
    const SCENARIO_COMPANY = 'company';

    public $username;
    public $password;
    public $company_id;

    private $_user = false;
    private $token;
    private $client;
    private $user_id;

    public function __construct(Token $token, ?array $config = [])
    {
        $this->token = $token;
        $this->client = SoloClients::findOne(['portal' => $token->getClaim('DOMAIN')]);
        $this->user_id = $token->getClaim('user_id');

        parent::__construct($config);
    }

    public function scenarios() : array
    {
        return [
            self::SCENARIO_LOGIN => [
                'username',
                'password',
            ],
            self::SCENARIO_COMPANY => [
                'username',
                'password',
                'company_id',
            ],
        ];
    }

    public function rules() : array
    {
        return [
            [['username'], 'trim'],
            [
                [
                    'username',
                    'password',
                    'company_id',
                ],
                'required',
            ],
            ['password', 'validatePassword'],
            ['company_id', 'validateCompany'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неверный пользователь или пароль.');
            }
        }
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateCompany($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $items = $this->getDropdownItems();
            if (!$items) {
                $this->addError($attribute, 'Нет компаний, доступных для интеграци.');
                return;
            }
            foreach ((array) $this->$attribute as $company_id) {
                if (!isset($items[$company_id])) {
                    $this->addError($attribute, 'Необходимо выбрать компанию, для которой подключается интеграция.');
                }
            }
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return Employee|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Employee::findIdentityByLogin($this->username);
        }

        return $this->_user;
    }

    public function attributeLabels() : array
    {
        return [
            'username' => 'Логин',
            'password' => 'Пароль',
            'company_id' => 'Компания',
        ];
    }

    public function getDropdownItems() : array
    {
        if ($user = $this->getUser()) {
            $companyArray = $user->companiesByChief;

            return ArrayHelper::map($companyArray, 'id', 'shortTitle');
        }

        return [];
    }

    public function getUserIsAdmin() : bool
    {
        return $this->client->user_id && $this->user_id && $this->client->user_id == $this->user_id;
    }

    public function save() : bool
    {
        if (!$this->validate() || ($this->getUserIsAdmin() && $this->scenario != self::SCENARIO_COMPANY)) {
            return false;
        }

        $user = WidgetUser::findOne([
            'client_id' => $this->client->id,
            'user_id' => $this->user_id,
        ]) ?: new WidgetUser([
            'client_id' => $this->client->id,
            'user_id' => $this->user_id,
            'employee_id' => $this->getUser()->id,
        ]);

        return Yii::$app->db->transaction(function ($db) use ($user) {
            if ((!$user->getIsNewRecord() || $user->save(false)) && (!$this->getUserIsAdmin() || $this->_saveCompanies())) {
                return true;
            }

            $db->transaction->rollBack();

            return false;
        });
    }

    private function _saveCompanies() : bool
    {
        $rows = [];
        $time = time();
        foreach ($this->company_id as $company_id) {
            $rows[] = [
                $this->client->id,
                $company_id,
                $time,
            ];
        }

        if (!empty($rows)) {
            $db = Yii::$app->db;
            $db->createCommand()->delete(WidgetCompany::tableName(), [
                'client_id' => $this->client->id,
            ])->execute();
            $sql = (new \yii\db\QueryBuilder($db))->batchInsert(WidgetCompany::tableName(), [
                'client_id',
                'company_id',
                'created_at',
            ], $rows);
            $sql = 'INSERT IGNORE' . mb_substr($sql, strlen('INSERT'));

            return (bool) $db->createCommand($sql)->execute();
        }

        return false;
    }
}
