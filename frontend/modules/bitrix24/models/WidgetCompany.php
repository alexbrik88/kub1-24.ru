<?php

namespace frontend\modules\bitrix24\models;

use Yii;
use common\models\Company;

/**
 * This is the model class for table "bitrix24_widget_company".
 *
 * @property int $client_id ID аккаунта, из которого сделан запрос
 * @property int $company_id ID компании в КУБ
 * @property int $created_at Timestamp, когда подключен пользователь
 *
 * @property Company $company
 */
class WidgetCompany extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bitrix24_widget_company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'client_id' => 'Account ID',
            'company_id' => 'Company ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[AmocrmWidget]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(SoloClients::className(), ['id' => 'client_id']);
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
