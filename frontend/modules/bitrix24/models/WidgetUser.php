<?php

namespace frontend\modules\bitrix24\models;

use Yii;
use common\models\employee\Employee;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bitrix24_widget_user".
 *
 * @property int $client_id ID аккаунта, из которого сделан запрос
 * @property int $user_id ID пользователя, из под которого сделан запрос
 * @property string $token Токен для доступа из виджета
 * @property int $employee_id ID сотрудника в КУБ
 * @property int $created_at Timestamp, когда подключен пользователь
 *
 * @property EmployeeCompany $employeeCompany
 */
class WidgetUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bitrix24_widget_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'client_id' => 'Account ID',
            'user_id' => 'Amo User ID',
            'token' => 'Access Token',
            'employee_id' => 'Employee ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[SoloClients]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(SoloClients::className(), ['id' => 'client_id']);
    }

    /**
     * Gets query for [[Employee]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id'])->andOnCondition([
            'is_deleted' => false,
        ]);
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->employee->getCompanies()->andOnCondition([
            'id' => ArrayHelper::getColumn($this->client->companies, 'id'),
        ]);
    }

    /**
     * @return array
     */
    public function getCompanyIds()
    {
        return ArrayHelper::getColumn($this->companies, 'id');
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function tokenByCompany($companyId)
    {
        return $companyId ? sprintf('%s_%s', $companyId, $this->token) : $this->token;
    }
}
