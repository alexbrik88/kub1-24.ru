<?php

namespace frontend\modules\bitrix24\models;

use Yii;

/**
 * This is the model class for table "bitrix24_purchased_application".
 *
 * @property int $id
 * @property int $app_id
 * @property int $client_id
 * @property string|null $date
 * @property string|null $date_limit
 * @property int|null $installed
 * @property string|null $settings
 * @property int|null $user_limit
 *
 * @property SoloApplications $app
 * @property SoloClients $client
 * @property SoloAppHostApps[] $soloAppHostApps
 */
class SoloPurchasedApplications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bitrix24_purchased_application';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['app_id', 'client_id'], 'required'],
            [['app_id', 'client_id', 'installed', 'user_limit'], 'integer'],
            [['date', 'date_limit'], 'safe'],
            [['settings'], 'string', 'max' => 255],
            [['app_id'], 'exist', 'skipOnError' => true, 'targetClass' => SoloApplications::className(), 'targetAttribute' => ['app_id' => 'id']],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => SoloClients::className(), 'targetAttribute' => ['client_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'app_id' => 'App ID',
            'client_id' => 'Client ID',
            'date' => 'Date',
            'date_limit' => 'Date Limit',
            'installed' => 'Installed',
            'settings' => 'Settings',
            'user_limit' => 'User Limit',
        ];
    }

    /**
     * Gets query for [[App]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApp()
    {
        return $this->hasOne(SoloApplications::className(), ['id' => 'app_id']);
    }

    /**
     * Gets query for [[Client]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(SoloClients::className(), ['id' => 'client_id']);
    }

    /**
     * Gets query for [[SoloAppHostApps]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSoloAppHostApps()
    {
        return $this->hasMany(SoloAppHostApps::className(), ['purch_app_id' => 'id']);
    }
}
