<?php

namespace frontend\modules\bitrix24\models;

use common\components\date\DateHelper;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\OrderDocument;
use common\models\document\OrderDocumentProduct;
use common\models\document\OrderPackingList;
use common\models\document\PackingList;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\OrderDocumentStatus;
use common\models\employee\Employee;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductInitialBalance;
use common\models\product\ProductStore;
use common\models\product\ProductTurnover;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\Cookie;

class ProductSearch extends Product
{
    public $documentType;
    public $exclude;

    public $balanceStart;
    public $balanseEnd;

    public $balanceStartValue;
    public $balanceEndValue;
    public $periodBuyValue;
    public $periodSaleValue;

    public $amountForBuy;
    public $amountForSell;

    public $reserveCount;
    public $availableCount;

    public $store_id;
    public $quantity;
    public $reserve;
    public $available;

    public $filterStatus = self::ALL;
    public $filterImage = self::ALL;
    public $filterComment = self::ALL;
    public $filterDate = self::ALL;
    public $storeArchive = false;

    public $dateFrom;
    public $dateTo;

    public $turnoverType = self::TURNOVER_BY_COUNT;

    const ALL = 0;

    const IN_ARCHIVE = 1;
    const IN_WORK = 2;
    const IN_RESERVE = 3;

    const HAS_IMAGE = 1;
    const NO_IMAGE = 2;

    const HAS_COMMENT = 1;
    const NO_COMMENT = 2;

    const FILTER_YESTERDAY = 1;
    const FILTER_TODAY = 2;
    const FILTER_WEEK = 3;
    const FILTER_MONTH = 4;


    const DEFAULT_SORTING_TITLE = 'title';
    const DEFAULT_SORTING_TITLE_EN = 'title_en';
    const DEFAULT_SORTING_GROUP_ID = 'group_id';

    const TURNOVER_BY_COUNT = 1;
    const TURNOVER_BY_AMOUNT = 2;

    private $_summary;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'title',
                    'article',
                    'group_id',
                    'product_unit_id',
                ],
                'safe',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'quantity' => 'Количество на складе',
        ]);
    }

    /**
     * @return ProductQuery
     */
    public function baseQuery()
    {
        $query = ProductSearch::find()->alias('product')
            ->byCompany($this->company_id)
            ->byUser()
            ->notForSale($this->documentType == Documents::IO_TYPE_OUT ? false : null)
            ->andWhere(['product.production_type' => $this->production_type])
            ->andWhere(['product.is_deleted' => false])
            ->andWhere(['not', ['product.status' => Product::DELETED]]);

        if (!empty($this->exclude)) {
            $query->andWhere(['not', ['product.id' => $this->exclude]]);
        }

        $query->andFilterWhere([
            'group_id' => $this->group_id,
            'product_unit_id' => $this->product_unit_id,
            'article' => $this->article,
        ]);

        if ($this->title) {
            $query->andWhere([
                'or',
                ['like', Product::tableName() . '.title', $this->title],
                ['like', Product::tableName() . '.article', $this->title],
            ]);
        }

        return $query;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param string $formName
     * @param bool $hideZeroesQuantityProducts
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        $this->load($params);

        $query = $this->baseQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'totalCount' => $this->baseQuery()->limit(-1)->offset(-1)->orderBy([])->count('product.id'),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'title' => SORT_ASC,
                ],
            ]
        ]);

        return $dataProvider;
    }

    /**
     * @return float
     */
    public function getAllQuantity()
    {
        return $this->_summary[0]['quantity'] ?? 0;
    }

    /**
     * @return float
     */
    public function getAllReserve()
    {
        return $this->_summary[0]['reserve'] ?? 0;
    }

    /**
     * @return float
     */
    public function getAllReserveAmount()
    {
        return $this->_summary[0]['reserve_amount'] ?? 0;
    }

    /**
     * @return integer
     */
    public function getAllAmountBuy()
    {
        return $this->_summary[0]['buy_amount'] ?? 0;
    }

    /**
     * @return integer
     */
    public function getAllAmountSell()
    {
        return $this->_summary[0]['sell_amount'] ?? 0;
    }

    protected function createTmpTable()
    {
        $db = Yii::$app->db;

        $query1 = (new \yii\db\Query())
            ->select([
                'product_id',
                'quantity' => 'IF([[type]]=2,-[[quantity]],[[quantity]])',
            ])
            ->from([
                't' => ProductTurnover::tableName()
            ])
            ->leftJoin([
                'p' => Product::tableName()
            ], '{{p}}.[[id]]={{t}}.[[product_id]]')
            ->andWhere([
                't.company_id' => $this->company_id,
                't.production_type' => Product::PRODUCTION_TYPE_GOODS,
                't.is_invoice_actual' => 1,
                't.is_document_actual' => 1,
                'p.is_deleted' => false
            ])
            ->andWhere(['not', ['p.status' => Product::DELETED]])
            ->andWhere([
                '<=',
                't.date',
                date('Y-m-d'),
            ]);

        $query = (new \yii\db\Query())
            ->select([
                'product_id',
                'balance' => 'SUM([[quantity]])',
            ])
            ->from(['t' => $query1])
            ->groupBy('product_id');

        $sql = $query->createCommand()->rawSql;

        $comand = $db->createCommand("DROP TEMPORARY TABLE IF EXISTS __product_balance");
        $comand->execute();
        $comand = $db->createCommand("CREATE TEMPORARY TABLE `__product_balance` (
            `product_id` INT(11) NOT NULL,
            `balance` DECIMAL(20,10) NOT NULL,
            PRIMARY KEY (`product_id`)
        )");
        $comand->execute();
        $comand = $db->createCommand("INSERT INTO __product_balance (`product_id`, `balance`) {$sql}");
        $comand->execute();
    }

}
