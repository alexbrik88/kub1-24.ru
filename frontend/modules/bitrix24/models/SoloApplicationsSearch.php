<?php

namespace frontend\modules\bitrix24\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\bitrix24\models\SoloApplications;

/**
 * SoloApplicationsSearch represents the model behind the search form of `frontend\modules\bitrix24\models\SoloApplications`.
 */
class SoloApplicationsSearch extends SoloApplications
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'demo_period', 'active'], 'integer'],
            [['code', 'name', 'short_descr', 'prefix', 'app_id', 'app_secret'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SoloApplications::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'demo_period' => $this->demo_period,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'short_descr', $this->short_descr])
            ->andFilterWhere(['like', 'prefix', $this->prefix])
            ->andFilterWhere(['like', 'app_id', $this->app_id])
            ->andFilterWhere(['like', 'app_secret', $this->app_secret]);

        return $dataProvider;
    }
}
