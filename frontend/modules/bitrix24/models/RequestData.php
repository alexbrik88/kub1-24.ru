<?php

namespace frontend\modules\bitrix24\models;

use Yii;

class RequestData extends \yii\base\Model
{
    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_IFRAME = 'iframe';

    public static $getAttributes = [
        'app',
        'DOMAIN',
        'PROTOCOL',
        'LANG',
        'APP_SID',
    ];

    public static $postAttributes = [
        'AUTH_ID',
        'AUTH_EXPIRES',
        'REFRESH_ID',
        'PLACEMENT',
        'PLACEMENT_OPTIONS',
        'member_id',
        'status',
    ];

    public $app;
    public $DOMAIN;
    public $PROTOCOL;
    public $LANG;
    public $APP_SID;
    public $AUTH_ID;
    public $AUTH_EXPIRES;
    public $REFRESH_ID;
    public $member_id;
    public $status;
    public $PLACEMENT;
    public $PLACEMENT_OPTIONS;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_IFRAME => [
                'DOMAIN',
                'PROTOCOL',
                'LANG',
                'APP_SID',
                'AUTH_ID',
                'AUTH_EXPIRES',
                'REFRESH_ID',
                'member_id',
                'status',
                'PLACEMENT',
                'PLACEMENT_OPTIONS',
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'app',
                    'DOMAIN',
                    'PROTOCOL',
                    'LANG',
                    'APP_SID',
                    'AUTH_ID',
                    'AUTH_EXPIRES',
                    'REFRESH_ID',
                    'member_id',
                    'status',
                    'PLACEMENT',
                    'PLACEMENT_OPTIONS',
                ],
                'trim',
            ],
            [
                [
                    'app',
                    'DOMAIN',
                    'PROTOCOL',
                    'LANG',
                    'APP_SID',
                    'AUTH_ID',
                    'AUTH_EXPIRES',
                    'REFRESH_ID',
                    'member_id',
                    'status',
                    'PLACEMENT',
                    'PLACEMENT_OPTIONS',
                ],
                'required',
            ],
            [
                [
                    'app',
                ],
                'compare',
                'compareValue' => Yii::$app->bitrix24Helper->app->code,
            ],
        ];
    }
}
