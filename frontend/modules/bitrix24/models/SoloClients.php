<?php

namespace frontend\modules\bitrix24\models;

use Yii;
use common\models\Company;
use common\models\document\Invoice;

/**
 * This is the model class for table "bitrix24_client".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $portal
 * @property int|null $user_id
 * @property string|null $data
 *
 * @property SoloAppHostApps[] $soloAppHostApps
 * @property SoloPurchasedApplications[] $soloPurchasedApplications
 */
class SoloClients extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bitrix24_client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['title'], 'string', 'max' => 50],
            [['portal'], 'string', 'max' => 100],
            [['data'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'portal' => 'Portal',
            'user_id' => 'User ID',
            'data' => 'Data',
        ];
    }

    /**
     * Gets query for [[SoloAppHostApps]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSoloAppHostApps()
    {
        return $this->hasMany(SoloAppHostApps::className(), ['company_id' => 'id']);
    }

    /**
     * Gets query for [[SoloAppHostApps]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), [
            'id' => 'company_id',
        ])->viaTable(WidgetCompany::tableName(), [
            'client_id' => 'id',
        ])->andOnCondition([
            'blocked' => false,
        ]);
    }

    /**
     * Gets query for [[SoloAppHostApps]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['id' => 'invoice_id'])->viaTable('{{%bitrix24_widget_invoice}}', ['account_id' => 'id']);
    }

    /**
     * Gets query for [[SoloPurchasedApplications]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSoloPurchasedApplications()
    {
        return $this->hasMany(SoloPurchasedApplications::className(), ['client_id' => 'id']);
    }
}
