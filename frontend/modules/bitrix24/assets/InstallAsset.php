<?php

namespace frontend\modules\bitrix24\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class InstallAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/bitrix24/assets/install';

    /**
     * @var array
     */
    public $css = [
        'https://fonts.googleapis.com/css?family=Roboto:300,400,500&display=swap',
        'https://fonts.googleapis.com/icon?family=Material+Icons',
        'styles.016542450068f02f5e74.css',
    ];

    /**
     * @var array
     */
    public $js = [
        [
            'runtime-es2015.cdfb0ddb511f65fdc0a0.js', 'type' => 'module',
        ],
        [
            'runtime-es5.cdfb0ddb511f65fdc0a0.js', 'nomodule' => true, 'defer' => true,
        ],
        [
            'polyfills-es5.0290b245fbcca09184ac.js', 'nomodule' => true, 'defer' => true,
        ],
        [
            'polyfills-es2015.ffa9bb4e015925544f91.js', 'type' => 'module',
        ],
        [
            'main-es2015.ac6959af802d0baef4a1.js', 'type' => 'module',
        ],
        [
            'main-es5.ac6959af802d0baef4a1.js', 'nomodule' => true, 'defer' => true,
        ],
        [
            'vendor-es2015.df8f015f032d94011504.js', 'type' => 'module',
        ],
        [
            'vendor-es5.df8f015f032d94011504.js', 'nomodule' => true, 'defer' => true,
        ],
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
