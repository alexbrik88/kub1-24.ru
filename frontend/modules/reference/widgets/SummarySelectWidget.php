<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 07.12.2019
 * Time: 18:06
 */

namespace frontend\modules\reference\widgets;

use yii\base\Widget;

class SummarySelectWidget extends Widget
{
    public $buttons = [];

    public function run()
    {
        return $this->render('summary_select', ['widget' => $this]);
    }
}