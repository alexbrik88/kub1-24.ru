<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 02.12.2019
 * Time: 23:56
 */

namespace frontend\modules\reference\controllers;

use Carbon\Carbon;
use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\ImageHelper;
use common\models\balance\BalanceArticle;
use common\models\bank\Bank;
use common\models\Contractor;
use common\models\employee\Employee;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\components\PageSize;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\models\AbstractBankModel;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\modules\reference\models\BalanceArticleForm;
use frontend\modules\reference\models\BalanceArticlesSearch;
use frontend\modules\reference\models\SellBalanceArticleForm;
use frontend\modules\reference\models\UpdateBalanceArticleForm;
use Yii;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class BalanceArticlesController extends FrontendController
{
    public $layout = '@frontend/themes/kub/modules/reports/views/layouts/finance';

    /**
     * @return array
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function($rule, $action) {
                            if (BusinessAnalyticsAccess::matchSubscribe(BusinessAnalyticsAccess::SECTION_FINANCE)) {
                                if (BusinessAnalyticsAccess::matchLimit(BusinessAnalyticsAccess::SECTION_FINANCE)) {
                                    return true;
                                }

                                throw new \yii\web\ForbiddenHttpException('Превышен лимит сотрудников');
                            }

                            $tarriffNameArray = BusinessAnalyticsAccess::sectionTariffNames(BusinessAnalyticsAccess::SECTION_FINANCE);

                            throw new \yii\web\ForbiddenHttpException(
                                'Чтобы получить доступ в раздел, необходимо '.
                                \yii\helpers\Html::a('оплатить', ['/subscribe/default/index']) .
                                ' один из тарифов: ' . implode(', ', $tarriffNameArray)
                            );
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return array_merge(parent::actions(), [
            'add-modal-contractor' => [
                'class' => 'frontend\components\AddModalContractorAction',
            ],
        ]);
    }

    public function beforeAction($action)
    {
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        if ($user->is_old_kub_theme) {
            return $this->redirect(['/site/only-new-design', 'redirectUri' => Yii::$app->request->absoluteUrl])->send();
        }

        if (parent::beforeAction($action)) {
            return true;
        }

        return false;
    }

    public function actionIndex($type)
    {
        $searchModel = new BalanceArticlesSearch($type);
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->pagination->pageSize = PageSize::get();

        return $this->render('index', [
            'type' => (int)$type,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate($type, $id = null)
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect(Yii::$app->request->referrer ?? ['index', 'type' => $type]);
        }

        $article = null;
        if ($id !== null) {
            $article = $this->findModel($id);
        }
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $model = new BalanceArticleForm($user, (int)$type, $article);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $message = (int)$type === BalanceArticle::TYPE_FIXED_ASSERTS
                ? 'Основное средство успешно добавлено.'
                : 'Нематериальный актив успешно добавлен.';
            Yii::$app->session->setFlash('success', $message);

            return $this->redirect(['index', 'type' => $type]);
        }

        return $this->renderAjax('_partial/_form', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (!Yii::$app->request->isAjax) {
            $this->redirect(Yii::$app->request->referrer ?? ['index', 'type' => $model->type]);
        }

        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $form = new UpdateBalanceArticleForm($model, $user);
        if ($form->load(Yii::$app->request->post()) && $form->save()) {
            $message = $model->type === BalanceArticle::TYPE_FIXED_ASSERTS
                ? 'Основное средство успешно обновлено.'
                : 'Нематериальный актив успешно обновлен.';
            Yii::$app->session->setFlash('success', $message);

            return $this->redirect(['index', 'type' => $model->type]);
        }

        return $this->renderAjax('_partial/_update_form', [
            'model' => $form,
        ]);
    }

    public function actionWriteOff($type)
    {
        $errorMessage = 'Произошла ошибка при списании '
            . ((int)$type === BalanceArticle::TYPE_FIXED_ASSERTS ? 'основных средств.' : 'нематериальных активов.');
        try {
            $writtenOffAt = Carbon::parse(Yii::$app->request->post('written_off_at'))
                ->format(DateHelper::FORMAT_DATE);
        } catch (\Exception $e) {
            Yii::error($e);
            Yii::$app->session->setFlash('error', $errorMessage);

            return $this->redirect(['index', 'type' => $type]);
        }

        $models = Yii::$app->request->post('BalanceArticle');

        $transaction = Yii::$app->db->beginTransaction();

        foreach ($models as $id => $model) {
            if ($model['checked']) {
                $balanceArticle = $this->findModel($id);
                $balanceArticle->status = BalanceArticle::STATUS_WRITTEN_OFF;
                $balanceArticle->written_off_at = $writtenOffAt;
                if (!$balanceArticle->save()) {
                    Yii::$app->session->setFlash('error', $errorMessage);
                    $transaction->rollBack();

                    return $this->redirect(['index', 'type' => $type]);
                }
            }
        }

        $transaction->commit();

        Yii::$app->session->setFlash('success', ((int)$type === BalanceArticle::TYPE_FIXED_ASSERTS
                ? 'Основные средства'
                : 'Нематериальные активы') . ' успешно списаны.');

        return $this->redirect(['index', 'type' => $type]);
    }

    public function actionSell($type, $flowType = null, $contractorId = null)
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect(Yii::$app->request->referrer ?? ['index', 'type' => $type]);
        }
        $articleIds = [];
        foreach (Yii::$app->request->post('BalanceArticle', []) as $id => $model) {
            if ($model['checked']) {
                $articleIds[] = $id;
            }
        }

        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $model = new SellBalanceArticleForm($articleIds, $user, (int)$type);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $message = (int)$type === BalanceArticle::TYPE_FIXED_ASSERTS
                ? 'Основные средства успешно проданы.'
                : 'Нематериальные активы успешно проданы.';
            Yii::$app->session->setFlash('success', $message);

            return $this->redirect(['index', 'type' => $type]);
        }

        if ($contractorId !== null) {
            $contractor = Contractor::findOne(['id' => $contractorId]);
        } else {
            $contractor = $model->getContractor();
        }

        return $this->renderAjax('_partial/_sell_form', [
            'model' => $model,
            'type' => (int)$type,
            'flowType' => $flowType,
            'contractor' => $contractor,
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $type = $model->type;

        if ($model->delete()) {
            Yii::$app->session->setFlash('success', $type === BalanceArticle::TYPE_FIXED_ASSERTS
                ? 'Основное средство успешно удалено'
                : 'Нематериальный актив успешно удален');
        } else {
            Yii::$app->session->setFlash('error', 'Произошла ошибка при удалени '
                . ($type === BalanceArticle::TYPE_FIXED_ASSERTS
                    ? 'основного средства'
                    : 'нематериального актива'));
        }

        return $this->redirect(['index', 'type' => $type]);
    }

    public function actionManyDelete($type)
    {
        $models = Yii::$app->request->post('BalanceArticle');

        $transaction = Yii::$app->db->beginTransaction();

        foreach ($models as $id => $model) {
            if ($model['checked']) {
                $balanceArticle = $this->findModel($id);
                if (!$balanceArticle->delete()) {
                    Yii::$app->session->setFlash('error', 'Произошла ошибка при удалении '
                        . ((int)$type === BalanceArticle::TYPE_FIXED_ASSERTS ? 'основных средств.' : 'нематериальных активов.'));
                    $transaction->rollBack();

                    return $this->redirect(['index', 'type' => $type]);
                }
            }
        }

        $transaction->commit();

        Yii::$app->session->setFlash('success', ((int)$type === BalanceArticle::TYPE_FIXED_ASSERTS
                ? 'Основные средства'
                : 'Нематериальные активы') . ' успешно удалены.');

        return $this->redirect(['index', 'type' => $type]);
    }

    /**
     * @param $id
     * @return BalanceArticle|null
     * @throws NotFoundHttpException
     */
    private function findModel($id)
    {
        $model = BalanceArticle::findOne(['id' => $id]);
        if ($model === null) {
            throw new NotFoundHttpException();
        }

        return $model;
    }
}