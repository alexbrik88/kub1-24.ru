<?php

namespace frontend\modules\reference;

/**
 * Class Module
 * @package frontend\modules\reference
 */
class Module extends \yii\base\Module
{
    /**
     * @var string
     */
    public $controllerNamespace = 'frontend\modules\reference\controllers';

    /**
     *
     */
    public function init()
    {
        parent::init();
    }
}
