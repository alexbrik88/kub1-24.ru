<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 06.04.2020
 * Time: 23:42
 */

namespace frontend\modules\reference\models;

use common\components\helpers\ArrayHelper;
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\Employee;
use common\models\ExpenseItemFlowOfFunds;
use common\models\IncomeItemFlowOfFunds;
use frontend\modules\analytics\models\AbstractFinance;
use frontend\modules\reports\models\ProfitAndLossCompanyItem;
use Yii;
use yii\base\Exception;
use yii\base\Model;

class ArticleDropDownForm extends Model
{
    const TYPE_EXISTS_ITEM = 0;
    const TYPE_NEW_ITEM = 1;
    const TYPE_UPDATE_BREAK_EVEN_COLUMN = 2;
    const TYPE_UPDATE_PREPAYMENT_COLUMN = 3;

    public $isNew;

    public $name;

    public $items;

    public $items2;

    public $breakEvenBlock = null;

    /**
     * @var Employee
     */
    private $user;

    private $type;

    public function __construct(Employee $user, $type, array $config = [])
    {
        parent::__construct($config);
        $this->user = $user;
        $this->type = (int)$type;
    }

    public function rules()
    {
        return [
            [['isNew'], 'required', 'message' => 'Необходимо выбрать хотя бы один пункт'],
            [['name'], 'required', 'when' => function (self $model) {
                return $model->isNew !== '' && $model->isNew == self::TYPE_NEW_ITEM;
            }, 'message' => 'Необходимо заполнить имя'],
            [['name'], 'string'],
            [['items'], 'required', 'when' => function (self $model) {
                return $model->isNew !== '' && $model->isNew == self::TYPE_EXISTS_ITEM;
            }, 'message' => 'Необходимо выбрать хотя бы одну статью'],
            [['items'], 'validateItems'],
            [['name'], 'validateName'],
            [['items2'], 'required', 'when' => function (self $model) {
                return $model->isNew !== '' && $model->isNew == self::TYPE_UPDATE_BREAK_EVEN_COLUMN;
            }, 'message' => 'Необходимо выбрать хотя бы одну статью'],
            [['items2'], 'validateItems2'],
        ];
    }

    public function validateName($attribute)
    {
        if ($this->type === ArticlesSearch::TYPE_INCOME) {
            $query = InvoiceIncomeItem::find();
            $errorMessage = 'Такой тип прихода уже существует';
        } else {
            $query = InvoiceExpenditureItem::find();
            $errorMessage = 'Такой тип расхода уже существует';
        }

        if ($query->andWhere([
            'AND',
            ['name' => $this->$attribute],
            [
                'OR',
                ['company_id' => null],
                ['company_id' => $this->user->company_id],
            ],
        ])->exists()) {
            $this->addError($attribute, $errorMessage);
        }
    }

    public function validateItems($attribute)
    {
        if (!is_array($this->$attribute)) {
            $this->addError($attribute, 'Выбраны неверные статьи');
        }

        if (!empty(array_diff($this->$attribute, ArrayHelper::getColumn($this->getExistsItems(), 'id')))) {
            $this->addError($attribute, 'Выбраны неверные статьи');
        }
    }

    public function validateItems2($attribute)
    {
        if (!is_array($this->$attribute)) {
            $this->addError($attribute, 'Выбраны неверные статьи');
        }
    }

    public function save()
    {
        if (!$this->validate()) {
            return null;
        }

        $transaction = Yii::$app->db->beginTransaction();

        $items = [];
        if ($this->isNew == self::TYPE_UPDATE_PREPAYMENT_COLUMN) {

            if ($this->type == ArticlesSearch::TYPE_INCOME) {
                $allItems = IncomeItemFlowOfFunds::find()->where([
                    'company_id' => $this->user->company_id,
                ])->all();
                foreach ($allItems as $item) {
                    if (in_array($item->income_item_id, (array)$this->items2)) {
                        if (!$item->is_prepayment)
                            $item->updateAttributes(['is_prepayment' => true]);
                    } else {
                        if ($item->is_prepayment)
                            $item->updateAttributes(['is_prepayment' => false]);
                    }
                    $items[] = $item->income_item_id;
                }
                
            } elseif ($this->type == ArticlesSearch::TYPE_EXPENSE) {
                $allItems = ExpenseItemFlowOfFunds::find()->where([
                    'company_id' => $this->user->company_id,
                ])->all();
                foreach ($allItems as $item) {
                    if (in_array($item->expense_item_id, (array)$this->items2)) {
                        if (!$item->is_prepayment)
                            $item->updateAttributes(['is_prepayment' => true]);
                    } else {
                        if ($item->is_prepayment)
                            $item->updateAttributes(['is_prepayment' => false]);
                    }
                    $items[] = $item->expense_item_id;
                }
            } else {
                throw new Exception('Unknown articleDropdownForm type');
            }
        }
        elseif ($this->isNew == self::TYPE_UPDATE_BREAK_EVEN_COLUMN) {

            // update break even column
            foreach ($this->items2 as $itemId) {
                $item = ExpenseItemFlowOfFunds::findOne([
                    'expense_item_id' => $itemId,
                    'company_id' => $this->user->company_id,
                ]);

                if ($item !== null) {
                    if ($this->breakEvenBlock !== null) {
                        $item->break_even_block = $this->breakEvenBlock;
                    }
                    if (!$item->save()) {
                        $transaction->rollBack();
                        return null;
                    }

                    $invoiceItem = $item->expenseItem;

                    $items[] = [
                        'id' => $invoiceItem->id,
                        'name' => $invoiceItem->name,
                    ];
                }
            }

        }
        elseif ($this->isNew == self::TYPE_NEW_ITEM) {
            if ($this->type === ArticlesSearch::TYPE_INCOME) {
                $model = new InvoiceIncomeItem();
                $model->company_id = $this->user->company_id;
                $model->name = $this->name;
                if (!$model->save()) {
                    $transaction->rollBack();
                    return null;
                }

                $incomeItemFlowOfFunds = new IncomeItemFlowOfFunds();
                $incomeItemFlowOfFunds->company_id = $this->user->company_id;
                $incomeItemFlowOfFunds->income_item_id = $model->id;
                $incomeItemFlowOfFunds->is_visible = $model->is_visible;
                $incomeItemFlowOfFunds->is_prepayment = true;
                if (!$incomeItemFlowOfFunds->save()) {
                    $transaction->rollBack();
                    return null;
                }
            } else {
                $model = new InvoiceExpenditureItem();
                $model->company_id = $this->user->company_id;
                $model->name = $this->name;
                if (!$model->save()) {
                    $transaction->rollBack();
                    return null;
                }

                $expenseItemFlowOfFunds = new ExpenseItemFlowOfFunds();
                $expenseItemFlowOfFunds->company_id = $this->user->company_id;
                $expenseItemFlowOfFunds->expense_item_id = $model->id;
                $expenseItemFlowOfFunds->is_visible = $model->is_visible;
                $expenseItemFlowOfFunds->is_prepayment = true;
                if ($this->breakEvenBlock !== null) {
                    $expenseItemFlowOfFunds->break_even_block = $this->breakEvenBlock;
                }

                $profitAndLossCompanyItem = new ProfitAndLossCompanyItem();
                $profitAndLossCompanyItem->company_id = $this->user->company_id;
                $profitAndLossCompanyItem->item_id = $model->id;
                $profitAndLossCompanyItem->expense_type = ProfitAndLossCompanyItem::CONSTANT_EXPENSE_TYPE;
                $profitAndLossCompanyItem->can_update = true;
                if (!$expenseItemFlowOfFunds->save() || !$profitAndLossCompanyItem->save()) {
                    $transaction->rollBack();
                    return null;
                }
            }

            $items[] = [
                'id' => $model->id,
                'name' => $model->name,
            ];
        }
        else {
            foreach ($this->items as $itemId) {
                if ($this->type === ArticlesSearch::TYPE_INCOME) {
                    $item = IncomeItemFlowOfFunds::findOne([
                        'income_item_id' => $itemId,
                        'company_id' => $this->user->company_id,
                    ]);
                } else {
                    $item = ExpenseItemFlowOfFunds::findOne([
                        'expense_item_id' => $itemId,
                        'company_id' => $this->user->company_id,
                    ]);
                }

                if ($item !== null) {
                    $item->is_visible = true;
                    if ($this->breakEvenBlock !== null) {
                        $item->break_even_block = $this->breakEvenBlock;
                    }
                    if (!$item->save()) {
                        $transaction->rollBack();
                        return null;
                    }

                    if ($this->type === ArticlesSearch::TYPE_INCOME) {
                        $invoiceItem = $item->incomeItem;
                    } else {
                        $invoiceItem = $item->expenseItem;
                    }

                    $items[] = [
                        'id' => $invoiceItem->id,
                        'name' => $invoiceItem->name,
                    ];
                }
            }
        }

        $transaction->commit();

        return $items;
    }

    public function getExistsItems()
    {
        if ($this->type === ArticlesSearch::TYPE_INCOME) {
            $query = InvoiceIncomeItem::find()
                ->alias('i')
                ->leftJoin(IncomeItemFlowOfFunds::tableName() . ' fi', 'fi.income_item_id = i.id');
        } else {
            $query = InvoiceExpenditureItem::find()
                ->alias('i')
                ->leftJoin(ExpenseItemFlowOfFunds::tableName() . ' fi', 'fi.expense_item_id = i.id');
        }

        return $query
            ->andWhere(['i.parent_id' => null])
            ->andWhere([
                'AND',
                ['fi.company_id' => $this->user->currentEmployeeCompany->company_id],
                [
                    'OR',
                    ['i.company_id' => null],
                    ['i.company_id' => $this->user->currentEmployeeCompany->company_id],
                ],
                ['i.can_be_controlled' => true],
                ['fi.is_visible' => false],
            ])
            ->orderBy('i.name')
            ->all();
    }

    public function getExistsBreakEvenItems()
    {
        $query = InvoiceExpenditureItem::find()
            ->alias('i')
            ->leftJoin(ExpenseItemFlowOfFunds::tableName() . ' fi', 'fi.expense_item_id = i.id');

        $items = $query
            ->andWhere([
                'AND',
                ['fi.company_id' => $this->user->currentEmployeeCompany->company_id],
                ['fi.break_even_block' => null],
                [
                    'OR',
                    ['i.company_id' => null],
                    ['i.company_id' => $this->user->currentEmployeeCompany->company_id],
                ],
                ['fi.is_visible' => true],
            ])
            ->all();

        $ret = [];
        foreach ($items as $i) {
            if ($i->parent_id) {
                $ret[] = [
                    'id' => $i->id,
                    'name' => $i->parent->name .' -> '. $i->name
                ];
            } else {
                $ret[] = [
                    'id' => $i->id,
                    'name' => $i->name
                ];
            }
        }

        uasort($ret, function ($a, $b) {
            if ($a['name'] == $b['name']) return 0;
            return ($a['name'] < $b['name']) ? -1 : 1;
        });

        return $ret;
    }

    public function getExistsFinanceModelItems()
    {
        $query = InvoiceExpenditureItem::find()
            ->alias('i')
            ->leftJoin(ExpenseItemFlowOfFunds::tableName() . ' fi', 'fi.expense_item_id = i.id');

        $items = $query
            ->andWhere([
                'AND',
                ['fi.company_id' => $this->user->currentEmployeeCompany->company_id],
                [
                    'OR',
                    ['i.company_id' => null],
                    ['i.company_id' => $this->user->currentEmployeeCompany->company_id],
                ],
                ['fi.is_visible' => true],
            ])
            ->all();

        $ret = [];
        foreach ($items as $i) {
            if ($i->parent_id) {
                $ret[] = [
                    'id' => $i->id,
                    'name' => $i->parent->name .' -> '. $i->name
                ];
            } else {
                $ret[] = [
                    'id' => $i->id,
                    'name' => $i->name
                ];
            }
        }

        uasort($ret, function ($a, $b) {
            if ($a['name'] == $b['name']) return 0;
            return ($a['name'] < $b['name']) ? -1 : 1;
        });

        return $ret;
    }

    public function getDebtorPrepaymentItems()
    {
        if ($this->type === ArticlesSearch::TYPE_INCOME) {
            $query = InvoiceIncomeItem::find()
                ->alias('i')
                ->leftJoin(IncomeItemFlowOfFunds::tableName() . ' fi', 'fi.income_item_id = i.id');
        } else {
            $query = InvoiceExpenditureItem::find()
                ->alias('i')
                ->leftJoin(ExpenseItemFlowOfFunds::tableName() . ' fi', 'fi.expense_item_id = i.id');
        }

        return $query
            ->andWhere(['i.parent_id' => null])            
            ->andWhere([
                'AND',
                ['fi.company_id' => $this->user->currentEmployeeCompany->company_id],
                [
                    'OR',
                    ['i.company_id' => null],
                    ['i.company_id' => $this->user->currentEmployeeCompany->company_id],
                ],
            ])
            ->orderBy('i.name')
            ->select('i.id, i.name, fi.is_prepayment, i.company_id')
            ->asArray()
            ->all();
    }
}