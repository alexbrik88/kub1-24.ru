<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 28.11.2019
 * Time: 22:25
 */

namespace frontend\modules\reference\models;

use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\Employee;
use common\models\ExpenseItemFlowOfFunds;
use common\models\IncomeItemFlowOfFunds;
use frontend\modules\reports\models\AbstractFinance;
use frontend\modules\reports\models\ProfitAndLossCompanyItem;
use Yii;
use yii\base\Model;

class ArticleForm extends Model
{
    const TYPE_INCOME = 0;
    const TYPE_EXPENSE = 1;

    const TYPE_ITEM_MAP = [
        self::TYPE_INCOME => InvoiceIncomeItem::class,
        self::TYPE_EXPENSE => InvoiceExpenditureItem::class,
    ];

    const TYPE_ITEM_FLOW_OF_FUNDS_MAP = [
        self::TYPE_INCOME => IncomeItemFlowOfFunds::class,
        self::TYPE_EXPENSE => ExpenseItemFlowOfFunds::class,
    ];

    public $type;

    public $parentId;

    public $name;

    public $activityType;

    public $sellers = [];

    public $customers = [];

    /** @var Employee */
    private $user;

    public function __construct(Employee $user, array $config = [])
    {
        parent::__construct($config);
        $this->user = $user;
    }

    public function rules()
    {
        return [
            [['type', 'name', 'activityType'], 'required'],
            [['type'], 'boolean'],
            [['activityType'], 'in', 'range' => array_keys(AbstractFinance::$blocks)],
            [['name'], 'string'],
            [['activityType'], 'integer'],
            [['name'], 'validateName'],
            [['sellers', 'customers'], 'safe'],
            [['parentId'], 'integer']
        ];
    }

    public function validateName($attribute)
    {
        /** @var InvoiceIncomeItem|InvoiceExpenditureItem|null $className */
        $className = self::TYPE_ITEM_MAP[$this->type] ?? null;
        if ($className === null) {
            return;
        }

        if ($className::find()
            ->andWhere(['name' => $this->$attribute])
            ->andWhere([
                'OR',
                ['company_id' => null],
                ['company_id' => $this->user->currentEmployeeCompany->company_id],
            ])->exists()) {
            $this->addError($attribute, 'Такое название статьи уже существует.');
        }
    }

    public function attributeLabels()
    {
        return [
            'type' => 'Тип статьи',
            'name' => 'Название статьи',
            'activityType' => 'Вид деятельности',
        ];
    }

    public function getUser()
    {
        return $this->user;
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $parentModel = $parentFlowOfFundsItem = null;
        if ($this->parentId) {
                if ($this->type == 1) {
                    $parentQuery = InvoiceExpenditureItem::find();
                }
                if ($this->type == 0) {
                    $parentQuery = InvoiceIncomeItem::find();
                }

            if (isset($parentQuery)) {
                $parentModel = $parentQuery
                    ->andWhere(['id' => $this->parentId])
                    ->andWhere(['or', ['company_id' => null], ['company_id' => $this->user->currentEmployeeCompany->company_id]])
                    ->one();
            }
            if ($parentModel) {
                $parentFlowOfFundsItem = $parentModel->getFlowOfFundsItemByCompany(Yii::$app->user->identity->currentEmployeeCompany->company_id)->one();
            }
        }

        $transaction = Yii::$app->db->beginTransaction();

        $itemClassName = self::TYPE_ITEM_MAP[$this->type];
        /** @var InvoiceIncomeItem|InvoiceExpenditureItem $item */
        $item = new $itemClassName();
        $item->company_id = $this->user->currentEmployeeCompany->company_id;
        $item->name = $this->name;
        $item->parent_id = ($parentModel) ? $parentModel->id : null;
        if (!$item->save()) {
            $transaction->rollBack();
            return false;
        }

        $itemFlowOfFundsClassName = self::TYPE_ITEM_FLOW_OF_FUNDS_MAP[$this->type];
        /** @var IncomeItemFlowOfFunds|ExpenseItemFlowOfFunds $itemFlowOfFunds */
        $itemFlowOfFunds = new $itemFlowOfFundsClassName();
        $itemFlowOfFunds->company_id = $item->company_id;
        $itemFlowOfFunds->is_visible = ($parentFlowOfFundsItem) ? $parentFlowOfFundsItem->is_visible : $item->is_visible;
        $itemFlowOfFunds->flow_of_funds_block = $this->activityType ?: AbstractFinance::OPERATING_ACTIVITIES_BLOCK;
        if ((int)$this->type === self::TYPE_INCOME) {
            $itemFlowOfFunds->income_item_id = $item->id;
            $contractorItemAttribute = 'invoice_income_item_id';
        } else {
            $itemFlowOfFunds->expense_item_id = $item->id;
            $contractorItemAttribute = 'invoice_expenditure_item_id';
        }

        if (!$itemFlowOfFunds->save()) {
            $transaction->rollBack();
            return false;
        }

        if ((int)$this->type === self::TYPE_INCOME) {
            $contractors = $this->customers;
        } else {
            $contractors = $this->sellers;
        }

        if ($contractors) {
            $contractorsModels = Contractor::find()
                ->byCompany($this->user->currentEmployeeCompany->company_id)
                ->andWhere(['IN', 'id', $contractors])
                ->all();

            /** @var Contractor $c */
            foreach ($contractorsModels as $c) {
                $c->{$contractorItemAttribute} = $item->id;
                if (!$c->save(true, [$contractorItemAttribute])) {
                    $transaction->rollBack();
                    return false;
                }
            }
        }

        $transaction->commit();

        return true;
    }
}