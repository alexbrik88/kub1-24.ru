<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 05.07.2020
 * Time: 23:05
 */

namespace frontend\modules\reference\models;

use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\models\cash\query\CashOrderFlow;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\Employee;
use common\models\ExpenseItemFlowOfFunds;
use common\models\IncomeItemFlowOfFunds;
use frontend\modules\reports\models\AbstractFinance;
use InvalidArgumentException;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class AddArticleToContractorForm extends Model
{
    public $type;
    public $activityType;
    public $contractorName;
    public $itemId;
    public $subItemId;
    public $changeOldOperations = false;

    private $contractorId;

    /**
     * @var Employee
     */
    private $user;

    /**
     * @var Contractor
     */
    private $contractor;

    /** @var null|array */
    private $articlesByTypes = null;

    public function init()
    {
        parent::init();
        $this->contractor = $this->getContractor();
        if ($this->contractor === null) {
            throw new InvalidArgumentException('Invalid contractor id param');
        }

        $this->contractorName = $this->contractor->getNameWithType();
        $this->itemId = $this->contractor->type === Contractor::TYPE_CUSTOMER
            ? $this->contractor->invoice_income_item_id
            : $this->contractor->invoice_expenditure_item_id;
        if ($this->itemId !== null) {

            // check is child
            if ($_item = ($this->contractor->type === Contractor::TYPE_CUSTOMER) ?
                InvoiceIncomeItem::findOne([$this->itemId]) : InvoiceExpenditureItem::findOne([$this->itemId])) {
                if ($_item->parent_id) {
                    $this->itemId = $_item->parent_id;
                    $this->subItemId = $_item->id;
                }
            }

            $articlesByTypes = $this->getArticlesByTypes();
            foreach ($articlesByTypes as $type => $articles) {
                if (array_search($this->itemId, $articles) !== false) {
                    $this->activityType = $type;
                    break;
                }
            }
        } else {
            $this->activityType = AbstractFinance::OPERATING_ACTIVITIES_BLOCK;
        }
    }

    public function __construct(int $contractorId, Employee $user, int $type, array $config = [])
    {
        $this->user = $user;
        $this->contractorId = $contractorId;
        $this->type = $type;
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['itemId'], 'required'],
            [['changeOldOperations'], 'boolean'],
            [['itemId'], 'integer'],
            [['subItemId'], 'filter', 'filter' => function ($value) {
                return (int) $value;
            }],
            [['subItemId'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'type' => 'Тип статьи',
            'name' => 'Название статьи',
            'activityType' => 'Вид деятельности',
            'contractorName' => $this->type === ArticlesSearch::TYPE_INCOME ? 'Покупатель' : 'Поставщик',
            'itemId' => 'Название статьи',
            'subItemId' => 'Название подстатьи',
        ];
    }

    public function save(): bool
    {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        $contractor = $this->contractor;
        if ($contractor->type === Contractor::TYPE_CUSTOMER) {
            $class = InvoiceIncomeItem::class;
            $contractorItemField = 'invoice_income_item_id';
            $operationField = 'income_item_id';
            $operationFlowType = CashBankFlows::FLOW_TYPE_INCOME;
        } else {
            $class = InvoiceExpenditureItem::class;
            $contractorItemField = 'invoice_expenditure_item_id';
            $operationField = 'expenditure_item_id';
            $operationFlowType = CashBankFlows::FLOW_TYPE_EXPENSE;
        }

        /** @var InvoiceIncomeItem|InvoiceExpenditureItem $class */
        $item = $class::findOne([
            ['id' => ($this->subItemId ?: $this->itemId)],
            ['company_id' => $this->user->currentEmployeeCompany->company_id]
        ]);

        if ($item === null) {
            $transaction->rollBack();
            return false;
        }

        $contractor->$contractorItemField = $item->id;
        if (!$contractor->save()) {
            $transaction->rollBack();
            return false;
        }

        if ($this->changeOldOperations) {
            $this->updateOldOperations($operationField, $operationFlowType);
        }

        $transaction->commit();

        return true;
    }

    public function getContractor(): ?Contractor
    {
        return Contractor::findOne([
            ['id' => $this->contractorId],
            ['company_id' => $this->user->currentEmployeeCompany->company_id],
        ]);
    }

    public function getSubarticles($type, $byParentId = null)
    {
        $query = ($type == ArticlesSearch::TYPE_EXPENSE) ? InvoiceExpenditureItem::find() : InvoiceIncomeItem::find();
        $rows = $query
            ->select(['parent_id', 'id', 'name'])
            ->where(['company_id' => $this->user->currentEmployeeCompany->company_id])
            ->andWhere(['not', ['parent_id' => null]])
            ->indexBy('id')
            ->orderBy('name')
            ->all();

        $ret = [];
        foreach ($rows as $r) {
            if (!isset($ret[$r['parent_id']])) {
                $ret[$r['parent_id']] = [
                    [
                        'id' => null,
                        'name' => ''
                    ]
                ];
            }

            $ret[$r['parent_id']][] = [
                'id' => $r['id'],
                'name' => $r['name']
            ];
        }

        if ($byParentId)
            return ArrayHelper::getValue($ret, $byParentId);

        return $ret;
    }

    public function getArticlesByTypes(): array
    {
        if ($this->articlesByTypes !== null) {
            return $this->articlesByTypes;
        }

        $typeItem = [
            AbstractFinance::FINANCIAL_OPERATIONS_BLOCK => [
                Contractor::TYPE_CUSTOMER => [
                    InvoiceIncomeItem::ITEM_LOAN,
                    InvoiceIncomeItem::ITEM_CREDIT,
                    InvoiceIncomeItem::ITEM_FROM_FOUNDER,
                    InvoiceIncomeItem::ITEM_BORROWING_REDEMPTION,
                    InvoiceIncomeItem::ITEM_RECEIVED_PERCENTS,
                    InvoiceIncomeItem::ITEM_RECEIVED_PERCENTS_BY_DEPOSITS,
                ],
                Contractor::TYPE_SELLER => [
                    InvoiceExpenditureItem::ITEM_LOAN_REPAYMENT,
                    InvoiceExpenditureItem::ITEM_REPAYMENT_CREDIT,
                    InvoiceExpenditureItem::ITEM_PAYMENT_PERCENT,
                    InvoiceExpenditureItem::ITEM_RETURN_FOUNDER,
                    InvoiceExpenditureItem::ITEM_BORROWING_EXTRADITION,
                    InvoiceExpenditureItem::ITEM_DIVIDEND_PAYMENT,
                ],
            ],
            AbstractFinance::OPERATING_ACTIVITIES_BLOCK => [
                Contractor::TYPE_CUSTOMER => [],
                Contractor::TYPE_SELLER => [],
            ],
            AbstractFinance::INVESTMENT_ACTIVITIES_BLOCK => [
                Contractor::TYPE_CUSTOMER => [],
                Contractor::TYPE_SELLER => [],
            ],
        ];
        foreach ($typeItem as $blockId => $blockData) {
            $itemsIds = $blockData[$this->contractor->type];
            if ($this->contractor->type === Contractor::TYPE_CUSTOMER) {
                $itemsClassName = InvoiceIncomeItem::class;
                $excludedItem = InvoiceIncomeItem::ITEM_OWN_FOUNDS;
                $flowOfFundItemName = 'income_item_id';
                $query = $itemsClassName::find()
                    ->alias('i')
                    ->leftJoin(IncomeItemFlowOfFunds::tableName() . ' fi', 'fi.income_item_id = i.id');
            } else {
                $itemsClassName = InvoiceExpenditureItem::class;
                $excludedItem = InvoiceExpenditureItem::ITEM_OWN_FOUNDS;
                $flowOfFundItemName = 'expense_item_id';
                $query = $itemsClassName::find()
                    ->alias('i')
                    ->leftJoin(ExpenseItemFlowOfFunds::tableName() . ' fi', 'fi.expense_item_id = i.id');
            }

            if ($blockId === AbstractFinance::OPERATING_ACTIVITIES_BLOCK) {
                $itemsIds = $itemsClassName::find()
                    ->andWhere(['company_id' => null])
                    ->andWhere(['not', ['in', 'id', $typeItem[AbstractFinance::FINANCIAL_OPERATIONS_BLOCK][$this->contractor->type]]])
                    ->andWhere(['not', ['id' => $excludedItem]])
                    ->column();
            }

            $this->articlesByTypes[$blockId] = $query->select(['i.id'])
                ->joinWith('contractors c')
                ->andWhere([
                    'AND',
                    ['not', ['i.id' => $excludedItem]],
                    ['fi.company_id' => $this->user->currentEmployeeCompany->company_id],
                    [
                        'OR',
                        ['i.company_id' => null],
                        ['i.company_id' => $this->user->currentEmployeeCompany->company_id],
                    ],
                    [
                        'OR',
                        ['fi.flow_of_funds_block' => $blockId],
                        ['and',
                            ['fi.flow_of_funds_block' => null],
                            ['in', "fi.{$flowOfFundItemName}", $itemsIds],
                        ],
                        $blockId == AbstractFinance::OPERATING_ACTIVITIES_BLOCK ?
                            ['and',
                                ['fi.flow_of_funds_block' => null],
                                ['not', ['i.company_id' => null]],
                            ] : [],
                    ],
                ])
                ->andWhere(['parent_id' => null])
                ->groupBy('i.id')
                ->orderBy('i.name')
                ->column();
        }

        return $this->articlesByTypes;
    }

    private function updateOldOperations(string $operationItemAttribute, int $flowType)
    {
        /** @var CashBankFlows|CashOrderFlows|CashEmoneyFlows $cashFlowClass */
        foreach ([CashBankFlows::class, CashOrderFlows::class, CashEmoneyFlows::class] as $cashFlowClass) {
            $cashFlowClass::updateAll([$operationItemAttribute => $this->itemId], [
                'AND',
                ['flow_type' => $flowType],
                ['company_id' => $this->user->currentEmployeeCompany->company_id],
                ['contractor_id' => (string)$this->contractor->id],
                [
                    'OR',
                    ['NOT', [$operationItemAttribute => (int)$this->itemId]],
                    [$operationItemAttribute => null],
                ],
            ]);
        }
    }

}
