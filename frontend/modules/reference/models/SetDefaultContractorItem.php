<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 02.04.2020
 * Time: 21:49
 */

namespace frontend\modules\reference\models;

use common\models\Company;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use yii\base\Model;

class SetDefaultContractorItem extends Model
{
    public $expenditureItemId;

    public $incomeItemId;

    /**
     * @var Company
     */
    private $company;

    public function init()
    {
        parent::init();
        $this->expenditureItemId = $this->company->default_contractor_expenditure_item_id;
        $this->incomeItemId = $this->company->default_contractor_income_item_id;
    }

    public function __construct(Company $company, array $config = [])
    {
        $this->company = $company;
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['expenditureItemId', 'incomeItemId'], 'integer'],
            [['expenditureItemId'], 'exist', 'targetClass' => InvoiceExpenditureItem::class, 'targetAttribute' => 'id'],
            [['incomeItemId'], 'exist', 'targetClass' => InvoiceIncomeItem::class, 'targetAttribute' => 'id'],
        ];
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $company = $this->company;
        if ($this->expenditureItemId !== null) {
            $company->default_contractor_expenditure_item_id = $this->expenditureItemId;
        }

        if ($this->incomeItemId !== null) {
            $company->default_contractor_income_item_id = $this->incomeItemId;
        }

        return $company->save();
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }
}