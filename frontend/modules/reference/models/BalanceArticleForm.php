<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.12.2019
 * Time: 0:06
 */

namespace frontend\modules\reference\models;

use Carbon\Carbon;
use common\components\date\DatePickerFormatBehavior;
use common\components\TextHelper;
use common\models\balance\BalanceArticle;
use common\models\employee\Employee;
use Yii;
use yii\base\Model;

class BalanceArticleForm extends Model
{
    public $type;

    public $name;

    public $category;

    public $subcategory;

    public $useful_life_in_month;

    public $count;

    public $purchased_at;

    public $amount;

    public $description;

    /**
     * @var Employee
     */
    private $user;

    public function __construct(Employee $user, $type, BalanceArticle $article = null, array $config = [])
    {
        parent::__construct($config);
        if (!in_array($type, [BalanceArticle::TYPE_FIXED_ASSERTS, BalanceArticle::TYPE_INTANGIBLE_ASSETS], true)) {
            throw new \InvalidArgumentException("Unknown type: {$type}");
        }

        $this->type = $type;
        $this->user = $user;
        if ($this->category === null) {
            $this->category = ($this->type === BalanceArticle::TYPE_FIXED_ASSERTS)
                ? BalanceArticlesCategories::REAL_ESTATE
                : BalanceArticlesCategories::SOFTWARE;
        }

        if ($article !== null) {
            $this->name = $article->name;
            $this->category = $article->category;
            $this->subcategory = $article->subcategory;
            $this->useful_life_in_month = $article->useful_life_in_month;
            $this->count = $article->count;
            $this->purchased_at = $article->purchased_at;
            $this->amount = $article->amount;
            $this->description = $article->description;
        }
    }

    public function behaviors()
    {
        return [
            [
                'class' => DatePickerFormatBehavior::class,
                'attributes' => [
                    'purchased_at' => [
                        'message' => 'Дата приобретения указана неверно.',
                    ],
                ],
                'dateFormatParams' => [
                    'whenClient' => 'function(){}',
                ],
            ],
        ];
    }

    public function rules()
    {
        return [
            [
                ['name', 'category', 'subcategory', 'count', 'purchased_at', 'amount'],
                'required',
                'message' => 'Необходимо заполнить.',
            ],
            [['name'], 'string', 'max' => 255],
            [['description'], 'string'],
            [['category', 'subcategory', 'count', 'useful_life_in_month'], 'integer'],
            [['category'], 'in', 'range' => array_keys(BalanceArticlesCategories::MAP)],
            [['subcategory'], 'in', 'range' => array_keys(BalanceArticlesSubcategories::MAP)],
            [
                ['amount'],
                'number',
                'numberPattern' => Yii::$app->params['numberPattern'],
                'max' => Yii::$app->params['maxCashSum'] * 100,
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
                'whenClient' => 'function(){}',
            ],
            [['amount'], 'compare', 'operator' => '>', 'compareValue' => 0, 'whenClient' => 'function(){}'],
            [['purchased_at'], 'safe'],
            [['purchased_at'], 'validatePurchasedAt'],
        ];
    }

    public function validatePurchasedAt($attribute)
    {
        $purchasedAt = Carbon::parse($this->$attribute);

        if ($purchasedAt->isFuture()) {
            $this->addError($attribute, 'Дата покупки не может быть больше текущей даты.');
        }
    }

    public function attributeLabels()
    {
        return [
            'type' => 'Категория',
            'name' => 'Наименование',
            'category' => 'Вид',
            'subcategory' => 'Подвид',
            'useful_life_in_month' => 'Срок полезного использования (мес.)',
            'count' => 'Количество',
            'purchased_at' => 'Дата приобретения',
            'amount' => 'Стоимость приобретения (руб.)',
            'description' => 'Описание',
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->amount = round(TextHelper::parseMoneyInput($this->amount) * 100);

        return parent::beforeValidate();
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $model = new BalanceArticle();
        $model->company_id = $this->user->currentEmployeeCompany->company_id;
        $model->type = $this->type;
        $model->status = BalanceArticle::STATUS_ON_BALANCE;
        $model->name = $this->name;
        $model->category = $this->category;
        $model->subcategory = $this->subcategory;
        $model->useful_life_in_month = $this->useful_life_in_month ?: BalanceArticlesSubcategories::USEFUL_LIFE_IN_MONTH_MAP[$model->subcategory];
        $model->count = $this->count;
        $model->purchased_at = $this->purchased_at;
        $model->amount = $this->amount;
        $model->description = $this->description;

        return $model->save();
    }
}