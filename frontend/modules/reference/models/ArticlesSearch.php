<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 24.11.2019
 * Time: 22:37
 */

namespace frontend\modules\reference\models;

use common\components\excel\Excel;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\Employee;
use common\models\ExpenseItemFlowOfFunds;
use common\models\IncomeItemFlowOfFunds;
use frontend\modules\reports\models\AbstractFinance;
use Yii;
use yii\base\Model;

class ArticlesSearch extends Model
{
    const TYPE_INCOME = 0;
    const TYPE_EXPENSE = 1;
    const TYPE_BY_ACTIVITY = 2;

    const BLOCK_FREE_CONTRACTORS = 'free_contractors';

    public $search;

    public $withContractors = true;

    private $type;

    /** @var Employee */
    private $user;

    private $hasItems;

    public function rules()
    {
        return [
            [['search'], 'string'],
        ];
    }

    public function __construct($type = self::TYPE_BY_ACTIVITY, $config = [])
    {
        parent::__construct($config);
        if (!in_array($type, [self::TYPE_INCOME, self::TYPE_EXPENSE, self::TYPE_BY_ACTIVITY])) {
            throw new \InvalidArgumentException("Unknown type: {$type}");
        }

        $this->type = (int)$type;
        $this->user = Yii::$app->user->identity;
    }

    public function search($params = [])
    {
        $this->load($params);
        if (in_array($this->type, [self::TYPE_INCOME, self::TYPE_EXPENSE])) {
            return $this->searchByType();
        }

        return $this->searchByActivity();
    }

    public function generateXls()
    {
        $data = [];
        if ($this->type === self::TYPE_BY_ACTIVITY) {
            $data = $this->searchByActivity();
        }

        $this->buildXls($data);
    }

    public function getHasItems()
    {
        return $this->hasItems;
    }

    private function buildXls($data)
    {
        $models = [];
        foreach ($data as $item) {
            $models[] = [
                'name' => $item['name'],
                'contractorsCount' => $item['contractorCount'],
                'isVisible' => '',
            ];
            foreach ($item['items'] as $flowItem) {
                $models[] = [
                    'name' => "    {$flowItem['name']}",
                    'contractorsCount' => $flowItem['contractorCount'],
                    'isVisible' => '',
                ];

                if (!isset($flowItem['items'])) continue;

                foreach ($flowItem['items'] as $basicItem) {
                    $isVisible = '';
                    if ($basicItem['canBeControlled'] && $basicItem['flowOfFundsItemId']) {
                        $isVisible = $basicItem['isVisibleFlowOfFundsItem'] ? 'Да' : 'Нет';
                    }

                    $models[] = [
                        'name' => "        {$basicItem['name']}",
                        'contractorsCount' => $basicItem['contractorCount'],
                        'isVisible' => $isVisible,
                    ];
                }
            }
        }

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $models,
            'title' => 'Статьи по видам деятельности',
            'rangeHeader' => range('A', 'C'),
            'columns' => [
                [
                    'attribute' => 'name',
                    'type' => \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                    'header' => 'Статьи',
                ],
                [
                    'attribute' => 'contractorsCount',
                    'type' => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER,
                    'header' => 'Контрагенты',
                ],
                [
                    'attribute' => 'isVisible',
                    'type' => \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                    'header' => 'Показывать',
                ],
            ],
            'format' => 'Excel5',
            'fileName' => 'Статьи по видам деятельности',
        ]);
    }

    private function searchByActivity()
    {
        $hasItems = false;
        $data = [];
        $subData = [];

        foreach (AbstractFinance::$typeItem as $typeID => $itemIDs) {
            $block = AbstractFinance::$blockByType[$typeID];
            $flowType = AbstractFinance::$flowTypeByActivityType[$typeID];
            if (!isset($data["block-{$block}"])) {
                $data["block-{$block}"] = [
                    'id' => $block,
                    'name' => AbstractFinance::$blocks[$block],
                    'contractorCount' => 0,
                ];
            }

            if (!isset($data["block-{$block}"]['items']["flow-type-{$flowType}-{$block}"])) {
                $data["block-{$block}"]['items']["flow-type-{$flowType}-{$block}"] = [
                    'id' => $flowType,
                    'flowType' => $flowType == CashBankFlows::FLOW_TYPE_INCOME ? self::TYPE_INCOME : self::TYPE_EXPENSE,
                    'name' => AbstractFinance::$types[$typeID],
                    'contractorCount' => 0,
                ];
            }

            if ($flowType === CashFlowsBase::FLOW_TYPE_INCOME) {
                $contractorType = Contractor::TYPE_CUSTOMER;
                $contractorItemAttribute = 'invoice_income_item_id';
                $flowOfFundsItemTableName = IncomeItemFlowOfFunds::tableName();
                $excludedItem = InvoiceIncomeItem::ITEM_OWN_FOUNDS;
                $flowOfFundItemName = 'income_item_id';
                $query = InvoiceIncomeItem::find()
                    ->alias('i')
                    ->leftJoin("{$flowOfFundsItemTableName} fi", 'fi.income_item_id = i.id');
            } else {
                $contractorType = Contractor::TYPE_SELLER;
                $contractorItemAttribute = 'invoice_expenditure_item_id';
                $flowOfFundsItemTableName = ExpenseItemFlowOfFunds::tableName();
                $excludedItem = InvoiceExpenditureItem::ITEM_OWN_FOUNDS;
                $flowOfFundItemName = 'expense_item_id';
                $query = InvoiceExpenditureItem::find()
                    ->alias('i')
                    ->leftJoin("{$flowOfFundsItemTableName} fi", 'fi.expense_item_id = i.id');
            }

            if (in_array($typeID, [AbstractFinance::INCOME_OPERATING_ACTIVITIES, AbstractFinance::WITHOUT_TYPE])) {
                if ($flowType == CashFlowsBase::FLOW_TYPE_INCOME) {
                    $itemIDs = InvoiceIncomeItem::find()
                        ->andWhere(['company_id' => null])
                        ->andWhere(['not', ['in', 'id', AbstractFinance::$typeItem[AbstractFinance::RECEIPT_FINANCING_TYPE_FIRST]]])
                        ->andWhere(['not', ['id' => $excludedItem]])
                        ->column();
                } else {
                    $itemIDs = InvoiceExpenditureItem::find()
                        ->andWhere(['company_id' => null])
                        ->andWhere(['not', ['in', 'id', AbstractFinance::$typeItem[AbstractFinance::RECEIPT_FINANCING_TYPE_SECOND]]])
                        ->andWhere(['not', ['id' => $excludedItem]])
                        ->column();
                }
            }

            $query->select([
                'i.id',
                'i.parent_id as parentId',
                'i.name as itemName',
                'i.can_be_controlled as canBeControlled',
                'i.company_id as companyId',
                'fi.id as flowOfFundsItemId',
                'fi.is_visible as isVisibleFlowOfFundsItem',
            ])
                ->joinWith('contractors c', false)
                ->andWhere([
                    'AND',
                    ['not', ['i.id' => $excludedItem]],
                    ['fi.company_id' => $this->user->company->id],
                    [
                        'OR',
                        ['i.company_id' => null],
                        ['i.company_id' => $this->user->company->id],
                    ],
                    [
                        'OR',
                        ['c.company_id' => null],
                        ['c.company_id' => $this->user->company->id],
                    ],
                    [
                        'OR',
                        ['fi.flow_of_funds_block' => $block],
                        ['and',
                            ['fi.flow_of_funds_block' => null],
                            ['in', "fi.{$flowOfFundItemName}", $itemIDs],
                        ],
                        $typeID == AbstractFinance::INCOME_OPERATING_ACTIVITIES || $typeID == AbstractFinance::WITHOUT_TYPE ?
                            ['and',
                                ['fi.flow_of_funds_block' => null],
                                ['not', ['i.company_id' => null]],
                            ] : [],
                    ],
                ]);

            $query->andFilterWhere([
                'OR',
                ['LIKE', 'i.name', $this->search],
                ['LIKE', 'c.name', $this->search],
            ]);

            $itemsData = [];
            $queryItems = $query->groupBy('i.id')->orderBy('i.name')->asArray()->all();
            if ($hasItems === false) {
                $hasItems = (bool)count($queryItems);
            }

            foreach ($queryItems as $item) {
                $contractors = $this->withContractors ? Contractor::getSorted()
                    ->byContractor($contractorType)
                    ->byDeleted()
                    ->byCompany($this->user->company->id)
                    ->byStatus(Contractor::ACTIVE)
                    ->andWhere([$contractorItemAttribute => $item['id']])
                    ->all() : [];

                $itemData = [
                    'id' => $item['id'],
                    'parentId' => $item['parentId'],
                    'flowType' => $flowType == CashBankFlows::FLOW_TYPE_INCOME ? self::TYPE_INCOME : self::TYPE_EXPENSE,
                    'name' => $item['itemName'],
                    'companyId' => $item['companyId'],
                    'canBeControlled' => (bool)$item['canBeControlled'],
                    'flowOfFundsItemId' => $item['flowOfFundsItemId'],
                    'isVisibleFlowOfFundsItem' => (bool)$item['isVisibleFlowOfFundsItem'],
                    'contractorCount' => count($contractors),
                    'contractors' => array_map(function (Contractor $contractor) {
                        return [
                            'id' => $contractor->id,
                            'name' => $contractor->getNameWithType(),
                        ];
                    }, $contractors),
                    'children' => []
                ];

                if ($item['parentId']) {
                    $inverseFlowType = ($flowType == CashBankFlows::FLOW_TYPE_INCOME) ? self::TYPE_INCOME : self::TYPE_EXPENSE;
                    $subData[$inverseFlowType]["item-{$item['parentId']}"]["item-{$item['id']}"] = $itemData;
                } else {
                    $itemsData["item-{$item['id']}"] = $itemData;
                }

                $data["block-{$block}"]['contractorCount'] += count($contractors);
                $data["block-{$block}"]['items']["flow-type-{$flowType}-{$block}"]['contractorCount'] += count($contractors);
            }

            foreach ($itemsData as $key => $item)
            {
                if (isset($item['items'])) {
                    foreach ($item['items'] as $k => $i) {
                        if (isset($subData[$i['flowType']][$k])) {
                            $itemsData[$key]['items'][$k]['children'] = $subData[$i['flowType']][$k];
                        }
                    }

                } elseif (isset($subData[$item['flowType']][$key])) {
                    $itemsData[$key]['children'] = $subData[$item['flowType']][$key];
                }
            }

            $data["block-{$block}"]['items']["flow-type-{$flowType}-{$block}"]['items'] = $itemsData;
        }

        if ($this->withContractors) {
            $data['block-' . self::BLOCK_FREE_CONTRACTORS] = [
                'id' => self::BLOCK_FREE_CONTRACTORS,
                'name' => 'Контрагенты без статьи',
                'contractorCount' => 0,
                'items' => [
                    'flow-type-' . CashFlowsBase::FLOW_TYPE_INCOME . '-' . self::BLOCK_FREE_CONTRACTORS => [
                        'id' => CashFlowsBase::FLOW_TYPE_INCOME,
                        'flowType' => self::TYPE_INCOME,
                        'name' => 'Приход',
                        'contractorCount' => 0,
                    ],
                    'flow-type-' . CashFlowsBase::FLOW_TYPE_EXPENSE . '-' . self::BLOCK_FREE_CONTRACTORS => [
                        'id' => CashFlowsBase::FLOW_TYPE_EXPENSE,
                        'flowType' => self::TYPE_EXPENSE,
                        'name' => 'Расход',
                        'contractorCount' => 0,
                    ],
                ],
            ];

            /** @var Contractor $contractor */
            foreach (Contractor::getSorted()
                         ->byDeleted()
                         ->byCompany($this->user->currentEmployeeCompany->company_id)
                         ->byStatus(Contractor::ACTIVE)
                         ->andWhere([
                             'OR',
                             [
                                 'AND',
                                 ['type' => Contractor::TYPE_CUSTOMER],
                                 ['invoice_income_item_id' => null],
                             ],
                             [
                                 'AND',
                                 ['type' => Contractor::TYPE_SELLER],
                                 ['invoice_expenditure_item_id' => null],
                             ],
                         ])
                         ->all() as $contractor) {
                $flowType = $contractor->type === Contractor::TYPE_CUSTOMER ? CashFlowsBase::FLOW_TYPE_INCOME : CashFlowsBase::FLOW_TYPE_EXPENSE;
                $data['block-' . self::BLOCK_FREE_CONTRACTORS]['items']["flow-type-{$flowType}-" . self::BLOCK_FREE_CONTRACTORS]['contractorCount'] += 1;
                $data['block-' . self::BLOCK_FREE_CONTRACTORS]['items']["flow-type-{$flowType}-" . self::BLOCK_FREE_CONTRACTORS]['contractors'][] = [
                    'id' => $contractor->id,
                    'name' => $contractor->getNameWithType(),
                ];
            }
        }

        $this->hasItems = $hasItems;

        return $data;
    }

    private function searchByType()
    {
        if ($this->type === self::TYPE_INCOME) {
            $enabledName = 'Статьи Прихода, которые ВКЛЮЧЕНЫ';
            $disabledName = 'Статьи Прихода, которые ОТКЛЮЧЕНЫ';
            $contractorType = Contractor::TYPE_CUSTOMER;
            $contractorItemField = 'invoice_income_item_id';
        } else {
            $enabledName = 'Статьи Расхода, которые ВКЛЮЧЕНЫ';
            $disabledName = 'Статьи Расхода, которые ОТКЛЮЧЕНЫ';
            $contractorType = Contractor::TYPE_SELLER;
            $contractorItemField = 'invoice_expenditure_item_id';
        }

        $visibleData = $this->searchItems(true);
        $notVisibleData = $this->searchItems(false);
        $data = [
            [
                'id' => 'visible-items',
                'name' => $enabledName,
                'contractorsCount' => $visibleData['contractorsCount'],
                'items' => $visibleData['data'],
            ],
            [
                'id' => 'not-visible-items',
                'name' => $disabledName,
                'contractorsCount' => $notVisibleData['contractorsCount'],
                'items' => $notVisibleData['data'],
            ],
            'block-' . self::BLOCK_FREE_CONTRACTORS => [
                'id' => self::BLOCK_FREE_CONTRACTORS,
                'flowType' => $this->type,
                'name' => 'Контрагенты без статьи',
                'contractorsCount' => 0,
                'contractors' => [],
            ],
        ];

        if ($this->withContractors) {
            /** @var Contractor $contractor */
            foreach (Contractor::getSorted()
                         ->byDeleted()
                         ->byCompany($this->user->currentEmployeeCompany->company_id)
                         ->byStatus(Contractor::ACTIVE)
                         ->andWhere([
                             'AND',
                             ['type' => $contractorType],
                             [$contractorItemField => null],
                         ])->all() as $contractor) {
                $data['block-' . self::BLOCK_FREE_CONTRACTORS]['contractorsCount'] += 1;
                $data['block-' . self::BLOCK_FREE_CONTRACTORS]['contractors'][] = [
                    'id' => $contractor->id,
                    'name' => $contractor->getNameWithType(),
                ];
            }
        }

        return $data;
    }

    private function searchItems($isVisible)
    {
        $data = [];
        $subData = [];
        $totalContractorsCount = 0;
        if ($this->type === self::TYPE_INCOME) {
            $contractorType = Contractor::TYPE_CUSTOMER;
            $contractorItemAttribute = 'invoice_income_item_id';
            $flowOfFundsItemTableName = IncomeItemFlowOfFunds::tableName();
            $query = InvoiceIncomeItem::find()
                ->alias('i')
                ->select([
                    'i.id',
                    'i.parent_id as parentId',
                    'i.name as itemName',
                    'i.can_be_controlled as canBeControlled',
                    'i.company_id as companyId',
                    'fi.id as flowOfFundsItemId',
                    'fi.is_visible as isVisibleFlowOfFundsItem',
                ])
                ->leftJoin("{$flowOfFundsItemTableName} fi", 'fi.income_item_id = i.id')
                ->orderBy('i.name');
        } else {
            $contractorType = Contractor::TYPE_SELLER;
            $contractorItemAttribute = 'invoice_expenditure_item_id';
            $flowOfFundsItemTableName = ExpenseItemFlowOfFunds::tableName();
            $query = InvoiceExpenditureItem::find()
                ->alias('i')
                ->select([
                    'i.id',
                    'i.parent_id as parentId',
                    'i.name as itemName',
                    'i.group_id',
                    'i.can_be_controlled as canBeControlled',
                    'i.company_id as companyId',
                    'ig.id as groupId',
                    'ig.name as groupName',
                    'fi.id as flowOfFundsItemId',
                    'fi.is_visible as isVisibleFlowOfFundsItem',
                ])
                ->leftJoin("{$flowOfFundsItemTableName} fi", 'fi.expense_item_id = i.id')
                ->joinWith('itemGroup ig')
                ->orderBy('ig.name, i.name');
        }

        $query->joinWith('contractors c')
            ->andWhere([
                'AND',
                ['fi.is_visible' => $isVisible],
                ['fi.company_id' => $this->user->currentEmployeeCompany->company_id],
                [
                    'OR',
                    ['i.company_id' => null],
                    ['i.company_id' => $this->user->currentEmployeeCompany->company_id],
                ],
            ]);

        $query->andFilterWhere([
            'OR',
            ['LIKE', 'i.name', $this->search],
            ['LIKE', 'c.name', $this->search],
        ]);

        $queryItems = $query->groupBy('i.id')->asArray()->all();
        $this->hasItems = (bool)count($queryItems);

        foreach ($queryItems as $item) {
            $contractors = Contractor::getSorted()
                ->byContractor($contractorType)
                ->byDeleted()
                ->byCompany($this->user->currentEmployeeCompany->company_id)
                ->byStatus(Contractor::ACTIVE)
                ->andWhere([$contractorItemAttribute => $item['id']])
                ->all();
            $itemData = [
                'id' => $item['id'],
                'parentId' => $item['parentId'],
                'flowType' => $this->type,
                'name' => $item['itemName'],
                'companyId' => $item['companyId'],
                'canBeControlled' => (bool)$item['canBeControlled'],
                'flowOfFundsItemId' => $item['flowOfFundsItemId'],
                'isVisibleFlowOfFundsItem' => (bool)$item['isVisibleFlowOfFundsItem'],
                'contractorCount' => count($contractors),
                'contractors' => array_map(function (Contractor $contractor) {
                    return [
                        'id' => $contractor->id,
                        'name' => $contractor->getNameWithType(),
                    ];
                }, $contractors),
                'children' => []
            ];
            $totalContractorsCount += count($contractors);
            if ($item['parentId']) {
                $subData["item-{$item['parentId']}"]["item-{$item['id']}"] = $itemData;
            } elseif (isset($item['groupId'])) {
                if (!isset($data["group-{$item['groupId']}"])) {
                    $data["group-{$item['groupId']}"] = [
                        'id' => $item['groupId'],
                        'name' => $item['groupName'],
                        'contractorCount' => count($contractors),
                        'items' => [
                            "item-{$item['id']}" => $itemData,
                        ],
                    ];
                } else {
                    $data["group-{$item['groupId']}"]['contractorCount'] += count($contractors);
                    $data["group-{$item['groupId']}"]['items']["item-{$item['id']}"] = $itemData;
                }
            } else {
                $data["item-{$item['id']}"] = $itemData;
            }
        }

        foreach ($data as $key => $item)
        {
            if (isset($item['items'])) {
                foreach ($item['items'] as $k => $i) {
                    if (isset($subData[$k])) {
                        $data[$key]['items'][$k]['children'] = $subData[$k];
                        $data[$key]['items'][$k]['contractorCount'] += array_sum(array_column($subData[$k], 'contractorCount'));
                    }
                }

            } elseif (isset($subData[$key])) {
                $data[$key]['children'] = $subData[$key];
                $data[$key]['contractorCount'] += array_sum(array_column($subData[$key], 'contractorCount'));
            }
        }

        array_multisort(ArrayHelper::getColumn($data, 'name'), SORT_ASC, $data);

        return [
            'data' => $data,
            'contractorsCount' => $totalContractorsCount,
        ];
    }
}
