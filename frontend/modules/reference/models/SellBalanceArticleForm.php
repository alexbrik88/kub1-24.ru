<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 07.12.2019
 * Time: 19:58
 */

namespace frontend\modules\reference\models;


use Carbon\Carbon;
use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\components\TextHelper;
use common\models\address\Country;
use common\models\balance\BalanceArticle;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFactory;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrdersReasonsTypes;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\document\Order;
use common\models\document\OrderHelper;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use common\models\product\Product;
use common\models\product\ProductUnit;
use common\models\TaxRate;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use Yii;
use yii\base\Model;

class SellBalanceArticleForm extends Model
{
    public $sold_at;

    public $amount;

    public $contractor_id;

    public $articlesIds;

    public $flow_type;

    public $cashbox_id;

    public $emoney_id;

    public $create_new;

    public $selected_flows;

    public $is_accounting = true;

    /** @var BalanceArticle[] */
    private $articles;

    private $articleType;

    private $cashboxList;

    private $emoneyList;

    /** @var Invoice */
    private $invoice;

    /**
     * @var Employee
     */
    private $user;

    public function behaviors()
    {
        return [
            [
                'class' => DatePickerFormatBehavior::class,
                'attributes' => [
                    'sold_at' => [
                        'message' => 'Дата оплаты указана неверно.',
                    ],
                ],
                'dateFormatParams' => [
                    'whenClient' => 'function(){}',
                ],
            ],
        ];
    }

    public function __construct(array $articleIds, Employee $user, $articleType, array $config = [])
    {
        $this->articlesIds = $articleIds;
        $this->user = $user;
        $this->articleType = $articleType;

        parent::__construct($config);
    }

    public function init()
    {
        parent::init();

        $this->cashbox_id = key($this->getCashboxList());
        $this->emoney_id = key($this->getEmoneyList());
        foreach ($this->getArticles() as $article) {
            $monthInUse = round((Carbon::now())->diffInDays(Carbon::parse($article->purchased_at)) / 30);
            $this->amount += $article->amount - ($article->amount / $article->useful_life_in_month * $monthInUse);
        }
    }

    public function rules()
    {
        return [
            [['sold_at', 'amount', 'contractor_id'], 'required', 'message' => 'Необходимо заполнить.'],
            [['sold_at', 'articlesIds'], 'safe'],
            [['contractor_id', 'flow_type'], 'integer'],
            [['flow_type'], 'in', 'range' => [
                CashFactory::TYPE_BANK,
                CashFactory::TYPE_ORDER,
                CashFactory::TYPE_EMONEY,
            ]],
            [
                ['cashbox_id'], 'required',
                'when' => function (self $model) {
                    return $model->flow_type == CashFactory::TYPE_ORDER;
                },
            ],
            [
                ['cashbox_id'], 'in',
                'range' => array_keys($this->getCashboxList()),
                'when' => function (self $model) {
                    return $model->flow_type == CashFactory::TYPE_ORDER;
                },
            ],
            [
                ['emoney_id'], 'required',
                'when' => function (self $model) {
                    return $model->flow_type == CashFactory::TYPE_EMONEY;
                },
            ],
            [
                ['emoney_id'], 'in',
                'range' => array_keys($this->getEmoneyList()),
                'when' => function (self $model) {
                    return $model->flow_type == CashFactory::TYPE_EMONEY;
                },
            ],
            [['create_new'], 'boolean', 'when' => function (self $model) {
                return $model->flow_type == CashFactory::TYPE_BANK;
            },],
            [
                ['create_new'], 'compare', 'compareValue' => '1',
                'when' => function (self $model) {
                    return empty($model->selected_flows);
                },
                'message' => 'Выберите операцию или создайте новую.',
            ],
            [['selected_flows'], 'validateSelectedFlows'],
            [['is_accounting'], 'boolean', 'when' => function (self $model) {
                return $model->flow_type != CashFactory::TYPE_BANK;
            },],
            [
                ['amount'],
                'number',
                'numberPattern' => Yii::$app->params['numberPattern'],
                'max' => Yii::$app->params['maxCashSum'] * 100,
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
                'whenClient' => 'function(){}',
            ],
            [['amount'], 'compare', 'operator' => '>', 'compareValue' => 0, 'whenClient' => 'function(){}'],
        ];
    }

    public function beforeValidate()
    {
        $this->amount = round(TextHelper::parseMoneyInput($this->amount) * 100);

        return parent::beforeValidate();
    }

    public function validateSelectedFlows($attribute)
    {
        $contractor = $this->getContractor();
        if ($this->hasErrors() || $contractor === null || empty($this->selected_flows)) {
            return;
        }

        $company = $this->user->currentEmployeeCompany->company;
        $range = null;
        switch ($this->flow_type) {
            case (CashFactory::TYPE_BANK):
                $range = $contractor->getNotLinkedBankFlows($company)->column();
                break;
            case (CashFactory::TYPE_ORDER):
                $range = $contractor->getNotLinkedOrderFlows($company)->column();
                break;
            case (CashFactory::TYPE_EMONEY):
                $range = $contractor->getNotLinkedEmoneyFlows($company)->column();
                break;
        }

        if (!empty(array_diff($this->$attribute, $range))) {
            $this->addError($attribute, 'Нельзя выбрать эту операцию.');
        }
    }

    public function attributeLabels()
    {
        return [
            'sold_at' => 'Дата оплаты',
            'amount' => 'Сумма',
            'contractor_id' => 'Покупатель',
            'create_new' => 'Создать новую операцию',
            'cashbox_id' => 'Через кассу',
            'emoney_id' => 'Через E-money',
            'is_accounting' => 'Учитывать в бухгалтерии',
        ];
    }

    public function formName()
    {
        return 'SellBalanceArticleForm';
    }

    public function getCashboxList()
    {
        if ($this->cashboxList === null) {
            $this->cashboxList = $this->user->getCashboxes()->select('name')->indexBy('id')->orderBy([
                'IF([[responsible_employee_id]] = :uid, 0, 1)' => SORT_ASC,
                'is_main' => SORT_DESC,
            ])->params([':uid' => $this->user->id])->column();

        }
        return $this->cashboxList;
    }

    public function getEmoneyList()
    {
        if ($this->emoneyList === null) {
            $this->emoneyList = $this->user->currentEmployeeCompany->company->getEmoneys()->select('name')->andWhere([
                'is_closed' => false,
            ])->orderBy([
                'is_closed' => SORT_ASC,
                'is_main' => SORT_DESC,
                'name' => SORT_ASC,
            ])->indexBy('id')->column();

        }
        return $this->emoneyList;
    }

    /**
     * @return BalanceArticle[]
     */
    public function getArticles()
    {
        if (empty($this->articles)) {
            $this->articles = array_map(function ($articleId) {
                return BalanceArticle::findOne(['id' => $articleId]);
            }, $this->articlesIds);
        }

        return $this->articles;
    }

    /**
     * @return Contractor|null
     */
    public function getContractor()
    {
        return Contractor::findOne(['id' => $this->contractor_id]);
    }

    /**
     * @return Employee
     */
    public function getUser()
    {
        return $this->user;
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        foreach ($this->getFlows() as $flow) {
            if ($flow->isNewRecord) {
                if (!LogHelper::save($flow, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE)) {
                    $transaction->rollBack();
                    return false;
                }
            }

            $invoice = $this->createInvoice();
            if ($invoice === null) {
                $transaction->rollBack();
                return false;
            }

            if (!$flow->linkInvoice($invoice, $this->amount)) {
                $transaction->rollBack();
                return false;
            }
        }

        foreach ($this->getArticles() as $article) {
            $article->status = BalanceArticle::STATUS_SOLD;
            $article->sold_at = $this->sold_at;
            if (!$article->save()) {
                $transaction->rollBack();
                return false;
            }
        }

        $transaction->commit();

        return true;
    }

    /**
     * @return CashBankFlows[]|CashOrderFlows[]|CashEmoneyFlows[]
     */
    private function getFlows()
    {
        if ($this->create_new == false) {
            switch ($this->flow_type) {
                case CashFactory::TYPE_BANK:
                    return $this->getContractor()
                        ->getNotLinkedBankFlows($this->user->currentEmployeeCompany->company)
                        ->andWhere(['flow.id' => $this->selected_flows])
                        ->all();
                case CashFactory::TYPE_ORDER:
                    return $this->getContractor()
                        ->getNotLinkedOrderFlows($this->user->currentEmployeeCompany->company)
                        ->andWhere(['flow.id' => $this->selected_flows])
                        ->all();
                case CashFactory::TYPE_EMONEY:
                    return $this->getContractor()
                        ->getNotLinkedEmoneyFlows($this->user->currentEmployeeCompany->company)
                        ->andWhere(['flow.id' => $this->selected_flows])
                        ->all();
            }

            return [];
        }

        return [$this->createFlow()];
    }

    private function createFlow()
    {
        /* @var CashFlowsBase $flowClass */
        $flowClass = CashFactory::getCashClass($this->flow_type);
        /* @var CashFlowsBase $flowModel */
        $flowModel = new $flowClass();
        $flowModel->company_id = $this->user->currentEmployeeCompany->company_id;
        $flowModel->contractor_id = $this->contractor_id;
        $flowModel->date = Carbon::parse($this->sold_at)->format(DateHelper::FORMAT_USER_DATE);
        $flowModel->description = $this->articleType === BalanceArticle::TYPE_FIXED_ASSERTS
            ? 'Продажа осовных средств'
            : 'Продажа нематериальных активов';
        $flowModel->flow_type = CashFlowsBase::FLOW_TYPE_INCOME;
        $flowModel->income_item_id = InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER;
        if ($flowModel instanceof CashBankFlows) {
            $flowModel->rs = $this->user->currentEmployeeCompany->company->mainAccountant->rs;
            $flowModel->bank_name = $this->user->currentEmployeeCompany->company->mainAccountant->bank_name;
        }

        $flowModel->amount = $this->amount / 100; // cash flow takes float on input.
        if ($flowModel->hasAttribute('is_accounting')) {
            $flowModel->is_accounting = $this->is_accounting;
        }

        if ($flowModel instanceof CashOrderFlows) {
            /* @var CashOrderFlows $flowModel */
            $flowModel->author_id = $this->user->id;
            $flowModel->cashbox_id = $this->cashbox_id;
            $flowModel->number = CashOrderFlows::getNextNumber($flowModel->flow_type, $flowModel, $this->user->currentEmployeeCompany->company_id);
            $flowModel->reason_id = CashOrdersReasonsTypes::VALUE_PROCEEDS;
        }

        if ($flowModel instanceof CashEmoneyFlows) {
            $flowModel->emoney_id = $this->emoney_id;
        }

        return $flowModel;
    }

    /**
     * @return Invoice|null
     */
    private function createInvoice()
    {
        if ($this->invoice === null) {
            /** @var Product[] $products */
            $products = [];
            foreach ($this->getArticles() as $article) {
                $product = $this->createProduct($article);
                if ($product === null) {
                    return null;
                }

                $products[] = $product;
            }

            $invoice = new Invoice();
            $invoice->type = Documents::IO_TYPE_OUT;
            $invoice->invoice_status_id = InvoiceStatus::STATUS_CREATED;
            $invoice->invoice_status_author_id = $this->user->id;
            $invoice->document_author_id = $this->user->id;
            $invoice->document_date = Carbon::parse($this->sold_at)->format(DateHelper::FORMAT_USER_DATE);
            $invoice->document_number = (string)Invoice::getNextDocumentNumber(
                $this->user->currentEmployeeCompany->company,
                Documents::IO_TYPE_OUT,
                null,
                $this->sold_at
            );
            $invoice->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime('+10 days', strtotime($invoice->document_date)));
            $invoice->invoice_expenditure_item_id = InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT;
            $invoice->isAutoinvoice = 0;
            $invoice->production_type = Product::PRODUCTION_TYPE_GOODS;

            $company = $this->user->currentEmployeeCompany->company;
            $invoice->company_id = $company->id;
            $invoice->company_inn = $company->inn;
            $invoice->company_kpp = $company->kpp;
            $invoice->company_egrip = $company->egrip;
            $invoice->company_okpo = $company->okpo;
            $invoice->company_name_full = $company->getTitle();
            $invoice->company_name_short = $company->getTitle(true, true);
            $invoice->company_address_legal_full = $company->getAddressLegalFull();
            $invoice->company_phone = $company->phone;
            $invoice->company_chief_post_name = $company->chief_post_name;
            $invoice->company_chief_lastname = $company->chief_lastname;
            $invoice->company_chief_firstname_initials = $company->chief_firstname_initials;
            $invoice->company_chief_patronymic_initials = $company->chief_patronymic_initials;
            $invoice->company_chief_accountant_lastname = $company->chief_accountant_lastname;
            $invoice->company_chief_accountant_firstname_initials = $company->chief_accountant_firstname_initials;
            $invoice->company_chief_accountant_patronymic_initials = $company->chief_accountant_patronymic_initials;
            $invoice->company_print_filename = $company->print_link;
            $invoice->company_chief_signature_filename = $company->chief_signature_link;

            $contractor = $this->getContractor();
            $invoice->contractor_id = $contractor->id;
            $invoice->contractor_name_short = $contractor->getTitle(true);
            $invoice->contractor_name_full = $contractor->getTitle(false);
            $invoice->contractor_address_legal_full = ($contractor->face_type == Contractor::TYPE_LEGAL_PERSON ? $contractor->legal_address : $contractor->physical_address);
            $invoice->contractor_bank_name = $contractor->bank_name;
            $invoice->contractor_bik = $contractor->BIC;
            $invoice->contractor_inn = $contractor->ITN;
            $invoice->contractor_kpp = $contractor->PPC;
            $invoice->contractor_ks = $contractor->corresp_account;
            $invoice->contractor_rs = $contractor->current_account;

            $invoice->total_order_count = count($products);
            $invoice->nds_view_type_id = Invoice::NDS_VIEW_IN;
            $invoice->price_precision = 2;
            /** @var Order[] $orderArray */
            $orderArray = [];
            foreach ($products as $key => $product) {
                $order = OrderHelper::createOrderByProduct(
                    $product,
                    $invoice,
                    1,
                    $product->price_for_sell_with_nds / 100
                );
                $order->product_title = $product->title;
                $order->number = ++$key;
                $orderArray[] = $order;
            }

            $invoice->populateRelation('orders', $orderArray);
            if (!$invoice->save()) {
                return null;
            }

            foreach ($orderArray as $order) {
                $order->invoice_id = $invoice->id;
                if (!$order->save()) {
                    return null;
                }
            }

            $this->invoice = $invoice;
        }

        return $this->invoice;
    }

    /**
     * @param BalanceArticle $article
     * @return Product|null
     */
    private function createProduct(BalanceArticle $article)
    {
        $monthInUse = round((Carbon::now())->diffInDays(Carbon::parse($article->purchased_at)) / 30);
        $priceForSell = $article->amount - ($article->amount / $article->useful_life_in_month * $monthInUse);
        $product = new Product();
        $product->company_id = $this->user->currentEmployeeCompany->company_id;
        $product->production_type = Product::PRODUCTION_TYPE_SERVICE;
        $product->title = $article->name;
        $product->price_for_buy_with_nds = $article->amount / 100;
        $product->price_for_sell_with_nds = $priceForSell / 100;
        $product->price_for_sell_nds_id = TaxRate::RATE_WITHOUT;
        $product->price_for_buy_nds_id = TaxRate::RATE_WITHOUT;
        $product->country_origin_id = Country::COUNTRY_WITHOUT;
        $product->product_unit_id = ProductUnit::UNIT_COUNT;
        if (!$product->save()) {
            return null;
        }

        return $product;
    }
}