<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.07.2020
 * Time: 0:41
 */

namespace frontend\modules\reference\models;

use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\models\Contractor;
use common\models\employee\Employee;
use Yii;
use yii\base\Model;

class ManyChangeItemsForm extends Model
{
    public $incomeItemIdManyItem;

    public $expenditureItemIdManyItem;

    public $contractors;

    public $changeOldOperations = false;

    /** @var Employee */
    private $user;

    public function __construct(Employee $user, array $config = [])
    {
        parent::__construct($config);
        $this->user = $user;
    }

    public function rules(): array
    {
        return [
            [['incomeItemIdManyItem', 'expenditureItemIdManyItem'], 'integer'],
            [['contractors'], 'safe'],
            [['changeOldOperations'], 'boolean'],
        ];
    }

    public function save()
    {
        $transaction = Yii::$app->db->beginTransaction();
        $contractors = [];
        foreach ($this->contractors as $contractorId) {
            $contractor = $this->findContractor($contractorId);
            if ($contractor !== null) {
                $contractors[$contractor->type][] = (string)$contractor->id;
                $attribute = $contractor->type == Contractor::TYPE_CUSTOMER ? 'invoice_income_item_id' : 'invoice_expenditure_item_id';
                $value = $contractor->type == Contractor::TYPE_CUSTOMER ? $this->incomeItemIdManyItem : $this->expenditureItemIdManyItem;
                $contractor->$attribute = $value;
                if (!$contractor->save(true, [$attribute])) {
                    $transaction->rollBack();
                    return false;
                }
            }
        }

        if ($this->changeOldOperations) {
            foreach ($contractors as $type => $contractorsList) {
                if ($type == Contractor::TYPE_CUSTOMER) {
                    $operationFlowType = CashBankFlows::FLOW_TYPE_INCOME;
                    $operationField = 'income_item_id';
                    $itemId = $this->incomeItemIdManyItem;
                } else {
                    $operationFlowType = CashBankFlows::FLOW_TYPE_EXPENSE;
                    $operationField = 'expenditure_item_id';
                    $itemId = $this->expenditureItemIdManyItem;
                }

                /** @var CashBankFlows|CashOrderFlows|CashEmoneyFlows $cashFlowClass */
                foreach ([CashBankFlows::class, CashOrderFlows::class, CashEmoneyFlows::class] as $cashFlowClass) {
                    $cashFlowClass::updateAll([$operationField => $itemId], [
                        'AND',
                        ['flow_type' => $operationFlowType],
                        ['company_id' => $this->user->currentEmployeeCompany->company_id],
                        ['IN', 'contractor_id', $contractorsList],
                        [
                            'OR',
                            ['NOT', [$operationField => (int)$itemId]],
                            [$operationField => null],
                        ],
                    ]);
                }
            }
        }

        $transaction->commit();

        return true;
    }

    private function findContractor($id): ?Contractor
    {
        return Contractor::find()
            ->andWhere([
                'AND',
                ['id' => $id],
                ['company_id' => $this->user->currentEmployeeCompany->company_id],
            ])->one();
    }
}