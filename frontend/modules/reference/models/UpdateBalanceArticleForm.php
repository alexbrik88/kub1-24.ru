<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 05.12.2019
 * Time: 23:17
 */

namespace frontend\modules\reference\models;

use common\models\balance\BalanceArticle;
use common\models\employee\Employee;

class UpdateBalanceArticleForm extends BalanceArticleForm
{
    /**
     * @var BalanceArticle
     */
    private $model;

    public function __construct(BalanceArticle $model, Employee $user, array $config = [])
    {
        $this->model = $model;
        $this->name = $model->name;
        $this->category = $model->category;
        $this->subcategory = $model->subcategory;
        $this->useful_life_in_month = $model->useful_life_in_month;
        $this->count = $model->count;
        $this->purchased_at = $model->purchased_at;
        $this->amount = $model->amount;
        $this->description = $model->description;
        parent::__construct($user, $model->type, null, $config);
    }

    /**
     * @return BalanceArticle
     */
    public function getModel()
    {
        return $this->model;
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $model = $this->model;
        $model->name = $this->name;
        $model->category = $this->category;
        $model->subcategory = $this->subcategory;
        $model->useful_life_in_month = $this->useful_life_in_month ?: BalanceArticlesSubcategories::USEFUL_LIFE_IN_MONTH_MAP[$model->subcategory];
        $model->count = $this->count;
        $model->purchased_at = $this->purchased_at;
        $model->amount = $this->amount;
        $model->description = $this->description;

        return $model->save();
    }
}