<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 01.12.2019
 * Time: 18:16
 */

namespace frontend\modules\reference\models;

use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\Employee;
use common\models\ExpenseItemFlowOfFunds;
use common\models\IncomeItemFlowOfFunds;
use frontend\modules\reports\models\AbstractFinance;
use InvalidArgumentException;
use Yii;
use yii\base\Model;

class UpdateArticleForm extends Model
{
    public $name;

    public $activityType;

    public $sellers = [];

    public $customers = [];

    public $changeOldOperations = false;

    /**
     * @var InvoiceIncomeItem|InvoiceExpenditureItem
     */
    public $model;

    /**
     * @var Employee
     */
    private $user;

    /**
     * @var IncomeItemFlowOfFunds|ExpenseItemFlowOfFunds
     */
    private $flowOfFundsItem;

    private $type;

    public function __construct($model, Employee $user, array $config = [])
    {
        if (!$model instanceof InvoiceIncomeItem && !$model instanceof InvoiceExpenditureItem) {
            throw new InvalidArgumentException('Invalid model param');
        }

        $this->user = $user;
        $this->model = $model;
        parent::__construct($config);
    }

    public function init()
    {
        parent::init();
        $this->name = $this->model->name;
        if ($this->model instanceof InvoiceIncomeItem) {
            $this->type = ArticleForm::TYPE_INCOME;
            /** @var IncomeItemFlowOfFunds $flowOfFundsItem */
            $this->flowOfFundsItem = $this->model->getIncomeItemsFlowOfFunds()
                ->andWhere(['company_id' => $this->user->currentEmployeeCompany->company_id])
                ->one();
            $this->activityType = $this->flowOfFundsItem->flow_of_funds_block;
            if ($this->activityType === null) {
                if (in_array($this->model->id, AbstractFinance::$typeItem[AbstractFinance::RECEIPT_FINANCING_TYPE_FIRST])) {
                    $this->activityType = AbstractFinance::FINANCIAL_OPERATIONS_BLOCK;
                } else {
                    $this->activityType = AbstractFinance::OPERATING_ACTIVITIES_BLOCK;
                }
            }
        } else {
            $this->type = ArticleForm::TYPE_EXPENSE;
            /** @var ExpenseItemFlowOfFunds $flowOfFundsItem */
            $this->flowOfFundsItem = $this->model->getExpenseItemsFlowOfFunds()
                ->andWhere(['company_id' => $this->user->currentEmployeeCompany->company_id])
                ->one();
            $this->activityType = $this->flowOfFundsItem->flow_of_funds_block;
            if ($this->activityType === null) {
                if (in_array($this->user->id, AbstractFinance::$typeItem[AbstractFinance::RECEIPT_FINANCING_TYPE_SECOND])) {
                    $this->activityType = AbstractFinance::FINANCIAL_OPERATIONS_BLOCK;
                } else {
                    $this->activityType = AbstractFinance::OPERATING_ACTIVITIES_BLOCK;
                }
            }
        }
    }

    public function rules()
    {
        return [
            [['activityType'], 'required'],
            [['activityType'], 'in', 'range' => array_keys(AbstractFinance::$blocks)],
            [['name'], 'string'],
            [['activityType'], 'integer'],
            [['changeOldOperations'], 'boolean'],
            [['name'], 'validateName'],
            [['sellers', 'customers'], 'safe'],
        ];
    }

    public function validateName($attribute)
    {
        if ($this->model::find()
            ->andWhere(['name' => $this->$attribute])
            ->andWhere(['not', ['id' => $this->model->id]])
            ->andWhere([
                'OR',
                ['company_id' => null],
                ['company_id' => $this->user->currentEmployeeCompany->company_id],
            ])->exists()) {
            $this->addError($attribute, 'Такое название статьи уже существует.');
        }
    }

    public function attributeLabels()
    {
        return [
            'type' => 'Тип статьи',
            'name' => 'Название',
            'activityType' => 'Вид деятельности',
        ];
    }

    public function getType()
    {
        return $this->type;
    }

    public function getItem()
    {
        return $this->model;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        $model = $this->model;
        if ($model->company_id !== null && $this->name !== null) {
            $model->name = $this->name;
            if (!$model->save()) {
                $transaction->rollBack();
                return false;
            }
        }

        $flowOfFundsItem = $this->flowOfFundsItem;
        if ($this->activityType !== null) {
            $flowOfFundsItem->flow_of_funds_block = $this->activityType;
        }

        if (!$flowOfFundsItem->save()) {
            $transaction->rollBack();
            return false;
        }

        if ($model->children) {
            foreach ($model->children as $child) {
                if ($fofChild = $child->getFlowOfFundsItemByCompany($this->user->currentEmployeeCompany->company_id)->one()) {
                    $fofChild->flow_of_funds_block = $this->activityType;
                    if (!$fofChild->save()) {
                        $transaction->rollBack();
                        return false;
                    }
                }
            }
        }

        if ($this->type === ArticleForm::TYPE_INCOME) {
            $newContractorsItems = empty($this->customers) ? [] : $this->customers;
            $contractorItemAttribute = 'invoice_income_item_id';
            $operationFlowType = CashBankFlows::FLOW_TYPE_INCOME;
            $operationField = 'income_item_id';
        } else {
            $newContractorsItems = empty($this->sellers) ? [] : $this->sellers;
            $contractorItemAttribute = 'invoice_expenditure_item_id';
            $operationFlowType = CashBankFlows::FLOW_TYPE_EXPENSE;
            $operationField = 'expenditure_item_id';
        }

        $oldContractorsItems = $this->getOldContractors();
        $deleteContractorsItems = array_diff($oldContractorsItems, $newContractorsItems);
        $newFormattedContractorsItems = array_diff($newContractorsItems, $oldContractorsItems);
        foreach ($deleteContractorsItems as $contractorId) {
            $contractor = Contractor::findOne($contractorId);
            $contractor->$contractorItemAttribute = null;
            if (!$contractor->save(true, [$contractorItemAttribute])) {
                $transaction->rollBack();
                return false;
            }
        }

        foreach ($newFormattedContractorsItems as $contractorId) {
            $contractor = Contractor::find()
                ->andWhere(['company_id' => $this->user->currentEmployeeCompany->company_id])
                ->andWhere(['id' => $contractorId])
                ->one();
            if ($contractor !== null) {
                $contractor->$contractorItemAttribute = $this->model->id;
                if (!$contractor->save(true, [$contractorItemAttribute])) {
                    $transaction->rollBack();
                    return false;
                }
            }
        }

        if ($this->changeOldOperations) {
            /** @var CashBankFlows|CashOrderFlows|CashEmoneyFlows $cashFlowClass */
            foreach ([CashBankFlows::class, CashOrderFlows::class, CashEmoneyFlows::class] as $cashFlowClass) {
                $cashFlowClass::updateAll([$operationField => $model->id], [
                    'AND',
                    ['flow_type' => $operationFlowType],
                    ['company_id' => $this->user->currentEmployeeCompany->company_id],
                    ['IN', 'contractor_id', $newContractorsItems],
                    [
                        'OR',
                        ['NOT', [$operationField => (int)$model->id]],
                        [$operationField => null],
                    ],
                ]);
            }
        }

        $transaction->commit();

        return true;
    }

    public function getOldContractors()
    {
        $contractors = $this->model->getContractors()
            ->select(['id'])
            ->andWhere(['type' => $this->model instanceof InvoiceIncomeItem ? Contractor::TYPE_CUSTOMER : Contractor::TYPE_SELLER])
            ->andWhere(['company_id' => $this->user->currentEmployeeCompany->company_id])
            ->andWhere(['is_deleted' => false])
            ->andWhere(['status' => Contractor::ACTIVE])
            ->column();

        return array_map('intval', $contractors);
    }
}