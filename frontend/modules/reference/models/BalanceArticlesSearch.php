<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 05.12.2019
 * Time: 1:01
 */

namespace frontend\modules\reference\models;

use common\models\balance\BalanceArticle;
use common\models\employee\Employee;
use frontend\components\StatisticPeriod;
use InvalidArgumentException;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

class BalanceArticlesSearch extends Model
{
    public $category;

    public $subcategory;

    public $status;

    private $type;

    /** @var Employee */
    private $user;

    /** @var ActiveQuery */
    private $query;

    public function __construct($type, $config = [])
    {
        parent::__construct($config);
        if (!in_array($type, [BalanceArticle::TYPE_FIXED_ASSERTS, BalanceArticle::TYPE_INTANGIBLE_ASSETS])) {
            throw new InvalidArgumentException("Unknown type: {$type}");
        }

        $this->type = (int)$type;
        $this->user = Yii::$app->user->identity;
    }

    public function rules()
    {
        return [
            [['category'], 'in', 'range' => array_keys(BalanceArticlesCategories::MAP)],
            [['status'], 'in', 'range' => array_keys(BalanceArticle::STATUS_MAP)],
            [['subcategory'], 'in', 'range' => array_keys(BalanceArticlesSubcategories::MAP)],
        ];
    }

    public function search($params = [])
    {
        $period = StatisticPeriod::getSessionPeriod();
        $this->query = BalanceArticle::find()
            ->alias('ba')
            ->select([
                'ba.*',
                // approximate calculation (required for sorting)
                'ROUND(DATEDIFF(NOW(), ba.purchased_at) / 30) as month_in_use',
                '(ba.useful_life_in_month - ROUND(DATEDIFF(NOW(), ba.purchased_at) / 30)) as month_left',
                '(ba.amount / ba.useful_life_in_month * ROUND(DATEDIFF(NOW(), ba.purchased_at) / 30)) as depreciation_for_today',
                '(ba.amount - (ba.amount / ba.useful_life_in_month * ROUND(DATEDIFF(NOW(), ba.purchased_at) / 30))) as cost_for_today',
                'ADDDATE(ba.purchased_at, ba.useful_life_in_month * 30) as write_off_date',
            ])
            ->andWhere(['type' => $this->type])
            ->andWhere(['company_id' => $this->user->currentEmployeeCompany->company_id])
            ->andWhere([
                'OR',
                [
                    'AND',
                    ['<=', 'purchased_at', $period['to']],
                    ['status' => BalanceArticle::STATUS_ON_BALANCE],
                ],
                [
                    'AND',
                    ['<', 'purchased_at', $period['from']],
                    [
                        'OR',
                        [
                            'AND',
                            ['status' => BalanceArticle::STATUS_SOLD],
                            ['between', 'sold_at', $period['from'], $period['to']],
                        ],
                        [
                            'AND',
                            ['status' => BalanceArticle::STATUS_WRITTEN_OFF],
                            ['between', 'written_off_at', $period['from'], $period['to']],
                        ],
                    ],
                ],
                [
                    'AND',
                    ['between', 'purchased_at', $period['from'], $period['to']],
                    [
                        'OR',
                        [
                            'AND',
                            ['status' => BalanceArticle::STATUS_SOLD],
                            ['>=', 'sold_at', $period['from']],
                        ],
                        [
                            'AND',
                            ['status' => BalanceArticle::STATUS_WRITTEN_OFF],
                            ['>=', 'written_off_at', $period['from']],
                        ],
                    ],
                ],
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $this->query,
            'sort' => [
                'attributes' => [
                    'purchased_at',
                    'name',
                    'count',
                    'amount',
                    'depreciation_for_today',
                    'cost_for_today',
                    'useful_life_in_month',
                    'month_in_use',
                    'month_left',
                    'write_off_date',
                ],
            ],
        ]);

        $this->load($params);

        $this->query->andFilterWhere(['category' => $this->category]);

        $this->query->andFilterWhere(['subcategory' => $this->subcategory]);

        $this->query->andFilterWhere(['status' => $this->status]);

        return $dataProvider;
    }

    public function getCategoryFilter()
    {
        $filter = [null => 'Все'];
        foreach ((clone $this->query)->select(['category'])->groupBy('category')->column() as $category) {
            $filter[$category] = BalanceArticlesCategories::MAP[$category];
        }

        return $filter;
    }

    public function getSubCategoryFilter()
    {
        $filter = [null => 'Все'];
        foreach ((clone $this->query)->select(['subcategory'])->groupBy('subcategory')->column() as $subcategory) {
            $filter[$subcategory] = BalanceArticlesSubcategories::MAP[$subcategory];
        }

        return $filter;
    }

    public function getStatusFilter()
    {
        $filter = [null => 'Все'];
        foreach ((clone $this->query)->select(['status'])->groupBy('status')->column() as $status) {
            $filter[$status] = BalanceArticle::STATUS_MAP[$status];
        }

        return $filter;
    }
}