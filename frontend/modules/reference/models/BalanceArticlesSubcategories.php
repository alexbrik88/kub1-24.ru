<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.12.2019
 * Time: 23:17
 */

namespace frontend\modules\reference\models;

final class BalanceArticlesSubcategories
{
    const BUILDING = 0;
    const OFFICE = 1;
    const WAREHOUSE = 2;
    const SHOP = 3;
    const SHOPPING_PAVILION = 4;
    const COMMERCIAL_PREMISES_IN_SHIPPING_CENTER = 5;
    const PRODUCTION_ROOM = 6;
    const LAND_PLOT = 7;
    const MANUFACTORY = 8;
    const HANGAR = 9;
    const SHIP = 10;
    const CAR = 11;
    const TRUCK = 12;
    const TRAILER = 13;
    const SPECIAL_MACHINERY = 14;
    const TRACTOR = 15;
    const TRUCK_CRANE = 16;
    const AGRICULTURAL_MACHINERY = 17;
    const LOADER = 18;
    const SHOWCASE = 19;
    const RACK = 20;
    const TRADE_SOFTWARE = 21;
    const FREEZER_EQUIPMENT = 22;
    const PRINTING_EQUIPMENT = 23;
    const LIFTING_EQUIPMENT = 24;
    const SCAFFOLDING_AND_TIMBERING = 25;
    const METALWORKING_MACHINES = 26;
    const WOODWORKING_MACHINES = 27;
    const UMPS_AND_COMPRESSORS = 28;
    const MEANS_OF_COMMUNICATIONS = 29;
    const OTHER_MECHANISMS = 30;
    const COMPUTER = 31;
    const SERVERS = 32;
    const NETWORK_HARDWARE = 33;
    const OFFICE_EQUIPMENT = 34;
    const VIDEO_MONITORING = 35;
    const OFFICE_FURNITURE = 36;
    const MEDICAL_FURNITURE = 37;
    const FURNITURE_FOR_ENTERPRISES = 38;
    const ONE_C_ACCOUNTING = 39;
    const CONSULTANT_PLUS = 40;
    const OFFICE_PROGRAM = 41;
    const LICENSED_SOFTWARE = 42;
    const SPECIAL_PROGRAM = 43;
    const ALCOHOL_LICENSE = 44;
    const TRANSPORTATION_LICENSE = 45;
    const FSB_LICENSE = 46;
    const EDUCATION_LICENSE = 47;
    const MEDICAL_LICENSE = 48;
    const INVENTION_PATENT = 49;
    const SOFTWARE_PATENT = 50;
    const UTILITY_MODEL_PATENT = 51;
    const INDUSTRIAL_DESIGN_PATENT = 52;
    const TRADEMARK = 53;

    const MAP = [
        self::BUILDING => 'Здание',
        self::OFFICE => 'Офис',
        self::WAREHOUSE => 'Склад',
        self::SHOP => 'Магазин',
        self::SHOPPING_PAVILION => 'Торговый павильон',
        self::COMMERCIAL_PREMISES_IN_SHIPPING_CENTER => 'Торговое помещение в ТЦ',
        self::PRODUCTION_ROOM => 'Производственное помещение',
        self::LAND_PLOT => 'Земельный участок',
        self::MANUFACTORY => 'Цех',
        self::HANGAR => 'Ангар',
        self::SHIP => 'Судно (корабль)',
        self::CAR => 'Автомобиль',
        self::TRUCK => 'Грузовик',
        self::TRAILER => 'Прицеп',
        self::SPECIAL_MACHINERY => 'Спецтехника',
        self::TRACTOR => 'Трактор',
        self::TRUCK_CRANE => 'Автокран',
        self::AGRICULTURAL_MACHINERY => 'Сельхоз. техника',
        self::LOADER => 'Погрузчик',
        self::SHOWCASE => 'Витрины',
        self::RACK => 'Стеллажи',
        self::TRADE_SOFTWARE => 'Торговое оборудование',
        self::FREEZER_EQUIPMENT => 'Морозильное оборудование',
        self::PRINTING_EQUIPMENT => 'Типографическое оборудование',
        self::LIFTING_EQUIPMENT => 'Подъемное оборудование',
        self::SCAFFOLDING_AND_TIMBERING => 'Леса и опалубка',
        self::METALWORKING_MACHINES => 'Станки металлообрабатывающие',
        self::WOODWORKING_MACHINES => 'Станки деревообрабатывающие',
        self::UMPS_AND_COMPRESSORS => 'Насосы и компрессоры',
        self::MEANS_OF_COMMUNICATIONS => 'Средства связи',
        self::OTHER_MECHANISMS => 'Прочие механизмы',
        self::COMPUTER => 'Компьютеры',
        self::SERVERS => 'Серверы',
        self::NETWORK_HARDWARE => 'Сетевое оборудование',
        self::OFFICE_EQUIPMENT => 'Офисное оборудование',
        self::VIDEO_MONITORING => 'Видеонаблюдение',
        self::OFFICE_FURNITURE => 'Мебель офисная',
        self::MEDICAL_FURNITURE => 'Мебель медицинская',
        self::FURNITURE_FOR_ENTERPRISES => 'Мебель для предприятий',
        self::ONE_C_ACCOUNTING => '1С Бухгалтерия',
        self::CONSULTANT_PLUS => 'Консультант плюс',
        self::OFFICE_PROGRAM => 'Офисные программы',
        self::LICENSED_SOFTWARE => 'Лицензионное ПО',
        self::SPECIAL_PROGRAM => 'Специальные программы',
        self::ALCOHOL_LICENSE => 'Лицензия на продажу алкоголя',
        self::TRANSPORTATION_LICENSE => 'Лицензия на перевозку',
        self::FSB_LICENSE => 'Лицензия ФСБ',
        self::EDUCATION_LICENSE => 'Лицензия на образование',
        self::MEDICAL_LICENSE => 'Лицензия на медицинскую деятельность',
        self::INVENTION_PATENT => 'Патент на изобретение',
        self::SOFTWARE_PATENT => 'Патент на ПО',
        self::UTILITY_MODEL_PATENT => 'Патент на полезную модель',
        self::INDUSTRIAL_DESIGN_PATENT => 'Патент на промышленный образец',
        self::TRADEMARK => 'Торговый знак',
    ];

    const MAP_BY_CATEGORIES = [
        BalanceArticlesCategories::REAL_ESTATE => [
            self::BUILDING => self::MAP[self::BUILDING],
            self::OFFICE => self::MAP[self::OFFICE],
            self::WAREHOUSE => self::MAP[self::WAREHOUSE],
            self::SHOP => self::MAP[self::SHOP],
            self::SHOPPING_PAVILION => self::MAP[self::SHOPPING_PAVILION],
            self::COMMERCIAL_PREMISES_IN_SHIPPING_CENTER => self::MAP[self::COMMERCIAL_PREMISES_IN_SHIPPING_CENTER],
            self::PRODUCTION_ROOM => self::MAP[self::PRODUCTION_ROOM],
            self::LAND_PLOT => self::MAP[self::LAND_PLOT],
            self::MANUFACTORY => self::MAP[self::MANUFACTORY],
            self::HANGAR => self::MAP[self::HANGAR],
            self::SHIP => self::MAP[self::SHIP],
        ],
        BalanceArticlesCategories::MOTOR_TRANSPORT => [
            self::CAR => self::MAP[self::CAR],
            self::TRUCK => self::MAP[self::TRUCK],
            self::TRAILER => self::MAP[self::TRAILER],
            self::SPECIAL_MACHINERY => self::MAP[self::SPECIAL_MACHINERY],
            self::TRACTOR => self::MAP[self::TRACTOR],
            self::TRUCK_CRANE => self::MAP[self::TRUCK_CRANE],
            self::AGRICULTURAL_MACHINERY => self::MAP[self::AGRICULTURAL_MACHINERY],
            self::LOADER => self::MAP[self::LOADER],
        ],
        BalanceArticlesCategories::EQUIPMENT => [
            self::SHOWCASE => self::MAP[self::SHOWCASE],
            self::RACK => self::MAP[self::RACK],
            self::TRADE_SOFTWARE => self::MAP[self::TRADE_SOFTWARE],
            self::FREEZER_EQUIPMENT => self::MAP[self::FREEZER_EQUIPMENT],
            self::PRINTING_EQUIPMENT => self::MAP[self::PRINTING_EQUIPMENT],
            self::LIFTING_EQUIPMENT => self::MAP[self::LIFTING_EQUIPMENT],
            self::SCAFFOLDING_AND_TIMBERING => self::MAP[self::SCAFFOLDING_AND_TIMBERING],
            self::METALWORKING_MACHINES => self::MAP[self::METALWORKING_MACHINES],
            self::WOODWORKING_MACHINES => self::MAP[self::WOODWORKING_MACHINES],
            self::UMPS_AND_COMPRESSORS => self::MAP[self::UMPS_AND_COMPRESSORS],
            self::MEANS_OF_COMMUNICATIONS => self::MAP[self::MEANS_OF_COMMUNICATIONS],
            self::OTHER_MECHANISMS => self::MAP[self::OTHER_MECHANISMS],
        ],
        BalanceArticlesCategories::TECH => [
            self::COMPUTER => self::MAP[self::COMPUTER],
            self::SERVERS => self::MAP[self::SERVERS],
            self::NETWORK_HARDWARE => self::MAP[self::NETWORK_HARDWARE],
            self::OFFICE_EQUIPMENT => self::MAP[self::OFFICE_EQUIPMENT],
            self::VIDEO_MONITORING => self::MAP[self::VIDEO_MONITORING],
        ],
        BalanceArticlesCategories::FURNITURE => [
            self::OFFICE_FURNITURE => self::MAP[self::OFFICE_FURNITURE],
            self::MEDICAL_FURNITURE => self::MAP[self::MEDICAL_FURNITURE],
            self::FURNITURE_FOR_ENTERPRISES => self::MAP[self::FURNITURE_FOR_ENTERPRISES],
        ],
        BalanceArticlesCategories::SOFTWARE => [
            self::ONE_C_ACCOUNTING => self::MAP[self::ONE_C_ACCOUNTING],
            self::CONSULTANT_PLUS => self::MAP[self::CONSULTANT_PLUS],
            self::OFFICE_PROGRAM => self::MAP[self::OFFICE_PROGRAM],
            self::LICENSED_SOFTWARE => self::MAP[self::LICENSED_SOFTWARE],
            self::SPECIAL_PROGRAM => self::MAP[self::SPECIAL_PROGRAM],
        ],
        BalanceArticlesCategories::LICENSE => [
            self::ALCOHOL_LICENSE => self::MAP[self::ALCOHOL_LICENSE],
            self::TRANSPORTATION_LICENSE => self::MAP[self::TRANSPORTATION_LICENSE],
            self::FSB_LICENSE => self::MAP[self::FSB_LICENSE],
            self::EDUCATION_LICENSE => self::MAP[self::EDUCATION_LICENSE],
            self::MEDICAL_LICENSE => self::MAP[self::MEDICAL_LICENSE],
        ],
        BalanceArticlesCategories::PATENT => [
            self::INVENTION_PATENT => self::MAP[self::INVENTION_PATENT],
            self::SOFTWARE_PATENT => self::MAP[self::SOFTWARE_PATENT],
            self::UTILITY_MODEL_PATENT => self::MAP[self::UTILITY_MODEL_PATENT],
            self::INDUSTRIAL_DESIGN_PATENT => self::MAP[self::INDUSTRIAL_DESIGN_PATENT],
        ],
        BalanceArticlesCategories::TRADEMARK => [
            self::TRADEMARK => self::MAP[self::TRADEMARK],
        ],
    ];

    const USEFUL_LIFE_IN_MONTH_MAP = [
        self::BUILDING => 120,
        self::OFFICE => 120,
        self::WAREHOUSE => 120,
        self::SHOP => 120,
        self::SHOPPING_PAVILION => 60,
        self::COMMERCIAL_PREMISES_IN_SHIPPING_CENTER => 120,
        self::PRODUCTION_ROOM => 120,
        self::LAND_PLOT => null,
        self::MANUFACTORY => 120,
        self::HANGAR => 60,
        self::SHIP => 120,
        self::CAR => 48,
        self::TRUCK => 84,
        self::TRAILER => 84,
        self::SPECIAL_MACHINERY => 60,
        self::TRACTOR => 60,
        self::TRUCK_CRANE => 84,
        self::AGRICULTURAL_MACHINERY => 60,
        self::LOADER => 48,
        self::SHOWCASE => 60,
        self::RACK => 24,
        self::TRADE_SOFTWARE => 24,
        self::FREEZER_EQUIPMENT => 60,
        self::PRINTING_EQUIPMENT => 60,
        self::LIFTING_EQUIPMENT => 60,
        self::SCAFFOLDING_AND_TIMBERING => 60,
        self::METALWORKING_MACHINES => 60,
        self::WOODWORKING_MACHINES => 60,
        self::UMPS_AND_COMPRESSORS => 60,
        self::MEANS_OF_COMMUNICATIONS => 60,
        self::OTHER_MECHANISMS => 60,
        self::COMPUTER => 24,
        self::SERVERS => 24,
        self::NETWORK_HARDWARE => 24,
        self::OFFICE_EQUIPMENT => 24,
        self::VIDEO_MONITORING => 24,
        self::OFFICE_FURNITURE => 60,
        self::MEDICAL_FURNITURE => 60,
        self::FURNITURE_FOR_ENTERPRISES => 60,
        self::ONE_C_ACCOUNTING => 24,
        self::CONSULTANT_PLUS => 24,
        self::OFFICE_PROGRAM => 24,
        self::LICENSED_SOFTWARE => 24,
        self::SPECIAL_PROGRAM => 24,
        self::ALCOHOL_LICENSE => 24,
        self::TRANSPORTATION_LICENSE => 24,
        self::FSB_LICENSE => 60,
        self::EDUCATION_LICENSE => 60,
        self::MEDICAL_LICENSE => 60,
        self::INVENTION_PATENT => 60,
        self::SOFTWARE_PATENT => 60,
        self::UTILITY_MODEL_PATENT => 60,
        self::INDUSTRIAL_DESIGN_PATENT => 60,
        self::TRADEMARK => 24,
    ];
}