<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.12.2019
 * Time: 23:15
 */

namespace frontend\modules\reference\models;

final class BalanceArticlesCategories
{
    const REAL_ESTATE = 0;
    const MOTOR_TRANSPORT = 1;
    const EQUIPMENT = 2;
    const TECH = 3;
    const FURNITURE = 4;
    const SOFTWARE = 5;
    const LICENSE = 6;
    const PATENT = 7;
    const TRADEMARK = 8;

    const MAP = [
        self::REAL_ESTATE => 'Недвижимость',
        self::MOTOR_TRANSPORT => 'Автотранспорт',
        self::EQUIPMENT => 'Оборудование',
        self::TECH => 'Техника',
        self::FURNITURE => 'Мебель',
        self::SOFTWARE => 'Программное обеспечение',
        self::LICENSE => 'Лицензии',
        self::PATENT => 'Патенты',
        self::TRADEMARK => 'Торговый знак',
    ];

    const FIXED_ASSETS_CATEGORIES_MAP = [
        self::REAL_ESTATE => self::MAP[self::REAL_ESTATE],
        self::MOTOR_TRANSPORT => self::MAP[self::MOTOR_TRANSPORT],
        self::EQUIPMENT => self::MAP[self::EQUIPMENT],
        self::TECH => self::MAP[self::TECH],
        self::FURNITURE => self::MAP[self::FURNITURE],
    ];

    const INTANGIBLE_ASSETS_CATEGORIES_MAP = [
        self::SOFTWARE => self::MAP[self::SOFTWARE],
        self::LICENSE => self::MAP[self::LICENSE],
        self::PATENT => self::MAP[self::PATENT],
        self::TRADEMARK => self::MAP[self::TRADEMARK],
    ];
}