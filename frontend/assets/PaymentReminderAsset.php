<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class PaymentReminderAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'scripts/payment-reminder.js'
    ];
}