<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Контрагенты/Досье
 */
class ContractorDossierAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    /**
     * @var array
     */
    public $css = [
        'css/contractor-dossier.css',
    ];

}