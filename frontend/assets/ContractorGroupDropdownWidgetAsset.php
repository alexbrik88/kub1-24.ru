<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 18.01.2022
 * Time: 11:43
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class Select2Asset
 * @package common\assets
 */
class ContractorGroupDropdownWidgetAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    /**
     * @var array
     */
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

    /**
     * @var array
     */
    public $css = [
        'css/expenditure-item-widget.css',
    ];
    /**
     * @var array
     */
    public $js = [
        'scripts/contractor-group-widget.js',
    ];
    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
