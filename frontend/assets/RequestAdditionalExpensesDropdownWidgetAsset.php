<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 05.12.2018
 * Time: 14:43
 */

namespace frontend\assets;

use yii\web\AssetBundle;


/**
 * Class RequestAdditionalExpensesDropdownWidgetAsset
 * @package frontend\assets
 */
class RequestAdditionalExpensesDropdownWidgetAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $basePath = '@webroot';
    /**
     * @var string
     */
    public $baseUrl = '@web';

    /**
     * @var array
     */
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

    /**
     * @var array
     */
    public $css = [
        'css/expenditure-item-widget.css',
    ];
    /**
     * @var array
     */
    public $js = [
        'scripts/expenditure-item-widget.js',
    ];
    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}