<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $basePath = '@webroot';
    /**
     * @var string
     */
    public $baseUrl = '@web';

    /**
     * @var array
     */
    public $css = [
        'css/subscribe.css',
        'css/export.css',
        'css/chat.css',
        'css/custom.css',
    ];
    /**
     * @var array
     */
    public $js = [
        'scripts/custom.js',
        'scripts/export.js',
        'scripts/vidimus.js',
        'scripts/scroll-button.js',
        'scripts/cash-flow-form.js',
        'scripts/bootstrap-tabdrop.js',
    ];
    /**
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'common\assets\CommonAsset',
        'common\assets\BootstrapTimepickerAsset',
        'yii\widgets\PjaxAsset',
    ];
}
