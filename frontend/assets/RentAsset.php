<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Учёт аренды
 * `/rent/*`
 */
class RentAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/rent.css',
    ];

    public $js = [
        'scripts/rent.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
