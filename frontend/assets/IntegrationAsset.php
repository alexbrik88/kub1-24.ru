<?php

namespace frontend\assets;

use yii\web\AssetBundle;


class IntegrationAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    /**
     * @var array
     */
    public $css = [
        'css/integration.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'scripts/integration-email.js',
    ];

}
