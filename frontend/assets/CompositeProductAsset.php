<?php

namespace frontend\assets;


use yii\web\AssetBundle;

/**
 * Class CompositeProductAsset
 * @package frontend\assets
 */
class CompositeProductAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $basePath = '@webroot';

    /**
     * @var array
     */
    public $js = [
        'scripts/composite_product.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
