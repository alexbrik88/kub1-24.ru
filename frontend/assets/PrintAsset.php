<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Improved performance but broken styles. Not used yet
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PrintAsset extends AssetBundle
{

    /**
     * @var string
     */
    public $basePath = '@webroot';

    /**
     * @var array
     */
    public $css = [
        'css/print/common.css',
    ];
}
