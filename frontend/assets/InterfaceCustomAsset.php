<?php

namespace frontend\assets;

use yii\web\AssetBundle;


class InterfaceCustomAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'scripts/interface-custom.js',
    ];

}
