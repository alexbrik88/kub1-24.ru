<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class Select2Asset
 * @package common\assets
 */
class ProductPriceDropdownWidgetAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    /**
     * @var array
     */
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

    /**
     * @var array
     */
    public $css = [
        //'css/expenditure-item-widget.css',
    ];
    /**
     * @var array
     */
    public $js = [
        'scripts/product-price-widget.js',
    ];
    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
