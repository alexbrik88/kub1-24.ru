<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 *
 */
class ProxyAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        //
    ];

    public $js = [
        'scripts/proxy.js',
    ];
}
