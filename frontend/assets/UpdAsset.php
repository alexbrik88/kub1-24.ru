<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 17.06.2017
 * Time: 20:11
 */

namespace frontend\assets;


use yii\web\AssetBundle;

/**
 * Class UpdAsset
 * @package frontend\assets
 */
class UpdAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $basePath = '@webroot';

    /**
     * @var array
     */
    public $css = [
        'css/print/common.css',
        'css/print/upd.css',
    ];
}