<?php

namespace frontend\models;

use common\models\product\CompositeMap;
use common\models\product\Product;
use yii\base\InvalidArgumentException;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class CompositeServiceForm extends Model
{
    private Product $_product;

    public $_servicesMaps = [];
    public $_goodsMaps = [];
    public $services = [];
    public $goods = [];
    public $services_quantity = [];
    public $goods_quantity = [];

    public function __construct(Product $product)
    {
        if ($product->production_type != Product::PRODUCTION_TYPE_SERVICE) {
            throw new InvalidArgumentException('The product must be of type "Service"');
        }
        $this->_product = $product;
        foreach ($product->compositeMaps as $compositeMap) {
            $simpleProduct = $compositeMap->simpleProduct;
            if ($simpleProduct->production_type == Product::PRODUCTION_TYPE_SERVICE) {
                $this->_servicesMaps[] = $compositeMap;
                $this->services[] = $compositeMap->simple_product_id;
                $this->services_quantity[] = +$compositeMap->quantity;
            } elseif ($simpleProduct->production_type == Product::PRODUCTION_TYPE_GOODS) {
                $this->_goodsMaps[] = $compositeMap;
                $this->goods[] = $compositeMap->simple_product_id;
                $this->goods_quantity[] = +$compositeMap->quantity;
            }
        }

        parent::__construct();
    }

    public function getProduct() : Product
    {
        return $this->_product;
    }

    public function getServicesMaps() : array
    {
        return $this->_servicesMaps;
    }

    public function getGoodsMaps() : array
    {
        return $this->_goodsMaps;
    }

    public function rules()
    {
        return [
            [['services'], function ($attribute, $params) {
                if (!$this->_product->isNewRecord && $this->_product->getCompositeProducts()->exists()) {
                    $msg = sprintf('Услуга "%s" уже является частью другой составной услуги.', $this->_product->title);
                    $this->addError($attribute, $msg);
                }
            }],
            [
                ['services'], 'required',
                'enableClientValidation' => false,
                'message' => 'Необходимо заполнить',
            ],
            ['services', 'each', 'rule' => [
                'exist',
                'targetClass' => Product::class,
                'targetAttribute' => 'id',
                'filter' => [
                    'company_id' => $this->_product->company_id,
                    'production_type' => Product::PRODUCTION_TYPE_SERVICE,
                    'unit_type' => Product::UNIT_TYPE_SIMPLE,
                ],
            ]],
            ['goods', 'each', 'rule' => [
                'exist',
                'targetClass' => Product::class,
                'targetAttribute' => 'id',
                'filter' => [
                    'company_id' => $this->_product->company_id,
                    'production_type' => Product::PRODUCTION_TYPE_GOODS,
                    'unit_type' => Product::UNIT_TYPE_SIMPLE,
                ],
            ]],
            [['services_quantity', 'goods_quantity'], 'safe'],
            [['services', 'goods'], 'validateItem', 'enableClientValidation' => false],
        ];
    }

    public function validateItem($attribute, $params)
    {
        if ($this->$attribute) {
            if (!is_array($this->$attribute)) {
                $this->addError($attribute, 'Не верное значение.');
            } elseif (count($this->$attribute) !== count(array_unique($this->$attribute))) {
                $this->addError($attribute, 'Повторяющиеся позиции не допускаются.');
            } else {
                $quantityAttribute = $attribute.'_quantity';
                if (!is_array($this->$quantityAttribute) || count($this->$attribute) !== count($this->$quantityAttribute)) {
                    $this->addError($attribute, 'Необходимо указать количество для каждой позиции.');
                } else {
                    foreach ($this->$quantityAttribute as $key => $value) {
                        $value = strtr($value, ',', '.');
                        $this->$quantityAttribute[$key] = $value;
                        if (!is_numeric($value) || $value <= 0) {
                            $this->addError($attribute, 'Количество должно быть положительным числом, отличным от нуля.');
                        }
                    }
                }
            }
        }
    }

    public function attributeLabels() :array
    {
        return [
            'services' => 'Услуги/Работы',
            'goods' => 'Материалы',
            'services_quantity' => 'Количество',
            'goods_quantity' => 'Количество',
        ];
    }

    public function getItemIds() :array
    {
        return array_merge($this->services, $this->goods);
    }

    public function getSimpleProducts() :array
    {
        $itemIds = $this->getItemIds();

        if (empty($itemIds)) {
            return [];
        }

        return Product::find()->andWhere([
            'id' => $itemIds,
            'company_id' => $this->_product->company_id,
        ])->indexBy('id')->all();
    }

    public function getItemList() :array
    {
        return ArrayHelper::map($this->getSimpleProducts(), 'id', 'title');
    }

    /**
     * @inheritdoc
     */
    /*public function load($data, $formName = null)
    {
        return intval($this->model->load($data)) * intval(parent::load($data, $formName)) > 0;
    }*/

    public function save($validate = true) :bool
    {
        if ($validate && !$this->validate()) {
            return false;
        }

        $itemIds = $this->getItemIds();

        if (empty($itemIds)) {
            return false;
        }

        $rows = [];
        foreach ($this->services as $key => $simpleId) {
            $rows[] = [$this->_product->id, $simpleId, $this->services_quantity[$key]];
        }
        foreach ($this->goods as $key => $simpleId) {
            $rows[] = [$this->_product->id, $simpleId, $this->goods_quantity[$key]];
        }


        $productUpdateAttributes = [];
        if ($this->_product->unit_type != Product::UNIT_TYPE_COMPOSITE) {
            $productUpdateAttributes['unit_type'] = Product::UNIT_TYPE_COMPOSITE;
        }
        $simpleProducts = $this->getSimpleProducts();
        $costPrice = 0;
        foreach ($rows as $row) {
            $costPrice += round($simpleProducts[$row[1]]->price_for_buy_with_nds * $row[2]);
        }
        if ($this->_product->price_for_buy_with_nds != $costPrice) {
            $productUpdateAttributes['price_for_buy_with_nds'] = $costPrice;
        }

        CompositeMap::deleteAll([
            'composite_product_id' => $this->_product->id,
        ]);

        \Yii::$app->db->createCommand()->batchInsert(CompositeMap::tableName(), [
            'composite_product_id',
            'simple_product_id',
            'quantity',
        ], $rows)->execute();

        if ($productUpdateAttributes) {
            $this->_product->updateAttributes($productUpdateAttributes);
        }

        return true;
    }
}
