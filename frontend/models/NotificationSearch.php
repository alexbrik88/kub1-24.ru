<?php
/**
 * Created by konstantin.
 * Date: 21.7.15
 * Time: 17.31
 */

namespace frontend\models;


use common\models\Company;
use common\models\notification\Notification;
use yii\data\ActiveDataProvider;
use Yii;

/**
 * Class NotificationSearch
 * @package frontend\models
 */
class NotificationSearch extends Notification
{

    /**
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param $params
     * @param Company $company
     * @return ActiveDataProvider
     */
    public function search($params, Company $company)
    {
        $query = static::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 6,
            ],
            'sort' => [
                'defaultOrder' => ['event_date' => SORT_ASC],
            ],
        ]);
        $nextDay = strtotime("-1 day");

        $query->byActivationDate()
            ->byEventDate($nextDay)
            ->byNotificationType($this->notification_type)
            ->byWhom($company);

        return $dataProvider;
    }


    /**
     * @param $params
     * @param $month
     * @param $year
     * @param Company $company
     * @param int $pageSize
     * @return ActiveDataProvider
     */
    public function searchIndex($params, $month, $year, Company $company, $pageSize = 20)
    {
        $query = static::find()
            ->andWhere(['between',
                'event_date',
                $year . '-' . $month . '-01',
                $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, (int) $month, $year)])
            ->byNotificationType($this->notification_type)
            ->byWhom($company);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $pageSize,
            ],
            'sort' => [
                'defaultOrder' => [
                    'event_date' => SORT_ASC,
                ],
            ],
        ]);

        $this->load($params);

        return $dataProvider;
    }
}
