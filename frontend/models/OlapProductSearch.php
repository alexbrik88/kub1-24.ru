<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\product\ProductTurnover;

/**
 * OlapProductSearch represents the model behind the search form of `common\models\product\ProductTurnover`.
 */
class OlapProductSearch extends ProductTurnover
{
    protected $periodFrom;
    protected $periodTo;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'order_id',
                    'document_id',
                    'product_id',
                    'product_group_id',
                    'invoice_id',
                    'contractor_id',
                    'type',
                    'production_type',
                    'year',
                    'month',
                    'not_for_sale',
                ],
                'integer',
            ],
            [
                [
                    'document_table',
                    'date',
                ],
                'safe',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * {@inheritdoc}
     */
    public function setPeriodFrom(\DateTimeInterface $dateTime)
    {
        $this->periodFrom = $dateTime;
    }

    /**
     * {@inheritdoc}
     */
    public function setPeriodTo(\DateTimeInterface $dateTime)
    {
        $this->periodTo = $dateTime;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductTurnover::find();

        // add conditions that should always apply here
        $query->andWhere([
            'company_id' => $this->company_id,
        ])->andFilterWhere([
            '>=', 'date', $this->periodFrom ? $this->periodFrom->format('Y-m-d') : null,
        ])->andFilterWhere([
            '<=', 'date', $this->periodTo ? $this->periodTo->format('Y-m-d') : null,
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');

            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_id' => $this->order_id,
            'document_id' => $this->document_id,
            'date' => $this->date,
            'product_id' => $this->product_id,
            'product_group_id' => $this->product_group_id,
            'invoice_id' => $this->invoice_id,
            'contractor_id' => $this->contractor_id,
            'type' => $this->type,
            'production_type' => $this->production_type,
            'is_invoice_actual' => $this->is_invoice_actual,
            'is_document_actual' => $this->is_document_actual,
            'purchase_price' => $this->purchase_price,
            'price_one' => $this->price_one,
            'quantity' => $this->quantity,
            'total_amount' => $this->total_amount,
            'purchase_amount' => $this->purchase_amount,
            'margin' => $this->margin,
            'year' => $this->year,
            'month' => $this->month,
            'not_for_sale' => $this->not_for_sale,
            'document_table' => $this->document_table,
        ]);

        return $dataProvider;
    }
}
