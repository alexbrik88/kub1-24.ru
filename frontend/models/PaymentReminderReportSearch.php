<?php

namespace frontend\models;

use common\models\Contractor;
use common\models\paymentReminder\PaymentReminderMessageCategory;
use common\models\paymentReminder\Report;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PaymentReminderReportSearch represents the model behind the search form of `common\models\paymentReminder\Report`.
 */
class PaymentReminderReportSearch extends Report
{
    protected $_query;

    public $responsible_employee_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contractor_id', 'category_id', 'is_sent'], 'integer'],        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $period = null)
    {
        $this->_query = $query = Report::find()->joinWIth('contractor');

        // add conditions that should always apply here
        $query->andWhere([
            'payment_reminder_report.company_id' => $this->company_id,
        ]);
        if (isset($this->responsible_employee_id)) {
            $query->andWhere([
                'contractor.responsible_employee_id' => $this->responsible_employee_id,
            ]);
        }
        if (isset($period)) {
            $query->andWhere(['between', 'payment_reminder_report.date', $period['from'], $period['to']]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'date' => SORT_DESC,
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'payment_reminder_report.contractor_id' => $this->contractor_id,
            'payment_reminder_report.is_sent' => $this->is_sent,
            'payment_reminder_report.category_id' => $this->category_id,
        ]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getSearchQuery()
    {
        return $this->_query ? clone $this->_query : null;
    }

    /**
     * @return array
     */
    public function getContractorFilterItems()
    {
        return ['' => 'Все'] + Contractor::getSorted()->select([
                'contractor_name' => 'CONCAT_WS(" ", {{company_type}}.[[name_short]], {{contractor}}.[[name]])',
                'contractor.id',
            ])->andWhere([
                'contractor.id' => $this->getSearchQuery()->select('contractor_id')->distinct()->column(),
            ])->indexBy('id')->column();
    }

    /**
     * @return array
     */
    public function getIsSentFilterItems()
    {
        return ['' => 'Все'] + Report::$isSentItems;
    }

    /**
     * @return array
     */
    public function getCategoryFilterItems()
    {
        $items = PaymentReminderMessageCategory::find()->select('name')->indexBy('id')->column();

        return ['' => 'Все'] + $items;
    }
}
