<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 23.01.2018
 * Time: 12:30
 */

namespace frontend\models;


use yii\base\Model;
use common\models\employee\Employee;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * Class ChangePushNoveltyForm
 * @package frontend\models
 */
class ChangePushNotifyForm extends Model
{
    /**
     * @var
     */
    public $pushNotificationNewMessage;
    /**
     * @var
     */
    public $pushNotificationCreateClosestDocument;
    /**
     * @var
     */
    public $pushNotificationOverdueInvoice;

    /**
     * @var Employee
     */
    private $_user;

    /**
     * @param array $config
     * @param null $user
     * @throws NotFoundHttpException
     */
    public function __construct($config = [], $user = null)
    {
        $this->_user = $user ? $user : Yii::$app->user->identity;
        if (!$this->_user) {
            throw new NotFoundHttpException;
        }

        parent::__construct($config = []);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pushNotificationNewMessage', 'pushNotificationCreateClosestDocument', 'pushNotificationOverdueInvoice'], 'safe'],
            [['pushNotificationNewMessage', 'pushNotificationCreateClosestDocument', 'pushNotificationOverdueInvoice'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pushNotificationNewMessage' => 'Push-уведомление по новому сообщению в чате',
            'pushNotificationCreateClosestDocument' => 'Push-уведомление по необходимости создания закрывающего документа',
            'pushNotificationOverdueInvoice' => 'Push-уведомление по появлению просроченного счета',
        ];
    }

    /**
     * @return bool
     */
    public function savePushNotify()
    {
        $user = $this->_user;
        $user->push_notification_new_message = $this->pushNotificationNewMessage;
        $user->push_notification_create_closest_document = $this->pushNotificationCreateClosestDocument;
        $user->push_notification_overdue_invoice = $this->pushNotificationOverdueInvoice;

        return $user->save(false, [
            'push_notification_new_message', 'push_notification_create_closest_document', 'push_notification_overdue_invoice',
        ]);
    }
}