<?php
namespace frontend\models;

use common\components\sender\unisender\UniSender;
use common\models\employee\Employee;
use common\models\employee\EmployeeQuery;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    /**
     * @var
     */
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => Employee::className(),
                'filter' => function (EmployeeQuery $query) {
                    return $query->isActual();
                },
                'message' => 'Пользователь с таким e-mail не найден.',
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user Employee */
        $user = Employee::find()
            ->isActual()
            ->byEmail($this->email)
            ->one();

        if ($user) {
            if (!Employee::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
            }

            if ($user->save(true, ['password_reset_token'])) {
                return \Yii::$app->mailer->compose([
                    'html' => 'system/password-reset-token/html',
                    'text' => 'system/password-reset-token/text',
                ], [
                    'passwordRecoverUrl' => \Yii::$app->urlManager->createAbsoluteUrl([
                        'site/reset-password',
                        'token' => $user->password_reset_token,
                    ]),
                    'supportEmail' => \Yii::$app->params['emailList']['support'],
                    'user' => $user,
                    'subject' => 'Восстановление пароля',
                ])
                    ->setFrom([\Yii::$app->params['emailList']['support'] => \Yii::$app->params['emailFromName']])
                    ->setTo($this->email)
                    ->setSubject('Восстановление пароля для ' . \Yii::$app->params['serviceSite'])
                    ->send();
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function sendMailMobileApplication()
    {
        /* @var $user Employee */
        $user = Employee::find()
            ->isActual()
            ->byEmail($this->email)
            ->one();

        if ($user) {
            if (!Employee::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
            }

            if ($user->save(true, ['password_reset_token'])) {
                return \Yii::$app->mailer->compose([
                    'html' => 'system/password-reset-token-mobile/html',
                    'text' => 'system/password-reset-token-mobile/text',
                ], [
                    'passwordResetToken' => $user->password_reset_token,
                    'supportEmail' => \Yii::$app->params['emailList']['support'],
                    'user' => $user,
                    'subject' => 'Восстановление пароля',
                ])
                    ->setFrom([\Yii::$app->params['emailList']['support'] => \Yii::$app->params['emailFromName']])
                    ->setTo($this->email)
                    ->setSubject('Восстановление пароля для ' . \Yii::$app->params['serviceSite'])
                    ->send();
            }
        }

        return false;
    }
}
