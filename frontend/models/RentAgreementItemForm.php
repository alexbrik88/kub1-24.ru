<?php

namespace frontend\models;

use common\models\rent\Entity;
use common\models\rent\RentAgreement;
use common\models\rent\RentAgreementItem;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class RentAgreementItemForm
 */
class RentAgreementItemForm extends \yii\base\Model
{
    public $id;
    public $rent_entity_id;
    public $price;
    public $discount;

    private ?RentAgreement $rentAgreement = null;
    private ?RentAgreementItem $model = null;
    private $data = [];

    public function __construct(?RentAgreement $rentAgreement = null, ?RentAgreementItem $item = null)
    {
        $this->rentAgreement = $rentAgreement;
        if ($item) {
            $this->id = $item->id;
            $this->rent_entity_id = $item->rent_entity_id;
            $this->price = $item->price / 100;
            $this->discount = $item->discount * 1;
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'price',
                ],
                'required',
            ],
            [
                [
                    'price',
                ],
                'number',
                'min' => '0.01',
            ],
            [
                [
                    'discount',
                ],
                'number',
                'min' => 0,
            ],
            [
                ['id'], 'exist',
                'targetClass' => RentAgreementItem::class,
                'targetAttribute' => 'id',
                'filter' => ['rent_agreement_id' => $this->rentAgreement->id],
            ],
            [
                ['rent_entity_id'], 'exist',
                'targetClass' => Entity::class,
                'targetAttribute' => 'id',
                'filter' => $this->rentAgreement ? ['company_id' => $this->rentAgreement->company_id] : [],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'price' => 'Цена объекта',
            'discount' => 'Скидка на объект',
            'rent_entity_id' => 'Объект',
        ];
    }

    public function getAmount()
    {
        $discount_type = ArrayHelper::getValue($this->rentAgreement, ['discount_type']);
        $price = is_numeric($this->price) ? $this->price : 0;
        $discount = is_numeric($this->discount) ? $this->discount : 0;
        $amount = $discount_type == 1 ? ($price - $discount) : ($price - $price / 100 * $discount);

        return round($amount, 2);
    }

    public function getEntity() : ?Entity
    {
        if (!array_key_exists('getEntity', $this->data)) {
            $this->data['getEntity'] = Entity::findOne($this->rent_entity_id);
        }

        return $this->data['getEntity'];
    }

    public function getModel() : ?RentAgreementItem
    {
        return $this->model;
    }

    public function save(int $number)
    {
        $this->model = null;

        if (!$this->validate()) {
            return false;
        }

        $model = $this->id ? RentAgreementItem::findOne($this->id) : new RentAgreementItem([
            'rent_agreement_id' => $this->rentAgreement->id,
        ]);

        $model->rent_entity_id = $this->rent_entity_id;
        $model->number = $number;
        $model->price = round($this->price * 100);
        $model->discount = is_numeric($this->discount) ? $this->discount : 0;
        $model->amount = round($this->getAmount() * 100);

        if ($model->save()) {
            $this->model = $model;

            return true;
        } else {
            \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);
        }

        return false;
    }
}
