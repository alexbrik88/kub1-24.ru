<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 13.10.15
 * Time: 12.15
 * Email: t.kanstantsin@gmail.com
 */

namespace frontend\models;

use common\models\Company;
use common\models\employee\Employee;
use yii\base\Model;

/**
 * Class VisitCardSendForm
 * @package frontend\models
 */
class VisitCardSendForm extends Model
{
    /**
     * Recipient
     * @var string
     */
    public $email;

    /**
     * @var Company
     */
    public $company;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required',],
            [['email'], 'email',],
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'email' => 'E-mail',
        ];
    }

    /**
     * Sends invoices to participants
     */
    public function send()
    {
        \Yii::$app->mailer->htmlLayout = 'layouts/document-html';
        /** @var Employee $sender */
        $sender = \Yii::$app->user->identity;
        $params = [
            'company' => $this->company,
            'employee' => $sender,
            'subject' => 'Визитка компании',
            'pixel' => [
                'company_id' => $this->company->id,
                'email' => is_array($this->email) ? implode(',', $this->email) : $this->email,
            ],
        ];

        return \Yii::$app->mailer->compose([
            'html' => 'system/visit-card/html',
            'text' => 'system/visit-card/text',
        ], $params)
            ->setFrom([\Yii::$app->params['emailList']['info'] => $this->company->chiefFio])
            ->setTo($this->email)
            ->setReplyTo([$sender->email => $sender->getFio(true)])
            ->setSubject('Визитка компании')
            ->send();
    }
}