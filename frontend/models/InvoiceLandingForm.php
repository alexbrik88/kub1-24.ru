<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 09.12.2016
 * Time: 5:30
 */

namespace frontend\models;


use common\components\date\DateHelper;
use common\components\pdf\PdfRenderer;
use common\components\validators\EmployeeEmailValidator;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\TimeZone;
use yii\base\Model;
use yii\db\Connection;
use yii\web\NotFoundHttpException;

/**
 * Class InvoiceLandingForm
 * @package frontend\models
 */
class InvoiceLandingForm extends Model
{
    /**
     * @var
     */
    public $fromEmail;
    /**
     * @var
     */
    public $toEmail;
    /**
     * @var
     */
    public $companyId;
    /**
     * @var bool
     */
    public $sendMessage = false;
    /**
     * @var null
     */
    public $emailSubject = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fromEmail', 'toEmail', 'companyId'], 'required', 'message' => 'Необходимо заполнить.'],
            [['fromEmail', 'toEmail'], 'email'],
            [['fromEmail'], EmployeeEmailValidator::className(),],
            [
                ['fromEmail'], 'unique',
                'targetClass' => Employee::className(),
                'targetAttribute' => 'email',
                'message' => 'Пользователь с указанным email уже использует сервис КУБ. Выставить счет можно через <a href="login">личный кабинет</a>'
            ],
            [['emailSubject'], 'string', 'max' => 255],
            [['companyId'], 'integer'],
            [['sendMessage'], 'boolean'],
            [['companyId'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['companyId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fromEmail' => 'От',
            'toEmail' => 'Кому',
            'emailSubject' => 'Тема',
            'companyId' => 'Company ID',
        ];
    }

    /**
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function _save()
    {
        /* @var $company Company
         * @var $invoice Invoice
         * @var $contractor Contractor
         */
        $company = Company::findOne($this->companyId);
        $invoice = Invoice::findOne(['company_id' => $company->id]);
        $contractor = Contractor::findOne(['company_id' => $company->id]);
        if ($company == null) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }
        if ($invoice == null) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }
        if ($contractor == null) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }
        $user = $this->createUser($company);
        $password = Employee::generatePassword(Employee::PASS_LENGTH);
        $user->setPassword($password);

        return \Yii::$app->db->transaction(function (Connection $db) use ($company, $user, $password, $invoice, $contractor) {
            if ($user->save(false)) {
                $employeeCompany = new EmployeeCompany([
                    'employee_id' => $user->id,
                    'company_id' => $company->id,
                    'employee_role_id' => (int) $user->employee_role_id,
                ]);
                $employeeCompany->setEmployee($user);
                if ($employeeCompany->save(false)) {
                    $this->sendUserEmail($user, $password, $company);
                    $contractor->employee_id = $user->id;
                    $contractor->director_email = $this->toEmail;
                    $contractor->save(false, ['employee_id', 'director_email']);
                    $company->test = !Company::TEST_COMPANY;
                    $company->email = $user->email;
                    $company->save(false, ['test', 'email']);
                    if ($this->sendMessage) {
                        if ($this->sendInvoice($invoice, $user)) {
                            $invoice->document_author_id = $user->id;
                            $invoice->invoice_status_author_id = $user->id;
                            $invoice->email_messages += 1;
                            $invoice->save(true, ['email_messages', 'document_author_id', 'invoice_status_author_id']);

                            return true;
                        }
                    } else {
                        return true;
                    }
                }
            }
            $db->transaction->rollBack();

            return false;
        });
    }

    /**
     * @param Company $company
     * @return Employee
     */
    public function createUser(Company $company)
    {
        $user = new Employee();
        if ($company->company_type_id !== CompanyType::TYPE_IP) {
            $prefix = 'chief_';
            $chiefPostName = 'Генеральный директор';
        } else {
            $prefix = 'ip_';
            $chiefPostName = 'Предприниматель';
        }
        $user->setAttributes([
            'employee_role_id' => EmployeeRole::ROLE_CHIEF,
            'email' => $this->fromEmail,
            'is_registration_completed' => true,
            'time_zone_id' => TimeZone::DEFAULT_TIME_ZONE,
            'company_id' => $company->id,
            'main_company_id' => $company->id,
            'position' => $chiefPostName,
            'lastname' => $company->getAttribute($prefix . 'lastname'),
            'firstname' => $company->getAttribute($prefix . 'firstname'),
            'patronymic' => $company->getAttribute($prefix . 'patronymic'),
            'firstname_initial' => mb_substr($company->getAttribute($prefix . 'firstname'), 0, 1),
            'patronymic_initial' => mb_substr($company->getAttribute($prefix . 'patronymic'), 0, 1),
//            'wp_id' => $form->wpId,
        ]);

        return $user;
    }

    /**
     * @param Employee $user
     * @param $password
     * @param Company $company
     */
    public function sendUserEmail(Employee $user, $password, Company $company)
    {
        $mailer = clone \Yii::$app->mailer;
        $mailer->htmlLayout = 'layouts/html2';
        $mailer->compose([
            'html' => 'system/new-registration/html',
            'text' => 'system/new-registration/text',
        ], [
            'login' => $user->email,
            'password' => $password,
            'subject' => 'Добро пожаловать в КУБ24',
        ])
            ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
            ->setTo($user->email)
            ->setSubject('Добро пожаловать в КУБ24')
            ->send();
    }

    /**
     * @param Invoice $invoice
     * @param Employee $user
     * @return bool
     * @throws NotFoundHttpException
     * @throws \MpdfException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function sendInvoice(Invoice $invoice, Employee $user)
    {
        $fileName = \Yii::getAlias('@frontend/runtime/pdf-cache/') . 'out-invoice-' . $invoice->id;
        $renderer = Invoice::getRenderer(null, $invoice, PdfRenderer::DESTINATION_STRING, null)->output(false);
        $emailMessagePath = [
            'html' => 'system/documents/invoice-out/html',
            'text' => 'system/documents/invoice-out/text',
        ];
        $subject = 'Счет No ' . $invoice->fullNumber
            . ' от ' . DateHelper::format($invoice->document_date, 'd.m.y', DateHelper::FORMAT_DATE)
            . ' от ' . $invoice->company_name_short;
        $attachContent['content'] = mb_convert_encoding(\Yii::$app->view->render('@frontend/modules/documents/views/invoice/1C_payment_order.php', [
            'model' => $invoice,
        ]), 'CP1251');
        $attachContent['fileName'] = 'payment_order.txt';
        $attachContent['contentType'] = 'text/plain';

        $params = [
            'model' => $invoice,
            'employee' => $user,
            'subject' => $subject,
        ];

        \Yii::$app->mailer->htmlLayout = 'layouts/document-html';
        $message = \Yii::$app->mailer->compose($emailMessagePath, $params)
            //->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
            ->setFrom([\Yii::$app->params['emailList']['docs'] => $user->getFio()])
            ->setReplyTo([$user->email => $user->getFio(true)])
            ->setSubject($subject)
            ->attachContent($renderer, [
                'fileName' => $invoice->pdfFileName,
                'mime' => 'application/pdf',
            ]);

        if ($attachContent) {
            $message->attachContent($attachContent['content'], [
                'fileName' => $attachContent['fileName'],
                'contentType' => $attachContent['contentType'],
            ]);
        }

        return $message->setTo($this->toEmail)->send();
    }
}