<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\web\NotFoundHttpException;
use common\models\employee\Employee;

/**
 * Password reset form
 *
 * @property Employee $user
 */
class ChangeNotifyForm extends Model
{
    public $notifyNearlyReport;
    public $notifyNewFeatures;

    /**
     * @var Employee
     */
    private $_user;

    /**
     * @param array $config
     * @param null $user
     * @throws NotFoundHttpException
     */
    public function __construct($config = [], $user = null)
    {
        $this->_user = $user ? $user : Yii::$app->user->identity;
        if (!$this->_user) {
            throw new NotFoundHttpException;
        }

        parent::__construct($config = []);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notifyNearlyReport', 'notifyNewFeatures'], 'safe'],
            [['notifyNearlyReport', 'notifyNewFeatures'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'notifyNearlyReport' => 'о приближающейся отчетности и налогах (за 5 дней)',
            'notifyNewFeatures' => 'о новых возможностях сервиса',
        ];
    }

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function saveNotify()
    {
        $user = $this->_user;
        $user->notify_nearly_report = $this->notifyNearlyReport;
        $user->notify_new_features = $this->notifyNewFeatures;

        return $user->save(true, [
            'notify_nearly_report', 'notify_new_features',
        ]);
    }

    public function getUser()
    {
        return $this->_user;
    }
}
