<?php

namespace frontend\models;

use common\components\date\DateHelper;
use common\components\validators\EmployeeEmailValidator;
use common\components\validators\PasswordValidator;
use common\components\validators\PhoneValidator;
use common\models\Company;
use common\models\company\CompanyNotification;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use common\models\company\RegistrationPageType;
use common\models\CompanyProductType;
use common\models\Contractor;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\notification\Notification;
use common\models\product\Product;
use common\models\product\ProductSearch;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use common\models\TaxationType;
use common\models\TimeZone;
use console\components\sender\Findirector;
use frontend\components\StatisticPeriod;
use frontend\modules\subscribe\forms\PromoCodeForm;
use Yii;
use yii\base\Model;
use yii\db\Connection;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class FinanceRegistrationForm
 * @property Company $company
 * @property Employee $user
 * @package frontend\models
 */
class FinanceRegistrationForm extends Model
{
    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_CAPTCHA = 'captcha';

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $phone;

    /**
     * @var string
     */
    public $promoCode;

    /**
     * @var null
     */
    public $registrationPageTypeId = null;

    /**
     * @var string
     */
    public $googleAnalyticsId = null;

    /**
     * @var string
     */
    public $utm = null;

    /**
     * @var string
     */
    public $source = null;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $req;

    /**
     * @var string
     */
    public $reCaptcha;

    /**
     * @var Company
     */
    private $_company;
    /**
     * @var Employee
     */
    private $_user;

    /**
     * @var array
     */
    public static $typeIds = [
        CompanyType::TYPE_IP,
        CompanyType::TYPE_OOO,
        CompanyType::TYPE_PAO,
        CompanyType::TYPE_EMPTY,
    ];

    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => [
                'name',
                'email',
                'phone',
                'promoCode',
                'registrationPageTypeId',
                'googleAnalyticsId',
                'utm',
                'source',
                'password',
                'req',
            ],
            self::SCENARIO_CAPTCHA => [
                'name',
                'email',
                'phone',
                'promoCode',
                'registrationPageTypeId',
                'googleAnalyticsId',
                'utm',
                'source',
                'password',
                'req',
                'reCaptcha',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'phone'], 'trim'],
            [['name', 'email', 'phone'], 'required'],
            [['name', 'promoCode'], 'string', 'max' => 50],
            [['name', 'promoCode', 'googleAnalyticsId', 'utm', 'source'], 'string'],
            [['email'], 'email'],
            [
                ['email'], 'unique',
                'targetClass' => Employee::className(),
                'targetAttribute' => 'email',
                'filter' => ['is_deleted' => false],
            ],
            [['phone'], PhoneValidator::className()],
            [['req'], 'string'],
            [['registrationPageTypeId'], 'integer'],
            [['registrationPageTypeId'], 'default', 'value' => RegistrationPageType::PAGE_TYPE_ANALYTICS],
            [
                ['registrationPageTypeId'], 'exist',
                'skipOnError' => true,
                'targetClass' => RegistrationPageType::className(),
                'targetAttribute' => ['registrationPageTypeId' => 'id'],
            ],
            [['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator2::className()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'promoCode' => 'Промокод',
        ];
    }

    /**
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function save($validate = true)
    {
        if ($validate && !$this->validate()) {
            return false;
        }

        return \Yii::$app->db->transaction(function (Connection $db) {
            if (empty($this->password)) {
                $this->password = Employee::generatePassword(Employee::PASS_LENGTH);
            }
            // create user
            $user = RegistrationForm::getNewEmployee([
                'firstname' => $this->name,
                'email' => $this->email,
                'phone' => $this->phone,
                'is_old_kub_theme' => false,
                'notify_new_features' => 1,
                'notify_nearly_report' => 1
            ]);
            $user->setPassword($this->password);

            // save user
            if ($user->save(false)) {
                // create company
                $company = RegistrationForm::getNewCompany([
                    'self_employed' => false,
                    'owner_employee_id' => $user->id,
                    'created_by' => $user->id,
                    'company_type_id' => CompanyType::TYPE_OOO,
                    'email' => $this->email,
                    'phone' => $this->phone,
                    'registration_page_type_id' => $this->registrationPageTypeId,
                    'form_utm' => $this->utm,
                    'form_source' => $this->source
                ]);

                $companyTaxationType = new CompanyTaxationType();
                $company->populateRelation('companyTaxationType', $companyTaxationType);

                $company->saveInvite($this->req);

                // save company
                if ($company->save(false) && $company->createTrialSubscribe()) {
                    $company->main_id = $company->id;
                    $company->updateAttributes(['main_id' => $company->id]);
                    $company->companyTaxationType->company_id = $company->id;
                    if ($company->companyTaxationType->save(false) && Contractor::createFounder($company)) {
                        $employeeCompany = new EmployeeCompany([
                            'company_id' => $company->id,
                        ]);
                        $employeeCompany->employee = $user;
                        if ($this->promoCode) {
                            $promoCode = new PromoCodeForm([
                                'company' => $company,
                                'code' => $this->promoCode,
                            ]);
                            $promoCode->save();
                        }
                        if ($employeeCompany->save(false)) {
                            $user->updateAttributes([
                                'company_id' => $company->id,
                                'main_company_id' => $company->id,
                            ]);
                            $user->config->updateAttributes([
                                //'cash_operations_help' => 1,
                                'theme_wide' => 1,
                            ]);
                            if ($user->getCompany(true) !== null) {
                                $this->_user = $user;
                                $this->_company = $company;
                                //$this->sendEmail();

                                return true;
                            }
                        }
                    }
                }
            }

            if ($db->getTransaction()->isActive) {
                $db->getTransaction()->rollBack();
            }

            return false;
        });
    }

    /**
     * @return Employee
     */
    public function getUser()
    {
        return $this->_user;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @return Company
     */
    public function sendEmail()
    {
        $subject = 'Регистрация в КУБ24.ФинДиректор';
        \Yii::$app->mailer->compose([
            'html' => 'system/new-analytics-registration/html',
            'text' => 'system/new-analytics-registration/text',
        ], [
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'time' => $this->_user->created_at,
            'regPage' => ArrayHelper::getValue($this->getCompany(), ['registrationPageType', 'name']),
            'source' => 'Лендинг',
            'subject' => $subject,
        ])
        ->setFrom([Yii::$app->params['emailList']['info'] => Findirector::$from])
        ->setTo(Yii::$app->params['emailList']['support'])
        ->setSubject($subject)
        ->send();
    }
}
