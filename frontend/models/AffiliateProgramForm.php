<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 10.07.2017
 * Time: 14:31
 */

namespace frontend\models;


use common\components\validators\PhoneValidator;
use common\models\employee\Employee;
use common\models\service\RewardRequest;
use yii\base\Model;
use yii\db\Connection;

/**
 * Class AffiliateProgramForm
 * @package frontend\models
 */
class AffiliateProgramForm extends Model
{
    /**
     *
     */
    const RS_TYPE = 1;
    /**
     *
     */
    const INDIVIDUAL_ACCOUNT_TYPE = 2;

    /**
     * @var
     */
    public $amount;

    /**
     * @var
     */
    public $type;

    /**
     * @var
     */
    public $withdrawal_amount;

    /**
     * @var
     */
    public $phone;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount', 'type', 'withdrawal_amount', 'phone'], 'required'],
            [['amount', 'type'], 'integer'],
            [['phone'], PhoneValidator::className()],
            [['withdrawal_amount'], 'integer', 'min' => 1],
            [['withdrawal_amount'],
                'compare',
                'compareValue' => $this->amount,
                'operator' => '<=',
                'message' => 'Допустимая сумма вывода вознаграждения не должна превышать ' . $this->amount . ' руб.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'amount' => 'Вознаграждение',
            'type' => 'Куда выводить',
            'withdrawal_amount' => 'Сумма',
            'phone' => 'Телефон',
        ];
    }

    /**
     * @return bool
     */
    public function _save()
    {
        /* @var $user Employee */
        $user = \Yii::$app->user->identity;
        $company = $user->company;
        $company->affiliate_sum -= (int) $this->withdrawal_amount;
        if ($this->type == self::RS_TYPE) {
            $company->payed_reward_for_rs += (int) $this->withdrawal_amount;
        } else {
            $company->payed_reward_for_card += (int) $this->withdrawal_amount;
        }
        $company->active_reward_requests += 1;
        if ($this->phone) {
            $company->phone = $this->phone;
        }

        return \Yii::$app->db->transaction(function (Connection $db) use ($company, $user) {
            if ($company->save(true, ['affiliate_sum', 'payed_reward_for_rs', 'payed_reward_for_card', 'phone', 'active_reward_requests'])) {
                $rewardRequest = new RewardRequest();
                $rewardRequest->company_id = $company->id;
                $rewardRequest->sum = $this->withdrawal_amount;
                $rewardRequest->type = $this->type;
                if ($rewardRequest->save()) {
                    return \Yii::$app->mailer->compose([
                        'html' => 'system/affiliate-program/html',
                        'text' => 'system/affiliate-program/text',
                    ], [
                        'subject' => 'Заявка на вывод вознаграждения',
                        'affiliateProgramForm' => $this,
                        'userFIO' => $user->getFio(),
                        'email' => $company->email,
                        'phone' => $company->phone,
                    ])
                        ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
                        ->setTo(\Yii::$app->params['emailList']['support'])
                        ->setSubject('Заявка на вывод вознаграждения')
                        ->send();
                }
            }
            $db->transaction->rollBack();

            return false;
        });
    }
}