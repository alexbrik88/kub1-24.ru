<?php
/**
 * Created by PhpStorm.
 * User: valik
 * Date: 08.02.2016
 * Time: 14:04
 */

namespace frontend\models;


use common\models\Company;
use common\models\company\CompanyTaxationType;
use yii\db\ActiveQuery;
use common\components\date\DateHelper;
use common\models\notification\ForWhom;

/**
 * Class NotificationQuery
 * @package frontend\models
 */
class NotificationQuery extends ActiveQuery
{
    /**
     * @param null $timestamp
     * @return $this
     */
    public function byActivationDate($timestamp = null)
    {
        if ($timestamp === null) {
            $date = date(DateHelper::FORMAT_DATE);
        } else {
            $date = date(DateHelper::FORMAT_DATE, $timestamp);
        }

        return $this->andWhere([
            '<=', 'activation_date', $date,
        ]);
    }

    /**
     * @param null $timestamp
     * @return $this
     */
    public function byEventDate($timestamp = null)
    {
        if ($timestamp === null) {
            $date = date(DateHelper::FORMAT_DATE);
        } else {
            $date = date(DateHelper::FORMAT_DATE, $timestamp);
        }

        return $this->andWhere([
            '>=', 'event_date', date(DateHelper::FORMAT_DATE, $timestamp),
        ]);
    }

    /**
     * @param $notificationType
     * @return $this
     */
    public function byNotificationType($notificationType)
    {
        return $this->andWhere([
            'notification_type' => $notificationType,
        ]);
    }

    /**
     * @param Company $company
     * @return $this
     */
    public function byWhom(Company $company)
    {
        return $this->andWhere([
            'in', 'for_whom', ForWhom::getForWhomByTaxationType($company),
        ]);
    }

}