<?php

namespace frontend\models;

use common\components\pdf\PdfRenderer;
use common\components\TextHelper;
use common\models\cash\CashFlowsBase;
use common\models\Company;
use common\models\Contractor;
use common\models\company\CompanyType;
use common\models\currency\Currency;
use common\models\currency\CurrencyRate;
use common\models\document\Act;
use common\models\document\ForeignCurrencyInvoice;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\document\OrderAct;
use common\models\document\OrderInvoiceFacture;
use common\models\document\OrderPackingList;
use common\models\document\OrderUpd;
use common\models\document\Upd;
use common\models\document\status\InvoiceStatus;
use frontend\models\Documents;
use frontend\modules\documents\assets\DocumentPrintAsset;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Yii;
use yii\base\Model;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\Html;

/**
 * Class CollateForm
 * @package frontend\models
 *
 * @property Company $company
 * @property Contractor $contractor
 */
class CollateForm extends Model
{
    const DOC_INVOICE = 1;
    const DOC_ACT_PACKING_LIST_UPD = 2;
    const DOC_INVOICE_FACTURE_UPD = 3;
    const DOC_UPD = 4;

    const ACCOUNTING_ALL = 1;
    const ACCOUNTING_YES = 2;
    const ACCOUNTING_NO = 3;

    // create params
    public $accounting = 1;
    public $document = 1;
    public $dateFrom;
    public $dateTill;
    public $dateSigned;
    public $fillByContractor = false;

    //send params
    public $sendToChief;
    public $sendToChiefAccountant;
    public $sendToContact;
    public $sendToOther;
    public $chiefEmail;
    public $chiefAccountantEmail;
    public $contactEmail;
    public $otherEmail;

    public $isPrint = false;

    public $view = 'form';

    public static $viewArray = [
        'form',
        'view',
        'send',
        'print',
        'pdf',
        'excel',
    ];

    public static $accountingItems = [
        self::ACCOUNTING_ALL => 'Все',
        self::ACCOUNTING_YES => 'Для учёта в бухгалтерии',
        self::ACCOUNTING_NO => 'Не для учёта в бухгалтерии',
    ];

    protected static $documentItems = [
        self::DOC_INVOICE => 'Счетам',
        self::DOC_ACT_PACKING_LIST_UPD => 'Актам, тов. накладным, УПД',
        self::DOC_INVOICE_FACTURE_UPD => 'Счет-фактурам, УПД',
        //self::DOC_UPD => 'УПД',
    ];

    protected $fieldPrefix;

    private $_company;
    private $_contractor;
    private $_sqlFrom;
    private $_sqlTill;
    private $_initDebit = null;
    private $_initCredit = null;
    private $_periodDebit = null;
    private $_periodCredit = null;
    private $_totalDebit = null;
    private $_totalCredit = null;
    private $_initBalanceByDocs = null;
    private $_initBalanceByFlows = null;
    private $_periodBalanceByDocs = null;
    private $_periodBalanceByFlows = null;
    private $_totalBalanceByDocs = null;
    private $_totalBalanceByFlows = null;
    private $_resultData = null;
    private $_periodItems = null;
    private $_tableRows = null;
    private $_emailText = null;

    /**
     * @param array $config
     */
    public function __construct(Company $company, Contractor $contractor, $config = [])
    {
        $this->_company = $company;
        $this->_contractor = $contractor;
        $this->fieldPrefix = $this->contractor->type == Contractor::TYPE_CUSTOMER ? 'selling' : 'purchase';

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->createCurrensyRateTable();
    }

    private function createCurrensyRateTable()
    {
        $db = \Yii::$app->db;

        $query = (new \yii\db\Query())->select([
            'currency' => '{{t}}.[[name]]',
            'rate' => 'IFNULL({{r}}.[[value]], 1)',
            'count' => 'IFNULL({{r}}.[[amount]], 1)',
        ])->from([
            't' => Currency::tableName(),
        ])->leftJoin([
            'r' => CurrencyRate::tableName(),
        ], '{{r}}.[[name]] = {{t}}.[[name]] AND {{r}}.[[date]] = (
            SELECT MAX({{r2}}.[[date]])
            FROM {{'.CurrencyRate::tableName().'}} AS {{r2}}
            WHERE {{r}}.[[name]] = {{r2}}.[[name]]
            AND {{r2}}.[[date]] <= CURDATE()
        )');
        $sql = $query->createCommand()->rawSql;
        $comand = $db->createCommand("DROP TEMPORARY TABLE IF EXISTS __rate");
        $comand->execute();
        $comand = $db->createCommand("CREATE TEMPORARY TABLE __rate {$sql}");
        $comand->execute();
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => [
                'accounting',
                'document',
                'dateFrom',
                'dateTill',
                'dateSigned',
                'fillByContractor',
            ],
            'send' => [
                'sendToChief',
                'sendToChiefAccountant',
                'sendToContact',
                'sendToOther',
                'chiefEmail',
                'chiefAccountantEmail',
                'contactEmail',
                'otherEmail',
                'emailText',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['accounting', 'document', 'dateFrom', 'dateTill', 'emailText'], 'required'],
            [['accounting'], 'in', 'range' => array_keys(self::$accountingItems)],
            [['document'], 'in', 'range' => $this->allowedDocIdArray],
            [['dateFrom', 'dateTill', 'dateSigned'], 'date', 'format' => 'php:d.m.Y'],
            [['fillByContractor'], 'boolean'],

            //sending rules
            [['emailText'], 'string'],
            [['sendToChief', 'sendToChiefAccountant', 'sendToContact', 'sendToOther'], 'boolean'],
            [
                ['sendToChief'], function ($attribute) {
                if (!$this->sendToChief && !$this->sendToChiefAccountant && !$this->sendToContact && !$this->sendToOther) {
                    $this->addError($attribute, 'Необходимо выбрать хотя бы 1 пункт для отправки.');
                }
            },
            ],
            [
                ['chiefEmail'], 'required',
                'when' => function ($model) {
                    return ($model->sendToChief && empty($model->contractor->director_email));
                },
                'enableClientValidation' => false,
            ],
            [
                ['chiefAccountantEmail'], 'required',
                'when' => function ($model) {
                    return ($model->sendToChiefAccountant && empty($model->contractor->chief_accountant_email));
                },
                'enableClientValidation' => false,
            ],
            [
                ['contactEmail'], 'required',
                'when' => function ($model) {
                    return ($model->sendToContact && empty($model->contractor->contact_email));
                },
                'enableClientValidation' => false,
            ],
            [
                ['otherEmail'], 'required',
                'when' => function ($model) {
                    return (bool)$model->sendToOther;
                },
                'enableClientValidation' => false,
            ],
            [['chiefEmail', 'chiefAccountantEmail', 'otherEmail', 'contactEmail'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'accounting' => 'Включить оплаты',
            'document' => 'Сформировать по',
            'dateFrom' => 'Начало периода',
            'dateTill' => 'Конец периода',
            'dateSigned' => 'Дата подписания акта',
            'fillByContractor' => 'Заполнить по данным контрагента',
            'sendToChief' => 'Руководитель',
            'sendToChiefAccountant' => 'Главный бухгалтер',
            'sendToContact' => 'Контактное лицо',
            'sendToOther' => 'Еще',
            'chiefEmail' => 'Email руководителя',
            'chiefAccountantEmail' => 'Email главного бухгалтера',
            'contactEmail' => 'Email контактного лица',
            'otherEmail' => 'Email',
            'emailText' => 'Текст письма',
        ];
    }

    /**
     * @return array
     */
    public function getDocumentItems()
    {
        if (!$this->company->hasNds()) {
            unset(self::$documentItems[self::DOC_INVOICE_FACTURE_UPD]);
        }

        return self::$documentItems;
    }

    /**
     * @return array
     */
    public function getAllowedDocIdArray()
    {
        $idArray = [
            self::DOC_INVOICE,
            self::DOC_ACT_PACKING_LIST_UPD,
        ];
        if ($this->company->hasNds()) {
            $idArray[] = self::DOC_INVOICE_FACTURE_UPD;
        }
        if (($this->company->isDocsTypeUpd || $this->company->getHasUpd($this->contractor))
        ) {
            $idArray[] = self::DOC_UPD;
        }

        return $idArray;
    }

    /**
     * @return string
     */
    public function getViewPath()
    {
        return "collate/{$this->view}";
    }

    /**
     * @return string
     */
    public function getSqlFrom()
    {
        if (!$this->_sqlFrom) {
            $this->_sqlFrom = date_create_from_format('d.m.Y', $this->dateFrom)->format('Y-m-d');
        }

        return $this->_sqlFrom;
    }

    /**
     * @return string
     */
    public function getSqlTill()
    {
        if (!$this->_sqlTill) {
            $this->_sqlTill = date_create_from_format('d.m.Y', $this->dateTill)->format('Y-m-d');
        }

        return $this->_sqlTill;
    }

    /**
     * @return array
     */
    protected function getCollateData()
    {
        $queryItems = $this->getDocumentItemsQuery($this->contractor->type);
        if ($this->contractor->opposite) {
            $queryItems->union($this->getDocumentItemsQuery($this->contractor->oppositeType), true);
        }
        switch ($this->accounting) {
            case self::ACCOUNTING_ALL:
                $queryItems
                    ->union($this->getBankForeignCurrencyItemsQuery(), true)
                    ->union($this->getBankItemsQuery(), true)
                    ->union($this->getOrderItemsQuery(), true)
                    ->union($this->getEmoneyItemsQuery(), true);
                break;

            case self::ACCOUNTING_YES:
                $queryItems
                    ->union($this->getBankForeignCurrencyItemsQuery(), true)
                    ->union($this->getBankItemsQuery(), true)
                    ->union($this->getOrderItemsQuery()->andWhere(['is_accounting' => CashFlowsBase::ACCOUNTING]), true)
                    ->union($this->getEmoneyItemsQuery()->andWhere(['is_accounting' => CashFlowsBase::ACCOUNTING]), true);
                break;

            case self::ACCOUNTING_NO:
                $queryItems
                    ->union($this->getOrderItemsQuery()->andWhere(['is_accounting' => CashFlowsBase::NON_ACCOUNTING]), true)
                    ->union($this->getEmoneyItemsQuery()->andWhere(['is_accounting' => CashFlowsBase::NON_ACCOUNTING]), true);
                break;
        }

        $query = (new Query)
            ->select([
                'item_date',
                'item_type',
                'item_document',
                'item_number',
                'item_sum_rub',
                'item_sum',
                'item_currency_name',
            ])
            ->from(['t' => $queryItems])
            ->orderBy(['item_date' => SORT_ASC, 'LENGTH(item_number)' => SORT_ASC, 'item_number' => SORT_ASC]);

        return $query->all();
    }

    /**
     * @param $contractorType
     * @return array
     */
    protected function getInitAmountByDocuments($contractorType)
    {
        $debit = 0;
        $credit = 0;
        if ($contractorType == Contractor::TYPE_CUSTOMER) {
            $_taxRateColumn = 'sale_tax_rate_id';
            $_priceColumn = 'selling_price_with_vat';
            $_priceNoVatColumn = 'selling_price_no_vat';
            $_documentType = Documents::IO_TYPE_OUT;
        } else {
            $_taxRateColumn = 'purchase_tax_rate_id';
            $_priceColumn = 'purchase_price_with_vat';
            $_priceNoVatColumn = 'purchase_price_no_vat';
            $_documentType = Documents::IO_TYPE_IN;
        }
        //$selectAmount =  new Expression("ROUND(invoiceOrder.{$_priceColumn} * docOrder.quantity)");
        $selectAmountWithNdsCalc = new Expression("SUM(ROUND(IF(invoice.nds_view_type_id = 1, (1 + tax_rate.rate) * invoiceOrder.{$_priceNoVatColumn}, invoiceOrder.{$_priceColumn}) * docOrder.quantity))");

        $queryArray = [];
        switch ($this->document) {
            case self::DOC_INVOICE:
                $query1 = $this->company->getInvoices()
                    ->byContractorId($this->contractor->id)
                    ->byDeleted(false)
                    ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
                    ->andWhere(['<', 'invoice.document_date', $this->sqlFrom])
                    ->andWhere(['invoice.type' => $_documentType]);
                $query2 = clone $query1;

                // todo: оставить только 1 запрос в завис. от $contractorType ($debit и $credit считается отдельно 2 вызовами метода)

                $query1->byIOType(Documents::IO_TYPE_OUT);
                $query2->byIOType(Documents::IO_TYPE_IN);

                $debit += (int) $query1->sum('invoice.total_amount_with_nds');
                $credit += (int) $query2->sum('invoice.total_amount_with_nds');

                $query1 = $this->company->getForeignCurrencyInvoices()->alias('invoice')
                    ->leftJoin([
                        'r' => '__rate'
                    ], '{{r}}.[[currency]]  = {{invoice}}.[[currency_name]] ')
                    ->byContractorId($this->contractor->id)
                    ->byDeleted(false)
                    ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
                    ->andWhere(['<', 'invoice.document_date', $this->sqlFrom]);
                $query2 = clone $query1;

                $query1->byIOType(Documents::IO_TYPE_OUT);
                $query2->byIOType(Documents::IO_TYPE_IN);

                $debit += (int) $query1->sum('ROUND({{invoice}}.[[total_amount_with_nds]] * IFNULL({{r}}.[[rate]], 1) / IFNULL({{r}}.[[count]], 1))');
                $credit += (int) $query2->sum('ROUND({{invoice}}.[[total_amount_with_nds]] * IFNULL({{r}}.[[rate]], 1) / IFNULL({{r}}.[[count]], 1))');

                return [
                    'debit' => $debit,
                    'credit' => $credit,
                ];
                break;

            case self::DOC_ACT_PACKING_LIST_UPD:
                $queryArray[] = OrderAct::find()->alias('docOrder')
                    ->joinWith(['act document', 'act.invoice invoice'])
                    ->leftJoin(['invoiceOrder' => 'order'], 'invoiceOrder.invoice_id = invoice.id AND invoiceOrder.id = docOrder.order_id')
                    ->leftJoin('tax_rate', "invoiceOrder.{$_taxRateColumn} = tax_rate.id");
                $queryArray[] = OrderPackingList::find()->alias('docOrder')
                    ->joinWith(['packingList document', 'packingList.invoice invoice'])
                    ->leftJoin(['invoiceOrder' => 'order'], 'invoiceOrder.invoice_id = invoice.id AND invoiceOrder.id = docOrder.order_id')
                    ->leftJoin('tax_rate', "invoiceOrder.{$_taxRateColumn} = tax_rate.id");
                $queryArray[] = OrderUpd::find()->alias('docOrder')
                    ->joinWith(['upd document', 'upd.invoice invoice'])
                    ->leftJoin(['invoiceOrder' => 'order'], 'invoiceOrder.invoice_id = invoice.id AND invoiceOrder.id = docOrder.order_id')
                    ->leftJoin('tax_rate', "invoiceOrder.{$_taxRateColumn} = tax_rate.id");
                break;

            case self::DOC_INVOICE_FACTURE_UPD:
                $queryArray[] = OrderInvoiceFacture::find()->alias('docOrder')
                    ->joinWith(['invoiceFacture document', 'invoiceFacture.invoice invoice'])
                    ->leftJoin(['invoiceOrder' => 'order'], 'invoiceOrder.invoice_id = invoice.id AND invoiceOrder.id = docOrder.order_id')
                    ->leftJoin('tax_rate', "invoiceOrder.{$_taxRateColumn} = tax_rate.id");
                $queryArray[] = OrderUpd::find()->alias('docOrder')
                    ->joinWith(['upd document', 'upd.invoice invoice'])
                    ->leftJoin(['invoiceOrder' => 'order'], 'invoiceOrder.invoice_id = invoice.id AND invoiceOrder.id = docOrder.order_id')
                    ->leftJoin('tax_rate', "invoiceOrder.{$_taxRateColumn} = tax_rate.id");
                break;

            case self::DOC_UPD:
                $queryArray[] = OrderUpd::find()->alias('docOrder')
                    ->joinWith(['upd document', 'upd.invoice invoice'])
                    ->leftJoin(['invoiceOrder' => 'order'], 'invoiceOrder.invoice_id = invoice.id AND invoiceOrder.id = docOrder.order_id')
                    ->leftJoin('tax_rate', "invoiceOrder.{$_taxRateColumn} = tax_rate.id");
                break;
        }

        foreach ($queryArray as $query) {

            $subQuery1 = $query->select([
                    'amount' => $selectAmountWithNdsCalc,
                ])->andWhere([
                    'document.type' => $_documentType,
                    'invoice.company_id' => $this->company->id,
                    'invoice.contractor_id' => $this->contractor->id,
                    'invoice.is_deleted' => false,
                ])
                ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
                ->andWhere(['<', 'document.document_date', $this->sqlFrom]);

            // todo: оставить только 1 запрос в завис. от $contractorType ($debit и $credit считается отдельно 2 вызовами метода)

            $subQuery2 = clone $subQuery1;
            $subQuery1->andWhere(['document.type' => Documents::IO_TYPE_OUT]);
            $subQuery2->andWhere(['document.type' => Documents::IO_TYPE_IN]);

            $query1 = (new Query())->select("SUM(amount)")->from($subQuery1);
            $query2 = (new Query())->select("SUM(amount)")->from($subQuery2);

            $debit += (int) $query1->scalar();
            $credit += (int) $query2->scalar();
        }

        return [
            'debit' => $debit,
            'credit' => $credit,
        ];
    }

    /**
     * @return int
     */
    protected function getInitAmountByFlows()
    {
        $debit = 0;
        $credit = 0;

        $bankForeignCurrencyQuery1 = $this->company->getCashBankForeignCurrencyFlows()
        ->alias('flow')
        ->leftJoin([
            'r' => '__rate'
        ], '{{r}}.[[currency]]  = {{flow}}.[[currency_name]] ')
        ->andWhere(['flow.contractor_id' => $this->_contractor->id])
        ->andWhere(['<', 'flow.date', $this->sqlFrom]);

        $bankQuery1 = $this->company->getCashBankFlows()
        ->alias('flow')
        ->andWhere(['flow.contractor_id' => $this->_contractor->id])
        ->andWhere(['<', 'flow.date', $this->sqlFrom]);

        $orderQuery1 = $this->_company->getCashOrderFlows()
        ->alias('flow')
        ->andWhere(['flow.contractor_id' => $this->_contractor->id])
        ->andWhere(['<', 'flow.date', $this->sqlFrom]);

        $emoneyQuery1 = $this->_company->getCashEmoneyFlows()
        ->alias('flow')
        ->andWhere(['flow.contractor_id' => $this->_contractor->id])
        ->andWhere(['<', 'flow.date', $this->sqlFrom]);

        $bankForeignCurrencyQuery2 = clone $bankForeignCurrencyQuery1;
        $bankQuery2 = clone $bankQuery1;
        $orderQuery2 = clone $orderQuery1;
        $emoneyQuery2 = clone $emoneyQuery1;

        $bankForeignCurrencyQuery1->byFlowType(CashFlowsBase::FLOW_TYPE_EXPENSE);
        $bankQuery1->byFlowType(CashFlowsBase::FLOW_TYPE_EXPENSE);
        $orderQuery1->byFlowType(CashFlowsBase::FLOW_TYPE_EXPENSE);
        $emoneyQuery1->byFlowType(CashFlowsBase::FLOW_TYPE_EXPENSE);

        $bankForeignCurrencyQuery2->byFlowType(CashFlowsBase::FLOW_TYPE_INCOME);
        $bankQuery2->byFlowType(CashFlowsBase::FLOW_TYPE_INCOME);
        $orderQuery2->byFlowType(CashFlowsBase::FLOW_TYPE_INCOME);
        $emoneyQuery2->byFlowType(CashFlowsBase::FLOW_TYPE_INCOME);

        switch ($this->accounting) {
            case self::ACCOUNTING_ALL:
                $debit = $bankQuery1->sum('amount')
                    + $bankForeignCurrencyQuery1->sum('ROUND({{flow}}.[[amount]] * IFNULL({{r}}.[[rate]], 1) / IFNULL({{r}}.[[count]], 1))')
                    + $orderQuery1->sum('amount')
                    + $emoneyQuery1->sum('amount');
                $credit = $bankQuery2->sum('amount')
                    + $bankForeignCurrencyQuery2->sum('ROUND({{flow}}.[[amount]] * IFNULL({{r}}.[[rate]], 1) / IFNULL({{r}}.[[count]], 1))')
                    + $orderQuery2->sum('amount')
                    + $emoneyQuery2->sum('amount');
                break;

            case self::ACCOUNTING_YES:
                $debit = $bankQuery1->sum('amount')
                    + $bankForeignCurrencyQuery1->sum('ROUND({{flow}}.[[amount]] * IFNULL({{r}}.[[rate]], 1) / IFNULL({{r}}.[[count]], 1))')
                    + $orderQuery1->andWhere(['is_accounting' => CashFlowsBase::ACCOUNTING])->sum('amount')
                    + $emoneyQuery1->andWhere(['is_accounting' => CashFlowsBase::ACCOUNTING])->sum('amount');
                $credit = $bankQuery2->sum('amount')
                    + $bankForeignCurrencyQuery2->sum('ROUND({{flow}}.[[amount]] * IFNULL({{r}}.[[rate]], 1) / IFNULL({{r}}.[[count]], 1))')
                    + $orderQuery2->andWhere(['is_accounting' => CashFlowsBase::ACCOUNTING])->sum('amount')
                    + $emoneyQuery2->andWhere(['is_accounting' => CashFlowsBase::ACCOUNTING])->sum('amount');
                break;

            case self::ACCOUNTING_NO:
                $debit = $orderQuery1->andWhere(['is_accounting' => CashFlowsBase::NON_ACCOUNTING])->sum('amount')
                    + $emoneyQuery1->andWhere(['is_accounting' => CashFlowsBase::NON_ACCOUNTING])->sum('amount');
                $credit = $orderQuery2->andWhere(['is_accounting' => CashFlowsBase::NON_ACCOUNTING])->sum('amount')
                    + $emoneyQuery2->andWhere(['is_accounting' => CashFlowsBase::NON_ACCOUNTING])->sum('amount');
                break;
        }

        return [
            'debit' => $debit,
            'credit' => $credit,
        ];
    }

    /**
     * @param $contractorType
     * @return mixed
     */
    protected function getDocumentItemsQuery($contractorType)
    {
        $invoiceTable = Invoice::tableName();
        $documentTable = 'document';
        if ($contractorType == Contractor::TYPE_CUSTOMER) {
            $_taxRateColumn = 'sale_tax_rate_id';
            $_priceColumn = 'selling_price_with_vat';
            $_priceNoVatColumn = 'selling_price_no_vat';
            $_documentType = Documents::IO_TYPE_OUT;
        } else {
            $_taxRateColumn = 'purchase_tax_rate_id';
            $_priceColumn = 'purchase_price_with_vat';
            $_priceNoVatColumn = 'purchase_price_no_vat';
            $_documentType = Documents::IO_TYPE_IN;
        }
        // $selectAmount =  new Expression("ROUND(invoiceOrder.{$_priceColumn} * docOrder.quantity)");
        $selectAmountWithNdsCalc = new Expression("SUM(ROUND(IF(invoice.nds_view_type_id = 1, (1 + tax_rate.rate) * invoiceOrder.{$_priceNoVatColumn}, invoiceOrder.{$_priceColumn}) * docOrder.quantity))");
        
        $queryDataArray = [];
        switch ($this->document) {
            case self::DOC_INVOICE:
                $documentTable = $invoiceTable;
                $queryDataArray[] = [
                    'query' => Invoice::find(),
                    'itemType' => 'Счет',
                    'itemDoc' => 'Счет',
                    'amount' => "$invoiceTable.total_amount_with_nds",
                ];
                $queryDataArray[] = [
                    'query' => ForeignCurrencyInvoice::find()->alias($invoiceTable)->leftJoin([
                        'r' => '__rate'
                    ], '({{r}}.[[currency]] ) = ({{'.$invoiceTable.'}}.[[currency_name]] )'),
                    'itemType' => 'Инвойс',
                    'itemDoc' => 'Инвойс',
                    'amount' => 'ROUND({{'.$invoiceTable.'}}.[[total_amount_with_nds]] * IFNULL({{r}}.[[rate]], 1) / IFNULL({{r}}.[[count]], 1))',
                    'curr_amount' => "$invoiceTable.total_amount_with_nds",
                    'curr_name' => "$invoiceTable.currency_name",
                ];
                break;

            case self::DOC_ACT_PACKING_LIST_UPD:
                $queryDataArray[] = [
                    'query' => Act::find()->alias($documentTable)->joinWith('invoice')->groupBy($documentTable.'.id'),
                    'itemType' => $this->contractor->type == Contractor::TYPE_CUSTOMER ? 'Продажа' : 'Покупка',
                    'itemDoc' => 'Акт',
                    'amount' => OrderAct::find()->alias('docOrder')
                        ->joinWith('order invoiceOrder')
                        ->leftJoin('tax_rate', "invoiceOrder.{$_taxRateColumn} = tax_rate.id")
                        ->select($selectAmountWithNdsCalc)
                        ->where("{{docOrder}}.[[act_id]] = {{{$documentTable}}}.[[id]]"),
                ];
                $queryDataArray[] = [
                    'query' => PackingList::find()->alias($documentTable)->joinWith('invoice')->groupBy($documentTable.'.id'),
                    'itemType' => $this->contractor->type == Contractor::TYPE_CUSTOMER ? 'Продажа' : 'Покупка',
                    'itemDoc' => 'Товарная накладная',
                    'amount' => OrderPackingList::find()->alias('docOrder')
                        ->joinWith('order invoiceOrder')
                        ->leftJoin('tax_rate', "invoiceOrder.{$_taxRateColumn} = tax_rate.id")
                        ->select($selectAmountWithNdsCalc)
                        ->where("{{docOrder}}.[[packing_list_id]] = {{{$documentTable}}}.[[id]]"),
                ];
                $queryDataArray[] = [
                    'query' => Upd::find()->alias($documentTable)->joinWith('invoice')->groupBy($documentTable.'.id'),
                    'itemDoc' => 'УПД',
                    'amount' => OrderUpd::find()->alias('docOrder')
                        ->joinWith('order invoiceOrder')
                        ->leftJoin('tax_rate', "invoiceOrder.{$_taxRateColumn} = tax_rate.id")
                        ->select($selectAmountWithNdsCalc)
                        ->where("{{docOrder}}.[[upd_id]] = {{{$documentTable}}}.[[id]]"),
                ];
                break;

            case self::DOC_INVOICE_FACTURE_UPD:
                $queryDataArray[] = [
                    'query' => InvoiceFacture::find()->alias($documentTable)->joinWith('invoice')->groupBy($documentTable.'.id'),
                    'itemDoc' => 'Счет-фактура',
                    'amount' => OrderInvoiceFacture::find()->alias('docOrder')
                        ->joinWith('order invoiceOrder')
                        ->leftJoin('tax_rate', "invoiceOrder.{$_taxRateColumn} = tax_rate.id")
                        ->select($selectAmountWithNdsCalc)
                        ->where("{{docOrder}}.[[invoice_facture_id]] = {{{$documentTable}}}.[[id]]"),
                ];
                $queryDataArray[] = [
                    'query' => Upd::find()->alias($documentTable)->joinWith('invoice')->groupBy($documentTable.'.id'),
                    'itemDoc' => 'УПД',
                    'amount' => OrderUpd::find()->alias('docOrder')
                        ->joinWith('order invoiceOrder')
                        ->leftJoin('tax_rate', "invoiceOrder.{$_taxRateColumn} = tax_rate.id")
                        ->select($selectAmountWithNdsCalc)
                        ->where("{{docOrder}}.[[upd_id]] = {{{$documentTable}}}.[[id]]"),
                ];
                break;

            case self::DOC_UPD:
                $queryDataArray[] = [
                    'query' => Upd::find()->alias($documentTable)->joinWith('invoice')->groupBy($documentTable.'.id'),
                    'itemDoc' => 'УПД',
                    'amount' => OrderUpd::find()->alias('docOrder')
                        ->joinWith('order invoiceOrder')
                        ->leftJoin('tax_rate', "invoiceOrder.{$_taxRateColumn} = tax_rate.id")
                        ->select($selectAmountWithNdsCalc)
                        ->where("{{docOrder}}.[[upd_id]] = {{{$documentTable}}}.[[id]]"),
                ];
                break;
        }

        $queryArray = [];
        foreach ($queryDataArray as $key => $queryData) {
            if ($queryData['itemDoc'] == 'Счет' || $queryData['itemDoc'] == 'Инвойс') {
                $itemNumber = "IF({{{$documentTable}}}.[[is_additional_number_before]] = true,
                    CONCAT(
                        IFNULL({{{$documentTable}}}.[[document_additional_number]], ''),
                        {{{$documentTable}}}.[[document_number]]
                    ),
                    CONCAT(
                        {{{$documentTable}}}.[[document_number]],
                        IFNULL({{{$documentTable}}}.[[document_additional_number]], '')
                    )
                )";
            } else {
                $itemNumber = "CONCAT(
                    {{{$documentTable}}}.[[document_number]],
                    IFNULL({{{$documentTable}}}.[[document_additional_number]], '')
                )";
            }
            $queryArray[] = $queryData['query']->select([
                'item_date' => "{$documentTable}.document_date",
                'item_type' => "IF({{{$documentTable}}}.[[type]]=2, 'debit', 'credit')",
                //'item_document' => new Expression("\"{$queryData['itemDoc']}\""),
                'item_document' => "IF({{{$documentTable}}}.[[type]]=2, 'Продажа', 'Покупка')",
                'item_number' => new Expression($itemNumber),
                'item_sum_rub' => $queryData['amount'],
                'item_sum' => $queryData['curr_amount'] ?? $queryData['amount'],
                'item_currency_name' => $queryData['curr_name'] ?? new Expression('"'.Currency::DEFAULT_NAME.'"'),
            ])
            ->andWhere([
                "{$invoiceTable}.company_id" => $this->_company->id,
                "{$invoiceTable}.contractor_id" => $this->_contractor->id,
                "{$invoiceTable}.is_deleted" => false,
                "{$invoiceTable}.type" => $_documentType,
            ])
            ->andWhere(['not', ["{$invoiceTable}.invoice_status_id" => InvoiceStatus::STATUS_REJECTED]])
            ->andWhere(['between', "{$documentTable}.document_date", $this->sqlFrom, $this->sqlTill]);
        }

        $query = array_shift($queryArray);
        foreach ($queryArray as $queryItem) {
            $query->union($queryItem, true);
        }

        return $query;
    }

    /**
     * @return \yii\db\Query
     */
    protected function getBankForeignCurrencyItemsQuery()
    {
        return $this->_company->getCashBankForeignCurrencyFlows()->alias('flow')
            ->select([
                'item_date' => 'flow.date',
                'item_type' => "IF({{flow}}.[[flow_type]]=0, 'debit', 'credit')",
                'item_document' => new Expression('"Оплата"'),
                'item_number' => '({{flow}}.[[payment_order_number]] )',
                'item_sum_rub' => 'ROUND({{flow}}.[[amount]] * IFNULL({{r}}.[[rate]], 1) / IFNULL({{r}}.[[count]], 1))',
                'item_sum' => 'flow.amount',
                'item_currency_name' => '({{flow}}.[[currency_name]] )',
            ])
            ->leftJoin([
                'r' => '__rate'
            ], '{{r}}.[[currency]]  = {{flow}}.[[currency_name]] ')
            ->andWhere(['between', 'flow.date', $this->sqlFrom, $this->sqlTill])
            ->andWhere(['flow.contractor_id' => $this->_contractor->id]);
    }

    /**
     * @return \yii\db\Query
     */
    protected function getBankItemsQuery()
    {
        return $this->_company->getCashBankFlows()
            ->select([
                'item_date' => 'cash_bank_flows.date',
                'item_type' => "IF({{cash_bank_flows}}.[[flow_type]]=0, 'debit', 'credit')",
                'item_document' => new Expression('"Оплата"'),
                'item_number' => '({{cash_bank_flows}}.[[payment_order_number]] )',
                'item_sum_rub' => 'cash_bank_flows.amount',
                'item_sum' => 'cash_bank_flows.amount',
                'item_currency_name' => new Expression('"'.Currency::DEFAULT_NAME.'"'),
            ])
            ->andWhere(['between', 'cash_bank_flows.date', $this->sqlFrom, $this->sqlTill])
            ->andWhere(['cash_bank_flows.contractor_id' => $this->_contractor->id]);
    }

    /**
     * @return \yii\db\Query
     */
    protected function getOrderItemsQuery()
    {
        return $this->_company->getCashOrderFlows()
            ->select([
                'item_date' => 'cash_order_flows.date',
                'item_type' => "IF({{cash_order_flows}}.[[flow_type]]=0, 'debit', 'credit')",
                'item_document' => new Expression('"Оплата"'),
                'item_number' => new Expression('NULL'),
                'item_sum_rub' => 'cash_order_flows.amount',
                'item_sum' => 'cash_order_flows.amount',
                'item_currency_name' => new Expression('"'.Currency::DEFAULT_NAME.'"'),
            ])
            ->andWhere(['between', 'cash_order_flows.date', $this->sqlFrom, $this->sqlTill])
            ->andWhere(['cash_order_flows.contractor_id' => $this->_contractor->id]);
    }

    /**
     * @return \yii\db\Query
     */
    protected function getEmoneyItemsQuery()
    {
        return $this->_company->getCashEmoneyFlows()
            ->select([
                'item_date' => 'cash_emoney_flows.date',
                'item_type' => "IF({{cash_emoney_flows}}.[[flow_type]]=0, 'debit', 'credit')",
                'item_document' => new Expression('"Оплата"'),
                'item_number' => new Expression('NULL'),
                'item_sum_rub' => 'cash_emoney_flows.amount',
                'item_sum' => 'cash_emoney_flows.amount',
                'item_currency_name' => new Expression('"'.Currency::DEFAULT_NAME.'"'),
            ])
            ->andWhere(['between', 'cash_emoney_flows.date', $this->sqlFrom, $this->sqlTill])
            ->andWhere(['cash_emoney_flows.contractor_id' => $this->_contractor->id]);
    }

    /**
     * @return \yii\db\Query
     */
    public function dataProcessing()
    {
        $initByDocuments = $this->getInitAmountByDocuments($this->contractor->type);
        $oppositeInitByDocuments = ($this->contractor->opposite)
            ? $this->getInitAmountByDocuments($this->contractor->oppositeType)
            : ['debit' => 0, 'credit' => 0];
        $initByFlows = $this->getInitAmountByFlows();
        $this->_initDebit = ($initByDocuments['debit'] + $oppositeInitByDocuments['debit']) + $initByFlows['debit'];
        $this->_initCredit = ($initByDocuments['credit'] + $oppositeInitByDocuments['credit']) + $initByFlows['credit'];
        $this->_periodDebit = 0;
        $this->_periodCredit = 0;
        $this->_resultData = $this->getCollateData();
        if ($this->_initDebit > $this->_initCredit) {
            $this->_initDebit -= $this->_initCredit;
            $this->_initCredit = 0;
        } elseif ($this->_initDebit < $this->_initCredit) {
            $this->_initCredit -= $this->_initDebit;
            $this->_initDebit = 0;
        } else {
            $this->_initDebit = $this->_initCredit = 0;
        }

        $this->_periodItems = [];
        $this->_tableRows = '';
        $totalRowLines = 0;
        $noBreakRows = 7;
        $firstLineRowsCount = 18;
        $secondLineRowsCount = 32;
        $secondLineRowsWithFooterCount = 15;
        $totalRowsCount = count($this->_resultData);
        foreach ($this->_resultData as $key => $data) {
            $debit = $data['item_type'] == 'debit' ? (int)$data['item_sum_rub'] : 0;
            $credit = $data['item_type'] == 'credit' ? (int)$data['item_sum_rub'] : 0;
            $this->_periodDebit += $debit;
            $this->_periodCredit += $credit;
            $itemDate = date_create_from_format('Y-m-d', $data['item_date'])->format('d.m.Y');
            $itemNuber = $data['item_number'] ? "№{$data['item_number']} " : '';
            $docLabel = "{$data['item_document']} ({$itemNuber}от {$itemDate})";
            if ($data['item_currency_name'] != Currency::DEFAULT_NAME) {
                $currAmount = (int)$data['item_sum'];
                $currAmount = number_format($currAmount / 100, 2, ',', ' ');
                $docLabel .= " ({$currAmount} {$data['item_currency_name']})";
            }
            $itemValues = [
                0 => $itemDate,
                1 => $docLabel,
                2 => ($debit ? number_format($debit / 100, 2, ',', ' ') : null),
                3 => ($credit ? number_format($credit / 100, 2, ',', ' ') : null),
            ];
            if ($this->isPrint) {
                $name = $contactorFlowName = implode('<br/>', mb_str_split($itemValues[1], 30));
            } else {
                $name = $contactorFlowName = $itemValues[1];
            }
            if (mb_strpos($name, 'Продажа') !== false) {
                $contactorFlowName = str_replace('Продажа', 'Покупка', $contactorFlowName);
            } elseif (mb_strpos($name, 'Покупка') !== false) {
                $contactorFlowName = str_replace('Покупка', 'Продажа', $contactorFlowName);
            }

            $totalRowLines += count(explode('<br/>', $name));
            $cells = Html::tag('td', $itemValues[0], ['style' => 'white-space: nowrap;']) . "\n";
            $cells .= Html::tag('td', $name) . "\n";
            $cells .= Html::tag('td', $itemValues[2], ['style' => 'text-align: right;']) . "\n";
            $cells .= Html::tag('td', $itemValues[3], ['style' => 'text-align: right;']) . "\n";
            $cells .= Html::tag('td', $this->fillByContractor ? $itemValues[0] : str_repeat('&nbsp;', 20)) . "\n";
            $cells .= Html::tag('td', $this->fillByContractor ? $contactorFlowName : null) . "\n";
            if ($this->fillByContractor) {
                $contractorDebit = $itemValues[3];
                $contractorCredit = $itemValues[2];
            } else {
                $contractorDebit = null;
                $contractorCredit = null;
            }

            $cells .= Html::tag('td', $contractorDebit, ['style' => 'text-align: right;']) . "\n";
            $cells .= Html::tag('td', $contractorCredit, ['style' => 'text-align: right;']) . "\n";

            $this->_periodItems[] = $itemValues;
            if ($totalRowsCount == $key + 1) {
                if ($this->isPrint && $totalRowLines > $noBreakRows) {
                    $rowsLinesWithoutFirstPage = $totalRowLines - $firstLineRowsCount;
                    if ($rowsLinesWithoutFirstPage < 0 || $rowsLinesWithoutFirstPage % $secondLineRowsCount > $secondLineRowsWithFooterCount) {
                        $this->_tableRows .= '</table><pagebreak style="display:block!important; page-break-before: always;" />';
                        $this->_tableRows .= '<table id="collate-table" class="collate-table" cellspacing="0" style="width: 100%">
                            <thead>
                            <tr>
                                <td colspan="4">
                                    По данным ' . $this->company->getTitle(true) . ', руб.
                                </td>
                                <td colspan="4">
                                    По данным ' . $this->contractor->getShortName(true) . ', руб.
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center" style="width: 5%;">Дата</td>
                                <td class="text-center" style="width: 20%;">Документ</td>
                                <td class="text-center" style="width: 10%;">Дебет</td>
                                <td class="text-center" style="width: 10%;">Кредит</td>
                                <td class="text-center" style="width: 5%;">Дата</td>
                                <td class="text-center" style="width: 20%;">Документ</td>
                                <td class="text-center" style="width: 10%;">Дебет</td>
                                <td class="text-center" style="width: 10%;">Кредит</td>
                            </tr>
                            </thead>';
                        $this->_tableRows .= Html::tag('tr', $cells) . "\n";
                        continue;
                    }
                }
            }

            $this->_tableRows .= Html::tag('tr', $cells) . "\n";
        }

        $this->_totalDebit = $this->_initDebit + $this->_periodDebit;
        $this->_totalCredit = $this->_initCredit + $this->_periodCredit;
        if ($this->_totalDebit > $this->_totalCredit) {
            $this->_totalDebit -= $this->_totalCredit;
            $this->_totalCredit = 0;
        } elseif ($this->_totalDebit < $this->_totalCredit) {
            $this->_totalCredit -= $this->_totalDebit;
            $this->_totalDebit = 0;
        } else {
            $this->_totalDebit = $this->_totalCredit = 0;
        }

        if ($this->_emailText === null) {
            $sender = $this->company->getChiefFio(true) . "\r\n";
            if ($this->company->company_type_id != CompanyType::TYPE_IP) {
                $sender .= $this->company->chief_post_name . "\r\n";
            }

            $this->_emailText = <<<EOT
Здравствуйте!

Акт сверки и взаимных расчетов за период {$this->dateFrom} - {$this->dateTill}
между {$this->company->getTitle(true)} и {$this->contractor->getShortName()}
во вложении

С уважением,
{$sender}
EOT;
        }
        \common\models\company\CompanyFirstEvent::checkEvent($this->_company, 51);

        return $this;
    }

    /**
     * Get summary parameters of the $this->dataProcessing() Begin
     */
    public function getPeriodDebit()
    {
        if ($this->_periodDebit === null) {
            $this->dataProcessing();
        }

        return $this->_periodDebit; // interger
    }

    public function getPeriodCredit()
    {
        if ($this->_periodCredit === null) {
            $this->dataProcessing();
        }

        return $this->_periodCredit; // interger
    }

    public function getInitDebit()
    {
        if ($this->_initDebit === null) {
            $this->dataProcessing();
        }

        return $this->_initDebit; // interger
    }

    public function getInitCredit()
    {
        if ($this->_initCredit === null) {
            $this->dataProcessing();
        }

        return $this->_initCredit; // interger
    }

    public function getTotalDebit()
    {
        if ($this->_totalDebit === null) {
            $this->dataProcessing();
        }

        return $this->_totalDebit; // interger
    }

    public function getTotalCredit()
    {
        if ($this->_totalCredit === null) {
            $this->dataProcessing();
        }

        return $this->_totalCredit; // interger
    }

    public function getResultData()
    {
        if ($this->_resultData === null) {
            $this->dataProcessing();
        }

        return $this->_resultData; // array
    }

    public function getPeriodItems()
    {
        if ($this->_periodItems === null) {
            $this->dataProcessing();
        }

        return $this->_periodItems; // string (HTML)
    }

    public function getTableRows()
    {
        if ($this->_tableRows === null) {
            $this->dataProcessing();
        }

        return $this->_tableRows; // string (HTML)
    }
    /* Get summary parameters of the $this->dataProcessing() End */

    /**
     * @return string
     */
    public function setEmailText($text)
    {
        $this->_emailText = $text;
    }

    /**
     * @return string
     */
    public function getEmailText()
    {
        if ($this->_emailText === null) {
            $this->dataProcessing();
        }

        return $this->_emailText;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @return Contractor
     */
    public function getContractor()
    {
        return $this->_contractor;
    }

    /**
     * @return Employye
     */
    public function getSender()
    {
        //if (\Yii::$app->id == 'app-frontend') {
        //    return \Yii::$app->user->identity->currentEmployeeCompany;
        //} else {
        return $this->company->chiefEmployeeCompany;
        //}
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return 'Акт сверки ' . $this->dateFrom . ' - ' . $this->dateTill . ' ' .
            $this->company->getTitle(true) . ' - ' . $this->contractor->getShortName();
    }

    /**
     * @return string
     */
    public function getUrlTitle()
    {
        return htmlspecialchars(strtr($this->title, [' ' => '_', '"' => '']));
    }

    /**
     * @return string
     */
    public function getPdfTitle()
    {
        return strtr($this->title, ['"' => '']);
    }

    /**
     * @return string
     */
    public function getPdfFileName()
    {
        return strtr($this->pdfTitle, [' ' => '_']) . '.pdf';
    }

    /**
     * Sending emails
     *
     * @return integer
     */
    public function send()
    {
        $sentCount = 0;
        $emailArray = $this->getEmailList();
        $totalCount = count($emailArray);
        $subject = "Акт сверки {$this->company->getTitle(true)}";

        $params = [
            'model' => $this,
            'subject' => $subject,
            'pixel' => [
                'company_id' => $this->company->id,
                'email' => implode(',', $emailArray),
            ],
        ];
        // render pdf content
        $renderer = $this->renderer;
        $content = $renderer->output(false);

        \Yii::$app->mailer->htmlLayout = 'layouts/document-html';
        $message = \Yii::$app->mailer->compose([
            'html' => 'system/documents/collate/html',
            'text' => 'system/documents/collate/text',
        ], $params)
            ->setFrom([\Yii::$app->params['emailList']['docs'] => $this->company->getChiefFio()]);
        if ($this->company->email) {
            $message->setReplyTo([$this->company->email => $this->company->getChiefFio(true)]);
        }
        $message->setSubject($subject)
            ->attachContent($content, [
                'fileName' => $this->pdfFileName,
                'contentType' => 'application/pdf',
            ]);

        foreach ($emailArray as $email) {
            if ($message->setTo($email)->send()) {
                $sentCount++;
            }
        }

        return $sentCount;
    }

    /**
     * @return PdfRenderer
     */
    public function getExcelWriter($type = 'Xlsx')
    {
        $initDebit = ($val = $this->initDebit) ? number_format($val / 100, 2, ',', '') : '';
        $initCredit = ($val = $this->initCredit) ? number_format($val / 100, 2, ',', '') : '';
        $periodDebit = ($val = $this->periodDebit) ? number_format($val / 100, 2, ',', '') : '';
        $periodCredit = ($val = $this->periodCredit) ? number_format($val / 100, 2, ',', '') : '';
        $totalDebit = ($val = $this->totalDebit) ? number_format($val / 100, 2, ',', '') : '';
        $totalCredit = ($val = $this->totalCredit) ? number_format($val / 100, 2, ',', '') : '';

        $agreement = null;
        $agreementArray = $this->contractor->getAgreements()->all();
        if (count($agreementArray) == 1) {
            $agreement = reset($agreementArray);
        }

        $description = "взаимных расчетов за период: {$this->dateFrom} - {$this->dateTill}\n между {$this->company->getTitle(true)} (ИНН {$this->company->inn})\n и {$this->contractor->getShortName(true)} (ИНН {$this->contractor->ITN})";
        if ($agreement) {
            $description .= "по {$agreement->agreementType->name_dative} № {$agreement->agreementType->name_dative}";
            if ($d = date_create_from_format('Y-m-d', $agreement->document_date)) {
                $description .= "от {$d->format('d.m.Y')}";
            }
        }

        $companyChiefFio = $this->company->company_type_id !== CompanyType::TYPE_IP ? " {$this->company->getChiefFio()}" : null;
        $contractorChiefFio = $this->contractor->company_type_id !== CompanyType::TYPE_IP ? " {$this->contractor->director_name}" : null;
        $headerText = "Мы, нижеподписавшиеся, {$this->company->chief_post_name} {$this->company->getTitle(true)}{$companyChiefFio}, с одной стороны, ";
        $headerText .= "и {$this->contractor->director_post_name} {$this->contractor->getShortName(true)}{$contractorChiefFio},";
        $headerText .= ", с другой стороны, составили настоящий акт сверки в том, что состояние взаимных расчетов по данным учета следующее:";

        $dataArray = [
            ['Акт сверки'],
            [$description],
            [$headerText],
            [
                "По данным {$this->company->getTitle(true)}",
                null, null, null,
                "По данным {$this->contractor->getShortName(true)}",
                null, null, null,
            ],
            ['Дата', 'Документ', 'Дебет', 'Кредит', 'Дата', 'Документ', 'Дебет', 'Кредит'],
            [
                'Сальдо начальное',
                null,
                $initDebit,
                $initCredit,
                'Сальдо начальное',
                null,
                $this->fillByContractor ? $initCredit : null,
                $this->fillByContractor ? $initDebit : null,
            ],
        ];
        if ($this->periodItems) {
            foreach ($this->periodItems as $key => $item) {
                if ($this->contractor->type === Contractor::TYPE_CUSTOMER) {
                    $contactorFlowName = str_replace('Продажа', 'Покупка', $item[1]);
                } else {
                    $contactorFlowName = str_replace('Покупка', 'Продажа', $item[1]);
                }

                $item[2] = str_replace(' ', '', $item[2]);
                $item[3] = str_replace(' ', '', $item[3]);
                $item[4] = $item[0];
                $item[5] = $contactorFlowName;
                if ($this->fillByContractor) {
                    $item[6] = $item[3];
                    $item[7] = $item[2];
                } else {
                    $item[6] = null;
                    $item[7] = null;
                }

                $dataArray[] = $item;
            }
        } else {
            $key = null;
            $dataArray[] = ['За указанный период движений не было', null, null, null, null, null, null, null];
        }
        $dataArray[] = [
            'Обороты за период',
            null,
            $periodDebit,
            $periodCredit,
            'Обороты за период',
            null,
            $this->fillByContractor ? $periodCredit : null,
            $this->fillByContractor ? $periodDebit : null,
        ];
        $dataArray[] = [
            'Сальдо конечное',
            null,
            $totalDebit,
            $totalCredit,
            'Сальдо конечное',
            null,
            $this->fillByContractor ? $totalCredit : null,
            $this->fillByContractor ? $totalDebit : null,
        ];
        $dataArray[] = [null, null, null, null, null, null, null, null];
        $dataArray[] = [
            "По данным {$this->company->getTitle(true)} и {$this->contractor->getShortName(true)}",
            null,
            null,
            null,
            "По данным {$this->company->getTitle(true)} и {$this->contractor->getShortName(true)}",
            null,
            null,
            null,
        ];
        $debt = "на {$this->dateTill}";
        if ($this->totalDebit || $this->totalCredit) {
            $debt .= ' задолженность в пользу ';
        }

        if ($this->totalDebit) {
            $debt .= ($this->company->getTitle(true) . ' ' . number_format($this->totalDebit / 100, 2, ',', ' ') . " руб.\n"
            . '(' . TextHelper::amountToWords($this->totalDebit / 100) . ')');
        } elseif ($this->totalCredit) {
            $debt .= ($this->contractor->getShortName(true) . ' ' . number_format($this->totalCredit / 100, 2, ',', ' ') . " руб.\n"
                . '(' . TextHelper::amountToWords($this->totalCredit / 100) . ')');
        } else {
            $debt .= ' задолженности нет';
        }

        $contractorDebt = null;
        if ($this->fillByContractor) {
            $contractorDebt = $debt;
        } else {
            $contractorDebt = "на {$this->dateTill}";
            if ($this->totalDebit || $this->totalCredit) {
                $contractorDebt .= ' задолженность в пользу ';
            }
        }

        $dataArray[] = [$debt, null, null, null, $contractorDebt, null, null, null];
        $dataArray[] = [null, null, null, null, null, null, null, null];
        $dataArray[] = [null, null, null, null, null, null, null, null];
        $dataArray[] = ["От {$this->company->getTitle(true)}", null, null, null, "От {$this->contractor->getShortName(true)}", null, null, null];
        $dataArray[] = [null, null, null, null, null, null, null, null];
        $dataArray[] = [$this->company->chief_post_name, null, null, null, $this->contractor->director_post_name, null, null, null];
        $dataArray[] = [null, null, null, null, null, null, null, null];
        $dataArray[] = [null, null, null, null, null, null, null, null];
        $dataArray[] = [
            (($name = $this->company->getChiefFio(true)) ? '(' . $name . ')' : ''),
            null,
            null,
            null,
            '(' . ($this->contractor->company_type_id !== CompanyType::TYPE_IP ? $this->contractor->getDirectorFio() : $this->contractor->getShortName(true)) . ')',
            null,
            null,
            null,
        ];
        $dataArray[] = [
            'М.П.',
            null,
            null,
            ($this->dateSigned ? $this->dateSigned . 'г.' : ''),
            'М.П.',
            null,
            null,
            ($this->dateSigned ? $this->dateSigned . 'г.' : ''),
        ];

        $objPHPExcel = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(18);
        $objPHPExcel->getActiveSheet()->fromArray($dataArray);

        $objPHPExcel->getActiveSheet()->mergeCells("A1:H1");
        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true)->setSize(20);
        $objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(36);

        $objPHPExcel->getActiveSheet()->mergeCells("A2:H2");
        $objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(70);
        $objPHPExcel->getActiveSheet()->getStyle('A1:H2')->applyFromArray([
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ]);

        $objPHPExcel->getActiveSheet()->mergeCells("A3:H3");
        $objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(55);
        $objPHPExcel->getActiveSheet()->getStyle('A3:H3')->applyFromArray([
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_LEFT,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
        ]);
        $objPHPExcel->getActiveSheet()->getStyle('A3:H3')->getAlignment()->setWrapText(true);

        $objPHPExcel->getActiveSheet()->mergeCells("A4:D4");
        $objPHPExcel->getActiveSheet()->mergeCells("E4:H4");
        $objPHPExcel->getActiveSheet()->mergeCells("A6:B6");
        $objPHPExcel->getActiveSheet()->mergeCells("E6:F6");
        if ($key === null) {
            $objPHPExcel->getActiveSheet()->mergeCells("A6:D6");
        }
        $i = $key + 8;
        $objPHPExcel->getActiveSheet()->getStyle('A6:H6')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A{$i}:B{$i}");
        $objPHPExcel->getActiveSheet()->mergeCells("E{$i}:F{$i}");
        $objPHPExcel->getActiveSheet()->getStyle("A{$i}:H{$i}")->getFont()->setBold(true);
        $i++;
        $objPHPExcel->getActiveSheet()->mergeCells("A{$i}:B{$i}");
        $objPHPExcel->getActiveSheet()->mergeCells("E{$i}:F{$i}");
        $objPHPExcel->getActiveSheet()->getStyle("A{$i}:H{$i}")->getFont()->setBold(true);
        $i = $i + 2;
        $objPHPExcel->getActiveSheet()->mergeCells("A{$i}:D{$i}");
        $objPHPExcel->getActiveSheet()->mergeCells("E{$i}:H{$i}");
        $i++;
        $objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(49);
        $objPHPExcel->getActiveSheet()->mergeCells("A{$i}:D{$i}");
        $objPHPExcel->getActiveSheet()->mergeCells("E{$i}:H{$i}");
        $objPHPExcel->getActiveSheet()->getStyle("A{$i}:H{$i}")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A{$i}:H{$i}")->applyFromArray([
            'alignment' => [
                'wrapText' => true,
            ],
        ]);
        $i = $i + 3;
        $objPHPExcel->getActiveSheet()->mergeCells("A{$i}:D{$i}");
        $objPHPExcel->getActiveSheet()->mergeCells("E{$i}:H{$i}");
        $i = $i + 2;
        $objPHPExcel->getActiveSheet()->mergeCells("A{$i}:D{$i}");
        $objPHPExcel->getActiveSheet()->mergeCells("E{$i}:H{$i}");
        $i = $i + 3;
        $objPHPExcel->getActiveSheet()->mergeCells("A{$i}:D{$i}");
        $objPHPExcel->getActiveSheet()->mergeCells("E{$i}:H{$i}");
        $objPHPExcel->getActiveSheet()->getStyle("A{$i}:H{$i}")->applyFromArray([
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_RIGHT,
            ],
        ]);
        $i++;
        $objPHPExcel->getActiveSheet()->getStyle("D{$i}:D{$i}")->applyFromArray([
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_RIGHT,
            ],
        ]);
        $objPHPExcel->getActiveSheet()->getStyle("H{$i}:H{$i}")->applyFromArray([
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_RIGHT,
            ],
        ]);

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getStyle("A1:H{$i}")->applyFromArray([
            'borders' => array(
                'allborders' => array(
                    'style' => Border::BORDER_THIN
                )
            ),
        ]);

        return \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objPHPExcel, $type);
    }

    /**
     * @return PdfRenderer
     */
    public function getRenderer($destination = PdfRenderer::DESTINATION_STRING, $view = 'pdf')
    {
        $renderer = new PdfRenderer([
            'view' => '@frontend/views/contractor/collate/' . $view,
            'params' => array_merge([
                'model' => $this,
            ]),
            'title' => $this->title,
            'filename' => $this->pdfFileName,
            'destination' => $destination,
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => 'A4-L',
        ]);
        \Yii::$app->controller->view->params['asset'] = DocumentPrintAsset::className();

        return $renderer;
    }

    /**
     * Return array of emails.
     * @return array
     */
    public function getEmailList()
    {
        $emailArray = [];
        $contractor = $this->contractor;

        if ($this->sendToChief) {
            if ($this->chiefEmail) {
                $contractor->director_email = $this->chiefEmail;
                if ($contractor->save(true, ['director_email'])) {
                    $emailArray[] = $this->chiefEmail;
                }
            } else {
                $emailArray[] = $contractor->director_email;
            }
        }

        if ($this->sendToChiefAccountant) {
            if ($this->chiefAccountantEmail) {
                $contractor->chief_accountant_email = $this->chiefAccountantEmail;
                if ($contractor->save(true, ['chief_accountant_email'])) {
                    $emailArray[] = $this->chiefAccountantEmail;
                }
            } else {
                $emailArray[] = $contractor->chief_accountant_email;
            }
        }

        if ($this->sendToContact) {
            if ($this->contactEmail) {
                $contractor->contact_email = $this->contactEmail;
                if ($contractor->save(true, ['contact_email'])) {
                    $emailArray[] = $this->contactEmail;
                }
            } else {
                $emailArray[] = $contractor->contact_email;
            }
        }

        if ($this->sendToOther) {
            $emailArray[] = $this->otherEmail;
        }

        return $emailArray;
    }

    public function getFlowTitlePrintable($maxStrLen = 35)
    {
        return implode('<br/>', mb_str_split($this->product_title, $maxStrLen));
    }

    public function getFlowTitlePrintableRowsCount($maxStrLen = 35)
    {
        return count(explode('<br/>', $this->getFlowTitlePrintable($maxStrLen)));
    }
}
