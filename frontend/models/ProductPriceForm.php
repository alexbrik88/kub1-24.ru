<?php

namespace frontend\models;

use common\models\Company;
use Yii;
use yii\base\Model;

/**
 * ProductPriceForm.
 */
class ProductPriceForm extends Model
{
    const UNIT_PERCENT = 1;
    const UNIT_MONEY = 2;
    const CHANGE_UP = 1;
    const CHANGE_DOWN = 2;

    protected $_company;

    public $unit;
    public $buy_type;
    public $buy_value;
    public $sell_type;
    public $sell_value;
    public $product_id;
    public $form_error;
    public $is_alco;

    public static $unitItems = [
        self::UNIT_PERCENT => '%%',
        self::UNIT_MONEY => 'Рублях',
    ];

    public static $typeItems = [
        '' => '---',
        self::CHANGE_UP => 'Увеличить',
        self::CHANGE_DOWN => 'Уменьшить',
    ];

    /**
     * @param Company $company
     * @param array $config
     */
    public function __construct(Company $company, $config = [])
    {
        $this->_company = $company;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['form_error'], 'required',
                'message' => 'Необходимо выбрать изменяемую цену',
            ],
            [['unit'], 'required', 'message' => 'Необходимо выбрать'],
            [['unit', 'sell_type', 'buy_type'], 'in', 'range' => [1, 2]],
            [['sell_value', 'buy_value'], 'trim'],
            [['sell_value', 'buy_value'], 'filter', 'filter' => function ($value) {
                return strtr($value, [',' => '.']);
            }],
            [
                ['sell_value', 'buy_value'], 'integer',
                'integerOnly' => false,
                'message' => 'Должно быть числом',
            ],
            [
                ['sell_value', 'buy_value'], 'compare',
                'compareValue' => 0,
                'operator' => '>',
                'message' => 'Должно быть больше «0»',
            ],
            [
                ['buy_value'], 'required',
                'message' => 'Необходимо заполнить ',
                'when' => function ($model) {
                    return !empty($model->buy_type);
                }
            ],
            [
                ['sell_value'], 'required',
                'message' => 'Необходимо заполнить ',
                'when' => function ($model) {
                    return !empty($model->sell_type);
                }
            ],
            /*[
                ['buy_value'], function ($attribute, $params) {
                    if ($this->$attribute >= 100) {
                        $this->addError($attribute, 'Должно быть меньше «100»');
                    }
                },
                'when' => function ($model) {
                    return $model->unit == self::UNIT_PERCENT && $model->buy_type == self::CHANGE_DOWN;
                }
            ],*/
            /*[
                ['sell_value'], function ($attribute, $params) {
                    if ($this->$attribute >= 100) {
                        $this->addError($attribute, 'Должно быть меньше «100»');
                    }
                },
                'when' => function ($model) {
                    return $model->unit == self::UNIT_PERCENT && $model->sell_type == self::CHANGE_DOWN;
                }
            ],*/
            [['product_id', 'is_alco'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'unit' => 'Изменение в',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @inheritdoc
     */
    public function changePrice()
    {
        $isBuyChanged = false;
        $isSellChanged = false;
        $buyChangedCount = 0;
        $sellChangedCount = 0;
        $buyNoChanged = [];
        $sellNoChanged = [];
        $productArray = $this->company->getProducts()->andWhere(['id' => $this->product_id])->all();
        foreach ($productArray as $product) {
            $oldBuyPrice = $newBuyPrice = $product->price_for_buy_with_nds;
            $oldSellPrice = $newSellPrice = $product->price_for_sell_with_nds;
            switch ($this->unit) {
                case self::UNIT_PERCENT:
                    $buyDifference = $this->buy_value ? round($oldBuyPrice * $this->buy_value / 100) : 0;
                    $sellDifference = $this->sell_value ? round($oldSellPrice * $this->sell_value / 100) : 0;
                    break;
                case self::UNIT_MONEY:
                    $buyDifference = $this->buy_value ? round($this->buy_value * 100) : 0;
                    $sellDifference = $this->sell_value ? round($this->sell_value * 100) : 0;
                    break;
                default:
                    $buyDifference = 0;
                    $sellDifference = 0;
                    break;
            }

            if ($buyDifference) {
                switch ($this->buy_type) {
                    case self::CHANGE_UP:
                        $newBuyPrice = $oldBuyPrice + $buyDifference;
                        break;
                    case self::CHANGE_DOWN:
                        $newBuyPrice = max(0, $oldBuyPrice - $buyDifference);
                        break;
                }
            }

            if (!$product->not_for_sale && $sellDifference) {
                switch ($this->sell_type) {
                    case self::CHANGE_UP:
                        $newSellPrice = $oldSellPrice + $sellDifference;
                        break;
                    case self::CHANGE_DOWN:
                        $newSellPrice = max(0, $oldSellPrice - $sellDifference);
                        break;
                }
            }

            $updateAattributes = [];

            if ($newBuyPrice == $oldBuyPrice) {
                if ($this->buy_type) {
                    $buyNoChanged[] = $product->title;
                }
            } else {
                $updateAattributes['price_for_buy_with_nds'] = $newBuyPrice;
                $buyChangedCount++;
            }

            if ($newSellPrice == $oldSellPrice) {
                if ($this->sell_type) {
                    $sellNoChanged[] = $product->title;
                }
            } else {
                $updateAattributes['price_for_sell_with_nds'] = $newSellPrice;
                $sellChangedCount++;
            }

            if ($updateAattributes) {
                $product->updateAttributes($updateAattributes);
            }
        }

        $success = [];
        $info = [];
        if ($buyChangedCount) {
            $success[] = "Цена покупки изменена. Всего позиций изменено: {$buyChangedCount}.";
        }
        if ($sellChangedCount) {
            $success[] = "Цена продажи изменена. Всего позиций изменено: {$sellChangedCount}.";
        }
        if ($success) {
            Yii::$app->session->setFlash('success', $success);
            if ($buyNoChanged) {
                $buyList = implode(', ', $buyNoChanged);
                $info[] = "Не удалось изменить цену покупки по следующим позициям: {$buyList}.";
            }
            if ($sellNoChanged) {
                $sellList = implode(', ', $sellNoChanged);
                $info[] = "Не удалось изменить цену продажи по следующим позициям: {$sellList}.";
            }
            if ($info) {
                Yii::$app->session->setFlash('info', $info);
            }
        } else {
            Yii::$app->session->setFlash('error', "Не удалось изменить цену по выбранным позициям.");
        }
    }
}
