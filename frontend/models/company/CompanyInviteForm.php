<?php

namespace frontend\models\company;

use common\components\helpers\Html;
use common\models\company\CompanyAffiliateLink;
use common\models\employee\Employee;
use common\models\service\ServiceModule;
use Exception;
use Yii;
use yii\base\Model;


class CompanyInviteForm extends Model
{
    /** @var $senderName */
    public $senderName;

    /** @var $senderEmail */
    public $senderEmail;

    /** @var $inviteProduct */
    public $inviteProduct;

    /** @var $hint */
    public $hint;

    /** @var $from */
    public $from;
    /** @var $email */
    public $email;
    /** @var $emailRecipient */
    public $emailRecipient;
    /** @var $product */
    public $product;
    /** @var $subject */
    public $subject;
    /** @var $body */
    public $body;
    /** @var $textBody */
    public $textBody;
    /** @var $htmlBody */
    public $htmlBody;

    /** @var Employee $user */
    private $user;
    private $company;

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->user = Yii::$app->user->identity;
        $this->company = $this->user->company;

        $this->senderName = $this->user->getFio();
        $this->senderEmail = $this->company->email;

        $this->inviteProduct = ServiceModule::$serviceModuleLabel;

        $this->textBody = 'Здравствуйте!';

        parent::init();
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'from' => 'От кого',
            'email' => 'Получатель',
            'product' => 'Рекомендуемый продукт',
            'subject' => 'Тема сообщения',
            'body' => 'Сообщение',
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['product', 'subject'], 'required'],
            [['body'], 'safe'],
            [['from', 'email', 'emailRecipient'], 'safe'],
        ];
    }

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        $this->email = isset($data['email']) ? Html::encode($data['email']) : null;
        $this->emailRecipient = isset($data['username']) ? Html::encode($data['username']) : null;

        return parent::load($data, $formName);
    }

    /**
     * @return bool
     */
    public function send()
    {
        $emailMessagePath = [
            'html' => 'system/invite-company/html',
            'text' => 'system/invite-company/text',
        ];

        $params = [
            'user' => $this->user,
            'subject' => Html::encode($this->subject),
            'userMessage' => Html::encode($this->body),
            'inviteUrl' => $this->getInviteProduct(),
            'supportEmail' => Yii::$app->params['emailList']['support'],
        ];

        try {
            $mail = Yii::$app->mailer->compose($emailMessagePath, $params)
                ->setFrom([$this->senderEmail => $this->senderName])
                ->setTo([$this->email => $this->emailRecipient])
                ->setSubject(Html::encode($this->subject));

            $mail->send();
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function getInviteProduct() {
        $product = $this->product ?: 'invoices';

        $companyAffiliateLink = CompanyAffiliateLink::find()
            ->andWhere(['and',
                ['company_id' => $this->company->id],
                ['service_module_id' => $product],
            ])
            ->one();

        return Yii::$app->params['siteModules'][$product] . '?req=' . $companyAffiliateLink->link;
    }

}