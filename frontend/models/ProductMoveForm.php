<?php

namespace frontend\models;

use common\models\Company;
use common\models\product\Product;
use Yii;
use yii\base\Model;

/**
 * ProductMoveForm.
 */
class ProductMoveForm extends Model
{
    protected $_product;
    protected $_company;
    protected $_stores;
    protected $_productStores;

    public $from;
    public $to;
    public $count;
    public $saved = false;

    /**
     * @param Product $product
     * @param array $config
     */
    public function __construct(Product $product, $config = [])
    {
        $this->_product = $product;
        $this->_company = $product->company;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from', 'to'], 'required', 'message' => 'Необходимо выбрать'],
            [['count'], 'required'],
            [['count'], 'number'],
            [['from'], 'in', 'range' => array_keys($this->getProductStores())],
            [['to'], 'in', 'range' => array_keys($this->getStores())],
            [['to'], function ($attribute, $params) {
                if ($this->$attribute == $this->from) {
                    $this->addError($attribute, 'Выберите другой склад.');
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'from' => 'Со склада',
            'to' => 'На склад',
            'count' => 'Количество',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @inheritdoc
     */
    public function getProduct()
    {
        return $this->_product;
    }

    /**
     * @inheritdoc
     */
    public function getStores()
    {
        if ($this->_stores === null) {
            $this->_stores = $this->_company->getStores()
                ->andWhere(['is_closed' => false])->indexBy('id')->all();
        }

        return $this->_stores;
    }

    /**
     * @inheritdoc
     */
    public function getProductStores()
    {
        if ($this->_productStores === null) {
            $this->_productStores = $this->_product->getProductStores()
                ->andWhere(['>', 'quantity', 0])->indexBy('store_id')->all();
        }

        return $this->_productStores;
    }

    /**
     * @inheritdoc
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }
        if ($this->from == $this->to) {
            return true;
        }
        $from = $this->productStores[$this->from];
        $count = min($from->quantity, $this->count);
        $to = $this->_product->getProductStoreByStore($this->to);

        $from->quantity -= $count;
        $to->quantity += $count;

        $this->saved = Yii::$app->db->transaction(function ($db) use ($from, $to) {
            if ($from->save() && $to->save()) {
                return true;
            } else {
                $db->transaction->rollBack();

                return false;
            }
        });

        return $this->saved;
    }
}
