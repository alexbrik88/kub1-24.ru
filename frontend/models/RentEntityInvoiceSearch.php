<?php
namespace frontend\models;

use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\rent\RentAgreement;
use common\models\rent\Entity;
use frontend\components\PageSize;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

class RentEntityInvoiceSearch extends Invoice
{
    public $search;

    private Entity $entity;

    public function __construct(Entity $entity, $config = [])
    {
        $this->entity = $entity;

        parent::__construct($config);
    }

    public function getEntity()
    {
        return $this->entity;
    }

    public function rules()
    {
        return [
            [['search',], 'trim'],
            [['contractor_id',], 'integer'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $paidStatuses = implode(',', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]);
        $query = Invoice::find()->alias('invoice')
        ->joinWith('contractor')
        ->addSelect([
            'invoice.*',
            'payDate' => new Expression("
                IF({{invoice}}.[[invoice_status_id]] in ({$paidStatuses}), {{invoice}}.[[invoice_status_updated_at]], NULL)
            "),
        ])
        ->rightJoin([
            'rent_agreement_invoice' => '{{%rent_agreement_invoice}}'
        ], '{{invoice}}.[[id]] = {{rent_agreement_invoice}}.[[invoice_id]]')
        ->rightJoin([
            'rent_agreement' => '{{%rent_agreement}}',
        ], '{{rent_agreement}}.[[id]] = {{rent_agreement_invoice}}.[[rent_agreement_id]]')
        ->rightJoin([
            'rent_agreement_item' => '{{%rent_agreement_item}}',
        ], '{{rent_agreement}}.[[id]] = {{rent_agreement_item}}.[[rent_agreement_id]]')
        ->andWhere([
            'invoice.company_id' => $this->entity->company_id,
            'invoice.is_deleted' => false,
            'invoice.from_demo_out_invoice' => false,
            'invoice.type' => Documents::IO_TYPE_OUT,
            'rent_agreement_item.rent_entity_id' => $this->entity->id,
        ]);

        $this->load($params);

        $query->andFilterWhere([
            'invoice.contractor_id' => $this->contractor_id,
        ]);

        if (!empty($this->search)) {
            $paramArray = explode(' ', $this->search);
            foreach ($paramArray as $param) {
                $query->andFilterWhere([
                    'or',
                    [Invoice::tableName() . '.contractor_inn' => $param],
                    ['like', Invoice::tableName() . '.document_number', $param],
                    ['like', Invoice::tableName() . '.document_additional_number', $param],
                    ['like', Contractor::tableName() . '.name', $param]
                ]);
            }
        }

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'document_date' => [
                        'asc' => [
                            'invoice.document_date' => SORT_ASC,
                            '({{invoice}}.[[document_number]] * 1)' => SORT_ASC,
                        ],
                        'desc' => [
                            'invoice.document_date' => SORT_DESC,
                            '({{invoice}}.[[document_number]] * 1)' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'payDate' => [
                        'default' => SORT_DESC,
                    ],
                    'document_number' => [
                        'asc' => ['({{invoice}}.[[document_number]] * 1)' => SORT_ASC],
                        'desc' => ['({{invoice}}.[[document_number]] * 1)' => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                    'total_amount_with_nds',
                    'payment_limit_date',
                    'has_file',
                ],
                'defaultOrder' => ['document_date' => SORT_DESC],
            ],
            'pagination' => [
                'pageSize' => PageSize::get()
            ]
        ]);
    }
}
