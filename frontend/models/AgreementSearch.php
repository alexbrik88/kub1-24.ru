<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Agreement;

/**
 * AgreementSearch represents the model behind the search form about `common\models\Agreement`.
 */
class AgreementSearch extends Agreement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contractor_id', 'document_type_id', 'created_by'], 'integer'],
            [['document_date', 'document_number', 'document_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $formName = null)
    {
        $query = Agreement::find();

        $query->andWhere([
            'company_id' => $this->company_id,
            'is_created' => true,
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params, $formName);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'contractor_id' => $this->contractor_id,
            'document_date' => $this->document_date,
            'document_type_id' => $this->document_type_id,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'document_number', $this->document_number])
            ->andFilterWhere(['like', 'document_name', $this->document_name]);

        return $dataProvider;
    }
}
