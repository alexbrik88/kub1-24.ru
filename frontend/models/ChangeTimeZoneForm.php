<?php
namespace frontend\models;

use common\models\employee\Employee;
use Yii;
use yii\base\Model;
use yii\web\NotFoundHttpException;

class ChangeTimeZoneForm extends Model
{
    public $newTimeZone;
    /**
     * @var \common\models\employee\Employee
     */
    private $_user;

    /**
     * @param array $config
     * @param null $user
     * @throws NotFoundHttpException
     */
    public function __construct($config = [], $user = null)
    {
        $this->_user = $user ? $user : Yii::$app->user->identity;
        if (!$this->_user) {
            throw new NotFoundHttpException;
        }

        parent::__construct($config = []);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['newTimeZone', 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'newTimeZone' => 'Новый часовой пояс',
        ];
    }

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function save()
    {
        $user = $this->_user;
        $user->time_zone_id = $this->newTimeZone;

        return $user->save(true, [
            'time_zone_id',
        ]);
    }
}