<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use common\models\employee\EmployeeSalary;

/**
 * EmployeeSalarySearch represents the model behind the search form about `common\models\employee\EmployeeSalary`.
 */
class EmployeeSalarySearch extends EmployeeSalary
{
    public $position;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmployeeSalary::find();

        // add conditions that should always apply here
        $query->joinWith([
            'employeeCompany',
        ])->with([
            'employeeSalarySummary',
        ])->andWhere([
            'employee_salary.company_id' => $this->company_id,
            'employee_salary.date' => $this->date,
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');

            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'employee_company.position' => $this->position,
        ]);

        return $dataProvider;
    }

    /**
     * Elements of the drop-down list of employee positions
     * @param  ActiveQuery $searchQuery
     * @return array
     */
    public function filterPosition(ActiveQuery $searchQuery)
    {
        $query = clone $searchQuery;

        $items = $query->select('position')->distinct()->column();

        return ['' => 'Все'] + array_combine($items, $items);
    }
}
