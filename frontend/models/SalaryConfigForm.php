<?php
namespace frontend\models;

use common\components\validators\EmployeeEmailValidator;
use common\models\EmployeeCompany;
use common\models\employee\EmployeeSalary;
use common\models\employee\EmployeeSalarySummary;
use yii\base\Model;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * SalaryConfigForm
 */
class SalaryConfigForm extends Model
{
    use \frontend\components\SalaryFormTrait;

    public $has_salary_1;
    public $salary_1_amount;
    public $salary_1_prepay_day;
    public $salary_1_prepay_sum;
    public $salary_1_pay_day;
    public $has_bonus_1;
    public $bonus_1_amount;
    public $bonus_1_prepay_day;
    public $bonus_1_prepay_sum;
    public $bonus_1_pay_day;
    public $has_salary_2;
    public $salary_2_amount;
    public $salary_2_prepay_day;
    public $salary_2_prepay_sum;
    public $salary_2_pay_day;
    public $has_bonus_2;
    public $bonus_2_amount;
    public $bonus_2_prepay_day;
    public $bonus_2_prepay_sum;
    public $bonus_2_pay_day;

    /**
     * @var array
     */
    public static $company_attributes = [
        'salary_1_pay_day',
        'salary_1_prepay_day',
        'bonus_1_pay_day',
        'bonus_1_prepay_day',
        'salary_2_pay_day',
        'salary_2_prepay_day',
        'bonus_2_pay_day',
        'bonus_2_prepay_day',
    ];

    /**
     * @var array
     */
    public static $employee_attributes = [
        'has_salary_1',
        'has_bonus_1',
        'has_salary_2',
        'has_bonus_2',
        'salary_1_amount',
        'bonus_1_amount',
        'salary_2_amount',
        'bonus_2_amount',
        'salary_1_prepay_sum',
        'bonus_1_prepay_sum',
        'salary_2_prepay_sum',
        'bonus_2_prepay_sum',
    ];

    /**
     * @var \common\models\EmployeeCompany
     */
    protected $_employee_company;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $employee = $this->getEmployeeCompany();
        $company = $employee->company;

        $this->setAttributes($company->getAttributes(self::$company_attributes), false);
        $this->setAttributes($employee->getAttributes(self::$employee_attributes), false);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'has_salary_1',
                    'has_bonus_1',
                    'has_salary_2',
                    'has_bonus_2',
                ],
                'boolean',
            ],
            [
                [
                    'has_salary_1',
                ],
                'hasSalaryValidator',
            ],
            [
                [
                    'salary1Amount',
                    'bonus1Amount',
                    'salary2Amount',
                    'bonus2Amount',
                    'salary1PrepaySum',
                    'bonus1PrepaySum',
                    'salary2PrepaySum',
                    'bonus2PrepaySum',
                ],
                'safe',
            ],
            [
                [
                    'salary_1_amount',
                    'salary_1_prepay_sum',
                    'bonus_1_amount',
                    'bonus_1_prepay_sum',
                    'salary_2_amount',
                    'salary_2_prepay_sum',
                    'bonus_2_amount',
                    'bonus_2_prepay_sum',
                ],
                'integer',
            ],
            [
                [
                    'salary_1_pay_day',
                    'salary_1_prepay_day',
                    'bonus_1_pay_day',
                    'bonus_1_prepay_day',
                    'salary_2_pay_day',
                    'salary_2_prepay_day',
                    'bonus_2_pay_day',
                    'bonus_2_prepay_day',
                ],
                'integer',
                'min' => 1,
                'max' => 31,
            ],
            [
                [
                    'salary_1_amount',
                    'bonus_1_amount',
                    'salary_2_amount',
                    'bonus_2_amount',
                ],
                'amountValidator',
            ],
            [
                [
                    'salary_1_pay_day',
                    'bonus_1_pay_day',
                    'salary_2_pay_day',
                    'bonus_2_pay_day',
                ],
                'payDayValidator',
                'skipOnEmpty' => false,
            ],
            [
                [
                    'salary_1_prepay_day',
                    'bonus_1_prepay_day',
                    'salary_2_prepay_day',
                    'bonus_2_prepay_day',
                ],
                'prepayDayValidator',
                'skipOnEmpty' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function hasSalaryValidator($attribte)
    {
        if (!($this->has_salary_1 || $this->has_bonus_1 || $this->has_salary_2 || $this->has_bonus_2)) {
            $this->addError($attribte, 'Выберете  вид расчета - поставьте галочку в чекбоксе слева');
        }
    }

    /**
     * @inheritdoc
     */
    public function amountValidator($attribte)
    {
        preg_match('/^(.+)_amount$/', $attribte, $matches);
        $subStr = $matches[1];
        if ($this->{'has_' . $subStr} && empty($this->$attribte)) {
            $this->addError(strtr($subStr, ['_' => '']) . 'Amount', 'Необходимо заполнить «' . $this->attributeLabels()[$attribte] . '»');
        }
    }

    /**
     * @inheritdoc
     */
    public function payDayValidator($attribte)
    {
        preg_match('/^(.+)_pay_day$/', $attribte, $matches);
        $subStr = $matches[1];
        if (!$this->$attribte &&
            ($this->{'has_' . $subStr} || $this->payTypeExistsQuery($subStr)->exists())
        ) {
            $this->addError($attribte, 'Необходимо заполнить «' . $this->attributeLabels()[$attribte] . '»');
        }
    }

    /**
     * @inheritdoc
     */
    public function prepayDayValidator($attribte)
    {
        preg_match('/^(.+)_prepay_day$/', $attribte, $matches);
        $subStr = $matches[1];
        if (!$this->$attribte &&
            (($this->{'has_' . $subStr} && $this->{$subStr . '_prepay_sum'}) || $this->prepayExistsQuery($subStr)->exists())
        ) {
            $this->addError($attribte, 'Необходимо заполнить «' . $this->attributeLabels()[$attribte] . '»');
        }
    }

    /**
     * @param string $subStr
     * @return yii\db\ActiveQuery
     */
    public function payTypeExistsQuery($subStr)
    {
        return EmployeeCompany::find()->andWhere([
            'company_id' => $this->getEmployeeCompany()->company_id,
            'has_' . $subStr => true,
        ]);
    }

    /**
     * @param string $subStr
     * @return yii\db\ActiveQuery
     */
    public function prepayExistsQuery($subStr)
    {
        return $this->payTypeExistsQuery($subStr)->andWhere(['>', $subStr . '_prepay_sum', 0]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'has_salary_1' => 'Оклад 1',
            'has_bonus_1' => 'Премия 1',
            'has_salary_2' => 'Оклад 2',
            'has_bonus_2' => 'Премия 2',
            'salary_1_amount' => 'Оклад 1 - Сумма на руки',
            'bonus_1_amount' => 'Премия 1 - Сумма на руки',
            'salary_2_amount' => 'Оклад 2 - Сумма на руки',
            'bonus_2_amount' => 'Премия 2 - Сумма на руки',
            'salary_1_prepay_sum' => 'Оклад 1 - Аванса',
            'bonus_1_prepay_sum' => 'Премия 1 - Аванса',
            'salary_2_prepay_sum' => 'Оклад 2 - Аванса',
            'bonus_2_prepay_sum' => 'Премия 2 - Аванса',
            'salary_1_pay_sum' => 'Оклад 1 - Зарплата',
            'bonus_1_pay_sum' => 'Премия 1 - Зарплата',
            'salary_2_pay_sum' => 'Оклад 2 - Зарплата',
            'bonus_2_pay_sum' => 'Премия 2 - Зарплата',
            'salary_1_expenses' => 'Оклад 1 - Затраты для компании',
            'bonus_1_expenses' => 'Премия 1 - Затраты для компании',
            'salary_2_expenses' => 'Оклад 2 - Затраты для компании',
            'bonus_2_expenses' => 'Премия 2 - Затраты для компании',
            'salary_1_prepay_day' => 'Оклад 1 - Дата аванса',
            'bonus_1_prepay_day' => 'Премия 1 - Дата аванса',
            'salary_2_prepay_day' => 'Оклад 2 - Дата аванса',
            'bonus_2_prepay_day' => 'Премия 2 - Дата аванса',
            'salary_1_pay_day' => 'Оклад 1 - Дата зарплаты',
            'bonus_1_pay_day' => 'Премия 1 - Дата зарплаты',
            'salary_2_pay_day' => 'Оклад 2 - Дата зарплаты',
            'bonus_2_pay_day' => 'Премия 2 - Дата зарплаты',
            'total_amount' => 'Сумма выплат на руки',
            'total_tax_ndfl' => 'Сумма НДФЛ',
            'total_tax_social' => 'Сумма соц. налогов',
            'total_expenses' => 'Итого затраты для компании',
            'total_prepay_sum' => 'Сумма аванса',
            'total_pay_sum' => 'Сумма зарплат',
        ];
    }

    /**
     * @inheritdoc
     */
    public function setEmployeeCompany(EmployeeCompany $employeeCompany)
    {
        $this->_employee_company = $employeeCompany;
    }

    /**
     * @inheritdoc
     */
    public function getEmployeeCompany()
    {
        return $this->_employee_company;
    }

    /**
     * @return integer
     */
    public function getSalary_1_pay_sum()
    {
        return $this->salary_1_amount - $this->salary_1_prepay_sum;
    }

    /**
     * @return integer
     */
    public function getSalary_1_expenses()
    {
        $sum = $this->salary_1_amount + EmployeeSalary::taxNdfl($this->salary_1_amount);

        return $sum + EmployeeSalary::taxSocial($sum);
    }

    /**
     * @return integer
     */
    public function getBonus_1_pay_sum()
    {
        return $this->bonus_1_amount - $this->bonus_1_prepay_sum;
    }

    /**
     * @return integer
     */
    public function getBonus_1_expenses()
    {
        $sum = $this->bonus_1_amount + EmployeeSalary::taxNdfl($this->bonus_1_amount);

        return $sum + EmployeeSalary::taxSocial($sum);
    }

    /**
     * @return integer
     */
    public function getSalary_2_pay_sum()
    {
        return $this->salary_2_amount - $this->salary_2_prepay_sum;
    }

    /**
     * @return integer
     */
    public function getSalary_2_expenses()
    {
        return $this->salary_2_amount;
    }

    /**
     * @return integer
     */
    public function getBonus_2_pay_sum()
    {
        return $this->bonus_2_amount - $this->bonus_2_prepay_sum;
    }

    /**
     * @return integer
     */
    public function getBonus_2_expenses()
    {
        return $this->bonus_2_amount;
    }

    /**
     * @return boolean if password was reset.
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $employeeCompany = $this->getEmployeeCompany();
        $company = $employeeCompany->company;

        $company->setAttributes($this->getAttributes(self::$company_attributes), false);
        $employeeCompany->setAttributes($this->getAttributes(self::$employee_attributes), false);

        return Yii::$app->db->transaction(function ($db) use ($company, $employeeCompany) {
            if ($company->save(false, self::$company_attributes) && $employeeCompany->save(false, self::$employee_attributes)) {
                $date = new \DateTime('first day of this month');
                $model = EmployeeSalarySummary::find()->with('employeeSalaries')->andWhere([
                    'company_id' => $company->id,
                    'date' => $date->format('Y-m-d'),
                ])->one();

                if ($model !== null) {
                    $model->setData($company, $date);
                    foreach ($model->employeeSalaries as $employeeSalary) {
                        if ($employeeCompany->employee_id == $employeeSalary->employee_id) {
                            $employeeSalary->setData($company, $employeeCompany);
                        } else {
                            $employeeSalary->setData($company);
                        }
                    }

                    $model->save();
                }

                return true;
            }
            $db->transaction->rollBack();

            return false;
        });
    }

    /**
     * @return array
     */
    public function getCalculatedData()
    {
        $employeeCompany = $this->getEmployeeCompany();
        $company = $employeeCompany->company;

        $company->setAttributes($this->getAttributes(self::$company_attributes), false);
        $employeeCompany->setAttributes($this->getAttributes(self::$employee_attributes), false);

        $employeeSalary = new EmployeeSalary([
            'employee_id' => $employeeCompany->employee_id,
            'company_id' => $company->id,
        ]);

        $employeeSalary->setData($company, $employeeCompany);
        $employeeSalary->calculateTotalFields();

        return $employeeSalary->getAttributes([
            'salary_1_amount',
            'bonus_1_amount',
            'salary_2_amount',
            'bonus_2_amount',
            'salary_1_prepay_sum',
            'bonus_1_prepay_sum',
            'salary_2_prepay_sum',
            'bonus_2_prepay_sum',
            'salary_1_pay_sum',
            'bonus_1_pay_sum',
            'salary_2_pay_sum',
            'bonus_2_pay_sum',
            'salary_1_expenses',
            'bonus_1_expenses',
            'salary_2_expenses',
            'bonus_2_expenses',
            'total_amount',
            'total_tax_ndfl',
            'total_tax_social',
            'total_expenses',
            'total_prepay_sum',
            'total_pay_sum',
        ]);
    }

    /**
     * @return array
     */
    public function getFormatCalculatedData()
    {
        $data = $this->getCalculatedData();
        $result = [];
        foreach ($data as $key => $value) {
            $result[$key] = number_format($value / 100, 2, '.', ' ');
        }

        return $result;
    }
}
