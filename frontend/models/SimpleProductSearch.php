<?php

namespace frontend\models;

use Yii;
use common\models\Company;
use common\models\product\Product;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class SimpleProductSearch extends Product
{
    public $search;
    public $exclude;

    private Company $company;

    /**
     * @inheritdoc
     */
    public function __construct(Company $company, $production_type)
    {
        $this->company = $company;

        if (!in_array($production_type, [Product::PRODUCTION_TYPE_GOODS, Product::PRODUCTION_TYPE_SERVICE])) {
            throw new InvalidConfigException("Invalid Configuration");
        }
        $this->production_type = $production_type;

        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'search',
                    'unit_type',
                    'group_id',
                    'product_unit_id',
                    'exclude',
                ],
                'safe',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return parent::attributeLabels();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->company->getProducts()->byDeleted()->byUser()->byStatus(Product::ACTIVE)->notForSale(false)->andWhere([
            'production_type' => $this->production_type,
        ])->with([
            'group',
            'productUnit',
        ]);

        $this->load($params);

        $query->andFilterWhere([
            'unit_type' => $this->unit_type,
            'group_id' => $this->group_id,
            'product_unit_id' => $this->product_unit_id,
        ]);

        $query->andFilterWhere([
            'or',
            ['like', 'title', $this->search],
            ['like', 'article', $this->search],
        ]);

        $query->andFilterWhere([
            'not',
            ['id' => $this->exclude],
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
