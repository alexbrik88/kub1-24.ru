<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use common\models\cash\Emoney;

/**
 * EmoneySearch represents the model behind the search form about `common\models\cash\Emoney`.
 */
class EmoneySearch extends Emoney
{
    /**
     * search query
     * @var ActiveQuery
     */
    private $_query;

    public function setQuery(ActiveQuery $query)
    {
        $this->_query = $query;
    }

    public function getQuery() : ActiveQuery
    {
        if (!isset($this->_query)) {
            $this->_query = Emoney::find()->andWhere([
                'company_id' => $this->company_id,
            ]);
        }

        return $this->_query;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_main', 'is_accounting', 'is_closed'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->getQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'is_closed' => SORT_ASC,
                    'is_main' => SORT_DESC,
                    'name' => SORT_ASC,
                ],
                'attributes' => [
                    'name',
                    'is_main',
                    'is_closed',
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_main' => $this->is_main,
            'is_accounting' => $this->is_accounting,
            'is_closed' => $this->is_closed,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
