<?php

namespace frontend\models;

use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductTurnover;
use common\models\product\ProductUnit;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class ProductTurnoverSearch extends Product
{
    const TURNOVER_BY_COUNT = 1;
    const TURNOVER_BY_AMOUNT = 2;

    public $balance_start;
    public $balance_in;
    public $balance_out;
    public $balance_end;
    public $exclude;

    public $documentType;
    public $turnoverType = self::TURNOVER_BY_COUNT;

    protected $_dataProvider;
    protected $_data;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'group_id',
                    'product_unit_id',
                    'title',
                ],
                'safe',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'quantity' => 'Количество на складе',
        ]);
    }

    /**
     * @return ProductQuery
     */
    public function productQuery()
    {
        $query = Product::find()->alias('product')
            ->byCompany($this->company_id)
            ->byUser()
            ->notForSale($this->documentType == Documents::IO_TYPE_OUT ? false : null)
            ->andWhere(['product.production_type' => $this->production_type])
            ->andWhere(['product.is_deleted' => false])
            ->andWhere(['not', ['product.status' => Product::DELETED]]);

        $query->andFilterWhere([
            'product.group_id' => $this->group_id,
            'product.product_unit_id' => $this->product_unit_id,
        ]);

        $query->andFilterWhere(['not', ['product.id' => $this->exclude]]);

        $query->andFilterWhere([
            'or',
            ['like', Product::tableName() . '.title', $this->title],
            ['like', Product::tableName() . '.article', $this->title],
        ]);

        return $query;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param bool $hideZeroTurnover
     *
     * @return SqlDataProvider
     */
    public function search($params, $hideZeroTurnover = false)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        $this->load($params);

        $this->createBalanceTmpTables();

        $query = $this->productQuery();
        $query
            ->leftJoin(['b' => '__product_balance'], '{{product}}.[[id]] = {{b}}.[[product_id]]')
            ->leftJoin(['t' => '__product_turnover'], '{{product}}.[[id]] = {{t}}.[[product_id]]')
            ->select([
                'product.id',
                'product.title',
                'product.group_id',
                'product.product_unit_id',
                'balance_start' => 'IFNULL({{b}}.[[balance]], 0)',
                'balance_in' => 'IFNULL({{t}}.[[balance_in]], 0)',
                'balance_out' => 'IFNULL({{t}}.[[balance_out]], 0)',
                'balance_end' => 'IFNULL({{b}}.[[balance]], 0) + IFNULL({{t}}.[[balance_in]], 0) - IFNULL({{t}}.[[balance_out]], 0)',
            ]);

        if ($hideZeroTurnover) {
            $query->andHaving([
                'or',
                ['<>', 'balance_in', 0],
                ['<>', 'balance_out',  0],
            ]);
        }

        /**
         * Создание временной таблицы результата поиска, чтобы не прогонять несколько раз тяжелый запрос
         */
        $db = Yii::$app->db;
        $comand = $db->createCommand("DROP TEMPORARY TABLE IF EXISTS `__product_turnover_result`");
        $comand->execute();
        $comand = $db->createCommand("CREATE TEMPORARY TABLE `__product_turnover_result` (
            `id` INT(11) NOT NULL,
            `title` TEXT(65535) NULL,
            `group_id` INT(11) NULL,
            `product_unit_id` INT(11) NULL,
            `balance_start` DECIMAL(20,10) NOT NULL DEFAULT '0',
            `balance_in` DECIMAL(20,10) NOT NULL DEFAULT '0',
            `balance_out` DECIMAL(20,10) NOT NULL DEFAULT '0',
            `balance_end` DECIMAL(20,10) NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`)
        )");
        $comand->execute();
        $sql = $query->createCommand()->rawSql;
        $comand = $db->createCommand("INSERT INTO `__product_turnover_result`
            (`id`, `title`, `group_id`, `product_unit_id`, `balance_start`, `balance_in`, `balance_out`, `balance_end`) {$sql}");
        $comand->execute();

        /**
         * Берем данные из временной таблицы
         */
        $this->_dataProvider = new SqlDataProvider([
            'sql' => 'SELECT * FROM `__product_turnover_result`',
            'totalCount' => Yii::$app->db->createCommand('SELECT COUNT(*) FROM `__product_turnover_result`')->queryScalar(),
            'pagination' => [
                'pageSize' => Yii::$app->request->get('per-page'),
            ],
            'sort' => [
                'attributes' => [
                    'title',
                    'balance_start',
                    'balance_in',
                    'balance_out',
                    'balance_end',
                ],
                'defaultOrder' => [
                    'title' => SORT_ASC,
                ],
            ]
        ]);

        return $this->_dataProvider;
    }

    /**
     * @return float
     */
    public function getSummary()
    {
        if (!isset($this->_data['getSummary'])) {
            if ($this->_dataProvider) {
                $query = (new Query)->from('__product_turnover_result')->select([
                    'balance_start' => 'SUM([[balance_start]])',
                    'balance_in' => 'SUM([[balance_in]])',
                    'balance_out' => 'SUM([[balance_out]])',
                    'balance_end' => 'SUM([[balance_end]])',
                ]);

                $this->_data['getSummary'] = $query->one();
            } else {
                $this->_data['getSummary'] = [];
            }
        }

        return $this->_data['getSummary'];
    }

    /**
     * @return float
     */
    public function getBalanceStart()
    {
        return ArrayHelper::getValue($this->getSummary(), 'balance_start', 0);
    }

    /**
     * @return float
     */
    public function getBalanceIn()
    {
        return ArrayHelper::getValue($this->getSummary(), 'balance_in', 0);
    }

    /**
     * @return float
     */
    public function getBalanceOut()
    {
        return ArrayHelper::getValue($this->getSummary(), 'balance_out', 0);
    }

    /**
     * @return integer
     */
    public function getBalanceEnd()
    {
        return ArrayHelper::getValue($this->getSummary(), 'balance_end', 0);
    }

    /**
     * @return integer
     */
    public function getUnitFilter()
    {
        if (!isset($this->_data['getUnitFilter'])) {
            if ($this->_dataProvider) {
                $query = (new Query)->from('__product_turnover_result');

                $this->_data['getUnitFilter'] = ProductUnit::find()->select('name')->where([
                    'id' => $query->select('product_unit_id')->distinct()->column(),
                ])->indexBy('id')->column();
            } else {
                $this->_data['getUnitFilter'] = [];
            }
        }

        return $this->_data['getUnitFilter'];
    }

    /**
     * @return integer
     */
    public function getGroupFilter()
    {
        if (!isset($this->_data['getGroupFilter'])) {
            if ($this->_dataProvider) {
                $query = (new Query)->from('__product_turnover_result');

                $this->_data['getGroupFilter'] = ProductGroup::find()->select('title')->where([
                    'id' => $query->select('group_id')->distinct()->column(),
                ])->indexBy('id')->column();
            } else {
                $this->_data['getGroupFilter'] = [];
            }
        }

        return $this->_data['getGroupFilter'];
    }

    protected function createBalanceTmpTables()
    {
        $period = StatisticPeriod::getSessionPeriod();
        $startDate = $period['from'];
        $endDate = $period['to'];

        $isAmount = $this->turnoverType == self::TURNOVER_BY_AMOUNT;

        $db = Yii::$app->db;

        /**
         * start balance
         */
        $query = (new \yii\db\Query())
            ->select([
                'product_id',
                'balance' => $isAmount ?
                    'SUM(IF([[type]]=2, -1, 1) * [[quantity]] * [[purchase_price]])' :
                    'SUM(IF([[type]]=2, -1, 1) * [[quantity]])',
            ])
            ->from([
                't' => ProductTurnover::tableName()
            ])
            ->andWhere([
                't.company_id' => $this->company_id,
                't.production_type' => $this->production_type,
                't.is_invoice_actual' => 1,
                't.is_document_actual' => 1,
            ])
            ->andWhere([
                '<',
                't.date',
                $startDate,
            ])
            ->groupBy('product_id');

        $sql = $query->createCommand()->rawSql;

        $comand = $db->createCommand("DROP TEMPORARY TABLE IF EXISTS __product_balance");
        $comand->execute();
        $comand = $db->createCommand("CREATE TEMPORARY TABLE `__product_balance` (
            `product_id` INT(11) NOT NULL,
            `balance` DECIMAL(20,10) NOT NULL,
            PRIMARY KEY (`product_id`)
        )");
        $comand->execute();
        $comand = $db->createCommand("INSERT INTO __product_balance (`product_id`, `balance`) {$sql}");
        $comand->execute();

        /**
         * turnover for period
         */
        $query = (new \yii\db\Query())
            ->select([
                'product_id',
                'balance_in' => $isAmount ?
                    'SUM(IF([[type]]=2, 0, [[quantity]] * [[purchase_price]]))' :
                    'SUM(IF([[type]]=2, 0, [[quantity]]))',
                'balance_out' => $isAmount ?
                    'SUM(IF([[type]]=2, [[quantity]] * [[purchase_price]], 0))' :
                    'SUM(IF([[type]]=2, [[quantity]], 0))',
            ])
            ->from([
                't' => ProductTurnover::tableName()
            ])
            ->andWhere([
                't.company_id' => $this->company_id,
                't.production_type' => $this->production_type,
                't.is_invoice_actual' => 1,
                't.is_document_actual' => 1,
            ])
            ->andWhere([
                'between',
                't.date',
                $startDate,
                $endDate,
            ])
            ->groupBy('product_id');

        $sql = $query->createCommand()->rawSql;

        $comand = $db->createCommand("DROP TEMPORARY TABLE IF EXISTS __product_turnover");
        $comand->execute();
        $comand = $db->createCommand("CREATE TEMPORARY TABLE `__product_turnover` (
            `product_id` INT(11) NOT NULL,
            `balance_in` DECIMAL(20,10) NOT NULL,
            `balance_out` DECIMAL(20,10) NOT NULL,
            PRIMARY KEY (`product_id`)
        )");
        $comand->execute();
        $comand = $db->createCommand("INSERT INTO __product_turnover (`product_id`, `balance_in`, `balance_out`) {$sql}");
        $comand->execute();
    }
}
