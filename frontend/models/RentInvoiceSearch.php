<?php
namespace frontend\models;

use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\rent\RentAgreement;
use frontend\components\PageSize;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

class RentInvoiceSearch extends Model
{
    /** @var int */
    public $company_id;
    /** @var int */
    public $entity;
    /** @var int */
    public $contractor;
    /** @var int */
    public $employee;
    /** @var string */
    public $byNumber;
    /** @var string */
    public $payDate;

    public function rules()
    {
        return [
            [['entity', 'contractor', 'employee'], 'integer'],
            ['byNumber', 'safe']
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        if ($this->validate() === false) {
            foreach ($this->errors as $attribute => $item) {
                $this->$attribute = null;
            }
        }

        $invoicesIds = RentAgreement::findByFilter($this->company_id, $this->entity, $this->contractor, $this->employee)
            ->select('invoice_id')->column();

        $paidStatuses = implode(',', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]);

        $query = Invoice::find()->with('contractor')
            ->addSelect([
                'invoice.*',
                'payDate' => new Expression("
                    IF({{invoice}}.[[invoice_status_id]] in ({$paidStatuses}), {{invoice}}.[[invoice_status_updated_at]], NULL)
                "),
            ])
            ->andWhere([
                'invoice.id' => $invoicesIds,
                'invoice.is_deleted' => false,
                'invoice.from_demo_out_invoice' => false
            ]);

        $this->byNumber = trim($this->byNumber);
        if (!empty($this->byNumber)) {
            $query->joinWith('contractor');

            $paramArray = explode(' ', $this->byNumber);
            foreach ($paramArray as $param) {
                $query->andFilterWhere([
                    'or',
                    [Invoice::tableName() . '.contractor_inn' => $param],
                    ['like', Invoice::tableName() . '.document_number', $param],
                    ['like', Invoice::tableName() . '.document_additional_number', $param],
                    ['like', Contractor::tableName() . '.name', $param]
                ]);
            }
        }

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'sortParam' => (!empty($sortParamTitle)) ? $sortParamTitle : 'sort',
                'attributes' => [
                    'document_date' => [
                        'asc' => [
                            Invoice::tableName() . '.`document_date`' => SORT_ASC,
                            Invoice::tableName() . '.`document_number` * 1' => SORT_ASC,
                        ],
                        'desc' => [
                            Invoice::tableName() . '.`document_date`' => SORT_DESC,
                            Invoice::tableName() . '.`document_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'payDate' => [
                        'default' => SORT_DESC,
                    ],
                    'document_number' => [
                        'asc' => [Invoice::tableName() . '.`document_number` * 1' => SORT_ASC],
                        'desc' => [Invoice::tableName() . '.`document_number` * 1' => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                    'total_amount_with_nds',
                    'payment_limit_date',
                    'has_file',
                ],
                'defaultOrder' => ['document_date' => SORT_DESC],
            ],
            'pagination' => [
                'pageSize' => PageSize::get()
            ]
        ]);
    }
}
