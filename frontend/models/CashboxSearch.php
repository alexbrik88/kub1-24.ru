<?php

namespace frontend\models;

use common\models\cash\Cashbox;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use common\models\EmployeeCompany;
use common\models\employee\EmployeeRole;

/**
 * EmployeeCompanySearch represents the model behind the search form about `common\models\EmployeeCompany`.
 */
class CashboxSearch extends Cashbox
{
    /**
     * search query
     * @var ActiveQuery
     */
    private $_query;

    public function setQuery(ActiveQuery $query)
    {
        $this->_query = $query;
    }

    public function getQuery() : ActiveQuery
    {
        if (!isset($this->_query)) {
            $this->_query = self::find()->andWhere([
                'cashbox.company_id' => $this->company_id,
            ]);
        }

        return $this->_query;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->getQuery();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'is_main' => SORT_DESC,
                ],
                'attributes' => [
                    'name',
                    'is_main',
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cashbox.responsible_employee_id' => $this->responsible_employee_id,
        ]);

        return $dataProvider;
    }
}
